// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package uk.co.senab.photoview;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.util.Log;

// Referenced classes of package uk.co.senab.photoview:
//            PhotoViewAttacher, ScrollerProxy, Compat

private class mScroller
    implements Runnable
{

    private int mCurrentX;
    private int mCurrentY;
    private final ScrollerProxy mScroller;
    final PhotoViewAttacher this$0;

    public void cancelFling()
    {
        if (PhotoViewAttacher.DEBUG)
        {
            Log.d("PhotoViewAttacher", "Cancel Fling");
        }
        mScroller.forceFinished(true);
    }

    public void fling(int i, int j, int k, int l)
    {
        RectF rectf = getDisplayRect();
        if (rectf != null)
        {
            int i1 = Math.round(-rectf.left);
            int j1;
            int k1;
            int l1;
            if ((float)i < rectf.width())
            {
                j1 = Math.round(rectf.width() - (float)i);
                i = 0;
            } else
            {
                j1 = i1;
                i = i1;
            }
            k1 = Math.round(-rectf.top);
            if ((float)j < rectf.height())
            {
                l1 = Math.round(rectf.height() - (float)j);
                j = 0;
            } else
            {
                l1 = k1;
                j = k1;
            }
            mCurrentX = i1;
            mCurrentY = k1;
            if (PhotoViewAttacher.DEBUG)
            {
                Log.d("PhotoViewAttacher", (new StringBuilder("fling. StartX:")).append(i1).append(" StartY:").append(k1).append(" MaxX:").append(j1).append(" MaxY:").append(l1).toString());
            }
            if (i1 != j1 || k1 != l1)
            {
                mScroller.fling(i1, k1, k, l, i, j1, j, l1, 0, 0);
                return;
            }
        }
    }

    public void run()
    {
        android.widget.ImageView imageview = getImageView();
        if (imageview != null && mScroller.computeScrollOffset())
        {
            int i = mScroller.getCurrX();
            int j = mScroller.getCurrY();
            if (PhotoViewAttacher.DEBUG)
            {
                Log.d("PhotoViewAttacher", (new StringBuilder("fling run(). CurrentX:")).append(mCurrentX).append(" CurrentY:").append(mCurrentY).append(" NewX:").append(i).append(" NewY:").append(j).toString());
            }
            PhotoViewAttacher.access$0(PhotoViewAttacher.this).postTranslate(mCurrentX - i, mCurrentY - j);
            PhotoViewAttacher.access$2(PhotoViewAttacher.this, getDisplayMatrix());
            mCurrentX = i;
            mCurrentY = j;
            Compat.postOnAnimation(imageview, this);
        }
    }

    public (Context context)
    {
        this$0 = PhotoViewAttacher.this;
        super();
        mScroller = ScrollerProxy.getScroller(context);
    }
}
