// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package uk.co.senab.photoview;

import android.content.Context;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;

public abstract class VersionedGestureDetector
{
    private static class CupcakeDetector extends VersionedGestureDetector
    {

        private boolean mIsDragging;
        float mLastTouchX;
        float mLastTouchY;
        final float mMinimumVelocity;
        final float mTouchSlop;
        private VelocityTracker mVelocityTracker;

        float getActiveX(MotionEvent motionevent)
        {
            return motionevent.getX();
        }

        float getActiveY(MotionEvent motionevent)
        {
            return motionevent.getY();
        }

        public boolean isScaling()
        {
            return false;
        }

        public boolean onTouchEvent(MotionEvent motionevent)
        {
            boolean flag = false;
            motionevent.getAction();
            JVM INSTR tableswitch 0 3: default 36
        //                       0 38
        //                       1 212
        //                       2 78
        //                       3 191;
               goto _L1 _L2 _L3 _L4 _L5
_L1:
            return true;
_L2:
            mVelocityTracker = VelocityTracker.obtain();
            mVelocityTracker.addMovement(motionevent);
            mLastTouchX = getActiveX(motionevent);
            mLastTouchY = getActiveY(motionevent);
            mIsDragging = false;
            return true;
_L4:
            float f = getActiveX(motionevent);
            float f2 = getActiveY(motionevent);
            float f4 = f - mLastTouchX;
            float f5 = f2 - mLastTouchY;
            if (!mIsDragging)
            {
                if (FloatMath.sqrt(f4 * f4 + f5 * f5) >= mTouchSlop)
                {
                    flag = true;
                }
                mIsDragging = flag;
            }
            if (mIsDragging)
            {
                mListener.onDrag(f4, f5);
                mLastTouchX = f;
                mLastTouchY = f2;
                if (mVelocityTracker != null)
                {
                    mVelocityTracker.addMovement(motionevent);
                    return true;
                }
            }
            continue; /* Loop/switch isn't completed */
_L5:
            if (mVelocityTracker != null)
            {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
                return true;
            }
            if (true) goto _L1; else goto _L3
_L3:
            if (mIsDragging && mVelocityTracker != null)
            {
                mLastTouchX = getActiveX(motionevent);
                mLastTouchY = getActiveY(motionevent);
                mVelocityTracker.addMovement(motionevent);
                mVelocityTracker.computeCurrentVelocity(1000);
                float f1 = mVelocityTracker.getXVelocity();
                float f3 = mVelocityTracker.getYVelocity();
                if (Math.max(Math.abs(f1), Math.abs(f3)) >= mMinimumVelocity)
                {
                    mListener.onFling(mLastTouchX, mLastTouchY, -f1, -f3);
                }
            }
            if (mVelocityTracker != null)
            {
                mVelocityTracker.recycle();
                mVelocityTracker = null;
                return true;
            }
            if (true) goto _L1; else goto _L6
_L6:
        }

        public CupcakeDetector(Context context)
        {
            context = ViewConfiguration.get(context);
            mMinimumVelocity = context.getScaledMinimumFlingVelocity();
            mTouchSlop = context.getScaledTouchSlop();
        }
    }

    private static class EclairDetector extends CupcakeDetector
    {

        private static final int INVALID_POINTER_ID = -1;
        private int mActivePointerId;
        private int mActivePointerIndex;

        float getActiveX(MotionEvent motionevent)
        {
            float f;
            try
            {
                f = motionevent.getX(mActivePointerIndex);
            }
            catch (Exception exception)
            {
                return motionevent.getX();
            }
            return f;
        }

        float getActiveY(MotionEvent motionevent)
        {
            float f;
            try
            {
                f = motionevent.getY(mActivePointerIndex);
            }
            catch (Exception exception)
            {
                return motionevent.getY();
            }
            return f;
        }

        public boolean onTouchEvent(MotionEvent motionevent)
        {
            boolean flag = false;
            motionevent.getAction() & 0xff;
            JVM INSTR tableswitch 0 6: default 52
        //                       0 82
        //                       1 94
        //                       2 52
        //                       3 94
        //                       4 52
        //                       5 52
        //                       6 102;
               goto _L1 _L2 _L3 _L1 _L3 _L1 _L1 _L4
_L1:
            int i = ((flag) ? 1 : 0);
            if (mActivePointerId != -1)
            {
                i = mActivePointerId;
            }
            mActivePointerIndex = motionevent.findPointerIndex(i);
            return super.onTouchEvent(motionevent);
_L2:
            mActivePointerId = motionevent.getPointerId(0);
            continue; /* Loop/switch isn't completed */
_L3:
            mActivePointerId = -1;
            continue; /* Loop/switch isn't completed */
_L4:
            int j = (motionevent.getAction() & 0xff00) >> 8;
            if (motionevent.getPointerId(j) == mActivePointerId)
            {
                if (j == 0)
                {
                    j = 1;
                } else
                {
                    j = 0;
                }
                mActivePointerId = motionevent.getPointerId(j);
                mLastTouchX = motionevent.getX(j);
                mLastTouchY = motionevent.getY(j);
            }
            if (true) goto _L1; else goto _L5
_L5:
        }

        public EclairDetector(Context context)
        {
            super(context);
            mActivePointerId = -1;
            mActivePointerIndex = 0;
        }
    }

    private static class FroyoDetector extends EclairDetector
    {

        private final ScaleGestureDetector mDetector;
        private final android.view.ScaleGestureDetector.OnScaleGestureListener mScaleListener = new _cls1();

        public boolean isScaling()
        {
            return mDetector.isInProgress();
        }

        public boolean onTouchEvent(MotionEvent motionevent)
        {
            mDetector.onTouchEvent(motionevent);
            return super.onTouchEvent(motionevent);
        }

        public FroyoDetector(Context context)
        {
            super(context);
            class _cls1
                implements android.view.ScaleGestureDetector.OnScaleGestureListener
            {

                final FroyoDetector this$1;

                public boolean onScale(ScaleGestureDetector scalegesturedetector)
                {
                    mListener.onScale(scalegesturedetector.getScaleFactor(), scalegesturedetector.getFocusX(), scalegesturedetector.getFocusY());
                    return true;
                }

                public boolean onScaleBegin(ScaleGestureDetector scalegesturedetector)
                {
                    return true;
                }

                public void onScaleEnd(ScaleGestureDetector scalegesturedetector)
                {
                }

                _cls1()
                {
                    this$1 = FroyoDetector.this;
                    super();
                }
            }

            mDetector = new ScaleGestureDetector(context, mScaleListener);
        }
    }

    public static interface OnGestureListener
    {

        public abstract void onDrag(float f, float f1);

        public abstract void onFling(float f, float f1, float f2, float f3);

        public abstract void onScale(float f, float f1, float f2);
    }


    static final String LOG_TAG = "VersionedGestureDetector";
    OnGestureListener mListener;

    public VersionedGestureDetector()
    {
    }

    public static VersionedGestureDetector newInstance(Context context, OnGestureListener ongesturelistener)
    {
        int i = android.os.Build.VERSION.SDK_INT;
        if (i < 5)
        {
            context = new CupcakeDetector(context);
        } else
        if (i < 8)
        {
            context = new EclairDetector(context);
        } else
        {
            context = new FroyoDetector(context);
        }
        context.mListener = ongesturelistener;
        return context;
    }

    public abstract boolean isScaling();

    public abstract boolean onTouchEvent(MotionEvent motionevent);
}
