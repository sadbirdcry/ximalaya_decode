// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package uk.co.senab.photoview;

import android.graphics.Matrix;

// Referenced classes of package uk.co.senab.photoview:
//            PhotoViewAttacher, Compat

private class mDeltaScale
    implements Runnable
{

    static final float ANIMATION_SCALE_PER_ITERATION_IN = 1.07F;
    static final float ANIMATION_SCALE_PER_ITERATION_OUT = 0.93F;
    private final float mDeltaScale;
    private final float mFocalX;
    private final float mFocalY;
    private final float mTargetZoom;
    final PhotoViewAttacher this$0;

    public void run()
    {
        float f;
label0:
        {
            android.widget.ImageView imageview = getImageView();
            if (imageview != null)
            {
                PhotoViewAttacher.access$0(PhotoViewAttacher.this).postScale(mDeltaScale, mDeltaScale, mFocalX, mFocalY);
                PhotoViewAttacher.access$1(PhotoViewAttacher.this);
                f = getScale();
                if ((mDeltaScale <= 1.0F || f >= mTargetZoom) && (mDeltaScale >= 1.0F || mTargetZoom >= f))
                {
                    break label0;
                }
                Compat.postOnAnimation(imageview, this);
            }
            return;
        }
        f = mTargetZoom / f;
        PhotoViewAttacher.access$0(PhotoViewAttacher.this).postScale(f, f, mFocalX, mFocalY);
        PhotoViewAttacher.access$1(PhotoViewAttacher.this);
    }

    public (float f, float f1, float f2, float f3)
    {
        this$0 = PhotoViewAttacher.this;
        super();
        mTargetZoom = f1;
        mFocalX = f2;
        mFocalY = f3;
        if (f < f1)
        {
            mDeltaScale = 1.07F;
            return;
        } else
        {
            mDeltaScale = 0.93F;
            return;
        }
    }
}
