// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package uk.co.senab.photoview;

import android.content.Context;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;

// Referenced classes of package uk.co.senab.photoview:
//            IPhotoView, PhotoViewAttacher

public class PhotoView extends ImageView
    implements IPhotoView
{

    private final PhotoViewAttacher mAttacher;
    private android.widget.ImageView.ScaleType mPendingScaleType;

    public PhotoView(Context context)
    {
        this(context, null);
    }

    public PhotoView(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
    }

    public PhotoView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        super.setScaleType(android.widget.ImageView.ScaleType.MATRIX);
        mAttacher = new PhotoViewAttacher(this);
        if (mPendingScaleType != null)
        {
            setScaleType(mPendingScaleType);
            mPendingScaleType = null;
        }
    }

    public boolean canZoom()
    {
        return mAttacher.canZoom();
    }

    public RectF getDisplayRect()
    {
        return mAttacher.getDisplayRect();
    }

    public float getMaxScale()
    {
        return mAttacher.getMaxScale();
    }

    public float getMidScale()
    {
        return mAttacher.getMidScale();
    }

    public float getMinScale()
    {
        return mAttacher.getMinScale();
    }

    public float getScale()
    {
        return mAttacher.getScale();
    }

    public android.widget.ImageView.ScaleType getScaleType()
    {
        return mAttacher.getScaleType();
    }

    protected void onDetachedFromWindow()
    {
        mAttacher.cleanup();
        super.onDetachedFromWindow();
    }

    public void setAllowParentInterceptOnEdge(boolean flag)
    {
        mAttacher.setAllowParentInterceptOnEdge(flag);
    }

    public void setImageDrawable(Drawable drawable)
    {
        super.setImageDrawable(drawable);
        if (mAttacher != null)
        {
            mAttacher.update();
        }
    }

    public void setImageResource(int i)
    {
        super.setImageResource(i);
        if (mAttacher != null)
        {
            mAttacher.update();
        }
    }

    public void setImageURI(Uri uri)
    {
        super.setImageURI(uri);
        if (mAttacher != null)
        {
            mAttacher.update();
        }
    }

    public void setMaxScale(float f)
    {
        mAttacher.setMaxScale(f);
    }

    public void setMidScale(float f)
    {
        mAttacher.setMidScale(f);
    }

    public void setMinScale(float f)
    {
        mAttacher.setMinScale(f);
    }

    public void setOnLongClickListener(android.view.View.OnLongClickListener onlongclicklistener)
    {
        mAttacher.setOnLongClickListener(onlongclicklistener);
    }

    public void setOnMatrixChangeListener(PhotoViewAttacher.OnMatrixChangedListener onmatrixchangedlistener)
    {
        mAttacher.setOnMatrixChangeListener(onmatrixchangedlistener);
    }

    public void setOnPhotoTapListener(PhotoViewAttacher.OnPhotoTapListener onphototaplistener)
    {
        mAttacher.setOnPhotoTapListener(onphototaplistener);
    }

    public void setOnViewTapListener(PhotoViewAttacher.OnViewTapListener onviewtaplistener)
    {
        mAttacher.setOnViewTapListener(onviewtaplistener);
    }

    public void setScaleType(android.widget.ImageView.ScaleType scaletype)
    {
        if (mAttacher != null)
        {
            mAttacher.setScaleType(scaletype);
            return;
        } else
        {
            mPendingScaleType = scaletype;
            return;
        }
    }

    public void setZoomable(boolean flag)
    {
        mAttacher.setZoomable(flag);
    }

    public void zoomTo(float f, float f1, float f2)
    {
        mAttacher.zoomTo(f, f1, f2);
    }
}
