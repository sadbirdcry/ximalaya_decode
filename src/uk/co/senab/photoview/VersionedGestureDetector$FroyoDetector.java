// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package uk.co.senab.photoview;

import android.content.Context;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

// Referenced classes of package uk.co.senab.photoview:
//            VersionedGestureDetector

private static class mScaleListener extends 
{

    private final ScaleGestureDetector mDetector;
    private final android.view.ener mScaleListener = new _cls1();

    public boolean isScaling()
    {
        return mDetector.isInProgress();
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        mDetector.onTouchEvent(motionevent);
        return super.onTouchEvent(motionevent);
    }

    public _cls1.this._cls1(Context context)
    {
        super(context);
        class _cls1
            implements android.view.ScaleGestureDetector.OnScaleGestureListener
        {

            final VersionedGestureDetector.FroyoDetector this$1;

            public boolean onScale(ScaleGestureDetector scalegesturedetector)
            {
                mListener.onScale(scalegesturedetector.getScaleFactor(), scalegesturedetector.getFocusX(), scalegesturedetector.getFocusY());
                return true;
            }

            public boolean onScaleBegin(ScaleGestureDetector scalegesturedetector)
            {
                return true;
            }

            public void onScaleEnd(ScaleGestureDetector scalegesturedetector)
            {
            }

            _cls1()
            {
                this$1 = VersionedGestureDetector.FroyoDetector.this;
                super();
            }
        }

        mDetector = new ScaleGestureDetector(context, mScaleListener);
    }
}
