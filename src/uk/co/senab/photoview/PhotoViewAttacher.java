// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package uk.co.senab.photoview;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

// Referenced classes of package uk.co.senab.photoview:
//            IPhotoView, VersionedGestureDetector, PhotoView, Compat, 
//            ScrollerProxy

public class PhotoViewAttacher
    implements android.view.GestureDetector.OnDoubleTapListener, android.view.View.OnTouchListener, android.view.ViewTreeObserver.OnGlobalLayoutListener, IPhotoView, VersionedGestureDetector.OnGestureListener
{
    private class AnimatedZoomRunnable
        implements Runnable
    {

        static final float ANIMATION_SCALE_PER_ITERATION_IN = 1.07F;
        static final float ANIMATION_SCALE_PER_ITERATION_OUT = 0.93F;
        private final float mDeltaScale;
        private final float mFocalX;
        private final float mFocalY;
        private final float mTargetZoom;
        final PhotoViewAttacher this$0;

        public void run()
        {
            float f;
label0:
            {
                ImageView imageview = getImageView();
                if (imageview != null)
                {
                    mSuppMatrix.postScale(mDeltaScale, mDeltaScale, mFocalX, mFocalY);
                    checkAndDisplayMatrix();
                    f = getScale();
                    if ((mDeltaScale <= 1.0F || f >= mTargetZoom) && (mDeltaScale >= 1.0F || mTargetZoom >= f))
                    {
                        break label0;
                    }
                    Compat.postOnAnimation(imageview, this);
                }
                return;
            }
            f = mTargetZoom / f;
            mSuppMatrix.postScale(f, f, mFocalX, mFocalY);
            checkAndDisplayMatrix();
        }

        public AnimatedZoomRunnable(float f, float f1, float f2, float f3)
        {
            this$0 = PhotoViewAttacher.this;
            super();
            mTargetZoom = f1;
            mFocalX = f2;
            mFocalY = f3;
            if (f < f1)
            {
                mDeltaScale = 1.07F;
                return;
            } else
            {
                mDeltaScale = 0.93F;
                return;
            }
        }
    }

    private class FlingRunnable
        implements Runnable
    {

        private int mCurrentX;
        private int mCurrentY;
        private final ScrollerProxy mScroller;
        final PhotoViewAttacher this$0;

        public void cancelFling()
        {
            if (PhotoViewAttacher.DEBUG)
            {
                Log.d("PhotoViewAttacher", "Cancel Fling");
            }
            mScroller.forceFinished(true);
        }

        public void fling(int i, int j, int k, int l)
        {
            RectF rectf = getDisplayRect();
            if (rectf != null)
            {
                int i1 = Math.round(-rectf.left);
                int j1;
                int k1;
                int l1;
                if ((float)i < rectf.width())
                {
                    j1 = Math.round(rectf.width() - (float)i);
                    i = 0;
                } else
                {
                    j1 = i1;
                    i = i1;
                }
                k1 = Math.round(-rectf.top);
                if ((float)j < rectf.height())
                {
                    l1 = Math.round(rectf.height() - (float)j);
                    j = 0;
                } else
                {
                    l1 = k1;
                    j = k1;
                }
                mCurrentX = i1;
                mCurrentY = k1;
                if (PhotoViewAttacher.DEBUG)
                {
                    Log.d("PhotoViewAttacher", (new StringBuilder("fling. StartX:")).append(i1).append(" StartY:").append(k1).append(" MaxX:").append(j1).append(" MaxY:").append(l1).toString());
                }
                if (i1 != j1 || k1 != l1)
                {
                    mScroller.fling(i1, k1, k, l, i, j1, j, l1, 0, 0);
                    return;
                }
            }
        }

        public void run()
        {
            ImageView imageview = getImageView();
            if (imageview != null && mScroller.computeScrollOffset())
            {
                int i = mScroller.getCurrX();
                int j = mScroller.getCurrY();
                if (PhotoViewAttacher.DEBUG)
                {
                    Log.d("PhotoViewAttacher", (new StringBuilder("fling run(). CurrentX:")).append(mCurrentX).append(" CurrentY:").append(mCurrentY).append(" NewX:").append(i).append(" NewY:").append(j).toString());
                }
                mSuppMatrix.postTranslate(mCurrentX - i, mCurrentY - j);
                setImageViewMatrix(getDisplayMatrix());
                mCurrentX = i;
                mCurrentY = j;
                Compat.postOnAnimation(imageview, this);
            }
        }

        public FlingRunnable(Context context)
        {
            this$0 = PhotoViewAttacher.this;
            super();
            mScroller = ScrollerProxy.getScroller(context);
        }
    }

    public static interface OnMatrixChangedListener
    {

        public abstract void onMatrixChanged(RectF rectf);
    }

    public static interface OnPhotoTapListener
    {

        public abstract void onPhotoTap(View view, float f, float f1);
    }

    public static interface OnViewTapListener
    {

        public abstract void onViewTap(View view, float f, float f1);
    }


    private static int $SWITCH_TABLE$android$widget$ImageView$ScaleType[];
    static final boolean DEBUG = Log.isLoggable("PhotoViewAttacher", 3);
    public static final float DEFAULT_MAX_SCALE = 3F;
    public static final float DEFAULT_MID_SCALE = 1.75F;
    public static final float DEFAULT_MIN_SCALE = 1F;
    static final int EDGE_BOTH = 2;
    static final int EDGE_LEFT = 0;
    static final int EDGE_NONE = -1;
    static final int EDGE_RIGHT = 1;
    static final String LOG_TAG = "PhotoViewAttacher";
    private boolean mAllowParentInterceptOnEdge;
    private final Matrix mBaseMatrix = new Matrix();
    private FlingRunnable mCurrentFlingRunnable;
    private final RectF mDisplayRect = new RectF();
    private final Matrix mDrawMatrix = new Matrix();
    private GestureDetector mGestureDetector;
    private WeakReference mImageView;
    private int mIvBottom;
    private int mIvLeft;
    private int mIvRight;
    private int mIvTop;
    private android.view.View.OnLongClickListener mLongClickListener;
    private OnMatrixChangedListener mMatrixChangeListener;
    private final float mMatrixValues[] = new float[9];
    private float mMaxScale;
    private float mMidScale;
    private float mMinScale;
    private OnPhotoTapListener mPhotoTapListener;
    private VersionedGestureDetector mScaleDragDetector;
    private android.widget.ImageView.ScaleType mScaleType;
    private int mScrollEdge;
    private final Matrix mSuppMatrix = new Matrix();
    private OnViewTapListener mViewTapListener;
    private ViewTreeObserver mViewTreeObserver;
    private boolean mZoomEnabled;

    static int[] $SWITCH_TABLE$android$widget$ImageView$ScaleType()
    {
        int ai[] = $SWITCH_TABLE$android$widget$ImageView$ScaleType;
        if (ai != null)
        {
            return ai;
        }
        ai = new int[android.widget.ImageView.ScaleType.values().length];
        try
        {
            ai[android.widget.ImageView.ScaleType.CENTER.ordinal()] = 1;
        }
        catch (NoSuchFieldError nosuchfielderror7) { }
        try
        {
            ai[android.widget.ImageView.ScaleType.CENTER_CROP.ordinal()] = 2;
        }
        catch (NoSuchFieldError nosuchfielderror6) { }
        try
        {
            ai[android.widget.ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 3;
        }
        catch (NoSuchFieldError nosuchfielderror5) { }
        try
        {
            ai[android.widget.ImageView.ScaleType.FIT_CENTER.ordinal()] = 4;
        }
        catch (NoSuchFieldError nosuchfielderror4) { }
        try
        {
            ai[android.widget.ImageView.ScaleType.FIT_END.ordinal()] = 5;
        }
        catch (NoSuchFieldError nosuchfielderror3) { }
        try
        {
            ai[android.widget.ImageView.ScaleType.FIT_START.ordinal()] = 6;
        }
        catch (NoSuchFieldError nosuchfielderror2) { }
        try
        {
            ai[android.widget.ImageView.ScaleType.FIT_XY.ordinal()] = 7;
        }
        catch (NoSuchFieldError nosuchfielderror1) { }
        try
        {
            ai[android.widget.ImageView.ScaleType.MATRIX.ordinal()] = 8;
        }
        catch (NoSuchFieldError nosuchfielderror) { }
        $SWITCH_TABLE$android$widget$ImageView$ScaleType = ai;
        return ai;
    }

    public PhotoViewAttacher(ImageView imageview)
    {
        mMinScale = 1.0F;
        mMidScale = 1.75F;
        mMaxScale = 3F;
        mAllowParentInterceptOnEdge = true;
        mScrollEdge = 2;
        mScaleType = android.widget.ImageView.ScaleType.FIT_CENTER;
        mImageView = new WeakReference(imageview);
        imageview.setOnTouchListener(this);
        mViewTreeObserver = imageview.getViewTreeObserver();
        mViewTreeObserver.addOnGlobalLayoutListener(this);
        setImageViewScaleTypeMatrix(imageview);
        if (!imageview.isInEditMode())
        {
            mScaleDragDetector = VersionedGestureDetector.newInstance(imageview.getContext(), this);
            mGestureDetector = new GestureDetector(imageview.getContext(), new _cls1());
            mGestureDetector.setOnDoubleTapListener(this);
            setZoomable(true);
        }
    }

    private void cancelFling()
    {
        if (mCurrentFlingRunnable != null)
        {
            mCurrentFlingRunnable.cancelFling();
            mCurrentFlingRunnable = null;
        }
    }

    private void checkAndDisplayMatrix()
    {
        checkMatrixBounds();
        setImageViewMatrix(getDisplayMatrix());
    }

    private void checkImageViewScaleType()
    {
        ImageView imageview = getImageView();
        if (imageview != null && !(imageview instanceof PhotoView) && imageview.getScaleType() != android.widget.ImageView.ScaleType.MATRIX)
        {
            throw new IllegalStateException("The ImageView's ScaleType has been changed since attaching a PhotoViewAttacher");
        } else
        {
            return;
        }
    }

    private void checkMatrixBounds()
    {
        float f1;
        ImageView imageview;
        f1 = 0.0F;
        imageview = getImageView();
        if (imageview != null) goto _L2; else goto _L1
_L1:
        RectF rectf;
        return;
_L2:
        if ((rectf = getDisplayRect(getDisplayMatrix())) == null) goto _L1; else goto _L3
_L3:
        float f;
        float f2;
        int i;
        f = rectf.height();
        f2 = rectf.width();
        i = imageview.getHeight();
        if (f > (float)i) goto _L5; else goto _L4
_L4:
        $SWITCH_TABLE$android$widget$ImageView$ScaleType()[mScaleType.ordinal()];
        JVM INSTR tableswitch 5 6: default 88
    //                   5 192
    //                   6 182;
           goto _L6 _L7 _L8
_L6:
        f = ((float)i - f) / 2.0F - rectf.top;
_L12:
        i = imageview.getWidth();
        if (f2 > (float)i)
        {
            break MISSING_BLOCK_LABEL_277;
        }
        $SWITCH_TABLE$android$widget$ImageView$ScaleType()[mScaleType.ordinal()];
        JVM INSTR tableswitch 5 6: default 152
    //                   5 262
    //                   6 252;
           goto _L9 _L10 _L11
_L9:
        f1 = ((float)i - f2) / 2.0F - rectf.left;
_L13:
        mScrollEdge = 2;
_L14:
        mSuppMatrix.postTranslate(f1, f);
        return;
_L8:
        f = -rectf.top;
          goto _L12
_L7:
        f = (float)i - f - rectf.top;
          goto _L12
_L5:
        if (rectf.top > 0.0F)
        {
            f = -rectf.top;
        } else
        if (rectf.bottom < (float)i)
        {
            f = (float)i - rectf.bottom;
        } else
        {
            f = 0.0F;
        }
          goto _L12
_L11:
        f1 = -rectf.left;
          goto _L13
_L10:
        f1 = (float)i - f2 - rectf.left;
          goto _L13
        if (rectf.left > 0.0F)
        {
            mScrollEdge = 0;
            f1 = -rectf.left;
        } else
        if (rectf.right < (float)i)
        {
            f1 = (float)i - rectf.right;
            mScrollEdge = 1;
        } else
        {
            mScrollEdge = -1;
        }
          goto _L14
    }

    private static void checkZoomLevels(float f, float f1, float f2)
    {
        if (f >= f1)
        {
            throw new IllegalArgumentException("MinZoom should be less than MidZoom");
        }
        if (f1 >= f2)
        {
            throw new IllegalArgumentException("MidZoom should be less than MaxZoom");
        } else
        {
            return;
        }
    }

    private RectF getDisplayRect(Matrix matrix)
    {
        Object obj = getImageView();
        if (obj != null)
        {
            obj = ((ImageView) (obj)).getDrawable();
            if (obj != null)
            {
                mDisplayRect.set(0.0F, 0.0F, ((Drawable) (obj)).getIntrinsicWidth(), ((Drawable) (obj)).getIntrinsicHeight());
                matrix.mapRect(mDisplayRect);
                return mDisplayRect;
            }
        }
        return null;
    }

    private float getValue(Matrix matrix, int i)
    {
        matrix.getValues(mMatrixValues);
        return mMatrixValues[i];
    }

    private static boolean hasDrawable(ImageView imageview)
    {
        return imageview != null && imageview.getDrawable() != null;
    }

    private static boolean isSupportedScaleType(android.widget.ImageView.ScaleType scaletype)
    {
        if (scaletype == null)
        {
            return false;
        }
        switch ($SWITCH_TABLE$android$widget$ImageView$ScaleType()[scaletype.ordinal()])
        {
        default:
            return true;

        case 8: // '\b'
            throw new IllegalArgumentException((new StringBuilder(String.valueOf(scaletype.name()))).append(" is not supported in PhotoView").toString());
        }
    }

    private void resetMatrix()
    {
        mSuppMatrix.reset();
        setImageViewMatrix(getDisplayMatrix());
        checkMatrixBounds();
    }

    private void setImageViewMatrix(Matrix matrix)
    {
        ImageView imageview = getImageView();
        if (imageview != null)
        {
            checkImageViewScaleType();
            imageview.setImageMatrix(matrix);
            if (mMatrixChangeListener != null)
            {
                matrix = getDisplayRect(matrix);
                if (matrix != null)
                {
                    mMatrixChangeListener.onMatrixChanged(matrix);
                }
            }
        }
    }

    private static void setImageViewScaleTypeMatrix(ImageView imageview)
    {
        if (imageview != null && !(imageview instanceof PhotoView))
        {
            imageview.setScaleType(android.widget.ImageView.ScaleType.MATRIX);
        }
    }

    private void updateBaseMatrix(Drawable drawable)
    {
        float f;
        float f1;
        float f2;
        float f3;
        int i;
        int j;
        ImageView imageview = getImageView();
        if (imageview == null || drawable == null)
        {
            return;
        }
        f = imageview.getWidth();
        f1 = imageview.getHeight();
        i = drawable.getIntrinsicWidth();
        j = drawable.getIntrinsicHeight();
        mBaseMatrix.reset();
        f2 = f / (float)i;
        f3 = f1 / (float)j;
        if (mScaleType != android.widget.ImageView.ScaleType.CENTER) goto _L2; else goto _L1
_L1:
        mBaseMatrix.postTranslate((f - (float)i) / 2.0F, (f1 - (float)j) / 2.0F);
_L4:
        resetMatrix();
        return;
_L2:
        if (mScaleType == android.widget.ImageView.ScaleType.CENTER_CROP)
        {
            f2 = Math.max(f2, f3);
            mBaseMatrix.postScale(f2, f2);
            mBaseMatrix.postTranslate((f - (float)i * f2) / 2.0F, (f1 - (float)j * f2) / 2.0F);
            continue; /* Loop/switch isn't completed */
        }
        if (mScaleType != android.widget.ImageView.ScaleType.CENTER_INSIDE)
        {
            break; /* Loop/switch isn't completed */
        }
        f2 = Math.min(1.0F, Math.min(f2, f3));
        mBaseMatrix.postScale(f2, f2);
        mBaseMatrix.postTranslate((f - (float)i * f2) / 2.0F, (f1 - (float)j * f2) / 2.0F);
        if (true) goto _L4; else goto _L3
_L3:
        drawable = new RectF(0.0F, 0.0F, i, j);
        RectF rectf = new RectF(0.0F, 0.0F, f, f1);
        switch ($SWITCH_TABLE$android$widget$ImageView$ScaleType()[mScaleType.ordinal()])
        {
        case 4: // '\004'
            mBaseMatrix.setRectToRect(drawable, rectf, android.graphics.Matrix.ScaleToFit.CENTER);
            break;

        case 6: // '\006'
            mBaseMatrix.setRectToRect(drawable, rectf, android.graphics.Matrix.ScaleToFit.START);
            break;

        case 5: // '\005'
            mBaseMatrix.setRectToRect(drawable, rectf, android.graphics.Matrix.ScaleToFit.END);
            break;

        case 7: // '\007'
            mBaseMatrix.setRectToRect(drawable, rectf, android.graphics.Matrix.ScaleToFit.FILL);
            break;
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    public final boolean canZoom()
    {
        return mZoomEnabled;
    }

    public final void cleanup()
    {
        if (mImageView != null && mImageView.get() != null)
        {
            ((ImageView)mImageView.get()).getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
        mViewTreeObserver = null;
        mMatrixChangeListener = null;
        mPhotoTapListener = null;
        mViewTapListener = null;
        mImageView = null;
    }

    protected Matrix getDisplayMatrix()
    {
        mDrawMatrix.set(mBaseMatrix);
        mDrawMatrix.postConcat(mSuppMatrix);
        return mDrawMatrix;
    }

    public final RectF getDisplayRect()
    {
        checkMatrixBounds();
        return getDisplayRect(getDisplayMatrix());
    }

    public final ImageView getImageView()
    {
        ImageView imageview = null;
        if (mImageView != null)
        {
            imageview = (ImageView)mImageView.get();
        }
        if (imageview == null)
        {
            cleanup();
        }
        return imageview;
    }

    public float getMaxScale()
    {
        return mMaxScale;
    }

    public float getMidScale()
    {
        return mMidScale;
    }

    public float getMinScale()
    {
        return mMinScale;
    }

    public final float getScale()
    {
        return getValue(mSuppMatrix, 0);
    }

    public final android.widget.ImageView.ScaleType getScaleType()
    {
        return mScaleType;
    }

    public final boolean onDoubleTap(MotionEvent motionevent)
    {
        float f;
        float f1;
        float f2;
        f = getScale();
        f1 = motionevent.getX();
        f2 = motionevent.getY();
        if (f < mMidScale)
        {
            zoomTo(mMidScale, f1, f2);
            break MISSING_BLOCK_LABEL_82;
        }
        if (f >= mMidScale && f < mMaxScale)
        {
            zoomTo(mMaxScale, f1, f2);
            break MISSING_BLOCK_LABEL_82;
        }
        try
        {
            zoomTo(mMinScale, f1, f2);
        }
        // Misplaced declaration of an exception variable
        catch (MotionEvent motionevent) { }
        return true;
    }

    public final boolean onDoubleTapEvent(MotionEvent motionevent)
    {
        return false;
    }

    public final void onDrag(float f, float f1)
    {
        if (DEBUG)
        {
            Log.d("PhotoViewAttacher", String.format("onDrag: dx: %.2f. dy: %.2f", new Object[] {
                Float.valueOf(f), Float.valueOf(f1)
            }));
        }
        ImageView imageview = getImageView();
        if (imageview != null && hasDrawable(imageview))
        {
            mSuppMatrix.postTranslate(f, f1);
            checkAndDisplayMatrix();
            if (mAllowParentInterceptOnEdge && !mScaleDragDetector.isScaling() && (mScrollEdge == 2 || mScrollEdge == 0 && f >= 1.0F || mScrollEdge == 1 && f <= -1F))
            {
                imageview.getParent().requestDisallowInterceptTouchEvent(false);
            }
        }
    }

    public final void onFling(float f, float f1, float f2, float f3)
    {
        if (DEBUG)
        {
            Log.d("PhotoViewAttacher", (new StringBuilder("onFling. sX: ")).append(f).append(" sY: ").append(f1).append(" Vx: ").append(f2).append(" Vy: ").append(f3).toString());
        }
        ImageView imageview = getImageView();
        if (hasDrawable(imageview))
        {
            mCurrentFlingRunnable = new FlingRunnable(imageview.getContext());
            mCurrentFlingRunnable.fling(imageview.getWidth(), imageview.getHeight(), (int)f2, (int)f3);
            imageview.post(mCurrentFlingRunnable);
        }
    }

    public final void onGlobalLayout()
    {
        ImageView imageview = getImageView();
        if (imageview != null && mZoomEnabled)
        {
            int i = imageview.getTop();
            int j = imageview.getRight();
            int k = imageview.getBottom();
            int l = imageview.getLeft();
            if (i != mIvTop || k != mIvBottom || l != mIvLeft || j != mIvRight)
            {
                updateBaseMatrix(imageview.getDrawable());
                mIvTop = i;
                mIvRight = j;
                mIvBottom = k;
                mIvLeft = l;
            }
        }
    }

    public final void onScale(float f, float f1, float f2)
    {
        if (DEBUG)
        {
            Log.d("PhotoViewAttacher", String.format("onScale: scale: %.2f. fX: %.2f. fY: %.2f", new Object[] {
                Float.valueOf(f), Float.valueOf(f1), Float.valueOf(f2)
            }));
        }
        if (hasDrawable(getImageView()) && (getScale() < mMaxScale || f < 1.0F))
        {
            mSuppMatrix.postScale(f, f, f1, f2);
            checkAndDisplayMatrix();
        }
    }

    public final boolean onSingleTapConfirmed(MotionEvent motionevent)
    {
        ImageView imageview = getImageView();
        if (imageview != null)
        {
            if (mPhotoTapListener != null)
            {
                RectF rectf = getDisplayRect();
                if (rectf != null)
                {
                    float f1 = motionevent.getX();
                    float f = motionevent.getY();
                    if (rectf.contains(f1, f))
                    {
                        f1 = (f1 - rectf.left) / rectf.width();
                        f = (f - rectf.top) / rectf.height();
                        mPhotoTapListener.onPhotoTap(imageview, f1, f);
                        return true;
                    }
                }
            }
            if (mViewTapListener != null)
            {
                mViewTapListener.onViewTap(imageview, motionevent.getX(), motionevent.getY());
            }
        }
        return false;
    }

    public final boolean onTouch(View view, MotionEvent motionevent)
    {
        boolean flag;
        boolean flag1;
        flag = false;
        flag1 = false;
        if (!mZoomEnabled) goto _L2; else goto _L1
_L1:
        flag = flag1;
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 52
    //                   0 117
    //                   1 138
    //                   2 56
    //                   3 138;
           goto _L3 _L4 _L5 _L6 _L5
_L6:
        break; /* Loop/switch isn't completed */
_L3:
        flag = flag1;
_L8:
        flag1 = flag;
        if (mGestureDetector != null)
        {
            flag1 = flag;
            if (mGestureDetector.onTouchEvent(motionevent))
            {
                flag1 = true;
            }
        }
        flag = flag1;
        if (mScaleDragDetector != null)
        {
            flag = flag1;
            if (mScaleDragDetector.onTouchEvent(motionevent))
            {
                flag = true;
            }
        }
_L2:
        return flag;
_L4:
        view.getParent().requestDisallowInterceptTouchEvent(true);
        cancelFling();
        flag = flag1;
        continue; /* Loop/switch isn't completed */
_L5:
        flag = flag1;
        if (getScale() < mMinScale)
        {
            RectF rectf = getDisplayRect();
            flag = flag1;
            if (rectf != null)
            {
                view.post(new AnimatedZoomRunnable(getScale(), mMinScale, rectf.centerX(), rectf.centerY()));
                flag = true;
            }
        }
        if (true) goto _L8; else goto _L7
_L7:
    }

    public void setAllowParentInterceptOnEdge(boolean flag)
    {
        mAllowParentInterceptOnEdge = flag;
    }

    public void setMaxScale(float f)
    {
        checkZoomLevels(mMinScale, mMidScale, f);
        mMaxScale = f;
    }

    public void setMidScale(float f)
    {
        checkZoomLevels(mMinScale, f, mMaxScale);
        mMidScale = f;
    }

    public void setMinScale(float f)
    {
        checkZoomLevels(f, mMidScale, mMaxScale);
        mMinScale = f;
    }

    public final void setOnLongClickListener(android.view.View.OnLongClickListener onlongclicklistener)
    {
        mLongClickListener = onlongclicklistener;
    }

    public final void setOnMatrixChangeListener(OnMatrixChangedListener onmatrixchangedlistener)
    {
        mMatrixChangeListener = onmatrixchangedlistener;
    }

    public final void setOnPhotoTapListener(OnPhotoTapListener onphototaplistener)
    {
        mPhotoTapListener = onphototaplistener;
    }

    public final void setOnViewTapListener(OnViewTapListener onviewtaplistener)
    {
        mViewTapListener = onviewtaplistener;
    }

    public final void setScaleType(android.widget.ImageView.ScaleType scaletype)
    {
        if (isSupportedScaleType(scaletype) && scaletype != mScaleType)
        {
            mScaleType = scaletype;
            update();
        }
    }

    public final void setZoomable(boolean flag)
    {
        mZoomEnabled = flag;
        update();
    }

    public final void update()
    {
label0:
        {
            ImageView imageview = getImageView();
            if (imageview != null)
            {
                if (!mZoomEnabled)
                {
                    break label0;
                }
                setImageViewScaleTypeMatrix(imageview);
                updateBaseMatrix(imageview.getDrawable());
            }
            return;
        }
        resetMatrix();
    }

    public final void zoomTo(float f, float f1, float f2)
    {
        ImageView imageview = getImageView();
        if (imageview != null)
        {
            imageview.post(new AnimatedZoomRunnable(getScale(), f, f1, f2));
        }
    }







    private class _cls1 extends android.view.GestureDetector.SimpleOnGestureListener
    {

        final PhotoViewAttacher this$0;

        public void onLongPress(MotionEvent motionevent)
        {
            if (mLongClickListener != null)
            {
                mLongClickListener.onLongClick((View)mImageView.get());
            }
        }

        _cls1()
        {
            this$0 = PhotoViewAttacher.this;
            super();
        }
    }

}
