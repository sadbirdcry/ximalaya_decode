// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package uk.co.senab.photoview;

import android.content.Context;
import android.widget.OverScroller;
import android.widget.Scroller;

public abstract class ScrollerProxy
{
    private static class GingerScroller extends ScrollerProxy
    {

        private OverScroller mScroller;

        public boolean computeScrollOffset()
        {
            return mScroller.computeScrollOffset();
        }

        public void fling(int i, int j, int k, int l, int i1, int j1, int k1, 
                int l1, int i2, int j2)
        {
            mScroller.fling(i, j, k, l, i1, j1, k1, l1, i2, j2);
        }

        public void forceFinished(boolean flag)
        {
            mScroller.forceFinished(flag);
        }

        public int getCurrX()
        {
            return mScroller.getCurrX();
        }

        public int getCurrY()
        {
            return mScroller.getCurrY();
        }

        public GingerScroller(Context context)
        {
            mScroller = new OverScroller(context);
        }
    }

    private static class PreGingerScroller extends ScrollerProxy
    {

        private Scroller mScroller;

        public boolean computeScrollOffset()
        {
            return mScroller.computeScrollOffset();
        }

        public void fling(int i, int j, int k, int l, int i1, int j1, int k1, 
                int l1, int i2, int j2)
        {
            mScroller.fling(i, j, k, l, i1, j1, k1, l1);
        }

        public void forceFinished(boolean flag)
        {
            mScroller.forceFinished(flag);
        }

        public int getCurrX()
        {
            return mScroller.getCurrX();
        }

        public int getCurrY()
        {
            return mScroller.getCurrY();
        }

        public PreGingerScroller(Context context)
        {
            mScroller = new Scroller(context);
        }
    }


    public ScrollerProxy()
    {
    }

    public static ScrollerProxy getScroller(Context context)
    {
        if (android.os.Build.VERSION.SDK_INT < 9)
        {
            return new PreGingerScroller(context);
        } else
        {
            return new GingerScroller(context);
        }
    }

    public abstract boolean computeScrollOffset();

    public abstract void fling(int i, int j, int k, int l, int i1, int j1, int k1, 
            int l1, int i2, int j2);

    public abstract void forceFinished(boolean flag);

    public abstract int getCurrX();

    public abstract int getCurrY();
}
