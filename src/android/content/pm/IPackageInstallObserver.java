// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package android.content.pm;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPackageInstallObserver
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements IPackageInstallObserver
    {

        public static IPackageInstallObserver asInterface(IBinder ibinder)
        {
            return null;
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            return false;
        }

        public Stub()
        {
        }
    }


    public abstract void packageInstalled(String s, int i)
        throws RemoteException;
}
