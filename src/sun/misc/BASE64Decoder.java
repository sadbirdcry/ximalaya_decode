// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package sun.misc;

import com.rt.CEFormatException;
import com.rt.CEStreamExhausted;
import com.rt.CharacterDecoder;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PushbackInputStream;

public class BASE64Decoder extends CharacterDecoder
{

    private static final char pem_array[];
    private static final byte pem_convert_array[];
    byte decode_buffer[];

    public BASE64Decoder()
    {
        decode_buffer = new byte[4];
    }

    protected int bytesPerAtom()
    {
        return 4;
    }

    protected int bytesPerLine()
    {
        return 72;
    }

    protected void decodeAtom(PushbackInputStream pushbackinputstream, OutputStream outputstream, int i)
        throws IOException
    {
        byte byte0 = -1;
        if (i < 2)
        {
            throw new CEFormatException("BASE64Decoder: Not enough bytes for an atom.");
        }
        int j;
        do
        {
            j = pushbackinputstream.read();
            if (j == -1)
            {
                throw new CEStreamExhausted();
            }
        } while (j == 10 || j == 13);
        decode_buffer[0] = (byte)j;
        if (readFully(pushbackinputstream, decode_buffer, 1, i - 1) == -1)
        {
            throw new CEStreamExhausted();
        }
        if (i > 3 && decode_buffer[3] == 61)
        {
            i = 3;
        }
        int k;
        byte byte1;
        if (i > 2 && decode_buffer[2] == 61)
        {
            k = 2;
        } else
        {
            k = i;
        }
        j = byte0;
        k;
        JVM INSTR tableswitch 2 4: default 156
    //                   2 385
    //                   3 209
    //                   4 193;
           goto _L1 _L2 _L3 _L4
_L2:
        break MISSING_BLOCK_LABEL_385;
_L1:
        i = -1;
        j = -1;
        byte1 = -1;
          goto _L5
_L4:
        j = pem_convert_array[decode_buffer[3] & 0xff];
_L3:
        i = pem_convert_array[decode_buffer[2] & 0xff];
        byte0 = j;
_L6:
        j = pem_convert_array[decode_buffer[1] & 0xff];
        byte1 = pem_convert_array[decode_buffer[0] & 0xff];
_L5:
        switch (k)
        {
        default:
            return;

        case 2: // '\002'
            outputstream.write((byte)(byte1 << 2 & 0xfc | j >>> 4 & 3));
            return;

        case 3: // '\003'
            outputstream.write((byte)(byte1 << 2 & 0xfc | j >>> 4 & 3));
            outputstream.write((byte)(j << 4 & 0xf0 | i >>> 2 & 0xf));
            return;

        case 4: // '\004'
            outputstream.write((byte)(byte1 << 2 & 0xfc | j >>> 4 & 3));
            break;
        }
        outputstream.write((byte)(j << 4 & 0xf0 | i >>> 2 & 0xf));
        outputstream.write((byte)(byte0 & 0x3f | i << 6 & 0xc0));
        return;
        i = -1;
          goto _L6
    }

    static 
    {
        int i;
        boolean flag;
        flag = false;
        pem_array = (new char[] {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
            'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 
            'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 
            'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 
            'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', 
            '8', '9', '+', '/'
        });
        pem_convert_array = new byte[256];
        i = 0;
_L3:
        if (i < 255) goto _L2; else goto _L1
_L1:
        i = ((flag) ? 1 : 0);
_L4:
        if (i >= pem_array.length)
        {
            return;
        }
        break MISSING_BLOCK_LABEL_428;
_L2:
        pem_convert_array[i] = -1;
        i++;
          goto _L3
        pem_convert_array[pem_array[i]] = (byte)i;
        i++;
          goto _L4
    }
}
