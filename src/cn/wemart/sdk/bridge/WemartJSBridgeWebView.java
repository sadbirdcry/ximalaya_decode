// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package cn.wemart.sdk.bridge;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Looper;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import cn.wemart.sdk.b.a;
import cn.wemart.sdk.b.b;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package cn.wemart.sdk.bridge:
//            a, b, c, e, 
//            d

public class WemartJSBridgeWebView extends WebView
{
    class a extends WebViewClient
    {

        boolean a;
        final WemartJSBridgeWebView b;

        public void onPageFinished(WebView webview, String s)
        {
            super.onPageFinished(webview, s);
            if (a || s.startsWith("about:"))
            {
                a = false;
                if (b.a != null)
                {
                    cn.wemart.sdk.bridge.c.a(webview, b.a);
                }
                if (b.e != null)
                {
                    webview = b.e.iterator();
                    do
                    {
                        if (!webview.hasNext())
                        {
                            b.e = null;
                            return;
                        }
                        s = (cn.wemart.sdk.bridge.b)webview.next();
                        cn.wemart.sdk.bridge.WemartJSBridgeWebView.a(b, s);
                    } while (true);
                }
            }
        }

        public void onPageStarted(WebView webview, String s, Bitmap bitmap)
        {
            super.onPageStarted(webview, s, bitmap);
            a = true;
        }

        public void onReceivedError(WebView webview, int i, String s, String s1)
        {
            if (a);
        }

        public boolean shouldOverrideUrlLoading(WebView webview, String s)
        {
            String s1 = URLDecoder.decode(s, "UTF-8");
            s = s1;
_L2:
            cn.wemart.sdk.b.a.a("WemartJSBridge", (new StringBuilder("\u8C03\u7528 shouldOverrideUrlLoading, url = ")).append(s).toString());
            if (s.startsWith("wtjs://return/"))
            {
                cn.wemart.sdk.bridge.WemartJSBridgeWebView.a(b, s);
                return true;
            }
            break; /* Loop/switch isn't completed */
            UnsupportedEncodingException unsupportedencodingexception;
            unsupportedencodingexception;
            unsupportedencodingexception.printStackTrace();
            if (true) goto _L2; else goto _L1
_L1:
            if (s.startsWith("wtjs://"))
            {
                b.a();
                return true;
            } else
            {
                return super.shouldOverrideUrlLoading(webview, s);
            }
        }

        a()
        {
            b = WemartJSBridgeWebView.this;
            super();
        }
    }


    String a;
    Map b;
    Map c;
    d d;
    List e;
    long f;
    private final String g;

    public WemartJSBridgeWebView(Context context)
    {
        super(context);
        g = "WemartJSBridge";
        a = null;
        b = new HashMap();
        c = new HashMap();
        d = new cn.wemart.sdk.bridge.a();
        e = new ArrayList();
        f = 0L;
        a(context);
    }

    public WemartJSBridgeWebView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        g = "WemartJSBridge";
        a = null;
        b = new HashMap();
        c = new HashMap();
        d = new cn.wemart.sdk.bridge.a();
        e = new ArrayList();
        f = 0L;
        a(context);
    }

    public WemartJSBridgeWebView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        g = "WemartJSBridge";
        a = null;
        b = new HashMap();
        c = new HashMap();
        d = new cn.wemart.sdk.bridge.a();
        e = new ArrayList();
        f = 0L;
        a(context);
    }

    private void a(Context context)
    {
        b(context);
        setWebViewClient(new a());
        setWebChromeClient(new WebChromeClient());
        a("http://www.wemart.cn/v2/sdk/WemartJSBridge.js");
    }

    static void a(WemartJSBridgeWebView wemartjsbridgewebview, cn.wemart.sdk.bridge.b b1)
    {
        wemartjsbridgewebview.b(b1);
    }

    static void a(WemartJSBridgeWebView wemartjsbridgewebview, String s)
    {
        wemartjsbridgewebview.b(s);
    }

    private void a(cn.wemart.sdk.bridge.b b1)
    {
        if (e != null)
        {
            e.add(b1);
            return;
        } else
        {
            b(b1);
            return;
        }
    }

    private void a(String s, b b1, String s1)
    {
        cn.wemart.sdk.bridge.b b2 = new cn.wemart.sdk.bridge.b();
        if (!TextUtils.isEmpty(s))
        {
            b2.d(s);
        }
        if (b1 != null)
        {
            long l = f + 1L;
            f = l;
            s = String.format("JAVA_CB_%s", new Object[] {
                (new StringBuilder(String.valueOf(l))).append("_").append(SystemClock.currentThreadTimeMillis()).toString()
            });
            b.put(s, b1);
            b2.c(s);
        }
        if (!TextUtils.isEmpty(s1))
        {
            b2.e(s1);
        }
        a(b2);
    }

    private void b(Context context)
    {
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        WebSettings websettings = getSettings();
        websettings.setJavaScriptEnabled(true);
        websettings.setJavaScriptCanOpenWindowsAutomatically(true);
        websettings.setLayoutAlgorithm(android.webkit.WebSettings.LayoutAlgorithm.NORMAL);
        websettings.setSaveFormData(false);
        websettings.setSavePassword(false);
        String s = getContext().getApplicationContext().getDir("database", 0).getPath();
        websettings.setDatabaseEnabled(true);
        websettings.setDatabasePath(s);
        websettings.setDomStorageEnabled(true);
        websettings.setAppCacheMaxSize(0x500000L);
        websettings.setAppCachePath(s);
        websettings.setAppCacheEnabled(true);
        try
        {
            websettings.setUserAgentString((new StringBuilder(String.valueOf(websettings.getUserAgentString()))).append("; WemartApp/1.1; app=").append(context.getPackageName()).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
    }

    static void b(WemartJSBridgeWebView wemartjsbridgewebview, cn.wemart.sdk.bridge.b b1)
    {
        wemartjsbridgewebview.a(b1);
    }

    private void b(cn.wemart.sdk.bridge.b b1)
    {
        b1 = String.format("javascript:WemartJSBridge._handleMessageFromNative('%s');", new Object[] {
            b1.f()
        });
        cn.wemart.sdk.b.a.a("WemartJSBridge", (new StringBuilder("\u5206\u53D1JavaScript\u8FD0\u884C\u811A\u672C\u5230Web\u7AEF\uFF0C\u811A\u672C\u5185\u5BB9\uFF1A ")).append(b1).toString());
        if (Thread.currentThread() == Looper.getMainLooper().getThread())
        {
            loadUrl(b1);
            return;
        } else
        {
            cn.wemart.sdk.b.a.c("WemartJSBridge", "Cause: Thread.currentThread() != Looper.getMainLooper().getThread(), \u5F53\u524D\u7EBF\u7A0B\u4E0EMain Looper\u4E2D\u7EBF\u7A0B\u4E0D\u4E00\u81F4\uFF0C\u4E0D\u80FD\u53D1\u9001JavaScript\u811A\u672C\u5185\u5BB9\u5230Web\u7AEF\u3002");
            return;
        }
    }

    private void b(String s)
    {
        String s1 = cn.wemart.sdk.bridge.c.c(s);
        b b1 = (b)b.get(s1);
        s = cn.wemart.sdk.bridge.c.b(s);
        cn.wemart.sdk.b.a.a("WemartJSBridge", (new StringBuilder("\u5904\u7406\u8FD4\u56DE\u7684\u6570\u636E\uFF1A functionName = ")).append(s1).append(" f = ").append(b1).append(" data = ").append(s).toString());
        if (b1 != null)
        {
            b1.a(s);
            b.remove(s1);
        }
    }

    public void a()
    {
        if (Thread.currentThread() == Looper.getMainLooper().getThread())
        {
            a("javascript:WemartJSBridge._fetchJSQueue();", ((b) (new e(this))));
            return;
        } else
        {
            cn.wemart.sdk.b.a.a("WemartJSBridge", "Thread.currentThread() != Looper.getMainLooper().getThread(), \u5F53\u524D\u7EBF\u7A0B\u4E0EMain Looper\u4E2D\u7EBF\u7A0B\u4E0D\u4E00\u81F4\uFF0C\u4E0D\u80FD\u6267\u884CflushMessageQueue\u3002");
            return;
        }
    }

    public void a(String s)
    {
        if (s != null)
        {
            a = s;
        }
    }

    public void a(String s, b b1)
    {
        loadUrl(s);
        b.put(cn.wemart.sdk.bridge.c.a(s), b1);
        cn.wemart.sdk.b.a.a("WemartJSBridge", (new StringBuilder("WemartJSBridgeWebView put map key = ")).append(cn.wemart.sdk.bridge.c.a(s)).append(" value = ").append(b1).toString());
    }

    public void a(String s, d d1)
    {
        if (d1 != null)
        {
            c.put(s, d1);
        }
    }

    public void b(String s, b b1)
    {
        a("", b1, s);
    }
}
