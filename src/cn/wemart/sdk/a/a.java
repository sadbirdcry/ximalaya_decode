// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package cn.wemart.sdk.a;

import android.text.TextUtils;

public class a
{

    private String a;
    private String b;
    private String c;

    public a(String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            s = s.split(";");
            int j = s.length;
            int i = 0;
            while (i < j) 
            {
                String s1 = s[i];
                if (s1.startsWith("resultStatus"))
                {
                    a = a(s1, "resultStatus");
                }
                if (s1.startsWith("result"))
                {
                    b = a(s1, "result");
                }
                if (s1.startsWith("memo"))
                {
                    c = a(s1, "memo");
                }
                i++;
            }
        }
    }

    private String a(String s, String s1)
    {
        s1 = (new StringBuilder(String.valueOf(s1))).append("={").toString();
        int i = s.indexOf(s1);
        return s.substring(s1.length() + i, s.lastIndexOf("}"));
    }

    public String a()
    {
        return a;
    }

    public String toString()
    {
        return (new StringBuilder("resultStatus={")).append(a).append("};memo={").append(c).append("};result={").append(b).append("}").toString();
    }
}
