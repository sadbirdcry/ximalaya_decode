// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package cn.wemart.sdk;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import cn.wemart.sdk.a.b;
import cn.wemart.sdk.b.a;
import cn.wemart.sdk.bridge.WemartJSBridgeWebView;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package cn.wemart.sdk:
//            a, b, c, d

public class WemartWebView extends WemartJSBridgeWebView
{

    private final String g;
    private Activity h;
    private PayReq i;
    private IWXAPI j;
    private cn.wemart.sdk.b.b k;
    private Handler l;

    public WemartWebView(Context context)
    {
        super(context);
        g = "WemartWebView";
        h = null;
        l = new cn.wemart.sdk.a(this);
    }

    public WemartWebView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        g = "WemartWebView";
        h = null;
        l = new cn.wemart.sdk.a(this);
    }

    public WemartWebView(Context context, AttributeSet attributeset, int i1)
    {
        super(context, attributeset, i1);
        g = "WemartWebView";
        h = null;
        l = new cn.wemart.sdk.a(this);
    }

    static Activity a(WemartWebView wemartwebview)
    {
        return wemartwebview.h;
    }

    static void a(WemartWebView wemartwebview, String s, cn.wemart.sdk.b.b b1)
    {
        wemartwebview.c(s, b1);
    }

    static Handler b(WemartWebView wemartwebview)
    {
        return wemartwebview.l;
    }

    static void b(WemartWebView wemartwebview, String s, cn.wemart.sdk.b.b b1)
    {
        wemartwebview.d(s, b1);
    }

    private void c(String s, cn.wemart.sdk.b.b b1)
    {
        if (h == null)
        {
            cn.wemart.sdk.b.a.b("WemartWebView", "\u60A8\u8FD8\u6CA1\u6709\u5F00\u542F\u652F\u4ED8\u5B9D\u652F\u4ED8\u529F\u80FD\uFF0C\u8BF7\u4F7F\u7528 enableAliPay \u65B9\u6CD5\u5F00\u59CB\u652F\u4ED8\u5B9D\u652F\u4ED8\u529F\u80FD");
        }
        (new Thread(new cn.wemart.sdk.b(this, cn.wemart.sdk.a.b.a(s), b1))).start();
    }

    private void d(String s, cn.wemart.sdk.b.b b1)
    {
        k = b1;
        try
        {
            s = (new JSONObject(s)).getJSONObject("order");
            b1 = s.getString("appId");
            i.appId = b1;
            i.partnerId = s.getString("partnerId");
            i.prepayId = s.getString("prepayId");
            i.packageValue = s.getString("packageStr");
            i.nonceStr = s.getString("nonceStr");
            i.timeStamp = s.getString("timeStamp");
            i.sign = s.getString("paySign");
            j.registerApp(b1);
            j.sendReq(i);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    public void a(int i1)
    {
label0:
        {
            if (k != null)
            {
                if (i1 != 0)
                {
                    break label0;
                }
                k.a("9000");
            }
            return;
        }
        k.a(String.valueOf(i1));
    }

    public void a(Activity activity)
    {
        if (h == null)
        {
            h = activity;
        }
        a("nativePay", ((cn.wemart.sdk.bridge.d) (new c(this))));
    }

    public void b(Activity activity)
    {
        if (h == null)
        {
            h = activity;
        }
        i = new PayReq();
        j = WXAPIFactory.createWXAPI(h, null);
        a("payWithWechat", new d(this));
    }
}
