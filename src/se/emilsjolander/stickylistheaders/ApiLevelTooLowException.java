// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package se.emilsjolander.stickylistheaders;


public class ApiLevelTooLowException extends RuntimeException
{

    private static final long serialVersionUID = 0xb3f2e2db7b293dcbL;

    public ApiLevelTooLowException(int i)
    {
        super((new StringBuilder()).append("Requires API level ").append(i).toString());
    }
}
