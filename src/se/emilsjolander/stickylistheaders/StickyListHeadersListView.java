// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package se.emilsjolander.stickylistheaders;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SectionIndexer;

// Referenced classes of package se.emilsjolander.stickylistheaders:
//            g, a, ApiLevelTooLowException, f, 
//            WrapperView, e, StickyListHeadersAdapter

public class StickyListHeadersListView extends FrameLayout
{
    public static interface OnHeaderClickListener
    {

        public abstract void onHeaderClick(StickyListHeadersListView stickylistheaderslistview, View view, int i, long l, boolean flag);
    }

    public static interface OnStickyHeaderOffsetChangedListener
    {

        public abstract void onStickyHeaderOffsetChanged(StickyListHeadersListView stickylistheaderslistview, View view, int i);
    }

    private class a extends DataSetObserver
    {

        final StickyListHeadersListView a;

        public void onChanged()
        {
            a.clearHeader();
        }

        public void onInvalidated()
        {
            a.clearHeader();
        }

        private a()
        {
            a = StickyListHeadersListView.this;
            super();
        }

        a(f f1)
        {
            this();
        }
    }

    private class b
        implements a.a
    {

        final StickyListHeadersListView a;

        public void a(View view, int i, long l)
        {
            a.mOnHeaderClickListener.onHeaderClick(a, view, i, l, false);
        }

        private b()
        {
            a = StickyListHeadersListView.this;
            super();
        }

        b(f f1)
        {
            this();
        }
    }

    private class c
        implements android.widget.AbsListView.OnScrollListener
    {

        final StickyListHeadersListView a;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            if (a.mOnScrollListenerDelegate != null)
            {
                a.mOnScrollListenerDelegate.onScroll(abslistview, i, j, k);
            }
            a.updateOrClearHeader(a.mList.a());
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (a.mOnScrollListenerDelegate != null)
            {
                a.mOnScrollListenerDelegate.onScrollStateChanged(abslistview, i);
            }
        }

        private c()
        {
            a = StickyListHeadersListView.this;
            super();
        }

        c(f f1)
        {
            this();
        }
    }

    private class d
        implements g.a
    {

        final StickyListHeadersListView a;

        public void a(Canvas canvas)
        {
label0:
            {
                if (android.os.Build.VERSION.SDK_INT < 8)
                {
                    a.updateOrClearHeader(a.mList.a());
                }
                if (a.mHeader != null)
                {
                    if (!a.mClippingToPadding)
                    {
                        break label0;
                    }
                    canvas.save();
                    canvas.clipRect(0, a.mPaddingTop, a.getRight(), a.getBottom());
                    a.drawChild(canvas, a.mHeader, 0L);
                    canvas.restore();
                }
                return;
            }
            a.drawChild(canvas, a.mHeader, 0L);
        }

        private d()
        {
            a = StickyListHeadersListView.this;
            super();
        }

        d(f f1)
        {
            this();
        }
    }


    private se.emilsjolander.stickylistheaders.a mAdapter;
    private boolean mAreHeadersSticky;
    private boolean mClippingToPadding;
    private a mDataSetObserver;
    private Drawable mDivider;
    private int mDividerHeight;
    private View mHeader;
    private Long mHeaderId;
    private Integer mHeaderOffset;
    private Integer mHeaderPosition;
    private boolean mIsDrawingListUnderStickyHeader;
    private g mList;
    private OnHeaderClickListener mOnHeaderClickListener;
    private android.widget.AbsListView.OnScrollListener mOnScrollListenerDelegate;
    private OnStickyHeaderOffsetChangedListener mOnStickyHeaderOffsetChangedListener;
    private int mPaddingBottom;
    private int mPaddingLeft;
    private int mPaddingRight;
    private int mPaddingTop;

    public StickyListHeadersListView(Context context)
    {
        this(context, null);
    }

    public StickyListHeadersListView(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
    }

    public StickyListHeadersListView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mAreHeadersSticky = true;
        mClippingToPadding = true;
        mIsDrawingListUnderStickyHeader = true;
        mPaddingLeft = 0;
        mPaddingTop = 0;
        mPaddingRight = 0;
        mPaddingBottom = 0;
        mList = new g(context);
        mDivider = mList.getDivider();
        mDividerHeight = mList.getDividerHeight();
        mList.setDivider(null);
        mList.setDividerHeight(0);
        Drawable drawable = getBackground();
        mList.setBackgroundDrawable(drawable);
        mList.setVerticalScrollBarEnabled(isVerticalScrollBarEnabled());
        mList.setHorizontalScrollBarEnabled(isHorizontalScrollBarEnabled());
        if (attributeset == null) goto _L2; else goto _L1
_L1:
        context = context.getTheme().obtainStyledAttributes(attributeset, com.ximalaya.ting.android.R.styleable.StickyListHeadersListView, 0, 0);
        i = context.getDimensionPixelSize(1, 0);
        mPaddingLeft = context.getDimensionPixelSize(2, i);
        mPaddingTop = context.getDimensionPixelSize(3, i);
        mPaddingRight = context.getDimensionPixelSize(4, i);
        mPaddingBottom = context.getDimensionPixelSize(5, i);
        setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom);
        mClippingToPadding = context.getBoolean(7, true);
        super.setClipToPadding(true);
        mList.setClipToPadding(mClippingToPadding);
        mList.setFadingEdgeLength(context.getDimensionPixelSize(6, mList.getVerticalFadingEdgeLength()));
        i = context.getInt(17, 0);
        if (i != 4096) goto _L4; else goto _L3
_L3:
        mList.setVerticalFadingEdgeEnabled(false);
        mList.setHorizontalFadingEdgeEnabled(true);
_L5:
        mList.setCacheColorHint(context.getColor(11, mList.getCacheColorHint()));
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            mList.setChoiceMode(context.getInt(14, mList.getChoiceMode()));
        }
        mList.setDrawSelectorOnTop(context.getBoolean(9, false));
        mList.setFastScrollEnabled(context.getBoolean(15, mList.isFastScrollEnabled()));
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            mList.setFastScrollAlwaysVisible(context.getBoolean(16, mList.isFastScrollAlwaysVisible()));
        }
        mList.setScrollBarStyle(context.getInt(0, 0));
        if (context.hasValue(8))
        {
            mList.setSelector(context.getDrawable(8));
        }
        mList.setScrollingCacheEnabled(context.getBoolean(10, mList.isScrollingCacheEnabled()));
        if (context.hasValue(12))
        {
            mDivider = context.getDrawable(12);
            mList.setDivider(mDivider);
        }
        mDividerHeight = context.getDimensionPixelSize(13, mDividerHeight);
        mAreHeadersSticky = context.getBoolean(18, true);
        mIsDrawingListUnderStickyHeader = context.getBoolean(19, true);
        context.recycle();
_L2:
        mList.a(new d(null));
        mList.setOnScrollListener(new c(null));
        addView(mList);
        return;
_L4:
        if (i != 8192)
        {
            break MISSING_BLOCK_LABEL_601;
        }
        mList.setVerticalFadingEdgeEnabled(true);
        mList.setHorizontalFadingEdgeEnabled(false);
          goto _L5
        attributeset;
        context.recycle();
        throw attributeset;
        mList.setVerticalFadingEdgeEnabled(false);
        mList.setHorizontalFadingEdgeEnabled(false);
          goto _L5
    }

    private void clearHeader()
    {
        if (mHeader != null)
        {
            removeView(mHeader);
            mHeader = null;
            mHeaderId = null;
            mHeaderPosition = null;
            mHeaderOffset = null;
            mList.a(0);
            updateHeaderVisibilities();
        }
    }

    private void ensureHeaderHasCorrectLayoutParams(View view)
    {
        android.view.ViewGroup.LayoutParams layoutparams = view.getLayoutParams();
        if (layoutparams != null) goto _L2; else goto _L1
_L1:
        Object obj = new android.widget.FrameLayout.LayoutParams(-1, -2);
_L4:
        view.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        return;
_L2:
        obj = layoutparams;
        if (layoutparams.height == -1)
        {
            layoutparams.height = -2;
            obj = layoutparams;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private int getHeaderOverlap(int i)
    {
        if (!isStartOfSection(i))
        {
            View view = mAdapter.getHeaderView(i, null, mList);
            if (view == null)
            {
                throw new NullPointerException("header may not be null");
            } else
            {
                ensureHeaderHasCorrectLayoutParams(view);
                measureHeader(view);
                return view.getMeasuredHeight();
            }
        } else
        {
            return 0;
        }
    }

    private boolean isStartOfSection(int i)
    {
        return i == 0 || mAdapter.getHeaderId(i) != mAdapter.getHeaderId(i - 1);
    }

    private void measureHeader(View view)
    {
        if (view != null)
        {
            measureChild(view, android.view.View.MeasureSpec.makeMeasureSpec(getMeasuredWidth() - mPaddingLeft - mPaddingRight, 0x40000000), android.view.View.MeasureSpec.makeMeasureSpec(0, 0));
        }
    }

    private void requireSdkVersion(int i)
    {
        if (android.os.Build.VERSION.SDK_INT < i)
        {
            throw new ApiLevelTooLowException(i);
        } else
        {
            return;
        }
    }

    private void setHeaderOffet(int i)
    {
        if (mHeaderOffset == null || mHeaderOffset.intValue() != i)
        {
            mHeaderOffset = Integer.valueOf(i);
            if (android.os.Build.VERSION.SDK_INT >= 11)
            {
                mHeader.setTranslationY(mHeaderOffset.intValue());
            } else
            {
                android.view.ViewGroup.MarginLayoutParams marginlayoutparams = (android.view.ViewGroup.MarginLayoutParams)mHeader.getLayoutParams();
                marginlayoutparams.topMargin = mHeaderOffset.intValue();
                mHeader.setLayoutParams(marginlayoutparams);
            }
            if (mOnStickyHeaderOffsetChangedListener != null)
            {
                mOnStickyHeaderOffsetChangedListener.onStickyHeaderOffsetChanged(this, mHeader, -mHeaderOffset.intValue());
            }
        }
    }

    private void swapHeader(View view)
    {
        if (mHeader != null)
        {
            removeView(mHeader);
        }
        mHeader = view;
        addView(mHeader);
        mHeader.setOnClickListener(new f(this));
    }

    private void updateHeader(int i)
    {
        boolean flag = false;
        if (mHeaderPosition == null || mHeaderPosition.intValue() != i)
        {
            mHeaderPosition = Integer.valueOf(i);
            long l1 = mAdapter.getHeaderId(i);
            if (mHeaderId == null || mHeaderId.longValue() != l1)
            {
                mHeaderId = Long.valueOf(l1);
                View view = mAdapter.getHeaderView(mHeaderPosition.intValue(), mHeader, this);
                if (mHeader != view)
                {
                    if (view == null)
                    {
                        throw new NullPointerException("header may not be null");
                    }
                    swapHeader(view);
                }
                ensureHeaderHasCorrectLayoutParams(mHeader);
                measureHeader(mHeader);
                mHeaderOffset = null;
            }
        }
        int i1 = mHeader.getMeasuredHeight();
        int j;
        if (mClippingToPadding)
        {
            i = mPaddingTop;
        } else
        {
            i = 0;
        }
        j = 0;
        do
        {
label0:
            {
                int k = ((flag) ? 1 : 0);
                if (j < mList.getChildCount())
                {
                    View view1 = mList.getChildAt(j);
                    int l;
                    int j1;
                    boolean flag1;
                    if ((view1 instanceof WrapperView) && ((WrapperView)view1).hasHeader())
                    {
                        k = 1;
                    } else
                    {
                        k = 0;
                    }
                    flag1 = mList.a(view1);
                    j1 = view1.getTop();
                    if (mClippingToPadding)
                    {
                        l = mPaddingTop;
                    } else
                    {
                        l = 0;
                    }
                    if (j1 < l || k == 0 && !flag1)
                    {
                        break label0;
                    }
                    k = Math.min(view1.getTop() - (i1 + i), 0);
                }
                setHeaderOffet(k);
                if (!mIsDrawingListUnderStickyHeader)
                {
                    mList.a(mHeader.getMeasuredHeight() + mHeaderOffset.intValue());
                }
                updateHeaderVisibilities();
                return;
            }
            j++;
        } while (true);
    }

    private void updateHeaderVisibilities()
    {
        int i;
        if (mHeader != null)
        {
            int j = mHeader.getMeasuredHeight();
            int k;
            if (mHeaderOffset != null)
            {
                i = mHeaderOffset.intValue();
            } else
            {
                i = 0;
            }
            i += j;
        } else
        if (mClippingToPadding)
        {
            i = mPaddingTop;
        } else
        {
            i = 0;
        }
        k = mList.getChildCount();
        j = 0;
        while (j < k) 
        {
            Object obj = mList.getChildAt(j);
            if (obj instanceof WrapperView)
            {
                obj = (WrapperView)obj;
                if (((WrapperView) (obj)).hasHeader())
                {
                    View view = ((WrapperView) (obj)).mHeader;
                    if (((WrapperView) (obj)).getTop() < i)
                    {
                        if (view.getVisibility() != 4)
                        {
                            view.setVisibility(4);
                        }
                    } else
                    if (view.getVisibility() != 0)
                    {
                        view.setVisibility(0);
                    }
                }
            }
            j++;
        }
    }

    private void updateOrClearHeader(int i)
    {
        int k;
        int l;
        boolean flag1 = false;
        int j;
        if (mAdapter == null)
        {
            j = 0;
        } else
        {
            j = mAdapter.getCount();
        }
        if (j == 0 || !mAreHeadersSticky)
        {
            return;
        }
        l = i - mList.getHeaderViewsCount();
        boolean flag;
        int i1;
        if (mList.getChildCount() != 0)
        {
            i = 1;
        } else
        {
            i = 0;
        }
        if (i == 0 || mList.getFirstVisiblePosition() != 0) goto _L2; else goto _L1
_L1:
        i1 = mList.getChildAt(0).getTop();
        if (mClippingToPadding)
        {
            k = mPaddingTop;
        } else
        {
            k = 0;
        }
        if (i1 <= k) goto _L2; else goto _L3
_L3:
        k = 1;
_L5:
label0:
        {
            if (l <= j - 1)
            {
                flag = flag1;
                if (l >= 0)
                {
                    break label0;
                }
            }
            flag = true;
        }
        if (i == 0 || flag || k != 0)
        {
            clearHeader();
            return;
        } else
        {
            updateHeader(l);
            return;
        }
_L2:
        k = 0;
        if (true) goto _L5; else goto _L4
_L4:
    }

    public void addFooterView(View view)
    {
        if (!(view.getLayoutParams() instanceof android.widget.AbsListView.LayoutParams))
        {
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, -2));
        }
        mList.addFooterView(view);
    }

    public void addHeaderView(View view)
    {
        if (!(view.getLayoutParams() instanceof android.widget.AbsListView.LayoutParams))
        {
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, -2));
        }
        mList.addHeaderView(view);
    }

    public void addHeaderView(View view, Object obj, boolean flag)
    {
        if (!(view.getLayoutParams() instanceof android.widget.AbsListView.LayoutParams))
        {
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, -2));
        }
        mList.addHeaderView(view, obj, flag);
    }

    public boolean areHeadersSticky()
    {
        return mAreHeadersSticky;
    }

    protected void dispatchDraw(Canvas canvas)
    {
        drawChild(canvas, mList, 0L);
    }

    public StickyListHeadersAdapter getAdapter()
    {
        if (mAdapter == null)
        {
            return null;
        } else
        {
            return mAdapter.a;
        }
    }

    public boolean getAreHeadersSticky()
    {
        return areHeadersSticky();
    }

    public int getCheckedItemCount()
    {
        requireSdkVersion(11);
        return mList.getCheckedItemCount();
    }

    public long[] getCheckedItemIds()
    {
        requireSdkVersion(8);
        return mList.getCheckedItemIds();
    }

    public int getCheckedItemPosition()
    {
        return mList.getCheckedItemPosition();
    }

    public SparseBooleanArray getCheckedItemPositions()
    {
        return mList.getCheckedItemPositions();
    }

    public int getCount()
    {
        return mList.getCount();
    }

    public Drawable getDivider()
    {
        return mDivider;
    }

    public int getDividerHeight()
    {
        return mDividerHeight;
    }

    public View getEmptyView()
    {
        return mList.getEmptyView();
    }

    public int getFirstVisiblePosition()
    {
        return mList.getFirstVisiblePosition();
    }

    public int getFooterViewsCount()
    {
        return mList.getFooterViewsCount();
    }

    public int getHeaderViewsCount()
    {
        return mList.getHeaderViewsCount();
    }

    public Object getItemAtPosition(int i)
    {
        return mList.getItemAtPosition(i);
    }

    public long getItemIdAtPosition(int i)
    {
        return mList.getItemIdAtPosition(i);
    }

    public int getLastVisiblePosition()
    {
        return mList.getLastVisiblePosition();
    }

    public View getListChildAt(int i)
    {
        return mList.getChildAt(i);
    }

    public int getListChildCount()
    {
        return mList.getChildCount();
    }

    public int getPaddingBottom()
    {
        return mPaddingBottom;
    }

    public int getPaddingLeft()
    {
        return mPaddingLeft;
    }

    public int getPaddingRight()
    {
        return mPaddingRight;
    }

    public int getPaddingTop()
    {
        return mPaddingTop;
    }

    public int getPositionForView(View view)
    {
        return mList.getPositionForView(view);
    }

    public int getScrollBarStyle()
    {
        return mList.getScrollBarStyle();
    }

    public ListView getWrappedList()
    {
        return mList;
    }

    public void invalidateViews()
    {
        mList.invalidateViews();
    }

    public boolean isDrawingListUnderStickyHeader()
    {
        return mIsDrawingListUnderStickyHeader;
    }

    public boolean isFastScrollAlwaysVisible()
    {
        if (android.os.Build.VERSION.SDK_INT < 11)
        {
            return false;
        } else
        {
            return mList.isFastScrollAlwaysVisible();
        }
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l)
    {
        mList.layout(0, 0, mList.getMeasuredWidth(), getHeight());
        if (mHeader != null)
        {
            j = ((android.view.ViewGroup.MarginLayoutParams)mHeader.getLayoutParams()).topMargin;
            if (mClippingToPadding)
            {
                i = mPaddingTop;
            } else
            {
                i = 0;
            }
            i += j;
            mHeader.layout(mPaddingLeft, i, mHeader.getMeasuredWidth() + mPaddingLeft, mHeader.getMeasuredHeight() + i);
        }
    }

    protected void onMeasure(int i, int j)
    {
        super.onMeasure(i, j);
        measureHeader(mHeader);
    }

    protected void recomputePadding()
    {
        setPadding(mPaddingLeft, mPaddingTop, mPaddingRight, mPaddingBottom);
    }

    public void removeFooterView(View view)
    {
        mList.removeFooterView(view);
    }

    public void removeHeaderView(View view)
    {
        mList.removeHeaderView(view);
    }

    public void setAdapter(StickyListHeadersAdapter stickylistheadersadapter)
    {
        if (stickylistheadersadapter == null)
        {
            mList.setAdapter(null);
            clearHeader();
            return;
        }
        if (mAdapter != null)
        {
            mAdapter.unregisterDataSetObserver(mDataSetObserver);
        }
        if (stickylistheadersadapter instanceof SectionIndexer)
        {
            mAdapter = new e(getContext(), stickylistheadersadapter);
        } else
        {
            mAdapter = new se.emilsjolander.stickylistheaders.a(getContext(), stickylistheadersadapter);
        }
        mDataSetObserver = new a(null);
        mAdapter.registerDataSetObserver(mDataSetObserver);
        if (mOnHeaderClickListener != null)
        {
            mAdapter.a(new b(null));
        } else
        {
            mAdapter.a(null);
        }
        mAdapter.a(mDivider, mDividerHeight);
        mList.setAdapter(mAdapter);
        clearHeader();
    }

    public void setAreHeadersSticky(boolean flag)
    {
        mAreHeadersSticky = flag;
        if (!flag)
        {
            clearHeader();
        } else
        {
            updateOrClearHeader(mList.a());
        }
        mList.invalidate();
    }

    public void setChoiceMode(int i)
    {
        mList.setChoiceMode(i);
    }

    public void setClipToPadding(boolean flag)
    {
        if (mList != null)
        {
            mList.setClipToPadding(flag);
        }
        mClippingToPadding = flag;
    }

    public void setDivider(Drawable drawable)
    {
        mDivider = drawable;
        if (mAdapter != null)
        {
            mAdapter.a(mDivider, mDividerHeight);
        }
    }

    public void setDividerHeight(int i)
    {
        mDividerHeight = i;
        if (mAdapter != null)
        {
            mAdapter.a(mDivider, mDividerHeight);
        }
    }

    public void setDrawingListUnderStickyHeader(boolean flag)
    {
        mIsDrawingListUnderStickyHeader = flag;
        mList.a(0);
    }

    public void setEmptyView(View view)
    {
        mList.setEmptyView(view);
    }

    public void setFastScrollAlwaysVisible(boolean flag)
    {
        requireSdkVersion(11);
        mList.setFastScrollAlwaysVisible(flag);
    }

    public void setFastScrollEnabled(boolean flag)
    {
        mList.setFastScrollEnabled(flag);
    }

    public void setFooterDividersEnabled(boolean flag)
    {
        mList.setFooterDividersEnabled(flag);
    }

    public void setHorizontalScrollBarEnabled(boolean flag)
    {
        mList.setHorizontalScrollBarEnabled(flag);
    }

    public void setItemChecked(int i, boolean flag)
    {
        mList.setItemChecked(i, flag);
    }

    public void setOnCreateContextMenuListener(android.view.View.OnCreateContextMenuListener oncreatecontextmenulistener)
    {
        mList.setOnCreateContextMenuListener(oncreatecontextmenulistener);
    }

    public void setOnHeaderClickListener(OnHeaderClickListener onheaderclicklistener)
    {
label0:
        {
            mOnHeaderClickListener = onheaderclicklistener;
            if (mAdapter != null)
            {
                if (mOnHeaderClickListener == null)
                {
                    break label0;
                }
                mAdapter.a(new b(null));
            }
            return;
        }
        mAdapter.a(null);
    }

    public void setOnItemClickListener(android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        mList.setOnItemClickListener(onitemclicklistener);
    }

    public void setOnItemLongClickListener(android.widget.AdapterView.OnItemLongClickListener onitemlongclicklistener)
    {
        mList.setOnItemLongClickListener(onitemlongclicklistener);
    }

    public void setOnScrollListener(android.widget.AbsListView.OnScrollListener onscrolllistener)
    {
        mOnScrollListenerDelegate = onscrolllistener;
    }

    public void setOnStickyHeaderOffsetChangedListener(OnStickyHeaderOffsetChangedListener onstickyheaderoffsetchangedlistener)
    {
        mOnStickyHeaderOffsetChangedListener = onstickyheaderoffsetchangedlistener;
    }

    public void setPadding(int i, int j, int k, int l)
    {
        mPaddingLeft = i;
        mPaddingTop = j;
        mPaddingRight = k;
        mPaddingBottom = l;
        if (mList != null)
        {
            mList.setPadding(i, j, k, l);
        }
        super.setPadding(0, 0, 0, 0);
        requestLayout();
    }

    public void setScrollBarStyle(int i)
    {
        mList.setScrollBarStyle(i);
    }

    public void setSelection(int i)
    {
        setSelectionFromTop(i, 0);
    }

    public void setSelectionAfterHeaderView()
    {
        mList.setSelectionAfterHeaderView();
    }

    public void setSelectionFromTop(int i, int j)
    {
        int l = 0;
        int k;
        if (mAdapter == null)
        {
            k = 0;
        } else
        {
            k = getHeaderOverlap(i);
        }
        if (!mClippingToPadding)
        {
            l = mPaddingTop;
        }
        mList.setSelectionFromTop(i, (k + j) - l);
    }

    public void setSelector(int i)
    {
        mList.setSelector(i);
    }

    public void setSelector(Drawable drawable)
    {
        mList.setSelector(drawable);
    }

    public void setVerticalScrollBarEnabled(boolean flag)
    {
        mList.setVerticalScrollBarEnabled(flag);
    }

    public boolean showContextMenu()
    {
        return mList.showContextMenu();
    }

    public void smoothScrollBy(int i, int j)
    {
        requireSdkVersion(8);
        mList.smoothScrollBy(i, j);
    }

    public void smoothScrollByOffset(int i)
    {
        requireSdkVersion(11);
        mList.smoothScrollByOffset(i);
    }

    public void smoothScrollToPosition(int i)
    {
        int k = 0;
        if (android.os.Build.VERSION.SDK_INT < 11)
        {
            mList.smoothScrollToPosition(i);
            return;
        }
        int j;
        if (mAdapter == null)
        {
            j = 0;
        } else
        {
            j = getHeaderOverlap(i);
        }
        if (!mClippingToPadding)
        {
            k = mPaddingTop;
        }
        mList.smoothScrollToPositionFromTop(i, j - k);
    }

    public void smoothScrollToPosition(int i, int j)
    {
        requireSdkVersion(8);
        mList.smoothScrollToPosition(i, j);
    }

    public void smoothScrollToPositionFromTop(int i, int j)
    {
        int l = 0;
        requireSdkVersion(11);
        int k;
        if (mAdapter == null)
        {
            k = 0;
        } else
        {
            k = getHeaderOverlap(i);
        }
        if (!mClippingToPadding)
        {
            l = mPaddingTop;
        }
        mList.smoothScrollToPositionFromTop(i, (k + j) - l);
    }

    public void smoothScrollToPositionFromTop(int i, int j, int k)
    {
        int i1 = 0;
        requireSdkVersion(11);
        int l;
        if (mAdapter == null)
        {
            l = 0;
        } else
        {
            l = getHeaderOverlap(i);
        }
        if (!mClippingToPadding)
        {
            i1 = mPaddingTop;
        }
        mList.smoothScrollToPositionFromTop(i, (l + j) - i1, k);
    }












}
