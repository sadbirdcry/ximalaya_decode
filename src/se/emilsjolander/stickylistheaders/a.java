// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package se.emilsjolander.stickylistheaders;

import android.content.Context;
import android.database.DataSetObserver;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Checkable;
import java.util.LinkedList;
import java.util.List;

// Referenced classes of package se.emilsjolander.stickylistheaders:
//            StickyListHeadersAdapter, b, WrapperView, c, 
//            d

class se.emilsjolander.stickylistheaders.a extends BaseAdapter
    implements StickyListHeadersAdapter
{
    static interface a
    {

        public abstract void a(View view, int i, long l);
    }


    final StickyListHeadersAdapter a;
    private final List b = new LinkedList();
    private final Context c;
    private Drawable d;
    private int e;
    private a f;
    private DataSetObserver g;

    se.emilsjolander.stickylistheaders.a(Context context, StickyListHeadersAdapter stickylistheadersadapter)
    {
        g = new b(this);
        c = context;
        a = stickylistheadersadapter;
        stickylistheadersadapter.registerDataSetObserver(g);
    }

    private View a()
    {
        if (b.size() > 0)
        {
            return (View)b.remove(0);
        } else
        {
            return null;
        }
    }

    private View a(WrapperView wrapperview, int i)
    {
        View view;
        if (wrapperview.mHeader == null)
        {
            view = a();
        } else
        {
            view = wrapperview.mHeader;
        }
        wrapperview = a.getHeaderView(i, view, wrapperview);
        if (wrapperview == null)
        {
            throw new NullPointerException("Header view must not be null.");
        } else
        {
            wrapperview.setClickable(true);
            wrapperview.setOnClickListener(new c(this, i));
            return wrapperview;
        }
    }

    static List a(se.emilsjolander.stickylistheaders.a a1)
    {
        return a1.b;
    }

    private void a(WrapperView wrapperview)
    {
        wrapperview = wrapperview.mHeader;
        if (wrapperview != null)
        {
            wrapperview.setVisibility(0);
            b.add(wrapperview);
        }
    }

    private boolean a(int i)
    {
        return i != 0 && a.getHeaderId(i) == a.getHeaderId(i - 1);
    }

    static void b(se.emilsjolander.stickylistheaders.a a1)
    {
        a1.BaseAdapter.notifyDataSetInvalidated();
    }

    static void c(se.emilsjolander.stickylistheaders.a a1)
    {
        a1.BaseAdapter.notifyDataSetChanged();
    }

    static a d(se.emilsjolander.stickylistheaders.a a1)
    {
        return a1.f;
    }

    public WrapperView a(int i, View view, ViewGroup viewgroup)
    {
        View view2;
        View view1;
        if (view == null)
        {
            view = new WrapperView(c);
        } else
        {
            view = (WrapperView)view;
        }
        view2 = a.getView(i, ((WrapperView) (view)).mItem, viewgroup);
        view1 = null;
        if (a(i))
        {
            a(((WrapperView) (view)));
        } else
        {
            view1 = a(((WrapperView) (view)), i);
        }
        if (!(view2 instanceof Checkable) || (view instanceof d)) goto _L2; else goto _L1
_L1:
        viewgroup = new d(c);
_L4:
        viewgroup.update(view2, view1, d, e);
        return viewgroup;
_L2:
        viewgroup = view;
        if (!(view2 instanceof Checkable))
        {
            viewgroup = view;
            if (view instanceof d)
            {
                viewgroup = new WrapperView(c);
            }
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    void a(Drawable drawable, int i)
    {
        d = drawable;
        e = i;
        notifyDataSetChanged();
    }

    public void a(a a1)
    {
        f = a1;
    }

    public boolean areAllItemsEnabled()
    {
        return a.areAllItemsEnabled();
    }

    public boolean equals(Object obj)
    {
        return a.equals(obj);
    }

    public int getCount()
    {
        return a.getCount();
    }

    public View getDropDownView(int i, View view, ViewGroup viewgroup)
    {
        return ((BaseAdapter)a).getDropDownView(i, view, viewgroup);
    }

    public long getHeaderId(int i)
    {
        return a.getHeaderId(i);
    }

    public View getHeaderView(int i, View view, ViewGroup viewgroup)
    {
        return a.getHeaderView(i, view, viewgroup);
    }

    public Object getItem(int i)
    {
        return a.getItem(i);
    }

    public long getItemId(int i)
    {
        return a.getItemId(i);
    }

    public int getItemViewType(int i)
    {
        return a.getItemViewType(i);
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        return a(i, view, viewgroup);
    }

    public int getViewTypeCount()
    {
        return a.getViewTypeCount();
    }

    public boolean hasStableIds()
    {
        return a.hasStableIds();
    }

    public int hashCode()
    {
        return a.hashCode();
    }

    public boolean isEmpty()
    {
        return a.isEmpty();
    }

    public boolean isEnabled(int i)
    {
        return a.isEnabled(i);
    }

    public void notifyDataSetChanged()
    {
        ((BaseAdapter)a).notifyDataSetChanged();
    }

    public void notifyDataSetInvalidated()
    {
        ((BaseAdapter)a).notifyDataSetInvalidated();
    }

    public String toString()
    {
        return a.toString();
    }
}
