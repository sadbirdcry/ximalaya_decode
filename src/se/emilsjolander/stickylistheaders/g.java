// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package se.emilsjolander.stickylistheaders;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package se.emilsjolander.stickylistheaders:
//            WrapperView

class g extends ListView
{
    static interface a
    {

        public abstract void a(Canvas canvas);
    }


    private a a;
    private List b;
    private int c;
    private Rect d;
    private Field e;
    private boolean f;

    public g(Context context)
    {
        super(context);
        d = new Rect();
        f = true;
        try
        {
            context = android/widget/AbsListView.getDeclaredField("mSelectorRect");
            context.setAccessible(true);
            d = (Rect)context.get(this);
            if (android.os.Build.VERSION.SDK_INT >= 14)
            {
                e = android/widget/AbsListView.getDeclaredField("mSelectorPosition");
                e.setAccessible(true);
            }
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    private void b()
    {
        if (!d.isEmpty())
        {
            int i = c();
            if (i >= 0)
            {
                Object obj = getChildAt(i - a());
                if (obj instanceof WrapperView)
                {
                    obj = (WrapperView)obj;
                    Rect rect = d;
                    int j = ((WrapperView) (obj)).getTop();
                    rect.top = ((WrapperView) (obj)).mItemTop + j;
                }
            }
        }
    }

    private int c()
    {
        if (e == null)
        {
            for (int i = 0; i < getChildCount(); i++)
            {
                if (getChildAt(i).getBottom() == d.bottom)
                {
                    return i + a();
                }
            }

            break MISSING_BLOCK_LABEL_65;
        }
        int j = e.getInt(this);
        return j;
        Object obj;
        obj;
        ((IllegalArgumentException) (obj)).printStackTrace();
_L2:
        return -1;
        obj;
        ((IllegalAccessException) (obj)).printStackTrace();
        if (true) goto _L2; else goto _L1
_L1:
    }

    int a()
    {
        int i;
        int j;
        j = getFirstVisiblePosition();
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            return j;
        }
        i = 0;
_L3:
        if (i >= getChildCount())
        {
            break MISSING_BLOCK_LABEL_90;
        }
        if (getChildAt(i).getBottom() < 0) goto _L2; else goto _L1
_L1:
        i += j;
_L4:
        j = i;
        if (!f)
        {
            j = i;
            if (getPaddingTop() > 0)
            {
                j = i;
                if (i > 0)
                {
                    j = i;
                    if (getChildAt(0).getTop() > 0)
                    {
                        j = i - 1;
                    }
                }
            }
        }
        return j;
_L2:
        i++;
          goto _L3
        i = j;
          goto _L4
    }

    void a(int i)
    {
        c = i;
    }

    void a(a a1)
    {
        a = a1;
    }

    boolean a(View view)
    {
        if (b == null)
        {
            return false;
        } else
        {
            return b.contains(view);
        }
    }

    public void addFooterView(View view)
    {
        super.addFooterView(view);
        if (b == null)
        {
            b = new ArrayList();
        }
        b.add(view);
    }

    protected void dispatchDraw(Canvas canvas)
    {
        b();
        if (c != 0)
        {
            canvas.save();
            Rect rect = canvas.getClipBounds();
            rect.top = c;
            canvas.clipRect(rect);
            super.dispatchDraw(canvas);
            canvas.restore();
        } else
        {
            super.dispatchDraw(canvas);
        }
        a.a(canvas);
    }

    public boolean performItemClick(View view, int i, long l)
    {
        View view1 = view;
        if (view instanceof WrapperView)
        {
            view1 = ((WrapperView)view).mItem;
        }
        return super.performItemClick(view1, i, l);
    }

    public boolean removeFooterView(View view)
    {
        if (super.removeFooterView(view))
        {
            b.remove(view);
            return true;
        } else
        {
            return false;
        }
    }

    public void setClipToPadding(boolean flag)
    {
        f = flag;
        super.setClipToPadding(flag);
    }
}
