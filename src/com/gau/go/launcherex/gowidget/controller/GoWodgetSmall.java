// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.gau.go.launcherex.gowidget.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.gau.go.launcherex.gowidget.framework.GoWidgetFrame;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.DiskLruCache;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

// Referenced classes of package com.gau.go.launcherex.gowidget.controller:
//            a, b, c, d, 
//            e

public class GoWodgetSmall extends GoWidgetFrame
{
    private static class a
        implements Runnable
    {

        private Context a;
        private String b;
        private boolean c;
        private Handler d;
        private b e;

        public void a()
        {
            c = true;
        }

        public void a(Context context, Handler handler, ImageView imageview, String s)
        {
            c = false;
            a = context;
            b = s;
            d = handler;
            if (e == null)
            {
                e = new b(null);
            }
            e.a = imageview;
        }

        public void run()
        {
            if (GoWodgetSmall.sInstanceCount > 0 && a != null) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if (e.a == null)
            {
                Log.e("LoadImgTask", "mTaskWrapper.imageView == null, return");
                return;
            }
            if (d == null)
            {
                Log.e("LoadImgTask", "mUIHandler == null, return");
                return;
            }
            if (TextUtils.isEmpty(b))
            {
                Log.e("LoadImgTask", "mUrl is empty, return");
                return;
            }
            if (GoWodgetSmall.sDiskCache == null) goto _L1; else goto _L3
_L3:
            Object obj;
            obj = GoWodgetSmall.sDiskCache.get(b);
            if (c)
            {
                Log.e("LoadImgTask", "Canceled, return");
                return;
            }
            if (obj != null)
            {
                Log.e("LoadImgTask", (new StringBuilder()).append("Xm get bitmap from cache ").append(b).toString());
                e.b = GoWodgetSmall.getRoundCornerBitmap(ThumbnailUtils.extractThumbnail(((Bitmap) (obj)), ToolUtil.dp2px(a, 100F), ToolUtil.dp2px(a, 100F)), ToolUtil.dp2px(a, 10F));
                if (c)
                {
                    Log.e("LoadImgTask", "Canceled, return");
                    return;
                } else
                {
                    obj = d.obtainMessage();
                    obj.obj = e;
                    ((Message) (obj)).sendToTarget();
                    return;
                }
            }
            try
            {
                Log.e("LoadImgTask", (new StringBuilder()).append("Xm get bitmap from net ").append(b).toString());
                obj = FreeFlowUtil.getHttpURLConnection(new URL(b));
                ((HttpURLConnection) (obj)).setConnectTimeout(5000);
                ((HttpURLConnection) (obj)).setRequestMethod("GET");
                ((HttpURLConnection) (obj)).setRequestProperty("Accept", "*/*");
                ((HttpURLConnection) (obj)).connect();
                if (c)
                {
                    Log.e("LoadImgTask", "Canceled, return");
                    return;
                }
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((Exception) (obj)).printStackTrace();
                return;
            }
            obj = BitmapFactory.decodeStream(((HttpURLConnection) (obj)).getInputStream());
            if (c)
            {
                Log.e("LoadImgTask", "Canceled, return");
                return;
            }
            if (obj != null)
            {
                break MISSING_BLOCK_LABEL_339;
            }
            Log.e("LoadImgTask", "Bitmap == null, decode from stream, return");
            return;
            e.b = GoWodgetSmall.getRoundCornerBitmap(ThumbnailUtils.extractThumbnail(((Bitmap) (obj)), ToolUtil.dp2px(a, 100F), ToolUtil.dp2px(a, 100F)), ToolUtil.dp2px(a, 10F));
            if (c)
            {
                Log.e("LoadImgTask", "Canceled, return");
                return;
            }
            Message message = d.obtainMessage();
            message.obj = e;
            message.sendToTarget();
            GoWodgetSmall.sDiskCache.put(b, ((Bitmap) (obj)));
            a = null;
            d = null;
            return;
        }

        public a(Context context, Handler handler, ImageView imageview, String s)
        {
            a(context, handler, imageview, s);
        }
    }

    private static class b
    {

        ImageView a;
        Bitmap b;

        private b()
        {
        }

        b(com.gau.go.launcherex.gowidget.controller.a a1)
        {
            this();
        }
    }

    private static class c extends Handler
    {

        public void handleMessage(Message message)
        {
            boolean flag1 = true;
            Object obj = message.obj;
            if (obj != null && (obj instanceof b))
            {
                message = (b)obj;
                if (((b) (message)).a != null && ((b) (message)).b != null)
                {
                    ((b) (message)).a.setImageBitmap(((b) (message)).b);
                } else
                {
                    StringBuilder stringbuilder = (new StringBuilder()).append("UpdateHandler do nothing ");
                    boolean flag;
                    if (((b) (message)).a == null)
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                    stringbuilder = stringbuilder.append(flag).append("/");
                    if (((b) (message)).b == null)
                    {
                        flag = flag1;
                    } else
                    {
                        flag = false;
                    }
                    Log.e("UpdateHandler", stringbuilder.append(flag).toString());
                }
                message.a = null;
                message.b = null;
                return;
            } else
            {
                super.handleMessage(message);
                return;
            }
        }

        private c()
        {
        }

        c(com.gau.go.launcherex.gowidget.controller.a a1)
        {
            this();
        }
    }


    private static final int DISK_CACHE_SIZE = 0x3200000;
    private static final String DOWNLOAD_CACHE_DIR;
    private static final String TAG = "GoWodgetSmall";
    private static final String sClsName = "com.ximalaya.ting.android.service.play.LocalMediaService";
    private static DiskLruCache sDiskCache;
    private static int sInstanceCount = 0;
    private static boolean sIsPlay = false;
    private static Handler sLoadHander;
    private static HandlerThread sLoadThread;
    private static final String sPkgName = "com.ximalaya.ting.android";
    private static String sSoundInfoStr;
    private ImageView mBtnNext;
    private ImageView mBtnPlayOrPause;
    private ImageView mBtnPre;
    private View mContentView;
    private Context mContext;
    private IntentFilter mFilter;
    private a mLoadTask;
    private ProgressBar mProgressBar;
    private BroadcastReceiver mReceiver;
    private Handler mUIHandler;
    private ImageView mWidgetIcon;
    private TextView mWidgetTitle;

    public GoWodgetSmall(Context context)
    {
        super(context);
        mReceiver = new com.gau.go.launcherex.gowidget.controller.a(this);
        init(context, null, 0);
    }

    public GoWodgetSmall(Context context, AttributeSet attributeset)
    {
        super(context, attributeset, 0);
        mReceiver = new com.gau.go.launcherex.gowidget.controller.a(this);
        init(context, attributeset, 0);
    }

    public GoWodgetSmall(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mReceiver = new com.gau.go.launcherex.gowidget.controller.a(this);
        init(context, attributeset, i);
    }

    public static Bitmap getRoundCornerBitmap(Bitmap bitmap, float f)
    {
        int i = bitmap.getWidth();
        int j = bitmap.getHeight();
        Bitmap bitmap1 = Bitmap.createBitmap(i, j, android.graphics.Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap1);
        Paint paint = new Paint();
        Rect rect = new Rect(0, 0, i, j);
        RectF rectf = new RectF(rect);
        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        canvas.drawRoundRect(rectf, f, f, paint);
        paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return bitmap1;
    }

    private void init(Context context, AttributeSet attributeset, int i)
    {
        Log.e("GoWodgetSmall", "Xm goWidget init");
        mContext = context;
        initDiskCache();
        initLoader();
        mUIHandler = new c(null);
        initView();
    }

    private void initDiskCache()
    {
        if (sDiskCache == null)
        {
            File file = new File(DOWNLOAD_CACHE_DIR);
            sDiskCache = DiskLruCache.openCache(mContext, file, 0x3200000L);
        }
        sInstanceCount++;
    }

    private void initLoader()
    {
        if (sLoadThread == null)
        {
            sLoadThread = new HandlerThread("img-loader");
            sLoadThread.start();
            sLoadHander = new Handler(sLoadThread.getLooper());
        }
    }

    private void initView()
    {
        mContentView = LayoutInflater.from(mContext).inflate(0x7f030043, this, true);
        mWidgetIcon = (ImageView)mContentView.findViewById(0x7f0a016e);
        mWidgetTitle = (TextView)mContentView.findViewById(0x7f0a016f);
        mProgressBar = (ProgressBar)mContentView.findViewById(0x7f0a0170);
        mBtnPre = (ImageView)mContentView.findViewById(0x7f0a0171);
        mBtnNext = (ImageView)mContentView.findViewById(0x7f0a0173);
        mBtnPlayOrPause = (ImageView)mContentView.findViewById(0x7f0a0172);
        mWidgetIcon.setOnClickListener(new com.gau.go.launcherex.gowidget.controller.b(this));
        mBtnPlayOrPause.setOnClickListener(new com.gau.go.launcherex.gowidget.controller.c(this));
        mBtnPre.setOnClickListener(new d(this));
        mBtnNext.setOnClickListener(new e(this));
    }

    private void parseSoundInfo(String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            String s1 = null;
            try
            {
                s = (SoundInfo)JSON.parseObject(s, com/ximalaya/ting/android/model/sound/SoundInfo);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = s1;
            }
            if (s != null)
            {
                if (TextUtils.isEmpty(((SoundInfo) (s)).coverSmall))
                {
                    s1 = ((SoundInfo) (s)).coverLarge;
                } else
                {
                    s1 = ((SoundInfo) (s)).coverSmall;
                }
                mLoadTask = new a(mContext, mUIHandler, mWidgetIcon, s1);
                sLoadHander.post(mLoadTask);
                mWidgetTitle.setText(((SoundInfo) (s)).title);
                return;
            }
        }
    }

    public boolean onApplyTheme(Bundle bundle)
    {
        return false;
    }

    public void onDelete(int i)
    {
        Log.e("GoWodgetSmall", "Xm goWidget onDelete");
        mContext.unregisterReceiver(mReceiver);
        if (mLoadTask != null)
        {
            mLoadTask.a();
            sLoadHander.removeCallbacks(mLoadTask);
            mLoadTask = null;
        }
        i = sInstanceCount - 1;
        sInstanceCount = i;
        if (i <= 0)
        {
            sLoadThread.quit();
            sLoadThread = null;
            sLoadHander = null;
            sDiskCache = null;
        }
    }

    public void onEnter(int i)
    {
        Log.e("GoWodgetSmall", "Xm goWidget onEnter");
    }

    public void onLeave(int i)
    {
        Log.e("GoWodgetSmall", "Xm goWidget onLeave");
    }

    public void onRemove(int i)
    {
        Log.e("GoWodgetSmall", "Xm goWidget onRemove");
    }

    public void onStart(Bundle bundle)
    {
        Log.e("GoWodgetSmall", "Xm goWidget onStart");
        mFilter = new IntentFilter();
        mFilter.addAction("com.ximalaya.ting.android.action.ACTION_PLAY_CHANGE_SOUND");
        mFilter.addAction("com.ximalaya.ting.android.action.ACTION_PLAY_START");
        mFilter.addAction("com.ximalaya.ting.android.action.ACTION_PLAY_PAUSE");
        mFilter.addAction("com.ximalaya.ting.android.ACTION_PLAY_PROGRESS_UPDATE");
        mFilter.addAction("com.ximalaya.ting.android.action.ACTION_PLAY_CHANGE_SOUND");
        mContext.registerReceiver(mReceiver, mFilter);
        if (mLoadTask != null)
        {
            mLoadTask.a();
            sLoadHander.removeCallbacks(mLoadTask);
        }
        parseSoundInfo(sSoundInfoStr);
        if (sIsPlay)
        {
            mBtnPlayOrPause.setImageResource(0x7f0205c8);
            mBtnPlayOrPause.setContentDescription("\u6682\u505C");
            return;
        } else
        {
            mBtnPlayOrPause.setImageResource(0x7f0205cb);
            mBtnPlayOrPause.setContentDescription("\u5F00\u59CB\u64AD\u653E");
            return;
        }
    }

    static 
    {
        DOWNLOAD_CACHE_DIR = com.ximalaya.ting.android.a.af;
        sInstanceCount = 0;
        sIsPlay = false;
    }







/*
    static boolean access$402(boolean flag)
    {
        sIsPlay = flag;
        return flag;
    }

*/






/*
    static String access$802(String s)
    {
        sSoundInfoStr = s;
        return s;
    }

*/

}
