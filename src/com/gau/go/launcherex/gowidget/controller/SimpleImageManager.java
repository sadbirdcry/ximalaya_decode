// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.gau.go.launcherex.gowidget.controller;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

public class SimpleImageManager
{

    private static SimpleImageManager sInstance;
    private static byte sLock[] = new byte[0];
    private Handler mLoadHandler;
    private HandlerThread mLoadThread;
    private Handler mUIHandler;

    private SimpleImageManager()
    {
        mLoadThread = new HandlerThread("img_loader");
        mLoadThread.start();
        mLoadHandler = new Handler(mLoadThread.getLooper());
        mUIHandler = new Handler(Looper.getMainLooper());
    }

    public static SimpleImageManager getInstance()
    {
        if (sInstance == null)
        {
            synchronized (sLock)
            {
                if (sInstance == null)
                {
                    sInstance = new SimpleImageManager();
                }
            }
        }
        return sInstance;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

}
