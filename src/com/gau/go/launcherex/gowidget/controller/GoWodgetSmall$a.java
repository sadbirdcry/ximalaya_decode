// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.gau.go.launcherex.gowidget.controller;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import com.ximalaya.ting.android.util.DiskLruCache;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.net.HttpURLConnection;
import java.net.URL;

// Referenced classes of package com.gau.go.launcherex.gowidget.controller:
//            GoWodgetSmall

private static class a
    implements Runnable
{

    private Context a;
    private String b;
    private boolean c;
    private Handler d;
    private d e;

    public void a()
    {
        c = true;
    }

    public void a(Context context, Handler handler, ImageView imageview, String s)
    {
        c = false;
        a = context;
        b = s;
        d = handler;
        if (e == null)
        {
            e = new <init>(null);
        }
        e.a = imageview;
    }

    public void run()
    {
        if (GoWodgetSmall.access$100() > 0 && a != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (e.a == null)
        {
            Log.e("LoadImgTask", "mTaskWrapper.imageView == null, return");
            return;
        }
        if (d == null)
        {
            Log.e("LoadImgTask", "mUIHandler == null, return");
            return;
        }
        if (TextUtils.isEmpty(b))
        {
            Log.e("LoadImgTask", "mUrl is empty, return");
            return;
        }
        if (GoWodgetSmall.access$200() == null) goto _L1; else goto _L3
_L3:
        Object obj;
        obj = GoWodgetSmall.access$200().get(b);
        if (c)
        {
            Log.e("LoadImgTask", "Canceled, return");
            return;
        }
        if (obj != null)
        {
            Log.e("LoadImgTask", (new StringBuilder()).append("Xm get bitmap from cache ").append(b).toString());
            e.b = GoWodgetSmall.getRoundCornerBitmap(ThumbnailUtils.extractThumbnail(((android.graphics.Bitmap) (obj)), ToolUtil.dp2px(a, 100F), ToolUtil.dp2px(a, 100F)), ToolUtil.dp2px(a, 10F));
            if (c)
            {
                Log.e("LoadImgTask", "Canceled, return");
                return;
            } else
            {
                obj = d.obtainMessage();
                obj.obj = e;
                ((Message) (obj)).sendToTarget();
                return;
            }
        }
        try
        {
            Log.e("LoadImgTask", (new StringBuilder()).append("Xm get bitmap from net ").append(b).toString());
            obj = FreeFlowUtil.getHttpURLConnection(new URL(b));
            ((HttpURLConnection) (obj)).setConnectTimeout(5000);
            ((HttpURLConnection) (obj)).setRequestMethod("GET");
            ((HttpURLConnection) (obj)).setRequestProperty("Accept", "*/*");
            ((HttpURLConnection) (obj)).connect();
            if (c)
            {
                Log.e("LoadImgTask", "Canceled, return");
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((Exception) (obj)).printStackTrace();
            return;
        }
        obj = BitmapFactory.decodeStream(((HttpURLConnection) (obj)).getInputStream());
        if (c)
        {
            Log.e("LoadImgTask", "Canceled, return");
            return;
        }
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_339;
        }
        Log.e("LoadImgTask", "Bitmap == null, decode from stream, return");
        return;
        e.b = GoWodgetSmall.getRoundCornerBitmap(ThumbnailUtils.extractThumbnail(((android.graphics.Bitmap) (obj)), ToolUtil.dp2px(a, 100F), ToolUtil.dp2px(a, 100F)), ToolUtil.dp2px(a, 10F));
        if (c)
        {
            Log.e("LoadImgTask", "Canceled, return");
            return;
        }
        Message message = d.obtainMessage();
        message.obj = e;
        message.sendToTarget();
        GoWodgetSmall.access$200().put(b, ((android.graphics.Bitmap) (obj)));
        a = null;
        d = null;
        return;
    }

    public (Context context, Handler handler, ImageView imageview, String s)
    {
        a(context, handler, imageview, s);
    }
}
