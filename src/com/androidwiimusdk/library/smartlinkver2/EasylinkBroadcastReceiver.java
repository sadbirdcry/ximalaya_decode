// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.androidwiimusdk.library.smartlinkver2;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.io.PrintStream;

// Referenced classes of package com.androidwiimusdk.library.smartlinkver2:
//            IEasylinkSearchExecutor, a, OnLinkingListener

public class EasylinkBroadcastReceiver extends BroadcastReceiver
{

    IEasylinkSearchExecutor easylinkSearchExecutor;
    boolean isRegisted;
    OnLinkingListener linkListener;
    private a linkTask;
    private String password;

    public EasylinkBroadcastReceiver()
    {
        isRegisted = false;
        easylinkSearchExecutor = null;
    }

    private void executeHeader()
    {
        if (easylinkSearchExecutor != null)
        {
            System.err.println("sending EasyLinkHeader entry-----");
            easylinkSearchExecutor.executeEasylinkSearch();
        }
    }

    private void onStartEasylink(String s, int i, int j, int k, OnLinkingListener onlinkinglistener)
    {
        if (s == null)
        {
            throw new IllegalArgumentException("password is null");
        } else
        {
            linkListener = onlinkinglistener;
            password = s;
            linkTask = new a(this, false, s, i);
            linkTask.a(j);
            linkTask.b(k);
            (new Thread(linkTask)).start();
            return;
        }
    }

    private void onStartEasylink(String s, int i, OnLinkingListener onlinkinglistener)
    {
        if (s == null)
        {
            throw new IllegalArgumentException("password is null");
        } else
        {
            linkListener = onlinkinglistener;
            password = s;
            linkTask = new a(this, false, s, i);
            (new Thread(linkTask)).start();
            return;
        }
    }

    private void onStartEasylink(String s, OnLinkingListener onlinkinglistener)
    {
        if (s == null)
        {
            throw new IllegalArgumentException("password is null");
        } else
        {
            linkListener = onlinkinglistener;
            password = s;
            linkTask = new a(this, false, s, 0);
            (new Thread(linkTask)).start();
            return;
        }
    }

    private void registEasylinkBroadcast(Context context)
    {
        if (!isRegisted)
        {
            context.registerReceiver(this, new IntentFilter("easy link successful"));
            isRegisted = true;
        }
    }

    private void unregistEasylinkBroadcast(Context context)
    {
        if (isRegisted)
        {
            context.unregisterReceiver(this);
            isRegisted = false;
        }
    }

    public void onCreate(Application application)
    {
        registEasylinkBroadcast(application);
    }

    public void onDestroy(Application application)
    {
        unregistEasylinkBroadcast(application);
        if (linkTask != null)
        {
            a.a(linkTask);
        }
    }

    public void onReceive(Context context, Intent intent)
    {
        if (intent.getAction().equals("easy link successful") && intent.hasExtra("IP") && intent.hasExtra("UUID"))
        {
            unregistEasylinkBroadcast(context);
            context = intent.getStringExtra("IP");
            intent = intent.getStringExtra("UUID");
            if (linkTask != null)
            {
                linkTask.a(context, intent);
            }
        }
    }

    public void onStartEasylink(Context context, String s, int i, int j, int k, OnLinkingListener onlinkinglistener)
    {
        if (s == null || context == null)
        {
            throw new IllegalArgumentException("password is null");
        } else
        {
            linkListener = onlinkinglistener;
            password = s;
            linkTask = new a(this, true, s, i);
            linkTask.a(context);
            linkTask.a(j);
            linkTask.b(k);
            (new Thread(linkTask)).start();
            return;
        }
    }

    public void onStartEasylink(Context context, String s, OnLinkingListener onlinkinglistener)
    {
        if (s == null || context == null)
        {
            throw new IllegalArgumentException("password is null");
        } else
        {
            linkListener = onlinkinglistener;
            password = s;
            linkTask = new a(this, true, s, 0);
            linkTask.a(context);
            (new Thread(linkTask)).start();
            return;
        }
    }

    public void onStopEasylink()
    {
        if (linkTask != null)
        {
            a.a(linkTask);
        }
    }

    public void setEasylinkSearchExecutor(IEasylinkSearchExecutor ieasylinksearchexecutor)
    {
        easylinkSearchExecutor = ieasylinksearchexecutor;
    }

}
