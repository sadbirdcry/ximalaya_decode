// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.androidwiimusdk.library.smartlinkver2;

import android.content.Context;
import java.io.PrintStream;
import java.util.Timer;

// Referenced classes of package com.androidwiimusdk.library.smartlinkver2:
//            EasyLinkController, b, EasylinkBroadcastReceiver, OnLinkingListener, 
//            c, d

final class a
    implements Runnable
{

    EasyLinkController a;
    Timer b;
    final EasylinkBroadcastReceiver c;
    private boolean d;
    private Context e;
    private String f;
    private String g;

    public a(EasylinkBroadcastReceiver easylinkbroadcastreceiver, boolean flag, String s, int i)
    {
        c = easylinkbroadcastreceiver;
        super();
        d = flag;
        f = s;
        a = new EasyLinkController();
        a.setMaxDuration(i);
        b = new Timer();
        (new StringBuilder(String.valueOf(s))).append(" / ").append(System.currentTimeMillis()).toString();
    }

    private Context a()
    {
        return e;
    }

    static void a(a a1)
    {
        a1.b();
    }

    static EasylinkBroadcastReceiver b(a a1)
    {
        return a1.c;
    }

    private void b()
    {
        if (b != null)
        {
            b.cancel();
            b = null;
        }
        if (a != null)
        {
            a.stopSearch();
        }
    }

    public final void a(int i)
    {
        if (a != null)
        {
            a.setCycleMillis(i);
        }
    }

    public final void a(Context context)
    {
        e = context;
    }

    public final void a(String s, String s1)
    {
        b();
        (new b(this, (new StringBuilder("http://")).append(s).append("/httpapi.asp?command=EasyLinkResponseStop").toString(), s)).start();
        System.out.println("sending EasyLinkHeader parse ok all-----stopSearchAndCheckUpnp");
        if (c.linkListener != null)
        {
            c.linkListener.onLinkCompleted(s, s1);
        }
    }

    public final void b(int i)
    {
        if (a != null)
        {
            a.setSendingMillis(i);
        }
    }

    public final void run()
    {
        if (b == null)
        {
            b = new Timer();
        }
        b.scheduleAtFixedRate(new c(this), 0L, 4000L);
        a.setOnLinkingListener(new d(this));
        if (!d)
        {
            a.beginSearch(f);
        } else
        {
            try
            {
                a.beginSearch(e, f);
                return;
            }
            catch (Exception exception)
            {
                b();
            }
            if (c.linkListener != null)
            {
                c.linkListener.onTimeout();
                return;
            }
        }
    }
}
