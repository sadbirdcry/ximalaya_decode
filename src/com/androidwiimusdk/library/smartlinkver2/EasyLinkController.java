// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.androidwiimusdk.library.smartlinkver2;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;

// Referenced classes of package com.androidwiimusdk.library.smartlinkver2:
//            OnLinkingListener

public class EasyLinkController
{

    private static boolean a = false;
    private static String b = "UDP_LINK_HANDLER";
    private static int c = 22349;
    private static int d = 239;
    private static int e = 120;
    private static String f = "224.0.0.251";
    private static int g = 5353;
    private static int h = 0x11170;
    private static final int l = 400;
    private static final int m = 50;
    private static int p = 4;
    private static int q = 85;
    private static int r = 170;
    private int i;
    private int j;
    private boolean k;
    private int n;
    private int o;
    private OnLinkingListener s;

    public EasyLinkController()
    {
        i = 0x11170;
        k = true;
        n = 400;
        o = 50;
    }

    private native void WithWifiInfoBeginSearch(String s1, String s2, int i1);

    private void a()
    {
        if (s != null)
        {
            s.onTimeout();
        }
    }

    private void a(String s1, byte abyte0[], int i1)
    {
        DatagramSocket datagramsocket;
        Inet4Address.getLocalHost();
        datagramsocket = new DatagramSocket(22349);
        datagramsocket.setReuseAddress(true);
        abyte0 = new DatagramPacket(abyte0, 0);
        abyte0.setAddress(InetAddress.getByName(s1));
        datagramsocket.send(abyte0);
        if (s != null && k)
        {
            s.onSending(j, getMaxDuration());
        }
        datagramsocket.close();
_L1:
        return;
        s1;
        Log.e("UDP_LINK_HANDLER", (new StringBuilder("search err: ")).append(s1.getMessage()).toString());
        try
        {
            datagramsocket.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            return;
        }
        s1;
        if (s != null)
        {
            s.onSendFailure(s1);
            return;
        }
          goto _L1
    }

    private void a(byte abyte0[], int i1)
    {
        int ai[];
        int j1;
        ai = new int[4];
        j1 = 0;
_L3:
        if (j1 < 4) goto _L2; else goto _L1
_L1:
        int k1;
        a(getIP(ai), abyte0, 0);
        k1 = 0;
        j1 = 0;
_L4:
        if (k1 >= i1 || !k)
        {
            return;
        }
        break MISSING_BLOCK_LABEL_71;
_L2:
        if (j1 == 0)
        {
            ai[0] = 239;
        } else
        {
            ai[j1] = 0;
        }
        j1++;
          goto _L3
        char c1;
        if (j1 % 2 > 0)
        {
            c1 = 'U';
        } else
        {
            c1 = '\252';
        }
        ai[1] = Math.abs(j1 & 0x7f);
        ai[2] = Math.abs(c1);
        c1 = abyte0[k1];
        k1++;
        if (c1 < 0)
        {
            ai[3] = c1 & 0xff;
        } else
        {
            ai[3] = c1;
        }
        j1++;
        a(getIP(ai), abyte0, 0);
        try
        {
            Thread.sleep(getSendingMillis());
        }
        catch (InterruptedException interruptedexception)
        {
            interruptedexception.printStackTrace();
        }
          goto _L4
    }

    private static int b(byte abyte0[], int i1)
    {
        int j1;
        long l1;
        j1 = 0;
        l1 = 0L;
_L3:
        if (j1 < i1) goto _L2; else goto _L1
_L1:
        if (l1 >> 16 <= 0L)
        {
            return (short)(int)(~l1) & 0xffff;
        }
        break MISSING_BLOCK_LABEL_47;
_L2:
        long l2 = abyte0[j1];
        j1++;
        l1 = l2 + l1;
          goto _L3
        l1 = (l1 >> 16) + (65535L & l1);
          goto _L1
    }

    private static void b()
    {
    }

    private void beginSearch(String s1, String s2, int i1)
    {
    }

    private void beginSearch(byte abyte0[])
    {
        int k1;
        k = true;
        j = 0;
        if (s != null)
        {
            s.onStart();
            s.onSending(j, getMaxDuration());
        }
        k1 = abyte0.length;
_L6:
        if (k) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (j < getMaxDuration())
        {
            break; /* Loop/switch isn't completed */
        }
        stopSearch();
        if (s != null)
        {
            s.onTimeout();
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        int ai[];
        int i1;
        ai = new int[4];
        i1 = 0;
_L7:
        if (i1 < 4) goto _L5; else goto _L4
_L4:
        int j1;
        a(getIP(ai), abyte0, 0);
        j1 = 0;
        i1 = 0;
_L8:
        if (j1 < k1 && k)
        {
            break MISSING_BLOCK_LABEL_182;
        }
        InterruptedException interruptedexception1;
        char c1;
        try
        {
            Thread.sleep(getCycleMillis());
        }
        catch (InterruptedException interruptedexception)
        {
            interruptedexception.printStackTrace();
        }
        j = j + getCycleMillis();
          goto _L6
_L5:
        if (i1 == 0)
        {
            ai[0] = 239;
        } else
        {
            ai[i1] = 0;
        }
        i1++;
          goto _L7
        if (i1 % 2 > 0)
        {
            c1 = 'U';
        } else
        {
            c1 = '\252';
        }
        ai[1] = Math.abs(i1 & 0x7f);
        ai[2] = Math.abs(c1);
        c1 = abyte0[j1];
        j1++;
        if (c1 < 0)
        {
            ai[3] = c1 & 0xff;
        } else
        {
            ai[3] = c1;
        }
        i1++;
        a(getIP(ai), abyte0, 0);
        try
        {
            Thread.sleep(getSendingMillis());
        }
        // Misplaced declaration of an exception variable
        catch (InterruptedException interruptedexception1)
        {
            interruptedexception1.printStackTrace();
        }
          goto _L8
    }

    private static String getIP(int ai[])
    {
        String s1 = "";
        int i1 = 0;
        do
        {
            if (i1 >= 4)
            {
                return s1;
            }
            if (i1 < 3)
            {
                s1 = (new StringBuilder(String.valueOf(s1))).append(ai[i1]).append(".").toString();
            } else
            {
                s1 = (new StringBuilder(String.valueOf(s1))).append(ai[3]).toString();
            }
            i1++;
        } while (true);
    }

    private native boolean test(byte abyte0[]);

    private native boolean withPassword2Search(String s1);

    public void beginSearch(Context context, String s1)
    {
        int i1 = 0;
        Object obj = null;
        context = (WifiManager)context.getSystemService("wifi");
        boolean flag;
        if (context.getWifiState() == 3)
        {
            context = context.getConnectionInfo();
            i1 = context.getIpAddress();
            context = new StringBuffer(context.getSSID());
            if (context.charAt(0) == '"')
            {
                context.deleteCharAt(0);
            }
            if (context.charAt(context.length() - 1) == '"')
            {
                context.deleteCharAt(context.length() - 1);
            }
            context = context.toString();
            flag = true;
        } else
        {
            flag = false;
            context = obj;
        }
        if (!flag)
        {
            throw new IllegalArgumentException("wfman.getWifiState()!=WifiManager.WIFI_STATE_ENABLED");
        } else
        {
            WithWifiInfoBeginSearch(s1, context, i1);
            return;
        }
    }

    public void beginSearch(String s1)
    {
        if (!withPassword2Search(s1))
        {
            k = false;
            j = 0;
            if (s != null)
            {
                s.onSendFailure(new Exception(" beginSearch called failed"));
            }
        }
    }

    public int getCycleMillis()
    {
        return n;
    }

    public int getDuration()
    {
        return j;
    }

    public int getMaxDuration()
    {
        return i;
    }

    public int getSendingMillis()
    {
        return o;
    }

    public boolean isSearching()
    {
        return k;
    }

    public void setCycleMillis(int i1)
    {
        if (i1 >= 200) goto _L2; else goto _L1
_L1:
        int j1 = 200;
_L4:
        n = j1;
        return;
_L2:
        j1 = i1;
        if (i1 > 2000)
        {
            j1 = 2000;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void setMaxDuration(int i1)
    {
        if (i1 <= 0)
        {
            i = 0x11170;
            return;
        } else
        {
            i = i1;
            return;
        }
    }

    public void setOnLinkingListener(OnLinkingListener onlinkinglistener)
    {
        s = onlinkinglistener;
    }

    public void setSendingMillis(int i1)
    {
        if (i1 >= 20) goto _L2; else goto _L1
_L1:
        int j1 = 20;
_L4:
        o = j1;
        return;
_L2:
        j1 = i1;
        if (i1 > 60)
        {
            j1 = 60;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void stopSearch()
    {
        k = false;
        j = 0;
        if (s != null)
        {
            s.onStop();
        }
    }

    static 
    {
        System.loadLibrary("EasylinkControllerVer2");
    }
}
