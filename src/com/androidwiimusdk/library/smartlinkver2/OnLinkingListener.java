// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.androidwiimusdk.library.smartlinkver2;


public interface OnLinkingListener
{

    public abstract void onLinkCompleted(String s, String s1);

    public abstract void onSendFailure(Exception exception);

    public abstract void onSending(int i, int j);

    public abstract void onStart();

    public abstract void onStop();

    public abstract void onTimeout();
}
