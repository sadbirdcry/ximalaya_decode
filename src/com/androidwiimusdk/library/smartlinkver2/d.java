// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.androidwiimusdk.library.smartlinkver2;

import java.util.Timer;

// Referenced classes of package com.androidwiimusdk.library.smartlinkver2:
//            OnLinkingListener, a, EasylinkBroadcastReceiver

final class d
    implements OnLinkingListener
{

    private a a;

    d(a a1)
    {
        a = a1;
        super();
    }

    public final void onLinkCompleted(String s, String s1)
    {
        if (com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener != null)
        {
            com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener.onLinkCompleted(s, s1);
        }
    }

    public final void onSendFailure(Exception exception)
    {
        if (com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener != null)
        {
            com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener.onSendFailure(exception);
        }
    }

    public final void onSending(int i, int j)
    {
        if (com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener != null)
        {
            com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener.onSending(i, j);
        }
    }

    public final void onStart()
    {
        if (com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener != null)
        {
            com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener.onStart();
        }
    }

    public final void onStop()
    {
        if (a.b != null)
        {
            a.b.cancel();
            a.b = null;
        }
        if (com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener != null)
        {
            com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener.onStop();
        }
    }

    public final void onTimeout()
    {
        if (com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener != null)
        {
            com.androidwiimusdk.library.smartlinkver2.a.b(a).linkListener.onTimeout();
        }
    }
}
