// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.xiaomi.account.openauth;

import android.os.Bundle;

// Referenced classes of package com.xiaomi.account.openauth:
//            i

private static class h
{

    public final String a;
    public final String b;
    public final String c;
    public final String d;
    public final String e;
    public final String f;
    public final String g;
    public final String h;

    static h a(Bundle bundle)
    {
        return b(bundle);
    }

    private static b b(Bundle bundle)
    {
        String s = i.a(bundle, "access_token", "extra_access_token");
        String s1 = i.a(bundle, "mac_key", "extra_mac_key");
        String s2 = i.a(bundle, "mac_algorithm", "extra_mac_algorithm");
        return new <init>(s, i.b(bundle, "expires_in", "extra_expires_in"), i.a(bundle, "scope", "extra_scope"), i.a(bundle, "state", "extra_state"), i.a(bundle, "token_type", "extra_token_type"), s1, s2, i.a(bundle, "code", "extra_code"));
    }

    public String toString()
    {
        return (new StringBuilder()).append("accessToken=").append(a).append(",expiresIn=").append(b).append(",scope=").append(c).append(",state=").append(d).append(",tokenType=").append(e).append(",macKey=").append(f).append(",macAlogorithm=").append(g).append(",code=").append(h).toString();
    }

    public (String s, String s1, String s2, String s3, String s4, String s5, String s6, 
            String s7)
    {
        a = s;
        b = s1;
        c = s2;
        d = s3;
        e = s4;
        f = s5;
        g = s6;
        h = s7;
    }
}
