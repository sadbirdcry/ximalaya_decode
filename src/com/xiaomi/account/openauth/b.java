// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.xiaomi.account.openauth;

import android.net.Uri;
import android.text.TextUtils;
import com.xiaomi.account.openauth.a.a;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

// Referenced classes of package com.xiaomi.account.openauth:
//            c

public class b
{

    public static final boolean a;
    public static final String b;
    private static Random c = new Random();

    protected static String a()
    {
        long l = c.nextLong();
        int i = (int)(System.currentTimeMillis() / 60000L);
        return (new StringBuilder()).append(l).append(":").append(i).toString();
    }

    protected static String a(String s, String s1, String s2, String s3, String s4, String s5, String s6)
        throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException
    {
        if ("HmacSHA1".equalsIgnoreCase(s6))
        {
            s6 = new StringBuilder("");
            s6.append((new StringBuilder()).append(s).append("\n").toString());
            s6.append((new StringBuilder()).append(s1.toUpperCase()).append("\n").toString());
            s6.append((new StringBuilder()).append(s2).append("\n").toString());
            s6.append((new StringBuilder()).append(s3).append("\n").toString());
            if (!TextUtils.isEmpty(s4))
            {
                s = new StringBuffer();
                s1 = new ArrayList();
                URLEncodedUtils.parse(s1, new Scanner(s4), "UTF-8");
                Collections.sort(s1, new c());
                s.append(URLEncodedUtils.format(s1, "UTF-8"));
                s6.append((new StringBuilder()).append(s.toString()).append("\n").toString());
            }
            return a(a(s6.toString().getBytes("UTF-8"), s5.getBytes("UTF-8")));
        } else
        {
            throw new NoSuchAlgorithmException((new StringBuilder()).append("error mac algorithm : ").append(s6).toString());
        }
    }

    protected static String a(String s, List list)
    {
        Object obj = s;
        if (list != null)
        {
            obj = s;
            if (list.size() > 0)
            {
                s = Uri.parse(s).buildUpon();
                for (list = list.iterator(); list.hasNext(); s.appendQueryParameter(((NameValuePair) (obj)).getName(), ((NameValuePair) (obj)).getValue()))
                {
                    obj = (NameValuePair)list.next();
                }

                obj = s.build().toString();
            }
        }
        return ((String) (obj));
    }

    protected static String a(byte abyte0[])
    {
        return new String(com.xiaomi.account.openauth.a.a.a(abyte0));
    }

    protected static HashMap a(String s, String s1, String s2)
        throws UnsupportedEncodingException
    {
        s = String.format("MAC access_token=\"%s\", nonce=\"%s\",mac=\"%s\"", new Object[] {
            URLEncoder.encode(s, "UTF-8"), URLEncoder.encode(s1, "UTF-8"), URLEncoder.encode(s2, "UTF-8")
        });
        s1 = new HashMap();
        s1.put("Authorization", s);
        return s1;
    }

    protected static byte[] a(byte abyte0[], byte abyte1[])
        throws NoSuchAlgorithmException, InvalidKeyException
    {
        abyte1 = new SecretKeySpec(abyte1, "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(abyte1);
        mac.update(abyte0);
        return mac.doFinal();
    }

    static 
    {
        a = (new File("/data/system/oauth_staging_preview")).exists();
        String s;
        if (a)
        {
            s = "http://account.preview.n.xiaomi.net";
        } else
        {
            s = "https://account.xiaomi.com";
        }
        b = s;
    }
}
