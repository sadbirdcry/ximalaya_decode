// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.xiaomi.account.openauth;

import android.webkit.WebView;
import android.webkit.WebViewClient;

// Referenced classes of package com.xiaomi.account.openauth:
//            AuthorizeActivity

class a extends WebViewClient
{

    final AuthorizeActivity a;

    public boolean shouldOverrideUrlLoading(WebView webview, String s)
    {
        String s1 = new String(s);
        int i = s1.indexOf('?');
        if (i > 0)
        {
            String s2 = s1.substring(i + 1);
            if (s2.startsWith("code=") || s2.contains("&code="))
            {
                a.a(AuthorizeActivity.a, s1);
                return true;
            }
            if (s2.startsWith("error=") || s2.contains("&error="))
            {
                a.a(AuthorizeActivity.b, s1);
                return true;
            }
        } else
        {
            int j = s1.indexOf('#');
            if (j > 0)
            {
                String s3 = s1.substring(j + 1);
                if (s3.startsWith("access_token=") || s3.contains("&access_token="))
                {
                    a.a(AuthorizeActivity.a, s1.replace("#", "?"));
                    return true;
                }
                if (s3.startsWith("error=") || s3.contains("&error="))
                {
                    a.a(AuthorizeActivity.b, s1.replace("#", "?"));
                    return true;
                }
            }
        }
        return super.shouldOverrideUrlLoading(webview, s);
    }

    (AuthorizeActivity authorizeactivity)
    {
        a = authorizeactivity;
        super();
    }
}
