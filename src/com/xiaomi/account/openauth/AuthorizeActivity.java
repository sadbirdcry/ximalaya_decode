// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.xiaomi.account.openauth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import com.xiaomi.account.XiaomiOAuthResponse;
import com.xiaomi.account.a;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

// Referenced classes of package com.xiaomi.account.openauth:
//            b

public class AuthorizeActivity extends Activity
{
    class a extends WebViewClient
    {

        final AuthorizeActivity a;

        public boolean shouldOverrideUrlLoading(WebView webview, String s)
        {
            String s1 = new String(s);
            int j = s1.indexOf('?');
            if (j > 0)
            {
                String s2 = s1.substring(j + 1);
                if (s2.startsWith("code=") || s2.contains("&code="))
                {
                    a.a(com.xiaomi.account.openauth.AuthorizeActivity.a, s1);
                    return true;
                }
                if (s2.startsWith("error=") || s2.contains("&error="))
                {
                    a.a(AuthorizeActivity.b, s1);
                    return true;
                }
            } else
            {
                int k = s1.indexOf('#');
                if (k > 0)
                {
                    String s3 = s1.substring(k + 1);
                    if (s3.startsWith("access_token=") || s3.contains("&access_token="))
                    {
                        a.a(com.xiaomi.account.openauth.AuthorizeActivity.a, s1.replace("#", "?"));
                        return true;
                    }
                    if (s3.startsWith("error=") || s3.contains("&error="))
                    {
                        a.a(AuthorizeActivity.b, s1.replace("#", "?"));
                        return true;
                    }
                }
            }
            return super.shouldOverrideUrlLoading(webview, s);
        }

        a()
        {
            a = AuthorizeActivity.this;
            super();
        }
    }


    public static int a = -1;
    public static int b = 1;
    public static int c = 0;
    private static final String d;
    private WebView e;
    private WebSettings f;
    private String g;
    private boolean h;
    private XiaomiOAuthResponse i;

    public AuthorizeActivity()
    {
        h = false;
    }

    public static Intent a(Activity activity, Intent intent, com.xiaomi.account.a a1)
    {
        activity = new Intent(activity, com/xiaomi/account/openauth/AuthorizeActivity);
        activity.putExtra("extra_my_intent", intent);
        activity.putExtra("extra_response", new XiaomiOAuthResponse(a1));
        return activity;
    }

    public static Intent a(Activity activity, String s, String s1, String s2, String s3, String s4, Boolean boolean1, boolean flag, 
            com.xiaomi.account.a a1)
    {
        activity = new Intent(activity, com/xiaomi/account/openauth/AuthorizeActivity);
        Bundle bundle = new Bundle();
        bundle.putString("client_id", String.valueOf(s));
        bundle.putString("redirect_uri", s1);
        bundle.putString("response_type", s2);
        bundle.putString("scope", s3);
        bundle.putString("state", s4);
        if (boolean1 != null)
        {
            bundle.putString("skip_confirm", String.valueOf(boolean1));
        }
        activity.putExtra("url_param", bundle);
        activity.putExtra("extra_keep_cookies ", flag);
        activity.putExtra("extra_response", new XiaomiOAuthResponse(a1));
        return activity;
    }

    private Bundle a(String s)
    {
        Bundle bundle = new Bundle();
        if (s != null)
        {
            try
            {
                s = URLEncodedUtils.parse(new URI(s), "UTF-8").iterator();
                do
                {
                    if (!s.hasNext())
                    {
                        break;
                    }
                    NameValuePair namevaluepair = (NameValuePair)s.next();
                    if (!TextUtils.isEmpty(namevaluepair.getName()) && !TextUtils.isEmpty(namevaluepair.getValue()))
                    {
                        bundle.putString(namevaluepair.getName(), namevaluepair.getValue());
                    }
                } while (true);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Log.e("openauth", s.getMessage());
            }
        }
        return bundle;
    }

    private View a()
    {
        LinearLayout linearlayout = new LinearLayout(this);
        linearlayout.setLayoutParams(new android.view.ViewGroup.LayoutParams(-1, -1));
        linearlayout.setOrientation(1);
        e = new WebView(this);
        linearlayout.addView(e, new android.view.ViewGroup.LayoutParams(-1, -1));
        return linearlayout;
    }

    private String a(Bundle bundle)
    {
        if (bundle == null)
        {
            return "";
        }
        ArrayList arraylist = new ArrayList();
        Iterator iterator = bundle.keySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            String s = (String)iterator.next();
            String s1 = bundle.getString(s);
            if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s1))
            {
                arraylist.add(new BasicNameValuePair(s, s1));
            }
        } while (true);
        return URLEncodedUtils.format(arraylist, "UTF-8");
    }

    private static String a(Locale locale)
    {
        if (locale == null)
        {
            locale = null;
        } else
        {
            String s = locale.getLanguage();
            String s1 = locale.getCountry();
            locale = s;
            if (!TextUtils.isEmpty(s1))
            {
                return String.format("%s_%s", new Object[] {
                    s, s1
                });
            }
        }
        return locale;
    }

    private Bundle b(Bundle bundle)
    {
        String s;
        if (bundle != null && !bundle.containsKey("_locale"))
        {
            if (!TextUtils.isEmpty(s = a(Locale.getDefault())))
            {
                bundle.putString("_locale", s);
                return bundle;
            }
        }
        return bundle;
    }

    private void b()
    {
        if (!h)
        {
            CookieSyncManager.createInstance(this);
            CookieManager.getInstance().removeAllCookie();
        }
    }

    void a(int j, Bundle bundle)
    {
        Intent intent = new Intent();
        if (bundle != null)
        {
            intent.putExtras(bundle);
        }
        setResult(j, intent);
        if (i != null)
        {
            if (j == 0)
            {
                i.a();
            } else
            {
                i.a(bundle);
            }
        }
        b();
        finish();
    }

    void a(int j, String s)
    {
        a(j, a(s));
    }

    protected void onActivityResult(int j, int k, Intent intent)
    {
        if (j == 1001)
        {
            if (intent != null)
            {
                intent = intent.getExtras();
            } else
            {
                intent = null;
            }
            a(k, intent);
        }
    }

    public void onBackPressed()
    {
        if (e.canGoBack())
        {
            e.goBack();
            return;
        } else
        {
            a(c, (Bundle)null);
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getIntent();
        Object obj = bundle.getBundleExtra("extra_my_bundle");
        if (obj != null)
        {
            a(bundle.getIntExtra("extra_result_code", -1), ((Bundle) (obj)));
            return;
        }
        i = (XiaomiOAuthResponse)bundle.getParcelableExtra("extra_response");
        obj = (Intent)bundle.getParcelableExtra("extra_my_intent");
        if (obj != null)
        {
            startActivityForResult(((Intent) (obj)), 1001);
            return;
        } else
        {
            h = bundle.getBooleanExtra("extra_keep_cookies ", false);
            setContentView(a());
            f = e.getSettings();
            f.setJavaScriptEnabled(true);
            f.setSavePassword(false);
            f.setSaveFormData(false);
            bundle = b(bundle.getBundleExtra("url_param"));
            g = (new StringBuilder()).append(d).append("?").append(a(bundle)).toString();
            b();
            e.loadUrl(g);
            e.setWebViewClient(new a());
            return;
        }
    }

    static 
    {
        d = (new StringBuilder()).append(b.b).append("/oauth2/authorize").toString();
    }
}
