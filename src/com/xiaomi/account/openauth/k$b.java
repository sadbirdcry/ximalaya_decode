// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.xiaomi.account.openauth;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.xiaomi.account.a;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

// Referenced classes of package com.xiaomi.account.openauth:
//            k, r, s, i, 
//            g, AuthorizeActivity

private static class b extends FutureTask
{

    private final Activity a;
    private final g b;

    static a a(b b1)
    {
        return b1.b();
    }

    private a b()
    {
        return new s(this);
    }

    public Bundle a()
    {
        throw new IllegalStateException("this should never be called");
    }

    public Bundle a(long l, TimeUnit timeunit)
    {
        throw new IllegalStateException("this should never be called");
    }

    public void a(Bundle bundle)
    {
        if (bundle != null && bundle.containsKey("extra_intent"))
        {
            a((Intent)bundle.getParcelable("extra_intent"));
            return;
        } else
        {
            bundle = com.xiaomi.account.openauth.i.a(bundle);
            b.set(bundle);
            return;
        }
    }

    public boolean a(Intent intent)
    {
        if (intent == null)
        {
            return false;
        }
        Bundle bundle = intent.getExtras();
        bundle.setClassLoader(getClass().getClassLoader());
        Intent intent1 = intent;
        if (!bundle.containsKey("extra_response"))
        {
            intent1 = com.xiaomi.account.openauth.AuthorizeActivity.a(a, intent, b());
        }
        a.startActivity(intent1);
        return true;
    }

    public Object get()
        throws InterruptedException, ExecutionException
    {
        return a();
    }

    public Object get(long l, TimeUnit timeunit)
        throws InterruptedException, ExecutionException, TimeoutException
    {
        return a(l, timeunit);
    }

    public void set(Object obj)
    {
        a((Bundle)obj);
    }

    protected void setException(Throwable throwable)
    {
        b.setException(throwable);
    }

    public ption(Activity activity, g g1)
    {
        super(new r());
        a = activity;
        b = g1;
    }
}
