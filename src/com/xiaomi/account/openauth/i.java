// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.xiaomi.account.openauth;

import android.os.Bundle;
import android.text.TextUtils;

public class i
{
    public static class a
    {

        public final int a;
        public final String b;

        static a a(Bundle bundle)
        {
            return b(bundle);
        }

        private static a b(Bundle bundle)
        {
            return new a(i.c(bundle, "extra_error_code", "error"), i.a(bundle, "extra_error_description", "error_description"));
        }

        public String toString()
        {
            return (new StringBuilder()).append("errorCode=").append(a).append(",errorMessage=").append(b).toString();
        }

        public a(int j, String s)
        {
            a = j;
            b = s;
        }
    }

    private static class b
    {

        public final String a;
        public final String b;
        public final String c;
        public final String d;
        public final String e;
        public final String f;
        public final String g;
        public final String h;

        static b a(Bundle bundle)
        {
            return b(bundle);
        }

        private static b b(Bundle bundle)
        {
            String s = i.a(bundle, "access_token", "extra_access_token");
            String s1 = i.a(bundle, "mac_key", "extra_mac_key");
            String s2 = i.a(bundle, "mac_algorithm", "extra_mac_algorithm");
            return new b(s, i.b(bundle, "expires_in", "extra_expires_in"), i.a(bundle, "scope", "extra_scope"), i.a(bundle, "state", "extra_state"), i.a(bundle, "token_type", "extra_token_type"), s1, s2, i.a(bundle, "code", "extra_code"));
        }

        public String toString()
        {
            return (new StringBuilder()).append("accessToken=").append(a).append(",expiresIn=").append(b).append(",scope=").append(c).append(",state=").append(d).append(",tokenType=").append(e).append(",macKey=").append(f).append(",macAlogorithm=").append(g).append(",code=").append(h).toString();
        }

        public b(String s, String s1, String s2, String s3, String s4, String s5, String s6, 
                String s7)
        {
            a = s;
            b = s1;
            c = s2;
            d = s3;
            e = s4;
            f = s5;
            g = s6;
            h = s7;
        }
    }


    private final b a;
    private final a b;
    private final Bundle c;

    private i(Bundle bundle, a a1)
    {
        c = bundle;
        a = null;
        b = a1;
    }

    private i(Bundle bundle, b b1)
    {
        c = bundle;
        a = b1;
        b = null;
    }

    public static i a(Bundle bundle)
    {
        if (bundle == null)
        {
            return null;
        }
        if (e(bundle, "extra_error_code", "error") != 0)
        {
            return new i(bundle, a.a(bundle));
        } else
        {
            return new i(bundle, b.a(bundle));
        }
    }

    static String a(Bundle bundle, String s, String s1)
    {
        return d(bundle, s, s1);
    }

    static String b(Bundle bundle, String s, String s1)
    {
        return f(bundle, s, s1);
    }

    static int c(Bundle bundle, String s, String s1)
    {
        return e(bundle, s, s1);
    }

    private static String d(Bundle bundle, String s, String s1)
    {
        if (bundle.containsKey(s))
        {
            return bundle.getString(s);
        } else
        {
            return bundle.getString(s1);
        }
    }

    private static int e(Bundle bundle, String s, String s1)
    {
        if (bundle.containsKey(s))
        {
            return bundle.getInt(s);
        } else
        {
            return bundle.getInt(s1);
        }
    }

    private static String f(Bundle bundle, String s, String s1)
    {
        String as[] = new String[2];
        as[0] = s;
        as[1] = s1;
        int k = as.length;
        int j = 0;
        do
        {
            if (j >= k)
            {
                break;
            }
            s = as[j];
            if (!TextUtils.isEmpty(s) && bundle.containsKey(s))
            {
                s = ((String) (bundle.get(s)));
                if (s != null)
                {
                    if (s instanceof Integer)
                    {
                        return ((Integer)s).toString();
                    } else
                    {
                        return s.toString();
                    }
                }
            }
            j++;
        } while (true);
        return null;
    }

    public String a()
    {
        if (a != null)
        {
            return a.a;
        } else
        {
            return null;
        }
    }

    public String b()
    {
        if (a != null)
        {
            return a.b;
        } else
        {
            return null;
        }
    }

    public String c()
    {
        if (a != null)
        {
            return a.f;
        } else
        {
            return null;
        }
    }

    public String d()
    {
        if (a != null)
        {
            return a.g;
        } else
        {
            return null;
        }
    }

    public String toString()
    {
        if (a != null)
        {
            return a.toString();
        }
        if (b != null)
        {
            return b.toString();
        } else
        {
            throw new IllegalStateException("should not be here.");
        }
    }
}
