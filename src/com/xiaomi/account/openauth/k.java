// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.xiaomi.account.openauth;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.xiaomi.account.a;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

// Referenced classes of package com.xiaomi.account.openauth:
//            m, o, g, n, 
//            p, q, l, f, 
//            r, s, i, AuthorizeActivity

public class k
{
    private static final class a extends Enum
    {

        public static final a a;
        public static final a b;
        public static final a c;
        public static final a d;
        public static final a e;
        private static final a f[];

        public static a valueOf(String s)
        {
            return (a)Enum.valueOf(com/xiaomi/account/openauth/k$a, s);
        }

        public static a[] values()
        {
            return (a[])f.clone();
        }

        static 
        {
            a = new a("INIT", 0);
            b = new a("ADD_SYSTEM_ACCOUNT", 1);
            c = new a("OAUTH_FROM_MIUI", 2);
            d = new a("OAUTH_FROM_MIUI_WITH_RESPONSE", 3);
            e = new a("OAUTH_FROM_3RD_PARTY", 4);
            f = (new a[] {
                a, b, c, d, e
            });
        }

        private a(String s, int i)
        {
            super(s, i);
        }
    }

    private static class b extends FutureTask
    {

        private final Activity a;
        private final g b;

        static com.xiaomi.account.a a(b b1)
        {
            return b1.b();
        }

        private com.xiaomi.account.a b()
        {
            return new s(this);
        }

        public Bundle a()
        {
            throw new IllegalStateException("this should never be called");
        }

        public Bundle a(long l1, TimeUnit timeunit)
        {
            throw new IllegalStateException("this should never be called");
        }

        public void a(Bundle bundle)
        {
            if (bundle != null && bundle.containsKey("extra_intent"))
            {
                a((Intent)bundle.getParcelable("extra_intent"));
                return;
            } else
            {
                bundle = com.xiaomi.account.openauth.i.a(bundle);
                b.set(bundle);
                return;
            }
        }

        public boolean a(Intent intent)
        {
            if (intent == null)
            {
                return false;
            }
            Bundle bundle = intent.getExtras();
            bundle.setClassLoader(getClass().getClassLoader());
            Intent intent1 = intent;
            if (!bundle.containsKey("extra_response"))
            {
                intent1 = com.xiaomi.account.openauth.AuthorizeActivity.a(a, intent, b());
            }
            a.startActivity(intent1);
            return true;
        }

        public Object get()
            throws InterruptedException, ExecutionException
        {
            return a();
        }

        public Object get(long l1, TimeUnit timeunit)
            throws InterruptedException, ExecutionException, TimeoutException
        {
            return a(l1, timeunit);
        }

        public void set(Object obj)
        {
            a((Bundle)obj);
        }

        protected void setException(Throwable throwable)
        {
            b.setException(throwable);
        }

        public b(Activity activity, g g1)
        {
            super(new r());
            a = activity;
            b = g1;
        }
    }


    private boolean a;
    private int b[];
    private Long c;
    private String d;
    private Boolean e;
    private String f;
    private boolean g;

    public k()
    {
        a = false;
        b = null;
        c = null;
        d = null;
        e = null;
        f = null;
        g = false;
    }

    static Bundle a(Context context, Account account, Bundle bundle)
        throws ExecutionException, InterruptedException
    {
        return b(context, account, bundle);
    }

    static Bundle a(Context context, Bundle bundle, com.xiaomi.account.a a1)
        throws ExecutionException, InterruptedException
    {
        return b(context, bundle, a1);
    }

    private g a(Activity activity, String s)
    {
        if (activity == null || activity.isFinishing())
        {
            throw new IllegalArgumentException("activity should not be null and should be running");
        }
        if (c == null || c.longValue() <= 0L)
        {
            throw new IllegalArgumentException("client id is error!!!");
        }
        if (TextUtils.isEmpty(d))
        {
            throw new IllegalArgumentException("redirect url is empty!!!");
        }
        if (TextUtils.isEmpty(s))
        {
            throw new IllegalArgumentException("responseType is empty!!!");
        } else
        {
            return (new m(this, activity, s)).b();
        }
    }

    static boolean a(Context context)
    {
        return b(context);
    }

    static boolean a(k k1)
    {
        return k1.a;
    }

    static boolean a(k k1, Context context)
    {
        return k1.c(context);
    }

    static Account b(k k1, Context context)
    {
        return k1.d(context);
    }

    private static Bundle b(Context context, Account account, Bundle bundle)
        throws ExecutionException, InterruptedException
    {
        return (Bundle)(new o(context, account, bundle)).b().get();
    }

    private static Bundle b(Context context, Bundle bundle, com.xiaomi.account.a a1)
        throws ExecutionException, InterruptedException
    {
        return (Bundle)(new n(context, null, bundle, a1)).b().get();
    }

    static Long b(k k1)
    {
        return k1.c;
    }

    static String b(int ai[])
    {
        return c(ai);
    }

    private static boolean b(Context context)
    {
        boolean flag;
        try
        {
            flag = ((Boolean)(new p(context, null, null)).b().get()).booleanValue();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        return flag;
    }

    static String c(k k1)
    {
        return k1.d;
    }

    private static String c(int ai[])
    {
        int j = 0;
        if (ai != null && ai.length > 0)
        {
            StringBuilder stringbuilder = new StringBuilder();
            int i1 = ai.length;
            for (int i = 0; j < i1; i++)
            {
                int j1 = ai[j];
                if (i > 0)
                {
                    stringbuilder.append(' ');
                }
                stringbuilder.append(j1);
                j++;
            }

            return stringbuilder.toString();
        } else
        {
            return null;
        }
    }

    private boolean c(Context context)
    {
        boolean flag;
        try
        {
            flag = ((Boolean)(new q(this, context, null, null)).b().get()).booleanValue();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        return flag;
    }

    private Account d(Context context)
    {
        context = AccountManager.get(context).getAccountsByType("com.xiaomi");
        if (context.length == 0)
        {
            return null;
        } else
        {
            return context[0];
        }
    }

    static int[] d(k k1)
    {
        return k1.b;
    }

    static String e(k k1)
    {
        return k1.f;
    }

    static Boolean f(k k1)
    {
        return k1.e;
    }

    static boolean g(k k1)
    {
        return k1.g;
    }

    public f a(Activity activity)
    {
        return a(activity, "token");
    }

    public f a(Context context, long l1, String s, String s1, String s2, String s3)
    {
        return (new l(this, context, s, l1, s1, s2, s3)).b();
    }

    public k a(long l1)
    {
        c = Long.valueOf(l1);
        return this;
    }

    public k a(String s)
    {
        d = s;
        return this;
    }

    public k a(int ai[])
    {
        b = ai;
        return this;
    }

    // Unreferenced inner class com/xiaomi/account/openauth/k$1

/* anonymous class */
    static class _cls1
    {

        static final int a[];

        static 
        {
            a = new int[a.values().length];
            try
            {
                a[com.xiaomi.account.openauth.a.a.ordinal()] = 1;
            }
            catch (NoSuchFieldError nosuchfielderror4) { }
            try
            {
                a[a.b.ordinal()] = 2;
            }
            catch (NoSuchFieldError nosuchfielderror3) { }
            try
            {
                a[a.c.ordinal()] = 3;
            }
            catch (NoSuchFieldError nosuchfielderror2) { }
            try
            {
                a[a.d.ordinal()] = 4;
            }
            catch (NoSuchFieldError nosuchfielderror1) { }
            try
            {
                a[a.e.ordinal()] = 5;
            }
            catch (NoSuchFieldError nosuchfielderror)
            {
                return;
            }
        }
    }

}
