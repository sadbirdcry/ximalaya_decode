// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.xiaomi.account.openauth;

import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

// Referenced classes of package com.xiaomi.account.openauth:
//            j, k, AuthorizeActivity, g

class m extends j
{

    final Activity a;
    final String b;
    final k d;

    m(k k1, Activity activity, String s)
    {
        d = k1;
        a = activity;
        b = s;
        super();
    }

    private void a(k.b b1)
        throws ExecutionException, InterruptedException, IOException, OperationCanceledException
    {
        Object obj = k.a.a;
_L1:
        k._cls1.a[((k.a) (obj)).ordinal()];
        JVM INSTR tableswitch 1 5: default 48
    //                   1 51
    //                   2 127
    //                   3 207
    //                   4 234
    //                   5 251;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L2:
        if (k.a(d) || !k.a(a))
        {
            obj = k.a.e;
        } else
        if (k.a(d, a))
        {
            obj = k.a.d;
        } else
        if (k.b(d, a) != null)
        {
            obj = k.a.c;
        } else
        {
            obj = k.a.b;
        }
          goto _L1
_L3:
        obj = (Bundle)AccountManager.get(a).addAccount("com.xiaomi", null, null, null, a, null, null).getResult();
        if (obj == null) goto _L8; else goto _L7
_L7:
        if (!((Bundle) (obj)).containsKey("authAccount")) goto _L8; else goto _L9
_L9:
        obj = k.a.c;
          goto _L1
_L8:
        try
        {
            b1.setException(new Exception("fail to add account"));
            return;
        }
        catch (SecurityException securityexception)
        {
            securityexception = k.a.e;
        }
        catch (AuthenticatorException authenticatorexception)
        {
            authenticatorexception = k.a.e;
        }
          goto _L1
_L4:
        b1.a(k.a(a, k.b(d, a), c()));
        return;
_L5:
        k.a(a, c(), k.b.a(b1));
        return;
_L6:
        b1.a(AuthorizeActivity.a(a, String.valueOf(k.b(d)), k.c(d), b, k.b(k.d(d)), k.e(d), k.f(d), k.g(d), k.b.a(b1)));
        return;
    }

    private Bundle c()
    {
        Bundle bundle = new Bundle();
        bundle.putString("extra_client_id", String.valueOf(k.b(d)));
        bundle.putString("extra_redirect_uri", k.c(d));
        bundle.putString("extra_response_type", b);
        if (k.f(d) != null)
        {
            bundle.putBoolean("extra_skip_confirm", k.f(d).booleanValue());
        }
        if (!TextUtils.isEmpty(k.e(d)))
        {
            bundle.putString("extra_state", k.e(d));
        }
        String s = k.b(k.d(d));
        if (!TextUtils.isEmpty(s))
        {
            bundle.putString("extra_scope", s);
        }
        return bundle;
    }

    public void a()
    {
        k.b b1 = new k.b(a, c);
        try
        {
            a(b1);
            return;
        }
        catch (ExecutionException executionexception)
        {
            c.setException(executionexception.getCause());
            return;
        }
        catch (InterruptedException interruptedexception)
        {
            c.setException(interruptedexception);
            return;
        }
        catch (OperationCanceledException operationcanceledexception)
        {
            c.setException(operationcanceledexception);
            return;
        }
        catch (IOException ioexception)
        {
            c.setException(ioexception);
        }
    }
}
