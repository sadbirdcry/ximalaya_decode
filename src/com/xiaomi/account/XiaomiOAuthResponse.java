// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.xiaomi.account;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;

// Referenced classes of package com.xiaomi.account:
//            c, a

public class XiaomiOAuthResponse
    implements Parcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new c();
    private static final String a = com/xiaomi/account/XiaomiOAuthResponse.getName();
    private a b;

    public XiaomiOAuthResponse(Parcel parcel)
    {
        b = com.xiaomi.account.a.a.a(parcel.readStrongBinder());
    }

    public XiaomiOAuthResponse(a a1)
    {
        b = a1;
    }

    public static void a(a a1)
    {
        if (a1 == null)
        {
            return;
        }
        try
        {
            a1.a();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (a a1)
        {
            Log.e(a, "RuntimeException", a1);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (a a1)
        {
            Log.e(a, "RemoteException", a1);
        }
    }

    public static void a(a a1, Bundle bundle)
    {
        if (a1 == null || bundle == null)
        {
            return;
        }
        try
        {
            a1.a(bundle);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Bundle bundle)
        {
            Log.e(a, "RemoteException", bundle);
            Bundle bundle1 = new Bundle();
            bundle1.putInt("extra_error_code", -1);
            bundle1.putString("extra_error_description", bundle.getMessage());
            try
            {
                a1.a(bundle1);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (a a1)
            {
                Log.e(a, "RuntimeException", a1);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (a a1)
            {
                Log.e(a, "RemoteException", a1);
            }
            return;
        }
        // Misplaced declaration of an exception variable
        catch (a a1)
        {
            Log.e(a, "RemoteException", a1);
        }
    }

    public void a()
    {
        a(b);
    }

    public void a(Bundle bundle)
    {
        a(b, bundle);
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeStrongBinder(b.asBinder());
    }

}
