// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.squareup.picasso;

import android.graphics.Bitmap;
import android.net.NetworkInfo;
import java.io.IOException;

// Referenced classes of package com.squareup.picasso:
//            Request

public abstract class RequestHandler
{
    public static final class Result
    {

        private final Bitmap bitmap;
        private final int exifOrientation;
        private final Picasso.LoadedFrom loadedFrom;

        public Bitmap getBitmap()
        {
            return bitmap;
        }

        int getExifOrientation()
        {
            return exifOrientation;
        }

        public Picasso.LoadedFrom getLoadedFrom()
        {
            return loadedFrom;
        }

        public Result(Bitmap bitmap1, Picasso.LoadedFrom loadedfrom)
        {
            this(bitmap1, loadedfrom, 0);
        }

        Result(Bitmap bitmap1, Picasso.LoadedFrom loadedfrom, int i)
        {
            bitmap = bitmap1;
            loadedFrom = loadedfrom;
            exifOrientation = i;
        }
    }


    public RequestHandler()
    {
    }

    static void calculateInSampleSize(int i, int j, int k, int l, android.graphics.BitmapFactory.Options options, Request request)
    {
label0:
        {
            boolean flag = true;
            int i1;
            if (j <= 0 || l <= j)
            {
                i1 = ((flag) ? 1 : 0);
                if (i <= 0)
                {
                    break label0;
                }
                i1 = ((flag) ? 1 : 0);
                if (k <= i)
                {
                    break label0;
                }
            }
            if (j == 0)
            {
                i1 = (int)Math.floor((float)k / (float)i);
            } else
            if (i == 0)
            {
                i1 = (int)Math.floor((float)l / (float)j);
            } else
            {
                j = (int)Math.floor((float)l / (float)j);
                i = (int)Math.floor((float)k / (float)i);
                if (request.centerInside)
                {
                    i1 = Math.max(j, i);
                } else
                {
                    i1 = Math.min(j, i);
                }
            }
        }
        options.inSampleSize = i1;
        options.inJustDecodeBounds = false;
    }

    static void calculateInSampleSize(int i, int j, android.graphics.BitmapFactory.Options options, Request request)
    {
        calculateInSampleSize(i, j, options.outWidth, options.outHeight, options, request);
    }

    static android.graphics.BitmapFactory.Options createBitmapOptions(Request request)
    {
        android.graphics.BitmapFactory.Options options;
        boolean flag;
        if (request.config != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        options = new android.graphics.BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        if (flag)
        {
            options.inPreferredConfig = request.config;
        }
        return options;
    }

    static boolean requiresInSampleSize(android.graphics.BitmapFactory.Options options)
    {
        return options != null && options.inJustDecodeBounds;
    }

    public abstract boolean canHandleRequest(Request request);

    int getRetryCount()
    {
        return 0;
    }

    public abstract Result load(Request request)
        throws IOException;

    boolean shouldRetry(boolean flag, NetworkInfo networkinfo)
    {
        return false;
    }

    boolean supportsReplay()
    {
        return false;
    }
}
