// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.squareup.picasso;

import android.graphics.Bitmap;

// Referenced classes of package com.squareup.picasso:
//            Action, Callback, Picasso, Request

class FetchAction extends Action
{

    private final Object target = new Object();

    FetchAction(Picasso picasso, Request request, boolean flag, String s, Object obj, Callback callback)
    {
        super(picasso, null, request, flag, false, false, 0, null, s, obj, callback);
    }

    void complete(Bitmap bitmap, Picasso.LoadedFrom loadedfrom)
    {
        if (callback != null)
        {
            callback.onSuccess(bitmap);
        }
    }

    public void error()
    {
        if (callback != null)
        {
            callback.onError();
        }
    }

    Object getTarget()
    {
        return target;
    }
}
