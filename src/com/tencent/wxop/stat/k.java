// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.a.h;
import com.tencent.wxop.stat.b.b;
import java.util.Map;

// Referenced classes of package com.tencent.wxop.stat:
//            e, p, f

final class k
    implements Runnable
{

    final String b;
    final f bM;
    final Context e;

    k(Context context, String s, f f)
    {
        e = context;
        b = s;
        bM = f;
        super();
    }

    public final void run()
    {
        com.tencent.wxop.stat.e.p(e);
        Object obj1;
        synchronized (com.tencent.wxop.stat.e.M())
        {
            obj1 = (Long)com.tencent.wxop.stat.e.M().remove(b);
        }
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_191;
        }
        String s;
        try
        {
            obj1 = Long.valueOf((System.currentTimeMillis() - ((Long) (obj1)).longValue()) / 1000L);
        }
        catch (Throwable throwable)
        {
            com.tencent.wxop.stat.e.K().b(throwable);
            com.tencent.wxop.stat.e.a(e, throwable);
            return;
        }
        obj = obj1;
        if (((Long) (obj1)).longValue() <= 0L)
        {
            obj = Long.valueOf(1L);
        }
        s = com.tencent.wxop.stat.e.O();
        obj1 = s;
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_94;
        }
        obj1 = s;
        if (s.equals(b))
        {
            obj1 = "-";
        }
        obj = new h(e, ((String) (obj1)), b, com.tencent.wxop.stat.e.a(e, false, bM), ((Long) (obj)), bM);
        if (!b.equals(com.tencent.wxop.stat.e.N()))
        {
            com.tencent.wxop.stat.e.K().warn("Invalid invocation since previous onResume on diff page.");
        }
        (new p(((com.tencent.wxop.stat.a.d) (obj)))).ah();
        com.tencent.wxop.stat.e.r(b);
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        com.tencent.wxop.stat.e.K().d((new StringBuilder("Starttime for PageID:")).append(b).append(" not found, lost onResume()?").toString());
        return;
    }
}
