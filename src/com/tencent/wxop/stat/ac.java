// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tencent.wxop.stat.b.b;
import com.tencent.wxop.stat.b.r;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.tencent.wxop.stat:
//            c, t, ad

final class ac extends SQLiteOpenHelper
{

    private String a;
    private Context co;

    public ac(Context context, String s)
    {
        super(context, s, null, 3);
        a = "";
        co = null;
        a = s;
        co = context.getApplicationContext();
        if (c.k())
        {
            t.ao().b((new StringBuilder("SQLiteOpenHelper ")).append(a).toString());
        }
    }

    private static void a(SQLiteDatabase sqlitedatabase)
    {
        String s = null;
        Cursor cursor1 = sqlitedatabase.query("user", null, null, null, null, null, null);
        Cursor cursor = cursor1;
        ContentValues contentvalues = new ContentValues();
        cursor = cursor1;
        if (!cursor1.moveToNext())
        {
            break MISSING_BLOCK_LABEL_90;
        }
        cursor = cursor1;
        s = cursor1.getString(0);
        cursor = cursor1;
        cursor1.getInt(1);
        cursor = cursor1;
        cursor1.getString(2);
        cursor = cursor1;
        cursor1.getLong(3);
        cursor = cursor1;
        contentvalues.put("uid", r.q(s));
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_115;
        }
        cursor = cursor1;
        sqlitedatabase.update("user", contentvalues, "uid=?", new String[] {
            s
        });
        if (cursor1 != null)
        {
            cursor1.close();
        }
_L2:
        return;
        sqlitedatabase;
        cursor1 = null;
_L5:
        cursor = cursor1;
        t.ao().b(sqlitedatabase);
        if (cursor1 == null) goto _L2; else goto _L1
_L1:
        cursor1.close();
        return;
        sqlitedatabase;
        cursor = null;
_L4:
        if (cursor != null)
        {
            cursor.close();
        }
        throw sqlitedatabase;
        sqlitedatabase;
        if (true) goto _L4; else goto _L3
_L3:
        sqlitedatabase;
          goto _L5
    }

    private static void b(SQLiteDatabase sqlitedatabase)
    {
        Object obj = sqlitedatabase.query("events", null, null, null, null, null, null);
        Object obj1;
        Object obj2;
        obj2 = new ArrayList();
        for (; ((Cursor) (obj)).moveToNext(); ((List) (obj2)).add(new ad(((Cursor) (obj)).getLong(0), ((Cursor) (obj)).getString(1), ((Cursor) (obj)).getInt(2), ((Cursor) (obj)).getInt(3)))) { }
          goto _L1
_L6:
        t.ao().b(((Throwable) (obj)));
        if (sqlitedatabase != null)
        {
            sqlitedatabase.close();
        }
_L3:
        return;
_L1:
        try
        {
            ContentValues contentvalues = new ContentValues();
            ad ad1;
            for (obj2 = ((List) (obj2)).iterator(); ((Iterator) (obj2)).hasNext(); sqlitedatabase.update("events", contentvalues, "event_id=?", new String[] {
    Long.toString(ad1.K)
}))
            {
                ad1 = (ad)((Iterator) (obj2)).next();
                contentvalues.put("content", r.q(ad1.b));
            }

            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            sqlitedatabase = ((SQLiteDatabase) (obj));
            obj = obj1;
        }
        finally { }
        continue; /* Loop/switch isn't completed */
_L4:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw sqlitedatabase;
        if (obj == null) goto _L3; else goto _L2
_L2:
        ((Cursor) (obj)).close();
        return;
        sqlitedatabase;
        obj = null;
          goto _L4
        contentvalues;
        obj = sqlitedatabase;
        sqlitedatabase = contentvalues;
          goto _L4
        obj;
        sqlitedatabase = null;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public final void close()
    {
        this;
        JVM INSTR monitorenter ;
        super.close();
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public final void onCreate(SQLiteDatabase sqlitedatabase)
    {
        sqlitedatabase.execSQL("create table if not exists events(event_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, content TEXT, status INTEGER, send_count INTEGER, timestamp LONG)");
        sqlitedatabase.execSQL("create table if not exists user(uid TEXT PRIMARY KEY, user_type INTEGER, app_ver TEXT, ts INTEGER)");
        sqlitedatabase.execSQL("create table if not exists config(type INTEGER PRIMARY KEY NOT NULL, content TEXT, md5sum TEXT, version INTEGER)");
        sqlitedatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
        sqlitedatabase.execSQL("CREATE INDEX if not exists status_idx ON events(status)");
    }

    public final void onUpgrade(SQLiteDatabase sqlitedatabase, int i, int j)
    {
        t.ao().debug((new StringBuilder("upgrade DB from oldVersion ")).append(i).append(" to newVersion ").append(j).toString());
        if (i == 1)
        {
            sqlitedatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
            a(sqlitedatabase);
            b(sqlitedatabase);
        }
        if (i == 2)
        {
            a(sqlitedatabase);
            b(sqlitedatabase);
        }
    }
}
