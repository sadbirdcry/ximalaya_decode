// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.a.d;
import com.tencent.wxop.stat.a.e;
import com.tencent.wxop.stat.b.b;
import com.tencent.wxop.stat.b.l;
import com.tencent.wxop.stat.b.q;
import java.util.Map;

// Referenced classes of package com.tencent.wxop.stat:
//            c, t, s, e, 
//            ak, g, f, d, 
//            j, af, q, r, 
//            aj

final class p
{

    private static volatile long bU = 0L;
    private d bP;
    private com.tencent.wxop.stat.d bQ;
    private boolean bR;
    private Context bS;
    private long bT;

    public p(d d1)
    {
        bQ = null;
        bR = false;
        bS = null;
        bT = System.currentTimeMillis();
        bP = d1;
        bQ = com.tencent.wxop.stat.c.j();
        bR = d1.X();
        bS = d1.J();
    }

    private void H()
    {
        if (t.ai().aI > 0 && c.ax)
        {
            t.ai().b(bP, null, bR, true);
            t.ai().b(-1);
            return;
        } else
        {
            a(new s(this));
            return;
        }
    }

    static Context a(p p1)
    {
        return p1.bS;
    }

    private void a(aj aj)
    {
        ak.Z(e.J()).a(bP, aj);
    }

    static d b(p p1)
    {
        return p1.bP;
    }

    static boolean c(p p1)
    {
        return p1.bR;
    }

    public final void ah()
    {
        if (c.ae <= 0) goto _L2; else goto _L1
_L1:
        Integer integer;
        Integer integer1;
        if (bT > e.P())
        {
            e.Q().clear();
            e.c(bT + c.af);
            if (com.tencent.wxop.stat.c.k())
            {
                e.K().b((new StringBuilder("clear methodsCalledLimitMap, nextLimitCallClearTime=")).append(e.P()).toString());
            }
        }
        integer = Integer.valueOf(bP.ac().r());
        integer1 = (Integer)e.Q().get(integer);
        if (integer1 == null) goto _L4; else goto _L3
_L3:
        e.Q().put(integer, Integer.valueOf(integer1.intValue() + 1));
        if (integer1.intValue() <= c.ae) goto _L2; else goto _L5
_L5:
        boolean flag;
        if (com.tencent.wxop.stat.c.k())
        {
            e.K().d((new StringBuilder("event ")).append(bP.af()).append(" was discard, cause of called limit, current:").append(integer1).append(", limit:").append(c.ae).append(", period:").append(c.af).append(" ms").toString());
        }
        flag = true;
_L12:
        if (!flag) goto _L7; else goto _L6
_L6:
        return;
_L4:
        e.Q().put(integer, Integer.valueOf(1));
_L2:
        flag = false;
        continue; /* Loop/switch isn't completed */
_L7:
        if (c.ay > 0 && bT >= bU)
        {
            e.p(bS);
            bU = bT + c.az;
            if (com.tencent.wxop.stat.c.k())
            {
                e.K().b((new StringBuilder("nextFlushTime=")).append(bU).toString());
            }
        }
        if (!g.r(bS).X())
        {
            break; /* Loop/switch isn't completed */
        }
        if (com.tencent.wxop.stat.c.k())
        {
            e.K().b((new StringBuilder("sendFailedCount=")).append(e.aI).toString());
        }
        if (e.a())
        {
            break; /* Loop/switch isn't completed */
        }
        if (bP.ae() != null && bP.ae().R())
        {
            bQ = d.aB;
        }
        if (c.ah && g.r(e.J()).W())
        {
            bQ = d.aB;
        }
        if (com.tencent.wxop.stat.c.k())
        {
            e.K().b((new StringBuilder("strategy=")).append(bQ.name()).toString());
        }
        switch (j.bL[bQ.ordinal()])
        {
        default:
            e.K().error((new StringBuilder("Invalid stat strategy:")).append(com.tencent.wxop.stat.c.j()).toString());
            return;

        case 7: // '\007'
            continue; /* Loop/switch isn't completed */

        case 1: // '\001'
            H();
            return;

        case 2: // '\002'
            t.s(bS).b(bP, null, bR, false);
            if (com.tencent.wxop.stat.c.k())
            {
                e.K().b((new StringBuilder("PERIOD currTime=")).append(bT).append(",nextPeriodSendTs=").append(e.aZ).append(",difftime=").append(e.aZ - bT).toString());
            }
            if (e.aZ == 0L)
            {
                e.aZ = q.f(bS, "last_period_ts");
                if (bT > e.aZ)
                {
                    com.tencent.wxop.stat.e.q(bS);
                }
                long l1 = bT + (long)(com.tencent.wxop.stat.c.u() * 60 * 1000);
                if (e.aZ > l1)
                {
                    e.aZ = l1;
                }
                af.Y(bS).ah();
            }
            if (com.tencent.wxop.stat.c.k())
            {
                e.K().b((new StringBuilder("PERIOD currTime=")).append(bT).append(",nextPeriodSendTs=").append(e.aZ).append(",difftime=").append(e.aZ - bT).toString());
            }
            if (bT > e.aZ)
            {
                com.tencent.wxop.stat.e.q(bS);
                return;
            }
            break;

        case 3: // '\003'
        case 4: // '\004'
            t.s(bS).b(bP, null, bR, false);
            return;

        case 5: // '\005'
            t.s(bS).b(bP, new com.tencent.wxop.stat.q(this), bR, true);
            return;

        case 6: // '\006'
            if (g.r(e.J()).D() == 1)
            {
                H();
                return;
            } else
            {
                t.s(bS).b(bP, null, bR, false);
                return;
            }
        }
        if (true) goto _L6; else goto _L8
        if (!l.y(bS)) goto _L6; else goto _L9
_L9:
        a(new r(this));
        return;
_L8:
        t.s(bS).b(bP, null, bR, false);
        if (bT - e.aX > 0x1b7740L)
        {
            e.n(bS);
            return;
        }
        if (true) goto _L6; else goto _L10
_L10:
        t.s(bS).b(bP, null, bR, false);
        return;
        if (true) goto _L12; else goto _L11
_L11:
    }

}
