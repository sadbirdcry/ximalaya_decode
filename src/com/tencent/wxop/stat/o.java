// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.a.g;
import com.tencent.wxop.stat.b.b;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;

// Referenced classes of package com.tencent.wxop.stat:
//            b, e, c, p, 
//            f

final class o
    implements Runnable
{

    private f bM;
    private Map bO;
    private Context e;

    public o(Context context)
    {
        e = null;
        bO = null;
        bM = null;
        e = context;
        bM = null;
    }

    private static com.tencent.wxop.stat.b a(String s, int i)
    {
        com.tencent.wxop.stat.b b1;
        Socket socket;
        boolean flag;
        b1 = new com.tencent.wxop.stat.b();
        socket = new Socket();
        flag = false;
        b1.setDomain(s);
        b1.setPort(i);
        long l = System.currentTimeMillis();
        s = new InetSocketAddress(s, i);
        socket.connect(s, 30000);
        b1.a(System.currentTimeMillis() - l);
        b1.k(s.getAddress().getHostAddress());
        socket.close();
        socket.close();
        i = ((flag) ? 1 : 0);
_L1:
        b1.setStatusCode(i);
        return b1;
        s;
        com.tencent.wxop.stat.e.K().b(s);
        i = ((flag) ? 1 : 0);
          goto _L1
        s;
        i = -1;
        com.tencent.wxop.stat.e.K().b(s);
        try
        {
            socket.close();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            com.tencent.wxop.stat.e.K().b(s);
        }
          goto _L1
        s;
        try
        {
            socket.close();
        }
        catch (Throwable throwable)
        {
            com.tencent.wxop.stat.e.K().b(throwable);
        }
        throw s;
    }

    private static Map ag()
    {
        HashMap hashmap;
        String s;
        hashmap = new HashMap();
        s = c.l("__MTA_TEST_SPEED__");
        if (s != null && s.trim().length() != 0) goto _L2; else goto _L1
_L1:
        return hashmap;
_L2:
        String as[];
        int i;
        int j;
        as = s.split(";");
        j = as.length;
        i = 0;
_L4:
        if (i >= j) goto _L1; else goto _L3
_L3:
        String as1[];
        String s1;
        as1 = as[i].split(",");
        if (as1 == null || as1.length != 2)
        {
            break MISSING_BLOCK_LABEL_113;
        }
        s1 = as1[0];
        if (s1 == null || s1.trim().length() == 0)
        {
            break MISSING_BLOCK_LABEL_113;
        }
        int k = Integer.valueOf(as1[1]).intValue();
        hashmap.put(s1, Integer.valueOf(k));
_L5:
        i++;
          goto _L4
          goto _L1
        NumberFormatException numberformatexception;
        numberformatexception;
        com.tencent.wxop.stat.e.K().b(numberformatexception);
          goto _L5
    }

    public final void run()
    {
        Object obj;
        Iterator iterator;
        java.util.Map.Entry entry;
        String s;
        try
        {
            if (bO == null)
            {
                bO = ag();
            }
            if (bO == null || bO.size() == 0)
            {
                com.tencent.wxop.stat.e.K().b("empty domain list.");
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            com.tencent.wxop.stat.e.K().b(((Throwable) (obj)));
            return;
        }
        obj = new JSONArray();
        iterator = bO.entrySet().iterator();
_L1:
        if (!iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_201;
        }
        entry = (java.util.Map.Entry)iterator.next();
        s = (String)entry.getKey();
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_108;
        }
        if (s.length() != 0)
        {
            break MISSING_BLOCK_LABEL_128;
        }
        com.tencent.wxop.stat.e.K().c("empty domain name.");
          goto _L1
label0:
        {
            if ((Integer)entry.getValue() != null)
            {
                break label0;
            }
            com.tencent.wxop.stat.e.K().c((new StringBuilder("port is null for ")).append(s).toString());
        }
          goto _L1
        ((JSONArray) (obj)).put(a((String)entry.getKey(), ((Integer)entry.getValue()).intValue()).i());
          goto _L1
        if (((JSONArray) (obj)).length() != 0)
        {
            g g1 = new g(e, com.tencent.wxop.stat.e.a(e, false, bM), bM);
            g1.b(((JSONArray) (obj)).toString());
            (new p(g1)).ah();
        }
        return;
    }
}
