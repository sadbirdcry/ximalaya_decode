// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;


public final class d extends Enum
{

    public static final d aB;
    public static final d aC;
    public static final d aD;
    public static final d aE;
    public static final d aF;
    public static final d aG;
    public static final d aH;
    private static final d aJ[];
    int aI;

    private d(String s, int i, int j)
    {
        super(s, i);
        aI = j;
    }

    public static d a(int i)
    {
        d ad[] = values();
        int k = ad.length;
        for (int j = 0; j < k; j++)
        {
            d d1 = ad[j];
            if (i == d1.aI)
            {
                return d1;
            }
        }

        return null;
    }

    public static d valueOf(String s)
    {
        return (d)Enum.valueOf(com/tencent/wxop/stat/d, s);
    }

    public static d[] values()
    {
        return (d[])aJ.clone();
    }

    static 
    {
        aB = new d("INSTANT", 0, 1);
        aC = new d("ONLY_WIFI", 1, 2);
        aD = new d("BATCH", 2, 3);
        aE = new d("APP_LAUNCH", 3, 4);
        aF = new d("DEVELOPER", 4, 5);
        aG = new d("PERIOD", 5, 6);
        aH = new d("ONLY_WIFI_NO_CACHE", 6, 7);
        aJ = (new d[] {
            aB, aC, aD, aE, aF, aG, aH
        });
    }
}
