// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import com.tencent.wxop.stat.a.d;
import com.tencent.wxop.stat.b.b;
import com.tencent.wxop.stat.b.c;
import com.tencent.wxop.stat.b.f;
import com.tencent.wxop.stat.b.l;
import com.tencent.wxop.stat.b.r;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

// Referenced classes of package com.tencent.wxop.stat:
//            ac, ah, c, e, 
//            ak, aa, aj, ad, 
//            w, ab, x, y, 
//            u, v

public class t
{

    private static b bZ = l.av();
    private static Context ca = null;
    private static t cb = null;
    volatile int aI;
    private String ab;
    private ac bW;
    private ac bX;
    c bY;
    private f be;
    private String bq;
    private int cc;
    private ConcurrentHashMap cd;
    private boolean ce;
    private HashMap cf;

    private t(Context context)
    {
        bW = null;
        bX = null;
        be = null;
        ab = "";
        bq = "";
        aI = 0;
        bY = null;
        cc = 0;
        cd = null;
        ce = false;
        cf = new HashMap();
        try
        {
            be = new f();
            ca = context.getApplicationContext();
            cd = new ConcurrentHashMap();
            ab = l.J(context);
            bq = (new StringBuilder("pri_")).append(l.J(context)).toString();
            bW = new ac(ca, ab);
            bX = new ac(ca, bq);
            b(true);
            b(false);
            aj();
            t(ca);
            I();
            an();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            bZ.b(context);
        }
    }

    private void I()
    {
        Object obj1 = bW.getReadableDatabase().query("config", null, null, null, null, null, null);
_L4:
        Object obj = obj1;
        if (!((Cursor) (obj1)).moveToNext()) goto _L2; else goto _L1
_L1:
        obj = obj1;
        int i = ((Cursor) (obj1)).getInt(0);
        obj = obj1;
        String s1 = ((Cursor) (obj1)).getString(1);
        obj = obj1;
        String s2 = ((Cursor) (obj1)).getString(2);
        obj = obj1;
        int j = ((Cursor) (obj1)).getInt(3);
        obj = obj1;
        ah ah1 = new ah(i);
        obj = obj1;
        ah1.aI = i;
        obj = obj1;
        ah1.df = new JSONObject(s1);
        obj = obj1;
        ah1.c = s2;
        obj = obj1;
        ah1.L = j;
        obj = obj1;
        com.tencent.wxop.stat.c.a(ca, ah1);
        if (true) goto _L4; else goto _L3
_L3:
        Throwable throwable;
        throwable;
_L10:
        obj = obj1;
        bZ.b(throwable);
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
_L6:
        return;
_L2:
        if (obj1 == null) goto _L6; else goto _L5
_L5:
        ((Cursor) (obj1)).close();
        return;
        obj1;
        obj = null;
_L8:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw obj1;
        obj1;
        if (true) goto _L8; else goto _L7
_L7:
        throwable;
        obj1 = null;
        if (true) goto _L10; else goto _L9
_L9:
    }

    private void a(int i, boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        if (aI <= 0 || i <= 0) goto _L2; else goto _L1
_L1:
        boolean flag1 = e.a();
        if (!flag1) goto _L3; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        if (com.tencent.wxop.stat.c.k())
        {
            bZ.b((new StringBuilder("Load ")).append(aI).append(" unsent events").toString());
        }
        ArrayList arraylist = new ArrayList(i);
        b(arraylist, i, flag);
        if (arraylist.size() > 0)
        {
            if (com.tencent.wxop.stat.c.k())
            {
                bZ.b((new StringBuilder("Peek ")).append(arraylist.size()).append(" unsent events.").toString());
            }
            a(((List) (arraylist)), 2, flag);
            com.tencent.wxop.stat.ak.Z(ca).b(arraylist, new aa(this, arraylist, flag));
        }
        continue; /* Loop/switch isn't completed */
        Object obj;
        obj;
        bZ.b(((Throwable) (obj)));
        if (true) goto _L2; else goto _L4
_L4:
        obj;
        throw obj;
    }

    private void a(d d1, aj aj1, boolean flag, boolean flag1)
    {
        Object obj1 = null;
        Object obj = null;
        this;
        JVM INSTR monitorenter ;
        if (com.tencent.wxop.stat.c.s() <= 0) goto _L2; else goto _L1
_L1:
        int i = c.ay;
        if (i > 0 && !flag && !flag1) goto _L4; else goto _L3
_L3:
        SQLiteDatabase sqlitedatabase = c(flag);
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        sqlitedatabase.beginTransaction();
        if (flag)
        {
            break MISSING_BLOCK_LABEL_125;
        }
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        if (aI <= com.tencent.wxop.stat.c.s())
        {
            break MISSING_BLOCK_LABEL_125;
        }
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        bZ.warn("Too many events stored in db.");
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        aI = aI - bW.getWritableDatabase().delete("events", "event_id in (select event_id from events where timestamp in (select min(timestamp) from events) limit 1)", null);
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        ContentValues contentvalues = new ContentValues();
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        String s1 = d1.af();
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        if (!com.tencent.wxop.stat.c.k())
        {
            break MISSING_BLOCK_LABEL_202;
        }
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        bZ.b((new StringBuilder("insert 1 event, content:")).append(s1).toString());
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        contentvalues.put("content", com.tencent.wxop.stat.b.r.q(s1));
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        contentvalues.put("send_count", "0");
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        contentvalues.put("status", Integer.toString(1));
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        contentvalues.put("timestamp", Long.valueOf(d1.ad()));
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        long l1 = sqlitedatabase.insert("events", null, contentvalues);
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        sqlitedatabase.setTransactionSuccessful();
        long l2 = l1;
        if (sqlitedatabase == null) goto _L6; else goto _L5
_L5:
        sqlitedatabase.endTransaction();
_L12:
        if (l1 <= 0L) goto _L8; else goto _L7
_L7:
        aI = aI + 1;
        if (com.tencent.wxop.stat.c.k())
        {
            bZ.e((new StringBuilder("directStoreEvent insert event to db, event:")).append(d1.af()).toString());
        }
        if (aj1 == null) goto _L2; else goto _L9
_L9:
        aj1.ah();
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
        obj;
        bZ.b(((Throwable) (obj)));
        continue; /* Loop/switch isn't completed */
        Throwable throwable1;
        throwable1;
        l2 = -1L;
        obj1 = obj;
        bZ.b(throwable1);
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        ((SQLiteDatabase) (obj)).endTransaction();
        l1 = -1L;
        continue; /* Loop/switch isn't completed */
        Throwable throwable;
        throwable;
        bZ.b(throwable);
        l1 = -1L;
        continue; /* Loop/switch isn't completed */
        d1;
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_474;
        }
        ((SQLiteDatabase) (obj1)).endTransaction();
_L10:
        throw d1;
        d1;
        this;
        JVM INSTR monitorexit ;
        throw d1;
        aj1;
        bZ.b(aj1);
          goto _L10
_L8:
        bZ.error((new StringBuilder("Failed to store event:")).append(d1.af()).toString());
        continue; /* Loop/switch isn't completed */
_L4:
        if (c.ay <= 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (com.tencent.wxop.stat.c.k())
        {
            bZ.b((new StringBuilder("cacheEventsInMemory.size():")).append(cd.size()).append(",numEventsCachedInMemory:").append(c.ay).append(",numStoredEvents:").append(aI).toString());
            bZ.b((new StringBuilder("cache event:")).append(d1.af()).toString());
        }
        cd.put(d1, "");
        if (cd.size() >= c.ay)
        {
            am();
        }
        if (aj1 == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (cd.size() > 0)
        {
            am();
        }
        aj1.ah();
        if (true) goto _L2; else goto _L6
_L6:
        l1 = l2;
        if (true) goto _L12; else goto _L11
_L11:
    }

    private void a(ah ah1)
    {
        this;
        JVM INSTR monitorenter ;
        Cursor cursor1;
        String s2;
        ContentValues contentvalues;
        s2 = ah1.df.toString();
        String s1 = l.t(s2);
        contentvalues = new ContentValues();
        contentvalues.put("content", ah1.df.toString());
        contentvalues.put("md5sum", s1);
        ah1.c = s1;
        contentvalues.put("version", Integer.valueOf(ah1.L));
        cursor1 = bW.getReadableDatabase().query("config", null, null, null, null, null, null);
_L4:
        Cursor cursor = cursor1;
        if (!cursor1.moveToNext()) goto _L2; else goto _L1
_L1:
        cursor = cursor1;
        if (cursor1.getInt(0) != ah1.aI) goto _L4; else goto _L3
_L3:
        boolean flag = true;
_L15:
        cursor = cursor1;
        bW.getWritableDatabase().beginTransaction();
        if (!flag) goto _L6; else goto _L5
_L5:
        cursor = cursor1;
        long l1 = bW.getWritableDatabase().update("config", contentvalues, "type=?", new String[] {
            Integer.toString(ah1.aI)
        });
_L9:
        if (l1 != -1L) goto _L8; else goto _L7
_L7:
        cursor = cursor1;
        bZ.d((new StringBuilder("Failed to store cfg:")).append(s2).toString());
_L10:
        cursor = cursor1;
        bW.getWritableDatabase().setTransactionSuccessful();
        if (cursor1 == null)
        {
            break MISSING_BLOCK_LABEL_230;
        }
        cursor1.close();
        try
        {
            bW.getWritableDatabase().endTransaction();
        }
        // Misplaced declaration of an exception variable
        catch (ah ah1) { }
        this;
        JVM INSTR monitorexit ;
        return;
_L6:
        cursor = cursor1;
        contentvalues.put("type", Integer.valueOf(ah1.aI));
        cursor = cursor1;
        l1 = bW.getWritableDatabase().insert("config", null, contentvalues);
          goto _L9
_L8:
        cursor = cursor1;
        bZ.e((new StringBuilder("Sucessed to store cfg:")).append(s2).toString());
          goto _L10
        ah1;
_L13:
        cursor = cursor1;
        bZ.b(ah1);
        if (cursor1 == null)
        {
            break MISSING_BLOCK_LABEL_331;
        }
        cursor1.close();
        try
        {
            bW.getWritableDatabase().endTransaction();
        }
        // Misplaced declaration of an exception variable
        catch (ah ah1) { }
        break MISSING_BLOCK_LABEL_240;
        ah1;
        cursor = null;
_L12:
        if (cursor == null)
        {
            break MISSING_BLOCK_LABEL_361;
        }
        cursor.close();
        try
        {
            bW.getWritableDatabase().endTransaction();
        }
        catch (Exception exception) { }
        throw ah1;
        ah1;
        this;
        JVM INSTR monitorexit ;
        throw ah1;
        ah1;
        if (true) goto _L12; else goto _L11
_L11:
        ah1;
        cursor1 = null;
        if (true) goto _L13; else goto _L2
_L2:
        flag = false;
        if (true) goto _L15; else goto _L14
_L14:
    }

    static void a(t t1)
    {
        t1.am();
    }

    static void a(t t1, int i, boolean flag)
    {
        if (i == -1)
        {
            if (!flag)
            {
                i = t1.ak();
            } else
            {
                i = t1.al();
            }
        }
        if (i > 0)
        {
            int k = com.tencent.wxop.stat.c.u() * 60 * com.tencent.wxop.stat.c.q();
            int j = i;
            if (i > k)
            {
                j = i;
                if (k > 0)
                {
                    j = k;
                }
            }
            int i1 = com.tencent.wxop.stat.c.r();
            int j1 = j / i1;
            int k1 = j % i1;
            if (com.tencent.wxop.stat.c.k())
            {
                bZ.b((new StringBuilder("sentStoreEventsByDb sendNumbers=")).append(j).append(",important=").append(flag).append(",maxSendNumPerFor1Period=").append(k).append(",maxCount=").append(j1).append(",restNumbers=").append(k1).toString());
            }
            for (i = 0; i < j1; i++)
            {
                t1.a(i1, flag);
            }

            if (k1 > 0)
            {
                t1.a(k1, flag);
            }
        }
    }

    static void a(t t1, d d1, aj aj1, boolean flag, boolean flag1)
    {
        t1.a(d1, aj1, flag, flag1);
    }

    static void a(t t1, ah ah1)
    {
        t1.a(ah1);
    }

    static void a(t t1, List list, int i, boolean flag)
    {
        t1.a(list, i, flag);
    }

    static void a(t t1, List list, boolean flag)
    {
        t1.a(list, flag);
    }

    private void a(List list, int i, boolean flag)
    {
        String s2 = null;
        String s1 = null;
        this;
        JVM INSTR monitorenter ;
        int j = list.size();
        if (j != 0) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (flag) goto _L4; else goto _L3
_L3:
        j = com.tencent.wxop.stat.c.p();
_L8:
        SQLiteDatabase sqlitedatabase1 = c(flag);
        if (i != 2) goto _L6; else goto _L5
_L5:
        SQLiteDatabase sqlitedatabase = sqlitedatabase1;
        s2 = (new StringBuilder("update events set status=")).append(i).append(", send_count=send_count+1  where ").append(b(list)).toString();
        list = s1;
        s1 = s2;
_L9:
        sqlitedatabase = sqlitedatabase1;
        if (!com.tencent.wxop.stat.c.k())
        {
            break MISSING_BLOCK_LABEL_126;
        }
        sqlitedatabase = sqlitedatabase1;
        bZ.b((new StringBuilder("update sql:")).append(s1).toString());
        sqlitedatabase = sqlitedatabase1;
        sqlitedatabase1.beginTransaction();
        sqlitedatabase = sqlitedatabase1;
        sqlitedatabase1.execSQL(s1);
        if (list == null)
        {
            break MISSING_BLOCK_LABEL_195;
        }
        sqlitedatabase = sqlitedatabase1;
        bZ.b((new StringBuilder("update for delete sql:")).append(list).toString());
        sqlitedatabase = sqlitedatabase1;
        sqlitedatabase1.execSQL(list);
        sqlitedatabase = sqlitedatabase1;
        aj();
        sqlitedatabase = sqlitedatabase1;
        sqlitedatabase1.setTransactionSuccessful();
        if (sqlitedatabase1 == null) goto _L1; else goto _L7
_L7:
        sqlitedatabase1.endTransaction();
          goto _L1
        list;
        bZ.b(list);
          goto _L1
        list;
        throw list;
_L4:
        j = com.tencent.wxop.stat.c.n();
          goto _L8
_L6:
        sqlitedatabase = sqlitedatabase1;
        s1 = (new StringBuilder("update events set status=")).append(i).append(" where ").append(b(list)).toString();
        list = s2;
        sqlitedatabase = sqlitedatabase1;
        if (cc % 3 != 0)
        {
            break MISSING_BLOCK_LABEL_316;
        }
        sqlitedatabase = sqlitedatabase1;
        list = (new StringBuilder("delete from events where send_count>")).append(j).toString();
        sqlitedatabase = sqlitedatabase1;
        cc = cc + 1;
          goto _L9
        list;
        sqlitedatabase1 = null;
_L14:
        sqlitedatabase = sqlitedatabase1;
        bZ.b(list);
        if (sqlitedatabase1 == null) goto _L1; else goto _L10
_L10:
        sqlitedatabase1.endTransaction();
          goto _L1
        list;
        bZ.b(list);
          goto _L1
        list;
        sqlitedatabase = null;
_L13:
        if (sqlitedatabase == null)
        {
            break MISSING_BLOCK_LABEL_386;
        }
        sqlitedatabase.endTransaction();
_L11:
        throw list;
        Throwable throwable;
        throwable;
        bZ.b(throwable);
          goto _L11
        list;
        if (true) goto _L13; else goto _L12
_L12:
        list;
          goto _L14
    }

    private void a(List list, boolean flag)
    {
        Object obj = null;
        SQLiteDatabase sqlitedatabase = null;
        this;
        JVM INSTR monitorenter ;
        int i = list.size();
        if (i != 0) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        StringBuilder stringbuilder;
        int j;
        if (com.tencent.wxop.stat.c.k())
        {
            bZ.b((new StringBuilder("Delete ")).append(list.size()).append(" events, important:").append(flag).toString());
        }
        stringbuilder = new StringBuilder(list.size() * 3);
        stringbuilder.append("event_id in (");
        j = list.size();
        list = list.iterator();
        i = 0;
_L6:
        if (!list.hasNext())
        {
            break MISSING_BLOCK_LABEL_159;
        }
        stringbuilder.append(((ad)list.next()).K);
        if (i == j - 1)
        {
            break MISSING_BLOCK_LABEL_401;
        }
        stringbuilder.append(",");
        break MISSING_BLOCK_LABEL_401;
        stringbuilder.append(")");
        list = sqlitedatabase;
        sqlitedatabase = c(flag);
        list = sqlitedatabase;
        obj = sqlitedatabase;
        sqlitedatabase.beginTransaction();
        list = sqlitedatabase;
        obj = sqlitedatabase;
        i = sqlitedatabase.delete("events", stringbuilder.toString(), null);
        list = sqlitedatabase;
        obj = sqlitedatabase;
        if (!com.tencent.wxop.stat.c.k())
        {
            break MISSING_BLOCK_LABEL_278;
        }
        list = sqlitedatabase;
        obj = sqlitedatabase;
        bZ.b((new StringBuilder("delete ")).append(j).append(" event ").append(stringbuilder.toString()).append(", success delete:").append(i).toString());
        list = sqlitedatabase;
        obj = sqlitedatabase;
        aI = aI - i;
        list = sqlitedatabase;
        obj = sqlitedatabase;
        sqlitedatabase.setTransactionSuccessful();
        list = sqlitedatabase;
        obj = sqlitedatabase;
        aj();
        if (sqlitedatabase == null) goto _L1; else goto _L3
_L3:
        sqlitedatabase.endTransaction();
          goto _L1
        list;
        bZ.b(list);
          goto _L1
        list;
        throw list;
        Throwable throwable1;
        throwable1;
        obj = list;
        bZ.b(throwable1);
        if (list == null) goto _L1; else goto _L4
_L4:
        list.endTransaction();
          goto _L1
        list;
        bZ.b(list);
          goto _L1
        list;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_388;
        }
        ((SQLiteDatabase) (obj)).endTransaction();
_L5:
        throw list;
        Throwable throwable;
        throwable;
        bZ.b(throwable);
          goto _L5
        i++;
          goto _L6
    }

    public static t ai()
    {
        return cb;
    }

    private void aj()
    {
        aI = ak() + al();
    }

    private int ak()
    {
        return (int)DatabaseUtils.queryNumEntries(bW.getReadableDatabase(), "events");
    }

    private int al()
    {
        return (int)DatabaseUtils.queryNumEntries(bX.getReadableDatabase(), "events");
    }

    private void am()
    {
        Object obj2;
label0:
        {
            obj2 = null;
            obj = null;
            if (ce)
            {
                return;
            }
            synchronized (cd)
            {
                if (cd.size() != 0)
                {
                    break label0;
                }
            }
            return;
        }
        ce = true;
        if (com.tencent.wxop.stat.c.k())
        {
            bZ.b((new StringBuilder("insert ")).append(cd.size()).append(" events ,numEventsCachedInMemory:").append(c.ay).append(",numStoredEvents:").append(aI).toString());
        }
        Object obj3 = bW.getWritableDatabase();
        obj = obj3;
        obj2 = obj3;
        ((SQLiteDatabase) (obj3)).beginTransaction();
        obj = obj3;
        obj2 = obj3;
        Iterator iterator = cd.entrySet().iterator();
_L2:
        obj = obj3;
        obj2 = obj3;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        obj = obj3;
        obj2 = obj3;
        d d1 = (d)((java.util.Map.Entry)iterator.next()).getKey();
        obj = obj3;
        obj2 = obj3;
        ContentValues contentvalues = new ContentValues();
        obj = obj3;
        obj2 = obj3;
        String s1 = d1.af();
        obj = obj3;
        obj2 = obj3;
        if (!com.tencent.wxop.stat.c.k())
        {
            break MISSING_BLOCK_LABEL_240;
        }
        obj = obj3;
        obj2 = obj3;
        bZ.b((new StringBuilder("insert content:")).append(s1).toString());
        obj = obj3;
        obj2 = obj3;
        contentvalues.put("content", com.tencent.wxop.stat.b.r.q(s1));
        obj = obj3;
        obj2 = obj3;
        contentvalues.put("send_count", "0");
        obj = obj3;
        obj2 = obj3;
        contentvalues.put("status", Integer.toString(1));
        obj = obj3;
        obj2 = obj3;
        contentvalues.put("timestamp", Long.valueOf(d1.ad()));
        obj = obj3;
        obj2 = obj3;
        ((SQLiteDatabase) (obj3)).insert("events", null, contentvalues);
        obj = obj3;
        obj2 = obj3;
        iterator.remove();
        if (true) goto _L2; else goto _L1
        obj3;
        obj2 = obj;
        bZ.b(((Throwable) (obj3)));
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_359;
        }
        ((SQLiteDatabase) (obj)).endTransaction();
        aj();
_L4:
        ce = false;
        if (com.tencent.wxop.stat.c.k())
        {
            bZ.b((new StringBuilder("after insert, cacheEventsInMemory.size():")).append(cd.size()).append(",numEventsCachedInMemory:").append(c.ay).append(",numStoredEvents:").append(aI).toString());
        }
        concurrenthashmap;
        JVM INSTR monitorexit ;
        return;
_L1:
        obj = obj3;
        obj2 = obj3;
        ((SQLiteDatabase) (obj3)).setTransactionSuccessful();
        if (obj3 == null) goto _L4; else goto _L3
_L3:
        ((SQLiteDatabase) (obj3)).endTransaction();
        aj();
          goto _L4
        Object obj1;
        obj1;
        bZ.b(((Throwable) (obj1)));
          goto _L4
        obj1;
        bZ.b(((Throwable) (obj1)));
          goto _L4
        obj1;
        if (obj2 == null)
        {
            break MISSING_BLOCK_LABEL_486;
        }
        ((SQLiteDatabase) (obj2)).endTransaction();
        aj();
_L5:
        throw obj1;
        Throwable throwable;
        throwable;
        bZ.b(throwable);
          goto _L5
    }

    private void an()
    {
        Object obj1 = bW.getReadableDatabase().query("keyvalues", null, null, null, null, null, null);
_L4:
        Object obj = obj1;
        if (!((Cursor) (obj1)).moveToNext()) goto _L2; else goto _L1
_L1:
        obj = obj1;
        cf.put(((Cursor) (obj1)).getString(0), ((Cursor) (obj1)).getString(1));
        if (true) goto _L4; else goto _L3
_L3:
        Throwable throwable;
        throwable;
_L10:
        obj = obj1;
        bZ.b(throwable);
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
_L6:
        return;
_L2:
        if (obj1 == null) goto _L6; else goto _L5
_L5:
        ((Cursor) (obj1)).close();
        return;
        obj1;
        obj = null;
_L8:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw obj1;
        obj1;
        if (true) goto _L8; else goto _L7
_L7:
        throwable;
        obj1 = null;
        if (true) goto _L10; else goto _L9
_L9:
    }

    static b ao()
    {
        return bZ;
    }

    private static String b(List list)
    {
        StringBuilder stringbuilder = new StringBuilder(list.size() * 3);
        stringbuilder.append("event_id in (");
        int j = list.size();
        list = list.iterator();
        for (int i = 0; list.hasNext(); i++)
        {
            stringbuilder.append(((ad)list.next()).K);
            if (i != j - 1)
            {
                stringbuilder.append(",");
            }
        }

        stringbuilder.append(")");
        return stringbuilder.toString();
    }

    private void b(List list, int i, boolean flag)
    {
        if (flag) goto _L2; else goto _L1
_L1:
        Object obj = bW.getReadableDatabase();
_L6:
        String s1 = Integer.toString(1);
        String s2 = Integer.toString(i);
        obj = ((SQLiteDatabase) (obj)).query("events", null, "status=?", new String[] {
            s1
        }, null, null, null, s2);
_L5:
        if (!((Cursor) (obj)).moveToNext()) goto _L4; else goto _L3
_L3:
        String s3;
        long l1;
        l1 = ((Cursor) (obj)).getLong(0);
        s3 = ((Cursor) (obj)).getString(1);
        Object obj1 = s3;
        if (!c.ad)
        {
            obj1 = com.tencent.wxop.stat.b.r.t(s3);
        }
        i = ((Cursor) (obj)).getInt(2);
        int j = ((Cursor) (obj)).getInt(3);
        obj1 = new ad(l1, ((String) (obj1)), i, j);
        if (com.tencent.wxop.stat.c.k())
        {
            bZ.b((new StringBuilder("peek event, id=")).append(l1).append(",send_count=").append(j).append(",timestamp=").append(((Cursor) (obj)).getLong(4)).toString());
        }
        list.add(obj1);
          goto _L5
        Object obj2;
        obj2;
        list = ((List) (obj));
        obj = obj2;
_L11:
        bZ.b(((Throwable) (obj)));
        if (list != null)
        {
            list.close();
        }
_L8:
        return;
_L2:
        obj = bX.getReadableDatabase();
          goto _L6
_L4:
        if (obj == null) goto _L8; else goto _L7
_L7:
        ((Cursor) (obj)).close();
        return;
        list;
        obj = null;
_L10:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw list;
        list;
        continue; /* Loop/switch isn't completed */
        obj2;
        obj = list;
        list = ((List) (obj2));
        if (true) goto _L10; else goto _L9
_L9:
        obj;
        list = null;
          goto _L11
    }

    private void b(boolean flag)
    {
        Object obj;
        Object obj1;
        obj1 = null;
        obj = null;
        SQLiteDatabase sqlitedatabase = c(flag);
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        sqlitedatabase.beginTransaction();
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        ContentValues contentvalues = new ContentValues();
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        contentvalues.put("status", Integer.valueOf(1));
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        int i = sqlitedatabase.update("events", contentvalues, "status=?", new String[] {
            Long.toString(2L)
        });
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        if (!com.tencent.wxop.stat.c.k())
        {
            break MISSING_BLOCK_LABEL_136;
        }
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        bZ.b((new StringBuilder("update ")).append(i).append(" unsent events.").toString());
        obj = sqlitedatabase;
        obj1 = sqlitedatabase;
        sqlitedatabase.setTransactionSuccessful();
        if (sqlitedatabase == null)
        {
            break MISSING_BLOCK_LABEL_157;
        }
        sqlitedatabase.endTransaction();
_L2:
        return;
        obj;
        bZ.b(((Throwable) (obj)));
        return;
        Throwable throwable2;
        throwable2;
        obj1 = obj;
        bZ.b(throwable2);
        if (obj == null) goto _L2; else goto _L1
_L1:
        try
        {
            ((SQLiteDatabase) (obj)).endTransaction();
            return;
        }
        catch (Throwable throwable)
        {
            bZ.b(throwable);
        }
        return;
        Exception exception;
        exception;
        if (obj1 != null)
        {
            try
            {
                ((SQLiteDatabase) (obj1)).endTransaction();
            }
            catch (Throwable throwable1)
            {
                bZ.b(throwable1);
            }
        }
        throw exception;
    }

    private SQLiteDatabase c(boolean flag)
    {
        if (!flag)
        {
            return bW.getWritableDatabase();
        } else
        {
            return bX.getWritableDatabase();
        }
    }

    public static t s(Context context)
    {
        if (cb != null) goto _L2; else goto _L1
_L1:
        com/tencent/wxop/stat/t;
        JVM INSTR monitorenter ;
        if (cb == null)
        {
            cb = new t(context);
        }
        com/tencent/wxop/stat/t;
        JVM INSTR monitorexit ;
_L2:
        return cb;
        context;
        throw context;
    }

    final void H()
    {
        if (!com.tencent.wxop.stat.c.l())
        {
            return;
        }
        try
        {
            be.a(new w(this));
            return;
        }
        catch (Throwable throwable)
        {
            bZ.b(throwable);
        }
    }

    final void b(int i)
    {
        be.a(new ab(this, i));
    }

    final void b(d d1, aj aj1, boolean flag, boolean flag1)
    {
        if (be != null)
        {
            be.a(new x(this, d1, aj1, flag, flag1));
        }
    }

    final void b(ah ah1)
    {
        if (ah1 == null)
        {
            return;
        } else
        {
            be.a(new y(this, ah1));
            return;
        }
    }

    final void b(List list, boolean flag)
    {
        if (be != null)
        {
            be.a(new u(this, list, flag));
        }
    }

    final void c(List list, boolean flag)
    {
        if (be != null)
        {
            be.a(new v(this, list, flag));
        }
    }

    public final int r()
    {
        return aI;
    }

    public final c t(Context context)
    {
        this;
        JVM INSTR monitorenter ;
        if (bY == null) goto _L2; else goto _L1
_L1:
        context = bY;
_L27:
        this;
        JVM INSTR monitorexit ;
        return context;
_L2:
        Object obj1;
        bW.getWritableDatabase().beginTransaction();
        if (com.tencent.wxop.stat.c.k())
        {
            bZ.b("try to load user info from db.");
        }
        obj1 = bW.getReadableDatabase().query("user", null, null, null, null, null, null, null);
        int i = 0;
        if (!((Cursor) (obj1)).moveToNext()) goto _L4; else goto _L3
_L3:
        Object obj;
        Object obj2;
        String s2;
        int i1;
        long l1;
        long l3;
        s2 = ((Cursor) (obj1)).getString(0);
        obj2 = com.tencent.wxop.stat.b.r.t(s2);
        i1 = ((Cursor) (obj1)).getInt(1);
        obj = ((Cursor) (obj1)).getString(2);
        l1 = ((Cursor) (obj1)).getLong(3);
        l3 = System.currentTimeMillis() / 1000L;
        if (i1 == 1) goto _L6; else goto _L5
_L5:
        if (l.d(l1 * 1000L).equals(l.d(1000L * l3))) goto _L6; else goto _L7
_L7:
        i = 1;
_L36:
        Object obj3;
        String as[];
        Object obj6;
        int j;
        int k;
        if (!((String) (obj)).equals(l.G(context)))
        {
            j = i | 2;
        } else
        {
            j = i;
        }
        as = ((String) (obj2)).split(",");
        k = 0;
        i = 0;
        if (as == null) goto _L9; else goto _L8
_L8:
        if (as.length <= 0) goto _L9; else goto _L10
_L10:
        obj = as[0];
        if (obj == null) goto _L12; else goto _L11
_L11:
        if (((String) (obj)).length() >= 11) goto _L13; else goto _L12
_L12:
        obj3 = com.tencent.wxop.stat.b.r.b(context);
        if (obj3 == null) goto _L15; else goto _L14
_L14:
        if (((String) (obj3)).length() <= 10) goto _L15; else goto _L16
_L16:
        i = 1;
        obj = obj3;
          goto _L17
_L28:
        if (as == null) goto _L19; else goto _L18
_L18:
        if (as.length < 2) goto _L19; else goto _L20
_L20:
        obj6 = as[1];
        obj3 = (new StringBuilder()).append(((String) (obj2))).append(",").append(((String) (obj6))).toString();
        k = i;
_L30:
        bY = new c(((String) (obj2)), ((String) (obj6)), j);
        obj = new ContentValues();
        ((ContentValues) (obj)).put("uid", com.tencent.wxop.stat.b.r.q(((String) (obj3))));
        ((ContentValues) (obj)).put("user_type", Integer.valueOf(j));
        ((ContentValues) (obj)).put("app_ver", l.G(context));
        ((ContentValues) (obj)).put("ts", Long.valueOf(l3));
        if (k == 0)
        {
            break MISSING_BLOCK_LABEL_406;
        }
        bW.getWritableDatabase().update("user", ((ContentValues) (obj)), "uid=?", new String[] {
            s2
        });
        if (j == i1)
        {
            break MISSING_BLOCK_LABEL_889;
        }
        String s1;
        long l2;
        try
        {
            bW.getWritableDatabase().replace("user", null, ((ContentValues) (obj)));
            break MISSING_BLOCK_LABEL_889;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            context = ((Context) (obj1));
        }
        finally
        {
            continue; /* Loop/switch isn't completed */
        }
          goto _L21
_L4:
        if (i != 0) goto _L23; else goto _L22
_L22:
        obj2 = l.c(context);
        obj3 = l.w(context);
        if (obj3 == null) goto _L25; else goto _L24
_L24:
        if (((String) (obj3)).length() <= 0) goto _L25; else goto _L26
_L26:
        obj = (new StringBuilder()).append(((String) (obj2))).append(",").append(((String) (obj3))).toString();
_L35:
        l2 = System.currentTimeMillis() / 1000L;
        context = l.G(context);
        obj6 = new ContentValues();
        ((ContentValues) (obj6)).put("uid", com.tencent.wxop.stat.b.r.q(((String) (obj))));
        ((ContentValues) (obj6)).put("user_type", Integer.valueOf(0));
        ((ContentValues) (obj6)).put("app_ver", context);
        ((ContentValues) (obj6)).put("ts", Long.valueOf(l2));
        bW.getWritableDatabase().insert("user", null, ((ContentValues) (obj6)));
        bY = new c(((String) (obj2)), ((String) (obj3)), 0);
_L23:
        bW.getWritableDatabase().setTransactionSuccessful();
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_611;
        }
        ((Cursor) (obj1)).close();
        bW.getWritableDatabase().endTransaction();
_L32:
        context = bY;
          goto _L27
_L9:
        obj = l.c(context);
        i = 1;
        obj2 = obj;
          goto _L28
_L19:
        s1 = l.w(context);
        k = i;
        obj6 = s1;
        obj3 = obj;
        if (s1 == null) goto _L30; else goto _L29
_L29:
        k = i;
        obj6 = s1;
        obj3 = obj;
        if (s1.length() <= 0) goto _L30; else goto _L31
_L31:
        obj3 = (new StringBuilder()).append(((String) (obj2))).append(",").append(s1).toString();
        k = 1;
        obj6 = s1;
          goto _L30
        context;
        bZ.b(context);
          goto _L32
        context;
        throw context;
        obj;
        context = null;
_L21:
        bZ.b(((Throwable) (obj)));
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_758;
        }
        context.close();
        bW.getWritableDatabase().endTransaction();
          goto _L32
        context;
        bZ.b(context);
          goto _L32
        context;
        obj1 = null;
_L34:
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_795;
        }
        ((Cursor) (obj1)).close();
        bW.getWritableDatabase().endTransaction();
_L33:
        throw context;
        obj;
        bZ.b(((Throwable) (obj)));
          goto _L33
        obj;
        obj1 = context;
        context = ((Context) (obj));
        if (true) goto _L34; else goto _L21
_L25:
        obj = obj2;
          goto _L35
_L15:
        i = k;
          goto _L17
_L13:
        Object obj4 = obj;
        obj = obj2;
        obj2 = obj4;
          goto _L28
_L6:
        i = i1;
          goto _L36
_L17:
        Object obj5 = obj;
        obj = obj2;
        obj2 = obj5;
          goto _L28
        i = 1;
          goto _L4
    }

}
