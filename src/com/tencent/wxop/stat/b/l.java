// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.b;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Bundle;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.tencent.wxop.stat.c;
import com.tencent.wxop.stat.f;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpHost;
import org.json.JSONObject;

// Referenced classes of package com.tencent.wxop.stat.b:
//            b, r, a, q, 
//            p, m, o

public final class l
{

    private static int U = -1;
    private static String W = null;
    private static String a = null;
    private static String aR = null;
    private static String b = null;
    private static int bG = -1;
    private static volatile int bn = -1;
    private static String bq = null;
    private static String br = "";
    private static String bs = "";
    private static String c = null;
    private static String cC = null;
    private static String cE = "";
    private static Random cR = null;
    private static DisplayMetrics cS = null;
    private static b cT = null;
    private static String cU = null;
    private static String cV = null;
    private static long cW = -1L;
    private static o cX = null;
    private static String cY = "__MTA_FIRST_ACTIVATE__";
    private static long cZ = -1L;
    private static String da = "";
    private static int w = 0;

    public static String A(Context context)
    {
        context = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_46;
        }
        context = ((Context) (((ApplicationInfo) (context)).metaData.get("InstallChannel")));
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_38;
        }
        return context.toString();
        cT.c("Could not read InstallChannel meta-data from AndroidManifest.xml");
_L2:
        return null;
        context;
        cT.d("Could not read InstallChannel meta-data from AndroidManifest.xml");
        if (true) goto _L2; else goto _L1
_L1:
    }

    public static String B(Context context)
    {
        if (context == null)
        {
            return null;
        } else
        {
            return context.getClass().getName();
        }
    }

    public static String C(Context context)
    {
        if (bq != null)
        {
            return bq;
        }
        if (!com.tencent.wxop.stat.b.r.a(context, "android.permission.READ_PHONE_STATE"))
        {
            break MISSING_BLOCK_LABEL_71;
        }
        boolean flag;
        if (context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) != 0)
        {
            flag = false;
        } else
        {
            flag = true;
        }
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_62;
        }
        context = (TelephonyManager)context.getSystemService("phone");
        if (context != null)
        {
            try
            {
                bq = context.getSimOperator();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                cT.b(context);
            }
        }
        return bq;
        cT.d("Could not get permission of android.permission.READ_PHONE_STATE");
        break MISSING_BLOCK_LABEL_62;
    }

    public static String D(Context context)
    {
        if (e(br))
        {
            return br;
        }
        context = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        br = context;
        if (context == null)
        {
            return "";
        }
        break MISSING_BLOCK_LABEL_48;
        context;
        cT.b(context);
        return br;
    }

    public static String E(Context context)
    {
        Object obj;
        if (!com.tencent.wxop.stat.b.r.a(context, "android.permission.INTERNET") || !com.tencent.wxop.stat.b.r.a(context, "android.permission.ACCESS_NETWORK_STATE"))
        {
            break MISSING_BLOCK_LABEL_83;
        }
        obj = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_114;
        }
        if (!((NetworkInfo) (obj)).isConnected())
        {
            break MISSING_BLOCK_LABEL_114;
        }
        context = ((NetworkInfo) (obj)).getTypeName();
        obj = ((NetworkInfo) (obj)).getExtraInfo();
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_114;
        }
        if (context.equalsIgnoreCase("WIFI"))
        {
            return "WIFI";
        }
        if (context.equalsIgnoreCase("MOBILE"))
        {
            if (obj != null)
            {
                return ((String) (obj));
            } else
            {
                return "MOBILE";
            }
        }
        break MISSING_BLOCK_LABEL_108;
        try
        {
            cT.d("can not get the permission of android.permission.ACCESS_WIFI_STATE");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            cT.b(context);
            return "";
        }
        return "";
        if (obj != null)
        {
            return ((String) (obj));
        }
        break MISSING_BLOCK_LABEL_117;
        context = "";
        return context;
    }

    public static Integer F(Context context)
    {
        context = (TelephonyManager)context.getSystemService("phone");
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_25;
        }
        int i = context.getNetworkType();
        return Integer.valueOf(i);
        context;
        return null;
    }

    public static String G(Context context)
    {
        if (e(bs))
        {
            return bs;
        }
        context = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        bs = context;
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_46;
        }
        if (bs.length() != 0)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        return "unknown";
        context;
        cT.b(context);
        return bs;
    }

    public static String H(Context context)
    {
        if (e(cU))
        {
            return cU;
        }
        if (!com.tencent.wxop.stat.b.r.a(context, "android.permission.WRITE_EXTERNAL_STORAGE"))
        {
            break MISSING_BLOCK_LABEL_138;
        }
        context = Environment.getExternalStorageState();
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_136;
        }
        if (!context.equals("mounted"))
        {
            break MISSING_BLOCK_LABEL_136;
        }
        context = Environment.getExternalStorageDirectory().getPath();
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_136;
        }
        context = new StatFs(context);
        long l1 = ((long)context.getBlockCount() * (long)context.getBlockSize()) / 0xf4240L;
        long l2 = ((long)context.getAvailableBlocks() * (long)context.getBlockSize()) / 0xf4240L;
        context = (new StringBuilder()).append(String.valueOf(l2)).append("/").append(String.valueOf(l1)).toString();
        cU = context;
        return context;
        context;
        cT.b(context);
        return null;
        cT.warn("can not get the permission of android.permission.WRITE_EXTERNAL_STORAGE");
        return null;
    }

    static String I(Context context)
    {
        try
        {
label0:
            {
                if (aR != null)
                {
                    return aR;
                }
                int i = Process.myPid();
                context = ((ActivityManager)context.getSystemService("activity")).getRunningAppProcesses().iterator();
                android.app.ActivityManager.RunningAppProcessInfo runningappprocessinfo;
                do
                {
                    if (!context.hasNext())
                    {
                        break label0;
                    }
                    runningappprocessinfo = (android.app.ActivityManager.RunningAppProcessInfo)context.next();
                } while (runningappprocessinfo.pid != i);
                aR = runningappprocessinfo.processName;
            }
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        return aR;
    }

    public static String J(Context context)
    {
        return e(context, a.ct);
    }

    static b K()
    {
        return cT;
    }

    public static Integer K(Context context)
    {
        int i = 0;
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorenter ;
        int j;
        if (bn <= 0)
        {
            break MISSING_BLOCK_LABEL_79;
        }
        j = bn;
        if (j % 1000 != 0)
        {
            break MISSING_BLOCK_LABEL_44;
        }
        j = bn;
        if (bn < 0x7ffe795f)
        {
            i = j + 1000;
        }
        q.b(context, "MTA_EVENT_INDEX", i);
_L1:
        i = bn + 1;
        bn = i;
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorexit ;
        return Integer.valueOf(i);
        context;
        cT.c(context);
          goto _L1
        context;
        throw context;
        bn = q.a(context, "MTA_EVENT_INDEX", 0);
        q.b(context, "MTA_EVENT_INDEX", bn + 1000);
          goto _L1
    }

    public static String L(Context context)
    {
        try
        {
            context = (ActivityManager)context.getSystemService("activity");
            android.app.ActivityManager.MemoryInfo memoryinfo = new android.app.ActivityManager.MemoryInfo();
            context.getMemoryInfo(memoryinfo);
            long l1 = memoryinfo.availMem / 0xf4240L;
            long l2 = ay() / 0xf4240L;
            context = (new StringBuilder()).append(String.valueOf(l1)).append("/").append(String.valueOf(l2)).toString();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return null;
        }
        return context;
    }

    public static String M(Context context)
    {
        if (e(cE))
        {
            return cE;
        }
        context = (SensorManager)context.getSystemService("sensor");
        if (context == null) goto _L2; else goto _L1
_L1:
        context = context.getSensorList(-1);
        if (context == null) goto _L2; else goto _L3
_L3:
        StringBuilder stringbuilder = new StringBuilder(context.size() * 10);
        int i = 0;
_L8:
        if (i >= context.size()) goto _L5; else goto _L4
_L4:
        stringbuilder.append(((Sensor)context.get(i)).getType());
        if (i != context.size() - 1)
        {
            stringbuilder.append(",");
        }
          goto _L6
_L5:
        try
        {
            cE = stringbuilder.toString();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            cT.b(context);
        }
_L2:
        return cE;
_L6:
        i++;
        if (true) goto _L8; else goto _L7
_L7:
    }

    public static int N(Context context)
    {
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorenter ;
        if (U == -1) goto _L2; else goto _L1
_L1:
        int i = U;
_L4:
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorexit ;
        return i;
_L2:
        O(context);
        i = U;
        if (true) goto _L4; else goto _L3
_L3:
        context;
        throw context;
    }

    public static void O(Context context)
    {
        int i = q.a(context, cY, 1);
        U = i;
        if (i == 1)
        {
            q.b(context, cY, 0);
        }
    }

    public static boolean P(Context context)
    {
        if (cZ < 0L)
        {
            cZ = com.tencent.wxop.stat.b.q.f(context, "mta.qq.com.checktime");
        }
        return Math.abs(System.currentTimeMillis() - cZ) > 0x5265c00L;
    }

    public static void Q(Context context)
    {
        cZ = System.currentTimeMillis();
        q.a(context, "mta.qq.com.checktime", cZ);
    }

    public static String R(Context context)
    {
        if (context != null)
        {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            context = context.getPackageManager().resolveActivity(intent, 0);
            if (((ResolveInfo) (context)).activityInfo != null && !((ResolveInfo) (context)).activityInfo.packageName.equals("android"))
            {
                return ((ResolveInfo) (context)).activityInfo.packageName;
            }
        }
        return null;
    }

    public static int a(Context context, boolean flag)
    {
        if (flag)
        {
            w = q.a(context, "mta.qq.com.difftime", 0);
        }
        return w;
    }

    private static Long a(String s, String s1, Long long1)
    {
        if (s != null && s1 != null) goto _L2; else goto _L1
_L1:
        return long1;
_L2:
        String s2;
label0:
        {
            if (!s1.equalsIgnoreCase("."))
            {
                s2 = s1;
                if (!s1.equalsIgnoreCase("|"))
                {
                    break label0;
                }
            }
            s2 = (new StringBuilder("\\")).append(s1).toString();
        }
        s1 = s.split(s2);
        if (s1.length != 3) goto _L1; else goto _L3
_L3:
        int i;
        long l1;
        long l2;
        try
        {
            s = Long.valueOf(0L);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return long1;
        }
        i = 0;
        if (i >= s1.length)
        {
            break; /* Loop/switch isn't completed */
        }
        l1 = s.longValue();
        l2 = Long.valueOf(s1[i]).longValue();
        i++;
        s = Long.valueOf(100L * (l1 + l2));
        if (true) goto _L5; else goto _L4
_L5:
        break MISSING_BLOCK_LABEL_70;
_L4:
        return s;
    }

    public static void a(Context context, int i)
    {
        w = i;
        q.b(context, "mta.qq.com.difftime", i);
    }

    public static boolean a(f f1)
    {
        if (f1 == null)
        {
            return false;
        } else
        {
            return e(f1.S());
        }
    }

    public static long ad()
    {
        long l1;
        try
        {
            Calendar calendar = Calendar.getInstance();
            calendar.set(11, 0);
            calendar.set(12, 0);
            calendar.set(13, 0);
            calendar.set(14, 0);
            l1 = calendar.getTimeInMillis();
        }
        catch (Throwable throwable)
        {
            cT.b(throwable);
            return System.currentTimeMillis() + 0x5265c00L;
        }
        return l1 + 0x5265c00L;
    }

    private static Random at()
    {
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorenter ;
        Random random;
        if (cR == null)
        {
            cR = new Random();
        }
        random = cR;
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorexit ;
        return random;
        Exception exception;
        exception;
        throw exception;
    }

    public static int au()
    {
        if (bG != -1)
        {
            return bG;
        }
        try
        {
            if (p.a())
            {
                bG = 1;
            }
        }
        catch (Throwable throwable)
        {
            cT.b(throwable);
        }
        bG = 0;
        return 0;
    }

    public static b av()
    {
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorenter ;
        b b2;
        if (cT == null)
        {
            b b1 = new b("MtaSDK");
            cT = b1;
            b1.ap();
        }
        b2 = cT;
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorexit ;
        return b2;
        Exception exception;
        exception;
        throw exception;
    }

    public static String aw()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.roll(6, 0);
        return (new SimpleDateFormat("yyyyMMdd")).format(calendar.getTime());
    }

    public static String ax()
    {
        if (e(cC))
        {
            return cC;
        } else
        {
            Object obj = new StatFs(Environment.getDataDirectory().getPath());
            long l1 = ((StatFs) (obj)).getBlockSize();
            l1 = ((long)((StatFs) (obj)).getBlockCount() * l1) / 0xf4240L;
            obj = new StatFs(Environment.getDataDirectory().getPath());
            long l2 = ((StatFs) (obj)).getBlockSize();
            l2 = ((long)((StatFs) (obj)).getAvailableBlocks() * l2) / 0xf4240L;
            obj = (new StringBuilder()).append(String.valueOf(l2)).append("/").append(String.valueOf(l1)).toString();
            cC = ((String) (obj));
            return ((String) (obj));
        }
    }

    private static long ay()
    {
        long l1;
        long l2;
        if (cW > 0L)
        {
            return cW;
        }
        l2 = 1L;
        l1 = l2;
        BufferedReader bufferedreader = new BufferedReader(new FileReader("/proc/meminfo"), 8192);
        l1 = l2;
        l2 = Integer.valueOf(bufferedreader.readLine().split("\\s+")[1]).intValue() * 1024;
        l1 = l2;
        bufferedreader.close();
        l1 = l2;
_L2:
        cW = l1;
        return l1;
        Exception exception;
        exception;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public static JSONObject az()
    {
        JSONObject jsonobject = new JSONObject();
        String s;
        int i;
        try
        {
            jsonobject.put("n", m.r());
            s = m.ax();
        }
        catch (Throwable throwable)
        {
            Log.w("MtaSDK", "get cpu error", throwable);
            return jsonobject;
        }
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_43;
        }
        if (s.length() > 0)
        {
            jsonobject.put("na", s);
        }
        i = m.aA();
        if (i <= 0)
        {
            break MISSING_BLOCK_LABEL_64;
        }
        jsonobject.put("fx", i / 0xf4240);
        i = m.D();
        if (i <= 0)
        {
            break MISSING_BLOCK_LABEL_85;
        }
        jsonobject.put("fn", i / 0xf4240);
        return jsonobject;
    }

    public static byte[] b(byte abyte0[])
    {
        ByteArrayInputStream bytearrayinputstream = new ByteArrayInputStream(abyte0);
        GZIPInputStream gzipinputstream = new GZIPInputStream(bytearrayinputstream);
        byte abyte1[] = new byte[4096];
        abyte0 = new ByteArrayOutputStream(abyte0.length * 2);
        do
        {
            int i = gzipinputstream.read(abyte1);
            if (i != -1)
            {
                abyte0.write(abyte1, 0, i);
            } else
            {
                byte abyte2[] = abyte0.toByteArray();
                bytearrayinputstream.close();
                gzipinputstream.close();
                abyte0.close();
                return abyte2;
            }
        } while (true);
    }

    public static String c(Context context)
    {
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorenter ;
        if (a == null || a.trim().length() == 0) goto _L2; else goto _L1
_L1:
        context = a;
_L4:
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorexit ;
        return context;
_L2:
        context = com.tencent.wxop.stat.b.r.b(context);
        a = context;
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_55;
        }
        if (a.trim().length() != 0)
        {
            break MISSING_BLOCK_LABEL_70;
        }
        a = Integer.toString(at().nextInt(0x7fffffff));
        context = a;
        if (true) goto _L4; else goto _L3
_L3:
        context;
        throw context;
    }

    public static String d(long l1)
    {
        return (new SimpleDateFormat("yyyyMMdd")).format(new Date(l1));
    }

    public static String e(Context context, String s)
    {
        String s1 = s;
        if (com.tencent.wxop.stat.c.E())
        {
            if (aR == null)
            {
                aR = I(context);
            }
            s1 = s;
            if (aR != null)
            {
                s1 = (new StringBuilder()).append(s).append("_").append(aR).toString();
            }
        }
        return s1;
    }

    public static boolean e(String s)
    {
        return s != null && s.trim().length() != 0;
    }

    public static int r()
    {
        return at().nextInt(0x7fffffff);
    }

    public static String t(String s)
    {
        if (s == null)
        {
            return "0";
        }
        Object obj;
        int i;
        int j;
        try
        {
            obj = MessageDigest.getInstance("MD5");
            ((MessageDigest) (obj)).update(s.getBytes());
            s = ((MessageDigest) (obj)).digest();
            obj = new StringBuffer();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return "0";
        }
        i = 0;
        if (i >= s.length)
        {
            break; /* Loop/switch isn't completed */
        }
        j = s[i] & 0xff;
        if (j >= 16)
        {
            break MISSING_BLOCK_LABEL_66;
        }
        ((StringBuffer) (obj)).append("0");
        ((StringBuffer) (obj)).append(Integer.toHexString(j));
        i++;
        if (true) goto _L2; else goto _L1
_L2:
        break MISSING_BLOCK_LABEL_38;
_L1:
        s = ((StringBuffer) (obj)).toString();
        return s;
    }

    public static long u(String s)
    {
        return a(s, ".", Long.valueOf(0L)).longValue();
    }

    public static HttpHost v(Context context)
    {
        if (context == null)
        {
            return null;
        }
        if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0)
        {
            return null;
        }
        context = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (context == null)
        {
            return null;
        }
        if (context.getTypeName() != null && context.getTypeName().equalsIgnoreCase("WIFI"))
        {
            return null;
        }
        context = context.getExtraInfo();
        if (context == null)
        {
            return null;
        }
        if (!context.equals("cmwap") && !context.equals("3gwap") && !context.equals("uniwap")) goto _L2; else goto _L1
_L1:
        context = new HttpHost("10.0.0.172", 80);
        return context;
        context;
        cT.b(context);
_L4:
        return null;
_L2:
        if (context.equals("ctwap"))
        {
            return new HttpHost("10.0.0.200", 80);
        }
        context = Proxy.getDefaultHost();
        if (context == null) goto _L4; else goto _L3
_L3:
        if (context.trim().length() <= 0) goto _L4; else goto _L5
_L5:
        context = new HttpHost(context, Proxy.getDefaultPort());
        return context;
    }

    public static String w(Context context)
    {
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorenter ;
        if (c == null || c.trim().length() == 0)
        {
            c = com.tencent.wxop.stat.b.r.c(context);
        }
        context = c;
        com/tencent/wxop/stat/b/l;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public static DisplayMetrics x(Context context)
    {
        if (cS == null)
        {
            cS = new DisplayMetrics();
            ((WindowManager)context.getApplicationContext().getSystemService("window")).getDefaultDisplay().getMetrics(cS);
        }
        return cS;
    }

    public static boolean y(Context context)
    {
        if (!com.tencent.wxop.stat.b.r.a(context, "android.permission.ACCESS_WIFI_STATE")) goto _L2; else goto _L1
_L1:
        context = (ConnectivityManager)context.getApplicationContext().getSystemService("connectivity");
        if (context == null) goto _L4; else goto _L3
_L3:
        context = context.getAllNetworkInfo();
        if (context == null) goto _L4; else goto _L5
_L5:
        int i = 0;
_L6:
        if (i >= context.length)
        {
            break; /* Loop/switch isn't completed */
        }
        if (context[i].getTypeName().equalsIgnoreCase("WIFI") && context[i].isConnected())
        {
            return true;
        }
        i++;
        continue; /* Loop/switch isn't completed */
_L2:
        try
        {
            cT.warn("can not get the permission of android.permission.ACCESS_WIFI_STATE");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            cT.b(context);
        }
        return false;
        if (true) goto _L6; else goto _L4
_L4:
        return false;
    }

    public static String z(Context context)
    {
        if (b != null)
        {
            return b;
        }
        context = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
        if (context == null) goto _L2; else goto _L1
_L1:
        context = ((ApplicationInfo) (context)).metaData.getString("TA_APPKEY");
        if (context == null) goto _L4; else goto _L3
_L3:
        b = context;
        return context;
_L2:
        return null;
_L4:
        try
        {
            cT.b("Could not read APPKEY meta-data from AndroidManifest.xml");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            cT.b("Could not read APPKEY meta-data from AndroidManifest.xml");
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

}
