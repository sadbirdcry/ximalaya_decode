// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.b;


// Referenced classes of package com.tencent.wxop.stat.b:
//            j, k

public class h
{

    static final boolean cH;

    private h()
    {
    }

    public static byte[] d(byte abyte0[])
    {
        int i = abyte0.length;
        j j1 = new j(new byte[(i * 3) / 4]);
        if (!j1.a(abyte0, i))
        {
            throw new IllegalArgumentException("bad base-64");
        }
        if (j1.g == j1.cI.length)
        {
            return j1.cI;
        } else
        {
            abyte0 = new byte[j1.g];
            System.arraycopy(j1.cI, 0, abyte0, 0, j1.g);
            return abyte0;
        }
    }

    public static byte[] e(byte abyte0[])
    {
        k k1;
        int l;
        int i1;
        i1 = abyte0.length;
        k1 = new k();
        l = (i1 / 3) * 4;
        if (!k1.ba) goto _L2; else goto _L1
_L1:
        int i;
        i = l;
        if (i1 % 3 > 0)
        {
            i = l + 4;
        }
_L4:
        l = i;
        if (k1.bb)
        {
            l = i;
            if (i1 > 0)
            {
                int j1 = (i1 - 1) / 57;
                if (k1.cP)
                {
                    l = 2;
                } else
                {
                    l = 1;
                }
                l = i + l * (j1 + 1);
            }
        }
        k1.cI = new byte[l];
        k1.a(abyte0, i1);
        if (!cH && k1.g != l)
        {
            throw new AssertionError();
        } else
        {
            return k1.cI;
        }
_L2:
        i = l;
        switch (i1 % 3)
        {
        default:
            i = l;
            break;

        case 1: // '\001'
            i = l + 2;
            break;

        case 2: // '\002'
            i = l + 3;
            break;

        case 0: // '\0'
            break;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    static 
    {
        boolean flag;
        if (!com/tencent/wxop/stat/b/h.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        cH = flag;
    }
}
