// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.b;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.util.Collections;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.tencent.wxop.stat.b:
//            s, g, h

public final class r
{

    private static String a = "";

    private static WifiInfo T(Context context)
    {
        if (a(context, "android.permission.ACCESS_WIFI_STATE"))
        {
            context = (WifiManager)context.getApplicationContext().getSystemService("wifi");
            if (context != null)
            {
                return context.getConnectionInfo();
            }
        }
        return null;
    }

    public static String U(Context context)
    {
        context = T(context);
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_26;
        }
        context = context.getBSSID();
        return context;
        context;
        Log.e("MtaSDK", "encode error", context);
        return null;
    }

    public static String V(Context context)
    {
        context = T(context);
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_26;
        }
        context = context.getSSID();
        return context;
        context;
        Log.e("MtaSDK", "encode error", context);
        return null;
    }

    public static boolean W(Context context)
    {
        if (!a(context, "android.permission.INTERNET") || !a(context, "android.permission.ACCESS_NETWORK_STATE"))
        {
            break MISSING_BLOCK_LABEL_60;
        }
        context = (ConnectivityManager)context.getSystemService("connectivity");
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_68;
        }
        context = context.getActiveNetworkInfo();
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_50;
        }
        if (context.isAvailable())
        {
            return true;
        }
        Log.w("MtaSDK", "Network error");
        return false;
        try
        {
            Log.e("MtaSDK", "can not get the permisson of android.permission.INTERNET");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.e("MtaSDK", "isNetworkAvailable error", context);
        }
        return false;
    }

    public static JSONArray X(Context context)
    {
        if (!a(context, "android.permission.INTERNET") || !a(context, "android.permission.ACCESS_NETWORK_STATE")) goto _L2; else goto _L1
_L1:
        context = (WifiManager)context.getSystemService("wifi");
        if (context == null) goto _L4; else goto _L3
_L3:
        context = context.getScanResults();
        if (context == null) goto _L4; else goto _L5
_L5:
        if (context.size() <= 0) goto _L4; else goto _L6
_L6:
        JSONArray jsonarray;
        Collections.sort(context, new s());
        jsonarray = new JSONArray();
        int i = 0;
_L8:
        if (i >= context.size() || i >= 10)
        {
            break; /* Loop/switch isn't completed */
        }
        ScanResult scanresult = (ScanResult)context.get(i);
        JSONObject jsonobject = new JSONObject();
        jsonobject.put("bs", scanresult.BSSID);
        jsonobject.put("ss", scanresult.SSID);
        jsonarray.put(jsonobject);
        i++;
        if (true) goto _L8; else goto _L7
_L2:
        try
        {
            Log.e("MtaSDK", "can not get the permisson of android.permission.INTERNET");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.e("MtaSDK", "isWifiNet error", context);
        }
_L4:
        return null;
_L7:
        return jsonarray;
    }

    public static void a(JSONObject jsonobject, String s1, String s2)
    {
        if (s2 == null)
        {
            break MISSING_BLOCK_LABEL_18;
        }
        if (s2.length() > 0)
        {
            jsonobject.put(s1, s2);
        }
        return;
        jsonobject;
        Log.e("MtaSDK", "jsonPut error", jsonobject);
        return;
    }

    public static boolean a(Context context, String s1)
    {
        boolean flag = false;
        int i;
        try
        {
            i = context.getPackageManager().checkPermission(s1, context.getPackageName());
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.e("MtaSDK", "checkPermission error", context);
            return false;
        }
        if (i == 0)
        {
            flag = true;
        }
        return flag;
    }

    public static String b(Context context)
    {
        if (!a(context, "android.permission.READ_PHONE_STATE"))
        {
            break MISSING_BLOCK_LABEL_28;
        }
        context = ((TelephonyManager)context.getSystemService("phone")).getDeviceId();
        if (context != null)
        {
            return context;
        }
        break MISSING_BLOCK_LABEL_36;
        try
        {
            Log.e("MtaSDK", "Could not get permission of android.permission.READ_PHONE_STATE");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.e("MtaSDK", "get device id error", context);
        }
        return null;
    }

    public static String c(Context context)
    {
        if (!a(context, "android.permission.ACCESS_WIFI_STATE"))
        {
            break MISSING_BLOCK_LABEL_49;
        }
        try
        {
            context = (WifiManager)context.getSystemService("wifi");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.e("MtaSDK", "get wifi address error", context);
            return "";
        }
        if (context == null)
        {
            return "";
        }
        context = context.getConnectionInfo().getMacAddress();
        return context;
        Log.e("MtaSDK", "Could not get permission of android.permission.ACCESS_WIFI_STATE");
        return "";
    }

    public static String q(String s1)
    {
        String s2;
        if (s1 == null)
        {
            s2 = null;
        } else
        {
            s2 = s1;
            if (android.os.Build.VERSION.SDK_INT >= 8)
            {
                String s3;
                try
                {
                    s3 = new String(h.e(g.b(s1.getBytes("UTF-8"))), "UTF-8");
                }
                catch (Throwable throwable)
                {
                    Log.e("MtaSDK", "encode error", throwable);
                    return s1;
                }
                return s3;
            }
        }
        return s2;
    }

    public static String t(String s1)
    {
        String s2;
        if (s1 == null)
        {
            s2 = null;
        } else
        {
            s2 = s1;
            if (android.os.Build.VERSION.SDK_INT >= 8)
            {
                String s3;
                try
                {
                    s3 = new String(g.c(h.d(s1.getBytes("UTF-8"))), "UTF-8");
                }
                catch (Throwable throwable)
                {
                    Log.e("MtaSDK", "decode error", throwable);
                    return s1;
                }
                return s3;
            }
        }
        return s2;
    }

}
