// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

// Referenced classes of package com.tencent.wxop.stat.b:
//            l

public final class q
{

    private static SharedPreferences db = null;

    private static SharedPreferences S(Context context)
    {
        com/tencent/wxop/stat/b/q;
        JVM INSTR monitorenter ;
        SharedPreferences sharedpreferences;
        sharedpreferences = context.getSharedPreferences(".mta-wxop", 0);
        db = sharedpreferences;
        if (sharedpreferences != null)
        {
            break MISSING_BLOCK_LABEL_26;
        }
        db = PreferenceManager.getDefaultSharedPreferences(context);
        context = db;
        com/tencent/wxop/stat/b/q;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public static int a(Context context, String s, int i)
    {
        s = l.e(context, (new StringBuilder("wxop_")).append(s).toString());
        return S(context).getInt(s, i);
    }

    public static void a(Context context, String s, long l1)
    {
        s = l.e(context, (new StringBuilder("wxop_")).append(s).toString());
        context = S(context).edit();
        context.putLong(s, l1);
        context.commit();
    }

    public static String b(Context context, String s, String s1)
    {
        s = l.e(context, (new StringBuilder("wxop_")).append(s).toString());
        return S(context).getString(s, s1);
    }

    public static void b(Context context, String s, int i)
    {
        s = l.e(context, (new StringBuilder("wxop_")).append(s).toString());
        context = S(context).edit();
        context.putInt(s, i);
        context.commit();
    }

    public static void c(Context context, String s, String s1)
    {
        s = l.e(context, (new StringBuilder("wxop_")).append(s).toString());
        context = S(context).edit();
        context.putString(s, s1);
        context.commit();
    }

    public static long f(Context context, String s)
    {
        s = l.e(context, (new StringBuilder("wxop_")).append(s).toString());
        return S(context).getLong(s, 0L);
    }

}
