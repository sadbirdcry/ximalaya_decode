// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.b;

import android.util.Log;
import com.tencent.wxop.stat.c;

public final class b
{

    private String a;
    private boolean ch;
    private int cp;

    public b()
    {
        a = "default";
        ch = true;
        cp = 2;
    }

    public b(String s)
    {
        a = "default";
        ch = true;
        cp = 2;
        a = s;
    }

    private String c()
    {
        StackTraceElement astacktraceelement[] = Thread.currentThread().getStackTrace();
        if (astacktraceelement != null)
        {
            int j = astacktraceelement.length;
            int i = 0;
            while (i < j) 
            {
                StackTraceElement stacktraceelement = astacktraceelement[i];
                if (!stacktraceelement.isNativeMethod() && !stacktraceelement.getClassName().equals(java/lang/Thread.getName()) && !stacktraceelement.getClassName().equals(getClass().getName()))
                {
                    return (new StringBuilder("[")).append(Thread.currentThread().getName()).append("(").append(Thread.currentThread().getId()).append("): ").append(stacktraceelement.getFileName()).append(":").append(stacktraceelement.getLineNumber()).append("]").toString();
                }
                i++;
            }
        }
        return null;
    }

    public final void a(Throwable throwable)
    {
        if (cp <= 6)
        {
            Log.e(a, "", throwable);
            com.tencent.wxop.stat.c.F();
        }
    }

    public final void ap()
    {
        ch = false;
    }

    public final void b(Object obj)
    {
        if (ch && cp <= 4)
        {
            String s = c();
            if (s == null)
            {
                obj = obj.toString();
            } else
            {
                obj = (new StringBuilder()).append(s).append(" - ").append(obj).toString();
            }
            Log.i(a, ((String) (obj)));
            com.tencent.wxop.stat.c.F();
        }
    }

    public final void b(Throwable throwable)
    {
        if (ch)
        {
            a(throwable);
        }
    }

    public final void c(Object obj)
    {
        if (ch)
        {
            warn(obj);
        }
    }

    public final void d(Object obj)
    {
        if (ch)
        {
            error(obj);
        }
    }

    public final void debug(Object obj)
    {
        if (cp <= 3)
        {
            String s = c();
            if (s == null)
            {
                obj = obj.toString();
            } else
            {
                obj = (new StringBuilder()).append(s).append(" - ").append(obj).toString();
            }
            Log.d(a, ((String) (obj)));
            com.tencent.wxop.stat.c.F();
        }
    }

    public final void e(Object obj)
    {
        if (ch)
        {
            debug(obj);
        }
    }

    public final void error(Object obj)
    {
        if (cp <= 6)
        {
            String s = c();
            if (s == null)
            {
                obj = obj.toString();
            } else
            {
                obj = (new StringBuilder()).append(s).append(" - ").append(obj).toString();
            }
            Log.e(a, ((String) (obj)));
            com.tencent.wxop.stat.c.F();
        }
    }

    public final void warn(Object obj)
    {
        if (cp <= 5)
        {
            String s = c();
            if (s == null)
            {
                obj = obj.toString();
            } else
            {
                obj = (new StringBuilder()).append(s).append(" - ").append(obj).toString();
            }
            Log.w(a, ((String) (obj)));
            com.tencent.wxop.stat.c.F();
        }
    }
}
