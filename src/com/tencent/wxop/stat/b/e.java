// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.b;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import com.tencent.wxop.stat.c;
import com.tencent.wxop.stat.g;
import com.tencent.wxop.stat.t;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.tencent.wxop.stat.b:
//            l, r, c

final class e
{

    int L;
    String M;
    String a;
    int aQ;
    String aR;
    String aS;
    String ab;
    String al;
    String b;
    String bq;
    String br;
    String bs;
    String bt;
    DisplayMetrics cA;
    Context cB;
    private String cC;
    private String cD;
    private String cE;
    private String cF;

    private e(Context context)
    {
        b = "2.0.3";
        L = android.os.Build.VERSION.SDK_INT;
        M = Build.MODEL;
        ab = Build.MANUFACTURER;
        bq = Locale.getDefault().getLanguage();
        aQ = 0;
        aR = null;
        aS = null;
        cB = null;
        cC = null;
        cD = null;
        cE = null;
        cF = null;
        cB = context.getApplicationContext();
        cA = l.x(cB);
        a = l.D(cB);
        br = c.e(cB);
        bs = l.C(cB);
        bt = TimeZone.getDefault().getID();
        context = cB;
        aQ = l.au();
        al = l.H(cB);
        aR = cB.getPackageName();
        if (L >= 14)
        {
            cC = l.M(cB);
        }
        context = cB;
        cD = l.az().toString();
        cE = l.L(cB);
        cF = l.ax();
        aS = l.R(cB);
    }

    e(Context context, byte byte0)
    {
        this(context);
    }

    final void a(JSONObject jsonobject, Thread thread)
    {
        if (thread == null)
        {
            if (cA != null)
            {
                jsonobject.put("sr", (new StringBuilder()).append(cA.widthPixels).append("*").append(cA.heightPixels).toString());
                jsonobject.put("dpi", (new StringBuilder()).append(cA.xdpi).append("*").append(cA.ydpi).toString());
            }
            if (g.r(cB).W())
            {
                thread = new JSONObject();
                r.a(thread, "bs", r.U(cB));
                r.a(thread, "ss", r.V(cB));
                if (thread.length() > 0)
                {
                    r.a(jsonobject, "wf", thread.toString());
                }
            }
            thread = r.X(cB);
            if (thread != null && thread.length() > 0)
            {
                r.a(jsonobject, "wflist", thread.toString());
            }
            r.a(jsonobject, "sen", cC);
        } else
        {
            r.a(jsonobject, "thn", thread.getName());
            r.a(jsonobject, "qq", c.f(cB));
            r.a(jsonobject, "cui", c.g(cB));
            if (l.e(cE) && cE.split("/").length == 2)
            {
                r.a(jsonobject, "fram", cE.split("/")[0]);
            }
            if (l.e(cF) && cF.split("/").length == 2)
            {
                r.a(jsonobject, "from", cF.split("/")[0]);
            }
            if (t.s(cB).t(cB) != null)
            {
                jsonobject.put("ui", t.s(cB).t(cB).b());
            }
            r.a(jsonobject, "mid", c.h(cB));
        }
        r.a(jsonobject, "pcn", l.I(cB));
        r.a(jsonobject, "osn", android.os.Build.VERSION.RELEASE);
        r.a(jsonobject, "av", a);
        r.a(jsonobject, "ch", br);
        r.a(jsonobject, "mf", ab);
        r.a(jsonobject, "sv", b);
        r.a(jsonobject, "osd", Build.DISPLAY);
        r.a(jsonobject, "prod", Build.PRODUCT);
        r.a(jsonobject, "tags", Build.TAGS);
        r.a(jsonobject, "id", Build.ID);
        r.a(jsonobject, "fng", Build.FINGERPRINT);
        r.a(jsonobject, "lch", aS);
        r.a(jsonobject, "ov", Integer.toString(L));
        jsonobject.put("os", 1);
        r.a(jsonobject, "op", bs);
        r.a(jsonobject, "lg", bq);
        r.a(jsonobject, "md", M);
        r.a(jsonobject, "tz", bt);
        if (aQ != 0)
        {
            jsonobject.put("jb", aQ);
        }
        r.a(jsonobject, "sd", al);
        r.a(jsonobject, "apn", aR);
        r.a(jsonobject, "cpu", cD);
        r.a(jsonobject, "abi", Build.CPU_ABI);
        r.a(jsonobject, "abi2", Build.CPU_ABI2);
        r.a(jsonobject, "ram", cE);
        r.a(jsonobject, "rom", cF);
    }
}
