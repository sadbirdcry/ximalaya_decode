// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.b;

import android.net.wifi.ScanResult;
import java.util.Comparator;

final class s
    implements Comparator
{

    s()
    {
    }

    public final int compare(Object obj, Object obj1)
    {
        obj = (ScanResult)obj;
        obj1 = (ScanResult)obj1;
        int i = Math.abs(((ScanResult) (obj)).level);
        int j = Math.abs(((ScanResult) (obj1)).level);
        if (i > j)
        {
            return 1;
        }
        return i != j ? -1 : 0;
    }
}
