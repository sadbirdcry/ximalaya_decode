// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.b.b;
import java.util.Map;

// Referenced classes of package com.tencent.wxop.stat:
//            e, c, f

final class as
    implements Runnable
{

    final String a;
    final f bM;
    final Context co;

    as(String s, Context context, f f)
    {
        a = s;
        co = context;
        bM = f;
        super();
    }

    public final void run()
    {
label0:
        {
            synchronized (e.M())
            {
                if (e.M().size() < c.v())
                {
                    break label0;
                }
                e.K().error((new StringBuilder("The number of page events exceeds the maximum value ")).append(Integer.toString(c.v())).toString());
            }
            return;
        }
        e.q(a);
        if (!e.M().containsKey(e.N()))
        {
            break MISSING_BLOCK_LABEL_126;
        }
        e.K().d((new StringBuilder("Duplicate PageID : ")).append(e.N()).append(", onResume() repeated?").toString());
        obj;
        JVM INSTR monitorexit ;
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        obj;
        e.K().b(((Throwable) (obj)));
        e.a(co, ((Throwable) (obj)));
        return;
        e.M().put(e.N(), Long.valueOf(System.currentTimeMillis()));
        obj;
        JVM INSTR monitorexit ;
        e.a(co, true, bM);
        return;
    }
}
