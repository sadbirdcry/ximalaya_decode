// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import android.content.Context;
import com.tencent.wxop.stat.a.i;
import com.tencent.wxop.stat.b.b;
import com.tencent.wxop.stat.b.c;
import com.tencent.wxop.stat.b.f;
import com.tencent.wxop.stat.b.l;
import com.tencent.wxop.stat.b.q;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.wxop.stat:
//            c, ah, t, f, 
//            i, p, as, ap, 
//            m, k, ar, an, 
//            l, ao, ak, aq, 
//            g, h

public class e
{

    private static volatile boolean S = true;
    static volatile int aI = 0;
    private static f aK;
    private static volatile Map aL = new ConcurrentHashMap();
    private static volatile Map aM = new ConcurrentHashMap();
    private static volatile Map aN = new ConcurrentHashMap(10);
    private static volatile long aO = 0L;
    private static volatile long aP = 0L;
    private static volatile int aQ = 0;
    private static volatile String aR = "";
    private static volatile String aS = "";
    private static Map aT = new ConcurrentHashMap();
    private static Map aU = new ConcurrentHashMap();
    private static b aV = com.tencent.wxop.stat.b.l.av();
    private static Thread.UncaughtExceptionHandler aW = null;
    static volatile long aX = 0L;
    private static Context aY = null;
    static volatile long aZ = 0L;
    private static volatile long af = 0L;
    private static String al = "";

    public e()
    {
    }

    private static JSONObject G()
    {
        JSONObject jsonobject = new JSONObject();
        try
        {
            JSONObject jsonobject1 = new JSONObject();
            if (c.P.L != 0)
            {
                jsonobject1.put("v", c.P.L);
            }
            jsonobject.put(Integer.toString(c.P.aI), jsonobject1);
            jsonobject1 = new JSONObject();
            if (c.O.L != 0)
            {
                jsonobject1.put("v", c.O.L);
            }
            jsonobject.put(Integer.toString(c.O.aI), jsonobject1);
        }
        catch (JSONException jsonexception)
        {
            aV.b(jsonexception);
            return jsonobject;
        }
        return jsonobject;
    }

    static void H()
    {
        aI = 0;
        aX = 0L;
    }

    static void I()
    {
        aI++;
        aX = System.currentTimeMillis();
        p(aY);
    }

    static Context J()
    {
        return aY;
    }

    static b K()
    {
        return aV;
    }

    static Thread.UncaughtExceptionHandler L()
    {
        return aW;
    }

    static Map M()
    {
        return aT;
    }

    static String N()
    {
        return aR;
    }

    static String O()
    {
        return aS;
    }

    static long P()
    {
        return aO;
    }

    static Map Q()
    {
        return aN;
    }

    static int a(Context context, boolean flag, com.tencent.wxop.stat.f f1)
    {
        boolean flag2 = true;
        long l1 = System.currentTimeMillis();
        String s;
        boolean flag1;
        if (flag && l1 - af >= (long)com.tencent.wxop.stat.c.m())
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        af = l1;
        if (aP == 0L)
        {
            aP = com.tencent.wxop.stat.b.l.ad();
        }
        if (l1 >= aP)
        {
            aP = com.tencent.wxop.stat.b.l.ad();
            if (t.s(context).t(context).as() != 1)
            {
                t.s(context).t(context).z();
            }
            com.tencent.wxop.stat.c.C();
            aI = 0;
            al = com.tencent.wxop.stat.b.l.aw();
            flag1 = true;
        }
        s = al;
        if (com.tencent.wxop.stat.b.l.a(f1))
        {
            s = (new StringBuilder()).append(f1.S()).append(al).toString();
        }
        if (!aU.containsKey(s))
        {
            flag1 = flag2;
        }
        if (flag1)
        {
            if (!com.tencent.wxop.stat.b.l.a(f1))
            {
                if (com.tencent.wxop.stat.c.D() < com.tencent.wxop.stat.c.A())
                {
                    com.tencent.wxop.stat.b.l.O(context);
                    a(context, ((com.tencent.wxop.stat.f) (null)));
                } else
                {
                    aV.d("Exceed StatConfig.getMaxDaySessionNumbers().");
                }
            } else
            {
                a(context, f1);
            }
            aU.put(s, Long.valueOf(1L));
        }
        if (!S) goto _L2; else goto _L1
_L1:
        if (!com.tencent.wxop.stat.c.l()) goto _L4; else goto _L3
_L3:
        context = i(context);
        if (context != null) goto _L6; else goto _L5
_L5:
        aV.error("The Context of StatService.testSpeed() can not be null!");
_L4:
        S = false;
_L2:
        return aQ;
_L6:
        if (k(context) != null)
        {
            aK.a(new com.tencent.wxop.stat.i(context));
        }
        if (true) goto _L4; else goto _L7
_L7:
    }

    static Thread.UncaughtExceptionHandler a(Thread.UncaughtExceptionHandler uncaughtexceptionhandler)
    {
        aW = uncaughtexceptionhandler;
        return uncaughtexceptionhandler;
    }

    private static void a(Context context, com.tencent.wxop.stat.f f1)
    {
        if (k(context) != null)
        {
            if (com.tencent.wxop.stat.c.k())
            {
                aV.e("start new session.");
            }
            if (f1 == null || aQ == 0)
            {
                aQ = com.tencent.wxop.stat.b.l.r();
            }
            com.tencent.wxop.stat.c.z();
            com.tencent.wxop.stat.c.B();
            (new p(new i(context, aQ, G(), f1))).ah();
        }
    }

    public static void a(Context context, String s, com.tencent.wxop.stat.f f1)
    {
        if (com.tencent.wxop.stat.c.l())
        {
            context = i(context);
            if (context == null || s == null || s.length() == 0)
            {
                aV.error("The Context or pageName of StatService.trackBeginPage() can not be null or empty!");
                return;
            }
            s = new String(s);
            if (k(context) != null)
            {
                aK.a(new as(s, context, f1));
                return;
            }
        }
    }

    static void a(Context context, Throwable throwable)
    {
        if (com.tencent.wxop.stat.c.l())
        {
            context = i(context);
            if (context == null)
            {
                aV.error("The Context of StatService.reportSdkSelfException() can not be null!");
                return;
            }
            if (k(context) != null)
            {
                aK.a(new ap(context, throwable));
                return;
            }
        }
    }

    static boolean a()
    {
        if (aI >= 2)
        {
            aX = System.currentTimeMillis();
            return true;
        } else
        {
            return false;
        }
    }

    public static boolean a(Context context, String s, String s1)
    {
        if (com.tencent.wxop.stat.c.l())
        {
            break MISSING_BLOCK_LABEL_17;
        }
        aV.error("MTA StatService is disable.");
        return false;
        try
        {
            if (com.tencent.wxop.stat.c.k())
            {
                aV.e((new StringBuilder("MTA SDK version, current: ")).append("2.0.3").append(" ,required: ").append(s1).toString());
            }
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            aV.b(context);
            return false;
        }
          goto _L1
_L2:
        aV.error("Context or mtaSdkVersion in StatService.startStatService() is null, please check it!");
        com.tencent.wxop.stat.c.a(false);
        return false;
_L3:
        if (com.tencent.wxop.stat.b.l.u("2.0.3") >= com.tencent.wxop.stat.b.l.u(s1))
        {
            break MISSING_BLOCK_LABEL_164;
        }
        context = (new StringBuilder("MTA SDK version conflicted, current: ")).append("2.0.3").append(",required: ").append(s1).toString();
        context = (new StringBuilder()).append(context).append(". please delete the current SDK and download the latest one. official website: http://mta.qq.com/ or http://mta.oa.com/").toString();
        aV.error(context);
        com.tencent.wxop.stat.c.a(false);
        return false;
        s1 = com.tencent.wxop.stat.c.e(context);
        if (s1 == null)
        {
            break MISSING_BLOCK_LABEL_180;
        }
        if (s1.length() != 0)
        {
            break MISSING_BLOCK_LABEL_186;
        }
        com.tencent.wxop.stat.c.n("-");
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_195;
        }
        com.tencent.wxop.stat.c.b(context, s);
        if (k(context) != null)
        {
            aK.a(new m(context));
        }
        return true;
_L1:
        if (context != null && s1 != null) goto _L3; else goto _L2
    }

    public static void b(Context context, String s, com.tencent.wxop.stat.f f1)
    {
        if (com.tencent.wxop.stat.c.l())
        {
            context = i(context);
            if (context == null || s == null || s.length() == 0)
            {
                aV.error("The Context or pageName of StatService.trackEndPage() can not be null or empty!");
                return;
            }
            s = new String(s);
            if (k(context) != null)
            {
                aK.a(new k(context, s, f1));
                return;
            }
        }
    }

    static long c(long l1)
    {
        aO = l1;
        return l1;
    }

    public static void d(Context context, String s)
    {
        if (com.tencent.wxop.stat.c.l())
        {
            context = i(context);
            if (context == null)
            {
                aV.error("The Context of StatService.trackCustomEvent() can not be null!");
                return;
            }
            boolean flag;
            if (s == null || s.length() == 0)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            if (flag)
            {
                aV.error("The event_id of StatService.trackCustomEvent() can not be null or empty.");
                return;
            }
            s = new com.tencent.wxop.stat.a.b(s);
            if (k(context) != null)
            {
                aK.a(new ar(context, s));
                return;
            }
        }
    }

    private static Context i(Context context)
    {
        if (context != null)
        {
            return context;
        } else
        {
            return aY;
        }
    }

    private static void j(Context context)
    {
        boolean flag1 = false;
        com/tencent/wxop/stat/e;
        JVM INSTR monitorenter ;
        if (context != null) goto _L2; else goto _L1
_L1:
        com/tencent/wxop/stat/e;
        JVM INSTR monitorexit ;
        return;
_L2:
        long l1;
        long l3;
        if (aK != null)
        {
            continue; /* Loop/switch isn't completed */
        }
        l1 = com.tencent.wxop.stat.b.q.f(context, com.tencent.wxop.stat.c.c);
        l3 = com.tencent.wxop.stat.b.l.u("2.0.3");
        boolean flag;
        flag = true;
        if (l3 > l1)
        {
            break MISSING_BLOCK_LABEL_80;
        }
        aV.error((new StringBuilder("MTA is disable for current version:")).append(l3).append(",wakeup version:").append(l1).toString());
        flag = false;
        long l2 = com.tencent.wxop.stat.b.q.f(context, c.W);
        if (l2 <= System.currentTimeMillis())
        {
            break MISSING_BLOCK_LABEL_135;
        }
        aV.error((new StringBuilder("MTA is disable for current time:")).append(System.currentTimeMillis()).append(",wakeup time:").append(l2).toString());
        flag = flag1;
        com.tencent.wxop.stat.c.a(flag);
        if (!flag)
        {
            continue; /* Loop/switch isn't completed */
        }
        context = context.getApplicationContext();
        aY = context;
        aK = new f();
        al = com.tencent.wxop.stat.b.l.aw();
        aO = System.currentTimeMillis() + c.af;
        aK.a(new an(context));
        if (true) goto _L1; else goto _L3
_L3:
        context;
        throw context;
    }

    private static f k(Context context)
    {
        if (aK != null) goto _L2; else goto _L1
_L1:
        com/tencent/wxop/stat/e;
        JVM INSTR monitorenter ;
        f f1 = aK;
        if (f1 != null)
        {
            break MISSING_BLOCK_LABEL_21;
        }
        j(context);
_L3:
        com/tencent/wxop/stat/e;
        JVM INSTR monitorexit ;
_L2:
        return aK;
        context;
        aV.a(context);
        com.tencent.wxop.stat.c.a(false);
          goto _L3
        context;
        throw context;
    }

    public static void l(Context context)
    {
        while (!com.tencent.wxop.stat.c.l() || k(context) == null) 
        {
            return;
        }
        aK.a(new com.tencent.wxop.stat.l(context));
    }

    public static void m(Context context)
    {
        while (!com.tencent.wxop.stat.c.l() || k(context) == null) 
        {
            return;
        }
        aK.a(new ao(context));
    }

    static void n(Context context)
    {
        if (!com.tencent.wxop.stat.c.l())
        {
            return;
        }
        context = i(context);
        if (context == null)
        {
            aV.error("The Context of StatService.sendNetworkDetector() can not be null!");
            return;
        }
        try
        {
            com.tencent.wxop.stat.a.f f1 = new com.tencent.wxop.stat.a.f(context);
            ak.Z(context).a(f1, new aq());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            aV.b(context);
        }
    }

    public static void o(Context context)
    {
        if (com.tencent.wxop.stat.c.l())
        {
            if (com.tencent.wxop.stat.c.k())
            {
                aV.b("commitEvents, maxNumber=-1");
            }
            context = i(context);
            if (context == null)
            {
                aV.error("The Context of StatService.commitEvents() can not be null!");
                return;
            }
            if (g.r(aY).X() && k(context) != null)
            {
                aK.a(new h(context));
                return;
            }
        }
    }

    public static Properties p(String s)
    {
        return (Properties)aM.get(s);
    }

    public static void p(Context context)
    {
        while (!com.tencent.wxop.stat.c.l() || c.ay <= 0) 
        {
            return;
        }
        context = i(context);
        if (context == null)
        {
            aV.error("The Context of StatService.testSpeed() can not be null!");
            return;
        } else
        {
            t.s(context).H();
            return;
        }
    }

    static String q(String s)
    {
        aR = s;
        return s;
    }

    static void q(Context context)
    {
        aZ = System.currentTimeMillis() + (long)(60000 * com.tencent.wxop.stat.c.u());
        com.tencent.wxop.stat.b.q.a(context, "last_period_ts", aZ);
        o(context);
    }

    static String r(String s)
    {
        aS = s;
        return s;
    }

}
