// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import android.content.Context;
import android.content.IntentFilter;
import com.tencent.wxop.stat.b.b;
import com.tencent.wxop.stat.b.f;
import com.tencent.wxop.stat.b.l;
import com.tencent.wxop.stat.b.r;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.HttpHost;
import org.json.JSONObject;

// Referenced classes of package com.tencent.wxop.stat:
//            ak, c, e, z

public class g
{

    private static g bg = null;
    private List bc;
    private volatile HttpHost bd;
    private f be;
    private int bf;
    private Context bh;
    private b bi;
    private volatile String c;
    private volatile int g;

    private g(Context context)
    {
        bc = null;
        g = 2;
        c = "";
        bd = null;
        be = null;
        bf = 0;
        bh = null;
        bi = null;
        bh = context.getApplicationContext();
        be = new f();
        ak.j(context);
        bi = l.av();
        Y();
        bc = new ArrayList(10);
        bc.add("117.135.169.101");
        bc.add("140.207.54.125");
        bc.add("180.153.8.53");
        bc.add("120.198.203.175");
        bc.add("14.17.43.18");
        bc.add("163.177.71.186");
        bc.add("111.30.131.31");
        bc.add("123.126.121.167");
        bc.add("123.151.152.111");
        bc.add("113.142.45.79");
        bc.add("123.138.162.90");
        bc.add("103.7.30.94");
        Z();
    }

    private String O()
    {
        String s;
        if (d("pingma.qq.com"))
        {
            break MISSING_BLOCK_LABEL_28;
        }
        s = InetAddress.getByName("pingma.qq.com").getHostAddress();
        return s;
        Exception exception;
        exception;
        bi.b(exception);
        return "";
    }

    private void Y()
    {
        g = 0;
        bd = null;
        c = null;
    }

    static f a(g g1)
    {
        return g1.be;
    }

    private static boolean d(String s)
    {
        return Pattern.compile("(2[5][0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})\\.(25[0-5]|2[0-4]\\d|1\\d{2}|\\d{1,2})").matcher(s).matches();
    }

    public static g r(Context context)
    {
        if (bg != null) goto _L2; else goto _L1
_L1:
        com/tencent/wxop/stat/g;
        JVM INSTR monitorenter ;
        if (bg == null)
        {
            bg = new g(context);
        }
        com/tencent/wxop/stat/g;
        JVM INSTR monitorexit ;
_L2:
        return bg;
        context;
        throw context;
    }

    public final int D()
    {
        return g;
    }

    public final void I()
    {
        bf = (bf + 1) % bc.size();
    }

    public final HttpHost V()
    {
        return bd;
    }

    public final boolean W()
    {
        return g == 1;
    }

    public final boolean X()
    {
        return g != 0;
    }

    final void Z()
    {
        if (!com.tencent.wxop.stat.b.r.W(bh)) goto _L2; else goto _L1
_L1:
        if (!c.ad) goto _L4; else goto _L3
_L3:
        String s1;
        s1 = O();
        if (com.tencent.wxop.stat.c.k())
        {
            bi.b((new StringBuilder("remoteIp ip is ")).append(s1).toString());
        }
        if (!l.e(s1)) goto _L4; else goto _L5
_L5:
        if (!bc.contains(s1)) goto _L7; else goto _L6
_L6:
        String s = s1;
_L8:
        com.tencent.wxop.stat.c.o((new StringBuilder("http://")).append(s).append(":80/mstat/report").toString());
_L4:
        c = l.E(bh);
        if (com.tencent.wxop.stat.c.k())
        {
            bi.b((new StringBuilder("NETWORK name:")).append(c).toString());
        }
        if (l.e(c))
        {
            String s2;
            if ("WIFI".equalsIgnoreCase(c))
            {
                g = 1;
            } else
            {
                g = 2;
            }
            bd = l.v(bh);
        }
        if (e.a())
        {
            e.n(bh);
        }
        return;
_L7:
        s2 = (String)bc.get(bf);
        s = s2;
        if (com.tencent.wxop.stat.c.k())
        {
            bi.c((new StringBuilder()).append(s1).append(" not in ip list, change to:").append(s2).toString());
            s = s2;
        }
        if (true) goto _L8; else goto _L2
_L2:
        if (com.tencent.wxop.stat.c.k())
        {
            bi.b("NETWORK TYPE: network is close.");
        }
        Y();
        return;
    }

    public final void aa()
    {
        bh.getApplicationContext().registerReceiver(new z(this), new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
    }

    public final String b()
    {
        return c;
    }

    public final void b(String s)
    {
        if (com.tencent.wxop.stat.c.k())
        {
            bi.b((new StringBuilder("updateIpList ")).append(s).toString());
        }
        Iterator iterator;
        if (!l.e(s))
        {
            break MISSING_BLOCK_LABEL_230;
        }
        s = new JSONObject(s);
        if (s.length() <= 0)
        {
            break MISSING_BLOCK_LABEL_230;
        }
        iterator = s.keys();
_L4:
        String as[];
        int j;
        String s1;
        do
        {
            if (!iterator.hasNext())
            {
                break MISSING_BLOCK_LABEL_230;
            }
            s1 = s.getString((String)iterator.next());
        } while (!l.e(s1));
        as = s1.split(";");
        j = as.length;
        int i = 0;
_L2:
        String s2;
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        s2 = as[i];
        String as1[];
        if (!l.e(s2))
        {
            break MISSING_BLOCK_LABEL_212;
        }
        as1 = s2.split(":");
        if (as1.length <= 1)
        {
            break MISSING_BLOCK_LABEL_212;
        }
        as1 = as1[0];
        if (d(as1) && !bc.contains(as1))
        {
            if (com.tencent.wxop.stat.c.k())
            {
                bi.b((new StringBuilder("add new ip:")).append(as1).toString());
            }
            bc.add(as1);
        }
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        if (true) goto _L4; else goto _L3
_L3:
        s;
        bi.b(s);
        bf = (new Random()).nextInt(bc.size());
        return;
    }

}
