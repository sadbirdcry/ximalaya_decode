// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import com.tencent.wxop.stat.a.c;
import com.tencent.wxop.stat.b.b;

// Referenced classes of package com.tencent.wxop.stat:
//            c, e, t

final class n
    implements Thread.UncaughtExceptionHandler
{

    n()
    {
    }

    public final void uncaughtException(Thread thread, Throwable throwable)
    {
        if (c.l() && e.J() != null)
        {
            if (c.x())
            {
                t.s(e.J()).b(new c(e.J(), e.a(e.J(), false, null), throwable, thread), null, false, true);
                e.K().debug("MTA has caught the following uncaught exception:");
                e.K().a(throwable);
            }
            e.p(e.J());
            if (e.L() != null)
            {
                e.K().e("Call the original uncaught exception handler.");
                if (!(e.L() instanceof n))
                {
                    e.L().uncaughtException(thread, throwable);
                    return;
                }
            }
        }
    }
}
