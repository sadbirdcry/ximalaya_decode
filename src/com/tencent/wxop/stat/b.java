// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat;

import org.json.JSONException;
import org.json.JSONObject;

public final class b
{

    private long K;
    private int L;
    private String M;
    private String c;
    private int g;

    public b()
    {
        K = 0L;
        g = 0;
        c = "";
        L = 0;
        M = "";
    }

    public final void a(long l)
    {
        K = l;
    }

    public final JSONObject i()
    {
        JSONObject jsonobject = new JSONObject();
        try
        {
            jsonobject.put("tm", K);
            jsonobject.put("st", g);
            if (c != null)
            {
                jsonobject.put("dm", c);
            }
            jsonobject.put("pt", L);
            if (M != null)
            {
                jsonobject.put("rip", M);
            }
            jsonobject.put("ts", System.currentTimeMillis() / 1000L);
        }
        catch (JSONException jsonexception)
        {
            return jsonobject;
        }
        return jsonobject;
    }

    public final void k(String s)
    {
        M = s;
    }

    public final void setDomain(String s)
    {
        c = s;
    }

    public final void setPort(int j)
    {
        L = j;
    }

    public final void setStatusCode(int j)
    {
        g = j;
    }
}
