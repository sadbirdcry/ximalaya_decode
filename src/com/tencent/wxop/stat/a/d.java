// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.a;

import android.content.Context;
import com.tencent.a.a.a.a.h;
import com.tencent.wxop.stat.b.l;
import com.tencent.wxop.stat.b.r;
import com.tencent.wxop.stat.c;
import com.tencent.wxop.stat.f;
import com.tencent.wxop.stat.t;
import org.json.JSONObject;

// Referenced classes of package com.tencent.wxop.stat.a:
//            e

public abstract class d
{

    protected static String bt = null;
    protected int L;
    protected long aZ;
    protected String b;
    protected int bf;
    protected com.tencent.wxop.stat.b.c bp;
    protected String bq;
    protected String br;
    protected String bs;
    protected boolean bu;
    protected Context bv;
    private f bw;

    d(Context context, int i, f f1)
    {
        b = null;
        bp = null;
        bq = null;
        br = null;
        bs = null;
        bu = false;
        bw = null;
        bv = context;
        aZ = System.currentTimeMillis() / 1000L;
        L = i;
        br = com.tencent.wxop.stat.c.e(context);
        bs = l.D(context);
        b = com.tencent.wxop.stat.c.d(context);
        if (f1 != null)
        {
            bw = f1;
            if (l.e(f1.S()))
            {
                b = f1.S();
            }
            if (l.e(f1.T()))
            {
                br = f1.T();
            }
            if (l.e(f1.getVersion()))
            {
                bs = f1.getVersion();
            }
            bu = f1.U();
        }
        bq = com.tencent.wxop.stat.c.g(context);
        bp = t.s(context).t(context);
        if (ac() != e.bF)
        {
            bf = l.K(context).intValue();
        } else
        {
            bf = -e.bF.r();
        }
        if (!h.e(bt))
        {
            context = com.tencent.wxop.stat.c.h(context);
            bt = context;
            if (!l.e(context))
            {
                bt = "0";
            }
        }
    }

    private boolean c(JSONObject jsonobject)
    {
        int i;
        boolean flag;
        try
        {
            r.a(jsonobject, "ky", b);
            jsonobject.put("et", ac().r());
            if (bp == null)
            {
                break MISSING_BLOCK_LABEL_97;
            }
            jsonobject.put("ui", bp.b());
            r.a(jsonobject, "mc", bp.ar());
            i = bp.as();
            jsonobject.put("ut", i);
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject)
        {
            return false;
        }
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_97;
        }
        if (l.N(bv) == 1)
        {
            jsonobject.put("ia", 1);
        }
        r.a(jsonobject, "cui", bq);
        if (ac() != e.by)
        {
            r.a(jsonobject, "av", bs);
            r.a(jsonobject, "ch", br);
        }
        if (bu)
        {
            jsonobject.put("impt", 1);
        }
        r.a(jsonobject, "mid", bt);
        jsonobject.put("idx", bf);
        jsonobject.put("si", L);
        jsonobject.put("ts", aZ);
        jsonobject.put("dts", l.a(bv, false));
        flag = b(jsonobject);
        return flag;
    }

    public final Context J()
    {
        return bv;
    }

    public final boolean X()
    {
        return bu;
    }

    public abstract e ac();

    public final long ad()
    {
        return aZ;
    }

    public final f ae()
    {
        return bw;
    }

    public final String af()
    {
        Object obj;
        try
        {
            obj = new JSONObject();
            c(((JSONObject) (obj)));
            obj = ((JSONObject) (obj)).toString();
        }
        catch (Throwable throwable)
        {
            return "";
        }
        return ((String) (obj));
    }

    public abstract boolean b(JSONObject jsonobject);

}
