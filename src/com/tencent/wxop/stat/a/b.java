// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.a;

import org.json.JSONArray;
import org.json.JSONObject;

public final class b
{

    public String a;
    public JSONArray bl;
    public JSONObject bm;

    public b()
    {
        bm = null;
    }

    public b(String s)
    {
        bm = null;
        a = s;
        bm = new JSONObject();
    }

    public final boolean equals(Object obj)
    {
        if (obj != null)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj instanceof b)
            {
                obj = (b)obj;
                return toString().equals(((b) (obj)).toString());
            }
        }
        return false;
    }

    public final int hashCode()
    {
        return toString().hashCode();
    }

    public final String toString()
    {
        StringBuilder stringbuilder = new StringBuilder(32);
        stringbuilder.append(a).append(",");
        if (bl != null)
        {
            stringbuilder.append(bl.toString());
        }
        if (bm != null)
        {
            stringbuilder.append(bm.toString());
        }
        return stringbuilder.toString();
    }
}
