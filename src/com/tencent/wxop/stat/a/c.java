// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.a;

import android.content.Context;
import com.tencent.wxop.stat.b.d;
import com.tencent.wxop.stat.b.r;
import com.tencent.wxop.stat.f;
import java.io.PrintWriter;
import java.io.StringWriter;
import org.json.JSONObject;

// Referenced classes of package com.tencent.wxop.stat.a:
//            d, e

public final class c extends com.tencent.wxop.stat.a.d
{

    private String a;
    private int ay;
    private int bn;
    private Thread bo;

    public c(Context context, int i, Throwable throwable, f f)
    {
        super(context, i, f);
        bn = 100;
        bo = null;
        a(99, throwable);
    }

    public c(Context context, int i, Throwable throwable, Thread thread)
    {
        super(context, i, null);
        bn = 100;
        bo = null;
        a(2, throwable);
        bo = thread;
    }

    private void a(int i, Throwable throwable)
    {
        if (throwable != null)
        {
            StringWriter stringwriter = new StringWriter();
            PrintWriter printwriter = new PrintWriter(stringwriter);
            throwable.printStackTrace(printwriter);
            a = stringwriter.toString();
            ay = i;
            printwriter.close();
        }
    }

    public final e ac()
    {
        return e.bz;
    }

    public final boolean b(JSONObject jsonobject)
    {
        r.a(jsonobject, "er", a);
        jsonobject.put("ea", ay);
        if (ay == 2 || ay == 3)
        {
            (new d(bv)).a(jsonobject, bo);
        }
        return true;
    }
}
