// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.a;

import android.content.Context;
import com.tencent.wxop.stat.e;
import com.tencent.wxop.stat.f;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.wxop.stat.a:
//            d, b, e

public final class a extends d
{

    protected b bj;
    private long bk;

    public a(Context context, int i, String s, f f)
    {
        super(context, i, f);
        bj = new b();
        bk = -1L;
        bj.a = s;
    }

    public final b ab()
    {
        return bj;
    }

    public final com.tencent.wxop.stat.a.e ac()
    {
        return e.bA;
    }

    public final boolean b(JSONObject jsonobject)
    {
        jsonobject.put("ei", bj.a);
        if (bk > 0L)
        {
            jsonobject.put("du", bk);
        }
        if (bj.bl == null)
        {
            if (bj.a != null)
            {
                Object obj = e.p(bj.a);
                if (obj != null && ((Properties) (obj)).size() > 0)
                {
                    if (bj.bm == null || bj.bm.length() == 0)
                    {
                        bj.bm = new JSONObject(((java.util.Map) (obj)));
                    } else
                    {
                        obj = ((Properties) (obj)).entrySet().iterator();
                        while (((Iterator) (obj)).hasNext()) 
                        {
                            java.util.Map.Entry entry = (java.util.Map.Entry)((Iterator) (obj)).next();
                            try
                            {
                                bj.bm.put(entry.getKey().toString(), entry.getValue());
                            }
                            catch (JSONException jsonexception)
                            {
                                jsonexception.printStackTrace();
                            }
                        }
                    }
                }
            }
            jsonobject.put("kv", bj.bm);
        } else
        {
            jsonobject.put("ar", bj.bl);
        }
        return true;
    }
}
