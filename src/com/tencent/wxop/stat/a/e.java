// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.wxop.stat.a;


public final class e extends Enum
{

    public static final e bA;
    public static final e bB;
    public static final e bC;
    public static final e bD;
    public static final e bE;
    public static final e bF;
    private static final e bH[];
    public static final e bx;
    public static final e by;
    public static final e bz;
    private int bG;

    private e(String s, int i, int j)
    {
        super(s, i);
        bG = j;
    }

    public final int r()
    {
        return bG;
    }

    static 
    {
        bx = new e("PAGE_VIEW", 0, 1);
        by = new e("SESSION_ENV", 1, 2);
        bz = new e("ERROR", 2, 3);
        bA = new e("CUSTOM", 3, 1000);
        bB = new e("ADDITION", 4, 1001);
        bC = new e("MONITOR_STAT", 5, 1002);
        bD = new e("MTA_GAME_USER", 6, 1003);
        bE = new e("NETWORK_MONITOR", 7, 1004);
        bF = new e("NETWORK_DETECTOR", 8, 1005);
        bH = (new e[] {
            bx, by, bz, bA, bB, bC, bD, bE, bF
        });
    }
}
