// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.modelmsg;

import android.graphics.Bitmap;
import android.os.Bundle;
import com.tencent.mm.sdk.b.a;
import java.io.ByteArrayOutputStream;

public final class WXMediaMessage
{
    public static class Builder
    {

        public static final String KEY_IDENTIFIER = "_wxobject_identifier_";

        public static WXMediaMessage fromBundle(Bundle bundle)
        {
            WXMediaMessage wxmediamessage = new WXMediaMessage();
            wxmediamessage.sdkVer = bundle.getInt("_wxobject_sdkVer");
            wxmediamessage.title = bundle.getString("_wxobject_title");
            wxmediamessage.description = bundle.getString("_wxobject_description");
            wxmediamessage.thumbData = bundle.getByteArray("_wxobject_thumbdata");
            wxmediamessage.mediaTagName = bundle.getString("_wxobject_mediatagname");
            wxmediamessage.messageAction = bundle.getString("_wxobject_message_action");
            wxmediamessage.messageExt = bundle.getString("_wxobject_message_ext");
            String s = pathOldToNew(bundle.getString("_wxobject_identifier_"));
            if (s == null || s.length() <= 0)
            {
                return wxmediamessage;
            }
            try
            {
                wxmediamessage.mediaObject = (IMediaObject)Class.forName(s).newInstance();
                wxmediamessage.mediaObject.unserialize(bundle);
            }
            // Misplaced declaration of an exception variable
            catch (Bundle bundle)
            {
                bundle.printStackTrace();
                a.a("MicroMsg.SDK.WXMediaMessage", (new StringBuilder("get media object from bundle failed: unknown ident ")).append(s).append(", ex = ").append(bundle.getMessage()).toString());
                return wxmediamessage;
            }
            return wxmediamessage;
        }

        private static String pathNewToOld(String s)
        {
            if (s == null || s.length() == 0)
            {
                a.a("MicroMsg.SDK.WXMediaMessage", "pathNewToOld fail, newPath is null");
                return s;
            } else
            {
                return s.replace("com.tencent.mm.sdk.modelmsg", "com.tencent.mm.sdk.openapi");
            }
        }

        private static String pathOldToNew(String s)
        {
            if (s == null || s.length() == 0)
            {
                a.a("MicroMsg.SDK.WXMediaMessage", "pathOldToNew fail, oldPath is null");
                return s;
            } else
            {
                return s.replace("com.tencent.mm.sdk.openapi", "com.tencent.mm.sdk.modelmsg");
            }
        }

        public static Bundle toBundle(WXMediaMessage wxmediamessage)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("_wxobject_sdkVer", wxmediamessage.sdkVer);
            bundle.putString("_wxobject_title", wxmediamessage.title);
            bundle.putString("_wxobject_description", wxmediamessage.description);
            bundle.putByteArray("_wxobject_thumbdata", wxmediamessage.thumbData);
            if (wxmediamessage.mediaObject != null)
            {
                bundle.putString("_wxobject_identifier_", pathNewToOld(wxmediamessage.mediaObject.getClass().getName()));
                wxmediamessage.mediaObject.serialize(bundle);
            }
            bundle.putString("_wxobject_mediatagname", wxmediamessage.mediaTagName);
            bundle.putString("_wxobject_message_action", wxmediamessage.messageAction);
            bundle.putString("_wxobject_message_ext", wxmediamessage.messageExt);
            return bundle;
        }

        public Builder()
        {
        }
    }

    public static interface IMediaObject
    {

        public static final int TYPE_APPDATA = 7;
        public static final int TYPE_CARD_SHARE = 16;
        public static final int TYPE_DEVICE_ACCESS = 12;
        public static final int TYPE_EMOJI = 8;
        public static final int TYPE_EMOTICON_GIFT = 11;
        public static final int TYPE_EMOTICON_SHARED = 15;
        public static final int TYPE_FILE = 6;
        public static final int TYPE_IMAGE = 2;
        public static final int TYPE_LOCATION_SHARE = 17;
        public static final int TYPE_MALL_PRODUCT = 13;
        public static final int TYPE_MUSIC = 3;
        public static final int TYPE_OLD_TV = 14;
        public static final int TYPE_PRODUCT = 10;
        public static final int TYPE_RECORD = 19;
        public static final int TYPE_TEXT = 1;
        public static final int TYPE_TV = 20;
        public static final int TYPE_UNKNOWN = 0;
        public static final int TYPE_URL = 5;
        public static final int TYPE_VIDEO = 4;

        public abstract boolean checkArgs();

        public abstract void serialize(Bundle bundle);

        public abstract int type();

        public abstract void unserialize(Bundle bundle);
    }


    public static final String ACTION_WXAPPMESSAGE = "com.tencent.mm.sdk.openapi.Intent.ACTION_WXAPPMESSAGE";
    private static final int DESCRIPTION_LENGTH_LIMIT = 1024;
    private static final int MEDIA_TAG_NAME_LENGTH_LIMIT = 64;
    private static final int MESSAGE_ACTION_LENGTH_LIMIT = 2048;
    private static final int MESSAGE_EXT_LENGTH_LIMIT = 2048;
    private static final String TAG = "MicroMsg.SDK.WXMediaMessage";
    public static final int THUMB_LENGTH_LIMIT = 32768;
    private static final int TITLE_LENGTH_LIMIT = 512;
    public String description;
    public IMediaObject mediaObject;
    public String mediaTagName;
    public String messageAction;
    public String messageExt;
    public int sdkVer;
    public byte thumbData[];
    public String title;

    public WXMediaMessage()
    {
        this(null);
    }

    public WXMediaMessage(IMediaObject imediaobject)
    {
        mediaObject = imediaobject;
    }

    final boolean checkArgs()
    {
        if (getType() == 8 && (thumbData == null || thumbData.length == 0))
        {
            a.a("MicroMsg.SDK.WXMediaMessage", "checkArgs fail, thumbData should not be null when send emoji");
            return false;
        }
        if (thumbData != null && thumbData.length > 32768)
        {
            a.a("MicroMsg.SDK.WXMediaMessage", "checkArgs fail, thumbData is invalid");
            return false;
        }
        if (title != null && title.length() > 512)
        {
            a.a("MicroMsg.SDK.WXMediaMessage", "checkArgs fail, title is invalid");
            return false;
        }
        if (description != null && description.length() > 1024)
        {
            a.a("MicroMsg.SDK.WXMediaMessage", "checkArgs fail, description is invalid");
            return false;
        }
        if (mediaObject == null)
        {
            a.a("MicroMsg.SDK.WXMediaMessage", "checkArgs fail, mediaObject is null");
            return false;
        }
        if (mediaTagName != null && mediaTagName.length() > 64)
        {
            a.a("MicroMsg.SDK.WXMediaMessage", "checkArgs fail, mediaTagName is too long");
            return false;
        }
        if (messageAction != null && messageAction.length() > 2048)
        {
            a.a("MicroMsg.SDK.WXMediaMessage", "checkArgs fail, messageAction is too long");
            return false;
        }
        if (messageExt != null && messageExt.length() > 2048)
        {
            a.a("MicroMsg.SDK.WXMediaMessage", "checkArgs fail, messageExt is too long");
            return false;
        } else
        {
            return mediaObject.checkArgs();
        }
    }

    public final int getType()
    {
        if (mediaObject == null)
        {
            return 0;
        } else
        {
            return mediaObject.type();
        }
    }

    public final void setThumbImage(Bitmap bitmap)
    {
        try
        {
            ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
            bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 85, bytearrayoutputstream);
            thumbData = bytearrayoutputstream.toByteArray();
            bytearrayoutputstream.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Bitmap bitmap)
        {
            bitmap.printStackTrace();
        }
        a.a("MicroMsg.SDK.WXMediaMessage", "put thumb failed");
    }
}
