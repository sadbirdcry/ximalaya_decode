// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.openapi;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.tencent.mm.sdk.a.a.b;
import com.tencent.mm.sdk.b.a;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelpay.PayResp;
import com.tencent.wxop.stat.c;
import com.tencent.wxop.stat.d;
import com.tencent.wxop.stat.e;

// Referenced classes of package com.tencent.mm.sdk.openapi:
//            IWXAPI, MMSharedPreferences, WXApiImplComm, IWXAPIEventHandler

final class WXApiImplV10
    implements IWXAPI
{
    private static final class ActivityLifecycleCb
        implements android.app.Application.ActivityLifecycleCallbacks
    {

        private static final int DELAYED = 800;
        private static final String TAG = "MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb";
        private Context context;
        private Handler handler;
        private boolean isForeground;
        private Runnable onPausedRunnable;
        private Runnable onResumedRunnable;

        public final void detach()
        {
            handler.removeCallbacks(onResumedRunnable);
            handler.removeCallbacks(onPausedRunnable);
            context = null;
        }

        public final void onActivityCreated(Activity activity, Bundle bundle)
        {
        }

        public final void onActivityDestroyed(Activity activity)
        {
        }

        public final void onActivityPaused(Activity activity)
        {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", (new StringBuilder()).append(activity.getComponentName().getClassName()).append("  onActivityPaused").toString());
            handler.removeCallbacks(onResumedRunnable);
            handler.postDelayed(onPausedRunnable, 800L);
        }

        public final void onActivityResumed(Activity activity)
        {
            Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", (new StringBuilder()).append(activity.getComponentName().getClassName()).append("  onActivityResumed").toString());
            handler.removeCallbacks(onPausedRunnable);
            handler.postDelayed(onResumedRunnable, 800L);
        }

        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle)
        {
        }

        public final void onActivityStarted(Activity activity)
        {
        }

        public final void onActivityStopped(Activity activity)
        {
        }



/*
        static boolean access$202(ActivityLifecycleCb activitylifecyclecb, boolean flag)
        {
            activitylifecyclecb.isForeground = flag;
            return flag;
        }

*/


        private ActivityLifecycleCb(Context context1)
        {
            isForeground = false;
            handler = new Handler(Looper.getMainLooper());
            class _cls1
                implements Runnable
            {

                final ActivityLifecycleCb this$0;

                public void run()
                {
                    if (WXApiImplV10.activityCb != null && isForeground)
                    {
                        Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", "WXStat trigger onBackground");
                        e.d(context, "onBackground_WX");
                        isForeground = false;
                    }
                }

                _cls1()
                {
                    this$0 = ActivityLifecycleCb.this;
                    super();
                }
            }

            onPausedRunnable = new _cls1();
            class _cls2
                implements Runnable
            {

                final ActivityLifecycleCb this$0;

                public void run()
                {
                    if (WXApiImplV10.activityCb != null && !isForeground)
                    {
                        Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", "WXStat trigger onForeground");
                        e.d(context, "onForeground_WX");
                        isForeground = true;
                    }
                }

                _cls2()
                {
                    this$0 = ActivityLifecycleCb.this;
                    super();
                }
            }

            onResumedRunnable = new _cls2();
            context = context1;
        }

    }


    private static final String TAG = "MicroMsg.SDK.WXApiImplV10";
    private static ActivityLifecycleCb activityCb = null;
    private static String wxappPayEntryClassname = null;
    private String appId;
    private boolean checkSignature;
    private Context context;
    private boolean detached;

    WXApiImplV10(Context context1, String s, boolean flag)
    {
        checkSignature = false;
        detached = false;
        a.d("MicroMsg.SDK.WXApiImplV10", (new StringBuilder("<init>, appId = ")).append(s).append(", checkSignature = ").append(flag).toString());
        context = context1;
        appId = s;
        checkSignature = flag;
    }

    private boolean checkSumConsistent(byte abyte0[], byte abyte1[])
    {
        if (abyte0 != null && abyte0.length != 0 && abyte1 != null && abyte1.length != 0) goto _L2; else goto _L1
_L1:
        a.a("MicroMsg.SDK.WXApiImplV10", "checkSumConsistent fail, invalid arguments");
_L4:
        return false;
_L2:
        if (abyte0.length != abyte1.length)
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "checkSumConsistent fail, length is different");
            return false;
        }
        int i = 0;
label0:
        do
        {
label1:
            {
                if (i >= abyte0.length)
                {
                    break label1;
                }
                if (abyte0[i] != abyte1[i])
                {
                    break label0;
                }
                i++;
            }
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
        return true;
    }

    private void initMta(Context context1, String s)
    {
        s = (new StringBuilder("AWXOP")).append(s).toString();
        c.b(context1, s);
        c.w();
        c.a(d.aG);
        c.t();
        c.c(context1, "Wechat_Sdk");
        try
        {
            e.a(context1, s, "2.0.3");
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context1)
        {
            context1.printStackTrace();
        }
    }

    private boolean sendAddCardToWX(Context context1, Bundle bundle)
    {
        context1 = context1.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/addCardToWX"), null, null, new String[] {
            appId, bundle.getString("_wxapi_add_card_to_wx_card_list"), bundle.getString("_wxapi_basereq_transaction")
        }, null);
        if (context1 != null)
        {
            context1.close();
        }
        return true;
    }

    private boolean sendJumpToBizProfileReq(Context context1, Bundle bundle)
    {
        context1 = context1.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile"), null, null, new String[] {
            appId, bundle.getString("_wxapi_jump_to_biz_profile_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_profile_req_ext_msg"), (new StringBuilder()).append(bundle.getInt("_wxapi_jump_to_biz_profile_req_scene")).toString(), (new StringBuilder()).append(bundle.getInt("_wxapi_jump_to_biz_profile_req_profile_type")).toString()
        }, null);
        if (context1 != null)
        {
            context1.close();
        }
        return true;
    }

    private boolean sendJumpToBizWebviewReq(Context context1, Bundle bundle)
    {
        context1 = context1.getContentResolver().query(Uri.parse("content://com.tencent.mm.sdk.comm.provider/jumpToBizProfile"), null, null, new String[] {
            appId, bundle.getString("_wxapi_jump_to_biz_webview_req_to_user_name"), bundle.getString("_wxapi_jump_to_biz_webview_req_ext_msg"), (new StringBuilder()).append(bundle.getInt("_wxapi_jump_to_biz_webview_req_scene")).toString()
        }, null);
        if (context1 != null)
        {
            context1.close();
        }
        return true;
    }

    private boolean sendPayReq(Context context1, Bundle bundle)
    {
        if (wxappPayEntryClassname == null)
        {
            wxappPayEntryClassname = (new MMSharedPreferences(context1)).getString("_wxapp_pay_entry_classname_", null);
            a.d("MicroMsg.SDK.WXApiImplV10", (new StringBuilder("pay, set wxappPayEntryClassname = ")).append(wxappPayEntryClassname).toString());
            if (wxappPayEntryClassname == null)
            {
                a.a("MicroMsg.SDK.WXApiImplV10", "pay fail, wxappPayEntryClassname is null");
                return false;
            }
        }
        com.tencent.mm.sdk.a.a.a a1 = new com.tencent.mm.sdk.a.a.a();
        a1.n = bundle;
        a1.k = "com.tencent.mm";
        a1.l = wxappPayEntryClassname;
        return com.tencent.mm.sdk.a.a.a(context1, a1);
    }

    public final void detach()
    {
        a.d("MicroMsg.SDK.WXApiImplV10", "detach");
        detached = true;
        if (activityCb == null || android.os.Build.VERSION.SDK_INT < 14) goto _L2; else goto _L1
_L1:
        if (!(context instanceof Activity)) goto _L4; else goto _L3
_L3:
        ((Activity)context).getApplication().unregisterActivityLifecycleCallbacks(activityCb);
_L6:
        activityCb.detach();
_L2:
        context = null;
        return;
_L4:
        if (context instanceof Service)
        {
            ((Service)context).getApplication().unregisterActivityLifecycleCallbacks(activityCb);
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    public final int getWXAppSupportAPI()
    {
        if (detached)
        {
            throw new IllegalStateException("getWXAppSupportAPI fail, WXMsgImpl has been detached");
        }
        if (!isWXAppInstalled())
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "open wx app failed, not installed or signature check failed");
            return 0;
        } else
        {
            return (new MMSharedPreferences(context)).getInt("_build_info_sdk_int_", 0);
        }
    }

    public final boolean handleIntent(Intent intent, IWXAPIEventHandler iwxapieventhandler)
    {
        if (!WXApiImplComm.isIntentFromWx(intent, "com.tencent.mm.openapi.token"))
        {
            a.c("MicroMsg.SDK.WXApiImplV10", "handleIntent fail, intent not from weixin msg");
            return false;
        }
        if (detached)
        {
            throw new IllegalStateException("handleIntent fail, WXMsgImpl has been detached");
        }
        String s = intent.getStringExtra("_mmessage_content");
        int i = intent.getIntExtra("_mmessage_sdkVersion", 0);
        String s1 = intent.getStringExtra("_mmessage_appPackage");
        if (s1 == null || s1.length() == 0)
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "invalid argument");
            return false;
        }
        if (!checkSumConsistent(intent.getByteArrayExtra("_mmessage_checksum"), b.a(s, i, s1)))
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "checksum fail");
            return false;
        }
        i = intent.getIntExtra("_wxapi_command_type", 0);
        switch (i)
        {
        case 7: // '\007'
        case 8: // '\b'
        default:
            a.a("MicroMsg.SDK.WXApiImplV10", (new StringBuilder("unknown cmd = ")).append(i).toString());
            return false;

        case 1: // '\001'
            iwxapieventhandler.onResp(new com.tencent.mm.sdk.modelmsg.SendAuth.Resp(intent.getExtras()));
            return true;

        case 2: // '\002'
            iwxapieventhandler.onResp(new com.tencent.mm.sdk.modelmsg.SendMessageToWX.Resp(intent.getExtras()));
            return true;

        case 3: // '\003'
            iwxapieventhandler.onReq(new com.tencent.mm.sdk.modelmsg.GetMessageFromWX.Req(intent.getExtras()));
            return true;

        case 4: // '\004'
            iwxapieventhandler.onReq(new com.tencent.mm.sdk.modelmsg.ShowMessageFromWX.Req(intent.getExtras()));
            return true;

        case 5: // '\005'
            iwxapieventhandler.onResp(new PayResp(intent.getExtras()));
            return true;

        case 6: // '\006'
            iwxapieventhandler.onReq(new com.tencent.mm.sdk.modelmsg.LaunchFromWX.Req(intent.getExtras()));
            return true;

        case 9: // '\t'
            iwxapieventhandler.onResp(new com.tencent.mm.sdk.modelbiz.AddCardToWXCardPackage.Resp(intent.getExtras()));
            break;
        }
        return true;
    }

    public final boolean isWXAppInstalled()
    {
        if (detached)
        {
            throw new IllegalStateException("isWXAppInstalled fail, WXMsgImpl has been detached");
        }
        PackageInfo packageinfo;
        boolean flag;
        try
        {
            packageinfo = context.getPackageManager().getPackageInfo("com.tencent.mm", 64);
        }
        catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception)
        {
            return false;
        }
        if (packageinfo == null)
        {
            return false;
        }
        flag = WXApiImplComm.validateAppSignature(context, packageinfo.signatures, checkSignature);
        return flag;
    }

    public final boolean isWXAppSupportAPI()
    {
        if (detached)
        {
            throw new IllegalStateException("isWXAppSupportAPI fail, WXMsgImpl has been detached");
        }
        return getWXAppSupportAPI() >= 0x22010003;
    }

    public final boolean openWXApp()
    {
        if (detached)
        {
            throw new IllegalStateException("openWXApp fail, WXMsgImpl has been detached");
        }
        if (!isWXAppInstalled())
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "open wx app failed, not installed or signature check failed");
            return false;
        }
        try
        {
            context.startActivity(context.getPackageManager().getLaunchIntentForPackage("com.tencent.mm"));
        }
        catch (Exception exception)
        {
            a.a("MicroMsg.SDK.WXApiImplV10", (new StringBuilder("startActivity fail, exception = ")).append(exception.getMessage()).toString());
            return false;
        }
        return true;
    }

    public final boolean registerApp(String s)
    {
        if (detached)
        {
            throw new IllegalStateException("registerApp fail, WXMsgImpl has been detached");
        }
        if (!WXApiImplComm.validateAppSignatureForPackage(context, "com.tencent.mm", checkSignature))
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "register app failed for wechat app signature check failed");
            return false;
        }
        if (activityCb == null && android.os.Build.VERSION.SDK_INT >= 14)
        {
            if (context instanceof Activity)
            {
                initMta(context, s);
                activityCb = new ActivityLifecycleCb(context);
                ((Activity)context).getApplication().registerActivityLifecycleCallbacks(activityCb);
            } else
            if (context instanceof Service)
            {
                initMta(context, s);
                activityCb = new ActivityLifecycleCb(context);
                ((Service)context).getApplication().registerActivityLifecycleCallbacks(activityCb);
            } else
            {
                a.b("MicroMsg.SDK.WXApiImplV10", "context is not instanceof Activity or Service, disable WXStat");
            }
        }
        a.d("MicroMsg.SDK.WXApiImplV10", (new StringBuilder("registerApp, appId = ")).append(s).toString());
        if (s != null)
        {
            appId = s;
        }
        a.d("MicroMsg.SDK.WXApiImplV10", (new StringBuilder("register app ")).append(context.getPackageName()).toString());
        s = new com.tencent.mm.sdk.a.a.a.a();
        s.o = "com.tencent.mm";
        s.p = "com.tencent.mm.plugin.openapi.Intent.ACTION_HANDLE_APP_REGISTER";
        s.m = (new StringBuilder("weixin://registerapp?appid=")).append(appId).toString();
        return com.tencent.mm.sdk.a.a.a.a(context, s);
    }

    public final boolean sendReq(BaseReq basereq)
    {
        if (detached)
        {
            throw new IllegalStateException("sendReq fail, WXMsgImpl has been detached");
        }
        if (!WXApiImplComm.validateAppSignatureForPackage(context, "com.tencent.mm", checkSignature))
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "sendReq failed for wechat app signature check failed");
            return false;
        }
        if (!basereq.checkArgs())
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "sendReq checkArgs fail");
            return false;
        }
        a.d("MicroMsg.SDK.WXApiImplV10", (new StringBuilder("sendReq, req type = ")).append(basereq.getType()).toString());
        Bundle bundle = new Bundle();
        basereq.toBundle(bundle);
        if (basereq.getType() == 5)
        {
            return sendPayReq(context, bundle);
        }
        if (basereq.getType() == 7)
        {
            return sendJumpToBizProfileReq(context, bundle);
        }
        if (basereq.getType() == 8)
        {
            return sendJumpToBizWebviewReq(context, bundle);
        }
        if (basereq.getType() == 9)
        {
            return sendAddCardToWX(context, bundle);
        } else
        {
            basereq = new com.tencent.mm.sdk.a.a.a();
            basereq.n = bundle;
            basereq.m = (new StringBuilder("weixin://sendreq?appid=")).append(appId).toString();
            basereq.k = "com.tencent.mm";
            basereq.l = "com.tencent.mm.plugin.base.stub.WXEntryActivity";
            return com.tencent.mm.sdk.a.a.a(context, basereq);
        }
    }

    public final boolean sendResp(BaseResp baseresp)
    {
        if (detached)
        {
            throw new IllegalStateException("sendResp fail, WXMsgImpl has been detached");
        }
        if (!WXApiImplComm.validateAppSignatureForPackage(context, "com.tencent.mm", checkSignature))
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "sendResp failed for wechat app signature check failed");
            return false;
        }
        if (!baseresp.checkArgs())
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "sendResp checkArgs fail");
            return false;
        } else
        {
            Bundle bundle = new Bundle();
            baseresp.toBundle(bundle);
            baseresp = new com.tencent.mm.sdk.a.a.a();
            baseresp.n = bundle;
            baseresp.m = (new StringBuilder("weixin://sendresp?appid=")).append(appId).toString();
            baseresp.k = "com.tencent.mm";
            baseresp.l = "com.tencent.mm.plugin.base.stub.WXEntryActivity";
            return com.tencent.mm.sdk.a.a.a(context, baseresp);
        }
    }

    public final void unregisterApp()
    {
        if (detached)
        {
            throw new IllegalStateException("unregisterApp fail, WXMsgImpl has been detached");
        }
        if (!WXApiImplComm.validateAppSignatureForPackage(context, "com.tencent.mm", checkSignature))
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "unregister app failed for wechat app signature check failed");
            return;
        }
        a.d("MicroMsg.SDK.WXApiImplV10", (new StringBuilder("unregisterApp, appId = ")).append(appId).toString());
        if (appId == null || appId.length() == 0)
        {
            a.a("MicroMsg.SDK.WXApiImplV10", "unregisterApp fail, appId is empty");
            return;
        } else
        {
            a.d("MicroMsg.SDK.WXApiImplV10", (new StringBuilder("unregister app ")).append(context.getPackageName()).toString());
            com.tencent.mm.sdk.a.a.a.a a1 = new com.tencent.mm.sdk.a.a.a.a();
            a1.o = "com.tencent.mm";
            a1.p = "com.tencent.mm.plugin.openapi.Intent.ACTION_HANDLE_APP_UNREGISTER";
            a1.m = (new StringBuilder("weixin://unregisterapp?appid=")).append(appId).toString();
            com.tencent.mm.sdk.a.a.a.a(context, a1);
            return;
        }
    }


}
