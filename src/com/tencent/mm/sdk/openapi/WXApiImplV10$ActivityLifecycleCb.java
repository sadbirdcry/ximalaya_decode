// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.openapi;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.tencent.wxop.stat.e;

// Referenced classes of package com.tencent.mm.sdk.openapi:
//            WXApiImplV10

private static final class <init>
    implements android.app.backs
{

    private static final int DELAYED = 800;
    private static final String TAG = "MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb";
    private Context context;
    private Handler handler;
    private boolean isForeground;
    private Runnable onPausedRunnable;
    private Runnable onResumedRunnable;

    public final void detach()
    {
        handler.removeCallbacks(onResumedRunnable);
        handler.removeCallbacks(onPausedRunnable);
        context = null;
    }

    public final void onActivityCreated(Activity activity, Bundle bundle)
    {
    }

    public final void onActivityDestroyed(Activity activity)
    {
    }

    public final void onActivityPaused(Activity activity)
    {
        Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", (new StringBuilder()).append(activity.getComponentName().getClassName()).append("  onActivityPaused").toString());
        handler.removeCallbacks(onResumedRunnable);
        handler.postDelayed(onPausedRunnable, 800L);
    }

    public final void onActivityResumed(Activity activity)
    {
        Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", (new StringBuilder()).append(activity.getComponentName().getClassName()).append("  onActivityResumed").toString());
        handler.removeCallbacks(onPausedRunnable);
        handler.postDelayed(onResumedRunnable, 800L);
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle)
    {
    }

    public final void onActivityStarted(Activity activity)
    {
    }

    public final void onActivityStopped(Activity activity)
    {
    }



/*
    static boolean access$202(_cls2 _pcls2, boolean flag)
    {
        _pcls2.isForeground = flag;
        return flag;
    }

*/


    private _cls2.this._cls0(Context context1)
    {
        isForeground = false;
        handler = new Handler(Looper.getMainLooper());
        class _cls1
            implements Runnable
        {

            final WXApiImplV10.ActivityLifecycleCb this$0;

            public void run()
            {
                if (WXApiImplV10.access$100() != null && isForeground)
                {
                    Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", "WXStat trigger onBackground");
                    e.d(context, "onBackground_WX");
                    isForeground = false;
                }
            }

            _cls1()
            {
                this$0 = WXApiImplV10.ActivityLifecycleCb.this;
                super();
            }
        }

        onPausedRunnable = new _cls1();
        class _cls2
            implements Runnable
        {

            final WXApiImplV10.ActivityLifecycleCb this$0;

            public void run()
            {
                if (WXApiImplV10.access$100() != null && !isForeground)
                {
                    Log.v("MicroMsg.SDK.WXApiImplV10.ActivityLifecycleCb", "WXStat trigger onForeground");
                    e.d(context, "onForeground_WX");
                    isForeground = true;
                }
            }

            _cls2()
            {
                this$0 = WXApiImplV10.ActivityLifecycleCb.this;
                super();
            }
        }

        onResumedRunnable = new _cls2();
        context = context1;
    }

    context(Context context1, context context2)
    {
        this(context1);
    }
}
