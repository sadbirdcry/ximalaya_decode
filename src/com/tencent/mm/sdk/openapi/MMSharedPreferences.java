// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.openapi;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import com.tencent.mm.sdk.b.a;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class MMSharedPreferences
    implements SharedPreferences
{
    private static class REditor
        implements android.content.SharedPreferences.Editor
    {

        private boolean clear;
        private ContentResolver cr;
        private Set remove;
        private Map values;

        public void apply()
        {
        }

        public android.content.SharedPreferences.Editor clear()
        {
            clear = true;
            return this;
        }

        public boolean commit()
        {
            ContentValues contentvalues = new ContentValues();
            if (clear)
            {
                cr.delete(com.tencent.mm.sdk.c.a.b.CONTENT_URI, null, null);
                clear = false;
            }
            String s;
            for (Iterator iterator = remove.iterator(); iterator.hasNext(); cr.delete(com.tencent.mm.sdk.c.a.b.CONTENT_URI, "key = ?", new String[] {
        s
    }))
            {
                s = (String)iterator.next();
            }

            Iterator iterator1 = values.entrySet().iterator();
            do
            {
                if (iterator1.hasNext())
                {
                    java.util.Map.Entry entry = (java.util.Map.Entry)iterator1.next();
                    Object obj = entry.getValue();
                    byte byte0;
                    if (obj == null)
                    {
                        a.a("MicroMsg.SDK.PluginProvider.Resolver", "unresolve failed, null value");
                        byte0 = 0;
                    } else
                    if (obj instanceof Integer)
                    {
                        byte0 = 1;
                    } else
                    if (obj instanceof Long)
                    {
                        byte0 = 2;
                    } else
                    if (obj instanceof String)
                    {
                        byte0 = 3;
                    } else
                    if (obj instanceof Boolean)
                    {
                        byte0 = 4;
                    } else
                    if (obj instanceof Float)
                    {
                        byte0 = 5;
                    } else
                    if (obj instanceof Double)
                    {
                        byte0 = 6;
                    } else
                    {
                        a.a("MicroMsg.SDK.PluginProvider.Resolver", (new StringBuilder("unresolve failed, unknown type=")).append(obj.getClass().toString()).toString());
                        byte0 = 0;
                    }
                    if (byte0 == 0)
                    {
                        byte0 = 0;
                    } else
                    {
                        contentvalues.put("type", Integer.valueOf(byte0));
                        contentvalues.put("value", obj.toString());
                        byte0 = 1;
                    }
                    if (byte0 != 0)
                    {
                        cr.update(com.tencent.mm.sdk.c.a.b.CONTENT_URI, contentvalues, "key = ?", new String[] {
                            (String)entry.getKey()
                        });
                    }
                } else
                {
                    return true;
                }
            } while (true);
        }

        public android.content.SharedPreferences.Editor putBoolean(String s, boolean flag)
        {
            values.put(s, Boolean.valueOf(flag));
            remove.remove(s);
            return this;
        }

        public android.content.SharedPreferences.Editor putFloat(String s, float f)
        {
            values.put(s, Float.valueOf(f));
            remove.remove(s);
            return this;
        }

        public android.content.SharedPreferences.Editor putInt(String s, int i)
        {
            values.put(s, Integer.valueOf(i));
            remove.remove(s);
            return this;
        }

        public android.content.SharedPreferences.Editor putLong(String s, long l)
        {
            values.put(s, Long.valueOf(l));
            remove.remove(s);
            return this;
        }

        public android.content.SharedPreferences.Editor putString(String s, String s1)
        {
            values.put(s, s1);
            remove.remove(s);
            return this;
        }

        public android.content.SharedPreferences.Editor putStringSet(String s, Set set)
        {
            return null;
        }

        public android.content.SharedPreferences.Editor remove(String s)
        {
            remove.add(s);
            return this;
        }

        public REditor(ContentResolver contentresolver)
        {
            values = new HashMap();
            remove = new HashSet();
            clear = false;
            cr = contentresolver;
        }
    }


    private final String columns[] = {
        "_id", "key", "type", "value"
    };
    private final ContentResolver cr;
    private REditor editor;
    private final HashMap values = new HashMap();

    public MMSharedPreferences(Context context)
    {
        editor = null;
        cr = context.getContentResolver();
    }

    private Object getValue(String s)
    {
        Cursor cursor;
        int i;
        int j;
        try
        {
            cursor = cr.query(com.tencent.mm.sdk.c.a.b.CONTENT_URI, columns, "key = ?", new String[] {
                s
            }, null);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return null;
        }
        if (cursor == null)
        {
            return null;
        }
        i = cursor.getColumnIndex("type");
        j = cursor.getColumnIndex("value");
        if (!cursor.moveToFirst())
        {
            break MISSING_BLOCK_LABEL_94;
        }
        s = ((String) (com.tencent.mm.sdk.c.a.a.a(cursor.getInt(i), cursor.getString(j))));
_L1:
        cursor.close();
        return s;
        s = null;
          goto _L1
    }

    public boolean contains(String s)
    {
        return getValue(s) != null;
    }

    public android.content.SharedPreferences.Editor edit()
    {
        if (editor == null)
        {
            editor = new REditor(cr);
        }
        return editor;
    }

    public Map getAll()
    {
        Object obj;
        Object obj1;
        int i;
        int j;
        int k;
        try
        {
            obj = cr.query(com.tencent.mm.sdk.c.a.b.CONTENT_URI, columns, null, null, null);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((Exception) (obj)).printStackTrace();
            return values;
        }
        if (obj == null)
        {
            return null;
        }
        i = ((Cursor) (obj)).getColumnIndex("key");
        j = ((Cursor) (obj)).getColumnIndex("type");
        k = ((Cursor) (obj)).getColumnIndex("value");
        for (; ((Cursor) (obj)).moveToNext(); values.put(((Cursor) (obj)).getString(i), obj1))
        {
            obj1 = com.tencent.mm.sdk.c.a.a.a(((Cursor) (obj)).getInt(j), ((Cursor) (obj)).getString(k));
        }

        ((Cursor) (obj)).close();
        obj = values;
        return ((Map) (obj));
    }

    public boolean getBoolean(String s, boolean flag)
    {
        s = ((String) (getValue(s)));
        boolean flag1 = flag;
        if (s != null)
        {
            flag1 = flag;
            if (s instanceof Boolean)
            {
                flag1 = ((Boolean)s).booleanValue();
            }
        }
        return flag1;
    }

    public float getFloat(String s, float f)
    {
        s = ((String) (getValue(s)));
        float f1 = f;
        if (s != null)
        {
            f1 = f;
            if (s instanceof Float)
            {
                f1 = ((Float)s).floatValue();
            }
        }
        return f1;
    }

    public int getInt(String s, int i)
    {
        s = ((String) (getValue(s)));
        int j = i;
        if (s != null)
        {
            j = i;
            if (s instanceof Integer)
            {
                j = ((Integer)s).intValue();
            }
        }
        return j;
    }

    public long getLong(String s, long l)
    {
        s = ((String) (getValue(s)));
        long l1 = l;
        if (s != null)
        {
            l1 = l;
            if (s instanceof Long)
            {
                l1 = ((Long)s).longValue();
            }
        }
        return l1;
    }

    public String getString(String s, String s1)
    {
        s = ((String) (getValue(s)));
        if (s != null && (s instanceof String))
        {
            return (String)s;
        } else
        {
            return s1;
        }
    }

    public Set getStringSet(String s, Set set)
    {
        return null;
    }

    public void registerOnSharedPreferenceChangeListener(android.content.SharedPreferences.OnSharedPreferenceChangeListener onsharedpreferencechangelistener)
    {
    }

    public void unregisterOnSharedPreferenceChangeListener(android.content.SharedPreferences.OnSharedPreferenceChangeListener onsharedpreferencechangelistener)
    {
    }
}
