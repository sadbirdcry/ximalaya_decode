// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.modelbiz;

import android.os.Bundle;
import com.tencent.mm.sdk.modelbase.BaseReq;
import java.util.Iterator;
import java.util.List;
import org.json.JSONStringer;

// Referenced classes of package com.tencent.mm.sdk.modelbiz:
//            AddCardToWXCardPackage

public static class rdItem extends BaseReq
{

    public List cardArrary;

    public boolean checkArgs()
    {
        if (cardArrary == null || cardArrary.size() == 0 || cardArrary.size() > 40)
        {
            return false;
        }
        for (Iterator iterator = cardArrary.iterator(); iterator.hasNext();)
        {
            rdItem rditem = (rdItem)iterator.next();
            if (rditem == null || rditem.cardId == null || rditem.cardId.length() > 1024 || rditem.cardExtMsg != null && rditem.cardExtMsg.length() > 1024)
            {
                return false;
            }
        }

        return true;
    }

    public int getType()
    {
        return 9;
    }

    public void toBundle(Bundle bundle)
    {
        JSONStringer jsonstringer;
        super.toBundle(bundle);
        jsonstringer = new JSONStringer();
        Iterator iterator;
        jsonstringer.object();
        jsonstringer.key("card_list");
        jsonstringer.array();
        iterator = cardArrary.iterator();
_L1:
        Object obj;
        if (!iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_138;
        }
        obj = (rdItem)iterator.next();
        jsonstringer.object();
        jsonstringer.key("card_id");
        jsonstringer.value(((rdItem) (obj)).cardId);
        jsonstringer.key("card_ext");
        if (((rdItem) (obj)).cardExtMsg != null)
        {
            break MISSING_BLOCK_LABEL_130;
        }
        obj = "";
_L2:
        jsonstringer.value(obj);
        jsonstringer.endObject();
          goto _L1
        try
        {
            jsonstringer.endArray();
            jsonstringer.endObject();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((Exception) (obj)).printStackTrace();
        }
        bundle.putString("_wxapi_add_card_to_wx_card_list", jsonstringer.toString());
        return;
        obj = ((rdItem) (obj)).cardExtMsg;
          goto _L2
    }

    public rdItem()
    {
    }
}
