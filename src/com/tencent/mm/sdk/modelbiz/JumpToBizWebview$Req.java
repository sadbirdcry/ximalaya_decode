// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.modelbiz;

import android.os.Bundle;
import com.tencent.mm.sdk.b.a;
import com.tencent.mm.sdk.modelbase.BaseReq;

// Referenced classes of package com.tencent.mm.sdk.modelbiz:
//            JumpToBizWebview

public static class scene extends BaseReq
{

    private static final int EXT_MSG_LENGTH = 1024;
    private static final String TAG = "MicroMsg.SDK.JumpToBizWebview.Req";
    public String extMsg;
    public int scene;
    public String toUserName;
    public int webType;

    public boolean checkArgs()
    {
        if (toUserName == null || toUserName.length() <= 0)
        {
            a.a("MicroMsg.SDK.JumpToBizWebview.Req", "checkArgs fail, toUserName is invalid");
            return false;
        }
        if (extMsg != null && extMsg.length() > 1024)
        {
            a.a("MicroMsg.SDK.JumpToBizWebview.Req", "ext msg is not null, while the length exceed 1024 bytes");
            return false;
        } else
        {
            return true;
        }
    }

    public int getType()
    {
        return 8;
    }

    public void toBundle(Bundle bundle)
    {
        super.toBundle(bundle);
        bundle.putString("_wxapi_jump_to_biz_webview_req_to_user_name", toUserName);
        bundle.putString("_wxapi_jump_to_biz_webview_req_ext_msg", extMsg);
        bundle.putInt("_wxapi_jump_to_biz_webview_req_web_type", webType);
        bundle.putInt("_wxapi_jump_to_biz_webview_req_scene", scene);
    }

    public ()
    {
        scene = 1;
    }
}
