// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.modelbiz;

import android.os.Bundle;
import com.tencent.mm.sdk.modelbase.BaseResp;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONTokener;

// Referenced classes of package com.tencent.mm.sdk.modelbiz:
//            AddCardToWXCardPackage

public static class fromBundle extends BaseResp
{

    public List cardArrary;

    public boolean checkArgs()
    {
        return cardArrary != null && cardArrary.size() != 0;
    }

    public void fromBundle(Bundle bundle)
    {
        super.fromBundle(bundle);
        if (cardArrary == null)
        {
            cardArrary = new LinkedList();
        }
        bundle = bundle.getString("_wxapi_add_card_to_wx_card_list");
        if (bundle == null || bundle.length() <= 0) goto _L2; else goto _L1
_L1:
        JSONObject jsonobject;
        dItem ditem;
        int i;
        try
        {
            bundle = ((JSONObject)(new JSONTokener(bundle)).nextValue()).getJSONArray("card_list");
        }
        // Misplaced declaration of an exception variable
        catch (Bundle bundle)
        {
            break; /* Loop/switch isn't completed */
        }
        i = 0;
_L3:
        if (i >= bundle.length())
        {
            break; /* Loop/switch isn't completed */
        }
        jsonobject = bundle.getJSONObject(i);
        ditem = new dItem();
        ditem.cardId = jsonobject.optString("card_id");
        ditem.cardExtMsg = jsonobject.optString("card_ext");
        ditem.cardState = jsonobject.optInt("is_succ");
        cardArrary.add(ditem);
        i++;
        continue; /* Loop/switch isn't completed */
        if (true) goto _L3; else goto _L2
_L2:
    }

    public int getType()
    {
        return 9;
    }

    public void toBundle(Bundle bundle)
    {
        JSONStringer jsonstringer;
        super.toBundle(bundle);
        jsonstringer = new JSONStringer();
        Iterator iterator;
        jsonstringer.object();
        jsonstringer.key("card_list");
        jsonstringer.array();
        iterator = cardArrary.iterator();
_L1:
        dItem ditem;
        if (!iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_160;
        }
        ditem = (dItem)iterator.next();
        jsonstringer.object();
        jsonstringer.key("card_id");
        jsonstringer.value(ditem.cardId);
        jsonstringer.key("card_ext");
        if (ditem.cardExtMsg != null)
        {
            break MISSING_BLOCK_LABEL_151;
        }
        Object obj = "";
_L2:
        jsonstringer.value(obj);
        jsonstringer.key("is_succ");
        jsonstringer.value(ditem.cardState);
        jsonstringer.endObject();
          goto _L1
        try
        {
            jsonstringer.endArray();
            jsonstringer.endObject();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((Exception) (obj)).printStackTrace();
        }
        bundle.putString("_wxapi_add_card_to_wx_card_list", jsonstringer.toString());
        return;
        obj = ditem.cardExtMsg;
          goto _L2
    }

    public dItem()
    {
    }

    public dItem(Bundle bundle)
    {
        fromBundle(bundle);
    }
}
