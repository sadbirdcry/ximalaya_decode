// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.a.a;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.tencent.mm.sdk.b.e;

// Referenced classes of package com.tencent.mm.sdk.a.a:
//            b

public final class com.tencent.mm.sdk.a.a.a
{
    public static final class a
    {

        public String m;
        public Bundle n;
        public String o;
        public String p;

        public a()
        {
        }
    }


    public static boolean a(Context context, a a1)
    {
        if (context == null || a1 == null)
        {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.MMessage", "send fail, invalid argument");
            return false;
        }
        if (e.j(a1.p))
        {
            com.tencent.mm.sdk.b.a.a("MicroMsg.SDK.MMessage", "send fail, action is null");
            return false;
        }
        String s = null;
        if (!e.j(a1.o))
        {
            s = (new StringBuilder()).append(a1.o).append(".permission.MM_MESSAGE").toString();
        }
        Intent intent = new Intent(a1.p);
        if (a1.n != null)
        {
            intent.putExtras(a1.n);
        }
        String s1 = context.getPackageName();
        intent.putExtra("_mmessage_sdkVersion", 0x22010003);
        intent.putExtra("_mmessage_appPackage", s1);
        intent.putExtra("_mmessage_content", a1.m);
        intent.putExtra("_mmessage_checksum", b.a(a1.m, 0x22010003, s1));
        context.sendBroadcast(intent, s);
        com.tencent.mm.sdk.b.a.d("MicroMsg.SDK.MMessage", (new StringBuilder("send mm message, intent=")).append(intent).append(", perm=").append(s).toString());
        return true;
    }
}
