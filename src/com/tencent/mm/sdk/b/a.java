// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.b;

import android.os.Build;
import android.os.Looper;
import android.os.Process;

// Referenced classes of package com.tencent.mm.sdk.b:
//            b, d

public final class com.tencent.mm.sdk.b.a
{
    public static interface a
    {

        public abstract void e(String s1, String s2);

        public abstract void f(String s1, String s2);

        public abstract void g(String s1, String s2);

        public abstract int h();

        public abstract void h(String s1, String s2);
    }


    private static int level = 6;
    public static d q;
    private static a r;
    private static a s;
    private static final String t;

    public static void a(String s1, String s2)
    {
        a(s1, s2, null);
    }

    public static transient void a(String s1, String s2, Object aobj[])
    {
        if (s != null && s.h() <= 4)
        {
            if (aobj != null)
            {
                s2 = String.format(s2, aobj);
            }
            aobj = s2;
            if (s2 == null)
            {
                aobj = "";
            }
            s1 = i(s1);
            s2 = s;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            s2.h(s1, ((String) (aobj)));
        }
    }

    public static void b(String s1, String s2)
    {
        if (s != null && s.h() <= 3)
        {
            String s3 = s2;
            if (s2 == null)
            {
                s3 = "";
            }
            s1 = i(s1);
            s2 = s;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            s2.g(s1, s3);
        }
    }

    public static void c(String s1, String s2)
    {
        if (s != null && s.h() <= 2)
        {
            String s3 = s2;
            if (s2 == null)
            {
                s3 = "";
            }
            s1 = i(s1);
            s2 = s;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            s2.e(s1, s3);
        }
    }

    public static void d(String s1, String s2)
    {
        if (s != null && s.h() <= 1)
        {
            String s3 = s2;
            if (s2 == null)
            {
                s3 = "";
            }
            s1 = i(s1);
            s2 = s;
            Process.myPid();
            Thread.currentThread().getId();
            Looper.getMainLooper().getThread().getId();
            s2.f(s1, s3);
        }
    }

    static int g()
    {
        return level;
    }

    private static String i(String s1)
    {
        String s2 = s1;
        if (q != null)
        {
            s2 = q.i(s1);
        }
        return s2;
    }

    static 
    {
        Object obj = new b();
        r = ((a) (obj));
        s = ((a) (obj));
        obj = new StringBuilder();
        ((StringBuilder) (obj)).append((new StringBuilder("VERSION.RELEASE:[")).append(android.os.Build.VERSION.RELEASE).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] VERSION.CODENAME:[")).append(android.os.Build.VERSION.CODENAME).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] VERSION.INCREMENTAL:[")).append(android.os.Build.VERSION.INCREMENTAL).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] BOARD:[")).append(Build.BOARD).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] DEVICE:[")).append(Build.DEVICE).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] DISPLAY:[")).append(Build.DISPLAY).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] FINGERPRINT:[")).append(Build.FINGERPRINT).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] HOST:[")).append(Build.HOST).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] MANUFACTURER:[")).append(Build.MANUFACTURER).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] MODEL:[")).append(Build.MODEL).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] PRODUCT:[")).append(Build.PRODUCT).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] TAGS:[")).append(Build.TAGS).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] TYPE:[")).append(Build.TYPE).toString());
        ((StringBuilder) (obj)).append((new StringBuilder("] USER:[")).append(Build.USER).append("]").toString());
        t = ((StringBuilder) (obj)).toString();
    }
}
