// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.mm.sdk.b;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Set;

public final class c
{

    private int A;
    private int B;
    private int C;
    private int D;
    private int size;
    private final LinkedHashMap u;
    private int v;

    private void trimToSize(int i)
    {
_L1:
        this;
        JVM INSTR monitorenter ;
        if (size < 0 || u.isEmpty() && size != 0)
        {
            throw new IllegalStateException((new StringBuilder()).append(getClass().getName()).append(".sizeOf() is reporting inconsistent results!").toString());
        }
        break MISSING_BLOCK_LABEL_64;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        if (size > i && !u.isEmpty())
        {
            break MISSING_BLOCK_LABEL_85;
        }
        this;
        JVM INSTR monitorexit ;
        return;
        java.util.Map.Entry entry = (java.util.Map.Entry)u.entrySet().iterator().next();
        Object obj = entry.getKey();
        entry.getValue();
        u.remove(obj);
        size = size - 1;
        B = B + 1;
        this;
        JVM INSTR monitorexit ;
          goto _L1
    }

    public final boolean a(Object obj)
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag = u.containsKey(obj);
        this;
        JVM INSTR monitorexit ;
        return flag;
        obj;
        throw obj;
    }

    public final Object get(Object obj)
    {
        if (obj == null)
        {
            throw new NullPointerException("key == null");
        }
        this;
        JVM INSTR monitorenter ;
        obj = u.get(obj);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_43;
        }
        C = C + 1;
        this;
        JVM INSTR monitorexit ;
        return obj;
        D = D + 1;
        this;
        JVM INSTR monitorexit ;
        return null;
        obj;
        throw obj;
    }

    public final Object put(Object obj, Object obj1)
    {
        if (obj == null || obj1 == null)
        {
            throw new NullPointerException("key == null || value == null");
        }
        this;
        JVM INSTR monitorenter ;
        A = A + 1;
        size = size + 1;
        obj = u.put(obj, obj1);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_64;
        }
        size = size - 1;
        this;
        JVM INSTR monitorexit ;
        trimToSize(v);
        return obj;
        obj;
        throw obj;
    }

    public final String toString()
    {
        int i = 0;
        this;
        JVM INSTR monitorenter ;
        int j = C + D;
        if (j == 0)
        {
            break MISSING_BLOCK_LABEL_28;
        }
        i = (C * 100) / j;
        String s = String.format("LruCache[maxSize=%d,hits=%d,misses=%d,hitRate=%d%%]", new Object[] {
            Integer.valueOf(v), Integer.valueOf(C), Integer.valueOf(D), Integer.valueOf(i)
        });
        this;
        JVM INSTR monitorexit ;
        return s;
        Exception exception;
        exception;
        throw exception;
    }
}
