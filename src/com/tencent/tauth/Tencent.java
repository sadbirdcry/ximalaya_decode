// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.tauth;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.tencent.connect.auth.QQAuth;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.share.QQShare;
import com.tencent.connect.share.QzoneShare;
import com.tencent.open.SocialApi;
import com.tencent.open.a.f;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.HttpUtils;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.TemporaryStorage;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.tauth:
//            IUiListener, IRequestListener

public class Tencent
{

    private static Tencent sInstance;
    private final QQAuth mQQAuth;

    private Tencent(String s, Context context)
    {
        Global.setContext(context.getApplicationContext());
        mQQAuth = QQAuth.createInstance(s, context);
    }

    private static boolean checkManifestConfig(Context context, String s)
    {
        try
        {
            ComponentName componentname = new ComponentName(context.getPackageName(), "com.tencent.tauth.AuthActivity");
            context.getPackageManager().getActivityInfo(componentname, 0);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context = (new StringBuilder()).append("\u6CA1\u6709\u5728AndroidManifest.xml\u4E2D\u68C0\u6D4B\u5230com.tencent.tauth.AuthActivity,\u8BF7\u52A0\u4E0Acom.tencent.open.AuthActivity,\u5E76\u914D\u7F6E<data android:scheme=\"tencent").append(s).append("\" />,\u8BE6\u7EC6\u4FE1\u606F\u8BF7\u67E5\u770B\u5B98\u7F51\u6587\u6863.").toString();
            f.e("AndroidManifest.xml \u6CA1\u6709\u68C0\u6D4B\u5230com.tencent.tauth.AuthActivity", (new StringBuilder()).append(context).append("\n\u914D\u7F6E\u793A\u4F8B\u5982\u4E0B: \n<activity\n     android:name=\"com.tencent.connect.util.AuthActivity\"\n     android:noHistory=\"true\"\n     android:launchMode=\"singleTask\">\n<intent-filter>\n    <action android:name=\"android.intent.action.VIEW\" />\n     <category android:name=\"android.intent.category.DEFAULT\" />\n    <category android:name=\"android.intent.category.BROWSABLE\" />\n    <data android:scheme=\"tencent").append(s).append("\" />\n").append("</intent-filter>\n").append("</activity>").toString());
            return false;
        }
        try
        {
            s = new ComponentName(context.getPackageName(), "com.tencent.connect.common.AssistActivity");
            context.getPackageManager().getActivityInfo(s, 0);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            f.e("AndroidManifest.xml \u6CA1\u6709\u68C0\u6D4B\u5230com.tencent.connect.common.AssistActivity", (new StringBuilder()).append("\u6CA1\u6709\u5728AndroidManifest.xml\u4E2D\u68C0\u6D4B\u5230com.tencent.connect.common.AssistActivity,\u8BF7\u52A0\u4E0Acom.tencent.connect.common.AssistActivity,\u8BE6\u7EC6\u4FE1\u606F\u8BF7\u67E5\u770B\u5B98\u7F51\u6587\u6863.").append("\n\u914D\u7F6E\u793A\u4F8B\u5982\u4E0B: \n<activity\n     android:name=\"com.tencent.connect.common.AssistActivity\"\n     android:screenOrientation=\"portrait\"\n     android:theme=\"@android:style/Theme.Translucent.NoTitleBar\"\n     android:configChanges=\"orientation|keyboardHidden\">\n</activity>").toString());
            return false;
        }
        return true;
    }

    public static Tencent createInstance(String s, Context context)
    {
        com/tencent/tauth/Tencent;
        JVM INSTR monitorenter ;
        f.a("openSDK_LOG", "createInstance()  -- start");
        if (sInstance != null) goto _L2; else goto _L1
_L1:
        sInstance = new Tencent(s, context);
_L4:
        boolean flag = checkManifestConfig(context, s);
        if (flag)
        {
            break MISSING_BLOCK_LABEL_86;
        }
        s = null;
_L5:
        com/tencent/tauth/Tencent;
        JVM INSTR monitorexit ;
        return s;
_L2:
        if (s.equals(sInstance.getAppId())) goto _L4; else goto _L3
_L3:
        sInstance.logout(context);
        sInstance = new Tencent(s, context);
          goto _L4
        s;
        throw s;
        f.a("openSDK_LOG", "createInstance()  -- end");
        s = sInstance;
          goto _L5
    }

    public static void handleResultData(Intent intent, IUiListener iuilistener)
    {
        BaseApi.handleDataToListener(intent, iuilistener);
    }

    public int ask(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        (new SocialApi(mQQAuth.getQQToken())).ask(activity, bundle, iuilistener);
        return 0;
    }

    public void checkLogin(IUiListener iuilistener)
    {
        mQQAuth.checkLogin(iuilistener);
    }

    public String getAccessToken()
    {
        return mQQAuth.getQQToken().getAccessToken();
    }

    public String getAppId()
    {
        return mQQAuth.getQQToken().getAppId();
    }

    public long getExpiresIn()
    {
        return mQQAuth.getQQToken().getExpireTimeInSecond();
    }

    public String getOpenId()
    {
        return mQQAuth.getQQToken().getOpenId();
    }

    public QQToken getQQToken()
    {
        return mQQAuth.getQQToken();
    }

    public int gift(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        (new SocialApi(mQQAuth.getQQToken())).gift(activity, bundle, iuilistener);
        return 0;
    }

    public void handleLoginData(Intent intent, IUiListener iuilistener)
    {
        BaseApi.handleDataToListener(intent, iuilistener);
    }

    public int invite(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        (new SocialApi(mQQAuth.getQQToken())).invite(activity, bundle, iuilistener);
        return 0;
    }

    public boolean isReady()
    {
        return isSessionValid() && getOpenId() != null;
    }

    public boolean isSessionValid()
    {
        return mQQAuth.isSessionValid();
    }

    public boolean isSupportSSOLogin(Activity activity)
    {
        if (SystemUtils.getAppVersionName(activity, "com.tencent.mobileqq") == null)
        {
            return false;
        } else
        {
            return SystemUtils.checkMobileQQ(activity);
        }
    }

    public int login(Activity activity, String s, IUiListener iuilistener)
    {
        return mQQAuth.login(activity, s, iuilistener);
    }

    public int login(Fragment fragment, String s, IUiListener iuilistener)
    {
        return mQQAuth.login(fragment, s, iuilistener, "");
    }

    public int loginServerSide(Activity activity, String s, IUiListener iuilistener)
    {
        return mQQAuth.login(activity, (new StringBuilder()).append(s).append(",server_side").toString(), iuilistener);
    }

    public int loginServerSide(Fragment fragment, String s, IUiListener iuilistener)
    {
        return mQQAuth.login(fragment, (new StringBuilder()).append(s).append(",server_side").toString(), iuilistener, "");
    }

    public int loginWithOEM(Activity activity, String s, IUiListener iuilistener, String s1, String s2, String s3)
    {
        return mQQAuth.loginWithOEM(activity, s, iuilistener, s1, s2, s3);
    }

    public void logout(Context context)
    {
        mQQAuth.getQQToken().setAccessToken(null, "0");
        mQQAuth.getQQToken().setOpenId(null);
    }

    public boolean onActivityResult(int i, int j, Intent intent)
    {
        return false;
    }

    public int reAuth(Activity activity, String s, IUiListener iuilistener)
    {
        return mQQAuth.reAuth(activity, s, iuilistener);
    }

    public void releaseResource()
    {
        TemporaryStorage.remove("shareToQQ");
        TemporaryStorage.remove("shareToQzone");
        TemporaryStorage.remove("sendToMyComputer");
        TemporaryStorage.remove("addToQQFavorites");
        TemporaryStorage.remove("shareToTroopBar");
    }

    public void reportDAU()
    {
        mQQAuth.reportDAU();
    }

    public JSONObject request(String s, Bundle bundle, String s1)
        throws IOException, JSONException, com.tencent.open.utils.HttpUtils.NetworkUnavailableException, com.tencent.open.utils.HttpUtils.HttpStatusException
    {
        return HttpUtils.request(mQQAuth.getQQToken(), Global.getContext(), s, bundle, s1);
    }

    public void requestAsync(String s, Bundle bundle, String s1, IRequestListener irequestlistener, Object obj)
    {
        HttpUtils.requestAsync(mQQAuth.getQQToken(), Global.getContext(), s, bundle, s1, irequestlistener);
    }

    public void setAccessToken(String s, String s1)
    {
        f.a("openSDK_LOG", (new StringBuilder()).append("setAccessToken(), expiresIn = ").append(s1).append("").toString());
        mQQAuth.setAccessToken(s, s1);
    }

    public void setOpenId(String s)
    {
        f.a("openSDK_LOG", "setOpenId() --start");
        mQQAuth.setOpenId(Global.getContext(), s);
        f.a("openSDK_LOG", "setOpenId() --end");
    }

    public void shareToQQ(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        (new QQShare(activity, mQQAuth.getQQToken())).shareToQQ(activity, bundle, iuilistener);
    }

    public void shareToQzone(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        (new QzoneShare(activity, mQQAuth.getQQToken())).shareToQzone(activity, bundle, iuilistener);
    }

    public int story(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        (new SocialApi(mQQAuth.getQQToken())).story(activity, bundle, iuilistener);
        return 0;
    }
}
