// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.tauth;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.connect.auth.AuthDialog;
import com.tencent.connect.auth.AuthMap;
import com.tencent.connect.common.AssistActivity;
import com.tencent.connect.common.BaseApi;
import com.tencent.open.a.f;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.TemporaryStorage;
import com.tencent.open.utils.Util;
import org.json.JSONObject;

// Referenced classes of package com.tencent.tauth:
//            IUiListener

public class AuthActivity extends Activity
{

    private static final String ACTION_ADD_TO_QQFAVORITES = "addToQQFavorites";
    public static final String ACTION_KEY = "action";
    private static final String ACTION_SEND_TO_MY_COMPUTER = "sendToMyComputer";
    public static final String ACTION_SHARE_PRIZE = "sharePrize";
    private static final String ACTION_SHARE_TO_QQ = "shareToQQ";
    private static final String ACTION_SHARE_TO_QZONE = "shareToQzone";
    private static final String ACTION_SHARE_TO_TROOP_BAR = "shareToTroopBar";
    private static final String SHARE_PRIZE_ACTIVITY_ID = "activityid";
    private static final String TAG = "openSDK_LOG.AuthActivity";
    private static int mShareQzoneBackTime = 0;

    public AuthActivity()
    {
    }

    private void execAuthCallback(Bundle bundle, String s)
    {
        if (bundle == null || s == null)
        {
            finish();
            return;
        }
        AuthMap authmap = AuthMap.getInstance();
        String s1 = bundle.getString("serial");
        com.tencent.connect.auth.AuthMap.Auth auth = authmap.get(s1);
        if (auth != null)
        {
            if (s.indexOf("://cancel") != -1)
            {
                auth.listener.onCancel();
                auth.dialog.dismiss();
            } else
            {
                s = bundle.getString("access_token");
                if (s != null)
                {
                    bundle.putString("access_token", authmap.decode(s, auth.key));
                }
                bundle = Util.encodeUrl(bundle);
                bundle = Util.decodeUrlToJson(new JSONObject(), bundle);
                s = bundle.optString("cb");
                if (!"".equals(s))
                {
                    auth.dialog.callJs(s, bundle.toString());
                } else
                {
                    auth.listener.onComplete(bundle);
                    auth.dialog.dismiss();
                }
            }
            authmap.remove(s1);
        }
        finish();
    }

    private void handleActionUri(Uri uri)
    {
        Object obj;
        Object obj1;
        if (uri == null || uri.toString() == null || uri.toString().equals(""))
        {
            finish();
            return;
        }
        obj1 = uri.toString();
        uri = Util.decodeUrl(((String) (obj1)).substring(((String) (obj1)).indexOf("#") + 1));
        if (uri == null)
        {
            finish();
            return;
        }
        obj = uri.getString("action");
        f.b("openSDK_LOG.AuthActivity", (new StringBuilder()).append("-->handleActionUri, action: ").append(((String) (obj))).toString());
        if (obj == null)
        {
            execAuthCallback(uri, ((String) (obj1)));
            return;
        }
        if (((String) (obj)).equals("shareToQQ") || ((String) (obj)).equals("shareToQzone") || ((String) (obj)).equals("sendToMyComputer") || ((String) (obj)).equals("shareToTroopBar"))
        {
            if (((String) (obj)).equals("shareToQzone") && SystemUtils.getAppVersionName(this, "com.tencent.mobileqq") != null && SystemUtils.compareQQVersion(this, "5.2.0") < 0)
            {
                mShareQzoneBackTime++;
                if (mShareQzoneBackTime == 2)
                {
                    mShareQzoneBackTime = 0;
                    finish();
                    return;
                }
            }
            obj = new Intent(this, com/tencent/connect/common/AssistActivity);
            ((Intent) (obj)).putExtras(uri);
            ((Intent) (obj)).setFlags(0x24000000);
            AssistActivity.hackAuthActivity = true;
            startActivity(((Intent) (obj)));
            finish();
            return;
        }
        if (((String) (obj)).equals("addToQQFavorites"))
        {
            obj1 = getIntent();
            ((Intent) (obj1)).putExtras(uri);
            ((Intent) (obj1)).putExtra("key_action", "action_share");
            uri = ((Uri) (TemporaryStorage.get(((String) (obj)))));
            if (uri != null)
            {
                BaseApi.handleDataToListener(((Intent) (obj1)), (IUiListener)uri);
            }
            finish();
            return;
        }
        if (!((String) (obj)).equals("sharePrize")) goto _L2; else goto _L1
_L1:
        obj1 = getPackageManager().getLaunchIntentForPackage(getPackageName());
        obj = uri.getString("response");
        uri = "";
        obj = Util.parseJson(((String) (obj))).getString("activityid");
        uri = ((Uri) (obj));
_L3:
        if (!TextUtils.isEmpty(uri))
        {
            ((Intent) (obj1)).putExtra("sharePrize", true);
            Bundle bundle = new Bundle();
            bundle.putString("activityid", uri);
            ((Intent) (obj1)).putExtras(bundle);
        }
        startActivity(((Intent) (obj1)));
        finish();
        return;
        Exception exception;
        exception;
        f.e("openSDK_LOG.AuthActivity", "sharePrize parseJson has exception.");
        if (true) goto _L3; else goto _L2
_L2:
        execAuthCallback(uri, ((String) (obj1)));
        return;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        if (getIntent() == null)
        {
            return;
        }
        bundle = null;
        Uri uri = getIntent().getData();
        bundle = uri;
_L2:
        f.b("openSDK_LOG.AuthActivity", (new StringBuilder()).append("-->onCreate, uri: ").append(bundle).toString());
        handleActionUri(bundle);
        return;
        Exception exception;
        exception;
        f.e("openSDK_LOG.AuthActivity", (new StringBuilder()).append("-->onCreate, getIntent().getData() has exception! ").append(exception.getMessage()).toString());
        if (true) goto _L2; else goto _L1
_L1:
    }

}
