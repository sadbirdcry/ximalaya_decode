// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.a.a.a.a;

import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import com.tencent.wxop.stat.b.g;
import org.json.JSONObject;

public final class h
{

    private static void a(String s, Throwable throwable)
    {
        Log.e("MID", s, throwable);
    }

    static void a(JSONObject jsonobject, String s, String s1)
    {
        if (d(s1))
        {
            jsonobject.put(s, s1);
        }
    }

    static boolean a(Context context, String s)
    {
        boolean flag = false;
        int i;
        try
        {
            i = context.getPackageManager().checkPermission(s, context.getPackageName());
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            a("checkPermission error", ((Throwable) (context)));
            return false;
        }
        if (i == 0)
        {
            flag = true;
        }
        return flag;
    }

    static String b(Context context)
    {
        if (!a(context, "android.permission.READ_PHONE_STATE"))
        {
            break MISSING_BLOCK_LABEL_28;
        }
        context = ((TelephonyManager)context.getSystemService("phone")).getDeviceId();
        if (context != null)
        {
            return context;
        }
        break MISSING_BLOCK_LABEL_36;
        try
        {
            Log.i("MID", "Could not get permission of android.permission.READ_PHONE_STATE");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.w("MID", context);
        }
        return "";
    }

    static String c(Context context)
    {
        if (!a(context, "android.permission.ACCESS_WIFI_STATE"))
        {
            break MISSING_BLOCK_LABEL_62;
        }
        try
        {
            context = (WifiManager)context.getSystemService("wifi");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Log.i("MID", (new StringBuilder("get wifi address error")).append(context).toString());
            return "";
        }
        if (context == null)
        {
            return "";
        }
        context = context.getConnectionInfo().getMacAddress();
        return context;
        Log.i("MID", "Could not get permission of android.permission.ACCESS_WIFI_STATE");
        return "";
    }

    static boolean d(String s)
    {
        return s != null && s.trim().length() != 0;
    }

    public static boolean e(String s)
    {
        return s != null && s.trim().length() >= 40;
    }

    static String f(String s)
    {
        String s1;
        if (s == null)
        {
            s1 = null;
        } else
        {
            s1 = s;
            if (android.os.Build.VERSION.SDK_INT >= 8)
            {
                String s2;
                try
                {
                    s2 = (new String(com.tencent.wxop.stat.b.g.c(Base64.decode(s.getBytes("UTF-8"), 0)), "UTF-8")).trim().replace("\t", "").replace("\n", "").replace("\r", "");
                }
                catch (Throwable throwable)
                {
                    a("decode error", throwable);
                    return s;
                }
                return s2;
            }
        }
        return s1;
    }

    static String g(String s)
    {
        String s1;
        if (s == null)
        {
            s1 = null;
        } else
        {
            s1 = s;
            if (android.os.Build.VERSION.SDK_INT >= 8)
            {
                String s2;
                try
                {
                    s2 = (new String(Base64.encode(com.tencent.wxop.stat.b.g.b(s.getBytes("UTF-8")), 0), "UTF-8")).trim().replace("\t", "").replace("\n", "").replace("\r", "");
                }
                catch (Throwable throwable)
                {
                    a("decode error", throwable);
                    return s;
                }
                return s2;
            }
        }
        return s1;
    }
}
