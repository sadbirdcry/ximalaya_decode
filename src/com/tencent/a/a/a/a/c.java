// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.a.a.a.a;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.a.a.a.a:
//            h

public final class c
{

    String a;
    String b;
    String c;
    long d;

    public c()
    {
        a = null;
        b = null;
        c = "0";
        d = 0L;
    }

    static c c(String s)
    {
        c c1 = new c();
        if (h.d(s))
        {
            try
            {
                s = new JSONObject(s);
                if (!s.isNull("ui"))
                {
                    c1.a = s.getString("ui");
                }
                if (!s.isNull("mc"))
                {
                    c1.b = s.getString("mc");
                }
                if (!s.isNull("mid"))
                {
                    c1.c = s.getString("mid");
                }
                if (!s.isNull("ts"))
                {
                    c1.d = s.getLong("ts");
                }
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Log.w("MID", s);
                return c1;
            }
        }
        return c1;
    }

    private JSONObject d()
    {
        JSONObject jsonobject = new JSONObject();
        try
        {
            h.a(jsonobject, "ui", a);
            h.a(jsonobject, "mc", b);
            h.a(jsonobject, "mid", c);
            jsonobject.put("ts", d);
        }
        catch (JSONException jsonexception)
        {
            Log.w("MID", jsonexception);
            return jsonobject;
        }
        return jsonobject;
    }

    public final String c()
    {
        return c;
    }

    public final String toString()
    {
        return d().toString();
    }
}
