// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.a.a.a.a;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.tencent.a.a.a.a:
//            e, b, d, f, 
//            c, h

public final class g
{

    private static g i = null;
    private Map f;
    private int g;
    private Context h;

    private g(Context context)
    {
        f = null;
        g = 0;
        h = null;
        h = context.getApplicationContext();
        f = new HashMap(3);
        f.put(Integer.valueOf(1), new e(context));
        f.put(Integer.valueOf(2), new b(context));
        f.put(Integer.valueOf(4), new d(context));
    }

    private c a(List list)
    {
label0:
        {
            if (list == null || list.size() < 0)
            {
                break label0;
            }
            list = list.iterator();
            Object obj;
            do
            {
                do
                {
                    if (!list.hasNext())
                    {
                        break label0;
                    }
                    obj = (Integer)list.next();
                    obj = (f)f.get(obj);
                } while (obj == null);
                obj = ((f) (obj)).e();
            } while (obj == null || !com.tencent.a.a.a.a.h.e(((c) (obj)).c));
            return ((c) (obj));
        }
        return new c();
    }

    public static g a(Context context)
    {
        com/tencent/a/a/a/a/g;
        JVM INSTR monitorenter ;
        if (i == null)
        {
            i = new g(context);
        }
        context = i;
        com/tencent/a/a/a/a/g;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public final void b(String s)
    {
        c c1 = f();
        c1.c = s;
        if (!com.tencent.a.a.a.a.h.d(c1.a))
        {
            c1.a = com.tencent.a.a.a.a.h.b(h);
        }
        if (!com.tencent.a.a.a.a.h.d(c1.b))
        {
            c1.b = com.tencent.a.a.a.a.h.c(h);
        }
        c1.d = System.currentTimeMillis();
        for (s = f.entrySet().iterator(); s.hasNext(); ((f)((java.util.Map.Entry)s.next()).getValue()).a(c1)) { }
    }

    public final c f()
    {
        return a(new ArrayList(Arrays.asList(new Integer[] {
            Integer.valueOf(1), Integer.valueOf(2), Integer.valueOf(4)
        })));
    }

}
