// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.common;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.tencent.open.utils.Global;
import com.tencent.tauth.IRequestListener;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.connect.common:
//            BaseApi

public class _cls1
    implements IRequestListener
{

    private final Handler mHandler;
    private final IUiListener mListener;
    final BaseApi this$0;

    public void onComplete(JSONObject jsonobject)
    {
        Message message = mHandler.obtainMessage();
        message.obj = jsonobject;
        message.what = 0;
        mHandler.sendMessage(message);
    }

    public void onConnectTimeoutException(ConnectTimeoutException connecttimeoutexception)
    {
        Message message = mHandler.obtainMessage();
        message.obj = connecttimeoutexception.getMessage();
        message.what = -7;
        mHandler.sendMessage(message);
    }

    public void onHttpStatusException(com.tencent.open.utils.n n)
    {
        Message message = mHandler.obtainMessage();
        message.obj = n.getMessage();
        message.what = -9;
        mHandler.sendMessage(message);
    }

    public void onIOException(IOException ioexception)
    {
        Message message = mHandler.obtainMessage();
        message.obj = ioexception.getMessage();
        message.what = -2;
        mHandler.sendMessage(message);
    }

    public void onJSONException(JSONException jsonexception)
    {
        Message message = mHandler.obtainMessage();
        message.obj = jsonexception.getMessage();
        message.what = -4;
        mHandler.sendMessage(message);
    }

    public void onMalformedURLException(MalformedURLException malformedurlexception)
    {
        Message message = mHandler.obtainMessage();
        message.obj = malformedurlexception.getMessage();
        message.what = -3;
        mHandler.sendMessage(message);
    }

    public void onNetworkUnavailableException(com.tencent.open.utils.Exception exception)
    {
        Message message = mHandler.obtainMessage();
        message.obj = exception.getMessage();
        message.what = -10;
        mHandler.sendMessage(message);
    }

    public void onSocketTimeoutException(SocketTimeoutException sockettimeoutexception)
    {
        Message message = mHandler.obtainMessage();
        message.obj = sockettimeoutexception.getMessage();
        message.what = -8;
        mHandler.sendMessage(message);
    }

    public void onUnknowException(Exception exception)
    {
        Message message = mHandler.obtainMessage();
        message.obj = exception.getMessage();
        message.what = -6;
        mHandler.sendMessage(message);
    }


    public _cls1.val.this._cls0(IUiListener iuilistener)
    {
        this$0 = BaseApi.this;
        super();
        mListener = iuilistener;
        class _cls1 extends Handler
        {

            final BaseApi.TempRequestListener this$1;
            final BaseApi val$this$0;

            public void handleMessage(Message message)
            {
                if (message.what == 0)
                {
                    mListener.onComplete(message.obj);
                    return;
                } else
                {
                    mListener.onError(new UiError(message.what, (String)message.obj, null));
                    return;
                }
            }

            _cls1(BaseApi baseapi)
            {
                this$1 = BaseApi.TempRequestListener.this;
                this$0 = baseapi;
                super(final_looper);
            }
        }

        mHandler = new _cls1(BaseApi.this);
    }
}
