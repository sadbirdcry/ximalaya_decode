// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.open.a.f;
import com.tencent.open.b.d;
import com.tencent.open.utils.TemporaryStorage;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONObject;

// Referenced classes of package com.tencent.connect.common:
//            BaseApi

public class AssistActivity extends Activity
{

    private static final String RESTART_FLAG = "RESTART_FLAG";
    private static final String TAG = "AssistActivity";
    public static boolean hackAuthActivity = false;
    public static boolean hackShareSend = false;
    public static boolean isQQMobileShare = false;
    private static BaseApi sApiObject;
    private BaseApi mAPiObject;

    public AssistActivity()
    {
    }

    public static Intent getAssistActivityIntent(Context context)
    {
        return new Intent(context, com/tencent/connect/common/AssistActivity);
    }

    private void openBrowser(Bundle bundle)
    {
        String s1 = bundle.getString("viaShareType");
        Object obj = bundle.getString("callbackAction");
        String s3 = bundle.getString("url");
        String s2 = bundle.getString("openId");
        String s4 = bundle.getString("appId");
        bundle = "";
        String s = "";
        if ("shareToQQ".equals(obj))
        {
            bundle = "ANDROIDQQ.SHARETOQQ.XX";
            s = "10";
        } else
        if ("shareToQzone".equals(obj))
        {
            bundle = "ANDROIDQQ.SHARETOQZ.XX";
            s = "11";
        }
        if (!Util.openBrowser(this, s3))
        {
            obj = (IUiListener)TemporaryStorage.get(((String) (obj)));
            if (obj != null)
            {
                ((IUiListener) (obj)).onError(new UiError(-6, "\u6253\u5F00\u6D4F\u89C8\u5668\u5931\u8D25!", null));
            }
            d.a().a(s2, s4, bundle, s, "3", "1", s1, "0", "2", "0");
            finish();
        } else
        {
            d.a().a(s2, s4, bundle, s, "3", "0", s1, "0", "2", "0");
        }
        getIntent().removeExtra("shareH5");
    }

    public static void setApiObject(BaseApi baseapi)
    {
        sApiObject = baseapi;
    }

    public static void setResultDataForLogin(Activity activity, Intent intent)
    {
        if (activity != null)
        {
label0:
            {
                if (intent == null)
                {
                    activity.setResult(10101, intent);
                    return;
                }
                try
                {
                    String s = intent.getStringExtra("key_response");
                    f.b("AssistActivity", (new StringBuilder()).append("AssistActivity--setResultDataForLogin-- ").append(s).toString());
                    if (!TextUtils.isEmpty(s))
                    {
                        Object obj = new JSONObject(s);
                        s = ((JSONObject) (obj)).optString("openid");
                        obj = ((JSONObject) (obj)).optString("access_token");
                        if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(((CharSequence) (obj))))
                        {
                            activity.setResult(10101, intent);
                            return;
                        }
                        break label0;
                    }
                }
                // Misplaced declaration of an exception variable
                catch (Activity activity)
                {
                    activity.printStackTrace();
                    return;
                }
            }
        }
        return;
        activity.setResult(12345, intent);
        return;
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        f.b("AssistActivity", (new StringBuilder()).append("AssistActivity--onActivityResult--").append(j).append(" data=").append(intent).toString());
        f.b("AssistActivity", (new StringBuilder()).append("--requestCode: ").append(i).append(" | resultCode: ").append(j).append(" | data: ").append(intent).toString());
        super.onActivityResult(i, j, intent);
        if (i == 0)
        {
            if (!isFinishing())
            {
                finish();
            }
            return;
        }
        if (intent != null)
        {
            intent.putExtra("key_action", "action_login");
        }
        if (mAPiObject != null)
        {
            f.b("AssistActivity", "AssistActivity--onActivityResult-- mAPiObject != null");
            mAPiObject.onActivityResult(this, i, j, intent);
        } else
        {
            f.b("AssistActivity", "AssistActivity--onActivityResult-- mAPiObject == null");
            setResultDataForLogin(this, intent);
        }
        finish();
    }

    protected void onCreate(Bundle bundle)
    {
        boolean flag = false;
        super.onCreate(bundle);
        requestWindowFeature(1);
        f.b("AssistActivity", "AssistActivity--onCreate--");
        if (sApiObject != null)
        {
            mAPiObject = sApiObject;
            sApiObject = null;
            Intent intent = mAPiObject.getActivityIntent();
            Bundle bundle1;
            int i;
            if (intent == null)
            {
                i = 0;
            } else
            {
                i = intent.getIntExtra("key_request_code", 0);
            }
            bundle1 = getIntent().getBundleExtra("h5_share_data");
            if (bundle != null)
            {
                flag = bundle.getBoolean("RESTART_FLAG");
            }
            if (!flag)
            {
                if (bundle1 == null)
                {
                    startActivityForResult(intent, i);
                    return;
                } else
                {
                    openBrowser(bundle1);
                    return;
                }
            }
        }
    }

    protected void onDestroy()
    {
        f.b("AssistActivity", "-->onDestroy");
        super.onDestroy();
    }

    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        Object obj = TemporaryStorage.get(intent.getStringExtra("action"));
        StringBuilder stringbuilder = (new StringBuilder()).append("AssistActivity--onNewIntent--");
        String s;
        if (obj == null)
        {
            s = "mAPiObject = null";
        } else
        {
            s = "mAPiObject != null";
        }
        f.b("AssistActivity", stringbuilder.append(s).toString());
        intent.putExtra("key_action", "action_share");
        if (obj != null)
        {
            BaseApi.handleDataToListener(intent, (IUiListener)obj);
        } else
        {
            setResult(-1, intent);
        }
        if (!isFinishing())
        {
            finish();
        }
    }

    protected void onPause()
    {
        f.b("AssistActivity", "-->onPause");
        super.onPause();
    }

    protected void onResume()
    {
        f.b("AssistActivity", "-->onResume");
        super.onResume();
        Intent intent = getIntent();
        if (!intent.getBooleanExtra("is_login", false))
        {
            if (!intent.getBooleanExtra("is_qq_mobile_share", false))
            {
                if (!hackShareSend && !isFinishing())
                {
                    finish();
                }
                hackShareSend = false;
            }
            if (hackAuthActivity)
            {
                hackAuthActivity = false;
                return;
            }
        }
    }

    protected void onSaveInstanceState(Bundle bundle)
    {
        f.b("AssistActivity", "AssistActivity--onSaveInstanceState--");
        bundle.putBoolean("RESTART_FLAG", true);
        super.onSaveInstanceState(bundle);
    }

    protected void onStart()
    {
        f.b("AssistActivity", "-->onStart");
        super.onStart();
    }

    protected void onStop()
    {
        f.b("AssistActivity", "-->onStop");
        super.onStop();
    }

}
