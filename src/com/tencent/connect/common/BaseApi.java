// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.tencent.connect.auth.QQAuth;
import com.tencent.connect.auth.QQToken;
import com.tencent.open.TDialog;
import com.tencent.open.a.f;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IRequestListener;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.connect.common:
//            AssistActivity

public abstract class BaseApi
{
    public class ApiTask
    {

        public IUiListener mListener;
        public int mRequestCode;
        final BaseApi this$0;

        public ApiTask(int i, IUiListener iuilistener)
        {
            this$0 = BaseApi.this;
            super();
            mRequestCode = i;
            mListener = iuilistener;
        }
    }

    public class TempRequestListener
        implements IRequestListener
    {

        private final Handler mHandler;
        private final IUiListener mListener;
        final BaseApi this$0;

        public void onComplete(JSONObject jsonobject)
        {
            Message message = mHandler.obtainMessage();
            message.obj = jsonobject;
            message.what = 0;
            mHandler.sendMessage(message);
        }

        public void onConnectTimeoutException(ConnectTimeoutException connecttimeoutexception)
        {
            Message message = mHandler.obtainMessage();
            message.obj = connecttimeoutexception.getMessage();
            message.what = -7;
            mHandler.sendMessage(message);
        }

        public void onHttpStatusException(com.tencent.open.utils.HttpUtils.HttpStatusException httpstatusexception)
        {
            Message message = mHandler.obtainMessage();
            message.obj = httpstatusexception.getMessage();
            message.what = -9;
            mHandler.sendMessage(message);
        }

        public void onIOException(IOException ioexception)
        {
            Message message = mHandler.obtainMessage();
            message.obj = ioexception.getMessage();
            message.what = -2;
            mHandler.sendMessage(message);
        }

        public void onJSONException(JSONException jsonexception)
        {
            Message message = mHandler.obtainMessage();
            message.obj = jsonexception.getMessage();
            message.what = -4;
            mHandler.sendMessage(message);
        }

        public void onMalformedURLException(MalformedURLException malformedurlexception)
        {
            Message message = mHandler.obtainMessage();
            message.obj = malformedurlexception.getMessage();
            message.what = -3;
            mHandler.sendMessage(message);
        }

        public void onNetworkUnavailableException(com.tencent.open.utils.HttpUtils.NetworkUnavailableException networkunavailableexception)
        {
            Message message = mHandler.obtainMessage();
            message.obj = networkunavailableexception.getMessage();
            message.what = -10;
            mHandler.sendMessage(message);
        }

        public void onSocketTimeoutException(SocketTimeoutException sockettimeoutexception)
        {
            Message message = mHandler.obtainMessage();
            message.obj = sockettimeoutexception.getMessage();
            message.what = -8;
            mHandler.sendMessage(message);
        }

        public void onUnknowException(Exception exception)
        {
            Message message = mHandler.obtainMessage();
            message.obj = exception.getMessage();
            message.what = -6;
            mHandler.sendMessage(message);
        }


        public TempRequestListener(IUiListener iuilistener)
        {
            this$0 = BaseApi.this;
            super();
            mListener = iuilistener;
            class _cls1 extends Handler
            {

                final TempRequestListener this$1;
                final BaseApi val$this$0;

                public void handleMessage(Message message)
                {
                    if (message.what == 0)
                    {
                        mListener.onComplete(message.obj);
                        return;
                    } else
                    {
                        mListener.onError(new UiError(message.what, (String)message.obj, null));
                        return;
                    }
                }

                _cls1(BaseApi baseapi)
                {
                    this$1 = TempRequestListener.this;
                    this$0 = baseapi;
                    super(final_looper);
                }
            }

            mHandler = new _cls1(BaseApi.this);
        }
    }


    protected static final String ACTION_CHECK_TOKEN = "action_check_token";
    protected static final String ACTIVITY_AGENT = "com.tencent.open.agent.AgentActivity";
    protected static final String ACTIVITY_ENCRY_TOKEN = "com.tencent.open.agent.EncryTokenActivity";
    protected static final String DEFAULT_PF = "openmobile_android";
    private static final String KEY_REQUEST_CODE = "key_request_code";
    private static final int MSG_COMPLETE = 0;
    protected static final String PARAM_ENCRY_EOKEN = "encry_token";
    protected static final String PLATFORM = "desktop_m_qq";
    protected static final String PREFERENCE_PF = "pfStore";
    private static final String TAG = com/tencent/connect/common/BaseApi.getName();
    protected static final String VERSION = "android";
    public static String businessId = null;
    public static String installChannel = null;
    public static boolean isOEM = false;
    public static String registerChannel = null;
    protected static int sRequestCode = 1000;
    protected Intent mActivityIntent;
    protected ProgressDialog mProgressDialog;
    protected QQAuth mQQAuth;
    protected List mTaskList;
    protected QQToken mToken;
    protected IUiListener mUiListener;

    public BaseApi(QQAuth qqauth, QQToken qqtoken)
    {
        mTaskList = null;
        mActivityIntent = null;
        mUiListener = null;
        mQQAuth = qqauth;
        mToken = qqtoken;
        mTaskList = new ArrayList();
    }

    public BaseApi(QQToken qqtoken)
    {
        this(null, qqtoken);
    }

    private Intent getAssitIntent(Activity activity)
    {
        activity = new Intent(activity.getApplicationContext(), com/tencent/connect/common/AssistActivity);
        activity.putExtra("is_login", true);
        return activity;
    }

    public static void handleDataToListener(Intent intent, IUiListener iuilistener)
    {
        if (intent == null)
        {
            iuilistener.onCancel();
        } else
        {
            Object obj = intent.getStringExtra("key_action");
            if ("action_login".equals(obj))
            {
                int i = intent.getIntExtra("key_error_code", 0);
                if (i == 0)
                {
                    intent = intent.getStringExtra("key_response");
                    if (intent != null)
                    {
                        try
                        {
                            iuilistener.onComplete(Util.parseJson(intent));
                            return;
                        }
                        // Misplaced declaration of an exception variable
                        catch (Object obj)
                        {
                            iuilistener.onError(new UiError(-4, "\u670D\u52A1\u5668\u8FD4\u56DE\u6570\u636E\u683C\u5F0F\u6709\u8BEF!", intent));
                        }
                        f.b("openSDK_LOG", "OpenUi, onActivityResult, json error", ((Throwable) (obj)));
                        return;
                    } else
                    {
                        f.b("openSDK_LOG", "OpenUi, onActivityResult, onComplete");
                        iuilistener.onComplete(new JSONObject());
                        return;
                    }
                } else
                {
                    f.e("openSDK_LOG", (new StringBuilder()).append("OpenUi, onActivityResult, onError = ").append(i).append("").toString());
                    iuilistener.onError(new UiError(i, intent.getStringExtra("key_error_msg"), intent.getStringExtra("key_error_detail")));
                    return;
                }
            }
            if ("action_share".equals(obj))
            {
                String s1 = intent.getStringExtra("result");
                String s = intent.getStringExtra("response");
                if ("cancel".equals(s1))
                {
                    iuilistener.onCancel();
                    return;
                }
                if ("error".equals(s1))
                {
                    iuilistener.onError(new UiError(-6, "unknown error", (new StringBuilder()).append(s).append("").toString()));
                    return;
                }
                if ("complete".equals(s1))
                {
                    if (s == null)
                    {
                        intent = "{\"ret\": 0}";
                    } else
                    {
                        intent = s;
                    }
                    try
                    {
                        iuilistener.onComplete(new JSONObject(intent));
                        return;
                    }
                    // Misplaced declaration of an exception variable
                    catch (Intent intent)
                    {
                        intent.printStackTrace();
                    }
                    iuilistener.onError(new UiError(-4, "json error", (new StringBuilder()).append(s).append("").toString()));
                    return;
                }
            }
        }
    }

    protected Bundle composeActivityParams()
    {
        Bundle bundle = new Bundle();
        bundle.putString("appid", mToken.getAppId());
        if (mToken.isSessionValid())
        {
            bundle.putString("keystr", mToken.getAccessToken());
            bundle.putString("keytype", "0x80");
        }
        Object obj = mToken.getOpenId();
        if (obj != null)
        {
            bundle.putString("hopenid", ((String) (obj)));
        }
        bundle.putString("platform", "androidqz");
        obj = Global.getContext().getSharedPreferences("pfStore", 0);
        if (isOEM)
        {
            bundle.putString("pf", (new StringBuilder()).append("desktop_m_qq-").append(installChannel).append("-").append("android").append("-").append(registerChannel).append("-").append(businessId).toString());
        } else
        {
            bundle.putString("pf", ((SharedPreferences) (obj)).getString("pf", "openmobile_android"));
            bundle.putString("pf", "openmobile_android");
        }
        bundle.putString("sdkv", "2.9.1.lite");
        bundle.putString("sdkp", "a");
        return bundle;
    }

    protected Bundle composeCGIParams()
    {
        Bundle bundle = new Bundle();
        bundle.putString("format", "json");
        bundle.putString("status_os", android.os.Build.VERSION.RELEASE);
        bundle.putString("status_machine", Build.MODEL);
        bundle.putString("status_version", android.os.Build.VERSION.SDK);
        bundle.putString("sdkv", "2.9.1.lite");
        bundle.putString("sdkp", "a");
        if (mToken != null && mToken.isSessionValid())
        {
            bundle.putString("access_token", mToken.getAccessToken());
            bundle.putString("oauth_consumer_key", mToken.getAppId());
            bundle.putString("openid", mToken.getOpenId());
            bundle.putString("appid_for_getting_config", mToken.getAppId());
        }
        SharedPreferences sharedpreferences = Global.getContext().getSharedPreferences("pfStore", 0);
        if (isOEM)
        {
            bundle.putString("pf", (new StringBuilder()).append("desktop_m_qq-").append(installChannel).append("-").append("android").append("-").append(registerChannel).append("-").append(businessId).toString());
            return bundle;
        } else
        {
            bundle.putString("pf", sharedpreferences.getString("pf", "openmobile_android"));
            return bundle;
        }
    }

    Intent getActivityIntent()
    {
        return mActivityIntent;
    }

    protected Intent getAgentIntent()
    {
        return getTargetActivityIntent("com.tencent.open.agent.AgentActivity");
    }

    protected Intent getAgentIntentWithTarget(String s)
    {
        Intent intent = new Intent();
        for (s = getTargetActivityIntent(s); s == null || s.getComponent() == null;)
        {
            return null;
        }

        intent.setClassName(s.getComponent().getPackageName(), "com.tencent.open.agent.AgentActivity");
        return intent;
    }

    protected String getCommonDownloadQQUrl(String s)
    {
        Bundle bundle = composeCGIParams();
        StringBuilder stringbuilder = new StringBuilder();
        if (!TextUtils.isEmpty(s))
        {
            bundle.putString("need_version", s);
        }
        stringbuilder.append("http://openmobile.qq.com/oauth2.0/m_jump_by_version?");
        stringbuilder.append(Util.encodeUrl(bundle));
        return stringbuilder.toString();
    }

    protected Intent getTargetActivityIntent(String s)
    {
        Intent intent = new Intent();
        intent.setClassName("com.tencent.mobileqq", s);
        if (SystemUtils.isActivityExist(Global.getContext(), intent))
        {
            return intent;
        } else
        {
            return null;
        }
    }

    protected void handleDownloadLastestQQ(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        iuilistener = new StringBuilder();
        iuilistener.append("http://qzs.qq.com/open/mobile/login/qzsjump.html?");
        iuilistener.append(Util.encodeUrl(bundle));
        (new TDialog(activity, "", iuilistener.toString(), null, mToken)).show();
    }

    protected boolean hasActivityForIntent()
    {
        if (mActivityIntent != null)
        {
            return SystemUtils.isActivityExist(Global.getContext(), mActivityIntent);
        } else
        {
            return false;
        }
    }

    public void onActivityResult(Activity activity, int i, int j, Intent intent)
    {
        Object obj = mTaskList.iterator();
_L4:
        if (!((Iterator) (obj)).hasNext()) goto _L2; else goto _L1
_L1:
        ApiTask apitask = (ApiTask)((Iterator) (obj)).next();
        if (apitask.mRequestCode != i) goto _L4; else goto _L3
_L3:
        obj = apitask.mListener;
        mTaskList.remove(apitask);
_L6:
        if (obj == null)
        {
            f.b(TAG, "BaseApi--onActivityResult-- listener == null");
            AssistActivity.setResultDataForLogin(activity, intent);
            return;
        }
        if (j == -1)
        {
            handleDataToListener(intent, ((IUiListener) (obj)));
        } else
        {
            f.b("openSDK_LOG", "OpenUi, onActivityResult, Constants.ACTIVITY_CANCEL");
            ((IUiListener) (obj)).onCancel();
        }
        f.b();
        return;
_L2:
        obj = null;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void releaseResource()
    {
    }

    protected void showProgressDialog(Context context, String s, String s1)
    {
        String s2 = s;
        if (TextUtils.isEmpty(s))
        {
            s2 = "\u8BF7\u7A0D\u5019";
        }
        s = s1;
        if (TextUtils.isEmpty(s1))
        {
            s = "\u6B63\u5728\u52A0\u8F7D...";
        }
        mProgressDialog = ProgressDialog.show(context, s2, s);
        mProgressDialog.setCancelable(true);
    }

    protected void startAssistActivity(Activity activity, int i)
    {
        AssistActivity.setApiObject(this);
        Intent intent = new Intent(activity.getApplicationContext(), com/tencent/connect/common/AssistActivity);
        AssistActivity.hackShareSend = true;
        if (AssistActivity.isQQMobileShare)
        {
            intent.putExtra("is_qq_mobile_share", true);
            AssistActivity.isQQMobileShare = false;
        }
        activity.startActivityForResult(intent, i);
    }

    protected void startAssistActivity(Activity activity, Bundle bundle, int i)
    {
        AssistActivity.setApiObject(this);
        Intent intent = new Intent(activity.getApplicationContext(), com/tencent/connect/common/AssistActivity);
        AssistActivity.hackShareSend = true;
        intent.putExtra("h5_share_data", bundle);
        activity.startActivityForResult(intent, i);
    }

    protected void startAssitActivity(Activity activity, IUiListener iuilistener)
    {
        AssistActivity.setApiObject(this);
        int i = sRequestCode;
        sRequestCode = i + 1;
        mActivityIntent.putExtra("key_request_code", i);
        mTaskList.add(new ApiTask(i, iuilistener));
        activity.startActivityForResult(getAssitIntent(activity), 10100);
    }

    protected void startAssitActivity(Fragment fragment, IUiListener iuilistener)
    {
        AssistActivity.setApiObject(this);
        int i = sRequestCode;
        sRequestCode = i + 1;
        mActivityIntent.putExtra("key_request_code", i);
        mTaskList.add(new ApiTask(i, iuilistener));
        fragment.startActivityForResult(getAssitIntent(fragment.getActivity()), 10100);
    }

}
