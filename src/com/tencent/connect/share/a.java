// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.share;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.text.TextUtils;
import com.tencent.open.a.f;
import com.tencent.open.utils.AsynLoadImgBack;
import com.tencent.open.utils.Util;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class a
{

    public static final int a(android.graphics.BitmapFactory.Options options, int i, int j)
    {
        int k = b(options, i, j);
        if (k <= 8)
        {
            i = 1;
            do
            {
                j = i;
                if (i >= k)
                {
                    break;
                }
                i <<= 1;
            } while (true);
        } else
        {
            j = ((k + 7) / 8) * 8;
        }
        return j;
    }

    private static Bitmap a(Bitmap bitmap, int i)
    {
        Matrix matrix = new Matrix();
        int j = bitmap.getWidth();
        int k = bitmap.getHeight();
        float f1;
        if (j <= k)
        {
            j = k;
        }
        f1 = (float)i / (float)j;
        matrix.postScale(f1, f1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static final Bitmap a(String s, int i)
    {
        Object obj;
        if (TextUtils.isEmpty(s))
        {
            obj = null;
        } else
        {
            obj = new android.graphics.BitmapFactory.Options();
            obj.inJustDecodeBounds = true;
            int j;
            int k;
            try
            {
                BitmapFactory.decodeFile(s, ((android.graphics.BitmapFactory.Options) (obj)));
            }
            catch (OutOfMemoryError outofmemoryerror)
            {
                outofmemoryerror.printStackTrace();
            }
            j = ((android.graphics.BitmapFactory.Options) (obj)).outWidth;
            k = ((android.graphics.BitmapFactory.Options) (obj)).outHeight;
            if (((android.graphics.BitmapFactory.Options) (obj)).mCancel || ((android.graphics.BitmapFactory.Options) (obj)).outWidth == -1 || ((android.graphics.BitmapFactory.Options) (obj)).outHeight == -1)
            {
                return null;
            }
            if (j <= k)
            {
                j = k;
            }
            obj.inPreferredConfig = android.graphics.Bitmap.Config.RGB_565;
            if (j > i)
            {
                obj.inSampleSize = a(((android.graphics.BitmapFactory.Options) (obj)), -1, i * i);
            }
            obj.inJustDecodeBounds = false;
            try
            {
                s = BitmapFactory.decodeFile(s, ((android.graphics.BitmapFactory.Options) (obj)));
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = null;
            }
            if (s == null)
            {
                return null;
            }
            j = ((android.graphics.BitmapFactory.Options) (obj)).outWidth;
            k = ((android.graphics.BitmapFactory.Options) (obj)).outHeight;
            if (j <= k)
            {
                j = k;
            }
            obj = s;
            if (j > i)
            {
                return a(((Bitmap) (s)), i);
            }
        }
        return ((Bitmap) (obj));
    }

    protected static final String a(Bitmap bitmap, String s, String s1)
    {
        File file = new File(s);
        if (!file.exists())
        {
            file.mkdirs();
        }
        s = s + s1;
        s1 = new File(s);
        if (s1.exists())
        {
            s1.delete();
        }
        if (bitmap == null)
        {
            break MISSING_BLOCK_LABEL_101;
        }
        s1 = new FileOutputStream(s1);
        bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 80, s1);
        s1.flush();
        s1.close();
        bitmap.recycle();
        return s;
        bitmap;
        bitmap.printStackTrace();
_L2:
        return null;
        bitmap;
        bitmap.printStackTrace();
        if (true) goto _L2; else goto _L1
_L1:
    }

    public static final void a(Context context, String s, AsynLoadImgBack asynloadimgback)
    {
        f.b("AsynScaleCompressImage", "scaleCompressImage");
        if (TextUtils.isEmpty(s))
        {
            asynloadimgback.saved(1, null);
            return;
        }
        if (!Util.hasSDCard())
        {
            asynloadimgback.saved(2, null);
            return;
        } else
        {
            (new Thread(new _cls2(s, new _cls1(context.getMainLooper(), asynloadimgback)))).start();
            return;
        }
    }

    public static final void a(Context context, ArrayList arraylist, AsynLoadImgBack asynloadimgback)
    {
        f.b("AsynScaleCompressImage", "batchScaleCompressImage");
        if (arraylist == null)
        {
            asynloadimgback.saved(1, null);
            return;
        } else
        {
            (new Thread(new _cls4(arraylist, new _cls3(context.getMainLooper(), asynloadimgback)))).start();
            return;
        }
    }

    static boolean a(String s, int i, int j)
    {
        return b(s, i, j);
    }

    private static int b(android.graphics.BitmapFactory.Options options, int i, int j)
    {
        double d = options.outWidth;
        double d1 = options.outHeight;
        int k;
        int l;
        if (j == -1)
        {
            k = 1;
        } else
        {
            k = (int)Math.ceil(Math.sqrt((d * d1) / (double)j));
        }
        if (i == -1)
        {
            l = 128;
        } else
        {
            l = (int)Math.min(Math.floor(d / (double)i), Math.floor(d1 / (double)i));
        }
        if (l >= k)
        {
            if (j == -1 && i == -1)
            {
                return 1;
            }
            if (i != -1)
            {
                return l;
            }
        }
        return k;
    }

    private static final boolean b(String s, int i, int j)
    {
        if (TextUtils.isEmpty(s))
        {
            return false;
        }
        android.graphics.BitmapFactory.Options options = new android.graphics.BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        int k;
        int l;
        try
        {
            BitmapFactory.decodeFile(s, options);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
        k = options.outWidth;
        l = options.outHeight;
        if (options.mCancel || options.outWidth == -1 || options.outHeight == -1)
        {
            return false;
        }
        int i1;
        if (k > l)
        {
            i1 = k;
        } else
        {
            i1 = l;
        }
        if (k >= l)
        {
            k = l;
        }
        f.b("AsynScaleCompressImage", (new StringBuilder()).append("longSide=").append(i1).append("shortSide=").append(k).toString());
        options.inPreferredConfig = android.graphics.Bitmap.Config.RGB_565;
        return i1 > j || k > i;
    }

    private class _cls2
        implements Runnable
    {

        final String a;
        final Handler b;

        public void run()
        {
            Object obj = a.a(a, 140);
            if (obj != null)
            {
                Object obj1 = (new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/tmp/").toString();
                String s = Util.encrypt(a);
                s = (new StringBuilder()).append("share2qq_temp").append(s).append(".jpg").toString();
                if (!a.a(a, 140, 140))
                {
                    f.b("AsynScaleCompressImage", "not out of bound,not compress!");
                    obj = a;
                } else
                {
                    f.b("AsynScaleCompressImage", "out of bound,compress!");
                    obj = a.a(((Bitmap) (obj)), ((String) (obj1)), s);
                }
                f.b("AsynScaleCompressImage", (new StringBuilder()).append("-->destFilePath: ").append(((String) (obj))).toString());
                if (obj != null)
                {
                    obj1 = b.obtainMessage(101);
                    obj1.obj = obj;
                    b.sendMessage(((Message) (obj1)));
                    return;
                }
            }
            obj = b.obtainMessage(102);
            obj.arg1 = 3;
            b.sendMessage(((Message) (obj)));
        }

        _cls2(String s, Handler handler)
        {
            a = s;
            b = handler;
            super();
        }
    }


    private class _cls1 extends Handler
    {

        final AsynLoadImgBack a;

        public void handleMessage(Message message)
        {
            int i;
            switch (message.what)
            {
            default:
                super.handleMessage(message);
                return;

            case 101: // 'e'
                message = (String)message.obj;
                a.saved(0, message);
                return;

            case 102: // 'f'
                i = message.arg1;
                break;
            }
            a.saved(i, null);
        }

        _cls1(Looper looper, AsynLoadImgBack asynloadimgback)
        {
            a = asynloadimgback;
            super(looper);
        }
    }


    private class _cls4
        implements Runnable
    {

        final ArrayList a;
        final Handler b;

        public void run()
        {
            int i = 0;
            while (i < a.size()) 
            {
                String s = (String)a.get(i);
                if (Util.isValidUrl(s) || !Util.fileExists(s))
                {
                    continue;
                }
                Bitmap bitmap = a.a(s, 10000);
                if (bitmap == null)
                {
                    continue;
                }
                String s1 = (new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/tmp/").toString();
                String s2 = Util.encrypt(s);
                s2 = (new StringBuilder()).append("share2qzone_temp").append(s2).append(".jpg").toString();
                if (!a.a(s, 640, 10000))
                {
                    f.b("AsynScaleCompressImage", "not out of bound,not compress!");
                } else
                {
                    f.b("AsynScaleCompressImage", "out of bound, compress!");
                    s = a.a(bitmap, s1, s2);
                }
                if (s != null)
                {
                    a.set(i, s);
                }
                i++;
            }
            Message message = b.obtainMessage(101);
            Bundle bundle = new Bundle();
            bundle.putStringArrayList("images", a);
            message.setData(bundle);
            b.sendMessage(message);
        }

        _cls4(ArrayList arraylist, Handler handler)
        {
            a = arraylist;
            b = handler;
            super();
        }
    }


    private class _cls3 extends Handler
    {

        final AsynLoadImgBack a;

        public void handleMessage(Message message)
        {
            switch (message.what)
            {
            default:
                super.handleMessage(message);
                return;

            case 101: // 'e'
                message = message.getData().getStringArrayList("images");
                break;
            }
            a.batchSaved(0, message);
        }

        _cls3(Looper looper, AsynLoadImgBack asynloadimgback)
        {
            a = asynloadimgback;
            super(looper);
        }
    }

}
