// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.share;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Base64;
import com.tencent.connect.a.a;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.open.TDialog;
import com.tencent.open.a.f;
import com.tencent.open.b.d;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.TemporaryStorage;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.net.URLEncoder;
import java.util.ArrayList;

// Referenced classes of package com.tencent.connect.share:
//            a, QQShare

public class QzoneShare extends BaseApi
{

    public static final String SHARE_TO_QQ_APP_NAME = "appName";
    public static final String SHARE_TO_QQ_AUDIO_URL = "audio_url";
    public static final String SHARE_TO_QQ_EXT_INT = "cflag";
    public static final String SHARE_TO_QQ_EXT_STR = "share_qq_ext_str";
    public static final String SHARE_TO_QQ_IMAGE_LOCAL_URL = "imageLocalUrl";
    public static final String SHARE_TO_QQ_IMAGE_URL = "imageUrl";
    public static final String SHARE_TO_QQ_SITE = "site";
    public static final String SHARE_TO_QQ_SUMMARY = "summary";
    public static final String SHARE_TO_QQ_TARGET_URL = "targetUrl";
    public static final String SHARE_TO_QQ_TITLE = "title";
    public static final String SHARE_TO_QZONE_KEY_TYPE = "req_type";
    public static final int SHARE_TO_QZONE_TYPE_APP = 6;
    public static final int SHARE_TO_QZONE_TYPE_IMAGE = 5;
    public static final int SHARE_TO_QZONE_TYPE_IMAGE_TEXT = 1;
    public static final int SHARE_TO_QZONE_TYPE_NO_TYPE = 0;
    private boolean a;
    private boolean b;
    private boolean c;
    private boolean d;
    public String mViaShareQzoneType;

    public QzoneShare(Context context, QQToken qqtoken)
    {
        super(qqtoken);
        mViaShareQzoneType = "";
        a = true;
        b = false;
        c = false;
        d = false;
    }

    private void a(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        f.c("openSDK_LOG", "doshareToQzone() --start");
        StringBuffer stringbuffer = new StringBuffer("mqqapi://share/to_qzone?src_type=app&version=1&file_type=news");
        ArrayList arraylist = bundle.getStringArrayList("imageUrl");
        String s = bundle.getString("title");
        String s1 = bundle.getString("summary");
        String s2 = bundle.getString("targetUrl");
        String s3 = bundle.getString("audio_url");
        int k = bundle.getInt("req_type", 1);
        String s4 = bundle.getString("appName");
        int l = bundle.getInt("cflag", 0);
        bundle = bundle.getString("share_qq_ext_str");
        String s5 = mToken.getAppId();
        String s6 = mToken.getOpenId();
        f.b("doshareToQzone", (new StringBuilder()).append("openId:").append(s6).toString());
        if (arraylist != null)
        {
            StringBuffer stringbuffer1 = new StringBuffer();
            int i;
            int j;
            if (arraylist.size() > 9)
            {
                i = 9;
            } else
            {
                i = arraylist.size();
            }
            for (j = 0; j < i; j++)
            {
                stringbuffer1.append(URLEncoder.encode((String)arraylist.get(j)));
                if (j != i - 1)
                {
                    stringbuffer1.append(";");
                }
            }

            stringbuffer.append((new StringBuilder()).append("&image_url=").append(Base64.encodeToString(Util.getBytesUTF8(stringbuffer1.toString()), 2)).toString());
        }
        if (!TextUtils.isEmpty(s))
        {
            stringbuffer.append((new StringBuilder()).append("&title=").append(Base64.encodeToString(Util.getBytesUTF8(s), 2)).toString());
        }
        if (!TextUtils.isEmpty(s1))
        {
            stringbuffer.append((new StringBuilder()).append("&description=").append(Base64.encodeToString(Util.getBytesUTF8(s1), 2)).toString());
        }
        if (!TextUtils.isEmpty(s5))
        {
            stringbuffer.append((new StringBuilder()).append("&share_id=").append(s5).toString());
        }
        if (!TextUtils.isEmpty(s2))
        {
            stringbuffer.append((new StringBuilder()).append("&url=").append(Base64.encodeToString(Util.getBytesUTF8(s2), 2)).toString());
        }
        if (!TextUtils.isEmpty(s4))
        {
            stringbuffer.append((new StringBuilder()).append("&app_name=").append(Base64.encodeToString(Util.getBytesUTF8(s4), 2)).toString());
        }
        if (!Util.isEmpty(s6))
        {
            stringbuffer.append((new StringBuilder()).append("&open_id=").append(Base64.encodeToString(Util.getBytesUTF8(s6), 2)).toString());
        }
        if (!Util.isEmpty(s3))
        {
            stringbuffer.append((new StringBuilder()).append("&audioUrl=").append(Base64.encodeToString(Util.getBytesUTF8(s3), 2)).toString());
        }
        stringbuffer.append((new StringBuilder()).append("&req_type=").append(Base64.encodeToString(Util.getBytesUTF8(String.valueOf(k)), 2)).toString());
        if (!Util.isEmpty(bundle))
        {
            stringbuffer.append((new StringBuilder()).append("&share_qq_ext_str=").append(Base64.encodeToString(Util.getBytesUTF8(bundle), 2)).toString());
        }
        stringbuffer.append((new StringBuilder()).append("&cflag=").append(Base64.encodeToString(Util.getBytesUTF8(String.valueOf(l)), 2)).toString());
        f.b("doshareToQzone, url: ", stringbuffer.toString());
        com.tencent.connect.a.a.a(Global.getContext(), mToken, "requireApi", new String[] {
            "shareToNativeQQ"
        });
        mActivityIntent = new Intent("android.intent.action.VIEW");
        mActivityIntent.setData(Uri.parse(stringbuffer.toString()));
        mActivityIntent.putExtra("pkg_name", activity.getPackageName());
        if (SystemUtils.compareQQVersion(activity, "4.6.0") < 0)
        {
            if (hasActivityForIntent())
            {
                startAssitActivity(activity, iuilistener);
            }
            f.c("openSDK_LOG", "doShareToQzone() -- QQ Version is < 4.6.0");
        } else
        {
            f.c("openSDK_LOG", "doShareToQzone() -- QQ Version is > 4.6.0");
            if (TemporaryStorage.set("shareToQzone", iuilistener) != null)
            {
                f.c("openSDK_LOG", "doShareToQzone() -- do listener onCancel()");
            }
            if (hasActivityForIntent())
            {
                startAssistActivity(activity, 10104);
            }
        }
        if (hasActivityForIntent())
        {
            com.tencent.open.b.d.a().a(mToken.getOpenId(), mToken.getAppId(), "ANDROIDQQ.SHARETOQZ.XX", "11", "3", "0", mViaShareQzoneType, "0", "1", "0");
            com.tencent.open.b.d.a().a(0, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "");
        } else
        {
            com.tencent.open.b.d.a().a(mToken.getOpenId(), mToken.getAppId(), "ANDROIDQQ.SHARETOQZ.XX", "11", "3", "1", mViaShareQzoneType, "0", "1", "0");
            com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "hasActivityForIntent fail");
        }
        f.c("openSDK_LOG", "doShareToQzone() --end");
    }

    static void a(QzoneShare qzoneshare, Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        qzoneshare.a(activity, bundle, iuilistener);
    }

    public void onActivityResult(Activity activity, int i, int j, Intent intent)
    {
    }

    public void releaseResource()
    {
        TemporaryStorage.remove("shareToQzone");
    }

    public void shareToQzone(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        String s2;
        String s4;
        String s5;
        ArrayList arraylist;
        String s6;
        f.c("openSDK_LOG", "shareToQzone() -- start");
        if (bundle == null)
        {
            iuilistener.onError(new UiError(-6, "\u4F20\u5165\u53C2\u6570\u4E0D\u53EF\u4EE5\u4E3A\u7A7A", null));
            com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "\u4F20\u5165\u53C2\u6570\u4E0D\u53EF\u4EE5\u4E3A\u7A7A");
            return;
        }
        s2 = bundle.getString("title");
        s6 = bundle.getString("summary");
        s4 = bundle.getString("targetUrl");
        arraylist = bundle.getStringArrayList("imageUrl");
        s5 = Util.getApplicationLable(activity);
        if (s5 != null) goto _L2; else goto _L1
_L1:
        String s = bundle.getString("appName");
_L20:
        int j = bundle.getInt("req_type");
        j;
        JVM INSTR tableswitch 1 6: default 168
    //                   1 412
    //                   2 168
    //                   3 168
    //                   4 168
    //                   5 422
    //                   6 402;
           goto _L3 _L4 _L3 _L3 _L3 _L5 _L6
_L3:
        mViaShareQzoneType = "1";
_L13:
        j;
        JVM INSTR tableswitch 1 6: default 216
    //                   1 552
    //                   2 216
    //                   3 216
    //                   4 216
    //                   5 587
    //                   6 432;
           goto _L7 _L8 _L7 _L7 _L7 _L9 _L10
_L10:
        break; /* Loop/switch isn't completed */
_L7:
        if (Util.isEmpty(s2) && Util.isEmpty(s6))
        {
            if (arraylist != null && arraylist.size() != 0)
            {
                a = false;
            } else
            {
                s2 = (new StringBuilder()).append("\u6765\u81EA").append(s).append("\u7684\u5206\u4EAB").toString();
                a = true;
            }
        } else
        {
            a = true;
        }
        b = false;
        c = true;
        d = false;
        s5 = s4;
        s4 = s2;
        s2 = s5;
        if (!Util.hasSDCard() && SystemUtils.compareQQVersion(activity, "4.5.0") < 0)
        {
            iuilistener.onError(new UiError(-6, "\u5206\u4EAB\u56FE\u7247\u5931\u8D25\uFF0C\u68C0\u6D4B\u4E0D\u5230SD\u5361!", null));
            f.e("openSDK_LOG", "shareToQzone() sdcard is null--end");
            com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "\u5206\u4EAB\u56FE\u7247\u5931\u8D25\uFF0C\u68C0\u6D4B\u4E0D\u5230SD\u5361!");
            return;
        }
        if (a)
        {
            if (TextUtils.isEmpty(s2))
            {
                iuilistener.onError(new UiError(-5, "targetUrl\u4E3A\u5FC5\u586B\u9879\uFF0C\u8BF7\u8865\u5145\u540E\u5206\u4EAB", null));
                f.e("openSDK_LOG", "shareToQzone() targetUrl null error--end");
                com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "targetUrl\u4E3A\u5FC5\u586B\u9879\uFF0C\u8BF7\u8865\u5145\u540E\u5206\u4EAB");
                return;
            }
            if (!Util.isValidUrl(s2))
            {
                iuilistener.onError(new UiError(-5, "targetUrl\u6709\u8BEF", null));
                f.e("openSDK_LOG", "shareToQzone() targetUrl error--end");
                com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "targetUrl\u6709\u8BEF");
                return;
            }
        }
          goto _L11
_L2:
        s = s5;
        if (s5.length() > 20)
        {
            s = (new StringBuilder()).append(s5.substring(0, 20)).append("...").toString();
        }
        continue; /* Loop/switch isn't completed */
_L6:
        mViaShareQzoneType = "4";
        continue; /* Loop/switch isn't completed */
_L4:
        mViaShareQzoneType = "1";
        continue; /* Loop/switch isn't completed */
_L5:
        mViaShareQzoneType = "2";
        if (true) goto _L13; else goto _L12
_L12:
        if (SystemUtils.compareQQVersion(activity, "5.0.0") < 0)
        {
            iuilistener.onError(new UiError(-15, "\u624BQ\u7248\u672C\u8FC7\u4F4E\uFF0C\u5E94\u7528\u5206\u4EAB\u53EA\u652F\u6301\u624BQ5.0\u53CA\u5176\u4EE5\u4E0A\u7248\u672C", null));
            f.b("openSDK_LOG", "-->shareToQzone, app share is not support below qq5.0.");
            com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone, app share is not support below qq5.0.");
            return;
        }
        s5 = String.format("http://fusion.qq.com/cgi-bin/qzapps/unified_jump?appid=%1$s&from=%2$s&isOpenAppID=1", new Object[] {
            mToken.getAppId(), "mqq"
        });
        bundle.putString("targetUrl", s5);
        s4 = s2;
        s2 = s5;
        break MISSING_BLOCK_LABEL_277;
_L8:
        a = true;
        b = false;
        c = true;
        d = false;
        s5 = s2;
        s2 = s4;
        s4 = s5;
        break MISSING_BLOCK_LABEL_277;
_L9:
        iuilistener.onError(new UiError(-5, "\u6682\u4E0D\u652F\u6301\u7EAF\u56FE\u7247\u5206\u4EAB\u5230\u7A7A\u95F4\uFF0C\u5EFA\u8BAE\u4F7F\u7528\u56FE\u6587\u5206\u4EAB", null));
        f.e("openSDK_LOG", "shareToQzone() error--end\u6682\u4E0D\u652F\u6301\u7EAF\u56FE\u7247\u5206\u4EAB\u5230\u7A7A\u95F4\uFF0C\u5EFA\u8BAE\u4F7F\u7528\u56FE\u6587\u5206\u4EAB");
        com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone() \u6682\u4E0D\u652F\u6301\u7EAF\u56FE\u7247\u5206\u4EAB\u5230\u7A7A\u95F4\uFF0C\u5EFA\u8BAE\u4F7F\u7528\u56FE\u6587\u5206\u4EAB");
        return;
_L11:
        if (!b) goto _L15; else goto _L14
_L14:
        bundle.putString("title", "");
        bundle.putString("summary", "");
_L18:
        if (!TextUtils.isEmpty(s))
        {
            bundle.putString("appName", s);
        }
        if (arraylist != null && (arraylist == null || arraylist.size() != 0))
        {
            break; /* Loop/switch isn't completed */
        }
        if (d)
        {
            iuilistener.onError(new UiError(-6, "\u7EAF\u56FE\u5206\u4EAB\uFF0CimageUrl \u4E0D\u80FD\u4E3A\u7A7A", null));
            f.e("openSDK_LOG", "shareToQzone() imageUrl is null -- end");
            com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone() imageUrl is null");
            return;
        }
          goto _L16
_L15:
        if (c && Util.isEmpty(s4))
        {
            iuilistener.onError(new UiError(-6, "title\u4E0D\u80FD\u4E3A\u7A7A!", null));
            f.e("openSDK_LOG", "shareToQzone() title is null--end");
            com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone() title is null");
            return;
        }
        if (!Util.isEmpty(s4) && s4.length() > 200)
        {
            bundle.putString("title", Util.subString(s4, 200, null, null));
        }
        if (!Util.isEmpty(s6) && s6.length() > 600)
        {
            bundle.putString("summary", Util.subString(s6, 600, null, null));
        }
        if (true) goto _L18; else goto _L17
_L17:
        for (int i = 0; i < arraylist.size(); i++)
        {
            String s1 = (String)arraylist.get(i);
            if (!Util.isValidUrl(s1) && !Util.isValidPath(s1))
            {
                arraylist.remove(i);
            }
        }

        if (arraylist.size() == 0)
        {
            iuilistener.onError(new UiError(-6, "\u975E\u6CD5\u7684\u56FE\u7247\u5730\u5740!", null));
            f.e("openSDK_LOG", "shareToQzone() MSG_PARAM_IMAGE_URL_FORMAT_ERROR--end");
            com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone() \u975E\u6CD5\u7684\u56FE\u7247\u5730\u5740!");
            return;
        }
        bundle.putStringArrayList("imageUrl", arraylist);
_L16:
        if (SystemUtils.compareQQVersion(activity, "4.6.0") >= 0)
        {
            com.tencent.connect.share.a.a(activity, arraylist, new _cls1(bundle, activity, iuilistener));
        } else
        if (SystemUtils.compareQQVersion(activity, "4.2.0") >= 0 && SystemUtils.compareQQVersion(activity, "4.6.0") < 0)
        {
            QQShare qqshare = new QQShare(activity, mToken);
            if (arraylist != null && arraylist.size() > 0)
            {
                String s3 = (String)arraylist.get(0);
                if (j == 5 && !Util.fileExists(s3))
                {
                    iuilistener.onError(new UiError(-6, "\u624BQ\u7248\u672C\u8FC7\u4F4E\uFF0C\u7EAF\u56FE\u5206\u4EAB\u4E0D\u652F\u6301\u7F51\u8DEF\u56FE\u7247", null));
                    f.e("openSDK_LOG", "shareToQzone()\u624BQ\u7248\u672C\u8FC7\u4F4E\uFF0C\u7EAF\u56FE\u5206\u4EAB\u4E0D\u652F\u6301\u7F51\u8DEF\u56FE\u7247");
                    com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone()\u624BQ\u7248\u672C\u8FC7\u4F4E\uFF0C\u7EAF\u56FE\u5206\u4EAB\u4E0D\u652F\u6301\u7F51\u8DEF\u56FE\u7247");
                    return;
                }
                bundle.putString("imageLocalUrl", s3);
            }
            if (SystemUtils.compareQQVersion(activity, "4.5.0") >= 0)
            {
                bundle.putInt("cflag", 1);
            }
            qqshare.shareToQQ(activity, bundle, iuilistener);
        } else
        {
            (new TDialog(activity, "", getCommonDownloadQQUrl(""), null, mToken)).show();
        }
        f.c("openSDK_LOG", "shareToQzone() --end");
        return;
        if (true) goto _L20; else goto _L19
_L19:
    }

    private class _cls1
        implements AsynLoadImgBack
    {

        final Bundle a;
        final Activity b;
        final IUiListener c;
        final QzoneShare d;

        public void batchSaved(int i, ArrayList arraylist)
        {
            if (i == 0)
            {
                a.putStringArrayList("imageUrl", arraylist);
            }
            com.tencent.connect.share.QzoneShare.a(d, b, a, c);
        }

        public void saved(int i, String s)
        {
        }

        _cls1(Bundle bundle, Activity activity, IUiListener iuilistener)
        {
            d = QzoneShare.this;
            a = bundle;
            b = activity;
            c = iuilistener;
            super();
        }
    }

}
