// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.share;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import com.tencent.connect.auth.QQToken;
import com.tencent.open.a.f;
import com.tencent.open.b.d;
import com.tencent.open.utils.AsynLoadImgBack;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.util.ArrayList;

// Referenced classes of package com.tencent.connect.share:
//            QQShare

class e
    implements AsynLoadImgBack
{

    final Bundle a;
    final String b;
    final String c;
    final IUiListener d;
    final Activity e;
    final QQShare f;

    public void batchSaved(int i, ArrayList arraylist)
    {
    }

    public void saved(int i, String s)
    {
        if (i == 0)
        {
            a.putString("imageLocalUrl", s);
        } else
        if (TextUtils.isEmpty(b) && TextUtils.isEmpty(c))
        {
            if (d != null)
            {
                d.onError(new UiError(-6, "\u83B7\u53D6\u5206\u4EAB\u56FE\u7247\u5931\u8D25!", null));
                com.tencent.open.a.f.e("openSDK_LOG.QQShare", "shareToMobileQQ -- error: \u83B7\u53D6\u5206\u4EAB\u56FE\u7247\u5931\u8D25!");
            }
            com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", QQShare.b(f).getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "\u83B7\u53D6\u5206\u4EAB\u56FE\u7247\u5931\u8D25!");
            return;
        }
        QQShare.a(f, e, a, d);
    }

    ck(QQShare qqshare, Bundle bundle, String s, String s1, IUiListener iuilistener, Activity activity)
    {
        f = qqshare;
        a = bundle;
        b = s;
        c = s1;
        d = iuilistener;
        e = activity;
        super();
    }
}
