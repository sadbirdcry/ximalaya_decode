// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.share;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Base64;
import com.tencent.connect.a.a;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.AssistActivity;
import com.tencent.connect.common.BaseApi;
import com.tencent.open.TDialog;
import com.tencent.open.a.f;
import com.tencent.open.b.d;
import com.tencent.open.utils.AsynLoadImg;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.TemporaryStorage;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.File;

// Referenced classes of package com.tencent.connect.share:
//            a

public class QQShare extends BaseApi
{

    public static final int QQ_SHARE_SUMMARY_MAX_LENGTH = 60;
    public static final int QQ_SHARE_TITLE_MAX_LENGTH = 45;
    public static final String SHARE_TO_QQ_APP_NAME = "appName";
    public static final String SHARE_TO_QQ_AUDIO_URL = "audio_url";
    public static final String SHARE_TO_QQ_EXT_INT = "cflag";
    public static final String SHARE_TO_QQ_EXT_STR = "share_qq_ext_str";
    public static final int SHARE_TO_QQ_FLAG_QZONE_AUTO_OPEN = 1;
    public static final int SHARE_TO_QQ_FLAG_QZONE_ITEM_HIDE = 2;
    public static final String SHARE_TO_QQ_IMAGE_LOCAL_URL = "imageLocalUrl";
    public static final String SHARE_TO_QQ_IMAGE_URL = "imageUrl";
    public static final String SHARE_TO_QQ_KEY_TYPE = "req_type";
    public static final String SHARE_TO_QQ_SITE = "site";
    public static final String SHARE_TO_QQ_SUMMARY = "summary";
    public static final String SHARE_TO_QQ_TARGET_URL = "targetUrl";
    public static final String SHARE_TO_QQ_TITLE = "title";
    public static final int SHARE_TO_QQ_TYPE_APP = 6;
    public static final int SHARE_TO_QQ_TYPE_AUDIO = 2;
    public static final int SHARE_TO_QQ_TYPE_DEFAULT = 1;
    public static final int SHARE_TO_QQ_TYPE_IMAGE = 5;
    public String mViaShareQQType;

    public QQShare(Context context, QQToken qqtoken)
    {
        super(qqtoken);
        mViaShareQQType = "";
    }

    static QQToken a(QQShare qqshare)
    {
        return qqshare.mToken;
    }

    private void a(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        f.c("openSDK_LOG", "shareToMobileQQ() -- start.");
        String s = bundle.getString("imageUrl");
        String s1 = bundle.getString("title");
        String s2 = bundle.getString("summary");
        f.b("openSDK_LOG.QQShare", (new StringBuilder()).append("shareToMobileQQ -- imageUrl: ").append(s).toString());
        if (!TextUtils.isEmpty(s))
        {
            if (Util.isValidUrl(s))
            {
                if (TextUtils.isEmpty(s1) && TextUtils.isEmpty(s2))
                {
                    if (iuilistener != null)
                    {
                        iuilistener.onError(new UiError(-6, "\u5206\u4EAB\u56FE\u7247\u5931\u8D25\uFF0C\u68C0\u6D4B\u4E0D\u5230SD\u5361!", null));
                        f.e("openSDK_LOG.QQShare", "\u5206\u4EAB\u56FE\u7247\u5931\u8D25\uFF0C\u68C0\u6D4B\u4E0D\u5230SD\u5361!");
                    }
                    d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "\u5206\u4EAB\u56FE\u7247\u5931\u8D25\uFF0C\u68C0\u6D4B\u4E0D\u5230SD\u5361!");
                    return;
                }
                if (SystemUtils.compareQQVersion(activity, "4.3.0") >= 0)
                {
                    b(activity, bundle, iuilistener);
                } else
                {
                    (new AsynLoadImg(activity)).save(s, new _cls1(bundle, s1, s2, iuilistener, activity));
                }
            } else
            {
                bundle.putString("imageUrl", null);
                if (SystemUtils.compareQQVersion(activity, "4.3.0") < 0)
                {
                    f.b("openSDK_LOG.QQShare", "shareToMobileQQ -- QQ Version is < 4.3.0 ");
                    b(activity, bundle, iuilistener);
                } else
                {
                    f.b("openSDK_LOG.QQShare", "shareToMobileQQ -- QQ Version is > 4.3.0 ");
                    com.tencent.connect.share.a.a(activity, s, new _cls2(bundle, s1, s2, iuilistener, activity));
                }
            }
        } else
        {
            b(activity, bundle, iuilistener);
        }
        f.c("openSDK_LOG", "shareToMobileQQ() -- end");
    }

    static void a(QQShare qqshare, Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        qqshare.b(activity, bundle, iuilistener);
    }

    static QQToken b(QQShare qqshare)
    {
        return qqshare.mToken;
    }

    private void b(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        f.c("openSDK_LOG", "doShareToQQ() -- start");
        StringBuffer stringbuffer = new StringBuffer("mqqapi://share/to_fri?src_type=app&version=1&file_type=news");
        String s4 = bundle.getString("imageUrl");
        String s5 = bundle.getString("title");
        String s6 = bundle.getString("summary");
        String s7 = bundle.getString("targetUrl");
        String s2 = bundle.getString("audio_url");
        int i = bundle.getInt("req_type", 1);
        int j = bundle.getInt("cflag", 0);
        String s3 = bundle.getString("share_qq_ext_str");
        String s1 = Util.getApplicationLable(activity);
        String s = s1;
        if (s1 == null)
        {
            s = bundle.getString("appName");
        }
        bundle = bundle.getString("imageLocalUrl");
        String s8 = mToken.getAppId();
        s1 = mToken.getOpenId();
        f.a("openSDK_LOG.QQShare", (new StringBuilder()).append("doShareToQQ -- openid: ").append(s1).toString());
        if (!TextUtils.isEmpty(s4))
        {
            stringbuffer.append((new StringBuilder()).append("&image_url=").append(Base64.encodeToString(Util.getBytesUTF8(s4), 2)).toString());
        }
        if (!TextUtils.isEmpty(bundle))
        {
            stringbuffer.append((new StringBuilder()).append("&file_data=").append(Base64.encodeToString(Util.getBytesUTF8(bundle), 2)).toString());
        }
        if (!TextUtils.isEmpty(s5))
        {
            stringbuffer.append((new StringBuilder()).append("&title=").append(Base64.encodeToString(Util.getBytesUTF8(s5), 2)).toString());
        }
        if (!TextUtils.isEmpty(s6))
        {
            stringbuffer.append((new StringBuilder()).append("&description=").append(Base64.encodeToString(Util.getBytesUTF8(s6), 2)).toString());
        }
        if (!TextUtils.isEmpty(s8))
        {
            stringbuffer.append((new StringBuilder()).append("&share_id=").append(s8).toString());
        }
        if (!TextUtils.isEmpty(s7))
        {
            stringbuffer.append((new StringBuilder()).append("&url=").append(Base64.encodeToString(Util.getBytesUTF8(s7), 2)).toString());
        }
        if (!TextUtils.isEmpty(s))
        {
            bundle = s;
            if (s.length() > 20)
            {
                bundle = (new StringBuilder()).append(s.substring(0, 20)).append("...").toString();
            }
            stringbuffer.append((new StringBuilder()).append("&app_name=").append(Base64.encodeToString(Util.getBytesUTF8(bundle), 2)).toString());
        }
        if (!TextUtils.isEmpty(s1))
        {
            stringbuffer.append((new StringBuilder()).append("&open_id=").append(Base64.encodeToString(Util.getBytesUTF8(s1), 2)).toString());
        }
        if (!TextUtils.isEmpty(s2))
        {
            stringbuffer.append((new StringBuilder()).append("&audioUrl=").append(Base64.encodeToString(Util.getBytesUTF8(s2), 2)).toString());
        }
        stringbuffer.append((new StringBuilder()).append("&req_type=").append(Base64.encodeToString(Util.getBytesUTF8(String.valueOf(i)), 2)).toString());
        if (!TextUtils.isEmpty(s3))
        {
            stringbuffer.append((new StringBuilder()).append("&share_qq_ext_str=").append(Base64.encodeToString(Util.getBytesUTF8(s3), 2)).toString());
        }
        stringbuffer.append((new StringBuilder()).append("&cflag=").append(Base64.encodeToString(Util.getBytesUTF8(String.valueOf(j)), 2)).toString());
        f.a("openSDK_LOG.QQShare", (new StringBuilder()).append("doShareToQQ -- url: ").append(stringbuffer.toString()).toString());
        com.tencent.connect.a.a.a(Global.getContext(), mToken, "requireApi", new String[] {
            "shareToNativeQQ"
        });
        mActivityIntent = new Intent("android.intent.action.VIEW");
        mActivityIntent.setData(Uri.parse(stringbuffer.toString()));
        mActivityIntent.putExtra("pkg_name", activity.getPackageName());
        if (SystemUtils.compareQQVersion(activity, "4.6.0") < 0)
        {
            f.c("openSDK_LOG.QQShare", "doShareToQQ, qqver below 4.6.");
            if (hasActivityForIntent())
            {
                startAssitActivity(activity, iuilistener);
            }
        } else
        {
            if (TemporaryStorage.set("shareToQQ", iuilistener) != null)
            {
                f.c("openSDK_LOG.QQShare", "doShareToQQ, last listener is not null, cancel it.");
            }
            if (hasActivityForIntent())
            {
                AssistActivity.isQQMobileShare = true;
                startAssistActivity(activity, 10103);
            }
        }
        if (hasActivityForIntent())
        {
            d.a().a(mToken.getOpenId(), mToken.getAppId(), "ANDROIDQQ.SHARETOQQ.XX", "10", "3", "0", mViaShareQQType, "0", "1", "0");
            d.a().a(0, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "");
        } else
        {
            d.a().a(mToken.getOpenId(), mToken.getAppId(), "ANDROIDQQ.SHARETOQQ.XX", "10", "3", "1", mViaShareQQType, "0", "1", "0");
            d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "hasActivityForIntent fail");
        }
        f.c("openSDK_LOG", "doShareToQQ() --end");
    }

    public void onActivityResult(Activity activity, int i, int j, Intent intent)
    {
    }

    public void releaseResource()
    {
        TemporaryStorage.remove("shareToQQ");
    }

    public void shareToQQ(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        String s;
        String s1;
        String s2;
        String s3;
        String s4;
        int i;
        f.c("openSDK_LOG.QQShare", "shareToQQ() -- start.");
        s1 = bundle.getString("imageUrl");
        s2 = bundle.getString("title");
        s3 = bundle.getString("summary");
        s = bundle.getString("targetUrl");
        s4 = bundle.getString("imageLocalUrl");
        i = bundle.getInt("req_type", 1);
        f.c("openSDK_LOG.QQShare", (new StringBuilder()).append("shareToQQ -- type: ").append(i).toString());
        i;
        JVM INSTR tableswitch 1 6: default 124
    //                   1 202
    //                   2 212
    //                   3 124
    //                   4 124
    //                   5 222
    //                   6 232;
           goto _L1 _L2 _L3 _L1 _L1 _L4 _L5
_L5:
        break MISSING_BLOCK_LABEL_232;
_L1:
        break; /* Loop/switch isn't completed */
_L2:
        mViaShareQQType = "1";
          goto _L6
_L3:
        mViaShareQQType = "3";
          goto _L6
_L4:
        mViaShareQQType = "2";
          goto _L6
        mViaShareQQType = "4";
_L6:
        if (i == 6)
        {
            if (SystemUtils.compareQQVersion(activity, "5.0.0") < 0)
            {
                iuilistener.onError(new UiError(-15, "\u624BQ\u7248\u672C\u8FC7\u4F4E\uFF0C\u5E94\u7528\u5206\u4EAB\u53EA\u652F\u6301\u624BQ5.0\u53CA\u5176\u4EE5\u4E0A\u7248\u672C", null));
                f.e("openSDK_LOG.QQShare", "shareToQQ, app share is not support below qq5.0.");
                d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQQ, app share is not support below qq5.0.");
                return;
            }
            s = String.format("http://fusion.qq.com/cgi-bin/qzapps/unified_jump?appid=%1$s&from=%2$s&isOpenAppID=1", new Object[] {
                mToken.getAppId(), "mqq"
            });
            bundle.putString("targetUrl", s);
        }
        if (!Util.hasSDCard() && SystemUtils.compareQQVersion(activity, "4.5.0") < 0)
        {
            iuilistener.onError(new UiError(-6, "\u5206\u4EAB\u56FE\u7247\u5931\u8D25\uFF0C\u68C0\u6D4B\u4E0D\u5230SD\u5361!", null));
            f.e("openSDK_LOG.QQShare", "shareToQQ sdcard is null--end");
            d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQQ sdcard is null");
            return;
        }
        if (i == 5)
        {
            if (SystemUtils.compareQQVersion(activity, "4.3.0") < 0)
            {
                iuilistener.onError(new UiError(-6, "\u4F4E\u7248\u672C\u624BQ\u4E0D\u652F\u6301\u8BE5\u9879\u529F\u80FD!", null));
                f.e("openSDK_LOG.QQShare", "shareToQQ, version below 4.3 is not support.");
                d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQQ, version below 4.3 is not support.");
                return;
            }
            if (!Util.fileExists(s4))
            {
                iuilistener.onError(new UiError(-6, "\u975E\u6CD5\u7684\u56FE\u7247\u5730\u5740!", null));
                f.e("openSDK_LOG.QQShare", "shareToQQ -- error: \u975E\u6CD5\u7684\u56FE\u7247\u5730\u5740!");
                d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "\u975E\u6CD5\u7684\u56FE\u7247\u5730\u5740!");
                return;
            }
        }
        if (i != 5)
        {
            if (TextUtils.isEmpty(s) || !s.startsWith("http://") && !s.startsWith("https://"))
            {
                iuilistener.onError(new UiError(-6, "\u4F20\u5165\u53C2\u6570\u6709\u8BEF!", null));
                f.e("openSDK_LOG.QQShare", "shareToQQ, targetUrl is empty or illegal..");
                d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQQ, targetUrl is empty or illegal..");
                return;
            }
            if (TextUtils.isEmpty(s2))
            {
                iuilistener.onError(new UiError(-6, "title\u4E0D\u80FD\u4E3A\u7A7A!", null));
                f.e("openSDK_LOG.QQShare", "shareToQQ, title is empty.");
                d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQQ, title is empty.");
                return;
            }
        }
        if (!TextUtils.isEmpty(s1) && !s1.startsWith("http://") && !s1.startsWith("https://") && !(new File(s1)).exists())
        {
            iuilistener.onError(new UiError(-6, "\u975E\u6CD5\u7684\u56FE\u7247\u5730\u5740!", null));
            f.e("openSDK_LOG.QQShare", " shareToQQ, image url is emprty or illegal.");
            d.a().a(1, "SHARE_CHECK_SDK", "1000", mToken.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQQ, image url is emprty or illegal.");
            return;
        }
        if (!TextUtils.isEmpty(s2) && s2.length() > 45)
        {
            bundle.putString("title", Util.subString(s2, 45, null, null));
        }
        if (!TextUtils.isEmpty(s3) && s3.length() > 60)
        {
            bundle.putString("summary", Util.subString(s3, 60, null, null));
        }
        if (Util.isMobileQQSupportShare(activity))
        {
            a(activity, bundle, iuilistener);
        } else
        {
            (new TDialog(activity, "", getCommonDownloadQQUrl(""), null, mToken)).show();
        }
        f.c("openSDK_LOG.QQShare", "shareToQQ() -- end.");
        return;
    }

    private class _cls1
        implements AsynLoadImgBack
    {

        final Bundle a;
        final String b;
        final String c;
        final IUiListener d;
        final Activity e;
        final QQShare f;

        public void batchSaved(int i, ArrayList arraylist)
        {
        }

        public void saved(int i, String s)
        {
            if (i == 0)
            {
                a.putString("imageLocalUrl", s);
            } else
            if (TextUtils.isEmpty(b) && TextUtils.isEmpty(c))
            {
                if (d != null)
                {
                    d.onError(new UiError(-6, "\u83B7\u53D6\u5206\u4EAB\u56FE\u7247\u5931\u8D25!", null));
                    com.tencent.open.a.f.e("openSDK_LOG.QQShare", "shareToMobileQQ -- error: \u83B7\u53D6\u5206\u4EAB\u56FE\u7247\u5931\u8D25!");
                }
                com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", com.tencent.connect.share.QQShare.a(f).getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "\u83B7\u53D6\u5206\u4EAB\u56FE\u7247\u5931\u8D25!");
                return;
            }
            com.tencent.connect.share.QQShare.a(f, e, a, d);
        }

        _cls1(Bundle bundle, String s, String s1, IUiListener iuilistener, Activity activity)
        {
            f = QQShare.this;
            a = bundle;
            b = s;
            c = s1;
            d = iuilistener;
            e = activity;
            super();
        }
    }


    private class _cls2
        implements AsynLoadImgBack
    {

        final Bundle a;
        final String b;
        final String c;
        final IUiListener d;
        final Activity e;
        final QQShare f;

        public void batchSaved(int i, ArrayList arraylist)
        {
        }

        public void saved(int i, String s)
        {
            if (i == 0)
            {
                a.putString("imageLocalUrl", s);
            } else
            if (TextUtils.isEmpty(b) && TextUtils.isEmpty(c))
            {
                if (d != null)
                {
                    d.onError(new UiError(-6, "\u83B7\u53D6\u5206\u4EAB\u56FE\u7247\u5931\u8D25!", null));
                    com.tencent.open.a.f.e("openSDK_LOG.QQShare", "shareToMobileQQ -- error: \u83B7\u53D6\u5206\u4EAB\u56FE\u7247\u5931\u8D25!");
                }
                com.tencent.open.b.d.a().a(1, "SHARE_CHECK_SDK", "1000", QQShare.b(f).getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "\u83B7\u53D6\u5206\u4EAB\u56FE\u7247\u5931\u8D25!");
                return;
            }
            com.tencent.connect.share.QQShare.a(f, e, a, d);
        }

        _cls2(Bundle bundle, String s, String s1, IUiListener iuilistener, Activity activity)
        {
            f = QQShare.this;
            a = bundle;
            b = s;
            c = s1;
            d = iuilistener;
            e = activity;
            super();
        }
    }

}
