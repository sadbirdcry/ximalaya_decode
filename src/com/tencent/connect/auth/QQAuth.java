// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.auth;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import android.widget.Toast;
import com.tencent.connect.a.a;
import com.tencent.connect.common.BaseApi;
import com.tencent.open.a.f;
import com.tencent.open.utils.ApkExternalInfoTool;
import com.tencent.open.utils.Global;
import com.tencent.tauth.IUiListener;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.tencent.connect.auth:
//            QQToken, AuthAgent

public class QQAuth
{

    private AuthAgent a;
    private QQToken b;

    private QQAuth(String s, Context context)
    {
        f.c("openSDK_LOG", "new QQAuth() --start");
        b = new QQToken(s);
        a = new AuthAgent(b);
        com.tencent.connect.a.a.c(context, b);
        f.c("openSDK_LOG", "new QQAuth() --end");
    }

    private int a(Activity activity, Fragment fragment, String s, IUiListener iuilistener, String s1)
    {
        Iterator iterator;
        s1 = activity.getApplicationContext().getPackageName();
        iterator = activity.getPackageManager().getInstalledApplications(128).iterator();
_L4:
        if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        ApplicationInfo applicationinfo = (ApplicationInfo)iterator.next();
        if (!s1.equals(applicationinfo.packageName)) goto _L4; else goto _L3
_L3:
        s1 = applicationinfo.sourceDir;
_L6:
        if (s1 == null)
        {
            break MISSING_BLOCK_LABEL_174;
        }
        int i;
        s1 = ApkExternalInfoTool.readChannelId(new File(s1));
        if (TextUtils.isEmpty(s1))
        {
            break MISSING_BLOCK_LABEL_174;
        }
        f.b("openSDK_LOG", (new StringBuilder()).append("-->login channelId: ").append(s1).toString());
        i = loginWithOEM(activity, s, iuilistener, s1, s1, "");
        return i;
        s1;
        f.b("openSDK_LOG", (new StringBuilder()).append("-->login get channel id exception.").append(s1.getMessage()).toString());
        s1.printStackTrace();
        f.b("openSDK_LOG", "-->login channelId is null ");
        BaseApi.isOEM = false;
        return a.doLogin(activity, s, iuilistener, false, fragment);
_L2:
        s1 = null;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public static QQAuth createInstance(String s, Context context)
    {
        Global.setContext(context.getApplicationContext());
        f.c("openSDK_LOG", "QQAuth -- createInstance() --start");
        try
        {
            PackageManager packagemanager = context.getPackageManager();
            packagemanager.getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.tauth.AuthActivity"), 0);
            packagemanager.getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.connect.common.AssistActivity"), 0);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            f.b("openSDK_LOG", "createInstance() error --end", s);
            Toast.makeText(context.getApplicationContext(), "\u8BF7\u53C2\u7167\u6587\u6863\u5728Androidmanifest.xml\u52A0\u4E0AAuthActivity\u548CAssitActivity\u7684\u5B9A\u4E49 ", 1).show();
            return null;
        }
        s = new QQAuth(s, context);
        f.c("openSDK_LOG", "QQAuth -- createInstance()  --end");
        return s;
    }

    public void checkLogin(IUiListener iuilistener)
    {
        a.b(iuilistener);
    }

    public QQToken getQQToken()
    {
        return b;
    }

    public boolean isSessionValid()
    {
        StringBuilder stringbuilder = (new StringBuilder()).append("isSessionValid(), result = ");
        String s;
        if (b.isSessionValid())
        {
            s = "true";
        } else
        {
            s = "false";
        }
        f.a("openSDK_LOG", stringbuilder.append(s).append("").toString());
        return b.isSessionValid();
    }

    public int login(Activity activity, String s, IUiListener iuilistener)
    {
        f.c("openSDK_LOG", "login()");
        return login(activity, s, iuilistener, "");
    }

    public int login(Activity activity, String s, IUiListener iuilistener, String s1)
    {
        f.c("openSDK_LOG", (new StringBuilder()).append("-->login activity: ").append(activity).toString());
        return a(activity, null, s, iuilistener, s1);
    }

    public int login(Fragment fragment, String s, IUiListener iuilistener, String s1)
    {
        android.support.v4.app.FragmentActivity fragmentactivity = fragment.getActivity();
        f.c("openSDK_LOG", (new StringBuilder()).append("-->login activity: ").append(fragmentactivity).toString());
        return a(fragmentactivity, fragment, s, iuilistener, s1);
    }

    public int loginWithOEM(Activity activity, String s, IUiListener iuilistener, String s1, String s2, String s3)
    {
        f.c("openSDK_LOG", "loginWithOEM");
        BaseApi.isOEM = true;
        String s4 = s1;
        if (s1.equals(""))
        {
            s4 = "null";
        }
        s1 = s2;
        if (s2.equals(""))
        {
            s1 = "null";
        }
        s2 = s3;
        if (s3.equals(""))
        {
            s2 = "null";
        }
        BaseApi.installChannel = s1;
        BaseApi.registerChannel = s4;
        BaseApi.businessId = s2;
        return a.doLogin(activity, s, iuilistener);
    }

    public void logout(Context context)
    {
        f.c("openSDK_LOG", "logout() --start");
        CookieSyncManager.createInstance(context);
        setAccessToken(null, null);
        setOpenId(context, null);
        f.c("openSDK_LOG", "logout() --end");
    }

    public boolean onActivityResult(int i, int j, Intent intent)
    {
        f.c("openSDK_LOG", (new StringBuilder()).append("onActivityResult() ,resultCode = ").append(j).append("").toString());
        return true;
    }

    public int reAuth(Activity activity, String s, IUiListener iuilistener)
    {
        f.c("openSDK_LOG", "reAuth()");
        return a.doLogin(activity, s, iuilistener, true, null);
    }

    public void reportDAU()
    {
        a.a(null);
    }

    public void setAccessToken(String s, String s1)
    {
        f.a("openSDK_LOG", (new StringBuilder()).append("setAccessToken(), validTimeInSecond = ").append(s1).append("").toString());
        b.setAccessToken(s, s1);
    }

    public void setOpenId(Context context, String s)
    {
        f.a("openSDK_LOG", "setOpenId() --start");
        b.setOpenId(s);
        com.tencent.connect.a.a.d(context, b);
        f.a("openSDK_LOG", "setOpenId() --end");
    }
}
