// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.auth;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.graphics.drawable.PaintDrawable;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.connect.a.a;
import com.tencent.connect.common.AssistActivity;
import com.tencent.connect.common.BaseApi;
import com.tencent.open.a.f;
import com.tencent.open.b.d;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.HttpUtils;
import com.tencent.open.utils.ServerSetting;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.ThreadManager;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.connect.auth:
//            QQToken

public class AuthAgent extends BaseApi
{
    private class CheckLoginListener
        implements IUiListener
    {

        IUiListener a;
        final AuthAgent b;

        public void onCancel()
        {
            if (a != null)
            {
                a.onCancel();
            }
        }

        public void onComplete(Object obj)
        {
            if (obj != null) goto _L2; else goto _L1
_L1:
            com.tencent.open.a.f.e("CheckLoginListener", "response data is null");
_L5:
            return;
_L2:
            obj = (JSONObject)obj;
            int i;
            try
            {
                i = ((JSONObject) (obj)).getInt("ret");
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((JSONException) (obj)).printStackTrace();
                com.tencent.open.a.f.e("CheckLoginListener", "response data format error");
                return;
            }
            if (i != 0)
            {
                break MISSING_BLOCK_LABEL_80;
            }
            obj = "success";
_L3:
            if (a != null)
            {
                a.onComplete((new JSONObject()).put("ret", i).put("msg", obj));
                return;
            }
            continue; /* Loop/switch isn't completed */
            obj = ((JSONObject) (obj)).getString("msg");
              goto _L3
            if (true) goto _L5; else goto _L4
_L4:
        }

        public void onError(UiError uierror)
        {
            if (a != null)
            {
                a.onError(uierror);
            }
        }

        public CheckLoginListener(IUiListener iuilistener)
        {
            b = AuthAgent.this;
            super();
            a = iuilistener;
        }
    }

    private class FeedConfirmListener
        implements IUiListener
    {

        IUiListener a;
        final AuthAgent b;
        private final String c = "sendinstall";
        private final String d = "installwording";
        private final String e = "http://appsupport.qq.com/cgi-bin/qzapps/mapp_addapp.cgi";

        private Drawable a(String s, Context context)
        {
            context = context.getApplicationContext().getAssets();
            context = context.open(s);
            if (context == null)
            {
                return null;
            }
            boolean flag = s.endsWith(".9.png");
            if (!flag) goto _L2; else goto _L1
_L1:
            s = BitmapFactory.decodeStream(context);
_L7:
            if (s == null) goto _L4; else goto _L3
_L3:
            context = s.getNinePatchChunk();
            NinePatch.isNinePatchChunk(context);
            s = new NinePatchDrawable(s, context, new Rect(), null);
            return s;
            context;
            s = null;
_L5:
            context.printStackTrace();
            return s;
            s;
            s.printStackTrace();
            s = null;
            continue; /* Loop/switch isn't completed */
_L2:
            s = Drawable.createFromStream(context, s);
            context.close();
            return s;
            context;
            if (true) goto _L5; else goto _L4
_L4:
            return null;
            if (true) goto _L7; else goto _L6
_L6:
        }

        private View a(Context context, Drawable drawable, String s, android.view.View.OnClickListener onclicklistener, android.view.View.OnClickListener onclicklistener1)
        {
            Object obj = new DisplayMetrics();
            ((WindowManager)context.getSystemService("window")).getDefaultDisplay().getMetrics(((DisplayMetrics) (obj)));
            float f1 = ((DisplayMetrics) (obj)).density;
            obj = new RelativeLayout(context);
            Object obj1 = new ImageView(context);
            ((ImageView) (obj1)).setImageDrawable(drawable);
            ((ImageView) (obj1)).setScaleType(android.widget.ImageView.ScaleType.FIT_XY);
            ((ImageView) (obj1)).setId(1);
            int i = (int)(60F * f1);
            int j = (int)(60F * f1);
            int k = (int)(14F * f1);
            k = (int)(18F * f1);
            int l = (int)(6F * f1);
            int i1 = (int)(18F * f1);
            drawable = new android.widget.RelativeLayout.LayoutParams(i, j);
            drawable.addRule(9);
            drawable.setMargins(0, k, l, i1);
            ((RelativeLayout) (obj)).addView(((View) (obj1)), drawable);
            drawable = new TextView(context);
            drawable.setText(s);
            drawable.setTextSize(14F);
            drawable.setGravity(3);
            drawable.setIncludeFontPadding(false);
            drawable.setPadding(0, 0, 0, 0);
            drawable.setLines(2);
            drawable.setId(5);
            drawable.setMinWidth((int)(185F * f1));
            s = new android.widget.RelativeLayout.LayoutParams(-2, -2);
            s.addRule(1, 1);
            s.addRule(6, 1);
            i = (int)(10F * f1);
            s.setMargins(0, 0, (int)(5F * f1), 0);
            ((RelativeLayout) (obj)).addView(drawable, s);
            drawable = new View(context);
            drawable.setBackgroundColor(Color.rgb(214, 214, 214));
            drawable.setId(3);
            s = new android.widget.RelativeLayout.LayoutParams(-2, 2);
            s.addRule(3, 1);
            s.addRule(5, 1);
            s.addRule(7, 5);
            s.setMargins(0, 0, 0, (int)(12F * f1));
            ((RelativeLayout) (obj)).addView(drawable, s);
            drawable = new LinearLayout(context);
            s = new android.widget.RelativeLayout.LayoutParams(-2, -2);
            s.addRule(5, 1);
            s.addRule(7, 5);
            s.addRule(3, 3);
            obj1 = new Button(context);
            ((Button) (obj1)).setText("\u8DF3\u8FC7");
            ((Button) (obj1)).setBackgroundDrawable(a("buttonNegt.png", context));
            ((Button) (obj1)).setTextColor(Color.rgb(36, 97, 131));
            ((Button) (obj1)).setTextSize(20F);
            ((Button) (obj1)).setOnClickListener(onclicklistener1);
            ((Button) (obj1)).setId(4);
            onclicklistener1 = new android.widget.LinearLayout.LayoutParams(0, (int)(45F * f1));
            onclicklistener1.rightMargin = (int)(14F * f1);
            onclicklistener1.leftMargin = (int)(4F * f1);
            onclicklistener1.weight = 1.0F;
            drawable.addView(((View) (obj1)), onclicklistener1);
            onclicklistener1 = new Button(context);
            onclicklistener1.setText("\u786E\u5B9A");
            onclicklistener1.setTextSize(20F);
            onclicklistener1.setTextColor(Color.rgb(255, 255, 255));
            onclicklistener1.setBackgroundDrawable(a("buttonPost.png", context));
            onclicklistener1.setOnClickListener(onclicklistener);
            context = new android.widget.LinearLayout.LayoutParams(0, (int)(45F * f1));
            context.weight = 1.0F;
            context.rightMargin = (int)(4F * f1);
            drawable.addView(onclicklistener1, context);
            ((RelativeLayout) (obj)).addView(drawable, s);
            context = new android.widget.FrameLayout.LayoutParams((int)(279F * f1), (int)(163F * f1));
            ((RelativeLayout) (obj)).setPadding((int)(14F * f1), 0, (int)(12F * f1), (int)(12F * f1));
            ((RelativeLayout) (obj)).setLayoutParams(context);
            ((RelativeLayout) (obj)).setBackgroundColor(Color.rgb(247, 251, 247));
            context = new PaintDrawable(Color.rgb(247, 251, 247));
            context.setCornerRadius(f1 * 5F);
            ((RelativeLayout) (obj)).setBackgroundDrawable(context);
            return ((View) (obj));
        }

        private void a(String s, IUiListener iuilistener, Object obj)
        {
            Drawable drawable = null;
            Dialog dialog = new Dialog(AuthAgent.e(b));
            dialog.requestWindowFeature(1);
            Object obj2 = AuthAgent.e(b).getPackageManager();
            class _cls1 extends ButtonListener
            {

                final IUiListener a;
                final Object b;
                final FeedConfirmListener c;

                public void onClick(View view)
                {
                    c.a();
                    if (d != null && d.isShowing())
                    {
                        d.dismiss();
                    }
                    if (a != null)
                    {
                        a.onComplete(b);
                    }
                }

                _cls1(Dialog dialog, IUiListener iuilistener, Object obj)
                {
                    c = FeedConfirmListener.this;
                    a = iuilistener;
                    b = obj;
                    super(FeedConfirmListener.this, dialog);
                }
            }

            class _cls2 extends ButtonListener
            {

                final IUiListener a;
                final Object b;
                final FeedConfirmListener c;

                public void onClick(View view)
                {
                    if (d != null && d.isShowing())
                    {
                        d.dismiss();
                    }
                    if (a != null)
                    {
                        a.onComplete(b);
                    }
                }

                _cls2(Dialog dialog, IUiListener iuilistener, Object obj)
                {
                    c = FeedConfirmListener.this;
                    a = iuilistener;
                    b = obj;
                    super(FeedConfirmListener.this, dialog);
                }
            }

            class _cls3
                implements android.content.DialogInterface.OnCancelListener
            {

                final IUiListener a;
                final Object b;
                final FeedConfirmListener c;

                public void onCancel(DialogInterface dialoginterface)
                {
                    if (a != null)
                    {
                        a.onComplete(b);
                    }
                }

                _cls3(IUiListener iuilistener, Object obj)
                {
                    c = FeedConfirmListener.this;
                    a = iuilistener;
                    b = obj;
                    super();
                }
            }

            Object obj1;
            ColorDrawable colordrawable;
            try
            {
                obj1 = ((PackageManager) (obj2)).getPackageInfo(AuthAgent.e(b).getPackageName(), 0);
            }
            catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception)
            {
                namenotfoundexception.printStackTrace();
                namenotfoundexception = null;
            }
            if (obj1 != null)
            {
                drawable = ((PackageInfo) (obj1)).applicationInfo.loadIcon(((PackageManager) (obj2)));
            }
            obj1 = new _cls1(dialog, iuilistener, obj);
            obj2 = new _cls2(dialog, iuilistener, obj);
            colordrawable = new ColorDrawable();
            colordrawable.setAlpha(0);
            dialog.getWindow().setBackgroundDrawable(colordrawable);
            dialog.setContentView(a(((Context) (AuthAgent.e(b))), drawable, s, ((android.view.View.OnClickListener) (obj1)), ((android.view.View.OnClickListener) (obj2))));
            dialog.setOnCancelListener(new _cls3(iuilistener, obj));
            if (AuthAgent.e(b) != null && !AuthAgent.e(b).isFinishing())
            {
                dialog.show();
            }
        }

        protected void a()
        {
            Bundle bundle = AuthAgent.g(b);
            HttpUtils.requestAsync(AuthAgent.h(b), AuthAgent.e(b), "http://appsupport.qq.com/cgi-bin/qzapps/mapp_addapp.cgi", bundle, "POST", null);
        }

        public void onCancel()
        {
            if (a != null)
            {
                a.onCancel();
            }
        }

        public void onComplete(Object obj)
        {
            Object obj1;
            boolean flag;
            boolean flag1;
            flag1 = false;
            flag = false;
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_98;
            }
            obj1 = (JSONObject)obj;
            if (obj1 == null)
            {
                break MISSING_BLOCK_LABEL_98;
            }
            if (((JSONObject) (obj1)).getInt("sendinstall") == 1)
            {
                flag = true;
            }
            flag1 = flag;
            try
            {
                obj1 = ((JSONObject) (obj1)).getString("installwording");
            }
            catch (JSONException jsonexception)
            {
                com.tencent.open.a.f.d("FeedConfirm", "There is no value for sendinstall.");
                flag = flag1;
                jsonexception = "";
            }
            obj1 = URLDecoder.decode(((String) (obj1)));
            com.tencent.open.a.f.b("TAG", (new StringBuilder()).append(" WORDING = ").append(((String) (obj1))).append("xx").toString());
            if (flag && !TextUtils.isEmpty(((CharSequence) (obj1))))
            {
                a(((String) (obj1)), a, obj);
            } else
            if (a != null)
            {
                a.onComplete(obj);
                return;
            }
        }

        public void onError(UiError uierror)
        {
            if (a != null)
            {
                a.onError(uierror);
            }
        }

        public FeedConfirmListener(IUiListener iuilistener)
        {
            b = AuthAgent.this;
            super();
            a = iuilistener;
        }
    }

    private abstract class FeedConfirmListener.ButtonListener
        implements android.view.View.OnClickListener
    {

        Dialog d;
        final FeedConfirmListener e;

        FeedConfirmListener.ButtonListener(FeedConfirmListener feedconfirmlistener, Dialog dialog)
        {
            e = feedconfirmlistener;
            super();
            d = dialog;
        }
    }

    private class TokenListener
        implements IUiListener
    {

        final AuthAgent a;
        private final IUiListener b;
        private final boolean c;
        private final Context d;

        public void onCancel()
        {
            com.tencent.open.a.f.b("openSDK_LOG", "OpenUi, TokenListener() onCancel");
            b.onCancel();
            com.tencent.open.a.f.b();
        }

        public void onComplete(Object obj)
        {
            com.tencent.open.a.f.b("openSDK_LOG", "OpenUi, TokenListener() onComplete");
            obj = (JSONObject)obj;
            String s;
            String s1;
            String s2;
            s = ((JSONObject) (obj)).getString("access_token");
            s1 = ((JSONObject) (obj)).getString("expires_in");
            s2 = ((JSONObject) (obj)).getString("openid");
            if (s == null)
            {
                break MISSING_BLOCK_LABEL_91;
            }
            if (com.tencent.connect.auth.AuthAgent.a(a) == null || s2 == null)
            {
                break MISSING_BLOCK_LABEL_91;
            }
            AuthAgent.b(a).setAccessToken(s, s1);
            AuthAgent.c(a).setOpenId(s2);
            com.tencent.connect.a.a.d(d, com.tencent.connect.auth.AuthAgent.d(a));
            s = ((JSONObject) (obj)).getString("pf");
            if (s == null)
            {
                break MISSING_BLOCK_LABEL_131;
            }
            d.getSharedPreferences("pfStore", 0).edit().putString("pf", s).commit();
_L1:
            if (c)
            {
                CookieSyncManager.getInstance().sync();
            }
_L2:
            b.onComplete(obj);
            a.releaseResource();
            com.tencent.open.a.f.b();
            return;
            Object obj1;
            obj1;
            ((Exception) (obj1)).printStackTrace();
            com.tencent.open.a.f.b("openSDK_LOG", "OpenUi, TokenListener() onComplete error", ((Throwable) (obj1)));
              goto _L1
            obj1;
            ((JSONException) (obj1)).printStackTrace();
            com.tencent.open.a.f.b("openSDK_LOG", "OpenUi, TokenListener() onComplete error", ((Throwable) (obj1)));
              goto _L2
        }

        public void onError(UiError uierror)
        {
            com.tencent.open.a.f.b("openSDK_LOG", "OpenUi, TokenListener() onError");
            b.onError(uierror);
            com.tencent.open.a.f.b();
        }

        public TokenListener(Context context, IUiListener iuilistener, boolean flag, boolean flag1)
        {
            a = AuthAgent.this;
            super();
            d = context;
            b = iuilistener;
            c = flag;
            com.tencent.open.a.f.b("openSDK_LOG", "OpenUi, TokenListener()");
        }
    }


    public static final String SECURE_LIB_FILE_NAME = "libwbsafeedit";
    public static final String SECURE_LIB_NAME = "libwbsafeedit.so";
    private IUiListener a;
    private String b;
    private Activity c;

    public AuthAgent(QQToken qqtoken)
    {
        super(qqtoken);
    }

    private int a(boolean flag, IUiListener iuilistener)
    {
        com.tencent.open.a.f.c("openSDK_LOG", "OpenUi, showDialog -- start");
        CookieSyncManager.createInstance(Global.getContext());
        Object obj = composeCGIParams();
        if (flag)
        {
            ((Bundle) (obj)).putString("isadd", "1");
        }
        ((Bundle) (obj)).putString("scope", b);
        ((Bundle) (obj)).putString("client_id", mToken.getAppId());
        Object obj1;
        if (isOEM)
        {
            ((Bundle) (obj)).putString("pf", (new StringBuilder()).append("desktop_m_qq-").append(installChannel).append("-").append("android").append("-").append(registerChannel).append("-").append(businessId).toString());
        } else
        {
            ((Bundle) (obj)).putString("pf", "openmobile_android");
        }
        obj1 = (new StringBuilder()).append(System.currentTimeMillis() / 1000L).append("").toString();
        ((Bundle) (obj)).putString("sign", SystemUtils.getAppSignatureMD5(Global.getContext(), ((String) (obj1))));
        ((Bundle) (obj)).putString("time", ((String) (obj1)));
        ((Bundle) (obj)).putString("display", "mobile");
        ((Bundle) (obj)).putString("response_type", "token");
        ((Bundle) (obj)).putString("redirect_uri", "auth://tauth.qq.com/");
        ((Bundle) (obj)).putString("cancel_display", "1");
        ((Bundle) (obj)).putString("switch", "1");
        ((Bundle) (obj)).putString("status_userip", Util.getUserIp());
        obj1 = new StringBuilder();
        ((StringBuilder) (obj1)).append(ServerSetting.getInstance().getEnvUrl(Global.getContext(), "http://openmobile.qq.com/oauth2.0/m_authorize?"));
        ((StringBuilder) (obj1)).append(Util.encodeUrl(((Bundle) (obj))));
        obj = ((StringBuilder) (obj1)).toString();
        iuilistener = new TokenListener(Global.getContext(), iuilistener, true, false);
        com.tencent.open.a.f.b("openSDK_LOG", "OpenUi, showDialog TDialog");
        ThreadManager.executeOnSubThread(new _cls1(((String) (obj)), iuilistener));
        com.tencent.open.a.f.c("openSDK_LOG", "OpenUi, showDialog -- end");
        return 2;
    }

    static QQToken a(AuthAgent authagent)
    {
        return authagent.mToken;
    }

    private void a(String s)
    {
        try
        {
            Object obj = Util.parseJson(s);
            s = ((JSONObject) (obj)).getString("access_token");
            String s1 = ((JSONObject) (obj)).getString("expires_in");
            obj = ((JSONObject) (obj)).getString("openid");
            if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s1) && !TextUtils.isEmpty(((CharSequence) (obj))))
            {
                mToken.setAccessToken(s, s1);
                mToken.setOpenId(((String) (obj)));
            }
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

    private boolean a(Activity activity, Fragment fragment, boolean flag)
    {
        com.tencent.open.a.f.c("openSDK_LOG", "startActionActivity() -- start");
        Intent intent = getTargetActivityIntent("com.tencent.open.agent.AgentActivity");
        if (intent != null)
        {
            Bundle bundle = composeCGIParams();
            if (flag)
            {
                bundle.putString("isadd", "1");
            }
            bundle.putString("scope", b);
            bundle.putString("client_id", mToken.getAppId());
            if (isOEM)
            {
                bundle.putString("pf", (new StringBuilder()).append("desktop_m_qq-").append(installChannel).append("-").append("android").append("-").append(registerChannel).append("-").append(businessId).toString());
            } else
            {
                bundle.putString("pf", "openmobile_android");
            }
            bundle.putString("need_pay", "1");
            bundle.putString("oauth_app_name", SystemUtils.getAppName(Global.getContext()));
            intent.putExtra("key_action", "action_login");
            intent.putExtra("key_params", bundle);
            mActivityIntent = intent;
            if (hasActivityForIntent())
            {
                a = new FeedConfirmListener(a);
                if (fragment != null)
                {
                    com.tencent.open.a.f.b("AuthAgent", "startAssitActivity fragment");
                    startAssitActivity(fragment, a);
                } else
                {
                    com.tencent.open.a.f.b("AuthAgent", "startAssitActivity activity");
                    startAssitActivity(activity, a);
                }
                com.tencent.open.a.f.c("openSDK_LOG", "startActionActivity() -- end");
                com.tencent.open.b.d.a().a(0, "LOGIN_CHECK_SDK", "1000", mToken.getAppId(), "", Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "");
                return true;
            }
        }
        com.tencent.open.b.d.a().a(1, "LOGIN_CHECK_SDK", "1000", mToken.getAppId(), "", Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "startActionActivity fail");
        com.tencent.open.a.f.c("openSDK_LOG", "startActionActivity() -- end");
        return false;
    }

    static QQToken b(AuthAgent authagent)
    {
        return authagent.mToken;
    }

    static QQToken c(AuthAgent authagent)
    {
        return authagent.mToken;
    }

    static QQToken d(AuthAgent authagent)
    {
        return authagent.mToken;
    }

    static Activity e(AuthAgent authagent)
    {
        return authagent.c;
    }

    static QQToken f(AuthAgent authagent)
    {
        return authagent.mToken;
    }

    static Bundle g(AuthAgent authagent)
    {
        return authagent.composeActivityParams();
    }

    static QQToken h(AuthAgent authagent)
    {
        return authagent.mToken;
    }

    protected void a(IUiListener iuilistener)
    {
        com.tencent.open.a.f.c("openSDK_LOG", "reportDAU() -- start");
        String s1 = mToken.getAccessToken();
        String s2 = mToken.getOpenId();
        String s3 = mToken.getAppId();
        String s = "";
        iuilistener = s;
        if (!TextUtils.isEmpty(s1))
        {
            iuilistener = s;
            if (!TextUtils.isEmpty(s2))
            {
                iuilistener = s;
                if (!TextUtils.isEmpty(s3))
                {
                    iuilistener = Util.encrypt((new StringBuilder()).append("tencent&sdk&qazxc***14969%%").append(s1).append(s3).append(s2).append("qzone3.4").toString());
                }
            }
        }
        if (TextUtils.isEmpty(iuilistener))
        {
            com.tencent.open.a.f.e("openSDK_LOG", "reportDAU -- encrytoken is null");
            return;
        } else
        {
            Bundle bundle = composeCGIParams();
            bundle.putString("encrytoken", iuilistener);
            HttpUtils.requestAsync(mToken, Global.getContext(), "https://openmobile.qq.com/user/user_login_statis", bundle, "POST", null);
            com.tencent.open.a.f.c("openSDK_LOG", "reportDAU() -- end");
            return;
        }
    }

    protected void b(IUiListener iuilistener)
    {
        Bundle bundle = composeCGIParams();
        bundle.putString("reqType", "checkLogin");
        iuilistener = new com.tencent.connect.common.BaseApi.TempRequestListener(this, new CheckLoginListener(iuilistener));
        HttpUtils.requestAsync(mToken, Global.getContext(), "https://openmobile.qq.com/v3/user/get_info", bundle, "GET", iuilistener);
    }

    public int doLogin(Activity activity, String s, IUiListener iuilistener)
    {
        return doLogin(activity, s, iuilistener, false, null);
    }

    public int doLogin(Activity activity, String s, IUiListener iuilistener, boolean flag, Fragment fragment)
    {
        b = s;
        c = activity;
        a = iuilistener;
        if (a(activity, fragment, flag))
        {
            com.tencent.open.a.f.c("openSDK_LOG", "OpenUi, showUi, return Constants.UI_ACTIVITY");
            com.tencent.open.b.d.a().a(mToken.getOpenId(), mToken.getAppId(), "2", "1", "5", "0", "0", "0");
            return 1;
        } else
        {
            com.tencent.open.b.d.a().a(mToken.getOpenId(), mToken.getAppId(), "2", "1", "5", "1", "0", "0");
            com.tencent.open.a.f.d("openSDK_LOG", "startActivity fail show dialog.");
            a = new FeedConfirmListener(a);
            return a(flag, a);
        }
    }

    public void onActivityResult(Activity activity, int i, int j, Intent intent)
    {
        Object obj;
        ThreadManager.executeOnSubThread(new _cls2());
        obj = mTaskList.iterator();
_L4:
        if (!((Iterator) (obj)).hasNext()) goto _L2; else goto _L1
_L1:
        com.tencent.connect.common.BaseApi.ApiTask apitask = (com.tencent.connect.common.BaseApi.ApiTask)((Iterator) (obj)).next();
        if (apitask.mRequestCode != i) goto _L4; else goto _L3
_L3:
        obj = apitask.mListener;
        mTaskList.remove(apitask);
_L6:
        if (intent == null)
        {
            if (obj != null)
            {
                ((IUiListener) (obj)).onCancel();
            }
            return;
        }
        a(intent.getStringExtra("key_response"));
        if (obj == null)
        {
            AssistActivity.setResultDataForLogin(activity, intent);
            return;
        }
        if (j == -1)
        {
            handleDataToListener(intent, ((IUiListener) (obj)));
        } else
        {
            com.tencent.open.a.f.b("openSDK_LOG", "OpenUi, onActivityResult, Constants.ACTIVITY_CANCEL");
            ((IUiListener) (obj)).onCancel();
        }
        releaseResource();
        com.tencent.open.a.f.b();
        return;
_L2:
        obj = null;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void releaseResource()
    {
        c = null;
        a = null;
    }

    private class _cls1
        implements Runnable
    {

        final String a;
        final IUiListener b;
        final AuthAgent c;

        public void run()
        {
            SystemUtils.extractSecureLib("libwbsafeedit", "libwbsafeedit.so", 2);
            class _cls1
                implements Runnable
            {

                final _cls1 a;

                public void run()
                {
                    AuthDialog authdialog = new AuthDialog(AuthAgent.e(a.c), "action_login", a.a, a.b, com.tencent.connect.auth.AuthAgent.f(a.c));
                    if (AuthAgent.e(a.c) != null && !AuthAgent.e(a.c).isFinishing())
                    {
                        authdialog.show();
                    }
                }

                _cls1()
                {
                    a = _cls1.this;
                    super();
                }
            }

            if (AuthAgent.e(c) != null)
            {
                AuthAgent.e(c).runOnUiThread(new _cls1());
            }
        }

        _cls1(String s, IUiListener iuilistener)
        {
            c = AuthAgent.this;
            a = s;
            b = iuilistener;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final AuthAgent a;

        public void run()
        {
            Global.saveVersionCode();
        }

        _cls2()
        {
            a = AuthAgent.this;
            super();
        }
    }

}
