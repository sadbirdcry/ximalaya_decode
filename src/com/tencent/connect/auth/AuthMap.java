// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.auth;

import com.tencent.tauth.IUiListener;
import java.util.HashMap;

// Referenced classes of package com.tencent.connect.auth:
//            AuthDialog

public class AuthMap
{
    public static class Auth
    {

        public AuthDialog dialog;
        public String key;
        public IUiListener listener;

        public Auth()
        {
        }
    }


    static final boolean a;
    private static int b = 0;
    public static AuthMap sInstance;
    public final String KEY_CHAR_LIST = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    public HashMap authMap;

    public AuthMap()
    {
        authMap = new HashMap();
    }

    private String a(String s, String s1)
    {
        int i = 0;
        if (!a && s.length() % 2 != 0)
        {
            throw new AssertionError();
        }
        StringBuilder stringbuilder = new StringBuilder();
        int k = s1.length();
        int l = s.length() / 2;
        int j = 0;
        for (; i < l; i++)
        {
            stringbuilder.append((char)(Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16) ^ s1.charAt(j)));
            j = (j + 1) % k;
        }

        return stringbuilder.toString();
    }

    public static AuthMap getInstance()
    {
        if (sInstance == null)
        {
            sInstance = new AuthMap();
        }
        return sInstance;
    }

    public static int getSerial()
    {
        int i = b + 1;
        b = i;
        return i;
    }

    public String decode(String s, String s1)
    {
        return a(s, s1);
    }

    public Auth get(String s)
    {
        return (Auth)authMap.get(s);
    }

    public String makeKey()
    {
        int j = (int)Math.ceil(Math.random() * 20D + 3D);
        char ac[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        int k = ac.length;
        StringBuffer stringbuffer = new StringBuffer();
        for (int i = 0; i < j; i++)
        {
            stringbuffer.append(ac[(int)(Math.random() * (double)k)]);
        }

        return stringbuffer.toString();
    }

    public void remove(String s)
    {
        authMap.remove(s);
    }

    public String set(Auth auth)
    {
        int i = getSerial();
        try
        {
            authMap.put((new StringBuilder()).append("").append(i).toString(), auth);
        }
        // Misplaced declaration of an exception variable
        catch (Auth auth)
        {
            auth.printStackTrace();
        }
        return (new StringBuilder()).append("").append(i).toString();
    }

    static 
    {
        boolean flag;
        if (!com/tencent/connect/auth/AuthMap.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        a = flag;
    }
}
