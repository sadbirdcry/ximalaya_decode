// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.auth;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.graphics.drawable.PaintDrawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.open.a.f;
import com.tencent.open.utils.HttpUtils;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.connect.auth:
//            AuthAgent

private class a
    implements IUiListener
{
    private abstract class ButtonListener
        implements android.view.View.OnClickListener
    {

        Dialog d;
        final AuthAgent.FeedConfirmListener e;

        ButtonListener(Dialog dialog)
        {
            e = AuthAgent.FeedConfirmListener.this;
            super();
            d = dialog;
        }
    }


    IUiListener a;
    final AuthAgent b;
    private final String c = "sendinstall";
    private final String d = "installwording";
    private final String e = "http://appsupport.qq.com/cgi-bin/qzapps/mapp_addapp.cgi";

    private Drawable a(String s, Context context)
    {
        context = context.getApplicationContext().getAssets();
        context = context.open(s);
        if (context == null)
        {
            return null;
        }
        boolean flag = s.endsWith(".9.png");
        if (!flag) goto _L2; else goto _L1
_L1:
        s = BitmapFactory.decodeStream(context);
_L7:
        if (s == null) goto _L4; else goto _L3
_L3:
        context = s.getNinePatchChunk();
        NinePatch.isNinePatchChunk(context);
        s = new NinePatchDrawable(s, context, new Rect(), null);
        return s;
        context;
        s = null;
_L5:
        context.printStackTrace();
        return s;
        s;
        s.printStackTrace();
        s = null;
        continue; /* Loop/switch isn't completed */
_L2:
        s = Drawable.createFromStream(context, s);
        context.close();
        return s;
        context;
        if (true) goto _L5; else goto _L4
_L4:
        return null;
        if (true) goto _L7; else goto _L6
_L6:
    }

    private View a(Context context, Drawable drawable, String s, android.view.irmListener irmlistener, android.view.irmListener irmlistener1)
    {
        Object obj = new DisplayMetrics();
        ((WindowManager)context.getSystemService("window")).getDefaultDisplay().getMetrics(((DisplayMetrics) (obj)));
        float f1 = ((DisplayMetrics) (obj)).density;
        obj = new RelativeLayout(context);
        Object obj1 = new ImageView(context);
        ((ImageView) (obj1)).setImageDrawable(drawable);
        ((ImageView) (obj1)).setScaleType(android.widget.mListener);
        ((ImageView) (obj1)).setId(1);
        int i = (int)(60F * f1);
        int j = (int)(60F * f1);
        int k = (int)(14F * f1);
        k = (int)(18F * f1);
        int l = (int)(6F * f1);
        int i1 = (int)(18F * f1);
        drawable = new android.widget.nit>(i, j);
        drawable.dRule(9);
        drawable.tMargins(0, k, l, i1);
        ((RelativeLayout) (obj)).addView(((View) (obj1)), drawable);
        drawable = new TextView(context);
        drawable.setText(s);
        drawable.setTextSize(14F);
        drawable.setGravity(3);
        drawable.setIncludeFontPadding(false);
        drawable.setPadding(0, 0, 0, 0);
        drawable.setLines(2);
        drawable.setId(5);
        drawable.setMinWidth((int)(185F * f1));
        s = new android.widget.nit>(-2, -2);
        s.dRule(1, 1);
        s.dRule(6, 1);
        i = (int)(10F * f1);
        s.tMargins(0, 0, (int)(5F * f1), 0);
        ((RelativeLayout) (obj)).addView(drawable, s);
        drawable = new View(context);
        drawable.setBackgroundColor(Color.rgb(214, 214, 214));
        drawable.setId(3);
        s = new android.widget.nit>(-2, 2);
        s.dRule(3, 1);
        s.dRule(5, 1);
        s.dRule(7, 5);
        s.tMargins(0, 0, 0, (int)(12F * f1));
        ((RelativeLayout) (obj)).addView(drawable, s);
        drawable = new LinearLayout(context);
        s = new android.widget.nit>(-2, -2);
        s.dRule(5, 1);
        s.dRule(7, 5);
        s.dRule(3, 3);
        obj1 = new Button(context);
        ((Button) (obj1)).setText("\u8DF3\u8FC7");
        ((Button) (obj1)).setBackgroundDrawable(a("buttonNegt.png", context));
        ((Button) (obj1)).setTextColor(Color.rgb(36, 97, 131));
        ((Button) (obj1)).setTextSize(20F);
        ((Button) (obj1)).setOnClickListener(irmlistener1);
        ((Button) (obj1)).setId(4);
        irmlistener1 = new android.widget.t>(0, (int)(45F * f1));
        irmlistener1.tMargin = (int)(14F * f1);
        irmlistener1.Margin = (int)(4F * f1);
        irmlistener1.ht = 1.0F;
        drawable.addView(((View) (obj1)), irmlistener1);
        irmlistener1 = new Button(context);
        irmlistener1.setText("\u786E\u5B9A");
        irmlistener1.setTextSize(20F);
        irmlistener1.setTextColor(Color.rgb(255, 255, 255));
        irmlistener1.setBackgroundDrawable(a("buttonPost.png", context));
        irmlistener1.setOnClickListener(irmlistener);
        context = new android.widget.t>(0, (int)(45F * f1));
        context.ht = 1.0F;
        context.tMargin = (int)(4F * f1);
        drawable.addView(irmlistener1, context);
        ((RelativeLayout) (obj)).addView(drawable, s);
        context = new android.widget.>((int)(279F * f1), (int)(163F * f1));
        ((RelativeLayout) (obj)).setPadding((int)(14F * f1), 0, (int)(12F * f1), (int)(12F * f1));
        ((RelativeLayout) (obj)).setLayoutParams(context);
        ((RelativeLayout) (obj)).setBackgroundColor(Color.rgb(247, 251, 247));
        context = new PaintDrawable(Color.rgb(247, 251, 247));
        context.setCornerRadius(f1 * 5F);
        ((RelativeLayout) (obj)).setBackgroundDrawable(context);
        return ((View) (obj));
    }

    private void a(String s, IUiListener iuilistener, Object obj)
    {
        Drawable drawable = null;
        Dialog dialog = new Dialog(AuthAgent.e(b));
        dialog.requestWindowFeature(1);
        Object obj2 = AuthAgent.e(b).getPackageManager();
        class _cls1 extends ButtonListener
        {

            final IUiListener a;
            final Object b;
            final AuthAgent.FeedConfirmListener c;

            public void onClick(View view)
            {
                c.a();
                if (d != null && d.isShowing())
                {
                    d.dismiss();
                }
                if (a != null)
                {
                    a.onComplete(b);
                }
            }

            _cls1(Dialog dialog, IUiListener iuilistener, Object obj)
            {
                c = AuthAgent.FeedConfirmListener.this;
                a = iuilistener;
                b = obj;
                super(dialog);
            }
        }

        class _cls2 extends ButtonListener
        {

            final IUiListener a;
            final Object b;
            final AuthAgent.FeedConfirmListener c;

            public void onClick(View view)
            {
                if (d != null && d.isShowing())
                {
                    d.dismiss();
                }
                if (a != null)
                {
                    a.onComplete(b);
                }
            }

            _cls2(Dialog dialog, IUiListener iuilistener, Object obj)
            {
                c = AuthAgent.FeedConfirmListener.this;
                a = iuilistener;
                b = obj;
                super(dialog);
            }
        }

        class _cls3
            implements android.content.DialogInterface.OnCancelListener
        {

            final IUiListener a;
            final Object b;
            final AuthAgent.FeedConfirmListener c;

            public void onCancel(DialogInterface dialoginterface)
            {
                if (a != null)
                {
                    a.onComplete(b);
                }
            }

            _cls3(IUiListener iuilistener, Object obj)
            {
                c = AuthAgent.FeedConfirmListener.this;
                a = iuilistener;
                b = obj;
                super();
            }
        }

        Object obj1;
        ColorDrawable colordrawable;
        try
        {
            obj1 = ((PackageManager) (obj2)).getPackageInfo(AuthAgent.e(b).getPackageName(), 0);
        }
        catch (android.content.pm.eption eption)
        {
            eption.printStackTrace();
            eption = null;
        }
        if (obj1 != null)
        {
            drawable = ((PackageInfo) (obj1)).applicationInfo.loadIcon(((PackageManager) (obj2)));
        }
        obj1 = new _cls1(dialog, iuilistener, obj);
        obj2 = new _cls2(dialog, iuilistener, obj);
        colordrawable = new ColorDrawable();
        colordrawable.setAlpha(0);
        dialog.getWindow().setBackgroundDrawable(colordrawable);
        dialog.setContentView(a(((Context) (AuthAgent.e(b))), drawable, s, ((android.view.irmListener.b) (obj1)), ((android.view.irmListener.b) (obj2))));
        dialog.setOnCancelListener(new _cls3(iuilistener, obj));
        if (AuthAgent.e(b) != null && !AuthAgent.e(b).isFinishing())
        {
            dialog.show();
        }
    }

    protected void a()
    {
        android.os.Bundle bundle = AuthAgent.g(b);
        HttpUtils.requestAsync(AuthAgent.h(b), AuthAgent.e(b), "http://appsupport.qq.com/cgi-bin/qzapps/mapp_addapp.cgi", bundle, "POST", null);
    }

    public void onCancel()
    {
        if (a != null)
        {
            a.onCancel();
        }
    }

    public void onComplete(Object obj)
    {
        Object obj1;
        boolean flag;
        boolean flag1;
        flag1 = false;
        flag = false;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_98;
        }
        obj1 = (JSONObject)obj;
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_98;
        }
        if (((JSONObject) (obj1)).getInt("sendinstall") == 1)
        {
            flag = true;
        }
        flag1 = flag;
        try
        {
            obj1 = ((JSONObject) (obj1)).getString("installwording");
        }
        catch (JSONException jsonexception)
        {
            f.d("FeedConfirm", "There is no value for sendinstall.");
            flag = flag1;
            jsonexception = "";
        }
        obj1 = URLDecoder.decode(((String) (obj1)));
        f.b("TAG", (new StringBuilder()).append(" WORDING = ").append(((String) (obj1))).append("xx").toString());
        if (flag && !TextUtils.isEmpty(((CharSequence) (obj1))))
        {
            a(((String) (obj1)), a, obj);
        } else
        if (a != null)
        {
            a.onComplete(obj);
            return;
        }
    }

    public void onError(UiError uierror)
    {
        if (a != null)
        {
            a.onError(uierror);
        }
    }

    public ButtonListener.d(AuthAgent authagent, IUiListener iuilistener)
    {
        b = authagent;
        super();
        a = iuilistener;
    }
}
