// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.auth;

import android.os.SystemClock;
import com.tencent.open.b.g;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.connect.auth:
//            AuthDialog

private class e
    implements IUiListener
{

    String a;
    String b;
    final AuthDialog c;
    private String d;
    private IUiListener e;

    static void a(e e1, String s)
    {
        e1.a(s);
    }

    private void a(String s)
    {
        try
        {
            onComplete(Util.parseJson(s));
            return;
        }
        catch (JSONException jsonexception)
        {
            jsonexception.printStackTrace();
        }
        onError(new UiError(-4, "\u670D\u52A1\u5668\u8FD4\u56DE\u6570\u636E\u683C\u5F0F\u6709\u8BEF!", s));
    }

    public void onCancel()
    {
        if (e != null)
        {
            e.onCancel();
            e = null;
        }
    }

    public void onComplete(Object obj)
    {
        obj = (JSONObject)obj;
        g.a().a((new StringBuilder()).append(d).append("_H5").toString(), SystemClock.elapsedRealtime(), 0L, 0L, ((JSONObject) (obj)).optInt("ret", -6), a, false);
        if (e != null)
        {
            e.onComplete(obj);
            e = null;
        }
    }

    public void onError(UiError uierror)
    {
        String s;
        if (uierror.errorMessage != null)
        {
            s = (new StringBuilder()).append(uierror.errorMessage).append(a).toString();
        } else
        {
            s = a;
        }
        g.a().a((new StringBuilder()).append(d).append("_H5").toString(), SystemClock.elapsedRealtime(), 0L, 0L, uierror.errorCode, s, false);
        AuthDialog.a(c, s);
        if (e != null)
        {
            e.onError(uierror);
            e = null;
        }
    }

    public (AuthDialog authdialog, String s, String s1, String s2, IUiListener iuilistener)
    {
        c = authdialog;
        super();
        d = s;
        a = s1;
        b = s2;
        e = iuilistener;
    }
}
