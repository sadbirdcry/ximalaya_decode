// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.auth;

import android.content.Context;
import android.content.SharedPreferences;
import android.webkit.CookieSyncManager;
import com.tencent.connect.a.a;
import com.tencent.open.a.f;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.connect.auth:
//            AuthAgent, QQToken

private class c
    implements IUiListener
{

    final AuthAgent a;
    private final IUiListener b;
    private final boolean c;
    private final Context d;

    public void onCancel()
    {
        f.b("openSDK_LOG", "OpenUi, TokenListener() onCancel");
        b.onCancel();
        f.b();
    }

    public void onComplete(Object obj)
    {
        f.b("openSDK_LOG", "OpenUi, TokenListener() onComplete");
        obj = (JSONObject)obj;
        String s;
        String s1;
        String s2;
        s = ((JSONObject) (obj)).getString("access_token");
        s1 = ((JSONObject) (obj)).getString("expires_in");
        s2 = ((JSONObject) (obj)).getString("openid");
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_91;
        }
        if (com.tencent.connect.auth.AuthAgent.a(a) == null || s2 == null)
        {
            break MISSING_BLOCK_LABEL_91;
        }
        AuthAgent.b(a).setAccessToken(s, s1);
        AuthAgent.c(a).setOpenId(s2);
        com.tencent.connect.a.a.d(d, AuthAgent.d(a));
        s = ((JSONObject) (obj)).getString("pf");
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_131;
        }
        d.getSharedPreferences("pfStore", 0).edit().putString("pf", s).commit();
_L1:
        if (c)
        {
            CookieSyncManager.getInstance().sync();
        }
_L2:
        b.onComplete(obj);
        a.releaseResource();
        f.b();
        return;
        Object obj1;
        obj1;
        ((Exception) (obj1)).printStackTrace();
        f.b("openSDK_LOG", "OpenUi, TokenListener() onComplete error", ((Throwable) (obj1)));
          goto _L1
        obj1;
        ((JSONException) (obj1)).printStackTrace();
        f.b("openSDK_LOG", "OpenUi, TokenListener() onComplete error", ((Throwable) (obj1)));
          goto _L2
    }

    public void onError(UiError uierror)
    {
        f.b("openSDK_LOG", "OpenUi, TokenListener() onError");
        b.onError(uierror);
        f.b();
    }

    public (AuthAgent authagent, Context context, IUiListener iuilistener, boolean flag, boolean flag1)
    {
        a = authagent;
        super();
        d = context;
        b = iuilistener;
        c = flag;
        f.b("openSDK_LOG", "OpenUi, TokenListener()");
    }
}
