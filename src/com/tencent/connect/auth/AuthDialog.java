// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.auth;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.open.a.f;
import com.tencent.open.b.g;
import com.tencent.open.c.c;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.Util;
import com.tencent.open.web.security.SecureJsInterface;
import com.tencent.open.web.security.b;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.connect.auth:
//            QQToken, AuthMap

public class AuthDialog extends Dialog
{
    private class LoginWebViewClient extends WebViewClient
    {

        final AuthDialog a;

        public void onPageFinished(WebView webview, String s1)
        {
            super.onPageFinished(webview, s1);
            com.tencent.open.a.f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->onPageFinished, url: ").append(s1).toString());
            com.tencent.connect.auth.AuthDialog.g(a).setVisibility(8);
            if (AuthDialog.e(a) != null)
            {
                AuthDialog.e(a).setVisibility(0);
            }
            if (!TextUtils.isEmpty(s1))
            {
                AuthDialog.n(a).removeCallbacks((Runnable)AuthDialog.p(a).remove(s1));
            }
        }

        public void onPageStarted(WebView webview, String s1, Bitmap bitmap)
        {
            com.tencent.open.a.f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->onPageStarted, url: ").append(s1).toString());
            super.onPageStarted(webview, s1, bitmap);
            com.tencent.connect.auth.AuthDialog.g(a).setVisibility(0);
            AuthDialog.a(a, SystemClock.elapsedRealtime());
            if (!TextUtils.isEmpty(AuthDialog.i(a)))
            {
                AuthDialog.n(a).removeCallbacks((Runnable)AuthDialog.p(a).remove(AuthDialog.i(a)));
            }
            com.tencent.connect.auth.AuthDialog.c(a, s1);
            webview = a. new TimeOutRunable(AuthDialog.i(a));
            AuthDialog.p(a).put(s1, webview);
            AuthDialog.n(a).postDelayed(webview, 0x1d4c0L);
        }

        public void onReceivedError(WebView webview, int i1, String s1, String s2)
        {
            super.onReceivedError(webview, i1, s1, s2);
            com.tencent.open.a.f.c("openSDK_LOG.authDlg", (new StringBuilder()).append("-->onReceivedError, errorCode: ").append(i1).append(" | description: ").append(s1).toString());
            if (!Util.checkNetWork(AuthDialog.a(a)))
            {
                com.tencent.connect.auth.AuthDialog.f(a).onError(new UiError(9001, "\u5F53\u524D\u7F51\u7EDC\u4E0D\u53EF\u7528\uFF0C\u8BF7\u7A0D\u540E\u91CD\u8BD5\uFF01", s2));
                a.dismiss();
                return;
            }
            if (!AuthDialog.i(a).startsWith("http://qzs.qq.com/open/mobile/login/qzsjump.html?"))
            {
                long l1 = SystemClock.elapsedRealtime();
                long l2 = AuthDialog.j(a);
                if (AuthDialog.k(a) < 1 && l1 - l2 < AuthDialog.l(a))
                {
                    AuthDialog.m(a);
                    class _cls1
                        implements Runnable
                    {

                        final LoginWebViewClient a;

                        public void run()
                        {
                            AuthDialog.e(a.a).loadUrl(AuthDialog.i(a.a));
                        }

                _cls1()
                {
                    a = LoginWebViewClient.this;
                    super();
                }
                    }

                    AuthDialog.n(a).postDelayed(new _cls1(), 500L);
                    return;
                } else
                {
                    AuthDialog.e(a).loadUrl(AuthDialog.o(a));
                    return;
                }
            } else
            {
                com.tencent.connect.auth.AuthDialog.f(a).onError(new UiError(i1, s1, s2));
                a.dismiss();
                return;
            }
        }

        public void onReceivedSslError(WebView webview, SslErrorHandler sslerrorhandler, SslError sslerror)
        {
            sslerrorhandler.cancel();
            com.tencent.connect.auth.AuthDialog.f(a).onError(new UiError(sslerror.getPrimaryError(), "\u8BF7\u6C42\u4E0D\u5408\u6CD5\uFF0C\u8BF7\u68C0\u67E5\u624B\u673A\u5B89\u5168\u8BBE\u7F6E\uFF0C\u5982\u7CFB\u7EDF\u65F6\u95F4\u3001\u4EE3\u7406\u7B49\u3002", "ssl error"));
            a.dismiss();
        }

        public boolean shouldOverrideUrlLoading(WebView webview, String s1)
        {
            com.tencent.open.a.f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->Redirect URL: ").append(s1).toString());
            if (!s1.startsWith("auth://browser")) goto _L2; else goto _L1
_L1:
            webview = Util.parseUrlToJson(s1);
            AuthDialog.a(a, com.tencent.connect.auth.AuthDialog.b(a));
            if (!com.tencent.connect.auth.AuthDialog.c(a)) goto _L4; else goto _L3
_L3:
            return true;
_L4:
            if (webview.optString("fail_cb", null) != null)
            {
                a.callJs(webview.optString("fail_cb"), "");
            } else
            if (webview.optInt("fall_to_wv") == 1)
            {
                s1 = a;
                if (AuthDialog.d(a).indexOf("?") > -1)
                {
                    webview = "&";
                } else
                {
                    webview = "?";
                }
                AuthDialog.a(s1, webview);
                AuthDialog.a(a, "browser_error=1");
                AuthDialog.e(a).loadUrl(AuthDialog.d(a));
            } else
            {
                webview = webview.optString("redir", null);
                if (webview != null)
                {
                    AuthDialog.e(a).loadUrl(webview);
                }
            }
            if (true) goto _L3; else goto _L2
_L2:
            if (s1.startsWith("auth://tauth.qq.com/"))
            {
                com.tencent.connect.auth.AuthDialog.f(a).onComplete(Util.parseUrlToJson(s1));
                a.dismiss();
                return true;
            }
            if (s1.startsWith("auth://cancel"))
            {
                com.tencent.connect.auth.AuthDialog.f(a).onCancel();
                a.dismiss();
                return true;
            }
            if (s1.startsWith("auth://close"))
            {
                a.dismiss();
                return true;
            }
            if (s1.startsWith("download://"))
            {
                try
                {
                    webview = new Intent("android.intent.action.VIEW", Uri.parse(Uri.decode(s1.substring("download://".length()))));
                    webview.addFlags(0x10000000);
                    AuthDialog.a(a).startActivity(webview);
                }
                // Misplaced declaration of an exception variable
                catch (WebView webview)
                {
                    com.tencent.open.a.f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->start download activity exception, e: ").append(webview.getMessage()).toString());
                }
                return true;
            }
            if (!s1.startsWith("auth://progress"))
            {
                break MISSING_BLOCK_LABEL_471;
            }
            webview = Uri.parse(s1).getPathSegments();
            if (webview.isEmpty())
            {
                return true;
            }
            int i1;
            try
            {
                i1 = Integer.valueOf((String)webview.get(0)).intValue();
            }
            // Misplaced declaration of an exception variable
            catch (WebView webview)
            {
                return true;
            }
            if (i1 != 0)
            {
                break MISSING_BLOCK_LABEL_449;
            }
            com.tencent.connect.auth.AuthDialog.g(a).setVisibility(8);
            AuthDialog.e(a).setVisibility(0);
            break MISSING_BLOCK_LABEL_555;
            if (i1 != 1)
            {
                break MISSING_BLOCK_LABEL_555;
            }
            com.tencent.connect.auth.AuthDialog.g(a).setVisibility(0);
            break MISSING_BLOCK_LABEL_555;
            if (s1.startsWith("auth://onLoginSubmit"))
            {
                try
                {
                    webview = Uri.parse(s1).getPathSegments();
                    if (!webview.isEmpty())
                    {
                        com.tencent.connect.auth.AuthDialog.b(a, (String)webview.get(0));
                    }
                }
                // Misplaced declaration of an exception variable
                catch (WebView webview) { }
                return true;
            }
            if (AuthDialog.h(a).a(AuthDialog.e(a), s1))
            {
                return true;
            } else
            {
                com.tencent.open.a.f.c("openSDK_LOG.authDlg", "-->Redirect URL: return false");
                return false;
            }
            return true;
        }

        private LoginWebViewClient()
        {
            a = AuthDialog.this;
            super();
        }

        LoginWebViewClient(_cls1 _pcls1)
        {
            this();
        }
    }

    private class OnTimeListener
        implements IUiListener
    {

        String a;
        String b;
        final AuthDialog c;
        private String d;
        private IUiListener e;

        static void a(OnTimeListener ontimelistener, String s1)
        {
            ontimelistener.a(s1);
        }

        private void a(String s1)
        {
            try
            {
                onComplete(Util.parseJson(s1));
                return;
            }
            catch (JSONException jsonexception)
            {
                jsonexception.printStackTrace();
            }
            onError(new UiError(-4, "\u670D\u52A1\u5668\u8FD4\u56DE\u6570\u636E\u683C\u5F0F\u6709\u8BEF!", s1));
        }

        public void onCancel()
        {
            if (e != null)
            {
                e.onCancel();
                e = null;
            }
        }

        public void onComplete(Object obj)
        {
            obj = (JSONObject)obj;
            com.tencent.open.b.g.a().a((new StringBuilder()).append(d).append("_H5").toString(), SystemClock.elapsedRealtime(), 0L, 0L, ((JSONObject) (obj)).optInt("ret", -6), a, false);
            if (e != null)
            {
                e.onComplete(obj);
                e = null;
            }
        }

        public void onError(UiError uierror)
        {
            String s1;
            if (uierror.errorMessage != null)
            {
                s1 = (new StringBuilder()).append(uierror.errorMessage).append(a).toString();
            } else
            {
                s1 = a;
            }
            com.tencent.open.b.g.a().a((new StringBuilder()).append(d).append("_H5").toString(), SystemClock.elapsedRealtime(), 0L, 0L, uierror.errorCode, s1, false);
            AuthDialog.a(c, s1);
            if (e != null)
            {
                e.onError(uierror);
                e = null;
            }
        }

        public OnTimeListener(String s1, String s2, String s3, IUiListener iuilistener)
        {
            c = AuthDialog.this;
            super();
            d = s1;
            a = s2;
            b = s3;
            e = iuilistener;
        }
    }

    private class THandler extends Handler
    {

        final AuthDialog a;
        private OnTimeListener b;

        public void handleMessage(Message message)
        {
            switch (message.what)
            {
            default:
                return;

            case 1: // '\001'
                OnTimeListener.a(b, (String)message.obj);
                return;

            case 2: // '\002'
                b.onCancel();
                return;

            case 3: // '\003'
                AuthDialog.a(AuthDialog.a(a), (String)message.obj);
                break;
            }
        }

        public THandler(OnTimeListener ontimelistener, Looper looper)
        {
            a = AuthDialog.this;
            super(looper);
            b = ontimelistener;
        }
    }

    class TimeOutRunable
        implements Runnable
    {

        String a;
        final AuthDialog b;

        public void run()
        {
            com.tencent.open.a.f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->timeoutUrl: ").append(a).append(" | mRetryUrl: ").append(AuthDialog.i(b)).toString());
            if (a.equals(AuthDialog.i(b)))
            {
                com.tencent.connect.auth.AuthDialog.f(b).onError(new UiError(9002, "\u8BF7\u6C42\u9875\u9762\u8D85\u65F6\uFF0C\u8BF7\u7A0D\u540E\u91CD\u8BD5\uFF01", AuthDialog.i(b)));
                b.dismiss();
            }
        }

        public TimeOutRunable(String s1)
        {
            b = AuthDialog.this;
            super();
            a = "";
            a = s1;
        }
    }


    private String a;
    private OnTimeListener b;
    private IUiListener c;
    private Handler d;
    private FrameLayout e;
    private LinearLayout f;
    private FrameLayout g;
    private ProgressBar h;
    private String i;
    private c j;
    private Context k;
    private b l;
    private boolean m;
    private int n;
    private String o;
    private String p;
    private long q;
    private long r;
    private HashMap s;

    public AuthDialog(Context context, String s1, String s2, IUiListener iuilistener, QQToken qqtoken)
    {
        super(context, 0x1030010);
        m = false;
        q = 0L;
        r = 30000L;
        k = context;
        a = s2;
        b = new OnTimeListener(s1, s2, qqtoken.getAppId(), iuilistener);
        d = new THandler(b, context.getMainLooper());
        c = iuilistener;
        i = s1;
        l = new b();
        getWindow().setSoftInputMode(32);
    }

    static long a(AuthDialog authdialog, long l1)
    {
        authdialog.q = l1;
        return l1;
    }

    static Context a(AuthDialog authdialog)
    {
        return authdialog.k;
    }

    private String a()
    {
        String s1 = a.substring(a.indexOf("?") + 1);
        s1 = (new StringBuilder()).append("http://qzs.qq.com/open/mobile/login/qzsjump.html?").append(s1).toString();
        com.tencent.open.a.f.c("openSDK_LOG.authDlg", "-->generateDownloadUrl, url: http://qzs.qq.com/open/mobile/login/qzsjump.html?");
        return s1;
    }

    static String a(AuthDialog authdialog, Object obj)
    {
        obj = (new StringBuilder()).append(authdialog.a).append(obj).toString();
        authdialog.a = ((String) (obj));
        return ((String) (obj));
    }

    static String a(AuthDialog authdialog, String s1)
    {
        return authdialog.a(s1);
    }

    private String a(String s1)
    {
        s1 = new StringBuilder(s1);
        if (!TextUtils.isEmpty(p) && p.length() >= 4)
        {
            String s2 = p.substring(p.length() - 4);
            s1.append("_u_").append(s2);
        }
        return s1.toString();
    }

    static void a(Context context, String s1)
    {
        b(context, s1);
    }

    static boolean a(AuthDialog authdialog, boolean flag)
    {
        authdialog.m = flag;
        return flag;
    }

    static String b(AuthDialog authdialog, String s1)
    {
        authdialog.p = s1;
        return s1;
    }

    private void b()
    {
        c();
        android.widget.FrameLayout.LayoutParams layoutparams = new android.widget.FrameLayout.LayoutParams(-1, -1);
        j = new c(k);
        j.setLayoutParams(layoutparams);
        e = new FrameLayout(k);
        layoutparams.gravity = 17;
        e.setLayoutParams(layoutparams);
        e.addView(j);
        e.addView(g);
        setContentView(e);
    }

    private static void b(Context context, String s1)
    {
        try
        {
            s1 = Util.parseJson(s1);
            int i1 = s1.getInt("type");
            s1 = s1.getString("msg");
            Toast.makeText(context.getApplicationContext(), s1, i1).show();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    static boolean b(AuthDialog authdialog)
    {
        return authdialog.e();
    }

    static String c(AuthDialog authdialog, String s1)
    {
        authdialog.o = s1;
        return s1;
    }

    private void c()
    {
        h = new ProgressBar(k);
        Object obj = new android.widget.LinearLayout.LayoutParams(-2, -2);
        h.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        f = new LinearLayout(k);
        obj = null;
        if (i.equals("action_login"))
        {
            Object obj1 = new android.widget.LinearLayout.LayoutParams(-2, -2);
            obj1.gravity = 16;
            obj1.leftMargin = 5;
            obj = new TextView(k);
            if (Locale.getDefault().getLanguage().equals("zh"))
            {
                ((TextView) (obj)).setText("\u767B\u5F55\u4E2D...");
            } else
            {
                ((TextView) (obj)).setText("Logging in...");
            }
            ((TextView) (obj)).setTextColor(Color.rgb(255, 255, 255));
            ((TextView) (obj)).setTextSize(18F);
            ((TextView) (obj)).setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj1)));
        }
        obj1 = new android.widget.FrameLayout.LayoutParams(-2, -2);
        obj1.gravity = 17;
        f.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj1)));
        f.addView(h);
        if (obj != null)
        {
            f.addView(((android.view.View) (obj)));
        }
        g = new FrameLayout(k);
        obj = new android.widget.FrameLayout.LayoutParams(-1, -2);
        obj.leftMargin = 80;
        obj.rightMargin = 80;
        obj.topMargin = 40;
        obj.bottomMargin = 40;
        obj.gravity = 17;
        g.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        g.setBackgroundResource(0x1080000);
        g.addView(f);
    }

    static boolean c(AuthDialog authdialog)
    {
        return authdialog.m;
    }

    static String d(AuthDialog authdialog)
    {
        return authdialog.a;
    }

    private void d()
    {
        j.setVerticalScrollBarEnabled(false);
        j.setHorizontalScrollBarEnabled(false);
        j.setWebViewClient(new LoginWebViewClient(null));
        j.setWebChromeClient(new WebChromeClient());
        j.clearFormData();
        j.clearSslPreferences();
        j.setOnLongClickListener(new _cls1());
        j.setOnTouchListener(new _cls2());
        WebSettings websettings = j.getSettings();
        websettings.setSavePassword(false);
        websettings.setSaveFormData(false);
        websettings.setCacheMode(-1);
        websettings.setNeedInitialFocus(false);
        websettings.setBuiltInZoomControls(true);
        websettings.setSupportZoom(true);
        websettings.setRenderPriority(android.webkit.WebSettings.RenderPriority.HIGH);
        websettings.setJavaScriptEnabled(true);
        websettings.setDatabaseEnabled(true);
        websettings.setDatabasePath(k.getDir("databases", 0).getPath());
        websettings.setDomStorageEnabled(true);
        com.tencent.open.a.f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->mUrl : ").append(a).toString());
        o = a;
        j.loadUrl(a);
        j.setVisibility(4);
        j.getSettings().setSavePassword(false);
        l.a(new SecureJsInterface(), "SecureJsInterface");
        SecureJsInterface.isPWDEdit = false;
        super.setOnDismissListener(new _cls3());
    }

    static c e(AuthDialog authdialog)
    {
        return authdialog.j;
    }

    private boolean e()
    {
        Object obj = AuthMap.getInstance();
        String s1 = ((AuthMap) (obj)).makeKey();
        Object obj1 = new AuthMap.Auth();
        obj1.listener = c;
        obj1.dialog = this;
        obj1.key = s1;
        obj = ((AuthMap) (obj)).set(((AuthMap.Auth) (obj1)));
        obj1 = a.substring(0, a.indexOf("?"));
        Bundle bundle = Util.parseUrl(a);
        bundle.putString("token_key", s1);
        bundle.putString("serial", ((String) (obj)));
        bundle.putString("browser", "1");
        a = (new StringBuilder()).append(((String) (obj1))).append("?").append(Util.encodeUrl(bundle)).toString();
        return Util.openBrowser(k, a);
    }

    static OnTimeListener f(AuthDialog authdialog)
    {
        return authdialog.b;
    }

    static FrameLayout g(AuthDialog authdialog)
    {
        return authdialog.g;
    }

    static b h(AuthDialog authdialog)
    {
        return authdialog.l;
    }

    static String i(AuthDialog authdialog)
    {
        return authdialog.o;
    }

    static long j(AuthDialog authdialog)
    {
        return authdialog.q;
    }

    static int k(AuthDialog authdialog)
    {
        return authdialog.n;
    }

    static long l(AuthDialog authdialog)
    {
        return authdialog.r;
    }

    static int m(AuthDialog authdialog)
    {
        int i1 = authdialog.n;
        authdialog.n = i1 + 1;
        return i1;
    }

    static Handler n(AuthDialog authdialog)
    {
        return authdialog.d;
    }

    static String o(AuthDialog authdialog)
    {
        return authdialog.a();
    }

    static HashMap p(AuthDialog authdialog)
    {
        return authdialog.s;
    }

    public void callJs(String s1, String s2)
    {
        s1 = (new StringBuilder()).append("javascript:").append(s1).append("(").append(s2).append(");void(").append(System.currentTimeMillis()).append(");").toString();
        j.loadUrl(s1);
    }

    public void dismiss()
    {
        s.clear();
        d.removeCallbacksAndMessages(null);
        if (isShowing())
        {
            super.dismiss();
        }
        if (j != null)
        {
            j.destroy();
            j = null;
        }
    }

    public void onBackPressed()
    {
        if (!m)
        {
            b.onCancel();
        }
        super.onBackPressed();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        b();
        d();
        s = new HashMap();
    }

    protected void onStop()
    {
        super.onStop();
    }

    static 
    {
        Context context = Global.getContext();
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_110;
        }
        if ((new File((new StringBuilder()).append(context.getFilesDir().toString()).append("/").append("libwbsafeedit.so").toString())).exists())
        {
            System.load((new StringBuilder()).append(context.getFilesDir().toString()).append("/").append("libwbsafeedit.so").toString());
            com.tencent.open.a.f.b("openSDK_LOG.authDlg", "-->load wbsafeedit lib success.");
            break MISSING_BLOCK_LABEL_117;
        }
        try
        {
            com.tencent.open.a.f.b("openSDK_LOG.authDlg", "-->load wbsafeedit lib fail, because so is not exists.");
        }
        catch (Exception exception)
        {
            com.tencent.open.a.f.b("openSDK_LOG.authDlg", "-->load wbsafeedit lib error.", exception);
        }
        break MISSING_BLOCK_LABEL_117;
        com.tencent.open.a.f.b("openSDK_LOG.authDlg", "-->load wbsafeedit lib fail, because context is null.");
    }

    private class _cls1
        implements android.view.View.OnLongClickListener
    {

        final AuthDialog a;

        public boolean onLongClick(View view)
        {
            return true;
        }

        _cls1()
        {
            a = AuthDialog.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnTouchListener
    {

        final AuthDialog a;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            motionevent.getAction();
            JVM INSTR tableswitch 0 1: default 28
        //                       0 30
        //                       1 30;
               goto _L1 _L2 _L2
_L1:
            return false;
_L2:
            if (!view.hasFocus())
            {
                view.requestFocus();
            }
            if (true) goto _L1; else goto _L3
_L3:
        }

        _cls2()
        {
            a = AuthDialog.this;
            super();
        }
    }


    private class _cls3
        implements android.content.DialogInterface.OnDismissListener
    {

        final AuthDialog a;

        public void onDismiss(DialogInterface dialoginterface)
        {
            try
            {
                JniInterface.clearAllPWD();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (DialogInterface dialoginterface)
            {
                return;
            }
        }

        _cls3()
        {
            a = AuthDialog.this;
            super();
        }
    }

}
