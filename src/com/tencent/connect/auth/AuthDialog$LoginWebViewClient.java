// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect.auth;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import com.tencent.open.a.f;
import com.tencent.open.c.c;
import com.tencent.open.utils.Util;
import com.tencent.open.web.security.b;
import com.tencent.tauth.UiError;
import java.util.HashMap;
import java.util.List;
import org.json.JSONObject;

// Referenced classes of package com.tencent.connect.auth:
//            AuthDialog

private class <init> extends WebViewClient
{

    final AuthDialog a;

    public void onPageFinished(WebView webview, String s)
    {
        super.onPageFinished(webview, s);
        f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->onPageFinished, url: ").append(s).toString());
        AuthDialog.g(a).setVisibility(8);
        if (AuthDialog.e(a) != null)
        {
            AuthDialog.e(a).setVisibility(0);
        }
        if (!TextUtils.isEmpty(s))
        {
            AuthDialog.n(a).removeCallbacks((Runnable)AuthDialog.p(a).remove(s));
        }
    }

    public void onPageStarted(WebView webview, String s, Bitmap bitmap)
    {
        f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->onPageStarted, url: ").append(s).toString());
        super.onPageStarted(webview, s, bitmap);
        AuthDialog.g(a).setVisibility(0);
        AuthDialog.a(a, SystemClock.elapsedRealtime());
        if (!TextUtils.isEmpty(AuthDialog.i(a)))
        {
            AuthDialog.n(a).removeCallbacks((Runnable)AuthDialog.p(a).remove(AuthDialog.i(a)));
        }
        com.tencent.connect.auth.AuthDialog.c(a, s);
        webview = new t>(a, AuthDialog.i(a));
        AuthDialog.p(a).put(s, webview);
        AuthDialog.n(a).postDelayed(webview, 0x1d4c0L);
    }

    public void onReceivedError(WebView webview, int i, String s, String s1)
    {
        super.onReceivedError(webview, i, s, s1);
        f.c("openSDK_LOG.authDlg", (new StringBuilder()).append("-->onReceivedError, errorCode: ").append(i).append(" | description: ").append(s).toString());
        if (!Util.checkNetWork(AuthDialog.a(a)))
        {
            com.tencent.connect.auth.AuthDialog.f(a).ror(new UiError(9001, "\u5F53\u524D\u7F51\u7EDC\u4E0D\u53EF\u7528\uFF0C\u8BF7\u7A0D\u540E\u91CD\u8BD5\uFF01", s1));
            a.dismiss();
            return;
        }
        if (!AuthDialog.i(a).startsWith("http://qzs.qq.com/open/mobile/login/qzsjump.html?"))
        {
            long l = SystemClock.elapsedRealtime();
            long l1 = AuthDialog.j(a);
            if (AuthDialog.k(a) < 1 && l - l1 < AuthDialog.l(a))
            {
                AuthDialog.m(a);
                class _cls1
                    implements Runnable
                {

                    final AuthDialog.LoginWebViewClient a;

                    public void run()
                    {
                        AuthDialog.e(a.a).loadUrl(AuthDialog.i(a.a));
                    }

            _cls1()
            {
                a = AuthDialog.LoginWebViewClient.this;
                super();
            }
                }

                AuthDialog.n(a).postDelayed(new _cls1(), 500L);
                return;
            } else
            {
                AuthDialog.e(a).loadUrl(AuthDialog.o(a));
                return;
            }
        } else
        {
            com.tencent.connect.auth.AuthDialog.f(a).ror(new UiError(i, s, s1));
            a.dismiss();
            return;
        }
    }

    public void onReceivedSslError(WebView webview, SslErrorHandler sslerrorhandler, SslError sslerror)
    {
        sslerrorhandler.cancel();
        com.tencent.connect.auth.AuthDialog.f(a).ror(new UiError(sslerror.getPrimaryError(), "\u8BF7\u6C42\u4E0D\u5408\u6CD5\uFF0C\u8BF7\u68C0\u67E5\u624B\u673A\u5B89\u5168\u8BBE\u7F6E\uFF0C\u5982\u7CFB\u7EDF\u65F6\u95F4\u3001\u4EE3\u7406\u7B49\u3002", "ssl error"));
        a.dismiss();
    }

    public boolean shouldOverrideUrlLoading(WebView webview, String s)
    {
        f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->Redirect URL: ").append(s).toString());
        if (!s.startsWith("auth://browser")) goto _L2; else goto _L1
_L1:
        webview = Util.parseUrlToJson(s);
        AuthDialog.a(a, com.tencent.connect.auth.AuthDialog.b(a));
        if (!com.tencent.connect.auth.AuthDialog.c(a)) goto _L4; else goto _L3
_L3:
        return true;
_L4:
        if (webview.optString("fail_cb", null) != null)
        {
            a.callJs(webview.optString("fail_cb"), "");
        } else
        if (webview.optInt("fall_to_wv") == 1)
        {
            s = a;
            if (AuthDialog.d(a).indexOf("?") > -1)
            {
                webview = "&";
            } else
            {
                webview = "?";
            }
            AuthDialog.a(s, webview);
            AuthDialog.a(a, "browser_error=1");
            AuthDialog.e(a).loadUrl(AuthDialog.d(a));
        } else
        {
            webview = webview.optString("redir", null);
            if (webview != null)
            {
                AuthDialog.e(a).loadUrl(webview);
            }
        }
        if (true) goto _L3; else goto _L2
_L2:
        if (s.startsWith("auth://tauth.qq.com/"))
        {
            com.tencent.connect.auth.AuthDialog.f(a).mplete(Util.parseUrlToJson(s));
            a.dismiss();
            return true;
        }
        if (s.startsWith("auth://cancel"))
        {
            com.tencent.connect.auth.AuthDialog.f(a).ncel();
            a.dismiss();
            return true;
        }
        if (s.startsWith("auth://close"))
        {
            a.dismiss();
            return true;
        }
        if (s.startsWith("download://"))
        {
            try
            {
                webview = new Intent("android.intent.action.VIEW", Uri.parse(Uri.decode(s.substring("download://".length()))));
                webview.addFlags(0x10000000);
                AuthDialog.a(a).startActivity(webview);
            }
            // Misplaced declaration of an exception variable
            catch (WebView webview)
            {
                f.b("openSDK_LOG.authDlg", (new StringBuilder()).append("-->start download activity exception, e: ").append(webview.getMessage()).toString());
            }
            return true;
        }
        if (!s.startsWith("auth://progress"))
        {
            break MISSING_BLOCK_LABEL_471;
        }
        webview = Uri.parse(s).getPathSegments();
        if (webview.isEmpty())
        {
            return true;
        }
        int i;
        try
        {
            i = Integer.valueOf((String)webview.get(0)).intValue();
        }
        // Misplaced declaration of an exception variable
        catch (WebView webview)
        {
            return true;
        }
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_449;
        }
        AuthDialog.g(a).setVisibility(8);
        AuthDialog.e(a).setVisibility(0);
        break MISSING_BLOCK_LABEL_555;
        if (i != 1)
        {
            break MISSING_BLOCK_LABEL_555;
        }
        AuthDialog.g(a).setVisibility(0);
        break MISSING_BLOCK_LABEL_555;
        if (s.startsWith("auth://onLoginSubmit"))
        {
            try
            {
                webview = Uri.parse(s).getPathSegments();
                if (!webview.isEmpty())
                {
                    com.tencent.connect.auth.AuthDialog.b(a, (String)webview.get(0));
                }
            }
            // Misplaced declaration of an exception variable
            catch (WebView webview) { }
            return true;
        }
        if (AuthDialog.h(a).a(AuthDialog.e(a), s))
        {
            return true;
        } else
        {
            f.c("openSDK_LOG.authDlg", "-->Redirect URL: return false");
            return false;
        }
        return true;
    }

    private _cls1(AuthDialog authdialog)
    {
        a = authdialog;
        super();
    }

    a(AuthDialog authdialog, a a1)
    {
        this(authdialog);
    }
}
