// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.connect;

import android.content.Context;
import android.os.Bundle;
import com.tencent.connect.auth.QQAuth;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.HttpUtils;
import com.tencent.tauth.IUiListener;

public class UserInfo extends BaseApi
{

    public static final String GRAPH_OPEN_ID = "oauth2.0/m_me";

    public UserInfo(Context context, QQAuth qqauth, QQToken qqtoken)
    {
        super(qqauth, qqtoken);
    }

    public UserInfo(Context context, QQToken qqtoken)
    {
        super(qqtoken);
    }

    public void getOpenId(IUiListener iuilistener)
    {
        Bundle bundle = composeCGIParams();
        iuilistener = new com.tencent.connect.common.BaseApi.TempRequestListener(this, iuilistener);
        HttpUtils.requestAsync(mToken, Global.getContext(), "oauth2.0/m_me", bundle, "GET", iuilistener);
    }

    public void getTenPayAddr(IUiListener iuilistener)
    {
        Bundle bundle = composeCGIParams();
        bundle.putString("ver", "1");
        iuilistener = new com.tencent.connect.common.BaseApi.TempRequestListener(this, iuilistener);
        HttpUtils.requestAsync(mToken, Global.getContext(), "cft_info/get_tenpay_addr", bundle, "GET", iuilistener);
    }

    public void getUserInfo(IUiListener iuilistener)
    {
        Bundle bundle = composeCGIParams();
        iuilistener = new com.tencent.connect.common.BaseApi.TempRequestListener(this, iuilistener);
        HttpUtils.requestAsync(mToken, Global.getContext(), "user/get_simple_userinfo", bundle, "GET", iuilistener);
    }

    public void getVipUserInfo(IUiListener iuilistener)
    {
        Bundle bundle = composeCGIParams();
        iuilistener = new com.tencent.connect.common.BaseApi.TempRequestListener(this, iuilistener);
        HttpUtils.requestAsync(mToken, Global.getContext(), "user/get_vip_info", bundle, "GET", iuilistener);
    }

    public void getVipUserRichInfo(IUiListener iuilistener)
    {
        Bundle bundle = composeCGIParams();
        iuilistener = new com.tencent.connect.common.BaseApi.TempRequestListener(this, iuilistener);
        HttpUtils.requestAsync(mToken, Global.getContext(), "user/get_vip_rich_info", bundle, "GET", iuilistener);
    }
}
