// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.a;


// Referenced classes of package com.tencent.open.a:
//            c, h

public abstract class i
{

    private volatile int a;
    private volatile boolean b;
    private h c;

    public i()
    {
        this(c.a, true, h.a);
    }

    public i(int j, boolean flag, h h1)
    {
        a = c.a;
        b = true;
        c = h.a;
        a(j);
        a(flag);
        a(h1);
    }

    public void a(int j)
    {
        a = j;
    }

    protected abstract void a(int j, Thread thread, long l, String s, String s1, Throwable throwable);

    public void a(h h1)
    {
        c = h1;
    }

    public void a(boolean flag)
    {
        b = flag;
    }

    public void b(int j, Thread thread, long l, String s, String s1, Throwable throwable)
    {
        if (d() && com.tencent.open.a.d.a.a(a, j))
        {
            a(j, thread, l, s, s1, throwable);
        }
    }

    public boolean d()
    {
        return b;
    }

    public h e()
    {
        return c;
    }
}
