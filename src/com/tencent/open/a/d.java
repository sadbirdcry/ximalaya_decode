// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.a;

import android.os.Environment;
import android.os.StatFs;
import java.io.File;
import java.text.SimpleDateFormat;

public class com.tencent.open.a.d
{
    public static final class a
    {

        public static final boolean a(int i, int j)
        {
            return j == (i & j);
        }
    }

    public static final class b
    {

        public static boolean a(File file)
        {
            int i = 0;
            if (file == null) goto _L2; else goto _L1
_L1:
            if (!file.isFile()) goto _L4; else goto _L3
_L3:
            if (file.delete()) goto _L6; else goto _L5
_L5:
            file.deleteOnExit();
_L2:
            return false;
_L6:
            return true;
_L4:
            if (file.isDirectory())
            {
                File afile[] = file.listFiles();
                if (afile != null && afile.length != 0)
                {
                    for (int j = afile.length; i < j; i++)
                    {
                        a(afile[i]);
                    }

                    return file.delete();
                }
            }
            if (true) goto _L2; else goto _L7
_L7:
        }
    }

    public static final class c
    {

        public static boolean a()
        {
            String s = Environment.getExternalStorageState();
            return "mounted".equals(s) || "mounted_ro".equals(s);
        }

        public static d b()
        {
            if (!a())
            {
                return null;
            } else
            {
                return d.b(Environment.getExternalStorageDirectory());
            }
        }
    }

    public static class d
    {

        private File a;
        private long b;
        private long c;

        public static d b(File file)
        {
            d d1 = new d();
            d1.a(file);
            file = new StatFs(file.getAbsolutePath());
            long l = file.getBlockSize();
            long l1 = file.getBlockCount();
            long l2 = file.getAvailableBlocks();
            d1.a(l1 * l);
            d1.b(l2 * l);
            return d1;
        }

        public File a()
        {
            return a;
        }

        public void a(long l)
        {
            b = l;
        }

        public void a(File file)
        {
            a = file;
        }

        public long b()
        {
            return b;
        }

        public void b(long l)
        {
            c = l;
        }

        public long c()
        {
            return c;
        }

        public String toString()
        {
            return String.format("[%s : %d / %d]", new Object[] {
                a().getAbsolutePath(), Long.valueOf(c()), Long.valueOf(b())
            });
        }

        public d()
        {
        }
    }

    public static final class e
    {

        public static SimpleDateFormat a(String s)
        {
            return new SimpleDateFormat(s);
        }
    }

}
