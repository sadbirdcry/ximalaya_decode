// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open;

import android.os.Handler;
import android.os.Message;
import com.tencent.open.a.f;

// Referenced classes of package com.tencent.open:
//            TDialog

private class <init> extends <init>
{

    final TDialog this$0;

    public void onAddShare(String s)
    {
        f.b("TDialog", "onAddShare");
        onComplete(s);
    }

    public void onCancel(String s)
    {
        f.b("TDialog", (new StringBuilder()).append("onCancel --msg = ").append(s).toString());
        TDialog.b(TDialog.this).obtainMessage(2, s).sendToTarget();
        dismiss();
    }

    public void onCancelAddShare(String s)
    {
        f.b("TDialog", "onCancelAddShare");
        onCancel("cancel");
    }

    public void onCancelInvite()
    {
        f.b("TDialog", "onCancelInvite");
        onCancel("");
    }

    public void onCancelLogin()
    {
        onCancel("");
    }

    public void onComplete(String s)
    {
        TDialog.b(TDialog.this).obtainMessage(1, s).sendToTarget();
        f.e("onComplete", s);
        dismiss();
    }

    public void onInvite(String s)
    {
        onComplete(s);
    }

    public void onLoad(String s)
    {
        TDialog.b(TDialog.this).obtainMessage(4, s).sendToTarget();
    }

    public void showMsg(String s)
    {
        TDialog.b(TDialog.this).obtainMessage(3, s).sendToTarget();
    }

    private ()
    {
        this$0 = TDialog.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
