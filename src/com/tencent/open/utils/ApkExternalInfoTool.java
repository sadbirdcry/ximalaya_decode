// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.ProtocolException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Properties;
import java.util.zip.ZipException;

// Referenced classes of package com.tencent.open.utils:
//            ZipLong, ZipShort

public final class ApkExternalInfoTool
{
    private static class ApkExternalInfo
    {

        Properties a;
        byte b[];

        void a(byte abyte0[])
            throws IOException
        {
            if (abyte0 != null)
            {
                ByteBuffer bytebuffer = ByteBuffer.wrap(abyte0);
                int i = ApkExternalInfoTool.a().getBytes().length;
                byte abyte1[] = new byte[i];
                bytebuffer.get(abyte1);
                if (!ApkExternalInfoTool.a().equals(new ZipShort(abyte1)))
                {
                    throw new ProtocolException((new StringBuilder()).append("unknow protocl [").append(Arrays.toString(abyte0)).append("]").toString());
                }
                if (abyte0.length - i > 2)
                {
                    byte abyte2[] = new byte[2];
                    bytebuffer.get(abyte2);
                    int j = (new ZipShort(abyte2)).getValue();
                    if (abyte0.length - i - 2 >= j)
                    {
                        byte abyte3[] = new byte[j];
                        bytebuffer.get(abyte3);
                        a.load(new ByteArrayInputStream(abyte3));
                        i = abyte0.length - i - j - 2;
                        if (i > 0)
                        {
                            b = new byte[i];
                            bytebuffer.get(b);
                            return;
                        }
                    }
                }
            }
        }

        public String toString()
        {
            return (new StringBuilder()).append("ApkExternalInfo [p=").append(a).append(", otherData=").append(Arrays.toString(b)).append("]").toString();
        }

        private ApkExternalInfo()
        {
            a = new Properties();
        }

    }


    public static final String CHANNELID = "channelNo";
    private static final ZipLong a = new ZipLong(0x6054b50L);
    private static final ZipShort b = new ZipShort(38651);

    public ApkExternalInfoTool()
    {
    }

    static ZipShort a()
    {
        return b;
    }

    private static byte[] a(RandomAccessFile randomaccessfile)
        throws IOException
    {
        int i;
        long l;
        boolean flag = true;
        l = randomaccessfile.length() - 22L;
        randomaccessfile.seek(l);
        byte abyte0[] = a.getBytes();
        i = randomaccessfile.read();
        do
        {
            if (i == -1)
            {
                break MISSING_BLOCK_LABEL_161;
            }
            if (i == abyte0[0] && randomaccessfile.read() == abyte0[1] && randomaccessfile.read() == abyte0[2] && randomaccessfile.read() == abyte0[3])
            {
                i = ((flag) ? 1 : 0);
                break MISSING_BLOCK_LABEL_74;
            }
            l--;
            randomaccessfile.seek(l);
            i = randomaccessfile.read();
        } while (true);
_L2:
        if (i == 0)
        {
            throw new ZipException("archive is not a ZIP archive");
        }
        randomaccessfile.seek(16L + l + 4L);
        byte abyte1[] = new byte[2];
        randomaccessfile.readFully(abyte1);
        i = (new ZipShort(abyte1)).getValue();
        if (i == 0)
        {
            return null;
        } else
        {
            byte abyte2[] = new byte[i];
            randomaccessfile.read(abyte2);
            return abyte2;
        }
        i = 0;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public static String read(File file, String s)
        throws IOException
    {
        Object obj = null;
        RandomAccessFile randomaccessfile = new RandomAccessFile(file, "r");
        file = a(randomaccessfile);
        if (file != null) goto _L2; else goto _L1
_L1:
        file = obj;
        if (randomaccessfile != null)
        {
            randomaccessfile.close();
            file = obj;
        }
_L4:
        return file;
_L2:
        ApkExternalInfo apkexternalinfo = new ApkExternalInfo();
        apkexternalinfo.a(file);
        s = apkexternalinfo.a.getProperty(s);
        file = s;
        if (randomaccessfile == null) goto _L4; else goto _L3
_L3:
        randomaccessfile.close();
        return s;
        file;
        s = null;
_L6:
        if (s != null)
        {
            s.close();
        }
        throw file;
        file;
        s = randomaccessfile;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public static String readChannelId(File file)
        throws IOException
    {
        return read(file, "channelNo");
    }

}
