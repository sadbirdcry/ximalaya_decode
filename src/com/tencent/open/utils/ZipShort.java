// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;


public final class ZipShort
    implements Cloneable
{

    private int a;

    public ZipShort(int i)
    {
        a = i;
    }

    public ZipShort(byte abyte0[])
    {
        this(abyte0, 0);
    }

    public ZipShort(byte abyte0[], int i)
    {
        a = abyte0[i + 1] << 8 & 0xff00;
        a = a + (abyte0[i] & 0xff);
    }

    public boolean equals(Object obj)
    {
        while (obj == null || !(obj instanceof ZipShort) || a != ((ZipShort)obj).getValue()) 
        {
            return false;
        }
        return true;
    }

    public byte[] getBytes()
    {
        return (new byte[] {
            (byte)(a & 0xff), (byte)((a & 0xff00) >> 8)
        });
    }

    public int getValue()
    {
        return a;
    }

    public int hashCode()
    {
        return a;
    }
}
