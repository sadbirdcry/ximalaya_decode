// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;


public final class ZipLong
    implements Cloneable
{

    private long a;

    public ZipLong(long l)
    {
        a = l;
    }

    public ZipLong(byte abyte0[])
    {
        this(abyte0, 0);
    }

    public ZipLong(byte abyte0[], int i)
    {
        a = (long)(abyte0[i + 3] << 24) & 0xff000000L;
        a = a + (long)(abyte0[i + 2] << 16 & 0xff0000);
        a = a + (long)(abyte0[i + 1] << 8 & 0xff00);
        a = a + (long)(abyte0[i] & 0xff);
    }

    public boolean equals(Object obj)
    {
        while (obj == null || !(obj instanceof ZipLong) || a != ((ZipLong)obj).getValue()) 
        {
            return false;
        }
        return true;
    }

    public byte[] getBytes()
    {
        return (new byte[] {
            (byte)(int)(a & 255L), (byte)(int)((a & 65280L) >> 8), (byte)(int)((a & 0xff0000L) >> 16), (byte)(int)((a & 0xff000000L) >> 24)
        });
    }

    public long getValue()
    {
        return a;
    }

    public int hashCode()
    {
        return (int)a;
    }
}
