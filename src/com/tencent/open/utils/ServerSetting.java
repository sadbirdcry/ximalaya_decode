// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;
import com.tencent.open.a.f;
import java.lang.ref.WeakReference;
import java.net.URL;

public class ServerSetting
{

    public static final String APP_DETAIL_PAGE = "http://fusion.qq.com/cgi-bin/qzapps/unified_jump?appid=%1$s&from=%2$s&isOpenAppID=1";
    public static final String CGI_FETCH_QQ_URL = "http://fusion.qq.com/cgi-bin/qzapps/mapp_getappinfo.cgi";
    public static final String DEFAULT_CGI_AUTHORIZE = "http://openmobile.qq.com/oauth2.0/m_authorize?";
    public static final String DEFAULT_LOCAL_STORAGE_URI = "http://qzs.qq.com";
    public static final String DEFAULT_REDIRECT_URI = "auth://tauth.qq.com/";
    public static final String DEFAULT_URL_ASK = "http://qzs.qq.com/open/mobile/request/sdk_request.html?";
    public static final String DEFAULT_URL_BRAG = "http://qzs.qq.com/open/mobile/brag/sdk_brag.html?";
    public static final String DEFAULT_URL_GIFT = "http://qzs.qq.com/open/mobile/request/sdk_request.html?";
    public static final String DEFAULT_URL_GRAPH_BASE = "https://openmobile.qq.com/";
    public static final String DEFAULT_URL_INVITE = "http://qzs.qq.com/open/mobile/invite/sdk_invite.html?";
    public static final String DEFAULT_URL_REACTIVE = "http://qzs.qq.com/open/mobile/reactive/sdk_reactive.html?";
    public static final String DEFAULT_URL_REPORT = "http://wspeed.qq.com/w.cgi";
    public static final String DEFAULT_URL_SEND_STORY = "http://qzs.qq.com/open/mobile/sendstory/sdk_sendstory_v1.3.html?";
    public static final String DEFAULT_URL_VOICE = "http://qzs.qq.com/open/mobile/not_support.html?";
    public static final String DOWNLOAD_QQ_URL = "http://qzs.qq.com/open/mobile/login/qzsjump.html?";
    public static final String DOWNLOAD_QQ_URL_COMMON = "http://qzs.qq.com/open/mobile/sdk_common/down_qq.htm?";
    public static final int ENVIRONMENT_EXPERIENCE = 1;
    public static final int ENVIRONMENT_NORMOL = 0;
    public static final String KEY_HOST_ANALY = "analy.qq.com";
    public static final String KEY_HOST_APPIC = "appic.qq.com";
    public static final String KEY_HOST_APP_SUPPORT = "appsupport.qq.com";
    public static final String KEY_HOST_FUSION = "fusion.qq.com";
    public static final String KEY_HOST_I_GTIMG = "i.gtimg.cn";
    public static final String KEY_HOST_MAPP_QZONE = "mapp.qzone.qq.com";
    public static final String KEY_HOST_OPEN_MOBILE = "openmobile.qq.com";
    public static final String KEY_HOST_QZAPP_QLOGO = "qzapp.qlogo.cn";
    public static final String KEY_HOST_QZS_QQ = "qzs.qq.com";
    public static final String KEY_OPEN_ENV = "OpenEnvironment";
    public static final String KEY_OPEN_SETTING = "OpenSettings";
    public static final String NEED_QQ_VERSION_TIPS_URL = "http://openmobile.qq.com/oauth2.0/m_jump_by_version?";
    public static final String URL_FUSION_BASE = "http://fusion.qq.com";
    public static final String URL_FUSION_CGI_BASE = "http://fusion.qq.com/cgi-bin";
    public static final String URL_PRIZE_EXCHANGE = "http://fusion.qq.com/cgi-bin/prize_sharing/exchange_prize.cgi";
    public static final String URL_PRIZE_GET_ACTIVITY_STATE = "http://fusion.qq.com/cgi-bin/prize_sharing/get_activity_state.cgi";
    public static final String URL_PRIZE_MAKE_SHARE_URL = "http://fusion.qq.com/cgi-bin/prize_sharing/make_share_url.cgi";
    public static final String URL_PRIZE_QUERY_UNEXCHANGE = "http://fusion.qq.com/cgi-bin/prize_sharing/query_unexchange_prize.cgi";
    private static final String a = (new StringBuilder()).append("openSDK_LOG.").append(com/tencent/open/utils/ServerSetting.getName()).toString();
    private static ServerSetting b = null;
    private volatile WeakReference c;

    public ServerSetting()
    {
        c = null;
    }

    public static ServerSetting getInstance()
    {
        com/tencent/open/utils/ServerSetting;
        JVM INSTR monitorenter ;
        ServerSetting serversetting;
        if (b == null)
        {
            b = new ServerSetting();
        }
        serversetting = b;
        com/tencent/open/utils/ServerSetting;
        JVM INSTR monitorexit ;
        return serversetting;
        Exception exception;
        exception;
        throw exception;
    }

    public void changeServer()
    {
        c = null;
    }

    public String getEnvUrl(Context context, String s)
    {
        String s1;
        String s2;
        if (c == null || c.get() == null)
        {
            c = new WeakReference(context.getSharedPreferences("ServerPrefs", 0));
        }
        context = s;
        try
        {
            s1 = (new URL(s)).getHost();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            f.e(a, (new StringBuilder()).append("getEnvUrl url=").append(context).append("error.: ").append(s.getMessage()).toString());
            return context;
        }
        if (s1 != null)
        {
            break MISSING_BLOCK_LABEL_82;
        }
        context = s;
        f.e(a, (new StringBuilder()).append("Get host error. url=").append(s).toString());
        return s;
        context = s;
        s2 = ((SharedPreferences)c.get()).getString(s1, null);
        if (s2 == null)
        {
            break MISSING_BLOCK_LABEL_119;
        }
        context = s;
        if (!s1.equals(s2))
        {
            break MISSING_BLOCK_LABEL_198;
        }
        context = s;
        f.b(a, (new StringBuilder()).append("host=").append(s1).append(", envHost=").append(s2).toString());
        return s;
        context = s;
        s = s.replace(s1, s2);
        context = s;
        f.b(a, (new StringBuilder()).append("return environment url : ").append(s).toString());
        return s;
    }

    public void setEnvironment(Context context, int i)
    {
        if (context != null && (c == null || c.get() == null))
        {
            c = new WeakReference(context.getSharedPreferences("ServerPrefs", 0));
        }
        if (i == 0 || i == 1) goto _L2; else goto _L1
_L1:
        f.e(a, "\u5207\u6362\u73AF\u5883\u53C2\u6570\u9519\u8BEF\uFF0C\u6B63\u5F0F\u73AF\u5883\u4E3A0\uFF0C\u4F53\u9A8C\u73AF\u5883\u4E3A1");
_L4:
        return;
_L2:
        android.content.SharedPreferences.Editor editor1;
        switch (i)
        {
        default:
            return;

        case 0: // '\0'
            android.content.SharedPreferences.Editor editor = ((SharedPreferences)c.get()).edit();
            if (editor != null)
            {
                editor.putInt("ServerType", 0);
                editor.putString("OpenEnvironment", "formal");
                editor.putString("qzs.qq.com", "qzs.qq.com");
                editor.putString("openmobile.qq.com", "openmobile.qq.com");
                editor.commit();
                changeServer();
                Toast.makeText(context, "\u5DF2\u5207\u6362\u5230\u6B63\u5F0F\u73AF\u5883", 0).show();
                return;
            }
            break;

        case 1: // '\001'
            editor1 = ((SharedPreferences)c.get()).edit();
            continue; /* Loop/switch isn't completed */
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (editor1 == null) goto _L4; else goto _L5
_L5:
        editor1.putInt("ServerType", 1);
        editor1.putString("OpenEnvironment", "exp");
        editor1.putString("qzs.qq.com", "testmobile.qq.com");
        editor1.putString("openmobile.qq.com", "test.openmobile.qq.com");
        editor1.commit();
        changeServer();
        Toast.makeText(context, "\u5DF2\u5207\u6362\u5230\u4F53\u9A8C\u73AF\u5883", 0).show();
        return;
    }

}
