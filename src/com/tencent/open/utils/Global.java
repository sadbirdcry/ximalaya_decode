// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.io.File;

public final class Global
{

    private static Context a;

    public Global()
    {
    }

    public static final Context getContext()
    {
        if (a == null)
        {
            return null;
        } else
        {
            return a;
        }
    }

    public static final File getFilesDir()
    {
        if (getContext() == null)
        {
            return null;
        } else
        {
            return getContext().getFilesDir();
        }
    }

    public static final String getPackageName()
    {
        if (getContext() == null)
        {
            return "";
        } else
        {
            return getContext().getPackageName();
        }
    }

    public static final SharedPreferences getSharedPreferences(String s, int i)
    {
        if (getContext() == null)
        {
            return null;
        } else
        {
            return getContext().getSharedPreferences(s, i);
        }
    }

    public static int getVersionCode()
    {
        return a.getSharedPreferences("openSdk.pref", 0).getInt("app.vercode", 0);
    }

    public static void saveVersionCode()
    {
        Object obj = getContext();
        if (obj != null) goto _L2; else goto _L1
_L1:
        PackageInfo packageinfo;
        return;
_L2:
        if ((packageinfo = ((Context) (obj)).getPackageManager().getPackageInfo(((Context) (obj)).getPackageName(), 0)) == null) goto _L1; else goto _L3
_L3:
        try
        {
            obj = ((Context) (obj)).getSharedPreferences("openSdk.pref", 0).edit();
            ((android.content.SharedPreferences.Editor) (obj)).putInt("app.vercode", packageinfo.versionCode);
            ((android.content.SharedPreferences.Editor) (obj)).commit();
            return;
        }
        catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception)
        {
            namenotfoundexception.printStackTrace();
        }
        return;
    }

    public static final void setContext(Context context)
    {
        a = context;
    }
}
