// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import com.tencent.open.a.f;
import com.tencent.open.b.a;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.open.utils:
//            SystemUtils, Global, HttpUtils

public class Util
{
    public static class Statistic
    {

        public long reqSize;
        public String response;
        public long rspSize;

        public Statistic(String s, int j)
        {
            response = s;
            reqSize = j;
            if (response != null)
            {
                rspSize = response.length();
            }
        }
    }

    private static class TBufferedOutputStream extends BufferedOutputStream
    {

        private int a;

        public int getLength()
        {
            return a;
        }

        public void write(byte abyte0[])
            throws IOException
        {
            super.write(abyte0);
            a = a + abyte0.length;
        }

        public TBufferedOutputStream(OutputStream outputstream)
        {
            super(outputstream);
            a = 0;
        }
    }


    private static final String a = (new StringBuilder()).append("openSDK_LOG.").append(com/tencent/open/utils/Util.getName()).toString();
    private static String b = "";
    private static String c = "";
    private static String d = "";
    private static String e = "";
    private static int f = -1;
    private static String g;
    private static boolean h = true;
    private static String i = "0123456789ABCDEF";

    public Util()
    {
    }

    private static char a(int j)
    {
        j &= 0xf;
        if (j < 10)
        {
            return (char)(j + 48);
        } else
        {
            return (char)((j - 10) + 97);
        }
    }

    static String a()
    {
        return a;
    }

    private static String a(HttpResponse httpresponse)
        throws IllegalStateException, IOException
    {
        byte abyte0[] = httpresponse.getEntity().getContent();
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        httpresponse = httpresponse.getFirstHeader("Content-Encoding");
        if (httpresponse != null && httpresponse.getValue().toLowerCase().indexOf("gzip") > -1)
        {
            httpresponse = new GZIPInputStream(abyte0);
        } else
        {
            httpresponse = abyte0;
        }
        abyte0 = new byte[512];
        do
        {
            int j = httpresponse.read(abyte0);
            if (j != -1)
            {
                bytearrayoutputstream.write(abyte0, 0, j);
            } else
            {
                return new String(bytearrayoutputstream.toByteArray(), "UTF-8");
            }
        } while (true);
    }

    private static void a(Context context, String s, String s1, String s2)
    {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(s, s1));
        intent.setAction("android.intent.action.VIEW");
        intent.addFlags(0x40000000);
        intent.addFlags(0x10000000);
        intent.setData(Uri.parse(s2));
        context.startActivity(intent);
    }

    private static boolean a(Context context)
    {
        boolean flag1 = false;
        String s;
        Signature asignature[];
        boolean flag;
        boolean flag2;
        try
        {
            context = context.getPackageManager().getPackageInfo("com.tencent.mtt", 64);
            s = ((PackageInfo) (context)).versionName;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        flag = flag1;
        if (SystemUtils.compareVersion(s, "4.3") < 0)
        {
            break MISSING_BLOCK_LABEL_97;
        }
        flag = flag1;
        if (s.startsWith("4.4"))
        {
            break MISSING_BLOCK_LABEL_97;
        }
        asignature = ((PackageInfo) (context)).signatures;
        flag = flag1;
        if (asignature == null)
        {
            break MISSING_BLOCK_LABEL_97;
        }
        context = MessageDigest.getInstance("MD5");
        context.update(asignature[0].toByteArray());
        asignature = toHexString(context.digest());
        context.reset();
        flag2 = asignature.equals("d8391a394d4a179e6fe7bdb8a301258b");
        flag = flag1;
        if (flag2)
        {
            flag = true;
        }
        return flag;
        context;
        com.tencent.open.a.f.e(a, (new StringBuilder()).append("isQQBrowerAvailable has exception: ").append(context.getMessage()).toString());
        return false;
    }

    public static boolean checkNetWork(Context context)
    {
        boolean flag1;
        flag1 = false;
        context = (ConnectivityManager)context.getSystemService("connectivity");
        if (context != null) goto _L2; else goto _L1
_L1:
        boolean flag = true;
_L4:
        return flag;
_L2:
        context = context.getAllNetworkInfo();
        flag = flag1;
        if (context == null)
        {
            continue;
        }
        int j = 0;
        do
        {
            flag = flag1;
            if (j >= context.length)
            {
                continue;
            }
            if (context[j].isConnectedOrConnecting())
            {
                return true;
            }
            j++;
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static Bundle composeHaboCgiReportParams(String s, String s1, String s2, String s3, String s4, String s5, String s6, String s7, 
            String s8)
    {
        Bundle bundle = new Bundle();
        bundle.putString("platform", "1");
        bundle.putString("result", s);
        bundle.putString("code", s1);
        bundle.putString("tmcost", s2);
        bundle.putString("rate", s3);
        bundle.putString("cmd", s4);
        bundle.putString("uin", s5);
        bundle.putString("appid", s6);
        bundle.putString("share_type", s7);
        bundle.putString("detail", s8);
        bundle.putString("os_ver", android.os.Build.VERSION.RELEASE);
        bundle.putString("network", com.tencent.open.b.a.a(Global.getContext()));
        bundle.putString("apn", com.tencent.open.b.a.b(Global.getContext()));
        bundle.putString("model_name", Build.MODEL);
        bundle.putString("sdk_ver", "2.9.1.lite");
        bundle.putString("packagename", Global.getPackageName());
        bundle.putString("app_ver", getAppVersionName(Global.getContext(), Global.getPackageName()));
        return bundle;
    }

    public static Bundle composeViaReportParams(String s, String s1, String s2, String s3, String s4, String s5)
    {
        return composeViaReportParams(s, s2, s3, s1, s4, s5, "", "", "", "", "", "");
    }

    public static Bundle composeViaReportParams(String s, String s1, String s2, String s3, String s4, String s5, String s6, String s7, 
            String s8, String s9, String s10, String s11)
    {
        Bundle bundle = new Bundle();
        bundle.putString("openid", s);
        bundle.putString("report_type", s1);
        bundle.putString("act_type", s2);
        bundle.putString("via", s3);
        bundle.putString("app_id", s4);
        bundle.putString("result", s5);
        bundle.putString("type", s6);
        bundle.putString("login_status", s7);
        bundle.putString("need_user_auth", s8);
        bundle.putString("to_uin", s9);
        bundle.putString("call_source", s10);
        bundle.putString("to_type", s11);
        return bundle;
    }

    public static Bundle decodeUrl(String s)
    {
        Object obj;
        Bundle bundle;
        int j;
        j = 0;
        bundle = new Bundle();
        obj = bundle;
        if (s == null) goto _L2; else goto _L1
_L1:
        int k;
        s = s.split("&");
        k = s.length;
_L3:
        obj = bundle;
        if (j >= k)
        {
            break; /* Loop/switch isn't completed */
        }
        obj = s[j].split("=");
        if (obj.length == 2)
        {
            bundle.putString(URLDecoder.decode(obj[0]), URLDecoder.decode(obj[1]));
        }
        j++;
        if (true) goto _L3; else goto _L2
        s;
        obj = null;
_L2:
        return ((Bundle) (obj));
    }

    public static JSONObject decodeUrlToJson(JSONObject jsonobject, String s)
    {
        JSONObject jsonobject1 = jsonobject;
        if (jsonobject == null)
        {
            jsonobject1 = new JSONObject();
        }
        if (s != null)
        {
            jsonobject = s.split("&");
            int k = jsonobject.length;
            int j = 0;
            while (j < k) 
            {
                s = jsonobject[j].split("=");
                if (s.length == 2)
                {
                    try
                    {
                        jsonobject1.put(URLDecoder.decode(s[0]), URLDecoder.decode(s[1]));
                    }
                    // Misplaced declaration of an exception variable
                    catch (String s)
                    {
                        com.tencent.open.a.f.e(a, (new StringBuilder()).append("decodeUrlToJson has exception: ").append(s.getMessage()).toString());
                    }
                }
                j++;
            }
        }
        return jsonobject1;
    }

    public static String encodePostBody(Bundle bundle, String s)
    {
        if (bundle == null)
        {
            return "";
        }
        StringBuilder stringbuilder = new StringBuilder();
        int k = bundle.size();
        Iterator iterator = bundle.keySet().iterator();
        int j = -1;
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            String s1 = (String)iterator.next();
            j++;
            Object obj = bundle.get(s1);
            if (obj instanceof String)
            {
                stringbuilder.append((new StringBuilder()).append("Content-Disposition: form-data; name=\"").append(s1).append("\"").append("\r\n").append("\r\n").append((String)obj).toString());
                if (j < k - 1)
                {
                    stringbuilder.append((new StringBuilder()).append("\r\n--").append(s).append("\r\n").toString());
                }
            }
        } while (true);
        return stringbuilder.toString();
    }

    public static String encodeUrl(Bundle bundle)
    {
        if (bundle == null)
        {
            return "";
        }
        StringBuilder stringbuilder = new StringBuilder();
        Iterator iterator = bundle.keySet().iterator();
        boolean flag = true;
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj = (String)iterator.next();
            Object obj1 = bundle.get(((String) (obj)));
            if ((obj1 instanceof String) || (obj1 instanceof String[]))
            {
                if (obj1 instanceof String[])
                {
                    if (flag)
                    {
                        flag = false;
                    } else
                    {
                        stringbuilder.append("&");
                    }
                    stringbuilder.append((new StringBuilder()).append(URLEncoder.encode(((String) (obj)))).append("=").toString());
                    obj = bundle.getStringArray(((String) (obj)));
                    if (obj != null)
                    {
                        int j = 0;
                        while (j < obj.length) 
                        {
                            if (j == 0)
                            {
                                stringbuilder.append(URLEncoder.encode(obj[j]));
                            } else
                            {
                                stringbuilder.append(URLEncoder.encode((new StringBuilder()).append(",").append(obj[j]).toString()));
                            }
                            j++;
                        }
                    }
                } else
                {
                    if (flag)
                    {
                        flag = false;
                    } else
                    {
                        stringbuilder.append("&");
                    }
                    stringbuilder.append((new StringBuilder()).append(URLEncoder.encode(((String) (obj)))).append("=").append(URLEncoder.encode(bundle.getString(((String) (obj))))).toString());
                }
            }
        } while (true);
        return stringbuilder.toString();
    }

    public static String encrypt(String s)
    {
        Object obj;
        byte abyte0[];
        int j;
        int k;
        byte byte0;
        try
        {
            MessageDigest messagedigest = MessageDigest.getInstance("MD5");
            messagedigest.update(getBytesUTF8(s));
            abyte0 = messagedigest.digest();
        }
        catch (NoSuchAlgorithmException nosuchalgorithmexception)
        {
            com.tencent.open.a.f.e(a, (new StringBuilder()).append("encrypt has exception: ").append(nosuchalgorithmexception.getMessage()).toString());
            return s;
        }
        obj = s;
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_84;
        }
        obj = new StringBuilder();
        k = abyte0.length;
        j = 0;
_L2:
        if (j >= k)
        {
            break; /* Loop/switch isn't completed */
        }
        byte0 = abyte0[j];
        ((StringBuilder) (obj)).append(a(byte0 >>> 4));
        ((StringBuilder) (obj)).append(a(byte0));
        j++;
        if (true) goto _L2; else goto _L1
_L1:
        obj = ((StringBuilder) (obj)).toString();
        return ((String) (obj));
    }

    public static boolean fileExists(String s)
    {
        if (s != null)
        {
            if ((s = new File(s)) != null && s.exists())
            {
                return true;
            }
        }
        return false;
    }

    public static String getAppVersion(Context context)
    {
        PackageManager packagemanager = context.getPackageManager();
        try
        {
            context = packagemanager.getPackageInfo(context.getPackageName(), 0).versionName;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            com.tencent.open.a.f.e(a, (new StringBuilder()).append("getAppVersion error").append(context.getMessage()).toString());
            return "";
        }
        return context;
    }

    public static String getAppVersionName(Context context, String s)
    {
        if (context == null)
        {
            return "";
        } else
        {
            getPackageInfo(context, s);
            return b;
        }
    }

    public static final String getApplicationLable(Context context)
    {
        if (context != null)
        {
            context = context.getPackageManager().getApplicationLabel(context.getApplicationInfo());
            if (context != null)
            {
                return context.toString();
            }
        }
        return null;
    }

    public static byte[] getBytesUTF8(String s)
    {
        try
        {
            s = s.getBytes("UTF-8");
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return s;
    }

    public static String getLocation(Context context)
    {
        if (context == null)
        {
            return "";
        }
        Object obj;
        context = (LocationManager)context.getSystemService("location");
        obj = new Criteria();
        ((Criteria) (obj)).setCostAllowed(false);
        ((Criteria) (obj)).setAccuracy(2);
        obj = context.getBestProvider(((Criteria) (obj)), true);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_121;
        }
        context = context.getLastKnownLocation(((String) (obj)));
        if (context == null)
        {
            return "";
        }
        double d1 = context.getLatitude();
        double d2 = context.getLongitude();
        g = (new StringBuilder()).append(d1).append("*").append(d2).toString();
        context = g;
        return context;
        context;
        com.tencent.open.a.f.b("getLocation", "getLocation>>>", context);
        return "";
    }

    public static void getPackageInfo(Context context, String s)
    {
        if (context == null)
        {
            return;
        }
        try
        {
            context = context.getPackageManager().getPackageInfo(s, 0);
            c = ((PackageInfo) (context)).versionName;
            b = c.substring(0, c.lastIndexOf('.'));
            e = c.substring(c.lastIndexOf('.') + 1, c.length());
            f = ((PackageInfo) (context)).versionCode;
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            com.tencent.open.a.f.e(a, (new StringBuilder()).append("getPackageInfo has exception: ").append(context.getMessage()).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            com.tencent.open.a.f.e(a, (new StringBuilder()).append("getPackageInfo has exception: ").append(context.getMessage()).toString());
        }
    }

    public static String getQUA3(Context context, String s)
    {
        if (context == null)
        {
            return "";
        } else
        {
            d = getAppVersionName(context, s);
            return d;
        }
    }

    public static String getUserIp()
    {
        Object obj = NetworkInterface.getNetworkInterfaces();
_L2:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_77;
        }
        Enumeration enumeration;
        if (!((Enumeration) (obj)).hasMoreElements())
        {
            break MISSING_BLOCK_LABEL_77;
        }
        enumeration = ((NetworkInterface)((Enumeration) (obj)).nextElement()).getInetAddresses();
_L4:
        if (!enumeration.hasMoreElements()) goto _L2; else goto _L1
_L1:
        InetAddress inetaddress = (InetAddress)enumeration.nextElement();
        if (inetaddress.isLoopbackAddress()) goto _L4; else goto _L3
_L3:
        obj = inetaddress.getHostAddress().toString();
        return ((String) (obj));
        SocketException socketexception;
        socketexception;
        logd("Tencent-Util", socketexception.toString());
        return "";
    }

    public static String getVersionName(Context context, String s)
    {
        if (context == null)
        {
            return "";
        } else
        {
            getPackageInfo(context, s);
            return c;
        }
    }

    public static boolean hasSDCard()
    {
        File file = null;
        if (Environment.getExternalStorageState().equals("mounted"))
        {
            file = Environment.getExternalStorageDirectory();
        }
        return file != null;
    }

    public static String hexToString(String s)
    {
        int j = 0;
        String s1 = s;
        if ("0x".equals(s.substring(0, 2)))
        {
            s1 = s.substring(2);
        }
        s = new byte[s1.length() / 2];
        while (j < s.length) 
        {
            try
            {
                s[j] = (byte)(Integer.parseInt(s1.substring(j * 2, j * 2 + 2), 16) & 0xff);
            }
            catch (Exception exception)
            {
                com.tencent.open.a.f.e(a, (new StringBuilder()).append("hexToString has exception: ").append(exception.getMessage()).toString());
            }
            j++;
        }
        try
        {
            s = new String(s, "utf-8");
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            com.tencent.open.a.f.e(a, (new StringBuilder()).append("hexToString has exception: ").append(s.getMessage()).toString());
            return s1;
        }
        return s;
    }

    public static boolean isEmpty(String s)
    {
        return s == null || s.length() == 0;
    }

    public static boolean isMobileQQSupportShare(Context context)
    {
        boolean flag = false;
        context = context.getPackageManager();
        int j;
        try
        {
            j = SystemUtils.compareVersion(context.getPackageInfo("com.tencent.mobileqq", 0).versionName, "4.1");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            com.tencent.open.a.f.b("checkMobileQQ", "error");
            return false;
        }
        if (j >= 0)
        {
            flag = true;
        }
        return flag;
    }

    public static boolean isNumeric(String s)
    {
        return Pattern.compile("[0-9]*").matcher(s).matches();
    }

    public static final boolean isValidPath(String s)
    {
        if (s != null)
        {
            if ((s = new File(s)) != null && s.exists())
            {
                return true;
            }
        }
        return false;
    }

    public static final boolean isValidUrl(String s)
    {
        while (s == null || !s.startsWith("http://") && !s.startsWith("https://")) 
        {
            return false;
        }
        return true;
    }

    public static void logd(String s, String s1)
    {
        if (h)
        {
            com.tencent.open.a.f.b(s, s1);
        }
    }

    public static boolean openBrowser(Context context, String s)
    {
        boolean flag = a(context);
        if (!flag) goto _L2; else goto _L1
_L1:
        a(context, "com.tencent.mtt", "com.tencent.mtt.MainActivity", s);
          goto _L3
_L2:
        a(context, "com.android.browser", "com.android.browser.BrowserActivity", s);
          goto _L3
        Exception exception;
        exception;
_L4:
        if (flag)
        {
            try
            {
                a(context, "com.android.browser", "com.android.browser.BrowserActivity", s);
            }
            // Misplaced declaration of an exception variable
            catch (Exception exception)
            {
                try
                {
                    a(context, "com.google.android.browser", "com.android.browser.BrowserActivity", s);
                }
                // Misplaced declaration of an exception variable
                catch (Exception exception)
                {
                    try
                    {
                        a(context, "com.android.chrome", "com.google.android.apps.chrome.Main", s);
                    }
                    // Misplaced declaration of an exception variable
                    catch (Context context)
                    {
                        return false;
                    }
                }
            }
        } else
        {
            try
            {
                a(context, "com.google.android.browser", "com.android.browser.BrowserActivity", s);
            }
            // Misplaced declaration of an exception variable
            catch (Exception exception)
            {
                try
                {
                    a(context, "com.android.chrome", "com.google.android.apps.chrome.Main", s);
                }
                // Misplaced declaration of an exception variable
                catch (Context context)
                {
                    return false;
                }
            }
        }
        break; /* Loop/switch isn't completed */
        exception;
        flag = false;
        if (true) goto _L4; else goto _L3
_L3:
        return true;
    }

    public static int parseIntValue(String s)
    {
        return parseIntValue(s, 0);
    }

    public static int parseIntValue(String s, int j)
    {
        int k;
        try
        {
            k = Integer.parseInt(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return j;
        }
        return k;
    }

    public static JSONObject parseJson(String s)
        throws JSONException
    {
        String s1 = s;
        if (s.equals("false"))
        {
            s1 = "{value : false}";
        }
        s = s1;
        if (s1.equals("true"))
        {
            s = "{value : true}";
        }
        s1 = s;
        if (s.contains("allback("))
        {
            s1 = s.replaceFirst("[\\s\\S]*allback\\(([\\s\\S]*)\\);[^\\)]*\\z", "$1").trim();
        }
        s = s1;
        if (s1.contains("online[0]="))
        {
            s = (new StringBuilder()).append("{online:").append(s1.charAt(s1.length() - 2)).append("}").toString();
        }
        return new JSONObject(s);
    }

    public static Bundle parseUrl(String s)
    {
        s = s.replace("auth://", "http://");
        Bundle bundle;
        try
        {
            s = new URL(s);
            bundle = decodeUrl(s.getQuery());
            bundle.putAll(decodeUrl(s.getRef()));
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return new Bundle();
        }
        return bundle;
    }

    public static JSONObject parseUrlToJson(String s)
    {
        s = s.replace("auth://", "http://");
        JSONObject jsonobject;
        try
        {
            s = new URL(s);
            jsonobject = decodeUrlToJson(null, s.getQuery());
            decodeUrlToJson(jsonobject, s.getRef());
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return new JSONObject();
        }
        return jsonobject;
    }

    public static void reportBernoulli(Context context, String s, long l, String s1)
    {
        Bundle bundle = new Bundle();
        bundle.putString("appid_for_getting_config", s1);
        bundle.putString("strValue", s1);
        bundle.putString("nValue", s);
        bundle.putString("qver", "2.9.1.lite");
        if (l != 0L)
        {
            bundle.putLong("elt", l);
        }
        (new _cls1(context, bundle)).start();
    }

    public static void showAlert(Context context, String s, String s1)
    {
        context = new android.app.AlertDialog.Builder(context);
        context.setTitle(s);
        context.setMessage(s1);
        context.create().show();
    }

    public static final String subString(String s, int j, String s1, String s2)
    {
        int k = 0;
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        s1 = "";
_L4:
        return s1;
_L2:
        String s3;
        int l;
        int i1;
        if (!TextUtils.isEmpty(s1))
        {
            s3 = s1;
        } else
        {
            s3 = "UTF-8";
        }
        s1 = s;
        if (s.getBytes(s3).length <= j) goto _L4; else goto _L3
_L3:
        l = 0;
_L6:
        s1 = s;
        if (k >= s.length()) goto _L4; else goto _L5
_L5:
        i1 = s.substring(k, k + 1).getBytes(s3).length;
        if (l + i1 <= j)
        {
            break MISSING_BLOCK_LABEL_119;
        }
        s1 = s.substring(0, k);
        s = s1;
        s1 = s;
        if (!TextUtils.isEmpty(s2))
        {
            s1 = (new StringBuilder()).append(s).append(s2).toString();
        }
        return s1;
        l += i1;
        k++;
          goto _L6
        s1;
_L8:
        System.out.println((new StringBuilder()).append("StructMsg sSubString error : ").append(s1.getMessage()).toString());
        return s;
        s1;
        if (true) goto _L8; else goto _L7
_L7:
    }

    public static String toHexString(String s)
    {
        s = getBytesUTF8(s);
        StringBuilder stringbuilder = new StringBuilder(s.length * 2);
        for (int j = 0; j < s.length; j++)
        {
            stringbuilder.append(i.charAt((s[j] & 0xf0) >> 4));
            stringbuilder.append(i.charAt((s[j] & 0xf) >> 0));
        }

        return stringbuilder.toString();
    }

    public static String toHexString(byte abyte0[])
    {
        if (abyte0 == null)
        {
            return null;
        }
        StringBuilder stringbuilder = new StringBuilder(abyte0.length * 2);
        for (int j = 0; j < abyte0.length; j++)
        {
            String s1 = Integer.toString(abyte0[j] & 0xff, 16);
            String s = s1;
            if (s1.length() == 1)
            {
                s = (new StringBuilder()).append("0").append(s1).toString();
            }
            stringbuilder.append(s);
        }

        return stringbuilder.toString();
    }

    public static Statistic upload(Context context, String s, Bundle bundle)
        throws MalformedURLException, IOException, HttpUtils.NetworkUnavailableException, HttpUtils.HttpStatusException
    {
        if (context != null)
        {
            Object obj = (ConnectivityManager)context.getSystemService("connectivity");
            if (obj != null)
            {
                obj = ((ConnectivityManager) (obj)).getActiveNetworkInfo();
                if (obj == null || !((NetworkInfo) (obj)).isAvailable())
                {
                    throw new HttpUtils.NetworkUnavailableException("network unavailable");
                }
            }
        }
        bundle = new Bundle(bundle);
        Object obj1 = bundle.getString("appid_for_getting_config");
        bundle.remove("appid_for_getting_config");
        context = HttpUtils.getHttpClient(context, ((String) (obj1)), s);
        s = new HttpPost(s);
        obj1 = new Bundle();
        Object obj2 = bundle.keySet().iterator();
        do
        {
            if (!((Iterator) (obj2)).hasNext())
            {
                break;
            }
            String s1 = (String)((Iterator) (obj2)).next();
            Object obj3 = bundle.get(s1);
            if (obj3 instanceof byte[])
            {
                ((Bundle) (obj1)).putByteArray(s1, (byte[])(byte[])obj3);
            }
        } while (true);
        s.setHeader("Content-Type", "multipart/form-data; boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
        s.setHeader("Connection", "Keep-Alive");
        obj2 = new ByteArrayOutputStream();
        ((ByteArrayOutputStream) (obj2)).write(getBytesUTF8("--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
        ((ByteArrayOutputStream) (obj2)).write(getBytesUTF8(encodePostBody(bundle, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f")));
        if (!((Bundle) (obj1)).isEmpty())
        {
            int j1 = ((Bundle) (obj1)).size();
            ((ByteArrayOutputStream) (obj2)).write(getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
            bundle = ((Bundle) (obj1)).keySet().iterator();
            int j = -1;
            do
            {
                if (!bundle.hasNext())
                {
                    break;
                }
                String s2 = (String)bundle.next();
                int l = j + 1;
                ((ByteArrayOutputStream) (obj2)).write(getBytesUTF8((new StringBuilder()).append("Content-Disposition: form-data; name=\"").append(s2).append("\"; filename=\"").append("value.file").append("\"").append("\r\n").toString()));
                ((ByteArrayOutputStream) (obj2)).write(getBytesUTF8("Content-Type: application/octet-stream\r\n\r\n"));
                byte abyte0[] = ((Bundle) (obj1)).getByteArray(s2);
                if (abyte0 != null)
                {
                    ((ByteArrayOutputStream) (obj2)).write(abyte0);
                }
                j = l;
                if (l < j1 - 1)
                {
                    ((ByteArrayOutputStream) (obj2)).write(getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
                    j = l;
                }
            } while (true);
        }
        ((ByteArrayOutputStream) (obj2)).write(getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f--\r\n"));
        bundle = ((ByteArrayOutputStream) (obj2)).toByteArray();
        int k = bundle.length;
        ((ByteArrayOutputStream) (obj2)).close();
        s.setEntity(new ByteArrayEntity(bundle));
        context = context.execute(s);
        int i1 = context.getStatusLine().getStatusCode();
        if (i1 == 200)
        {
            return new Statistic(a(context), k + 0);
        } else
        {
            throw new HttpUtils.HttpStatusException((new StringBuilder()).append("http status code error:").append(i1).toString());
        }
    }


    private class _cls1 extends Thread
    {

        final Context a;
        final Bundle b;

        public void run()
        {
            try
            {
                HttpUtils.openUrl2(a, "http://cgi.qplus.com/report/report", "GET", b);
                return;
            }
            catch (Exception exception)
            {
                com.tencent.open.a.f.e(com.tencent.open.utils.Util.a(), (new StringBuilder()).append("reportBernoulli has exception: ").append(exception.getMessage()).toString());
            }
        }

        _cls1(Context context, Bundle bundle)
        {
            a = context;
            b = bundle;
            super();
        }
    }

}
