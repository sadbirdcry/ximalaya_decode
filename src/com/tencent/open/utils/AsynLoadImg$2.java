// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import android.os.Handler;
import android.os.Message;
import com.tencent.open.a.f;
import java.io.File;

// Referenced classes of package com.tencent.open.utils:
//            AsynLoadImg, Util

class a
    implements Runnable
{

    final AsynLoadImg a;

    public void run()
    {
        f.a("AsynLoadImg", "saveFileRunnable:");
        String s = Util.encrypt(AsynLoadImg.b(a));
        s = (new StringBuilder()).append("share_qq_").append(s).append(".jpg").toString();
        String s1 = (new StringBuilder()).append(AsynLoadImg.a()).append(s).toString();
        File file = new File(s1);
        Message message = AsynLoadImg.c(a).obtainMessage();
        if (file.exists())
        {
            message.arg1 = 0;
            message.obj = s1;
            f.a("AsynLoadImg", (new StringBuilder()).append("file exists: time:").append(System.currentTimeMillis() - AsynLoadImg.d(a)).toString());
        } else
        {
            android.graphics.Bitmap bitmap = AsynLoadImg.getbitmap(AsynLoadImg.b(a));
            boolean flag;
            if (bitmap != null)
            {
                flag = a.saveFile(bitmap, s);
            } else
            {
                f.a("AsynLoadImg", "saveFileRunnable:get bmp fail---");
                flag = false;
            }
            if (flag)
            {
                message.arg1 = 0;
                message.obj = s1;
            } else
            {
                message.arg1 = 1;
            }
            f.a("AsynLoadImg", (new StringBuilder()).append("file not exists: download time:").append(System.currentTimeMillis() - AsynLoadImg.d(a)).toString());
        }
        AsynLoadImg.c(a).sendMessage(message);
    }

    (AsynLoadImg asynloadimg)
    {
        a = asynloadimg;
        super();
    }
}
