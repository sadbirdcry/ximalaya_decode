// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import com.tencent.connect.a.a;
import com.tencent.connect.auth.QQToken;
import com.tencent.open.a.f;
import com.tencent.open.b.g;
import com.tencent.tauth.IRequestListener;
import java.io.ByteArrayOutputStream;
import java.io.CharConversionException;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidClassException;
import java.io.InvalidObjectException;
import java.io.NotActiveException;
import java.io.NotSerializableException;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.io.SyncFailedException;
import java.io.UTFDataFormatException;
import java.io.UnsupportedEncodingException;
import java.io.WriteAbortedException;
import java.net.BindException;
import java.net.ConnectException;
import java.net.HttpRetryException;
import java.net.MalformedURLException;
import java.net.NoRouteToHostException;
import java.net.PortUnreachableException;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.net.UnknownServiceException;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.FileLockInterruptionException;
import java.nio.charset.MalformedInputException;
import java.nio.charset.UnmappableCharacterException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.InvalidPropertiesFormatException;
import java.util.Iterator;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLKeyException;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLProtocolException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import org.apache.http.ConnectionClosedException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.MalformedChunkCodingException;
import org.apache.http.NoHttpResponseException;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.open.utils:
//            OpenConfig, Util, ServerSetting

public class HttpUtils
{
    public static class CustomSSLSocketFactory extends SSLSocketFactory
    {

        private final SSLContext a = SSLContext.getInstance("TLS");

        public Socket createSocket()
            throws IOException
        {
            return a.getSocketFactory().createSocket();
        }

        public Socket createSocket(Socket socket, String s, int i, boolean flag)
            throws IOException, UnknownHostException
        {
            return a.getSocketFactory().createSocket(socket, s, i, flag);
        }

        public CustomSSLSocketFactory(KeyStore keystore)
            throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
        {
            super(keystore);
            try
            {
                keystore = new MyX509TrustManager();
            }
            // Misplaced declaration of an exception variable
            catch (KeyStore keystore)
            {
                keystore = null;
            }
            a.init(null, new TrustManager[] {
                keystore
            }, null);
        }
    }

    public static class HttpStatusException extends Exception
    {

        public static final String ERROR_INFO = "http status code error:";

        public HttpStatusException(String s)
        {
            super(s);
        }
    }

    public static class MyX509TrustManager
        implements X509TrustManager
    {

        X509TrustManager a;

        public void checkClientTrusted(X509Certificate ax509certificate[], String s)
            throws CertificateException
        {
            a.checkClientTrusted(ax509certificate, s);
        }

        public void checkServerTrusted(X509Certificate ax509certificate[], String s)
            throws CertificateException
        {
            a.checkServerTrusted(ax509certificate, s);
        }

        public X509Certificate[] getAcceptedIssuers()
        {
            return a.getAcceptedIssuers();
        }

        MyX509TrustManager()
            throws Exception
        {
            Object obj1;
            FileInputStream fileinputstream;
            Object obj;
            try
            {
                obj = KeyStore.getInstance("JKS");
            }
            catch (Exception exception)
            {
                exception = null;
            }
            if (obj == null) goto _L2; else goto _L1
_L1:
            fileinputstream = new FileInputStream("trustedCerts");
            ((KeyStore) (obj)).load(fileinputstream, "passphrase".toCharArray());
            obj1 = TrustManagerFactory.getInstance("SunX509", "SunJSSE");
            ((TrustManagerFactory) (obj1)).init(((KeyStore) (obj)));
            obj = ((TrustManagerFactory) (obj1)).getTrustManagers();
            obj1 = obj;
            if (fileinputstream != null)
            {
                fileinputstream.close();
                obj1 = obj;
            }
            break MISSING_BLOCK_LABEL_64;
            Exception exception1;
            exception1;
            obj1 = null;
_L3:
            if (obj1 != null)
            {
                ((FileInputStream) (obj1)).close();
            }
            throw exception1;
_L2:
            TrustManagerFactory trustmanagerfactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustmanagerfactory.init((KeyStore)null);
            obj1 = trustmanagerfactory.getTrustManagers();
            for (int i = 0; i < obj1.length; i++)
            {
                if (obj1[i] instanceof X509TrustManager)
                {
                    a = (X509TrustManager)obj1[i];
                    return;
                }
            }

            throw new Exception("Couldn't initialize");
            trustmanagerfactory;
            obj1 = fileinputstream;
              goto _L3
        }
    }

    public static class NetworkProxy
    {

        public final String host;
        public final int port;

        private NetworkProxy(String s, int i)
        {
            host = s;
            port = i;
        }

        NetworkProxy(String s, int i, _cls1 _pcls1)
        {
            this(s, i);
        }
    }

    public static class NetworkUnavailableException extends Exception
    {

        public static final String ERROR_INFO = "network unavailable";

        public NetworkUnavailableException(String s)
        {
            super(s);
        }
    }


    private static final String a = com/tencent/open/utils/HttpUtils.getName();

    private HttpUtils()
    {
    }

    private static int a(Context context)
    {
        int i = -1;
        if (android.os.Build.VERSION.SDK_INT >= 11) goto _L2; else goto _L1
_L1:
        if (context == null) goto _L4; else goto _L3
_L3:
        int k = Proxy.getPort(context);
        i = k;
        if (k < 0)
        {
            i = Proxy.getDefaultPort();
        }
_L6:
        return i;
_L4:
        return Proxy.getDefaultPort();
_L2:
        context = System.getProperty("http.proxyPort");
        if (!TextUtils.isEmpty(context))
        {
            int j;
            try
            {
                j = Integer.parseInt(context);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                return -1;
            }
            return j;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    private static String a(HttpResponse httpresponse)
        throws IllegalStateException, IOException
    {
        byte abyte0[] = httpresponse.getEntity().getContent();
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
        httpresponse = httpresponse.getFirstHeader("Content-Encoding");
        if (httpresponse != null && httpresponse.getValue().toLowerCase().indexOf("gzip") > -1)
        {
            httpresponse = new GZIPInputStream(abyte0);
        } else
        {
            httpresponse = abyte0;
        }
        abyte0 = new byte[512];
        do
        {
            int i = httpresponse.read(abyte0);
            if (i != -1)
            {
                bytearrayoutputstream.write(abyte0, 0, i);
            } else
            {
                return new String(bytearrayoutputstream.toByteArray(), "UTF-8");
            }
        } while (true);
    }

    private static void a(Context context, QQToken qqtoken, String s)
    {
        if (s.indexOf("add_share") > -1 || s.indexOf("upload_pic") > -1 || s.indexOf("add_topic") > -1 || s.indexOf("set_user_face") > -1 || s.indexOf("add_t") > -1 || s.indexOf("add_pic_t") > -1 || s.indexOf("add_pic_url") > -1 || s.indexOf("add_video") > -1)
        {
            com.tencent.connect.a.a.a(context, qqtoken, "requireApi", new String[] {
                s
            });
        }
    }

    private static String b(Context context)
    {
        if (android.os.Build.VERSION.SDK_INT < 11)
        {
            if (context != null)
            {
                String s = Proxy.getHost(context);
                context = s;
                if (TextUtils.isEmpty(s))
                {
                    context = Proxy.getDefaultHost();
                }
                return context;
            } else
            {
                return Proxy.getDefaultHost();
            }
        } else
        {
            return System.getProperty("http.proxyHost");
        }
    }

    public static String encodePostBody(Bundle bundle, String s)
    {
        if (bundle == null)
        {
            return "";
        }
        StringBuilder stringbuilder = new StringBuilder();
        int j = bundle.size();
        Iterator iterator = bundle.keySet().iterator();
        int i = -1;
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            String s1 = (String)iterator.next();
            i++;
            Object obj = bundle.get(s1);
            if (obj instanceof String)
            {
                stringbuilder.append((new StringBuilder()).append("Content-Disposition: form-data; name=\"").append(s1).append("\"").append("\r\n").append("\r\n").append((String)obj).toString());
                if (i < j - 1)
                {
                    stringbuilder.append((new StringBuilder()).append("\r\n--").append(s).append("\r\n").toString());
                }
            }
        } while (true);
        return stringbuilder.toString();
    }

    public static String encodeUrl(Bundle bundle)
    {
        if (bundle == null)
        {
            return "";
        }
        StringBuilder stringbuilder = new StringBuilder();
        Iterator iterator = bundle.keySet().iterator();
        int i = 1;
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj = (String)iterator.next();
            Object obj1 = bundle.get(((String) (obj)));
            if ((obj1 instanceof String) || (obj1 instanceof String[]))
            {
                if (obj1 instanceof String[])
                {
                    int j;
                    if (i != 0)
                    {
                        j = 0;
                    } else
                    {
                        stringbuilder.append("&");
                        j = i;
                    }
                    stringbuilder.append((new StringBuilder()).append(URLEncoder.encode(((String) (obj)))).append("=").toString());
                    obj = bundle.getStringArray(((String) (obj)));
                    i = j;
                    if (obj != null)
                    {
                        i = 0;
                        while (i < obj.length) 
                        {
                            if (i == 0)
                            {
                                stringbuilder.append(URLEncoder.encode(obj[i]));
                            } else
                            {
                                stringbuilder.append(URLEncoder.encode((new StringBuilder()).append(",").append(obj[i]).toString()));
                            }
                            i++;
                        }
                        i = j;
                    }
                } else
                {
                    if (i != 0)
                    {
                        i = 0;
                    } else
                    {
                        stringbuilder.append("&");
                    }
                    stringbuilder.append((new StringBuilder()).append(URLEncoder.encode(((String) (obj)))).append("=").append(URLEncoder.encode(bundle.getString(((String) (obj))))).toString());
                }
            }
        } while (true);
        return stringbuilder.toString();
    }

    public static int getErrorCodeFromException(IOException ioexception)
    {
        if (ioexception instanceof CharConversionException)
        {
            return -20;
        }
        if (ioexception instanceof MalformedInputException)
        {
            return -21;
        }
        if (ioexception instanceof UnmappableCharacterException)
        {
            return -22;
        }
        if (ioexception instanceof HttpResponseException)
        {
            return -23;
        }
        if (ioexception instanceof ClosedChannelException)
        {
            return -24;
        }
        if (ioexception instanceof ConnectionClosedException)
        {
            return -25;
        }
        if (ioexception instanceof EOFException)
        {
            return -26;
        }
        if (ioexception instanceof FileLockInterruptionException)
        {
            return -27;
        }
        if (ioexception instanceof FileNotFoundException)
        {
            return -28;
        }
        if (ioexception instanceof HttpRetryException)
        {
            return -29;
        }
        if (ioexception instanceof ConnectTimeoutException)
        {
            return -7;
        }
        if (ioexception instanceof SocketTimeoutException)
        {
            return -8;
        }
        if (ioexception instanceof InvalidPropertiesFormatException)
        {
            return -30;
        }
        if (ioexception instanceof MalformedChunkCodingException)
        {
            return -31;
        }
        if (ioexception instanceof MalformedURLException)
        {
            return -3;
        }
        if (ioexception instanceof NoHttpResponseException)
        {
            return -32;
        }
        if (ioexception instanceof InvalidClassException)
        {
            return -33;
        }
        if (ioexception instanceof InvalidObjectException)
        {
            return -34;
        }
        if (ioexception instanceof NotActiveException)
        {
            return -35;
        }
        if (ioexception instanceof NotSerializableException)
        {
            return -36;
        }
        if (ioexception instanceof OptionalDataException)
        {
            return -37;
        }
        if (ioexception instanceof StreamCorruptedException)
        {
            return -38;
        }
        if (ioexception instanceof WriteAbortedException)
        {
            return -39;
        }
        if (ioexception instanceof ProtocolException)
        {
            return -40;
        }
        if (ioexception instanceof SSLHandshakeException)
        {
            return -41;
        }
        if (ioexception instanceof SSLKeyException)
        {
            return -42;
        }
        if (ioexception instanceof SSLPeerUnverifiedException)
        {
            return -43;
        }
        if (ioexception instanceof SSLProtocolException)
        {
            return -44;
        }
        if (ioexception instanceof BindException)
        {
            return -45;
        }
        if (ioexception instanceof ConnectException)
        {
            return -46;
        }
        if (ioexception instanceof NoRouteToHostException)
        {
            return -47;
        }
        if (ioexception instanceof PortUnreachableException)
        {
            return -48;
        }
        if (ioexception instanceof SyncFailedException)
        {
            return -49;
        }
        if (ioexception instanceof UTFDataFormatException)
        {
            return -50;
        }
        if (ioexception instanceof UnknownHostException)
        {
            return -51;
        }
        if (ioexception instanceof UnknownServiceException)
        {
            return -52;
        }
        if (ioexception instanceof UnsupportedEncodingException)
        {
            return -53;
        }
        return !(ioexception instanceof ZipException) ? -2 : -54;
    }

    public static HttpClient getHttpClient(Context context, String s, String s1)
    {
        int i = 0;
        s1 = new SchemeRegistry();
        s1.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        BasicHttpParams basichttpparams;
        int j;
        int k;
        if (android.os.Build.VERSION.SDK_INT < 16)
        {
            try
            {
                Object obj = KeyStore.getInstance(KeyStore.getDefaultType());
                ((KeyStore) (obj)).load(null, null);
                obj = new CustomSSLSocketFactory(((KeyStore) (obj)));
                ((SSLSocketFactory) (obj)).setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
                s1.register(new Scheme("https", ((org.apache.http.conn.scheme.SocketFactory) (obj)), 443));
            }
            catch (Exception exception)
            {
                s1.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
            }
        } else
        {
            s1.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
        }
        basichttpparams = new BasicHttpParams();
        if (context != null)
        {
            s = OpenConfig.getInstance(context, s);
        } else
        {
            s = null;
        }
        if (s != null)
        {
            j = s.getInt("Common_HttpConnectionTimeout");
            i = s.getInt("Common_SocketConnectionTimeout");
        } else
        {
            j = 0;
        }
        k = j;
        if (j == 0)
        {
            k = 15000;
        }
        j = i;
        if (i == 0)
        {
            j = 30000;
        }
        HttpConnectionParams.setConnectionTimeout(basichttpparams, k);
        HttpConnectionParams.setSoTimeout(basichttpparams, j);
        HttpProtocolParams.setVersion(basichttpparams, HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(basichttpparams, "UTF-8");
        HttpProtocolParams.setUserAgent(basichttpparams, (new StringBuilder()).append("AndroidSDK_").append(android.os.Build.VERSION.SDK).append("_").append(Build.DEVICE).append("_").append(android.os.Build.VERSION.RELEASE).toString());
        s = new DefaultHttpClient(new ThreadSafeClientConnManager(basichttpparams, s1), basichttpparams);
        context = getProxy(context);
        if (context != null)
        {
            context = new HttpHost(((NetworkProxy) (context)).host, ((NetworkProxy) (context)).port);
            s.getParams().setParameter("http.route.default-proxy", context);
        }
        return s;
    }

    public static NetworkProxy getProxy(Context context)
    {
        if (context == null)
        {
            return null;
        }
        Object obj = (ConnectivityManager)context.getSystemService("connectivity");
        if (obj == null)
        {
            return null;
        }
        obj = ((ConnectivityManager) (obj)).getActiveNetworkInfo();
        if (obj == null)
        {
            return null;
        }
        if (((NetworkInfo) (obj)).getType() == 0)
        {
            String s = b(context);
            int i = a(context);
            if (!TextUtils.isEmpty(s) && i >= 0)
            {
                return new NetworkProxy(s, i, null);
            }
        }
        return null;
    }

    public static Util.Statistic openUrl2(Context context, String s, String s1, Bundle bundle)
        throws MalformedURLException, IOException, NetworkUnavailableException, HttpStatusException
    {
        if (context != null)
        {
            Object obj = (ConnectivityManager)context.getSystemService("connectivity");
            if (obj != null)
            {
                obj = ((ConnectivityManager) (obj)).getActiveNetworkInfo();
                if (obj == null || !((NetworkInfo) (obj)).isAvailable())
                {
                    throw new NetworkUnavailableException("network unavailable");
                }
            }
        }
        Object obj1;
        int i;
        int j;
        if (bundle != null)
        {
            bundle = new Bundle(bundle);
        } else
        {
            bundle = new Bundle();
        }
        obj1 = bundle.getString("appid_for_getting_config");
        bundle.remove("appid_for_getting_config");
        obj1 = getHttpClient(context, ((String) (obj1)), s);
        if (s1.equals("GET"))
        {
            s1 = encodeUrl(bundle);
            i = s1.length();
            f.b(a, (new StringBuilder()).append("-->openUrl2 before url =").append(s).toString());
            if (s.indexOf("?") == -1)
            {
                context = (new StringBuilder()).append(s).append("?").toString();
            } else
            {
                context = (new StringBuilder()).append(s).append("&").toString();
            }
            f.b(a, (new StringBuilder()).append("-->openUrl2 encodedParam =").append(s1).append(" -- url = ").append(context).toString());
            context = new HttpGet((new StringBuilder()).append(context).append(s1).toString());
            context.addHeader("Accept-Encoding", "gzip");
            i = 0 + i;
        } else
        if (s1.equals("POST"))
        {
            context = new HttpPost(s);
            context.addHeader("Accept-Encoding", "gzip");
            s = new Bundle();
            Iterator iterator = bundle.keySet().iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                String s3 = (String)iterator.next();
                Object obj2 = bundle.get(s3);
                if (obj2 instanceof byte[])
                {
                    s.putByteArray(s3, (byte[])(byte[])obj2);
                }
            } while (true);
            if (!bundle.containsKey("method"))
            {
                bundle.putString("method", s1);
            }
            context.setHeader("Content-Type", "multipart/form-data; boundary=3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f");
            context.setHeader("Connection", "Keep-Alive");
            s1 = new ByteArrayOutputStream();
            s1.write(Util.getBytesUTF8("--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
            s1.write(Util.getBytesUTF8(encodePostBody(bundle, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f")));
            if (!s.isEmpty())
            {
                int k = s.size();
                s1.write(Util.getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
                bundle = s.keySet().iterator();
                i = -1;
                do
                {
                    if (!bundle.hasNext())
                    {
                        break;
                    }
                    String s2 = (String)bundle.next();
                    j = i + 1;
                    s1.write(Util.getBytesUTF8((new StringBuilder()).append("Content-Disposition: form-data; name=\"").append(s2).append("\"; filename=\"").append(s2).append("\"").append("\r\n").toString()));
                    s1.write(Util.getBytesUTF8("Content-Type: content/unknown\r\n\r\n"));
                    byte abyte0[] = s.getByteArray(s2);
                    if (abyte0 != null)
                    {
                        s1.write(abyte0);
                    }
                    i = j;
                    if (j < k - 1)
                    {
                        s1.write(Util.getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f\r\n"));
                        i = j;
                    }
                } while (true);
            }
            s1.write(Util.getBytesUTF8("\r\n--3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f--\r\n"));
            s = s1.toByteArray();
            i = s.length + 0;
            s1.close();
            context.setEntity(new ByteArrayEntity(s));
        } else
        {
            context = null;
            i = 0;
        }
        context = ((HttpClient) (obj1)).execute(context);
        j = context.getStatusLine().getStatusCode();
        if (j == 200)
        {
            return new Util.Statistic(a(context), i);
        } else
        {
            throw new HttpStatusException((new StringBuilder()).append("http status code error:").append(j).toString());
        }
    }

    public static JSONObject request(QQToken qqtoken, Context context, String s, Bundle bundle, String s1)
        throws IOException, JSONException, NetworkUnavailableException, HttpStatusException
    {
        f.a("openSDK_LOG", "OpenApi request");
        String s2;
        String s3;
        Util.Statistic statistic;
        Object obj;
        int i;
        int j;
        int k;
        long l;
        long l1;
        long l2;
        if (!s.toLowerCase().startsWith("http"))
        {
            s3 = (new StringBuilder()).append(ServerSetting.getInstance().getEnvUrl(context, "https://openmobile.qq.com/")).append(s).toString();
            s2 = (new StringBuilder()).append(ServerSetting.getInstance().getEnvUrl(context, "https://openmobile.qq.com/")).append(s).toString();
        } else
        {
            s2 = s;
            s3 = s;
        }
        a(context, qqtoken, s);
        l = SystemClock.elapsedRealtime();
        j = OpenConfig.getInstance(context, qqtoken.getAppId()).getInt("Common_HttpRetryCount");
        f.b("OpenConfig_test", (new StringBuilder()).append("config 1:Common_HttpRetryCount            config_value:").append(j).append("   appid:").append(qqtoken.getAppId()).append("     url:").append(s2).toString());
        if (j == 0)
        {
            j = 3;
        }
        f.b("OpenConfig_test", (new StringBuilder()).append("config 1:Common_HttpRetryCount            result_value:").append(j).append("   appid:").append(qqtoken.getAppId()).append("     url:").append(s2).toString());
        i = 0;
        qqtoken = null;
        k = i + 1;
        statistic = openUrl2(context, s3, s1, bundle);
        s = Util.parseJson(statistic.response);
        try
        {
            i = s.getInt("ret");
        }
        // Misplaced declaration of an exception variable
        catch (QQToken qqtoken)
        {
            i = -4;
        }
        try
        {
            l1 = statistic.reqSize;
            l2 = statistic.rspSize;
        }
        // Misplaced declaration of an exception variable
        catch (QQToken qqtoken)
        {
            qqtoken.printStackTrace();
            g.a().a(s2, l, 0L, 0L, -4);
            throw qqtoken;
        }
        g.a().a(s2, l, l1, l2, i);
        return s;
        obj;
        qqtoken = s;
        s = ((String) (obj));
_L7:
        s.printStackTrace();
        i = -7;
        if (k >= j) goto _L2; else goto _L1
_L1:
        l = SystemClock.elapsedRealtime();
_L4:
        l2 = 0L;
        if (k < j)
        {
            break MISSING_BLOCK_LABEL_543;
        }
        l1 = 0L;
        s = qqtoken;
        break MISSING_BLOCK_LABEL_271;
_L2:
        g.a().a(s2, l, 0L, 0L, -7);
        throw s;
        obj;
        qqtoken = s;
        s = ((String) (obj));
_L5:
        s.printStackTrace();
        i = -8;
        if (k < j)
        {
            l = SystemClock.elapsedRealtime();
        } else
        {
            g.a().a(s2, l, 0L, 0L, -8);
            throw s;
        }
        if (true) goto _L4; else goto _L3
_L3:
        qqtoken;
        qqtoken.printStackTrace();
        context = qqtoken.getMessage();
        try
        {
            i = Integer.parseInt(context.replace("http status code error:", ""));
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            i = -9;
        }
        g.a().a(s2, l, 0L, 0L, i);
        throw qqtoken;
        qqtoken;
        qqtoken.printStackTrace();
        throw qqtoken;
        qqtoken;
        qqtoken.printStackTrace();
        g.a().a(s2, l, 0L, 0L, -3);
        throw qqtoken;
        qqtoken;
        qqtoken.printStackTrace();
        i = getErrorCodeFromException(qqtoken);
        g.a().a(s2, l, 0L, 0L, i);
        throw qqtoken;
        s;
          goto _L5
        s;
        if (true) goto _L7; else goto _L6
_L6:
        for (i = k; false;)
        {
        }

        break MISSING_BLOCK_LABEL_222;
    }

    public static void requestAsync(QQToken qqtoken, Context context, String s, Bundle bundle, String s1, IRequestListener irequestlistener)
    {
        f.a("openSDK_LOG", "OpenApi requestAsync");
        (new _cls1(qqtoken, context, s, bundle, s1, irequestlistener)).start();
    }

    public static JSONObject upload(QQToken qqtoken, Context context, String s, Bundle bundle)
        throws IOException, JSONException, NetworkUnavailableException, HttpStatusException
    {
        String s1;
        String s2;
        Util.Statistic statistic;
        Object obj;
        int i;
        int j;
        int k;
        long l;
        long l1;
        long l2;
        if (!s.toLowerCase().startsWith("http"))
        {
            s2 = (new StringBuilder()).append(ServerSetting.getInstance().getEnvUrl(context, "https://openmobile.qq.com/")).append(s).toString();
            s1 = (new StringBuilder()).append(ServerSetting.getInstance().getEnvUrl(context, "https://openmobile.qq.com/")).append(s).toString();
        } else
        {
            s1 = s;
            s2 = s;
        }
        a(context, qqtoken, s);
        l = SystemClock.elapsedRealtime();
        j = OpenConfig.getInstance(context, qqtoken.getAppId()).getInt("Common_HttpRetryCount");
        f.b("OpenConfig_test", (new StringBuilder()).append("config 1:Common_HttpRetryCount            config_value:").append(j).append("   appid:").append(qqtoken.getAppId()).append("     url:").append(s1).toString());
        if (j == 0)
        {
            j = 3;
        }
        f.b("OpenConfig_test", (new StringBuilder()).append("config 1:Common_HttpRetryCount            result_value:").append(j).append("   appid:").append(qqtoken.getAppId()).append("     url:").append(s1).toString());
        i = 0;
        qqtoken = null;
        k = i + 1;
        statistic = Util.upload(context, s2, bundle);
        s = Util.parseJson(statistic.response);
        try
        {
            i = s.getInt("ret");
        }
        // Misplaced declaration of an exception variable
        catch (QQToken qqtoken)
        {
            i = -4;
        }
        try
        {
            l1 = statistic.reqSize;
            l2 = statistic.rspSize;
        }
        // Misplaced declaration of an exception variable
        catch (QQToken qqtoken)
        {
            qqtoken.printStackTrace();
            g.a().a(s1, l, 0L, 0L, -4);
            throw qqtoken;
        }
        g.a().a(s1, l, l1, l2, i);
        return s;
        obj;
        qqtoken = s;
        s = ((String) (obj));
_L7:
        s.printStackTrace();
        i = -7;
        if (k >= j) goto _L2; else goto _L1
_L1:
        l = SystemClock.elapsedRealtime();
_L4:
        l2 = 0L;
        if (k < j)
        {
            break MISSING_BLOCK_LABEL_532;
        }
        l1 = 0L;
        s = qqtoken;
        break MISSING_BLOCK_LABEL_260;
_L2:
        g.a().a(s1, l, 0L, 0L, -7);
        throw s;
        obj;
        qqtoken = s;
        s = ((String) (obj));
_L5:
        s.printStackTrace();
        i = -8;
        if (k < j)
        {
            l = SystemClock.elapsedRealtime();
        } else
        {
            g.a().a(s1, l, 0L, 0L, -8);
            throw s;
        }
        if (true) goto _L4; else goto _L3
_L3:
        qqtoken;
        qqtoken.printStackTrace();
        context = qqtoken.getMessage();
        try
        {
            i = Integer.parseInt(context.replace("http status code error:", ""));
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            i = -9;
        }
        g.a().a(s1, l, 0L, 0L, i);
        throw qqtoken;
        qqtoken;
        qqtoken.printStackTrace();
        throw qqtoken;
        qqtoken;
        qqtoken.printStackTrace();
        g.a().a(s1, l, 0L, 0L, -3);
        throw qqtoken;
        qqtoken;
        qqtoken.printStackTrace();
        i = getErrorCodeFromException(qqtoken);
        g.a().a(s1, l, 0L, 0L, i);
        throw qqtoken;
        s;
          goto _L5
        s;
        if (true) goto _L7; else goto _L6
_L6:
        for (i = k; false;)
        {
        }

        break MISSING_BLOCK_LABEL_213;
    }


    private class _cls1 extends Thread
    {

        final QQToken a;
        final Context b;
        final String c;
        final Bundle d;
        final String e;
        final IRequestListener f;

        public void run()
        {
            JSONObject jsonobject = HttpUtils.request(a, b, c, d, e);
            if (f != null)
            {
                f.onComplete(jsonobject);
                com.tencent.open.a.f.b("openSDK_LOG", "OpenApi onComplete");
            }
_L2:
            return;
            Object obj;
            obj;
            if (f == null) goto _L2; else goto _L1
_L1:
            f.onMalformedURLException(((MalformedURLException) (obj)));
            com.tencent.open.a.f.b("openSDK_LOG", "OpenApi requestAsync MalformedURLException", ((Throwable) (obj)));
            return;
            obj;
            if (f == null) goto _L2; else goto _L3
_L3:
            f.onConnectTimeoutException(((ConnectTimeoutException) (obj)));
            com.tencent.open.a.f.b("openSDK_LOG", "OpenApi requestAsync onConnectTimeoutException", ((Throwable) (obj)));
            return;
            obj;
            if (f == null) goto _L2; else goto _L4
_L4:
            f.onSocketTimeoutException(((SocketTimeoutException) (obj)));
            com.tencent.open.a.f.b("openSDK_LOG", "OpenApi requestAsync onSocketTimeoutException", ((Throwable) (obj)));
            return;
            obj;
            if (f == null) goto _L2; else goto _L5
_L5:
            f.onNetworkUnavailableException(((NetworkUnavailableException) (obj)));
            com.tencent.open.a.f.b("openSDK_LOG", "OpenApi requestAsync onNetworkUnavailableException", ((Throwable) (obj)));
            return;
            obj;
            if (f == null) goto _L2; else goto _L6
_L6:
            f.onHttpStatusException(((HttpStatusException) (obj)));
            com.tencent.open.a.f.b("openSDK_LOG", "OpenApi requestAsync onHttpStatusException", ((Throwable) (obj)));
            return;
            obj;
            if (f == null) goto _L2; else goto _L7
_L7:
            f.onIOException(((IOException) (obj)));
            com.tencent.open.a.f.b("openSDK_LOG", "OpenApi requestAsync IOException", ((Throwable) (obj)));
            return;
            obj;
            if (f == null) goto _L2; else goto _L8
_L8:
            f.onJSONException(((JSONException) (obj)));
            com.tencent.open.a.f.b("openSDK_LOG", "OpenApi requestAsync JSONException", ((Throwable) (obj)));
            return;
            obj;
            if (f != null)
            {
                f.onUnknowException(((Exception) (obj)));
                com.tencent.open.a.f.b("openSDK_LOG", "OpenApi requestAsync onUnknowException", ((Throwable) (obj)));
                return;
            }
              goto _L2
        }

        _cls1(QQToken qqtoken, Context context, String s, Bundle bundle, String s1, IRequestListener irequestlistener)
        {
            a = qqtoken;
            b = context;
            c = s;
            d = bundle;
            e = s1;
            f = irequestlistener;
            super();
        }
    }

}
