// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import java.lang.reflect.Field;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class ThreadManager
{
    private static class SerialExecutor
        implements Executor
    {

        final Queue a;
        Runnable b;

        protected void a()
        {
            this;
            JVM INSTR monitorenter ;
            Runnable runnable;
            runnable = (Runnable)a.poll();
            b = runnable;
            if (runnable == null)
            {
                break MISSING_BLOCK_LABEL_36;
            }
            ThreadManager.NETWORK_EXECUTOR.execute(b);
            this;
            JVM INSTR monitorexit ;
            return;
            Exception exception;
            exception;
            throw exception;
        }

        public void execute(Runnable runnable)
        {
            this;
            JVM INSTR monitorenter ;
            class _cls1
                implements Runnable
            {

                final Runnable a;
                final SerialExecutor b;

                public void run()
                {
                    a.run();
                    b.a();
                    return;
                    Exception exception;
                    exception;
                    b.a();
                    throw exception;
                }

                _cls1(Runnable runnable)
                {
                    b = SerialExecutor.this;
                    a = runnable;
                    super();
                }
            }

            a.offer(new _cls1(runnable));
            if (b == null)
            {
                a();
            }
            this;
            JVM INSTR monitorexit ;
            return;
            runnable;
            throw runnable;
        }

        private SerialExecutor()
        {
            a = new LinkedList();
        }

    }


    public static final Executor NETWORK_EXECUTOR = a();
    private static Handler a;
    private static Object b = new Object();
    private static Handler c;
    private static HandlerThread d;
    private static Handler e;
    private static HandlerThread f;

    public ThreadManager()
    {
    }

    private static Executor a()
    {
        Object obj;
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            obj = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.SECONDS, new LinkedBlockingQueue());
        } else
        {
            try
            {
                obj = android/os/AsyncTask.getDeclaredField("sExecutor");
                ((Field) (obj)).setAccessible(true);
                obj = (Executor)((Field) (obj)).get(null);
            }
            catch (Exception exception)
            {
                exception = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.SECONDS, new LinkedBlockingQueue());
            }
        }
        if (obj instanceof ThreadPoolExecutor)
        {
            ((ThreadPoolExecutor)obj).setCorePoolSize(3);
        }
        return ((Executor) (obj));
    }

    public static void executeOnFileThread(Runnable runnable)
    {
        getFileThreadHandler().post(runnable);
    }

    public static void executeOnNetWorkThread(Runnable runnable)
    {
        try
        {
            NETWORK_EXECUTOR.execute(runnable);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Runnable runnable)
        {
            return;
        }
    }

    public static void executeOnSubThread(Runnable runnable)
    {
        getSubThreadHandler().post(runnable);
    }

    public static Handler getFileThreadHandler()
    {
        if (e != null) goto _L2; else goto _L1
_L1:
        com/tencent/open/utils/ThreadManager;
        JVM INSTR monitorenter ;
        f = new HandlerThread("SDK_FILE_RW");
        f.start();
        e = new Handler(f.getLooper());
        com/tencent/open/utils/ThreadManager;
        JVM INSTR monitorexit ;
_L2:
        return e;
        Exception exception;
        exception;
        com/tencent/open/utils/ThreadManager;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static Looper getFileThreadLooper()
    {
        return getFileThreadHandler().getLooper();
    }

    public static Handler getMainHandler()
    {
        if (a == null)
        {
            synchronized (b)
            {
                if (a == null)
                {
                    a = new Handler(Looper.getMainLooper());
                }
            }
        }
        return a;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static Thread getSubThread()
    {
        if (d == null)
        {
            getSubThreadHandler();
        }
        return d;
    }

    public static Handler getSubThreadHandler()
    {
        if (c != null) goto _L2; else goto _L1
_L1:
        com/tencent/open/utils/ThreadManager;
        JVM INSTR monitorenter ;
        d = new HandlerThread("SDK_SUB");
        d.start();
        c = new Handler(d.getLooper());
        com/tencent/open/utils/ThreadManager;
        JVM INSTR monitorexit ;
_L2:
        return c;
        Exception exception;
        exception;
        com/tencent/open/utils/ThreadManager;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static Looper getSubThreadLooper()
    {
        return getSubThreadHandler().getLooper();
    }

    public static void init()
    {
    }

    public static Executor newSerialExecutor()
    {
        return new SerialExecutor();
    }

}
