// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.net.Uri;
import android.view.MotionEvent;
import com.tencent.open.a.f;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.util.List;

// Referenced classes of package com.tencent.open.utils:
//            Global, Util

public class SystemUtils
{

    public static final String ACTION_LOGIN = "action_login";
    public static final String ACTION_SHARE = "action_share";
    public static final String H5_SHARE_DATA = "h5_share_data";
    public static final String IS_LOGIN = "is_login";
    public static final String IS_QQ_MOBILE_SHARE = "is_qq_mobile_share";
    public static final String QQDATALINE_CALLBACK_ACTION = "sendToMyComputer";
    public static final String QQFAVORITES_CALLBACK_ACTION = "addToQQFavorites";
    public static final String QQ_SHARE_CALLBACK_ACTION = "shareToQQ";
    public static final String QQ_VERSION_NAME_4_2_0 = "4.2.0";
    public static final String QQ_VERSION_NAME_4_3_0 = "4.3.0";
    public static final String QQ_VERSION_NAME_4_5_0 = "4.5.0";
    public static final String QQ_VERSION_NAME_4_6_0 = "4.6.0";
    public static final String QQ_VERSION_NAME_5_0_0 = "5.0.0";
    public static final String QQ_VERSION_NAME_5_1_0 = "5.1.0";
    public static final String QQ_VERSION_NAME_5_2_0 = "5.2.0";
    public static final String QQ_VERSION_NAME_5_3_0 = "5.3.0";
    public static final String QZONE_SHARE_CALLBACK_ACTION = "shareToQzone";
    public static final String TROOPBAR_CALLBACK_ACTION = "shareToTroopBar";

    public SystemUtils()
    {
    }

    private static long a(InputStream inputstream, OutputStream outputstream)
        throws IOException
    {
        long l = 0L;
        byte abyte0[] = new byte[8192];
        do
        {
            int i = inputstream.read(abyte0, 0, abyte0.length);
            if (i != -1)
            {
                outputstream.write(abyte0, 0, i);
                l += i;
            } else
            {
                f.c("openSDK_LOG.SysUtils", (new StringBuilder()).append("-->copy, copyed size is: ").append(l).toString());
                return l;
            }
        } while (true);
    }

    public static boolean checkMobileQQ(Context context)
    {
label0:
        {
            boolean flag1 = false;
            context = context.getPackageManager();
            int i;
            int j;
            boolean flag;
            try
            {
                context = context.getPackageInfo("com.tencent.mobileqq", 0);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                f.b("checkMobileQQ", "error");
                context.printStackTrace();
                context = null;
            }
            flag = flag1;
            if (context == null)
            {
                break label0;
            }
            context = ((PackageInfo) (context)).versionName;
            try
            {
                f.b("MobileQQ verson", context);
                context = context.split("\\.");
                i = Integer.parseInt(context[0]);
                j = Integer.parseInt(context[1]);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context.printStackTrace();
                return false;
            }
            if (i <= 4)
            {
                flag = flag1;
                if (i != 4)
                {
                    break label0;
                }
                flag = flag1;
                if (j < 1)
                {
                    break label0;
                }
            }
            flag = true;
        }
        return flag;
    }

    public static int compareQQVersion(Context context, String s)
    {
        return compareVersion(getAppVersionName(context, "com.tencent.mobileqq"), s);
    }

    public static int compareVersion(String s, String s1)
    {
        if (s != null || s1 != null) goto _L2; else goto _L1
_L1:
        return 0;
_L2:
        String as[];
        String as1[];
        int i;
        if (s != null && s1 == null)
        {
            return 1;
        }
        if (s == null && s1 != null)
        {
            return -1;
        }
        as = s.split("\\.");
        as1 = s1.split("\\.");
        i = 0;
_L3:
        int j;
        int k;
        if (i >= as.length || i >= as1.length)
        {
            break MISSING_BLOCK_LABEL_88;
        }
        j = Integer.parseInt(as[i]);
        k = Integer.parseInt(as1[i]);
        if (j < k)
        {
            return -1;
        }
        break MISSING_BLOCK_LABEL_117;
        if (as.length > i)
        {
            return 1;
        }
        try
        {
            j = as1.length;
        }
        catch (NumberFormatException numberformatexception)
        {
            return s.compareTo(s1);
        }
        if (j > i)
        {
            return -1;
        }
          goto _L1
        if (j > k)
        {
            return 1;
        }
        i++;
          goto _L3
    }

    public static boolean extractSecureLib(String s, String s1, int i)
    {
        String s2;
        String s3;
        String s4;
        android.content.SharedPreferences.Editor editor;
        Context context;
        s3 = null;
        s2 = null;
        editor = null;
        s4 = null;
        f.c("openSDK_LOG.SysUtils", (new StringBuilder()).append("-->extractSecureLib, libName: ").append(s).toString());
        context = Global.getContext();
        if (context != null) goto _L2; else goto _L1
_L1:
        f.c("openSDK_LOG.SysUtils", "-->extractSecureLib, global context is null. ");
_L4:
        return false;
_L2:
        SharedPreferences sharedpreferences = context.getSharedPreferences("secure_lib", 0);
        File file = new File(context.getFilesDir(), s1);
        if (!file.exists())
        {
            File file1 = file.getParentFile();
            if (file1 != null && file1.mkdirs())
            {
                try
                {
                    file.createNewFile();
                }
                catch (IOException ioexception1)
                {
                    ioexception1.printStackTrace();
                }
            }
        } else
        {
            int j = sharedpreferences.getInt("version", 0);
            f.c("openSDK_LOG.SysUtils", (new StringBuilder()).append("-->extractSecureLib, libVersion: ").append(i).append(" | oldVersion: ").append(j).toString());
            if (i == j)
            {
                return true;
            }
        }
        s = context.getAssets().open(s);
        s3 = s;
        s = s2;
        s2 = s3;
        s4 = editor;
        s1 = context.openFileOutput(s1, 0);
        s = s1;
        s2 = s3;
        s4 = s1;
        a(s3, s1);
        s = s1;
        s2 = s3;
        s4 = s1;
        editor = sharedpreferences.edit();
        s = s1;
        s2 = s3;
        s4 = s1;
        editor.putInt("version", i);
        s = s1;
        s2 = s3;
        s4 = s1;
        editor.commit();
        if (s3 != null)
        {
            try
            {
                s3.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s) { }
        }
        if (s1 != null)
        {
            try
            {
                s1.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s) { }
        }
        return true;
        s1;
        s3 = null;
_L7:
        s = s4;
        s2 = s3;
        f.b("openSDK_LOG.SysUtils", "-->extractSecureLib, when copy lib execption.", s1);
        if (s3 != null)
        {
            try
            {
                s3.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s) { }
        }
        if (s4 == null) goto _L4; else goto _L3
_L3:
        try
        {
            s4.close();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return false;
        }
        return false;
        s1;
        s2 = null;
        s = s3;
_L6:
        if (s2 != null)
        {
            try
            {
                s2.close();
            }
            catch (IOException ioexception) { }
        }
        if (s != null)
        {
            try
            {
                s.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s) { }
        }
        throw s1;
        s1;
        if (true) goto _L6; else goto _L5
_L5:
        s1;
          goto _L7
    }

    public static int getAndroidSDKVersion()
    {
        int i;
        try
        {
            i = Integer.valueOf(android.os.Build.VERSION.SDK).intValue();
        }
        catch (NumberFormatException numberformatexception)
        {
            return 0;
        }
        return i;
    }

    public static String getAppName(Context context)
    {
        return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    }

    public static String getAppSignatureMD5(Context context, String s)
    {
        f.a("openSDK_LOG", "OpenUi, getSignValidString");
        MessageDigest messagedigest;
        String s1 = context.getPackageName();
        context = context.getPackageManager().getPackageInfo(s1, 64).signatures;
        messagedigest = MessageDigest.getInstance("MD5");
        messagedigest.update(context[0].toByteArray());
        context = Util.toHexString(messagedigest.digest());
        messagedigest.reset();
        f.b("SystemUtils", (new StringBuilder()).append("-->sign: ").append(context).toString());
        messagedigest.update(Util.getBytesUTF8((new StringBuilder()).append(s1).append("_").append(context).append("_").append(s).append("").toString()));
        context = Util.toHexString(messagedigest.digest());
        messagedigest.reset();
        f.b("SystemUtils", (new StringBuilder()).append("-->signEncryped: ").append(context).toString());
        return context;
        s;
        context = "";
_L2:
        s.printStackTrace();
        f.b("openSDK_LOG", "OpenUi, getSignValidString error", s);
        return context;
        s;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public static String getAppVersionName(Context context, String s)
    {
        context = context.getPackageManager();
        try
        {
            context = context.getPackageInfo(s, 0).versionName;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static String getRealPathFromUri(Activity activity, Uri uri)
    {
        Object obj = null;
        uri = activity.managedQuery(uri, new String[] {
            "_data"
        }, null, null, null);
        activity = obj;
        if (uri != null)
        {
            int i = uri.getColumnIndexOrThrow("_data");
            uri.moveToFirst();
            activity = uri.getString(i);
        }
        return activity;
    }

    public static boolean isActivityExist(Context context, Intent intent)
    {
        while (context == null || intent == null || context.getPackageManager().queryIntentActivities(intent, 0).size() == 0) 
        {
            return false;
        }
        return true;
    }

    public static boolean isAppSignatureValid(Context context, String s, String s1)
    {
        boolean flag1 = false;
        f.a("openSDK_LOG", "OpenUi, validateAppSignatureForPackage");
        int i;
        int j;
        try
        {
            context = context.getPackageManager().getPackageInfo(s, 64);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        context = ((PackageInfo) (context)).signatures;
        j = context.length;
        i = 0;
        do
        {
label0:
            {
                boolean flag = flag1;
                if (i < j)
                {
                    if (!Util.encrypt(context[i].toCharsString()).equals(s1))
                    {
                        break label0;
                    }
                    flag = true;
                }
                return flag;
            }
            i++;
        } while (true);
    }

    public static boolean isLibExtracted(String s, int i)
    {
        Object obj = Global.getContext();
        if (obj == null)
        {
            f.c("openSDK_LOG.SysUtils", "-->isSecureLibExtracted, global context is null. ");
        } else
        {
            s = new File(((Context) (obj)).getFilesDir(), s);
            obj = ((Context) (obj)).getSharedPreferences("secure_lib", 0);
            if (s.exists())
            {
                int j = ((SharedPreferences) (obj)).getInt("version", 0);
                f.c("openSDK_LOG.SysUtils", (new StringBuilder()).append("-->extractSecureLib, libVersion: ").append(i).append(" | oldVersion: ").append(j).toString());
                if (i == j)
                {
                    return true;
                } else
                {
                    s = ((SharedPreferences) (obj)).edit();
                    s.putInt("version", i);
                    s.commit();
                    return false;
                }
            }
        }
        return false;
    }

    public static boolean isSupportMultiTouch()
    {
        Method amethod[] = android/view/MotionEvent.getDeclaredMethods();
        int j = amethod.length;
        int i = 0;
        boolean flag = false;
        boolean flag1 = false;
        for (; i < j; i++)
        {
            Method method = amethod[i];
            if (method.getName().equals("getPointerCount"))
            {
                flag1 = true;
            }
            if (method.getName().equals("getPointerId"))
            {
                flag = true;
            }
        }

        return getAndroidSDKVersion() >= 7 || flag1 && flag;
    }
}
