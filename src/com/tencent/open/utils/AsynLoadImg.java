// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import com.tencent.open.a.f;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

// Referenced classes of package com.tencent.open.utils:
//            AsynLoadImgBack, Util

public class AsynLoadImg
{

    private static String c;
    private String a;
    private AsynLoadImgBack b;
    private long d;
    private Handler e;
    private Runnable f;

    public AsynLoadImg(Activity activity)
    {
        f = new _cls2();
        e = new _cls1(activity.getMainLooper());
    }

    static AsynLoadImgBack a(AsynLoadImg asynloadimg)
    {
        return asynloadimg.b;
    }

    static String a()
    {
        return c;
    }

    static String b(AsynLoadImg asynloadimg)
    {
        return asynloadimg.a;
    }

    static Handler c(AsynLoadImg asynloadimg)
    {
        return asynloadimg.e;
    }

    static long d(AsynLoadImg asynloadimg)
    {
        return asynloadimg.d;
    }

    public static Bitmap getbitmap(String s)
    {
        com.tencent.open.a.f.a("AsynLoadImg", (new StringBuilder()).append("getbitmap:").append(s).toString());
        Bitmap bitmap;
        try
        {
            Object obj = (HttpURLConnection)(new URL(s)).openConnection();
            ((HttpURLConnection) (obj)).setDoInput(true);
            ((HttpURLConnection) (obj)).connect();
            obj = ((HttpURLConnection) (obj)).getInputStream();
            bitmap = BitmapFactory.decodeStream(((InputStream) (obj)));
            ((InputStream) (obj)).close();
            com.tencent.open.a.f.a("AsynLoadImg", (new StringBuilder()).append("image download finished.").append(s).toString());
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            com.tencent.open.a.f.a("AsynLoadImg", "getbitmap bmp fail---");
            return null;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            com.tencent.open.a.f.a("AsynLoadImg", "getbitmap bmp fail---");
            return null;
        }
        return bitmap;
    }

    public void save(String s, AsynLoadImgBack asynloadimgback)
    {
        com.tencent.open.a.f.a("AsynLoadImg", "--save---");
        if (s == null || s.equals(""))
        {
            asynloadimgback.saved(1, null);
            return;
        }
        if (!Util.hasSDCard())
        {
            asynloadimgback.saved(2, null);
            return;
        } else
        {
            c = (new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/tmp/").toString();
            d = System.currentTimeMillis();
            a = s;
            b = asynloadimgback;
            (new Thread(f)).start();
            return;
        }
    }

    public boolean saveFile(Bitmap bitmap, String s)
    {
        Object obj;
        Object obj1;
        Object obj2;
        String s1;
        s1 = c;
        obj2 = null;
        obj1 = null;
        obj = obj2;
        File file = new File(s1);
        obj = obj2;
        if (file.exists())
        {
            break MISSING_BLOCK_LABEL_45;
        }
        obj = obj2;
        file.mkdir();
        obj = obj2;
        s1 = (new StringBuilder()).append(s1).append(s).toString();
        obj = obj2;
        com.tencent.open.a.f.a("AsynLoadImg", (new StringBuilder()).append("saveFile:").append(s).toString());
        obj = obj2;
        s = new BufferedOutputStream(new FileOutputStream(new File(s1)));
        bitmap.compress(android.graphics.Bitmap.CompressFormat.JPEG, 80, s);
        s.flush();
        boolean flag;
        if (s != null)
        {
            try
            {
                s.close();
            }
            // Misplaced declaration of an exception variable
            catch (Bitmap bitmap)
            {
                bitmap.printStackTrace();
            }
        }
        flag = true;
_L2:
        return flag;
        s;
        bitmap = obj1;
_L5:
        obj = bitmap;
        s.printStackTrace();
        obj = bitmap;
        com.tencent.open.a.f.a("AsynLoadImg", "saveFile bmp fail---");
        flag = false;
        if (bitmap == null) goto _L2; else goto _L1
_L1:
        try
        {
            bitmap.close();
        }
        // Misplaced declaration of an exception variable
        catch (Bitmap bitmap)
        {
            bitmap.printStackTrace();
            return false;
        }
        return false;
        bitmap;
_L4:
        if (obj != null)
        {
            try
            {
                ((BufferedOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }
        throw bitmap;
        bitmap;
        obj = s;
        if (true) goto _L4; else goto _L3
_L3:
        IOException ioexception;
        ioexception;
        bitmap = s;
        s = ioexception;
          goto _L5
    }

    private class _cls2
        implements Runnable
    {

        final AsynLoadImg a;

        public void run()
        {
            com.tencent.open.a.f.a("AsynLoadImg", "saveFileRunnable:");
            String s = Util.encrypt(AsynLoadImg.b(a));
            s = (new StringBuilder()).append("share_qq_").append(s).append(".jpg").toString();
            String s1 = (new StringBuilder()).append(AsynLoadImg.a()).append(s).toString();
            File file = new File(s1);
            Message message = AsynLoadImg.c(a).obtainMessage();
            if (file.exists())
            {
                message.arg1 = 0;
                message.obj = s1;
                com.tencent.open.a.f.a("AsynLoadImg", (new StringBuilder()).append("file exists: time:").append(System.currentTimeMillis() - AsynLoadImg.d(a)).toString());
            } else
            {
                Bitmap bitmap = AsynLoadImg.getbitmap(AsynLoadImg.b(a));
                boolean flag;
                if (bitmap != null)
                {
                    flag = a.saveFile(bitmap, s);
                } else
                {
                    com.tencent.open.a.f.a("AsynLoadImg", "saveFileRunnable:get bmp fail---");
                    flag = false;
                }
                if (flag)
                {
                    message.arg1 = 0;
                    message.obj = s1;
                } else
                {
                    message.arg1 = 1;
                }
                com.tencent.open.a.f.a("AsynLoadImg", (new StringBuilder()).append("file not exists: download time:").append(System.currentTimeMillis() - AsynLoadImg.d(a)).toString());
            }
            AsynLoadImg.c(a).sendMessage(message);
        }

        _cls2()
        {
            a = AsynLoadImg.this;
            super();
        }
    }


    private class _cls1 extends Handler
    {

        final AsynLoadImg a;

        public void handleMessage(Message message)
        {
            com.tencent.open.a.f.a("AsynLoadImg", (new StringBuilder()).append("handleMessage:").append(message.arg1).toString());
            if (message.arg1 == 0)
            {
                AsynLoadImg.a(a).saved(message.arg1, (String)message.obj);
                return;
            } else
            {
                AsynLoadImg.a(a).saved(message.arg1, null);
                return;
            }
        }

        _cls1(Looper looper)
        {
            a = AsynLoadImg.this;
            super(looper);
        }
    }

}
