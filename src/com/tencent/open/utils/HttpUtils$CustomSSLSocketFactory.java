// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.utils;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import org.apache.http.conn.ssl.SSLSocketFactory;

// Referenced classes of package com.tencent.open.utils:
//            HttpUtils

public static class a extends SSLSocketFactory
{

    private final SSLContext a = SSLContext.getInstance("TLS");

    public Socket createSocket()
        throws IOException
    {
        return a.getSocketFactory().createSocket();
    }

    public Socket createSocket(Socket socket, String s, int i, boolean flag)
        throws IOException, UnknownHostException
    {
        return a.getSocketFactory().createSocket(socket, s, i, flag);
    }

    public (KeyStore keystore)
        throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException
    {
        super(keystore);
        try
        {
            keystore = new t>();
        }
        // Misplaced declaration of an exception variable
        catch (KeyStore keystore)
        {
            keystore = null;
        }
        a.init(null, new TrustManager[] {
            keystore
        }, null);
    }
}
