// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.webkit.WebChromeClient;

// Referenced classes of package com.tencent.open:
//            a

public abstract class b extends Dialog
{

    protected a jsBridge;
    protected final WebChromeClient mChromeClient;

    public b(Context context)
    {
        super(context);
        mChromeClient = new _cls1();
    }

    public b(Context context, int i)
    {
        super(context, i);
        mChromeClient = new _cls1();
    }

    protected abstract void onConsoleMessage(String s);

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        jsBridge = new a();
    }

    private class _cls1 extends WebChromeClient
    {

        final b a;

        public void onConsoleMessage(String s, int i, String s1)
        {
            f.c("WebConsole", (new StringBuilder()).append(s).append(" -- From 222 line ").append(i).append(" of ").append(s1).toString());
            if (android.os.Build.VERSION.SDK_INT == 7)
            {
                a.onConsoleMessage(s);
            }
        }

        public boolean onConsoleMessage(ConsoleMessage consolemessage)
        {
            if (consolemessage == null)
            {
                return false;
            }
            f.c("WebConsole", (new StringBuilder()).append(consolemessage.message()).append(" -- From  111 line ").append(consolemessage.lineNumber()).append(" of ").append(consolemessage.sourceId()).toString());
            if (android.os.Build.VERSION.SDK_INT > 7)
            {
                b b1 = a;
                if (consolemessage == null)
                {
                    consolemessage = "";
                } else
                {
                    consolemessage = consolemessage.message();
                }
                b1.onConsoleMessage(consolemessage);
            }
            return true;
        }

        _cls1()
        {
            a = b.this;
            super();
        }
    }

}
