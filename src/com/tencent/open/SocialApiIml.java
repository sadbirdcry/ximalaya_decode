// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import com.tencent.connect.auth.QQAuth;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.open.a.f;
import com.tencent.open.c.b;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.OpenConfig;
import com.tencent.open.utils.ServerSetting;
import com.tencent.open.utils.SystemUtils;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.open:
//            PKDialog, TDialog

public class SocialApiIml extends BaseApi
{
    private class a
        implements IUiListener
    {

        final SocialApiIml a;
        private IUiListener b;
        private String c;
        private String d;
        private Bundle e;
        private Activity f;

        public void onCancel()
        {
            b.onCancel();
        }

        public void onComplete(Object obj)
        {
            obj = (JSONObject)obj;
            try
            {
                obj = ((JSONObject) (obj)).getString("encry_token");
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((JSONException) (obj)).printStackTrace();
                com.tencent.open.a.f.b("openSDK_LOG", "OpenApi, EncrytokenListener() onComplete error", ((Throwable) (obj)));
                obj = null;
            }
            e.putString("encrytoken", ((String) (obj)));
            SocialApiIml.a(a, SocialApiIml.a(a), c, e, d, b);
            if (TextUtils.isEmpty(((CharSequence) (obj))))
            {
                com.tencent.open.a.f.b("miles", "The token get from qq or qzone is empty. Write temp token to localstorage.");
                a.writeEncryToken(f);
            }
        }

        public void onError(UiError uierror)
        {
            com.tencent.open.a.f.b("openSDK_LOG", (new StringBuilder()).append("OpenApi, EncryptTokenListener() onError").append(uierror.errorMessage).toString());
            b.onError(uierror);
        }

        a(Activity activity, IUiListener iuilistener, String s, String s1, Bundle bundle)
        {
            a = SocialApiIml.this;
            super();
            b = iuilistener;
            c = s;
            d = s1;
            e = bundle;
        }
    }


    private static final String a = com/tencent/open/SocialApiIml.getName();
    private Activity b;

    public SocialApiIml(QQAuth qqauth, QQToken qqtoken)
    {
        super(qqauth, qqtoken);
    }

    public SocialApiIml(QQToken qqtoken)
    {
        super(qqtoken);
    }

    static Activity a(SocialApiIml socialapiiml)
    {
        return socialapiiml.b;
    }

    private void a(Activity activity, Intent intent, String s, Bundle bundle, IUiListener iuilistener)
    {
        f.b("openSDK_LOG", (new StringBuilder()).append("-->handleIntentWithAgent ").append(s).append(" params=").append(bundle).append(" activityIntent=").append(intent).toString());
        intent.putExtra("key_action", s);
        intent.putExtra("key_params", bundle);
        mActivityIntent = intent;
        startAssitActivity(activity, iuilistener);
    }

    private void a(Activity activity, Intent intent, String s, Bundle bundle, String s1, IUiListener iuilistener, boolean flag)
    {
        f.b("openSDK_LOG", (new StringBuilder()).append("-->handleIntent ").append(s).append(" params=").append(bundle).append(" activityIntent=").append(intent).toString());
        if (intent != null)
        {
            a(activity, intent, s, bundle, iuilistener);
            return;
        }
        intent = OpenConfig.getInstance(Global.getContext(), mToken.getAppId());
        boolean flag1;
        if (flag || intent.getBoolean("C_LoginH5"))
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        if (flag1)
        {
            a(activity, s, bundle, s1, iuilistener);
            return;
        } else
        {
            handleDownloadLastestQQ(activity, bundle, iuilistener);
            return;
        }
    }

    private void a(Activity activity, String s, Bundle bundle, IUiListener iuilistener)
    {
        Intent intent;
        b = activity;
        Intent intent1 = getAgentIntentWithTarget("com.tencent.open.agent.SocialFriendChooser");
        intent = intent1;
        if (intent1 == null)
        {
            intent = getAgentIntentWithTarget("com.tencent.open.agent.RequestFreegiftActivity");
        }
        bundle.putAll(composeActivityParams());
        if (!"action_ask".equals(s)) goto _L2; else goto _L1
_L1:
        bundle.putString("type", "request");
_L4:
        a(activity, intent, s, bundle, ServerSetting.getInstance().getEnvUrl(Global.getContext(), "http://qzs.qq.com/open/mobile/request/sdk_request.html?"), iuilistener, false);
        return;
_L2:
        if ("action_gift".equals(s))
        {
            bundle.putString("type", "freegift");
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void a(Activity activity, String s, Bundle bundle, String s1, IUiListener iuilistener)
    {
        f.b("openSDK_LOG", (new StringBuilder()).append("-->handleIntentWithH5 ").append(s).append(" params=").append(bundle).toString());
        Intent intent = getTargetActivityIntent("com.tencent.open.agent.AgentActivity");
        s = new a(activity, iuilistener, s, s1, bundle);
        bundle = getTargetActivityIntent("com.tencent.open.agent.EncryTokenActivity");
        if (bundle != null && intent != null && intent.getComponent() != null && bundle.getComponent() != null && intent.getComponent().getPackageName().equals(bundle.getComponent().getPackageName()))
        {
            bundle.putExtra("oauth_consumer_key", mToken.getAppId());
            bundle.putExtra("openid", mToken.getOpenId());
            bundle.putExtra("access_token", mToken.getAccessToken());
            bundle.putExtra("key_action", "action_check_token");
            mActivityIntent = bundle;
            if (hasActivityForIntent())
            {
                startAssitActivity(activity, s);
            }
            return;
        }
        bundle = Util.encrypt((new StringBuilder()).append("tencent&sdk&qazxc***14969%%").append(mToken.getAccessToken()).append(mToken.getAppId()).append(mToken.getOpenId()).append("qzone3.4").toString());
        activity = new JSONObject();
        try
        {
            activity.put("encry_token", bundle);
        }
        // Misplaced declaration of an exception variable
        catch (Bundle bundle)
        {
            bundle.printStackTrace();
        }
        s.onComplete(activity);
    }

    private void a(Context context, String s, Bundle bundle, String s1, IUiListener iuilistener)
    {
        f.a("openSDK_LOG", "OpenUi, showDialog --start");
        CookieSyncManager.createInstance(context);
        bundle.putString("oauth_consumer_key", mToken.getAppId());
        if (mToken.isSessionValid())
        {
            bundle.putString("access_token", mToken.getAccessToken());
        }
        context = mToken.getOpenId();
        if (context != null)
        {
            bundle.putString("openid", context);
        }
        try
        {
            bundle.putString("pf", Global.getContext().getSharedPreferences("pfStore", 0).getString("pf", "openmobile_android"));
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            bundle.putString("pf", "openmobile_android");
        }
        context = new StringBuilder();
        context.append(s1);
        context.append(Util.encodeUrl(bundle));
        context = context.toString();
        f.b("openSDK_LOG", "OpenUi, showDialog TDialog");
        if ("action_challenge".equals(s) || "action_brag".equals(s))
        {
            f.b("openSDK_LOG", "OpenUi, showDialog PKDialog");
            (new PKDialog(b, s, context, iuilistener, mToken)).show();
            return;
        } else
        {
            (new TDialog(b, s, context, iuilistener, mToken)).show();
            return;
        }
    }

    static void a(SocialApiIml socialapiiml, Context context, String s, Bundle bundle, String s1, IUiListener iuilistener)
    {
        socialapiiml.a(context, s, bundle, s1, iuilistener);
    }

    public void ask(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        a(activity, "action_ask", bundle, iuilistener);
    }

    protected Intent getTargetActivityIntent(String s)
    {
        Intent intent = new Intent();
        intent.setClassName("com.qzone", s);
        Intent intent1 = new Intent();
        intent1.setClassName("com.tencent.mobileqq", s);
        if (SystemUtils.isActivityExist(Global.getContext(), intent1) && SystemUtils.compareQQVersion(Global.getContext(), "4.7") >= 0)
        {
            s = intent1;
        } else
        if (SystemUtils.isActivityExist(Global.getContext(), intent) && SystemUtils.compareVersion(SystemUtils.getAppVersionName(Global.getContext(), "com.qzone"), "4.2") >= 0)
        {
            s = intent;
            if (!SystemUtils.isAppSignatureValid(Global.getContext(), intent.getComponent().getPackageName(), "ec96e9ac1149251acbb1b0c5777cae95"))
            {
                return null;
            }
        } else
        {
            return null;
        }
        return s;
    }

    public void gift(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        a(activity, "action_gift", bundle, iuilistener);
    }

    public void invite(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        b = activity;
        Intent intent1 = getAgentIntentWithTarget("com.tencent.open.agent.SocialFriendChooser");
        Intent intent = intent1;
        if (intent1 == null)
        {
            intent = getAgentIntentWithTarget("com.tencent.open.agent.AppInvitationActivity");
        }
        bundle.putAll(composeActivityParams());
        a(activity, intent, "action_invite", bundle, ServerSetting.getInstance().getEnvUrl(Global.getContext(), "http://qzs.qq.com/open/mobile/invite/sdk_invite.html?"), iuilistener, false);
    }

    public void story(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        b = activity;
        Intent intent = getAgentIntentWithTarget("com.tencent.open.agent.SendStoryActivity");
        bundle.putAll(composeActivityParams());
        a(activity, intent, "action_story", bundle, ServerSetting.getInstance().getEnvUrl(Global.getContext(), "http://qzs.qq.com/open/mobile/sendstory/sdk_sendstory_v1.3.html?"), iuilistener, false);
    }

    public void writeEncryToken(Context context)
    {
        String s = mToken.getAccessToken();
        Object obj = mToken.getAppId();
        Object obj1 = mToken.getOpenId();
        if (s != null && s.length() > 0 && obj != null && ((String) (obj)).length() > 0 && obj1 != null && ((String) (obj1)).length() > 0)
        {
            s = Util.encrypt((new StringBuilder()).append("tencent&sdk&qazxc***14969%%").append(s).append(((String) (obj))).append(((String) (obj1))).append("qzone3.4").toString());
        } else
        {
            s = null;
        }
        obj = new b(context);
        obj1 = ((b) (obj)).getSettings();
        ((WebSettings) (obj1)).setDomStorageEnabled(true);
        ((WebSettings) (obj1)).setJavaScriptEnabled(true);
        ((WebSettings) (obj1)).setDatabaseEnabled(true);
        s = (new StringBuilder()).append("<!DOCTYPE HTML><html lang=\"en-US\"><head><meta charset=\"UTF-8\"><title>localStorage Test</title><script type=\"text/javascript\">document.domain = 'qq.com';localStorage[\"").append(mToken.getOpenId()).append("_").append(mToken.getAppId()).append("\"]=\"").append(s).append("\";</script></head><body></body></html>").toString();
        context = ServerSetting.getInstance().getEnvUrl(context, "http://qzs.qq.com");
        ((b) (obj)).loadDataWithBaseURL(context, s, "text/html", "utf-8", context);
    }

}
