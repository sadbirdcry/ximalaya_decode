// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.connect.auth.QQToken;
import com.tencent.open.a.f;
import com.tencent.open.b.g;
import com.tencent.open.c.b;
import com.tencent.open.utils.ServerSetting;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.File;
import java.lang.ref.WeakReference;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.open:
//            b, a

public class TDialog extends com.tencent.open.b
{
    private class FbWebViewClient extends WebViewClient
    {

        final TDialog this$0;

        public void onPageFinished(WebView webview, String s)
        {
            super.onPageFinished(webview, s);
            TDialog.d(TDialog.this).setVisibility(0);
        }

        public void onPageStarted(WebView webview, String s, Bitmap bitmap)
        {
            Util.logd("TDialog", (new StringBuilder()).append("Webview loading URL: ").append(s).toString());
            super.onPageStarted(webview, s, bitmap);
        }

        public void onReceivedError(WebView webview, int i1, String s, String s1)
        {
            super.onReceivedError(webview, i1, s, s1);
            TDialog.c(TDialog.this).onError(new UiError(i1, s, s1));
            if (TDialog.a(TDialog.this) != null && TDialog.a(TDialog.this).get() != null)
            {
                Toast.makeText((Context)TDialog.a(TDialog.this).get(), "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\u6216\u7CFB\u7EDF\u9519\u8BEF", 0).show();
            }
            dismiss();
        }

        public boolean shouldOverrideUrlLoading(WebView webview, String s)
        {
            Util.logd("TDialog", (new StringBuilder()).append("Redirect URL: ").append(s).toString());
            if (s.startsWith(ServerSetting.getInstance().getEnvUrl((Context)TDialog.a(TDialog.this).get(), "auth://tauth.qq.com/")))
            {
                TDialog.c(TDialog.this).onComplete(Util.parseUrlToJson(s));
                if (isShowing())
                {
                    dismiss();
                }
                return true;
            }
            if (s.startsWith("auth://cancel"))
            {
                TDialog.c(TDialog.this).onCancel();
                if (isShowing())
                {
                    dismiss();
                }
                return true;
            }
            if (s.startsWith("auth://close"))
            {
                if (isShowing())
                {
                    dismiss();
                }
                return true;
            }
            if (s.startsWith("download://"))
            {
                webview = new Intent("android.intent.action.VIEW", Uri.parse(Uri.decode(s.substring("download://".length()))));
                webview.addFlags(0x10000000);
                if (TDialog.a(TDialog.this) != null && TDialog.a(TDialog.this).get() != null)
                {
                    ((Context)TDialog.a(TDialog.this).get()).startActivity(webview);
                }
                return true;
            }
            return s.startsWith("auth://progress");
        }

        private FbWebViewClient()
        {
            this$0 = TDialog.this;
            super();
        }

    }

    private class JsListener extends a.b
    {

        final TDialog this$0;

        public void onAddShare(String s)
        {
            com.tencent.open.a.f.b("TDialog", "onAddShare");
            onComplete(s);
        }

        public void onCancel(String s)
        {
            com.tencent.open.a.f.b("TDialog", (new StringBuilder()).append("onCancel --msg = ").append(s).toString());
            com.tencent.open.TDialog.b(TDialog.this).obtainMessage(2, s).sendToTarget();
            dismiss();
        }

        public void onCancelAddShare(String s)
        {
            com.tencent.open.a.f.b("TDialog", "onCancelAddShare");
            onCancel("cancel");
        }

        public void onCancelInvite()
        {
            com.tencent.open.a.f.b("TDialog", "onCancelInvite");
            onCancel("");
        }

        public void onCancelLogin()
        {
            onCancel("");
        }

        public void onComplete(String s)
        {
            com.tencent.open.TDialog.b(TDialog.this).obtainMessage(1, s).sendToTarget();
            com.tencent.open.a.f.e("onComplete", s);
            dismiss();
        }

        public void onInvite(String s)
        {
            onComplete(s);
        }

        public void onLoad(String s)
        {
            com.tencent.open.TDialog.b(TDialog.this).obtainMessage(4, s).sendToTarget();
        }

        public void showMsg(String s)
        {
            com.tencent.open.TDialog.b(TDialog.this).obtainMessage(3, s).sendToTarget();
        }

        private JsListener()
        {
            this$0 = TDialog.this;
            super();
        }

    }

    private static class OnTimeListener
        implements IUiListener
    {

        private String mAction;
        String mAppid;
        String mUrl;
        private WeakReference mWeakCtx;
        private IUiListener mWeakL;

        private void onComplete(String s)
        {
            try
            {
                onComplete(Util.parseJson(s));
                return;
            }
            catch (JSONException jsonexception)
            {
                jsonexception.printStackTrace();
            }
            onError(new UiError(-4, "\u670D\u52A1\u5668\u8FD4\u56DE\u6570\u636E\u683C\u5F0F\u6709\u8BEF!", s));
        }

        public void onCancel()
        {
            if (mWeakL != null)
            {
                mWeakL.onCancel();
                mWeakL = null;
            }
        }

        public void onComplete(Object obj)
        {
            obj = (JSONObject)obj;
            com.tencent.open.b.g.a().a((new StringBuilder()).append(mAction).append("_H5").toString(), SystemClock.elapsedRealtime(), 0L, 0L, ((JSONObject) (obj)).optInt("ret", -6), mUrl, false);
            if (mWeakL != null)
            {
                mWeakL.onComplete(obj);
                mWeakL = null;
            }
        }

        public void onError(UiError uierror)
        {
            String s;
            if (uierror.errorMessage != null)
            {
                s = (new StringBuilder()).append(uierror.errorMessage).append(mUrl).toString();
            } else
            {
                s = mUrl;
            }
            com.tencent.open.b.g.a().a((new StringBuilder()).append(mAction).append("_H5").toString(), SystemClock.elapsedRealtime(), 0L, 0L, uierror.errorCode, s, false);
            if (mWeakL != null)
            {
                mWeakL.onError(uierror);
                mWeakL = null;
            }
        }


        public OnTimeListener(Context context, String s, String s1, String s2, IUiListener iuilistener)
        {
            mWeakCtx = new WeakReference(context);
            mAction = s;
            mUrl = s1;
            mAppid = s2;
            mWeakL = iuilistener;
        }
    }

    private class THandler extends Handler
    {

        private OnTimeListener mL;
        final TDialog this$0;

        public void handleMessage(Message message)
        {
            com.tencent.open.a.f.b("TAG", (new StringBuilder()).append("--handleMessage--msg.WHAT = ").append(message.what).toString());
            message.what;
            JVM INSTR tableswitch 1 5: default 64
        //                       1 65
        //                       2 80
        //                       3 88
        //                       4 64
        //                       5 135;
               goto _L1 _L2 _L3 _L4 _L1 _L5
_L1:
            return;
_L2:
            mL.onComplete((String)message.obj);
            return;
_L3:
            mL.onCancel();
            return;
_L4:
            if (TDialog.a(TDialog.this) != null && TDialog.a(TDialog.this).get() != null)
            {
                TDialog.a((Context)TDialog.a(TDialog.this).get(), (String)message.obj);
                return;
            }
            continue; /* Loop/switch isn't completed */
_L5:
            if (TDialog.a(TDialog.this) != null && TDialog.a(TDialog.this).get() != null)
            {
                com.tencent.open.TDialog.b((Context)TDialog.a(TDialog.this).get(), (String)message.obj);
                return;
            }
            if (true) goto _L1; else goto _L6
_L6:
        }

        public THandler(OnTimeListener ontimelistener, Looper looper)
        {
            this$0 = TDialog.this;
            super(looper);
            mL = ontimelistener;
        }
    }


    static final android.widget.FrameLayout.LayoutParams a = new android.widget.FrameLayout.LayoutParams(-1, -1);
    static Toast b = null;
    private static WeakReference d;
    private WeakReference c;
    private String e;
    private OnTimeListener f;
    private IUiListener g;
    private FrameLayout h;
    private b i;
    private Handler j;
    private boolean k;
    private QQToken l;

    public TDialog(Context context, String s, String s1, IUiListener iuilistener, QQToken qqtoken)
    {
        super(context, 0x1030010);
        k = false;
        l = null;
        c = new WeakReference(context);
        e = s1;
        f = new OnTimeListener(context, s, s1, qqtoken.getAppId(), iuilistener);
        j = new THandler(f, context.getMainLooper());
        g = iuilistener;
        l = qqtoken;
    }

    static WeakReference a(TDialog tdialog)
    {
        return tdialog.c;
    }

    private void a()
    {
        (new TextView((Context)c.get())).setText("test");
        android.widget.FrameLayout.LayoutParams layoutparams = new android.widget.FrameLayout.LayoutParams(-1, -1);
        i = new b((Context)c.get());
        i.setLayoutParams(layoutparams);
        h = new FrameLayout((Context)c.get());
        layoutparams.gravity = 17;
        h.setLayoutParams(layoutparams);
        h.addView(i);
        setContentView(h);
    }

    static void a(Context context, String s)
    {
        c(context, s);
    }

    static Handler b(TDialog tdialog)
    {
        return tdialog.j;
    }

    private void b()
    {
        i.setVerticalScrollBarEnabled(false);
        i.setHorizontalScrollBarEnabled(false);
        i.setWebViewClient(new FbWebViewClient());
        i.setWebChromeClient(mChromeClient);
        i.clearFormData();
        WebSettings websettings = i.getSettings();
        if (websettings == null)
        {
            return;
        }
        websettings.setSavePassword(false);
        websettings.setSaveFormData(false);
        websettings.setCacheMode(-1);
        websettings.setNeedInitialFocus(false);
        websettings.setBuiltInZoomControls(true);
        websettings.setSupportZoom(true);
        websettings.setRenderPriority(android.webkit.WebSettings.RenderPriority.HIGH);
        websettings.setJavaScriptEnabled(true);
        if (c != null && c.get() != null)
        {
            websettings.setDatabaseEnabled(true);
            websettings.setDatabasePath(((Context)c.get()).getApplicationContext().getDir("databases", 0).getPath());
        }
        websettings.setDomStorageEnabled(true);
        jsBridge.a(new JsListener(), "sdk_js_if");
        i.loadUrl(e);
        i.setLayoutParams(a);
        i.setVisibility(4);
        i.getSettings().setSavePassword(false);
    }

    static void b(Context context, String s)
    {
        d(context, s);
    }

    static OnTimeListener c(TDialog tdialog)
    {
        return tdialog.f;
    }

    private static void c(Context context, String s)
    {
        int i1;
        try
        {
            s = Util.parseJson(s);
            i1 = s.getInt("type");
            s = s.getString("msg");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return;
        }
        if (i1 != 0)
        {
            break MISSING_BLOCK_LABEL_82;
        }
        if (b != null)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        b = Toast.makeText(context, s, 0);
_L2:
        b.show();
        return;
        b.setView(b.getView());
        b.setText(s);
        b.setDuration(0);
        if (true) goto _L2; else goto _L1
_L1:
        if (i1 != 1)
        {
            break MISSING_BLOCK_LABEL_138;
        }
        if (b != null)
        {
            break MISSING_BLOCK_LABEL_109;
        }
        b = Toast.makeText(context, s, 1);
_L4:
        b.show();
        return;
        b.setView(b.getView());
        b.setText(s);
        b.setDuration(1);
        if (true) goto _L4; else goto _L3
_L3:
    }

    static b d(TDialog tdialog)
    {
        return tdialog.i;
    }

    private static void d(Context context, String s)
    {
        if (context != null && s != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int i1;
        try
        {
            s = Util.parseJson(s);
            i1 = s.getInt("action");
            s = s.getString("msg");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return;
        }
        if (i1 != 1)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (d == null || d.get() == null)
        {
            context = new ProgressDialog(context);
            context.setMessage(s);
            d = new WeakReference(context);
            context.show();
            return;
        }
        ((ProgressDialog)d.get()).setMessage(s);
        if (((ProgressDialog)d.get()).isShowing()) goto _L1; else goto _L3
_L3:
        ((ProgressDialog)d.get()).show();
        return;
        if (i1 != 0) goto _L1; else goto _L4
_L4:
        if (d == null || d.get() == null || !((ProgressDialog)d.get()).isShowing()) goto _L1; else goto _L5
_L5:
        ((ProgressDialog)d.get()).dismiss();
        d = null;
        return;
    }

    public void onBackPressed()
    {
        if (f != null)
        {
            f.onCancel();
        }
        super.onBackPressed();
    }

    protected void onConsoleMessage(String s)
    {
        com.tencent.open.a.f.b("TDialog", "--onConsoleMessage--");
        try
        {
            jsBridge.a(i, s);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        a();
        b();
    }

}
