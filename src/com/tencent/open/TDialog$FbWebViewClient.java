// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.tencent.open.c.b;
import com.tencent.open.utils.ServerSetting;
import com.tencent.open.utils.Util;
import com.tencent.tauth.UiError;
import java.lang.ref.WeakReference;

// Referenced classes of package com.tencent.open:
//            TDialog

private class <init> extends WebViewClient
{

    final TDialog this$0;

    public void onPageFinished(WebView webview, String s)
    {
        super.onPageFinished(webview, s);
        TDialog.d(TDialog.this).setVisibility(0);
    }

    public void onPageStarted(WebView webview, String s, Bitmap bitmap)
    {
        Util.logd("TDialog", (new StringBuilder()).append("Webview loading URL: ").append(s).toString());
        super.onPageStarted(webview, s, bitmap);
    }

    public void onReceivedError(WebView webview, int i, String s, String s1)
    {
        super.onReceivedError(webview, i, s, s1);
        TDialog.c(TDialog.this).nError(new UiError(i, s, s1));
        if (TDialog.a(TDialog.this) != null && TDialog.a(TDialog.this).get() != null)
        {
            Toast.makeText((Context)TDialog.a(TDialog.this).get(), "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\u6216\u7CFB\u7EDF\u9519\u8BEF", 0).show();
        }
        dismiss();
    }

    public boolean shouldOverrideUrlLoading(WebView webview, String s)
    {
        Util.logd("TDialog", (new StringBuilder()).append("Redirect URL: ").append(s).toString());
        if (s.startsWith(ServerSetting.getInstance().getEnvUrl((Context)TDialog.a(TDialog.this).get(), "auth://tauth.qq.com/")))
        {
            TDialog.c(TDialog.this).nComplete(Util.parseUrlToJson(s));
            if (isShowing())
            {
                dismiss();
            }
            return true;
        }
        if (s.startsWith("auth://cancel"))
        {
            TDialog.c(TDialog.this).nCancel();
            if (isShowing())
            {
                dismiss();
            }
            return true;
        }
        if (s.startsWith("auth://close"))
        {
            if (isShowing())
            {
                dismiss();
            }
            return true;
        }
        if (s.startsWith("download://"))
        {
            webview = new Intent("android.intent.action.VIEW", Uri.parse(Uri.decode(s.substring("download://".length()))));
            webview.addFlags(0x10000000);
            if (TDialog.a(TDialog.this) != null && TDialog.a(TDialog.this).get() != null)
            {
                ((Context)TDialog.a(TDialog.this).get()).startActivity(webview);
            }
            return true;
        }
        return s.startsWith("auth://progress");
    }

    private ()
    {
        this$0 = TDialog.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
