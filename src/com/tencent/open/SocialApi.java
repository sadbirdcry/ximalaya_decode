// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open;

import android.app.Activity;
import android.os.Bundle;
import com.tencent.connect.auth.QQToken;
import com.tencent.tauth.IUiListener;

// Referenced classes of package com.tencent.open:
//            SocialApiIml

public class SocialApi
{

    private SocialApiIml a;

    public SocialApi(QQToken qqtoken)
    {
        a = new SocialApiIml(qqtoken);
    }

    public void ask(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        a.ask(activity, bundle, iuilistener);
    }

    public void gift(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        a.gift(activity, bundle, iuilistener);
    }

    public void invite(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        a.invite(activity, bundle, iuilistener);
    }

    public void story(Activity activity, Bundle bundle, IUiListener iuilistener)
    {
        a.story(activity, bundle, iuilistener);
    }
}
