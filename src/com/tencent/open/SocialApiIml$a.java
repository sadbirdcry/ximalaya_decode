// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import com.tencent.open.a.f;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.open:
//            SocialApiIml

private class e
    implements IUiListener
{

    final SocialApiIml a;
    private IUiListener b;
    private String c;
    private String d;
    private Bundle e;
    private Activity f;

    public void onCancel()
    {
        b.onCancel();
    }

    public void onComplete(Object obj)
    {
        obj = (JSONObject)obj;
        try
        {
            obj = ((JSONObject) (obj)).getString("encry_token");
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((JSONException) (obj)).printStackTrace();
            com.tencent.open.a.f.b("openSDK_LOG", "OpenApi, EncrytokenListener() onComplete error", ((Throwable) (obj)));
            obj = null;
        }
        e.putString("encrytoken", ((String) (obj)));
        SocialApiIml.a(a, SocialApiIml.a(a), c, e, d, b);
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            com.tencent.open.a.f.b("miles", "The token get from qq or qzone is empty. Write temp token to localstorage.");
            a.writeEncryToken(f);
        }
    }

    public void onError(UiError uierror)
    {
        com.tencent.open.a.f.b("openSDK_LOG", (new StringBuilder()).append("OpenApi, EncryptTokenListener() onError").append(uierror.errorMessage).toString());
        b.onError(uierror);
    }

    (SocialApiIml socialapiiml, Activity activity, IUiListener iuilistener, String s, String s1, Bundle bundle)
    {
        a = socialapiiml;
        super();
        b = iuilistener;
        c = s;
        d = s1;
        e = bundle;
    }
}
