// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.tencent.connect.auth.QQToken;
import com.tencent.open.a.f;
import com.tencent.open.b.g;
import com.tencent.open.c.a;
import com.tencent.open.c.b;
import com.tencent.open.utils.ServerSetting;
import com.tencent.open.utils.Util;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.File;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.open:
//            b, a

public class PKDialog extends com.tencent.open.b
    implements com.tencent.open.c.a.a
{
    private class FbWebViewClient extends WebViewClient
    {

        final PKDialog this$0;

        public void onPageFinished(WebView webview, String s)
        {
            super.onPageFinished(webview, s);
            mWebView.setVisibility(0);
        }

        public void onPageStarted(WebView webview, String s, Bitmap bitmap)
        {
            Util.logd(PKDialog.TAG, (new StringBuilder()).append("Webview loading URL: ").append(s).toString());
            super.onPageStarted(webview, s, bitmap);
        }

        public void onReceivedError(WebView webview, int i, String s, String s1)
        {
            super.onReceivedError(webview, i, s, s1);
            mListener.onError(new UiError(i, s, s1));
            if (mWeakContext != null && mWeakContext.get() != null)
            {
                Toast.makeText((Context)mWeakContext.get(), "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\u6216\u7CFB\u7EDF\u9519\u8BEF", 0).show();
            }
            dismiss();
        }

        public boolean shouldOverrideUrlLoading(WebView webview, String s)
        {
            Util.logd(PKDialog.TAG, (new StringBuilder()).append("Redirect URL: ").append(s).toString());
            if (s.startsWith(ServerSetting.getInstance().getEnvUrl((Context)mWeakContext.get(), "auth://tauth.qq.com/")))
            {
                mListener.onComplete(Util.parseUrlToJson(s));
                dismiss();
                return true;
            }
            if (s.startsWith("auth://cancel"))
            {
                mListener.onCancel();
                dismiss();
                return true;
            }
            if (s.startsWith("auth://close"))
            {
                dismiss();
                return true;
            } else
            {
                return false;
            }
        }

        private FbWebViewClient()
        {
            this$0 = PKDialog.this;
            super();
        }

    }

    private class JsListener extends a.b
    {

        final PKDialog this$0;

        public void onCancel(String s)
        {
            mHandler.obtainMessage(2, s).sendToTarget();
            dismiss();
        }

        public void onComplete(String s)
        {
            mHandler.obtainMessage(1, s).sendToTarget();
            f.e("onComplete", s);
            dismiss();
        }

        public void onLoad(String s)
        {
            mHandler.obtainMessage(4, s).sendToTarget();
        }

        public void showMsg(String s)
        {
            mHandler.obtainMessage(3, s).sendToTarget();
        }

        private JsListener()
        {
            this$0 = PKDialog.this;
            super();
        }

    }

    private static class OnTimeListener
        implements IUiListener
    {

        private String mAction;
        String mAppid;
        String mUrl;
        private WeakReference mWeakCtx;
        private IUiListener mWeakL;

        private void onComplete(String s)
        {
            try
            {
                onComplete(Util.parseJson(s));
                return;
            }
            catch (JSONException jsonexception)
            {
                jsonexception.printStackTrace();
            }
            onError(new UiError(-4, "\u670D\u52A1\u5668\u8FD4\u56DE\u6570\u636E\u683C\u5F0F\u6709\u8BEF!", s));
        }

        public void onCancel()
        {
            if (mWeakL != null)
            {
                mWeakL.onCancel();
                mWeakL = null;
            }
        }

        public void onComplete(Object obj)
        {
            obj = (JSONObject)obj;
            g.a().a((new StringBuilder()).append(mAction).append("_H5").toString(), SystemClock.elapsedRealtime(), 0L, 0L, ((JSONObject) (obj)).optInt("ret", -6), mUrl, false);
            if (mWeakL != null)
            {
                mWeakL.onComplete(obj);
                mWeakL = null;
            }
        }

        public void onError(UiError uierror)
        {
            String s;
            if (uierror.errorMessage != null)
            {
                s = (new StringBuilder()).append(uierror.errorMessage).append(mUrl).toString();
            } else
            {
                s = mUrl;
            }
            g.a().a((new StringBuilder()).append(mAction).append("_H5").toString(), SystemClock.elapsedRealtime(), 0L, 0L, uierror.errorCode, s, false);
            if (mWeakL != null)
            {
                mWeakL.onError(uierror);
                mWeakL = null;
            }
        }


        public OnTimeListener(Context context, String s, String s1, String s2, IUiListener iuilistener)
        {
            mWeakCtx = new WeakReference(context);
            mAction = s;
            mUrl = s1;
            mAppid = s2;
            mWeakL = iuilistener;
        }
    }

    private class THandler extends Handler
    {

        private OnTimeListener mL;
        final PKDialog this$0;

        public void handleMessage(Message message)
        {
            f.b("PKDialog", (new StringBuilder()).append("msg = ").append(message.what).toString());
            message.what;
            JVM INSTR tableswitch 1 5: default 64
        //                       1 65
        //                       2 80
        //                       3 88
        //                       4 64
        //                       5 135;
               goto _L1 _L2 _L3 _L4 _L1 _L5
_L1:
            return;
_L2:
            mL.onComplete((String)message.obj);
            return;
_L3:
            mL.onCancel();
            return;
_L4:
            if (mWeakContext != null && mWeakContext.get() != null)
            {
                PKDialog.showTips((Context)mWeakContext.get(), (String)message.obj);
                return;
            }
            continue; /* Loop/switch isn't completed */
_L5:
            if (mWeakContext != null && mWeakContext.get() != null)
            {
                PKDialog.showProcessDialog((Context)mWeakContext.get(), (String)message.obj);
                return;
            }
            if (true) goto _L1; else goto _L6
_L6:
        }

        public THandler(OnTimeListener ontimelistener, Looper looper)
        {
            this$0 = PKDialog.this;
            super(looper);
            mL = ontimelistener;
        }
    }


    private static final int MSG_CANCEL = 2;
    private static final int MSG_COMPLETE = 1;
    private static final int MSG_ON_LOAD = 4;
    private static final int MSG_SHOW_PROCESS = 5;
    private static final int MSG_SHOW_TIPS = 3;
    private static final String TAG = com/tencent/open/PKDialog.getName();
    private static final int WEBVIEW_HEIGHT = 185;
    static Toast sToast = null;
    private IUiListener listener;
    private a mFlMain;
    private Handler mHandler;
    private OnTimeListener mListener;
    private String mUrl;
    private WeakReference mWeakContext;
    private b mWebView;
    private int mWebviewHeight;

    public PKDialog(Context context, String s, String s1, IUiListener iuilistener, QQToken qqtoken)
    {
        super(context, 0x1030010);
        mWeakContext = new WeakReference(context);
        mUrl = s1;
        mListener = new OnTimeListener(context, s, s1, qqtoken.getAppId(), iuilistener);
        mHandler = new THandler(mListener, context.getMainLooper());
        listener = iuilistener;
        mWebviewHeight = Math.round(185F * context.getResources().getDisplayMetrics().density);
        f.e(TAG, (new StringBuilder()).append("density=").append(context.getResources().getDisplayMetrics().density).append("; webviewHeight=").append(mWebviewHeight).toString());
    }

    private void createViews()
    {
        mFlMain = new a((Context)mWeakContext.get());
        mFlMain.setBackgroundColor(0x66000000);
        mFlMain.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(-1, -1));
        mWebView = new b((Context)mWeakContext.get());
        mWebView.setBackgroundColor(0);
        mWebView.setBackgroundDrawable(null);
        android.widget.RelativeLayout.LayoutParams layoutparams;
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            try
            {
                android/view/View.getMethod("setLayerType", new Class[] {
                    Integer.TYPE, android/graphics/Paint
                }).invoke(mWebView, new Object[] {
                    Integer.valueOf(1), new Paint()
                });
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, mWebviewHeight);
        layoutparams.addRule(13, -1);
        mWebView.setLayoutParams(layoutparams);
        mFlMain.addView(mWebView);
        mFlMain.a(this);
        setContentView(mFlMain);
    }

    private void initViews()
    {
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.setWebViewClient(new FbWebViewClient());
        mWebView.setWebChromeClient(mChromeClient);
        mWebView.clearFormData();
        WebSettings websettings = mWebView.getSettings();
        if (websettings == null)
        {
            return;
        }
        websettings.setSavePassword(false);
        websettings.setSaveFormData(false);
        websettings.setCacheMode(-1);
        websettings.setNeedInitialFocus(false);
        websettings.setBuiltInZoomControls(true);
        websettings.setSupportZoom(true);
        websettings.setRenderPriority(android.webkit.WebSettings.RenderPriority.HIGH);
        websettings.setJavaScriptEnabled(true);
        if (mWeakContext != null && mWeakContext.get() != null)
        {
            websettings.setDatabaseEnabled(true);
            websettings.setDatabasePath(((Context)mWeakContext.get()).getApplicationContext().getDir("databases", 0).getPath());
        }
        websettings.setDomStorageEnabled(true);
        jsBridge.a(new JsListener(), "sdk_js_if");
        mWebView.clearView();
        mWebView.loadUrl(mUrl);
        mWebView.getSettings().setSavePassword(false);
    }

    private void loadUrlWithBrowser(String s, String s1, String s2)
        throws Exception
    {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(s, s1));
        intent.setAction("android.intent.action.VIEW");
        intent.addFlags(0x40000000);
        intent.addFlags(0x10000000);
        intent.setData(Uri.parse(s2));
        if (mWeakContext != null && mWeakContext.get() != null)
        {
            ((Context)mWeakContext.get()).startActivity(intent);
        }
    }

    private static void showProcessDialog(Context context, String s)
    {
        if (context != null && s != null)
        {
            int i;
            try
            {
                context = Util.parseJson(s);
                i = context.getInt("action");
                context.getString("msg");
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context.printStackTrace();
                return;
            }
            if (i == 1)
            {
                return;
            }
        }
    }

    private static void showTips(Context context, String s)
    {
        int i;
        try
        {
            s = Util.parseJson(s);
            i = s.getInt("type");
            s = s.getString("msg");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return;
        }
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_82;
        }
        if (sToast != null)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        sToast = Toast.makeText(context, s, 0);
_L2:
        sToast.show();
        return;
        sToast.setView(sToast.getView());
        sToast.setText(s);
        sToast.setDuration(0);
        if (true) goto _L2; else goto _L1
_L1:
        if (i != 1)
        {
            break MISSING_BLOCK_LABEL_138;
        }
        if (sToast != null)
        {
            break MISSING_BLOCK_LABEL_109;
        }
        sToast = Toast.makeText(context, s, 1);
_L4:
        sToast.show();
        return;
        sToast.setView(sToast.getView());
        sToast.setText(s);
        sToast.setDuration(1);
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void callJs(String s, String s1)
    {
        s = (new StringBuilder()).append("javascript:").append(s).append("(").append(s1).append(")").toString();
        mWebView.loadUrl(s);
    }

    public void onBackPressed()
    {
        super.onBackPressed();
    }

    protected void onConsoleMessage(String s)
    {
        f.b("PKDialog", "--onConsoleMessage--");
        try
        {
            jsBridge.a(mWebView, s);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setSoftInputMode(16);
        getWindow().setSoftInputMode(1);
        createViews();
        initViews();
    }

    public void onKeyboardHidden()
    {
        mWebView.getLayoutParams().height = mWebviewHeight;
        f.e(TAG, "keyboard hide");
    }

    public void onKeyboardShown(int i)
    {
        if (mWeakContext != null && mWeakContext.get() != null)
        {
            if (i < mWebviewHeight && 2 == ((Context)mWeakContext.get()).getResources().getConfiguration().orientation)
            {
                mWebView.getLayoutParams().height = i;
            } else
            {
                mWebView.getLayoutParams().height = mWebviewHeight;
            }
        }
        f.e(TAG, "keyboard show");
    }








}
