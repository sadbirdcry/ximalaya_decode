// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.b;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.tencent.open.a.f;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.OpenConfig;
import com.tencent.open.utils.ThreadManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Executor;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tencent.open.b:
//            e, b, f, c

public class g
{

    protected static g a;
    protected Random b;
    protected List c;
    protected List d;
    protected HandlerThread e;
    protected Handler f;
    protected Executor g;
    protected Executor h;

    private g()
    {
        e = null;
        b = new Random();
        d = Collections.synchronizedList(new ArrayList());
        c = Collections.synchronizedList(new ArrayList());
        g = ThreadManager.newSerialExecutor();
        h = ThreadManager.newSerialExecutor();
        if (e == null)
        {
            e = new HandlerThread("opensdk.report.handlerthread", 10);
            e.start();
        }
        if (e.isAlive() && e.getLooper() != null)
        {
            f = new _cls1(e.getLooper());
        }
    }

    public static g a()
    {
        com/tencent/open/b/g;
        JVM INSTR monitorenter ;
        g g1;
        if (a == null)
        {
            a = new g();
        }
        g1 = a;
        com/tencent/open/b/g;
        JVM INSTR monitorexit ;
        return g1;
        Exception exception;
        exception;
        throw exception;
    }

    protected int a(int i)
    {
        if (i == 0)
        {
            int j = OpenConfig.getInstance(Global.getContext(), null).getInt("Common_CGIReportFrequencySuccess");
            i = j;
            if (j == 0)
            {
                i = 10;
            }
        } else
        {
            int k = OpenConfig.getInstance(Global.getContext(), null).getInt("Common_CGIReportFrequencyFailed");
            i = k;
            if (k == 0)
            {
                return 100;
            }
        }
        return i;
    }

    public void a(Bundle bundle, String s, boolean flag)
    {
        if (bundle != null)
        {
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->reportVia, bundle: ").append(bundle.toString()).toString());
            if (a("report_via", s) || flag)
            {
                g.execute(new _cls2(bundle, flag));
                return;
            }
        }
    }

    public void a(String s, long l, long l1, long l2, 
            int i)
    {
        a(s, l, l1, l2, i, "", false);
    }

    public void a(String s, long l, long l1, long l2, 
            int i, String s1, boolean flag)
    {
        com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->reportCgi, command: ").append(s).append(" | startTime: ").append(l).append(" | reqSize:").append(l1).append(" | rspSize: ").append(l2).append(" | responseCode: ").append(i).append(" | detail: ").append(s1).toString());
        if (!a("report_cgi", (new StringBuilder()).append("").append(i).toString()) && !flag)
        {
            return;
        } else
        {
            h.execute(new _cls3(l, s, s1, i, l1, l2, flag));
            return;
        }
    }

    public void a(String s, String s1, Bundle bundle, boolean flag)
    {
        ThreadManager.executeOnSubThread(new _cls6(bundle, s, flag, s1));
    }

    protected boolean a(String s, int i)
    {
        int j;
        boolean flag;
        j = 5;
        flag = false;
        if (!s.equals("report_cgi")) goto _L2; else goto _L1
_L1:
        int k = OpenConfig.getInstance(Global.getContext(), null).getInt("Common_CGIReportMaxcount");
        if (k != 0)
        {
            j = k;
        }
_L4:
        com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->availableCount, report: ").append(s).append(" | dataSize: ").append(i).append(" | maxcount: ").append(j).toString());
        if (i >= j)
        {
            flag = true;
        }
        return flag;
_L2:
        if (s.equals("report_via"))
        {
            int l = OpenConfig.getInstance(Global.getContext(), null).getInt("Agent_ReportBatchCount");
            if (l != 0)
            {
                j = l;
            }
        } else
        {
            j = 0;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    protected boolean a(String s, String s1)
    {
        boolean flag = true;
        boolean flag1 = false;
        com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->availableFrequency, report: ").append(s).append(" | ext: ").append(s1).toString());
        if (TextUtils.isEmpty(s))
        {
            return false;
        }
        int i;
        if (s.equals("report_cgi"))
        {
            try
            {
                i = Integer.parseInt(s1);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return false;
            }
            i = a(i);
            if (b.nextInt(100) >= i)
            {
                flag = false;
            }
        } else
        if (s.equals("report_via"))
        {
            i = com.tencent.open.b.e.a(s1);
            if (b.nextInt(100) < i)
            {
                flag = true;
            } else
            {
                flag = flag1;
            }
        } else
        {
            i = 100;
            flag = flag1;
        }
        com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->availableFrequency, result: ").append(flag).append(" | frequency: ").append(i).toString());
        return flag;
    }

    protected void b()
    {
        h.execute(new _cls4());
    }

    protected Bundle c()
    {
        Object obj;
        if (c.size() == 0)
        {
            return null;
        }
        obj = (b)c.get(0);
        if (obj == null)
        {
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->prepareCgiData, the 0th cgireportitem is null.");
            return null;
        }
        Object obj1 = (String)((b) (obj)).a.get("appid");
        obj = com.tencent.open.b.f.a().a("report_cgi");
        if (obj != null)
        {
            c.addAll(((java.util.Collection) (obj)));
        }
        com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->prepareCgiData, mCgiList size: ").append(c.size()).toString());
        if (c.size() == 0)
        {
            return null;
        }
        obj = new Bundle();
        int i;
        try
        {
            ((Bundle) (obj)).putString("appid", ((String) (obj1)));
            ((Bundle) (obj)).putString("releaseversion", "OpenSdk_2.9.1.lite");
            ((Bundle) (obj)).putString("device", Build.DEVICE);
            ((Bundle) (obj)).putString("qua", "V1_AND_OpenSDK_2.9.1.lite_1077_RDM_B");
            ((Bundle) (obj)).putString("key", "apn,frequency,commandid,resultcode,tmcost,reqsize,rspsize,detail,touin,deviceinfo");
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->prepareCgiData, exception.", ((Throwable) (obj)));
            return null;
        }
        i = 0;
        if (i >= c.size())
        {
            break; /* Loop/switch isn't completed */
        }
        obj1 = (b)c.get(i);
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_1").toString(), (String)((b) (obj1)).a.get("apn"));
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_2").toString(), (String)((b) (obj1)).a.get("frequency"));
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_3").toString(), (String)((b) (obj1)).a.get("commandid"));
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_4").toString(), (String)((b) (obj1)).a.get("resultCode"));
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_5").toString(), (String)((b) (obj1)).a.get("timeCost"));
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_6").toString(), (String)((b) (obj1)).a.get("reqSize"));
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_7").toString(), (String)((b) (obj1)).a.get("rspSize"));
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_8").toString(), (String)((b) (obj1)).a.get("detail"));
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_9").toString(), (String)((b) (obj1)).a.get("uin"));
        obj1 = (new StringBuilder()).append(com.tencent.open.b.c.e(Global.getContext())).append("&").append((String)((b) (obj1)).a.get("deviceInfo")).toString();
        ((Bundle) (obj)).putString((new StringBuilder()).append(i).append("_10").toString(), ((String) (obj1)));
        i++;
        if (true) goto _L2; else goto _L1
_L2:
        break MISSING_BLOCK_LABEL_184;
_L1:
        com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->prepareCgiData, end. params: ").append(((Bundle) (obj)).toString()).toString());
        return ((Bundle) (obj));
    }

    protected Bundle d()
    {
        JSONArray jsonarray;
        Iterator iterator;
        List list = com.tencent.open.b.f.a().a("report_via");
        if (list != null)
        {
            d.addAll(list);
        }
        com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->prepareViaData, mViaList size: ").append(d.size()).toString());
        if (d.size() == 0)
        {
            return null;
        }
        jsonarray = new JSONArray();
        iterator = d.iterator();
_L5:
        if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        JSONObject jsonobject1;
        b b1;
        Iterator iterator1;
        Serializable serializable = (Serializable)iterator.next();
        jsonobject1 = new JSONObject();
        b1 = (b)serializable;
        iterator1 = b1.a.keySet().iterator();
_L4:
        String s2;
        if (!iterator1.hasNext())
        {
            continue; /* Loop/switch isn't completed */
        }
        s2 = (String)iterator1.next();
        String s1 = (String)b1.a.get(s2);
        String s = s1;
        if (s1 == null)
        {
            s = "";
        }
        try
        {
            jsonobject1.put(s2, s);
        }
        catch (JSONException jsonexception)
        {
            com.tencent.open.a.f.a("openSDK_LOG.ReportManager", "-->prepareViaData, put bundle to json array exception", jsonexception);
        }
        if (true) goto _L4; else goto _L3
_L3:
        jsonarray.put(jsonobject1);
          goto _L5
_L2:
        com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->prepareViaData, JSONArray array: ").append(jsonarray.toString()).toString());
        Bundle bundle = new Bundle();
        JSONObject jsonobject = new JSONObject();
        try
        {
            jsonobject.put("data", jsonarray);
        }
        catch (JSONException jsonexception1)
        {
            com.tencent.open.a.f.a("openSDK_LOG.ReportManager", "-->prepareViaData, put bundle to json array exception", jsonexception1);
            return null;
        }
        bundle.putString("data", jsonobject.toString());
        return bundle;
    }

    protected void e()
    {
        g.execute(new _cls5());
    }

    private class _cls1 extends Handler
    {

        final g a;

        public void handleMessage(Message message)
        {
            message.what;
            JVM INSTR tableswitch 1000 1001: default 28
        //                       1000 34
        //                       1001 44;
               goto _L1 _L2 _L3
_L1:
            super.handleMessage(message);
            return;
_L2:
            a.b();
            continue; /* Loop/switch isn't completed */
_L3:
            a.e();
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls1(Looper looper)
        {
            a = g.this;
            super(looper);
        }
    }


    private class _cls2
        implements Runnable
    {

        final Bundle a;
        final boolean b;
        final g c;

        public void run()
        {
            int j;
            int k;
            Object obj = new Bundle();
            ((Bundle) (obj)).putString("uin", "1000");
            ((Bundle) (obj)).putString("imei", com.tencent.open.b.c.b(Global.getContext()));
            ((Bundle) (obj)).putString("imsi", com.tencent.open.b.c.c(Global.getContext()));
            ((Bundle) (obj)).putString("android_id", com.tencent.open.b.c.d(Global.getContext()));
            ((Bundle) (obj)).putString("mac", com.tencent.open.b.c.a());
            ((Bundle) (obj)).putString("platform", "1");
            ((Bundle) (obj)).putString("os_ver", android.os.Build.VERSION.RELEASE);
            ((Bundle) (obj)).putString("position", Util.getLocation(Global.getContext()));
            ((Bundle) (obj)).putString("network", com.tencent.open.b.a.a(Global.getContext()));
            ((Bundle) (obj)).putString("language", com.tencent.open.b.c.b());
            ((Bundle) (obj)).putString("resolution", com.tencent.open.b.c.a(Global.getContext()));
            ((Bundle) (obj)).putString("apn", com.tencent.open.b.a.b(Global.getContext()));
            ((Bundle) (obj)).putString("model_name", Build.MODEL);
            ((Bundle) (obj)).putString("timezone", TimeZone.getDefault().getID());
            ((Bundle) (obj)).putString("sdk_ver", "2.9.1.lite");
            ((Bundle) (obj)).putString("qz_ver", Util.getAppVersionName(Global.getContext(), "com.qzone"));
            ((Bundle) (obj)).putString("qq_ver", Util.getVersionName(Global.getContext(), "com.tencent.mobileqq"));
            ((Bundle) (obj)).putString("qua", Util.getQUA3(Global.getContext(), Global.getPackageName()));
            ((Bundle) (obj)).putString("packagename", Global.getPackageName());
            ((Bundle) (obj)).putString("app_ver", Util.getAppVersionName(Global.getContext(), Global.getPackageName()));
            if (a != null)
            {
                ((Bundle) (obj)).putAll(a);
            }
            obj = new b(((Bundle) (obj)));
            c.d.add(obj);
            k = c.d.size();
            j = OpenConfig.getInstance(Global.getContext(), null).getInt("Agent_ReportTimeInterval");
            int i;
            i = j;
            if (j == 0)
            {
                i = 10000;
            }
            if (c.a("report_via", k) || b)
            {
                c.e();
                c.f.removeMessages(1001);
                return;
            }
            try
            {
                if (!c.f.hasMessages(1001))
                {
                    Message message = Message.obtain();
                    message.what = 1001;
                    c.f.sendMessageDelayed(message, i);
                    return;
                }
            }
            catch (Exception exception)
            {
                com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "--> reporVia, exception in sub thread.", exception);
            }
            return;
        }

        _cls2(Bundle bundle, boolean flag)
        {
            c = g.this;
            a = bundle;
            b = flag;
            super();
        }
    }


    private class _cls3
        implements Runnable
    {

        final long a;
        final String b;
        final String c;
        final int d;
        final long e;
        final long f;
        final boolean g;
        final g h;

        public void run()
        {
            int j = 1;
            Object obj;
            StringBuilder stringbuilder;
            Object obj1;
            long l;
            long l1;
            l = SystemClock.elapsedRealtime();
            l1 = a;
            obj = new Bundle();
            obj1 = com.tencent.open.b.a.a(Global.getContext());
            ((Bundle) (obj)).putString("apn", ((String) (obj1)));
            ((Bundle) (obj)).putString("appid", "1000067");
            ((Bundle) (obj)).putString("commandid", b);
            ((Bundle) (obj)).putString("detail", c);
            stringbuilder = new StringBuilder();
            stringbuilder.append("network=").append(((String) (obj1))).append('&');
            obj1 = stringbuilder.append("sdcard=");
            int i;
            int k;
            if (Environment.getExternalStorageState().equals("mounted"))
            {
                i = 1;
            } else
            {
                i = 0;
            }
            try
            {
                ((StringBuilder) (obj1)).append(i).append('&');
                stringbuilder.append("wifi=").append(com.tencent.open.b.a.e(Global.getContext()));
                ((Bundle) (obj)).putString("deviceInfo", stringbuilder.toString());
                i = 100 / h.a(d);
            }
            catch (Exception exception)
            {
                com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "--> reportCGI, exception in sub thread.", exception);
                return;
            }
            if (i > 0) goto _L2; else goto _L1
_L1:
            i = j;
_L4:
            ((Bundle) (obj)).putString("frequency", (new StringBuilder()).append(i).append("").toString());
            ((Bundle) (obj)).putString("reqSize", (new StringBuilder()).append(e).append("").toString());
            ((Bundle) (obj)).putString("resultCode", (new StringBuilder()).append(d).append("").toString());
            ((Bundle) (obj)).putString("rspSize", (new StringBuilder()).append(f).append("").toString());
            ((Bundle) (obj)).putString("timeCost", (new StringBuilder()).append(l - l1).append("").toString());
            ((Bundle) (obj)).putString("uin", "1000");
            obj = new b(((Bundle) (obj)));
            h.c.add(obj);
            k = h.c.size();
            j = OpenConfig.getInstance(Global.getContext(), null).getInt("Agent_ReportTimeInterval");
            i = j;
            if (j == 0)
            {
                i = 10000;
            }
            if (h.a("report_cgi", k) || g)
            {
                h.b();
                h.f.removeMessages(1000);
                return;
            }
            if (!h.f.hasMessages(1000))
            {
                obj = Message.obtain();
                obj.what = 1000;
                h.f.sendMessageDelayed(((Message) (obj)), i);
                return;
            }
            return;
_L2:
            if (i > 100)
            {
                i = 100;
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        _cls3(long l, String s, String s1, int i, long l1, long l2, boolean flag)
        {
            h = g.this;
            a = l;
            b = s;
            c = s1;
            d = i;
            e = l1;
            f = l2;
            g = flag;
            super();
        }
    }


    private class _cls6
        implements Runnable
    {

        final Bundle a;
        final String b;
        final boolean c;
        final String d;
        final g e;

        public void run()
        {
            boolean flag = false;
            if (a == null)
            {
                com.tencent.open.a.f.e("openSDK_LOG.ReportManager", "-->httpRequest, params is null!");
                return;
            }
            int j = com.tencent.open.b.e.a();
            if (j == 0)
            {
                j = 3;
            }
            Object obj;
            HttpClient httpclient;
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->httpRequest, retryCount: ").append(j).toString());
            httpclient = HttpUtils.getHttpClient(Global.getContext(), null, b);
            obj = HttpUtils.encodeUrl(a);
            if (c)
            {
                obj = URLEncoder.encode(((String) (obj)));
            }
            if (!d.toUpperCase().equals("GET")) goto _L2; else goto _L1
_L1:
            StringBuffer stringbuffer = new StringBuffer(b);
            stringbuffer.append(((String) (obj)));
            obj = new HttpGet(stringbuffer.toString());
_L6:
            ((HttpUriRequest) (obj)).addHeader("Accept-Encoding", "gzip");
            ((HttpUriRequest) (obj)).addHeader("Content-Type", "application/x-www-form-urlencoded");
            int k = 0;
_L13:
            int l = k + 1;
            int i;
            i = httpclient.execute(((HttpUriRequest) (obj))).getStatusLine().getStatusCode();
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->httpRequest, statusCode: ").append(i).toString());
            if (i == 200) goto _L4; else goto _L3
_L3:
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->ReportCenter httpRequest : HttpStatuscode != 200");
_L7:
            if (flag)
            {
                try
                {
                    com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->ReportCenter httpRequest Thread request success");
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->httpRequest, exception in serial executor.");
                }
                return;
            }
              goto _L5
_L2:
            HttpPost httppost;
            if (!d.toUpperCase().equals("POST"))
            {
                break MISSING_BLOCK_LABEL_294;
            }
            httppost = new HttpPost(b);
            httppost.setEntity(new ByteArrayEntity(Util.getBytesUTF8(((String) (obj)))));
            obj = httppost;
              goto _L6
            com.tencent.open.a.f.e("openSDK_LOG.ReportManager", "-->httpRequest unkonw request method return.");
            return;
_L4:
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->ReportCenter httpRequest Thread success");
            flag = true;
              goto _L7
            Object obj1;
            obj1;
_L11:
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->ReportCenter httpRequest ConnectTimeoutException");
            i = ((flag) ? 1 : 0);
              goto _L8
_L10:
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->ReportCenter httpRequest SocketTimeoutException");
            i = ((flag) ? 1 : 0);
              goto _L8
_L9:
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->ReportCenter httpRequest Exception");
              goto _L7
_L5:
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->ReportCenter httpRequest Thread request failed");
            return;
            Exception exception;
            exception;
            flag = true;
              goto _L9
            obj1;
            flag = true;
              goto _L10
            obj1;
            flag = true;
              goto _L11
_L8:
            flag = i;
            k = l;
            if (l < j) goto _L13; else goto _L12
_L12:
            flag = i;
              goto _L7
            obj1;
              goto _L10
            exception;
              goto _L9
        }

        _cls6(Bundle bundle, String s, boolean flag, String s1)
        {
            e = g.this;
            a = bundle;
            b = s;
            c = flag;
            d = s1;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final g a;

        public void run()
        {
            int i;
            int j;
            boolean flag;
            int k;
            flag = false;
            Bundle bundle;
            HttpClient httpclient;
            HttpPost httppost;
            int l;
            try
            {
                bundle = a.c();
            }
            catch (Exception exception)
            {
                com.tencent.open.a.f.a("openSDK_LOG.ReportManager", "-->doReportCgi, doupload exception out.", exception);
                return;
            }
            if (bundle == null)
            {
                return;
            }
            i = OpenConfig.getInstance(Global.getContext(), null).getInt("Common_HttpRetryCount");
            if (i == 0)
            {
                i = 3;
            }
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->doReportCgi, retryCount: ").append(i).toString());
            j = 0;
_L4:
            k = j + 1;
            httpclient = HttpUtils.getHttpClient(Global.getContext(), null, "http://wspeed.qq.com/w.cgi");
            httppost = new HttpPost("http://wspeed.qq.com/w.cgi");
            httppost.addHeader("Accept-Encoding", "gzip");
            httppost.setHeader("Content-Type", "application/x-www-form-urlencoded");
            httppost.setEntity(new ByteArrayEntity(Util.getBytesUTF8(HttpUtils.encodeUrl(bundle))));
            l = httpclient.execute(httppost).getStatusLine().getStatusCode();
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->doReportCgi, statusCode: ").append(l).toString());
            j = ((flag) ? 1 : 0);
            if (l != 200)
            {
                break MISSING_BLOCK_LABEL_193;
            }
            com.tencent.open.b.f.a().b("report_cgi");
            j = 1;
_L2:
            if (j != 0)
            {
                break MISSING_BLOCK_LABEL_213;
            }
            com.tencent.open.b.f.a().a("report_cgi", a.c);
            a.c.clear();
            return;
            Object obj;
            obj;
            com.tencent.open.a.f.a("openSDK_LOG.ReportManager", "-->doReportCgi, doupload exception", ((Throwable) (obj)));
            break MISSING_BLOCK_LABEL_279;
            obj;
            com.tencent.open.a.f.a("openSDK_LOG.ReportManager", "-->doReportCgi, doupload exception", ((Throwable) (obj)));
            break MISSING_BLOCK_LABEL_279;
            Exception exception1;
            exception1;
            com.tencent.open.a.f.a("openSDK_LOG.ReportManager", "-->doReportCgi, doupload exception", exception1);
            j = ((flag) ? 1 : 0);
            break; /* Loop/switch isn't completed */
            for (j = k; k < i; j = k)
            {
                continue; /* Loop/switch isn't completed */
            }

            j = ((flag) ? 1 : 0);
            if (true) goto _L2; else goto _L1
_L1:
            if (true) goto _L4; else goto _L3
_L3:
        }

        _cls4()
        {
            a = g.this;
            super();
        }
    }


    private class _cls5
        implements Runnable
    {

        final g a;

        public void run()
        {
            int i;
            int j;
            int k;
            int l;
            int i1;
            int j1;
            int k1;
            int l1;
            long l2;
            long l3;
            long l4;
            long l5;
            long l6;
            long l7;
            boolean flag;
            boolean flag2;
            boolean flag3;
            boolean flag4;
            boolean flag5;
            boolean flag6;
            boolean flag7;
            Bundle bundle;
            com.tencent.open.utils.Util.Statistic statistic;
            JSONObject jsonobject;
            boolean flag1;
            try
            {
                bundle = a.d();
            }
            catch (Exception exception)
            {
                com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "-->doReportVia, exception in serial executor.", exception);
                return;
            }
            if (bundle == null)
            {
                return;
            }
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->doReportVia, params: ").append(bundle.toString()).toString());
            l1 = com.tencent.open.b.e.a();
            l = 0;
            flag1 = false;
            l5 = SystemClock.elapsedRealtime();
            l7 = 0L;
            l6 = 0L;
            k = 0;
_L5:
            j = l + 1;
            flag2 = flag1;
            k1 = j;
            flag3 = flag1;
            l = j;
            flag4 = flag1;
            flag5 = flag1;
            i1 = j;
            flag6 = flag1;
            j1 = j;
            flag7 = flag1;
            statistic = HttpUtils.openUrl2(Global.getContext(), "http://appsupport.qq.com/cgi-bin/appstage/mstats_batch_report", "POST", bundle);
            flag2 = flag1;
            k1 = j;
            flag3 = flag1;
            l = j;
            flag4 = flag1;
            flag5 = flag1;
            i1 = j;
            flag6 = flag1;
            j1 = j;
            flag7 = flag1;
            jsonobject = Util.parseJson(statistic.response);
            flag2 = flag1;
            k1 = j;
            flag3 = flag1;
            l = j;
            flag4 = flag1;
            flag5 = flag1;
            i1 = j;
            flag7 = flag1;
            i = jsonobject.getInt("ret");
_L8:
            if (i == 0) goto _L2; else goto _L1
_L1:
            flag = flag1;
            i = j;
            flag2 = flag1;
            k1 = j;
            flag3 = flag1;
            l = j;
            flag4 = flag1;
            flag5 = flag1;
            i1 = j;
            flag6 = flag1;
            j1 = j;
            flag7 = flag1;
            if (TextUtils.isEmpty(statistic.response)) goto _L3; else goto _L2
_L3:
            flag2 = flag;
            k1 = i;
            flag3 = flag;
            l = i;
            flag4 = flag;
            flag5 = flag;
            i1 = i;
            flag6 = flag;
            j1 = i;
            flag7 = flag;
            l2 = statistic.reqSize;
            flag2 = flag;
            k1 = i;
            flag3 = flag;
            l = i;
            flag5 = flag;
            i1 = i;
            flag6 = flag;
            j1 = i;
            flag7 = flag;
            l7 = statistic.rspSize;
            j = i;
            l4 = l5;
            l3 = l2;
            l2 = l7;
            i = k;
_L9:
            k = i;
            l6 = l2;
            l7 = l3;
            l5 = l4;
            flag1 = flag;
            l = j;
            if (j < l1) goto _L5; else goto _L4
_L4:
            a.a("mapp_apptrace_sdk", l4, l3, l2, i, null, false);
            if (!flag) goto _L7; else goto _L6
_L6:
            com.tencent.open.b.f.a().b("report_via");
_L10:
            a.d.clear();
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", (new StringBuilder()).append("-->doReportVia, uploadSuccess: ").append(flag).toString());
            return;
            JSONException jsonexception;
            jsonexception;
            i = -4;
              goto _L8
            Object obj1;
            obj1;
            l4 = SystemClock.elapsedRealtime();
            j = k1;
            i = -7;
            l2 = 0L;
            l3 = 0L;
            flag = flag2;
              goto _L9
            obj1;
            l4 = SystemClock.elapsedRealtime();
            l3 = 0L;
            l2 = 0L;
            i = -8;
            flag = flag3;
            j = l;
              goto _L9
            Object obj;
            obj;
            a.d.clear();
            com.tencent.open.a.f.b("openSDK_LOG.ReportManager", "doReportVia, NetworkUnavailableException.");
            return;
            obj;
            flag = flag4;
            l2 = l7;
_L12:
            i = Integer.parseInt(((com.tencent.open.utils.HttpUtils.HttpStatusException) (obj)).getMessage().replace("http status code error:", ""));
            k = i;
_L11:
            l4 = l5;
            i = k;
            l3 = l2;
            l2 = l6;
              goto _L4
            obj1;
            l3 = 0L;
            l2 = 0L;
            i = HttpUtils.getErrorCodeFromException(((IOException) (obj1)));
            l4 = l5;
            flag = flag5;
            j = i1;
              goto _L9
_L7:
            com.tencent.open.b.f.a().a("report_via", a.d);
              goto _L10
            obj;
              goto _L11
            obj;
              goto _L12
_L2:
            flag = true;
            i = l1;
              goto _L3
            obj1;
            l3 = 0L;
            l2 = 0L;
            i = -4;
            l4 = l5;
            flag = flag6;
            j = j1;
              goto _L9
            obj1;
            l3 = 0L;
            l2 = 0L;
            i = -6;
            j = l1;
            l4 = l5;
            flag = flag7;
              goto _L9
        }

        _cls5()
        {
            a = g.this;
            super();
        }
    }

}
