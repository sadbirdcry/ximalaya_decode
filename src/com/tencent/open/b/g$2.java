// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.b;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.tencent.open.a.f;
import com.tencent.open.utils.Global;
import com.tencent.open.utils.OpenConfig;
import com.tencent.open.utils.Util;
import java.util.List;
import java.util.TimeZone;

// Referenced classes of package com.tencent.open.b:
//            c, a, b, g

class 
    implements Runnable
{

    final Bundle a;
    final boolean b;
    final g c;

    public void run()
    {
        int j;
        int k;
        Object obj = new Bundle();
        ((Bundle) (obj)).putString("uin", "1000");
        ((Bundle) (obj)).putString("imei", com.tencent.open.b.c.b(Global.getContext()));
        ((Bundle) (obj)).putString("imsi", com.tencent.open.b.c.c(Global.getContext()));
        ((Bundle) (obj)).putString("android_id", com.tencent.open.b.c.d(Global.getContext()));
        ((Bundle) (obj)).putString("mac", com.tencent.open.b.c.a());
        ((Bundle) (obj)).putString("platform", "1");
        ((Bundle) (obj)).putString("os_ver", android.os.d.VERSION.RELEASE);
        ((Bundle) (obj)).putString("position", Util.getLocation(Global.getContext()));
        ((Bundle) (obj)).putString("network", com.tencent.open.b.a.a(Global.getContext()));
        ((Bundle) (obj)).putString("language", com.tencent.open.b.c.b());
        ((Bundle) (obj)).putString("resolution", com.tencent.open.b.c.a(Global.getContext()));
        ((Bundle) (obj)).putString("apn", com.tencent.open.b.a.b(Global.getContext()));
        ((Bundle) (obj)).putString("model_name", Build.MODEL);
        ((Bundle) (obj)).putString("timezone", TimeZone.getDefault().getID());
        ((Bundle) (obj)).putString("sdk_ver", "2.9.1.lite");
        ((Bundle) (obj)).putString("qz_ver", Util.getAppVersionName(Global.getContext(), "com.qzone"));
        ((Bundle) (obj)).putString("qq_ver", Util.getVersionName(Global.getContext(), "com.tencent.mobileqq"));
        ((Bundle) (obj)).putString("qua", Util.getQUA3(Global.getContext(), Global.getPackageName()));
        ((Bundle) (obj)).putString("packagename", Global.getPackageName());
        ((Bundle) (obj)).putString("app_ver", Util.getAppVersionName(Global.getContext(), Global.getPackageName()));
        if (a != null)
        {
            ((Bundle) (obj)).putAll(a);
        }
        obj = new b(((Bundle) (obj)));
        c.d.add(obj);
        k = c.d.size();
        j = OpenConfig.getInstance(Global.getContext(), null).getInt("Agent_ReportTimeInterval");
        int i;
        i = j;
        if (j == 0)
        {
            i = 10000;
        }
        if (c.a("report_via", k) || b)
        {
            c.e();
            c.f.removeMessages(1001);
            return;
        }
        try
        {
            if (!c.f.hasMessages(1001))
            {
                Message message = Message.obtain();
                message.what = 1001;
                c.f.sendMessageDelayed(message, i);
                return;
            }
        }
        catch (Exception exception)
        {
            f.b("openSDK_LOG.ReportManager", "--> reporVia, exception in sub thread.", exception);
        }
        return;
    }

    OpenConfig(g g1, Bundle bundle, boolean flag)
    {
        c = g1;
        a = bundle;
        b = flag;
        super();
    }
}
