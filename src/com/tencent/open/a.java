// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open;

import android.net.Uri;
import android.webkit.WebView;
import com.tencent.open.a.f;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class com.tencent.open.a
{
    public static class a
    {

        protected WeakReference a;
        protected long b;
        protected String c;

        public void a()
        {
            WebView webview = (WebView)a.get();
            if (webview == null)
            {
                return;
            } else
            {
                webview.loadUrl((new StringBuilder()).append("javascript:window.JsBridge&&JsBridge.callback(").append(b).append(",{'r':1,'result':'no such method'})").toString());
                return;
            }
        }

        public void a(Object obj)
        {
            String s;
            WebView webview;
            webview = (WebView)a.get();
            if (webview == null)
            {
                return;
            }
            s = "'undefined'";
            if (!(obj instanceof String)) goto _L2; else goto _L1
_L1:
            obj = ((String)obj).replace("\\", "\\\\").replace("'", "\\'");
            s = (new StringBuilder()).append("'").append(obj).append("'").toString();
_L4:
            webview.loadUrl((new StringBuilder()).append("javascript:window.JsBridge&&JsBridge.callback(").append(b).append(",{'r':0,'result':").append(s).append("});").toString());
            return;
_L2:
            if ((obj instanceof Number) || (obj instanceof Long) || (obj instanceof Integer) || (obj instanceof Double) || (obj instanceof Float))
            {
                s = obj.toString();
            } else
            if (obj instanceof Boolean)
            {
                s = obj.toString();
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        public void a(String s)
        {
            WebView webview = (WebView)a.get();
            if (webview != null)
            {
                webview.loadUrl((new StringBuilder()).append("javascript:").append(s).toString());
            }
        }

        public a(WebView webview, long l, String s)
        {
            a = new WeakReference(webview);
            b = l;
            c = s;
        }
    }

    public static class b
    {

        public void call(String s, List list, a a1)
        {
            Object obj;
            Method amethod[];
            int i;
            int j;
            amethod = getClass().getDeclaredMethods();
            obj = null;
            j = amethod.length;
            i = 0;
_L12:
            Method method;
            method = obj;
            if (i < j)
            {
                method = amethod[i];
                if (!method.getName().equals(s) || method.getParameterTypes().length != list.size())
                {
                    break MISSING_BLOCK_LABEL_572;
                }
            }
            if (method == null)
            {
                break MISSING_BLOCK_LABEL_559;
            }
            list.size();
            JVM INSTR tableswitch 0 5: default 568
        //                       0 258
        //                       1 272
        //                       2 296
        //                       3 330
        //                       4 374
        //                       5 428;
               goto _L1 _L2 _L3 _L4 _L5 _L6 _L7
_L7:
            break MISSING_BLOCK_LABEL_428;
_L1:
            s = ((String) (method.invoke(this, new Object[] {
                list.get(0), list.get(1), list.get(2), list.get(3), list.get(4), list.get(5)
            })));
_L9:
            list = method.getReturnType();
            f.b("openSDK_LOG.JsBridge", (new StringBuilder()).append("-->call, result: ").append(s).append(" | ReturnType: ").append(list.getName()).toString());
              goto _L8
_L2:
            s = ((String) (method.invoke(this, new Object[0])));
              goto _L9
_L3:
            s = ((String) (method.invoke(this, new Object[] {
                list.get(0)
            })));
              goto _L9
_L4:
            s = ((String) (method.invoke(this, new Object[] {
                list.get(0), list.get(1)
            })));
              goto _L9
_L5:
            s = ((String) (method.invoke(this, new Object[] {
                list.get(0), list.get(1), list.get(2)
            })));
              goto _L9
_L6:
            s = ((String) (method.invoke(this, new Object[] {
                list.get(0), list.get(1), list.get(2), list.get(3)
            })));
              goto _L9
            s = ((String) (method.invoke(this, new Object[] {
                list.get(0), list.get(1), list.get(2), list.get(3), list.get(4)
            })));
              goto _L9
_L8:
            if ("void".equals(list.getName()) || list == java/lang/Void)
            {
                if (a1 == null)
                {
                    break MISSING_BLOCK_LABEL_571;
                }
                try
                {
                    a1.a(null);
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    f.b("openSDK_LOG.JsBridge", (new StringBuilder()).append("-->handler call mehtod ex. targetMethod: ").append(method).toString(), s);
                }
                if (a1 != null)
                {
                    a1.a();
                    return;
                }
                break MISSING_BLOCK_LABEL_571;
            }
            if (a1 == null)
            {
                break MISSING_BLOCK_LABEL_571;
            }
            if (!customCallback())
            {
                break MISSING_BLOCK_LABEL_571;
            }
            if (s == null)
            {
                break MISSING_BLOCK_LABEL_554;
            }
            s = s.toString();
_L10:
            a1.a(s);
            return;
            s = null;
              goto _L10
            if (a1 != null)
            {
                a1.a();
                return;
            }
            return;
            i++;
            if (true) goto _L12; else goto _L11
_L11:
        }

        public boolean customCallback()
        {
            return false;
        }

        public b()
        {
        }
    }


    protected HashMap a;

    public com.tencent.open.a()
    {
        a = new HashMap();
    }

    public void a(b b1, String s)
    {
        a.put(s, b1);
    }

    public void a(String s, String s1, List list, a a1)
    {
        f.b("openSDK_LOG.JsBridge", (new StringBuilder()).append("getResult---objName = ").append(s).append(" methodName = ").append(s1).toString());
        int j = list.size();
        int i = 0;
        while (i < j) 
        {
            try
            {
                list.set(i, URLDecoder.decode((String)list.get(i), "UTF-8"));
            }
            catch (UnsupportedEncodingException unsupportedencodingexception)
            {
                unsupportedencodingexception.printStackTrace();
            }
            i++;
        }
        s = (b)a.get(s);
        if (s != null)
        {
            f.b("openSDK_LOG.JsBridge", "call----");
            s.call(s1, list, a1);
        } else
        {
            f.b("openSDK_LOG.JsBridge", "not call----objName NOT FIND");
            if (a1 != null)
            {
                a1.a();
                return;
            }
        }
    }

    public boolean a(WebView webview, String s)
    {
        f.b("openSDK_LOG.JsBridge", (new StringBuilder()).append("-->canHandleUrl---url = ").append(s).toString());
        break MISSING_BLOCK_LABEL_24;
        if (s != null && Uri.parse(s).getScheme().equals("jsbridge"))
        {
            Object obj = new ArrayList(Arrays.asList((new StringBuilder()).append(s).append("/#").toString().split("/")));
            if (((ArrayList) (obj)).size() >= 6)
            {
                String s1 = (String)((ArrayList) (obj)).get(2);
                String s2 = (String)((ArrayList) (obj)).get(3);
                obj = ((ArrayList) (obj)).subList(4, ((ArrayList) (obj)).size() - 1);
                s = new a(webview, 4L, s);
                webview.getUrl();
                a(s1, s2, ((List) (obj)), ((a) (s)));
                return true;
            }
        }
        return false;
    }
}
