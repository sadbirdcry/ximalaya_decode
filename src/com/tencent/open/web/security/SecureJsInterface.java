// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tencent.open.web.security;

import com.tencent.open.a.f;

// Referenced classes of package com.tencent.open.web.security:
//            JniInterface, a

public class SecureJsInterface extends com.tencent.open.a.b
{

    public static boolean isPWDEdit = false;
    private String a;

    public SecureJsInterface()
    {
    }

    public void clearAllEdit()
    {
        f.c("openSDK_LOG.SI", "-->clear all edit.");
        try
        {
            JniInterface.clearAllPWD();
            return;
        }
        catch (Exception exception)
        {
            f.e("openSDK_LOG.SI", (new StringBuilder()).append("-->clear all edit exception: ").append(exception.getMessage()).toString());
            throw new RuntimeException(exception);
        }
    }

    public void curPosFromJS(String s)
    {
        int i;
        f.b("openSDK_LOG.SI", (new StringBuilder()).append("-->curPosFromJS: ").append(s).toString());
        i = -1;
        int j = Integer.parseInt(s);
        i = j;
_L2:
        if (i < 0)
        {
            throw new RuntimeException("position is illegal.");
        }
        break; /* Loop/switch isn't completed */
        s;
        f.e("openSDK_LOG.SI", "-->curPosFromJS number format exception.");
        if (true) goto _L2; else goto _L1
_L1:
        if (a.c);
        if (a.b)
        {
            if (Boolean.valueOf(JniInterface.BackSpaceChar(a.b, i)).booleanValue())
            {
                a.b = false;
            }
            return;
        } else
        {
            a = a.a;
            JniInterface.insetTextToArray(i, a, a.length());
            f.b("openSDK_LOG.SI", (new StringBuilder()).append("mKey: ").append(a).toString());
            return;
        }
    }

    public boolean customCallback()
    {
        return true;
    }

    public String getMD5FromNative()
    {
        f.c("openSDK_LOG.SI", "-->get md5 form native");
        String s;
        try
        {
            s = JniInterface.getPWDKeyToMD5(null);
        }
        catch (Exception exception)
        {
            f.e("openSDK_LOG.SI", (new StringBuilder()).append("-->get md5 form native exception: ").append(exception.getMessage()).toString());
            throw new RuntimeException(exception);
        }
        f.b("openSDK_LOG.SI", (new StringBuilder()).append("-->getMD5FromNative, MD5= ").append(s).toString());
        return s;
    }

    public void isPasswordEdit(String s)
    {
        int i;
        f.c("openSDK_LOG.SI", (new StringBuilder()).append("-->is pswd edit, flag: ").append(s).toString());
        i = -1;
        int j = Integer.parseInt(s);
        i = j;
_L2:
        if (i != 0 && i != 1)
        {
            throw new RuntimeException("is pswd edit flag is illegal.");
        }
        break; /* Loop/switch isn't completed */
        s;
        f.e("openSDK_LOG.SI", (new StringBuilder()).append("-->is pswd edit exception: ").append(s.getMessage()).toString());
        if (true) goto _L2; else goto _L1
_L1:
        if (i == 0)
        {
            isPWDEdit = false;
        } else
        if (i == 1)
        {
            isPWDEdit = true;
            return;
        }
        return;
    }

}
