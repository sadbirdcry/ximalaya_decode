// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.content.Context;
import com.iflytek.cloud.a.d.d;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.d.a.j;

// Referenced classes of package com.iflytek.cloud:
//            InitListener, WakeuperListener

public class VoiceWakeuper extends d
{

    private static VoiceWakeuper a = null;
    private j c;

    private VoiceWakeuper(Context context, InitListener initlistener)
    {
        c = null;
        c = new j(context);
    }

    public static VoiceWakeuper createWakeuper(Context context, InitListener initlistener)
    {
        com/iflytek/cloud/VoiceWakeuper;
        JVM INSTR monitorenter ;
        if (a == null)
        {
            a = new VoiceWakeuper(context, initlistener);
        }
        context = a;
        com/iflytek/cloud/VoiceWakeuper;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public static VoiceWakeuper getWakeuper()
    {
        return a;
    }

    public void cancel()
    {
        c.cancel(false);
    }

    public boolean destroy()
    {
        boolean flag = true;
        if (c != null)
        {
            flag = c.destroy();
        }
        if (flag)
        {
            a = null;
        }
        return flag;
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public boolean isListening()
    {
        return c.f();
    }

    public boolean setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public void startListening(WakeuperListener wakeuperlistener)
    {
        c.setParameter("params", null);
        c.setParameter(b);
        c.a(wakeuperlistener);
    }

    public void stopListening()
    {
        c.e();
    }

    public int writeAudio(byte abyte0[], int i, int k)
    {
        if (c != null && c.f())
        {
            return c.a(abyte0, i, k);
        } else
        {
            com.iflytek.cloud.a.g.a.a.b("VoiceWakeup writeAudio failed, is not running");
            return 21004;
        }
    }

}
