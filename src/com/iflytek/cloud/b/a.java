// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.b;

import android.text.TextUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class a
{

    HashMap a;

    public a()
    {
        a = new HashMap();
    }

    public a(String s, String as[][])
    {
        a = new HashMap();
        a(s);
        a(as);
    }

    public static String f(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        } else
        {
            return s.replaceAll("[,\n ]", "|");
        }
    }

    public int a(String s, int i)
    {
        s = (String)a.get(s);
        int j = i;
        if (s != null)
        {
            try
            {
                j = Integer.parseInt(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return i;
            }
        }
        return j;
    }

    public void a()
    {
        a.clear();
    }

    public void a(a a1)
    {
        if (a1 != null)
        {
            a.putAll(a1.c());
        }
    }

    public void a(a a1, String s)
    {
        if (a1 == null)
        {
            return;
        } else
        {
            a(s, a1.e(s));
            return;
        }
    }

    public void a(String s)
    {
        a.clear();
        b(s);
    }

    public void a(String s, String s1)
    {
        a(s, s1, true);
    }

    public void a(String s, String s1, boolean flag)
    {
        while (TextUtils.isEmpty(s) || TextUtils.isEmpty(s1) || !flag && a.containsKey(s)) 
        {
            return;
        }
        a.put(s, s1);
    }

    public void a(String as[][])
    {
        if (as != null)
        {
            int i = 0;
            while (i < as.length) 
            {
                String as1[] = as[i];
                if (a.containsKey(as1[0]))
                {
                    String s = (String)a.get(as1[0]);
                    a.remove(as1[0]);
                    for (int j = 1; j < as1.length; j++)
                    {
                        a.put(as1[j], s);
                    }

                }
                i++;
            }
        }
    }

    public boolean a(String s, boolean flag)
    {
        boolean flag1;
        s = (String)a.get(s);
        flag1 = flag;
        if (s == null) goto _L2; else goto _L1
_L1:
        if (!s.equals("true") && !s.equals("1")) goto _L4; else goto _L3
_L3:
        flag1 = true;
_L2:
        return flag1;
_L4:
        if (s.equals("false"))
        {
            break; /* Loop/switch isn't completed */
        }
        flag1 = flag;
        if (!s.equals("0")) goto _L2; else goto _L5
_L5:
        return false;
    }

    public a b()
    {
        a a1 = new a();
        a1.a = (HashMap)a.clone();
        return a1;
    }

    public String b(String s, String s1)
    {
        s = (String)a.get(s);
        if (s == null)
        {
            return s1;
        } else
        {
            return s;
        }
    }

    public void b(String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            s = s.split(",");
            int j = s.length;
            for (int i = 0; i < j; i++)
            {
                String s2 = s[i];
                int k = s2.indexOf("=");
                if (k > 0 && k < s2.length())
                {
                    String s1 = s2.substring(0, k);
                    s2 = s2.substring(k + 1);
                    a.put(s1, s2);
                }
            }

        }
    }

    public Boolean c(String s)
    {
        boolean flag;
        if (a.remove(s) != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        return Boolean.valueOf(flag);
    }

    public HashMap c()
    {
        return a;
    }

    public Object clone()
        throws CloneNotSupportedException
    {
        return b();
    }

    public String d(String s)
    {
        return (String)a.remove(s);
    }

    public void d()
    {
        java.util.Map.Entry entry;
        for (Iterator iterator = a.entrySet().iterator(); iterator.hasNext(); entry.setValue(f((String)entry.getValue())))
        {
            entry = (java.util.Map.Entry)iterator.next();
        }

    }

    public String e(String s)
    {
        return (String)a.get(s);
    }

    public boolean g(String s)
    {
        return a.containsKey(s);
    }

    public String toString()
    {
        Object obj = new StringBuffer();
        for (Iterator iterator = a.entrySet().iterator(); iterator.hasNext(); ((StringBuffer) (obj)).append(","))
        {
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            ((StringBuffer) (obj)).append((String)entry.getKey());
            ((StringBuffer) (obj)).append("=");
            ((StringBuffer) (obj)).append((String)entry.getValue());
        }

        if (((StringBuffer) (obj)).length() > 0)
        {
            ((StringBuffer) (obj)).deleteCharAt(((StringBuffer) (obj)).length() - 1);
        }
        obj = ((StringBuffer) (obj)).toString();
        com.iflytek.cloud.a.g.a.a.c(((String) (obj)));
        return ((String) (obj));
    }
}
