// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.b;

import android.content.Context;
import android.net.ConnectivityManager;
import android.text.TextUtils;
import com.iflytek.cloud.Setting;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.Version;
import com.iflytek.cloud.a.g.a;
import com.iflytek.cloud.a.g.b;
import com.iflytek.cloud.a.g.e;
import com.iflytek.cloud.a.g.h;

// Referenced classes of package com.iflytek.cloud.b:
//            a, b

public class c
{

    private static String a = "xiaoyan";

    public static String a(Context context)
    {
        if (context != null) goto _L2; else goto _L1
_L1:
        Object obj = "null";
_L4:
        return ((String) (obj));
_L2:
        com.iflytek.cloud.b.a a1 = com.iflytek.cloud.a.g.a.a(context);
        obj = (new StringBuilder()).append(a1.e("os.imsi")).append("|").append(a1.e("os.imei")).toString();
        context = ((Context) (obj));
        if (((String) (obj)).length() < 10)
        {
            context = a1.e("net.mac");
        }
        if (TextUtils.isEmpty(context))
        {
            break; /* Loop/switch isn't completed */
        }
        obj = context;
        if (context.length() > 0) goto _L4; else goto _L3
_L3:
        return null;
    }

    public static String a(Context context, com.iflytek.cloud.a.d.a a1)
        throws SpeechError
    {
        if (context == null)
        {
            throw new SpeechError(20012);
        } else
        {
            a1 = a1.t().b();
            a(context, ((com.iflytek.cloud.b.a) (a1)));
            a1.a("timeout", "20000", false);
            a1.a("auth", "1", false);
            a1.a("msc.ver", Version.getVersion());
            a1.a("mac", com.iflytek.cloud.a.g.a.a(context).e("net.mac"), false);
            a1.a("dvc", a(context), false);
            a1.a("msc.lat", (new StringBuilder()).append("").append(com.iflytek.cloud.a.g.b.a(context).a("msc.lat")).toString(), false);
            a1.a("msc.lng", (new StringBuilder()).append("").append(com.iflytek.cloud.a.g.b.a(context).a("msc.lng")).toString(), false);
            a1.a(com.iflytek.cloud.a.g.a.b(context));
            a(((com.iflytek.cloud.b.a) (a1)));
            a1.a(b.c);
            return a1.toString();
        }
    }

    public static String a(Context context, String s, com.iflytek.cloud.a.d.a a1)
    {
        com.iflytek.cloud.b.a a2 = a1.t().b();
        a2.c("cloud_grammar");
        a(context, a2);
        a2.a("language", "zh_cn", false);
        a2.a("result_type", "json", false);
        a2.a("rse", a1.p(), false);
        a2.a("text_encoding", a1.o(), false);
        a2.a("ssm", "1", false);
        int i;
        if (TextUtils.isEmpty(s))
        {
            a2.a("subject", "iat", false);
        } else
        {
            a2.a("subject", "asr", false);
        }
        i = a1.q();
        a2.a("auf=audio/L16;rate", Integer.toString(i), false);
        if (i == 16000)
        {
            a2.a("aue", "speex-wb", false);
        } else
        {
            a2.a("aue", "speex", false);
        }
        if (a1.i())
        {
            a2.a("vad_bos", "5000", false);
            a2.a("vad_eos", "1800", false);
        } else
        {
            a2.a("vad_bos", "4000", false);
            a2.a("vad_eos", "700", false);
        }
        a2.a(b.c);
        return a2.toString();
    }

    public static void a(Context context, com.iflytek.cloud.b.a a1)
    {
        if (context == null)
        {
            a1.a("wap_proxy", "none", false);
            return;
        }
        context = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (context == null)
        {
            a1.a("wap_proxy", "none", false);
            return;
        } else
        {
            a1.a("wap_proxy", h.a(context), false);
            a1.a("net_subtype", com.iflytek.cloud.b.a.f(h.b(context)), false);
            return;
        }
    }

    private static void a(com.iflytek.cloud.b.a a1)
    {
        String s;
        byte byte0;
        if (a1 == null || Setting.c == com.iflytek.cloud.Setting.LOG_LEVEL.none)
        {
            return;
        }
        String s1 = Setting.d;
        s = s1;
        if (TextUtils.isEmpty(s1))
        {
            s = "/sdcard/msc/msc.log";
        }
        byte0 = -1;
        if (Setting.c != com.iflytek.cloud.Setting.LOG_LEVEL.detail) goto _L2; else goto _L1
_L1:
        byte0 = 31;
_L4:
        e.b(s);
        a1.a("log", s);
        a1.a("lvl", (new StringBuilder()).append("").append(byte0).toString());
        a1.a("output", "1", false);
        return;
_L2:
        if (Setting.c == com.iflytek.cloud.Setting.LOG_LEVEL.normal)
        {
            byte0 = 15;
        } else
        if (Setting.c == com.iflytek.cloud.Setting.LOG_LEVEL.low)
        {
            byte0 = 7;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static boolean a(String s)
    {
        while (TextUtils.isEmpty(s) || !s.contains("sms") && !s.contains("iat")) 
        {
            return false;
        }
        return true;
    }

    public static String b(Context context, com.iflytek.cloud.a.d.a a1)
    {
        com.iflytek.cloud.b.a a2 = a1.t().b();
        a(context, a2);
        a2.a("result_type", "json");
        a2.a("rse", a1.p());
        a2.a("text_encoding", a1.o());
        a2.a("ssm", "1", false);
        a2.a("subject", "ivp", false);
        int i = a1.q();
        a2.a("auf=audio/L16;rate", Integer.toString(i), false);
        if (i == 16000)
        {
            a2.a("aue", "speex-wb;10", false);
        } else
        {
            a2.a("aue", "speex", false);
        }
        a2.a("vad_bos", "3000", false);
        a2.a("vad_eos", "700", false);
        a2.a(b.c);
        return a2.toString();
    }

    public static String b(Context context, String s, com.iflytek.cloud.a.d.a a1)
    {
        if (s.equals("oneshot"))
        {
            return a(context, a1.t().e("cloud_grammar"), a1);
        } else
        {
            context = a1.t().b();
            context.a(b.c);
            return context.toString();
        }
    }

    private static void b(com.iflytek.cloud.b.a a1)
    {
        if (!a1.g("speed_increase"))
        {
            int i = a1.a("speed", 50);
            if (i <= 100)
            {
                a1.a("speed", (new StringBuilder()).append("").append(i).toString());
                a1.a("speed_increase", "1");
            } else
            {
                if (100 < i && i <= 150)
                {
                    a1.a("speed", (new StringBuilder()).append("").append(i - 50).toString());
                    a1.a("speed_increase", "2");
                    return;
                }
                if (150 < i && i <= 200)
                {
                    a1.a("speed", (new StringBuilder()).append("").append(i - 100).toString());
                    a1.a("speed_increase", "4");
                    return;
                }
            }
        }
    }

    public static String c(Context context, com.iflytek.cloud.a.d.a a1)
    {
        com.iflytek.cloud.b.a a2 = a1.t().b();
        a(context, a2);
        a2.a("ssm", "1", false);
        a2.a("result_type", "json", false);
        a2.a("rse", a1.p(), false);
        a2.a("text_encoding", a1.o(), false);
        a2.a(b.c);
        return a2.toString();
    }

    public static String d(Context context, com.iflytek.cloud.a.d.a a1)
    {
        com.iflytek.cloud.b.a a2 = a1.t().b();
        a(context, a2);
        a2.a("ssm", "1", false);
        int i = a1.q();
        a2.a("auf=audio/L16;rate", Integer.toString(i));
        if (i == 16000)
        {
            a2.a("aue", "speex-wb", false);
        } else
        {
            a2.a("aue", "speex", false);
        }
        a2.a("voice_name", a2.b("voice_name", a), true);
        a2.a("text_encoding", a1.o(), false);
        b(a2);
        a2.a(b.c);
        return a2.toString();
    }

}
