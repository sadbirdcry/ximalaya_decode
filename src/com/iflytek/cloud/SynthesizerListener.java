// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;

// Referenced classes of package com.iflytek.cloud:
//            SpeechError

public interface SynthesizerListener
{

    public abstract void onBufferProgress(int i, int j, int k, String s);

    public abstract void onCompleted(SpeechError speecherror);

    public abstract void onEvent(int i, int j, int k, Bundle bundle);

    public abstract void onSpeakBegin();

    public abstract void onSpeakPaused();

    public abstract void onSpeakProgress(int i, int j, int k);

    public abstract void onSpeakResumed();
}
