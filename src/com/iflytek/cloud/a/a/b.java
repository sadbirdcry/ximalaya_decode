// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.a;

import android.content.Context;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.d.a;
import com.iflytek.cloud.a.d.c;
import com.iflytek.cloud.a.g.h;
import com.iflytek.cloud.c.f;
import com.iflytek.msc.MSC;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

// Referenced classes of package com.iflytek.cloud.a.a:
//            a

public class b extends a
    implements com.iflytek.cloud.c.f.a
{

    public static int j = 0;
    public static int k = 0;
    protected volatile RecognizerListener a;
    protected boolean b;
    protected boolean c;
    protected boolean d;
    protected boolean e;
    protected int f;
    protected boolean g;
    protected com.iflytek.cloud.a.a.a h;
    protected f i;
    protected String l;
    protected ConcurrentLinkedQueue m;
    protected ArrayList n;
    protected c o;
    private int v;
    private int w;

    public b(Context context, com.iflytek.cloud.b.a a1, HandlerThread handlerthread)
    {
        super(context, handlerthread);
        a = null;
        b = false;
        c = false;
        d = false;
        e = false;
        f = 1;
        g = true;
        h = new com.iflytek.cloud.a.a.a();
        i = null;
        l = null;
        m = null;
        n = null;
        o = new c();
        v = 0;
        w = 0;
        m = new ConcurrentLinkedQueue();
        n = new ArrayList();
        e = false;
        a(a1);
    }

    private void a(boolean flag, byte abyte0[])
        throws SpeechError, UnsupportedEncodingException
    {
        com.iflytek.cloud.a.g.a.b.a("QISRGetResult", null);
        t = SystemClock.elapsedRealtime();
        if (abyte0 != null && abyte0.length > 0)
        {
            abyte0 = new String(abyte0, "utf-8");
        } else
        {
            if (n.size() <= 0)
            {
                abyte0 = t().e("local_grammar");
                if (!TextUtils.isEmpty(abyte0) && !"sms.irf".equals(abyte0))
                {
                    throw new SpeechError(20005);
                } else
                {
                    throw new SpeechError(10118);
                }
            }
            abyte0 = "";
        }
        n.add(abyte0);
        if (a != null && r())
        {
            Bundle bundle = new Bundle();
            bundle.putString("session_id", l());
            a.onEvent(20001, 0, 0, bundle);
            if (flag && t().a("request_audio_url", false))
            {
                Bundle bundle1 = new Bundle();
                bundle1.putString("audio_url", h.d());
                a.onEvent(23001, 0, 0, bundle1);
            }
            abyte0 = new RecognizerResult(abyte0);
            a.onResult(abyte0, flag);
        }
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("msc result time:").append(System.currentTimeMillis()).toString());
        if (flag)
        {
            c(((SpeechError) (null)));
        }
    }

    private void w()
        throws SpeechError, IOException, InterruptedException
    {
        com.iflytek.cloud.a.g.a.a.a("recording stop");
        x();
        o.a("app_lau");
        h.a();
        m();
        a(8, com.iflytek.cloud.a.d.a.a.c, false, u);
    }

    private void x()
    {
        if (i != null)
        {
            i.a(t().a("record_force_stop", false));
            i = null;
            o.a("rec_close");
            if (a != null)
            {
                a.onEvent(22003, 0, 0, null);
            }
        }
    }

    public int a()
    {
        return f;
    }

    protected void a(Message message)
        throws Exception
    {
        super.a(message);
        switch (message.what)
        {
        case 5: // '\005'
        case 6: // '\006'
        case 8: // '\b'
        default:
            return;

        case 0: // '\0'
            d();
            return;

        case 1: // '\001'
            e();
            return;

        case 9: // '\t'
            g();
            return;

        case 2: // '\002'
            b(message);
            return;

        case 3: // '\003'
            w();
            return;

        case 4: // '\004'
            c(message);
            return;

        case 7: // '\007'
            f();
            return;
        }
    }

    public void a(RecognizerListener recognizerlistener)
    {
        this;
        JVM INSTR monitorenter ;
        a = recognizerlistener;
        com.iflytek.cloud.a.g.a.a.a("startListening called");
        c();
        this;
        JVM INSTR monitorexit ;
        return;
        recognizerlistener;
        throw recognizerlistener;
    }

    protected void a(SpeechError speecherror)
    {
        com.iflytek.cloud.a.g.a.a.a("onSessionEnd");
        x();
        j = h.b("upflow");
        k = h.b("downflow");
        l();
        SpeechError speecherror1 = speecherror;
        if (n.size() <= 0)
        {
            speecherror1 = speecherror;
            if (speecherror == null)
            {
                speecherror1 = speecherror;
                if (t().a("asr_nomatch_error", true))
                {
                    speecherror1 = new SpeechError(10118);
                }
            }
        }
        c c1;
        if (speecherror1 != null)
        {
            o.a("app_ret", speecherror1.getErrorCode(), false);
        } else
        {
            o.a("app_ret", 0L, false);
        }
        com.iflytek.cloud.a.g.a.b.a("QISRSessionEnd", null);
        c1 = o;
        if (e)
        {
            speecherror = "1";
        } else
        {
            speecherror = "0";
        }
        c1.a("rec_ustop", speecherror, false);
        h.a("sessinfo", o.a());
        if (s)
        {
            h.a("user abort");
        } else
        if (speecherror1 != null)
        {
            h.a((new StringBuilder()).append("error").append(speecherror1.getErrorCode()).toString());
        } else
        {
            h.a("success");
        }
        super.a(speecherror1);
        if (a != null)
        {
            if (s)
            {
                com.iflytek.cloud.a.g.a.a.a("RecognizerListener#onCancel");
            } else
            {
                com.iflytek.cloud.a.g.a.a.a("RecognizerListener#onEnd");
                if (speecherror1 != null)
                {
                    a.onError(speecherror1);
                    return;
                }
            }
        }
    }

    public void a(byte abyte0[], int i1)
    {
        if (a != null && r())
        {
            a.onVolumeChanged(i1);
        }
    }

    public void a(byte abyte0[], int i1, int j1)
    {
        while (abyte0 == null || j1 <= 0 || abyte0.length < j1 || j1 <= 0 || !r()) 
        {
            return;
        }
        if (!b)
        {
            b = true;
            o.a("rec_start");
        }
        if (v > 0)
        {
            if (v >= j1)
            {
                v = v - j1;
                return;
            } else
            {
                byte abyte1[] = new byte[j1 - v];
                System.arraycopy(abyte0, v + i1, abyte1, 0, j1 - v);
                d(obtainMessage(2, abyte1));
                v = 0;
                return;
            }
        } else
        {
            byte abyte2[] = new byte[j1];
            System.arraycopy(abyte0, i1, abyte2, 0, j1);
            d(obtainMessage(2, abyte2));
            return;
        }
    }

    protected void a(byte abyte0[], boolean flag)
        throws SpeechError
    {
        if (!c)
        {
            c = true;
            o.a("app_fau");
            if (a != null)
            {
                a.onEvent(22002, 0, 0, null);
            }
        }
        com.iflytek.cloud.a.g.a.b.a("QISRAudioWrite", (new StringBuilder()).append("").append(abyte0.length).toString());
        h.a(abyte0, abyte0.length);
        if (flag)
        {
            int i1 = h.b();
            com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("QISRAudioWrite volume:").append(i1).toString());
            a(abyte0, i1);
        }
    }

    public boolean a(boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("stopRecognize, current status is :").append(s()).append(" usercancel : ").append(flag).toString());
        o.a("app_stop");
        x();
        e = flag;
        a(3);
        this;
        JVM INSTR monitorexit ;
        return true;
        Exception exception;
        exception;
        throw exception;
    }

    public ConcurrentLinkedQueue b()
    {
        return m;
    }

    protected void b(Message message)
        throws Exception
    {
        message = (byte[])(byte[])message.obj;
        if (message == null || message.length == 0)
        {
            return;
        } else
        {
            m.add(message);
            a(message, true);
            return;
        }
    }

    public void b(SpeechError speecherror)
    {
        c(speecherror);
    }

    public void b(boolean flag)
    {
        if (flag && r() && a != null)
        {
            a.onError(new SpeechError(20017));
        }
        x();
        if (s() == com.iflytek.cloud.a.d.a.b.c)
        {
            e = true;
        }
        super.b(flag);
    }

    protected void c()
    {
        o.a(t());
        super.c();
    }

    void c(Message message)
        throws SpeechError, InterruptedException, UnsupportedEncodingException
    {
        int i1 = message.arg1;
        message = (byte[])(byte[])message.obj;
        switch (i1)
        {
        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        default:
            return;

        case 0: // '\0'
            if (!d)
            {
                d = true;
                o.a("app_frs");
            }
            a(false, message);
            return;

        case 5: // '\005'
            break;
        }
        if (!d)
        {
            d = true;
            o.a("app_frs");
        }
        o.a("app_lrs");
        a(true, message);
    }

    public void c(boolean flag)
    {
        o.a("rec_ready");
    }

    protected void d()
        throws Exception
    {
        String s;
        com.iflytek.cloud.a.g.a.a.a("start connecting");
        s = t().e("engine_type");
        if (!t().a("net_check", true)) goto _L2; else goto _L1
_L1:
        if (!"cloud".equals(s)) goto _L4; else goto _L3
_L3:
        com.iflytek.cloud.a.g.h.a(r);
_L2:
        int i1;
        i1 = t().a("record_read_rate", 40);
        if (hasMessages(3))
        {
            throw new SpeechError(10118);
        }
        break; /* Loop/switch isn't completed */
_L4:
        if ("mix".equals(s) || "mixed".equals(s))
        {
            try
            {
                com.iflytek.cloud.a.g.h.a(r);
            }
            catch (Exception exception)
            {
                t().a("engine_type", "local");
            }
        }
        if (true) goto _L2; else goto _L5
_L5:
        if (f != -1 && r())
        {
            com.iflytek.cloud.a.g.a.a.a("start  record");
            if (f == -2)
            {
                String s1 = t().e("asr_source_path");
                i = new com.iflytek.cloud.c.a(q(), i1, f, s1);
            } else
            {
                i = new f(q(), i1, f);
            }
            o.a("rec_open");
            i.a(this);
            a(9, com.iflytek.cloud.a.d.a.a.c, false, p);
            if (a != null)
            {
                a.onBeginOfSpeech();
            }
        }
        com.iflytek.cloud.a.g.a.b.a("QISRSessionBegin", null);
        o.a("app_ssb");
        a(1, com.iflytek.cloud.a.d.a.a.a, false, 0);
        return;
    }

    protected void e()
        throws Exception
    {
        int i1 = h.a(r, l, this);
        if (i1 == 0 && h.a != null)
        {
            if (r())
            {
                MSC.QISRRegisterNotify(h.a, "rsltCb", "stusCb", "errCb", this);
                a(com.iflytek.cloud.a.d.a.b.c);
                if (t().a("asr_net_perf", false))
                {
                    a(7, com.iflytek.cloud.a.d.a.a.a, false, 0);
                }
            }
        } else
        {
            w = w + 1;
            if (w > 40)
            {
                throw new SpeechError(i1);
            }
            if (r())
            {
                Thread.sleep(15L);
                a(1, com.iflytek.cloud.a.d.a.a.a, false, 0);
                return;
            }
        }
    }

    void errCb(char ac[], int i1, byte abyte0[])
    {
        b(new SpeechError(i1));
    }

    public void f()
    {
        if (r())
        {
            int i1 = h.b("netperf");
            if (a != null)
            {
                a.onEvent(10001, i1, 0, null);
            }
            a(7, com.iflytek.cloud.a.d.a.a.b, false, 100);
        }
    }

    public void g()
    {
        if (com.iflytek.cloud.a.d.a.b.c == s())
        {
            if (a != null)
            {
                a.onEndOfSpeech();
            }
            a(false);
        }
    }

    protected void h()
    {
        l = t().e("cloud_grammar");
        f = t().a("audio_source", 1);
        g = com.iflytek.cloud.b.c.a(t().e("domain"));
        v = t().a("filter_audio_time", 0) * (((t().a("sample_rate", q) / 1000) * 16) / 8);
        p = t().a("speech_timeout", p);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("mSpeechTimeOut=").append(p).toString());
        super.h();
    }

    public boolean i()
    {
        return g;
    }

    public c j()
    {
        return o;
    }

    public void k()
    {
        if (i != null && (i instanceof com.iflytek.cloud.c.a))
        {
            a(true);
        }
    }

    public String l()
    {
        return h.c();
    }

    void rsltCb(char ac[], byte abyte0[], int i1, int j1)
    {
        if (abyte0 != null)
        {
            com.iflytek.cloud.a.g.a.a.a("MscRecognizer", (new StringBuilder()).append("rsltCb:").append(j1).append("result:").append(new String(abyte0)).toString());
        } else
        {
            com.iflytek.cloud.a.g.a.a.a("MscRecognizer", (new StringBuilder()).append("rsltCb:").append(j1).append("result:null").toString());
        }
        ac = obtainMessage(4, j1, 0, abyte0);
        if (hasMessages(4))
        {
            a(ac, com.iflytek.cloud.a.d.a.a.b, false, 0);
            return;
        } else
        {
            a(ac, com.iflytek.cloud.a.d.a.a.a, false, 0);
            return;
        }
    }

    void stusCb(char ac[], int i1, int j1, int k1, byte abyte0[])
    {
        if (i1 == 0 && j1 == 3)
        {
            g();
        }
    }

}
