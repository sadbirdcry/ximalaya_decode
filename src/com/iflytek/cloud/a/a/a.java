// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.a;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import com.iflytek.cloud.GrammarListener;
import com.iflytek.cloud.LexiconListener;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.d.b;
import com.iflytek.cloud.b.c;
import com.iflytek.msc.MSC;
import com.iflytek.msc.MSCSessionInfo;
import java.io.UnsupportedEncodingException;

public class a extends b
{

    private static GrammarListener f;
    private static LexiconListener g;
    private MSCSessionInfo c;
    private MSCSessionInfo d;
    private byte e[];
    private String h;

    public a()
    {
        c = new MSCSessionInfo();
        d = new MSCSessionInfo();
        e = null;
        h = "";
    }

    private void a(byte abyte0[], int i, int j)
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        j = MSC.QISRAudioWrite(a, abyte0, i, j, d);
        c.sesstatus = d.sesstatus;
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("QISRAudioWrite length:").append(i).toString());
        if (j == 0)
        {
            break MISSING_BLOCK_LABEL_77;
        }
        throw new SpeechError(d.errorcode);
        abyte0;
        this;
        JVM INSTR monitorexit ;
        throw abyte0;
        this;
        JVM INSTR monitorexit ;
    }

    public int a(Context context, String s, com.iflytek.cloud.a.d.a a1)
        throws SpeechError, UnsupportedEncodingException
    {
        context = com.iflytek.cloud.b.c.a(context, s, a1);
        long l = SystemClock.elapsedRealtime();
        com/iflytek/cloud/a/a/a;
        JVM INSTR monitorenter ;
        if (!TextUtils.isEmpty(s))
        {
            break MISSING_BLOCK_LABEL_139;
        }
        com.iflytek.cloud.a.g.a.a.a(context);
        a = MSC.QISRSessionBegin(null, context.getBytes(a1.n()), c);
_L1:
        com/iflytek/cloud/a/a/a;
        JVM INSTR monitorexit ;
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("sessionBegin ErrCode:").append(c.errorcode).append(" time:").append(SystemClock.elapsedRealtime() - l).toString());
        int i = c.errorcode;
        if (i != 0 && i != 10129 && i != 10113 && i != 10132)
        {
            throw new SpeechError(i);
        } else
        {
            return i;
        }
        a = MSC.QISRSessionBegin(s.getBytes(a1.n()), context.getBytes(a1.n()), c);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("sessionBegin grammarId:").append(s).toString());
          goto _L1
        context;
        com/iflytek/cloud/a/a/a;
        JVM INSTR monitorexit ;
        throw context;
    }

    public void a()
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        a(new byte[0], 0, 4);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void a(String s)
    {
        if (a == null)
        {
            return;
        }
        com.iflytek.cloud.a.g.a.a.a("sessionEnd enter ");
        long l = System.currentTimeMillis();
        boolean flag;
        if (MSC.QISRSessionEnd(a, s.getBytes()) == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("sessionEnd leavel:").append(flag).append(" time:").append(System.currentTimeMillis() - l).toString());
        a = null;
        b = null;
    }

    public void a(byte abyte0[], int i)
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        a(abyte0, i, 2);
        this;
        JVM INSTR monitorexit ;
        return;
        abyte0;
        throw abyte0;
    }

    public boolean a(String s, String s1)
    {
        boolean flag1 = false;
        this;
        JVM INSTR monitorenter ;
        boolean flag = flag1;
        if (TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        flag = flag1;
        if (TextUtils.isEmpty(s1)) goto _L2; else goto _L3
_L3:
        char ac[] = a;
        if (ac != null) goto _L5; else goto _L4
_L4:
        flag = flag1;
_L2:
        this;
        JVM INSTR monitorexit ;
        return flag;
_L5:
        int i = MSC.QISRSetParam(a, s.getBytes("utf-8"), s1.getBytes("utf-8"));
_L7:
        flag = flag1;
        if (i == 0)
        {
            flag = true;
        }
        continue; /* Loop/switch isn't completed */
        s;
        s.printStackTrace();
        i = -1;
        if (true) goto _L7; else goto _L6
_L6:
        s;
        throw s;
        if (true) goto _L2; else goto _L8
_L8:
    }

    public int b()
    {
        boolean flag = false;
        this;
        JVM INSTR monitorenter ;
        int i = MSC.QISRGetParam(a, "volume".getBytes(), d);
        if (i != 0) goto _L2; else goto _L1
_L1:
        int j = Integer.parseInt(new String(new String(d.buffer)));
        i = j;
_L3:
        this;
        JVM INSTR monitorexit ;
        return i;
_L2:
        com.iflytek.cloud.a.g.a.a.a("VAD CHECK FALSE");
        i = ((flag) ? 1 : 0);
          goto _L3
        Object obj;
        obj;
_L4:
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("getAudioVolume Exception vadret = ").append(i).toString());
        i = ((flag) ? 1 : 0);
          goto _L3
        obj;
        throw obj;
        obj;
        i = 0;
          goto _L4
    }

    public int b(String s)
    {
        boolean flag = false;
        this;
        JVM INSTR monitorenter ;
        char ac[] = a;
        if (ac != null) goto _L2; else goto _L1
_L1:
        int i = ((flag) ? 1 : 0);
_L4:
        this;
        JVM INSTR monitorexit ;
        return i;
_L2:
        s = c(s);
        i = ((flag) ? 1 : 0);
        try
        {
            if (!TextUtils.isEmpty(s))
            {
                i = Integer.parseInt(new String(s));
            }
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
        finally
        {
            this;
        }
        s.printStackTrace();
        i = ((flag) ? 1 : 0);
        if (true) goto _L4; else goto _L3
_L3:
        throw s;
    }

    protected String c()
    {
        if (b == null)
        {
            b = c("sid");
        }
        return b;
    }

    public String c(String s)
    {
        Object obj = null;
        this;
        JVM INSTR monitorenter ;
        char ac[] = a;
        if (ac != null) goto _L2; else goto _L1
_L1:
        String s1 = obj;
_L4:
        this;
        JVM INSTR monitorexit ;
        return s1;
_L2:
        s1 = obj;
        try
        {
            if (MSC.QISRGetParam(a, s.getBytes(), c) == 0)
            {
                s1 = new String(c.buffer);
            }
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s1 = obj;
        }
        finally
        {
            this;
        }
        continue; /* Loop/switch isn't completed */
        throw s;
        if (true) goto _L4; else goto _L3
_L3:
    }

    protected String d()
    {
        return c("audio_url");
    }

    int grammarCallBack(int i, char ac[])
    {
        Object obj = null;
        if (f != null)
        {
            if (i != 0)
            {
                GrammarListener grammarlistener = f;
                if (i == 0)
                {
                    ac = obj;
                } else
                {
                    ac = new SpeechError(i);
                }
                grammarlistener.onBuildFinish("", ac);
            } else
            {
                f.onBuildFinish(String.valueOf(ac), null);
            }
        }
        return 0;
    }

    int lexiconCallBack(int i, char ac[])
    {
        ac = null;
        if (g != null)
        {
            if (i != 0)
            {
                LexiconListener lexiconlistener = g;
                String s = h;
                if (i != 0)
                {
                    ac = new SpeechError(i);
                }
                lexiconlistener.onLexiconUpdated(s, ac);
            } else
            {
                g.onLexiconUpdated(h, null);
            }
        }
        return 0;
    }
}
