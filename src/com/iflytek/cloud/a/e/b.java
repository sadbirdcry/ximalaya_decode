// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.e;

import android.content.Context;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechListener;
import com.iflytek.cloud.a.d.a;
import java.io.IOException;

// Referenced classes of package com.iflytek.cloud.a.e:
//            a

public class b extends com.iflytek.cloud.a.d.a
{
    private class a
    {

        final b a;
        private byte b[];
        private String c;

        public byte[] a()
        {
            return b;
        }

        public String b()
        {
            return c;
        }

        public a(byte abyte0[], String s)
        {
            a = b.this;
            super();
            b = null;
            c = "";
            b = abyte0;
            c = s;
        }
    }


    private SpeechListener a;
    private com.iflytek.cloud.a.e.a b;

    public b(Context context, com.iflytek.cloud.b.a a1)
    {
        super(context);
        a = null;
        b = new com.iflytek.cloud.a.e.a();
        a(a1);
    }

    public b(Context context, com.iflytek.cloud.b.a a1, HandlerThread handlerthread)
    {
        super(context, handlerthread);
        a = null;
        b = new com.iflytek.cloud.a.e.a();
        a(a1);
    }

    public SpeechError a(String s, String s1)
    {
        com.iflytek.cloud.a.g.a.b.a("QMSPLogin", null);
        com.iflytek.cloud.a.e.a.a(r, s, s1, this);
        if (false)
        {
            (new StringBuilder()).append(v()).append(" occur Error = ");
            throw new NullPointerException();
        } else
        {
            return null;
        }
        s;
        s.printStackTrace();
        if (s != null)
        {
            com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append(v()).append(" occur Error = ").append(s.toString()).toString());
            return s;
        } else
        {
            return s;
        }
        s;
        s.printStackTrace();
        s = new SpeechError(20010);
        if (s != null)
        {
            com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append(v()).append(" occur Error = ").append(s.toString()).toString());
            return s;
        } else
        {
            return s;
        }
        s;
        if (false)
        {
            (new StringBuilder()).append(v()).append(" occur Error = ");
            throw new NullPointerException();
        } else
        {
            throw s;
        }
    }

    protected void a(Message message)
        throws Exception
    {
        super.a(message);
        message.what;
        JVM INSTR tableswitch 10 12: default 36
    //                   10 53
    //                   11 117
    //                   12 139;
           goto _L1 _L2 _L3 _L4
_L1:
        message = null;
_L6:
        if (message == null)
        {
            throw new SpeechError(20004);
        }
        break; /* Loop/switch isn't completed */
_L2:
        message = (a)message.obj;
        if (message.a() == null || message.a().length <= 0)
        {
            throw new SpeechError(20009);
        }
        com.iflytek.cloud.a.g.a.b.a("QMSPUploadData", null);
        message = b.a(r, message.b(), message.a(), this);
        continue; /* Loop/switch isn't completed */
_L3:
        com.iflytek.cloud.a.g.a.b.a("QMSPDownloadData", null);
        message = b.a(r, this);
        continue; /* Loop/switch isn't completed */
_L4:
        message = (String)message.obj;
        if (TextUtils.isEmpty(message))
        {
            throw new SpeechError(20009);
        }
        com.iflytek.cloud.a.g.a.b.a("QMSPSearch", null);
        message = b.a(r, this, message);
        if (true) goto _L6; else goto _L5
_L5:
        if (a != null)
        {
            a.onBufferReceived(message);
        }
        c(null);
        return;
    }

    protected void a(SpeechError speecherror)
    {
        super.a(speecherror);
        if (a != null && !s)
        {
            a.onCompleted(speecherror);
        }
    }

    public void a(SpeechListener speechlistener)
    {
        a = speechlistener;
        a(11);
    }

    public void a(SpeechListener speechlistener, String s)
    {
        a = speechlistener;
        d(obtainMessage(12, s));
    }

    public void a(SpeechListener speechlistener, String s, byte abyte0[])
    {
        a = speechlistener;
        d(obtainMessage(10, new a(abyte0, s)));
    }
}
