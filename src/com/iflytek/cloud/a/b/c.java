// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.b;

import android.content.Context;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.VerifierListener;
import com.iflytek.cloud.VerifierResult;
import com.iflytek.cloud.a.d.a;
import com.iflytek.cloud.a.g.h;
import com.iflytek.cloud.c.f;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ConcurrentLinkedQueue;

// Referenced classes of package com.iflytek.cloud.a.b:
//            d

public class c extends a
    implements com.iflytek.cloud.c.f.a
{

    protected volatile VerifierListener a;
    protected long b;
    protected boolean c;
    protected d d;
    protected f e;
    protected String f;
    protected String g;
    protected VerifierResult h;
    protected ConcurrentLinkedQueue i;
    protected int j;
    private long k;
    private int l;

    public c(Context context, com.iflytek.cloud.b.a a1, HandlerThread handlerthread)
    {
        super(context, handlerthread);
        a = null;
        b = 0L;
        c = true;
        d = new d();
        e = null;
        f = "train";
        g = "";
        h = null;
        i = null;
        j = 1;
        k = 0L;
        l = 0;
        i = new ConcurrentLinkedQueue();
        a(a1);
    }

    private boolean j()
    {
        return "train".equalsIgnoreCase(t().e("sst"));
    }

    private void l()
        throws SpeechError, IOException, InterruptedException
    {
        com.iflytek.cloud.a.g.a.a.a("--->onStoped: in");
        if (!j())
        {
            y();
        }
        d.a();
        a(4);
        com.iflytek.cloud.a.g.a.a.a("--->onStoped: out");
    }

    private void w()
        throws SpeechError, UnsupportedEncodingException
    {
        com.iflytek.cloud.a.d.b.a a1;
        com.iflytek.cloud.a.g.a.a.a("--->requestResult: in");
        a1 = d.e();
        static class _cls1
        {

            static final int a[];

            static 
            {
                a = new int[com.iflytek.cloud.a.d.b.a.values().length];
                try
                {
                    a[com.iflytek.cloud.a.d.b.a.c.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    a[com.iflytek.cloud.a.d.b.a.a.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        com.iflytek.cloud.a.b._cls1.a[a1.ordinal()];
        JVM INSTR tableswitch 1 2: default 44
    //                   1 44
    //                   2 50;
           goto _L1 _L1 _L2
_L1:
        com.iflytek.cloud.a.g.a.a.a("--->requestResult: out");
        return;
_L2:
        x();
        if (true) goto _L1; else goto _L3
_L3:
    }

    private void x()
        throws SpeechError, UnsupportedEncodingException
    {
        t = SystemClock.elapsedRealtime();
        h = new VerifierResult(new String(d.d(), "utf-8"));
        if (f.equals("train") && h.ret == 0 && h.suc < h.rgn)
        {
            if (a != null)
            {
                a.onResult(h);
            }
            a(0);
            return;
        }
        if (a != null)
        {
            a.onResult(h);
        }
        c(((SpeechError) (null)));
    }

    private void y()
    {
        if (e != null)
        {
            e.a(t().a("record_force_stop", false));
            e = null;
        }
    }

    protected void a(Message message)
        throws Exception
    {
        super.a(message);
        switch (message.what)
        {
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
        default:
            return;

        case 0: // '\0'
            b();
            return;

        case 1: // '\001'
            d();
            return;

        case 2: // '\002'
            b(message);
            return;

        case 3: // '\003'
            l();
            return;

        case 4: // '\004'
            c(message);
            return;

        case 9: // '\t'
            com.iflytek.cloud.a.g.a.a.a("--->on timeout vad");
            break;
        }
        e();
    }

    protected void a(SpeechError speecherror)
    {
        com.iflytek.cloud.a.g.a.a.a("--->onEnd: in");
        y();
        if (s)
        {
            d.a("user abort");
        } else
        if (speecherror != null)
        {
            d.a((new StringBuilder()).append("error").append(speecherror.getErrorCode()).toString());
        } else
        {
            d.a("success");
        }
        super.a(speecherror);
        if (a != null && !s) goto _L2; else goto _L1
_L1:
        com.iflytek.cloud.a.g.a.a.a("--->onEnd: out");
        return;
_L2:
        com.iflytek.cloud.a.g.a.a.a("VerifyListener#onEnd");
        if (speecherror != null)
        {
            a.onError(speecherror);
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public void a(VerifierListener verifierlistener)
    {
        this;
        JVM INSTR monitorenter ;
        com.iflytek.cloud.a.g.a.a.a("--->startVerify: in");
        a = verifierlistener;
        c();
        com.iflytek.cloud.a.g.a.a.a("--->startVerify: out");
        this;
        JVM INSTR monitorexit ;
        return;
        verifierlistener;
        throw verifierlistener;
    }

    public void a(byte abyte0[], int i1)
    {
        if (r())
        {
            a.onVolumeChanged(i1);
        }
    }

    public void a(byte abyte0[], int i1, int j1)
    {
        while (com.iflytek.cloud.a.d.a.b.c != s() || j1 <= 0) 
        {
            return;
        }
        if (l > 0)
        {
            if (l >= j1)
            {
                l = l - j1;
                return;
            } else
            {
                byte abyte1[] = new byte[j1 - l];
                System.arraycopy(abyte0, l + i1, abyte1, 0, j1 - l);
                d(obtainMessage(2, abyte1));
                l = 0;
                return;
            }
        } else
        {
            byte abyte2[] = new byte[j1];
            System.arraycopy(abyte0, i1, abyte2, 0, j1);
            d(obtainMessage(2, abyte2));
            return;
        }
    }

    protected void a(byte abyte0[], boolean flag)
        throws SpeechError
    {
label0:
        {
            d.a(abyte0, abyte0.length);
            if (flag)
            {
                if (!d.b())
                {
                    break label0;
                }
                com.iflytek.cloud.a.g.a.a.a("---> VadCheck Time: Vad End Point");
                e();
            }
            return;
        }
        a(abyte0, d.c());
    }

    public boolean a()
    {
        this;
        JVM INSTR monitorenter ;
        com.iflytek.cloud.a.g.a.a.a("--->stopRecord: in");
        if (s() == com.iflytek.cloud.a.d.a.b.c) goto _L2; else goto _L1
_L1:
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("endVerify fail  status is :").append(s()).toString());
        boolean flag = false;
_L4:
        this;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        if (!j())
        {
            y();
        }
        a(3);
        com.iflytek.cloud.a.g.a.a.a("--->stopRecord: out");
        flag = true;
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    protected void b()
        throws Exception
    {
        com.iflytek.cloud.a.g.a.a.a("--->onStart: in");
        String s = t().e("engine_type");
        boolean flag = t().a("net_check", true);
        if ("cloud".equals(s) && flag)
        {
            com.iflytek.cloud.a.g.h.a(r);
        }
        int i1 = t().a("record_read_rate", 40);
        if (j != -1 && r())
        {
            com.iflytek.cloud.a.g.a.a.a("start  record");
            if (e == null)
            {
                e = new f(q(), i1, j);
                e.a(this);
            }
        }
        if (s() != com.iflytek.cloud.a.d.a.b.e && a != null)
        {
            a.onBeginOfSpeech();
        }
        b = SystemClock.elapsedRealtime();
        removeMessages(9);
        a(9, com.iflytek.cloud.a.d.a.a.c, false, p);
        a(1, com.iflytek.cloud.a.d.a.a.a, false, 0);
        com.iflytek.cloud.a.g.a.a.a("--->onStart: out");
    }

    protected void b(Message message)
        throws Exception
    {
        message = (byte[])(byte[])message.obj;
        if (message == null || message.length == 0)
        {
            return;
        } else
        {
            i.add(message);
            a(message, true);
            return;
        }
    }

    public void b(SpeechError speecherror)
    {
        c(speecherror);
    }

    public void b(boolean flag)
    {
        if (flag && r() && a != null)
        {
            a.onError(new SpeechError(20017));
        }
        y();
        super.b(flag);
    }

    void c(Message message)
        throws SpeechError, InterruptedException, UnsupportedEncodingException
    {
        if (!j())
        {
            y();
        }
        w();
        if (s() == com.iflytek.cloud.a.d.a.b.d)
        {
            a(4, com.iflytek.cloud.a.d.a.a.b, false, 20);
        }
    }

    public void c(boolean flag)
    {
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("time cost: onRecordStarted:").append(SystemClock.elapsedRealtime() - k).toString());
    }

    protected void d()
        throws Exception
    {
        if (d.a == null)
        {
            d.a(r, g, this);
        }
        a(com.iflytek.cloud.a.d.a.b.c);
    }

    public void e()
    {
        if (com.iflytek.cloud.a.d.a.b.c == s())
        {
            com.iflytek.cloud.a.g.a.a.a("--->vadEndCall: out");
            a();
            if (a != null)
            {
                a.onEndOfSpeech();
            }
        }
    }

    public ConcurrentLinkedQueue f()
    {
        return i;
    }

    public int g()
    {
        return j;
    }

    protected void h()
    {
        p = t().a("speech_timeout", p);
        g = t().e("vid");
        j = t().a("audio_source", 1);
        l = t().a("filter_audio_time", 0) * (((t().a("sample_rate", q) / 1000) * 16) / 8);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("mSpeechTimeOut=").append(p).toString());
        super.h();
    }

    public void k()
    {
    }
}
