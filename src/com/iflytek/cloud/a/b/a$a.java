// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.b;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechListener;

// Referenced classes of package com.iflytek.cloud.a.b:
//            a, b

public static final class a
    implements SpeechListener
{

    private SpeechListener a;
    private Handler b;

    static SpeechListener a(stener stener)
    {
        return stener.a;
    }

    public void onBufferReceived(byte abyte0[])
    {
        abyte0 = b.obtainMessage(1, abyte0);
        b.sendMessage(abyte0);
    }

    public void onCompleted(SpeechError speecherror)
    {
        speecherror = b.obtainMessage(2, speecherror);
        b.sendMessage(speecherror);
    }

    public void onEvent(int i, Bundle bundle)
    {
        bundle = b.obtainMessage(0, i, 0, bundle);
        b.sendMessage(bundle);
    }

    public stener(SpeechListener speechlistener)
    {
        a = null;
        b = new b(this, Looper.getMainLooper());
        a = speechlistener;
    }
}
