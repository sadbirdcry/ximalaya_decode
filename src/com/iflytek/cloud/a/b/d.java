// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.b;

import android.content.Context;
import android.os.SystemClock;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.d.a;
import com.iflytek.cloud.a.d.b;
import com.iflytek.cloud.b.c;
import com.iflytek.msc.MSC;
import com.iflytek.msc.MSCSessionInfo;
import java.io.UnsupportedEncodingException;
import java.util.Date;

public class d extends b
{

    private MSCSessionInfo c;
    private MSCSessionInfo d;
    private MSCSessionInfo e;
    private byte f[];

    public d()
    {
        c = new MSCSessionInfo();
        d = new MSCSessionInfo();
        e = new MSCSessionInfo();
        f = null;
    }

    private void a(byte abyte0[], int i, int j)
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        i = MSC.QISVAudioWrite(a, null, abyte0, i, j, c);
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_36;
        }
        throw new SpeechError(i);
        abyte0;
        this;
        JVM INSTR monitorexit ;
        throw abyte0;
        this;
        JVM INSTR monitorexit ;
    }

    public int a(Context context, a a1)
        throws UnsupportedEncodingException, SpeechError
    {
        long l = SystemClock.elapsedRealtime();
        String s1 = a1.t().e("vid");
        String s = com.iflytek.cloud.b.c.b(context, a1);
        com.iflytek.cloud.a.g.a.a.a("sendRequest enter ");
        int i;
        if (s1 == null)
        {
            context = null;
        } else
        {
            context = s1.getBytes(a1.n());
        }
        MSC.QISVQueDelModelRelease(MSC.QISVQueDelModel(context, s.getBytes(a1.n()), e));
        if (e.errorcode != 0)
        {
            i = e.errorcode;
        } else
        if ("true".equals(new String(e.buffer)))
        {
            i = 0;
        } else
        {
            i = -1;
        }
        if (i != 0 && -1 != i)
        {
            throw new SpeechError(i);
        } else
        {
            com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("sendRequest leavel:").append(i).append(" time:").append(SystemClock.elapsedRealtime() - l).toString());
            return i;
        }
    }

    public int a(Context context, String s, a a1)
        throws SpeechError, UnsupportedEncodingException
    {
        context = com.iflytek.cloud.b.c.b(context, a1);
        long l = SystemClock.elapsedRealtime();
        byte abyte0[] = context.getBytes(a1.n());
        int i;
        if (s == null)
        {
            context = null;
        } else
        {
            context = s.getBytes(a1.n());
        }
        a = MSC.QISVSessionBegin(abyte0, context, c);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("sessionBegin ErrCode:").append(c.errorcode).append(" time:").append(SystemClock.elapsedRealtime() - l).toString());
        i = c.errorcode;
        if (i != 0 && i != 10129 && i != 10113 && i != 10132)
        {
            throw new SpeechError(i);
        } else
        {
            return 0;
        }
    }

    public void a()
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        a(new byte[0], 0, 4);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void a(String s)
    {
        if (a == null)
        {
            return;
        }
        com.iflytek.cloud.a.g.a.a.a("sessionEnd enter ");
        long l = System.currentTimeMillis();
        boolean flag;
        if (MSC.QISVSessionEnd(a, s.getBytes()) == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("sessionEnd leavel:").append(flag).append(" time:").append(System.currentTimeMillis() - l).toString());
        a = null;
        b = null;
    }

    public void a(byte abyte0[], int i)
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        a(abyte0, i, 2);
        this;
        JVM INSTR monitorexit ;
        return;
        abyte0;
        throw abyte0;
    }

    public boolean b()
    {
        this;
        JVM INSTR monitorenter ;
        int i = c.epstatues;
        boolean flag;
        if (i >= 3)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    public int c()
    {
        boolean flag = false;
        this;
        JVM INSTR monitorenter ;
        int i = MSC.QISVGetParam(a, "volume".getBytes(), d);
        if (i != 0) goto _L2; else goto _L1
_L1:
        int j = Integer.parseInt(new String(new String(d.buffer)));
        i = j;
_L3:
        this;
        JVM INSTR monitorexit ;
        return i;
_L2:
        com.iflytek.cloud.a.g.a.a.a("VAD CHECK FALSE");
        i = ((flag) ? 1 : 0);
          goto _L3
        Object obj;
        obj;
_L4:
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("getAudioVolume Exception vadret = ").append(i).toString());
        i = ((flag) ? 1 : 0);
          goto _L3
        obj;
        throw obj;
        obj;
        i = 0;
          goto _L4
    }

    public byte[] d()
    {
        return f;
    }

    public com.iflytek.cloud.a.d.b.a e()
        throws SpeechError
    {
        int i;
        Date date = new Date();
        f = MSC.QISVGetResult(a, null, c);
        Date date1 = new Date();
        StringBuilder stringbuilder = (new StringBuilder()).append("QISVGetResult leavel:");
        boolean flag;
        if (f != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        com.iflytek.cloud.a.g.a.a.a(stringbuilder.append(flag).append(" time:").append(date1.getTime() - date.getTime()).toString());
        i = c.errorcode;
        if (i != 0) goto _L2; else goto _L1
_L1:
        i = c.rsltstatus;
        i;
        JVM INSTR tableswitch 0 5: default 148
    //                   0 192
    //                   1 158
    //                   2 148
    //                   3 148
    //                   4 148
    //                   5 192;
           goto _L3 _L4 _L5 _L3 _L3 _L3 _L4
_L3:
        return com.iflytek.cloud.a.d.b.a.c;
_L5:
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("ResultStatus: noResult").append(i).toString());
        throw new SpeechError(20005);
_L4:
        if (f == null) goto _L3; else goto _L6
_L6:
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("ResultStatus: hasResult").append(i).toString());
        return com.iflytek.cloud.a.d.b.a.a;
_L2:
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("Result: error errorcode is ").append(i).toString());
        throw new SpeechError(i);
    }
}
