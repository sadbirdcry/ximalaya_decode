// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.f;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SynthesizerListener;
import com.iflytek.cloud.a.d.a;
import com.iflytek.cloud.c.b;
import java.io.IOException;
import java.util.ArrayList;

// Referenced classes of package com.iflytek.cloud.a.f:
//            b, c

final class h
    implements com.iflytek.cloud.a.f.b
{

    final Handler a;
    final String b;
    final c c;

    h(c c1, Handler handler, String s)
    {
        c = c1;
        a = handler;
        b = s;
        super();
    }

    public void a(SpeechError speecherror)
    {
label0:
        {
            if (com.iflytek.cloud.a.f.c.g(c) != null && speecherror != null)
            {
                if (!com.iflytek.cloud.a.f.c.h(c))
                {
                    break label0;
                }
                Message.obtain(a, 6, speecherror).sendToTarget();
            }
            return;
        }
        com.iflytek.cloud.a.f.c.g(c).onCompleted(speecherror);
    }

    public void a(ArrayList arraylist, int i, int j, int k, String s)
    {
        if (com.iflytek.cloud.a.f.c.i(c) != null && com.iflytek.cloud.a.f.c.j(c).t().a("tts_data_notify", false) && com.iflytek.cloud.a.f.c.g(c) != null && arraylist != null)
        {
            int l = 0;
            while (l < arraylist.size()) 
            {
                byte abyte0[] = (byte[])arraylist.get(l);
                Bundle bundle = new Bundle();
                bundle.putByteArray("buffer", abyte0);
                if (com.iflytek.cloud.a.f.c.h(c))
                {
                    Message message = Message.obtain();
                    message.what = 21001;
                    message.arg1 = 0;
                    message.arg2 = 0;
                    message.obj = bundle;
                    Message.obtain(a, 7, 0, 0, message).sendToTarget();
                } else
                {
                    com.iflytek.cloud.a.f.c.g(c).onEvent(21001, 0, 0, bundle);
                }
                l++;
            }
        }
        com.iflytek.cloud.a.f.c.c(c).a(arraylist, i, j, k);
        if (com.iflytek.cloud.a.f.c.g(c) == null) goto _L2; else goto _L1
_L1:
        arraylist = new Bundle();
        arraylist.putInt("percent", i);
        arraylist.putInt("begpos", j);
        arraylist.putInt("endpos", k);
        arraylist.putString("spellinfo", s);
        if (!com.iflytek.cloud.a.f.c.h(c)) goto _L4; else goto _L3
_L3:
        Message.obtain(a, 2, arraylist).sendToTarget();
_L2:
        if (i < 100) goto _L6; else goto _L5
_L5:
        if (com.iflytek.cloud.a.f.c.k(c) == null || !com.iflytek.cloud.a.f.c.l(c).t().a("tts_data_notify", false)) goto _L8; else goto _L7
_L7:
        if (!TextUtils.isEmpty(b) && !com.iflytek.cloud.a.f.c.c(c).b())
        {
            throw new IOException();
        }
          goto _L9
_L11:
        try
        {
            com.iflytek.cloud.a.f.c.g(c).onCompleted(null);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ArrayList arraylist)
        {
            arraylist.printStackTrace();
        }
        if (com.iflytek.cloud.a.f.c.g(c) != null)
        {
            if (com.iflytek.cloud.a.f.c.h(c))
            {
                Message.obtain(a, 6, new SpeechError(20010)).sendToTarget();
            } else
            {
                try
                {
                    com.iflytek.cloud.a.f.c.g(c).onCompleted(new SpeechError(20010));
                }
                // Misplaced declaration of an exception variable
                catch (ArrayList arraylist) { }
            }
        }
        if (com.iflytek.cloud.a.f.c.m(c) != null)
        {
            com.iflytek.cloud.a.f.c.n(c).b(false);
        }
_L6:
        return;
_L4:
        com.iflytek.cloud.a.f.c.g(c).onBufferProgress(i, j, k, s);
          goto _L2
_L8:
        if (!com.iflytek.cloud.a.f.c.c(c).b())
        {
            throw new IOException();
        }
_L9:
        if (com.iflytek.cloud.a.f.c.g(c) == null) goto _L6; else goto _L10
_L10:
        if (com.iflytek.cloud.a.f.c.h(c))
        {
            Message.obtain(a, 6, null).sendToTarget();
            return;
        }
          goto _L11
    }
}
