// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.f;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SynthesizerListener;
import com.iflytek.cloud.a.d.e;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.c.b;

// Referenced classes of package com.iflytek.cloud.a.f:
//            d, e, f, a, 
//            h, g, b

public class c extends e
{
    public static interface a
    {

        public abstract void a();
    }


    public boolean f;
    public boolean g;
    public String h;
    public SpeechError i;
    private com.iflytek.cloud.c.c j;
    private b k;
    private SynthesizerListener l;
    private SynthesizerListener m;
    private a n;
    private int o;
    private boolean p;
    private com.iflytek.cloud.a.f.b q;
    private com.iflytek.cloud.c.a r;
    private Handler s;
    private boolean t;

    public c(Context context)
    {
        super(context);
        j = null;
        k = null;
        l = null;
        m = null;
        n = null;
        o = 0;
        p = false;
        f = false;
        g = false;
        h = null;
        i = null;
        q = new d(this);
        r = new com.iflytek.cloud.a.f.e(this);
        s = new f(this, Looper.getMainLooper());
        t = true;
    }

    static SynthesizerListener a(c c1)
    {
        return c1.l;
    }

    static Handler b(c c1)
    {
        return c1.s;
    }

    static b c(c c1)
    {
        return c1.k;
    }

    static void d(c c1)
    {
        c1.j();
    }

    static a e(c c1)
    {
        return c1.n;
    }

    static com.iflytek.cloud.c.c f(c c1)
    {
        return c1.j;
    }

    static SynthesizerListener g(c c1)
    {
        return c1.m;
    }

    static boolean h(c c1)
    {
        return c1.t;
    }

    static com.iflytek.cloud.a.d.a i(c c1)
    {
        return c1.d;
    }

    static com.iflytek.cloud.a.d.a j(c c1)
    {
        return c1.d;
    }

    private void j()
    {
        if (!p && j != null && k.a(o))
        {
            p = true;
            com.iflytek.cloud.a.g.a.b.a("QTTSOnPlayBegin", null);
            j.a(k, r);
            if (l != null)
            {
                Message.obtain(s, 1).sendToTarget();
            }
        }
    }

    static com.iflytek.cloud.a.d.a k(c c1)
    {
        return c1.d;
    }

    static com.iflytek.cloud.a.d.a l(c c1)
    {
        return c1.d;
    }

    static com.iflytek.cloud.a.d.a m(c c1)
    {
        return c1.d;
    }

    static com.iflytek.cloud.a.d.a n(c c1)
    {
        return c1.d;
    }

    public void a(SynthesizerListener synthesizerlistener)
    {
        l = synthesizerlistener;
    }

    public void a(a a1)
    {
        n = a1;
    }

    public void a(String s1, com.iflytek.cloud.b.a a1)
    {
        setParameter(a1);
        h = s1;
    }

    public void a(String s1, com.iflytek.cloud.b.a a1, SynthesizerListener synthesizerlistener, boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        int i1;
        boolean flag1;
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("tts start:").append(System.currentTimeMillis()).toString());
        l = synthesizerlistener;
        h = s1;
        setParameter(a1);
        i1 = a1.a("stream_type", 3);
        flag1 = a1.a("request_audio_focus", true);
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_84;
        }
        j = new com.iflytek.cloud.c.c(a, i1, flag1);
        d = new com.iflytek.cloud.a.f.a(a, a1, a("tts"));
        synthesizerlistener = a1.e("tts_audio_path");
        k = new b(a, d.q(), synthesizerlistener);
        k.a(s1);
        o = a1.a("tts_buffer_time", 0);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("minPlaySec:").append(o).toString());
        p = false;
        ((com.iflytek.cloud.a.f.a)d).a(s1, q);
        f = true;
        this;
        JVM INSTR monitorexit ;
        return;
        s1;
        throw s1;
    }

    public void a(String s1, String s2, com.iflytek.cloud.b.a a1, SynthesizerListener synthesizerlistener)
    {
        t = a1.a("message_main_thread", true);
        m = synthesizerlistener;
        d = new com.iflytek.cloud.a.f.a(a, a1, a("tts"));
        k = new b(a, d.q(), s2);
        k.a(s1);
        s2 = new h(this, new g(this, Looper.getMainLooper()), s2);
        ((com.iflytek.cloud.a.f.a)d).a(s1, s2);
    }

    public void cancel(boolean flag)
    {
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("SpeakSession cancel notifyError:").append(flag).toString());
        if (h())
        {
            if (l != null)
            {
                l.onEvent(21002, 0, 0, null);
            }
            if (m != null)
            {
                m.onEvent(21002, 0, 0, null);
            }
            if (flag)
            {
                if (l != null)
                {
                    com.iflytek.cloud.a.g.a.a.a("tts-onCompleted-cancel");
                    l.onCompleted(new SpeechError(20017));
                }
                if (m != null)
                {
                    m.onCompleted(new SpeechError(20017));
                }
            }
        }
        l = null;
        m = null;
        super.cancel(false);
        if (j != null)
        {
            j.e();
        }
    }

    public boolean d()
    {
        return super.d();
    }

    public boolean destroy()
    {
        synchronized (c)
        {
            cancel(false);
        }
        return true;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void e()
    {
        if (g)
        {
            return;
        } else
        {
            a(h, b, ((SynthesizerListener) (null)), false);
            return;
        }
    }

    public int f()
    {
        if (k != null && j != null)
        {
            return j.a();
        } else
        {
            return 4;
        }
    }

    public void g()
    {
        if (k != null && j != null)
        {
            j.c();
        }
    }

    public boolean h()
    {
        while (d() || f() != 4 && f() != 0) 
        {
            return true;
        }
        return false;
    }

    public void i()
    {
        if (k != null && j != null)
        {
            j.d();
            return;
        } else
        {
            j = new com.iflytek.cloud.c.c(a);
            j();
            return;
        }
    }
}
