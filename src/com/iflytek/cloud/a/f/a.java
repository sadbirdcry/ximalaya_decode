// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.f;

import android.content.Context;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.g.a.b;
import com.iflytek.cloud.a.g.h;
import java.util.ArrayList;

// Referenced classes of package com.iflytek.cloud.a.f:
//            i, b

public class a extends com.iflytek.cloud.a.d.a
{

    public static int a = 0;
    public static int b = 0;
    private String c;
    private i d;
    private com.iflytek.cloud.a.f.b e;
    private ArrayList f;
    private int g;
    private int h;
    private StringBuilder i;
    private boolean j;
    private int k;
    private int l;

    public a(Context context, com.iflytek.cloud.b.a a1, HandlerThread handlerthread)
    {
        super(context, handlerthread);
        c = "";
        d = null;
        e = null;
        f = null;
        g = 0;
        h = 0;
        i = null;
        j = false;
        k = 0;
        l = 0;
        d = new i();
        f = new ArrayList();
        i = new StringBuilder();
        a(a1);
    }

    private void f()
    {
        int i1 = Math.min(99, (g * 100) / c.length());
        e.a(f, i1, h, g, i.toString());
        i.delete(0, i.length());
        f = new ArrayList();
        h = Math.min(g + 1, c.length() - 1);
    }

    protected void a()
        throws Exception
    {
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("tts msg start:").append(System.currentTimeMillis()).toString());
        String s = t().e("engine_type");
        boolean flag = t().a("net_check", true);
        if ("cloud".equals(s) && flag)
        {
            com.iflytek.cloud.a.g.h.a(r);
        }
        a(1);
    }

    protected void a(Message message)
        throws Exception
    {
        switch (message.what)
        {
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        default:
            return;

        case 0: // '\0'
            a();
            return;

        case 1: // '\001'
            b();
            return;

        case 5: // '\005'
            d();
            break;
        }
    }

    protected void a(SpeechError speecherror)
    {
label0:
        {
            a = d.b("upflow");
            b = d.b("downflow");
            e();
            com.iflytek.cloud.a.g.a.b.a("QTTSSessionEnd", null);
            if (e == null)
            {
                d.a("user abort");
            } else
            if (speecherror != null)
            {
                d.a((new StringBuilder()).append("error").append(speecherror.getErrorCode()).toString());
                com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("QTts Error Code = ").append(speecherror.getErrorCode()).toString());
            } else
            {
                d.a("success");
            }
            super.a(speecherror);
            if (e != null)
            {
                if (!s)
                {
                    break label0;
                }
                com.iflytek.cloud.a.g.a.a.a("MscSynthesizer#onCancel");
            }
            return;
        }
        com.iflytek.cloud.a.g.a.a.a("MscSynthesizer#onEnd");
        e.a(speecherror);
    }

    public void a(String s, com.iflytek.cloud.a.f.b b1)
    {
        c = s;
        e = b1;
        if (s == null || TextUtils.isEmpty(s))
        {
            s = new SpeechError(20009);
            e.a(s);
            return;
        } else
        {
            j = t().a("tts_spell_info", false);
            c();
            return;
        }
    }

    protected void b()
        throws Exception
    {
        com.iflytek.cloud.a.g.a.b.a("QTTSSessionBegin", null);
        int i1 = d.a(r, null, this);
        com.iflytek.cloud.a.g.a.b.a("QTTSSessionBeginEnd", null);
        if (i1 == 0)
        {
            com.iflytek.cloud.a.g.a.b.a("QTTSTextPut", null);
            com.iflytek.cloud.a.g.a.b.b("QTTSTextLen", (new StringBuilder()).append("").append(c.length()).toString());
            byte abyte0[] = c.getBytes(o());
            d.a(abyte0);
            a(com.iflytek.cloud.a.d.b.d);
            a(5);
            m();
        } else
        {
            l = l + 1;
            if (l > 40)
            {
                throw new SpeechError(i1);
            }
            if (r())
            {
                a(1, com.iflytek.cloud.a.d.a.b, false, 15);
                return;
            }
        }
    }

    public void b(boolean flag)
    {
        if (flag && r() && e != null)
        {
            e.a(new SpeechError(20017));
        }
        super.b(flag);
    }

    protected void d()
        throws Exception
    {
        if (!d.d())
        {
            byte abyte0[] = d.a();
            if (abyte0 != null && e != null)
            {
                com.iflytek.cloud.a.g.a.b.a("QTTSAudioGet", (new StringBuilder()).append("").append(abyte0.length).toString());
                int i1 = d.b() / 2 - 1;
                if (j)
                {
                    String s = d.c();
                    if (!TextUtils.isEmpty(s))
                    {
                        i.append(s);
                        i.append("#\n");
                    }
                }
                if (k > 0 && g != 0 && i1 != g && f.size() > 0)
                {
                    f();
                }
                m();
                g = i1;
                f.add(abyte0);
                if (k <= 0)
                {
                    f();
                }
                a(5, com.iflytek.cloud.a.d.a.b, false, 0);
            } else
            {
                a(5, com.iflytek.cloud.a.d.a.b, false, 10);
            }
            e();
            return;
        }
        if (e != null)
        {
            e.a(f, 100, h, c.length() - 1, i.toString());
        }
        c(null);
    }

    public String e()
    {
        return d.e();
    }

    protected void h()
    {
        if ("local".equals(t().e("engine_type")))
        {
            k = t().a("tts_buffer_time", 100);
        } else
        {
            k = t().a("tts_buffer_time", 2000);
        }
        if (k <= 0)
        {
            com.iflytek.cloud.a.g.a.b.a("QTTSRealTime", null);
        }
        super.h();
    }

    public String o()
    {
        return t().b("text_encoding", "unicode");
    }

}
