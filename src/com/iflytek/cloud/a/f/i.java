// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.f;

import android.content.Context;
import android.text.TextUtils;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.d.a;
import com.iflytek.cloud.a.d.b;
import com.iflytek.cloud.b.c;
import com.iflytek.msc.MSC;
import com.iflytek.msc.MSCSessionInfo;
import java.io.UnsupportedEncodingException;

public class i extends b
{

    private MSCSessionInfo c;

    public i()
    {
        c = new MSCSessionInfo();
    }

    public int a(Context context, String s, a a1)
        throws SpeechError, UnsupportedEncodingException
    {
        long l;
        a = null;
        context = com.iflytek.cloud.b.c.d(context, a1);
        l = System.currentTimeMillis();
        context = context.getBytes(a1.n());
        com/iflytek/cloud/a/f/i;
        JVM INSTR monitorenter ;
        a = MSC.QTTSSessionBegin(context, c);
        com/iflytek/cloud/a/f/i;
        JVM INSTR monitorexit ;
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("QTTSSessionBegin leave:").append(System.currentTimeMillis() - l).append(" ErrorCode:").append(c.errorcode).toString());
        int j = c.errorcode;
        if (j != 0 && j != 10129 && j != 10113 && j != 10132)
        {
            throw new SpeechError(j);
        } else
        {
            return j;
        }
        context;
        com/iflytek/cloud/a/f/i;
        JVM INSTR monitorexit ;
        throw context;
    }

    public void a(String s)
    {
        if (a == null)
        {
            return;
        }
        String s1 = s;
        if (TextUtils.isEmpty(s))
        {
            s1 = "unknown";
        }
        com.iflytek.cloud.a.g.a.a.a("QTTSSessionEnd enter");
        int j = MSC.QTTSSessionEnd(a, s1.getBytes());
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("QTTSSessionEnd leavel:").append(j).toString());
        a = null;
        b = null;
    }

    public void a(byte abyte0[])
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        int j;
        com.iflytek.cloud.a.g.a.a.a("QTTSTextPut enter");
        j = MSC.QTTSTextPut(a, abyte0);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("QTTSTextPut leavel:").append(j).toString());
        if (j == 0)
        {
            break MISSING_BLOCK_LABEL_56;
        }
        throw new SpeechError(j);
        abyte0;
        this;
        JVM INSTR monitorexit ;
        throw abyte0;
        this;
        JVM INSTR monitorexit ;
    }

    public byte[] a()
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        if (a == null)
        {
            throw new SpeechError(20003);
        }
        break MISSING_BLOCK_LABEL_25;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        byte abyte0[];
        StringBuilder stringbuilder;
        com.iflytek.cloud.a.g.a.a.a("QTTSAudioGet enter");
        abyte0 = MSC.QTTSAudioGet(a, c);
        stringbuilder = (new StringBuilder()).append("QTTSAudioGet leavel:").append(c.errorcode).append("value len = ");
        if (abyte0 != null) goto _L2; else goto _L1
_L1:
        int j = 0;
_L4:
        com.iflytek.cloud.a.g.a.a.a(stringbuilder.append(j).toString());
        j = c.errorcode;
        if (j == 0)
        {
            break; /* Loop/switch isn't completed */
        }
        throw new SpeechError(j);
_L2:
        j = abyte0.length;
        if (true) goto _L4; else goto _L3
_L3:
        this;
        JVM INSTR monitorexit ;
        return abyte0;
    }

    public int b()
    {
        int j;
        try
        {
            j = (new com.iflytek.cloud.b.a(new String(MSC.QTTSAudioInfo(a)), (String[][])null)).a("ced", 0);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            return 0;
        }
        return j;
    }

    public int b(String s)
    {
        boolean flag = false;
        this;
        JVM INSTR monitorenter ;
        char ac[] = a;
        if (ac != null) goto _L2; else goto _L1
_L1:
        int j = ((flag) ? 1 : 0);
_L4:
        this;
        JVM INSTR monitorexit ;
        return j;
_L2:
        s = c(s);
        j = ((flag) ? 1 : 0);
        try
        {
            if (!TextUtils.isEmpty(s))
            {
                j = Integer.parseInt(new String(s));
            }
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            j = ((flag) ? 1 : 0);
        }
        finally
        {
            this;
        }
        continue; /* Loop/switch isn't completed */
        throw s;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public String c()
    {
        String s = new String(MSC.QTTSAudioInfo(a));
        String s1 = new String(s.getBytes("iso8859-1"), "gb2312");
        return s1;
        Exception exception;
        exception;
        s = "";
_L2:
        exception.printStackTrace();
        return s;
        exception;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public String c(String s)
    {
        Object obj = null;
        this;
        JVM INSTR monitorenter ;
        char ac[] = a;
        if (ac != null) goto _L2; else goto _L1
_L1:
        String s1 = obj;
_L4:
        this;
        JVM INSTR monitorexit ;
        return s1;
_L2:
        s1 = obj;
        try
        {
            if (MSC.QTTSGetParam(a, s.getBytes(), c) == 0)
            {
                s1 = new String(c.buffer);
            }
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s1 = obj;
        }
        finally
        {
            this;
        }
        continue; /* Loop/switch isn't completed */
        throw s;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public boolean d()
    {
        this;
        JVM INSTR monitorenter ;
        int j = c.sesstatus;
        boolean flag;
        if (2 == j)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    protected String e()
    {
        if (b == null)
        {
            b = c("sid");
        }
        return b;
    }
}
