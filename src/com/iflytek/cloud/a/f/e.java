// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.f;

import android.os.Message;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.g.a.b;
import com.iflytek.cloud.c.c;

// Referenced classes of package com.iflytek.cloud.a.f:
//            c

final class e
    implements com.iflytek.cloud.c.c.a
{

    final com.iflytek.cloud.a.f.c a;

    e(com.iflytek.cloud.a.f.c c1)
    {
        a = c1;
        super();
    }

    public void a()
    {
        if (com.iflytek.cloud.a.f.c.a(a) != null)
        {
            Message.obtain(com.iflytek.cloud.a.f.c.b(a), 3).sendToTarget();
            com.iflytek.cloud.a.g.a.b.a("QTTSOnPlayPause", null);
        }
    }

    public void a(int i, int j, int k)
    {
        Message.obtain(com.iflytek.cloud.a.f.c.b(a), 5, i, j, Integer.valueOf(k)).sendToTarget();
    }

    public void a(SpeechError speecherror)
    {
        Message.obtain(com.iflytek.cloud.a.f.c.b(a), 6, speecherror).sendToTarget();
        if (com.iflytek.cloud.a.f.c.f(a) != null)
        {
            com.iflytek.cloud.a.f.c.f(a).e();
        }
        a.cancel(false);
    }

    public void b()
    {
        if (com.iflytek.cloud.a.f.c.a(a) != null)
        {
            Message.obtain(com.iflytek.cloud.a.f.c.b(a), 4).sendToTarget();
            com.iflytek.cloud.a.g.a.b.a("QTTSOnPlayResume", null);
        }
    }

    public void c()
    {
        Message.obtain(com.iflytek.cloud.a.f.c.b(a), 6, null).sendToTarget();
        com.iflytek.cloud.a.g.a.b.a("QTTSOnPlayStop", null);
    }
}
