// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.f;

import android.os.Bundle;
import android.os.Message;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.c.b;
import com.iflytek.cloud.c.c;
import java.io.IOException;
import java.util.ArrayList;

// Referenced classes of package com.iflytek.cloud.a.f:
//            b, c

final class d
    implements com.iflytek.cloud.a.f.b
{

    final com.iflytek.cloud.a.f.c a;

    d(com.iflytek.cloud.a.f.c c1)
    {
        a = c1;
        super();
    }

    public void a(SpeechError speecherror)
    {
        a.i = speecherror;
        if (speecherror == null)
        {
            a.g = true;
            com.iflytek.cloud.a.f.c.c(a).b();
            if (c.e(a) != null)
            {
                c.e(a).a();
                com.iflytek.cloud.a.g.a.a.a("onCompleted NextSession pause");
            }
        }
        if (com.iflytek.cloud.a.f.c.a(a) != null && speecherror != null)
        {
            Message.obtain(com.iflytek.cloud.a.f.c.b(a), 6, speecherror).sendToTarget();
            if (c.f(a) != null)
            {
                c.f(a).e();
            }
        }
    }

    public void a(ArrayList arraylist, int i, int j, int k, String s)
    {
        Bundle bundle = new Bundle();
        bundle.putInt("percent", i);
        bundle.putInt("begpos", j);
        bundle.putInt("endpos", k);
        bundle.putString("spellinfo", s);
        if (com.iflytek.cloud.a.f.c.a(a) != null)
        {
            Message.obtain(com.iflytek.cloud.a.f.c.b(a), 2, bundle).sendToTarget();
        }
        try
        {
            com.iflytek.cloud.a.f.c.c(a).a(arraylist, i, j, k);
            c.d(a);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (ArrayList arraylist)
        {
            arraylist.printStackTrace();
        }
        a.i = new SpeechError(20010);
        Message.obtain(com.iflytek.cloud.a.f.c.b(a), 6, a.i).sendToTarget();
        a.cancel(false);
    }
}
