// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.f;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SynthesizerListener;
import com.iflytek.cloud.a.g.a.a;

// Referenced classes of package com.iflytek.cloud.a.f:
//            c

final class f extends Handler
{

    final c a;

    f(c c1, Looper looper)
    {
        a = c1;
        super(looper);
    }

    public void handleMessage(Message message)
    {
        try
        {
            if (com.iflytek.cloud.a.f.c.a(a) == null)
            {
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (Message message)
        {
            com.iflytek.cloud.a.g.a.a.b((new StringBuilder()).append("SpeakSession mUiHandler error:").append(message).toString());
            return;
        }
        message.what;
        JVM INSTR tableswitch 1 6: default 273
    //                   1 52
    //                   2 94
    //                   3 164
    //                   4 182
    //                   5 200
    //                   6 248;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7
_L1:
        break MISSING_BLOCK_LABEL_273;
_L2:
        com.iflytek.cloud.a.g.a.a.a("tts-onSpeakBegin");
        com.iflytek.cloud.a.f.c.a(a).onSpeakBegin();
        return;
_L3:
        message = (Bundle)message.obj;
        int i = message.getInt("percent");
        int k = message.getInt("begpos");
        int l = message.getInt("endpos");
        message = message.getString("spellinfo");
        if (com.iflytek.cloud.a.f.c.a(a) != null)
        {
            com.iflytek.cloud.a.g.a.a.a("tts-onBufferProgress");
            com.iflytek.cloud.a.f.c.a(a).onBufferProgress(i, k, l, message);
            return;
        }
        break; /* Loop/switch isn't completed */
_L4:
        com.iflytek.cloud.a.g.a.a.a("tts-onSpeakPaused");
        com.iflytek.cloud.a.f.c.a(a).onSpeakPaused();
        return;
_L5:
        com.iflytek.cloud.a.g.a.a.a("tts-onSpeakResumed");
        com.iflytek.cloud.a.f.c.a(a).onSpeakResumed();
        return;
_L6:
        int j = ((Integer)message.obj).intValue();
        if (com.iflytek.cloud.a.f.c.a(a) != null)
        {
            com.iflytek.cloud.a.g.a.a.a("tts-onSpeakProgress");
            com.iflytek.cloud.a.f.c.a(a).onSpeakProgress(message.arg1, message.arg2, j);
            return;
        }
        break; /* Loop/switch isn't completed */
_L7:
        com.iflytek.cloud.a.g.a.a.a("tts-onCompleted");
        com.iflytek.cloud.a.f.c.a(a).onCompleted((SpeechError)message.obj);
        return;
    }
}
