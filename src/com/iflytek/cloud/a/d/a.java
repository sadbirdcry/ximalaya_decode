// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.d;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import com.iflytek.cloud.SpeechError;
import java.io.IOException;

public abstract class com.iflytek.cloud.a.d.a extends Handler
{
    protected static final class a extends Enum
    {

        public static final a a;
        public static final a b;
        public static final a c;
        private static final a d[];

        public static a valueOf(String s1)
        {
            return (a)Enum.valueOf(com/iflytek/cloud/a/d/a$a, s1);
        }

        public static a[] values()
        {
            return (a[])d.clone();
        }

        static 
        {
            a = new a("max", 0);
            b = new a("normal", 1);
            c = new a("min", 2);
            d = (new a[] {
                a, b, c
            });
        }

        private a(String s1, int j)
        {
            super(s1, j);
        }
    }

    protected static final class b extends Enum
    {

        public static final b a;
        public static final b b;
        public static final b c;
        public static final b d;
        public static final b e;
        public static final b f;
        private static final b g[];

        public static b valueOf(String s1)
        {
            return (b)Enum.valueOf(com/iflytek/cloud/a/d/a$b, s1);
        }

        public static b[] values()
        {
            return (b[])g.clone();
        }

        static 
        {
            a = new b("init", 0);
            b = new b("start", 1);
            c = new b("recording", 2);
            d = new b("waitresult", 3);
            e = new b("exiting", 4);
            f = new b("exited", 5);
            g = (new b[] {
                a, b, c, d, e, f
            });
        }

        private b(String s1, int j)
        {
            super(s1, j);
        }
    }


    private com.iflytek.cloud.b.a a;
    private volatile b b;
    private HandlerThread c;
    protected int p;
    protected int q;
    protected Context r;
    protected volatile boolean s;
    protected long t;
    protected int u;

    public com.iflytek.cloud.a.d.a(Context context)
    {
        p = 60000;
        q = 16000;
        r = null;
        a = new com.iflytek.cloud.b.a();
        s = false;
        b = b.a;
        t = 0L;
        u = 20000;
        r = context;
        s = false;
    }

    public com.iflytek.cloud.a.d.a(Context context, HandlerThread handlerthread)
    {
        super(handlerthread.getLooper());
        p = 60000;
        q = 16000;
        r = null;
        a = new com.iflytek.cloud.b.a();
        s = false;
        b = b.a;
        t = 0L;
        u = 20000;
        c = handlerthread;
        r = context;
        s = false;
    }

    private void a()
    {
        if (c.isAlive())
        {
            u();
            c.quit();
            c = null;
        }
    }

    protected void a(int j)
    {
        a(obtainMessage(j), a.b, false, 0);
    }

    protected void a(int j, a a1, boolean flag, int k)
    {
        a(obtainMessage(j), a1, flag, k);
    }

    protected void a(Message message)
        throws Exception
    {
    }

    protected void a(Message message, a a1, boolean flag, int j)
    {
        if (s() == b.f || s() == b.e)
        {
            return;
        }
        message.what;
        JVM INSTR lookupswitch 3: default 60
    //                   0: 79
    //                   3: 89
    //                   21: 99;
           goto _L1 _L2 _L3 _L4
_L1:
        break; /* Loop/switch isn't completed */
_L4:
        break MISSING_BLOCK_LABEL_99;
_L5:
        if (a1 == a.a && j <= 0)
        {
            sendMessageAtFrontOfQueue(message);
            return;
        } else
        {
            sendMessageDelayed(message, j);
            return;
        }
_L2:
        a(b.b);
          goto _L5
_L3:
        a(b.d);
          goto _L5
        a(b.e);
          goto _L5
    }

    protected void a(SpeechError speecherror)
    {
        a(b.f);
        u();
    }

    protected void a(b b1)
    {
        this;
        JVM INSTR monitorenter ;
        b b2;
        b b3;
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("curStatus=").append(b).append(",setStatus=").append(b1).toString());
        b2 = b;
        b3 = b.f;
        if (b2 != b3) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (b != b.e || b1 == b.f)
        {
            com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("setStatus success=").append(b1).toString());
            b = b1;
            t = SystemClock.elapsedRealtime();
        }
        if (true) goto _L1; else goto _L3
_L3:
        b1;
        throw b1;
    }

    protected void a(com.iflytek.cloud.b.a a1)
    {
        a = a1.b();
        h();
    }

    public void b(boolean flag)
    {
        s = true;
        u();
        c(null);
    }

    protected void c()
    {
        a(0, a.a, false, 0);
    }

    protected void c(SpeechError speecherror)
    {
        this;
        JVM INSTR monitorenter ;
        if (speecherror == null)
        {
            break MISSING_BLOCK_LABEL_10;
        }
        u();
        d(obtainMessage(21, speecherror));
        this;
        JVM INSTR monitorexit ;
        return;
        speecherror;
        throw speecherror;
    }

    protected void d(Message message)
    {
        a(message, a.b, false, 0);
    }

    protected void h()
    {
        u = a.a("timeout", u);
        q = a.a("sample_rate", q);
    }

    public void handleMessage(Message message)
    {
        message.what;
        JVM INSTR tableswitch 21 21: default 24
    //                   21 86;
           goto _L1 _L2
_L1:
        message.what;
        JVM INSTR tableswitch 8 8: default 309
    //                   8 102;
           goto _L3 _L4
_L3:
        a(message);
        if (false)
        {
            (new StringBuilder()).append(v()).append(" occur Error = ");
            throw new NullPointerException();
        }
_L5:
        return;
_L2:
        a((SpeechError)message.obj);
        a();
        return;
_L4:
        throw new SpeechError(20002);
        message;
        message.printStackTrace();
        message = new SpeechError(20010);
        if (message != null)
        {
            com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append(v()).append(" occur Error = ").append(message.toString()).toString());
            c(message);
            return;
        }
          goto _L5
        message;
        message.printStackTrace();
        if (message != null)
        {
            com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append(v()).append(" occur Error = ").append(message.toString()).toString());
            c(message);
            return;
        }
          goto _L5
        message;
        message.printStackTrace();
        message = new SpeechError(message);
        if (message == null) goto _L5; else goto _L6
_L6:
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append(v()).append(" occur Error = ").append(message.toString()).toString());
        c(message);
        return;
        message;
        if (false)
        {
            (new StringBuilder()).append(v()).append(" occur Error = ");
            throw new NullPointerException();
        } else
        {
            throw message;
        }
    }

    public boolean i()
    {
        return false;
    }

    protected void m()
    {
        removeMessages(8);
        a(8, a.b, false, u);
    }

    public String n()
    {
        return a.b("pte", "utf-8");
    }

    public String o()
    {
        return a.b("text_encoding", "utf-8");
    }

    public String p()
    {
        return a.b("rse", "utf-8");
    }

    public int q()
    {
        return q;
    }

    public boolean r()
    {
        return b != b.f && b != b.e && b != b.a;
    }

    protected b s()
    {
        this;
        JVM INSTR monitorenter ;
        b b1 = b;
        this;
        JVM INSTR monitorexit ;
        return b1;
        Exception exception;
        exception;
        throw exception;
    }

    public com.iflytek.cloud.b.a t()
    {
        return a;
    }

    protected void u()
    {
        com.iflytek.cloud.a.g.a.a.a("clear all message");
        for (int j = 0; j < 20; j++)
        {
            removeMessages(j);
        }

    }

    protected String v()
    {
        return getClass().toString();
    }
}
