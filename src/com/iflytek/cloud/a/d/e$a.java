// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.d;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechListener;

// Referenced classes of package com.iflytek.cloud.a.d:
//            e, f

protected class b
    implements SpeechListener
{

    final e a;
    private SpeechListener b;
    private Handler c;

    static SpeechListener a(stener stener)
    {
        return stener.b;
    }

    public void onBufferReceived(byte abyte0[])
    {
        abyte0 = c.obtainMessage(1, abyte0);
        c.sendMessage(abyte0);
    }

    public void onCompleted(SpeechError speecherror)
    {
        speecherror = c.obtainMessage(2, speecherror);
        c.sendMessage(speecherror);
    }

    public void onEvent(int i, Bundle bundle)
    {
        bundle = c.obtainMessage(0, i, 0, bundle);
        c.sendMessage(bundle);
    }

    public stener(e e1, SpeechListener speechlistener)
    {
        a = e1;
        super();
        b = null;
        c = new f(this, Looper.getMainLooper());
        b = speechlistener;
    }
}
