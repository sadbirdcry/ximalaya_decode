// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.d;

import android.os.SystemClock;
import android.text.TextUtils;
import com.iflytek.cloud.b.a;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class c
{

    JSONObject a;
    long b;
    long c;

    public c()
    {
        a = new JSONObject();
        b = 0L;
        c = 0L;
    }

    public String a()
    {
        this;
        JVM INSTR monitorenter ;
        String s = a.toString();
        this;
        JVM INSTR monitorexit ;
        return s;
        Exception exception;
        exception;
        throw exception;
    }

    public void a(a a1)
    {
        c = System.currentTimeMillis();
        b = SystemClock.elapsedRealtime();
        a("app_start", com.iflytek.cloud.a.g.c.a(c), false);
        a1 = a1.e("caller.appid");
        if (!TextUtils.isEmpty(a1))
        {
            a("app_caller_appid", ((String) (a1)), false);
        }
        a1 = com.iflytek.cloud.a.g.a.a(null).e("app.ver.code");
        if (!TextUtils.isEmpty(a1))
        {
            a("app_cver", ((String) (a1)), false);
        }
    }

    public void a(String s)
    {
        this;
        JVM INSTR monitorenter ;
        a(s, SystemClock.elapsedRealtime() - b, false);
        this;
        JVM INSTR monitorexit ;
        return;
        s;
        throw s;
    }

    public void a(String s, long l, boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag1 = TextUtils.isEmpty(s);
        if (!flag1) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (flag)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        a.put(s, l);
          goto _L1
        s;
        s.printStackTrace();
          goto _L1
        s;
        throw s;
        JSONArray jsonarray1 = a.optJSONArray(s);
        JSONArray jsonarray = jsonarray1;
        if (jsonarray1 != null) goto _L4; else goto _L3
_L3:
        jsonarray = new JSONArray();
        a.put(s, jsonarray);
_L4:
        if (jsonarray == null) goto _L1; else goto _L5
_L5:
        jsonarray.put(l);
          goto _L1
    }

    public void a(String s, String s1, boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        if (TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        boolean flag1 = TextUtils.isEmpty(s1);
        if (!flag1) goto _L3; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        if (flag)
        {
            break MISSING_BLOCK_LABEL_53;
        }
        a.put(s, s1);
          goto _L2
        s;
        s.printStackTrace();
          goto _L2
        s;
        throw s;
        JSONArray jsonarray1 = a.optJSONArray(s);
        JSONArray jsonarray = jsonarray1;
        if (jsonarray1 != null) goto _L5; else goto _L4
_L4:
        jsonarray = new JSONArray();
        a.put(s, jsonarray);
_L5:
        if (jsonarray == null) goto _L2; else goto _L6
_L6:
        jsonarray.put(s1);
          goto _L2
    }
}
