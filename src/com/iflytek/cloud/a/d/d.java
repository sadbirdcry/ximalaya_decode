// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.d;

import android.text.TextUtils;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.b.a;
import com.iflytek.msc.MSC;
import com.iflytek.speech.ISpeechModule;

public abstract class d
{
    public static final class a extends Enum
    {

        public static final a a;
        public static final a b;
        public static final a c;
        private static final a d[];

        public static a valueOf(String s)
        {
            return (a)Enum.valueOf(com/iflytek/cloud/a/d/d$a, s);
        }

        public static a[] values()
        {
            return (a[])d.clone();
        }

        static 
        {
            a = new a("MSC", 0);
            b = new a("PLUS", 1);
            c = new a("AUTO", 2);
            d = (new a[] {
                a, b, c
            });
        }

        private a(String s, int i)
        {
            super(s, i);
        }
    }


    protected com.iflytek.cloud.b.a b;

    public d()
    {
        b = new com.iflytek.cloud.b.a();
    }

    protected a a(String s, ISpeechModule ispeechmodule)
    {
        Object obj = SpeechUtility.getUtility();
        if (obj == null)
        {
            return com.iflytek.cloud.a.d.a.a;
        }
        if (((SpeechUtility) (obj)).getEngineMode() != a.c)
        {
            return ((SpeechUtility) (obj)).getEngineMode();
        }
        if (!MSC.isLoaded())
        {
            return a.b;
        }
        obj = getParameter("engine_type");
        if ("local".equals(obj))
        {
            return a.b;
        }
        if ("mix".equals(obj))
        {
            if (ispeechmodule == null || !ispeechmodule.isAvailable())
            {
                return com.iflytek.cloud.a.d.a.a;
            } else
            {
                return a.b;
            }
        }
        if ("tts".equals(s) && ispeechmodule != null && ispeechmodule.isAvailable())
        {
            return a.b;
        } else
        {
            return com.iflytek.cloud.a.d.a.a;
        }
    }

    public String getParameter(String s)
    {
        if ("params".equals(s))
        {
            return b.toString();
        } else
        {
            return b.e(s);
        }
    }

    public boolean setParameter(com.iflytek.cloud.b.a a1)
    {
        b = a1.b();
        return true;
    }

    public boolean setParameter(String s, String s1)
    {
        if (TextUtils.isEmpty(s))
        {
            return false;
        }
        if (s.equals("params"))
        {
            if (TextUtils.isEmpty(s1))
            {
                b.a();
                return true;
            } else
            {
                b.b(s1);
                return true;
            }
        }
        if (TextUtils.isEmpty(s1))
        {
            return b.c(s).booleanValue();
        } else
        {
            b.a(s, s1);
            return true;
        }
    }
}
