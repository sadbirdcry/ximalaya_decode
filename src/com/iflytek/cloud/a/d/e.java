// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.d;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechListener;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.a.g.b;

// Referenced classes of package com.iflytek.cloud.a.d:
//            d, a, f

public abstract class e extends d
{
    protected class a
        implements SpeechListener
    {

        final e a;
        private SpeechListener b;
        private Handler c;

        static SpeechListener a(a a1)
        {
            return a1.b;
        }

        public void onBufferReceived(byte abyte0[])
        {
            abyte0 = c.obtainMessage(1, abyte0);
            c.sendMessage(abyte0);
        }

        public void onCompleted(SpeechError speecherror)
        {
            speecherror = c.obtainMessage(2, speecherror);
            c.sendMessage(speecherror);
        }

        public void onEvent(int i, Bundle bundle)
        {
            bundle = c.obtainMessage(0, i, 0, bundle);
            c.sendMessage(bundle);
        }

        public a(SpeechListener speechlistener)
        {
            a = e.this;
            super();
            b = null;
            c = new f(this, Looper.getMainLooper());
            b = speechlistener;
        }
    }


    protected Context a;
    protected Object c;
    protected volatile com.iflytek.cloud.a.d.a d;
    protected volatile HandlerThread e;

    protected e(Context context)
    {
        a = null;
        c = new Object();
        d = null;
        e = null;
        if (context != null)
        {
            com.iflytek.cloud.a.g.b.a(context.getApplicationContext());
            a = context.getApplicationContext();
            try
            {
                b();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context.printStackTrace();
            }
            return;
        } else
        {
            a = null;
            return;
        }
    }

    protected HandlerThread a(String s)
    {
        e = new HandlerThread(s);
        e.start();
        return e;
    }

    protected boolean a_()
    {
        if (e != null && e.isAlive())
        {
            HandlerThread handlerthread = e;
            e = null;
            handlerthread.interrupt();
        }
        return true;
    }

    protected void b()
        throws Exception
    {
    }

    protected String c()
    {
        return getClass().toString();
    }

    public void cancel(boolean flag)
    {
        if (d != null)
        {
            d.b(flag);
        }
    }

    protected boolean d()
    {
        return d != null && d.r();
    }

    public boolean destroy()
    {
label0:
        {
            synchronized (c)
            {
                if (!d())
                {
                    break label0;
                }
                d.b(false);
            }
            return false;
        }
        boolean flag;
        flag = a_();
        com.iflytek.cloud.a.g.a.a.c((new StringBuilder()).append(c()).append("destory =").append(flag).toString());
        obj;
        JVM INSTR monitorexit ;
        return flag;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    protected void finalize()
        throws Throwable
    {
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append(c()).append(" finalize called").toString());
        super.finalize();
    }

    public int getSampleRate()
    {
        return b.a("sample_rate", 16000);
    }
}
