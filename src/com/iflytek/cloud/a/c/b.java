// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.c;

import android.content.Context;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Message;
import android.text.TextUtils;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.WakeuperListener;
import com.iflytek.cloud.WakeuperResult;
import com.iflytek.cloud.a.d.a;
import com.iflytek.cloud.c.f;
import com.iflytek.msc.MSC;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

// Referenced classes of package com.iflytek.cloud.a.c:
//            a

public class b extends a
    implements com.iflytek.cloud.c.f.a
{

    public String a;
    protected volatile WakeuperListener b;
    protected boolean c;
    protected int d;
    protected com.iflytek.cloud.a.c.a e;
    protected f f;
    protected ArrayList g;
    public boolean h;
    public boolean i;
    private int j;

    public b(Context context, com.iflytek.cloud.b.a a1, HandlerThread handlerthread)
    {
        super(context, handlerthread);
        b = null;
        c = false;
        d = 1;
        e = new com.iflytek.cloud.a.c.a();
        f = null;
        g = null;
        h = false;
        i = false;
        j = 0;
        c = false;
        a(a1);
        g = new ArrayList();
    }

    private void a(boolean flag, byte abyte0[], int l)
        throws SpeechError, UnsupportedEncodingException
    {
        if (l != 1) goto _L2; else goto _L1
_L1:
        if (abyte0 != null && abyte0.length > 0)
        {
            abyte0 = new String(abyte0, p());
        } else
        {
            if (g.size() <= 0)
            {
                abyte0 = t().e("local_grammar");
                if (!TextUtils.isEmpty(abyte0) && !"sms.irf".equals(abyte0))
                {
                    throw new SpeechError(20005);
                } else
                {
                    throw new SpeechError(10118);
                }
            }
            abyte0 = "";
        }
        g.add(abyte0);
        if (b != null && r())
        {
            Bundle bundle = new Bundle();
            bundle.putParcelable("rec_result", new RecognizerResult(abyte0));
            abyte0 = b;
            if (flag)
            {
                l = 1;
            } else
            {
                l = 0;
            }
            abyte0.onEvent(22001, l, 0, bundle);
        }
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("msc result time:").append(System.currentTimeMillis()).toString());
_L4:
        if (flag)
        {
            c(((SpeechError) (null)));
        }
        return;
_L2:
        if (l != 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (abyte0 == null || abyte0.length <= 0)
        {
            break; /* Loop/switch isn't completed */
        }
        abyte0 = new String(abyte0, "utf-8");
        if (b != null && r())
        {
            abyte0 = new WakeuperResult(abyte0);
            b.onResult(abyte0);
        }
        if (true) goto _L4; else goto _L3
_L3:
        throw new SpeechError(10118);
    }

    private void f()
        throws SpeechError, IOException, InterruptedException
    {
        com.iflytek.cloud.a.g.a.a.a("recording stop");
        if (!a.equals("enroll"))
        {
            g();
        }
        e.a();
    }

    private void g()
    {
        if (f != null)
        {
            f.a(t().a("record_force_stop", false));
            f = null;
        }
    }

    int MsgProcCallBack(char ac[], int l, int i1, int j1, byte abyte0[])
    {
        if (abyte0 != null)
        {
            com.iflytek.cloud.a.g.a.a.a("MscWakeuper", (new StringBuilder()).append("msg:").append(l).append("param1:").append(i1).append("param2:").append(j1).append("result:").append(new String(abyte0)).toString());
        } else
        {
            com.iflytek.cloud.a.g.a.a.a("MscWakeuper", (new StringBuilder()).append("msg:").append(l).append("param1:").append(i1).append("param2:").append(j1).append("result:null").toString());
        }
        l;
        JVM INSTR tableswitch 1 4: default 96
    //                   1 149
    //                   2 228
    //                   3 242
    //                   4 284;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        return 0;
_L2:
        h = true;
        if (i || a.equals("oneshot"))
        {
            ac = obtainMessage(4, 0, 0, abyte0);
        } else
        {
            ac = obtainMessage(4, 5, 0, abyte0);
        }
        if (hasMessages(4))
        {
            a(ac, com.iflytek.cloud.a.d.a.a.b, false, 0);
            return 0;
        } else
        {
            a(ac, com.iflytek.cloud.a.d.a.a.a, false, 0);
            return 0;
        }
_L3:
        b(new SpeechError(i1));
        return 0;
_L4:
        ac = obtainMessage(4, i1, 1, abyte0);
        if (hasMessages(4))
        {
            a(ac, com.iflytek.cloud.a.d.a.a.b, false, 0);
            return 0;
        } else
        {
            a(ac, com.iflytek.cloud.a.d.a.a.a, false, 0);
            return 0;
        }
_L5:
        if (i1 == 3)
        {
            e();
            return 0;
        }
        if (true) goto _L1; else goto _L6
_L6:
    }

    public int a()
    {
        return d;
    }

    protected void a(Message message)
        throws Exception
    {
        super.a(message);
        switch (message.what)
        {
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        default:
            return;

        case 0: // '\0'
            b();
            return;

        case 1: // '\001'
            d();
            return;

        case 2: // '\002'
            b(message);
            return;

        case 3: // '\003'
            f();
            return;

        case 4: // '\004'
            c(message);
            break;
        }
    }

    protected void a(SpeechError speecherror)
    {
        com.iflytek.cloud.a.g.a.a.a("onSessionEnd");
        g();
        SpeechError speecherror1 = speecherror;
        if (a.equals("oneshot"))
        {
            speecherror1 = speecherror;
            if (h)
            {
                speecherror1 = speecherror;
                if (g.size() <= 0)
                {
                    speecherror1 = speecherror;
                    if (speecherror == null)
                    {
                        speecherror1 = speecherror;
                        if (t().a("asr_nomatch_error", true))
                        {
                            speecherror1 = new SpeechError(10118);
                        }
                    }
                }
            }
        }
        if (s)
        {
            e.a("user abort");
        } else
        if (speecherror1 != null)
        {
            e.a((new StringBuilder()).append("error").append(speecherror1.getErrorCode()).toString());
        } else
        {
            e.a("success");
        }
        super.a(speecherror1);
        if (b != null)
        {
            if (s)
            {
                com.iflytek.cloud.a.g.a.a.a("WakeuperListener#onCancel");
            } else
            {
                com.iflytek.cloud.a.g.a.a.a("WakeuperListener#onEnd");
                if (speecherror1 != null)
                {
                    b.onError(speecherror1);
                    return;
                }
            }
        }
    }

    public void a(WakeuperListener wakeuperlistener)
    {
        this;
        JVM INSTR monitorenter ;
        b = wakeuperlistener;
        com.iflytek.cloud.a.g.a.a.a("startListening called");
        c();
        this;
        JVM INSTR monitorexit ;
        return;
        wakeuperlistener;
        throw wakeuperlistener;
    }

    public void a(byte abyte0[], int l, int i1)
    {
        while (abyte0.length < i1 || abyte0 == null || i1 <= 0 || i1 <= 0 || !r()) 
        {
            return;
        }
        byte abyte1[] = new byte[i1];
        System.arraycopy(abyte0, l, abyte1, 0, i1);
        d(obtainMessage(2, abyte1));
    }

    protected void a(byte abyte0[], boolean flag)
        throws SpeechError
    {
        e.a(abyte0, abyte0.length);
    }

    public boolean a(boolean flag)
    {
        this;
        JVM INSTR monitorenter ;
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("stopListening, current status is :").append(s()).append(" usercancel : ").append(flag).toString());
        if (!a.equals("enroll")) goto _L2; else goto _L1
_L1:
        c = flag;
        a(3);
_L3:
        this;
        JVM INSTR monitorexit ;
        return true;
_L2:
        if (!a.equals("oneshot") || !h)
        {
            break MISSING_BLOCK_LABEL_105;
        }
        g();
        c = flag;
        a(3);
          goto _L3
        Exception exception;
        exception;
        throw exception;
        b(false);
          goto _L3
    }

    protected void b()
        throws Exception
    {
        com.iflytek.cloud.a.g.a.a.a("start connecting");
        h = false;
        int l = t().a("record_read_rate", 40);
        if (d != -1 && r())
        {
            com.iflytek.cloud.a.g.a.a.a("start  record");
            if (f == null)
            {
                f = new f(q(), l, d);
                f.a(this);
            }
            if (b != null)
            {
                b.onBeginOfSpeech();
            }
        }
        a(1, com.iflytek.cloud.a.d.a.a.a, false, 0);
    }

    protected void b(Message message)
        throws Exception
    {
        message = (byte[])(byte[])message.obj;
        if (message == null || message.length == 0)
        {
            return;
        } else
        {
            a(message, true);
            return;
        }
    }

    public void b(SpeechError speecherror)
    {
        c(speecherror);
    }

    public void b(boolean flag)
    {
        if (flag && r() && b != null)
        {
            b.onError(new SpeechError(20017));
        }
        com.iflytek.cloud.a.g.a.a.a("cancel");
        g();
        if (s() == com.iflytek.cloud.a.d.a.b.c)
        {
            c = true;
        }
        super.b(flag);
    }

    void c(Message message)
        throws SpeechError, InterruptedException, UnsupportedEncodingException
    {
        int l = message.arg1;
        byte abyte0[] = (byte[])(byte[])message.obj;
        switch (l)
        {
        case 1: // '\001'
        case 3: // '\003'
        case 4: // '\004'
        default:
            return;

        case 0: // '\0'
            a(false, abyte0, message.arg2);
            return;

        case 2: // '\002'
            throw new SpeechError(20010);

        case 5: // '\005'
            a(true, abyte0, message.arg2);
            break;
        }
    }

    public void c(boolean flag)
    {
    }

    protected void d()
        throws Exception
    {
        if (e.a == null)
        {
            int l = e.a(r, a, this);
            if (l == 0 && e.a != null)
            {
                if (r())
                {
                    MSC.QIVWRegisterNotify(e.a, "MsgProcCallBack", this);
                    a(com.iflytek.cloud.a.d.a.b.c);
                }
            } else
            {
                j = j + 1;
                if (j > 40)
                {
                    throw new SpeechError(l);
                }
                if (r())
                {
                    Thread.sleep(15L);
                    a(1, com.iflytek.cloud.a.d.a.a.a, false, 0);
                    return;
                }
            }
        }
    }

    public void e()
    {
        if (com.iflytek.cloud.a.d.a.b.c == s())
        {
            a(false);
        }
    }

    protected void h()
    {
        a = t().b("sst", "wakeup");
        i = t().a("keep_alive", false);
        d = t().a("audio_source", 1);
        super.h();
    }

    public boolean i()
    {
        return false;
    }

    public void k()
    {
    }
}
