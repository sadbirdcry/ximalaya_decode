// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.c;

import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.d.b;
import com.iflytek.cloud.b.c;
import com.iflytek.msc.MSC;
import com.iflytek.msc.MSCSessionInfo;
import java.io.UnsupportedEncodingException;

public class a extends b
{

    private MSCSessionInfo c;
    private MSCSessionInfo d;

    public a()
    {
        c = new MSCSessionInfo();
        d = new MSCSessionInfo();
    }

    private void a(byte abyte0[], int i, int j)
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        j = MSC.QIVWAudioWrite(a, abyte0, i, j, d);
        c.sesstatus = d.sesstatus;
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("QISRAudioWrite length:").append(i).toString());
        if (j == 0)
        {
            break MISSING_BLOCK_LABEL_77;
        }
        throw new SpeechError(d.errorcode);
        abyte0;
        this;
        JVM INSTR monitorexit ;
        throw abyte0;
        this;
        JVM INSTR monitorexit ;
    }

    public int a(Context context, String s, com.iflytek.cloud.a.d.a a1)
        throws SpeechError, UnsupportedEncodingException
    {
        context = com.iflytek.cloud.b.c.b(context, s, a1);
        s = a1.t().e("cloud_grammar");
        long l = SystemClock.elapsedRealtime();
        int i;
        if (TextUtils.isEmpty(s))
        {
            a = MSC.QIVWSessionBegin(null, context.getBytes(a1.n()), c);
        } else
        {
            a = MSC.QIVWSessionBegin(s.getBytes(a1.n()), context.getBytes(a1.n()), c);
        }
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("sessionBegin ErrCode:").append(c.errorcode).append(" time:").append(SystemClock.elapsedRealtime() - l).toString());
        i = c.errorcode;
        if (i != 0 && i != 10129 && i != 10113 && i != 10132)
        {
            throw new SpeechError(i);
        } else
        {
            return i;
        }
    }

    public void a()
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        a(new byte[0], 0, 4);
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void a(String s)
    {
        if (a == null)
        {
            return;
        }
        com.iflytek.cloud.a.g.a.a.a("sessionEnd enter ");
        long l = System.currentTimeMillis();
        boolean flag;
        if (MSC.QIVWSessionEnd(a, s.getBytes()) == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("sessionEnd leavel:").append(flag).append(" time:").append(System.currentTimeMillis() - l).toString());
        a = null;
        b = null;
    }

    public void a(byte abyte0[], int i)
        throws SpeechError
    {
        this;
        JVM INSTR monitorenter ;
        a(abyte0, i, 2);
        this;
        JVM INSTR monitorexit ;
        return;
        abyte0;
        throw abyte0;
    }
}
