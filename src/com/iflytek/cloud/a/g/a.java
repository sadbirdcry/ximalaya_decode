// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.g;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import java.lang.reflect.Field;

public class a
{

    public static String a[][];
    private static String b[][];
    private static com.iflytek.cloud.b.a c = new com.iflytek.cloud.b.a();
    private static boolean d = false;

    public static com.iflytek.cloud.b.a a(Context context)
    {
        com/iflytek/cloud/a/g/a;
        JVM INSTR monitorenter ;
        if (!d) goto _L2; else goto _L1
_L1:
        context = c;
_L4:
        com/iflytek/cloud/a/g/a;
        JVM INSTR monitorexit ;
        return context;
_L2:
        c(context);
        context = c;
        if (true) goto _L4; else goto _L3
_L3:
        context;
        throw context;
    }

    private static String a(String s)
    {
        try
        {
            s = android/os/Build.getField(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return "unknown";
        }
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_32;
        }
        s = s.get(new Build()).toString();
        return s;
        return "unknown";
    }

    public static void a(com.iflytek.cloud.b.a a1, Context context)
    {
        try
        {
            PackageInfo packageinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            ApplicationInfo applicationinfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            a1.a("app.ver.name", packageinfo.versionName);
            a1.a("app.ver.code", (new StringBuilder()).append("").append(packageinfo.versionCode).toString());
            a1.a("app.pkg", applicationinfo.packageName);
            a1.a("app.path", applicationinfo.dataDir);
            a1.a("app.name", applicationinfo.loadLabel(context.getPackageManager()).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (com.iflytek.cloud.b.a a1)
        {
            return;
        }
    }

    public static com.iflytek.cloud.b.a b(Context context)
    {
        context = a(context);
        com.iflytek.cloud.b.a a1 = new com.iflytek.cloud.b.a();
        a1.a(context, "app.name");
        a1.a(context, "app.path");
        a1.a(context, "app.pkg");
        a1.a(context, "app.ver.name");
        a1.a(context, "app.ver.code");
        a1.a(context, "os.system");
        a1.a(context, "os.resolution");
        a1.a(context, "os.density");
        a1.a(context, "net.mac");
        a1.a(context, "os.imei");
        a1.a(context, "os.imsi");
        a1.a(context, "os.version");
        a1.a(context, "os.release");
        a1.a(context, "os.incremental");
        a1.a(context, "os.android_id");
        a1.a(context, a[0][0]);
        a1.a(context, a[1][0]);
        a1.a(context, a[2][0]);
        a1.a(context, a[3][0]);
        return a1;
    }

    private static void c(Context context)
    {
        c.a();
        c.a("os.system", "Android");
        a(c, context);
        Object obj = context.getResources().getDisplayMetrics();
        c.a("os.resolution", (new StringBuilder()).append(((DisplayMetrics) (obj)).widthPixels).append("*").append(((DisplayMetrics) (obj)).heightPixels).toString());
        c.a("os.density", (new StringBuilder()).append("").append(((DisplayMetrics) (obj)).density).toString());
        obj = (TelephonyManager)context.getSystemService("phone");
        c.a("os.imei", ((TelephonyManager) (obj)).getDeviceId());
        c.a("os.imsi", ((TelephonyManager) (obj)).getSubscriberId());
        obj = android.provider.Settings.Secure.getString(context.getContentResolver(), "android_id");
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            c.a("os.android_id", ((String) (obj)));
        }
        c.a("os.version", android.os.Build.VERSION.SDK);
        c.a("os.release", android.os.Build.VERSION.RELEASE);
        c.a("os.incremental", android.os.Build.VERSION.INCREMENTAL);
        int i = 0;
_L2:
        if (i >= a.length)
        {
            break; /* Loop/switch isn't completed */
        }
        c.a(a[i][0], a[i][1]);
        i++;
        if (true) goto _L2; else goto _L1
_L4:
        if (i >= b.length)
        {
            break MISSING_BLOCK_LABEL_270;
        }
        String s = a(b[i][1]);
        c.a(b[i][0], s);
        i++;
        continue; /* Loop/switch isn't completed */
        try
        {
            context = ((WifiManager)context.getSystemService("wifi")).getConnectionInfo();
            c.a("net.mac", context.getMacAddress());
            c.d();
            d = true;
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            com.iflytek.cloud.a.g.a.a.a("Failed to get prop Info");
        }
        d = false;
        return;
_L1:
        i = 0;
        if (true) goto _L4; else goto _L3
_L3:
    }

    static 
    {
        String as1[] = {
            "os.manufact", Build.MANUFACTURER
        };
        String s = Build.MODEL;
        String as2[] = {
            "os.product", Build.PRODUCT
        };
        String as3[] = {
            "os.display", Build.DISPLAY
        };
        String as4[] = {
            "os.host", Build.HOST
        };
        String as5[] = {
            "os.id", Build.ID
        };
        String as6[] = {
            "os.device", Build.DEVICE
        };
        String as7[] = {
            "os.board", Build.BOARD
        };
        String as8[] = {
            "os.brand", Build.BRAND
        };
        String as9[] = {
            "os.user", Build.USER
        };
        String as10[] = {
            "os.type", Build.TYPE
        };
        a = (new String[][] {
            as1, new String[] {
                "os.model", s
            }, as2, as3, as4, as5, as6, as7, as8, as9, 
            as10
        });
        String as[] = {
            "os.cpu2", "CPU_ABI2"
        };
        as1 = (new String[] {
            "os.serial", "SERIAL"
        });
        as2 = (new String[] {
            "os.bootloader", "BOOTLOADER"
        });
        b = (new String[][] {
            new String[] {
                "os.cpu", "CPU_ABI"
            }, as, as1, new String[] {
                "os.hardware", "HARDWARE"
            }, as2, new String[] {
                "os.radio", "RADIO"
            }
        });
    }
}
