// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.g;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.g.a.a;

public class h
{

    public static String a(NetworkInfo networkinfo)
    {
        if (networkinfo != null) goto _L2; else goto _L1
_L1:
        networkinfo = "none";
_L4:
        return networkinfo;
_L2:
        if (networkinfo.getType() == 1)
        {
            return "wifi";
        }
        String s = networkinfo.getExtraInfo().toLowerCase();
label0:
        {
            if (TextUtils.isEmpty(s))
            {
                return "none";
            }
            try
            {
                if (s.startsWith("3gwap") || s.startsWith("uniwap"))
                {
                    break label0;
                }
            }
            // Misplaced declaration of an exception variable
            catch (NetworkInfo networkinfo)
            {
                com.iflytek.cloud.a.g.a.a.a(networkinfo.toString());
                return "none";
            }
        }
        if (s.startsWith("cmwap"))
        {
            return "cmwap";
        }
        if (s.startsWith("ctwap"))
        {
            return "ctwap";
        }
        networkinfo = s;
        if (!s.startsWith("ctnet")) goto _L4; else goto _L3
_L3:
        return "ctnet";
        return "uniwap";
    }

    public static void a(Context context)
        throws SpeechError
    {
        if (context != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
label0:
        {
            context = ((ConnectivityManager)context.getSystemService("connectivity")).getAllNetworkInfo();
            if (context == null || context.length <= 0)
            {
                break label0;
            }
            int j = context.length;
            int i = 0;
            do
            {
                if (i >= j)
                {
                    break label0;
                }
                NetworkInfo networkinfo = context[i];
                if (networkinfo == null || networkinfo.isConnectedOrConnecting())
                {
                    break;
                }
                i++;
            } while (true);
        }
        if (true) goto _L1; else goto _L3
_L3:
        throw new SpeechError(20001);
    }

    public static String b(NetworkInfo networkinfo)
    {
        if (networkinfo == null)
        {
            return "none";
        }
        if (networkinfo.getType() == 1)
        {
            return "none";
        }
        try
        {
            String s = (new StringBuilder()).append("").append(networkinfo.getSubtypeName()).toString();
            networkinfo = (new StringBuilder()).append(s).append(";").append(networkinfo.getSubtype()).toString();
        }
        // Misplaced declaration of an exception variable
        catch (NetworkInfo networkinfo)
        {
            com.iflytek.cloud.a.g.a.a.a(networkinfo.toString());
            return "none";
        }
        return networkinfo;
    }
}
