// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.a.g;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import com.iflytek.cloud.Setting;
import com.iflytek.cloud.a.g.a.a;

// Referenced classes of package com.iflytek.cloud.a.g:
//            e, a

public class b
{

    public static b a = null;
    private SharedPreferences b;
    private Context c;
    private boolean d;
    private long e;

    private b(Context context)
    {
        b = null;
        c = null;
        d = true;
        e = 0L;
        c = context;
        b = context.getSharedPreferences("com.iflytek.msc", 0);
        d = b(context);
    }

    public static b a(Context context)
    {
        if (a == null && context != null)
        {
            c(context);
        }
        return a;
    }

    public static boolean b(Context context)
    {
        if (!Setting.b)
        {
            return false;
        }
        if (context == null) goto _L2; else goto _L1
_L1:
        int i;
        boolean flag;
        try
        {
            context = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            break; /* Loop/switch isn't completed */
        }
        i = 0;
_L3:
        if (i >= context.length)
        {
            break; /* Loop/switch isn't completed */
        }
        if ("android.permission.ACCESS_FINE_LOCATION".equalsIgnoreCase(context[i]))
        {
            break MISSING_BLOCK_LABEL_62;
        }
        flag = "android.permission.ACCESS_COARSE_LOCATION".equalsIgnoreCase(context[i]);
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_64;
        }
        return true;
        i++;
        continue; /* Loop/switch isn't completed */
        if (true) goto _L3; else goto _L2
_L2:
        return false;
    }

    private static b c(Context context)
    {
        com/iflytek/cloud/a/g/b;
        JVM INSTR monitorenter ;
        if (a == null)
        {
            a = new b(context);
        }
        com.iflytek.cloud.a.g.e.a(context);
        a(context);
        context = a;
        com/iflytek/cloud/a/g/b;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public float a(String s)
    {
        this;
        JVM INSTR monitorenter ;
        if (!d || System.currentTimeMillis() - e <= 0x34bc0L) goto _L2; else goto _L1
_L1:
        Object obj;
        Object obj1;
        obj = (LocationManager)c.getApplicationContext().getSystemService("location");
        e = System.currentTimeMillis();
        a("loction_last_update", e);
        com.iflytek.cloud.a.g.a.a.c((new StringBuilder()).append("getLocation begin:").append(System.currentTimeMillis()).toString());
        obj1 = new Criteria();
        ((Criteria) (obj1)).setAccuracy(1);
        obj1 = ((LocationManager) (obj)).getBestProvider(((Criteria) (obj1)), true);
        com.iflytek.cloud.a.g.a.a.c((new StringBuilder()).append("bestProvider:").append(((String) (obj1))).toString());
        obj1 = ((LocationManager) (obj)).getLastKnownLocation("gps");
        if (obj1 == null) goto _L4; else goto _L3
_L3:
        com.iflytek.cloud.a.g.a.a.a(((Location) (obj1)).toString());
        a(((Location) (obj1)));
_L6:
        com.iflytek.cloud.a.g.a.a.c((new StringBuilder()).append("getLocation end:").append(System.currentTimeMillis()).toString());
_L2:
        float f = b.getFloat(s, -0.1F);
        this;
        JVM INSTR monitorexit ;
        return f;
_L4:
        obj = ((LocationManager) (obj)).getLastKnownLocation("network");
        if (obj == null) goto _L6; else goto _L5
_L5:
        com.iflytek.cloud.a.g.a.a.a(((Location) (obj)).toString());
        a(((Location) (obj)));
          goto _L6
        Exception exception;
        exception;
          goto _L2
        s;
        throw s;
          goto _L6
    }

    public void a(Location location)
    {
        if (location == null)
        {
            return;
        } else
        {
            android.content.SharedPreferences.Editor editor = b.edit();
            editor.putFloat("msc.lat", (float)location.getLatitude());
            editor.putFloat("msc.lng", (float)location.getLongitude());
            editor.commit();
            return;
        }
    }

    public void a(String s, long l)
    {
        android.content.SharedPreferences.Editor editor = b.edit();
        editor.putLong(s, l);
        editor.commit();
    }

}
