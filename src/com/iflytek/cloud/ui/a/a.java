// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.ui.a;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

// Referenced classes of package com.iflytek.cloud.ui.a:
//            b

public class a
{

    public static int a = 3;
    public static int b = 4;
    public static int c = 7;
    public static int d = 8;
    private static HashMap e = new HashMap();
    private static HashMap f = new HashMap();

    public static Drawable a(Context context, String s)
        throws Exception
    {
        com/iflytek/cloud/ui/a/a;
        JVM INSTR monitorenter ;
        Drawable drawable1 = (Drawable)e.get(s);
        Drawable drawable;
        drawable = drawable1;
        if (drawable1 != null)
        {
            break MISSING_BLOCK_LABEL_35;
        }
        drawable = c(context, s);
        e.put(s, drawable);
        com/iflytek/cloud/ui/a/a;
        JVM INSTR monitorexit ;
        return drawable;
        context;
        throw context;
    }

    public static View a(Context context, String s, ViewGroup viewgroup)
        throws Exception
    {
        s = (new StringBuilder()).append("assets/iflytek/").append(s).append(".xml").toString();
        s = context.getAssets().openXmlResourceParser(s);
        return ((LayoutInflater)context.getSystemService("layout_inflater")).inflate(s, viewgroup);
    }

    public static int[] a()
    {
        return (new int[] {
            0xffe7e7e7, 0xff686e72
        });
    }

    private static InputStream b(Context context, String s)
        throws IOException
    {
        return context.getAssets().open(s);
    }

    public static int[] b()
    {
        return (new int[] {
            20, 16
        });
    }

    private static Drawable c(Context context, String s)
        throws Exception
    {
        InputStream inputstream = b(context, (new StringBuilder()).append("iflytek/").append(s).append(".png").toString());
        TypedValue typedvalue = new TypedValue();
        typedvalue.density = 240;
        if (android.os.Build.VERSION.SDK_INT > a)
        {
            context = com.iflytek.cloud.ui.a.b.a(context.getResources(), typedvalue, inputstream, s, null);
        } else
        {
            context = Drawable.createFromResourceStream(context.getResources(), typedvalue, inputstream, s);
        }
        if (inputstream != null)
        {
            inputstream.close();
        }
        return context;
    }

}
