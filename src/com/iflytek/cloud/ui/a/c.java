// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.ui.a;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;

// Referenced classes of package com.iflytek.cloud.ui.a:
//            d, e

public class c extends Dialog
{
    public static interface a
    {

        public abstract void a();
    }


    protected e a;
    private a b;

    public c(Context context)
    {
        super(context);
        a = null;
        b = new d(this);
    }

    public boolean destroy()
    {
        if (isShowing())
        {
            return false;
        } else
        {
            boolean flag = a.d();
            a = null;
            return flag;
        }
    }

    public void dismiss()
    {
        a.c();
        super.dismiss();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(a);
        getWindow().setBackgroundDrawable(new ColorDrawable(0));
    }

    public void show()
    {
        setCanceledOnTouchOutside(true);
        a.a(b);
        a.b();
        super.show();
    }
}
