// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.ui.a;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import java.io.InputStream;

public class b
{

    private static int a = 0;

    public static Bitmap a(Resources resources, TypedValue typedvalue, InputStream inputstream, Rect rect, android.graphics.BitmapFactory.Options options)
    {
        android.graphics.BitmapFactory.Options options1;
        options1 = options;
        if (options == null)
        {
            options1 = new android.graphics.BitmapFactory.Options();
        }
        if (options1.inDensity != 0 || typedvalue == null) goto _L2; else goto _L1
_L1:
        int i = typedvalue.density;
        if (i != 0) goto _L4; else goto _L3
_L3:
        options1.inDensity = 160;
_L2:
        if (options1.inTargetDensity == 0 && resources != null)
        {
            options1.inTargetDensity = resources.getDisplayMetrics().densityDpi;
        }
        return BitmapFactory.decodeStream(inputstream, rect, options1);
_L4:
        if (i != 65535)
        {
            options1.inDensity = i;
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    private static Drawable a(Resources resources, Bitmap bitmap, byte abyte0[], Rect rect, String s)
    {
        if (abyte0 != null)
        {
            return new NinePatchDrawable(resources, bitmap, abyte0, rect, s);
        } else
        {
            return new BitmapDrawable(resources, bitmap);
        }
    }

    public static Drawable a(Resources resources, TypedValue typedvalue, InputStream inputstream, String s, android.graphics.BitmapFactory.Options options)
    {
        Object obj = null;
        if (inputstream != null)
        {
            Rect rect = new Rect();
            android.graphics.BitmapFactory.Options options1 = options;
            if (options == null)
            {
                options1 = new android.graphics.BitmapFactory.Options();
            }
            options = a(resources, typedvalue, inputstream, rect, options1);
            if (options != null)
            {
                typedvalue = options.getNinePatchChunk();
                if (typedvalue == null || !NinePatch.isNinePatchChunk(typedvalue))
                {
                    typedvalue = null;
                    inputstream = obj;
                } else
                {
                    inputstream = rect;
                }
                return a(resources, ((Bitmap) (options)), ((byte []) (typedvalue)), ((Rect) (inputstream)), s);
            }
        }
        return null;
    }

}
