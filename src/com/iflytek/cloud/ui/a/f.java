// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.ui.a;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PaintFlagsDrawFilter;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;

// Referenced classes of package com.iflytek.cloud.ui.a:
//            a

public class f extends View
{

    Path a;
    private Drawable b;
    private Drawable c;

    public f(Context context)
    {
        super(context);
        try
        {
            b = com.iflytek.cloud.ui.a.a.a(getContext(), "voice_empty");
            c = com.iflytek.cloud.ui.a.a.a(getContext(), "voice_full");
            b.setBounds(new Rect(-b.getIntrinsicWidth() / 2, -b.getIntrinsicHeight() / 2, b.getIntrinsicWidth() / 2, b.getIntrinsicHeight() / 2));
            c.setBounds(new Rect(-c.getIntrinsicWidth() / 2, -c.getIntrinsicHeight() / 2, c.getIntrinsicWidth() / 2, c.getIntrinsicHeight() / 2));
            a = new Path();
            a(0);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    public void a(int i)
    {
        a.reset();
        a.addCircle(0.0F, 0.0F, (b.getIntrinsicWidth() * i) / 12, android.graphics.Path.Direction.CCW);
    }

    public void finalize()
        throws Throwable
    {
        b = null;
        c = null;
        super.finalize();
    }

    public void onDraw(Canvas canvas)
    {
        canvas.save();
        canvas.setDrawFilter(new PaintFlagsDrawFilter(1, 2));
        canvas.translate(getWidth() / 2, getHeight() / 2);
        b.draw(canvas);
        canvas.clipPath(a);
        c.draw(canvas);
        canvas.restore();
    }

    protected void onMeasure(int i, int j)
    {
        super.onMeasure(i, j);
        int l = android.view.View.MeasureSpec.getSize(i);
        int k = android.view.View.MeasureSpec.getSize(j);
        Drawable drawable = getBackground();
        if (drawable != null)
        {
            l = drawable.getMinimumWidth();
            k = drawable.getMinimumHeight();
        }
        setMeasuredDimension(resolveSize(l, i), resolveSize(k, j));
    }
}
