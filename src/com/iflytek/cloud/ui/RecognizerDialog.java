// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.ui;

import android.content.Context;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.resource.Resource;
import com.iflytek.cloud.ui.a.c;
import java.util.Locale;

// Referenced classes of package com.iflytek.cloud.ui:
//            a, RecognizerDialogListener

public class RecognizerDialog extends c
{

    public RecognizerDialog(Context context, InitListener initlistener)
    {
        super(context);
        a = new a(context, initlistener);
    }

    public void dismiss()
    {
        super.dismiss();
    }

    public void setListener(RecognizerDialogListener recognizerdialoglistener)
    {
        ((a)a).a(recognizerdialoglistener);
    }

    public void setParameter(String s, String s1)
    {
        ((a)a).a(s, s1);
    }

    public void setUILanguage(Locale locale)
    {
        Resource.setUILanguage(locale);
    }

    public void show()
    {
        super.show();
    }
}
