// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.resource.Resource;
import com.iflytek.cloud.ui.a.e;
import com.iflytek.cloud.ui.a.f;

// Referenced classes of package com.iflytek.cloud.ui:
//            b, RecognizerDialogListener

public final class com.iflytek.cloud.ui.a extends e
    implements android.view.View.OnClickListener
{
    public class a extends ClickableSpan
    {

        final com.iflytek.cloud.ui.a a;
        private String b;

        public void onClick(View view)
        {
            try
            {
                view = view.getContext();
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(b));
                intent.addFlags(0x10000000);
                intent.putExtra("com.android.browser.application_id", view.getPackageName());
                view.startActivity(intent);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (View view) { }
            finally
            {
                throw view;
            }
            view.printStackTrace();
            return;
        }

        public void updateDrawState(TextPaint textpaint)
        {
            super.updateDrawState(textpaint);
        }

        public a(String s)
        {
            a = com.iflytek.cloud.ui.a.this;
            super();
            b = s;
        }
    }


    public static int a = 9;
    private LinearLayout d;
    private f e;
    private RotateAnimation f;
    private SpeechRecognizer g;
    private RecognizerDialogListener h;
    private long i;
    private RecognizerListener j;
    private volatile int k;

    public com.iflytek.cloud.ui.a(Context context, InitListener initlistener)
    {
        super(context.getApplicationContext());
        e = null;
        f = null;
        i = 0L;
        j = new b(this);
        g = SpeechRecognizer.createRecognizer(context.getApplicationContext(), initlistener);
        a();
    }

    static int a(com.iflytek.cloud.ui.a a1)
    {
        return a1.k;
    }

    private void a(SpeechError speecherror)
    {
        try
        {
            LinearLayout linearlayout = (LinearLayout)d.findViewWithTag("error");
            a((TextView)linearlayout.findViewWithTag("errtxt"), speecherror);
            linearlayout.findViewWithTag("errview").setBackgroundDrawable(com.iflytek.cloud.ui.a.a.a(getContext(), "warning"));
            setTag(speecherror);
            k = 3;
            k();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (SpeechError speecherror)
        {
            speecherror.printStackTrace();
        }
    }

    static void a(com.iflytek.cloud.ui.a a1, SpeechError speecherror)
    {
        a1.a(speecherror);
    }

    static f b(com.iflytek.cloud.ui.a a1)
    {
        return a1.e;
    }

    static void c(com.iflytek.cloud.ui.a a1)
    {
        a1.j();
    }

    static RecognizerDialogListener d(com.iflytek.cloud.ui.a a1)
    {
        return a1.h;
    }

    static boolean e(com.iflytek.cloud.ui.a a1)
    {
        return a1.b;
    }

    private void g()
    {
        com.iflytek.cloud.a.g.a.a.a("startRecognizing");
        long l1 = i;
        i = SystemClock.elapsedRealtime();
        if (i - l1 < 300L)
        {
            return;
        }
        int l = g.startListening(j);
        if (l != 0)
        {
            a(new SpeechError(l));
            return;
        } else
        {
            i();
            return;
        }
    }

    private void h()
    {
        if (d != null)
        {
            d.destroyDrawingCache();
            d = null;
        }
        e = null;
        System.gc();
    }

    private void i()
    {
        if (e == null)
        {
            e = new f(getContext().getApplicationContext());
        }
        k = 1;
        k();
    }

    private void j()
    {
        try
        {
            ((FrameLayout)d.findViewWithTag("waiting")).findViewWithTag("control").startAnimation(f);
            k = 2;
            k();
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void k()
    {
        FrameLayout framelayout = (FrameLayout)d.findViewWithTag("waiting");
        TextView textview = (TextView)d.findViewWithTag("title");
        LinearLayout linearlayout = (LinearLayout)d.findViewWithTag("error");
        TextView textview1 = (TextView)framelayout.findViewWithTag("tips");
        if (k == 1)
        {
            linearlayout.setVisibility(8);
            textview.setVisibility(0);
            framelayout.setVisibility(8);
            textview.setText(Resource.getTitle(2));
            e.a(0);
            e.invalidate();
            e.setVisibility(0);
        } else
        {
            if (k == 2)
            {
                textview.setVisibility(8);
                e.setVisibility(8);
                framelayout.setVisibility(0);
                textview1.setVisibility(0);
                textview1.setText(Resource.getTitle(3));
                return;
            }
            if (k == 3)
            {
                textview.setVisibility(8);
                e.setVisibility(8);
                framelayout.setVisibility(8);
                linearlayout.setVisibility(0);
                return;
            }
        }
    }

    public void a()
    {
        try
        {
            Object obj = getContext().getApplicationContext();
            View view = com.iflytek.cloud.ui.a.a.a(((Context) (obj)), "recognize", this);
            view.setBackgroundDrawable(com.iflytek.cloud.ui.a.a.a(((Context) (obj)).getApplicationContext(), "voice_bg.9"));
            d = (LinearLayout)view.findViewWithTag("container");
            com.iflytek.cloud.a.g.f.a(this);
            e = new f(((Context) (obj)).getApplicationContext());
            obj = new android.widget.LinearLayout.LayoutParams(-1, 0, 1.0F);
            obj.bottomMargin = 20;
            d.addView(e, 1, ((android.view.ViewGroup.LayoutParams) (obj)));
            ((FrameLayout)d.findViewWithTag("waiting")).findViewWithTag("control").setBackgroundDrawable(com.iflytek.cloud.ui.a.a.a(getContext(), "waiting"));
            f = new RotateAnimation(0.0F, 360F, 1, 0.5F, 1, 0.5F);
            f.setRepeatCount(-1);
            f.setInterpolator(new LinearInterpolator());
            f.setDuration(700L);
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public void a(TextView textview, SpeechError speecherror)
    {
        textview.setText(Html.fromHtml(speecherror.getHtmlDescription(true)));
        textview.setMovementMethod(LinkMovementMethod.getInstance());
        textview.bringToFront();
        Object obj = textview.getText();
        if (obj instanceof Spannable)
        {
            int j1 = ((CharSequence) (obj)).length();
            Spannable spannable = (Spannable)textview.getText();
            URLSpan aurlspan[] = (URLSpan[])spannable.getSpans(0, j1, android/text/style/URLSpan);
            obj = new SpannableStringBuilder(((CharSequence) (obj)));
            ((SpannableStringBuilder) (obj)).clearSpans();
            int k1 = aurlspan.length;
            for (int l = 0; l < k1; l++)
            {
                URLSpan urlspan = aurlspan[l];
                ((SpannableStringBuilder) (obj)).setSpan(new a(urlspan.getURL()), spannable.getSpanStart(urlspan), spannable.getSpanEnd(urlspan), 34);
            }

            int i1 = speecherror.getHtmlDescription(false).length();
            k1 = speecherror.getHtmlDescription(true).length();
            int l1 = "<br>".length();
            ((SpannableStringBuilder) (obj)).setSpan(new ForegroundColorSpan(com.iflytek.cloud.ui.a.a.a()[0]), 0, i1, 34);
            ((SpannableStringBuilder) (obj)).setSpan(new AbsoluteSizeSpan(com.iflytek.cloud.ui.a.a.b()[0], true), 0, i1, 33);
            ((SpannableStringBuilder) (obj)).setSpan(new ForegroundColorSpan(com.iflytek.cloud.ui.a.a.a()[1]), i1 + 1, (k1 - l1) + 1, 34);
            ((SpannableStringBuilder) (obj)).setSpan(new AbsoluteSizeSpan(com.iflytek.cloud.ui.a.a.b()[1], true), i1 + 1, j1, 34);
            textview.setText(((CharSequence) (obj)));
        }
    }

    public void a(RecognizerDialogListener recognizerdialoglistener)
    {
        h = recognizerdialoglistener;
        setOnClickListener(this);
    }

    public void a(String s, String s1)
    {
        g.setParameter(s, s1);
    }

    public void b()
    {
        super.b();
        g();
    }

    public void c()
    {
        g.cancel();
        super.c();
    }

    protected boolean d()
    {
        if (super.d())
        {
            h();
            return g.destroy();
        } else
        {
            return false;
        }
    }

    public void onClick(View view)
    {
        switch (k)
        {
        case 2: // '\002'
        default:
            return;

        case 3: // '\003'
            if (view.getTag() != null && ((SpeechError)view.getTag()).getErrorCode() == 20001)
            {
                e();
                return;
            } else
            {
                g();
                return;
            }

        case 1: // '\001'
            g.stopListening();
            j();
            return;
        }
    }

}
