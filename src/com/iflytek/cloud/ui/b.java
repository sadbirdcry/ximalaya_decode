// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.ui;

import android.os.Bundle;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.ui.a.f;

// Referenced classes of package com.iflytek.cloud.ui:
//            a, RecognizerDialogListener

final class b
    implements RecognizerListener
{

    final a a;

    b(a a1)
    {
        a = a1;
        super();
    }

    public void onBeginOfSpeech()
    {
    }

    public void onEndOfSpeech()
    {
        com.iflytek.cloud.ui.a.c(a);
    }

    public void onError(SpeechError speecherror)
    {
        if (speecherror == null || !com.iflytek.cloud.ui.a.e(a))
        {
            a.();
        } else
        {
            com.iflytek.cloud.ui.a.a(a, speecherror);
        }
        if (com.iflytek.cloud.ui.a.d(a) != null)
        {
            com.iflytek.cloud.ui.a.d(a).onError(speecherror);
        }
    }

    public void onEvent(int i, int j, int k, Bundle bundle)
    {
    }

    public void onResult(RecognizerResult recognizerresult, boolean flag)
    {
        if (flag)
        {
            a.();
        }
        if (com.iflytek.cloud.ui.a.d(a) != null)
        {
            com.iflytek.cloud.ui.a.d(a).onResult(recognizerresult, flag);
        }
    }

    public void onVolumeChanged(int i)
    {
        if (com.iflytek.cloud.ui.a.a(a) == 1)
        {
            i = (i + 2) / 5;
            com.iflytek.cloud.ui.a.b(a).a(i);
            com.iflytek.cloud.ui.a.b(a).invalidate();
        }
    }
}
