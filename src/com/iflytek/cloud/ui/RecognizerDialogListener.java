// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.ui;

import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechError;

public interface RecognizerDialogListener
{

    public abstract void onError(SpeechError speecherror);

    public abstract void onResult(RecognizerResult recognizerresult, boolean flag);
}
