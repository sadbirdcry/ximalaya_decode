// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.util;

import android.content.Context;
import com.iflytek.cloud.util.a.b;

public abstract class ContactManager
{
    public static interface ContactListener
    {

        public abstract void onContactQueryFinish(String s, boolean flag);
    }


    protected ContactManager()
    {
    }

    public static ContactManager createManager(Context context, ContactListener contactlistener)
    {
        return b.a(context, contactlistener);
    }

    public static ContactManager getManager()
    {
        return b.a();
    }

    public abstract void asyncQueryAllContactsName();

    public abstract String queryAllContactsName();
}
