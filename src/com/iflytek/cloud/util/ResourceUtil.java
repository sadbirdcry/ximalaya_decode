// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.util;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.text.TextUtils;
import java.io.File;

public class ResourceUtil
{
    public static final class RESOURCE_TYPE extends Enum
    {

        private static final RESOURCE_TYPE a[];
        public static final RESOURCE_TYPE assets;
        public static final RESOURCE_TYPE path;
        public static final RESOURCE_TYPE res;

        public static RESOURCE_TYPE valueOf(String s)
        {
            return (RESOURCE_TYPE)Enum.valueOf(com/iflytek/cloud/util/ResourceUtil$RESOURCE_TYPE, s);
        }

        public static RESOURCE_TYPE[] values()
        {
            return (RESOURCE_TYPE[])a.clone();
        }

        static 
        {
            assets = new RESOURCE_TYPE("assets", 0);
            res = new RESOURCE_TYPE("res", 1);
            path = new RESOURCE_TYPE("path", 2);
            a = (new RESOURCE_TYPE[] {
                assets, res, path
            });
        }

        private RESOURCE_TYPE(String s, int i)
        {
            super(s, i);
        }
    }


    public static final String ASR_RES_PATH = "asr_res_path";
    public static final String ENGINE_DESTROY = "engine_destroy";
    public static final String ENGINE_START = "engine_start";
    public static final String GRM_BUILD_PATH = "grm_build_path";
    public static final String IVW_RES_PATH = "ivw_res_path";
    public static final String TTS_RES_PATH = "tts_res_path";

    public ResourceUtil()
    {
    }

    private static String a(Context context, RESOURCE_TYPE resource_type, String s)
    {
        Object obj;
        Object obj1;
        obj1 = null;
        obj = obj1;
        if (TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        if (context != null) goto _L4; else goto _L3
_L3:
        obj = obj1;
_L2:
        return ((String) (obj));
_L4:
        obj = context.getPackageResourcePath();
        if (resource_type != RESOURCE_TYPE.assets) goto _L6; else goto _L5
_L5:
        s = context.getAssets().openFd(s);
        context = s;
        resource_type = s;
        long l1 = s.getStartOffset();
        context = s;
        resource_type = s;
        long l = s.getLength();
_L8:
        context = s;
        resource_type = s;
        obj = (new StringBuilder()).append("fo|").append(((String) (obj))).append("|").append(l1).append("|").append(l).toString();
        context = ((Context) (obj));
        obj = context;
        if (s == null) goto _L2; else goto _L7
_L7:
        try
        {
            s.close();
        }
        // Misplaced declaration of an exception variable
        catch (RESOURCE_TYPE resource_type)
        {
            resource_type.printStackTrace();
            return context;
        }
        return context;
_L6:
        int i = Integer.valueOf(s).intValue();
        s = context.getResources().openRawResourceFd(i);
        context = s;
        resource_type = s;
        l1 = s.getStartOffset();
        context = s;
        resource_type = s;
        l = s.getLength();
          goto _L8
        s;
        resource_type = null;
_L12:
        context = resource_type;
        s.printStackTrace();
        obj = obj1;
        if (resource_type == null) goto _L2; else goto _L9
_L9:
        try
        {
            resource_type.close();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return null;
        }
        return null;
        resource_type;
        context = null;
_L11:
        if (context != null)
        {
            try
            {
                context.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context.printStackTrace();
            }
        }
        throw resource_type;
        resource_type;
        if (true) goto _L11; else goto _L10
_L10:
        s;
          goto _L12
    }

    private static String a(String s)
    {
        File file;
        if (!TextUtils.isEmpty(s))
        {
            if ((file = new File(s)).exists() && file.isFile())
            {
                long l = file.length();
                return (new StringBuilder()).append("fo|").append(s).append("|").append(0L).append("|").append(l).toString();
            }
        }
        return null;
    }

    public static String generateResourcePath(Context context, RESOURCE_TYPE resource_type, String s)
    {
        if (resource_type == RESOURCE_TYPE.path)
        {
            return a(s);
        } else
        {
            return a(context, resource_type, s);
        }
    }
}
