// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.util.a.a;

import android.os.Parcel;
import android.os.Parcelable;

// Referenced classes of package com.iflytek.cloud.util.a.a:
//            b

public class a
    implements Parcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new b();
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;

    public a()
    {
        a = null;
        b = null;
        c = null;
        d = null;
        e = null;
        f = null;
        g = null;
    }

    public a(String s, String s1, String s2, String s3, String s4, String s5)
    {
        a = null;
        b = null;
        c = null;
        d = null;
        e = null;
        f = null;
        g = null;
        a = s;
        b = s1;
        c = s2;
        d = s3;
        e = s4;
        g = s5;
    }

    static String a(a a1, String s)
    {
        a1.a = s;
        return s;
    }

    static String b(a a1, String s)
    {
        a1.b = s;
        return s;
    }

    static String c(a a1, String s)
    {
        a1.c = s;
        return s;
    }

    static String d(a a1, String s)
    {
        a1.d = s;
        return s;
    }

    static String e(a a1, String s)
    {
        a1.e = s;
        return s;
    }

    static String f(a a1, String s)
    {
        a1.f = s;
        return s;
    }

    static String g(a a1, String s)
    {
        a1.g = s;
        return s;
    }

    public String a()
    {
        return a;
    }

    public String b()
    {
        return b;
    }

    public String c()
    {
        return d;
    }

    public int describeContents()
    {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(a);
        parcel.writeString(b);
        parcel.writeString(c);
        parcel.writeString(d);
        parcel.writeString(e);
        parcel.writeString(f);
        parcel.writeString(g);
    }

}
