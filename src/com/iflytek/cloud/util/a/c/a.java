// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.util.a.c;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import com.iflytek.cloud.a.g.d;
import com.iflytek.cloud.util.a.e;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class a
{

    protected static final String a[] = {
        "number", "name", "date"
    };
    protected static String c[];
    protected Context b;

    public a(Context context)
    {
        b = null;
        b = context;
    }

    public abstract Uri a();

    public HashMap a(int i)
    {
        Object obj;
        HashMap hashmap;
        obj = (new StringBuilder()).append("date DESC limit ").append(i).toString();
        hashmap = new HashMap();
        Object obj1 = b.getContentResolver().query(android.provider.CallLog.Calls.CONTENT_URI, a, "0==0) GROUP BY (number", null, ((String) (obj)));
        if (obj1 != null) goto _L2; else goto _L1
_L1:
        obj = obj1;
        com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "queryCallLog ----------------cursor is null");
_L5:
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
_L7:
        return hashmap;
_L2:
        obj = obj1;
        if (((Cursor) (obj1)).getCount() != 0) goto _L4; else goto _L3
_L3:
        obj = obj1;
        com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "queryCallLog ----------------cursor getCount == 0");
          goto _L5
        Object obj2;
        obj2;
_L10:
        obj = obj1;
        ((Exception) (obj2)).printStackTrace();
        if (obj1 == null) goto _L7; else goto _L6
_L6:
        ((Cursor) (obj1)).close();
        return hashmap;
_L4:
        obj = obj1;
        if (!((Cursor) (obj1)).moveToNext())
        {
            break; /* Loop/switch isn't completed */
        }
        obj = obj1;
        String s2 = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("number"));
        obj = obj1;
        String s = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("name"));
        obj = obj1;
        String s1 = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex("date"));
        obj2 = s;
        if (s != null)
        {
            break MISSING_BLOCK_LABEL_204;
        }
        obj = obj1;
        obj2 = com.iflytek.cloud.util.a.e.a(s2);
        obj = obj1;
        hashmap.put(s1, obj2);
        if (true) goto _L4; else goto _L8
        obj2;
        obj1 = obj;
        obj = obj2;
_L9:
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw obj;
_L8:
        obj = obj1;
        com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", (new StringBuilder()).append("queryCallLog ----------------cursor getCount ==").append(((Cursor) (obj1)).getCount()).toString());
          goto _L5
        obj;
        obj1 = null;
          goto _L9
        obj2;
        obj1 = null;
          goto _L10
    }

    protected void a(Context context)
    {
        c = new String[100];
        c[0] = "\u5176\u4ED6";
        c[1] = "\u4F4F\u5B85";
        c[2] = "\u624B\u673A";
        c[3] = "\u5DE5\u4F5C";
        c[4] = "\u5DE5\u4F5C\u4F20\u771F";
        c[5] = "\u4F4F\u5B85\u4F20\u771F";
        c[6] = "\u5BFB\u547C\u673A";
        c[7] = "\u5176\u4ED6";
        c[9] = "SIM\u5361";
        for (int i = 10; i < c.length; i++)
        {
            c[i] = "\u81EA\u5B9A\u4E49\u7535\u8BDD";
        }

    }

    protected abstract String[] b();

    protected abstract String c();

    public HashMap d()
    {
        Object obj2;
        HashMap hashmap;
        obj2 = b();
        hashmap = new HashMap();
        Object obj1 = b.getContentResolver().query(a(), ((String []) (obj2)), null, null, c());
        if (obj1 != null) goto _L2; else goto _L1
_L1:
        Object obj = obj1;
        com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "queryContacts------cursor is null");
_L5:
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
_L7:
        return hashmap;
_L2:
        obj = obj1;
        if (((Cursor) (obj1)).getCount() != 0) goto _L4; else goto _L3
_L3:
        obj = obj1;
        com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "queryContacts------cursor getCount == 0");
          goto _L5
        obj2;
_L12:
        obj = obj1;
        ((Exception) (obj2)).printStackTrace();
        if (obj1 == null) goto _L7; else goto _L6
_L6:
        ((Cursor) (obj1)).close();
        return hashmap;
_L4:
        obj = obj1;
        if (!((Cursor) (obj1)).moveToNext()) goto _L9; else goto _L8
_L8:
        obj = obj1;
        String s = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex(obj2[0]));
        obj = obj1;
        String s1 = ((Cursor) (obj1)).getString(((Cursor) (obj1)).getColumnIndex(obj2[1]));
        if (s == null) goto _L4; else goto _L10
_L10:
        obj = obj1;
        hashmap.put(s1, s);
          goto _L4
        obj2;
        obj1 = obj;
        obj = obj2;
_L11:
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw obj;
_L9:
        obj = obj1;
        com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", (new StringBuilder()).append("queryContacts_20------count = ").append(((Cursor) (obj1)).getCount()).toString());
          goto _L5
        obj;
        obj1 = null;
          goto _L11
        obj2;
        obj1 = null;
          goto _L12
    }

    public List e()
    {
        ArrayList arraylist = new ArrayList();
        Object obj = b.getContentResolver().query(Uri.parse("content://icc/adn"), null, null, null, null);
        if (obj == null) goto _L2; else goto _L1
_L1:
        if (((Cursor) (obj)).getCount() <= 0) goto _L2; else goto _L3
_L3:
        if (!((Cursor) (obj)).moveToNext()) goto _L5; else goto _L4
_L4:
        String s;
        String s1;
        String s2;
        s = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("name"));
        s1 = ((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("_id"));
        s2 = com.iflytek.cloud.a.g.d.a(com.iflytek.cloud.util.a.e.a(((Cursor) (obj)).getString(((Cursor) (obj)).getColumnIndex("number"))));
        if (s == null) goto _L3; else goto _L6
_L6:
        arraylist.add(new com.iflytek.cloud.util.a.a.a(s1, s, null, s2, null, c[9]));
          goto _L3
        Object obj1;
        obj1;
_L14:
        ((Exception) (obj1)).printStackTrace();
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
_L10:
        return arraylist;
_L5:
        com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", (new StringBuilder()).append("querySIM-------count = ").append(((Cursor) (obj)).getCount()).toString());
_L8:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
            return arraylist;
        }
        break; /* Loop/switch isn't completed */
_L2:
        com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "querySIM-------cursor getCount = 0 or cursor is null");
        if (true) goto _L8; else goto _L7
_L7:
        if (true) goto _L10; else goto _L9
_L9:
        Exception exception;
        exception;
        obj1 = obj;
        obj = exception;
_L12:
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw obj;
        obj;
        obj1 = null;
        continue; /* Loop/switch isn't completed */
        exception;
        obj1 = obj;
        obj = exception;
        if (true) goto _L12; else goto _L11
_L11:
        obj1;
        obj = null;
        if (true) goto _L14; else goto _L13
_L13:
    }

    public Uri f()
    {
        return android.provider.CallLog.Calls.CONTENT_URI;
    }

}
