// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.util.a;

import android.content.ContentResolver;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.os.HandlerThread;
import com.iflytek.cloud.util.ContactManager;
import com.iflytek.cloud.util.a.b.a;
import java.io.File;

// Referenced classes of package com.iflytek.cloud.util.a:
//            a, e, d, c

public class com.iflytek.cloud.util.a.b extends ContactManager
{
    private class a extends ContentObserver
    {

        final com.iflytek.cloud.util.a.b a;

        public void onChange(boolean flag)
        {
            com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "CallLogObserver | onChange");
            if (System.currentTimeMillis() - com.iflytek.cloud.util.a.b.d(a) < 5000L)
            {
                com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "onChange too much");
                return;
            } else
            {
                com.iflytek.cloud.util.a.b.b(a, System.currentTimeMillis());
                com.iflytek.cloud.util.a.b.b(a);
                return;
            }
        }

        public a(Handler handler)
        {
            a = com.iflytek.cloud.util.a.b.this;
            super(handler);
        }
    }

    private class b extends ContentObserver
    {

        final com.iflytek.cloud.util.a.b a;

        public void onChange(boolean flag)
        {
            com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "ContactObserver_Contact| onChange");
            if (System.currentTimeMillis() - com.iflytek.cloud.util.a.b.c(a) < 5000L)
            {
                com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "onChange too much");
                return;
            } else
            {
                com.iflytek.cloud.util.a.b.a(a, System.currentTimeMillis());
                com.iflytek.cloud.util.a.b.a(a);
                com.iflytek.cloud.util.a.b.b(a);
                return;
            }
        }

        public b(Handler handler)
        {
            a = com.iflytek.cloud.util.a.b.this;
            super(handler);
        }
    }


    private static com.iflytek.cloud.util.a.b a = null;
    private static Context b = null;
    private static int c = 4;
    private static com.iflytek.cloud.util.a.c.a d = null;
    private static com.iflytek.cloud.util.a.a e = null;
    private static b f;
    private static a g;
    private static com.iflytek.cloud.util.ContactManager.ContactListener i = null;
    private HandlerThread h;
    private Handler j;
    private long k;
    private long l;

    private com.iflytek.cloud.util.a.b()
    {
        h = null;
        k = 0L;
        l = 0L;
        if (android.os.Build.VERSION.SDK_INT > c)
        {
            d = new com.iflytek.cloud.util.a.b.b(b);
        } else
        {
            d = new com.iflytek.cloud.util.a.b.a(b);
        }
        e = new com.iflytek.cloud.util.a.a(b, d);
        h = new HandlerThread("ContactManager_worker");
        h.start();
        j = new Handler(h.getLooper());
        h.setPriority(1);
        f = new b(j);
        g = new a(j);
    }

    static long a(com.iflytek.cloud.util.a.b b1, long l1)
    {
        b1.k = l1;
        return l1;
    }

    public static com.iflytek.cloud.util.a.b a()
    {
        return a;
    }

    public static com.iflytek.cloud.util.a.b a(Context context, com.iflytek.cloud.util.ContactManager.ContactListener contactlistener)
    {
        i = contactlistener;
        b = context;
        if (a == null)
        {
            a = new com.iflytek.cloud.util.a.b();
            b.getContentResolver().registerContentObserver(d.a(), true, f);
            b.getContentResolver().registerContentObserver(d.f(), true, g);
        }
        return a;
    }

    static void a(com.iflytek.cloud.util.a.b b1)
    {
        b1.b();
    }

    static long b(com.iflytek.cloud.util.a.b b1, long l1)
    {
        b1.l = l1;
        return l1;
    }

    private void b()
    {
        String s;
        String s1;
        String s2;
        if (i == null || e == null)
        {
            break MISSING_BLOCK_LABEL_119;
        }
        s = com.iflytek.cloud.util.a.e.a(e.a(), '\n');
        s1 = (new StringBuilder()).append(b.getFilesDir().getParent()).append('/').append("name.txt").toString();
        s2 = com.iflytek.cloud.util.a.d.a(s1);
        if (s == null || s2 == null)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        if (s.equals(s2))
        {
            com.iflytek.cloud.a.g.a.a.a("iFly_ContactManager", "contact name is not change.");
            i.onContactQueryFinish(s, false);
            return;
        }
        try
        {
            com.iflytek.cloud.util.a.d.a(s1, s, true);
            i.onContactQueryFinish(s, true);
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    static void b(com.iflytek.cloud.util.a.b b1)
    {
        b1.c();
    }

    static long c(com.iflytek.cloud.util.a.b b1)
    {
        return b1.k;
    }

    private void c()
    {
        if (e != null)
        {
            e.a(10);
        }
    }

    static long d(com.iflytek.cloud.util.a.b b1)
    {
        return b1.l;
    }

    public void asyncQueryAllContactsName()
    {
        j.post(new c(this));
    }

    public String queryAllContactsName()
    {
        if (e != null)
        {
            StringBuilder stringbuilder = new StringBuilder();
            String as[] = e.a();
            int j1 = as.length;
            for (int i1 = 0; i1 < j1; i1++)
            {
                String s = as[i1];
                stringbuilder.append((new StringBuilder()).append(s).append('\n').toString());
            }

            return stringbuilder.toString();
        } else
        {
            return null;
        }
    }

}
