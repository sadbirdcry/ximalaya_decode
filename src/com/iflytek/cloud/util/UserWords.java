// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.util;

import android.text.TextUtils;
import com.iflytek.cloud.a.g.a.a;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class UserWords
{

    private Hashtable a;

    public UserWords()
    {
        a = null;
        a = new Hashtable();
    }

    public UserWords(String s)
    {
        a = null;
        a = new Hashtable();
        a(s);
    }

    private String a()
    {
        if (a != null)
        {
            break MISSING_BLOCK_LABEL_14;
        }
        com.iflytek.cloud.a.g.a.a.a("mwords is null...");
        return null;
        Object obj;
        JSONArray jsonarray;
        Iterator iterator;
        obj = new JSONObject();
        jsonarray = new JSONArray();
        iterator = a.entrySet().iterator();
_L1:
        java.util.Map.Entry entry;
        JSONObject jsonobject;
        JSONArray jsonarray1;
        if (!iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_167;
        }
        entry = (java.util.Map.Entry)iterator.next();
        jsonobject = new JSONObject();
        jsonarray1 = new JSONArray();
        for (Iterator iterator1 = ((ArrayList)entry.getValue()).iterator(); iterator1.hasNext(); jsonarray1.put((String)iterator1.next())) { }
        try
        {
            jsonobject.put("words", jsonarray1);
            jsonobject.put("name", entry.getKey());
            jsonarray.put(jsonobject);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((JSONException) (obj)).printStackTrace();
            return null;
        }
          goto _L1
        ((JSONObject) (obj)).put("userword", jsonarray);
        obj = ((JSONObject) (obj)).toString();
        return ((String) (obj));
    }

    private void a(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            com.iflytek.cloud.a.g.a.a.a("userword is null...");
            return;
        }
        s = (new JSONObject(new JSONTokener(s))).getJSONArray("userword");
        int i = 0;
_L5:
        if (i >= s.length()) goto _L2; else goto _L1
_L1:
        JSONObject jsonobject;
        JSONArray jsonarray;
        ArrayList arraylist;
        jsonobject = (JSONObject)s.get(i);
        jsonarray = jsonobject.getJSONArray("words");
        arraylist = new ArrayList();
        int j = 0;
_L6:
        if (j >= jsonarray.length()) goto _L4; else goto _L3
_L3:
        String s1 = jsonarray.get(j).toString();
        if (arraylist == null)
        {
            break MISSING_BLOCK_LABEL_155;
        }
        try
        {
            if (!arraylist.contains(s1))
            {
                arraylist.add(s1);
            }
            break MISSING_BLOCK_LABEL_155;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
          goto _L2
_L4:
        a.put(jsonobject.get("name").toString(), arraylist);
        i++;
          goto _L5
_L2:
        return;
        j++;
          goto _L6
    }

    private boolean a(ArrayList arraylist, String s)
    {
        if (arraylist != null && !arraylist.contains(s))
        {
            arraylist.add(s);
            return true;
        } else
        {
            return false;
        }
    }

    private boolean a(ArrayList arraylist, ArrayList arraylist1)
    {
        if (arraylist == null || arraylist1 == null)
        {
            return false;
        }
        for (arraylist1 = arraylist1.iterator(); arraylist1.hasNext(); a(arraylist, (String)arraylist1.next())) { }
        return true;
    }

    public ArrayList getKeys()
    {
        if (a != null && !a.isEmpty()) goto _L2; else goto _L1
_L1:
        ArrayList arraylist = null;
_L4:
        return arraylist;
_L2:
        ArrayList arraylist1 = new ArrayList();
        Iterator iterator = a.keySet().iterator();
        do
        {
            arraylist = arraylist1;
            if (!iterator.hasNext())
            {
                continue;
            }
            arraylist1.add(iterator.next());
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
    }

    public ArrayList getWords()
    {
        return getWords("default");
    }

    public ArrayList getWords(String s)
    {
        return (ArrayList)a.get(s);
    }

    public boolean hasKey(String s)
    {
        return a.containsKey(s);
    }

    public boolean putWord(String s)
    {
        return putWord("default", s);
    }

    public boolean putWord(String s, String s1)
    {
        if (TextUtils.isEmpty(s) || TextUtils.isEmpty(s1))
        {
            return false;
        }
        ArrayList arraylist = getWords(s);
        if (arraylist != null)
        {
            a(arraylist, s1);
        } else
        {
            ArrayList arraylist1 = new ArrayList();
            a(arraylist1, s1);
            a.put(s, arraylist1);
        }
        return true;
    }

    public boolean putWords(String s, ArrayList arraylist)
    {
        if (TextUtils.isEmpty(s) || arraylist == null || arraylist.size() <= 0)
        {
            return false;
        }
        ArrayList arraylist1 = getWords(s);
        if (arraylist1 != null)
        {
            a(arraylist1, arraylist);
        } else
        {
            a(new ArrayList(), arraylist);
            a.put(s, arraylist);
        }
        return true;
    }

    public boolean putWords(ArrayList arraylist)
    {
        return putWords("default", arraylist);
    }

    public String toString()
    {
        return a();
    }
}
