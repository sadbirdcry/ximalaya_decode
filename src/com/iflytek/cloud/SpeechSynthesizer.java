// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.cloud.a.d.d;
import com.iflytek.cloud.b.a;
import com.iflytek.cloud.d.a.g;
import com.iflytek.msc.MSC;
import com.iflytek.speech.SpeechSynthesizerAidl;
import com.iflytek.speech.SynthesizerListener;

// Referenced classes of package com.iflytek.cloud:
//            g, SpeechUtility, InitListener, SynthesizerListener, 
//            i, h, SpeechError

public class SpeechSynthesizer extends d
{
    private final class a
        implements com.iflytek.cloud.SynthesizerListener
    {

        final SpeechSynthesizer a;
        private com.iflytek.cloud.SynthesizerListener b;
        private SynthesizerListener c;
        private Handler d;

        static com.iflytek.cloud.SynthesizerListener a(a a1)
        {
            return a1.b;
        }

        static Handler b(a a1)
        {
            return a1.d;
        }

        static SynthesizerListener c(a a1)
        {
            return a1.c;
        }

        public void onBufferProgress(int j, int k, int l, String s)
        {
            if (b != null)
            {
                Bundle bundle = new Bundle();
                bundle.putInt("percent", j);
                bundle.putInt("begpos", k);
                bundle.putInt("endpos", l);
                bundle.putString("spellinfo", s);
                if (b != null)
                {
                    Message.obtain(d, 2, bundle).sendToTarget();
                }
            }
        }

        public void onCompleted(SpeechError speecherror)
        {
            if (b != null)
            {
                Message.obtain(d, 6, speecherror).sendToTarget();
            }
        }

        public void onEvent(int j, int k, int l, Bundle bundle)
        {
            if (b != null)
            {
                Message message = Message.obtain();
                message.what = j;
                message.arg1 = 0;
                message.arg2 = 0;
                message.obj = bundle;
                Message.obtain(d, 7, 0, 0, message).sendToTarget();
            }
        }

        public void onSpeakBegin()
        {
            if (b != null)
            {
                Message.obtain(d, 1).sendToTarget();
            }
        }

        public void onSpeakPaused()
        {
            if (b != null)
            {
                Message.obtain(d, 3).sendToTarget();
            }
        }

        public void onSpeakProgress(int j, int k, int l)
        {
            if (b != null)
            {
                Message.obtain(d, 5, j, k, Integer.valueOf(l)).sendToTarget();
            }
        }

        public void onSpeakResumed()
        {
            if (b != null)
            {
                Message.obtain(d, 4).sendToTarget();
            }
        }

        public a(com.iflytek.cloud.SynthesizerListener synthesizerlistener)
        {
            a = SpeechSynthesizer.this;
            super();
            b = null;
            c = null;
            d = new i(this, Looper.getMainLooper());
            b = synthesizerlistener;
            c = new h(this, SpeechSynthesizer.this);
        }
    }


    private static SpeechSynthesizer c = null;
    InitListener a;
    private g d;
    private SpeechSynthesizerAidl e;
    private a f;
    private Handler g;

    protected SpeechSynthesizer(Context context, InitListener initlistener)
    {
        d = null;
        e = null;
        f = null;
        a = null;
        g = new com.iflytek.cloud.g(this, Looper.getMainLooper());
        a = initlistener;
        if (MSC.isLoaded())
        {
            d = new g(context);
        }
        SpeechUtility speechutility = SpeechUtility.getUtility();
        if (speechutility != null && speechutility.a() && speechutility.getEngineMode() != com.iflytek.cloud.a.d.d.a.a)
        {
            e = new SpeechSynthesizerAidl(context.getApplicationContext(), initlistener);
            return;
        } else
        {
            Message.obtain(g, 0, 0, 0, null).sendToTarget();
            return;
        }
    }

    public static SpeechSynthesizer createSynthesizer(Context context, InitListener initlistener)
    {
        if (c == null)
        {
            c = new SpeechSynthesizer(context, initlistener);
        }
        return c;
    }

    public static SpeechSynthesizer getSynthesizer()
    {
        return c;
    }

    protected void a(Context context)
    {
        SpeechUtility speechutility = SpeechUtility.getUtility();
        if (speechutility != null && speechutility.a() && speechutility.getEngineMode() != com.iflytek.cloud.a.d.d.a.a)
        {
            if (e != null && !e.isAvailable())
            {
                e.destory();
                e = null;
            }
            e = new SpeechSynthesizerAidl(context.getApplicationContext(), a);
        } else
        if (a != null && e != null)
        {
            e.destory();
            e = null;
            return;
        }
    }

    public boolean destroy()
    {
        boolean flag = true;
        if (e != null)
        {
            e.destory();
        }
        if (d != null)
        {
            flag = d.destroy();
        }
        if (flag)
        {
            c = null;
        }
        return flag;
    }

    public String getParameter(String s)
    {
        if ("local_speakers".equals(s) && e != null)
        {
            return e.getParameter(s);
        }
        if ("tts_play_state".equals(s))
        {
            if (a("tts", e) == com.iflytek.cloud.a.d.d.a.b && e != null)
            {
                return e.getParameter(s);
            }
            if (d != null)
            {
                return (new StringBuilder()).append("").append(d.h()).toString();
            }
        }
        return super.getParameter(s);
    }

    public boolean isSpeaking()
    {
        while (d != null && d.g() || e != null && e.isSpeaking()) 
        {
            return true;
        }
        return false;
    }

    public void pauseSpeaking()
    {
        if (d != null && d.g())
        {
            d.e();
        } else
        if (e != null && e.isSpeaking())
        {
            e.pauseSpeaking(a.c(f));
            return;
        }
    }

    public void resumeSpeaking()
    {
        if (d != null && d.g())
        {
            d.f();
        } else
        if (e != null && e.isSpeaking())
        {
            e.resumeSpeaking(a.c(f));
            return;
        }
    }

    public boolean setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public int startSpeaking(String s, com.iflytek.cloud.SynthesizerListener synthesizerlistener)
    {
        if (a("tts", e) != com.iflytek.cloud.a.d.d.a.b) goto _L2; else goto _L1
_L1:
        if (e != null) goto _L4; else goto _L3
_L3:
        return 21001;
_L4:
        e.setParameter("params", null);
        e.setParameter("params", b.toString());
        b.c("next_text");
        f = new a(synthesizerlistener);
        return e.startSpeaking(s, a.c(f));
_L2:
        if (d != null)
        {
            d.setParameter(b);
            b.c("next_text");
            return d.a(s, synthesizerlistener);
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    public void stopSpeaking()
    {
        if (d != null && d.g())
        {
            d.a(false);
        } else
        if (e != null && e.isSpeaking())
        {
            e.stopSpeaking(a.c(f));
            return;
        }
    }

    public int synthesizeToUri(String s, String s1, com.iflytek.cloud.SynthesizerListener synthesizerlistener)
    {
        if (a("tts", e) != com.iflytek.cloud.a.d.d.a.b) goto _L2; else goto _L1
_L1:
        if (e != null) goto _L4; else goto _L3
_L3:
        return 21001;
_L4:
        e.setParameter("params", null);
        e.setParameter("params", b.toString());
        e.setParameter("tts_audio_uri", s1);
        f = new a(synthesizerlistener);
        return e.synthesizeToUrl(s, a.c(f));
_L2:
        if (d != null)
        {
            d.setParameter(b);
            d.a(s, s1, synthesizerlistener);
            return 0;
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

}
