// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Handler;
import android.os.Looper;
import com.iflytek.speech.TextUnderstanderListener;

// Referenced classes of package com.iflytek.cloud:
//            TextUnderstanderListener, TextUnderstander, o, n, 
//            SpeechError, UnderstanderResult

private final class c
    implements com.iflytek.cloud.TextUnderstanderListener
{

    final TextUnderstander a;
    private com.iflytek.cloud.TextUnderstanderListener b;
    private TextUnderstanderListener c;
    private Handler d;

    static TextUnderstanderListener a(tener tener)
    {
        return tener.c;
    }

    static Handler b(c c1)
    {
        return c1.d;
    }

    static com.iflytek.cloud.TextUnderstanderListener c(d d1)
    {
        return d1.b;
    }

    public void onError(SpeechError speecherror)
    {
        speecherror = d.obtainMessage(0, speecherror);
        d.sendMessage(speecherror);
    }

    public void onResult(UnderstanderResult understanderresult)
    {
        understanderresult = d.obtainMessage(4, understanderresult);
        d.sendMessage(understanderresult);
    }

    public tener(TextUnderstander textunderstander, com.iflytek.cloud.TextUnderstanderListener textunderstanderlistener)
    {
        a = textunderstander;
        super();
        b = null;
        c = null;
        d = new o(this, Looper.getMainLooper());
        b = textunderstanderlistener;
        c = new n(this, textunderstander);
    }
}
