// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;


// Referenced classes of package com.iflytek.cloud:
//            SpeechError, UnderstanderResult

public interface TextUnderstanderListener
{

    public abstract void onError(SpeechError speecherror);

    public abstract void onResult(UnderstanderResult understanderresult);
}
