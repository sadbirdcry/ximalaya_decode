// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;
import android.os.Message;
import android.os.RemoteException;

// Referenced classes of package com.iflytek.cloud:
//            SpeechError, SpeechSynthesizer

final class h extends com.iflytek.speech.SynthesizerListener.Stub
{

    final SpeechSynthesizer a;
    final SpeechSynthesizer.a b;

    h(SpeechSynthesizer.a a1, SpeechSynthesizer speechsynthesizer)
    {
        b = a1;
        a = speechsynthesizer;
        super();
    }

    public void onBufferProgress(int i, int j, int k, String s)
        throws RemoteException
    {
        if (SpeechSynthesizer.a.a(b) != null)
        {
            s = new Bundle();
            s.putInt("percent", i);
            s.putInt("begpos", j);
            s.putInt("endpos", k);
            s.putString("spellinfo", "");
            if (SpeechSynthesizer.a.a(b) != null)
            {
                Message.obtain(SpeechSynthesizer.a.b(b), 2, s).sendToTarget();
            }
        }
    }

    public void onCompleted(int i)
        throws RemoteException
    {
        if (SpeechSynthesizer.a.a(b) != null)
        {
            android.os.Handler handler = SpeechSynthesizer.a.b(b);
            SpeechError speecherror;
            if (i == 0)
            {
                speecherror = null;
            } else
            {
                speecherror = new SpeechError(i);
            }
            Message.obtain(handler, 6, speecherror).sendToTarget();
        }
    }

    public void onEvent(int i, int j, int k, Bundle bundle)
        throws RemoteException
    {
        if (SpeechSynthesizer.a.a(b) != null)
        {
            Message message = Message.obtain();
            message.what = i;
            message.arg1 = 0;
            message.arg2 = 0;
            message.obj = bundle;
            Message.obtain(SpeechSynthesizer.a.b(b), 7, 0, 0, message).sendToTarget();
        }
    }

    public void onSpeakBegin()
        throws RemoteException
    {
        if (SpeechSynthesizer.a.a(b) != null)
        {
            Message.obtain(SpeechSynthesizer.a.b(b), 1).sendToTarget();
        }
    }

    public void onSpeakPaused()
        throws RemoteException
    {
        if (SpeechSynthesizer.a.a(b) != null)
        {
            Message.obtain(SpeechSynthesizer.a.b(b), 3).sendToTarget();
        }
    }

    public void onSpeakProgress(int i, int j, int k)
        throws RemoteException
    {
        if (SpeechSynthesizer.a.a(b) != null)
        {
            Message.obtain(SpeechSynthesizer.a.b(b), 5, i, j, Integer.valueOf(k)).sendToTarget();
        }
    }

    public void onSpeakResumed()
        throws RemoteException
    {
        if (SpeechSynthesizer.a.a(b) != null)
        {
            Message.obtain(SpeechSynthesizer.a.b(b), 4, 0, 0, null).sendToTarget();
        }
    }
}
