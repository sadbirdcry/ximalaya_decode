// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.resource;

import android.text.TextUtils;
import java.util.Locale;

// Referenced classes of package com.iflytek.cloud.resource:
//            a, b, c

public class Resource
{

    public static final int TAG_ERROR_CODE = 0;
    public static final int TAG_ERROR_UNKNOWN = 1;
    public static final int TEXT_AGAIN = 9;
    public static final int TEXT_CANCEL = 4;
    public static final int TEXT_DETAIL = 3;
    public static final int TEXT_HELP_LINK = 1;
    public static final int TEXT_HELP_RECO = 13;
    public static final int TEXT_HELP_SMS = 12;
    public static final int TEXT_KNOW = 2;
    public static final int TEXT_MORE = 7;
    public static final int TEXT_PLAYBACK = 10;
    public static final int TEXT_POWER_LINK = 0;
    public static final int TEXT_RETRIEVE = 11;
    public static final int TEXT_RETRY = 8;
    public static final int TEXT_SET = 6;
    public static final int TEXT_STOP = 5;
    public static final int TITLE_AUDIO_PLAYING = 6;
    public static final int TITLE_AUDIO_REQUEST = 4;
    public static final int TITLE_CONNECTING = 1;
    public static final int TITLE_DATA_UPLOAD = 7;
    public static final int TITLE_ERROR = 5;
    public static final int TITLE_HELP = 0;
    public static final int TITLE_RECOGNIZE_WAITING = 3;
    public static final int TITLE_RECORDING = 2;
    private static Locale a;

    private Resource()
    {
    }

    private static boolean a(String s)
    {
        while (TextUtils.isEmpty(s) || Locale.CHINA.toString().equalsIgnoreCase(s) || Locale.CHINESE.toString().equalsIgnoreCase(s)) 
        {
            return true;
        }
        return false;
    }

    public static String getErrorDescription(int i)
    {
        String as[] = a.c;
        if (a.equals(Locale.US))
        {
            as = b.c;
        } else
        if (a.equals(Locale.TRADITIONAL_CHINESE))
        {
            as = c.c;
        }
        if (i > 0 && i < as.length)
        {
            return as[i];
        } else
        {
            return getErrorTag(1);
        }
    }

    public static String getErrorTag(int i)
    {
        String as[] = a.d;
        if (a.equals(Locale.US))
        {
            as = b.d;
        } else
        if (a.equals(Locale.TRADITIONAL_CHINESE))
        {
            as = c.d;
        }
        if (i >= 0 && i < as.length)
        {
            return as[i];
        } else
        {
            return "";
        }
    }

    public static String getLanguage()
    {
        return a.toString();
    }

    public static String getText(int i)
    {
        String as[] = a.a;
        if (a.equals(Locale.US))
        {
            as = b.a;
        } else
        if (a.equals(Locale.TRADITIONAL_CHINESE))
        {
            as = c.a;
        }
        if (i >= 0 && i < as.length)
        {
            return as[i];
        } else
        {
            return "";
        }
    }

    public static String getTitle(int i)
    {
        String as[] = a.b;
        if (a.equals(Locale.US))
        {
            as = b.b;
        } else
        if (a.equals(Locale.TRADITIONAL_CHINESE))
        {
            as = c.b;
        }
        if (i >= 0 && i < as.length)
        {
            return as[i];
        } else
        {
            return "";
        }
    }

    public static boolean matchLanguage(String s)
    {
        String s1 = "";
        if (s != null)
        {
            s1 = s.trim();
        }
        if (a.toString().equalsIgnoreCase(s1))
        {
            return true;
        }
        return a(s1) && a(a.toString());
    }

    public static void setErrorDescription(int i, String s)
    {
        String as[] = a.c;
        if (!a.equals(Locale.US)) goto _L2; else goto _L1
_L1:
        as = b.c;
_L4:
        if (i > 0 && i < as.length)
        {
            as[i] = s;
        }
        return;
_L2:
        if (a.equals(Locale.TRADITIONAL_CHINESE))
        {
            as = c.c;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static void setText(int i, String s)
    {
        String as[] = a.a;
        if (!a.equals(Locale.US)) goto _L2; else goto _L1
_L1:
        as = b.a;
_L4:
        if (i >= 0 && i < as.length)
        {
            as[i] = s;
        }
        return;
_L2:
        if (a.equals(Locale.TRADITIONAL_CHINESE))
        {
            as = c.a;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static void setTitle(int i, String s)
    {
        String as[] = a.b;
        if (!a.equals(Locale.US)) goto _L2; else goto _L1
_L1:
        as = b.b;
_L4:
        if (i >= 0 && i < as.length)
        {
            as[i] = s;
        }
        return;
_L2:
        if (a.equals(Locale.TRADITIONAL_CHINESE))
        {
            as = c.b;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static void setUILanguage(Locale locale)
    {
        while (locale == null || !locale.equals(Locale.US) && !locale.equals(Locale.CHINA) && !locale.equals(Locale.TRADITIONAL_CHINESE)) 
        {
            return;
        }
        a = locale;
    }

    static 
    {
        a = Locale.CHINA;
    }
}
