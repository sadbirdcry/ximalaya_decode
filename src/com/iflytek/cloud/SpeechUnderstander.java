// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.cloud.a.d.d;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.d.a.h;
import com.iflytek.msc.MSC;
import com.iflytek.speech.SpeechUnderstanderAidl;
import com.iflytek.speech.SpeechUnderstanderListener;

// Referenced classes of package com.iflytek.cloud:
//            j, SpeechUtility, InitListener, SpeechUnderstanderListener, 
//            l, k, SpeechError, UnderstanderResult

public class SpeechUnderstander extends d
{
    private final class a
        implements com.iflytek.cloud.SpeechUnderstanderListener
    {

        final SpeechUnderstander a;
        private com.iflytek.cloud.SpeechUnderstanderListener b;
        private SpeechUnderstanderListener c;
        private Handler d;

        static SpeechUnderstanderListener a(a a1)
        {
            return a1.c;
        }

        static Handler b(a a1)
        {
            return a1.d;
        }

        static com.iflytek.cloud.SpeechUnderstanderListener c(a a1)
        {
            return a1.b;
        }

        public void onBeginOfSpeech()
        {
            Message message = d.obtainMessage(2, 0, 0, null);
            d.sendMessage(message);
        }

        public void onEndOfSpeech()
        {
            Message message = d.obtainMessage(3, 0, 0, null);
            d.sendMessage(message);
        }

        public void onError(SpeechError speecherror)
        {
            speecherror = d.obtainMessage(0, speecherror);
            d.sendMessage(speecherror);
        }

        public void onEvent(int i, int i1, int j1, Bundle bundle)
        {
            Message message = new Message();
            message.what = i;
            message.arg1 = i1;
            message.arg2 = j1;
            message.obj = bundle;
            bundle = d.obtainMessage(6, 0, 0, message);
            d.sendMessage(bundle);
        }

        public void onResult(UnderstanderResult understanderresult)
        {
            understanderresult = d.obtainMessage(4, understanderresult);
            d.sendMessage(understanderresult);
        }

        public void onVolumeChanged(int i)
        {
            Message message = d.obtainMessage(1, i, 0, null);
            d.sendMessage(message);
        }

        public a(com.iflytek.cloud.SpeechUnderstanderListener speechunderstanderlistener)
        {
            a = SpeechUnderstander.this;
            super();
            b = null;
            c = null;
            d = new l(this, Looper.getMainLooper());
            b = speechunderstanderlistener;
            c = new k(this, SpeechUnderstander.this);
        }
    }


    protected static SpeechUnderstander a = null;
    private h c;
    private SpeechUnderstanderAidl d;
    private a e;
    private InitListener f;
    private Handler g;

    protected SpeechUnderstander(Context context, InitListener initlistener)
    {
        c = null;
        d = null;
        e = null;
        f = null;
        g = new j(this, Looper.getMainLooper());
        f = initlistener;
        if (MSC.isLoaded())
        {
            c = new h(context);
        }
        SpeechUtility speechutility = SpeechUtility.getUtility();
        if (speechutility != null && speechutility.a() && speechutility.getEngineMode() != com.iflytek.cloud.a.d.d.a.a)
        {
            d = new SpeechUnderstanderAidl(context.getApplicationContext(), initlistener);
        } else
        if (initlistener != null)
        {
            Message.obtain(g, 0, 0, 0, null).sendToTarget();
            return;
        }
    }

    static InitListener a(SpeechUnderstander speechunderstander)
    {
        return speechunderstander.f;
    }

    public static SpeechUnderstander createUnderstander(Context context, InitListener initlistener)
    {
        com/iflytek/cloud/SpeechUnderstander;
        JVM INSTR monitorenter ;
        if (a == null)
        {
            a = new SpeechUnderstander(context, initlistener);
        }
        context = a;
        com/iflytek/cloud/SpeechUnderstander;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public static SpeechUnderstander getUnderstander()
    {
        return a;
    }

    protected void a(Context context)
    {
        SpeechUtility speechutility = SpeechUtility.getUtility();
        if (speechutility != null && speechutility.a() && speechutility.getEngineMode() != com.iflytek.cloud.a.d.d.a.a)
        {
            if (d != null && !d.isAvailable())
            {
                d.destory();
                d = null;
            }
            d = new SpeechUnderstanderAidl(context.getApplicationContext(), f);
        } else
        if (f != null && d != null)
        {
            d.destory();
            d = null;
            return;
        }
    }

    public void cancel()
    {
        if (c != null && c.a())
        {
            c.a(false);
            return;
        }
        if (d != null && d.isUnderstanding())
        {
            d.cancel(com.iflytek.cloud.a.a(e));
            return;
        } else
        {
            com.iflytek.cloud.a.g.a.a.b("SpeechUnderstander cancel failed, is not running");
            return;
        }
    }

    public boolean destroy()
    {
        boolean flag = true;
        if (d != null)
        {
            d.destory();
            d = null;
        }
        if (c != null)
        {
            flag = c.c();
        }
        if (flag)
        {
            a = null;
        }
        return flag;
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public boolean isUnderstanding()
    {
        while (c != null && c.a() || d != null && d.isUnderstanding()) 
        {
            return true;
        }
        return false;
    }

    public boolean setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public int startUnderstanding(com.iflytek.cloud.SpeechUnderstanderListener speechunderstanderlistener)
    {
        com.iflytek.cloud.a.d.d.a a1;
        a1 = a("nlu", d);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("start engine mode = ").append(a1.toString()).toString());
        if (a1 != com.iflytek.cloud.a.d.d.a.b) goto _L2; else goto _L1
_L1:
        if (d != null) goto _L4; else goto _L3
_L3:
        return 21001;
_L4:
        d.setParameter("params", null);
        d.setParameter("params", b.toString());
        e = new a(speechunderstanderlistener);
        return d.startUnderstanding(com.iflytek.cloud.a.a(e));
_L2:
        if (c != null)
        {
            c.a(b);
            return c.a(speechunderstanderlistener);
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    public void stopUnderstanding()
    {
        if (c != null && c.a())
        {
            c.b();
            return;
        }
        if (d != null && d.isUnderstanding())
        {
            d.stopUnderstanding(com.iflytek.cloud.a.a(e));
            return;
        } else
        {
            com.iflytek.cloud.a.g.a.a.a("SpeechUnderstander stopUnderstanding, is not understanding");
            return;
        }
    }

    public int writeAudio(byte abyte0[], int i, int k)
    {
        if (c != null && c.a())
        {
            return c.a(abyte0, i, k);
        }
        if (d != null && d.isUnderstanding())
        {
            return d.writeAudio(abyte0, i, k);
        } else
        {
            com.iflytek.cloud.a.g.a.a.a("SpeechUnderstander writeAudio, is not understanding");
            return 21004;
        }
    }

}
