// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.d.a;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.iflytek.cloud.DataDownloader;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechListener;
import com.iflytek.cloud.VerifierListener;
import com.iflytek.cloud.VerifierResult;
import com.iflytek.cloud.a.b.c;
import com.iflytek.cloud.a.d.e;
import com.iflytek.cloud.a.g.f;
import java.util.Random;

// Referenced classes of package com.iflytek.cloud.d.a:
//            b

public class com.iflytek.cloud.d.a.a extends e
{
    private final class a
        implements VerifierListener
    {

        final com.iflytek.cloud.d.a.a a;
        private VerifierListener b;
        private Handler c;

        static VerifierListener a(a a1)
        {
            return a1.b;
        }

        protected void a()
        {
            String s = com.iflytek.cloud.d.a.a.a(a).t().e("isv_audio_path");
            if (!TextUtils.isEmpty(s))
            {
                com.iflytek.cloud.a.g.e.a(((c)com.iflytek.cloud.d.a.a.b(a)).f(), s);
            }
            com.iflytek.cloud.a.g.f.b(com.iflytek.cloud.d.a.a.c(a), Boolean.valueOf(com.iflytek.cloud.d.a.a.d(a)), null);
        }

        public void onBeginOfSpeech()
        {
            android.os.Message message = c.obtainMessage(2, 0, 0, null);
            c.sendMessage(message);
        }

        public void onEndOfSpeech()
        {
            android.os.Message message = c.obtainMessage(3, 0, 0, null);
            c.sendMessage(message);
        }

        public void onError(SpeechError speecherror)
        {
            a();
            speecherror = c.obtainMessage(0, speecherror);
            c.sendMessage(speecherror);
        }

        public void onEvent(int i, int j, int k, Bundle bundle)
        {
        }

        public void onResult(VerifierResult verifierresult)
        {
            a();
            verifierresult = c.obtainMessage(4, verifierresult);
            c.sendMessage(verifierresult);
        }

        public void onVolumeChanged(int i)
        {
            android.os.Message message = c.obtainMessage(1, i, 0, null);
            c.sendMessage(message);
        }

        public a(VerifierListener verifierlistener)
        {
            a = com.iflytek.cloud.d.a.a.this;
            super();
            b = null;
            c = new b(this, Looper.getMainLooper());
            b = verifierlistener;
        }
    }


    private boolean f;

    public com.iflytek.cloud.d.a.a(Context context)
    {
        super(context);
        f = false;
    }

    static com.iflytek.cloud.a.d.a a(com.iflytek.cloud.d.a.a a1)
    {
        return a1.d;
    }

    static com.iflytek.cloud.a.d.a b(com.iflytek.cloud.d.a.a a1)
    {
        return a1.d;
    }

    static Context c(com.iflytek.cloud.d.a.a a1)
    {
        return a1.a;
    }

    static boolean d(com.iflytek.cloud.d.a.a a1)
    {
        return a1.f;
    }

    public int a(byte abyte0[], int i, int j)
    {
label0:
        {
            synchronized (c)
            {
                if (d != null)
                {
                    break label0;
                }
                com.iflytek.cloud.a.g.a.a.a("writeAudio error, no active session.");
            }
            return 21004;
        }
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_37;
        }
        if (abyte0.length > 0)
        {
            break MISSING_BLOCK_LABEL_55;
        }
        com.iflytek.cloud.a.g.a.a.a("writeAudio error,buffer is null.");
        obj;
        JVM INSTR monitorexit ;
        return 10109;
        abyte0;
        obj;
        JVM INSTR monitorexit ;
        throw abyte0;
        if (abyte0.length >= j + i)
        {
            break MISSING_BLOCK_LABEL_75;
        }
        com.iflytek.cloud.a.g.a.a.a("writeAudio error,buffer length < length.");
        obj;
        JVM INSTR monitorexit ;
        return 10109;
        if (((c)d).g() == -1)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        obj;
        JVM INSTR monitorexit ;
        return 10106;
        ((c)d).a(abyte0, i, j);
        obj;
        JVM INSTR monitorexit ;
        return 0;
    }

    public String a(int i)
    {
        StringBuffer stringbuffer = new StringBuffer();
        Random random = new Random();
        String s = (new StringBuilder()).append("023456789".charAt(random.nextInt("023456789".length()))).append("").toString();
        stringbuffer.append(s);
        for (int j = 0; j < i - 1; j++)
        {
            for (Boolean boolean1 = Boolean.valueOf(false); !boolean1.booleanValue();)
            {
                s = (new StringBuilder()).append("023456789".charAt(random.nextInt("023456789".length()))).append("").toString();
                if (stringbuffer.indexOf(s) >= 0)
                {
                    boolean1 = Boolean.valueOf(false);
                } else
                if (Integer.parseInt((new StringBuilder()).append(stringbuffer.charAt(stringbuffer.length() - 1)).append("").toString()) * Integer.parseInt(s) == 10)
                {
                    boolean1 = Boolean.valueOf(false);
                } else
                {
                    boolean1 = Boolean.valueOf(true);
                }
            }

            stringbuffer.append(s);
        }

        return stringbuffer.toString();
    }

    public void a(SpeechListener speechlistener)
    {
        DataDownloader datadownloader = new DataDownloader(a);
        datadownloader.setParameter(b);
        datadownloader.downloadData(speechlistener);
    }

    public void a(VerifierListener verifierlistener)
    {
        synchronized (c)
        {
            f = b.a("request_audio_focus", true);
            if (d != null && d.r())
            {
                d.b(b.a("isv_interrupt_error", false));
            }
            d = new c(a, b, a("verify"));
            com.iflytek.cloud.a.g.f.a(a, Boolean.valueOf(f), null);
            ((c)d).a(new a(verifierlistener));
        }
        return;
        verifierlistener;
        obj;
        JVM INSTR monitorexit ;
        throw verifierlistener;
    }

    public void a(String s, String s1, SpeechListener speechlistener)
    {
        b.a("cmd", s);
        b.a("auth_id", s1);
        (new com.iflytek.cloud.a.b.a(a, a("manager"))).a(b, new com.iflytek.cloud.a.b.a(speechlistener));
    }

    public void e()
    {
        synchronized (c)
        {
            if (d != null)
            {
                ((c)d).a();
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean f()
    {
        return d();
    }
}
