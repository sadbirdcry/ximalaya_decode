// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.d.a;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechUnderstanderListener;
import com.iflytek.cloud.UnderstanderResult;
import com.iflytek.cloud.b.a;

// Referenced classes of package com.iflytek.cloud.d.a:
//            c

public class h
{
    private class a
        implements RecognizerListener
    {

        final h a;
        private final SpeechUnderstanderListener b;

        public void onBeginOfSpeech()
        {
            if (b != null)
            {
                b.onBeginOfSpeech();
            }
        }

        public void onEndOfSpeech()
        {
            if (b != null)
            {
                b.onEndOfSpeech();
            }
        }

        public void onError(SpeechError speecherror)
        {
            if (b != null && speecherror != null)
            {
                b.onError(speecherror);
            }
        }

        public void onEvent(int i, int j, int k, Bundle bundle)
        {
            if (b != null)
            {
                b.onEvent(i, j, k, bundle);
            }
        }

        public void onResult(RecognizerResult recognizerresult, boolean flag)
        {
            if (b != null)
            {
                b.onResult(new UnderstanderResult(recognizerresult.getResultString()));
            }
        }

        public void onVolumeChanged(int i)
        {
            if (b != null)
            {
                b.onVolumeChanged(i);
            }
        }

        public a(SpeechUnderstanderListener speechunderstanderlistener)
        {
            a = h.this;
            super();
            b = speechunderstanderlistener;
        }
    }


    protected static h a = null;
    private c b;

    public h(Context context)
    {
        b = null;
        b = new c(context);
    }

    public int a(SpeechUnderstanderListener speechunderstanderlistener)
    {
        speechunderstanderlistener = new a(speechunderstanderlistener);
        if (TextUtils.isEmpty(b.getParameter("asr_sch")))
        {
            b.setParameter("asr_sch", "1");
        }
        if (TextUtils.isEmpty(b.getParameter("nlp_version")))
        {
            b.setParameter("nlp_version", "2.0");
        }
        if (TextUtils.isEmpty(b.getParameter("result_type")))
        {
            b.setParameter("result_type", "json");
        }
        b.a(speechunderstanderlistener);
        return 0;
    }

    public int a(byte abyte0[], int i, int j)
    {
        return b.a(abyte0, i, j);
    }

    public void a(boolean flag)
    {
        b.cancel(flag);
    }

    public boolean a()
    {
        return b.f();
    }

    public boolean a(com.iflytek.cloud.b.a a1)
    {
        return b.setParameter(a1);
    }

    public void b()
    {
        b.e();
    }

    public boolean c()
    {
        boolean flag = b.destroy();
        if (flag)
        {
            a = null;
        }
        return flag;
    }

}
