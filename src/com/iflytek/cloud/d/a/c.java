// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.d.a;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.iflytek.cloud.DataUploader;
import com.iflytek.cloud.GrammarListener;
import com.iflytek.cloud.LexiconListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.a.b;
import com.iflytek.cloud.a.d.e;
import com.iflytek.cloud.a.g.f;
import com.iflytek.cloud.b.a;

// Referenced classes of package com.iflytek.cloud.d.a:
//            d, e, f

public class c extends e
{
    private final class a
        implements RecognizerListener
    {

        final c a;
        private RecognizerListener b;
        private boolean c;
        private Handler d;

        static RecognizerListener a(a a1)
        {
            return a1.b;
        }

        static boolean a(a a1, boolean flag)
        {
            a1.c = flag;
            return flag;
        }

        static boolean b(a a1)
        {
            return a1.c;
        }

        protected void a()
        {
            String s = com.iflytek.cloud.d.a.c.a(a).t().e("asr_audio_path");
            if (!TextUtils.isEmpty(s))
            {
                com.iflytek.cloud.a.g.e.a(((b)com.iflytek.cloud.d.a.c.b(a)).b(), s);
            }
            com.iflytek.cloud.a.g.f.b(c.c(a), Boolean.valueOf(c.d(a)), null);
        }

        public void onBeginOfSpeech()
        {
            com.iflytek.cloud.a.g.a.a.a("onBeginOfSpeech");
            Message message = d.obtainMessage(2, 0, 0, null);
            d.sendMessage(message);
        }

        public void onEndOfSpeech()
        {
            Message message = d.obtainMessage(3, 0, 0, null);
            d.sendMessage(message);
        }

        public void onError(SpeechError speecherror)
        {
            a();
            speecherror = d.obtainMessage(0, speecherror);
            d.sendMessage(speecherror);
        }

        public void onEvent(int i, int j, int k, Bundle bundle)
        {
            Message message = new Message();
            message.what = i;
            message.arg1 = j;
            message.arg2 = k;
            message.obj = bundle;
            bundle = d.obtainMessage(6, 0, 0, message);
            d.sendMessage(bundle);
        }

        public void onResult(RecognizerResult recognizerresult, boolean flag)
        {
            int i = 1;
            if (flag)
            {
                a();
            }
            Handler handler = d;
            if (!flag)
            {
                i = 0;
            }
            recognizerresult = handler.obtainMessage(4, i, 0, recognizerresult);
            d.sendMessage(recognizerresult);
        }

        public void onVolumeChanged(int i)
        {
            Message message = d.obtainMessage(1, i, 0, null);
            d.sendMessage(message);
        }

        public a(RecognizerListener recognizerlistener)
        {
            a = c.this;
            super();
            b = null;
            c = false;
            d = new com.iflytek.cloud.d.a.f(this, Looper.getMainLooper());
            b = recognizerlistener;
        }
    }


    private boolean f;

    public c(Context context)
    {
        super(context);
        f = false;
    }

    static com.iflytek.cloud.a.d.a a(c c1)
    {
        return c1.d;
    }

    static com.iflytek.cloud.a.d.a b(c c1)
    {
        return c1.d;
    }

    static Context c(c c1)
    {
        return c1.a;
    }

    static boolean d(c c1)
    {
        return c1.f;
    }

    public int a(RecognizerListener recognizerlistener)
    {
        synchronized (c)
        {
            f = b.a("request_audio_focus", true);
            if (d != null && d.r())
            {
                d.b(b.a("asr_interrupt_error", false));
            }
            d = new b(a, b, a("iat"));
            com.iflytek.cloud.a.g.f.a(a, Boolean.valueOf(f), null);
            ((b)d).a(new a(recognizerlistener));
        }
        return 0;
        recognizerlistener;
        obj;
        JVM INSTR monitorexit ;
        throw recognizerlistener;
    }

    public int a(String s, String s1, GrammarListener grammarlistener)
    {
        char c2 = '\u4E2C';
        if (!TextUtils.isEmpty(s1)) goto _L2; else goto _L1
_L1:
        char c1 = '\u4E29';
_L4:
        return c1;
_L2:
        c1 = c2;
        if (TextUtils.isEmpty(s)) goto _L4; else goto _L3
_L3:
        c1 = c2;
        if (grammarlistener == null) goto _L4; else goto _L5
_L5:
        DataUploader datauploader;
        d d1;
        String s2;
        datauploader = new DataUploader(a);
        d1 = new d(this, grammarlistener);
        datauploader.setParameter(b);
        datauploader.setParameter("subject", "asr");
        datauploader.setParameter("data_type", s);
        s2 = b.b("text_encoding", "utf-8");
        grammarlistener = getParameter("grammar_name");
        if (!TextUtils.isEmpty(grammarlistener))
        {
            s = grammarlistener;
        }
        datauploader.uploadData(d1, s, s1.getBytes(s2));
        return 0;
        s;
        s.printStackTrace();
        return 20012;
    }

    public int a(String s, String s1, LexiconListener lexiconlistener)
    {
        if (TextUtils.isEmpty(s1))
        {
            return 20009;
        }
        if (TextUtils.isEmpty(s))
        {
            return 20012;
        }
        if (lexiconlistener == null)
        {
            return 20012;
        }
        DataUploader datauploader = new DataUploader(a);
        com.iflytek.cloud.d.a.e e1 = new com.iflytek.cloud.d.a.e(this, lexiconlistener);
        datauploader.setParameter(b);
        datauploader.setParameter("subject", "uup");
        String s2 = getParameter("lexicon_type");
        lexiconlistener = s2;
        if (TextUtils.isEmpty(s2))
        {
            lexiconlistener = s;
        }
        datauploader.setParameter("data_type", lexiconlistener);
        lexiconlistener = b.b("text_encoding", "utf-8");
        try
        {
            datauploader.uploadData(e1, s, s1.getBytes(lexiconlistener));
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return 20012;
        }
        return 0;
    }

    public int a(byte abyte0[], int i, int j)
    {
label0:
        {
            synchronized (c)
            {
                if (d != null)
                {
                    break label0;
                }
                com.iflytek.cloud.a.g.a.a.a("writeAudio error, no active session.");
            }
            return 21004;
        }
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_37;
        }
        if (abyte0.length > 0)
        {
            break MISSING_BLOCK_LABEL_55;
        }
        com.iflytek.cloud.a.g.a.a.a("writeAudio error,buffer is null.");
        obj;
        JVM INSTR monitorexit ;
        return 10109;
        abyte0;
        obj;
        JVM INSTR monitorexit ;
        throw abyte0;
        if (abyte0.length >= j + i)
        {
            break MISSING_BLOCK_LABEL_75;
        }
        com.iflytek.cloud.a.g.a.a.a("writeAudio error,buffer length < length.");
        obj;
        JVM INSTR monitorexit ;
        return 10109;
        if (((b)d).a() == -1)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        obj;
        JVM INSTR monitorexit ;
        return 10106;
        ((b)d).a(abyte0, i, j);
        obj;
        JVM INSTR monitorexit ;
        return 0;
    }

    public void b(String s)
    {
        synchronized (c)
        {
            if (d != null)
            {
                ((b)d).j().a(s);
            }
        }
        return;
        s;
        obj;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void cancel(boolean flag)
    {
        com.iflytek.cloud.a.g.f.b(a, Boolean.valueOf(f), null);
        super.cancel(flag);
    }

    public void e()
    {
        synchronized (c)
        {
            if (d != null)
            {
                ((b)d).a(true);
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean f()
    {
        return d();
    }
}
