// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.d.a;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.WakeuperListener;
import com.iflytek.cloud.WakeuperResult;
import com.iflytek.cloud.a.c.b;
import com.iflytek.cloud.a.d.e;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.a.g.f;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.iflytek.cloud.d.a:
//            k

public class j extends e
{
    private final class a
        implements WakeuperListener
    {

        final j a;
        private WakeuperListener b;
        private Handler c;

        static WakeuperListener a(a a1)
        {
            return a1.b;
        }

        protected void a()
        {
            com.iflytek.cloud.a.g.f.b(com.iflytek.cloud.d.a.j.a(a), Boolean.valueOf(com.iflytek.cloud.d.a.j.b(a)), null);
        }

        public void onBeginOfSpeech()
        {
            com.iflytek.cloud.a.g.a.a.a("onBeginOfSpeech");
            Message message = c.obtainMessage(2, 0, 0, null);
            c.sendMessage(message);
        }

        public void onError(SpeechError speecherror)
        {
            a();
            speecherror = c.obtainMessage(0, speecherror);
            c.sendMessage(speecherror);
        }

        public void onEvent(int i, int l, int i1, Bundle bundle)
        {
            Message message = Message.obtain();
            message.what = i;
            message.arg1 = l;
            message.arg2 = i1;
            message.obj = bundle;
            bundle = c.obtainMessage(6, 0, 0, message);
            c.sendMessage(bundle);
        }

        public void onResult(WakeuperResult wakeuperresult)
        {
            JSONObject jsonobject = new JSONObject(wakeuperresult.getResultString());
            if (!jsonobject.getString("sst").equals("enroll")) goto _L2; else goto _L1
_L1:
            if (jsonobject.getString("status").equals("done"))
            {
                a();
            }
_L4:
            wakeuperresult = c.obtainMessage(4, 1, 0, wakeuperresult);
            c.sendMessage(wakeuperresult);
            return;
_L2:
            try
            {
                a();
            }
            catch (JSONException jsonexception)
            {
                jsonexception.printStackTrace();
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        public a(WakeuperListener wakeuperlistener)
        {
            a = j.this;
            super();
            b = null;
            c = new k(this, Looper.getMainLooper());
            b = wakeuperlistener;
        }
    }


    private boolean f;

    public j(Context context)
    {
        super(context);
        f = false;
    }

    static Context a(j j1)
    {
        return j1.a;
    }

    static boolean b(j j1)
    {
        return j1.f;
    }

    public int a(byte abyte0[], int i, int k)
    {
label0:
        {
            synchronized (c)
            {
                if (d != null)
                {
                    break label0;
                }
                com.iflytek.cloud.a.g.a.a.a("writeAudio error, no active session.");
            }
            return 21004;
        }
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_37;
        }
        if (abyte0.length > 0)
        {
            break MISSING_BLOCK_LABEL_55;
        }
        com.iflytek.cloud.a.g.a.a.a("writeAudio error,buffer is null.");
        obj;
        JVM INSTR monitorexit ;
        return 10109;
        abyte0;
        obj;
        JVM INSTR monitorexit ;
        throw abyte0;
        if (abyte0.length >= k + i)
        {
            break MISSING_BLOCK_LABEL_75;
        }
        com.iflytek.cloud.a.g.a.a.a("writeAudio error,buffer length < length.");
        obj;
        JVM INSTR monitorexit ;
        return 10109;
        if (((b)d).a() == -1)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        obj;
        JVM INSTR monitorexit ;
        return 10106;
        ((b)d).a(abyte0, i, k);
        obj;
        JVM INSTR monitorexit ;
        return 0;
    }

    public void a(WakeuperListener wakeuperlistener)
    {
        synchronized (c)
        {
            f = b.a("request_audio_focus", true);
            if (d != null && d.r())
            {
                ((b)d).b(false);
            }
            d = new b(a, b, a("wakeuper"));
            com.iflytek.cloud.a.g.f.a(a, Boolean.valueOf(f), null);
            ((b)d).a(new a(wakeuperlistener));
        }
        return;
        wakeuperlistener;
        obj;
        JVM INSTR monitorexit ;
        throw wakeuperlistener;
    }

    public void cancel(boolean flag)
    {
        com.iflytek.cloud.a.g.f.b(a, Boolean.valueOf(f), null);
        super.cancel(flag);
    }

    public void e()
    {
        synchronized (c)
        {
            if (d != null)
            {
                ((b)d).a(true);
            }
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean f()
    {
        return d();
    }
}
