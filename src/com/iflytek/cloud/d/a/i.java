// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.d.a;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechListener;
import com.iflytek.cloud.TextUnderstanderListener;
import com.iflytek.cloud.UnderstanderResult;
import com.iflytek.cloud.a.d.e;
import com.iflytek.cloud.a.e.b;
import java.io.UnsupportedEncodingException;

public class i extends e
{
    private class a
        implements SpeechListener
    {

        final i a;
        private TextUnderstanderListener b;

        public void onBufferReceived(byte abyte0[])
        {
            if (abyte0 == null)
            {
                break MISSING_BLOCK_LABEL_32;
            }
            abyte0 = new UnderstanderResult(new String(abyte0, "utf-8"));
            b.onResult(abyte0);
            return;
            abyte0;
            abyte0.printStackTrace();
            return;
            abyte0;
            abyte0.printStackTrace();
            return;
        }

        public void onCompleted(SpeechError speecherror)
        {
            if (b != null && speecherror != null)
            {
                b.onError(speecherror);
            }
        }

        public void onEvent(int j, Bundle bundle)
        {
        }

        public a(TextUnderstanderListener textunderstanderlistener)
        {
            a = i.this;
            super();
            b = textunderstanderlistener;
        }
    }


    public i(Context context)
    {
        super(context);
    }

    public int a(String s, TextUnderstanderListener textunderstanderlistener)
    {
        if (TextUtils.isEmpty(getParameter("asr_sch")))
        {
            setParameter("asr_sch", "1");
        }
        if (TextUtils.isEmpty(getParameter("nlp_version")))
        {
            setParameter("nlp_version", "2.0");
        }
        if (TextUtils.isEmpty(getParameter("result_type")))
        {
            setParameter("result_type", "json");
        }
        d = new b(a, b, a("textunderstand"));
        textunderstanderlistener = new a(textunderstanderlistener);
        ((b)d).a(new com.iflytek.cloud.a.d.e.a(this, textunderstanderlistener), s);
        return 0;
    }

    public void cancel(boolean flag)
    {
        super.cancel(flag);
    }

    public boolean destroy()
    {
        return super.destroy();
    }

    public boolean e()
    {
        return d();
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public boolean setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }
}
