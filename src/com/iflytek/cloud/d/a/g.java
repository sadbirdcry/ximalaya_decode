// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.d.a;

import android.content.Context;
import android.text.TextUtils;
import com.iflytek.cloud.SynthesizerListener;
import com.iflytek.cloud.a.d.e;
import com.iflytek.cloud.a.f.c;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.a.g.a.b;

public class g extends e
    implements com.iflytek.cloud.a.f.c.a
{

    private Context f;
    private c g;
    private c h;

    public g(Context context)
    {
        super(context);
        f = null;
        g = null;
        h = null;
        f = context.getApplicationContext();
    }

    private void a(String s, SynthesizerListener synthesizerlistener, String s1)
    {
        com.iflytek.cloud.a.g.a.a.a("new Session Start");
        g = new c(f);
        g.a(this);
        g.a(s, b, synthesizerlistener, true);
        if (!TextUtils.isEmpty(s1))
        {
            h = new c(f);
            h.a(this);
            h.a(s1, b);
        }
    }

    public int a(String s, SynthesizerListener synthesizerlistener)
    {
        com.iflytek.cloud.a.g.a.a.a("startSpeaking enter");
        this;
        JVM INSTR monitorenter ;
        String s1;
        s1 = b.d("next_text");
        b.a("QTTSStart", null);
        if (g != null && g.h())
        {
            g.cancel(b.a("tts_interrupt_error", false));
        }
        if (h != null) goto _L2; else goto _L1
_L1:
        a(s, synthesizerlistener, s1);
_L3:
        this;
        JVM INSTR monitorexit ;
        com.iflytek.cloud.a.g.a.a.a("startSpeaking leave");
        return 0;
_L2:
        if (s.equals(h.h))
        {
            break MISSING_BLOCK_LABEL_122;
        }
        h.cancel(false);
        h = null;
        a(s, synthesizerlistener, s1);
          goto _L3
        s;
        this;
        JVM INSTR monitorexit ;
        throw s;
label0:
        {
            if (h.i == null && h.f)
            {
                break label0;
            }
            h.cancel(false);
            h = null;
            a(s, synthesizerlistener, s1);
        }
          goto _L3
        s = h;
        h = null;
        if (!TextUtils.isEmpty(s1))
        {
            h = new c(f);
            h.a(this);
            h.a(s1, b);
        }
        g = s;
        g.a(synthesizerlistener);
        g.i();
        if (g.g)
        {
            a();
            com.iflytek.cloud.a.g.a.a.a("startSpeaking NextSession pause");
        }
          goto _L3
    }

    public void a()
    {
        this;
        JVM INSTR monitorenter ;
        if (h != null)
        {
            h.e();
        }
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void a(String s, String s1, SynthesizerListener synthesizerlistener)
    {
        com.iflytek.cloud.a.g.a.a.a("synthesizeToUri enter");
        this;
        JVM INSTR monitorenter ;
        if (g != null && g.h())
        {
            g.cancel(b.a("tts_interrupt_error", false));
        }
        g = new c(f);
        g.a(s, s1, b, synthesizerlistener);
        this;
        JVM INSTR monitorexit ;
        com.iflytek.cloud.a.g.a.a.a("synthesizeToUri leave");
        return;
        s;
        this;
        JVM INSTR monitorexit ;
        throw s;
    }

    public void a(boolean flag)
    {
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("stopSpeaking enter:").append(flag).toString());
        this;
        JVM INSTR monitorenter ;
        if (g != null)
        {
            com.iflytek.cloud.a.g.a.a.a("-->stopSpeaking cur");
            g.cancel(flag);
            g = null;
        }
        if (h != null)
        {
            com.iflytek.cloud.a.g.a.a.a("-->stopSpeaking cur next");
            h.cancel(false);
            h = null;
        }
        this;
        JVM INSTR monitorexit ;
        com.iflytek.cloud.a.g.a.a.a("stopSpeaking leave");
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean destroy()
    {
        a(false);
        super.destroy();
        return true;
    }

    public void e()
    {
        com.iflytek.cloud.a.g.a.a.a("pauseSpeaking enter");
        this;
        JVM INSTR monitorenter ;
        if (g != null)
        {
            g.g();
        }
        this;
        JVM INSTR monitorexit ;
        com.iflytek.cloud.a.g.a.a.a("pauseSpeaking leave");
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void f()
    {
        com.iflytek.cloud.a.g.a.a.a("resumeSpeaking enter");
        this;
        JVM INSTR monitorenter ;
        if (g != null)
        {
            g.i();
        }
        this;
        JVM INSTR monitorexit ;
        com.iflytek.cloud.a.g.a.a.a("resumeSpeaking leave");
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean g()
    {
        boolean flag;
        flag = false;
        com.iflytek.cloud.a.g.a.a.a("isSpeaking enter");
        this;
        JVM INSTR monitorenter ;
        if (g != null)
        {
            flag = g.h();
        }
        this;
        JVM INSTR monitorexit ;
        com.iflytek.cloud.a.g.a.a.a("isSpeaking leave");
        return flag;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public int h()
    {
        int i;
        i = 4;
        com.iflytek.cloud.a.g.a.a.a("getState enter");
        this;
        JVM INSTR monitorenter ;
        if (g != null)
        {
            i = g.f();
        }
        this;
        JVM INSTR monitorexit ;
        com.iflytek.cloud.a.g.a.a.a("getState leave");
        return i;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }
}
