// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.speech.SpeechUnderstanderListener;

// Referenced classes of package com.iflytek.cloud:
//            SpeechUnderstanderListener, SpeechUnderstander, l, k, 
//            SpeechError, UnderstanderResult

private final class c
    implements com.iflytek.cloud.SpeechUnderstanderListener
{

    final SpeechUnderstander a;
    private com.iflytek.cloud.SpeechUnderstanderListener b;
    private SpeechUnderstanderListener c;
    private Handler d;

    static SpeechUnderstanderListener a(tener tener)
    {
        return tener.c;
    }

    static Handler b(c c1)
    {
        return c1.d;
    }

    static com.iflytek.cloud.SpeechUnderstanderListener c(d d1)
    {
        return d1.b;
    }

    public void onBeginOfSpeech()
    {
        Message message = d.obtainMessage(2, 0, 0, null);
        d.sendMessage(message);
    }

    public void onEndOfSpeech()
    {
        Message message = d.obtainMessage(3, 0, 0, null);
        d.sendMessage(message);
    }

    public void onError(SpeechError speecherror)
    {
        speecherror = d.obtainMessage(0, speecherror);
        d.sendMessage(speecherror);
    }

    public void onEvent(int i, int j, int i1, Bundle bundle)
    {
        Message message = new Message();
        message.what = i;
        message.arg1 = j;
        message.arg2 = i1;
        message.obj = bundle;
        bundle = d.obtainMessage(6, 0, 0, message);
        d.sendMessage(bundle);
    }

    public void onResult(UnderstanderResult understanderresult)
    {
        understanderresult = d.obtainMessage(4, understanderresult);
        d.sendMessage(understanderresult);
    }

    public void onVolumeChanged(int i)
    {
        Message message = d.obtainMessage(1, i, 0, null);
        d.sendMessage(message);
    }

    public tener(SpeechUnderstander speechunderstander, com.iflytek.cloud.SpeechUnderstanderListener speechunderstanderlistener)
    {
        a = speechunderstander;
        super();
        b = null;
        c = null;
        d = new l(this, Looper.getMainLooper());
        b = speechunderstanderlistener;
        c = new k(this, speechunderstander);
    }
}
