// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Parcel;
import android.os.Parcelable;

// Referenced classes of package com.iflytek.cloud:
//            a

public class RecognizerResult
    implements Parcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new a();
    private String a;

    public RecognizerResult(Parcel parcel)
    {
        a = "";
        a = parcel.readString();
    }

    public RecognizerResult(String s)
    {
        a = "";
        if (s != null)
        {
            a = s;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String getResultString()
    {
        return a;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(a);
    }

}
