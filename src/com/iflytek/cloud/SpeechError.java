// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.resource.Resource;

public class SpeechError extends Exception
{

    private static final long serialVersionUID = 0x3d8a3d5cb4c84b7cL;
    private int a;
    private String b;

    public SpeechError(int i)
    {
        byte byte0;
        byte0 = 11;
        super();
        a = 0;
        b = "";
        a = i;
        if (i >= 20001) goto _L2; else goto _L1
_L1:
        if (a != 10118) goto _L4; else goto _L3
_L3:
        i = byte0;
_L7:
        b = Resource.getErrorDescription(i);
        return;
_L4:
        if (10106 == a || 10107 == a || 10124 == a)
        {
            com.iflytek.cloud.a.g.a.a.a("sdk errorcode", (new StringBuilder()).append(a).append("").toString());
            i = 7;
            continue; /* Loop/switch isn't completed */
        }
        if (a == 10110)
        {
            i = 32;
            continue; /* Loop/switch isn't completed */
        }
        if (a == 10111)
        {
            i = 28;
            continue; /* Loop/switch isn't completed */
        }
        if (a >= 10200 && a < 10300)
        {
            i = 3;
            continue; /* Loop/switch isn't completed */
        }
        if (a == 10117 || a == 10101)
        {
            i = 16;
            continue; /* Loop/switch isn't completed */
        }
        if (a == 10113)
        {
            i = 17;
            continue; /* Loop/switch isn't completed */
        }
        if (a >= 10400 && a <= 10407)
        {
            i = 18;
            continue; /* Loop/switch isn't completed */
        }
        if (a >= 11000 && a < 11099)
        {
            if (a == 11005)
            {
                i = 23;
            } else
            if (a == 11006)
            {
                i = 24;
            } else
            {
                i = 18;
            }
            continue; /* Loop/switch isn't completed */
        }
        if (a == 10129)
        {
            i = 19;
            continue; /* Loop/switch isn't completed */
        }
        if (a == 10109)
        {
            i = 20;
            continue; /* Loop/switch isn't completed */
        }
        if (a == 10702)
        {
            i = 21;
            continue; /* Loop/switch isn't completed */
        }
        if (a >= 10500 && a < 10600)
        {
            i = 22;
            continue; /* Loop/switch isn't completed */
        }
        if (a >= 11200 && a <= 11250)
        {
            i = 25;
            continue; /* Loop/switch isn't completed */
        }
        if (a >= 14000 && a <= 14006)
        {
            i = 31;
            continue; /* Loop/switch isn't completed */
        }
        if (a >= 16000 && a <= 16006)
        {
            i = 31;
            continue; /* Loop/switch isn't completed */
        }
          goto _L5
_L2:
        if (a < 30000)
        {
            if (a == 20001)
            {
                i = 1;
            } else
            if (a == 20002)
            {
                i = 2;
            } else
            if (a == 20003)
            {
                i = 3;
            } else
            if (a == 20004)
            {
                i = 5;
            } else
            if (a == 20005)
            {
                i = 10;
            } else
            if (a == 20006)
            {
                i = 9;
            } else
            if (a == 20007)
            {
                i = 12;
            } else
            {
                i = byte0;
                if (a != 20008)
                {
                    if (a == 20009)
                    {
                        i = 13;
                    } else
                    if (a == 20010)
                    {
                        i = 14;
                    } else
                    if (a == 20012)
                    {
                        i = 7;
                    } else
                    if (a == 21003)
                    {
                        i = 28;
                    } else
                    if (a == 21002 || a == 21001)
                    {
                        i = 29;
                    } else
                    {
                        i = 30;
                    }
                }
            }
            continue; /* Loop/switch isn't completed */
        }
_L5:
        i = 3;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public SpeechError(int i, String s)
    {
        this(i);
        if ("wfr".equals(s))
        {
            if (10118 == i)
            {
                b = Resource.getErrorDescription(33);
            } else
            if (10119 == i)
            {
                b = Resource.getErrorDescription(34);
                return;
            }
        }
    }

    public SpeechError(Exception exception)
    {
        a = 0;
        b = "";
        a = 20999;
        b = exception.toString();
    }

    public int getErrorCode()
    {
        return a;
    }

    public String getErrorDescription()
    {
        return b;
    }

    public String getHtmlDescription(boolean flag)
    {
        String s1 = (new StringBuilder()).append(b).append("...").toString();
        String s = s1;
        if (flag)
        {
            s = (new StringBuilder()).append(s1).append("<br>(").toString();
            s = (new StringBuilder()).append(s).append(Resource.getErrorTag(0)).append(":").toString();
            s = (new StringBuilder()).append(s).append(a).append(")").toString();
        }
        return s;
    }

    public String getPlainDescription(boolean flag)
    {
        String s1 = b;
        String s = s1;
        if (flag)
        {
            s = (new StringBuilder()).append(s1).append(".").toString();
            s = (new StringBuilder()).append(s).append("(").append(Resource.getErrorTag(0)).append(":").toString();
            s = (new StringBuilder()).append(s).append(a).append(")").toString();
        }
        return s;
    }

    public String toString()
    {
        return getPlainDescription(true);
    }
}
