// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import com.iflytek.cloud.a.g.a.a;
import org.json.JSONObject;

public class VerifierResult
{

    public static final int MSS_ERROR_IVP_EXTRA_RGN_SOPPORT = 11601;
    public static final int MSS_ERROR_IVP_GENERAL = 11600;
    public static final int MSS_ERROR_IVP_MUCH_NOISE = 11603;
    public static final int MSS_ERROR_IVP_NO_ENOUGH_AUDIO = 11608;
    public static final int MSS_ERROR_IVP_TEXT_NOT_MATCH = 11607;
    public static final int MSS_ERROR_IVP_TOO_LOW = 11604;
    public static final int MSS_ERROR_IVP_TRUNCATED = 11602;
    public static final int MSS_ERROR_IVP_UTTER_TOO_SHORT = 11606;
    public static final int MSS_ERROR_IVP_ZERO_AUDIO = 11605;
    public static final String TAG = "VerifyResult";
    public String dcs;
    public int err;
    public int ret;
    public int rgn;
    public Double score;
    public Double score_raw;
    public String source;
    public String sst;
    public int suc;
    public String trs;
    public String vid;

    public VerifierResult(String s)
    {
        ret = 0;
        dcs = "";
        vid = "";
        suc = 0;
        rgn = 0;
        trs = "";
        err = 0;
        source = "";
        try
        {
            source = s;
            s = new JSONObject(source);
            a.a((new StringBuilder()).append("VerifyResult = ").append(source).toString());
            if (s.has("ret"))
            {
                ret = s.getInt("ret");
            }
            if (s.has("sst"))
            {
                sst = s.getString("sst");
            }
            if (s.has("dcs"))
            {
                dcs = s.getString("dcs");
            }
            if (s.has("suc"))
            {
                suc = s.getInt("suc");
            }
            if (s.has("vid"))
            {
                vid = s.getString("vid");
            }
            if (s.has("rgn"))
            {
                rgn = s.getInt("rgn");
            }
            if (s.has("trs"))
            {
                trs = s.getString("trs");
            }
            if (s.has("err"))
            {
                err = s.getInt("err");
            }
            if (s.has("score"))
            {
                score = Double.valueOf(s.getDouble("score"));
            }
            if (s.has("score_raw"))
            {
                score_raw = Double.valueOf(s.getDouble("score_raw"));
            }
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }
}
