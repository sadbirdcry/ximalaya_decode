// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;


// Referenced classes of package com.iflytek.cloud:
//            Setting

public static final class  extends Enum
{

    private static final none a[];
    public static final none all;
    public static final none detail;
    public static final none low;
    public static final none none;
    public static final none normal;

    public static  valueOf(String s)
    {
        return ()Enum.valueOf(com/iflytek/cloud/Setting$LOG_LEVEL, s);
    }

    public static [] values()
    {
        return ([])a.clone();
    }

    static 
    {
        all = new <init>("all", 0);
        detail = new <init>("detail", 1);
        normal = new <init>("normal", 2);
        low = new <init>("low", 3);
        none = new <init>("none", 4);
        a = (new a[] {
            all, detail, normal, low, none
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
