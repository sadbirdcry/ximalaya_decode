// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.cloud.a.d.d;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.d.a.i;
import com.iflytek.msc.MSC;
import com.iflytek.speech.TextUnderstanderAidl;
import com.iflytek.speech.TextUnderstanderListener;

// Referenced classes of package com.iflytek.cloud:
//            m, SpeechUtility, InitListener, TextUnderstanderListener, 
//            o, n, SpeechError, UnderstanderResult

public class TextUnderstander extends d
{
    private final class a
        implements com.iflytek.cloud.TextUnderstanderListener
    {

        final TextUnderstander a;
        private com.iflytek.cloud.TextUnderstanderListener b;
        private TextUnderstanderListener c;
        private Handler d;

        static TextUnderstanderListener a(a a1)
        {
            return a1.c;
        }

        static Handler b(a a1)
        {
            return a1.d;
        }

        static com.iflytek.cloud.TextUnderstanderListener c(a a1)
        {
            return a1.b;
        }

        public void onError(SpeechError speecherror)
        {
            speecherror = d.obtainMessage(0, speecherror);
            d.sendMessage(speecherror);
        }

        public void onResult(UnderstanderResult understanderresult)
        {
            understanderresult = d.obtainMessage(4, understanderresult);
            d.sendMessage(understanderresult);
        }

        public a(com.iflytek.cloud.TextUnderstanderListener textunderstanderlistener)
        {
            a = TextUnderstander.this;
            super();
            b = null;
            c = null;
            d = new o(this, Looper.getMainLooper());
            b = textunderstanderlistener;
            c = new n(this, TextUnderstander.this);
        }
    }


    private static TextUnderstander d = null;
    private i a;
    private TextUnderstanderAidl c;
    private a e;
    private InitListener f;
    private Handler g;

    protected TextUnderstander(Context context, InitListener initlistener)
    {
        a = null;
        c = null;
        e = null;
        f = null;
        g = new m(this, Looper.getMainLooper());
        f = initlistener;
        if (MSC.isLoaded())
        {
            a = new i(context);
        }
        SpeechUtility speechutility = SpeechUtility.getUtility();
        if (speechutility != null && speechutility.a() && speechutility.getEngineMode() != com.iflytek.cloud.a.d.d.a.a)
        {
            c = new TextUnderstanderAidl(context.getApplicationContext(), initlistener);
        } else
        if (initlistener != null)
        {
            Message.obtain(g, 0, 0, 0, null).sendToTarget();
            return;
        }
    }

    static InitListener a(TextUnderstander textunderstander)
    {
        return textunderstander.f;
    }

    public static TextUnderstander createTextUnderstander(Context context, InitListener initlistener)
    {
        com/iflytek/cloud/TextUnderstander;
        JVM INSTR monitorenter ;
        if (d == null)
        {
            d = new TextUnderstander(context, initlistener);
        }
        context = d;
        com/iflytek/cloud/TextUnderstander;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public static TextUnderstander getTextUnderstander()
    {
        return d;
    }

    protected void a(Context context)
    {
        SpeechUtility speechutility = SpeechUtility.getUtility();
        if (speechutility != null && speechutility.a() && speechutility.getEngineMode() != com.iflytek.cloud.a.d.d.a.a)
        {
            if (c != null && !c.isAvailable())
            {
                c.destory();
                c = null;
            }
            c = new TextUnderstanderAidl(context.getApplicationContext(), f);
        } else
        if (f != null && c != null)
        {
            c.destory();
            c = null;
            return;
        }
    }

    public void cancel()
    {
        if (a != null && a.e())
        {
            a.cancel(false);
        } else
        if (c != null && c.isUnderstanding())
        {
            c.cancel(com.iflytek.cloud.a.a(e));
        } else
        {
            com.iflytek.cloud.a.g.a.a.b("SpeechUnderstander cancel failed, is not running");
        }
        a.cancel(false);
    }

    public boolean destroy()
    {
        boolean flag = true;
        if (c != null)
        {
            c.destory();
            c = null;
        }
        if (a != null)
        {
            flag = a.destroy();
        }
        if (flag)
        {
            d = null;
        }
        return flag;
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public boolean isUnderstanding()
    {
        while (a != null && a.e() || c != null && c.isUnderstanding()) 
        {
            return true;
        }
        return false;
    }

    public boolean setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public int understandText(String s, com.iflytek.cloud.TextUnderstanderListener textunderstanderlistener)
    {
        com.iflytek.cloud.a.d.d.a a1;
        a1 = a("nlu", c);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("start engine mode = ").append(a1.toString()).toString());
        if (a1 != com.iflytek.cloud.a.d.d.a.b) goto _L2; else goto _L1
_L1:
        if (c != null) goto _L4; else goto _L3
_L3:
        return 21001;
_L4:
        c.setParameter("params", null);
        c.setParameter("params", b.toString());
        e = new a(textunderstanderlistener);
        return c.understandText(s, com.iflytek.cloud.a.a(e));
_L2:
        if (a != null)
        {
            a.setParameter(b);
            return a.a(s, textunderstanderlistener);
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

}
