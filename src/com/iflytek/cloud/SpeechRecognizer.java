// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.cloud.a.d.d;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.d.a.c;
import com.iflytek.msc.MSC;
import com.iflytek.speech.RecognizerListener;
import com.iflytek.speech.SpeechRecognizerAidl;

// Referenced classes of package com.iflytek.cloud:
//            b, SpeechUtility, c, d, 
//            InitListener, GrammarListener, RecognizerListener, LexiconListener, 
//            f, e, SpeechError, RecognizerResult

public final class SpeechRecognizer extends d
{
    private final class a
        implements com.iflytek.cloud.RecognizerListener
    {

        final SpeechRecognizer a;
        private com.iflytek.cloud.RecognizerListener b;
        private RecognizerListener c;
        private Handler d;

        static Handler a(a a1)
        {
            return a1.d;
        }

        static com.iflytek.cloud.RecognizerListener b(a a1)
        {
            return a1.b;
        }

        static RecognizerListener c(a a1)
        {
            return a1.c;
        }

        public void onBeginOfSpeech()
        {
            Message message = d.obtainMessage(2, 0, 0, null);
            d.sendMessage(message);
        }

        public void onEndOfSpeech()
        {
            Message message = d.obtainMessage(3, 0, 0, null);
            d.sendMessage(message);
        }

        public void onError(SpeechError speecherror)
        {
            speecherror = d.obtainMessage(0, speecherror);
            d.sendMessage(speecherror);
        }

        public void onEvent(int i, int j, int k, Bundle bundle)
        {
            Message message = new Message();
            message.what = i;
            message.arg1 = j;
            message.arg2 = k;
            message.obj = bundle;
            bundle = d.obtainMessage(6, 0, 0, message);
            d.sendMessage(bundle);
        }

        public void onResult(RecognizerResult recognizerresult, boolean flag)
        {
            int i = 1;
            Handler handler = d;
            if (!flag)
            {
                i = 0;
            }
            recognizerresult = handler.obtainMessage(4, i, 0, recognizerresult);
            d.sendMessage(recognizerresult);
        }

        public void onVolumeChanged(int i)
        {
            Message message = d.obtainMessage(1, i, 0, null);
            d.sendMessage(message);
        }

        public a(com.iflytek.cloud.RecognizerListener recognizerlistener)
        {
            a = SpeechRecognizer.this;
            super();
            b = null;
            c = null;
            d = new f(this, Looper.getMainLooper());
            b = recognizerlistener;
            c = new e(this, SpeechRecognizer.this);
        }
    }


    private static SpeechRecognizer a = null;
    private c c;
    private SpeechRecognizerAidl d;
    private a e;
    private InitListener f;
    private Handler g;

    protected SpeechRecognizer(Context context, InitListener initlistener)
    {
        c = null;
        d = null;
        e = null;
        f = null;
        g = new b(this, Looper.getMainLooper());
        f = initlistener;
        if (MSC.isLoaded())
        {
            c = new c(context);
        }
        SpeechUtility speechutility = SpeechUtility.getUtility();
        if (speechutility != null && speechutility.a() && speechutility.getEngineMode() != com.iflytek.cloud.a.d.d.a.a)
        {
            d = new SpeechRecognizerAidl(context.getApplicationContext(), initlistener);
        } else
        if (initlistener != null)
        {
            Message.obtain(g, 0, 0, 0, null).sendToTarget();
            return;
        }
    }

    static InitListener a(SpeechRecognizer speechrecognizer)
    {
        return speechrecognizer.f;
    }

    public static SpeechRecognizer createRecognizer(Context context, InitListener initlistener)
    {
        com/iflytek/cloud/SpeechRecognizer;
        JVM INSTR monitorenter ;
        if (a == null)
        {
            a = new SpeechRecognizer(context, initlistener);
        }
        context = a;
        com/iflytek/cloud/SpeechRecognizer;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public static SpeechRecognizer getRecognizer()
    {
        return a;
    }

    protected void a(Context context)
    {
        SpeechUtility speechutility = SpeechUtility.getUtility();
        if (speechutility != null && speechutility.a() && speechutility.getEngineMode() != com.iflytek.cloud.a.d.d.a.a)
        {
            if (d != null && !d.isAvailable())
            {
                d.destory();
                d = null;
            }
            d = new SpeechRecognizerAidl(context.getApplicationContext(), f);
        } else
        if (f != null && d != null)
        {
            d.destory();
            d = null;
            return;
        }
    }

    public int buildGrammar(String s, String s1, GrammarListener grammarlistener)
    {
        com.iflytek.cloud.a.d.d.a a1;
        a1 = a("asr", d);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("start engine mode = ").append(a1.toString()).toString());
        if (a1 != com.iflytek.cloud.a.d.d.a.b) goto _L2; else goto _L1
_L1:
        if (d != null) goto _L4; else goto _L3
_L3:
        return 21001;
_L4:
        d.setParameter("params", null);
        d.setParameter("params", b.toString());
        grammarlistener = new com.iflytek.cloud.c(this, grammarlistener);
        return d.buildGrammar(s, s1, grammarlistener);
_L2:
        if (c != null)
        {
            c.setParameter(b);
            return c.a(s, s1, grammarlistener);
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    public void cancel()
    {
        if (c != null && c.f())
        {
            c.cancel(false);
            return;
        }
        if (d != null && d.isListening())
        {
            d.cancel(com.iflytek.cloud.a.c(e));
            return;
        } else
        {
            com.iflytek.cloud.a.g.a.a.b("SpeechRecognizer cancel failed, is not running");
            return;
        }
    }

    public boolean destroy()
    {
        boolean flag = true;
        if (d != null)
        {
            d.destory();
            d = null;
        }
        if (c != null)
        {
            flag = c.destroy();
        }
        if (flag)
        {
            a = null;
        }
        return flag;
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public boolean isListening()
    {
        while (c != null && c.f() || d != null && d.isListening()) 
        {
            return true;
        }
        return false;
    }

    public boolean setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public int startListening(com.iflytek.cloud.RecognizerListener recognizerlistener)
    {
        com.iflytek.cloud.a.d.d.a a1;
        a1 = a("asr", d);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("start engine mode = ").append(a1.toString()).toString());
        if (a1 != com.iflytek.cloud.a.d.d.a.b) goto _L2; else goto _L1
_L1:
        if (d != null) goto _L4; else goto _L3
_L3:
        return 21001;
_L4:
        d.setParameter("params", null);
        d.setParameter("params", b.toString());
        e = new a(recognizerlistener);
        return d.startListening(com.iflytek.cloud.a.c(e));
_L2:
        if (c != null)
        {
            c.setParameter(b);
            return c.a(recognizerlistener);
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    public void stopListening()
    {
        if (c != null && c.f())
        {
            c.e();
            return;
        }
        if (d != null && d.isListening())
        {
            d.stopListening(com.iflytek.cloud.a.c(e));
            return;
        } else
        {
            com.iflytek.cloud.a.g.a.a.b("SpeechRecognizer stopListening failed, is not running");
            return;
        }
    }

    public int updateLexicon(String s, String s1, LexiconListener lexiconlistener)
    {
        com.iflytek.cloud.a.d.d.a a1;
        a1 = a("asr", d);
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("start engine mode = ").append(a1.toString()).toString());
        if (a1 != com.iflytek.cloud.a.d.d.a.b) goto _L2; else goto _L1
_L1:
        if (d != null) goto _L4; else goto _L3
_L3:
        return 21001;
_L4:
        d.setParameter("params", null);
        d.setParameter("params", b.toString());
        lexiconlistener = new com.iflytek.cloud.d(this, lexiconlistener);
        return d.updateLexicon(s, s1, lexiconlistener);
_L2:
        if (c != null)
        {
            c.setParameter(b);
            return c.a(s, s1, lexiconlistener);
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    public int writeAudio(byte abyte0[], int i, int j)
    {
        if (c != null && c.f())
        {
            return c.a(abyte0, i, j);
        }
        if (d != null && d.isListening())
        {
            return d.writeAudio(abyte0, i, j);
        } else
        {
            com.iflytek.cloud.a.g.a.a.b("SpeechRecognizer writeAudio failed, is not running");
            return 21004;
        }
    }

}
