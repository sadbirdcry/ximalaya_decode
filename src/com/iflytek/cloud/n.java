// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Handler;
import android.os.RemoteException;
import com.iflytek.speech.UnderstanderResult;

// Referenced classes of package com.iflytek.cloud:
//            SpeechError, UnderstanderResult, TextUnderstander

final class n extends com.iflytek.speech.TextUnderstanderListener.Stub
{

    final TextUnderstander a;
    final TextUnderstander.a b;

    n(TextUnderstander.a a1, TextUnderstander textunderstander)
    {
        b = a1;
        a = textunderstander;
        super();
    }

    public void onError(int i)
        throws RemoteException
    {
        android.os.Message message = TextUnderstander.a.b(b).obtainMessage(0, new SpeechError(i));
        TextUnderstander.a.b(b).sendMessage(message);
    }

    public void onResult(UnderstanderResult understanderresult)
        throws RemoteException
    {
        understanderresult = TextUnderstander.a.b(b).obtainMessage(4, new com.iflytek.cloud.UnderstanderResult(understanderresult.getResultString()));
        TextUnderstander.a.b(b).sendMessage(understanderresult);
    }
}
