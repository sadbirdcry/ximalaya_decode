// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;

// Referenced classes of package com.iflytek.cloud:
//            SpeechError

public interface SpeechListener
{

    public abstract void onBufferReceived(byte abyte0[]);

    public abstract void onCompleted(SpeechError speecherror);

    public abstract void onEvent(int i, Bundle bundle);
}
