// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

// Referenced classes of package com.iflytek.cloud:
//            SynthesizerListener, SpeechError

final class i extends Handler
{

    final SpeechSynthesizer.a a;

    i(SpeechSynthesizer.a a1, Looper looper)
    {
        a = a1;
        super(looper);
    }

    public void handleMessage(Message message)
    {
        if (SpeechSynthesizer.a.a(a) != null)
        {
            switch (message.what)
            {
            default:
                return;

            case 1: // '\001'
                SpeechSynthesizer.a.a(a).onSpeakBegin();
                return;

            case 2: // '\002'
                message = (Bundle)message.obj;
                int j = message.getInt("percent");
                int l = message.getInt("begpos");
                int i1 = message.getInt("endpos");
                message = message.getString("spellinfo");
                SpeechSynthesizer.a.a(a).onBufferProgress(j, l, i1, message);
                return;

            case 3: // '\003'
                SpeechSynthesizer.a.a(a).onSpeakPaused();
                return;

            case 4: // '\004'
                SpeechSynthesizer.a.a(a).onSpeakResumed();
                return;

            case 5: // '\005'
                int k = ((Integer)message.obj).intValue();
                SpeechSynthesizer.a.a(a).onSpeakProgress(message.arg1, message.arg2, k);
                return;

            case 6: // '\006'
                SpeechSynthesizer.a.a(a).onCompleted((SpeechError)message.obj);
                return;

            case 7: // '\007'
                message = (Message)message.obj;
                break;
            }
            if (message != null)
            {
                SpeechSynthesizer.a.a(a).onEvent(message.what, message.arg1, message.arg2, (Bundle)message.obj);
                return;
            }
        }
    }
}
