// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.c;

import android.content.Context;
import android.media.AudioTrack;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.a.g.f;

// Referenced classes of package com.iflytek.cloud.c:
//            d, e, b

public class c
{
    public static interface a
    {

        public abstract void a();

        public abstract void a(int i1, int j1, int k1);

        public abstract void a(SpeechError speecherror);

        public abstract void b();

        public abstract void c();
    }

    private class b extends Thread
    {

        final c a;

        public void run()
        {
            com.iflytek.cloud.a.g.a.a.a("PcmPlayer", "start player");
            com.iflytek.cloud.a.g.a.a.a("PcmPlayer", (new StringBuilder()).append("mAudioFocus= ").append(c.c(a)).toString());
            if (!c.c(a)) goto _L2; else goto _L1
_L1:
            com.iflytek.cloud.a.g.f.a(c.d(a), Boolean.valueOf(c.e(a)), a.a);
_L10:
            com.iflytek.cloud.c.c.f(a).c();
            if (c.g(a) != 4 && c.g(a) != 3)
            {
                com.iflytek.cloud.c.c.a(a, 2);
            }
_L9:
            if (c.g(a) == 4) goto _L4; else goto _L3
_L3:
            c.h(a);
            if (c.g(a) != 2 && c.g(a) != 1) goto _L6; else goto _L5
_L5:
            if (!com.iflytek.cloud.c.c.f(a).g()) goto _L8; else goto _L7
_L7:
            b.a a1;
            int i1;
            if (c.g(a) == 1)
            {
                com.iflytek.cloud.c.c.a(a, 2);
                Message.obtain(c.i(a), 2).sendToTarget();
            }
            i1 = com.iflytek.cloud.c.c.f(a).d();
            a1 = com.iflytek.cloud.c.c.f(a).e();
            if (a1 == null)
            {
                break MISSING_BLOCK_LABEL_260;
            }
            c.b(a, a1.d);
            Message.obtain(c.i(a), 3, i1, a1.c).sendToTarget();
            if (c.j(a).getPlayState() != 3)
            {
                c.j(a).play();
            }
            com.iflytek.cloud.c.c.f(a).a(c.j(a), c.k(a));
              goto _L9
            Exception exception;
            exception;
            exception.printStackTrace();
            Message.obtain(c.i(a), 0, new SpeechError(20011)).sendToTarget();
            com.iflytek.cloud.c.c.a(a, 4);
            if (c.j(a) != null)
            {
                c.j(a).release();
                com.iflytek.cloud.c.c.a(a, null);
            }
            Object obj;
            if (c.c(a))
            {
                com.iflytek.cloud.a.g.f.b(c.d(a), Boolean.valueOf(c.e(a)), a.a);
            } else
            {
                com.iflytek.cloud.a.g.f.b(c.d(a), Boolean.valueOf(c.e(a)), null);
            }
            obj = a;
            com.iflytek.cloud.c.c.a(((c) (obj)), null);
            return;
_L2:
            com.iflytek.cloud.a.g.f.a(c.d(a), Boolean.valueOf(c.e(a)), null);
              goto _L10
            obj;
            com.iflytek.cloud.c.c.a(a, 4);
            if (c.j(a) != null)
            {
                c.j(a).release();
                com.iflytek.cloud.c.c.a(a, null);
            }
            if (c.c(a))
            {
                com.iflytek.cloud.a.g.f.b(c.d(a), Boolean.valueOf(c.e(a)), a.a);
            } else
            {
                com.iflytek.cloud.a.g.f.b(c.d(a), Boolean.valueOf(c.e(a)), null);
            }
            com.iflytek.cloud.c.c.a(a, null);
            throw obj;
_L8:
            if (!com.iflytek.cloud.c.c.f(a).f()) goto _L12; else goto _L11
_L11:
            com.iflytek.cloud.a.g.a.a.a("play stoped");
            com.iflytek.cloud.c.c.a(a, 4);
            Message.obtain(c.i(a), 4).sendToTarget();
_L4:
            if (c.j(a) != null)
            {
                c.j(a).stop();
            }
            com.iflytek.cloud.c.c.a(a, 4);
            if (c.j(a) != null)
            {
                c.j(a).release();
                com.iflytek.cloud.c.c.a(a, null);
            }
            if (c.c(a))
            {
                com.iflytek.cloud.a.g.f.b(c.d(a), Boolean.valueOf(c.e(a)), a.a);
            } else
            {
                com.iflytek.cloud.a.g.f.b(c.d(a), Boolean.valueOf(c.e(a)), null);
            }
            obj = a;
            break MISSING_BLOCK_LABEL_421;
_L12:
            if (c.g(a) == 2)
            {
                com.iflytek.cloud.a.g.a.a.a("play onpaused!");
                com.iflytek.cloud.c.c.a(a, 1);
                Message.obtain(c.i(a), 1).sendToTarget();
            }
            sleep(5L);
              goto _L9
_L6:
            if (c.g(a) != 3) goto _L9; else goto _L13
_L13:
            if (2 != c.j(a).getPlayState())
            {
                c.j(a).pause();
            }
            sleep(5L);
              goto _L9
        }

        private b()
        {
            a = c.this;
            super();
        }

        b(d d1)
        {
            this();
        }
    }


    android.media.AudioManager.OnAudioFocusChangeListener a;
    private AudioTrack b;
    private com.iflytek.cloud.c.b c;
    private Context d;
    private b e;
    private a f;
    private volatile int g;
    private int h;
    private boolean i;
    private int j;
    private boolean k;
    private boolean l;
    private Object m;
    private int n;
    private Handler o;

    public c(Context context)
    {
        b = null;
        c = null;
        d = null;
        e = null;
        f = null;
        g = 0;
        h = 3;
        i = true;
        k = false;
        l = false;
        m = new Object();
        a = new d(this);
        n = 0;
        o = new e(this, Looper.getMainLooper());
        d = context;
    }

    public c(Context context, int i1, boolean flag)
    {
        b = null;
        c = null;
        d = null;
        e = null;
        f = null;
        g = 0;
        h = 3;
        i = true;
        k = false;
        l = false;
        m = new Object();
        a = new d(this);
        n = 0;
        o = new e(this, Looper.getMainLooper());
        d = context;
        h = i1;
        k = flag;
    }

    static int a(c c1, int i1)
    {
        c1.g = i1;
        return i1;
    }

    static AudioTrack a(c c1, AudioTrack audiotrack)
    {
        c1.b = audiotrack;
        return audiotrack;
    }

    static a a(c c1)
    {
        return c1.f;
    }

    static a a(c c1, a a1)
    {
        c1.f = a1;
        return a1;
    }

    static b a(c c1, b b1)
    {
        c1.e = b1;
        return b1;
    }

    static boolean a(c c1, boolean flag)
    {
        c1.l = flag;
        return flag;
    }

    static int b(c c1, int i1)
    {
        c1.n = i1;
        return i1;
    }

    static boolean b(c c1)
    {
        return c1.l;
    }

    static boolean c(c c1)
    {
        return c1.i;
    }

    static Context d(c c1)
    {
        return c1.d;
    }

    static boolean e(c c1)
    {
        return c1.k;
    }

    static com.iflytek.cloud.c.b f(c c1)
    {
        return c1.c;
    }

    private void f()
        throws Exception
    {
        com.iflytek.cloud.a.g.a.a.a("PcmPlayer", "createAudio start");
        int i1 = c.a();
        j = AudioTrack.getMinBufferSize(i1, 2, 2);
        if (b != null)
        {
            b();
        }
        com.iflytek.cloud.a.g.a.a.a("PcmPlayer", (new StringBuilder()).append("createAudio || mStreamType = ").append(h).toString());
        b = new AudioTrack(h, i1, 2, 2, j * 2, 1);
        if (j == -2 || j == -1)
        {
            throw new Exception();
        } else
        {
            com.iflytek.cloud.a.g.a.a.a("PcmPlayer", "createAudio end");
            return;
        }
    }

    static int g(c c1)
    {
        return c1.g;
    }

    private void g()
        throws Exception
    {
        if (b != null && b.getStreamType() == h)
        {
            return;
        } else
        {
            com.iflytek.cloud.a.g.a.a.a("PcmPlayer", "prepAudioPlayer || audiotrack stream type is change.");
            f();
            return;
        }
    }

    static void h(c c1)
        throws Exception
    {
        c1.g();
    }

    static Handler i(c c1)
    {
        return c1.o;
    }

    static AudioTrack j(c c1)
    {
        return c1.b;
    }

    static int k(c c1)
    {
        return c1.j;
    }

    static int l(c c1)
    {
        return c1.n;
    }

    public int a()
    {
        return g;
    }

    public boolean a(com.iflytek.cloud.c.b b1, a a1)
    {
        com.iflytek.cloud.a.g.a.a.a("PcmPlayer", (new StringBuilder()).append("play mPlaytate= ").append(g).append(",mAudioFocus= ").append(i).toString());
        if (g != 4 && g != 0 && g != 3 && e != null)
        {
            return false;
        } else
        {
            c = b1;
            f = a1;
            e = new b(null);
            e.start();
            return true;
        }
    }

    public void b()
    {
        synchronized (m)
        {
            if (b != null)
            {
                if (b.getPlayState() == 3)
                {
                    b.stop();
                }
                b.release();
                b = null;
            }
            com.iflytek.cloud.a.g.a.a.a("PcmPlayer", "mAudioTrack released");
        }
        return;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean c()
    {
        if (g == 4 || g == 3)
        {
            return false;
        } else
        {
            g = 3;
            return true;
        }
    }

    public boolean d()
    {
        if (g == 3)
        {
            g = 2;
            return true;
        } else
        {
            return false;
        }
    }

    public void e()
    {
        g = 4;
    }
}
