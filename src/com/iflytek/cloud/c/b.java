// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.c;

import android.content.Context;
import android.media.AudioTrack;
import android.os.MemoryFile;
import android.text.TextUtils;
import com.iflytek.cloud.a.g.a.a;
import com.iflytek.cloud.a.g.e;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class b
{
    public class a
    {

        long a;
        long b;
        int c;
        int d;
        final b e;

        public a(long l1, long l2, int i1, int j1)
        {
            e = b.this;
            super();
            a = l1;
            b = l2;
            c = i1;
            d = j1;
        }
    }


    private int a;
    private ArrayList b;
    private Context c;
    private int d;
    private volatile int e;
    private MemoryFile f;
    private volatile long g;
    private volatile int h;
    private a i;
    private String j;
    private String k;
    private byte l[];
    private int m;
    private int n;

    public b(Context context, int i1, String s)
    {
        a = 0x300000;
        b = null;
        c = null;
        d = 16000;
        e = 0;
        f = null;
        g = 0L;
        h = 0;
        i = null;
        j = "";
        k = null;
        l = null;
        m = 0;
        n = 0;
        c = context;
        e = 0;
        b = new ArrayList();
        g = 0L;
        d = i1;
        k = s;
    }

    private void a(byte abyte0[])
        throws IOException
    {
        if (abyte0 == null || abyte0.length == 0)
        {
            return;
        }
        if (f == null)
        {
            j = i();
            f = new MemoryFile(j, a);
            f.allowPurging(false);
        }
        f.writeBytes(abyte0, 0, (int)g, abyte0.length);
        g = g + (long)abyte0.length;
        return;
        abyte0;
        throw abyte0;
    }

    private void b(int i1)
        throws IOException
    {
        if (l == null)
        {
            l = new byte[i1 * 10];
        }
        i1 = l.length;
        int j1 = (int)(g - (long)h);
        if (j1 < i1)
        {
            i1 = j1;
        } else
        {
            j1 = i1;
        }
        f.readBytes(l, h, 0, j1);
        h = j1 + h;
        m = 0;
        n = i1;
    }

    private String i()
    {
        String s = com.iflytek.cloud.a.g.e.a(c);
        return (new StringBuilder()).append(s).append(System.currentTimeMillis()).append("tts.pcm").toString();
    }

    public int a()
    {
        return d;
    }

    public void a(AudioTrack audiotrack, int i1)
        throws IOException
    {
        if (m >= n)
        {
            b(i1);
        }
        int j1;
        if (i1 * 2 > n - m)
        {
            j1 = n - m;
        } else
        {
            j1 = i1;
        }
        audiotrack.write(l, m, j1);
        m = j1 + m;
        if (f())
        {
            b(audiotrack, i1);
        }
    }

    public void a(String s)
    {
        int i1 = 0x96000;
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        a = (s.length() / 5) * 4 * 32 * 1024;
        if (a > 0x96000)
        {
            i1 = a;
        }
        a = i1;
    }

    public void a(ArrayList arraylist, int i1, int j1, int k1)
        throws IOException
    {
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("buffer percent = ").append(i1).append(" beg=").append(j1).append(" end=").append(k1).toString());
        a a1 = new a(g, g, j1, k1);
        for (j1 = 0; j1 < arraylist.size(); j1++)
        {
            a((byte[])arraylist.get(j1));
        }

        a1.b = g;
        e = i1;
        synchronized (b)
        {
            b.add(a1);
        }
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("allSize = ").append(g).append(" maxSize=").append(a).toString());
        return;
        exception;
        arraylist;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean a(int i1)
    {
        while (e > 95 || g / 32L >= (long)i1 && 0L < g) 
        {
            return true;
        }
        return false;
    }

    public void b(AudioTrack audiotrack, int i1)
    {
        audiotrack.write(new byte[i1], 0, i1);
    }

    public boolean b()
    {
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("save to local: totalSize = ").append(g).append(" maxSize=").append(a).toString());
        return com.iflytek.cloud.a.g.e.a(f, g, k);
    }

    public void c()
        throws IOException
    {
        h = 0;
        i = null;
        if (b.size() > 0)
        {
            i = (a)b.get(0);
        }
    }

    public int d()
    {
        if (g <= 0L)
        {
            return 0;
        } else
        {
            return (int)((long)((h - (n - m)) * e) / g);
        }
    }

    public a e()
    {
        if (i != null)
        {
            if ((long)h >= i.a && (long)h <= i.b)
            {
                return i;
            }
            Object obj;
            synchronized (b)
            {
                obj = b.iterator();
                do
                {
                    if (!((Iterator) (obj)).hasNext())
                    {
                        break MISSING_BLOCK_LABEL_127;
                    }
                    i = (a)((Iterator) (obj)).next();
                } while ((long)h < i.a || (long)h > i.b);
                obj = i;
            }
            return ((a) (obj));
        }
          goto _L1
        exception;
        arraylist;
        JVM INSTR monitorexit ;
        throw exception;
        arraylist;
        JVM INSTR monitorexit ;
_L1:
        return null;
    }

    public boolean f()
    {
        return 100 == e && (long)h >= g && m >= n;
    }

    protected void finalize()
        throws Throwable
    {
        h();
        super.finalize();
    }

    public boolean g()
    {
        return (long)h < g || m < n;
    }

    public void h()
    {
        com.iflytek.cloud.a.g.a.a.a("deleteFile");
        try
        {
            if (f != null)
            {
                f.close();
                f = null;
            }
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }
}
