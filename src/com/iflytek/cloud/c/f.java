// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud.c;

import android.media.AudioRecord;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.a.g.a.a;

public class f extends Thread
{
    public static interface a
    {

        public abstract void a(byte abyte0[], int l, int i1);

        public abstract void b(SpeechError speecherror);

        public abstract void c(boolean flag);

        public abstract void k();
    }


    private final short a = 16;
    private byte b[];
    private AudioRecord c;
    private a d;
    private a e;
    private volatile boolean f;
    private int g;
    private int h;
    private int i;
    private int j;

    public f(int k, int l, int i1)
    {
        b = null;
        c = null;
        d = null;
        e = null;
        f = false;
        g = 16000;
        h = 40;
        i = 40;
        j = i1;
        g = k;
        h = l;
        if (h < 40 || h > 100)
        {
            h = 40;
        }
        i = 10;
    }

    private int a()
        throws SpeechError
    {
        int k;
        if (c == null || d == null)
        {
            k = 0;
        } else
        {
            int l = c.read(b, 0, b.length);
            k = l;
            if (l > 0)
            {
                k = l;
                if (d != null)
                {
                    d.a(b, 0, l);
                    return l;
                }
            }
        }
        return k;
    }

    private void b()
    {
        this;
        JVM INSTR monitorenter ;
        if (c != null)
        {
            com.iflytek.cloud.a.g.a.a.a("release record begin");
            c.release();
            c = null;
            if (e != null)
            {
                e.k();
                e = null;
            }
            com.iflytek.cloud.a.g.a.a.a("release record over");
        }
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
        Object obj;
        obj;
        com.iflytek.cloud.a.g.a.a.b(((Exception) (obj)).toString());
          goto _L1
        obj;
        this;
        JVM INSTR monitorexit ;
        throw obj;
    }

    public void a(a a1)
        throws SpeechError
    {
        d = a1;
        setPriority(10);
        start();
    }

    public void a(short word0, int k, int l)
        throws SpeechError
    {
        if (c != null)
        {
            b();
        }
        int l1 = (k * l) / 1000;
        int j1 = (l1 * 4 * 16 * word0) / 8;
        int i1;
        int k1;
        if (word0 == 1)
        {
            l = 2;
        } else
        {
            l = 3;
        }
        k1 = AudioRecord.getMinBufferSize(k, l, 2);
        i1 = j1;
        if (j1 < k1)
        {
            i1 = k1;
        }
        c = new AudioRecord(j, k, l, 2, i1);
        b = new byte[(l1 * word0 * 16) / 8];
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("\nSampleRate:").append(k).append("\nChannel:").append(l).append("\nFormat:").append(2).append("\nFramePeriod:").append(l1).append("\nBufferSize:").append(i1).append("\nMinBufferSize:").append(k1).append("\nActualBufferSize:").append(b.length).append("\n").toString());
        if (c.getState() != 1)
        {
            com.iflytek.cloud.a.g.a.a.a("create AudioRecord error");
            throw new SpeechError(20006);
        } else
        {
            return;
        }
    }

    public void a(boolean flag)
    {
        f = true;
        if (e == null)
        {
            e = d;
        }
        d = null;
        if (!flag) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorenter ;
        com.iflytek.cloud.a.g.a.a.a("stopRecord...release");
        if (c != null)
        {
            if (3 == c.getRecordingState() && 1 == c.getState())
            {
                com.iflytek.cloud.a.g.a.a.a("stopRecord releaseRecording ing...");
                c.release();
                com.iflytek.cloud.a.g.a.a.a("stopRecord releaseRecording end...");
                c = null;
            }
            if (e != null)
            {
                e.k();
                e = null;
            }
        }
_L3:
        this;
        JVM INSTR monitorexit ;
_L2:
        com.iflytek.cloud.a.g.a.a.a("stop record");
        return;
        Object obj;
        obj;
        com.iflytek.cloud.a.g.a.a.b(((Exception) (obj)).toString());
          goto _L3
        obj;
        this;
        JVM INSTR monitorexit ;
        throw obj;
    }

    protected void finalize()
        throws Throwable
    {
        b();
        super.finalize();
    }

    public void run()
    {
        int k;
        boolean flag;
        flag = false;
        k = 0;
_L4:
        boolean flag1 = f;
        int l;
        l = ((flag) ? 1 : 0);
        if (flag1)
        {
            break MISSING_BLOCK_LABEL_35;
        }
        a((short)1, g, h);
        l = ((flag) ? 1 : 0);
_L3:
        flag1 = f;
        if (flag1)
        {
            break MISSING_BLOCK_LABEL_173;
        }
        try
        {
            c.startRecording();
            if (c.getRecordingState() != 3)
            {
                throw new SpeechError(20006);
            }
            break MISSING_BLOCK_LABEL_173;
        }
        catch (Exception exception)
        {
            l++;
        }
        if (l >= 10) goto _L2; else goto _L1
_L1:
        sleep(40L);
          goto _L3
        Exception exception1;
        exception1;
        exception1.printStackTrace();
        if (d != null)
        {
            d.b(new SpeechError(20006));
        }
_L5:
        b();
        return;
        exception1;
        k++;
        if (k >= 10)
        {
            break MISSING_BLOCK_LABEL_151;
        }
        sleep(40L);
          goto _L4
        throw new SpeechError(20006);
_L2:
        throw new SpeechError(20006);
        if (d != null)
        {
            d.c(true);
        }
        while (!f) 
        {
            a();
            sleep(i);
        }
          goto _L5
    }
}
