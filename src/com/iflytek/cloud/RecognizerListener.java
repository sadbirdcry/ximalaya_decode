// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;

// Referenced classes of package com.iflytek.cloud:
//            SpeechError, RecognizerResult

public interface RecognizerListener
{

    public abstract void onBeginOfSpeech();

    public abstract void onEndOfSpeech();

    public abstract void onError(SpeechError speecherror);

    public abstract void onEvent(int i, int j, int k, Bundle bundle);

    public abstract void onResult(RecognizerResult recognizerresult, boolean flag);

    public abstract void onVolumeChanged(int i);
}
