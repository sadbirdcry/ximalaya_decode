// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.speech.SynthesizerListener;

// Referenced classes of package com.iflytek.cloud:
//            SynthesizerListener, SpeechSynthesizer, i, h, 
//            SpeechError

private final class c
    implements com.iflytek.cloud.SynthesizerListener
{

    final SpeechSynthesizer a;
    private com.iflytek.cloud.SynthesizerListener b;
    private SynthesizerListener c;
    private Handler d;

    static com.iflytek.cloud.SynthesizerListener a(c c1)
    {
        return c1.b;
    }

    static Handler b(b b1)
    {
        return b1.d;
    }

    static SynthesizerListener c(d d1)
    {
        return d1.c;
    }

    public void onBufferProgress(int j, int k, int l, String s)
    {
        if (b != null)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("percent", j);
            bundle.putInt("begpos", k);
            bundle.putInt("endpos", l);
            bundle.putString("spellinfo", s);
            if (b != null)
            {
                Message.obtain(d, 2, bundle).sendToTarget();
            }
        }
    }

    public void onCompleted(SpeechError speecherror)
    {
        if (b != null)
        {
            Message.obtain(d, 6, speecherror).sendToTarget();
        }
    }

    public void onEvent(int j, int k, int l, Bundle bundle)
    {
        if (b != null)
        {
            Message message = Message.obtain();
            message.what = j;
            message.arg1 = 0;
            message.arg2 = 0;
            message.obj = bundle;
            Message.obtain(d, 7, 0, 0, message).sendToTarget();
        }
    }

    public void onSpeakBegin()
    {
        if (b != null)
        {
            Message.obtain(d, 1).sendToTarget();
        }
    }

    public void onSpeakPaused()
    {
        if (b != null)
        {
            Message.obtain(d, 3).sendToTarget();
        }
    }

    public void onSpeakProgress(int j, int k, int l)
    {
        if (b != null)
        {
            Message.obtain(d, 5, j, k, Integer.valueOf(l)).sendToTarget();
        }
    }

    public void onSpeakResumed()
    {
        if (b != null)
        {
            Message.obtain(d, 4).sendToTarget();
        }
    }

    public (SpeechSynthesizer speechsynthesizer, com.iflytek.cloud.SynthesizerListener synthesizerlistener)
    {
        a = speechsynthesizer;
        super();
        b = null;
        c = null;
        d = new i(this, Looper.getMainLooper());
        b = synthesizerlistener;
        c = new h(this, speechsynthesizer);
    }
}
