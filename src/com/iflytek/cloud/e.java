// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import com.iflytek.speech.RecognizerResult;

// Referenced classes of package com.iflytek.cloud:
//            SpeechError, RecognizerResult, SpeechRecognizer

final class e extends com.iflytek.speech.RecognizerListener.Stub
{

    final SpeechRecognizer a;
    final SpeechRecognizer.a b;

    e(SpeechRecognizer.a a1, SpeechRecognizer speechrecognizer)
    {
        b = a1;
        a = speechrecognizer;
        super();
    }

    public void onBeginOfSpeech()
        throws RemoteException
    {
        Message message = SpeechRecognizer.a.a(b).obtainMessage(2);
        SpeechRecognizer.a.a(b).sendMessage(message);
    }

    public void onEndOfSpeech()
        throws RemoteException
    {
        Message message = SpeechRecognizer.a.a(b).obtainMessage(3);
        SpeechRecognizer.a.a(b).sendMessage(message);
    }

    public void onError(int i)
        throws RemoteException
    {
        Message message = SpeechRecognizer.a.a(b).obtainMessage(0, new SpeechError(i));
        SpeechRecognizer.a.a(b).sendMessage(message);
    }

    public void onEvent(int i, int j, int k, Bundle bundle)
        throws RemoteException
    {
        Message message = new Message();
        message.what = i;
        message.arg1 = j;
        message.arg2 = k;
        message.obj = bundle;
        bundle = SpeechRecognizer.a.a(b).obtainMessage(6, 0, 0, message);
        SpeechRecognizer.a.a(b).sendMessage(bundle);
    }

    public void onResult(RecognizerResult recognizerresult, boolean flag)
        throws RemoteException
    {
        int i = 1;
        Handler handler = SpeechRecognizer.a.a(b);
        if (!flag)
        {
            i = 0;
        }
        recognizerresult = handler.obtainMessage(4, i, 0, new com.iflytek.cloud.RecognizerResult(recognizerresult.getResultString()));
        SpeechRecognizer.a.a(b).sendMessage(recognizerresult);
    }

    public void onVolumeChanged(int i)
        throws RemoteException
    {
        Message message = SpeechRecognizer.a.a(b).obtainMessage(1, i, 0, null);
        SpeechRecognizer.a.a(b).sendMessage(message);
    }
}
