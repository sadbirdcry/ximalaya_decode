// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.RemoteException;

// Referenced classes of package com.iflytek.cloud:
//            LexiconListener, SpeechError, SpeechRecognizer

final class d extends com.iflytek.speech.LexiconListener.Stub
{

    final LexiconListener a;
    final SpeechRecognizer b;

    d(SpeechRecognizer speechrecognizer, LexiconListener lexiconlistener)
    {
        b = speechrecognizer;
        a = lexiconlistener;
        super();
    }

    public void onLexiconUpdated(String s, int i)
        throws RemoteException
    {
        if (a != null)
        {
            LexiconListener lexiconlistener = a;
            SpeechError speecherror;
            if (i == 0)
            {
                speecherror = null;
            } else
            {
                speecherror = new SpeechError(i);
            }
            lexiconlistener.onLexiconUpdated(s, speecherror);
        }
    }
}
