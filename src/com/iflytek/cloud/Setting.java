// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import com.iflytek.msc.MSC;

public class Setting
{
    public static final class LOG_LEVEL extends Enum
    {

        private static final LOG_LEVEL a[];
        public static final LOG_LEVEL all;
        public static final LOG_LEVEL detail;
        public static final LOG_LEVEL low;
        public static final LOG_LEVEL none;
        public static final LOG_LEVEL normal;

        public static LOG_LEVEL valueOf(String s)
        {
            return (LOG_LEVEL)Enum.valueOf(com/iflytek/cloud/Setting$LOG_LEVEL, s);
        }

        public static LOG_LEVEL[] values()
        {
            return (LOG_LEVEL[])a.clone();
        }

        static 
        {
            all = new LOG_LEVEL("all", 0);
            detail = new LOG_LEVEL("detail", 1);
            normal = new LOG_LEVEL("normal", 2);
            low = new LOG_LEVEL("low", 3);
            none = new LOG_LEVEL("none", 4);
            a = (new LOG_LEVEL[] {
                all, detail, normal, low, none
            });
        }

        private LOG_LEVEL(String s, int i)
        {
            super(s, i);
        }
    }


    public static boolean a = true;
    public static boolean b = true;
    public static LOG_LEVEL c;
    public static String d = null;

    private Setting()
    {
    }

    public static void saveLogFile(LOG_LEVEL log_level, String s)
    {
        c = log_level;
        d = s;
    }

    public static void setLocationEnable(boolean flag)
    {
        b = flag;
    }

    public static void showLogcat(boolean flag)
    {
        a = flag;
        if (!MSC.isLoaded())
        {
            break MISSING_BLOCK_LABEL_17;
        }
        MSC.DebugLog(a);
        return;
        UnsatisfiedLinkError unsatisfiedlinkerror;
        unsatisfiedlinkerror;
    }

    static 
    {
        c = LOG_LEVEL.none;
    }
}
