// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import com.iflytek.speech.UnderstanderResult;

// Referenced classes of package com.iflytek.cloud:
//            SpeechError, UnderstanderResult, SpeechUnderstander

final class k extends com.iflytek.speech.SpeechUnderstanderListener.Stub
{

    final SpeechUnderstander a;
    final SpeechUnderstander.a b;

    k(SpeechUnderstander.a a1, SpeechUnderstander speechunderstander)
    {
        b = a1;
        a = speechunderstander;
        super();
    }

    public void onBeginOfSpeech()
        throws RemoteException
    {
        Message message = SpeechUnderstander.a.b(b).obtainMessage(2);
        SpeechUnderstander.a.b(b).sendMessage(message);
    }

    public void onEndOfSpeech()
        throws RemoteException
    {
        Message message = SpeechUnderstander.a.b(b).obtainMessage(3);
        SpeechUnderstander.a.b(b).sendMessage(message);
    }

    public void onError(int i)
        throws RemoteException
    {
        Message message = SpeechUnderstander.a.b(b).obtainMessage(0, new SpeechError(i));
        SpeechUnderstander.a.b(b).sendMessage(message);
    }

    public void onEvent(int i, int j, int l, Bundle bundle)
        throws RemoteException
    {
        Message message = new Message();
        message.what = i;
        message.arg1 = j;
        message.arg2 = l;
        message.obj = bundle;
        bundle = SpeechUnderstander.a.b(b).obtainMessage(6, 0, 0, message);
        SpeechUnderstander.a.b(b).sendMessage(bundle);
    }

    public void onResult(UnderstanderResult understanderresult)
        throws RemoteException
    {
        understanderresult = SpeechUnderstander.a.b(b).obtainMessage(4, new com.iflytek.cloud.UnderstanderResult(understanderresult.getResultString()));
        SpeechUnderstander.a.b(b).sendMessage(understanderresult);
    }

    public void onVolumeChanged(int i)
        throws RemoteException
    {
        Message message = SpeechUnderstander.a.b(b).obtainMessage(1, i, 0, null);
        SpeechUnderstander.a.b(b).sendMessage(message);
    }
}
