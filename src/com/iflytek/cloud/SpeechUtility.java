// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import com.iflytek.cloud.a.d.d;
import com.iflytek.cloud.a.e.b;
import com.iflytek.cloud.b.a;
import com.iflytek.msc.MSC;
import com.iflytek.msc.MSCSessionInfo;
import com.iflytek.speech.SpeechComponent;
import com.iflytek.speech.UtilityConfig;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.iflytek.cloud:
//            Setting, SpeechError, SpeechRecognizer, SpeechSynthesizer, 
//            SpeechUnderstander, TextUnderstander

public class SpeechUtility extends d
{
    private class a extends BroadcastReceiver
    {

        final SpeechUtility a;

        public void onReceive(Context context, Intent intent)
        {
            context = intent.getAction();
            intent = intent.getDataString();
            String s = String.valueOf("package:").concat("com.iflytek.speechcloud");
            if (("android.intent.action.PACKAGE_ADDED".equals(context) || "android.intent.action.PACKAGE_REMOVED".equals(context) || "android.intent.action.PACKAGE_REPLACED".equals(context)) && s.equals(intent) && SpeechUtility.getUtility() != null)
            {
                SpeechUtility.getUtility().checkServiceInstalled();
            }
        }

        private a()
        {
            a = SpeechUtility.this;
            super();
        }

    }


    public static final String TAG_RESOURCE_CONTENT = "tag_rescontent";
    public static final String TAG_RESOURCE_RESULT = "result";
    public static final String TAG_RESOURCE_RET = "ret";
    private static SpeechUtility c = null;
    protected com.iflytek.cloud.a.d.d.a a;
    private ArrayList d;
    private int e;
    private Context f;
    private boolean g;
    private a h;

    private SpeechUtility(Context context, String s)
    {
        d = new ArrayList();
        e = -1;
        f = null;
        g = false;
        h = null;
        a = com.iflytek.cloud.a.d.d.a.c;
        f = context.getApplicationContext();
        super.setParameter("params", s);
        MSC.loadLibrary(b.b("lib_name", "msc"));
        if (MSC.isLoaded())
        {
            try
            {
                MSC.DebugLog(com.iflytek.cloud.Setting.a);
            }
            catch (UnsatisfiedLinkError unsatisfiedlinkerror) { }
        }
        setParameter("params", s);
        s = getParameter("engine_mode");
        if ("msc".equals(s))
        {
            a = com.iflytek.cloud.a.d.d.a.a;
        } else
        if ("plus".equals(s))
        {
            a = com.iflytek.cloud.a.d.d.a.b;
        }
        b();
        d();
        e();
        try
        {
            com.iflytek.common.a.a(context, "appid", b.e("appid"));
            com.iflytek.common.a.a(context);
            com.iflytek.common.a.a(false);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    private void a(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        PackageManager packagemanager = f.getPackageManager();
        s = new Intent(s);
        s.setPackage("com.iflytek.speechcloud");
        s = packagemanager.queryIntentServices(s, 224);
        if (s == null || s.size() <= 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        s = s.iterator();
_L6:
        SpeechComponent speechcomponent;
        ResolveInfo resolveinfo;
        do
        {
            if (!s.hasNext())
            {
                continue; /* Loop/switch isn't completed */
            }
            resolveinfo = (ResolveInfo)s.next();
            speechcomponent = b(resolveinfo.serviceInfo.packageName);
        } while (speechcomponent == null);
        String as[];
        int j;
        as = resolveinfo.serviceInfo.metaData.getString("enginetype").split(",");
        j = as.length;
        int i = 0;
_L4:
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        speechcomponent.addEngine(as[i]);
        i++;
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        exception.printStackTrace();
        if (true) goto _L6; else goto _L5
_L5:
        if (true) goto _L1; else goto _L7
_L7:
    }

    private static boolean a(Context context)
    {
        Object obj;
        int i;
        i = Process.myPid();
        obj = (ActivityManager)context.getSystemService("activity");
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_145;
        }
        obj = ((ActivityManager) (obj)).getRunningAppProcesses().iterator();
        android.app.ActivityManager.RunningAppProcessInfo runningappprocessinfo;
        do
        {
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break MISSING_BLOCK_LABEL_145;
                }
                runningappprocessinfo = (android.app.ActivityManager.RunningAppProcessInfo)((Iterator) (obj)).next();
            } while (runningappprocessinfo.pid != i);
            com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("process name:").append(runningappprocessinfo.processName).toString());
        } while (!context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).packageName.equals(runningappprocessinfo.processName));
        com.iflytek.cloud.a.g.a.a.a((new StringBuilder()).append("process name:").append(runningappprocessinfo.processName).append("is own process").toString());
        return true;
        context;
        context.printStackTrace();
        return false;
    }

    private int b()
    {
        if (!MSC.isLoaded())
        {
            return 21002;
        }
        com.iflytek.cloud.a.g.a.a.a("SpeechUtility start login");
        SpeechError speecherror = (new b(f, b)).a(b.e("usr"), b.e("pwd"));
        if (speecherror == null)
        {
            return 0;
        } else
        {
            return speecherror.getErrorCode();
        }
    }

    private SpeechComponent b(String s)
    {
        Iterator iterator;
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        iterator = d.iterator();
_L4:
        if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        if (!s.equals(((SpeechComponent)iterator.next()).getPackageName())) goto _L4; else goto _L3
_L3:
        boolean flag = true;
_L6:
        if (!flag)
        {
            s = new SpeechComponent(s);
            d.add(s);
        } else
        {
            s = null;
        }
        return s;
_L2:
        flag = false;
        if (true) goto _L6; else goto _L5
_L5:
    }

    private boolean c()
    {
        boolean flag;
        if (!MSC.isLoaded())
        {
            flag = false;
        } else
        {
            com.iflytek.cloud.a.g.a.b.a("QMSPLogOut", null);
            flag = true;
            if (MSC.isLoaded())
            {
                return com.iflytek.cloud.a.e.a.a();
            }
        }
        return flag;
    }

    private boolean c(String s)
    {
        PackageManager packagemanager = f.getPackageManager();
        s = new Intent(s);
        s.setPackage("com.iflytek.speechcloud");
        return packagemanager.queryIntentActivities(s, 1).size() > 0;
    }

    public static SpeechUtility createUtility(Context context, String s)
    {
        if (c == null)
        {
            com.iflytek.cloud.b.a a1 = new com.iflytek.cloud.b.a();
            a1.b(s);
            if (!a1.a("force_login", false) && !a(context.getApplicationContext()))
            {
                com.iflytek.cloud.a.g.a.a.b("init failed, please call this method in your main process!");
                c = null;
            } else
            {
                c = new SpeechUtility(context, s);
            }
        }
        return c;
    }

    private void d()
    {
        if (checkServiceInstalled())
        {
            a("com.iflytek.component.speechrecognizer");
            a("com.iflytek.component.speechsynthesizer");
            a("com.iflytek.component.speechunderstander");
            a("com.iflytek.component.textunderstander");
            a("com.iflytek.component.speechwakeuper");
        }
    }

    private void e()
    {
        h = new a();
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentfilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentfilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentfilter.addDataScheme("package");
        f.registerReceiver(h, intentfilter);
    }

    public static SpeechUtility getUtility()
    {
        return c;
    }

    protected boolean a()
    {
        boolean flag = false;
        PackageInfo packageinfo;
        try
        {
            packageinfo = f.getPackageManager().getPackageInfo("com.iflytek.speechcloud", 0);
        }
        catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception)
        {
            return false;
        }
        if (packageinfo != null)
        {
            flag = true;
        }
        return flag;
    }

    public boolean checkServiceInstalled()
    {
        byte byte0;
        boolean flag;
        boolean flag1;
        flag = false;
        flag1 = false;
        byte0 = -1;
        PackageInfo packageinfo = f.getPackageManager().getPackageInfo("com.iflytek.speechcloud", 0);
        int i;
        i = byte0;
        flag = flag1;
        if (packageinfo == null)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        flag = true;
        flag1 = true;
        i = packageinfo.versionCode;
        flag = flag1;
_L2:
        if (flag != g || e != i)
        {
            g = flag;
            e = i;
            if (SpeechRecognizer.getRecognizer() != null)
            {
                SpeechRecognizer.getRecognizer().a(f);
            }
            if (SpeechSynthesizer.getSynthesizer() != null)
            {
                SpeechSynthesizer.getSynthesizer().a(f);
            }
            if (SpeechUnderstander.getUnderstander() != null)
            {
                SpeechUnderstander.getUnderstander().a(f);
            }
            if (TextUnderstander.getTextUnderstander() != null)
            {
                TextUnderstander.getTextUnderstander().a(f);
            }
        }
        return flag;
        android.content.pm.PackageManager.NameNotFoundException namenotfoundexception;
        namenotfoundexception;
        i = byte0;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public boolean destroy()
    {
        boolean flag = true;
        if (c != null)
        {
            flag = c();
        }
        if (h != null)
        {
            f.unregisterReceiver(h);
            h = null;
        }
        if (flag)
        {
            c = null;
            com.iflytek.cloud.a.g.a.a.a(" SpeechUtility destory success,mInstance=null");
        }
        return flag;
    }

    public String getComponentUrl()
    {
        StringBuffer stringbuffer = new StringBuffer("http://open.voicecloud.cn/s?");
        UtilityConfig.appendHttpParam(stringbuffer, "key", URLEncoder.encode(Base64.encodeToString(UtilityConfig.getComponentUrlParam(f).getBytes(), 0)));
        UtilityConfig.appendHttpParam(stringbuffer, "version", "1.0");
        return stringbuffer.toString();
    }

    public com.iflytek.cloud.a.d.d.a getEngineMode()
    {
        return a;
    }

    public String getParameter(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        if (b.g(s))
        {
            return super.getParameter(s);
        }
        if (s.equals("tts") || s.equals("asr") || s.equals("all") || s.equals("ivw"))
        {
            try
            {
                s = getPlusLocalInfo(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return "{ret:20004}";
            }
            return s;
        }
        if (!MSC.isLoaded()) goto _L1; else goto _L3
_L3:
        MSCSessionInfo mscsessioninfo;
        s = s.getBytes("utf-8");
        mscsessioninfo = new MSCSessionInfo();
        if (MSC.QMSPGetParam(s, mscsessioninfo) != 0) goto _L1; else goto _L4
_L4:
        s = new String(mscsessioninfo.buffer, "utf-8");
        return s;
        s;
        s.printStackTrace();
        return null;
        s;
        s.printStackTrace();
        return null;
    }

    public String getPlusLocalInfo(String s)
        throws JSONException
    {
        JSONObject jsonobject = new JSONObject();
        if (!checkServiceInstalled())
        {
            jsonobject.put("ret", 21001);
            return jsonobject.toString();
        }
        if (getServiceVersion() < 97 || 10000 <= getServiceVersion() && getServiceVersion() <= 11000)
        {
            jsonobject.put("ret", 20018);
            return jsonobject.toString();
        }
        Cursor cursor = f.getContentResolver().query(Uri.parse("content://com.iflytek.speechcloud.providers.LocalResourceProvider"), null, s, null, null);
        int i = cursor.getColumnIndex("tag_rescontent");
        if (cursor != null && cursor.moveToFirst())
        {
            s = cursor.getString(i);
            Log.v("SpeechUtility", s);
        } else
        {
            s = "";
        }
        if (cursor != null)
        {
            cursor.close();
        }
        if (TextUtils.isEmpty(s))
        {
            jsonobject.put("ret", 20004);
            return jsonobject.toString();
        } else
        {
            jsonobject.put("ret", 0);
            jsonobject.put("result", new JSONObject(s));
            return jsonobject.toString();
        }
    }

    public int getServiceVersion()
    {
        if (e >= 0)
        {
            break MISSING_BLOCK_LABEL_33;
        }
        PackageInfo packageinfo = f.getPackageManager().getPackageInfo("com.iflytek.speechcloud", 0);
        if (packageinfo != null)
        {
            try
            {
                e = packageinfo.versionCode;
            }
            catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception) { }
        }
        return e;
    }

    public int openEngineSettings(String s)
    {
        String s1;
        Intent intent;
        try
        {
            intent = new Intent();
            intent.setPackage("com.iflytek.speechcloud");
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return 21002;
        }
        s1 = "com.iflytek.speechcloud";
        if (!"tts".equals(s) || !c("com.iflytek.speechcloud.activity.speaker.SpeakerSetting")) goto _L2; else goto _L1
_L1:
        s = "com.iflytek.speechcloud.activity.speaker.SpeakerSetting";
_L4:
        intent.setAction(s);
        intent.addFlags(0x10000000);
        f.startActivity(intent);
        return 0;
_L2:
        if ("asr".equals(s) && c("com.iflytek.speechcloud.settings.asr"))
        {
            s = "com.iflytek.speechcloud.settings.asr";
            continue; /* Loop/switch isn't completed */
        }
        s = s1;
        if (c("com.iflytek.speechcloud.settings.main"))
        {
            s = "com.iflytek.speechcloud.settings.main";
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public String[] queryAvailableEngines()
    {
        d.clear();
        d();
        ArrayList arraylist = new ArrayList();
        for (Iterator iterator = d.iterator(); iterator.hasNext(); arraylist.addAll(((SpeechComponent)iterator.next()).getEngines())) { }
        String as[] = new String[arraylist.size()];
        for (int i = 0; i < arraylist.size(); i++)
        {
            as[i] = (String)arraylist.get(i);
        }

        return as;
    }

    public boolean setParameter(String s, String s1)
    {
        boolean flag1 = true;
        if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s1)) goto _L2; else goto _L1
_L1:
        boolean flag = false;
_L4:
        return flag;
_L2:
        super.setParameter(s, s1);
        flag = flag1;
        if (!MSC.isLoaded()) goto _L4; else goto _L3
_L3:
        flag = flag1;
        if ("params".equals(s)) goto _L4; else goto _L5
_L5:
        int i = MSC.QMSPSetParam(s.getBytes("utf-8"), s1.getBytes("utf-8"));
        flag = flag1;
        if (i != 0)
        {
            return false;
        }
          goto _L4
        s;
        s.printStackTrace();
_L7:
        return false;
        s;
        s.printStackTrace();
        if (true) goto _L7; else goto _L6
_L6:
    }

}
