// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.content.Context;
import com.iflytek.cloud.a.d.d;
import com.iflytek.cloud.d.a.a;

// Referenced classes of package com.iflytek.cloud:
//            InitListener, SpeechListener, VerifierListener

public class SpeakerVerifier extends d
{

    private static SpeakerVerifier a = null;
    private a c;

    protected SpeakerVerifier(Context context, InitListener initlistener)
    {
        c = null;
        c = new a(context);
    }

    public static SpeakerVerifier createVerifier(Context context, InitListener initlistener)
    {
        if (a == null)
        {
            a = new SpeakerVerifier(context, initlistener);
        }
        return a;
    }

    public static SpeakerVerifier getVerifier()
    {
        return a;
    }

    public void cancel(boolean flag)
    {
        c.cancel(flag);
    }

    public boolean destroy()
    {
        boolean flag = true;
        if (c != null)
        {
            flag = c.destroy();
        }
        if (flag)
        {
            a = null;
        }
        return flag;
    }

    public String generatePassword(int i)
    {
        return c.a(i);
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public void getPasswordList(SpeechListener speechlistener)
    {
        c.setParameter("params", null);
        b.a("subject", "ivp", true);
        b.a("rse", "gb2312", false);
        c.setParameter(b);
        c.a(speechlistener);
    }

    public boolean isListening()
    {
        return c.f();
    }

    public void sendRequest(String s, String s1, SpeechListener speechlistener)
    {
        c.setParameter(b);
        c.a(s, s1, speechlistener);
    }

    public boolean setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public void startListening(VerifierListener verifierlistener)
    {
        c.setParameter(b);
        c.a(verifierlistener);
    }

    public void stopListening()
    {
        c.e();
    }

    public int writeAudio(byte abyte0[], int i, int j)
    {
        if (c != null && c.f())
        {
            return c.a(abyte0, i, j);
        } else
        {
            com.iflytek.cloud.a.g.a.a.b("SpeakerVerifier writeAudio failed, is not running");
            return 21004;
        }
    }

}
