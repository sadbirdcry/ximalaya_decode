// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.iflytek.speech.RecognizerListener;

// Referenced classes of package com.iflytek.cloud:
//            RecognizerListener, SpeechRecognizer, f, e, 
//            SpeechError, RecognizerResult

private final class c
    implements com.iflytek.cloud.RecognizerListener
{

    final SpeechRecognizer a;
    private com.iflytek.cloud.RecognizerListener b;
    private RecognizerListener c;
    private Handler d;

    static Handler a(c c1)
    {
        return c1.d;
    }

    static com.iflytek.cloud.RecognizerListener b(d d1)
    {
        return d1.b;
    }

    static RecognizerListener c(b b1)
    {
        return b1.c;
    }

    public void onBeginOfSpeech()
    {
        Message message = d.obtainMessage(2, 0, 0, null);
        d.sendMessage(message);
    }

    public void onEndOfSpeech()
    {
        Message message = d.obtainMessage(3, 0, 0, null);
        d.sendMessage(message);
    }

    public void onError(SpeechError speecherror)
    {
        speecherror = d.obtainMessage(0, speecherror);
        d.sendMessage(speecherror);
    }

    public void onEvent(int i, int j, int k, Bundle bundle)
    {
        Message message = new Message();
        message.what = i;
        message.arg1 = j;
        message.arg2 = k;
        message.obj = bundle;
        bundle = d.obtainMessage(6, 0, 0, message);
        d.sendMessage(bundle);
    }

    public void onResult(RecognizerResult recognizerresult, boolean flag)
    {
        int i = 1;
        Handler handler = d;
        if (!flag)
        {
            i = 0;
        }
        recognizerresult = handler.obtainMessage(4, i, 0, recognizerresult);
        d.sendMessage(recognizerresult);
    }

    public void onVolumeChanged(int i)
    {
        Message message = d.obtainMessage(1, i, 0, null);
        d.sendMessage(message);
    }

    public (SpeechRecognizer speechrecognizer, com.iflytek.cloud.RecognizerListener recognizerlistener)
    {
        a = speechrecognizer;
        super();
        b = null;
        c = null;
        d = new f(this, Looper.getMainLooper());
        b = recognizerlistener;
        c = new e(this, speechrecognizer);
    }
}
