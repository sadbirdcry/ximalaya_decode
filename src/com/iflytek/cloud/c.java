// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.cloud;

import android.os.RemoteException;

// Referenced classes of package com.iflytek.cloud:
//            GrammarListener, SpeechError, SpeechRecognizer

final class c extends com.iflytek.speech.GrammarListener.Stub
{

    final GrammarListener a;
    final SpeechRecognizer b;

    c(SpeechRecognizer speechrecognizer, GrammarListener grammarlistener)
    {
        b = speechrecognizer;
        a = grammarlistener;
        super();
    }

    public void onBuildFinish(String s, int i)
        throws RemoteException
    {
        if (a != null)
        {
            GrammarListener grammarlistener = a;
            SpeechError speecherror;
            if (i == 0)
            {
                speecherror = null;
            } else
            {
                speecherror = new SpeechError(i);
            }
            grammarlistener.onBuildFinish(s, speecherror);
        }
    }
}
