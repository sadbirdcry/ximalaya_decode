// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.msc;

import java.io.FileDescriptor;

// Referenced classes of package com.iflytek.msc:
//            MSCSessionInfo

public class MSC
{

    private static boolean a = false;

    public MSC()
    {
    }

    public static final native int DebugLog(boolean flag);

    public static final native int QHCRDataWrite(char ac[], byte abyte0[], byte abyte1[], int i, int j);

    public static final native int QHCRFini();

    public static final native byte[] QHCRGetResult(char ac[], byte abyte0[], MSCSessionInfo mscsessioninfo);

    public static final native int QHCRInit(byte abyte0[]);

    public static final native int QHCRLogEvent(char ac[], byte abyte0[], byte abyte1[]);

    public static final native char[] QHCRSessionBegin(byte abyte0[], MSCSessionInfo mscsessioninfo);

    public static final native int QHCRSessionEnd(char ac[], byte abyte0[]);

    public static final native int QISEAudioWrite(char ac[], byte abyte0[], int i, int j, MSCSessionInfo mscsessioninfo);

    public static final native int QISEFini();

    public static final native int QISEGetParam(char ac[], byte abyte0[], MSCSessionInfo mscsessioninfo);

    public static final native byte[] QISEGetResult(char ac[], MSCSessionInfo mscsessioninfo);

    public static final native int QISEInit(byte abyte0[]);

    public static final native char[] QISESessionBegin(byte abyte0[], byte abyte1[], MSCSessionInfo mscsessioninfo);

    public static final native int QISESessionEnd(char ac[], byte abyte0[]);

    public static final native int QISETextPut(char ac[], byte abyte0[], byte abyte1[]);

    public static final native int QISRAudioWrite(char ac[], byte abyte0[], int i, int j, MSCSessionInfo mscsessioninfo);

    public static final native int QISRBuildGrammar(byte abyte0[], byte abyte1[], int i, byte abyte2[], String s, Object obj);

    public static final native int QISRFini();

    public static final native int QISRGetParam(char ac[], byte abyte0[], MSCSessionInfo mscsessioninfo);

    public static final native byte[] QISRGetResult(char ac[], MSCSessionInfo mscsessioninfo);

    public static final native int QISRGrammarActivate(char ac[], byte abyte0[], byte abyte1[]);

    public static final native int QISRInit(byte abyte0[]);

    public static final native int QISRLogEvent(char ac[], byte abyte0[], byte abyte1[]);

    public static final native int QISRRegisterNotify(char ac[], String s, String s1, String s2, Object obj);

    public static final native char[] QISRSessionBegin(byte abyte0[], byte abyte1[], MSCSessionInfo mscsessioninfo);

    public static final native int QISRSessionEnd(char ac[], byte abyte0[]);

    public static final native int QISRSetParam(char ac[], byte abyte0[], byte abyte1[]);

    public static final native int QISRUpdateLexicon(byte abyte0[], byte abyte1[], int i, byte abyte2[], String s, Object obj);

    public static final native byte[] QISRUploadData(char ac[], byte abyte0[], byte abyte1[], int i, byte abyte2[], MSCSessionInfo mscsessioninfo);

    public static final native int QISVAudioWrite(char ac[], char ac1[], byte abyte0[], int i, int j, MSCSessionInfo mscsessioninfo);

    public static final native int QISVFini();

    public static final native int QISVGetParam(char ac[], byte abyte0[], MSCSessionInfo mscsessioninfo);

    public static final native byte[] QISVGetResult(char ac[], char ac1[], MSCSessionInfo mscsessioninfo);

    public static final native int QISVInit(byte abyte0[]);

    public static final native char[] QISVQueDelModel(byte abyte0[], byte abyte1[], MSCSessionInfo mscsessioninfo);

    public static final native int QISVQueDelModelRelease(char ac[]);

    public static final native char[] QISVSessionBegin(byte abyte0[], byte abyte1[], MSCSessionInfo mscsessioninfo);

    public static final native int QISVSessionEnd(char ac[], byte abyte0[]);

    public static final native int QIVWAudioWrite(char ac[], byte abyte0[], int i, int j, MSCSessionInfo mscsessioninfo);

    public static final native int QIVWRegisterNotify(char ac[], String s, Object obj);

    public static final native char[] QIVWSessionBegin(byte abyte0[], byte abyte1[], MSCSessionInfo mscsessioninfo);

    public static final native int QIVWSessionEnd(char ac[], byte abyte0[]);

    public static final native int QMSPDownload(byte abyte0[], byte abyte1[], Object obj);

    public static final native byte[] QMSPDownloadData(byte abyte0[], MSCSessionInfo mscsessioninfo);

    public static final native int QMSPGetParam(byte abyte0[], MSCSessionInfo mscsessioninfo);

    public static final native int QMSPLogOut();

    public static final native int QMSPLogin(byte abyte0[], byte abyte1[], byte abyte2[]);

    public static final native int QMSPRegisterNotify(String s, String s1);

    public static final native byte[] QMSPSearch(byte abyte0[], byte abyte1[], MSCSessionInfo mscsessioninfo);

    public static final native int QMSPSetParam(byte abyte0[], byte abyte1[]);

    public static final native byte[] QMSPUploadData(byte abyte0[], byte abyte1[], int i, byte abyte2[], MSCSessionInfo mscsessioninfo);

    public static final native byte[] QTTSAudioGet(char ac[], MSCSessionInfo mscsessioninfo);

    public static final native char[] QTTSAudioInfo(char ac[]);

    public static final native int QTTSFini();

    public static final native int QTTSGetParam(char ac[], byte abyte0[], MSCSessionInfo mscsessioninfo);

    public static final native int QTTSInit(byte abyte0[]);

    public static final native char[] QTTSSessionBegin(byte abyte0[], MSCSessionInfo mscsessioninfo);

    public static final native int QTTSSessionEnd(char ac[], byte abyte0[]);

    public static final native int QTTSTextPut(char ac[], byte abyte0[]);

    public static final native int UMSPLogin(byte abyte0[], byte abyte1[], byte abyte2[], Object obj);

    public static final native int getFileDescriptorFD(FileDescriptor filedescriptor);

    public static boolean isLoaded()
    {
        return a;
    }

    public static boolean loadLibrary(String s)
    {
        if (a)
        {
            return a;
        }
        try
        {
            System.loadLibrary(s);
            a = true;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            a = false;
        }
        return a;
    }

}
