// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.Intent;

public interface ISpeechModule
{

    public abstract boolean destory();

    public abstract Intent getIntent();

    public abstract String getParameter(String s);

    public abstract boolean isAvailable();

    public abstract int setParameter(String s, String s1);
}
