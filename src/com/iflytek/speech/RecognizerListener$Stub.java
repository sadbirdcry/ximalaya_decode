// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

// Referenced classes of package com.iflytek.speech:
//            RecognizerListener, RecognizerResult

public static abstract class attachInterface extends Binder
    implements RecognizerListener
{
    private static class Proxy
        implements RecognizerListener
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.RecognizerListener";
        }

        public void onBeginOfSpeech()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.RecognizerListener");
            mRemote.transact(2, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onEndOfSpeech()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.RecognizerListener");
            mRemote.transact(3, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onError(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.RecognizerListener");
            parcel.writeInt(i);
            mRemote.transact(5, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onEvent(int i, int j, int k, Bundle bundle)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.RecognizerListener");
            parcel.writeInt(i);
            parcel.writeInt(j);
            parcel.writeInt(k);
            if (bundle == null)
            {
                break MISSING_BLOCK_LABEL_71;
            }
            parcel.writeInt(1);
            bundle.writeToParcel(parcel, 0);
_L1:
            mRemote.transact(6, parcel, null, 1);
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            bundle;
            parcel.recycle();
            throw bundle;
        }

        public void onResult(RecognizerResult recognizerresult, boolean flag)
            throws RemoteException
        {
            Parcel parcel;
            int i;
            i = 1;
            parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.RecognizerListener");
            if (recognizerresult == null) goto _L2; else goto _L1
_L1:
            parcel.writeInt(1);
            recognizerresult.writeToParcel(parcel, 0);
              goto _L3
_L5:
            parcel.writeInt(i);
            mRemote.transact(4, parcel, null, 1);
            parcel.recycle();
            return;
_L2:
            parcel.writeInt(0);
            break; /* Loop/switch isn't completed */
            recognizerresult;
            parcel.recycle();
            throw recognizerresult;
_L6:
            i = 0;
            if (true) goto _L4; else goto _L3
_L4:
            break; /* Loop/switch isn't completed */
_L3:
            if (!flag) goto _L6; else goto _L5
        }

        public void onVolumeChanged(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.RecognizerListener");
            parcel.writeInt(i);
            mRemote.transact(1, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    private static final String DESCRIPTOR = "com.iflytek.speech.RecognizerListener";
    static final int TRANSACTION_onBeginOfSpeech = 2;
    static final int TRANSACTION_onEndOfSpeech = 3;
    static final int TRANSACTION_onError = 5;
    static final int TRANSACTION_onEvent = 6;
    static final int TRANSACTION_onResult = 4;
    static final int TRANSACTION_onVolumeChanged = 1;

    public static RecognizerListener asInterface(IBinder ibinder)
    {
        if (ibinder == null)
        {
            return null;
        }
        android.os.IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.RecognizerListener");
        if (iinterface != null && (iinterface instanceof RecognizerListener))
        {
            return (RecognizerListener)iinterface;
        } else
        {
            return new Proxy(ibinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
        throws RemoteException
    {
        Object obj1 = null;
        Object obj = null;
        int k;
        switch (i)
        {
        default:
            return super.onTransact(i, parcel, parcel1, j);

        case 1598968902: 
            parcel1.writeString("com.iflytek.speech.RecognizerListener");
            return true;

        case 1: // '\001'
            parcel.enforceInterface("com.iflytek.speech.RecognizerListener");
            onVolumeChanged(parcel.readInt());
            return true;

        case 2: // '\002'
            parcel.enforceInterface("com.iflytek.speech.RecognizerListener");
            onBeginOfSpeech();
            return true;

        case 3: // '\003'
            parcel.enforceInterface("com.iflytek.speech.RecognizerListener");
            onEndOfSpeech();
            return true;

        case 4: // '\004'
            parcel.enforceInterface("com.iflytek.speech.RecognizerListener");
            parcel1 = obj;
            if (parcel.readInt() != 0)
            {
                parcel1 = (RecognizerResult)RecognizerResult.CREATOR.eFromParcel(parcel);
            }
            boolean flag;
            if (parcel.readInt() != 0)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            onResult(parcel1, flag);
            return true;

        case 5: // '\005'
            parcel.enforceInterface("com.iflytek.speech.RecognizerListener");
            onError(parcel.readInt());
            return true;

        case 6: // '\006'
            parcel.enforceInterface("com.iflytek.speech.RecognizerListener");
            i = parcel.readInt();
            j = parcel.readInt();
            k = parcel.readInt();
            parcel1 = obj1;
            break;
        }
        if (parcel.readInt() != 0)
        {
            parcel1 = (Bundle)Bundle.CREATOR.eFromParcel(parcel);
        }
        onEvent(i, j, k, parcel1);
        return true;
    }

    public Proxy.mRemote()
    {
        attachInterface(this, "com.iflytek.speech.RecognizerListener");
    }
}
