// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Parcel;
import android.os.Parcelable;

public class UnderstanderResult
    implements Parcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new _cls1();
    private String mXml;

    public UnderstanderResult(Parcel parcel)
    {
        mXml = "";
        mXml = parcel.readString();
    }

    public UnderstanderResult(String s)
    {
        mXml = "";
        if (s != null)
        {
            mXml = s;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String getResultString()
    {
        return mXml;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(mXml);
    }


    private class _cls1
        implements android.os.Parcelable.Creator
    {

        public UnderstanderResult createFromParcel(Parcel parcel)
        {
            return new UnderstanderResult(parcel);
        }

        public volatile Object createFromParcel(Parcel parcel)
        {
            return createFromParcel(parcel);
        }

        public UnderstanderResult[] newArray(int i)
        {
            return new UnderstanderResult[i];
        }

        public volatile Object[] newArray(int i)
        {
            return newArray(i);
        }

        _cls1()
        {
        }
    }

}
