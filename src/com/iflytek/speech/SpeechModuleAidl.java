// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.b.a;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

// Referenced classes of package com.iflytek.speech:
//            ISpeechModule, UtilityConfig

abstract class SpeechModuleAidl
    implements ISpeechModule
{

    private String mBindAction;
    private ServiceConnection mConnection;
    protected Context mContext;
    private InitListener mInitListener;
    private HashMap mParams;
    protected IInterface mService;
    protected Object mSynLock;
    private Handler mUiHandler;
    private volatile boolean userDestroy;

    public SpeechModuleAidl(Context context, InitListener initlistener, String s)
    {
        mSynLock = new Object();
        mContext = null;
        mConnection = null;
        mInitListener = null;
        mBindAction = null;
        mParams = new HashMap();
        userDestroy = false;
        mUiHandler = new _cls2(Looper.getMainLooper());
        mContext = context;
        mInitListener = initlistener;
        mBindAction = s;
        bindService();
    }

    private void bindService()
    {
        if (!isActionInstalled(mContext, mBindAction))
        {
            if (mInitListener != null)
            {
                Message.obtain(mUiHandler, 21001, 0, 0, null).sendToTarget();
            }
            return;
        } else
        {
            Intent intent = getIntent();
            intent.setAction(mBindAction);
            intent.setPackage("com.iflytek.speechcloud");
            mConnection = new _cls1();
            mContext.bindService(intent, mConnection, 1);
            return;
        }
    }

    private IInterface getService(IBinder ibinder)
    {
        String s = ((Class)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getName();
        Log.d(getTag(), (new StringBuilder()).append("className = ").append(s).toString());
        ibinder = (IInterface)Class.forName((new StringBuilder()).append(s).append("$Stub").toString()).getDeclaredMethod("asInterface", new Class[] {
            android/os/IBinder
        }).invoke(null, new Object[] {
            ibinder
        });
        return ibinder;
        ibinder;
        ibinder.printStackTrace();
_L2:
        return null;
        ibinder;
        ibinder.printStackTrace();
        continue; /* Loop/switch isn't completed */
        ibinder;
        ibinder.printStackTrace();
        continue; /* Loop/switch isn't completed */
        ibinder;
        ibinder.printStackTrace();
        continue; /* Loop/switch isn't completed */
        ibinder;
        ibinder.printStackTrace();
        continue; /* Loop/switch isn't completed */
        ibinder;
        ibinder.printStackTrace();
        continue; /* Loop/switch isn't completed */
        ibinder;
        ibinder.printStackTrace();
        if (true) goto _L2; else goto _L1
_L1:
    }

    public boolean destory()
    {
        Log.d(getTag(), "destory");
        try
        {
            userDestroy = true;
            if (mConnection != null)
            {
                mContext.unbindService(mConnection);
                mConnection = null;
            }
        }
        catch (IllegalArgumentException illegalargumentexception)
        {
            illegalargumentexception.printStackTrace();
            return false;
        }
        return true;
    }

    public Intent getIntent()
    {
        Intent intent = new Intent();
        if (!mParams.isEmpty())
        {
            String s;
            for (Iterator iterator = mParams.keySet().iterator(); iterator.hasNext(); intent.putExtra(s, (String)mParams.get(s)))
            {
                s = (String)iterator.next();
            }

            HashMap hashmap = (new a((String)mParams.get("params"), (String[][])null)).c();
            if (hashmap != null && !hashmap.isEmpty())
            {
                String s1;
                for (Iterator iterator1 = hashmap.keySet().iterator(); iterator1.hasNext(); intent.putExtra(s1, (String)hashmap.get(s1)))
                {
                    s1 = (String)iterator1.next();
                }

            }
        }
        intent.putExtra("caller.appid", SpeechUtility.getUtility().getParameter("appid"));
        intent.putExtra("caller.name", UtilityConfig.getCallerInfo(mContext, "caller.name"));
        intent.putExtra("caller.pkg", UtilityConfig.getCallerInfo(mContext, "caller.pkg"));
        intent.putExtra("caller.ver.name", UtilityConfig.getCallerInfo(mContext, "caller.ver.name"));
        intent.putExtra("caller.ver.code", UtilityConfig.getCallerInfo(mContext, "caller.ver.code"));
        return intent;
    }

    public String getParameter(String s)
    {
        return (String)mParams.get(s);
    }

    protected final String getTag()
    {
        return getClass().toString();
    }

    public boolean isActionInstalled(Context context, String s)
    {
        while (context == null || TextUtils.isEmpty(s) || context.getPackageManager().resolveService(new Intent(s), 0) == null) 
        {
            return false;
        }
        return true;
    }

    public boolean isAvailable()
    {
        return mService != null;
    }

    public int setParameter(String s, String s1)
    {
        if (TextUtils.isEmpty(s))
        {
            return 20012;
        }
        if (TextUtils.isEmpty(s1))
        {
            mParams.remove(s);
            return 0;
        } else
        {
            mParams.put(s, s1);
            return 0;
        }
    }






    private class _cls2 extends Handler
    {

        final SpeechModuleAidl this$0;

        public void handleMessage(Message message)
        {
            if (mInitListener == null)
            {
                return;
            } else
            {
                mInitListener.onInit(message.what);
                return;
            }
        }

        _cls2(Looper looper)
        {
            this$0 = SpeechModuleAidl.this;
            super(looper);
        }
    }


    private class _cls1
        implements ServiceConnection
    {

        final SpeechModuleAidl this$0;

        public void onServiceConnected(ComponentName componentname, IBinder ibinder)
        {
            synchronized (mSynLock)
            {
                Log.d(getTag(), "init success");
                mService = getService(ibinder);
                Log.d(getTag(), (new StringBuilder()).append("mService :").append(mService).toString());
                if (mInitListener != null)
                {
                    Message.obtain(mUiHandler, 0, 0, 0, null).sendToTarget();
                }
            }
            return;
            ibinder;
            componentname;
            JVM INSTR monitorexit ;
            throw ibinder;
        }

        public void onServiceDisconnected(ComponentName componentname)
        {
            Log.d(getTag(), "onServiceDisconnected");
            mService = null;
            if (!userDestroy)
            {
                bindService();
            }
        }

        _cls1()
        {
            this$0 = SpeechModuleAidl.this;
            super();
        }
    }

}
