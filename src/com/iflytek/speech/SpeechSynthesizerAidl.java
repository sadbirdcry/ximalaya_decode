// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.speech.aidl.ISpeechSynthesizer;

// Referenced classes of package com.iflytek.speech:
//            SpeechModuleAidl, SynthesizerListener

public class SpeechSynthesizerAidl extends SpeechModuleAidl
{

    public SpeechSynthesizerAidl(Context context, InitListener initlistener)
    {
        super(context, initlistener, "com.iflytek.component.speechsynthesizer");
    }

    public boolean destory()
    {
        mService = null;
        return super.destory();
    }

    public volatile Intent getIntent()
    {
        return super.getIntent();
    }

    public String getParameter(String s)
    {
        Object obj;
        obj = null;
        if (!s.equals("local_speakers"))
        {
            break MISSING_BLOCK_LABEL_73;
        }
        if (SpeechUtility.getUtility() == null)
        {
            return null;
        }
        int i;
        try
        {
            i = SpeechUtility.getUtility().getServiceVersion();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return "20999";
        }
        s = obj;
        if (i < 44)
        {
            break MISSING_BLOCK_LABEL_79;
        }
        if (i >= 10000)
        {
            s = obj;
            if (i < 10013)
            {
                break MISSING_BLOCK_LABEL_79;
            }
        }
        s = ((ISpeechSynthesizer)mService).getLocalSpeakerList();
        return s;
        s = super.getParameter(s);
        return s;
    }

    public volatile boolean isActionInstalled(Context context, String s)
    {
        return super.isActionInstalled(context, s);
    }

    public volatile boolean isAvailable()
    {
        return super.isAvailable();
    }

    public boolean isSpeaking()
    {
        boolean flag;
        if (mService == null)
        {
            break MISSING_BLOCK_LABEL_27;
        }
        flag = ((ISpeechSynthesizer)mService).isSpeaking();
        return flag;
        RemoteException remoteexception;
        remoteexception;
        remoteexception.printStackTrace();
        return false;
    }

    public int pauseSpeaking(SynthesizerListener synthesizerlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (synthesizerlistener == null)
        {
            return 20012;
        }
        int i;
        try
        {
            i = ((ISpeechSynthesizer)mService).pauseSpeaking(synthesizerlistener);
        }
        // Misplaced declaration of an exception variable
        catch (SynthesizerListener synthesizerlistener)
        {
            synthesizerlistener.printStackTrace();
            return 21004;
        }
        return i;
    }

    public int resumeSpeaking(SynthesizerListener synthesizerlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (synthesizerlistener == null)
        {
            return 20012;
        }
        int i;
        try
        {
            i = ((ISpeechSynthesizer)mService).resumeSpeaking(synthesizerlistener);
        }
        // Misplaced declaration of an exception variable
        catch (SynthesizerListener synthesizerlistener)
        {
            synthesizerlistener.printStackTrace();
            return 21004;
        }
        return i;
    }

    public int setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public int startSpeaking(String s, SynthesizerListener synthesizerlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (synthesizerlistener == null)
        {
            return 20012;
        }
        int i;
        try
        {
            Intent intent = getIntent();
            intent.putExtra("text", s);
            i = ((ISpeechSynthesizer)mService).startSpeaking(intent, synthesizerlistener);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return 21004;
        }
        return i;
    }

    public int stopSpeaking(SynthesizerListener synthesizerlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (synthesizerlistener == null)
        {
            return 20012;
        }
        int i;
        try
        {
            i = ((ISpeechSynthesizer)mService).stopSpeaking(synthesizerlistener);
        }
        // Misplaced declaration of an exception variable
        catch (SynthesizerListener synthesizerlistener)
        {
            synthesizerlistener.printStackTrace();
            return 21004;
        }
        return i;
    }

    public int synthesizeToUrl(String s, SynthesizerListener synthesizerlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (synthesizerlistener == null)
        {
            return 20012;
        }
        int i;
        try
        {
            Intent intent = getIntent();
            intent.putExtra("text", s);
            i = ((ISpeechSynthesizer)mService).synthesizeToUrl(intent, synthesizerlistener);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return 21004;
        }
        return i;
    }
}
