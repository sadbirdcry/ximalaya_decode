// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Iterator;

public final class SpeechComponent
{

    private ArrayList mEngines;
    private String mPackageName;

    public SpeechComponent(String s)
    {
        mPackageName = "";
        mEngines = new ArrayList();
        if (s != null)
        {
            mPackageName = s;
        }
    }

    public void addEngine(String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            mEngines.add(s);
        }
    }

    public ArrayList getEngines()
    {
        return mEngines;
    }

    public String getPackageName()
    {
        return mPackageName;
    }

    public boolean isEngineAvaible(String s)
    {
        Iterator iterator;
        if (TextUtils.isEmpty(s))
        {
            return false;
        }
        if (mEngines.size() == 0)
        {
            return false;
        }
        iterator = mEngines.iterator();
_L2:
        String s1;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        s1 = (String)iterator.next();
        boolean flag = s1.contains(s);
        if (flag)
        {
            return true;
        }
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        exception.printStackTrace();
        if (true) goto _L2; else goto _L1
_L1:
        return false;
    }
}
