// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.util.Log;
import com.iflytek.cloud.InitListener;
import com.iflytek.speech.aidl.ISpeakerVerifier;
import java.util.ArrayList;

// Referenced classes of package com.iflytek.speech:
//            SpeechModuleAidl, VerifierListener

public class SpeakerVerifierAidl extends SpeechModuleAidl
{
    public static interface DownloadListener
    {

        public abstract void onData(ArrayList arraylist);

        public abstract void onError(int i);
    }

    public class PassWord
    {

        public String pwdt;
        public String pwid;
        public String pwtext;
        final SpeakerVerifierAidl this$0;

        public PassWord()
        {
            this$0 = SpeakerVerifierAidl.this;
            super();
            pwdt = "";
            pwid = "";
            pwtext = "";
        }
    }


    public static String IVP_PARAM_CONSISTTHRESHOLD = "consistThreshold";
    public static String IVP_PARAM_DTW_CHECK_THRESHOLD = "checkThreshold";
    public static String IVP_SENTENCE_CNT = "count";
    public static String USER_NAME = "name";
    public static String VID = "vid";
    private final String TAG = "SpeakerVerifier";

    public SpeakerVerifierAidl(Context context, InitListener initlistener)
    {
        super(context, initlistener, "com.iflytek.component.speakerverifier");
    }

    public volatile boolean destory()
    {
        return super.destory();
    }

    public void endSpeak()
    {
        try
        {
            Log.d("SpeakerVerifier", "SpeakerVerifier | endSpeak");
            ((ISpeakerVerifier)mService).endSpeak();
            return;
        }
        catch (RemoteException remoteexception)
        {
            remoteexception.printStackTrace();
        }
    }

    public volatile Intent getIntent()
    {
        return super.getIntent();
    }

    public String getParameter(String s)
    {
        return null;
    }

    public int getPasswordList(Context context, DownloadListener downloadlistener, String s)
    {
        return 0;
    }

    public int identify(String s, String s1, VerifierListener verifierlistener)
    {
        return 0;
    }

    public volatile boolean isActionInstalled(Context context, String s)
    {
        return super.isActionInstalled(context, s);
    }

    public volatile boolean isAvailable()
    {
        return super.isAvailable();
    }

    public int register(String s, String s1, VerifierListener verifierlistener)
    {
        Log.d("SpeakerVerifier", "SpeakerVerifier | register");
        if (mService == null)
        {
            return 21003;
        }
        if (verifierlistener == null)
        {
            return 20012;
        }
        try
        {
            ((ISpeakerVerifier)mService).register(s, s1, verifierlistener);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public int setParameter(String s, String s1)
    {
        return 0;
    }

    public void stopSpeak()
    {
        try
        {
            Log.d("SpeakerVerifier", "SpeakerVerifier | stopSpeak");
            ((ISpeakerVerifier)mService).stopSpeak();
            return;
        }
        catch (RemoteException remoteexception)
        {
            remoteexception.printStackTrace();
        }
    }

    public int verify(String s, String s1, VerifierListener verifierlistener)
    {
        Log.d("SpeakerVerifier", "SpeakerVerifier | verify");
        if (mService == null)
        {
            return 21003;
        }
        if (verifierlistener == null)
        {
            return 20012;
        }
        try
        {
            ((ISpeakerVerifier)mService).verify(s, s1, verifierlistener);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return 21004;
        }
        return 0;
    }

}
