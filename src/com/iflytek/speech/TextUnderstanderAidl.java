// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.iflytek.cloud.InitListener;
import com.iflytek.speech.aidl.ITextUnderstander;

// Referenced classes of package com.iflytek.speech:
//            SpeechModuleAidl, TextUnderstanderListener

public class TextUnderstanderAidl extends SpeechModuleAidl
{

    public static final String SCENE = "scene";
    private static final String TEXT = "text";

    public TextUnderstanderAidl(Context context, InitListener initlistener)
    {
        super(context, initlistener, "com.iflytek.component.textunderstander");
    }

    public int cancel(TextUnderstanderListener textunderstanderlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (textunderstanderlistener == null)
        {
            return 20012;
        }
        try
        {
            ((ITextUnderstander)mService).cancel(textunderstanderlistener);
        }
        // Misplaced declaration of an exception variable
        catch (TextUnderstanderListener textunderstanderlistener)
        {
            textunderstanderlistener.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public volatile boolean destory()
    {
        return super.destory();
    }

    public volatile Intent getIntent()
    {
        return super.getIntent();
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public volatile boolean isActionInstalled(Context context, String s)
    {
        return super.isActionInstalled(context, s);
    }

    public volatile boolean isAvailable()
    {
        return super.isAvailable();
    }

    public boolean isUnderstanding()
    {
        boolean flag;
        if (mService == null)
        {
            break MISSING_BLOCK_LABEL_27;
        }
        flag = ((ITextUnderstander)mService).isUnderstanding();
        return flag;
        RemoteException remoteexception;
        remoteexception;
        remoteexception.printStackTrace();
        return false;
    }

    public int setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public int understandText(String s, TextUnderstanderListener textunderstanderlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (textunderstanderlistener == null)
        {
            return 20012;
        }
        try
        {
            Intent intent = getIntent();
            intent.putExtra("text", s);
            ((ITextUnderstander)mService).understandText(intent, textunderstanderlistener);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return 21004;
        }
        return 0;
    }
}
