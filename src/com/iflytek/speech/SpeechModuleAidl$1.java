// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

// Referenced classes of package com.iflytek.speech:
//            SpeechModuleAidl

final class this._cls0
    implements ServiceConnection
{

    final SpeechModuleAidl this$0;

    public void onServiceConnected(ComponentName componentname, IBinder ibinder)
    {
        synchronized (mSynLock)
        {
            Log.d(getTag(), "init success");
            mService = SpeechModuleAidl.access$000(SpeechModuleAidl.this, ibinder);
            Log.d(getTag(), (new StringBuilder()).append("mService :").append(mService).toString());
            if (SpeechModuleAidl.access$100(SpeechModuleAidl.this) != null)
            {
                Message.obtain(SpeechModuleAidl.access$200(SpeechModuleAidl.this), 0, 0, 0, null).sendToTarget();
            }
        }
        return;
        ibinder;
        componentname;
        JVM INSTR monitorexit ;
        throw ibinder;
    }

    public void onServiceDisconnected(ComponentName componentname)
    {
        Log.d(getTag(), "onServiceDisconnected");
        mService = null;
        if (!SpeechModuleAidl.access$300(SpeechModuleAidl.this))
        {
            SpeechModuleAidl.access$400(SpeechModuleAidl.this);
        }
    }

    ()
    {
        this$0 = SpeechModuleAidl.this;
        super();
    }
}
