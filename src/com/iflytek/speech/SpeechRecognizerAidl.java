// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.speech.aidl.ISpeechRecognizer;

// Referenced classes of package com.iflytek.speech:
//            SpeechModuleAidl, GrammarListener, RecognizerListener, LexiconListener

public class SpeechRecognizerAidl extends SpeechModuleAidl
{

    public SpeechRecognizerAidl(Context context, InitListener initlistener)
    {
        super(context, initlistener, "com.iflytek.component.speechrecognizer");
    }

    public int buildGrammar(String s, String s1, GrammarListener grammarlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (grammarlistener == null)
        {
            return 20012;
        }
        try
        {
            Intent intent = getIntent();
            intent.putExtra("grammar_type", s);
            intent.putExtra("grammar_content", s1);
            ((ISpeechRecognizer)mService).buildGrammar(intent, grammarlistener);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public int cancel(RecognizerListener recognizerlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (recognizerlistener == null)
        {
            return 20012;
        }
        try
        {
            ((ISpeechRecognizer)mService).cancel(recognizerlistener);
        }
        // Misplaced declaration of an exception variable
        catch (RecognizerListener recognizerlistener)
        {
            recognizerlistener.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public boolean destory()
    {
        mService = null;
        return super.destory();
    }

    public volatile Intent getIntent()
    {
        return super.getIntent();
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public volatile boolean isActionInstalled(Context context, String s)
    {
        return super.isActionInstalled(context, s);
    }

    public volatile boolean isAvailable()
    {
        return super.isAvailable();
    }

    public boolean isListening()
    {
        boolean flag;
        if (mService == null)
        {
            break MISSING_BLOCK_LABEL_27;
        }
        flag = ((ISpeechRecognizer)mService).isListening();
        return flag;
        RemoteException remoteexception;
        remoteexception;
        remoteexception.printStackTrace();
        return false;
    }

    public int setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public int startListening(RecognizerListener recognizerlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (recognizerlistener == null)
        {
            return 20012;
        }
        try
        {
            ((ISpeechRecognizer)mService).startListening(getIntent(), recognizerlistener);
        }
        // Misplaced declaration of an exception variable
        catch (RecognizerListener recognizerlistener)
        {
            recognizerlistener.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public int stopListening(RecognizerListener recognizerlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (recognizerlistener == null)
        {
            return 20012;
        }
        try
        {
            ((ISpeechRecognizer)mService).stopListening(recognizerlistener);
        }
        // Misplaced declaration of an exception variable
        catch (RecognizerListener recognizerlistener)
        {
            recognizerlistener.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public int updateLexicon(String s, String s1, LexiconListener lexiconlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (lexiconlistener == null)
        {
            return 20012;
        }
        try
        {
            Intent intent = getIntent();
            intent.putExtra("lexicon_name", s);
            intent.putExtra("lexicon_content", s1);
            ((ISpeechRecognizer)mService).updateLexicon(intent, lexiconlistener);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public int writeAudio(byte abyte0[], int i, int j)
    {
        if (SpeechUtility.getUtility().getServiceVersion() < 44)
        {
            return 20018;
        }
        if (mService == null)
        {
            return 21003;
        }
        try
        {
            Intent intent = getIntent();
            ((ISpeechRecognizer)mService).writeAudio(intent, abyte0, i, j);
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            abyte0.printStackTrace();
            return 0;
        }
        return 0;
    }
}
