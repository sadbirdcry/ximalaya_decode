// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.speech.aidl.ISpeechUnderstander;

// Referenced classes of package com.iflytek.speech:
//            SpeechModuleAidl, SpeechUnderstanderListener

public class SpeechUnderstanderAidl extends SpeechModuleAidl
{

    public SpeechUnderstanderAidl(Context context, InitListener initlistener)
    {
        super(context, initlistener, "com.iflytek.component.speechunderstander");
    }

    public int cancel(SpeechUnderstanderListener speechunderstanderlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (speechunderstanderlistener == null)
        {
            return 20012;
        }
        try
        {
            ((ISpeechUnderstander)mService).cancel(speechunderstanderlistener);
        }
        // Misplaced declaration of an exception variable
        catch (SpeechUnderstanderListener speechunderstanderlistener)
        {
            speechunderstanderlistener.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public volatile boolean destory()
    {
        return super.destory();
    }

    public volatile Intent getIntent()
    {
        return super.getIntent();
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public volatile boolean isActionInstalled(Context context, String s)
    {
        return super.isActionInstalled(context, s);
    }

    public volatile boolean isAvailable()
    {
        return super.isAvailable();
    }

    public boolean isUnderstanding()
    {
        boolean flag;
        if (mService == null)
        {
            break MISSING_BLOCK_LABEL_27;
        }
        flag = ((ISpeechUnderstander)mService).isUnderstanding();
        return flag;
        RemoteException remoteexception;
        remoteexception;
        remoteexception.printStackTrace();
        return false;
    }

    public int setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public int startUnderstanding(SpeechUnderstanderListener speechunderstanderlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (speechunderstanderlistener == null)
        {
            return 20012;
        }
        try
        {
            ((ISpeechUnderstander)mService).startUnderstanding(getIntent(), speechunderstanderlistener);
        }
        // Misplaced declaration of an exception variable
        catch (SpeechUnderstanderListener speechunderstanderlistener)
        {
            speechunderstanderlistener.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public int stopUnderstanding(SpeechUnderstanderListener speechunderstanderlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (speechunderstanderlistener == null)
        {
            return 20012;
        }
        try
        {
            ((ISpeechUnderstander)mService).stopUnderstanding(speechunderstanderlistener);
        }
        // Misplaced declaration of an exception variable
        catch (SpeechUnderstanderListener speechunderstanderlistener)
        {
            speechunderstanderlistener.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public int writeAudio(byte abyte0[], int i, int j)
    {
        if (SpeechUtility.getUtility().getServiceVersion() < 44)
        {
            return 20018;
        }
        if (mService == null)
        {
            return 21003;
        }
        try
        {
            Intent intent = getIntent();
            ((ISpeechUnderstander)mService).writeAudio(intent, abyte0, i, j);
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            abyte0.printStackTrace();
            return 0;
        }
        return 0;
    }
}
