// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface LexiconListener
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements LexiconListener
    {

        private static final String DESCRIPTOR = "com.iflytek.speech.LexiconListener";
        static final int TRANSACTION_onLexiconUpdated = 1;

        public static LexiconListener asInterface(IBinder ibinder)
        {
            if (ibinder == null)
            {
                return null;
            }
            IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.LexiconListener");
            if (iinterface != null && (iinterface instanceof LexiconListener))
            {
                return (LexiconListener)iinterface;
            } else
            {
                return new Proxy(ibinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            switch (i)
            {
            default:
                return super.onTransact(i, parcel, parcel1, j);

            case 1598968902: 
                parcel1.writeString("com.iflytek.speech.LexiconListener");
                return true;

            case 1: // '\001'
                parcel.enforceInterface("com.iflytek.speech.LexiconListener");
                onLexiconUpdated(parcel.readString(), parcel.readInt());
                return true;
            }
        }

        public Stub()
        {
            attachInterface(this, "com.iflytek.speech.LexiconListener");
        }
    }

    private static class Stub.Proxy
        implements LexiconListener
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.LexiconListener";
        }

        public void onLexiconUpdated(String s, int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.LexiconListener");
            parcel.writeString(s);
            parcel.writeInt(i);
            mRemote.transact(1, parcel, null, 1);
            parcel.recycle();
            return;
            s;
            parcel.recycle();
            throw s;
        }

        Stub.Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    public abstract void onLexiconUpdated(String s, int i)
        throws RemoteException;
}
