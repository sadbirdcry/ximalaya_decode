// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.TextUnderstanderListener;

public interface ITextUnderstander
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements ITextUnderstander
    {

        private static final String DESCRIPTOR = "com.iflytek.speech.aidl.ITextUnderstander";
        static final int TRANSACTION_cancel = 2;
        static final int TRANSACTION_isUnderstanding = 3;
        static final int TRANSACTION_understandText = 1;

        public static ITextUnderstander asInterface(IBinder ibinder)
        {
            if (ibinder == null)
            {
                return null;
            }
            IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.aidl.ITextUnderstander");
            if (iinterface != null && (iinterface instanceof ITextUnderstander))
            {
                return (ITextUnderstander)iinterface;
            } else
            {
                return new Proxy(ibinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            boolean flag;
            switch (i)
            {
            default:
                return super.onTransact(i, parcel, parcel1, j);

            case 1598968902: 
                parcel1.writeString("com.iflytek.speech.aidl.ITextUnderstander");
                return true;

            case 1: // '\001'
                parcel.enforceInterface("com.iflytek.speech.aidl.ITextUnderstander");
                Intent intent;
                if (parcel.readInt() != 0)
                {
                    intent = (Intent)Intent.CREATOR.createFromParcel(parcel);
                } else
                {
                    intent = null;
                }
                understandText(intent, com.iflytek.speech.TextUnderstanderListener.Stub.asInterface(parcel.readStrongBinder()));
                parcel1.writeNoException();
                return true;

            case 2: // '\002'
                parcel.enforceInterface("com.iflytek.speech.aidl.ITextUnderstander");
                cancel(com.iflytek.speech.TextUnderstanderListener.Stub.asInterface(parcel.readStrongBinder()));
                parcel1.writeNoException();
                return true;

            case 3: // '\003'
                parcel.enforceInterface("com.iflytek.speech.aidl.ITextUnderstander");
                flag = isUnderstanding();
                parcel1.writeNoException();
                break;
            }
            if (flag)
            {
                i = 1;
            } else
            {
                i = 0;
            }
            parcel1.writeInt(i);
            return true;
        }

        public Stub()
        {
            attachInterface(this, "com.iflytek.speech.aidl.ITextUnderstander");
        }
    }

    private static class Stub.Proxy
        implements ITextUnderstander
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public void cancel(TextUnderstanderListener textunderstanderlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ITextUnderstander");
            if (textunderstanderlistener == null)
            {
                break MISSING_BLOCK_LABEL_57;
            }
            textunderstanderlistener = textunderstanderlistener.asBinder();
_L1:
            parcel.writeStrongBinder(textunderstanderlistener);
            mRemote.transact(2, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            textunderstanderlistener = null;
              goto _L1
            textunderstanderlistener;
            parcel1.recycle();
            parcel.recycle();
            throw textunderstanderlistener;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.aidl.ITextUnderstander";
        }

        public boolean isUnderstanding()
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            boolean flag;
            flag = false;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            int i;
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ITextUnderstander");
            mRemote.transact(3, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            if (i != 0)
            {
                flag = true;
            }
            parcel1.recycle();
            parcel.recycle();
            return flag;
            Exception exception;
            exception;
            parcel1.recycle();
            parcel.recycle();
            throw exception;
        }

        public void understandText(Intent intent, TextUnderstanderListener textunderstanderlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ITextUnderstander");
            if (intent == null) goto _L2; else goto _L1
_L1:
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L3:
            if (textunderstanderlistener == null)
            {
                break MISSING_BLOCK_LABEL_96;
            }
            intent = textunderstanderlistener.asBinder();
_L4:
            parcel.writeStrongBinder(intent);
            mRemote.transact(1, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
_L2:
            parcel.writeInt(0);
              goto _L3
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
            intent = null;
              goto _L4
        }

        Stub.Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    public abstract void cancel(TextUnderstanderListener textunderstanderlistener)
        throws RemoteException;

    public abstract boolean isUnderstanding()
        throws RemoteException;

    public abstract void understandText(Intent intent, TextUnderstanderListener textunderstanderlistener)
        throws RemoteException;
}
