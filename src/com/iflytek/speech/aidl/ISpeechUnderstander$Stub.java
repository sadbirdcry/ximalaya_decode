// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.SpeechUnderstanderListener;

// Referenced classes of package com.iflytek.speech.aidl:
//            ISpeechUnderstander

public static abstract class attachInterface extends Binder
    implements ISpeechUnderstander
{
    private static class Proxy
        implements ISpeechUnderstander
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public void cancel(SpeechUnderstanderListener speechunderstanderlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
            if (speechunderstanderlistener == null)
            {
                break MISSING_BLOCK_LABEL_57;
            }
            speechunderstanderlistener = speechunderstanderlistener.asBinder();
_L1:
            parcel.writeStrongBinder(speechunderstanderlistener);
            mRemote.transact(3, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            speechunderstanderlistener = null;
              goto _L1
            speechunderstanderlistener;
            parcel1.recycle();
            parcel.recycle();
            throw speechunderstanderlistener;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.aidl.ISpeechUnderstander";
        }

        public boolean isUnderstanding()
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            boolean flag;
            flag = false;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            int i;
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
            mRemote.transact(4, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            if (i != 0)
            {
                flag = true;
            }
            parcel1.recycle();
            parcel.recycle();
            return flag;
            Exception exception;
            exception;
            parcel1.recycle();
            parcel.recycle();
            throw exception;
        }

        public void startUnderstanding(Intent intent, SpeechUnderstanderListener speechunderstanderlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
            if (intent == null) goto _L2; else goto _L1
_L1:
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L3:
            if (speechunderstanderlistener == null)
            {
                break MISSING_BLOCK_LABEL_96;
            }
            intent = speechunderstanderlistener.asBinder();
_L4:
            parcel.writeStrongBinder(intent);
            mRemote.transact(1, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
_L2:
            parcel.writeInt(0);
              goto _L3
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
            intent = null;
              goto _L4
        }

        public void stopUnderstanding(SpeechUnderstanderListener speechunderstanderlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
            if (speechunderstanderlistener == null)
            {
                break MISSING_BLOCK_LABEL_57;
            }
            speechunderstanderlistener = speechunderstanderlistener.asBinder();
_L1:
            parcel.writeStrongBinder(speechunderstanderlistener);
            mRemote.transact(2, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            speechunderstanderlistener = null;
              goto _L1
            speechunderstanderlistener;
            parcel1.recycle();
            parcel.recycle();
            throw speechunderstanderlistener;
        }

        public void writeAudio(Intent intent, byte abyte0[], int i, int j)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
            if (intent == null)
            {
                break MISSING_BLOCK_LABEL_85;
            }
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L1:
            parcel.writeByteArray(abyte0);
            parcel.writeInt(i);
            parcel.writeInt(j);
            mRemote.transact(5, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
        }

        Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    private static final String DESCRIPTOR = "com.iflytek.speech.aidl.ISpeechUnderstander";
    static final int TRANSACTION_cancel = 3;
    static final int TRANSACTION_isUnderstanding = 4;
    static final int TRANSACTION_startUnderstanding = 1;
    static final int TRANSACTION_stopUnderstanding = 2;
    static final int TRANSACTION_writeAudio = 5;

    public static ISpeechUnderstander asInterface(IBinder ibinder)
    {
        if (ibinder == null)
        {
            return null;
        }
        android.os.IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.aidl.ISpeechUnderstander");
        if (iinterface != null && (iinterface instanceof ISpeechUnderstander))
        {
            return (ISpeechUnderstander)iinterface;
        } else
        {
            return new Proxy(ibinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
        throws RemoteException
    {
        Object obj = null;
        Intent intent = null;
        switch (i)
        {
        default:
            return super.onTransact(i, parcel, parcel1, j);

        case 1598968902: 
            parcel1.writeString("com.iflytek.speech.aidl.ISpeechUnderstander");
            return true;

        case 1: // '\001'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechUnderstander");
            if (parcel.readInt() != 0)
            {
                intent = (Intent)Intent.CREATOR.FromParcel(parcel);
            }
            startUnderstanding(intent, com.iflytek.speech.r.Stub.asInterface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 2: // '\002'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechUnderstander");
            stopUnderstanding(com.iflytek.speech.r.Stub.asInterface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 3: // '\003'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechUnderstander");
            cancel(com.iflytek.speech.r.Stub.asInterface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 4: // '\004'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechUnderstander");
            boolean flag = isUnderstanding();
            parcel1.writeNoException();
            if (flag)
            {
                i = 1;
            } else
            {
                i = 0;
            }
            parcel1.writeInt(i);
            return true;

        case 5: // '\005'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechUnderstander");
            intent = obj;
            break;
        }
        if (parcel.readInt() != 0)
        {
            intent = (Intent)Intent.CREATOR.FromParcel(parcel);
        }
        writeAudio(intent, parcel.createByteArray(), parcel.readInt(), parcel.readInt());
        parcel1.writeNoException();
        return true;
    }

    public Proxy.mRemote()
    {
        attachInterface(this, "com.iflytek.speech.aidl.ISpeechUnderstander");
    }
}
