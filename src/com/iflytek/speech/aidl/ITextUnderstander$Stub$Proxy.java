// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.TextUnderstanderListener;

// Referenced classes of package com.iflytek.speech.aidl:
//            ITextUnderstander

private static class mRemote
    implements ITextUnderstander
{

    private IBinder mRemote;

    public IBinder asBinder()
    {
        return mRemote;
    }

    public void cancel(TextUnderstanderListener textunderstanderlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ITextUnderstander");
        if (textunderstanderlistener == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        textunderstanderlistener = textunderstanderlistener.asBinder();
_L1:
        parcel.writeStrongBinder(textunderstanderlistener);
        mRemote.transact(2, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        textunderstanderlistener = null;
          goto _L1
        textunderstanderlistener;
        parcel1.recycle();
        parcel.recycle();
        throw textunderstanderlistener;
    }

    public String getInterfaceDescriptor()
    {
        return "com.iflytek.speech.aidl.ITextUnderstander";
    }

    public boolean isUnderstanding()
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        boolean flag;
        flag = false;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        int i;
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ITextUnderstander");
        mRemote.transact(3, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        if (i != 0)
        {
            flag = true;
        }
        parcel1.recycle();
        parcel.recycle();
        return flag;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }

    public void understandText(Intent intent, TextUnderstanderListener textunderstanderlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ITextUnderstander");
        if (intent == null) goto _L2; else goto _L1
_L1:
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L3:
        if (textunderstanderlistener == null)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        intent = textunderstanderlistener.asBinder();
_L4:
        parcel.writeStrongBinder(intent);
        mRemote.transact(1, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
_L2:
        parcel.writeInt(0);
          goto _L3
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
        intent = null;
          goto _L4
    }

    (IBinder ibinder)
    {
        mRemote = ibinder;
    }
}
