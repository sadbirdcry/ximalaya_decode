// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.WakeuperListener;

// Referenced classes of package com.iflytek.speech.aidl:
//            IWakeuper

private static class mRemote
    implements IWakeuper
{

    private IBinder mRemote;

    public IBinder asBinder()
    {
        return mRemote;
    }

    public void cancel(WakeuperListener wakeuperlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
        if (wakeuperlistener == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        wakeuperlistener = wakeuperlistener.asBinder();
_L1:
        parcel.writeStrongBinder(wakeuperlistener);
        mRemote.transact(3, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        wakeuperlistener = null;
          goto _L1
        wakeuperlistener;
        parcel1.recycle();
        parcel.recycle();
        throw wakeuperlistener;
    }

    public void destroy()
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
        mRemote.transact(5, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }

    public String getInterfaceDescriptor()
    {
        return "com.iflytek.speech.aidl.IWakeuper";
    }

    public boolean isListening()
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        boolean flag;
        flag = false;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        int i;
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
        mRemote.transact(4, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        if (i != 0)
        {
            flag = true;
        }
        parcel1.recycle();
        parcel.recycle();
        return flag;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }

    public void startListening(Intent intent, WakeuperListener wakeuperlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
        if (intent == null) goto _L2; else goto _L1
_L1:
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L3:
        if (wakeuperlistener == null)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        intent = wakeuperlistener.asBinder();
_L4:
        parcel.writeStrongBinder(intent);
        mRemote.transact(1, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
_L2:
        parcel.writeInt(0);
          goto _L3
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
        intent = null;
          goto _L4
    }

    public void stopListening(WakeuperListener wakeuperlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
        if (wakeuperlistener == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        wakeuperlistener = wakeuperlistener.asBinder();
_L1:
        parcel.writeStrongBinder(wakeuperlistener);
        mRemote.transact(2, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        wakeuperlistener = null;
          goto _L1
        wakeuperlistener;
        parcel1.recycle();
        parcel.recycle();
        throw wakeuperlistener;
    }

    (IBinder ibinder)
    {
        mRemote = ibinder;
    }
}
