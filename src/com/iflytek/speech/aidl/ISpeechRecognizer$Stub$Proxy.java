// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.GrammarListener;
import com.iflytek.speech.LexiconListener;
import com.iflytek.speech.RecognizerListener;

// Referenced classes of package com.iflytek.speech.aidl:
//            ISpeechRecognizer

private static class mRemote
    implements ISpeechRecognizer
{

    private IBinder mRemote;

    public IBinder asBinder()
    {
        return mRemote;
    }

    public void buildGrammar(Intent intent, GrammarListener grammarlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
        if (intent == null) goto _L2; else goto _L1
_L1:
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L3:
        if (grammarlistener == null)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        intent = grammarlistener.asBinder();
_L4:
        parcel.writeStrongBinder(intent);
        mRemote.transact(5, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
_L2:
        parcel.writeInt(0);
          goto _L3
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
        intent = null;
          goto _L4
    }

    public void cancel(RecognizerListener recognizerlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
        if (recognizerlistener == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        recognizerlistener = recognizerlistener.asBinder();
_L1:
        parcel.writeStrongBinder(recognizerlistener);
        mRemote.transact(3, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        recognizerlistener = null;
          goto _L1
        recognizerlistener;
        parcel1.recycle();
        parcel.recycle();
        throw recognizerlistener;
    }

    public String getInterfaceDescriptor()
    {
        return "com.iflytek.speech.aidl.ISpeechRecognizer";
    }

    public boolean isListening()
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        boolean flag;
        flag = false;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        int i;
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
        mRemote.transact(4, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        if (i != 0)
        {
            flag = true;
        }
        parcel1.recycle();
        parcel.recycle();
        return flag;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }

    public void startListening(Intent intent, RecognizerListener recognizerlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
        if (intent == null) goto _L2; else goto _L1
_L1:
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L3:
        if (recognizerlistener == null)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        intent = recognizerlistener.asBinder();
_L4:
        parcel.writeStrongBinder(intent);
        mRemote.transact(1, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
_L2:
        parcel.writeInt(0);
          goto _L3
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
        intent = null;
          goto _L4
    }

    public void stopListening(RecognizerListener recognizerlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
        if (recognizerlistener == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        recognizerlistener = recognizerlistener.asBinder();
_L1:
        parcel.writeStrongBinder(recognizerlistener);
        mRemote.transact(2, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        recognizerlistener = null;
          goto _L1
        recognizerlistener;
        parcel1.recycle();
        parcel.recycle();
        throw recognizerlistener;
    }

    public void updateLexicon(Intent intent, LexiconListener lexiconlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
        if (intent == null) goto _L2; else goto _L1
_L1:
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L3:
        if (lexiconlistener == null)
        {
            break MISSING_BLOCK_LABEL_97;
        }
        intent = lexiconlistener.asBinder();
_L4:
        parcel.writeStrongBinder(intent);
        mRemote.transact(6, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
_L2:
        parcel.writeInt(0);
          goto _L3
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
        intent = null;
          goto _L4
    }

    public void writeAudio(Intent intent, byte abyte0[], int i, int j)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
        if (intent == null)
        {
            break MISSING_BLOCK_LABEL_86;
        }
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L1:
        parcel.writeByteArray(abyte0);
        parcel.writeInt(i);
        parcel.writeInt(j);
        mRemote.transact(7, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        parcel.writeInt(0);
          goto _L1
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
    }

    (IBinder ibinder)
    {
        mRemote = ibinder;
    }
}
