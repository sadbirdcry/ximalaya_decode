// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.WakeuperListener;

// Referenced classes of package com.iflytek.speech.aidl:
//            IWakeuper

public static abstract class attachInterface extends Binder
    implements IWakeuper
{
    private static class Proxy
        implements IWakeuper
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public void cancel(WakeuperListener wakeuperlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
            if (wakeuperlistener == null)
            {
                break MISSING_BLOCK_LABEL_57;
            }
            wakeuperlistener = wakeuperlistener.asBinder();
_L1:
            parcel.writeStrongBinder(wakeuperlistener);
            mRemote.transact(3, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            wakeuperlistener = null;
              goto _L1
            wakeuperlistener;
            parcel1.recycle();
            parcel.recycle();
            throw wakeuperlistener;
        }

        public void destroy()
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
            mRemote.transact(5, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel1.recycle();
            parcel.recycle();
            throw exception;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.aidl.IWakeuper";
        }

        public boolean isListening()
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            boolean flag;
            flag = false;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            int i;
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
            mRemote.transact(4, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            if (i != 0)
            {
                flag = true;
            }
            parcel1.recycle();
            parcel.recycle();
            return flag;
            Exception exception;
            exception;
            parcel1.recycle();
            parcel.recycle();
            throw exception;
        }

        public void startListening(Intent intent, WakeuperListener wakeuperlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
            if (intent == null) goto _L2; else goto _L1
_L1:
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L3:
            if (wakeuperlistener == null)
            {
                break MISSING_BLOCK_LABEL_96;
            }
            intent = wakeuperlistener.asBinder();
_L4:
            parcel.writeStrongBinder(intent);
            mRemote.transact(1, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
_L2:
            parcel.writeInt(0);
              goto _L3
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
            intent = null;
              goto _L4
        }

        public void stopListening(WakeuperListener wakeuperlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.IWakeuper");
            if (wakeuperlistener == null)
            {
                break MISSING_BLOCK_LABEL_57;
            }
            wakeuperlistener = wakeuperlistener.asBinder();
_L1:
            parcel.writeStrongBinder(wakeuperlistener);
            mRemote.transact(2, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            wakeuperlistener = null;
              goto _L1
            wakeuperlistener;
            parcel1.recycle();
            parcel.recycle();
            throw wakeuperlistener;
        }

        Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    private static final String DESCRIPTOR = "com.iflytek.speech.aidl.IWakeuper";
    static final int TRANSACTION_cancel = 3;
    static final int TRANSACTION_destroy = 5;
    static final int TRANSACTION_isListening = 4;
    static final int TRANSACTION_startListening = 1;
    static final int TRANSACTION_stopListening = 2;

    public static IWakeuper asInterface(IBinder ibinder)
    {
        if (ibinder == null)
        {
            return null;
        }
        android.os.IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.aidl.IWakeuper");
        if (iinterface != null && (iinterface instanceof IWakeuper))
        {
            return (IWakeuper)iinterface;
        } else
        {
            return new Proxy(ibinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
        throws RemoteException
    {
        switch (i)
        {
        default:
            return super.onTransact(i, parcel, parcel1, j);

        case 1598968902: 
            parcel1.writeString("com.iflytek.speech.aidl.IWakeuper");
            return true;

        case 1: // '\001'
            parcel.enforceInterface("com.iflytek.speech.aidl.IWakeuper");
            Intent intent;
            if (parcel.readInt() != 0)
            {
                intent = (Intent)Intent.CREATOR.createFromParcel(parcel);
            } else
            {
                intent = null;
            }
            startListening(intent, com.iflytek.speech.r.Stub.asInterface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 2: // '\002'
            parcel.enforceInterface("com.iflytek.speech.aidl.IWakeuper");
            stopListening(com.iflytek.speech.r.Stub.asInterface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 3: // '\003'
            parcel.enforceInterface("com.iflytek.speech.aidl.IWakeuper");
            cancel(com.iflytek.speech.r.Stub.asInterface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 4: // '\004'
            parcel.enforceInterface("com.iflytek.speech.aidl.IWakeuper");
            boolean flag = isListening();
            parcel1.writeNoException();
            if (flag)
            {
                i = 1;
            } else
            {
                i = 0;
            }
            parcel1.writeInt(i);
            return true;

        case 5: // '\005'
            parcel.enforceInterface("com.iflytek.speech.aidl.IWakeuper");
            destroy();
            parcel1.writeNoException();
            return true;
        }
    }

    public Proxy.mRemote()
    {
        attachInterface(this, "com.iflytek.speech.aidl.IWakeuper");
    }
}
