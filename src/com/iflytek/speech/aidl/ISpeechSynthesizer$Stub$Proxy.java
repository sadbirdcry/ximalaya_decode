// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.SynthesizerListener;

// Referenced classes of package com.iflytek.speech.aidl:
//            ISpeechSynthesizer

private static class mRemote
    implements ISpeechSynthesizer
{

    private IBinder mRemote;

    public IBinder asBinder()
    {
        return mRemote;
    }

    public String getInterfaceDescriptor()
    {
        return "com.iflytek.speech.aidl.ISpeechSynthesizer";
    }

    public String getLocalSpeakerList()
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        String s;
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
        mRemote.transact(7, parcel, parcel1, 0);
        parcel1.readException();
        s = parcel1.readString();
        parcel1.recycle();
        parcel.recycle();
        return s;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }

    public boolean isSpeaking()
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        boolean flag;
        flag = false;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        int i;
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
        mRemote.transact(6, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        if (i != 0)
        {
            flag = true;
        }
        parcel1.recycle();
        parcel.recycle();
        return flag;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }

    public int pauseSpeaking(SynthesizerListener synthesizerlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
        if (synthesizerlistener == null)
        {
            break MISSING_BLOCK_LABEL_65;
        }
        synthesizerlistener = synthesizerlistener.asBinder();
_L1:
        int i;
        parcel.writeStrongBinder(synthesizerlistener);
        mRemote.transact(3, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        parcel1.recycle();
        parcel.recycle();
        return i;
        synthesizerlistener = null;
          goto _L1
        synthesizerlistener;
        parcel1.recycle();
        parcel.recycle();
        throw synthesizerlistener;
    }

    public int resumeSpeaking(SynthesizerListener synthesizerlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
        if (synthesizerlistener == null)
        {
            break MISSING_BLOCK_LABEL_65;
        }
        synthesizerlistener = synthesizerlistener.asBinder();
_L1:
        int i;
        parcel.writeStrongBinder(synthesizerlistener);
        mRemote.transact(4, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        parcel1.recycle();
        parcel.recycle();
        return i;
        synthesizerlistener = null;
          goto _L1
        synthesizerlistener;
        parcel1.recycle();
        parcel.recycle();
        throw synthesizerlistener;
    }

    public int startSpeaking(Intent intent, SynthesizerListener synthesizerlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
        if (intent == null) goto _L2; else goto _L1
_L1:
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L3:
        if (synthesizerlistener == null)
        {
            break MISSING_BLOCK_LABEL_105;
        }
        intent = synthesizerlistener.asBinder();
_L4:
        int i;
        parcel.writeStrongBinder(intent);
        mRemote.transact(2, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        parcel1.recycle();
        parcel.recycle();
        return i;
_L2:
        parcel.writeInt(0);
          goto _L3
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
        intent = null;
          goto _L4
    }

    public int stopSpeaking(SynthesizerListener synthesizerlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
        if (synthesizerlistener == null)
        {
            break MISSING_BLOCK_LABEL_65;
        }
        synthesizerlistener = synthesizerlistener.asBinder();
_L1:
        int i;
        parcel.writeStrongBinder(synthesizerlistener);
        mRemote.transact(5, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        parcel1.recycle();
        parcel.recycle();
        return i;
        synthesizerlistener = null;
          goto _L1
        synthesizerlistener;
        parcel1.recycle();
        parcel.recycle();
        throw synthesizerlistener;
    }

    public int synthesizeToUrl(Intent intent, SynthesizerListener synthesizerlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
        if (intent == null) goto _L2; else goto _L1
_L1:
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L3:
        if (synthesizerlistener == null)
        {
            break MISSING_BLOCK_LABEL_105;
        }
        intent = synthesizerlistener.asBinder();
_L4:
        int i;
        parcel.writeStrongBinder(intent);
        mRemote.transact(1, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        parcel1.recycle();
        parcel.recycle();
        return i;
_L2:
        parcel.writeInt(0);
          goto _L3
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
        intent = null;
          goto _L4
    }

    (IBinder ibinder)
    {
        mRemote = ibinder;
    }
}
