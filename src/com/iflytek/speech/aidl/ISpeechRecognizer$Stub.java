// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.GrammarListener;
import com.iflytek.speech.LexiconListener;
import com.iflytek.speech.RecognizerListener;

// Referenced classes of package com.iflytek.speech.aidl:
//            ISpeechRecognizer

public static abstract class attachInterface extends Binder
    implements ISpeechRecognizer
{
    private static class Proxy
        implements ISpeechRecognizer
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public void buildGrammar(Intent intent, GrammarListener grammarlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
            if (intent == null) goto _L2; else goto _L1
_L1:
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L3:
            if (grammarlistener == null)
            {
                break MISSING_BLOCK_LABEL_96;
            }
            intent = grammarlistener.asBinder();
_L4:
            parcel.writeStrongBinder(intent);
            mRemote.transact(5, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
_L2:
            parcel.writeInt(0);
              goto _L3
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
            intent = null;
              goto _L4
        }

        public void cancel(RecognizerListener recognizerlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
            if (recognizerlistener == null)
            {
                break MISSING_BLOCK_LABEL_57;
            }
            recognizerlistener = recognizerlistener.asBinder();
_L1:
            parcel.writeStrongBinder(recognizerlistener);
            mRemote.transact(3, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            recognizerlistener = null;
              goto _L1
            recognizerlistener;
            parcel1.recycle();
            parcel.recycle();
            throw recognizerlistener;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.aidl.ISpeechRecognizer";
        }

        public boolean isListening()
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            boolean flag;
            flag = false;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            int i;
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
            mRemote.transact(4, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            if (i != 0)
            {
                flag = true;
            }
            parcel1.recycle();
            parcel.recycle();
            return flag;
            Exception exception;
            exception;
            parcel1.recycle();
            parcel.recycle();
            throw exception;
        }

        public void startListening(Intent intent, RecognizerListener recognizerlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
            if (intent == null) goto _L2; else goto _L1
_L1:
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L3:
            if (recognizerlistener == null)
            {
                break MISSING_BLOCK_LABEL_96;
            }
            intent = recognizerlistener.asBinder();
_L4:
            parcel.writeStrongBinder(intent);
            mRemote.transact(1, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
_L2:
            parcel.writeInt(0);
              goto _L3
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
            intent = null;
              goto _L4
        }

        public void stopListening(RecognizerListener recognizerlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
            if (recognizerlistener == null)
            {
                break MISSING_BLOCK_LABEL_57;
            }
            recognizerlistener = recognizerlistener.asBinder();
_L1:
            parcel.writeStrongBinder(recognizerlistener);
            mRemote.transact(2, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            recognizerlistener = null;
              goto _L1
            recognizerlistener;
            parcel1.recycle();
            parcel.recycle();
            throw recognizerlistener;
        }

        public void updateLexicon(Intent intent, LexiconListener lexiconlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
            if (intent == null) goto _L2; else goto _L1
_L1:
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L3:
            if (lexiconlistener == null)
            {
                break MISSING_BLOCK_LABEL_97;
            }
            intent = lexiconlistener.asBinder();
_L4:
            parcel.writeStrongBinder(intent);
            mRemote.transact(6, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
_L2:
            parcel.writeInt(0);
              goto _L3
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
            intent = null;
              goto _L4
        }

        public void writeAudio(Intent intent, byte abyte0[], int i, int j)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechRecognizer");
            if (intent == null)
            {
                break MISSING_BLOCK_LABEL_86;
            }
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L1:
            parcel.writeByteArray(abyte0);
            parcel.writeInt(i);
            parcel.writeInt(j);
            mRemote.transact(7, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
        }

        Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    private static final String DESCRIPTOR = "com.iflytek.speech.aidl.ISpeechRecognizer";
    static final int TRANSACTION_buildGrammar = 5;
    static final int TRANSACTION_cancel = 3;
    static final int TRANSACTION_isListening = 4;
    static final int TRANSACTION_startListening = 1;
    static final int TRANSACTION_stopListening = 2;
    static final int TRANSACTION_updateLexicon = 6;
    static final int TRANSACTION_writeAudio = 7;

    public static ISpeechRecognizer asInterface(IBinder ibinder)
    {
        if (ibinder == null)
        {
            return null;
        }
        android.os.IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.aidl.ISpeechRecognizer");
        if (iinterface != null && (iinterface instanceof ISpeechRecognizer))
        {
            return (ISpeechRecognizer)iinterface;
        } else
        {
            return new Proxy(ibinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
        throws RemoteException
    {
        Object obj = null;
        Object obj1 = null;
        Object obj2 = null;
        Intent intent = null;
        switch (i)
        {
        default:
            return super.onTransact(i, parcel, parcel1, j);

        case 1598968902: 
            parcel1.writeString("com.iflytek.speech.aidl.ISpeechRecognizer");
            return true;

        case 1: // '\001'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechRecognizer");
            if (parcel.readInt() != 0)
            {
                intent = (Intent)Intent.CREATOR.teFromParcel(parcel);
            }
            startListening(intent, com.iflytek.speech..asInterface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 2: // '\002'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechRecognizer");
            stopListening(com.iflytek.speech..asInterface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 3: // '\003'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechRecognizer");
            cancel(com.iflytek.speech..asInterface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 4: // '\004'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechRecognizer");
            boolean flag = isListening();
            parcel1.writeNoException();
            if (flag)
            {
                i = 1;
            } else
            {
                i = 0;
            }
            parcel1.writeInt(i);
            return true;

        case 5: // '\005'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechRecognizer");
            intent = obj;
            if (parcel.readInt() != 0)
            {
                intent = (Intent)Intent.CREATOR.teFromParcel(parcel);
            }
            buildGrammar(intent, com.iflytek.speech.Interface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 6: // '\006'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechRecognizer");
            intent = obj1;
            if (parcel.readInt() != 0)
            {
                intent = (Intent)Intent.CREATOR.teFromParcel(parcel);
            }
            updateLexicon(intent, com.iflytek.speech.Interface(parcel.readStrongBinder()));
            parcel1.writeNoException();
            return true;

        case 7: // '\007'
            parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechRecognizer");
            intent = obj2;
            break;
        }
        if (parcel.readInt() != 0)
        {
            intent = (Intent)Intent.CREATOR.teFromParcel(parcel);
        }
        writeAudio(intent, parcel.createByteArray(), parcel.readInt(), parcel.readInt());
        parcel1.writeNoException();
        return true;
    }

    public Proxy.mRemote()
    {
        attachInterface(this, "com.iflytek.speech.aidl.ISpeechRecognizer");
    }
}
