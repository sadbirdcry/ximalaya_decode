// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.SpeechUnderstanderListener;

// Referenced classes of package com.iflytek.speech.aidl:
//            ISpeechUnderstander

private static class mRemote
    implements ISpeechUnderstander
{

    private IBinder mRemote;

    public IBinder asBinder()
    {
        return mRemote;
    }

    public void cancel(SpeechUnderstanderListener speechunderstanderlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
        if (speechunderstanderlistener == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        speechunderstanderlistener = speechunderstanderlistener.asBinder();
_L1:
        parcel.writeStrongBinder(speechunderstanderlistener);
        mRemote.transact(3, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        speechunderstanderlistener = null;
          goto _L1
        speechunderstanderlistener;
        parcel1.recycle();
        parcel.recycle();
        throw speechunderstanderlistener;
    }

    public String getInterfaceDescriptor()
    {
        return "com.iflytek.speech.aidl.ISpeechUnderstander";
    }

    public boolean isUnderstanding()
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        boolean flag;
        flag = false;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        int i;
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
        mRemote.transact(4, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        if (i != 0)
        {
            flag = true;
        }
        parcel1.recycle();
        parcel.recycle();
        return flag;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }

    public void startUnderstanding(Intent intent, SpeechUnderstanderListener speechunderstanderlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
        if (intent == null) goto _L2; else goto _L1
_L1:
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L3:
        if (speechunderstanderlistener == null)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        intent = speechunderstanderlistener.asBinder();
_L4:
        parcel.writeStrongBinder(intent);
        mRemote.transact(1, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
_L2:
        parcel.writeInt(0);
          goto _L3
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
        intent = null;
          goto _L4
    }

    public void stopUnderstanding(SpeechUnderstanderListener speechunderstanderlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
        if (speechunderstanderlistener == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        speechunderstanderlistener = speechunderstanderlistener.asBinder();
_L1:
        parcel.writeStrongBinder(speechunderstanderlistener);
        mRemote.transact(2, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        speechunderstanderlistener = null;
          goto _L1
        speechunderstanderlistener;
        parcel1.recycle();
        parcel.recycle();
        throw speechunderstanderlistener;
    }

    public void writeAudio(Intent intent, byte abyte0[], int i, int j)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechUnderstander");
        if (intent == null)
        {
            break MISSING_BLOCK_LABEL_85;
        }
        parcel.writeInt(1);
        intent.writeToParcel(parcel, 0);
_L1:
        parcel.writeByteArray(abyte0);
        parcel.writeInt(i);
        parcel.writeInt(j);
        mRemote.transact(5, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        parcel.writeInt(0);
          goto _L1
        intent;
        parcel1.recycle();
        parcel.recycle();
        throw intent;
    }

    (IBinder ibinder)
    {
        mRemote = ibinder;
    }
}
