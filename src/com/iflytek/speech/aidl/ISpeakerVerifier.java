// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.VerifierListener;

public interface ISpeakerVerifier
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements ISpeakerVerifier
    {

        private static final String DESCRIPTOR = "com.iflytek.speech.aidl.ISpeakerVerifier";
        static final int TRANSACTION_endSpeak = 3;
        static final int TRANSACTION_getParameter = 6;
        static final int TRANSACTION_register = 2;
        static final int TRANSACTION_setParameter = 5;
        static final int TRANSACTION_stopSpeak = 4;
        static final int TRANSACTION_verify = 1;

        public static ISpeakerVerifier asInterface(IBinder ibinder)
        {
            if (ibinder == null)
            {
                return null;
            }
            IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.aidl.ISpeakerVerifier");
            if (iinterface != null && (iinterface instanceof ISpeakerVerifier))
            {
                return (ISpeakerVerifier)iinterface;
            } else
            {
                return new Proxy(ibinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            switch (i)
            {
            default:
                return super.onTransact(i, parcel, parcel1, j);

            case 1598968902: 
                parcel1.writeString("com.iflytek.speech.aidl.ISpeakerVerifier");
                return true;

            case 1: // '\001'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeakerVerifier");
                i = verify(parcel.readString(), parcel.readString(), com.iflytek.speech.VerifierListener.Stub.asInterface(parcel.readStrongBinder()));
                parcel1.writeNoException();
                parcel1.writeInt(i);
                return true;

            case 2: // '\002'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeakerVerifier");
                i = register(parcel.readString(), parcel.readString(), com.iflytek.speech.VerifierListener.Stub.asInterface(parcel.readStrongBinder()));
                parcel1.writeNoException();
                parcel1.writeInt(i);
                return true;

            case 3: // '\003'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeakerVerifier");
                endSpeak();
                parcel1.writeNoException();
                return true;

            case 4: // '\004'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeakerVerifier");
                stopSpeak();
                parcel1.writeNoException();
                return true;

            case 5: // '\005'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeakerVerifier");
                i = setParameter(parcel.readString(), parcel.readString());
                parcel1.writeNoException();
                parcel1.writeInt(i);
                return true;

            case 6: // '\006'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeakerVerifier");
                parcel = getParameter(parcel.readString());
                parcel1.writeNoException();
                parcel1.writeString(parcel);
                return true;
            }
        }

        public Stub()
        {
            attachInterface(this, "com.iflytek.speech.aidl.ISpeakerVerifier");
        }
    }

    private static class Stub.Proxy
        implements ISpeakerVerifier
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public void endSpeak()
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
            mRemote.transact(3, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel1.recycle();
            parcel.recycle();
            throw exception;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.aidl.ISpeakerVerifier";
        }

        public String getParameter(String s)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
            parcel.writeString(s);
            mRemote.transact(6, parcel, parcel1, 0);
            parcel1.readException();
            s = parcel1.readString();
            parcel1.recycle();
            parcel.recycle();
            return s;
            s;
            parcel1.recycle();
            parcel.recycle();
            throw s;
        }

        public int register(String s, String s1, VerifierListener verifierlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
            parcel.writeString(s);
            parcel.writeString(s1);
            if (verifierlistener == null)
            {
                break MISSING_BLOCK_LABEL_87;
            }
            s = verifierlistener.asBinder();
_L1:
            int i;
            parcel.writeStrongBinder(s);
            mRemote.transact(2, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            parcel1.recycle();
            parcel.recycle();
            return i;
            s = null;
              goto _L1
            s;
            parcel1.recycle();
            parcel.recycle();
            throw s;
        }

        public int setParameter(String s, String s1)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            int i;
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
            parcel.writeString(s);
            parcel.writeString(s1);
            mRemote.transact(5, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            parcel1.recycle();
            parcel.recycle();
            return i;
            s;
            parcel1.recycle();
            parcel.recycle();
            throw s;
        }

        public void stopSpeak()
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
            mRemote.transact(4, parcel, parcel1, 0);
            parcel1.readException();
            parcel1.recycle();
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel1.recycle();
            parcel.recycle();
            throw exception;
        }

        public int verify(String s, String s1, VerifierListener verifierlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
            parcel.writeString(s);
            parcel.writeString(s1);
            if (verifierlistener == null)
            {
                break MISSING_BLOCK_LABEL_87;
            }
            s = verifierlistener.asBinder();
_L1:
            int i;
            parcel.writeStrongBinder(s);
            mRemote.transact(1, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            parcel1.recycle();
            parcel.recycle();
            return i;
            s = null;
              goto _L1
            s;
            parcel1.recycle();
            parcel.recycle();
            throw s;
        }

        Stub.Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    public abstract void endSpeak()
        throws RemoteException;

    public abstract String getParameter(String s)
        throws RemoteException;

    public abstract int register(String s, String s1, VerifierListener verifierlistener)
        throws RemoteException;

    public abstract int setParameter(String s, String s1)
        throws RemoteException;

    public abstract void stopSpeak()
        throws RemoteException;

    public abstract int verify(String s, String s1, VerifierListener verifierlistener)
        throws RemoteException;
}
