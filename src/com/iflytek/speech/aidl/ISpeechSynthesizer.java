// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.SynthesizerListener;

public interface ISpeechSynthesizer
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements ISpeechSynthesizer
    {

        private static final String DESCRIPTOR = "com.iflytek.speech.aidl.ISpeechSynthesizer";
        static final int TRANSACTION_getLocalSpeakerList = 7;
        static final int TRANSACTION_isSpeaking = 6;
        static final int TRANSACTION_pauseSpeaking = 3;
        static final int TRANSACTION_resumeSpeaking = 4;
        static final int TRANSACTION_startSpeaking = 2;
        static final int TRANSACTION_stopSpeaking = 5;
        static final int TRANSACTION_synthesizeToUrl = 1;

        public static ISpeechSynthesizer asInterface(IBinder ibinder)
        {
            if (ibinder == null)
            {
                return null;
            }
            IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.aidl.ISpeechSynthesizer");
            if (iinterface != null && (iinterface instanceof ISpeechSynthesizer))
            {
                return (ISpeechSynthesizer)iinterface;
            } else
            {
                return new Proxy(ibinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            Object obj = null;
            Intent intent = null;
            switch (i)
            {
            default:
                return super.onTransact(i, parcel, parcel1, j);

            case 1598968902: 
                parcel1.writeString("com.iflytek.speech.aidl.ISpeechSynthesizer");
                return true;

            case 1: // '\001'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechSynthesizer");
                if (parcel.readInt() != 0)
                {
                    intent = (Intent)Intent.CREATOR.createFromParcel(parcel);
                }
                i = synthesizeToUrl(intent, com.iflytek.speech.SynthesizerListener.Stub.asInterface(parcel.readStrongBinder()));
                parcel1.writeNoException();
                parcel1.writeInt(i);
                return true;

            case 2: // '\002'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechSynthesizer");
                Intent intent1 = obj;
                if (parcel.readInt() != 0)
                {
                    intent1 = (Intent)Intent.CREATOR.createFromParcel(parcel);
                }
                i = startSpeaking(intent1, com.iflytek.speech.SynthesizerListener.Stub.asInterface(parcel.readStrongBinder()));
                parcel1.writeNoException();
                parcel1.writeInt(i);
                return true;

            case 3: // '\003'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechSynthesizer");
                i = pauseSpeaking(com.iflytek.speech.SynthesizerListener.Stub.asInterface(parcel.readStrongBinder()));
                parcel1.writeNoException();
                parcel1.writeInt(i);
                return true;

            case 4: // '\004'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechSynthesizer");
                i = resumeSpeaking(com.iflytek.speech.SynthesizerListener.Stub.asInterface(parcel.readStrongBinder()));
                parcel1.writeNoException();
                parcel1.writeInt(i);
                return true;

            case 5: // '\005'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechSynthesizer");
                i = stopSpeaking(com.iflytek.speech.SynthesizerListener.Stub.asInterface(parcel.readStrongBinder()));
                parcel1.writeNoException();
                parcel1.writeInt(i);
                return true;

            case 6: // '\006'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechSynthesizer");
                boolean flag = isSpeaking();
                parcel1.writeNoException();
                if (flag)
                {
                    i = 1;
                } else
                {
                    i = 0;
                }
                parcel1.writeInt(i);
                return true;

            case 7: // '\007'
                parcel.enforceInterface("com.iflytek.speech.aidl.ISpeechSynthesizer");
                parcel = getLocalSpeakerList();
                parcel1.writeNoException();
                parcel1.writeString(parcel);
                return true;
            }
        }

        public Stub()
        {
            attachInterface(this, "com.iflytek.speech.aidl.ISpeechSynthesizer");
        }
    }

    private static class Stub.Proxy
        implements ISpeechSynthesizer
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.aidl.ISpeechSynthesizer";
        }

        public String getLocalSpeakerList()
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            String s;
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
            mRemote.transact(7, parcel, parcel1, 0);
            parcel1.readException();
            s = parcel1.readString();
            parcel1.recycle();
            parcel.recycle();
            return s;
            Exception exception;
            exception;
            parcel1.recycle();
            parcel.recycle();
            throw exception;
        }

        public boolean isSpeaking()
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            boolean flag;
            flag = false;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            int i;
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
            mRemote.transact(6, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            if (i != 0)
            {
                flag = true;
            }
            parcel1.recycle();
            parcel.recycle();
            return flag;
            Exception exception;
            exception;
            parcel1.recycle();
            parcel.recycle();
            throw exception;
        }

        public int pauseSpeaking(SynthesizerListener synthesizerlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
            if (synthesizerlistener == null)
            {
                break MISSING_BLOCK_LABEL_65;
            }
            synthesizerlistener = synthesizerlistener.asBinder();
_L1:
            int i;
            parcel.writeStrongBinder(synthesizerlistener);
            mRemote.transact(3, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            parcel1.recycle();
            parcel.recycle();
            return i;
            synthesizerlistener = null;
              goto _L1
            synthesizerlistener;
            parcel1.recycle();
            parcel.recycle();
            throw synthesizerlistener;
        }

        public int resumeSpeaking(SynthesizerListener synthesizerlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
            if (synthesizerlistener == null)
            {
                break MISSING_BLOCK_LABEL_65;
            }
            synthesizerlistener = synthesizerlistener.asBinder();
_L1:
            int i;
            parcel.writeStrongBinder(synthesizerlistener);
            mRemote.transact(4, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            parcel1.recycle();
            parcel.recycle();
            return i;
            synthesizerlistener = null;
              goto _L1
            synthesizerlistener;
            parcel1.recycle();
            parcel.recycle();
            throw synthesizerlistener;
        }

        public int startSpeaking(Intent intent, SynthesizerListener synthesizerlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
            if (intent == null) goto _L2; else goto _L1
_L1:
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L3:
            if (synthesizerlistener == null)
            {
                break MISSING_BLOCK_LABEL_105;
            }
            intent = synthesizerlistener.asBinder();
_L4:
            int i;
            parcel.writeStrongBinder(intent);
            mRemote.transact(2, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            parcel1.recycle();
            parcel.recycle();
            return i;
_L2:
            parcel.writeInt(0);
              goto _L3
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
            intent = null;
              goto _L4
        }

        public int stopSpeaking(SynthesizerListener synthesizerlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
            if (synthesizerlistener == null)
            {
                break MISSING_BLOCK_LABEL_65;
            }
            synthesizerlistener = synthesizerlistener.asBinder();
_L1:
            int i;
            parcel.writeStrongBinder(synthesizerlistener);
            mRemote.transact(5, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            parcel1.recycle();
            parcel.recycle();
            return i;
            synthesizerlistener = null;
              goto _L1
            synthesizerlistener;
            parcel1.recycle();
            parcel.recycle();
            throw synthesizerlistener;
        }

        public int synthesizeToUrl(Intent intent, SynthesizerListener synthesizerlistener)
            throws RemoteException
        {
            Parcel parcel;
            Parcel parcel1;
            parcel = Parcel.obtain();
            parcel1 = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeechSynthesizer");
            if (intent == null) goto _L2; else goto _L1
_L1:
            parcel.writeInt(1);
            intent.writeToParcel(parcel, 0);
_L3:
            if (synthesizerlistener == null)
            {
                break MISSING_BLOCK_LABEL_105;
            }
            intent = synthesizerlistener.asBinder();
_L4:
            int i;
            parcel.writeStrongBinder(intent);
            mRemote.transact(1, parcel, parcel1, 0);
            parcel1.readException();
            i = parcel1.readInt();
            parcel1.recycle();
            parcel.recycle();
            return i;
_L2:
            parcel.writeInt(0);
              goto _L3
            intent;
            parcel1.recycle();
            parcel.recycle();
            throw intent;
            intent = null;
              goto _L4
        }

        Stub.Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    public abstract String getLocalSpeakerList()
        throws RemoteException;

    public abstract boolean isSpeaking()
        throws RemoteException;

    public abstract int pauseSpeaking(SynthesizerListener synthesizerlistener)
        throws RemoteException;

    public abstract int resumeSpeaking(SynthesizerListener synthesizerlistener)
        throws RemoteException;

    public abstract int startSpeaking(Intent intent, SynthesizerListener synthesizerlistener)
        throws RemoteException;

    public abstract int stopSpeaking(SynthesizerListener synthesizerlistener)
        throws RemoteException;

    public abstract int synthesizeToUrl(Intent intent, SynthesizerListener synthesizerlistener)
        throws RemoteException;
}
