// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech.aidl;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import com.iflytek.speech.VerifierListener;

// Referenced classes of package com.iflytek.speech.aidl:
//            ISpeakerVerifier

private static class mRemote
    implements ISpeakerVerifier
{

    private IBinder mRemote;

    public IBinder asBinder()
    {
        return mRemote;
    }

    public void endSpeak()
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
        mRemote.transact(3, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }

    public String getInterfaceDescriptor()
    {
        return "com.iflytek.speech.aidl.ISpeakerVerifier";
    }

    public String getParameter(String s)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
        parcel.writeString(s);
        mRemote.transact(6, parcel, parcel1, 0);
        parcel1.readException();
        s = parcel1.readString();
        parcel1.recycle();
        parcel.recycle();
        return s;
        s;
        parcel1.recycle();
        parcel.recycle();
        throw s;
    }

    public int register(String s, String s1, VerifierListener verifierlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
        parcel.writeString(s);
        parcel.writeString(s1);
        if (verifierlistener == null)
        {
            break MISSING_BLOCK_LABEL_87;
        }
        s = verifierlistener.asBinder();
_L1:
        int i;
        parcel.writeStrongBinder(s);
        mRemote.transact(2, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        parcel1.recycle();
        parcel.recycle();
        return i;
        s = null;
          goto _L1
        s;
        parcel1.recycle();
        parcel.recycle();
        throw s;
    }

    public int setParameter(String s, String s1)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        int i;
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
        parcel.writeString(s);
        parcel.writeString(s1);
        mRemote.transact(5, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        parcel1.recycle();
        parcel.recycle();
        return i;
        s;
        parcel1.recycle();
        parcel.recycle();
        throw s;
    }

    public void stopSpeak()
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
        mRemote.transact(4, parcel, parcel1, 0);
        parcel1.readException();
        parcel1.recycle();
        parcel.recycle();
        return;
        Exception exception;
        exception;
        parcel1.recycle();
        parcel.recycle();
        throw exception;
    }

    public int verify(String s, String s1, VerifierListener verifierlistener)
        throws RemoteException
    {
        Parcel parcel;
        Parcel parcel1;
        parcel = Parcel.obtain();
        parcel1 = Parcel.obtain();
        parcel.writeInterfaceToken("com.iflytek.speech.aidl.ISpeakerVerifier");
        parcel.writeString(s);
        parcel.writeString(s1);
        if (verifierlistener == null)
        {
            break MISSING_BLOCK_LABEL_87;
        }
        s = verifierlistener.asBinder();
_L1:
        int i;
        parcel.writeStrongBinder(s);
        mRemote.transact(1, parcel, parcel1, 0);
        parcel1.readException();
        i = parcel1.readInt();
        parcel1.recycle();
        parcel.recycle();
        return i;
        s = null;
          goto _L1
        s;
        parcel1.recycle();
        parcel.recycle();
        throw s;
    }

    _cls9(IBinder ibinder)
    {
        mRemote = ibinder;
    }
}
