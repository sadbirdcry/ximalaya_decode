// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

// Referenced classes of package com.iflytek.speech:
//            UnderstanderResult

public interface TextUnderstanderListener
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements TextUnderstanderListener
    {

        private static final String DESCRIPTOR = "com.iflytek.speech.TextUnderstanderListener";
        static final int TRANSACTION_onError = 2;
        static final int TRANSACTION_onResult = 1;

        public static TextUnderstanderListener asInterface(IBinder ibinder)
        {
            if (ibinder == null)
            {
                return null;
            }
            IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.TextUnderstanderListener");
            if (iinterface != null && (iinterface instanceof TextUnderstanderListener))
            {
                return (TextUnderstanderListener)iinterface;
            } else
            {
                return new Proxy(ibinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            switch (i)
            {
            default:
                return super.onTransact(i, parcel, parcel1, j);

            case 1598968902: 
                parcel1.writeString("com.iflytek.speech.TextUnderstanderListener");
                return true;

            case 1: // '\001'
                parcel.enforceInterface("com.iflytek.speech.TextUnderstanderListener");
                if (parcel.readInt() != 0)
                {
                    parcel = (UnderstanderResult)UnderstanderResult.CREATOR.createFromParcel(parcel);
                } else
                {
                    parcel = null;
                }
                onResult(parcel);
                return true;

            case 2: // '\002'
                parcel.enforceInterface("com.iflytek.speech.TextUnderstanderListener");
                onError(parcel.readInt());
                return true;
            }
        }

        public Stub()
        {
            attachInterface(this, "com.iflytek.speech.TextUnderstanderListener");
        }
    }

    private static class Stub.Proxy
        implements TextUnderstanderListener
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.TextUnderstanderListener";
        }

        public void onError(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.TextUnderstanderListener");
            parcel.writeInt(i);
            mRemote.transact(2, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onResult(UnderstanderResult understanderresult)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.TextUnderstanderListener");
            if (understanderresult == null)
            {
                break MISSING_BLOCK_LABEL_44;
            }
            parcel.writeInt(1);
            understanderresult.writeToParcel(parcel, 0);
_L1:
            mRemote.transact(1, parcel, null, 1);
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            understanderresult;
            parcel.recycle();
            throw understanderresult;
        }

        Stub.Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    public abstract void onError(int i)
        throws RemoteException;

    public abstract void onResult(UnderstanderResult understanderresult)
        throws RemoteException;
}
