// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import com.iflytek.cloud.InitListener;
import com.iflytek.speech.aidl.IWakeuper;

// Referenced classes of package com.iflytek.speech:
//            SpeechModuleAidl, WakeuperListener

public class VoiceWakeuperAidl extends SpeechModuleAidl
{

    public static final String ARG_RES_PROVIDER_AUTHORITY = "content_provider_name";
    public static final String ARG_RES_TYPE = "engine_res_type";
    public static final String IVP_RES_ID = "ivp_res_id";
    public static final String IVP_USER_NAME = "ivp_user_name";
    public static final String IVW_AND_IVP = "ivw_and_ivp";
    public static final String IVW_FILES = "ivw_files";
    public static final String IVW_THRESHOLD = "ivw_threshold";
    public static final String IVW_WORD_ID = "ivw_word_id";
    public static final String PARAMS_SEPARATE = ";";
    public static final String PROVIDER_NAME = "com.iflytek.speech.SharedProvider";
    public static final int RES_FROM_ASSETS = 257;
    public static final int RES_FROM_CLIENT = 259;
    public static final int RES_SPECIFIED = 258;
    public static final String WAKEUP_RESULT_ID = "wakeup_result_id";
    public static final String WAKEUP_RESULT_SCORE = "wakeup_result_Score";

    public VoiceWakeuperAidl(Context context, InitListener initlistener)
    {
        super(context, initlistener, "com.iflytek.component.speechwakeuper");
    }

    public int cancel(WakeuperListener wakeuperlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (wakeuperlistener == null)
        {
            return 20012;
        }
        try
        {
            ((IWakeuper)mService).cancel(wakeuperlistener);
        }
        // Misplaced declaration of an exception variable
        catch (WakeuperListener wakeuperlistener)
        {
            wakeuperlistener.printStackTrace();
            return 21004;
        }
        return 0;
    }

    public volatile boolean destory()
    {
        return super.destory();
    }

    public boolean destroy()
    {
        if (mService == null)
        {
            return false;
        }
        try
        {
            ((IWakeuper)mService).destroy();
        }
        catch (RemoteException remoteexception)
        {
            remoteexception.printStackTrace();
        }
        return super.destory();
    }

    public volatile Intent getIntent()
    {
        return super.getIntent();
    }

    public String getParameter(String s)
    {
        return super.getParameter(s);
    }

    public volatile boolean isActionInstalled(Context context, String s)
    {
        return super.isActionInstalled(context, s);
    }

    public volatile boolean isAvailable()
    {
        return super.isAvailable();
    }

    public boolean isListening()
    {
        boolean flag;
        try
        {
            flag = ((IWakeuper)mService).isListening();
        }
        catch (RemoteException remoteexception)
        {
            remoteexception.printStackTrace();
            return false;
        }
        return flag;
    }

    public int setParameter(String s, String s1)
    {
        return super.setParameter(s, s1);
    }

    public int startListening(WakeuperListener wakeuperlistener)
    {
        if (mService == null)
        {
            return 21003;
        }
        if (wakeuperlistener == null)
        {
            return 20012;
        }
        try
        {
            ((IWakeuper)mService).startListening(getIntent(), wakeuperlistener);
        }
        // Misplaced declaration of an exception variable
        catch (WakeuperListener wakeuperlistener)
        {
            wakeuperlistener.printStackTrace();
            return 21004;
        }
        return 0;
    }
}
