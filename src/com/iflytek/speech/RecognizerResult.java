// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Parcel;
import android.os.Parcelable;

public class RecognizerResult
    implements Parcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new _cls1();
    private String json;

    public RecognizerResult(Parcel parcel)
    {
        json = "";
        json = parcel.readString();
    }

    public RecognizerResult(String s)
    {
        json = "";
        if (s != null)
        {
            json = s;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String getResultString()
    {
        return json;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(json);
    }


    private class _cls1
        implements android.os.Parcelable.Creator
    {

        public RecognizerResult createFromParcel(Parcel parcel)
        {
            return new RecognizerResult(parcel);
        }

        public volatile Object createFromParcel(Parcel parcel)
        {
            return createFromParcel(parcel);
        }

        public RecognizerResult[] newArray(int i)
        {
            return new RecognizerResult[i];
        }

        public volatile Object[] newArray(int i)
        {
            return newArray(i);
        }

        _cls1()
        {
        }
    }

}
