// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface SynthesizerListener
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements SynthesizerListener
    {

        private static final String DESCRIPTOR = "com.iflytek.speech.SynthesizerListener";
        static final int TRANSACTION_onBufferProgress = 6;
        static final int TRANSACTION_onCompleted = 4;
        static final int TRANSACTION_onEvent = 7;
        static final int TRANSACTION_onSpeakBegin = 1;
        static final int TRANSACTION_onSpeakPaused = 2;
        static final int TRANSACTION_onSpeakProgress = 5;
        static final int TRANSACTION_onSpeakResumed = 3;

        public static SynthesizerListener asInterface(IBinder ibinder)
        {
            if (ibinder == null)
            {
                return null;
            }
            IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.SynthesizerListener");
            if (iinterface != null && (iinterface instanceof SynthesizerListener))
            {
                return (SynthesizerListener)iinterface;
            } else
            {
                return new Proxy(ibinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            int k;
            switch (i)
            {
            default:
                return super.onTransact(i, parcel, parcel1, j);

            case 1598968902: 
                parcel1.writeString("com.iflytek.speech.SynthesizerListener");
                return true;

            case 1: // '\001'
                parcel.enforceInterface("com.iflytek.speech.SynthesizerListener");
                onSpeakBegin();
                return true;

            case 2: // '\002'
                parcel.enforceInterface("com.iflytek.speech.SynthesizerListener");
                onSpeakPaused();
                return true;

            case 3: // '\003'
                parcel.enforceInterface("com.iflytek.speech.SynthesizerListener");
                onSpeakResumed();
                return true;

            case 4: // '\004'
                parcel.enforceInterface("com.iflytek.speech.SynthesizerListener");
                onCompleted(parcel.readInt());
                return true;

            case 5: // '\005'
                parcel.enforceInterface("com.iflytek.speech.SynthesizerListener");
                onSpeakProgress(parcel.readInt(), parcel.readInt(), parcel.readInt());
                return true;

            case 6: // '\006'
                parcel.enforceInterface("com.iflytek.speech.SynthesizerListener");
                onBufferProgress(parcel.readInt(), parcel.readInt(), parcel.readInt(), parcel.readString());
                return true;

            case 7: // '\007'
                parcel.enforceInterface("com.iflytek.speech.SynthesizerListener");
                i = parcel.readInt();
                j = parcel.readInt();
                k = parcel.readInt();
                break;
            }
            if (parcel.readInt() != 0)
            {
                parcel = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
            } else
            {
                parcel = null;
            }
            onEvent(i, j, k, parcel);
            return true;
        }

        public Stub()
        {
            attachInterface(this, "com.iflytek.speech.SynthesizerListener");
        }
    }

    private static class Stub.Proxy
        implements SynthesizerListener
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.SynthesizerListener";
        }

        public void onBufferProgress(int i, int j, int k, String s)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SynthesizerListener");
            parcel.writeInt(i);
            parcel.writeInt(j);
            parcel.writeInt(k);
            parcel.writeString(s);
            mRemote.transact(6, parcel, null, 1);
            parcel.recycle();
            return;
            s;
            parcel.recycle();
            throw s;
        }

        public void onCompleted(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SynthesizerListener");
            parcel.writeInt(i);
            mRemote.transact(4, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onEvent(int i, int j, int k, Bundle bundle)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SynthesizerListener");
            parcel.writeInt(i);
            parcel.writeInt(j);
            parcel.writeInt(k);
            if (bundle == null)
            {
                break MISSING_BLOCK_LABEL_71;
            }
            parcel.writeInt(1);
            bundle.writeToParcel(parcel, 0);
_L1:
            mRemote.transact(7, parcel, null, 1);
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            bundle;
            parcel.recycle();
            throw bundle;
        }

        public void onSpeakBegin()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SynthesizerListener");
            mRemote.transact(1, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onSpeakPaused()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SynthesizerListener");
            mRemote.transact(2, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onSpeakProgress(int i, int j, int k)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SynthesizerListener");
            parcel.writeInt(i);
            parcel.writeInt(j);
            parcel.writeInt(k);
            mRemote.transact(5, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onSpeakResumed()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SynthesizerListener");
            mRemote.transact(3, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        Stub.Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    public abstract void onBufferProgress(int i, int j, int k, String s)
        throws RemoteException;

    public abstract void onCompleted(int i)
        throws RemoteException;

    public abstract void onEvent(int i, int j, int k, Bundle bundle)
        throws RemoteException;

    public abstract void onSpeakBegin()
        throws RemoteException;

    public abstract void onSpeakPaused()
        throws RemoteException;

    public abstract void onSpeakProgress(int i, int j, int k)
        throws RemoteException;

    public abstract void onSpeakResumed()
        throws RemoteException;
}
