// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

// Referenced classes of package com.iflytek.speech:
//            SynthesizeToUrlListener

public static abstract class attachInterface extends Binder
    implements SynthesizeToUrlListener
{
    private static class Proxy
        implements SynthesizeToUrlListener
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.SynthesizeToUrlListener";
        }

        public void onSynthesizeCompleted(String s, int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SynthesizeToUrlListener");
            parcel.writeString(s);
            parcel.writeInt(i);
            mRemote.transact(1, parcel, null, 1);
            parcel.recycle();
            return;
            s;
            parcel.recycle();
            throw s;
        }

        Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    private static final String DESCRIPTOR = "com.iflytek.speech.SynthesizeToUrlListener";
    static final int TRANSACTION_onSynthesizeCompleted = 1;

    public static SynthesizeToUrlListener asInterface(IBinder ibinder)
    {
        if (ibinder == null)
        {
            return null;
        }
        android.os.IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.SynthesizeToUrlListener");
        if (iinterface != null && (iinterface instanceof SynthesizeToUrlListener))
        {
            return (SynthesizeToUrlListener)iinterface;
        } else
        {
            return new Proxy(ibinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
        throws RemoteException
    {
        switch (i)
        {
        default:
            return super.onTransact(i, parcel, parcel1, j);

        case 1598968902: 
            parcel1.writeString("com.iflytek.speech.SynthesizeToUrlListener");
            return true;

        case 1: // '\001'
            parcel.enforceInterface("com.iflytek.speech.SynthesizeToUrlListener");
            onSynthesizeCompleted(parcel.readString(), parcel.readInt());
            return true;
        }
    }

    public Proxy.mRemote()
    {
        attachInterface(this, "com.iflytek.speech.SynthesizeToUrlListener");
    }
}
