// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

// Referenced classes of package com.iflytek.speech:
//            GrammarListener

public static abstract class attachInterface extends Binder
    implements GrammarListener
{
    private static class Proxy
        implements GrammarListener
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.GrammarListener";
        }

        public void onBuildFinish(String s, int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.GrammarListener");
            parcel.writeString(s);
            parcel.writeInt(i);
            mRemote.transact(1, parcel, null, 1);
            parcel.recycle();
            return;
            s;
            parcel.recycle();
            throw s;
        }

        Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    private static final String DESCRIPTOR = "com.iflytek.speech.GrammarListener";
    static final int TRANSACTION_onBuildFinish = 1;

    public static GrammarListener asInterface(IBinder ibinder)
    {
        if (ibinder == null)
        {
            return null;
        }
        android.os.IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.GrammarListener");
        if (iinterface != null && (iinterface instanceof GrammarListener))
        {
            return (GrammarListener)iinterface;
        } else
        {
            return new Proxy(ibinder);
        }
    }

    public IBinder asBinder()
    {
        return this;
    }

    public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
        throws RemoteException
    {
        switch (i)
        {
        default:
            return super.onTransact(i, parcel, parcel1, j);

        case 1598968902: 
            parcel1.writeString("com.iflytek.speech.GrammarListener");
            return true;

        case 1: // '\001'
            parcel.enforceInterface("com.iflytek.speech.GrammarListener");
            onBuildFinish(parcel.readString(), parcel.readInt());
            return true;
        }
    }

    public Proxy.mRemote()
    {
        attachInterface(this, "com.iflytek.speech.GrammarListener");
    }
}
