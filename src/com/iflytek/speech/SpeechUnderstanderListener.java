// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

// Referenced classes of package com.iflytek.speech:
//            UnderstanderResult

public interface SpeechUnderstanderListener
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements SpeechUnderstanderListener
    {

        private static final String DESCRIPTOR = "com.iflytek.speech.SpeechUnderstanderListener";
        static final int TRANSACTION_onBeginOfSpeech = 2;
        static final int TRANSACTION_onEndOfSpeech = 3;
        static final int TRANSACTION_onError = 5;
        static final int TRANSACTION_onEvent = 6;
        static final int TRANSACTION_onResult = 4;
        static final int TRANSACTION_onVolumeChanged = 1;

        public static SpeechUnderstanderListener asInterface(IBinder ibinder)
        {
            if (ibinder == null)
            {
                return null;
            }
            IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.SpeechUnderstanderListener");
            if (iinterface != null && (iinterface instanceof SpeechUnderstanderListener))
            {
                return (SpeechUnderstanderListener)iinterface;
            } else
            {
                return new Proxy(ibinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            Object obj1 = null;
            Object obj = null;
            int k;
            switch (i)
            {
            default:
                return super.onTransact(i, parcel, parcel1, j);

            case 1598968902: 
                parcel1.writeString("com.iflytek.speech.SpeechUnderstanderListener");
                return true;

            case 1: // '\001'
                parcel.enforceInterface("com.iflytek.speech.SpeechUnderstanderListener");
                onVolumeChanged(parcel.readInt());
                return true;

            case 2: // '\002'
                parcel.enforceInterface("com.iflytek.speech.SpeechUnderstanderListener");
                onBeginOfSpeech();
                return true;

            case 3: // '\003'
                parcel.enforceInterface("com.iflytek.speech.SpeechUnderstanderListener");
                onEndOfSpeech();
                return true;

            case 4: // '\004'
                parcel.enforceInterface("com.iflytek.speech.SpeechUnderstanderListener");
                parcel1 = obj;
                if (parcel.readInt() != 0)
                {
                    parcel1 = (UnderstanderResult)UnderstanderResult.CREATOR.createFromParcel(parcel);
                }
                onResult(parcel1);
                return true;

            case 5: // '\005'
                parcel.enforceInterface("com.iflytek.speech.SpeechUnderstanderListener");
                onError(parcel.readInt());
                return true;

            case 6: // '\006'
                parcel.enforceInterface("com.iflytek.speech.SpeechUnderstanderListener");
                i = parcel.readInt();
                j = parcel.readInt();
                k = parcel.readInt();
                parcel1 = obj1;
                break;
            }
            if (parcel.readInt() != 0)
            {
                parcel1 = (Bundle)Bundle.CREATOR.createFromParcel(parcel);
            }
            onEvent(i, j, k, parcel1);
            return true;
        }

        public Stub()
        {
            attachInterface(this, "com.iflytek.speech.SpeechUnderstanderListener");
        }
    }

    private static class Stub.Proxy
        implements SpeechUnderstanderListener
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.SpeechUnderstanderListener";
        }

        public void onBeginOfSpeech()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SpeechUnderstanderListener");
            mRemote.transact(2, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onEndOfSpeech()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SpeechUnderstanderListener");
            mRemote.transact(3, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onError(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SpeechUnderstanderListener");
            parcel.writeInt(i);
            mRemote.transact(5, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onEvent(int i, int j, int k, Bundle bundle)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SpeechUnderstanderListener");
            parcel.writeInt(i);
            parcel.writeInt(j);
            parcel.writeInt(k);
            if (bundle == null)
            {
                break MISSING_BLOCK_LABEL_71;
            }
            parcel.writeInt(1);
            bundle.writeToParcel(parcel, 0);
_L1:
            mRemote.transact(6, parcel, null, 1);
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            bundle;
            parcel.recycle();
            throw bundle;
        }

        public void onResult(UnderstanderResult understanderresult)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SpeechUnderstanderListener");
            if (understanderresult == null)
            {
                break MISSING_BLOCK_LABEL_44;
            }
            parcel.writeInt(1);
            understanderresult.writeToParcel(parcel, 0);
_L1:
            mRemote.transact(4, parcel, null, 1);
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            understanderresult;
            parcel.recycle();
            throw understanderresult;
        }

        public void onVolumeChanged(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.SpeechUnderstanderListener");
            parcel.writeInt(i);
            mRemote.transact(1, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        Stub.Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    public abstract void onBeginOfSpeech()
        throws RemoteException;

    public abstract void onEndOfSpeech()
        throws RemoteException;

    public abstract void onError(int i)
        throws RemoteException;

    public abstract void onEvent(int i, int j, int k, Bundle bundle)
        throws RemoteException;

    public abstract void onResult(UnderstanderResult understanderresult)
        throws RemoteException;

    public abstract void onVolumeChanged(int i)
        throws RemoteException;
}
