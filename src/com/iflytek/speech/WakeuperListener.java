// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

// Referenced classes of package com.iflytek.speech:
//            WakeuperResult

public interface WakeuperListener
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements WakeuperListener
    {

        private static final String DESCRIPTOR = "com.iflytek.speech.WakeuperListener";
        static final int TRANSACTION_onBeginOfSpeech = 2;
        static final int TRANSACTION_onEndOfSpeech = 3;
        static final int TRANSACTION_onError = 5;
        static final int TRANSACTION_onResult = 4;
        static final int TRANSACTION_onVolumeChanged = 1;

        public static WakeuperListener asInterface(IBinder ibinder)
        {
            if (ibinder == null)
            {
                return null;
            }
            IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.WakeuperListener");
            if (iinterface != null && (iinterface instanceof WakeuperListener))
            {
                return (WakeuperListener)iinterface;
            } else
            {
                return new Proxy(ibinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            switch (i)
            {
            default:
                return super.onTransact(i, parcel, parcel1, j);

            case 1598968902: 
                parcel1.writeString("com.iflytek.speech.WakeuperListener");
                return true;

            case 1: // '\001'
                parcel.enforceInterface("com.iflytek.speech.WakeuperListener");
                onVolumeChanged(parcel.readInt());
                return true;

            case 2: // '\002'
                parcel.enforceInterface("com.iflytek.speech.WakeuperListener");
                onBeginOfSpeech();
                return true;

            case 3: // '\003'
                parcel.enforceInterface("com.iflytek.speech.WakeuperListener");
                onEndOfSpeech();
                return true;

            case 4: // '\004'
                parcel.enforceInterface("com.iflytek.speech.WakeuperListener");
                if (parcel.readInt() != 0)
                {
                    parcel = (WakeuperResult)WakeuperResult.CREATOR.createFromParcel(parcel);
                } else
                {
                    parcel = null;
                }
                onResult(parcel);
                return true;

            case 5: // '\005'
                parcel.enforceInterface("com.iflytek.speech.WakeuperListener");
                onError(parcel.readInt());
                return true;
            }
        }

        public Stub()
        {
            attachInterface(this, "com.iflytek.speech.WakeuperListener");
        }
    }

    private static class Stub.Proxy
        implements WakeuperListener
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.WakeuperListener";
        }

        public void onBeginOfSpeech()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.WakeuperListener");
            mRemote.transact(2, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onEndOfSpeech()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.WakeuperListener");
            mRemote.transact(3, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onError(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.WakeuperListener");
            parcel.writeInt(i);
            mRemote.transact(5, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onResult(WakeuperResult wakeuperresult)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.WakeuperListener");
            if (wakeuperresult == null)
            {
                break MISSING_BLOCK_LABEL_44;
            }
            parcel.writeInt(1);
            wakeuperresult.writeToParcel(parcel, 0);
_L1:
            mRemote.transact(4, parcel, null, 1);
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            wakeuperresult;
            parcel.recycle();
            throw wakeuperresult;
        }

        public void onVolumeChanged(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.WakeuperListener");
            parcel.writeInt(i);
            mRemote.transact(1, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        Stub.Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    public abstract void onBeginOfSpeech()
        throws RemoteException;

    public abstract void onEndOfSpeech()
        throws RemoteException;

    public abstract void onError(int i)
        throws RemoteException;

    public abstract void onResult(WakeuperResult wakeuperresult)
        throws RemoteException;

    public abstract void onVolumeChanged(int i)
        throws RemoteException;
}
