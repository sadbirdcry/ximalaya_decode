// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Parcel;
import android.os.Parcelable;

public class VerifierResult
    implements Parcelable
{

    public static final android.os.Parcelable.Creator CREATOR = new _cls1();
    public String dcs;
    private String json;
    public boolean ret;
    public int rgn;
    public String source;
    public String sst;
    public int suc;
    public String trs;
    public String vid;

    public VerifierResult(Parcel parcel)
    {
        json = "";
        ret = false;
        dcs = "";
        vid = "";
        suc = 0;
        rgn = 0;
        trs = "";
        source = "";
        json = parcel.readString();
    }

    public VerifierResult(String s)
    {
        json = "";
        ret = false;
        dcs = "";
        vid = "";
        suc = 0;
        rgn = 0;
        trs = "";
        source = "";
        if (s != null)
        {
            json = s;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String getResultString()
    {
        return json;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(json);
    }


    private class _cls1
        implements android.os.Parcelable.Creator
    {

        public VerifierResult createFromParcel(Parcel parcel)
        {
            return new VerifierResult(parcel);
        }

        public volatile Object createFromParcel(Parcel parcel)
        {
            return createFromParcel(parcel);
        }

        public VerifierResult[] newArray(int i)
        {
            return new VerifierResult[i];
        }

        public volatile Object[] newArray(int i)
        {
            return newArray(i);
        }

        _cls1()
        {
        }
    }

}
