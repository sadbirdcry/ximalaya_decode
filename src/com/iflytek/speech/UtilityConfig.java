// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import java.util.HashMap;

// Referenced classes of package com.iflytek.speech:
//            Version

public class UtilityConfig
{

    public static final String ACTION_SPEAKER_VERIFIER = "com.iflytek.component.speakerverifier";
    public static final String ACTION_SPEECH_RECOGNIZER = "com.iflytek.component.speechrecognizer";
    public static final String ACTION_SPEECH_SYNTHESIZER = "com.iflytek.component.speechsynthesizer";
    public static final String ACTION_SPEECH_UNDERSTANDER = "com.iflytek.component.speechunderstander";
    public static final String ACTION_SPEECH_WAKEUP = "com.iflytek.component.speechwakeuper";
    public static final String ACTION_TEXT_UNDERSTANDER = "com.iflytek.component.textunderstander";
    public static final String CHANNEL_ID = "16010000";
    public static final String CHANNEL_NAME = "dev.voicecloud";
    public static final String COMPONENT_PKG = "com.iflytek.speechcloud";
    public static final String COMPONENT_URL = "http://open.voicecloud.cn/s?";
    public static final String KEY_CALLER_APPID = "caller.appid";
    public static final String KEY_CALLER_NAME = "caller.name";
    public static final String KEY_CALLER_PKG_NAME = "caller.pkg";
    public static final String KEY_CALLER_VER_CODE = "caller.ver.code";
    public static final String KEY_CALLER_VER_NAME = "caller.ver.name";
    public static final String KEY_CHANNEL_ID = "channel.id";
    public static final String KEY_CHANNEL_NAME = "channel.name";
    public static final String KEY_REQUEST_PACKAGE = "request.package";
    public static final String METADATA_KEY_ENGINE_TYPE = "enginetype";
    public static final String SDK_VER_NAME = "sdk.ver.name";
    public static final String SETTINGS_ACTION_ASR = "com.iflytek.speechcloud.settings.asr";
    public static final String SETTINGS_ACTION_MAIN = "com.iflytek.speechcloud.settings.main";
    public static final String SETTINGS_ACTION_TTS = "com.iflytek.speechcloud.activity.speaker.SpeakerSetting";
    private static HashMap callerHashMap = new HashMap();

    public UtilityConfig()
    {
    }

    public static void appendHttpParam(StringBuffer stringbuffer, String s, String s1)
    {
        if (stringbuffer != null && s != null && s1 != null && s1.length() > 0)
        {
            stringbuffer.append('&');
            stringbuffer.append(s);
            stringbuffer.append('=');
            stringbuffer.append(s1);
        }
    }

    public static String getCallerInfo(Context context, String s)
    {
        if (callerHashMap.containsKey(s))
        {
            return (String)callerHashMap.get(s);
        }
        try
        {
            Object obj = context.getPackageName();
            PackageInfo packageinfo = context.getPackageManager().getPackageInfo(((String) (obj)), 0);
            obj = context.getPackageManager().getApplicationInfo(((String) (obj)), 0);
            callerHashMap.put("caller.name", ((ApplicationInfo) (obj)).loadLabel(context.getPackageManager()).toString());
            callerHashMap.put("caller.pkg", ((ApplicationInfo) (obj)).packageName);
            callerHashMap.put("caller.ver.name", packageinfo.versionName);
            callerHashMap.put("caller.ver.code", String.valueOf(packageinfo.versionCode));
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
        return (String)callerHashMap.get(s);
    }

    public static String getComponentUrlParam(Context context)
    {
        String s = getCallerInfo(context, "caller.name");
        context = getCallerInfo(context, "caller.pkg");
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append("channel.id");
        stringbuffer.append('=');
        stringbuffer.append("16010000");
        appendHttpParam(stringbuffer, "sdk.ver.name", Version.getVersionName());
        appendHttpParam(stringbuffer, "caller.name", s);
        appendHttpParam(stringbuffer, "caller.pkg", context);
        return stringbuffer.toString();
    }

}
