// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.speech;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

// Referenced classes of package com.iflytek.speech:
//            VerifierResult

public interface VerifierListener
    extends IInterface
{
    public static abstract class Stub extends Binder
        implements VerifierListener
    {

        private static final String DESCRIPTOR = "com.iflytek.speech.VerifierListener";
        static final int TRANSACTION_onBeginOfSpeech = 2;
        static final int TRANSACTION_onCancel = 6;
        static final int TRANSACTION_onEnd = 5;
        static final int TRANSACTION_onEndOfSpeech = 3;
        static final int TRANSACTION_onError = 7;
        static final int TRANSACTION_onRegister = 4;
        static final int TRANSACTION_onVolumeChanged = 1;

        public static VerifierListener asInterface(IBinder ibinder)
        {
            if (ibinder == null)
            {
                return null;
            }
            IInterface iinterface = ibinder.queryLocalInterface("com.iflytek.speech.VerifierListener");
            if (iinterface != null && (iinterface instanceof VerifierListener))
            {
                return (VerifierListener)iinterface;
            } else
            {
                return new Proxy(ibinder);
            }
        }

        public IBinder asBinder()
        {
            return this;
        }

        public boolean onTransact(int i, Parcel parcel, Parcel parcel1, int j)
            throws RemoteException
        {
            Object obj1 = null;
            Object obj = null;
            switch (i)
            {
            default:
                return super.onTransact(i, parcel, parcel1, j);

            case 1598968902: 
                parcel1.writeString("com.iflytek.speech.VerifierListener");
                return true;

            case 1: // '\001'
                parcel.enforceInterface("com.iflytek.speech.VerifierListener");
                onVolumeChanged(parcel.readInt());
                return true;

            case 2: // '\002'
                parcel.enforceInterface("com.iflytek.speech.VerifierListener");
                onBeginOfSpeech();
                return true;

            case 3: // '\003'
                parcel.enforceInterface("com.iflytek.speech.VerifierListener");
                onEndOfSpeech();
                return true;

            case 4: // '\004'
                parcel.enforceInterface("com.iflytek.speech.VerifierListener");
                parcel1 = obj;
                if (parcel.readInt() != 0)
                {
                    parcel1 = (VerifierResult)VerifierResult.CREATOR.createFromParcel(parcel);
                }
                onRegister(parcel1);
                return true;

            case 5: // '\005'
                parcel.enforceInterface("com.iflytek.speech.VerifierListener");
                parcel1 = obj1;
                if (parcel.readInt() != 0)
                {
                    parcel1 = (VerifierResult)VerifierResult.CREATOR.createFromParcel(parcel);
                }
                onEnd(parcel1, parcel.readInt());
                return true;

            case 6: // '\006'
                parcel.enforceInterface("com.iflytek.speech.VerifierListener");
                onCancel();
                return true;

            case 7: // '\007'
                parcel.enforceInterface("com.iflytek.speech.VerifierListener");
                onError(parcel.readInt());
                return true;
            }
        }

        public Stub()
        {
            attachInterface(this, "com.iflytek.speech.VerifierListener");
        }
    }

    private static class Stub.Proxy
        implements VerifierListener
    {

        private IBinder mRemote;

        public IBinder asBinder()
        {
            return mRemote;
        }

        public String getInterfaceDescriptor()
        {
            return "com.iflytek.speech.VerifierListener";
        }

        public void onBeginOfSpeech()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.VerifierListener");
            mRemote.transact(2, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onCancel()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.VerifierListener");
            mRemote.transact(6, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onEnd(VerifierResult verifierresult, int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.VerifierListener");
            if (verifierresult == null)
            {
                break MISSING_BLOCK_LABEL_49;
            }
            parcel.writeInt(1);
            verifierresult.writeToParcel(parcel, 0);
_L1:
            parcel.writeInt(i);
            mRemote.transact(5, parcel, null, 1);
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            verifierresult;
            parcel.recycle();
            throw verifierresult;
        }

        public void onEndOfSpeech()
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.VerifierListener");
            mRemote.transact(3, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onError(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.VerifierListener");
            parcel.writeInt(i);
            mRemote.transact(7, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        public void onRegister(VerifierResult verifierresult)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.VerifierListener");
            if (verifierresult == null)
            {
                break MISSING_BLOCK_LABEL_44;
            }
            parcel.writeInt(1);
            verifierresult.writeToParcel(parcel, 0);
_L1:
            mRemote.transact(4, parcel, null, 1);
            parcel.recycle();
            return;
            parcel.writeInt(0);
              goto _L1
            verifierresult;
            parcel.recycle();
            throw verifierresult;
        }

        public void onVolumeChanged(int i)
            throws RemoteException
        {
            Parcel parcel = Parcel.obtain();
            parcel.writeInterfaceToken("com.iflytek.speech.VerifierListener");
            parcel.writeInt(i);
            mRemote.transact(1, parcel, null, 1);
            parcel.recycle();
            return;
            Exception exception;
            exception;
            parcel.recycle();
            throw exception;
        }

        Stub.Proxy(IBinder ibinder)
        {
            mRemote = ibinder;
        }
    }


    public abstract void onBeginOfSpeech()
        throws RemoteException;

    public abstract void onCancel()
        throws RemoteException;

    public abstract void onEnd(VerifierResult verifierresult, int i)
        throws RemoteException;

    public abstract void onEndOfSpeech()
        throws RemoteException;

    public abstract void onError(int i)
        throws RemoteException;

    public abstract void onRegister(VerifierResult verifierresult)
        throws RemoteException;

    public abstract void onVolumeChanged(int i)
        throws RemoteException;
}
