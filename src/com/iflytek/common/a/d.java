// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.common.a;

import android.content.Context;
import com.iflytek.common.c.a;
import com.iflytek.common.c.b;
import com.iflytek.common.c.c;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

// Referenced classes of package com.iflytek.common.a:
//            g, e, c, a, 
//            b

public class d
{

    private static d a;
    private com.iflytek.common.c.d b;
    private com.iflytek.common.a.a c;
    private Context d;
    private e e;
    private a f;
    private com.iflytek.common.a.b g;

    private d(Context context)
    {
        c = null;
        g = new g(this);
        b = new com.iflytek.common.c.d(context);
        d = context;
        f = new a(context);
        e = new e(b);
        com.iflytek.common.c.c.a(d, (new StringBuilder("load settings: pkgRun=")).append(e.g()).append(" periodRun=").append(e.a()).append(" periodUpdate=").append(e.b()).toString());
    }

    public static d a(Context context)
    {
        com/iflytek/common/a/d;
        JVM INSTR monitorenter ;
        if (a == null)
        {
            a = new d(context);
        }
        context = a;
        com/iflytek/common/a/d;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    static void a(d d1, String s, int i)
    {
        d1.a(s, i);
    }

    private void a(String s, int i)
    {
        if (i == 0 && s != null)
        {
            e.c(System.currentTimeMillis());
            com.iflytek.common.c.c.a(d, "get config success");
            com.iflytek.common.c.c.a(d, s);
            b(s);
            com.iflytek.common.a.c.a(d);
        } else
        {
            com.iflytek.common.c.c.a(d, (new StringBuilder("get config error:")).append(i).toString());
        }
        c = null;
    }

    private void b(String s)
    {
        Object obj;
        if (s == null || s.length() <= 0)
        {
            com.iflytek.common.c.c.a("LaunchSettings", "loadJson empty.");
            return;
        }
        obj = new JSONTokener(s);
        s = "";
        JSONArray jsonarray;
        String as[];
        long l;
        obj = new JSONObject(((JSONTokener) (obj)));
        l = ((JSONObject) (obj)).getLong("updateperiod");
        obj = ((JSONObject) (obj)).getJSONObject("launch");
        jsonarray = ((JSONObject) (obj)).getJSONArray("runpackage");
        as = new String[jsonarray.length()];
        int i = 0;
_L2:
        if (i >= jsonarray.length())
        {
            break; /* Loop/switch isn't completed */
        }
        as[i] = jsonarray.getString(i);
        s = (new StringBuilder()).append(s).append(as[i]).append(";").toString();
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        try
        {
            long l1 = ((JSONObject) (obj)).getLong("runperiod");
            e.a(as);
            e.a(l);
            e.b(l1);
            com.iflytek.common.c.c.a(d, (new StringBuilder("parse json updateperiod:")).append(l).append(" runperiod:").append(l1).append(" runpackage:").append(s).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s = s.getMessage();
        }
        com.iflytek.common.c.c.a(d, (new StringBuilder("parse json error:")).append(s).toString());
        return;
    }

    private String d()
    {
        String s = f();
        String s1 = e();
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("?t=");
        stringbuilder.append(s);
        stringbuilder.append("&p=");
        stringbuilder.append(s1);
        return stringbuilder.toString();
    }

    private String e()
    {
        Object obj;
        Object obj1;
        StringBuilder stringbuilder1;
        String s1;
label0:
        {
            String s = f.a();
            s1 = f.b();
            obj1 = e.f();
            stringbuilder1 = new StringBuilder();
            stringbuilder1.append("imei=");
            if (s != null)
            {
                obj = s;
                if (s.length() != 0)
                {
                    break label0;
                }
            }
            obj = "null";
        }
        stringbuilder1.append(((String) (obj)));
        stringbuilder1.append(",ua=");
        stringbuilder1.append(s1);
        stringbuilder1.append(",appid=");
        StringBuilder stringbuilder;
        if (obj1 == null || ((String) (obj1)).length() == 0)
        {
            obj = "null";
        } else
        {
            obj = obj1;
        }
        stringbuilder1.append(((String) (obj)));
        stringbuilder1.append(",sdkver=2.0");
        stringbuilder1.append((new StringBuilder(",pkg=")).append(d.getPackageName()).toString());
        obj = stringbuilder1.toString();
        obj1 = com.iflytek.common.c.b.a(((String) (obj)).getBytes()).toCharArray();
        for (int i = 0; i < 8; i++)
        {
            char c1 = obj1[i];
            obj1[i] = obj1[(obj1.length - 8) + i];
            obj1[(obj1.length - 8) + i] = c1;
        }

        stringbuilder = new StringBuilder();
        for (int j = 0; j < obj1.length; j++)
        {
            stringbuilder.append(obj1[j]);
        }

        com.iflytek.common.c.c.a(d, ((String) (obj)));
        return stringbuilder.toString();
    }

    private String f()
    {
        return (new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())).format(new Date());
    }

    public void a()
    {
        if (e.d() > System.currentTimeMillis())
        {
            e.c(System.currentTimeMillis() - e.b());
        }
        long l = System.currentTimeMillis() - e.d();
        long l1 = e.b();
        if (l > l1)
        {
            if (c == null)
            {
                String s = (new StringBuilder("http://hxqd.openspeech.cn/launchconfig")).append(d()).toString();
                c = new com.iflytek.common.a.a(s, g);
                c.start();
                com.iflytek.common.c.c.a(d, "check update start get config ");
                com.iflytek.common.c.c.a(d, s);
                return;
            } else
            {
                com.iflytek.common.c.c.a("LaunchSettings", "mGetThread running.");
                return;
            }
        } else
        {
            com.iflytek.common.c.c.a(d, (new StringBuilder("check update interval=")).append(l).append(" period=").append(l1).append(" ret=false").toString());
            return;
        }
    }

    public void a(long l)
    {
        e.d(l);
    }

    public void a(String s, String s1)
    {
        if ("appid".equals(s))
        {
            e.a(s1);
            return;
        } else
        {
            com.iflytek.common.c.c.a("LaunchSettings", (new StringBuilder("unkown key =")).append(s).toString());
            return;
        }
    }

    public boolean a(String s)
    {
        if (e.c() != null && s != null)
        {
            String as[] = e.c();
            int j = as.length;
            int i = 0;
            while (i < j) 
            {
                if (s.equals(as[i]))
                {
                    return true;
                }
                i++;
            }
        }
        return false;
    }

    public boolean b()
    {
        if (e.e() > System.currentTimeMillis())
        {
            e.d(System.currentTimeMillis() - e.a());
        }
        boolean flag1 = false;
        long l = System.currentTimeMillis() - e.e();
        boolean flag = flag1;
        if (e.a() > 0L)
        {
            flag = flag1;
            if (l > e.a())
            {
                flag = true;
            }
        }
        com.iflytek.common.c.c.a(d, (new StringBuilder("check launch interval=")).append(l).append(" period=").append(e.a()).append(" ret=").append(flag).toString());
        return flag;
    }

    public long c()
    {
        return e.a();
    }
}
