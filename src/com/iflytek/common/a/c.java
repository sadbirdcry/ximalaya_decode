// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.common.a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import com.iflytek.common.LaunchService;
import com.iflytek.common.c.e;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;

// Referenced classes of package com.iflytek.common.a:
//            d, f

public class c
{

    private static d a = null;
    private static int b = -1;

    public static void a(Context context)
    {
        com/iflytek/common/a/c;
        JVM INSTR monitorenter ;
        com.iflytek.common.c.c.a(context, "============start=============");
        d d1 = com.iflytek.common.a.d.a(context);
        a = d1;
        d1.a();
        if (a.b())
        {
            a.a(System.currentTimeMillis());
            e.a.execute(new f(context));
        }
        d(context);
        com/iflytek/common/a/c;
        JVM INSTR monitorexit ;
        return;
        context;
        throw context;
    }

    private static void a(Context context, ResolveInfo resolveinfo)
    {
        resolveinfo = resolveinfo.serviceInfo.packageName;
        if (!a.a(resolveinfo))
        {
            com.iflytek.common.c.c.a("LaunchImpl", (new StringBuilder("startService not need:")).append(resolveinfo).toString());
            return;
        }
        try
        {
            if (!context.getPackageName().equals(resolveinfo))
            {
                Intent intent = new Intent("com.iflytek.common.ACTION_LAUNCH");
                intent.setPackage(resolveinfo);
                resolveinfo = context.startService(intent);
                com.iflytek.common.c.c.a(context, (new StringBuilder("start app:")).append(resolveinfo).toString());
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            com.iflytek.common.c.c.b("LaunchImpl", "", context);
        }
        return;
    }

    public static void a(Context context, String s, String s1)
    {
        com/iflytek/common/a/c;
        JVM INSTR monitorenter ;
        if (a == null)
        {
            a = com.iflytek.common.a.d.a(context);
        }
        a.a(s, s1);
        com/iflytek/common/a/c;
        JVM INSTR monitorexit ;
        return;
        context;
        throw context;
    }

    static void b(Context context)
    {
        c(context);
    }

    private static void c(Context context)
    {
        Object obj;
        try
        {
            obj = context.getPackageManager().queryIntentServices(new Intent("com.iflytek.common.ACTION_LAUNCH"), 224);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            com.iflytek.common.c.c.b("LaunchImpl", "", ((Throwable) (obj)));
            obj = null;
        }
        if (obj == null)
        {
            com.iflytek.common.c.c.b("LaunchImpl", "query action null");
        } else
        {
            obj = ((List) (obj)).iterator();
            while (((Iterator) (obj)).hasNext()) 
            {
                a(context, (ResolveInfo)((Iterator) (obj)).next());
            }
        }
    }

    private static void d(Context context)
    {
        if (b != 0)
        {
            try
            {
                Intent intent = new Intent();
                intent.setClass(context, com/iflytek/common/LaunchService);
                if (context.startService(intent) != null)
                {
                    b = 1;
                    return;
                }
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                b = 0;
                com.iflytek.common.c.c.b("LaunchImpl", "start self Service error");
                return;
            }
        }
    }

}
