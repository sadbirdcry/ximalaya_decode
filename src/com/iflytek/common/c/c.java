// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.common.c;

import android.content.Context;
import android.os.Process;
import android.util.Log;
import java.io.File;
import java.io.RandomAccessFile;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class c
{

    protected static boolean a = false;

    public static int a(String s, String s1)
    {
        int i = 0;
        if (a)
        {
            i = Log.d((new StringBuilder("LaunchLib_")).append(s).toString(), s1);
        }
        return i;
    }

    public static int a(String s, String s1, Throwable throwable)
    {
        int i = 0;
        if (a)
        {
            i = Log.d((new StringBuilder("LaunchLib_")).append(s).toString(), s1, throwable);
        }
        return i;
    }

    public static String a(long l)
    {
        Date date = new Date(l);
        return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS ", Locale.CHINESE)).format(date);
    }

    public static void a(Context context, String s)
    {
        com/iflytek/common/c/c;
        JVM INSTR monitorenter ;
        boolean flag = a;
        if (flag) goto _L2; else goto _L1
_L1:
        com/iflytek/common/c/c;
        JVM INSTR monitorexit ;
        return;
_L2:
        StringBuffer stringbuffer = new StringBuffer();
        stringbuffer.append(a(System.currentTimeMillis()));
        stringbuffer.append((new StringBuilder("(")).append(Process.myPid()).append(") ").toString());
        stringbuffer.append(s);
        stringbuffer.append('\n');
_L4:
        a("", stringbuffer.toString());
        if (android.os.Build.VERSION.SDK_INT <= 7) goto _L1; else goto _L3
_L3:
        context = new File(context.getExternalCacheDir(), "launch.log");
        if (!context.exists())
        {
            context.createNewFile();
            a("LaunchLib_", (new StringBuilder("logfile:")).append(context.getAbsolutePath()).toString());
        }
        context = new RandomAccessFile(context, "rw");
        if (context.length() > 0x100000L)
        {
            context.setLength(0L);
        }
        context.seek(context.length());
        context.write(stringbuffer.toString().getBytes());
        context.close();
          goto _L1
        context;
        a("", "", ((Throwable) (context)));
          goto _L1
        context;
        throw context;
        s;
        a("", "", ((Throwable) (s)));
          goto _L4
    }

    public static void a(boolean flag)
    {
        a = flag;
    }

    public static int b(String s, String s1)
    {
        int i = 0;
        if (a)
        {
            i = Log.e((new StringBuilder("LaunchLib_")).append(s).toString(), s1);
        }
        return i;
    }

    public static int b(String s, String s1, Throwable throwable)
    {
        int i = 0;
        if (a)
        {
            i = Log.e((new StringBuilder("LaunchLib_")).append(s).toString(), s1, throwable);
        }
        return i;
    }

}
