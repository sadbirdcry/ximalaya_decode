// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.common.c;

import android.content.Context;
import android.content.SharedPreferences;

// Referenced classes of package com.iflytek.common.c:
//            c

public final class d
{

    private SharedPreferences a;

    public d(Context context)
    {
        a = context.getSharedPreferences("ifly_launch_lib", 0);
    }

    public final void a(String s, long l)
    {
        try
        {
            android.content.SharedPreferences.Editor editor = a.edit();
            editor.putLong(s, l);
            editor.commit();
            return;
        }
        catch (Exception exception)
        {
            c.b("LaunchSetting", (new StringBuilder("setSetting(")).append(s).append(", ").append(l).append(")").toString(), exception);
        }
    }

    public final void a(String s, String s1)
    {
        String s2 = s1;
        if (s1 != null)
        {
            s2 = s1.replace("\0", "");
        }
        try
        {
            s1 = a.edit();
            s1.putString(s, s2);
            s1.commit();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            c.b("LaunchSetting", (new StringBuilder("setSetting(")).append(s).append(", ").append(s2).append(")").toString(), s1);
        }
    }

    public final long b(String s, long l)
    {
        long l1;
        try
        {
            l1 = a.getLong(s, l);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            c.b("LaunchSetting", "getLongSetting()", s);
            return l;
        }
        return l1;
    }

    public final String b(String s, String s1)
    {
        try
        {
            s = a.getString(s, s1);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            c.b("LaunchSetting", "getString()", s);
            return s1;
        }
        return s;
    }
}
