// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.iflytek.common;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.os.Process;
import com.iflytek.common.a.d;
import com.iflytek.common.c.c;

// Referenced classes of package com.iflytek.common:
//            b

public class LaunchService extends Service
{

    private d a;
    private long b;
    private BroadcastReceiver c;

    public LaunchService()
    {
        b = 0L;
        c = new b(this);
    }

    private void a()
    {
        try
        {
            IntentFilter intentfilter = new IntentFilter();
            intentfilter.addAction((new StringBuilder()).append(getPackageName()).append("_LAUNCH_ALARM_LAUNCH").toString());
            registerReceiver(c, intentfilter);
            return;
        }
        catch (Exception exception)
        {
            com.iflytek.common.c.c.b("LaunchService", "", exception);
        }
    }

    static void a(LaunchService launchservice)
    {
        launchservice.b();
    }

    private void b()
    {
        this;
        JVM INSTR monitorenter ;
        long l;
        long l1;
        long l2;
        l = a.c();
        l1 = System.currentTimeMillis() + l;
        l2 = l1 - b;
        if (l2 <= 0L || l2 >= l) goto _L2; else goto _L1
_L1:
        com.iflytek.common.c.c.a(this, (new StringBuilder("reg next alarm too short:")).append(l2).toString());
_L3:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        AlarmManager alarmmanager;
        Object obj1;
        alarmmanager = (AlarmManager)getSystemService("alarm");
        obj1 = new Intent((new StringBuilder()).append(getPackageName()).append("_LAUNCH_ALARM_LAUNCH").toString());
        obj1 = PendingIntent.getBroadcast(getApplicationContext(), 0, ((Intent) (obj1)), 0x8000000);
        if (l <= 0L)
        {
            break MISSING_BLOCK_LABEL_190;
        }
        b = l1;
        alarmmanager.cancel(((PendingIntent) (obj1)));
        alarmmanager.set(0, b, ((PendingIntent) (obj1)));
        com.iflytek.common.c.c.a(this, (new StringBuilder("reg next alarm: ")).append(com.iflytek.common.c.c.a(b)).toString());
          goto _L3
        Object obj;
        obj;
        com.iflytek.common.c.c.b("LaunchService", "", ((Throwable) (obj)));
          goto _L3
        obj;
        throw obj;
        com.iflytek.common.c.c.a(this, "not reg alarm,periodrun = 0.");
          goto _L3
    }

    public IBinder onBind(Intent intent)
    {
        return null;
    }

    public void onCreate()
    {
        super.onCreate();
        com.iflytek.common.c.c.a(this, (new StringBuilder("LaunchService onCreate pid=")).append(Process.myTid()).toString());
        a = d.a(getApplicationContext());
        a();
        com.iflytek.common.a.c.a(this);
        b();
    }

    public void onDestroy()
    {
        super.onDestroy();
        com.iflytek.common.c.c.a(this, "LaunchService onDestroy");
        try
        {
            unregisterReceiver(c);
            return;
        }
        catch (Exception exception)
        {
            com.iflytek.common.c.c.b("LaunchService", "", exception);
        }
    }

    public void onStart(Intent intent, int i)
    {
        super.onStart(intent, i);
        com.iflytek.common.c.c.a(this, (new StringBuilder("LaunchService onStart lastalarm=")).append(b).append(" periodrun=").append(a.c()).toString());
        if (b == 0L && a.c() > 0L)
        {
            b();
        }
    }
}
