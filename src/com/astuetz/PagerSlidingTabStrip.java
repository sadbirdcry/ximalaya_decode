// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.astuetz;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Locale;

// Referenced classes of package com.astuetz:
//            a, b

public class PagerSlidingTabStrip extends HorizontalScrollView
    implements android.view.ViewTreeObserver.OnGlobalLayoutListener
{
    static class SavedState extends android.view.View.BaseSavedState
    {

        public static final android.os.Parcelable.Creator CREATOR = new com.astuetz.b();
        int a;

        public void writeToParcel(Parcel parcel, int i1)
        {
            super.writeToParcel(parcel, i1);
            parcel.writeInt(a);
        }


        private SavedState(Parcel parcel)
        {
            super(parcel);
            a = parcel.readInt();
        }

        SavedState(Parcel parcel, com.astuetz.a a1)
        {
            this(parcel);
        }

        public SavedState(Parcelable parcelable)
        {
            super(parcelable);
        }
    }

    public static interface a
    {

        public abstract int a(int i1);
    }

    private class b
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final PagerSlidingTabStrip a;

        public void onPageScrollStateChanged(int i1)
        {
            if (i1 == 0)
            {
                PagerSlidingTabStrip.a(a, PagerSlidingTabStrip.a(a).getCurrentItem(), 0);
            }
            if (a.a != null)
            {
                a.a.onPageScrollStateChanged(i1);
            }
        }

        public void onPageScrolled(int i1, float f1, int j1)
        {
            PagerSlidingTabStrip.a(a, i1);
            PagerSlidingTabStrip.a(a, f1);
            PagerSlidingTabStrip.a(a, i1, (int)((float)PagerSlidingTabStrip.b(a).getChildAt(i1).getWidth() * f1));
            a.invalidate();
            if (a.a != null)
            {
                a.a.onPageScrolled(i1, f1, j1);
            }
        }

        public void onPageSelected(int i1)
        {
            if (PagerSlidingTabStrip.c(a))
            {
                a.a(i1);
            }
            if (a.a != null)
            {
                a.a.onPageSelected(i1);
            }
        }

        private b()
        {
            a = PagerSlidingTabStrip.this;
            super();
        }

        b(com.astuetz.a a1)
        {
            this();
        }
    }

    public static interface c
    {

        public abstract View getTabWidget(int i1);
    }


    private static final int b[] = {
        0x1010095, 0x1010098
    };
    private Typeface A;
    private int B;
    private int C;
    private int D;
    private int E;
    private Locale F;
    private boolean G;
    private ViewGroup H;
    public android.support.v4.view.ViewPager.OnPageChangeListener a;
    private android.widget.LinearLayout.LayoutParams c;
    private android.widget.LinearLayout.LayoutParams d;
    private final b e;
    private LinearLayout f;
    private ViewPager g;
    private int h;
    private int i;
    private float j;
    private Paint k;
    private Paint l;
    private int m;
    private int n;
    private int o;
    private boolean p;
    private boolean q;
    private int r;
    private int s;
    private int t;
    private int u;
    private int v;
    private int w;
    private int x;
    private int y;
    private int z;

    public PagerSlidingTabStrip(Context context)
    {
        this(context, null);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attributeset, int i1)
    {
        super(context, attributeset, i1);
        e = new b(null);
        i = 0;
        j = 0.0F;
        m = 0xff666666;
        n = 0x1a000000;
        o = 0x1a000000;
        p = false;
        q = true;
        r = 52;
        s = 8;
        t = 2;
        u = 12;
        v = 24;
        w = 1;
        x = 12;
        y = 0xff666666;
        z = 0xffcccccc;
        A = null;
        B = 0;
        C = 0;
        D = com.astuetz.a.a.b.background_tab;
        E = com.astuetz.a.a.a.transparent;
        setFillViewport(true);
        setWillNotDraw(false);
        f = new LinearLayout(context);
        f.setOrientation(0);
        f.setLayoutParams(new android.widget.FrameLayout.LayoutParams(-1, -1));
        addView(f);
        Object obj = getResources().getDisplayMetrics();
        r = (int)TypedValue.applyDimension(1, r, ((DisplayMetrics) (obj)));
        s = (int)TypedValue.applyDimension(1, s, ((DisplayMetrics) (obj)));
        t = (int)TypedValue.applyDimension(1, t, ((DisplayMetrics) (obj)));
        u = (int)TypedValue.applyDimension(1, u, ((DisplayMetrics) (obj)));
        v = (int)TypedValue.applyDimension(1, v, ((DisplayMetrics) (obj)));
        w = (int)TypedValue.applyDimension(1, w, ((DisplayMetrics) (obj)));
        x = (int)TypedValue.applyDimension(2, x, ((DisplayMetrics) (obj)));
        obj = context.obtainStyledAttributes(attributeset, b);
        x = ((TypedArray) (obj)).getDimensionPixelSize(0, x);
        y = ((TypedArray) (obj)).getColor(1, y);
        ((TypedArray) (obj)).recycle();
        context = context.obtainStyledAttributes(attributeset, com.astuetz.a.a.c.PagerSlidingTabStrip);
        m = context.getColor(0, m);
        n = context.getColor(1, n);
        o = context.getColor(2, o);
        s = context.getDimensionPixelSize(3, s);
        t = context.getDimensionPixelSize(4, t);
        u = context.getDimensionPixelSize(5, u);
        v = context.getDimensionPixelSize(6, v);
        D = context.getResourceId(8, D);
        p = context.getBoolean(9, p);
        r = context.getDimensionPixelSize(7, r);
        q = context.getBoolean(10, q);
        G = context.getBoolean(11, G);
        y = context.getColor(12, y);
        z = context.getColor(13, z);
        context.recycle();
        k = new Paint();
        k.setAntiAlias(true);
        k.setStyle(android.graphics.Paint.Style.FILL);
        l = new Paint();
        l.setAntiAlias(true);
        l.setStrokeWidth(w);
        c = new android.widget.LinearLayout.LayoutParams(-2, -1);
        d = new android.widget.LinearLayout.LayoutParams(0, -1, 1.0F);
        if (F == null)
        {
            F = getResources().getConfiguration().locale;
        }
    }

    static float a(PagerSlidingTabStrip pagerslidingtabstrip, float f1)
    {
        pagerslidingtabstrip.j = f1;
        return f1;
    }

    static int a(PagerSlidingTabStrip pagerslidingtabstrip, int i1)
    {
        pagerslidingtabstrip.i = i1;
        return i1;
    }

    static ViewPager a(PagerSlidingTabStrip pagerslidingtabstrip)
    {
        return pagerslidingtabstrip.g;
    }

    private void a(int i1, int j1)
    {
        ImageButton imagebutton = new ImageButton(getContext());
        imagebutton.setImageResource(j1);
        a(i1, ((View) (imagebutton)));
    }

    private void a(int i1, View view)
    {
        view.setFocusable(true);
        view.setOnClickListener(new com.astuetz.a(this, i1));
        view.setPadding(v, 0, v, 0);
        LinearLayout linearlayout = f;
        android.widget.LinearLayout.LayoutParams layoutparams;
        if (p)
        {
            layoutparams = d;
        } else
        {
            layoutparams = c;
        }
        linearlayout.addView(view, i1, layoutparams);
    }

    private void a(int i1, String s1)
    {
        TextView textview = new TextView(getContext());
        textview.setText(s1);
        textview.setGravity(17);
        textview.setSingleLine();
        a(i1, ((View) (textview)));
    }

    static void a(PagerSlidingTabStrip pagerslidingtabstrip, int i1, int j1)
    {
        pagerslidingtabstrip.b(i1, j1);
    }

    static LinearLayout b(PagerSlidingTabStrip pagerslidingtabstrip)
    {
        return pagerslidingtabstrip.f;
    }

    private void b(int i1, int j1)
    {
        if (h != 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int k1 = f.getChildAt(i1).getLeft() + j1;
        if (i1 <= 0)
        {
            i1 = k1;
            if (j1 <= 0)
            {
                continue; /* Loop/switch isn't completed */
            }
        }
        i1 = k1 - r;
        if (i1 == C) goto _L1; else goto _L3
_L3:
        C = i1;
        scrollTo(i1, 0);
        return;
    }

    private void c()
    {
        int i1 = 0;
        while (i1 < h) 
        {
            Object obj = f.getChildAt(i1);
            int j1;
            if (!G)
            {
                j1 = D;
            } else
            {
                j1 = E;
            }
            ((View) (obj)).setBackgroundResource(j1);
            if (obj instanceof TextView)
            {
                obj = (TextView)obj;
                ((TextView) (obj)).setTextSize(0, x);
                ((TextView) (obj)).setTypeface(A, B);
                if (G && i1 != 0)
                {
                    j1 = z;
                } else
                {
                    j1 = y;
                }
                ((TextView) (obj)).setTextColor(j1);
                if (q)
                {
                    if (android.os.Build.VERSION.SDK_INT >= 14)
                    {
                        ((TextView) (obj)).setAllCaps(true);
                    } else
                    {
                        ((TextView) (obj)).setText(((TextView) (obj)).getText().toString().toUpperCase(F));
                    }
                }
            } else
            if (obj instanceof ImageButton)
            {
                obj = (ImageButton)obj;
                boolean flag;
                if (G && i1 == 0)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                ((ImageButton) (obj)).setSelected(flag);
            }
            i1++;
        }
    }

    static boolean c(PagerSlidingTabStrip pagerslidingtabstrip)
    {
        return pagerslidingtabstrip.G;
    }

    public void a()
    {
        f.removeAllViews();
        h = g.getAdapter().getCount();
        int i1 = 0;
        while (i1 < h) 
        {
            if (g.getAdapter() instanceof a)
            {
                a(i1, ((a)g.getAdapter()).a(i1));
            } else
            if (g.getAdapter() instanceof c)
            {
                a(i1, ((c)g.getAdapter()).getTabWidget(i1));
            } else
            {
                a(i1, g.getAdapter().getPageTitle(i1).toString());
            }
            i1++;
        }
        c();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    public void a(int i1)
    {
        int j1 = 0;
        while (j1 < h) 
        {
            Object obj = f.getChildAt(j1);
            if (obj instanceof TextView)
            {
                obj = (TextView)obj;
                int k1;
                if (i1 == j1)
                {
                    k1 = y;
                } else
                {
                    k1 = z;
                }
                ((TextView) (obj)).setTextColor(k1);
            } else
            {
                boolean flag;
                if (i1 == j1)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                ((View) (obj)).setSelected(flag);
            }
            j1++;
        }
    }

    public void b()
    {
        if (android.os.Build.VERSION.SDK_INT >= 16)
        {
            getViewTreeObserver().removeOnGlobalLayoutListener(this);
            return;
        } else
        {
            getViewTreeObserver().removeGlobalOnLayoutListener(this);
            return;
        }
    }

    public int getDividerColor()
    {
        return o;
    }

    public int getDividerPadding()
    {
        return u;
    }

    public int getIndicatorColor()
    {
        return m;
    }

    public int getIndicatorHeight()
    {
        return s;
    }

    public int getScrollOffset()
    {
        return r;
    }

    public boolean getShouldExpand()
    {
        return p;
    }

    public int getTabBackground()
    {
        return D;
    }

    public int getTabPaddingLeftRight()
    {
        return v;
    }

    public int getTextColor()
    {
        return y;
    }

    public int getTextSize()
    {
        return x;
    }

    public int getUnderlineColor()
    {
        return n;
    }

    public int getUnderlineHeight()
    {
        return t;
    }

    protected void onDetachedFromWindow()
    {
        b();
        super.onDetachedFromWindow();
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if (!isInEditMode() && h != 0)
        {
            int j1 = getHeight();
            k.setColor(m);
            View view = f.getChildAt(i);
            float f4 = view.getLeft();
            float f3 = view.getRight();
            float f2 = f4;
            float f1 = f3;
            if (j > 0.0F)
            {
                f2 = f4;
                f1 = f3;
                if (i < h - 1)
                {
                    View view1 = f.getChildAt(i + 1);
                    f2 = view1.getLeft();
                    f1 = view1.getRight();
                    float f5 = j;
                    f2 = f4 * (1.0F - j) + f2 * f5;
                    f1 = f1 * j + (1.0F - j) * f3;
                }
            }
            canvas.drawRect(f2, j1 - s, f1, j1, k);
            k.setColor(n);
            canvas.drawRect(0.0F, j1 - t, f.getWidth(), j1, k);
            l.setColor(o);
            int i1 = 0;
            while (i1 < h - 1) 
            {
                View view2 = f.getChildAt(i1);
                canvas.drawLine(view2.getRight(), u, view2.getRight(), j1 - u, l);
                i1++;
            }
        }
    }

    public void onGlobalLayout()
    {
        if (android.os.Build.VERSION.SDK_INT < 16)
        {
            getViewTreeObserver().removeGlobalOnLayoutListener(this);
        } else
        {
            getViewTreeObserver().removeOnGlobalLayoutListener(this);
        }
        i = g.getCurrentItem();
        b(i, 0);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        if (H == null) goto _L2; else goto _L1
_L1:
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 40
    //                   0 46
    //                   1 57
    //                   2 46
    //                   3 57;
           goto _L2 _L3 _L4 _L3 _L4
_L2:
        return super.onInterceptTouchEvent(motionevent);
_L3:
        H.requestDisallowInterceptTouchEvent(true);
        continue; /* Loop/switch isn't completed */
_L4:
        H.requestDisallowInterceptTouchEvent(false);
        if (true) goto _L2; else goto _L5
_L5:
    }

    public void onRestoreInstanceState(Parcelable parcelable)
    {
        parcelable = (SavedState)parcelable;
        super.onRestoreInstanceState(parcelable.getSuperState());
        i = ((SavedState) (parcelable)).a;
        requestLayout();
    }

    public Parcelable onSaveInstanceState()
    {
        SavedState savedstate = new SavedState(super.onSaveInstanceState());
        savedstate.a = i;
        return savedstate;
    }

    public void setActivateTextColor(int i1)
    {
        y = i1;
        c();
    }

    public void setAllCaps(boolean flag)
    {
        q = flag;
    }

    public void setDeactivateTextColor(int i1)
    {
        z = i1;
        c();
    }

    public void setDisallowInterceptTouchEventView(ViewGroup viewgroup)
    {
        H = viewgroup;
    }

    public void setDividerColor(int i1)
    {
        o = i1;
        invalidate();
    }

    public void setDividerColorResource(int i1)
    {
        o = getResources().getColor(i1);
        invalidate();
    }

    public void setDividerPadding(int i1)
    {
        u = i1;
        invalidate();
    }

    public void setIndicatorColor(int i1)
    {
        m = i1;
        invalidate();
    }

    public void setIndicatorColorResource(int i1)
    {
        m = getResources().getColor(i1);
        invalidate();
    }

    public void setIndicatorHeight(int i1)
    {
        s = i1;
        invalidate();
    }

    public void setOnPageChangeListener(android.support.v4.view.ViewPager.OnPageChangeListener onpagechangelistener)
    {
        a = onpagechangelistener;
    }

    public void setScrollOffset(int i1)
    {
        r = i1;
        invalidate();
    }

    public void setShouldExpand(boolean flag)
    {
        p = flag;
        requestLayout();
    }

    public void setTabBackground(int i1)
    {
        D = i1;
    }

    public void setTabPaddingLeftRight(int i1)
    {
        v = i1;
        c();
    }

    public void setTabSwitch(boolean flag)
    {
        G = flag;
        c();
    }

    public void setTextColor(int i1)
    {
        y = i1;
        c();
    }

    public void setTextColorResource(int i1)
    {
        y = getResources().getColor(i1);
        c();
    }

    public void setTextSize(int i1)
    {
        x = (int)(getResources().getDisplayMetrics().density * (float)i1 + 0.5F);
        c();
    }

    public void setUnderlineColor(int i1)
    {
        n = i1;
        invalidate();
    }

    public void setUnderlineColorResource(int i1)
    {
        n = getResources().getColor(i1);
        invalidate();
    }

    public void setUnderlineHeight(int i1)
    {
        t = i1;
        invalidate();
    }

    public void setViewPager(ViewPager viewpager)
    {
        g = viewpager;
        if (viewpager.getAdapter() == null)
        {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        } else
        {
            viewpager.setOnPageChangeListener(e);
            a();
            return;
        }
    }

}
