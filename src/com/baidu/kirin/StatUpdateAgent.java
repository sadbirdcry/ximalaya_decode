// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.kirin;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.baidu.kirin.objects.KirinCheckState;
import com.baidu.mobstat.at;
import com.baidu.mobstat.f;
import com.baidu.mobstat.g;
import com.baidu.mobstat.i;
import com.baidu.mobstat.j;
import com.baidu.mobstat.m;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.baidu.kirin:
//            KirinConfig, PostChoiceListener, CheckUpdateListener, a, 
//            b

public class StatUpdateAgent
{

    static HandlerThread a = new HandlerThread("CheckUpdateManagerKirinAgent");
    private static Handler b;
    private static JSONObject c = null;

    public StatUpdateAgent()
    {
    }

    private static JSONObject a(Context context, String s)
    {
        if (!g.a(context).a(s) && !KirinConfig.DEBUG_MODE) goto _L2; else goto _L1
_L1:
        Object obj;
        m.a().a(new Object[] {
            "can update!"
        });
        obj = new i(context, "/kirinsdk/updatequery");
        ((i) (obj)).a("updateMoment", s);
        try
        {
            context = ((i) (obj)).c();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            m.a().b("send update query error!!");
            return null;
        }
        if (KirinConfig.DEBUG_MODE)
        {
            c = ((i) (obj)).a();
        }
        m.a().a(new Object[] {
            (new StringBuilder()).append("updateResult is : ").append(context.toString()).toString()
        });
        try
        {
            context.put("returncode", ((i) (obj)).d());
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return context;
        }
        obj = context;
_L4:
        return ((JSONObject) (obj));
_L2:
        m.a().a(new Object[] {
            "can not update"
        });
        context = new JSONObject();
        context.put("need_update", "0");
        context.put("returncode", 0);
        obj = context;
        if (!KirinConfig.DEBUG_MODE) goto _L4; else goto _L3
_L3:
        c = new JSONObject();
        c.put("Send", (new StringBuilder()).append("didn't send request! at moment : ").append(s).toString());
        return context;
        s;
_L6:
        s.printStackTrace();
        return context;
        s;
        context = null;
        if (true) goto _L6; else goto _L5
_L5:
    }

    private static void a()
    {
        if (!a.isAlive())
        {
            a.start();
            b = new Handler(a.getLooper());
        }
        if (b == null)
        {
            b = new Handler(a.getLooper());
        }
    }

    static void a(Context context, int k, PostChoiceListener postchoicelistener)
    {
        b(context, k, postchoicelistener);
    }

    static void a(Context context, boolean flag, CheckUpdateListener checkupdatelistener)
    {
        b(context, flag, checkupdatelistener);
    }

    private static boolean a(JSONObject jsonobject, HashMap hashmap)
    {
        try
        {
            hashmap.put("updatetype", jsonobject.getString("updatetype"));
            hashmap.put("note", jsonobject.getString("note"));
            hashmap.put("time", jsonobject.getString("time"));
            hashmap.put("appurl", jsonobject.getString("appurl"));
            hashmap.put("appname", jsonobject.getString("appname"));
            hashmap.put("version", jsonobject.getString("version"));
            hashmap.put("buildid", jsonobject.getString("buildid"));
            hashmap.put("attach", jsonobject.getJSONArray("attach").toString());
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject)
        {
            jsonobject.printStackTrace();
            return false;
        }
        return true;
    }

    private static void b(Context context, int k, PostChoiceListener postchoicelistener)
    {
        if (g.a(context).c())
        {
            context = new j(context, "/kirinsdk/updatecommit");
            context.a("updateType", (new StringBuilder()).append(k).append("").toString());
            JSONObject jsonobject = context.c();
            if (postchoicelistener != null)
            {
                postchoicelistener.PostUpdateChoiceResponse(jsonobject);
            }
            if (KirinConfig.DEBUG_MODE)
            {
                c = context.a();
                return;
            }
        }
    }

    private static void b(Context context, boolean flag, CheckUpdateListener checkupdatelistener)
    {
        HashMap hashmap;
        KirinCheckState kirincheckstate;
        if (checkupdatelistener == null)
        {
            return;
        }
        hashmap = new HashMap();
        kirincheckstate = KirinCheckState.ERROR_CHECK_VERSION;
        if (!flag) goto _L2; else goto _L1
_L1:
        Object obj = a(context, "atStart");
_L5:
        if (obj != null) goto _L4; else goto _L3
_L3:
        try
        {
            m.a().b("updateResult is null, net error!");
            checkupdatelistener.checkUpdateResponse(kirincheckstate, new HashMap());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            context = KirinCheckState.ERROR_CHECK_VERSION;
        }
        m.a().b((new StringBuilder()).append("Error checking online version: ").append(((Exception) (obj)).getMessage()).toString());
        ((Exception) (obj)).printStackTrace();
_L6:
        checkupdatelistener.checkUpdateResponse(context, hashmap);
        return;
_L2:
        obj = a(context, "atSetting");
          goto _L5
_L4:
        int k;
        k = ((JSONObject) (obj)).getInt("returncode");
        m.a().a(new Object[] {
            (new StringBuilder()).append("updateQuery's retCode is : ").append(k).toString()
        });
        if (k != 0)
        {
            break MISSING_BLOCK_LABEL_307;
        }
        if (Integer.parseInt(((JSONObject) (obj)).getString("need_update")) != 1)
        {
            break MISSING_BLOCK_LABEL_300;
        }
        if (Integer.parseInt(((JSONObject) (obj)).getString("buildid")) <= f.d(context))
        {
            break MISSING_BLOCK_LABEL_293;
        }
        if (!a(((JSONObject) (obj)), hashmap))
        {
            checkupdatelistener.checkUpdateResponse(KirinCheckState.ALREADY_UP_TO_DATE, hashmap);
            return;
        }
label0:
        {
            if (!"".endsWith(((JSONObject) (obj)).getString("appurl")) && ((JSONObject) (obj)).getString("appurl") != null)
            {
                break label0;
            }
            m.a().b("appurl is null or appurl'size is 0!");
            context = KirinCheckState.ALREADY_UP_TO_DATE;
        }
          goto _L6
label1:
        {
            if (((JSONObject) (obj)).getString("appurl").startsWith("http://"))
            {
                break label1;
            }
            m.a().b("appurl is not start with http://");
            context = KirinCheckState.ERROR_CHECK_VERSION;
        }
          goto _L6
        context = KirinCheckState.NEWER_VERSION_FOUND;
          goto _L6
        context = KirinCheckState.ALREADY_UP_TO_DATE;
          goto _L6
        context = KirinCheckState.ALREADY_UP_TO_DATE;
          goto _L6
        m.a().a("KirinSDK protocol error when mutual with backend");
        context = KirinCheckState.ALREADY_UP_TO_DATE;
          goto _L6
    }

    public static void checkUpdate(Context context, boolean flag, CheckUpdateListener checkupdatelistener)
    {
        a();
        if (checkupdatelistener == null)
        {
            m.a().c(new Object[] {
                "sdkstat", "The param of CheckUpdateListener is null, please new a instance of CheckUpdateListener"
            });
            return;
        } else
        {
            context = new a(context, flag, checkupdatelistener);
            b.post(context);
            return;
        }
    }

    public static void postUserChoice(Context context, int k, PostChoiceListener postchoicelistener)
    {
        a();
        context = new b(context, k, postchoicelistener);
        b.post(context);
    }

    public static void setTestMode()
    {
        KirinConfig.DEBUG_MODE = true;
        KirinConfig.LOG_LEVEL = 3;
        m.a().a(KirinConfig.LOG_LEVEL);
        KirinConfig.DEFAULT_UPDATE_INTERVAL = 0;
    }

}
