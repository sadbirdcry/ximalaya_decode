// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.kirin.objects;


public final class KirinCheckState extends Enum
{

    public static final KirinCheckState ALREADY_UP_TO_DATE;
    public static final KirinCheckState ERROR_CHECK_VERSION;
    public static final KirinCheckState NEWER_VERSION_FOUND;
    private static final KirinCheckState a[];

    private KirinCheckState(String s, int i)
    {
        super(s, i);
    }

    public static KirinCheckState valueOf(String s)
    {
        return (KirinCheckState)Enum.valueOf(com/baidu/kirin/objects/KirinCheckState, s);
    }

    public static KirinCheckState[] values()
    {
        return (KirinCheckState[])a.clone();
    }

    static 
    {
        ALREADY_UP_TO_DATE = new KirinCheckState("ALREADY_UP_TO_DATE", 0);
        NEWER_VERSION_FOUND = new KirinCheckState("NEWER_VERSION_FOUND", 1);
        ERROR_CHECK_VERSION = new KirinCheckState("ERROR_CHECK_VERSION", 2);
        a = (new KirinCheckState[] {
            ALREADY_UP_TO_DATE, NEWER_VERSION_FOUND, ERROR_CHECK_VERSION
        });
    }
}
