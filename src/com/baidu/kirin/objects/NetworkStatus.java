// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.kirin.objects;


public final class NetworkStatus extends Enum
{

    public static final NetworkStatus NotReachable;
    public static final NetworkStatus ThreeG;
    public static final NetworkStatus TwoG;
    public static final NetworkStatus Wifi;
    private static final NetworkStatus a[];

    private NetworkStatus(String s, int i)
    {
        super(s, i);
    }

    public static NetworkStatus valueOf(String s)
    {
        return (NetworkStatus)Enum.valueOf(com/baidu/kirin/objects/NetworkStatus, s);
    }

    public static NetworkStatus[] values()
    {
        return (NetworkStatus[])a.clone();
    }

    static 
    {
        NotReachable = new NetworkStatus("NotReachable", 0);
        TwoG = new NetworkStatus("TwoG", 1);
        ThreeG = new NetworkStatus("ThreeG", 2);
        Wifi = new NetworkStatus("Wifi", 3);
        a = (new NetworkStatus[] {
            NotReachable, TwoG, ThreeG, Wifi
        });
    }
}
