// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.a.c;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.zip.GZIPInputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.util.EntityUtils;

public class c
{

    public static String a(HttpEntity httpentity)
        throws ParseException, IOException
    {
        if (httpentity == null)
        {
            return null;
        }
        if (c(httpentity))
        {
            return b(httpentity);
        } else
        {
            return EntityUtils.toString(httpentity);
        }
    }

    public static String b(HttpEntity httpentity)
        throws IllegalStateException, IOException
    {
        StringWriter stringwriter;
        char ac[];
        if (httpentity == null)
        {
            throw new IllegalArgumentException("HTTP entity may not be null");
        }
        httpentity = new GZIPInputStream(httpentity.getContent());
        stringwriter = new StringWriter();
        ac = new char[1024];
        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(httpentity, "UTF-8"));
_L1:
        int i = bufferedreader.read(ac);
        if (i == -1)
        {
            httpentity.close();
            return stringwriter.toString();
        }
        stringwriter.write(ac, 0, i);
          goto _L1
        Exception exception;
        exception;
        httpentity.close();
        throw exception;
    }

    public static boolean c(HttpEntity httpentity)
        throws IllegalStateException, IOException
    {
        if (httpentity != null)
        {
            if ((httpentity = httpentity.getContentEncoding()) != null && ((httpentity = httpentity.getValue()) != null && httpentity.contains("gzip")))
            {
                return true;
            }
        }
        return false;
    }
}
