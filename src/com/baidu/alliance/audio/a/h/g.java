// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.a.h;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class g
{

    private static HashMap a;

    public static String a(long l, String s)
    {
        Date date = new Date(l);
        return (new SimpleDateFormat(s)).format(date);
    }

    static 
    {
        a = new HashMap();
        a.put("short_time_years_ago", "\u5E74\u524D");
        a.put("short_time_month_ago", "\u4E2A\u6708\u524D");
        a.put("short_time_days_ago", "\u5929\u524D");
        a.put("short_time_hours_ago", "\u5C0F\u65F6\u524D");
        a.put("short_time_minutes_ago", "\u5206\u949F\u524D");
        a.put("short_time_recently", "\u521A\u521A");
        a.put("lebo_duration_format", "%1$02d'%2$02d\"");
    }
}
