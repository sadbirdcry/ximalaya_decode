// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.a.h;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.telephony.TelephonyManager;
import com.baidu.alliance.audio.a.f.a;
import org.apache.http.HttpHost;
import org.apache.http.params.HttpParams;

public class e
{

    private static final String a = com/baidu/alliance/audio/a/h/e.getSimpleName();

    public e()
    {
    }

    public static void a(Context context, HttpParams httpparams)
    {
        if (context == null)
        {
            throw new RuntimeException("context can't be null");
        }
        context = (ConnectivityManager)context.getSystemService("connectivity");
        if (context != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if ((context = context.getActiveNetworkInfo()) == null || context.getExtraInfo() == null || context.getType() == 1)
        {
            continue; /* Loop/switch isn't completed */
        }
        context = context.getExtraInfo().toLowerCase();
        if (context == null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (context.startsWith("cmwap") || context.startsWith("uniwap") || context.startsWith("3gwap"))
        {
            httpparams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", 80));
            return;
        }
        if (context.startsWith("ctwap"))
        {
            httpparams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.200", 80));
            return;
        }
        if (context.startsWith("cmnet") || context.startsWith("uninet") || context.startsWith("ctnet") || context.startsWith("3gnet")) goto _L1; else goto _L3
_L3:
        context = Proxy.getDefaultHost();
        int i = Proxy.getDefaultPort();
        if (context != null && context.length() > 0)
        {
            if ("10.0.0.172".equals(context.trim()))
            {
                httpparams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.172", i));
                return;
            }
            if ("10.0.0.200".equals(context.trim()))
            {
                httpparams.setParameter("http.route.default-proxy", new HttpHost("10.0.0.200", 80));
                return;
            }
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    public static boolean a(Context context)
    {
        if (context == null)
        {
            return false;
        }
        context = (ConnectivityManager)context.getSystemService("connectivity");
label0:
        {
            {
                if (context != null)
                {
                    break label0;
                }
                int i;
                try
                {
                    com.baidu.alliance.audio.a.f.a.b(a, "+++couldn't get connectivity manager");
                }
                // Misplaced declaration of an exception variable
                catch (Context context)
                {
                    context.printStackTrace();
                }
            }
            com.baidu.alliance.audio.a.f.a.b(a, "+++network is not available");
            return false;
        }
        context = context.getAllNetworkInfo();
        if (context == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        i = 0;
_L2:
        if (i >= context.length)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (context[i].getState() != android.net.NetworkInfo.State.CONNECTED)
        {
            break MISSING_BLOCK_LABEL_77;
        }
        com.baidu.alliance.audio.a.f.a.b(a, "+++network is available");
        return true;
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        if (true) goto _L4; else goto _L3
_L4:
        break MISSING_BLOCK_LABEL_28;
_L3:
    }

    public static int b(Context context)
    {
        Object obj = (ConnectivityManager)context.getSystemService("connectivity");
        if (obj != null)
        {
            if ((obj = ((ConnectivityManager) (obj)).getActiveNetworkInfo()) != null)
            {
                obj = ((NetworkInfo) (obj)).getTypeName();
                int i;
                if ("mobile".equalsIgnoreCase(((String) (obj))))
                {
                    i = c(context);
                } else
                if ("wifi".equalsIgnoreCase(((String) (obj))))
                {
                    i = 1;
                } else
                {
                    i = 0;
                }
                return i;
            }
        }
        return 0;
    }

    public static int c(Context context)
    {
        switch (((TelephonyManager)context.getSystemService("phone")).getNetworkType())
        {
        default:
            return 5;

        case 3: // '\003'
        case 5: // '\005'
        case 6: // '\006'
        case 8: // '\b'
        case 9: // '\t'
        case 10: // '\n'
        case 12: // '\f'
        case 13: // '\r'
        case 14: // '\016'
        case 15: // '\017'
            return 3;

        case 1: // '\001'
        case 2: // '\002'
        case 4: // '\004'
        case 7: // '\007'
        case 11: // '\013'
            return 2;
        }
    }

    public static int d(Context context)
    {
        context = ((TelephonyManager)context.getSystemService("phone")).getSimOperator();
        if (context != null)
        {
            if (context.equals("46000") || context.equals("46002") || context.equals("46007"))
            {
                return 0;
            }
            if (context.equals("46001"))
            {
                return 1;
            }
            if (context.equals("46003"))
            {
                return 2;
            }
        }
        return 3;
    }

}
