// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.a.d;

import android.os.Handler;
import android.os.Looper;

// Referenced classes of package com.baidu.alliance.audio.a.d:
//            c, d

public abstract class b
{

    private static int a = 0;
    private static Handler g = new _cls2(Looper.getMainLooper());
    private String b;
    private d c;
    private boolean d;
    private int e;
    private Runnable f;

    public b()
    {
        StringBuilder stringbuilder = new StringBuilder("Job-");
        int i = a;
        a = i + 1;
        b = stringbuilder.append(String.valueOf(i)).toString();
        c = com.baidu.alliance.audio.a.d.c.a();
        e = 30000;
        f = new _cls1();
    }

    static String a(b b1)
    {
        return b1.b;
    }

    static void a(b b1, boolean flag)
    {
        b1.d = flag;
    }

    static d b(b b1)
    {
        return b1.c;
    }

    static Handler e()
    {
        return g;
    }

    protected abstract void a();

    protected final void a(d d1)
    {
        c = d1;
    }

    protected Runnable b()
    {
        return f;
    }

    public String c()
    {
        return b;
    }

    protected void d()
    {
    }

    protected void finalize()
        throws Throwable
    {
        if (c != null)
        {
            c.c(this);
        }
        super.finalize();
    }


    private class _cls1
        implements Runnable
    {

        final b a;

        public void run()
        {
            try
            {
                a.a();
                com.baidu.alliance.audio.a.f.a.b("Job", (new StringBuilder(String.valueOf(com.baidu.alliance.audio.a.d.b.a(a)))).append(" finished, bye!").toString());
            }
            catch (Exception exception)
            {
                com.baidu.alliance.audio.a.f.a.a("Job", "job failed", exception);
            }
            com.baidu.alliance.audio.a.d.b.a(a, true);
            b.e().sendMessage(b.e().obtainMessage(1, a));
            if (b.b(a) != null)
            {
                b.b(a).c(a);
            }
        }

        _cls1()
        {
            a = b.this;
            super();
        }
    }


    private class _cls2 extends Handler
    {

        public void handleMessage(Message message)
        {
            switch (message.what)
            {
            default:
                return;

            case 1: // '\001'
                break;
            }
            try
            {
                ((b)message.obj).d();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Message message)
            {
                message.printStackTrace();
            }
        }

        _cls2(Looper looper)
        {
            super(looper);
        }
    }

}
