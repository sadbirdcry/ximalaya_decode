// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.a.d;

import com.baidu.alliance.audio.a.f.a;
import java.util.WeakHashMap;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.baidu.alliance.audio.a.d:
//            b

public abstract class d
{

    private WeakHashMap a;
    private ArrayBlockingQueue b;
    private ThreadPoolExecutor c;

    public d(int i, int j, long l, int k)
    {
        a = new WeakHashMap();
        b = new ArrayBlockingQueue(10);
        c = new ThreadPoolExecutor(i, j, l, TimeUnit.SECONDS, b, new _cls1(k), new _cls2());
    }

    void b(b b1)
    {
        if (b1 == null)
        {
            return;
        } else
        {
            com.baidu.alliance.audio.a.f.a.b("ThreadPool", (new StringBuilder("Put ")).append(b1.c()).append(" into thread pool!").toString());
            Future future = c.submit(b1.b());
            b1.a(this);
            a.put(b1, future);
            return;
        }
    }

    void c(b b1)
    {
        if (b1 == null)
        {
            return;
        }
        Future future = (Future)a.remove(b1);
        if (future != null)
        {
            future.cancel(true);
        }
        c.remove(b1.b());
        b1.a(null);
    }

    private class _cls1
        implements ThreadFactory
    {

        final d a;
        private final int b;

        public Thread newThread(Runnable runnable)
        {
            runnable = new Thread(runnable);
            runnable.setPriority(b);
            return runnable;
        }

        _cls1(int i)
        {
            a = d.this;
            b = i;
            super();
        }
    }


    private class _cls2
        implements RejectedExecutionHandler
    {

        final d a;

        public void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadpoolexecutor)
        {
            threadpoolexecutor.remove(runnable);
        }

        _cls2()
        {
            a = d.this;
            super();
        }
    }

}
