// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio;


public class SDKConfig
{

    public static final boolean DEBUG_CONFIG_EDITABLE = false;
    public static final boolean DEBUG_MODE = false;
    public static final String ENCODING = "UTF-8";
    public static final String TAG_DISPATCHMANAGER = "LOG_DispatchManager";
    public static final String TAG_PERFORMANCE = "LOG_PERFORMANCE";
    public static final String TAG_REPORTMANAGER = "LOG_ReportManager";
    public static final String TAG_SDKMANAGER = "LOG_SDKManager";
    public static final String TAG_VERIFY = "LOG_Verify";
    public static final String VERSION = "1.0.1";
    public static final int VERSION_CODE = 2;

    public SDKConfig()
    {
    }
}
