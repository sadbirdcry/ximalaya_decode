// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.report;

import com.baidu.alliance.audio.SDKConfiguration;
import com.baidu.alliance.audio.SDKLogUtils;
import com.baidu.alliance.audio.SDKManager;
import com.baidu.alliance.audio.a.h.g;
import com.baidu.alliance.audio.logic.ad.AdvertisementManager;
import com.baidu.alliance.audio.logic.api.model.AdInfo;
import com.baidu.alliance.audio.logic.b.f;
import com.baidu.alliance.audio.logic.b.l;
import com.baidu.alliance.audio.logic.c.a;
import com.baidu.alliance.audio.logic.oauth.OAuthManager;
import com.baidu.mobstat.StatService;

public class b
{

    private AdvertisementManager a;
    private l b;
    private f c;
    private long d;
    private long e;
    private long f;
    private boolean g;

    public b()
    {
        d = 0L;
        e = 0L;
        f = 0L;
        g = false;
        a = AdvertisementManager.getInstance();
        b = com.baidu.alliance.audio.logic.b.l.a();
        c = com.baidu.alliance.audio.logic.b.f.d();
    }

    private int a(long l1)
    {
        int j1 = (int)l1;
        int i1 = j1;
        if (j1 < 0)
        {
            i1 = 0;
        }
        return (int)((float)i1 / 1000F);
    }

    private void a()
    {
        d = 0L;
        e = 0L;
        f = 0L;
        g = false;
    }

    private void a(String s1, AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            String s2 = com.baidu.alliance.audio.a.h.g.a(System.currentTimeMillis(), "kk:mm:ss.S");
            s1 = (new StringBuilder("[")).append(s2).append("][").append(s1).append("] <===> id = ").append(adinfo.id).append(", info = ").append(adinfo.info).toString();
            SDKLogUtils.d("LOG_ReportManager", s1);
            com.baidu.alliance.audio.logic.c.a.b().a(s1);
            return;
        }
    }

    private boolean a(Object obj)
    {
        return obj == null;
    }

    private void b()
    {
        e = System.currentTimeMillis();
    }

    private void c()
    {
        f = System.currentTimeMillis();
        if (f > e)
        {
            long l1 = f - e;
            com.baidu.alliance.audio.a.f.a.b("LOG_ReportManager", (new StringBuilder("endMarkPtMills duration = ")).append(l1).toString());
            d = l1 + d;
        }
    }

    private void o(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a.displayAdvertisement(adinfo);
            return;
        }
    }

    private void p(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a.dismissAdvertisement();
            return;
        }
    }

    private void q(AdInfo adinfo)
    {
        if (adinfo != null)
        {
            Class class1 = AdvertisementManager.getInstance().getWebAdvertisementClass();
            com.baidu.alliance.audio.logic.a.a.b().a(adinfo, class1);
        }
    }

    private void r(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            SDKLogUtils.d("LOG_ReportManager", "\u53D1\u9001\uFF1A\u64AD\u653E\u7C7B\u578B-\u5E7F\u544A\u64AD\u653E\u5B8C\u6210\u65E5\u5FD7");
            com.baidu.alliance.audio.logic.b.a a1 = new com.baidu.alliance.audio.logic.b.a();
            a1.f = true;
            a1.b = adinfo.id;
            a1.a = "1";
            a1.c = SDKManager.getInstance().getOAuthManager().getAccessToken();
            a1.e = adinfo.stats_ps;
            a1.d = adinfo.stats_pt;
            b.a(a1);
            StatService.onEvent(SDKManager.getInstance().getConfiguration().getContext(), "play", "sdk_log");
            return;
        }
    }

    private void s(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        }
        int i1 = a(System.currentTimeMillis() - e);
        if (g)
        {
            i1 = a(adinfo.audioDuration);
        }
        adinfo.stats_pt = i1;
        SDKLogUtils.d("LOG_ReportManager", "\u53D1\u9001\uFF1A\u70B9\u51FB\u7C7B\u578B-\u70B9\u51FB\u5E7F\u544A\u65E5\u5FD7");
        com.baidu.alliance.audio.logic.b.a a1 = new com.baidu.alliance.audio.logic.b.a();
        a1.f = true;
        a1.b = adinfo.id;
        a1.a = "2";
        a1.c = SDKManager.getInstance().getOAuthManager().getAccessToken();
        a1.e = "";
        a1.d = adinfo.stats_pt;
        b.b(a1);
        StatService.onEvent(SDKManager.getInstance().getConfiguration().getContext(), "click", "sdk_log");
    }

    private void t(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        }
        int i1 = a(System.currentTimeMillis() - e);
        if (g)
        {
            i1 = a(adinfo.audioDuration);
        }
        adinfo.stats_pt = i1;
        SDKLogUtils.d("LOG_ReportManager", "\u53D1\u9001\uFF1A\u5173\u95ED\u7C7B\u578B-\u5173\u95ED\u5E7F\u544A\u65E5\u5FD7");
        com.baidu.alliance.audio.logic.b.a a1 = new com.baidu.alliance.audio.logic.b.a();
        a1.f = true;
        a1.b = adinfo.id;
        a1.a = "1";
        a1.c = SDKManager.getInstance().getOAuthManager().getAccessToken();
        a1.d = adinfo.stats_pt;
        a1.e = "2";
        b.b(a1);
        StatService.onEvent(SDKManager.getInstance().getConfiguration().getContext(), "close", "sdk_log");
    }

    public void a(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onShow", adinfo);
            return;
        }
    }

    public void a(AdInfo adinfo, boolean flag)
    {
        if (a(adinfo))
        {
            return;
        }
        g = true;
        a("onComplete", adinfo);
        c.a(flag);
        String s1 = "\u6B63\u5E38\u64AD\u653E\u5B8C\u6210";
        if (flag)
        {
            s1 = "\u88AB\u7528\u6237\u5207\u6389";
        }
        SDKLogUtils.d("LOG_ReportManager", s1);
        c();
        int i1;
        if (flag)
        {
            i1 = (int)d;
            adinfo.stats_ps = "2";
        } else
        {
            i1 = adinfo.audioDuration;
            adinfo.stats_ps = "1";
        }
        i1 = a(i1);
        adinfo.stats_pt = i1;
        com.baidu.alliance.audio.a.f.a.b("LOG_ReportManager", (new StringBuilder("onComplete sendPlayLog duration = ")).append(i1).toString());
        r(adinfo);
    }

    public void b(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        }
        a("onClick", adinfo);
        if (!a.isComplex())
        {
            p(adinfo);
        }
        s(adinfo);
        q(adinfo);
    }

    public void c(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onClose", adinfo);
            p(adinfo);
            a.notifyAdvertisementCleanCallback();
            t(adinfo);
            return;
        }
    }

    public void d(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a();
            a("onPlay", adinfo);
            c.a(adinfo);
            c.a();
            k(adinfo);
            c.b();
            o(adinfo);
            b();
            g = false;
            return;
        }
    }

    public void e(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onPause", adinfo);
            a.pauseAdvertisement();
            c();
            return;
        }
    }

    public void f(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onResume", adinfo);
            a.resumeAdvertisement();
            b();
            return;
        }
    }

    public void g(AdInfo adinfo)
    {
        if (a(adinfo) || g)
        {
            return;
        } else
        {
            a("onRelease", adinfo);
            a(adinfo, true);
            return;
        }
    }

    public void h(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onBuffct", adinfo);
            c.c();
            return;
        }
    }

    public void i(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onAudioConnectRequest", adinfo);
            c.b(adinfo);
            return;
        }
    }

    public void j(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onAudioConnectResponse", adinfo);
            c.c(adinfo);
            return;
        }
    }

    public void k(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            c.d(adinfo);
            return;
        }
    }

    public void l(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onDisplayerContentRequest", adinfo);
            c.e(adinfo);
            return;
        }
    }

    public void m(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onDisplayerContentResult", adinfo);
            c.f(adinfo);
            return;
        }
    }

    public void n(AdInfo adinfo)
    {
        if (a(adinfo))
        {
            return;
        } else
        {
            a("onDisplayerContentFailed", adinfo);
            c.g(adinfo);
            return;
        }
    }
}
