// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.report;

import com.baidu.alliance.audio.logic.api.model.AdInfo;

// Referenced classes of package com.baidu.alliance.audio.logic.report:
//            b

public class ReportManager
{

    private static b mReportOperateImpl;
    private static ReportManager manager;

    public ReportManager()
    {
    }

    public static ReportManager getInstance()
    {
        if (manager != null) goto _L2; else goto _L1
_L1:
        com/baidu/alliance/audio/logic/report/ReportManager;
        JVM INSTR monitorenter ;
        manager = new ReportManager();
        com/baidu/alliance/audio/logic/report/ReportManager;
        JVM INSTR monitorexit ;
_L2:
        if (mReportOperateImpl != null) goto _L4; else goto _L3
_L3:
        com/baidu/alliance/audio/logic/report/b;
        JVM INSTR monitorenter ;
        mReportOperateImpl = new b();
        com/baidu/alliance/audio/logic/report/b;
        JVM INSTR monitorexit ;
_L4:
        return manager;
        Exception exception;
        exception;
        com/baidu/alliance/audio/logic/report/ReportManager;
        JVM INSTR monitorexit ;
        throw exception;
        exception;
        com/baidu/alliance/audio/logic/report/b;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private boolean isNull(Object obj)
    {
        return obj == null;
    }

    public void release()
    {
        if (mReportOperateImpl != null)
        {
            mReportOperateImpl = null;
        }
        if (manager != null)
        {
            manager = null;
        }
    }

    public void reportAdvertisementOnAudioConnectRequest(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.i(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnAudioConnectResponse(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.j(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnBuffct(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.h(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnClick(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.b(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnClose(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.c(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnComplete(AdInfo adinfo, boolean flag)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.a(adinfo, flag);
            return;
        }
    }

    public void reportAdvertisementOnDisplayerContentFailed(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.n(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnDisplayerContentRequest(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.l(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnDisplayerContentResult(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.m(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnPause(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.e(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnPlay(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.d(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnRelease(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.g(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnResume(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.f(adinfo);
            return;
        }
    }

    public void reportAdvertisementOnShow(AdInfo adinfo)
    {
        if (isNull(mReportOperateImpl))
        {
            return;
        } else
        {
            mReportOperateImpl.a(adinfo);
            return;
        }
    }
}
