// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.oauth;

import com.baidu.alliance.audio.ContextManager;
import com.baidu.alliance.audio.logic.oauth.oauth.OAuthListener;
import com.baidu.alliance.audio.logic.oauth.oauth.OAuthToken;
import com.baidu.alliance.audio.logic.oauth.oauth.b;

public class OAuthManager
{

    private String mAccessToken;
    private String mAppKey;
    private String mAppSecretKey;

    public OAuthManager()
    {
    }

    private void loadToken()
    {
        mAccessToken = b.a(ContextManager.getContext()).getAccessToken();
    }

    private void reset()
    {
        mAppKey = "";
        mAppSecretKey = "";
        mAccessToken = "";
    }

    public void authorize(OAuthListener oauthlistener)
    {
        b.a(ContextManager.getContext(), mAppKey, mAppSecretKey, oauthlistener);
    }

    public String getAccessToken()
    {
        return mAccessToken;
    }

    public String getAppKey()
    {
        return mAppKey;
    }

    public String getAppSecretKey()
    {
        return mAppSecretKey;
    }

    public void init(String s, String s1)
    {
        mAppKey = s;
        mAppSecretKey = s1;
        loadToken();
    }

    public void release()
    {
        reset();
    }

    public void updateToken()
    {
        loadToken();
    }

    public long validate()
    {
        return b.b(ContextManager.getContext());
    }
}
