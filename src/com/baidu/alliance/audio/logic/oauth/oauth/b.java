// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.oauth.oauth;

import android.content.Context;
import android.content.SharedPreferences;
import com.baidu.alliance.audio.a.c.g;
import com.baidu.alliance.audio.a.f.a;
import com.baidu.alliance.audio.a.h.d;
import com.baidu.alliance.audio.a.h.f;
import com.baidu.alliance.audio.logic.api.Config;
import java.io.IOException;
import javax.net.ssl.SSLPeerUnverifiedException;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.baidu.alliance.audio.logic.oauth.oauth:
//            a, OAuthToken, OAuthException, OAuthListener

public class b
{

    public static OAuthToken a(Context context)
    {
        return c(context);
    }

    private static String a(String s)
    {
        int i = Config.getGlobalRole();
        int j = Config.getGlobalEnvironment();
        return (new StringBuilder(String.valueOf(s))).append("_").append(i).append("_").append(j).toString();
    }

    private static String a(String s, String s1)
        throws SSLPeerUnverifiedException
    {
        if (f.a(s) || f.a(s1))
        {
            return null;
        } else
        {
            String s2 = d.a();
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.append(com.baidu.alliance.audio.logic.oauth.oauth.a.b());
            stringbuilder.append("app_key=");
            stringbuilder.append(s);
            stringbuilder.append("&app_secret=");
            stringbuilder.append(s1);
            stringbuilder.append("&cuid=");
            stringbuilder.append(s2);
            stringbuilder.append("&conn_t_out=");
            stringbuilder.append(com.baidu.alliance.audio.a.c.d.a());
            stringbuilder.append("&read_t_out=");
            stringbuilder.append(com.baidu.alliance.audio.a.c.d.b());
            return c(stringbuilder.toString());
        }
    }

    private static void a(Context context, String s, OAuthToken oauthtoken)
    {
        if (oauthtoken == null)
        {
            return;
        } else
        {
            context = context.getSharedPreferences(s, 0).edit();
            context.putString("access_token", oauthtoken.getAccessToken());
            context.putLong("save_time", System.currentTimeMillis() / 1000L);
            context.putLong("expire_time", oauthtoken.getExpiresTime());
            context.commit();
            com.baidu.alliance.audio.a.f.a.b("OAuthHelper", "save Token.");
            return;
        }
    }

    public static void a(Context context, String s, String s1)
        throws OAuthException
    {
        com.baidu.alliance.audio.a.f.a.b("OAuthHelper", (new StringBuilder("requestClientCredentialsToken appkey : ")).append(s).toString());
        try
        {
            s = a(s, s1);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            throw new OAuthException(-2, context.getMessage());
        }
        if (f.a(s))
        {
            com.baidu.alliance.audio.a.f.a.b("OAuthHelper", "getClientCredentialsToken json is empty.");
            return;
        } else
        {
            s = b(s);
            a(context, a("alliance_auth_access_token"), ((OAuthToken) (s)));
            return;
        }
    }

    public static void a(Context context, String s, String s1, OAuthListener oauthlistener)
    {
        com.baidu.alliance.audio.a.d.a.a(new _cls1(context, s, s1, oauthlistener));
    }

    public static long b(Context context)
    {
        context = context.getSharedPreferences(a("alliance_auth_access_token"), 0);
        long l = context.getLong("save_time", -1L);
        long l1 = context.getLong("expire_time", -1L);
        if (l == -1L || l1 == -1L)
        {
            return -1L;
        } else
        {
            return (l + l1) - System.currentTimeMillis() / 1000L;
        }
    }

    private static OAuthToken b(String s)
        throws OAuthException
    {
        if (f.a(s))
        {
            return null;
        }
        OAuthToken oauthtoken = new OAuthToken();
        try
        {
            s = new JSONObject(s);
            if (s.has("error_code"))
            {
                throw new OAuthException(s.getInt("error_code"), s.getString("error_message"));
            }
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return null;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return null;
        }
        oauthtoken.setExpiresTime(Long.parseLong(s.getString("expires_in")));
        oauthtoken.setAccessToken(s.getString("access_token"));
        return oauthtoken;
    }

    private static OAuthToken c(Context context)
    {
        OAuthToken oauthtoken = new OAuthToken();
        context = context.getSharedPreferences(a("alliance_auth_access_token"), 0);
        oauthtoken.setAccessToken(context.getString("access_token", ""));
        oauthtoken.setExpiresTime(context.getLong("expire_time", 0L));
        return oauthtoken;
    }

    private static String c(String s)
        throws SSLPeerUnverifiedException
    {
        g g1;
        HttpGet httpget;
        com.baidu.alliance.audio.a.f.a.b("OAuthHelper", s);
        g1 = g.a();
        httpget = new HttpGet(s);
        int i;
        com.baidu.alliance.audio.a.f.a.b("ExecuteOAuthRequest", s);
        s = g1.execute(httpget);
        i = s.getStatusLine().getStatusCode();
        com.baidu.alliance.audio.a.f.a.b("ExecuteOAuthRequest", (new StringBuilder("response.getStatusLine().getStatusCode() : ")).append(i).toString());
        if (i != 200 && i != 400) goto _L2; else goto _L1
_L1:
        s = EntityUtils.toString(s.getEntity());
_L4:
        if (s != null)
        {
            com.baidu.alliance.audio.a.f.a.b("ExecuteOAuthRequest", (new StringBuilder("Token Result: ")).append(s).toString());
        }
        return s;
        s;
        s.printStackTrace();
        s = null;
        continue; /* Loop/switch isn't completed */
        s;
        com.baidu.alliance.audio.a.f.a.b("OAuthHelper", (new StringBuilder("NumberFormatException : ")).append(s.getMessage().toString()).toString());
        s = null;
        continue; /* Loop/switch isn't completed */
        s;
        s.printStackTrace();
        throw s;
        s;
        s.printStackTrace();
_L2:
        s = null;
        if (true) goto _L4; else goto _L3
_L3:
    }

    private class _cls1 extends com.baidu.alliance.audio.a.d.b
    {

        private int a;
        private final Context b;
        private final String c;
        private final String d;
        private final OAuthListener e;

        public void a()
        {
            try
            {
                com.baidu.alliance.audio.logic.oauth.oauth.b.a(b, c, d);
                return;
            }
            catch (OAuthException oauthexception)
            {
                a = oauthexception.a();
            }
        }

        protected void d()
        {
            if (e != null)
            {
                e.onAuthorizeFinished(a, com.baidu.alliance.audio.logic.oauth.oauth.OAuthErrorCode.a(a));
            }
        }

        _cls1(Context context, String s, String s1, OAuthListener oauthlistener)
        {
            b = context;
            c = s;
            d = s1;
            e = oauthlistener;
            super();
            a = 0;
        }
    }

}
