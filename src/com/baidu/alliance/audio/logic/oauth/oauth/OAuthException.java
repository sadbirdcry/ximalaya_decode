// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.oauth.oauth;

import com.baidu.alliance.audio.a.h.f;

// Referenced classes of package com.baidu.alliance.audio.logic.oauth.oauth:
//            OAuthErrorCode

public class OAuthException extends Exception
{

    private static final long serialVersionUID = 0xb0ffcb88c27f88aL;
    private int mErrorCode;
    private String mErrorMessage;

    public OAuthException(int i, String s)
    {
        super(s);
        mErrorCode = i;
        mErrorMessage = OAuthErrorCode.a(i);
        if (!f.a(s))
        {
            mErrorMessage = s;
        }
    }

    public int a()
    {
        return mErrorCode;
    }
}
