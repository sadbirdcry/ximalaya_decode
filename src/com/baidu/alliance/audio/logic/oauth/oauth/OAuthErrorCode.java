// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.oauth.oauth;


public class OAuthErrorCode extends Exception
{

    public static final int ERROR_CERTIFICATE_ERROR = -2;
    public static final int ERROR_EXPIRE = 22601;
    public static final int ERROR_ILLEGAL = 22602;
    public static final int ERROR_LOW_POWER = 22603;
    public static final int ERROR_PARAM_INVALID = 22605;
    public static final int ERROR_RATE_LIMIT = 22064;
    public static final int ERROR_UNKNOWN = -1;
    public static final int SUCCESS = 0;
    private static final long serialVersionUID = 0x8b5e74d244ca653aL;

    public OAuthErrorCode()
    {
    }

    public static String a(int i)
    {
        switch (i)
        {
        default:
            return (new StringBuilder("[")).append(i).append("]-\u672A\u77E5\u9519\u8BEF").toString();

        case 22601: 
            return "Token\u8FC7\u671F";

        case 22602: 
            return "\u975E\u6CD5Token";

        case 22603: 
            return "\u6CA1\u6709\u6743\u9650\u8BBF\u95EE";

        case 22064: 
            return "\u8BBF\u95EE\u8D85\u8FC7\u9891\u7387\u9650\u5236";

        case 22605: 
            return "\u53C2\u6570\u6821\u9A8C\u5931\u8D25";

        case 0: // '\0'
            return "\u6388\u6743\u6210\u529F";
        }
    }
}
