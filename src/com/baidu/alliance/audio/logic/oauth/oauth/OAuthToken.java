// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.oauth.oauth;


public class OAuthToken
{

    private String mAccessToken;
    private long mExpiresTime;

    public OAuthToken()
    {
    }

    public String getAccessToken()
    {
        return mAccessToken;
    }

    public long getExpiresTime()
    {
        return mExpiresTime;
    }

    protected void setAccessToken(String s)
    {
        mAccessToken = s;
    }

    protected void setExpiresTime(long l)
    {
        mExpiresTime = l;
    }

    public String toString()
    {
        return (new StringBuilder("OAuthToken: \nexpired_Time: ")).append(mExpiresTime).append("\n").append("access_token: ").append(mAccessToken).toString();
    }
}
