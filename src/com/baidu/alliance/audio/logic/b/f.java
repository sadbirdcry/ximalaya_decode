// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.b;

import com.baidu.alliance.audio.a.f.a;
import com.baidu.alliance.audio.a.g.b;
import com.baidu.alliance.audio.logic.api.model.AdInfo;
import java.util.ArrayList;

// Referenced classes of package com.baidu.alliance.audio.logic.b:
//            c, d, l, i, 
//            g, h

public class f
{
    public static class a
    {

        public boolean a;
        public long b;
        public long c;
        public boolean d;

        public a()
        {
            a = false;
            b = 0L;
            c = 0L;
            d = false;
        }
    }


    private static ArrayList l;
    private static f m;
    public int a;
    public int b;
    private AdInfo c;
    private long d;
    private long e;
    private long f;
    private int g;
    private long h;
    private long i;
    private boolean j;
    private a k;

    public f()
    {
        a = 1;
        b = 0;
        d = 0L;
        e = 0L;
        f = 0L;
        g = 0;
        h = 0L;
        i = 0L;
        j = false;
    }

    private c a(AdInfo adinfo, a a1, boolean flag)
    {
        if (adinfo == null || a1 == null)
        {
            return null;
        }
        c c1 = new c();
        c1.f = true;
        c1.b = adinfo.id;
        if (flag)
        {
            g = (int)(i - h);
        } else
        {
            g = -1;
        }
        c1.c = g;
        c1.e = a1.b;
        return c1;
    }

    private d a(a a1)
    {
        if (a1 == null)
        {
            return null;
        }
        d d1 = new d();
        d1.f = true;
        d1.b = c.id;
        long l1;
        if (a1.d)
        {
            l1 = a1.c - a1.b;
        } else
        {
            l1 = -1L;
        }
        d1.c = (int)l1;
        d1.e = a1.b;
        return d1;
    }

    private void a(b b1)
    {
        if (b1 != null)
        {
            com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", (new StringBuilder("reportPerformanceMonitorLog <===> ")).append(b1.getClass().getName()).toString());
        }
        com.baidu.alliance.audio.logic.b.l.a().d(b1);
    }

    private i b(a a1)
    {
        if (a1 == null || c == null)
        {
            return null;
        } else
        {
            i i1 = new i();
            i1.f = true;
            i1.b = c.id;
            i1.e = a1.b;
            return i1;
        }
    }

    private void b(b b1)
    {
        a(b1);
    }

    private g c(a a1)
    {
        if (a1 == null || c == null)
        {
            return null;
        } else
        {
            g g1 = new g();
            g1.f = true;
            g1.b = c.id;
            g1.e = a1.b;
            return g1;
        }
    }

    private void c(b b1)
    {
        a(b1);
    }

    public static f d()
    {
        if (m == null)
        {
            m = new f();
        }
        return m;
    }

    private h d(a a1)
    {
        if (a1 == null || c == null)
        {
            return null;
        }
        h h1 = new h();
        h1.f = true;
        h1.b = c.id;
        b = (int)(f - d);
        if (0L == f || 0L == d)
        {
            b = -1;
        }
        h1.c = b;
        h1.h = a;
        h1.e = a1.b;
        return h1;
    }

    private void d(b b1)
    {
        a(b1);
    }

    private void e()
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "allPerformanceLogReportEnd");
        f();
    }

    private void e(b b1)
    {
        a(b1);
    }

    private void f()
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "resetCurrentAdValues");
        a = 1;
        b = 0;
        d = 0L;
        e = 0L;
        f = 0L;
        g = 0;
        h = 0L;
        i = 0L;
        j = false;
        k = null;
        c = null;
    }

    private void f(b b1)
    {
        if (g())
        {
            a(b1);
        }
    }

    private boolean g()
    {
        if (c == null)
        {
            return false;
        } else
        {
            return c.showDisplayer();
        }
    }

    public void a()
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "onPlayStart");
        c(b(k));
    }

    public void a(AdInfo adinfo)
    {
        StringBuilder stringbuilder = new StringBuilder("onMonitoringAdvertisement set ad data <===> ");
        boolean flag;
        if (adinfo == null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", stringbuilder.append(flag).toString());
        c = adinfo;
        b(a(k));
    }

    public void a(String s, a a1)
    {
        boolean flag = l.contains(s);
        boolean flag1 = com.baidu.alliance.audio.logic.api.a.c();
        if (a1 != null && !com.baidu.alliance.audio.a.h.f.a(s) && a1.a && flag && flag1)
        {
            com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", (new StringBuilder("onResponsed <===> ")).append(s).toString());
            if ("material".equalsIgnoreCase(s))
            {
                k = a1;
                return;
            }
        }
    }

    public void a(boolean flag)
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", (new StringBuilder("onPlayEnd, byUser <===> ")).append(flag).toString());
        if (!j)
        {
            f(a(c, k, false));
            j = true;
        }
        e(d(k));
        e();
    }

    public void b()
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "onPlayDuration");
        d(c(k));
    }

    public void b(AdInfo adinfo)
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "onL2PRequest");
        d = System.currentTimeMillis();
    }

    public void c()
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "onBuffct");
        a = a + 1;
    }

    public void c(AdInfo adinfo)
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "onL2PResponse");
        e = System.currentTimeMillis();
    }

    public void d(AdInfo adinfo)
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "onL2PSpeaker");
        f = System.currentTimeMillis();
    }

    public void e(AdInfo adinfo)
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "onPic2LoadRequest");
        h = System.currentTimeMillis();
    }

    public void f(AdInfo adinfo)
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "onPic2LoadResult");
        i = System.currentTimeMillis();
        f(((b) (a(adinfo, k, true))));
        j = true;
    }

    public void g(AdInfo adinfo)
    {
        com.baidu.alliance.audio.a.f.a.b("LOG_PERFORMANCE", "onPic2LoadFailed");
        f(a(adinfo, k, false));
        j = true;
    }

    static 
    {
        l = new ArrayList();
        l.add("material");
    }
}
