// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.b;

import android.os.Environment;
import android.text.format.DateFormat;
import com.baidu.alliance.audio.ContextManager;
import com.baidu.alliance.audio.SDKLogUtils;
import com.baidu.alliance.audio.a.c.e;
import com.baidu.alliance.audio.a.f.a;
import com.baidu.alliance.audio.a.g.b;
import com.baidu.alliance.audio.a.h.f;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

// Referenced classes of package com.baidu.alliance.audio.logic.b:
//            b

public class k
{
    class a extends Thread
    {

        final k a;
        private boolean b;

        public void a()
        {
            b = true;
        }

        public void run()
        {
            do
            {
                do
                {
                    if (b)
                    {
                        return;
                    }
                    b b1;
                    try
                    {
                        b1 = (b)com.baidu.alliance.audio.logic.b.k.a(a).take();
                    }
                    catch (InterruptedException interruptedexception)
                    {
                        com.baidu.alliance.audio.a.f.a.a("LOG_ReportManager", "SendLogThread interrupted", interruptedexception);
                        interruptedexception = null;
                    }
                } while (b1 == null);
                a.b(b1);
            } while (true);
        }

        public a()
        {
            a = k.this;
            super("StatisticsThread");
            b = false;
        }
    }


    public static boolean a = false;
    private static long e = 0L;
    private static long f = -1L;
    private LinkedBlockingQueue b;
    private a c;
    private int d;
    private boolean g;

    public k()
    {
        b = new LinkedBlockingQueue();
        d = -1;
        g = false;
        c = new a();
        c.start();
    }

    static LinkedBlockingQueue a(k k1)
    {
        return k1.b;
    }

    private static void a(String s, String s1, String s2)
    {
        if (a) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        com.baidu.alliance.audio.a.f.a.b("LOG_ReportManager", (new StringBuilder("writeToSdcard: action ")).append(s2).append(" url:>>").append(s1).toString());
        obj = new File(Environment.getExternalStorageDirectory(), s);
        s = null;
        if (!((File) (obj)).exists())
        {
            ((File) (obj)).createNewFile();
        }
        obj = new FileOutputStream(((File) (obj)), true);
        ((FileOutputStream) (obj)).write(DateFormat.format("yyyy-MM-dd kk:mm:ss", System.currentTimeMillis()).toString().getBytes());
        ((FileOutputStream) (obj)).write((new StringBuilder("  ")).append(s2).toString().getBytes());
        ((FileOutputStream) (obj)).write("\n".getBytes());
        ((FileOutputStream) (obj)).write(s1.getBytes());
        ((FileOutputStream) (obj)).write("\n".getBytes());
        if (obj != null)
        {
            try
            {
                ((FileOutputStream) (obj)).close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        s1;
_L7:
        com.baidu.alliance.audio.a.f.a.b("LOG_ReportManager", "writeToSdcard,IOException");
        if (s == null) goto _L1; else goto _L4
_L4:
        try
        {
            s.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
        return;
        s;
        s1 = null;
_L6:
        if (s1 != null)
        {
            try
            {
                s1.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s1)
            {
                s1.printStackTrace();
            }
        }
        throw s;
        s;
        s1 = ((String) (obj));
        continue; /* Loop/switch isn't completed */
        s2;
        s1 = s;
        s = s2;
        if (true) goto _L6; else goto _L5
_L5:
        s;
        s = ((String) (obj));
          goto _L7
    }

    public static boolean a(String s)
    {
        if (com.baidu.alliance.audio.a.h.f.a(s))
        {
            return true;
        }
        s = (new e(s.replace(" ", "%20").replace(">", "%3E").replace("|", "%7C"))).b();
        if (s != null && s.a())
        {
            SDKLogUtils.d("LOG_ReportManager", (new StringBuilder("sendLogSync, \u53D1\u9001\u7EDF\u8BA1\u6570\u636E--\u6210\u529F, HTTP StatusCode = ")).append(s.b()).toString());
            return true;
        } else
        {
            SDKLogUtils.d("LOG_ReportManager", (new StringBuilder("sendLogSync, \u53D1\u9001\u7EDF\u8BA1\u6570\u636E--\u5931\u8D25, HTTP StatusCode = ")).append(s.b()).toString());
            return false;
        }
    }

    private void c(b b1)
    {
        if (b1 == null)
        {
            return;
        }
        String s = b1.b();
        String s1 = b1.a();
        b1 = (com.baidu.alliance.audio.logic.b.b)b1;
        b1.g = ((com.baidu.alliance.audio.logic.b.b) (b1)).g + 1;
        if (((com.baidu.alliance.audio.logic.b.b) (b1)).g > 5)
        {
            com.baidu.alliance.audio.a.f.a.b("LOG_ReportManager", (new StringBuilder("+++prevent dealFailedLog : retryTimes ")).append(((com.baidu.alliance.audio.logic.b.b) (b1)).g).append(", action ").append(s).append(", url: ").append(s1).toString());
            return;
        } else
        {
            a(b1);
            com.baidu.alliance.audio.a.f.a.b("LOG_ReportManager", (new StringBuilder("+++dealFailedLog : retryTimes ")).append(((com.baidu.alliance.audio.logic.b.b) (b1)).g).append(", action ").append(s).append(", url: ").append(s1).toString());
            return;
        }
    }

    public void a()
    {
        a = false;
        c.a();
        b();
    }

    public void a(b b1)
    {
        if (b1 == null)
        {
            return;
        }
        try
        {
            b.put(b1);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (b b1)
        {
            com.baidu.alliance.audio.a.f.a.a("LOG_ReportManager", "putIntoSendLogQueue error", b1);
        }
    }

    public void b()
    {
        if (b.isEmpty())
        {
            return;
        }
        b b1;
        while ((b1 = (b)b.poll()) != null) 
        {
            b(b1);
        }
    }

    public void b(b b1)
    {
        String s;
        String s1 = b1.b();
        s = b1.a();
        SDKLogUtils.d("LOG_ReportManager", (new StringBuilder("sendLogSync, action code: ")).append(s1).append(", url: ").append(s).toString());
        while (!com.baidu.alliance.audio.a.h.e.a(ContextManager.getContext()) || com.baidu.alliance.audio.a.h.f.a(s)) 
        {
            return;
        }
        try
        {
            a("alliance_log_url_start.txt", s, b1.b());
            if (a(s))
            {
                a("alliance_log_url_end.txt", (new StringBuilder("success: ")).append(s).toString(), b1.b());
                return;
            }
        }
        catch (Exception exception)
        {
            SDKLogUtils.e("LOG_ReportManager", "sendLogSync error", exception);
            a("alliance_log_url_end.txt", (new StringBuilder("exception: ")).append(s).toString(), b1.b());
            return;
        }
        c(b1);
        a("alliance_log_url_end.txt", (new StringBuilder("fail: ")).append(s).toString(), b1.b());
        return;
    }

}
