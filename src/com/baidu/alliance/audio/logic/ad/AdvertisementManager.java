// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.ad;

import android.view.View;
import com.baidu.alliance.audio.SDKManager;
import com.baidu.alliance.audio.a.h.f;
import com.baidu.alliance.audio.logic.api.b;
import com.baidu.alliance.audio.logic.api.model.AdInfo;
import com.baidu.alliance.audio.logic.oauth.OAuthManager;
import com.baidu.alliance.audio.logic.timer.DisplayerListener;
import com.baidu.alliance.audio.logic.timer.OnCompletionListener;
import com.baidu.alliance.audio.logic.timer.a;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.baidu.alliance.audio.logic.ad:
//            AdvertisementResultCallback, AdvertisementOperateCallback

public class AdvertisementManager
{

    public static final String TAG = com/baidu/alliance/audio/logic/ad/AdvertisementManager.getSimpleName();
    private static AdvertisementManager instance;
    private Class clazzWebAdvertisement;
    private boolean isComplex;
    private String key_web_online_url;
    private View mAdDisplayView;
    private a mDisplayTimer;
    private AdvertisementResultCallback mDisplayerResultCallback;
    private int mLastAdId;
    private ArrayList mOnCompletionListeners;
    private AdvertisementOperateCallback mOperateCallback;
    private ArrayList mResultCallbacks;
    private DisplayerListener mTimerListener;

    public AdvertisementManager()
    {
        isComplex = false;
        mLastAdId = -1;
        mResultCallbacks = new ArrayList();
        mOnCompletionListeners = new ArrayList();
        key_web_online_url = "web_advertisement_online_url";
    }

    private List cleanDisplayContentDuration(List list)
    {
        int j = list.size();
        int i = 0;
        do
        {
            if (i >= j)
            {
                return list;
            }
            if (i != j - 1)
            {
                AdInfo adinfo = (AdInfo)list.get(i);
                if (adinfo != null && adinfo.displayContent != null)
                {
                    adinfo.displayContent.duration = 0;
                }
            }
            i++;
        } while (true);
    }

    public static AdvertisementManager getInstance()
    {
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/baidu/alliance/audio/logic/ad/AdvertisementManager;
        JVM INSTR monitorenter ;
        instance = new AdvertisementManager();
        com/baidu/alliance/audio/logic/ad/AdvertisementManager;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        Exception exception;
        exception;
        com/baidu/alliance/audio/logic/ad/AdvertisementManager;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void notifyDisplayerOnError(int i, String s)
    {
        if (mDisplayerResultCallback != null)
        {
            mDisplayerResultCallback.onResultError(i, s);
        }
    }

    private void notifyDisplayerOnResult(List list)
    {
        if (mDisplayerResultCallback != null)
        {
            mDisplayerResultCallback.onResultCallback(list);
        }
    }

    private void notifyOnError(int i, String s)
    {
        notifyDisplayerOnError(i, s);
        if (mResultCallbacks == null || mResultCallbacks.isEmpty()) goto _L2; else goto _L1
_L1:
        Iterator iterator = mResultCallbacks.iterator();
_L5:
        if (iterator.hasNext()) goto _L3; else goto _L2
_L2:
        return;
_L3:
        ((AdvertisementResultCallback)iterator.next()).onResultError(i, s);
        if (true) goto _L5; else goto _L4
_L4:
    }

    private void notifyOnResult(List list)
    {
        Iterator iterator;
        if (list != null && list.size() > 1)
        {
            isComplex = true;
            list = cleanDisplayContentDuration(list);
            mLastAdId = ((AdInfo)list.get(list.size() - 1)).id;
        } else
        {
            isComplex = false;
            mLastAdId = -1;
        }
        notifyDisplayerOnResult(list);
        if (mResultCallbacks == null || mResultCallbacks.isEmpty()) goto _L2; else goto _L1
_L1:
        iterator = mResultCallbacks.iterator();
_L5:
        if (iterator.hasNext()) goto _L3; else goto _L2
_L2:
        return;
_L3:
        ((AdvertisementResultCallback)iterator.next()).onResultCallback(list);
        if (true) goto _L5; else goto _L4
_L4:
    }

    private void requestAdvertisement(String s)
    {
        b.a(SDKManager.getInstance().getOAuthManager().getAccessToken(), s, new _cls1());
    }

    public void addAdvertisementCallbackListener(AdvertisementResultCallback advertisementresultcallback)
    {
        if (advertisementresultcallback != null && !mResultCallbacks.contains(advertisementresultcallback))
        {
            mResultCallbacks.add(advertisementresultcallback);
        }
    }

    public void addAdvertisementOnCompletionListener(OnCompletionListener oncompletionlistener)
    {
        if (oncompletionlistener != null && !mOnCompletionListeners.contains(oncompletionlistener))
        {
            mOnCompletionListeners.add(oncompletionlistener);
        }
    }

    public void dismissAdvertisement()
    {
        if (mAdDisplayView != null)
        {
            mAdDisplayView.setVisibility(8);
        }
    }

    public void displayAdvertisement(AdInfo adinfo)
    {
        if (mDisplayTimer != null)
        {
            if (!mDisplayTimer.d())
            {
                mDisplayTimer.e();
            }
            mDisplayTimer = null;
        }
        mDisplayTimer = new a();
        mDisplayTimer.a(mAdDisplayView);
        mDisplayTimer.a(mTimerListener);
        mDisplayTimer.a(mOnCompletionListeners);
        mDisplayTimer.a(adinfo);
        mDisplayTimer.a();
    }

    public Class getWebAdvertisementClass()
    {
        return clazzWebAdvertisement;
    }

    public String getWebAdvertisementURLKey()
    {
        return key_web_online_url;
    }

    public boolean isComplex()
    {
        return isComplex;
    }

    public boolean isLast(AdInfo adinfo)
    {
        return mLastAdId == adinfo.id;
    }

    public void notifyAdvertisementCleanCallback()
    {
        if (mOperateCallback != null)
        {
            mOperateCallback.onClearAdvertisementCallback();
        }
    }

    public void notifyAdvertisementPauseCallback()
    {
        if (mOperateCallback != null)
        {
            mOperateCallback.onPauseAdvertisementCallback();
        }
    }

    public void notifyAdvertisementResumeCallback()
    {
        if (mOperateCallback != null)
        {
            mOperateCallback.onResumeAdvertisementCallback();
        }
    }

    public void pauseAdvertisement()
    {
        if (mDisplayTimer != null)
        {
            mDisplayTimer.b();
        }
    }

    public void release()
    {
        if (mDisplayTimer != null)
        {
            mDisplayTimer.e();
            mDisplayTimer = null;
        }
        if (mAdDisplayView != null)
        {
            mAdDisplayView = null;
        }
        if (mTimerListener != null)
        {
            mTimerListener = null;
        }
        if (mDisplayerResultCallback != null)
        {
            mDisplayerResultCallback = null;
        }
        if (mResultCallbacks != null)
        {
            mResultCallbacks.clear();
        }
        if (mOnCompletionListeners != null)
        {
            mOnCompletionListeners.clear();
        }
        if (mOperateCallback != null)
        {
            mOperateCallback = null;
        }
        if (clazzWebAdvertisement != null)
        {
            clazzWebAdvertisement = null;
        }
        if (instance != null)
        {
            instance = null;
        }
    }

    public void removeAdvertisementCallbackListener(AdvertisementResultCallback advertisementresultcallback)
    {
        if (advertisementresultcallback != null && mResultCallbacks.contains(advertisementresultcallback))
        {
            mResultCallbacks.remove(advertisementresultcallback);
        }
    }

    public void removeAdvertisementOnCompletionListener(OnCompletionListener oncompletionlistener)
    {
        if (oncompletionlistener != null && mOnCompletionListeners.contains(oncompletionlistener))
        {
            mOnCompletionListeners.remove(oncompletionlistener);
        }
    }

    public void requestAdvertisement()
    {
        requestAdvertisement("0");
    }

    public void requestAdvertisementAudioOnly()
    {
        requestAdvertisement("1");
    }

    public void resumeAdvertisement()
    {
        if (mDisplayTimer != null)
        {
            mDisplayTimer.c();
        }
    }

    public void setAdvertisementDisplayerCallbackView(View view)
    {
        mAdDisplayView = view;
    }

    public void setAdvertisementDisplayerListener(DisplayerListener displayerlistener)
    {
        mTimerListener = displayerlistener;
    }

    public void setAdvertisementDisplayerResultCallback(AdvertisementResultCallback advertisementresultcallback)
    {
        mDisplayerResultCallback = advertisementresultcallback;
    }

    public void setAdvertisementOperateCallback(AdvertisementOperateCallback advertisementoperatecallback)
    {
        mOperateCallback = advertisementoperatecallback;
    }

    public void setWebAdvertisementClass(Class class1)
    {
        clazzWebAdvertisement = class1;
    }

    public void setWebAdvertisementURLKey(String s)
    {
        if (f.a(s))
        {
            throw new RuntimeException("WebAdvertisementURLKey is empty");
        } else
        {
            key_web_online_url = s;
            return;
        }
    }




    private class _cls1
        implements com.baidu.alliance.audio.logic.api.b.a
    {

        final AdvertisementManager this$0;

        public void onDateGet(AdResult adresult)
        {
            com.baidu.alliance.audio.a.f.a.b(AdvertisementManager.TAG, "onDateGet ");
            if (adresult == null || adresult.adInfos == null)
            {
                notifyOnResult(null);
                return;
            } else
            {
                notifyOnResult(adresult.adInfos);
                return;
            }
        }

        public volatile void onDateGet(BaseModel basemodel)
        {
            onDateGet((AdResult)basemodel);
        }

        public void onError(int i, String s)
        {
            com.baidu.alliance.audio.a.f.a.b(AdvertisementManager.TAG, (new StringBuilder("requestAdvertisement ")).append(i).append(", msg = ").append(s).toString());
            notifyOnError(i, s);
        }

        _cls1()
        {
            this$0 = AdvertisementManager.this;
            super();
        }
    }

}
