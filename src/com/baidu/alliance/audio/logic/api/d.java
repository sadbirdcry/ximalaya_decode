// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.api;

import android.content.Context;
import android.text.TextUtils;
import com.baidu.alliance.audio.ContextManager;
import com.baidu.alliance.audio.SDKManager;
import com.baidu.alliance.audio.a.f.a;
import com.baidu.alliance.audio.a.h.c;
import com.baidu.alliance.audio.a.h.e;
import com.baidu.alliance.audio.a.h.f;
import com.baidu.alliance.audio.logic.api.model.BaseModel;
import com.baidu.alliance.audio.logic.oauth.OAuthManager;
import java.util.HashMap;

// Referenced classes of package com.baidu.alliance.audio.logic.api:
//            a, c

public class d
{

    private com.baidu.alliance.audio.a.c.e a;
    private Context b;

    public d(String s, String s1)
    {
        a = null;
        b = ContextManager.getContext();
        a = b(s, s1);
    }

    private boolean a()
    {
        return e.a(b);
    }

    private com.baidu.alliance.audio.a.c.e b(String s, String s1)
    {
        String s2;
        com.baidu.alliance.audio.a.c.e e1;
        e1 = new com.baidu.alliance.audio.a.c.e(com.baidu.alliance.audio.logic.api.a.b().replace("{callremotemethod}", s1));
        e1.a(s1);
        e1.a("User-Agent", "");
        s2 = (new c(ContextManager.getContext())).b();
        s1 = s2;
        s = s2;
        if (TextUtils.isEmpty(s2))
        {
            break MISSING_BLOCK_LABEL_116;
        }
        s = s2;
        com.baidu.alliance.audio.a.f.a.b("Request", (new StringBuilder("deviceId encrypt before <===> ")).append(s2).toString());
        s = s2;
        s1 = com.baidu.alliance.audio.a.b.a.a("jsdYdkHelLbusnxi", "jsdYdkHelLbusnxi", s2);
        s = s1;
        try
        {
            com.baidu.alliance.audio.a.f.a.b("Request", (new StringBuilder("deviceId encrypt after <===> ")).append(s1).toString());
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            s1.printStackTrace();
            s1 = s;
        }
        s = com.baidu.alliance.audio.a.h.d.a();
        com.baidu.alliance.audio.a.f.a.b("Request", (new StringBuilder("cuid <===> ")).append(s).toString());
        e1.b("platform", "android");
        e1.b("version", "1.0.1");
        e1.b("cuid", s);
        e1.b("deviceid", s1);
        e1.b("ua", c.a());
        e1.b("net", (new StringBuilder(String.valueOf(e.b(ContextManager.getContext())))).toString());
        e1.b("conn_t_out", (new StringBuilder(String.valueOf(com.baidu.alliance.audio.a.c.d.a()))).toString());
        e1.b("read_t_out", (new StringBuilder(String.valueOf(com.baidu.alliance.audio.a.c.d.b()))).toString());
        s = SDKManager.getInstance().getOAuthManager().getAccessToken();
        if (!f.a(s))
        {
            e1.b("access_token", s);
        }
        return e1;
    }

    private String b()
    {
        Object obj = a.b();
        if (obj != null && ((com.baidu.alliance.audio.a.c.f) (obj)).a())
        {
            obj = ((com.baidu.alliance.audio.a.c.f) (obj)).d();
            com.baidu.alliance.audio.a.f.a.b("Request", (new StringBuilder("api json: ")).append(((String) (obj))).toString());
            return ((String) (obj));
        } else
        {
            return null;
        }
    }

    public d a(String s, String s1)
    {
        if (a != null)
        {
            a.b(s, s1);
        }
        return this;
    }

    public BaseModel a(BaseModel basemodel)
    {
        return a(basemodel, -1L);
    }

    public BaseModel a(BaseModel basemodel, long l)
    {
        Object obj;
        if (a == null || basemodel == null)
        {
            return null;
        }
        com.baidu.alliance.audio.a.f.a.b("Request", a.c());
        obj = b();
        if (f.a(((String) (obj))))
        {
            if (!a())
            {
                basemodel.errno = -5;
                basemodel.errmsg = (String)com.baidu.alliance.audio.logic.api.c.a.get(Integer.valueOf(basemodel.errno));
                return basemodel;
            } else
            {
                basemodel.errno = -3;
                basemodel.errmsg = (String)com.baidu.alliance.audio.logic.api.c.a.get(Integer.valueOf(basemodel.errno));
                return basemodel;
            }
        }
        com.baidu.alliance.audio.a.f.a.a("Request", (new StringBuilder("json: ")).append(((String) (obj))).toString());
        obj = (BaseModel)com.baidu.alliance.audio.a.e.a.a(((String) (obj)), basemodel);
        basemodel = ((BaseModel) (obj));
_L2:
        return basemodel;
        Exception exception;
        exception;
        basemodel.errno = -1;
        com.baidu.alliance.audio.a.f.a.a("Request", exception);
        if (true) goto _L2; else goto _L1
_L1:
    }
}
