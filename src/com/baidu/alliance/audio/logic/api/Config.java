// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.api;


public class Config
{

    public static final int ENVIRONMENT_ONLINE = 0;
    public static final int ENVIRONMENT_SANDBOX = 1;
    public static final int ROLE_QA = 2;
    public static final int ROLE_RD = 1;
    public static final int ROLE_USER = 0;
    public static int sGlobalEnv = 0;
    public static int sGlobalRole = 0;

    public Config()
    {
    }

    public static int getGlobalEnvironment()
    {
        return sGlobalEnv;
    }

    public static int getGlobalRole()
    {
        return sGlobalRole;
    }

    public static void resetGlobalConfig()
    {
        sGlobalRole = 0;
        sGlobalEnv = 0;
    }

    public static void setGlobalEnvironment(int i)
    {
        sGlobalEnv = i;
    }

    public static void setGlobalRole(int i)
    {
        sGlobalRole = i;
    }

}
