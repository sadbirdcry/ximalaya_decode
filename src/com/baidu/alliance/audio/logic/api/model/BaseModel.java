// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.api.model;

import java.io.Serializable;

public class BaseModel
    implements Serializable
{

    private static final long serialVersionUID = 0x618bc1c1de9ba74L;
    public String errmsg;
    public int errno;

    public BaseModel()
    {
        errno = 22000;
        errmsg = "";
    }

    public boolean isAvailable()
    {
        return errno == 22000;
    }
}
