// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.api.model;

import java.io.Serializable;

public class AdInfo
    implements Serializable
{
    public static class DisplayContent
        implements Serializable
    {

        private static final long serialVersionUID = 0xe84db85b90f76d42L;
        public String attachment;
        public int duration;
        public JumpMessage jumpMessage;
        public String message;
        public String phone;
        public int picHeight;
        public int picWidth;
        public String picture;
        public String webUrl;

        public DisplayContent()
        {
        }
    }

    public static class DisplayContent.JumpMessage
        implements Serializable
    {

        private static final long serialVersionUID = 0xcd95e938fb005eaeL;
        public String albumId;
        public String artistId;
        public String songId;
        public int tpye;
        public String webUrl;

        public DisplayContent.JumpMessage()
        {
        }
    }


    public static final int ACTION_CLICK = 1;
    public static final int ACTION_PURE_AUDIO = 2;
    public static final int DISPLAY_TYPE_PURE_AUDIO = 0;
    public static final int DISPLAY_TYPE_SMS = 5;
    public static final int DISPLAY_TYPE_TEL = 4;
    public static final int DISPLAY_TYPE_WEB = 2;
    private static final long serialVersionUID = 1L;
    public int audioBitrate;
    public int audioDuration;
    public String audioPicture;
    public long audioSize;
    public String audioType;
    public String audioUrl;
    public DisplayContent displayContent;
    public int displayType;
    public int id;
    public String info;
    public String linkUrl;
    public String stats_ps;
    public int stats_pt;
    public String type;

    public AdInfo()
    {
        displayType = -1;
        stats_ps = "2";
        stats_pt = 0;
    }

    public boolean showDisplayer()
    {
        boolean flag;
        boolean flag1;
        if (-1 != displayType)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (displayType != 0)
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        return flag && flag1;
    }
}
