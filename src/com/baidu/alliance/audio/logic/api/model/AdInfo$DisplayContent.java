// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.api.model;

import java.io.Serializable;

// Referenced classes of package com.baidu.alliance.audio.logic.api.model:
//            AdInfo

public static class JumpMessage
    implements Serializable
{
    public static class JumpMessage
        implements Serializable
    {

        private static final long serialVersionUID = 0xcd95e938fb005eaeL;
        public String albumId;
        public String artistId;
        public String songId;
        public int tpye;
        public String webUrl;

        public JumpMessage()
        {
        }
    }


    private static final long serialVersionUID = 0xe84db85b90f76d42L;
    public String attachment;
    public int duration;
    public JumpMessage jumpMessage;
    public String message;
    public String phone;
    public int picHeight;
    public int picWidth;
    public String picture;
    public String webUrl;

    public JumpMessage()
    {
    }
}
