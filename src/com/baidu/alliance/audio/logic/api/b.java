// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.api;

import com.baidu.alliance.audio.a.d.a;
import com.baidu.alliance.audio.logic.api.model.BaseModel;

public class com.baidu.alliance.audio.logic.api.b
{
    public static interface a
        extends b
    {

        public abstract void onDateGet(BaseModel basemodel);
    }

    public static interface b
    {

        public abstract void onError(int i, String s);
    }

    public static interface c
    {

        public abstract BaseModel b();
    }


    public static com.baidu.alliance.audio.a.d.b a(c c1, a a1)
    {
        c1 = new _cls1(c1, a1);
        com.baidu.alliance.audio.a.d.a.a(c1);
        return c1;
    }

    public static com.baidu.alliance.audio.a.d.b a(String s, String s1, a a1)
    {
        return a(((c) (new _cls2(s, s1))), a1);
    }

    private class _cls1 extends com.baidu.alliance.audio.a.d.b
    {

        BaseModel a;
        private final c b;
        private final a c;

        private void a(a a1, int i, String s)
        {
            StringBuilder stringbuilder = new StringBuilder("");
            stringbuilder.append((new StringBuilder("[ErrorCode = ")).append(i).append(", ").toString());
            if (com.baidu.alliance.audio.logic.api.c.a.containsKey(Integer.valueOf(i)))
            {
                stringbuilder.append((new StringBuilder("ErrorMsg = ")).append((String)com.baidu.alliance.audio.logic.api.c.a.get(Integer.valueOf(i))).toString());
                a1.onError(i, (String)com.baidu.alliance.audio.logic.api.c.a.get(Integer.valueOf(i)));
            } else
            {
                stringbuilder.append((new StringBuilder("ErrorMsg = ")).append(s).toString());
                a1.onError(i, s);
            }
            com.baidu.alliance.audio.a.f.a.c("Job", stringbuilder.append("]").toString());
        }

        protected void a()
        {
            a = b.b();
        }

        protected void d()
        {
label0:
            {
label1:
                {
                    if (c != null)
                    {
                        if (a != null)
                        {
                            break label0;
                        }
                        if (e.a(ContextManager.getContext()))
                        {
                            break label1;
                        }
                        a(c, -5, null);
                    }
                    return;
                }
                a(c, -3, null);
                return;
            }
            try
            {
                a.errmsg = URLDecoder.decode(a.errmsg, "UTF-8");
            }
            catch (UnsupportedEncodingException unsupportedencodingexception)
            {
                a.errmsg = "error";
            }
            if (a.isAvailable())
            {
                try
                {
                    com.baidu.alliance.audio.a.f.a.c("Job", (new StringBuilder("[ErrorCode = ")).append(a.errno).append(", ErrorMsg = ").append(a.errmsg).append("]").toString());
                    c.onDateGet(a);
                    return;
                }
                catch (Exception exception)
                {
                    com.baidu.alliance.audio.a.f.a.a("Job", exception);
                }
                return;
            } else
            {
                a(c, a.errno, a.errmsg);
                return;
            }
        }

        _cls1(c c1, a a1)
        {
            b = c1;
            c = a1;
            super();
            a = null;
        }
    }


    private class _cls2
        implements c
    {

        private final String a;
        private final String b;

        public AdResult a()
        {
            d d1 = new d("", "material");
            d1.a("access_token", a);
            String s;
            if (TextUtils.isEmpty(b))
            {
                s = "0";
            } else
            {
                s = b;
            }
            d1.a("audio_only", s);
            return (AdResult)d1.a(new AdResult());
        }

        public BaseModel b()
        {
            return a();
        }

        _cls2(String s, String s1)
        {
            a = s;
            b = s1;
            super();
        }
    }

}
