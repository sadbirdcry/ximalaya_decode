// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.timer;

import com.baidu.alliance.audio.logic.api.model.AdInfo;

public interface DisplayerListener
{

    public abstract void onAudioFinish();

    public abstract void onDismiss();

    public abstract void onDisplay(AdInfo adinfo);

    public abstract void onDisplayPaused(AdInfo adinfo);

    public abstract void onDisplayResumed(AdInfo adinfo);

    public abstract void onFinish();

    public abstract void onHangFinish();

    public abstract void onUpdate(int i);
}
