// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.timer;

import android.os.Handler;

public abstract class b
{

    private final long a;
    private final long b;
    private long c;
    private boolean d;
    private Handler e;

    public b(long l, long l1)
    {
        d = false;
        e = new _cls1();
        a = l;
        b = l1;
    }

    static long a(b b1)
    {
        return b1.b;
    }

    static void a(b b1, long l)
    {
        b1.c = l;
    }

    static long b(b b1)
    {
        return b1.c;
    }

    static boolean c(b b1)
    {
        return b1.d;
    }

    public abstract void a();

    public abstract void a(long l);

    public final void b()
    {
        e.removeMessages(1);
    }

    public abstract void b(long l);

    public final void c()
    {
        d = true;
    }

    public final void d()
    {
        d = false;
    }

    public final b e()
    {
        this;
        JVM INSTR monitorenter ;
        if (a > 0L) goto _L2; else goto _L1
_L1:
        a();
_L4:
        this;
        JVM INSTR monitorexit ;
        return this;
_L2:
        c = a;
        e.sendMessage(e.obtainMessage(1));
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    private class _cls1 extends Handler
    {

        final b a;

        public void handleMessage(Message message)
        {
            long l;
            long l1;
label0:
            {
                synchronized (a)
                {
                    l = b.a(a);
                    l1 = b.b(a);
                    if (!b.c(a))
                    {
                        break label0;
                    }
                    a.b(l1);
                    sendMessageDelayed(obtainMessage(1), l);
                }
                return;
            }
            if (l1 > 0L) goto _L2; else goto _L1
_L1:
            a.a();
_L3:
            b b1 = a;
            b.a(b1, b.b(b1) - b.a(a));
            message;
            JVM INSTR monitorexit ;
            return;
            exception;
            message;
            JVM INSTR monitorexit ;
            throw exception;
_L2:
label1:
            {
                if (l1 >= b.a(a))
                {
                    break label1;
                }
                sendMessageDelayed(obtainMessage(1), l1);
            }
              goto _L3
            a.a(l1);
            sendMessageDelayed(obtainMessage(1), l);
              goto _L3
        }

        _cls1()
        {
            a = b.this;
            super();
        }
    }

}
