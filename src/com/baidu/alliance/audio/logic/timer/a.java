// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio.logic.timer;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.baidu.alliance.audio.logic.ad.AdvertisementManager;
import com.baidu.alliance.audio.logic.api.model.AdInfo;
import java.util.ArrayList;
import java.util.Iterator;

// Referenced classes of package com.baidu.alliance.audio.logic.timer:
//            DisplayerListener, OnCompletionListener, b

public class a
{

    private boolean a;
    private b b;
    private DisplayerListener c;
    private View d;
    private ArrayList e;
    private AdInfo f;
    private Handler g;

    public a()
    {
        a = false;
        g = new Handler(Looper.getMainLooper());
    }

    static Handler a(a a1)
    {
        return a1.g;
    }

    private b a(int i1, long l1, long l2)
    {
        return new _cls1(l1 + l2 + (long)600, 1000L, l2);
    }

    private void a(int i1)
    {
        if (m() && c != null)
        {
            c.onUpdate(i1);
        }
    }

    static void a(a a1, int i1)
    {
        a1.a(i1);
    }

    private void b(long l1)
    {
        if (m() && l1 > 0L && c != null)
        {
            c.onHangFinish();
        }
    }

    static void b(a a1)
    {
        a1.i();
    }

    private void f()
    {
        if (m() && c != null)
        {
            c.onDisplay(f);
        }
    }

    private void g()
    {
        if (m() && c != null)
        {
            c.onDisplayPaused(f);
        }
    }

    private void h()
    {
        if (m() && c != null)
        {
            c.onDisplayResumed(f);
        }
    }

    private void i()
    {
        if (c != null)
        {
            c.onAudioFinish();
        }
    }

    private void j()
    {
        AdvertisementManager advertisementmanager = AdvertisementManager.getInstance();
        if (advertisementmanager.isComplex() && f != null)
        {
            if (!advertisementmanager.isLast(f));
        }
        boolean flag = m();
        if (!flag);
        if (flag && c != null)
        {
            c.onDismiss();
        }
    }

    private void k()
    {
        if (m() && c != null)
        {
            c.onFinish();
        }
    }

    private void l()
    {
        if (e == null || e.isEmpty()) goto _L2; else goto _L1
_L1:
        Iterator iterator = e.iterator();
_L5:
        if (iterator.hasNext()) goto _L3; else goto _L2
_L2:
        return;
_L3:
        ((OnCompletionListener)iterator.next()).onCompletion(f);
        if (true) goto _L5; else goto _L4
_L4:
    }

    private boolean m()
    {
        return f.showDisplayer();
    }

    public void a()
    {
        b.e();
        f();
    }

    public void a(long l1)
    {
        b(l1);
        j();
        k();
        l();
        a = true;
    }

    public void a(View view)
    {
        d = view;
    }

    public void a(AdInfo adinfo)
    {
        long l2 = 0L;
        f = adinfo;
        int i1 = adinfo.id;
        long l3 = (long)adinfo.audioDuration * 1L;
        long l4 = adinfo.displayContent.duration;
        long l1 = l3;
        if (l3 < 0L)
        {
            l1 = 0L;
        }
        if (l4 > 0L)
        {
            l2 = 1000L * l4;
        }
        b = a(i1, l1, l2);
    }

    public void a(DisplayerListener displayerlistener)
    {
        c = displayerlistener;
    }

    public void a(ArrayList arraylist)
    {
        e = arraylist;
    }

    public void b()
    {
        b.c();
        g();
    }

    public void c()
    {
        b.d();
        h();
    }

    public boolean d()
    {
        return a;
    }

    public void e()
    {
        a = true;
        com.baidu.alliance.audio.logic.c.a.b().a();
        if (b != null)
        {
            b.b();
            b = null;
        }
        if (c != null)
        {
            c = null;
        }
        if (e != null)
        {
            e = null;
        }
        if (d != null)
        {
            d.setVisibility(8);
            d = null;
        }
        if (g != null)
        {
            g = null;
        }
        if (f != null)
        {
            f = null;
        }
    }

    private class _cls1 extends b
    {

        final a a;
        private final long b;

        static a a(_cls1 _pcls1)
        {
            return _pcls1.a;
        }

        public void a()
        {
            a.a(b);
            a.e();
        }

        public void a(long l1)
        {
            int i1;
            int j1;
            long l2;
            if (b <= 0L)
            {
                l2 = 0L;
            } else
            {
                l2 = b / 1000L;
            }
            j1 = (int)l2;
            i1 = (int)(l1 / 1000L);
            if (b <= 0L)
            {
                a.a(a, i1);
                class _cls1
                    implements Runnable
                {

                    final _cls1 a;

                    public void run()
                    {
                        a.b(_cls1.a(a));
                    }

                _cls1()
                {
                    a = _cls1.this;
                    super();
                }
                }

                if (i1 == 1 && a.a(a) != null)
                {
                    a.a(a).postDelayed(new _cls1(), 900L);
                }
            } else
            {
                j1 = i1 - j1;
                if (j1 > 0)
                {
                    a.a(a, j1);
                }
                if ((long)i1 == b / 1000L && a.a(a) != null)
                {
                    class _cls2
                        implements Runnable
                    {

                        final _cls1 a;

                        public void run()
                        {
                            a.b(_cls1.a(a));
                        }

                _cls2()
                {
                    a = _cls1.this;
                    super();
                }
                    }

                    a.a(a).postDelayed(new _cls2(), 900L);
                    return;
                }
            }
        }

        public void b(long l1)
        {
        }

        _cls1(long l1, long l2, long l3)
        {
            a = a.this;
            b = l3;
            super(l1, l2);
        }
    }

}
