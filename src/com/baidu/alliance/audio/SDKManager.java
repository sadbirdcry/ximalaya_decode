// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio;

import com.baidu.alliance.audio.a.h.b;
import com.baidu.alliance.audio.logic.a.a;
import com.baidu.alliance.audio.logic.ad.AdvertisementManager;
import com.baidu.alliance.audio.logic.api.Config;
import com.baidu.alliance.audio.logic.b.f;
import com.baidu.alliance.audio.logic.b.j;
import com.baidu.alliance.audio.logic.b.l;
import com.baidu.alliance.audio.logic.oauth.OAuthManager;
import com.baidu.alliance.audio.logic.report.ReportManager;
import com.baidu.mobstat.SendStrategyEnum;
import com.baidu.mobstat.StatService;

// Referenced classes of package com.baidu.alliance.audio:
//            SDKConfiguration, ContextManager, SDKLogUtils

public class SDKManager
{

    private static SDKManager instance;
    private SDKConfiguration mConfiguration;
    private OAuthManager mOAuthManager;

    public SDKManager()
    {
    }

    public static SDKManager getInstance()
    {
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/baidu/alliance/audio/SDKManager;
        JVM INSTR monitorenter ;
        instance = new SDKManager();
        com/baidu/alliance/audio/SDKManager;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        Exception exception;
        exception;
        com/baidu/alliance/audio/SDKManager;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void initManagers()
    {
        AdvertisementManager.getInstance();
        a.b();
        ReportManager.getInstance();
        l.a();
        f.d();
        com.baidu.alliance.audio.logic.c.a.b();
    }

    private void initOAuthManger()
    {
        mOAuthManager = new OAuthManager();
        mOAuthManager.init(mConfiguration.mAppKey, mConfiguration.mAppSecretKey);
        mOAuthManager.authorize(new _cls1());
    }

    private void initStat()
    {
        StatService.setAppKey("3729ab3ede");
        StatService.setOn(mConfiguration.mContext, 1);
        StatService.setLogSenderDelayed(10);
        StatService.setSendLogStrategy(mConfiguration.mContext, SendStrategyEnum.APP_START, 1, false);
        StatService.onEvent(mConfiguration.getContext(), "start", "sdk_log");
    }

    private void reportSdkStartLog()
    {
        j j1 = new j();
        j1.f = true;
        l.a().c(j1);
    }

    public boolean environmentEditable()
    {
        return true;
    }

    public SDKConfiguration getConfiguration()
    {
        return mConfiguration;
    }

    public OAuthManager getOAuthManager()
    {
        return mOAuthManager;
    }

    public void init(SDKConfiguration sdkconfiguration)
    {
        mConfiguration = sdkconfiguration;
        ContextManager.init(sdkconfiguration.mContext);
        SDKLogUtils.setDebug(sdkconfiguration.mDebugMode);
        b.a();
        initManagers();
        initOAuthManger();
        reportSdkStartLog();
        initStat();
    }

    public void release()
    {
        try
        {
            Thread.sleep(160L);
        }
        catch (InterruptedException interruptedexception)
        {
            SDKLogUtils.d("LOG_SDKManager", "release slp ex.");
        }
        AdvertisementManager.getInstance().release();
        l.a().b();
        a.b().a();
        ReportManager.getInstance().release();
        com.baidu.alliance.audio.logic.c.a.b().c();
        resetEnvironment();
        if (mConfiguration != null)
        {
            mConfiguration = null;
        }
        if (instance != null)
        {
            instance = null;
        }
    }

    public void resetEnvironment()
    {
        Config.resetGlobalConfig();
    }

    public boolean roleEditable()
    {
        return false;
    }

    public boolean setEnvironment(int i)
    {
        Config.setGlobalEnvironment(i);
        return true;
    }

    public boolean setRole(int i)
    {
        com.baidu.alliance.audio.a.f.a.c("LOG_SDKManager", "\u89D2\u8272\u4E0D\u53EF\u4EE5\u5207\u6362");
        return false;
    }


    private class _cls1
        implements OAuthListener
    {

        final SDKManager a;

        public void onAuthorizeFinished(int i, String s)
        {
            SDKLogUtils.d("LOG_SDKManager", (new StringBuilder("authorize finished, status: ")).append(i).append(", msg: ").append(s).toString());
            a.mOAuthManager.updateToken();
        }

        _cls1()
        {
            a = SDKManager.this;
            super();
        }
    }

}
