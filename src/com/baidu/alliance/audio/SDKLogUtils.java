// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio;

import com.baidu.alliance.audio.a.f.b;

public class SDKLogUtils
{

    private static b mLogger = new b();

    public SDKLogUtils()
    {
    }

    public static void d(String s, String s1)
    {
        mLogger.b(s, s1);
    }

    public static void e(String s, String s1)
    {
        mLogger.e(s, s1);
    }

    public static void e(String s, String s1, Throwable throwable)
    {
        mLogger.a(s, s1, throwable);
    }

    public static void e(String s, Throwable throwable)
    {
        mLogger.a(s, "", throwable);
    }

    public static void i(String s, String s1)
    {
        mLogger.c(s, s1);
    }

    public static void setDebug(boolean flag)
    {
        mLogger.a(flag);
    }

    public static void v(String s, String s1)
    {
        mLogger.a(s, s1);
    }

    public static void w(String s, String s1)
    {
        mLogger.d(s, s1);
    }

}
