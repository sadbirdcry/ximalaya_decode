// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio;

import android.content.Context;
import com.baidu.alliance.audio.a.c.d;

public final class SDKConfiguration
{
    public static class Builder
    {

        private String mAppKey;
        private String mAppSecretKey;
        private int mConnTimeOut;
        private Context mContext;
        private boolean mDebugMode;
        private int mSoTimeout;

        public SDKConfiguration build()
        {
            return new SDKConfiguration(this, null);
        }

        public Builder setAppKey(String s)
        {
            mAppKey = s;
            return this;
        }

        public Builder setAppSecretKey(String s)
        {
            mAppSecretKey = s;
            return this;
        }

        public Builder setContext(Context context)
        {
            mContext = context;
            return this;
        }

        public Builder setDebugMode(boolean flag)
        {
            mDebugMode = flag;
            return this;
        }

        public Builder setHttpConnectionTimeout(int i)
        {
            mConnTimeOut = i;
            return this;
        }

        public Builder setHttpReadTimeout(int i)
        {
            mSoTimeout = i;
            return this;
        }







        public Builder()
        {
            mDebugMode = false;
            mConnTimeOut = 30;
            mSoTimeout = 30;
        }
    }


    final String mAppKey;
    final String mAppSecretKey;
    final int mConnTimeOut;
    final Context mContext;
    final boolean mDebugMode;
    final int mSoTimeout;

    private SDKConfiguration(Builder builder)
    {
        mDebugMode = builder.mDebugMode;
        mContext = builder.mContext;
        mAppKey = builder.mAppKey;
        mAppSecretKey = builder.mAppSecretKey;
        mConnTimeOut = builder.mConnTimeOut;
        mSoTimeout = builder.mSoTimeout;
        d.a(mConnTimeOut);
        d.b(mSoTimeout);
    }

    SDKConfiguration(Builder builder, SDKConfiguration sdkconfiguration)
    {
        this(builder);
    }

    public Context getContext()
    {
        return mContext;
    }
}
