// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio;

import android.os.Handler;
import android.os.Looper;

public class SDKToastUtil
{

    public SDKToastUtil()
    {
    }

    public static void showLongToast(int i)
    {
        (new Handler(Looper.getMainLooper())).post(new _cls4(i));
    }

    public static void showLongToast(String s)
    {
        (new Handler(Looper.getMainLooper())).post(new _cls2(s));
    }

    public static void showShortToast(int i)
    {
        (new Handler(Looper.getMainLooper())).post(new _cls3(i));
    }

    public static void showShortToast(String s)
    {
        (new Handler(Looper.getMainLooper())).post(new _cls1(s));
    }

    private class _cls4
        implements Runnable
    {

        private final int a;

        public void run()
        {
            Toast.makeText(ContextManager.getContext(), a, 1).show();
        }

        _cls4(int i)
        {
            a = i;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        private final String a;

        public void run()
        {
            Toast.makeText(ContextManager.getContext(), a, 1).show();
        }

        _cls2(String s)
        {
            a = s;
            super();
        }
    }


    private class _cls3
        implements Runnable
    {

        private final int a;

        public void run()
        {
            Toast.makeText(ContextManager.getContext(), a, 0).show();
        }

        _cls3(int i)
        {
            a = i;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        private final String a;

        public void run()
        {
            Toast.makeText(ContextManager.getContext(), a, 0).show();
        }

        _cls1(String s)
        {
            a = s;
            super();
        }
    }

}
