// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.alliance.audio;

import android.content.Context;

// Referenced classes of package com.baidu.alliance.audio:
//            SDKConfiguration

public static class mSoTimeout
{

    private String mAppKey;
    private String mAppSecretKey;
    private int mConnTimeOut;
    private Context mContext;
    private boolean mDebugMode;
    private int mSoTimeout;

    public SDKConfiguration build()
    {
        return new SDKConfiguration(this, null);
    }

    public mSoTimeout setAppKey(String s)
    {
        mAppKey = s;
        return this;
    }

    public mAppKey setAppSecretKey(String s)
    {
        mAppSecretKey = s;
        return this;
    }

    public mAppSecretKey setContext(Context context)
    {
        mContext = context;
        return this;
    }

    public mContext setDebugMode(boolean flag)
    {
        mDebugMode = flag;
        return this;
    }

    public mDebugMode setHttpConnectionTimeout(int i)
    {
        mConnTimeOut = i;
        return this;
    }

    public mConnTimeOut setHttpReadTimeout(int i)
    {
        mSoTimeout = i;
        return this;
    }







    public ()
    {
        mDebugMode = false;
        mConnTimeOut = 30;
        mSoTimeout = 30;
    }
}
