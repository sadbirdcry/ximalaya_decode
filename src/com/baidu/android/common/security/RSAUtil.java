// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.android.common.security;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;
import javax.crypto.Cipher;

// Referenced classes of package com.baidu.android.common.security:
//            Base64

public final class RSAUtil
{

    public static final String ALGORITHM_RSA = "RSA";
    public static final int BYTE_IN_BITS = 8;
    public static final String PRIVATE_KEY = "PrivateKey";
    public static final int PT_MAXLEN_OFFSET = 11;
    public static final String PUBLIC_KEY = "PublicKey";

    private RSAUtil()
    {
    }

    public static byte[] decryptByPrivateKey(byte abyte0[], String s)
        throws Exception
    {
        s = new PKCS8EncodedKeySpec(Base64.decode(s.getBytes()));
        s = KeyFactory.getInstance("RSA").generatePrivate(s);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(2, s);
        return cipher.doFinal(abyte0);
    }

    public static byte[] decryptByPublicKey(byte abyte0[], String s)
        throws Exception
    {
        s = new X509EncodedKeySpec(Base64.decode(s.getBytes()));
        s = KeyFactory.getInstance("RSA").generatePublic(s);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(2, s);
        return cipher.doFinal(abyte0);
    }

    public static byte[] decryptLongByPrivateKey(byte abyte0[], String s, int i)
        throws Exception
    {
        s = new PKCS8EncodedKeySpec(Base64.decode(s.getBytes()));
        Object obj = KeyFactory.getInstance("RSA").generatePrivate(s);
        s = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        s.init(2, ((Key) (obj)));
        int k = i / 8;
        obj = new StringBuilder();
        int i1 = abyte0.length;
        int j;
        for (i = 0; i < i1; i = j + i)
        {
            int l = i1 - i;
            j = l;
            if (k < l)
            {
                j = k;
            }
            byte abyte1[] = new byte[j];
            System.arraycopy(abyte0, i, abyte1, 0, j);
            ((StringBuilder) (obj)).append(new String(s.doFinal(abyte1)));
        }

        return ((StringBuilder) (obj)).toString().getBytes();
    }

    public static byte[] encryptByPrivateKey(byte abyte0[], String s)
        throws Exception
    {
        s = new PKCS8EncodedKeySpec(Base64.decode(s.getBytes()));
        s = KeyFactory.getInstance("RSA").generatePrivate(s);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(1, s);
        return cipher.doFinal(abyte0);
    }

    public static byte[] encryptByPublicKey(byte abyte0[], String s)
        throws Exception
    {
        s = new X509EncodedKeySpec(Base64.decode(s.getBytes()));
        s = KeyFactory.getInstance("RSA").generatePublic(s);
        Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipher.init(1, s);
        return cipher.doFinal(abyte0);
    }

    public static byte[] encryptLongByPublicKey(byte abyte0[], String s, int i)
        throws Exception
    {
        s = new X509EncodedKeySpec(Base64.decode(s.getBytes()));
        java.security.PublicKey publickey = KeyFactory.getInstance("RSA").generatePublic(s);
        s = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        s.init(1, publickey);
        int j1 = i / 8;
        int l = j1 - 11;
        int k1 = abyte0.length;
        byte abyte1[] = new byte[(((k1 + l) - 1) / l) * j1];
        i = 0;
        for (int j = 0; j < k1;)
        {
            int i1 = k1 - j;
            int k = i1;
            if (l < i1)
            {
                k = l;
            }
            byte abyte2[] = new byte[k];
            System.arraycopy(abyte0, j, abyte2, 0, k);
            j += k;
            System.arraycopy(s.doFinal(abyte2), 0, abyte1, i, j1);
            i += j1;
        }

        return abyte1;
    }

    public static Map generateKey(int i)
        throws NoSuchAlgorithmException
    {
        Object obj = KeyPairGenerator.getInstance("RSA");
        ((KeyPairGenerator) (obj)).initialize(i);
        Object obj1 = ((KeyPairGenerator) (obj)).generateKeyPair();
        obj = (RSAPublicKey)((KeyPair) (obj1)).getPublic();
        obj1 = (RSAPrivateKey)((KeyPair) (obj1)).getPrivate();
        HashMap hashmap = new HashMap(2);
        hashmap.put("PublicKey", obj);
        hashmap.put("PrivateKey", obj1);
        return hashmap;
    }

    public static RSAPrivateKey generateRSAPrivateKey(BigInteger biginteger, BigInteger biginteger1)
        throws Exception
    {
        KeyFactory keyfactory;
        try
        {
            keyfactory = KeyFactory.getInstance("RSA");
        }
        // Misplaced declaration of an exception variable
        catch (BigInteger biginteger)
        {
            throw new Exception(biginteger.getMessage());
        }
        biginteger = new RSAPrivateKeySpec(biginteger, biginteger1);
        try
        {
            biginteger = (RSAPrivateKey)keyfactory.generatePrivate(biginteger);
        }
        // Misplaced declaration of an exception variable
        catch (BigInteger biginteger)
        {
            throw new Exception(biginteger.getMessage());
        }
        return biginteger;
    }

    public static RSAPublicKey generateRSAPublicKey(BigInteger biginteger, BigInteger biginteger1)
        throws Exception
    {
        KeyFactory keyfactory;
        try
        {
            keyfactory = KeyFactory.getInstance("RSA");
        }
        // Misplaced declaration of an exception variable
        catch (BigInteger biginteger)
        {
            throw new Exception(biginteger.getMessage());
        }
        biginteger = new RSAPublicKeySpec(biginteger, biginteger1);
        try
        {
            biginteger = (RSAPublicKey)keyfactory.generatePublic(biginteger);
        }
        // Misplaced declaration of an exception variable
        catch (BigInteger biginteger)
        {
            throw new Exception(biginteger.getMessage());
        }
        return biginteger;
    }

    public static String getPrivateKey(Map map)
        throws Exception
    {
        return Base64.encode(((Key)map.get("PrivateKey")).getEncoded(), "utf-8");
    }

    public static String getPublicKey(Map map)
        throws UnsupportedEncodingException
    {
        return Base64.encode(((Key)map.get("PublicKey")).getEncoded(), "utf-8");
    }
}
