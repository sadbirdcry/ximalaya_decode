// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.android.common.security;


public class RC4
{

    private static final int STATE_LENGTH = 256;
    private byte engineState[];
    private byte workingKey[];
    private int x;
    private int y;

    public RC4(String s)
    {
        engineState = null;
        x = 0;
        y = 0;
        workingKey = null;
        workingKey = s.getBytes();
    }

    private void processBytes(byte abyte0[], int i, int j, byte abyte1[], int k)
    {
        if (i + j > abyte0.length)
        {
            throw new RuntimeException("input buffer too short");
        }
        if (k + j > abyte1.length)
        {
            throw new RuntimeException("output buffer too short");
        }
        for (int l = 0; l < j; l++)
        {
            x = x + 1 & 0xff;
            y = engineState[x] + y & 0xff;
            byte byte0 = engineState[x];
            engineState[x] = engineState[y];
            engineState[y] = byte0;
            abyte1[l + k] = (byte)(abyte0[l + i] ^ engineState[engineState[x] + engineState[y] & 0xff]);
        }

    }

    private void reset()
    {
        setKey(workingKey);
    }

    private void setKey(byte abyte0[])
    {
        int k = 0;
        x = 0;
        y = 0;
        if (engineState == null)
        {
            engineState = new byte[256];
        }
        for (int i = 0; i < 256; i++)
        {
            engineState[i] = (byte)i;
        }

        int l = 0;
        int j = 0;
        for (; k < 256; k++)
        {
            l = l + ((abyte0[j] & 0xff) + engineState[k]) & 0xff;
            byte byte0 = engineState[k];
            engineState[k] = engineState[l];
            engineState[l] = byte0;
            j = (j + 1) % abyte0.length;
        }

    }

    public byte[] decrypt(byte abyte0[])
    {
        reset();
        byte abyte1[] = new byte[abyte0.length];
        processBytes(abyte0, 0, abyte0.length, abyte1, 0);
        return abyte1;
    }

    public byte[] encrypt(byte abyte0[])
    {
        reset();
        byte abyte1[] = new byte[abyte0.length];
        processBytes(abyte0, 0, abyte0.length, abyte1, 0);
        return abyte1;
    }
}
