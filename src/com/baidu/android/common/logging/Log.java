// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.android.common.logging;

import android.os.Environment;
import android.os.Process;
import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

// Referenced classes of package com.baidu.android.common.logging:
//            SimpleFormatter

public final class Log
{

    public static final int FILE_LIMETE = 0xa00000;
    public static final int FILE_NUMBER = 2;
    private static Logger sFilelogger;
    private static boolean sLog2File = false;
    private static boolean sLogEnabled = true;

    private Log()
    {
    }

    public static void d(String s, String s1)
    {
label0:
        {
            if (sLogEnabled)
            {
                if (!sLog2File || sFilelogger == null)
                {
                    break label0;
                }
                sFilelogger.log(Level.INFO, (new StringBuilder()).append(s).append(": ").append(s1).toString());
            }
            return;
        }
        android.util.Log.d(s, s1);
    }

    public static void d(String s, String s1, Throwable throwable)
    {
        d(s, (new StringBuilder()).append(s1).append('\n').append(getStackTraceString(throwable)).toString());
    }

    public static void e(String s, String s1)
    {
label0:
        {
            if (sLogEnabled)
            {
                if (!sLog2File || sFilelogger == null)
                {
                    break label0;
                }
                sFilelogger.log(Level.SEVERE, (new StringBuilder()).append(s).append(": ").append(s1).toString());
            }
            return;
        }
        android.util.Log.e(s, s1);
    }

    public static void e(String s, String s1, Throwable throwable)
    {
        e(s, (new StringBuilder()).append(s1).append('\n').append(getStackTraceString(throwable)).toString());
    }

    public static void e(String s, Throwable throwable)
    {
        e(s, getStackTraceString(throwable));
    }

    private static String getLogFileName()
    {
        String s1 = getProcessNameForPid(Process.myPid());
        String s = s1;
        if (TextUtils.isEmpty(s1))
        {
            s = "BaiduFileLog";
        }
        return s.replace(':', '_');
    }

    private static String getProcessNameForPid(int j)
    {
        Object obj;
        Object obj1;
        obj1 = (new StringBuilder()).append("/proc/").append(j).append("/cmdline").toString();
        obj = (new StringBuilder()).append("/proc/").append(j).append("/status").toString();
        String s;
        obj1 = new BufferedReader(new FileReader(new File(((String) (obj1)))));
        s = ((BufferedReader) (obj1)).readLine();
        if (TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        obj = s.substring(0, s.indexOf('\0'));
_L7:
        ((BufferedReader) (obj1)).close();
        return ((String) (obj));
_L2:
        obj = new BufferedReader(new FileReader(new File(((String) (obj)))));
        obj1 = ((BufferedReader) (obj)).readLine();
_L4:
        if (obj1 == null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!((String) (obj1)).startsWith("Name:"))
        {
            break MISSING_BLOCK_LABEL_169;
        }
        j = ((String) (obj1)).indexOf("\t");
        if (j < 0)
        {
            break; /* Loop/switch isn't completed */
        }
        s = ((String) (obj1)).substring(j + 1);
        obj1 = obj;
        obj = s;
        continue; /* Loop/switch isn't completed */
        obj1 = ((BufferedReader) (obj)).readLine();
        if (true) goto _L4; else goto _L3
        obj1;
        obj = "";
_L5:
        ((Exception) (obj1)).printStackTrace();
        return ((String) (obj));
        obj1;
        if (true) goto _L5; else goto _L3
_L3:
        String s1 = "";
        obj1 = obj;
        obj = s1;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public static String getStackTraceString(Throwable throwable)
    {
        if (throwable == null)
        {
            return "";
        } else
        {
            StringWriter stringwriter = new StringWriter();
            throwable.printStackTrace(new PrintWriter(stringwriter));
            return stringwriter.toString();
        }
    }

    public static void i(String s, String s1)
    {
label0:
        {
            if (sLogEnabled)
            {
                if (!sLog2File || sFilelogger == null)
                {
                    break label0;
                }
                sFilelogger.log(Level.INFO, (new StringBuilder()).append(s).append(": ").append(s1).toString());
            }
            return;
        }
        android.util.Log.i(s, s1);
    }

    public static void i(String s, String s1, Throwable throwable)
    {
        i(s, (new StringBuilder()).append(s1).append('\n').append(getStackTraceString(throwable)).toString());
    }

    public static void setLog2File(boolean flag)
    {
        String s;
        Object obj1;
        sLog2File = flag;
        if (!sLog2File || sFilelogger != null)
        {
            break MISSING_BLOCK_LABEL_100;
        }
        s = getLogFileName();
        obj1 = (new File(Environment.getExternalStorageDirectory(), s)).getAbsolutePath();
        obj1 = new FileHandler((new StringBuilder()).append(((String) (obj1))).append("_%g.log").toString(), 0xa00000, 2, true);
        ((FileHandler) (obj1)).setFormatter(new SimpleFormatter());
        sFilelogger = Logger.getLogger(s);
        sFilelogger.setLevel(Level.ALL);
        sFilelogger.addHandler(((java.util.logging.Handler) (obj1)));
        return;
        Object obj;
        obj;
        ((SecurityException) (obj)).printStackTrace();
        return;
        obj;
        ((IOException) (obj)).printStackTrace();
        return;
    }

    public static void setLogEnabled(boolean flag)
    {
        sLogEnabled = flag;
    }

    public static void v(String s, String s1)
    {
label0:
        {
            if (sLogEnabled)
            {
                if (!sLog2File || sFilelogger == null)
                {
                    break label0;
                }
                sFilelogger.log(Level.INFO, (new StringBuilder()).append(s).append(": ").append(s1).toString());
            }
            return;
        }
        android.util.Log.v(s, s1);
    }

    public static void v(String s, String s1, Throwable throwable)
    {
        v(s, (new StringBuilder()).append(s1).append('\n').append(getStackTraceString(throwable)).toString());
    }

    public static void w(String s, String s1)
    {
label0:
        {
            if (sLogEnabled)
            {
                if (!sLog2File || sFilelogger == null)
                {
                    break label0;
                }
                sFilelogger.log(Level.WARNING, (new StringBuilder()).append(s).append(": ").append(s1).toString());
            }
            return;
        }
        android.util.Log.w(s, s1);
    }

    public static void w(String s, String s1, Throwable throwable)
    {
        w(s, (new StringBuilder()).append(s1).append('\n').append(getStackTraceString(throwable)).toString());
    }

}
