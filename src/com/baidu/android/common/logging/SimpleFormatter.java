// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.android.common.logging;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

// Referenced classes of package com.baidu.android.common.logging:
//            Log

class SimpleFormatter extends Formatter
{

    private static String format = "{0,date} {0,time}";
    private Object args[];
    Date dat;
    private MessageFormat formatter;

    SimpleFormatter()
    {
        dat = new Date();
        args = new Object[1];
    }

    public String format(LogRecord logrecord)
    {
        Object obj1 = null;
        this;
        JVM INSTR monitorenter ;
        StackTraceElement astacktraceelement[];
        int j;
        astacktraceelement = (new Throwable()).getStackTrace();
        j = astacktraceelement.length;
        int i;
        boolean flag;
        i = 0;
        flag = false;
_L7:
        StackTraceElement stacktraceelement;
        if (i >= j)
        {
            break MISSING_BLOCK_LABEL_396;
        }
        stacktraceelement = astacktraceelement[i];
        boolean flag1;
        if (stacktraceelement.getClassName().startsWith(com/baidu/android/common/logging/Log.getName()))
        {
            flag1 = true;
            break MISSING_BLOCK_LABEL_404;
        }
        flag1 = flag;
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_404;
        }
        Object obj;
        obj1 = stacktraceelement.getClassName();
        obj = stacktraceelement.getMethodName();
        i = stacktraceelement.getLineNumber();
_L5:
        logrecord.setSourceClassName(((String) (obj1)));
        logrecord.setSourceMethodName(((String) (obj)));
        obj = new StringBuffer();
        dat.setTime(logrecord.getMillis());
        args[0] = dat;
        obj1 = new StringBuffer();
        if (formatter == null)
        {
            formatter = new MessageFormat(format);
        }
        formatter.format(args, ((StringBuffer) (obj1)), null);
        ((StringBuffer) (obj)).append(((StringBuffer) (obj1)));
        ((StringBuffer) (obj)).append((new StringBuilder()).append(".").append(logrecord.getMillis() % 1000L).toString());
        ((StringBuffer) (obj)).append(" ");
        if (logrecord.getSourceClassName() == null) goto _L2; else goto _L1
_L1:
        ((StringBuffer) (obj)).append(logrecord.getSourceClassName());
_L3:
        if (logrecord.getSourceMethodName() != null)
        {
            ((StringBuffer) (obj)).append(" ");
            ((StringBuffer) (obj)).append(logrecord.getSourceMethodName());
        }
        ((StringBuffer) (obj)).append(" ");
        ((StringBuffer) (obj)).append(i);
        ((StringBuffer) (obj)).append(" ");
        obj1 = formatMessage(logrecord);
        ((StringBuffer) (obj)).append(logrecord.getLevel().getLocalizedName());
        ((StringBuffer) (obj)).append(": ");
        ((StringBuffer) (obj)).append(((String) (obj1)));
        ((StringBuffer) (obj)).append("\n");
        obj1 = logrecord.getThrown();
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_362;
        }
        StringWriter stringwriter = new StringWriter();
        PrintWriter printwriter = new PrintWriter(stringwriter);
        logrecord.getThrown().printStackTrace(printwriter);
        printwriter.close();
        ((StringBuffer) (obj)).append(stringwriter.toString());
_L4:
        logrecord = ((StringBuffer) (obj)).toString();
        this;
        JVM INSTR monitorexit ;
        return logrecord;
_L2:
        ((StringBuffer) (obj)).append(logrecord.getLoggerName());
          goto _L3
        logrecord;
        throw logrecord;
        logrecord;
        logrecord.printStackTrace();
          goto _L4
        i = 0;
        obj = null;
          goto _L5
        i++;
        flag = flag1;
        if (true) goto _L7; else goto _L6
_L6:
    }

}
