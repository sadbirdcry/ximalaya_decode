// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.android.common.util;

import android.content.Context;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import com.baidu.android.common.security.AESUtil;
import com.baidu.android.common.security.Base64;
import com.baidu.android.common.security.MD5Util;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

public final class DeviceId
{
    static final class IMEIInfo
    {

        public static final String DEFAULT_TM_DEVICEID = "";
        private static final String KEY_IMEI = "bd_setting_i";
        public final boolean CAN_READ_AND_WRITE_SYSTEM_SETTINGS;
        public final String IMEI;

        private static String getIMEI(Context context, String s)
        {
            context = (TelephonyManager)context.getSystemService("phone");
            if (context == null)
            {
                break MISSING_BLOCK_LABEL_42;
            }
            context = context.getDeviceId();
_L1:
            context = imeiCheck(context);
            if (TextUtils.isEmpty(context))
            {
                return s;
            } else
            {
                return context;
            }
            context;
            Log.e("DeviceId", "Read IMEI failed", context);
            context = null;
              goto _L1
        }

        static IMEIInfo getIMEIInfo(Context context)
        {
            Object obj;
            boolean flag1;
            flag1 = true;
            obj = "";
            Object obj1 = android.provider.Settings.System.getString(context.getContentResolver(), "bd_setting_i");
            obj = obj1;
            if (!TextUtils.isEmpty(((CharSequence) (obj1)))) goto _L2; else goto _L1
_L1:
            obj = obj1;
            obj1 = getIMEI(context, "");
            obj = obj1;
_L7:
            android.provider.Settings.System.putString(context.getContentResolver(), "bd_setting_i", ((String) (obj)));
            boolean flag = false;
_L4:
            if (flag)
            {
                flag1 = false;
            }
            return new IMEIInfo(((String) (obj)), flag1);
            obj1;
_L5:
            Log.e("DeviceId", "Settings.System.getString or putString failed", ((Throwable) (obj1)));
            if (TextUtils.isEmpty(((CharSequence) (obj))))
            {
                obj = getIMEI(context, "");
                flag = true;
            } else
            {
                flag = true;
            }
            if (true) goto _L4; else goto _L3
_L3:
            obj1;
              goto _L5
_L2:
            obj = obj1;
            if (true) goto _L7; else goto _L6
_L6:
        }

        private static String imeiCheck(String s)
        {
            String s1 = s;
            if (s != null)
            {
                s1 = s;
                if (s.contains(":"))
                {
                    s1 = "";
                }
            }
            return s1;
        }

        private IMEIInfo(String s, boolean flag)
        {
            IMEI = s;
            CAN_READ_AND_WRITE_SYSTEM_SETTINGS = flag;
        }
    }


    private static final String AES_KEY = "30212102dicudiab";
    private static final boolean DEBUG = false;
    private static final String EXT_FILE = "baidu/.cuid";
    private static final String KEY_DEVICE_ID = "com.baidu.deviceid";
    private static final String TAG = "DeviceId";

    private DeviceId()
    {
    }

    private static void checkPermission(Context context, String s)
    {
        boolean flag;
        if (context.checkCallingOrSelfPermission(s) == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (!flag)
        {
            throw new SecurityException((new StringBuilder()).append("Permission Denial: requires permission ").append(s).toString());
        } else
        {
            return;
        }
    }

    public static String getAndroidId(Context context)
    {
        String s = android.provider.Settings.Secure.getString(context.getContentResolver(), "android_id");
        context = s;
        if (TextUtils.isEmpty(s))
        {
            context = "";
        }
        return context;
    }

    public static String getDeviceID(Context context)
    {
        checkPermission(context, "android.permission.WRITE_SETTINGS");
        checkPermission(context, "android.permission.READ_PHONE_STATE");
        checkPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        Object obj = IMEIInfo.getIMEIInfo(context);
        String s4 = ((IMEIInfo) (obj)).IMEI;
        String s5;
        boolean flag;
        if (!((IMEIInfo) (obj)).CAN_READ_AND_WRITE_SYSTEM_SETTINGS)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        s5 = getAndroidId(context);
        if (flag)
        {
            obj = MD5Util.toMd5((new StringBuilder()).append("com.baidu").append(s5).toString().getBytes(), true);
        } else
        {
            String s2 = null;
            String s1 = android.provider.Settings.System.getString(context.getContentResolver(), "com.baidu.deviceid");
            obj = s1;
            if (TextUtils.isEmpty(s1))
            {
                s1 = MD5Util.toMd5((new StringBuilder()).append("com.baidu").append(s4).append(s5).toString().getBytes(), true);
                String s3 = android.provider.Settings.System.getString(context.getContentResolver(), s1);
                s2 = s1;
                obj = s3;
                if (!TextUtils.isEmpty(s3))
                {
                    android.provider.Settings.System.putString(context.getContentResolver(), "com.baidu.deviceid", s3);
                    setExternalDeviceId(s4, s3);
                    obj = s3;
                    s2 = s1;
                }
            }
            s1 = ((String) (obj));
            if (TextUtils.isEmpty(((CharSequence) (obj))))
            {
                obj = getExternalDeviceId(s4);
                s1 = ((String) (obj));
                if (!TextUtils.isEmpty(((CharSequence) (obj))))
                {
                    android.provider.Settings.System.putString(context.getContentResolver(), s2, ((String) (obj)));
                    android.provider.Settings.System.putString(context.getContentResolver(), "com.baidu.deviceid", ((String) (obj)));
                    s1 = ((String) (obj));
                }
            }
            obj = s1;
            if (TextUtils.isEmpty(s1))
            {
                String s = UUID.randomUUID().toString();
                s = MD5Util.toMd5((new StringBuilder()).append(s4).append(s5).append(s).toString().getBytes(), true);
                android.provider.Settings.System.putString(context.getContentResolver(), s2, s);
                android.provider.Settings.System.putString(context.getContentResolver(), "com.baidu.deviceid", s);
                setExternalDeviceId(s4, s);
                return s;
            }
        }
        return ((String) (obj));
    }

    private static String getExternalDeviceId(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return "";
_L2:
        Object obj = new File(Environment.getExternalStorageDirectory(), "baidu/.cuid");
        StringBuilder stringbuilder;
        obj = new BufferedReader(new FileReader(((File) (obj))));
        stringbuilder = new StringBuilder();
_L3:
        String s1 = ((BufferedReader) (obj)).readLine();
label0:
        {
            if (s1 == null)
            {
                break label0;
            }
            String as[];
            try
            {
                stringbuilder.append(s1);
                stringbuilder.append("\r\n");
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return "";
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return "";
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return "";
            }
        }
          goto _L3
        ((BufferedReader) (obj)).close();
        as = (new String(AESUtil.decrypt("30212102dicudiab", "30212102dicudiab", Base64.decode(stringbuilder.toString().getBytes())))).split("=");
        if (as == null) goto _L1; else goto _L4
_L4:
        if (as.length != 2 || !s.equals(as[0])) goto _L1; else goto _L5
_L5:
        s = as[1];
        return s;
    }

    public static String getIMEI(Context context)
    {
        return IMEIInfo.getIMEIInfo(context).IMEI;
    }

    private static void setExternalDeviceId(String s, String s1)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append(s);
        stringbuilder.append("=");
        stringbuilder.append(s1);
        s = new File(Environment.getExternalStorageDirectory(), "baidu/.cuid");
        try
        {
            (new File(s.getParent())).mkdirs();
            s = new FileWriter(s, false);
            s.write(Base64.encode(AESUtil.encrypt("30212102dicudiab", "30212102dicudiab", stringbuilder.toString().getBytes()), "utf-8"));
            s.flush();
            s.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }
}
