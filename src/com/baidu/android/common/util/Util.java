// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.android.common.util;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.os.Process;
import android.text.TextUtils;
import com.baidu.android.common.security.MD5Util;
import java.util.Iterator;
import java.util.List;

public final class Util
{

    private Util()
    {
    }

    public static boolean hasOtherServiceRuninMyPid(Context context, String s)
    {
        for (context = ((ActivityManager)context.getApplicationContext().getSystemService("activity")).getRunningServices(100).iterator(); context.hasNext();)
        {
            android.app.ActivityManager.RunningServiceInfo runningserviceinfo = (android.app.ActivityManager.RunningServiceInfo)context.next();
            if (runningserviceinfo.pid == Process.myPid() && !TextUtils.equals(runningserviceinfo.service.getClassName(), s))
            {
                return true;
            }
        }

        return false;
    }

    public static String toHexString(byte abyte0[], String s, boolean flag)
    {
        return MD5Util.toHexString(abyte0, s, flag);
    }

    public static String toMd5(byte abyte0[], boolean flag)
    {
        return MD5Util.toMd5(abyte0, flag);
    }
}
