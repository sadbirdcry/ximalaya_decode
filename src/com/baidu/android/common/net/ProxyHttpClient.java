// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.android.common.net;

import android.content.Context;
import android.text.TextUtils;
import com.baidu.android.common.logging.Log;
import java.io.IOException;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

// Referenced classes of package com.baidu.android.common.net:
//            ConnectManager

public class ProxyHttpClient extends DefaultHttpClient
{

    private static final boolean DEBUG = false;
    private static final int HTTP_TIMEOUT_MS = 30000;
    private static final String TAG = com/baidu/android/common/net/ProxyHttpClient.getSimpleName();
    private RuntimeException mLeakedException;
    private String mPort;
    private String mProxy;
    private boolean mUseWap;

    public ProxyHttpClient(Context context)
    {
        this(context, null, null);
    }

    public ProxyHttpClient(Context context, ConnectManager connectmanager)
    {
        this(context, null, connectmanager);
    }

    public ProxyHttpClient(Context context, String s)
    {
        this(context, s, null);
    }

    public ProxyHttpClient(Context context, String s, ConnectManager connectmanager)
    {
        mLeakedException = new IllegalStateException("ProxyHttpClient created and never closed");
        ConnectManager connectmanager1 = connectmanager;
        if (connectmanager == null)
        {
            connectmanager1 = new ConnectManager(context);
        }
        mUseWap = connectmanager1.isWapNetwork();
        mProxy = connectmanager1.getProxy();
        mPort = connectmanager1.getProxyPort();
        if (mProxy != null && mProxy.length() > 0)
        {
            context = new HttpHost(mProxy, Integer.valueOf(mPort).intValue());
            getParams().setParameter("http.route.default-proxy", context);
        }
        HttpConnectionParams.setConnectionTimeout(getParams(), 30000);
        HttpConnectionParams.setSoTimeout(getParams(), 30000);
        HttpConnectionParams.setSocketBufferSize(getParams(), 8192);
        if (!TextUtils.isEmpty(s))
        {
            HttpProtocolParams.setUserAgent(getParams(), s);
        }
    }

    public void close()
    {
        if (mLeakedException != null)
        {
            getConnectionManager().shutdown();
            mLeakedException = null;
        }
    }

    protected HttpParams createHttpParams()
    {
        HttpParams httpparams = super.createHttpParams();
        HttpProtocolParams.setUseExpectContinue(httpparams, false);
        return httpparams;
    }

    public HttpResponse executeSafely(HttpUriRequest httpurirequest)
        throws ClientProtocolException, IOException
    {
        try
        {
            httpurirequest = execute(httpurirequest);
        }
        // Misplaced declaration of an exception variable
        catch (HttpUriRequest httpurirequest)
        {
            throw new ClientProtocolException(httpurirequest);
        }
        return httpurirequest;
    }

    protected void finalize()
        throws Throwable
    {
        super.finalize();
        if (mLeakedException != null)
        {
            Log.e(TAG, "Leak found", mLeakedException);
        }
    }

    public boolean isWap()
    {
        return mUseWap;
    }

}
