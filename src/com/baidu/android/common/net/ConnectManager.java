// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.android.common.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;

public class ConnectManager
{

    private static final boolean DEBUG = false;
    private static final String TAG = com/baidu/android/common/net/ConnectManager.getSimpleName();
    private String mApn;
    private String mNetType;
    private String mPort;
    private String mProxy;
    private boolean mUseWap;

    public ConnectManager(Context context)
    {
        checkNetworkType(context);
    }

    private void checkApn(Context context, NetworkInfo networkinfo)
    {
        if (networkinfo.getExtraInfo() != null)
        {
            context = networkinfo.getExtraInfo().toLowerCase();
            if (context != null)
            {
                if (context.startsWith("cmwap") || context.startsWith("uniwap") || context.startsWith("3gwap"))
                {
                    mUseWap = true;
                    mApn = context;
                    mProxy = "10.0.0.172";
                    mPort = "80";
                    return;
                }
                if (context.startsWith("ctwap"))
                {
                    mUseWap = true;
                    mApn = context;
                    mProxy = "10.0.0.200";
                    mPort = "80";
                    return;
                }
                if (context.startsWith("cmnet") || context.startsWith("uninet") || context.startsWith("ctnet") || context.startsWith("3gnet"))
                {
                    mUseWap = false;
                    mApn = context;
                    return;
                }
            }
        }
        context = Proxy.getDefaultHost();
        int i = Proxy.getDefaultPort();
        if (context != null && context.length() > 0)
        {
            mProxy = context;
            if ("10.0.0.172".equals(mProxy.trim()))
            {
                mUseWap = true;
                mPort = "80";
                return;
            }
            if ("10.0.0.200".equals(mProxy.trim()))
            {
                mUseWap = true;
                mPort = "80";
                return;
            } else
            {
                mUseWap = false;
                mPort = Integer.toString(i);
                return;
            }
        } else
        {
            mUseWap = false;
            return;
        }
    }

    private void checkNetworkType(Context context)
    {
        NetworkInfo networkinfo;
label0:
        {
            networkinfo = ((ConnectivityManager)context.getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
            if (networkinfo != null)
            {
                if (!"wifi".equals(networkinfo.getTypeName().toLowerCase()))
                {
                    break label0;
                }
                mNetType = "wifi";
                mUseWap = false;
            }
            return;
        }
        checkApn(context, networkinfo);
        mNetType = mApn;
    }

    public static boolean isNetworkConnected(Context context)
    {
        context = ((ConnectivityManager)context.getApplicationContext().getSystemService("connectivity")).getActiveNetworkInfo();
        if (context != null)
        {
            return context.isConnectedOrConnecting();
        } else
        {
            return false;
        }
    }

    public String getApn()
    {
        return mApn;
    }

    public String getNetType()
    {
        return mNetType;
    }

    public String getProxy()
    {
        return mProxy;
    }

    public String getProxyPort()
    {
        return mPort;
    }

    public boolean isWapNetwork()
    {
        return mUseWap;
    }

}
