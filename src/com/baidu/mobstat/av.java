// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.text.format.DateFormat;
import android.util.Log;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

// Referenced classes of package com.baidu.mobstat:
//            ar

public class av
{

    private static DateFormat a = null;

    public static int a(String s)
    {
        return a("sdkstat", s);
    }

    public static int a(String s, String s1)
    {
        b(s, s1);
        return Log.d(s, s1);
    }

    public static int a(String s, Throwable throwable)
    {
        a("sdkstat", s, throwable);
        return Log.d("sdkstat", s, throwable);
    }

    public static transient int a(Object aobj[])
    {
        return a(d(aobj));
    }

    public static void a()
    {
        ar.a("_b_sdk.log");
    }

    private static void a(String s, String s1, Throwable throwable)
    {
        StringWriter stringwriter = new StringWriter();
        PrintWriter printwriter = new PrintWriter(stringwriter);
        throwable.printStackTrace(printwriter);
        b(s, (new StringBuilder(s1)).append("\n").append(stringwriter.toString()).toString());
        printwriter.close();
        try
        {
            stringwriter.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Log.w("Log.debug", "", s);
        }
    }

    public static boolean a(int i, int j)
    {
        return i >= j;
    }

    public static int b(String s)
    {
        b("sdkstat", s);
        return Log.w("sdkstat", s);
    }

    public static transient int b(Object aobj[])
    {
        return b(d(aobj));
    }

    private static void b(String s, String s1)
    {
        com/baidu/mobstat/av;
        JVM INSTR monitorenter ;
    }

    public static int c(String s)
    {
        b("sdkstat", s);
        return Log.e("sdkstat", s);
    }

    public static transient int c(Object aobj[])
    {
        return c(d(aobj));
    }

    private static String d(Object aobj[])
    {
        StringBuilder stringbuilder = new StringBuilder();
        int j = aobj.length;
        for (int i = 0; i < j; i++)
        {
            stringbuilder.append(aobj[i]).append(' ');
        }

        return stringbuilder.toString();
    }

    static 
    {
        a();
        a = new DateFormat();
    }
}
