// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

public class BasicStoreToolsBase
{

    private final String a = "__Baidu_Stat_SDK_SendRem";
    private SharedPreferences b;
    private SharedPreferences c;

    public BasicStoreToolsBase()
    {
    }

    private SharedPreferences a(Context context)
    {
        if (b == null)
        {
            b = context.getSharedPreferences("__Baidu_Stat_SDK_SendRem", 0);
        }
        return b;
    }

    private SharedPreferences b(Context context)
    {
        if (c == null)
        {
            c = PreferenceManager.getDefaultSharedPreferences(context);
        }
        return c;
    }

    public boolean getBoolean(Context context, String s, boolean flag)
    {
        return a(context).getBoolean(s, flag);
    }

    public Float getFloatt(Context context, String s, int i)
    {
        return Float.valueOf(a(context).getFloat(s, i));
    }

    public int getInt(Context context, String s, int i)
    {
        return a(context).getInt(s, i);
    }

    public long getLong(Context context, String s, long l)
    {
        return a(context).getLong(s, l);
    }

    public boolean getSharedBoolean(Context context, String s, boolean flag)
    {
        return b(context).getBoolean(s, flag);
    }

    public int getSharedInt(Context context, String s, int i)
    {
        return b(context).getInt(s, i);
    }

    public long getSharedLong(Context context, String s, long l)
    {
        return b(context).getLong(s, l);
    }

    public String getSharedString(Context context, String s, String s1)
    {
        return b(context).getString(s, s1);
    }

    public String getString(Context context, String s, String s1)
    {
        return a(context).getString(s, s1);
    }

    public void putBoolean(Context context, String s, boolean flag)
    {
        a(context).edit().putBoolean(s, flag).commit();
    }

    public void putFloat(Context context, String s, Float float1)
    {
        a(context).edit().putFloat(s, float1.floatValue()).commit();
    }

    public void putInt(Context context, String s, int i)
    {
        a(context).edit().putInt(s, i).commit();
    }

    public void putLong(Context context, String s, long l)
    {
        a(context).edit().putLong(s, l).commit();
    }

    public void putSharedBoolean(Context context, String s, boolean flag)
    {
        b(context).edit().putBoolean(s, flag).commit();
    }

    public void putSharedInt(Context context, String s, int i)
    {
        b(context).edit().putInt(s, i).commit();
    }

    public void putSharedLong(Context context, String s, long l)
    {
        b(context).edit().putLong(s, l).commit();
    }

    public void putSharedString(Context context, String s, String s1)
    {
        b(context).edit().putString(s, s1).commit();
    }

    public void putString(Context context, String s, String s1)
    {
        a(context).edit().putString(s, s1).commit();
    }

    public void removeShare(Context context, String s)
    {
        b(context).edit().remove(s).commit();
    }

    public void removeString(Context context, String s)
    {
        a(context).edit().remove(s).commit();
    }

    public boolean updateShareBoolean(Intent intent, Activity activity, String s)
    {
        return updateShareBoolean(intent, activity, s, true);
    }

    public boolean updateShareBoolean(Intent intent, Activity activity, String s, boolean flag)
    {
        if (intent != null)
        {
            boolean flag1 = intent.getBooleanExtra(s, flag);
            if (flag1 != getSharedBoolean(activity, s, flag))
            {
                putSharedBoolean(activity, s, flag1);
                return true;
            }
        }
        return false;
    }

    public boolean updateShareInt(Intent intent, Activity activity, String s, int i)
    {
        if (intent != null)
        {
            int j = intent.getIntExtra(s, i);
            if (j != getSharedInt(activity, s, i))
            {
                putSharedInt(activity, s, j);
                return true;
            }
        }
        return false;
    }

    public boolean updateShareString(Intent intent, Activity activity, String s)
    {
        if (intent != null)
        {
            String s1 = intent.getStringExtra(s);
            intent = s1;
            if (s1 != null)
            {
                s1 = s1.trim();
                intent = s1;
                if (s1.length() == 0)
                {
                    intent = null;
                }
            }
            if (!TextUtils.equals(intent, getSharedString(activity, s, null)))
            {
                putSharedString(activity, s, intent);
                return true;
            }
        }
        return false;
    }
}
