// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.support.v4.app.Fragment;
import java.lang.ref.WeakReference;

// Referenced classes of package com.baidu.mobstat:
//            au, at, ag, al, 
//            ae

class am
    implements Runnable
{

    final ag a;
    private long b;
    private WeakReference c;
    private WeakReference d;
    private WeakReference e;
    private long f;
    private WeakReference g;
    private WeakReference h;
    private WeakReference i;
    private int j;
    private String k;

    public am(ag ag1, long l, Context context, Fragment fragment, long l1, 
            Context context1, Fragment fragment1, int i1, String s, Object obj, Object obj1)
    {
        a = ag1;
        super();
        k = null;
        b = l;
        if (context != null)
        {
            c = new WeakReference(context);
        }
        f = l1;
        if (context1 != null)
        {
            g = new WeakReference(context1);
        }
        if (fragment != null)
        {
            d = new WeakReference(fragment);
        }
        if (fragment1 != null)
        {
            h = new WeakReference(fragment1);
        }
        if (obj != null)
        {
            i = new WeakReference(obj);
        }
        if (obj1 != null)
        {
            e = new WeakReference(obj1);
        }
        j = i1;
        k = s;
    }

    public void run()
    {
        if (j != 1) goto _L2; else goto _L1
_L1:
        if (c.get() == g.get()) goto _L4; else goto _L3
_L3:
        if (k == null) goto _L6; else goto _L5
_L5:
        au.a().c(new Object[] {
            "statsdk", "onPageStart() \u6216 onPageEnd()\u5B89\u653E\u9519\u8BEF  || onPageStart() or onPageEnd() install error."
        });
_L11:
        return;
_L6:
        au.a().c(new Object[] {
            "statsdk", "onPause() \u6216 onResume()\u5B89\u653E\u9519\u8BEF  ||  onPause() or onResume() install error."
        });
        return;
_L4:
        Object obj;
        Object obj2;
        long l;
        l = b - f;
        obj2 = (Activity)c.get();
        if (obj2 == null)
        {
            au.a().c(new Object[] {
                "statsdk", "onPause,WeakReference is already been released"
            });
            return;
        }
        obj = new StringBuilder();
        if (k == null) goto _L8; else goto _L7
_L7:
        ((StringBuilder) (obj)).append(k);
        obj2 = a.b(k);
        if (obj2 != null)
        {
            long l3 = ((al) (obj2)).d - ((al) (obj2)).c;
            au.a().a("sdkstat", (new StringBuilder()).append("==============page time=").append(((al) (obj2)).a).append(";time=").append(l3).toString());
            l = l3;
            if (l3 < 20L)
            {
                au.a().b(new Object[] {
                    "sdkstat", "==============page time little than 20 milli"
                });
                return;
            }
        }
_L9:
        au.a().a("statsdk", (new StringBuilder()).append("new page view, page name = ").append(((StringBuilder) (obj)).toString()).append(",stay time = ").append(l).append("(ms)").toString());
        ag.a(a).a(((StringBuilder) (obj)).toString(), l, f);
        if (k != null)
        {
            obj = a.b(k);
            if (obj != null)
            {
                ag.a(a, (Context)c.get(), ((al) (obj)).d);
                a.c(k);
                return;
            }
        } else
        {
            ag.a(a, (Context)c.get(), b);
            return;
        }
        if (false)
        {
            break MISSING_BLOCK_LABEL_373;
        } else
        {
            continue; /* Loop/switch isn't completed */
        }
_L8:
        ((StringBuilder) (obj)).append(((Activity) (obj2)).getComponentName().getShortClassName());
        if (((StringBuilder) (obj)).charAt(0) == '.')
        {
            ((StringBuilder) (obj)).deleteCharAt(0);
        }
          goto _L9
_L2:
        if (j == 2)
        {
            if (d.get() != h.get())
            {
                au.a().c(new Object[] {
                    "statsdk", " Fragment onPause() \u6216 onResume()\u5B89\u653E\u9519\u8BEF||onPause() or onResume() install error."
                });
                return;
            }
            long l1 = b - f;
            Object obj1 = (Fragment)d.get();
            if (obj1 == null)
            {
                au.a().c(new Object[] {
                    "statsdk", "onPause,WeakReference is already been released"
                });
                return;
            } else
            {
                obj1 = obj1.getClass().getName().toString();
                String s = ((String) (obj1)).substring(((String) (obj1)).lastIndexOf(".") + 1);
                au.a().a("statsdk", (new StringBuilder()).append("Fragment new page view, page name = ").append(((String) (obj1)).toString()).append(",stay time = ").append(l1).append("(ms)").toString());
                ag.a(a).a(s, l1, f);
                ag.a(a, ((Fragment)d.get()).getActivity(), b);
                return;
            }
        }
        if (j == 3)
        {
            if (e.get() != i.get())
            {
                au.a().c(new Object[] {
                    "statsdk", " Fragment onPause() \u6216 onResume()\u5B89\u653E\u9519\u8BEF||onPause() or onResume() install error."
                });
                return;
            }
            long l2 = b - f;
            Object obj3 = e.get();
            if (obj3 == null)
            {
                au.a().c(new Object[] {
                    "statsdk", "onPause,WeakReference is already been released"
                });
                return;
            } else
            {
                Context context = ag.a(obj3);
                obj3 = obj3.getClass().getName().toString();
                String s1 = ((String) (obj3)).substring(((String) (obj3)).lastIndexOf(".") + 1);
                au.a().a("statsdk", (new StringBuilder()).append("android.app.Fragment new page view, page name = ").append(((String) (obj3)).toString()).append(",stay time = ").append(l2).append("(ms)").toString());
                ag.a(a).a(s1, l2, f);
                ag.a(a, context, b);
                return;
            }
        }
        if (true) goto _L11; else goto _L10
_L10:
    }
}
