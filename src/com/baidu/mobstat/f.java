// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import com.baidu.kirin.objects.LatitudeAndLongitude;
import com.baidu.kirin.objects.NetworkStatus;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.baidu.mobstat:
//            CooperService, m, at, ar

public class f
{

    public static String a()
    {
        Date date = new Date();
        return (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(date);
    }

    public static String a(Context context)
    {
        return CooperService.getAppChannel(context);
    }

    public static String a(Context context, boolean flag)
    {
        LatitudeAndLongitude latitudeandlongitude = new LatitudeAndLongitude();
        if (flag)
        {
            context = (LocationManager)context.getSystemService("location");
            for (Iterator iterator = context.getAllProviders().iterator(); iterator.hasNext();)
            {
                Object obj = (String)iterator.next();
                System.out.println(((String) (obj)));
                obj = context.getLastKnownLocation(((String) (obj)));
                if (obj != null)
                {
                    latitudeandlongitude.latitude = (new StringBuilder()).append(((Location) (obj)).getLatitude()).append("").toString();
                    latitudeandlongitude.longitude = (new StringBuilder()).append(((Location) (obj)).getLongitude()).append("").toString();
                } else
                {
                    latitudeandlongitude.latitude = "";
                    latitudeandlongitude.longitude = "";
                }
            }

        } else
        {
            latitudeandlongitude.latitude = "";
            latitudeandlongitude.longitude = "";
        }
        return (new StringBuilder()).append(latitudeandlongitude.latitude).append(",").append(latitudeandlongitude.longitude).toString();
    }

    public static boolean a(Context context, String s)
    {
        return context.getPackageManager().checkPermission(s, context.getPackageName()) == 0;
    }

    public static String b(Context context)
    {
        return CooperService.getAppKey(context);
    }

    public static String c(Context context)
    {
        return CooperService.getAppVersionName(context);
    }

    public static int d(Context context)
    {
        return CooperService.getAppVersionCode(context);
    }

    public static String e(Context context)
    {
        if (o(context))
        {
            context = android.os.Build.VERSION.RELEASE;
            com.baidu.mobstat.m.a().a(new Object[] {
                (new StringBuilder()).append("android_osVersion : ").append(context).toString()
            });
            return context;
        } else
        {
            com.baidu.mobstat.m.a().b("android OsVerson get failed");
            return "";
        }
    }

    public static String f(Context context)
    {
        return (new StringBuilder()).append("Android").append(CooperService.getOSVersion()).toString();
    }

    public static String g(Context context)
    {
        if (o(context))
        {
            context = Build.MANUFACTURER;
            com.baidu.mobstat.m.a().a(new Object[] {
                (new StringBuilder()).append("manufacturer_info : ").append(context).toString()
            });
            return context;
        } else
        {
            com.baidu.mobstat.m.a().b("android manufacturer get failed!");
            return "";
        }
    }

    public static String h(Context context)
    {
        return CooperService.getDeviceId((TelephonyManager)context.getSystemService("phone"), context);
    }

    public static String i(Context context)
    {
        Object obj;
        String s;
        s = "";
        obj = s;
        if (!ar.e(context, "android.permission.ACCESS_WIFI_STATE"))
        {
            break MISSING_BLOCK_LABEL_39;
        }
        obj = s;
        try
        {
            context = ((WifiManager)context.getSystemService("wifi")).getConnectionInfo().getMacAddress();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return ((String) (obj));
        }
        obj = context;
        return CooperService.getSecretValue(context);
        obj = s;
        com.baidu.mobstat.m.a().b("You need the android.Manifest.permission.ACCESS_WIFI_STATE permission. Open AndroidManifest.xml and just before the final </manifest> tag add:android.permission.ACCESS_WIFI_STATE");
        return "";
    }

    public static String j(Context context)
    {
        if (a(context, "android.permission.READ_PHONE_STATE"))
        {
            String s = "";
            if (o(context))
            {
                context = (TelephonyManager)context.getSystemService("phone");
                s = (new StringBuilder()).append(context.getPhoneType()).append("").toString();
            }
            if (s.length() != 0)
            {
                com.baidu.mobstat.m.a().a(new Object[] {
                    (new StringBuilder()).append("phoneType : ").append(s).toString()
                });
                return s;
            } else
            {
                com.baidu.mobstat.m.a().a("phoneType get nothing");
                return "";
            }
        } else
        {
            com.baidu.mobstat.m.a().b("lost permissioin : android.permission.READ_PHONE_STATE");
            return "";
        }
    }

    public static String k(Context context)
    {
        return CooperService.getCUID(context, true);
    }

    public static String l(Context context)
    {
        if (a(context, "android.permission.READ_PHONE_STATE"))
        {
            String s = "";
            if (o(context))
            {
                s = ((TelephonyManager)context.getSystemService("phone")).getDeviceId();
            }
            if (s != null)
            {
                com.baidu.mobstat.m.a().a(new Object[] {
                    (new StringBuilder()).append("Imei:").append(s).toString()
                });
                return CooperService.getSecretValue(s);
            } else
            {
                com.baidu.mobstat.m.a().a("Imei is null");
                return "";
            }
        } else
        {
            com.baidu.mobstat.m.a().b("lost permissioin : android.permission.READ_PHONE_STATE");
            return "";
        }
    }

    public static NetworkStatus m(Context context)
    {
        ConnectivityManager connectivitymanager = (ConnectivityManager)context.getSystemService("connectivity");
        if (connectivitymanager == null)
        {
            return NetworkStatus.NotReachable;
        }
        NetworkInfo networkinfo = connectivitymanager.getActiveNetworkInfo();
        if (networkinfo == null || !networkinfo.isAvailable())
        {
            return NetworkStatus.NotReachable;
        }
        context = (TelephonyManager)context.getSystemService("phone");
        if (connectivitymanager.getActiveNetworkInfo().getType() == 1)
        {
            return NetworkStatus.Wifi;
        }
        switch (context.getNetworkType())
        {
        case 8: // '\b'
        case 9: // '\t'
        case 10: // '\n'
        default:
            return NetworkStatus.TwoG;

        case 7: // '\007'
            return NetworkStatus.TwoG;

        case 4: // '\004'
            return NetworkStatus.TwoG;

        case 2: // '\002'
            return NetworkStatus.TwoG;

        case 5: // '\005'
            return NetworkStatus.ThreeG;

        case 6: // '\006'
            return NetworkStatus.ThreeG;

        case 1: // '\001'
            return NetworkStatus.TwoG;

        case 3: // '\003'
            return NetworkStatus.ThreeG;

        case 14: // '\016'
            return NetworkStatus.ThreeG;

        case 12: // '\f'
            return NetworkStatus.ThreeG;

        case 15: // '\017'
            return NetworkStatus.ThreeG;

        case 11: // '\013'
            return NetworkStatus.TwoG;

        case 13: // '\r'
            return NetworkStatus.ThreeG;

        case 0: // '\0'
            return NetworkStatus.TwoG;
        }
    }

    public static String n(Context context)
    {
        Object obj;
        int i1;
        int j1;
        int k1;
        k1 = android.os.Build.VERSION.SDK_INT;
        obj = new DisplayMetrics();
        context = ((WindowManager)context.getSystemService("window")).getDefaultDisplay();
        context.getMetrics(((DisplayMetrics) (obj)));
        i1 = ((DisplayMetrics) (obj)).widthPixels;
        j1 = ((DisplayMetrics) (obj)).heightPixels;
        com.baidu.mobstat.m.a().a(new Object[] {
            (new StringBuilder()).append("Run1 first get resolution:").append(i1).append(" * ").append(j1).append(",ver ").append(k1).toString()
        });
        if (k1 >= 13) goto _L2; else goto _L1
_L1:
        k1 = ((DisplayMetrics) (obj)).heightPixels;
        j1 = i1;
        i1 = k1;
_L7:
        com.baidu.mobstat.m.a().a(new Object[] {
            (new StringBuilder()).append("Run2 Calibration resolution:").append(j1).append(" * ").append(i1).toString()
        });
        return (new StringBuilder()).append(j1).append("*").append(i1).toString();
_L2:
        if (k1 == 13)
        {
            try
            {
                k1 = ((Integer)context.getClass().getMethod("getRealHeight", new Class[0]).invoke(context, new Object[0])).intValue();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context.printStackTrace();
                k1 = i1;
                i1 = j1;
                j1 = k1;
                continue; /* Loop/switch isn't completed */
            }
            j1 = k1;
            k1 = i1;
            i1 = j1;
            j1 = k1;
            continue; /* Loop/switch isn't completed */
        }
        if (k1 <= 13) goto _L4; else goto _L3
_L3:
        k1 = i1;
        obj = context.getClass().getMethod("getSize", new Class[] {
            android/graphics/Point
        });
        k1 = i1;
        Point point = new Point();
        k1 = i1;
        ((Method) (obj)).invoke(context, new Object[] {
            point
        });
        k1 = i1;
        i1 = point.x;
        k1 = i1;
        int i2 = point.y;
        com.baidu.mobstat.m.a().a(new Object[] {
            (new StringBuilder()).append("ver>13 resolution : ").append(i1).append(" * ").append(i2).toString()
        });
        j1 = i1;
        i1 = i2;
        continue; /* Loop/switch isn't completed */
        context;
        i1 = j1;
        j1 = k1;
_L5:
        context.printStackTrace();
        continue; /* Loop/switch isn't completed */
        context;
        j1 = i1;
        i1 = i2;
        if (true) goto _L5; else goto _L4
_L4:
        int l1 = i1;
        i1 = j1;
        j1 = l1;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public static boolean o(Context context)
    {
        return context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) == 0;
    }

    public static boolean p(Context context)
    {
        if (a(context, "android.permission.INTERNET"))
        {
            context = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (context != null && context.isAvailable())
            {
                return true;
            } else
            {
                com.baidu.mobstat.m.a().b("Network error");
                return false;
            }
        } else
        {
            com.baidu.mobstat.m.a().b(" lost  permission : android.permission.INTERNET");
            return false;
        }
    }
}
