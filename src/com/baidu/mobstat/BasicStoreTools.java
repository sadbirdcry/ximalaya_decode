// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;

// Referenced classes of package com.baidu.mobstat:
//            BasicStoreToolsBase

public class BasicStoreTools extends BasicStoreToolsBase
{

    public static final String APP_ANALYSIS_EXCEPTION = "exceptionanalysisflag";
    public static final String APP_ANALYSIS_EXCEPTION_TAG = "exceptionanalysistag";
    public static final String APP_MAC_ADDRESS = "mtjsdkmacss";
    public static final String APP_SET_APPKEY = "mjsetappkey";
    public static final String APP_SET_CHANNEL = "setchannelwithcodevalue";
    public static final String APP_SET_CHANNEL_WITH_CODE = "setchannelwithcode";
    public static final String DEVICE_CUID = "cuidsec";
    public static final String DEVICE_ID = "device_id";
    public static final String LAST_SEND_TIME = "lastsendtime";
    public static final String ONLY_WIFI = "onlywifi";
    public static final String SEND_LOG_TYPE = "sendLogtype";
    public static final String TIME_INTERVAL = "timeinterval";
    static BasicStoreTools a = new BasicStoreTools();

    private BasicStoreTools()
    {
    }

    public static BasicStoreTools getInstance()
    {
        return a;
    }

    protected boolean getAppChannelWithCode(Context context)
    {
        return getBoolean(context, "setchannelwithcode", false);
    }

    protected String getAppChannelWithPreference(Context context)
    {
        return getString(context, "setchannelwithcodevalue", null);
    }

    protected String getAppDeviceMac(Context context)
    {
        return getString(context, "mtjsdkmacss", null);
    }

    protected String getAppKey(Context context)
    {
        return getString(context, "mjsetappkey", null);
    }

    protected String getExceptionHeadTag(Context context)
    {
        return getString(context, "exceptionanalysistag", null);
    }

    protected boolean getExceptionTurn(Context context)
    {
        return getBoolean(context, "exceptionanalysisflag", false);
    }

    protected String getGenerateDeviceCUID(Context context)
    {
        return getString(context, "cuidsec", null);
    }

    protected String getGenerateDeviceId(Context context)
    {
        return getString(context, "device_id", null);
    }

    protected long getLastSendTime(Context context)
    {
        return getLong(context, "lastsendtime", 0L);
    }

    protected boolean getOnlyWifiChannel(Context context)
    {
        return getBoolean(context, "onlywifi", false);
    }

    protected int getSendStrategy(Context context)
    {
        return getInt(context, "sendLogtype", 0);
    }

    protected int getSendStrategyTime(Context context)
    {
        return getInt(context, "timeinterval", 1);
    }

    protected void setAppChannelWithCode(Context context, boolean flag)
    {
        putBoolean(context, "setchannelwithcode", flag);
    }

    protected void setAppChannelWithPreference(Context context, String s)
    {
        putString(context, "setchannelwithcodevalue", s);
    }

    protected void setAppDeviceMac(Context context, String s)
    {
        putString(context, "mtjsdkmacss", s);
    }

    protected void setAppKey(Context context, String s)
    {
        putString(context, "mjsetappkey", s);
    }

    protected void setExceptionHeadTag(Context context, String s)
    {
        putString(context, "exceptionanalysistag", s);
    }

    protected void setExceptionTurn(Context context, boolean flag)
    {
        putBoolean(context, "exceptionanalysisflag", flag);
    }

    protected void setGenerateDeviceCUID(Context context, String s)
    {
        if (getString(context, "cuid", null) != null)
        {
            removeString(context, "cuid");
        }
        putString(context, "cuidsec", s);
    }

    protected void setGenerateDeviceId(Context context, String s)
    {
        putString(context, "device_id", s);
    }

    protected void setLastSendTime(Context context, long l)
    {
        putLong(context, "lastsendtime", l);
    }

    protected void setOnlyWifi(Context context, boolean flag)
    {
        putBoolean(context, "onlywifi", flag);
    }

    protected void setSendStrategy(Context context, int i)
    {
        putInt(context, "sendLogtype", i);
    }

    protected void setSendStrategyTime(Context context, int i)
    {
        putInt(context, "timeinterval", i);
    }

}
