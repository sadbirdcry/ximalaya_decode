// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONObject;

// Referenced classes of package com.baidu.mobstat:
//            BasicStoreToolsBase, r, ao, au, 
//            at, BasicStoreTools, c, ap, 
//            as

public class CooperService extends BasicStoreToolsBase
{

    private static r a = new r();
    private static JSONObject b = new JSONObject();
    private static String c = "activehead";
    private static HashMap d = new HashMap();

    public CooperService()
    {
    }

    static r a()
    {
        return a;
    }

    private static String a(Context context)
    {
        String s = ao.g(context);
        context = s;
        if (s != null)
        {
            context = s.replaceAll(":", "");
        }
        return context;
    }

    private static String a(String s, Context context)
    {
        String s1;
        if (s == null)
        {
            s1 = null;
        } else
        {
            s1 = s;
            if (s.equals("000000000000000"))
            {
                s = a(context);
                au.a().a("statsdk", (new StringBuilder()).append("imei=null,mac=").append(s).toString());
                return s;
            }
        }
        return s1;
    }

    public static boolean checkCellLocationSetting(Context context)
    {
        context = ao.a(context, "BaiduMobAd_CELL_LOCATION");
        return context == null || !context.toLowerCase().equals("false");
    }

    public static boolean checkGPSLocationSetting(Context context)
    {
        context = ao.a(context, "BaiduMobAd_GPS_LOCATION");
        return context == null || !context.toLowerCase().equals("false");
    }

    public static boolean checkWifiLocationSetting(Context context)
    {
        context = ao.a(context, "BaiduMobAd_WIFI_LOCATION");
        return context == null || !context.toLowerCase().equals("false");
    }

    public static String getAppChannel(Context context)
    {
        boolean flag;
        if (a.j != null && !a.j.equals(""))
        {
            break MISSING_BLOCK_LABEL_87;
        }
        flag = BasicStoreTools.getInstance().getAppChannelWithCode(context);
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_48;
        }
        a.j = BasicStoreTools.getInstance().getAppChannelWithPreference(context);
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_75;
        }
        if (a.j != null && !a.j.equals(""))
        {
            break MISSING_BLOCK_LABEL_87;
        }
        a.j = ao.a(context, "BaiduMobAd_CHANNEL");
_L2:
        return a.j;
        context;
        au.a().a(new Object[] {
            context
        });
        if (true) goto _L2; else goto _L1
_L1:
    }

    public static String getAppKey(Context context)
    {
        if (a.c == null)
        {
            a.c = ao.a(context, "BaiduMobAd_STAT_ID");
        }
        return a.c;
    }

    public static int getAppVersionCode(Context context)
    {
        if (a.e == -1)
        {
            a.e = ao.c(context);
        }
        return a.e;
    }

    public static String getAppVersionName(Context context)
    {
        if (a.f == null || "".equals(a.f))
        {
            a.f = ao.d(context);
        }
        return a.f;
    }

    public static String getCUID(Context context, boolean flag)
    {
        if (a.d == null)
        {
            a.d = BasicStoreTools.getInstance().getGenerateDeviceCUID(context);
            if (a.d == null || "".equalsIgnoreCase(a.d))
            {
                try
                {
                    a.d = com.baidu.mobstat.c.a(context);
                    Matcher matcher = Pattern.compile("\\s*|\t|\r|\n").matcher(a.d);
                    a.d = matcher.replaceAll("");
                    a.d = as.a(ap.a("30212102dicudiab", "30212102dicudiab", a.d.getBytes()), "utf-8");
                    au.a().a("sdkstat", (new StringBuilder()).append("\u52A0\u5BC6=mHeadObject.cuid=").append(a.d).toString());
                    BasicStoreTools.getInstance().setGenerateDeviceCUID(context, a.d);
                }
                // Misplaced declaration of an exception variable
                catch (Context context)
                {
                    au.a().c(new Object[] {
                        "sdkstat", context.getMessage()
                    });
                }
            }
        }
        if (flag)
        {
            return a.d;
        }
        if (a.d == null)
        {
            break MISSING_BLOCK_LABEL_236;
        }
        context = new String(ap.b("30212102dicudiab", "30212102dicudiab", as.a(a.d.getBytes())));
        return context;
        context;
        context.printStackTrace();
        return null;
    }

    public static String getDeviceId(TelephonyManager telephonymanager, Context context)
    {
        Object obj;
        if (telephonymanager == null)
        {
            return a.g;
        }
        obj = a.g;
        if (obj != null && !((String) (obj)).equals("")) goto _L2; else goto _L1
_L1:
        Pattern pattern = Pattern.compile("\\s*|\t|\r|\n");
        telephonymanager = pattern.matcher(telephonymanager.getDeviceId()).replaceAll("");
        obj = telephonymanager;
        telephonymanager = a(telephonymanager, context);
        obj = telephonymanager;
_L4:
label0:
        {
            telephonymanager = ((TelephonyManager) (obj));
            if (obj == null)
            {
                telephonymanager = a(context);
            }
            if (telephonymanager != null)
            {
                obj = telephonymanager;
                if (!telephonymanager.equals("000000000000000"))
                {
                    break label0;
                }
            }
            obj = BasicStoreTools.getInstance().getGenerateDeviceId(context);
        }
label1:
        {
            if (obj != null)
            {
                telephonymanager = ((TelephonyManager) (obj));
                if (!((String) (obj)).equals("000000000000000"))
                {
                    break label1;
                }
            }
            telephonymanager = (new StringBuilder()).append((new Date()).getTime()).append("").toString();
            telephonymanager = (new StringBuilder()).append("hol").append(telephonymanager.hashCode()).append("mes").toString();
            BasicStoreTools.getInstance().setGenerateDeviceId(context, telephonymanager);
            au.a().a("statsdk", (new StringBuilder()).append("\u8BBE\u5907id\u4E3A\u7A7A\uFF0C\u7CFB\u7EDF\u751F\u6210id =").append(telephonymanager).toString());
        }
        a.g = telephonymanager;
        a.g = getSecretValue(a.g);
        au.a().a("sdkstat", (new StringBuilder()).append("\u52A0\u5BC6=mHeadObject.device_id=").append(a.g).toString());
_L2:
        try
        {
            telephonymanager = new String(ap.b("30212102dicudiab", "30212102dicudiab", as.a(a.g.getBytes())));
            au.a().a("sdkstat", (new StringBuilder()).append("device_id=").append(telephonymanager).toString());
        }
        // Misplaced declaration of an exception variable
        catch (TelephonyManager telephonymanager)
        {
            telephonymanager.printStackTrace();
        }
        return a.g;
        telephonymanager;
        au.a().a(new Object[] {
            telephonymanager
        });
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static String getLinkedWay(Context context)
    {
        if (a.p == null || "".equals(a.p))
        {
            a.p = ao.i(context);
        }
        return a.p;
    }

    public static String getMTJSDKVersion()
    {
        return "3.4.2.2";
    }

    public static String getMacID(Context context)
    {
        if (a.q == null || "".equals(a.q))
        {
            String s = BasicStoreTools.getInstance().getAppDeviceMac(context);
            if (s == null)
            {
                s = a(context);
                if (s != null)
                {
                    a.q = getSecretValue(s);
                    au.a().a("sdkstat", (new StringBuilder()).append("\u52A0\u5BC6=mHeadObject.mHeadObject.mac_addr=").append(a.q).toString());
                    if (a.q != "")
                    {
                        BasicStoreTools.getInstance().setAppDeviceMac(context, a.q);
                    }
                }
            } else
            {
                a.q = s;
            }
        }
        return a.q;
    }

    public static String getOSVersion()
    {
        if (a.b == null || "".equals(a.b))
        {
            a.b = android.os.Build.VERSION.SDK;
        }
        return a.b;
    }

    public static String getOperator(TelephonyManager telephonymanager)
    {
        if (a.k == null || "".equals(a.k))
        {
            a.k = telephonymanager.getNetworkOperator();
        }
        return a.k;
    }

    public static String getPhoneModel()
    {
        if (a.l == null || "".equals(a.l))
        {
            a.l = Build.MODEL;
        }
        return a.l;
    }

    public static String getSecretValue(String s)
    {
        String s1 = null;
        s = as.a(ap.a("30212102dicudiab", "30212102dicudiab", s.getBytes()), "utf-8");
        s1 = s;
        try
        {
            au.a().a("sdkstat", (new StringBuilder()).append("\u52A0\u5BC6=secretValue=").append(s).toString());
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = s1;
        }
        s1 = s;
        if (s == null)
        {
            s1 = "";
        }
        return s1;
    }

    public static int getTagValue()
    {
        return 2;
    }

}
