// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.baidu.mobstat:
//            au, at, ar, CooperService, 
//            r, ag, ao, aa, 
//            BasicStoreTools

public class DataCore
{

    private static JSONObject b = new JSONObject();
    private static DataCore h = new DataCore();
    private int a;
    private JSONArray c;
    private JSONArray d;
    private JSONArray e;
    private JSONArray f;
    private boolean g;

    private DataCore()
    {
        a = 0;
        c = new JSONArray();
        d = new JSONArray();
        e = new JSONArray();
        f = new JSONArray();
        g = false;
    }

    static JSONObject a()
    {
        return b;
    }

    private void a(boolean flag)
    {
        g = flag;
    }

    private boolean b()
    {
        return g;
    }

    public static DataCore getInstance()
    {
        return h;
    }

    public void flush(Context context)
    {
        Object obj;
        au.a().a("statsdk", "flush cache to __local_stat_cache.json");
        obj = new JSONObject();
        synchronized (c)
        {
            ((JSONObject) (obj)).put("pr", new JSONArray(c.toString()));
        }
        synchronized (d)
        {
            ((JSONObject) (obj)).put("ev", new JSONArray(d.toString()));
        }
        synchronized (f)
        {
            ((JSONObject) (obj)).put("ex", new JSONArray(f.toString()));
        }
_L2:
        obj = ((JSONObject) (obj)).toString();
        if (b())
        {
            au.a().a("statsdk", "cache.json exceed 204800B,stop flush.");
            return;
        }
        break; /* Loop/switch isn't completed */
        exception;
        obj1;
        JVM INSTR monitorexit ;
        try
        {
            throw exception;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            au.a().a("statsdk", "flushLogWithoutHeader() construct cache error");
        }
        if (true) goto _L2; else goto _L1
        exception1;
        obj1;
        JVM INSTR monitorexit ;
        throw exception1;
        exception2;
        obj1;
        JVM INSTR monitorexit ;
        throw exception2;
_L1:
        int i = ((String) (obj)).getBytes().length;
        if (i >= 0x32000)
        {
            a(true);
            return;
        } else
        {
            a = i;
            au.a().a("statsdk", (new StringBuilder()).append("flush:cacheFileSize is:").append(a).toString());
            ar.a(false, context, "__local_stat_cache.json", ((String) (obj)), false);
            return;
        }
    }

    public void getMemInfo(Context context)
    {
    }

    public void installHeader(Context context)
    {
        this;
        JVM INSTR monitorenter ;
        CooperService.a().b(context);
        this;
        JVM INSTR monitorexit ;
        return;
        context;
        throw context;
    }

    public boolean isPartEmpty()
    {
        return c.length() == 0 && d.length() == 0 && f.length() == 0;
    }

    public void loadLastSession(Context context)
    {
        au.a().a("statsdk", "LoadLastSession()");
        while (context == null || !ar.c(context, "__local_last_session.json")) 
        {
            return;
        }
        String s = ar.a(false, context, "__local_last_session.json");
        if (s.equals(""))
        {
            au.a().a("statsdk", "loadLastSession(): last_session.json file not found.");
            return;
        } else
        {
            ar.a(false, context, "__local_last_session.json", (new JSONObject()).toString(), false);
            putSession(s);
            flush(context);
            return;
        }
    }

    public void loadStatData(Context context)
    {
        boolean flag;
        flag = false;
        break MISSING_BLOCK_LABEL_3;
_L10:
        int i;
        long l;
        do
        {
            return;
        } while (context == null || !ar.c(context, "__local_stat_cache.json"));
        Object obj = ar.a(false, context, "__local_stat_cache.json");
        if (((String) (obj)).equals(""))
        {
            au.a().a("statsdk", "stat_cache file not found.");
            return;
        }
        au.a().a("statsdk", "loadStatData, ");
        JSONObject jsonobject;
        try
        {
            a = ((String) (obj)).getBytes().length;
            au.a().a("statsdk", (new StringBuilder()).append("load Stat Data:cacheFileSize is:").append(a).toString());
            context = new JSONObject(((String) (obj)));
            au.a().a("statsdk", (new StringBuilder()).append("Load cache:").append(((String) (obj))).toString());
            l = System.currentTimeMillis();
            obj = context.getJSONArray("pr");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            au.a().a("statsdk", (new StringBuilder()).append("Load stat data error:").append(context).toString());
            return;
        }
        i = 0;
_L11:
        if (i >= ((JSONArray) (obj)).length()) goto _L2; else goto _L1
_L1:
        jsonobject = ((JSONArray) (obj)).getJSONObject(i);
        if (l - jsonobject.getLong("s") <= 0x240c8400L) goto _L4; else goto _L3
_L4:
        putSession(jsonobject, true);
          goto _L3
_L2:
        Object obj1 = context.getJSONArray("ev");
        i = 0;
_L12:
        if (i >= ((JSONArray) (obj1)).length()) goto _L6; else goto _L5
_L5:
        JSONObject jsonobject1 = ((JSONArray) (obj1)).getJSONObject(i);
        if (l - jsonobject1.getLong("t") <= 0x240c8400L) goto _L8; else goto _L7
_L8:
        putEvent(jsonobject1, true);
          goto _L7
_L6:
        context = context.getJSONArray("ex");
        i = ((flag) ? 1 : 0);
_L13:
        if (i >= context.length()) goto _L10; else goto _L9
_L9:
        obj1 = context.getJSONObject(i);
        if (l - ((JSONObject) (obj1)).getLong("t") > 0x240c8400L)
        {
            break MISSING_BLOCK_LABEL_350;
        }
        putException(((JSONObject) (obj1)), true);
        break MISSING_BLOCK_LABEL_350;
_L3:
        i++;
          goto _L11
_L7:
        i++;
          goto _L12
        i++;
          goto _L13
    }

    public void putEvent(String s, String s1, int i, long l, long l1)
    {
        JSONObject jsonobject = new JSONObject();
        try
        {
            jsonobject.put("i", s);
            jsonobject.put("l", s1);
            jsonobject.put("c", i);
            jsonobject.put("t", l);
            jsonobject.put("d", l1);
            putEvent(jsonobject, false);
            au.a().a("statsdk", (new StringBuilder()).append("put event:").append(jsonobject.toString()).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            au.a().a("statsdk", s);
        }
    }

    public void putEvent(JSONObject jsonobject, boolean flag)
    {
        Object obj;
        Object obj1;
        JSONArray jsonarray;
        String s;
        String s1;
        JSONObject jsonobject1;
        int j;
        int k;
        int l;
        int k1;
        if (jsonobject != null && !flag)
        {
            int i = jsonobject.toString().getBytes().length;
            au.a().a(new Object[] {
                "statsdk", "putEvent:eventSize is:", Integer.valueOf(i)
            });
            if (i + a > 0x32000)
            {
                au.a().a("statsdk", "putEvent: size is full!");
                return;
            }
        }
        j = 0;
        int i1;
        long l1;
        long l2;
        try
        {
            s = jsonobject.getString("i");
            s1 = jsonobject.getString("l");
            l1 = jsonobject.getLong("t") / 0x36ee80L;
            obj = jsonobject.optString("s");
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject)
        {
            au.a().a("statsdk", jsonobject);
            return;
        }
        k = jsonobject.getInt("d");
        j = k;
_L10:
        if (j != 0) goto _L2; else goto _L1
_L1:
        jsonarray = d;
        jsonarray;
        JVM INSTR monitorenter ;
        k1 = d.length();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_157;
        }
        if (!((String) (obj)).equals(""))
        {
            break MISSING_BLOCK_LABEL_167;
        }
        jsonobject.put("s", "0|");
_L11:
        j = 0;
        k = k1;
_L9:
        if (j >= k1) goto _L4; else goto _L3
_L3:
        jsonobject1 = d.getJSONObject(j);
        au.a().a("statsdk", (new StringBuilder()).append(";event_iter=").append(jsonobject1.toString()).toString());
        obj = jsonobject1.getString("i");
        obj1 = jsonobject1.getString("l");
        l2 = jsonobject1.getLong("t") / 0x36ee80L;
        i1 = 0;
        l = jsonobject1.getInt("d");
        i1 = l;
_L12:
        l = k;
        if (l2 != l1) goto _L6; else goto _L5
_L5:
        if (i1 == 0) goto _L8; else goto _L7
_L7:
        l = k;
_L6:
        j++;
        k = l;
          goto _L9
        obj1;
        au.a().a("statsdk", "old version data, No duration Tag");
          goto _L10
        obj;
        au.a().a("statsdk", "event put s fail");
          goto _L11
        jsonobject;
        jsonarray;
        JVM INSTR monitorexit ;
        throw jsonobject;
        JSONException jsonexception;
        jsonexception;
        au.a().a("statsdk", "old version data, No duration Tag");
          goto _L12
        obj;
_L17:
        au.a().a("statsdk", ((Throwable) (obj)));
        l = k;
          goto _L6
_L8:
        au.a().a("statsdk", (new StringBuilder()).append(";event_iter=").append(jsonobject1.toString()).toString());
        l = k;
        if (!((String) (obj)).equals(s)) goto _L6; else goto _L13
_L13:
        l = k;
        if (!((String) (obj1)).equals(s1)) goto _L6; else goto _L14
_L14:
        int j1;
        l = jsonobject.getInt("c");
        j1 = jsonobject1.getInt("c");
        obj1 = jsonobject1.optString("s");
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_688;
        }
        obj = obj1;
        if (((String) (obj1)).equalsIgnoreCase(""))
        {
            break MISSING_BLOCK_LABEL_688;
        }
_L19:
        long l3 = jsonobject.getLong("t");
        long l4 = jsonobject1.getLong("t");
        obj = (new StringBuilder()).append(((String) (obj))).append(l3 - l4).append("|").toString();
        jsonobject1.remove("c");
        jsonobject1.put("c", j1 + l);
        jsonobject1.put("s", obj);
_L18:
        if (j >= k1)
        {
            break MISSING_BLOCK_LABEL_585;
        }
        jsonarray;
        JVM INSTR monitorexit ;
        return;
        d.put(k1, jsonobject);
_L15:
        jsonarray;
        JVM INSTR monitorexit ;
        return;
        jsonobject;
        au.a().a("statsdk", jsonobject);
          goto _L15
_L2:
        obj = d;
        obj;
        JVM INSTR monitorenter ;
        j = d.length();
        jsonobject.put("s", "0");
        d.put(j, jsonobject);
_L16:
        return;
        jsonobject;
        obj;
        JVM INSTR monitorexit ;
        throw jsonobject;
        jsonobject;
        au.a().a("statsdk", jsonobject);
          goto _L16
        obj;
        k = j;
          goto _L17
_L4:
        j = k;
          goto _L18
        obj = "0|";
          goto _L19
    }

    public void putException(long l, String s, String s1)
    {
        JSONObject jsonobject = new JSONObject();
        jsonobject.put("t", l);
        jsonobject.put("y", s1);
        if (s.getBytes().length <= 5120) goto _L2; else goto _L1
_L1:
        s1 = new byte[5120];
        s.getBytes(0, 5120, s1, 0);
        int i = s1.length;
        au.a().a(new Object[] {
            (new StringBuilder()).append("exception bytes=").append(i).toString()
        });
        jsonobject.put("c", new String(s1));
_L4:
        putException(jsonobject, false);
        return;
_L2:
        try
        {
            jsonobject.put("c", s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            au.a().a("statsdk", s);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void putException(JSONObject jsonobject, boolean flag)
    {
        if (jsonobject != null && !flag)
        {
            int i = jsonobject.toString().getBytes().length;
            au.a().a(new Object[] {
                "statsdk", "putException:addSize is:", Integer.valueOf(i)
            });
            if (i + a > 0x32000)
            {
                au.a().a("statsdk", "putException: size is full!");
                return;
            }
        }
        JSONArray jsonarray = f;
        jsonarray;
        JVM INSTR monitorenter ;
        int j = f.length();
        f.put(j, jsonobject);
_L1:
        return;
        jsonobject;
        jsonarray;
        JVM INSTR monitorexit ;
        throw jsonobject;
        jsonobject;
        au.a().a("statsdk", jsonobject);
          goto _L1
    }

    public void putSession(String s)
    {
        if (s.equals("{}") || s.equals(""))
        {
            return;
        }
        try
        {
            s = new JSONObject(s);
            putSession(((JSONObject) (s)), false);
            au.a().a("statsdk", (new StringBuilder()).append("Load last session:").append(s).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            au.a().a("statsdk", (new StringBuilder()).append("putSession()").append(s).toString());
        }
    }

    public void putSession(JSONObject jsonobject, boolean flag)
    {
        if (jsonobject != null && !flag)
        {
            int i = jsonobject.toString().getBytes().length;
            au.a().a(new Object[] {
                "statsdk", "putSession:addSize is:", Integer.valueOf(i)
            });
            if (i + a > 0x32000)
            {
                au.a().a("statsdk", "putSession: size is full!");
                return;
            }
        }
        JSONArray jsonarray = c;
        jsonarray;
        JVM INSTR monitorenter ;
        int j = c.length();
        c.put(j, jsonobject);
_L1:
        return;
        jsonobject;
        jsonarray;
        JVM INSTR monitorexit ;
        throw jsonobject;
        jsonobject;
        au.a().a("statsdk", jsonobject);
          goto _L1
    }

    public boolean sendLogData(Context context)
    {
        Object obj;
        boolean flag;
        flag = false;
        au.a().a("statsdk", "sendLogData() begin.");
        if (CooperService.a() != null && (CooperService.a().c == null || "".equalsIgnoreCase(CooperService.a().c)))
        {
            CooperService.a().a(context);
            au.a().a("statsdk", "constructHeader() begin.");
            if (CooperService.a().c == null || "".equalsIgnoreCase(CooperService.a().c))
            {
                au.a().b("\u4E0D\u80FD\u5728manifest.xml\u4E2D\u627E\u5230APP Key||can't find app key in manifest.xml.");
                return false;
            }
        }
        obj = new JSONObject();
        JSONObject jsonobject = b;
        jsonobject;
        JVM INSTR monitorenter ;
        b.put("t", System.currentTimeMillis());
        b.put("ss", ag.b().d());
        ((JSONObject) (obj)).put("he", b);
        JSONArray jsonarray = c;
        jsonarray;
        JVM INSTR monitorenter ;
        ((JSONObject) (obj)).put("pr", c);
        JSONArray jsonarray1 = d;
        jsonarray1;
        JVM INSTR monitorenter ;
        ((JSONObject) (obj)).put("ev", d);
        JSONArray jsonarray2 = e;
        jsonarray2;
        JVM INSTR monitorenter ;
        JSONArray jsonarray3 = f;
        jsonarray3;
        JVM INSTR monitorenter ;
        ((JSONObject) (obj)).put("ex", f);
        obj = ((JSONObject) (obj)).toString();
        au.a().a("statsdk", (new StringBuilder()).append("---Send Data Is:").append(((String) (obj))).toString());
        ao.a(context, "http://hmma.baidu.com/app.gif", ((String) (obj)), 50000, 50000);
        flag = true;
_L1:
        au.a().a("statsdk", (new StringBuilder()).append("send log data over. result=").append(flag).append("data=").append(((String) (obj))).toString());
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_381;
        }
        a(false);
        f = new JSONArray();
        d = new JSONArray();
        c = new JSONArray();
        flush(context);
        ag.b().c();
        aa.a().c(context);
        jsonarray3;
        JVM INSTR monitorexit ;
        jsonarray2;
        JVM INSTR monitorexit ;
        jsonarray1;
        JVM INSTR monitorexit ;
        jsonarray;
        JVM INSTR monitorexit ;
        jsonobject;
        JVM INSTR monitorexit ;
        au.a().a("statsdk", "sendLogData() end.");
        return flag;
        context;
        au.a().a("statsdk", context);
        jsonobject;
        JVM INSTR monitorexit ;
        return false;
        context;
        jsonobject;
        JVM INSTR monitorexit ;
        throw context;
        context;
        au.a().a("statsdk", context.toString());
        jsonarray;
        JVM INSTR monitorexit ;
        jsonobject;
        JVM INSTR monitorexit ;
        return false;
        context;
        au.a().a("statsdk", context);
        jsonarray1;
        JVM INSTR monitorexit ;
        jsonarray;
        JVM INSTR monitorexit ;
        jsonobject;
        JVM INSTR monitorexit ;
        return false;
        context;
        au.a().a("statsdk", context);
        jsonarray3;
        JVM INSTR monitorexit ;
        jsonarray2;
        JVM INSTR monitorexit ;
        jsonarray1;
        JVM INSTR monitorexit ;
        jsonarray;
        JVM INSTR monitorexit ;
        jsonobject;
        JVM INSTR monitorexit ;
        return false;
        Exception exception;
        exception;
        au.a().c(new Object[] {
            "statsdk", (new StringBuilder()).append("send error++++++").append(exception).toString()
        });
          goto _L1
        context;
        jsonarray3;
        JVM INSTR monitorexit ;
        throw context;
        context;
        jsonarray2;
        JVM INSTR monitorexit ;
        throw context;
        context;
        jsonarray1;
        JVM INSTR monitorexit ;
        throw context;
        context;
        jsonarray;
        JVM INSTR monitorexit ;
        throw context;
    }

    public void setAppChannel(Context context, String s, boolean flag)
    {
        if (s == null || s.equals(""))
        {
            au.a().c(new Object[] {
                "sdkstat", "\u8BBE\u7F6E\u7684\u6E20\u9053\u4E0D\u80FD\u4E3A\u7A7A\u6216\u8005\u4E3Anull || The channel that you have been set is null or empty, please check it."
            });
        }
        CooperService.a().j = s;
        if (flag && s != null && !s.equals(""))
        {
            BasicStoreTools.getInstance().setAppChannelWithPreference(context, s);
            BasicStoreTools.getInstance().setAppChannelWithCode(context, true);
        }
        if (!flag)
        {
            BasicStoreTools.getInstance().setAppChannelWithPreference(context, "");
            BasicStoreTools.getInstance().setAppChannelWithCode(context, false);
        }
    }

    public void setAppChannel(String s)
    {
        if (s == null || s.equals(""))
        {
            au.a().c(new Object[] {
                "sdkstat", "\u8BBE\u7F6E\u7684\u6E20\u9053\u4E0D\u80FD\u4E3A\u7A7A\u6216\u8005\u4E3Anull || The channel that you have been set is null or empty, please check it."
            });
        }
        CooperService.a().j = s;
    }

    public void setAppKey(String s)
    {
        CooperService.a().c = s;
    }

}
