// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;


public final class SendStrategyEnum extends Enum
{

    public static final SendStrategyEnum APP_START;
    public static final SendStrategyEnum ONCE_A_DAY;
    public static final SendStrategyEnum SET_TIME_INTERVAL;
    private static final SendStrategyEnum a[];

    private SendStrategyEnum(String s, int i)
    {
        super(s, i);
    }

    public static SendStrategyEnum valueOf(String s)
    {
        return (SendStrategyEnum)Enum.valueOf(com/baidu/mobstat/SendStrategyEnum, s);
    }

    public static SendStrategyEnum[] values()
    {
        return (SendStrategyEnum[])a.clone();
    }

    static 
    {
        APP_START = new SendStrategyEnum("APP_START", 0);
        ONCE_A_DAY = new SendStrategyEnum("ONCE_A_DAY", 1);
        SET_TIME_INTERVAL = new SendStrategyEnum("SET_TIME_INTERVAL", 2);
        a = (new SendStrategyEnum[] {
            APP_START, ONCE_A_DAY, SET_TIME_INTERVAL
        });
    }
}
