// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import android.location.LocationManager;
import java.io.IOException;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.baidu.mobstat:
//            h, f, CooperService, m, 
//            at, k, l, o, 
//            g

public class i extends h
{

    private String g;
    private JSONObject h;
    private JSONObject i;
    private JSONObject j;
    private boolean k;

    public i(Context context, String s)
    {
        super(context, s);
        c = getClass().getName();
    }

    private JSONObject a(Context context)
    {
        JSONObject jsonobject;
        LocationManager locationmanager;
        locationmanager = (LocationManager)context.getSystemService("location");
        jsonobject = new JSONObject();
        jsonobject.put("appkey", com.baidu.mobstat.f.b(context));
        jsonobject.put("channel", com.baidu.mobstat.f.a(a));
        jsonobject.put("os_version", com.baidu.mobstat.f.e(context));
        jsonobject.put("manufacturer", com.baidu.mobstat.f.g(context));
        jsonobject.put("phone_type", com.baidu.mobstat.f.j(context));
        jsonobject.put("deviceid", com.baidu.mobstat.f.h(context));
        jsonobject.put("imei", com.baidu.mobstat.f.l(context));
        jsonobject.put("resolution", com.baidu.mobstat.f.n(context));
        jsonobject.put("platform", "android");
        jsonobject.put("is_mobile_device", true);
        jsonobject.put("language", Locale.getDefault().getLanguage());
        jsonobject.put("modulename", CooperService.getPhoneModel());
        jsonobject.put("wifimac", com.baidu.mobstat.f.i(context));
        boolean flag;
        if (locationmanager == null)
        {
            flag = false;
        } else
        {
            flag = true;
        }
        try
        {
            jsonobject.put("havegps", flag);
            jsonobject.put("os_sdk", com.baidu.mobstat.f.f(context));
            jsonobject.put("tg", CooperService.getTagValue());
            jsonobject.put("cuid", com.baidu.mobstat.f.k(context));
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
        m.a().a(new Object[] {
            (new StringBuilder()).append("Satic Data : ").append(jsonobject.toString()).toString()
        });
        return jsonobject;
    }

    private JSONObject b(Context context)
    {
        JSONObject jsonobject = new JSONObject();
        try
        {
            jsonobject.put("time", com.baidu.mobstat.f.a());
            jsonobject.put("version_name", com.baidu.mobstat.f.c(context));
            jsonobject.put("version_code", com.baidu.mobstat.f.d(context));
            jsonobject.put("network_type", com.baidu.mobstat.f.m(context));
            jsonobject.put("latlongitude", com.baidu.mobstat.f.a(context, CooperService.checkGPSLocationSetting(context)));
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
        m.a().a(new Object[] {
            (new StringBuilder()).append("Dyna Data : ").append(jsonobject.toString()).toString()
        });
        return jsonobject;
    }

    private boolean g()
    {
        g = (new StringBuilder()).append("kirin_static_data_").append(a.getPackageName()).toString();
        i = com.baidu.mobstat.k.a(a, g);
        if (i == null)
        {
            m.a().a(new Object[] {
                "Static file is empty, need collect static data!"
            });
            h = a(a);
        } else
        {
            h = a(a);
            if (o.a(l.a(i.toString())).equals(o.a(l.a(h.toString()))))
            {
                return false;
            }
        }
        return true;
    }

    protected void b()
    {
        k = g();
        try
        {
            j = b(a);
            d = l.a(d, h);
            d = l.a(d, j);
            if (k)
            {
                m.a().a(new Object[] {
                    "send new static data!"
                });
                d.put("isUpdateClientData", "1");
                return;
            }
        }
        catch (Exception exception)
        {
            m.a().a(new Object[] {
                (new StringBuilder()).append("what's going on?? : ").append(exception.toString()).toString()
            });
            exception.printStackTrace();
            return;
        }
        m.a().a(new Object[] {
            "send cache static data!"
        });
        d.put("isUpdateClientData", "0");
        return;
    }

    protected void e()
    {
        m.a().a(new Object[] {
            (new StringBuilder()).append("isInfoChanged : ").append(k).append("  dump static data after success!!").toString()
        });
        if (k)
        {
            try
            {
                com.baidu.mobstat.k.a(a, g, h);
            }
            catch (IOException ioexception)
            {
                m.a().b("Dump static file has exception!!");
                ioexception.printStackTrace();
            }
        }
        try
        {
            Object obj = new JSONObject(e.getString("updateConfig"));
            com.baidu.mobstat.g.a(a).a(true, ((JSONObject) (obj)));
            obj = e.getString("logID");
            com.baidu.mobstat.g.a(a).b(((String) (obj)));
            return;
        }
        catch (JSONException jsonexception)
        {
            jsonexception.printStackTrace();
        }
        com.baidu.mobstat.g.a(a).a(false, null);
        com.baidu.mobstat.g.a(a).b("0");
    }

    protected void f()
    {
        if (com.baidu.mobstat.f.p(a))
        {
            com.baidu.mobstat.g.a(a).a(false, null);
        }
    }
}
