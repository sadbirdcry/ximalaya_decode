// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

// Referenced classes of package com.baidu.mobstat:
//            y, aa, au, at, 
//            z, s, ag, DataCore, 
//            aq, SendStrategyEnum

public class StatService
{

    public static final int EXCEPTION_LOG = 1;
    private static boolean a = false;

    public StatService()
    {
    }

    private static void a(Context context)
    {
        if (!a(context, "onError(...)"))
        {
            return;
        } else
        {
            y.a().a(context.getApplicationContext());
            aa.a().a(true, context.getApplicationContext());
            return;
        }
    }

    private static boolean a()
    {
        return a;
    }

    private static boolean a(Context context, String s1)
    {
        if (context == null)
        {
            au.a().c(new Object[] {
                "statsdk", (new StringBuilder()).append(s1).append(":context=null").toString()
            });
            return false;
        } else
        {
            return true;
        }
    }

    static boolean a(Class class1, String s1)
    {
        int i = 2;
        StackTraceElement astacktraceelement[] = (new Throwable()).getStackTrace();
        au.a().a(new Object[] {
            "isCalledBy", Integer.valueOf(astacktraceelement.length), class1, s1
        });
        if (astacktraceelement.length >= 2)
        {
            for (; i < astacktraceelement.length; i++)
            {
                if (astacktraceelement[i].getMethodName().equals(s1))
                {
                    return true;
                }
            }

        }
        return false;
    }

    private static void b()
    {
        a = true;
    }

    private static void b(Context context)
    {
        if (!z.a().b())
        {
            z.a().a(context.getApplicationContext());
        }
    }

    public static void onEvent(Context context, String s1, String s2)
    {
        onEvent(context, s1, s2, 1);
    }

    public static void onEvent(Context context, String s1, String s2, int i)
    {
        while (!a(context, "onEvent(...)") || s1 == null || s1.equals("")) 
        {
            return;
        }
        b(context);
        s.a().a(context.getApplicationContext(), s1, s2, i, System.currentTimeMillis());
    }

    public static void onEventDuration(Context context, String s1, String s2, long l)
    {
        while (!a(context, "onEventDuration(...)") || s1 == null || s1.equals("")) 
        {
            return;
        }
        if (l <= 0L)
        {
            au.a().b(new Object[] {
                "statsdk", "onEventDuration: duration must be greater than zero"
            });
            return;
        } else
        {
            b(context);
            s.a().c(context.getApplicationContext(), s1, s2, l);
            return;
        }
    }

    public static void onEventEnd(Context context, String s1, String s2)
    {
        while (!a(context, "onEventEnd(...)") || s1 == null || s1.equals("")) 
        {
            return;
        }
        b(context);
        s.a().b(context.getApplicationContext(), s1, s2, System.currentTimeMillis());
    }

    public static void onEventStart(Context context, String s1, String s2)
    {
        while (!a(context, "onEventStart(...)") || s1 == null || s1.equals("")) 
        {
            return;
        }
        b(context);
        s.a().a(context.getApplicationContext(), s1, s2, System.currentTimeMillis());
    }

    public static void onPageEnd(Context context, String s1)
    {
        com/baidu/mobstat/StatService;
        JVM INSTR monitorenter ;
        if (context == null || s1 == null) goto _L2; else goto _L1
_L1:
        if (!s1.equals("")) goto _L3; else goto _L2
_L2:
        au.a().c(new Object[] {
            "statsdk", "onPageEnd :parame=null || empty"
        });
_L5:
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        return;
_L3:
        au.a().a("sdkstat", (new StringBuilder()).append("*******onPageEnd=").append(s1).toString());
        ag.b().b(context, System.currentTimeMillis(), s1);
        if (true) goto _L5; else goto _L4
_L4:
        context;
        throw context;
    }

    public static void onPageStart(Context context, String s1)
    {
        com/baidu/mobstat/StatService;
        JVM INSTR monitorenter ;
        if (context == null || s1 == null) goto _L2; else goto _L1
_L1:
        if (!s1.equals("")) goto _L3; else goto _L2
_L2:
        au.a().c(new Object[] {
            "statsdk", "onPageStart :parame=null || empty"
        });
_L5:
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        return;
_L3:
        au.a().a("sdkstat", (new StringBuilder()).append("*******onPageStart=").append(s1).toString());
        b(context);
        ag.b().a(context, System.currentTimeMillis(), s1);
        if (true) goto _L5; else goto _L4
_L4:
        context;
        throw context;
    }

    public static void onPause(Context context)
    {
        com/baidu/mobstat/StatService;
        JVM INSTR monitorenter ;
        boolean flag = a(context, "onPause(...)");
        if (flag) goto _L2; else goto _L1
_L1:
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (!a(android/app/Activity, "onPause"))
        {
            throw new SecurityException("onPause(Context context)\u4E0D\u5728Activity.onPause()\u4E2D\u88AB\u8C03\u7528||onPause(Context context)is not called in Activity.onPause().");
        }
        break MISSING_BLOCK_LABEL_44;
        context;
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        throw context;
        ag.b().b(context, System.currentTimeMillis());
          goto _L1
    }

    public static void onPause(Fragment fragment)
    {
        com/baidu/mobstat/StatService;
        JVM INSTR monitorenter ;
        if (fragment != null) goto _L2; else goto _L1
_L1:
        au.a().c(new Object[] {
            "statsdk", "onResume :parame=null"
        });
_L3:
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (!a(android/support/v4/app/Fragment, "onPause"))
        {
            throw new SecurityException("Fragment onPause(Context context)\u4E0D\u5728Fragment.onPause()\u4E2D\u88AB\u8C03\u7528||onPause(Context context)is not called in Fragment.onPause().");
        }
        break MISSING_BLOCK_LABEL_58;
        fragment;
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        throw fragment;
        ag.b().b(fragment, System.currentTimeMillis());
          goto _L3
    }

    public static void onPause(Object obj)
    {
        com/baidu/mobstat/StatService;
        JVM INSTR monitorenter ;
        if (obj != null) goto _L2; else goto _L1
_L1:
        au.a().c(new Object[] {
            "statsdk", "android.app.Fragment onResume :parame=null"
        });
_L3:
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (!a(obj.getClass(), "onPause"))
        {
            throw new SecurityException("android.app.Fragment onPause(Context context)\u4E0D\u5728android.app.Fragment.onPause()\u4E2D\u88AB\u8C03\u7528||onPause(Context context)is not called in android.app.Fragment.onPause().");
        }
        break MISSING_BLOCK_LABEL_60;
        obj;
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        throw obj;
        ag.b().b(obj, System.currentTimeMillis());
          goto _L3
    }

    public static void onResume(Context context)
    {
        com/baidu/mobstat/StatService;
        JVM INSTR monitorenter ;
        boolean flag = a(context, "onResume(...)");
        if (flag) goto _L2; else goto _L1
_L1:
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (!a(android/app/Activity, "onResume"))
        {
            throw new SecurityException("onResume(Context context)\u4E0D\u5728Activity.onResume()\u4E2D\u88AB\u8C03\u7528||onResume(Context context)is not called in Activity.onResume().");
        }
        break MISSING_BLOCK_LABEL_44;
        context;
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        throw context;
        b(context);
        ag.b().a(context, System.currentTimeMillis());
          goto _L1
    }

    public static void onResume(Fragment fragment)
    {
        com/baidu/mobstat/StatService;
        JVM INSTR monitorenter ;
        if (fragment != null) goto _L2; else goto _L1
_L1:
        au.a().c(new Object[] {
            "statsdk", "onResume :parame=null"
        });
_L3:
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (!a(android/support/v4/app/Fragment, "onResume"))
        {
            throw new SecurityException("onResume(Context context)\u4E0D\u5728Activity.onResume()\u4E2D\u88AB\u8C03\u7528||onResume(Context context)is not called in Activity.onResume().");
        }
        break MISSING_BLOCK_LABEL_58;
        fragment;
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        throw fragment;
        b(fragment.getActivity());
        ag.b().a(fragment, System.currentTimeMillis());
          goto _L3
    }

    public static void onResume(Object obj)
    {
        com/baidu/mobstat/StatService;
        JVM INSTR monitorenter ;
        if (obj != null) goto _L2; else goto _L1
_L1:
        au.a().c(new Object[] {
            "statsdk", "onResume :parame=null"
        });
_L3:
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (!a(obj.getClass(), "onResume"))
        {
            throw new SecurityException("onResume(Context context)\u4E0D\u5728Activity.onResume()\u4E2D\u88AB\u8C03\u7528||onResume(Context context)is not called in Activity.onResume().");
        }
        break MISSING_BLOCK_LABEL_60;
        obj;
        com/baidu/mobstat/StatService;
        JVM INSTR monitorexit ;
        throw obj;
        b(ag.a(obj));
        ag.b().a(obj, System.currentTimeMillis());
          goto _L3
    }

    public static void setAppChannel(Context context, String s1, boolean flag)
    {
        DataCore.getInstance().setAppChannel(context, s1, flag);
    }

    public static void setAppChannel(String s1)
    {
        DataCore.getInstance().setAppChannel(s1);
    }

    public static void setAppKey(String s1)
    {
        DataCore.getInstance().setAppKey(s1);
    }

    public static void setDebugOn(boolean flag)
    {
        if (flag)
        {
            aq.a = 2;
            au.a().a(aq.a);
            return;
        } else
        {
            aq.a = 7;
            au.a().a(aq.a);
            return;
        }
    }

    public static void setLogSenderDelayed(int i)
    {
        aa.a().a(i);
    }

    public static void setOn(Context context, int i)
    {
        if (a(context, "setOn(...)"))
        {
            if (!a(android/app/Activity, "onCreate"))
            {
                throw new SecurityException("setOn()\u6CA1\u6709\u5728Activity.onCreate()\u5185\u88AB\u8C03\u7528||setOn()is not called in Activity.onCreate().");
            }
            if (!a())
            {
                b();
                if ((i & 1) != 0)
                {
                    a(context);
                    return;
                }
            }
        }
    }

    public static void setSendLogStrategy(Context context, SendStrategyEnum sendstrategyenum, int i)
    {
        setSendLogStrategy(context, sendstrategyenum, i, false);
    }

    public static void setSendLogStrategy(Context context, SendStrategyEnum sendstrategyenum, int i, boolean flag)
    {
        if (!a(context, "setSendLogStrategy(...)"))
        {
            return;
        } else
        {
            b(context);
            aa.a().a(context.getApplicationContext(), sendstrategyenum, i, flag);
            return;
        }
    }

    public static void setSessionTimeOut(int i)
    {
        if (i <= 0)
        {
            au.a().a("SessionTimeOut is between 1 and 600. Default value[30] is used");
            return;
        }
        if (i <= 600)
        {
            ag.b().a(i);
            return;
        } else
        {
            au.a().a("SessionTimeOut is between 1 and 600. Value[600] is used");
            ag.b().a(i);
            return;
        }
    }

}
