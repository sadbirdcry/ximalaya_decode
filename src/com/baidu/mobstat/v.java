// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import java.util.HashMap;

// Referenced classes of package com.baidu.mobstat:
//            z, s, x, au, 
//            at, DataCore

class v
    implements Runnable
{

    final String a;
    final String b;
    final long c;
    final Context d;
    final s e;

    v(s s1, String s2, String s3, long l, Context context)
    {
        e = s1;
        a = s2;
        b = s3;
        c = l;
        d = context;
        super();
    }

    public void run()
    {
        if (z.a().c())
        {
            break MISSING_BLOCK_LABEL_22;
        }
        Object obj = z.a();
        obj;
        JVM INSTR monitorenter ;
        z.a().wait();
_L2:
        Object obj1;
        obj = e.a(a, b);
        obj1 = (x)e.a.get(obj);
        if (obj1 == null)
        {
            au.a().b(new Object[] {
                "statsdk", (new StringBuilder()).append("EventStat: event_id[").append(a).append("] with label[").append(b).append("] is not started or alread done.").toString()
            });
            return;
        }
        break MISSING_BLOCK_LABEL_136;
        obj1;
        au.a().a("statsdk", ((Throwable) (obj1)));
        if (true) goto _L2; else goto _L1
_L1:
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
        if (!a.equals(((x) (obj1)).a) || !b.equals(((x) (obj1)).b))
        {
            au.a().a("statsdk", "EventStat: Wrong Case, eventId/label pair not match");
            return;
        }
        e.a.remove(obj);
        long l = c - ((x) (obj1)).c;
        if (l <= 0L)
        {
            au.a().a("statsdk", "EventStat: Wrong Case, Duration must be positive");
            return;
        } else
        {
            DataCore.getInstance().putEvent(a, b, 1, ((x) (obj1)).c, l);
            DataCore.getInstance().flush(d);
            return;
        }
    }
}
