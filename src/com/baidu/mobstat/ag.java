// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.Fragment;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.HashMap;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.baidu.mobstat:
//            ae, au, at, ar, 
//            ah, an, al, ai, 
//            aj, ak, am

class ag
{

    static HashMap a = new HashMap();
    private static HandlerThread b = new HandlerThread("SessionAnalysisThread");
    private static Handler c;
    private static ag l = new ag();
    private long d;
    private long e;
    private long f;
    private long g;
    private WeakReference h;
    private WeakReference i;
    private WeakReference j;
    private ae k;
    private int m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;

    private ag()
    {
        d = 0L;
        e = 0L;
        f = 0L;
        g = 0L;
        k = new ae();
        m = -1;
        n = true;
        o = false;
        p = false;
        q = false;
        b.start();
        b.setPriority(10);
        c = new Handler(b.getLooper());
    }

    static Context a(Object obj)
    {
        try
        {
            obj = (Context)obj.getClass().getMethod("getActivity", new Class[0]).invoke(obj, new Object[0]);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            au.a().a(new Object[] {
                ((Throwable) (obj)).getMessage()
            });
            return null;
        }
        return ((Context) (obj));
    }

    static ae a(ag ag1)
    {
        return ag1.k;
    }

    private void a(Context context)
    {
        if (context == null)
        {
            au.a().a("statsdk", "clearLastSession(Context context):context=null");
            return;
        } else
        {
            ar.a(false, context, "__local_last_session.json", "{}", false);
            return;
        }
    }

    static void a(ag ag1, Context context)
    {
        ag1.a(context);
    }

    static void a(ag ag1, Context context, long l1)
    {
        ag1.c(context, l1);
    }

    private void a(boolean flag)
    {
        n = flag;
    }

    public static ag b()
    {
        return l;
    }

    private void c(Context context, long l1)
    {
        au.a().a("statsdk", "flush current session to last_session.json");
        new JSONObject();
        Object obj = k.c();
        try
        {
            ((JSONObject) (obj)).put("e", l1);
        }
        catch (JSONException jsonexception)
        {
            au.a().a("statsdk", "StatSession.flushSession() failed");
        }
        obj = ((JSONObject) (obj)).toString();
        au.a().a("statsdk", (new StringBuilder()).append("cacheString=").append(((String) (obj))).toString());
        ar.a(false, context, "__local_last_session.json", ((String) (obj)), false);
    }

    private boolean e()
    {
        return n;
    }

    public int a()
    {
        if (m == -1)
        {
            m = 30000;
        }
        return m;
    }

    public void a(int i1)
    {
        m = i1 * 1000;
    }

    public void a(Context context, long l1)
    {
        au.a().a("statsdk", "AnalysisResume job");
        if (o)
        {
            au.a().c(new Object[] {
                "statsdk", "\u9057\u6F0FStatService.onPause() || missing StatService.onPause()"
            });
        }
        o = true;
        an an1;
        if (e())
        {
            au.a().a(new Object[] {
                "is_first_resume=true"
            });
            a(false);
            c.post(new ah(this));
        } else
        {
            au.a().a("statsdk", " is_first_resume=false");
        }
        an1 = new an(this, d, l1, context, null, null, 1);
        c.post(an1);
        h = new WeakReference(context);
        e = l1;
    }

    public void a(Context context, long l1, String s)
    {
        a(s);
        au.a().a("statsdk", "AnalysisPageStart");
        if (b(s).b)
        {
            au.a().c(new Object[] {
                "statsdk", "\u9057\u6F0FStatService.onPageEnd() || missing StatService.onPageEnd()"
            });
        }
        b(s).b = true;
        b(s).c = l1;
        if (e())
        {
            au.a().a("PPPPPPPPPPPPP is_first_resume=true");
            a(false);
            c.post(new ai(this));
        } else
        {
            au.a().a("statsdk", " is_first_resume=false");
        }
        s = new an(this, d, l1, context, null, null, 1);
        c.post(s);
        h = new WeakReference(context);
        e = l1;
    }

    public void a(Fragment fragment, long l1)
    {
        au.a().a("statsdk", "post resume job");
        if (p)
        {
            au.a().c(new Object[] {
                "statsdk", "\u9057\u6F0FStatService.onPause() || missing StatService.onPause()"
            });
        }
        p = true;
        an an1;
        if (e())
        {
            au.a().a("statsdk", "is_first_resume=true");
            a(false);
            c.post(new aj(this));
        } else
        {
            au.a().a("statsdk", "is_first_resume=false");
        }
        an1 = new an(this, d, l1, null, fragment, null, 2);
        c.post(an1);
        i = new WeakReference(fragment);
        f = l1;
    }

    public void a(Object obj, long l1)
    {
        au.a().a("statsdk", "post resume job");
        if (q)
        {
            au.a().c(new Object[] {
                "statsdk", "\u9057\u6F0FStatService.onPause() || missing StatService.onPause()"
            });
        }
        q = true;
        an an1;
        if (e())
        {
            au.a().a("statsdk", "is_first_resume=true");
            a(false);
            c.post(new ak(this));
        } else
        {
            au.a().a("statsdk", "is_first_resume=false");
        }
        an1 = new an(this, d, l1, null, null, obj, 3);
        c.post(an1);
        j = new WeakReference(obj);
        g = l1;
    }

    void a(String s)
    {
        if (s == null)
        {
            au.a().c(new Object[] {
                "sdkstat", "page Object is null"
            });
        } else
        {
            al al1 = new al(this, s);
            if (!a.containsKey(s))
            {
                a.put(s, al1);
                return;
            }
        }
    }

    al b(String s)
    {
        if (s == null)
        {
            au.a().c(new Object[] {
                "sdkstat", "pageName is null"
            });
            return null;
        }
        if (!a.containsKey(s))
        {
            a(s);
        }
        return (al)a.get(s);
    }

    public void b(Context context, long l1)
    {
        au.a().a("statsdk", "post pause job");
        if (!o)
        {
            au.a().c(new Object[] {
                "statsdk", "\u9057\u6F0FStatService.onResume() || missing StatService.onResume()"
            });
            return;
        } else
        {
            o = false;
            context = new am(this, l1, context, null, e, (Context)h.get(), null, 1, null, null, null);
            c.post(context);
            d = l1;
            return;
        }
    }

    public void b(Context context, long l1, String s)
    {
        au.a().a("statsdk", "post pause job");
        if (b(s) == null)
        {
            au.a().c(new Object[] {
                "statsdk", (new StringBuilder()).append("\u81EA\u5B9A\u4E49\u9875\u9762").append(s).append("\u6CA1\u6709\u4F18\u5148\u8C03\u7528\u6216\u8005\u9057\u6F0F\uFF0C\u8BF7\u68C0\u67E5\u786E\u4FDD\u5728onPageEnd\u51FD\u6570\u4E4B\u524D\u8C03\u7528onPageStart\u51FD\u6570").toString()
            });
            return;
        }
        if (!b(s).b)
        {
            au.a().c(new Object[] {
                "statsdk", "Please check (1)\u9057\u6F0FStatService.onPageStart() || missing StatService.onPageStart()"
            });
            return;
        } else
        {
            b(s).b = false;
            b(s).d = l1;
            context = new am(this, l1, context, null, b(s).c, (Context)h.get(), null, 1, s, null, null);
            c.post(context);
            d = l1;
            return;
        }
    }

    public void b(Fragment fragment, long l1)
    {
        au.a().a("statsdk", "post pause job");
        if (!p)
        {
            au.a().c(new Object[] {
                "statsdk", "\u9057\u6F0Fandroid.support.v4.app.Fragment StatService.onResume() || android.support.v4.app.Fragment missing StatService.onResume()"
            });
            return;
        } else
        {
            p = false;
            fragment = new am(this, l1, null, fragment, f, null, (Fragment)i.get(), 2, null, null, null);
            c.post(fragment);
            d = l1;
            return;
        }
    }

    public void b(Object obj, long l1)
    {
        au.a().a("statsdk", "post pause job");
        if (!q)
        {
            au.a().c(new Object[] {
                "statsdk", "\u9057\u6F0Fandroid.app.Fragment StatService.onResume() || android.app.Fragment missing StatService.onResume()"
            });
            return;
        } else
        {
            q = false;
            obj = new am(this, l1, null, null, g, null, null, 3, null, j.get(), obj);
            c.post(((Runnable) (obj)));
            d = l1;
            return;
        }
    }

    public void c()
    {
        k.a(k.d() + 1);
    }

    void c(String s)
    {
        if (s == null)
        {
            au.a().c(new Object[] {
                "sdkstat", "pageName is null"
            });
        } else
        if (a.containsKey(s))
        {
            a.remove(s);
            return;
        }
    }

    public long d()
    {
        return k.a();
    }

}
