// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.HandlerThread;
import java.lang.ref.WeakReference;
import java.util.Date;
import java.util.Timer;

// Referenced classes of package com.baidu.mobstat:
//            SendStrategyEnum, au, at, ao, 
//            y, BasicStoreTools, DataCore, ab, 
//            ad

class aa
{

    private static HandlerThread a = new HandlerThread("LogSenderThread");
    private static Handler i;
    private static aa j = new aa();
    private boolean b;
    private SendStrategyEnum c;
    private int d;
    private Timer e;
    private int f;
    private boolean g;
    private WeakReference h;

    private aa()
    {
        b = false;
        c = SendStrategyEnum.APP_START;
        d = 1;
        f = 0;
        g = false;
        a.start();
        i = new Handler(a.getLooper());
    }

    static int a(aa aa1, int k)
    {
        aa1.d = k;
        return k;
    }

    static SendStrategyEnum a(aa aa1, SendStrategyEnum sendstrategyenum)
    {
        aa1.c = sendstrategyenum;
        return sendstrategyenum;
    }

    public static aa a()
    {
        return j;
    }

    static Timer a(aa aa1, Timer timer)
    {
        aa1.e = timer;
        return timer;
    }

    static boolean a(aa aa1)
    {
        return aa1.g;
    }

    static boolean a(aa aa1, boolean flag)
    {
        aa1.g = flag;
        return flag;
    }

    static Handler b()
    {
        return i;
    }

    static Timer b(aa aa1)
    {
        return aa1.e;
    }

    static boolean b(aa aa1, boolean flag)
    {
        aa1.b = flag;
        return flag;
    }

    static SendStrategyEnum c(aa aa1)
    {
        return aa1.c;
    }

    static boolean d(aa aa1)
    {
        return aa1.b;
    }

    static int e(aa aa1)
    {
        return aa1.f;
    }

    private void e(Context context)
    {
        if (context == null)
        {
            au.a().a("sdkstat", (new StringBuilder()).append("initContext context=").append(null).toString());
        }
        if (h == null && context != null)
        {
            h = new WeakReference(context);
        }
    }

    public void a(int k)
    {
        if (k >= 0 && k <= 30)
        {
            f = k;
        }
    }

    public void a(Context context)
    {
        SendStrategyEnum sendstrategyenum;
        e(context);
        sendstrategyenum = SendStrategyEnum.APP_START;
        Object obj = ao.a(context, "BaiduMobAd_EXCEPTION_LOG");
        if (((String) (obj)).equals("")) goto _L2; else goto _L1
_L1:
        if (!((String) (obj)).equals("true")) goto _L4; else goto _L3
_L3:
        y.a().a(context);
        BasicStoreTools.getInstance().setExceptionTurn(context, true);
_L2:
        obj = sendstrategyenum;
        String s = ao.a(context, "BaiduMobAd_SEND_STRATEGY");
        Object obj1;
        obj1 = sendstrategyenum;
        obj = sendstrategyenum;
        if (s.equals("")) goto _L6; else goto _L5
_L5:
        obj = sendstrategyenum;
        if (!s.equals(SendStrategyEnum.APP_START.name())) goto _L8; else goto _L7
_L7:
        obj = sendstrategyenum;
        obj1 = SendStrategyEnum.APP_START;
        obj = obj1;
        int k;
        try
        {
            BasicStoreTools.getInstance().setSendStrategy(context, ((SendStrategyEnum) (obj1)).ordinal());
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            au.a().a(new Object[] {
                obj1
            });
            obj1 = obj;
        }
_L6:
        obj = ao.a(context, "BaiduMobAd_TIME_INTERVAL");
        if (((String) (obj)).equals(""))
        {
            break MISSING_BLOCK_LABEL_170;
        }
        k = Integer.parseInt(((String) (obj)));
        if (((SendStrategyEnum) (obj1)).ordinal() == SendStrategyEnum.SET_TIME_INTERVAL.ordinal() && k > 0 && k <= 24)
        {
            try
            {
                BasicStoreTools.getInstance().setSendStrategyTime(context, k);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                au.a().a(new Object[] {
                    obj
                });
            }
        }
        obj = ao.a(context, "BaiduMobAd_ONLY_WIFI");
        if (((String) (obj)).equals("")) goto _L10; else goto _L9
_L9:
        if (!((String) (obj)).equals("true")) goto _L12; else goto _L11
_L11:
        BasicStoreTools.getInstance().setOnlyWifi(context, true);
_L10:
        return;
_L4:
        try
        {
            if (((String) (obj)).equals("false"))
            {
                BasicStoreTools.getInstance().setExceptionTurn(context, false);
            }
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            au.a().a(new Object[] {
                obj
            });
        }
        continue; /* Loop/switch isn't completed */
_L8:
        obj = sendstrategyenum;
        if (!s.equals(SendStrategyEnum.ONCE_A_DAY.name()))
        {
            break MISSING_BLOCK_LABEL_315;
        }
        obj = sendstrategyenum;
        obj1 = SendStrategyEnum.ONCE_A_DAY;
        obj = obj1;
        BasicStoreTools.getInstance().setSendStrategy(context, ((SendStrategyEnum) (obj1)).ordinal());
        obj = obj1;
        BasicStoreTools.getInstance().setSendStrategyTime(context, 24);
          goto _L6
        obj1 = sendstrategyenum;
        obj = sendstrategyenum;
        if (!s.equals(SendStrategyEnum.SET_TIME_INTERVAL.name())) goto _L6; else goto _L13
_L13:
        obj = sendstrategyenum;
        obj1 = SendStrategyEnum.SET_TIME_INTERVAL;
        obj = obj1;
        BasicStoreTools.getInstance().setSendStrategy(context, ((SendStrategyEnum) (obj1)).ordinal());
          goto _L6
_L12:
        if (!((String) (obj)).equals("false")) goto _L10; else goto _L14
_L14:
        BasicStoreTools.getInstance().setOnlyWifi(context, false);
        return;
        context;
        au.a().a(new Object[] {
            context
        });
        return;
        if (true) goto _L2; else goto _L15
_L15:
    }

    public void a(Context context, SendStrategyEnum sendstrategyenum, int k, boolean flag)
    {
        if (!sendstrategyenum.equals(SendStrategyEnum.SET_TIME_INTERVAL)) goto _L2; else goto _L1
_L1:
        if (k > 0 && k <= 24)
        {
            d = k;
            c = SendStrategyEnum.SET_TIME_INTERVAL;
            BasicStoreTools.getInstance().setSendStrategy(context, c.ordinal());
            BasicStoreTools.getInstance().setSendStrategyTime(context, d);
        } else
        {
            au.a().c(new Object[] {
                "setSendLogStrategy", "time_interval is invalid, new strategy does not work"
            });
        }
_L4:
        b = flag;
        BasicStoreTools.getInstance().setOnlyWifi(context, b);
        au.a().a("sdkstat", (new StringBuilder()).append("sstype is:").append(c.name()).append(" And time_interval is:").append(d).append(" And m_only_wifi:").append(b).toString());
        return;
_L2:
        c = sendstrategyenum;
        BasicStoreTools.getInstance().setSendStrategy(context, c.ordinal());
        if (sendstrategyenum.equals(SendStrategyEnum.ONCE_A_DAY))
        {
            BasicStoreTools.getInstance().setSendStrategyTime(context, 24);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    protected void a(Context context, boolean flag)
    {
        if (flag)
        {
            try
            {
                if (!((WifiManager)context.getSystemService("wifi")).isWifiEnabled())
                {
                    au.a().a("statsdk", "sendLogData() does not send because of only_wifi setting");
                    return;
                }
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                au.a().a("statsdk", "sendLogData exception when get wifimanager");
                return;
            }
        }
        DataCore.getInstance().sendLogData(context);
    }

    public void a(boolean flag, Context context)
    {
        e(context);
        g = flag;
        au.a().a("sdkstat", (new StringBuilder()).append("APP_ANALYSIS_EXCEPTION is:").append(g).toString());
        BasicStoreTools.getInstance().setExceptionTurn(context, g);
    }

    public void b(Context context)
    {
        e(context);
        Context context1 = context;
        if (context == null)
        {
            context1 = context;
            if (h.get() != null)
            {
                context1 = (Context)h.get();
            }
        }
        i.post(new ab(this, context1));
    }

    public void c(Context context)
    {
        BasicStoreTools.getInstance().setLastSendTime(context, (new Date()).getTime());
    }

    public void d(Context context)
    {
        e = new Timer();
        e.schedule(new ad(this, context), d * 0x36ee80, d * 0x36ee80);
    }

}
