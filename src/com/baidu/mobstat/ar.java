// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

// Referenced classes of package com.baidu.mobstat:
//            au, at

public final class ar
{

    private static final Proxy a;
    private static final Proxy b;

    public static String a(Context context, String s)
    {
        au.a().a("MoUtil.read", s);
        context = b(context, s);
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_43;
        }
        context = new String(context, "utf-8");
        return context;
        context;
        Log.w("sdkstat", "MoUtil.read", context);
        return "";
    }

    public static String a(boolean flag, Context context, String s)
    {
        if (flag)
        {
            return b(s);
        } else
        {
            return a(context, s);
        }
    }

    public static HttpURLConnection a(Context context, String s, int i, int j)
    {
        s = new URL(s);
        Object obj = (ConnectivityManager)context.getSystemService("connectivity");
        context = ((ConnectivityManager) (obj)).getNetworkInfo(0);
        obj = ((ConnectivityManager) (obj)).getNetworkInfo(1);
        if (obj != null && ((NetworkInfo) (obj)).isAvailable())
        {
            au.a().a("", "WIFI is available");
            context = (HttpURLConnection)s.openConnection();
        } else
        if (context != null && context.isAvailable())
        {
            context = context.getExtraInfo();
            if (context != null)
            {
                context = context.toLowerCase();
            } else
            {
                context = "";
            }
            au.a().a("current APN", context);
            if (context.startsWith("cmwap") || context.startsWith("uniwap") || context.startsWith("3gwap"))
            {
                context = (HttpURLConnection)s.openConnection(a);
            } else
            if (context.startsWith("ctwap"))
            {
                context = (HttpURLConnection)s.openConnection(b);
            } else
            {
                context = (HttpURLConnection)s.openConnection();
            }
        } else
        {
            au.a().a("", "getConnection:not wifi and mobile");
            context = (HttpURLConnection)s.openConnection();
        }
        context.setConnectTimeout(i);
        context.setReadTimeout(j);
        return context;
    }

    public static void a(Context context, String s, String s1, boolean flag)
    {
        Context context1;
        Context context2;
        boolean flag1;
        flag1 = false;
        context2 = null;
        context1 = null;
        int i;
        if (flag)
        {
            i = 32768;
        } else
        {
            i = 0;
        }
        context = context.openFileOutput(s, i);
        if (context == null) goto _L2; else goto _L1
_L1:
        context1 = context;
        context2 = context;
        context.write(s1.getBytes("utf-8"));
_L5:
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_53;
        }
        context.close();
_L3:
        return;
_L2:
        context1 = context;
        context2 = context;
        s = (new StringBuilder()).append("MoUtil.write fout is null:");
        flag = flag1;
        if (context == null)
        {
            flag = true;
        }
        context1 = context;
        context2 = context;
        try
        {
            Log.w("sdkstat", s.append(flag).toString());
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context2 = context1;
        }
        finally
        {
            if (context2 != null)
            {
                try
                {
                    context2.close();
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    Log.w("sdkstat", "MoUtil.write", s);
                }
            }
            throw context;
        }
        Log.w("sdkstat", "MoUtil.write", context);
        if (context1 != null)
        {
            try
            {
                context1.close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                Log.w("sdkstat", "MoUtil.write", context);
            }
            return;
        }
          goto _L3
        context;
        Log.w("sdkstat", "MoUtil.write", context);
        return;
        if (true) goto _L5; else goto _L4
_L4:
    }

    public static void a(String s, String s1, boolean flag)
    {
        if ("mounted".equals(Environment.getExternalStorageState())) goto _L2; else goto _L1
_L1:
        return;
_L2:
        String s2;
        Object obj;
        Object obj1;
        Object obj2;
        obj1 = null;
        obj2 = null;
        obj = null;
        s2 = obj2;
        File file = Environment.getExternalStorageDirectory();
        s2 = obj2;
        s = new File((new StringBuilder()).append(file).append(File.separator).append(s).toString());
        s2 = obj2;
        if (s.exists())
        {
            break MISSING_BLOCK_LABEL_94;
        }
        s2 = obj2;
        s.getParentFile().mkdirs();
        s2 = obj2;
        s.createNewFile();
        s2 = obj2;
        s = new FileOutputStream(s, flag);
        s.write(s1.getBytes("utf-8"));
        if (s != null)
        {
            try
            {
                s.close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Log.w("sdkstat", "MoUtil.writeExt", s);
            }
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        s1;
        s = obj;
_L8:
        s2 = s;
        Log.e("sdkstat", "MoUtil.writeExt", s1);
        if (s != null)
        {
            try
            {
                s.close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Log.w("sdkstat", "MoUtil.writeExt", s);
            }
            return;
        }
          goto _L1
        s1;
        s = obj1;
_L7:
        s2 = s;
        Log.e("sdkstat", "MoUtil.writeExt", s1);
        if (s == null) goto _L1; else goto _L4
_L4:
        try
        {
            s.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Log.w("sdkstat", "MoUtil.writeExt", s);
        }
        return;
        s;
_L6:
        if (s2 != null)
        {
            try
            {
                s2.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s1)
            {
                Log.w("sdkstat", "MoUtil.writeExt", s1);
            }
        }
        throw s;
        s1;
        s2 = s;
        s = s1;
        if (true) goto _L6; else goto _L5
_L5:
        s1;
          goto _L7
        s1;
          goto _L8
    }

    public static void a(boolean flag, Context context, String s, String s1, boolean flag1)
    {
        if (flag)
        {
            a(s, s1, flag1);
            return;
        } else
        {
            a(context, s, s1, flag1);
            return;
        }
    }

    public static boolean a(String s)
    {
        au.a().a("MoUtil.deleteExt", s);
        if ("mounted".equals(Environment.getExternalStorageState()))
        {
            File file = Environment.getExternalStorageDirectory();
            s = new File((new StringBuilder()).append(file).append(File.separator).append(s).toString());
            if (s.exists())
            {
                return s.delete();
            }
        }
        return false;
    }

    public static String b(String s)
    {
        Object obj;
        au.a().a("MoUtil.readExt", s);
        obj = Environment.getExternalStorageState();
        if ("mounted".equals(obj) || "mounted_ro".equals(obj)) goto _L2; else goto _L1
_L1:
        return "";
_L2:
        obj = Environment.getExternalStorageDirectory();
        s = new File((new StringBuilder()).append(obj).append(File.separator).append(s).toString());
        if (!s.exists()) goto _L1; else goto _L3
_L3:
        obj = new FileInputStream(s);
        s = ((String) (obj));
        byte abyte0[] = new byte[((FileInputStream) (obj)).available()];
        s = ((String) (obj));
        ((FileInputStream) (obj)).read(abyte0);
        s = ((String) (obj));
        String s1 = new String(abyte0, "utf-8");
        s = s1;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_129;
        }
        ((FileInputStream) (obj)).close();
        s = s1;
_L4:
        return s;
        s;
        Log.w("sdkstat", "MoUtil.readExt", s);
        s = s1;
          goto _L4
        Object obj1;
        obj1;
        obj = null;
_L8:
        s = ((String) (obj));
        Log.e("sdkstat", "MoUtil.readExt", ((Throwable) (obj1)));
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_168;
        }
        ((FileInputStream) (obj)).close();
        s = "";
          goto _L4
        s;
        Log.w("sdkstat", "MoUtil.readExt", s);
        s = "";
          goto _L4
        obj1;
        obj = null;
_L7:
        s = ((String) (obj));
        Log.e("sdkstat", "MoUtil.readExt", ((Throwable) (obj1)));
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_212;
        }
        ((FileInputStream) (obj)).close();
        s = "";
          goto _L4
        s;
        Log.w("sdkstat", "MoUtil.readExt", s);
        s = "";
          goto _L4
        Exception exception;
        exception;
        s = null;
_L6:
        if (s != null)
        {
            try
            {
                s.close();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Log.w("sdkstat", "MoUtil.readExt", s);
            }
        }
        throw exception;
        exception;
        if (true) goto _L6; else goto _L5
_L5:
        obj1;
          goto _L7
        obj1;
          goto _L8
    }

    static byte[] b(Context context, String s)
    {
        Object obj;
        Object obj4;
        Object obj5;
        obj5 = null;
        obj = null;
        obj4 = null;
        context = context.openFileInput(s);
        if (context == null) goto _L2; else goto _L1
_L1:
        s = new byte[context.available()];
        context.read(s);
_L11:
        obj = s;
        if (context != null)
        {
            try
            {
                context.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                Log.e("sdkstat", "MoUtil.readBinary", context);
                return s;
            }
            obj = s;
        }
_L4:
        return ((byte []) (obj));
        Object obj3;
        obj3;
        context = null;
        s = obj4;
_L9:
        obj = s;
        Log.e("sdkstat", "MoUtil.readBinary", ((Throwable) (obj3)));
        obj = context;
        if (s == null) goto _L4; else goto _L3
_L3:
        try
        {
            s.close();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Log.e("sdkstat", "MoUtil.readBinary", s);
            return context;
        }
        return context;
        obj3;
        context = null;
        s = obj5;
_L8:
        obj = s;
        Log.e("sdkstat", "MoUtil.readBinary", ((Throwable) (obj3)));
        obj = context;
        if (s == null) goto _L4; else goto _L5
_L5:
        try
        {
            s.close();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Log.e("sdkstat", "MoUtil.readBinary", s);
            return context;
        }
        return context;
        context;
_L7:
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Log.e("sdkstat", "MoUtil.readBinary", s);
            }
        }
        throw context;
        s;
        obj = context;
        context = s;
        if (true) goto _L7; else goto _L6
_L6:
        obj3;
        Object obj1 = null;
        s = context;
        context = obj1;
          goto _L8
        obj3;
        String s1 = s;
        s = context;
        context = s1;
          goto _L8
        obj3;
        Object obj2 = null;
        s = context;
        context = obj2;
          goto _L9
        obj3;
        String s2 = s;
        s = context;
        context = s2;
          goto _L9
_L2:
        s = null;
        if (true) goto _L11; else goto _L10
_L10:
    }

    public static void c(String s)
    {
        au.a().c(new Object[] {
            "sdkstat", s
        });
        Log.e("sdkstat", (new StringBuilder()).append("SDK install error:").append(s).toString());
    }

    public static boolean c(Context context, String s)
    {
        boolean flag = context.getFileStreamPath(s).exists();
        au.a().a("MoUtil.exists", (new StringBuilder()).append(flag).append(" ").append(s).toString());
        return flag;
    }

    public static void d(Context context, String s)
    {
        if (!e(context, s))
        {
            c((new StringBuilder()).append("You need the ").append(s).append(" permission. Open AndroidManifest.xml and just before the final </manifest> tag add:  <uses-permission android:name=\"").append(s).append("\" />").toString());
        }
    }

    public static boolean e(Context context, String s)
    {
        boolean flag;
        if (context.checkCallingOrSelfPermission(s) != -1)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        au.a().a("hasPermission ", (new StringBuilder()).append(flag).append(" | ").append(s).toString());
        return flag;
    }

    static 
    {
        a = new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80));
        b = new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80));
    }
}
