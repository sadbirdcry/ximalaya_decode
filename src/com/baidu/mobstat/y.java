// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.baidu.mobstat:
//            au, at, q, DataCore

class y
{

    private static y a = new y();
    private boolean b;

    private y()
    {
        b = false;
    }

    public static y a()
    {
        return a;
    }

    public void a(Context context)
    {
        au.a().a("statsdk", "openExceptonAnalysis");
        if (!b)
        {
            b = true;
            q.a().a(context);
        }
    }

    public void b(Context context)
    {
        if (context != null) goto _L2; else goto _L1
_L1:
        au.a().a("statsdk", "exceptonAnalysis, context=null");
_L4:
        return;
_L2:
        JSONArray jsonarray = q.a().b(context);
        if (jsonarray == null)
        {
            au.a().a("statsdk", "no exception str");
            return;
        }
        au.a().a("statsdk", "move exception cache to stat cache");
        int i = 0;
        do
        {
            try
            {
                if (i >= jsonarray.length())
                {
                    break;
                }
                JSONObject jsonobject = (JSONObject)jsonarray.get(i);
                DataCore.getInstance().putException(jsonobject.getLong("t"), jsonobject.getString("c"), jsonobject.getString("y"));
                DataCore.getInstance().flush(context);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                au.a().a("statsdk", context);
                return;
            }
            i++;
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
    }

}
