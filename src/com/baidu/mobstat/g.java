// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.baidu.mobstat;

import android.content.Context;
import android.content.SharedPreferences;
import com.baidu.kirin.KirinConfig;
import java.io.PrintStream;
import java.lang.ref.SoftReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.baidu.mobstat:
//            m, at

public class g
    implements android.content.SharedPreferences.OnSharedPreferenceChangeListener
{

    private static SoftReference a = null;
    private final Context b;
    private final SharedPreferences c = f().getSharedPreferences((new StringBuilder()).append(f().getPackageName()).append(".kirin_strategy_control_pref").toString(), 0);

    private g(Context context)
    {
        b = context.getApplicationContext();
    }

    public static g a(Context context)
    {
        com/baidu/mobstat/g;
        JVM INSTR monitorenter ;
        if (a != null) goto _L2; else goto _L1
_L1:
        g g1 = null;
_L10:
        g g2 = g1;
        if (g1 != null) goto _L4; else goto _L3
_L3:
        com/baidu/mobstat/g;
        JVM INSTR monitorenter ;
        if (a != null) goto _L6; else goto _L5
_L5:
        g1 = null;
_L8:
        g2 = g1;
        if (g1 != null)
        {
            break MISSING_BLOCK_LABEL_54;
        }
        g2 = new g(context);
        a = new SoftReference(g2);
        com/baidu/mobstat/g;
        JVM INSTR monitorexit ;
_L4:
        com/baidu/mobstat/g;
        JVM INSTR monitorexit ;
        return g2;
_L2:
        g1 = (g)a.get();
        continue; /* Loop/switch isn't completed */
_L6:
        g1 = (g)a.get();
        if (true) goto _L8; else goto _L7
_L7:
        context;
        com/baidu/mobstat/g;
        JVM INSTR monitorexit ;
        throw context;
        context;
        com/baidu/mobstat/g;
        JVM INSTR monitorexit ;
        throw context;
        if (true) goto _L10; else goto _L9
_L9:
    }

    private boolean a(String s, String s1)
    {
        return c(s) > c(s1);
    }

    private int c(String s)
    {
        return Integer.parseInt(s.split(":")[0]) * 60 * 60 + Integer.parseInt(s.split(":")[1]) * 60 + Integer.parseInt(s.split(":")[2]);
    }

    private Context f()
    {
        return b;
    }

    private boolean g()
    {
        long l = (new Date()).getTime();
        int j = c.getInt("kirin_update_freqency", -1);
        long l1 = c.getLong("kirin_strategy_record_time", -1L);
        if (l1 == -1L)
        {
            return true;
        }
        if ((l - l1) / 1000L >= (long)j)
        {
            m.a().a(new Object[] {
                (new StringBuilder()).append(l).append(" --> exceed interval : ").append(j).toString()
            });
            return true;
        } else
        {
            m.a().a(new Object[] {
                (new StringBuilder()).append(l).append(" --> don't exceed interval : ").append(j).toString()
            });
            return false;
        }
    }

    private boolean h()
    {
        long l = (new Date()).getTime();
        long l1 = c.getLong("kirin_strategy_record_time", -1L);
        if (l1 == -1L)
        {
            return true;
        }
        if ((l - l1) / 1000L > (long)KirinConfig.DEFAULT_UPDATE_INTERVAL)
        {
            m.a().a(new Object[] {
                (new StringBuilder()).append(l).append(" --> exceed interval : ").append(KirinConfig.DEFAULT_UPDATE_INTERVAL).toString()
            });
            return true;
        } else
        {
            m.a().a(new Object[] {
                (new StringBuilder()).append(l).append(" --> don't exceed interval : ").append(KirinConfig.DEFAULT_UPDATE_INTERVAL).toString()
            });
            return false;
        }
    }

    private boolean i()
    {
        Object obj;
        Object obj1;
        SimpleDateFormat simpledateformat;
        String s;
        String s1;
        long l;
        boolean flag;
        flag = true;
        obj = e();
        s = ((String) (obj)).split("\\|")[0];
        s1 = ((String) (obj)).split("\\|")[1];
        System.out.println(s);
        System.out.println(s1);
        l = (new Date()).getTime();
        obj = (new SimpleDateFormat("yyyy-MM-dd")).format(Long.valueOf(l));
        obj1 = (new StringBuilder()).append(((String) (obj))).append(" ").append(s).toString();
        obj = (new StringBuilder()).append(((String) (obj))).append(" ").append(s1).toString();
        System.out.println(((String) (obj1)));
        System.out.println(((String) (obj)));
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        obj1 = simpledateformat.parse(((String) (obj1)));
        obj = simpledateformat.parse(((String) (obj)));
        ParseException parseexception;
        try
        {
            System.out.println((new StringBuilder()).append(((Date) (obj1)).getTime()).append(" - ").append(((Date) (obj)).getTime()).toString());
            break MISSING_BLOCK_LABEL_195;
        }
        // Misplaced declaration of an exception variable
        catch (ParseException parseexception) { }
          goto _L1
        parseexception;
        obj1 = null;
        obj = null;
_L1:
        parseexception.printStackTrace();
        long l1 = ((Date) (obj1)).getTime();
        long l2 = ((Date) (obj)).getTime();
        if (a(s1, s))
        {
            return l >= l1 && l <= l2;
        }
        if (l < l1 - (long)0x5265c00 || l > l2)
        {
            flag = false;
        }
        return flag;
        parseexception;
        obj = null;
          goto _L1
    }

    public void a(boolean flag, JSONObject jsonobject)
    {
        long l1 = (new Date()).getTime();
        android.content.SharedPreferences.Editor editor = c.edit();
        if (flag)
        {
            try
            {
                int j = jsonobject.getInt("updateSwitch");
                int k = jsonobject.getInt("updateFrequency");
                int l = jsonobject.getInt("popFrequency");
                jsonobject = jsonobject.getString("openPeriod");
                m.a().a(new Object[] {
                    (new StringBuilder()).append("write to strategy controller data is  :  success : ").append(flag).append("; updateSwith : ").append(j).append("; updateFrequency : ").append(k).append("; popFrequency : ").append(l).toString()
                });
                editor.putLong("kirin_strategy_record_time", l1);
                editor.putInt("kirin_update_switcher", j);
                editor.putInt("kirin_update_freqency", k * 0x15180);
                editor.putInt("kirin_update_remind_freqency", 0x15180 * l);
                editor.putString("kirin_open_peroid", jsonobject);
            }
            // Misplaced declaration of an exception variable
            catch (JSONObject jsonobject)
            {
                jsonobject.printStackTrace();
                editor.putLong("kirin_strategy_record_time", l1);
                editor.putInt("kirin_update_switcher", 0);
                editor.putInt("kirin_update_freqency", KirinConfig.DEFAULT_UPDATE_INTERVAL);
                editor.putInt("kirin_update_remind_freqency", KirinConfig.DEFAULT_POP_INTERVAL);
                editor.putString("kirin_open_peroid", "00:00:00|23:59:59");
            }
        } else
        {
            editor.putLong("kirin_strategy_record_time", l1);
            editor.putInt("kirin_update_switcher", 0);
            editor.putInt("kirin_update_freqency", KirinConfig.DEFAULT_UPDATE_INTERVAL);
            editor.putInt("kirin_update_remind_freqency", KirinConfig.DEFAULT_POP_INTERVAL);
            editor.putString("kirin_open_peroid", "00:00:00|23:59:59");
        }
        editor.commit();
    }

    public boolean a()
    {
        if (i())
        {
            int j = c.getInt("kirin_update_switcher", -1);
            if (!g() && j == 1)
            {
                m.a().a(new Object[] {
                    "!isExceedServerUpdateInterval() && switcher == 1"
                });
                return false;
            }
            if (j == 0)
            {
                m.a().a(new Object[] {
                    "else if(switcher == 0)"
                });
                if (h())
                {
                    return true;
                }
            } else
            {
                m.a().a(new Object[] {
                    "else!"
                });
                return true;
            }
        }
        return false;
    }

    public boolean a(String s)
    {
        if (i())
        {
            if (s.equals("atStart"))
            {
                return a();
            }
            if (s.equals("atSetting"))
            {
                return b();
            }
        }
        return false;
    }

    public void b(String s)
    {
        android.content.SharedPreferences.Editor editor = c.edit();
        editor.putString("kirin_log_id", s);
        editor.commit();
    }

    public boolean b()
    {
        return i();
    }

    public boolean c()
    {
        int j;
        if (i())
        {
            if ((j = c.getInt("kirin_update_switcher", -1)) == -1 || j == 1)
            {
                return true;
            }
        }
        return false;
    }

    public String d()
    {
        return c.getString("kirin_log_id", "0");
    }

    public String e()
    {
        return c.getString("kirin_open_peroid", "00:00:00|23:59:59");
    }

    public void onSharedPreferenceChanged(SharedPreferences sharedpreferences, String s)
    {
        if (sharedpreferences == c)
        {
            m.a().a(new Object[] {
                (new StringBuilder()).append(s).append(" : has changed").toString()
            });
        }
    }

}
