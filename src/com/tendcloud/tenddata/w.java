// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class w
{

    private static final int a = 0x36ee80;

    public w()
    {
    }

    private static int a(String s)
    {
        String s1 = "";
        Matcher matcher;
        int i1;
        try
        {
            matcher = Pattern.compile("([0-9]+)").matcher(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return 0;
        }
        s = s1;
        if (matcher.find())
        {
            s = matcher.toMatchResult().group(0);
        }
        i1 = Integer.valueOf(s).intValue();
        return i1;
    }

    public static String a()
    {
        return (new StringBuilder()).append("Android+").append(android.os.Build.VERSION.RELEASE).toString();
    }

    public static String a(Context context)
    {
        context = context.getResources().getDisplayMetrics();
        if (context != null)
        {
            int i1 = ((DisplayMetrics) (context)).widthPixels;
            int j1 = ((DisplayMetrics) (context)).heightPixels;
            return (new StringBuilder()).append(Math.min(i1, j1)).append("*").append(Math.max(i1, j1)).append("*").append(((DisplayMetrics) (context)).densityDpi).toString();
        } else
        {
            return "";
        }
    }

    private static String a(String s, String s1)
    {
        String s2 = s.toLowerCase();
        if (s2.startsWith("unknown") || s2.startsWith("alps") || s2.startsWith("android") || s2.startsWith("sprd") || s2.startsWith("spreadtrum") || s2.startsWith("rockchip") || s2.startsWith("wondermedia") || s2.startsWith("mtk") || s2.startsWith("mt65") || s2.startsWith("nvidia") || s2.startsWith("brcm") || s2.startsWith("marvell") || s1.toLowerCase().contains(s2))
        {
            s = null;
        }
        return s;
    }

    public static String b()
    {
        return Build.MANUFACTURER.trim();
    }

    private static String b(String s)
    {
        String s2;
        BufferedReader bufferedreader = null;
        Object obj1 = null;
        Object obj = null;
        String s1 = bufferedreader;
        FileReader filereader;
        char ac[];
        int i1;
        try
        {
            filereader = new FileReader(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return s1;
        }
        s1 = bufferedreader;
        s2 = obj1;
        ac = new char[1024];
        s1 = bufferedreader;
        s2 = obj1;
        bufferedreader = new BufferedReader(filereader, 1024);
        s = obj;
_L2:
        s1 = s;
        s2 = s;
        i1 = bufferedreader.read(ac, 0, 1024);
        if (-1 == i1)
        {
            break; /* Loop/switch isn't completed */
        }
        s1 = s;
        s2 = s;
        s = (new StringBuilder()).append(s).append(new String(ac, 0, i1)).toString();
        if (true) goto _L2; else goto _L1
_L1:
        s1 = s;
        s2 = s;
        bufferedreader.close();
        s1 = s;
        s2 = s;
        filereader.close();
        return s;
        s;
        return s2;
    }

    public static String c()
    {
        return Build.BRAND.trim();
    }

    public static String d()
    {
        return Build.MODEL.trim();
    }

    public static int e()
    {
        return TimeZone.getDefault().getRawOffset() / 0x36ee80;
    }

    public static String f()
    {
        String s2 = Build.MODEL.trim();
        String s1 = a(Build.MANUFACTURER.trim(), s2);
        String s = s1;
        if (TextUtils.isEmpty(s1))
        {
            s = a(Build.BRAND.trim(), s2);
        }
        s1 = s;
        if (s == null)
        {
            s1 = "";
        }
        return (new StringBuilder(s1)).append(":").append(s2).toString();
    }

    public static int g()
    {
        return android.os.Build.VERSION.SDK_INT;
    }

    public static String h()
    {
        return android.os.Build.VERSION.RELEASE;
    }

    public static String i()
    {
        return Locale.getDefault().getLanguage();
    }

    public static String j()
    {
        return Locale.getDefault().getCountry();
    }

    public static String[] k()
    {
        String as[];
        int j1;
        as = new String[4];
        for (int i1 = 0; i1 < 4; i1++)
        {
            as[i1] = "";
        }

        ArrayList arraylist = new ArrayList();
        Object obj;
        Object obj1;
        String s;
        Object obj2;
        int k1;
        int l1;
        try
        {
            obj = new FileReader("/proc/cpuinfo");
            obj1 = new BufferedReader(((java.io.Reader) (obj)), 1024);
        }
        catch (Throwable throwable)
        {
            j1 = 0;
            continue; /* Loop/switch isn't completed */
        }
        s = ((BufferedReader) (obj1)).readLine();
        if (s == null) goto _L2; else goto _L1
_L1:
        try
        {
            arraylist.add(s);
            break MISSING_BLOCK_LABEL_59;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj2) { }
        finally { }
        ((BufferedReader) (obj1)).close();
        ((FileReader) (obj)).close();
        j1 = 0;
_L7:
        if (j1 == 0) goto _L4; else goto _L3
_L3:
        l1 = arraylist.size();
        j1 = 0;
_L5:
        if (j1 >= 3)
        {
            break; /* Loop/switch isn't completed */
        }
        obj = Pattern.compile((new String[] {
            "Processor\\s*:\\s*(.*)", "CPU\\s*variant\\s*:\\s*0x(.*)", "Hardware\\s*:\\s*(.*)"
        })[j1]);
        for (k1 = 0; k1 < l1; k1++)
        {
            obj1 = ((Pattern) (obj)).matcher((String)arraylist.get(k1));
            if (((Matcher) (obj1)).find())
            {
                as[j1] = ((Matcher) (obj1)).toMatchResult().group(1);
            }
        }

        j1++;
        continue; /* Loop/switch isn't completed */
_L2:
        ((BufferedReader) (obj1)).close();
        ((FileReader) (obj)).close();
        j1 = 1;
        continue; /* Loop/switch isn't completed */
        obj;
        j1 = 1;
        continue; /* Loop/switch isn't completed */
        obj;
        j1 = 0;
        continue; /* Loop/switch isn't completed */
        Throwable throwable1;
        try
        {
            ((BufferedReader) (obj1)).close();
            ((FileReader) (obj)).close();
        }
        catch (IOException ioexception) { }
        throw obj2;
        if (true) goto _L5; else goto _L4
_L4:
        as[3] = b("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq");
        return as;
        throwable1;
        j1 = 1;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public static String[] l()
    {
        return null;
    }

    public static int[] m()
    {
        int ai[];
        int k1 = 0;
        ai = new int[2];
        ai[0] = 0;
        ai[1] = 0;
        int ai1[] = new int[4];
        for (int i1 = 0; i1 < 4; i1++)
        {
            ai1[i1] = 0;
        }

        Object obj;
        BufferedReader bufferedreader;
        Object obj1;
        int j1;
        try
        {
            obj = new FileReader("/proc/meminfo");
            bufferedreader = new BufferedReader(((java.io.Reader) (obj)), 1024);
        }
        catch (Throwable throwable)
        {
            return ai;
        }
        j1 = k1;
        if (j1 >= 4)
        {
            break; /* Loop/switch isn't completed */
        }
        ai1[j1] = a(bufferedreader.readLine());
        j1++;
        if (true) goto _L2; else goto _L1
_L2:
        break MISSING_BLOCK_LABEL_69;
_L1:
        ai[0] = ai1[0];
        j1 = ai1[1];
        k1 = ai1[2];
        ai[1] = ai1[3] + (j1 + k1);
        bufferedreader.close();
        ((FileReader) (obj)).close();
        return ai;
        obj1;
        bufferedreader.close();
        ((FileReader) (obj)).close();
        return ai;
        obj;
        return ai;
        obj1;
        try
        {
            bufferedreader.close();
            ((FileReader) (obj)).close();
        }
        catch (IOException ioexception) { }
        throw obj1;
        IOException ioexception1;
        ioexception1;
        return ai;
    }

    public static int[] n()
    {
        int i1;
        int j1;
        int k1;
        int l1;
        try
        {
            StatFs statfs = new StatFs(Environment.getDataDirectory().getAbsolutePath());
            i1 = (statfs.getBlockCount() * (statfs.getBlockSize() / 512)) / 2;
            j1 = statfs.getAvailableBlocks();
            j1 = ((statfs.getBlockSize() / 512) * j1) / 2;
            statfs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());
            k1 = (statfs.getBlockCount() * (statfs.getBlockSize() / 512)) / 2;
            l1 = statfs.getAvailableBlocks();
            l1 = ((statfs.getBlockSize() / 512) * l1) / 2;
        }
        catch (Throwable throwable)
        {
            return null;
        }
        return (new int[] {
            i1, j1, k1, l1
        });
    }

    public static int o()
    {
        int i1 = 0;
        try
        {
            Object obj = b("/sys/class/power_supply/battery/full_bat");
            obj = Pattern.compile("\\s*([0-9]+)").matcher(((CharSequence) (obj)));
            if (((Matcher) (obj)).find())
            {
                i1 = Integer.valueOf(((Matcher) (obj)).toMatchResult().group(0)).intValue();
            }
        }
        catch (Exception exception)
        {
            return 0;
        }
        return i1;
    }
}
