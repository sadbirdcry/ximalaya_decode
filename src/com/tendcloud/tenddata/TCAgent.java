// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import java.util.Map;
import java.util.TreeMap;

// Referenced classes of package com.tendcloud.tenddata:
//            ag, ao, s

public class TCAgent
{

    public static boolean LOG_ON = false;
    protected static final String a = "TD";
    static final boolean b = false;
    static final String c = "TDLog";
    static boolean d = false;
    static final Map e = new TreeMap();
    private static ao f;
    public static String platform = "Android";

    public TCAgent()
    {
    }

    private static void a(Context context)
    {
        com/tendcloud/tenddata/TCAgent;
        JVM INSTR monitorenter ;
        ag ag1;
        if (f != null)
        {
            break MISSING_BLOCK_LABEL_48;
        }
        System.currentTimeMillis();
        ag1 = new ag();
        if (f == null)
        {
            f = (ao)ag1.a(context, "analytics", "TD", com/tendcloud/tenddata/ao, com/tendcloud/tenddata/s, "com.tendcloud.tenddata.ota.j");
        }
_L2:
        com/tendcloud/tenddata/TCAgent;
        JVM INSTR monitorexit ;
        return;
        context;
        context.printStackTrace();
        if (true) goto _L2; else goto _L1
_L1:
        context;
        throw context;
    }

    public static String getDeviceId(Context context)
    {
        try
        {
            a(context);
            context = f.b(context);
            if (LOG_ON)
            {
                Log.i("TDLog", (new StringBuilder()).append("getDeviceId# ").append(context).toString());
            }
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return null;
        }
        return context;
    }

    public static void init(Context context)
    {
        try
        {
            a(context);
            f.a(context);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    public static void init(Context context, String s1, String s2)
    {
        try
        {
            a(context);
            f.a(context, s1, s2);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    public static void onError(Context context, Throwable throwable)
    {
        a(context);
        if (!LOG_ON || throwable == null)
        {
            break MISSING_BLOCK_LABEL_42;
        }
        Log.i("TDLog", (new StringBuilder()).append("onError# ").append(throwable.getMessage()).toString());
        f.a(context, throwable);
        return;
        context;
        context.printStackTrace();
        return;
    }

    public static void onEvent(Context context, String s1)
    {
        onEvent(context, s1, null);
    }

    public static void onEvent(Context context, String s1, String s2)
    {
        onEvent(context, s1, s2, null);
    }

    public static void onEvent(Context context, String s1, String s2, Map map)
    {
        a(context);
        if (e.size() <= 0 || s1.startsWith("__")) goto _L2; else goto _L1
_L1:
        Object obj1;
        obj1 = new TreeMap();
        ((Map) (obj1)).putAll(e);
        Object obj;
        obj = obj1;
        if (map == null)
        {
            break MISSING_BLOCK_LABEL_76;
        }
        obj = obj1;
        if (map.size() <= 0)
        {
            break MISSING_BLOCK_LABEL_76;
        }
        ((Map) (obj1)).putAll(map);
        obj = obj1;
_L4:
        if (!LOG_ON || s1.startsWith("__"))
        {
            break MISSING_BLOCK_LABEL_209;
        }
        obj1 = new StringBuilder();
        ((StringBuilder) (obj1)).append("onEvent#");
        if (s1 != null)
        {
            try
            {
                ((StringBuilder) (obj1)).append((new StringBuilder()).append(" eventId:").append(s1).toString());
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context.printStackTrace();
                return;
            }
        }
        if (s2 == null)
        {
            break MISSING_BLOCK_LABEL_166;
        }
        ((StringBuilder) (obj1)).append((new StringBuilder()).append(" label:").append(s2).toString());
        if (map == null)
        {
            break MISSING_BLOCK_LABEL_198;
        }
        ((StringBuilder) (obj1)).append((new StringBuilder()).append(" key-value:").append(map.toString()).toString());
        Log.i("TDLog", ((StringBuilder) (obj1)).toString());
        f.a(context, s1, s2, ((Map) (obj)));
        return;
_L2:
        obj = map;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static void onPageEnd(Context context, String s1)
    {
        a(context.getApplicationContext());
        if (!LOG_ON || s1 == null)
        {
            break MISSING_BLOCK_LABEL_42;
        }
        Log.i("TDLog", (new StringBuilder()).append("onPageEnd# ").append(s1).toString());
        f.c(context, s1);
        return;
        context;
        context.printStackTrace();
        return;
    }

    public static void onPageStart(Context context, String s1)
    {
        a(context.getApplicationContext());
        if (!LOG_ON || s1 == null)
        {
            break MISSING_BLOCK_LABEL_42;
        }
        Log.i("TDLog", (new StringBuilder()).append("onPageStart# ").append(s1).toString());
        f.b(context, s1);
        return;
        context;
        context.printStackTrace();
        return;
    }

    public static void onPause(Activity activity)
    {
        try
        {
            a(activity.getApplicationContext());
            if (LOG_ON)
            {
                Log.i("TDLog", (new StringBuilder()).append("onPause# ").append(activity.getLocalClassName()).toString());
            }
            f.b(activity);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            activity.printStackTrace();
        }
    }

    public static void onResume(Activity activity)
    {
        try
        {
            a(activity.getApplicationContext());
            if (LOG_ON)
            {
                Log.i("TDLog", (new StringBuilder()).append("onResume# ").append(activity.getLocalClassName()).toString());
            }
            f.a(activity);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            activity.printStackTrace();
        }
    }

    public static void removeGlobalKV(String s1)
    {
        if (LOG_ON && s1 != null)
        {
            Log.i("TDLog", (new StringBuilder()).append("removeGlobalKV# key:").append(s1).toString());
        }
        e.remove(s1);
    }

    public static void setGlobalKV(String s1, Object obj)
    {
        if (LOG_ON && s1 != null && obj != null)
        {
            Log.i("TDLog", (new StringBuilder()).append("setGlobalKV# key:").append(s1).append(" value:").append(obj.toString()).toString());
        }
        e.put(s1, obj);
    }

    public static void setReportUncaughtExceptions(boolean flag)
    {
        try
        {
            d = flag;
            if (LOG_ON)
            {
                Log.i("TDLog", (new StringBuilder()).append("setReportUncaughtExceptions# ").append(flag).toString());
            }
            return;
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
        }
    }

    static 
    {
        LOG_ON = true;
    }
}
