// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

// Referenced classes of package com.tendcloud.tenddata:
//            l, j, m

public class au extends OutputStream
{

    static final int a = 8192;
    static final boolean b;
    private OutputStream c;
    private final DataOutputStream d;
    private final m e;
    private final l f = new l(8192);
    private final j g;
    private final int h = 1;
    private boolean i;
    private boolean j;
    private boolean k;
    private int l;
    private boolean m;
    private IOException n;
    private final byte o[] = new byte[1];

    public au(OutputStream outputstream, byte abyte0[])
    {
        i = true;
        j = true;
        k = true;
        l = 0;
        m = false;
        n = null;
        if (outputstream == null)
        {
            throw new NullPointerException();
        }
        c = outputstream;
        d = new DataOutputStream(outputstream);
        int i1;
        int j1;
        int k1;
        int l1;
        if (abyte0 != null)
        {
            i1 = abyte0.length + 4096;
        } else
        {
            i1 = 4096;
        }
        j1 = a(i1);
        k1 = Runtime.getRuntime().availableProcessors();
        l1 = (int)(16D + (double)Runtime.getRuntime().availableProcessors() * 2.5D);
        g = com.tendcloud.tenddata.j.a(f, 1, 0, 0, i1, j1, k1 * 4 + 24, l1);
        e = g.c();
        if (abyte0 != null && abyte0.length > 0)
        {
            e.a(i1, abyte0);
            i = false;
        }
    }

    private static int a(int i1)
    {
        if (8192 > i1)
        {
            return 8192 - i1;
        } else
        {
            return 0;
        }
    }

    private void a(int i1, int j1)
    {
        char c1;
        if (k)
        {
            if (i)
            {
                c1 = '\340';
            } else
            {
                c1 = '\300';
            }
        } else
        if (j)
        {
            c1 = '\240';
        } else
        {
            c1 = '\200';
        }
        d.writeByte(c1 | i1 - 1 >>> 16);
        d.writeShort(i1 - 1);
        d.writeShort(j1 - 1);
        if (k)
        {
            d.writeByte(h);
        }
        f.a(c);
        k = false;
        j = false;
        i = false;
    }

    private void b()
    {
        int j1 = f.c();
        int i1 = g.d();
        if (!b && j1 <= 0)
        {
            throw new AssertionError(j1);
        }
        if (!b && i1 <= 0)
        {
            throw new AssertionError(i1);
        }
        if (j1 + 2 < i1)
        {
            a(i1, j1);
        } else
        {
            g.b();
            i1 = g.d();
            if (!b && i1 <= 0)
            {
                throw new AssertionError(i1);
            }
            b(i1);
        }
        l = l - i1;
        g.e();
        f.a();
    }

    private void b(int i1)
    {
        while (i1 > 0) 
        {
            int k1 = Math.min(i1, 8192);
            DataOutputStream dataoutputstream = d;
            int j1;
            if (i)
            {
                j1 = 1;
            } else
            {
                j1 = 2;
            }
            dataoutputstream.writeByte(j1);
            d.writeShort(k1 - 1);
            e.a(c, i1, k1);
            i1 -= k1;
            i = false;
        }
        j = true;
    }

    private void c()
    {
        if (!b && m)
        {
            throw new AssertionError();
        }
        if (n != null)
        {
            throw n;
        }
        e.d();
        try
        {
            while (l > 0) 
            {
                g.f();
                b();
            }
        }
        catch (IOException ioexception)
        {
            n = ioexception;
            throw ioexception;
        }
        c.write(0);
        m = true;
        return;
    }

    public void a()
    {
        if (!m)
        {
            c();
            m = true;
        }
    }

    public void close()
    {
        if (c == null) goto _L2; else goto _L1
_L1:
        IOException ioexception;
        if (!m)
        {
            try
            {
                c();
            }
            catch (IOException ioexception1) { }
        }
        c.close();
_L4:
        c = null;
_L2:
        if (n != null)
        {
            throw n;
        } else
        {
            return;
        }
        ioexception;
        if (n == null)
        {
            n = ioexception;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void flush()
    {
        if (n != null)
        {
            throw n;
        }
        if (m)
        {
            throw new IOException("Stream finished or closed");
        }
        try
        {
            e.c();
            while (l > 0) 
            {
                g.f();
                b();
            }
        }
        catch (IOException ioexception)
        {
            n = ioexception;
            throw ioexception;
        }
        c.flush();
        return;
    }

    public void write(int i1)
    {
        o[0] = (byte)i1;
        write(o, 0, 1);
    }

    public void write(byte abyte0[], int i1, int j1)
    {
        if (i1 < 0 || j1 < 0 || i1 + j1 < 0 || i1 + j1 > abyte0.length)
        {
            throw new IndexOutOfBoundsException();
        }
        if (n != null)
        {
            throw n;
        }
        if (m)
        {
            throw new IOException("Stream finished or closed");
        }
_L2:
        if (j1 <= 0)
        {
            break; /* Loop/switch isn't completed */
        }
        int k1;
        int l1;
        int i2;
        try
        {
            i2 = e.a(abyte0, i1, j1);
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            n = abyte0;
            throw abyte0;
        }
        k1 = i1 + i2;
        l1 = j1 - i2;
        l = i2 + l;
        i1 = k1;
        j1 = l1;
        if (!g.f())
        {
            continue; /* Loop/switch isn't completed */
        }
        b();
        i1 = k1;
        j1 = l1;
        if (true) goto _L2; else goto _L1
_L1:
    }

    static 
    {
        boolean flag;
        if (!com/tendcloud/tenddata/au.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        b = flag;
    }
}
