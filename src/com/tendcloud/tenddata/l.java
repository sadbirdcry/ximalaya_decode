// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import java.io.OutputStream;
import java.util.Arrays;

public final class l
{

    static final int a = 8;
    static final int b = 0xff000000;
    static final int c = 11;
    static final int d = 2048;
    static final short e = 1024;
    static final int f = 5;
    static final boolean g;
    private static final int h = 4;
    private static final int i = 4;
    private static final int j[];
    private long k;
    private int l;
    private int m;
    private byte n;
    private final byte o[];
    private int p;

    public l(int i1)
    {
        o = new byte[i1];
        a();
    }

    public static int a(int i1)
    {
        return i1 << 4;
    }

    public static int a(int i1, int j1)
    {
        if (!g && j1 != 0 && j1 != 1)
        {
            throw new AssertionError();
        } else
        {
            return j[(-j1 & 0x7ff ^ i1) >>> 4];
        }
    }

    public static final void a(short aword0[])
    {
        Arrays.fill(aword0, (short)1024);
    }

    public static int b(short aword0[], int i1)
    {
        int j1 = 0;
        i1 = aword0.length | i1;
        int k1;
        int l1;
        do
        {
            k1 = i1 >>> 1;
            l1 = j1 + a(aword0[k1], i1 & 1);
            j1 = l1;
            i1 = k1;
        } while (k1 != 1);
        return l1;
    }

    public static int d(short aword0[], int i1)
    {
        i1 = aword0.length | i1;
        int j1 = 0;
        int k1 = 1;
        int l1;
        int i2;
        do
        {
            int j2 = i1 & 1;
            l1 = i1 >>> 1;
            i2 = j1 + a(aword0[k1], j2);
            k1 = k1 << 1 | j2;
            j1 = i2;
            i1 = l1;
        } while (l1 != 1);
        return i2;
    }

    private void d()
    {
        int i1 = (int)(k >>> 32);
        if (i1 != 0 || k < 0xff000000L)
        {
            char c1 = n;
            int j1;
            do
            {
                byte abyte0[] = o;
                j1 = p;
                p = j1 + 1;
                abyte0[j1] = (byte)(c1 + i1);
                c1 = '\377';
                j1 = m - 1;
                m = j1;
            } while (j1 != 0);
            n = (byte)(int)(k >>> 24);
        }
        m = m + 1;
        k = (k & 0xffffffL) << 8;
    }

    public void a()
    {
        k = 0L;
        l = -1;
        n = 0;
        m = 1;
        p = 0;
    }

    public void a(OutputStream outputstream)
    {
        outputstream.write(o, 0, p);
    }

    public void a(short aword0[], int i1)
    {
        int k1 = aword0.length;
        int j1 = 1;
        int l1;
        do
        {
            l1 = k1 >>> 1;
            int i2 = i1 & l1;
            a(aword0, j1, i2);
            k1 = j1 << 1;
            j1 = k1;
            if (i2 != 0)
            {
                j1 = k1 | 1;
            }
            k1 = l1;
        } while (l1 != 1);
    }

    public void a(short aword0[], int i1, int j1)
    {
        short word0 = aword0[i1];
        int k1 = (l >>> 11) * word0;
        if (j1 == 0)
        {
            l = k1;
            aword0[i1] = (short)(word0 + (2048 - word0 >>> 5));
        } else
        {
            k = k + ((long)k1 & 0xffffffffL);
            l = l - k1;
            aword0[i1] = (short)(word0 - (word0 >>> 5));
        }
        if ((l & 0xff000000) == 0)
        {
            l = l << 8;
            d();
        }
    }

    public int b()
    {
        return (p + m + 5) - 1;
    }

    public void b(int i1, int j1)
    {
        int k1;
        do
        {
            l = l >>> 1;
            long l2 = k;
            int l1 = l;
            k1 = j1 - 1;
            k = l2 + (long)(l1 & 0 - (i1 >>> k1 & 1));
            if ((l & 0xff000000) == 0)
            {
                l = l << 8;
                d();
            }
            j1 = k1;
        } while (k1 != 0);
    }

    public int c()
    {
        for (int i1 = 0; i1 < 5; i1++)
        {
            d();
        }

        return p;
    }

    public void c(short aword0[], int i1)
    {
        i1 = aword0.length | i1;
        int j1 = 1;
        int k1;
        do
        {
            int l1 = i1 & 1;
            k1 = i1 >>> 1;
            a(aword0, j1, l1);
            j1 = j1 << 1 | l1;
            i1 = k1;
        } while (k1 != 1);
    }

    static 
    {
        boolean flag;
        if (!com/tendcloud/tenddata/l.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        g = flag;
        j = new int[128];
        for (int i1 = 8; i1 < 2048; i1 += 16)
        {
            int l1 = 0;
            int k1 = 0;
            int j1 = i1;
            for (; l1 < 4; l1++)
            {
                j1 *= j1;
                for (k1 <<= 1; (0xffff0000 & j1) != 0; k1++)
                {
                    j1 >>>= 1;
                }

            }

            j[i1 >> 4] = 161 - k1;
        }

    }
}
