// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.SystemClock;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrength;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.apache.http.HttpHost;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.tendcloud.tenddata:
//            an, t

public class ba
{

    static TelephonyManager a;
    static boolean b = false;
    static final long c = 0x493e0L;
    static long d = 0xfffffffffffb6c20L;
    private static final String e[] = {
        "UNKNOWN", "GPRS", "EDGE", "UMTS", "CDMA", "EVDO_0", "EVDO_A", "1xRTT", "HSDPA", "HSUPA", 
        "HSPA", "IDEN", "EVDO_B", "LTE", "EHRPD", "HSPAP"
    };
    private static final String f[] = {
        "NONE", "GSM", "CDMA", "SIP"
    };

    public ba()
    {
    }

    private static JSONArray A(Context context)
    {
        Object obj;
        JSONArray jsonarray;
        Object obj1;
        try
        {
            jsonarray = new JSONArray();
            obj1 = (TelephonyManager)context.getSystemService("phone");
            context = Class.forName("com.android.internal.telephony.Phone");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        obj = context.getField("GEMINI_SIM_1");
        ((Field) (obj)).setAccessible(true);
        obj = (Integer)((Field) (obj)).get(null);
        context = context.getField("GEMINI_SIM_2");
        context.setAccessible(true);
        context = (Integer)context.get(null);
_L2:
        Object obj2 = android/telephony/TelephonyManager.getMethod("getDefault", new Class[] {
            Integer.TYPE
        });
        obj = (TelephonyManager)((Method) (obj2)).invoke(obj1, new Object[] {
            obj
        });
        context = (TelephonyManager)((Method) (obj2)).invoke(obj1, new Object[] {
            context
        });
        obj2 = ((TelephonyManager) (obj)).getDeviceId().trim();
        obj1 = context.getDeviceId().trim();
        if (!b(((String) (obj2))).booleanValue())
        {
            break MISSING_BLOCK_LABEL_166;
        }
        obj = a(((TelephonyManager) (obj)), ((String) (obj2)));
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_166;
        }
        jsonarray.put(obj);
        if (!b(((String) (obj1))).booleanValue())
        {
            break; /* Loop/switch isn't completed */
        }
        context = a(context, ((String) (obj1)));
        if (context == null)
        {
            break; /* Loop/switch isn't completed */
        }
        jsonarray.put(context);
        break; /* Loop/switch isn't completed */
        context;
        obj = Integer.valueOf(0);
        context = Integer.valueOf(1);
        if (true) goto _L2; else goto _L1
_L1:
        return jsonarray;
    }

    private static JSONArray B(Context context)
    {
        JSONArray jsonarray;
        Object obj;
        String s2;
        jsonarray = new JSONArray();
        obj = Class.forName("com.android.internal.telephony.PhoneFactory");
        s2 = (String)((Class) (obj)).getMethod("getServiceName", new Class[] {
            java/lang/String, Integer.TYPE
        }).invoke(obj, new Object[] {
            "phone", Integer.valueOf(1)
        });
        obj = (TelephonyManager)context.getSystemService("phone");
        String s1 = ((TelephonyManager) (obj)).getDeviceId().trim();
        context = (TelephonyManager)context.getSystemService(s2);
        s2 = context.getDeviceId().trim();
        if (!b(s1).booleanValue())
        {
            break MISSING_BLOCK_LABEL_123;
        }
        obj = a(((TelephonyManager) (obj)), s1);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_123;
        }
        jsonarray.put(obj);
        if (!b(s2).booleanValue())
        {
            break MISSING_BLOCK_LABEL_151;
        }
        context = a(context, s2);
        if (context != null)
        {
            try
            {
                jsonarray.put(context);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                return null;
            }
        }
        return jsonarray;
    }

    private static JSONArray C(Context context)
    {
        JSONArray jsonarray;
        try
        {
            jsonarray = new JSONArray();
            Class class1 = Class.forName("android.telephony.MSimTelephonyManager");
            context = ((Context) (context.getSystemService("phone_msim")));
            Integer integer = Integer.valueOf(0);
            Integer integer1 = Integer.valueOf(1);
            Object obj = class1.getMethod("getDeviceId", new Class[] {
                Integer.TYPE
            });
            String s1 = ((String)((Method) (obj)).invoke(context, new Object[] {
                integer
            })).trim();
            obj = ((String)((Method) (obj)).invoke(context, new Object[] {
                integer1
            })).trim();
            if (b(s1).booleanValue())
            {
                jsonarray.put(a(class1, context, integer, s1, ""));
            }
            if (b(((String) (obj))).booleanValue())
            {
                jsonarray.put(a(class1, context, integer1, ((String) (obj)), ""));
            }
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return jsonarray;
    }

    private static Boolean a(String s1)
    {
        char c1 = '0';
        if (s1.length() > 0)
        {
            c1 = s1.charAt(0);
        }
        for (int i1 = 0; i1 < s1.length(); i1++)
        {
            if (c1 != s1.charAt(i1))
            {
                return Boolean.valueOf(false);
            }
        }

        return Boolean.valueOf(true);
    }

    private static String a(int i1)
    {
        if (i1 >= 0 && i1 < e.length)
        {
            return e[i1];
        } else
        {
            return String.valueOf(i1);
        }
    }

    private static JSONArray a(BitSet bitset)
    {
        int i1 = bitset.cardinality();
        if (bitset != null && i1 >= 1)
        {
            JSONArray jsonarray = new JSONArray();
            int j1 = bitset.nextSetBit(0);
            while (j1 >= 0) 
            {
                jsonarray.put(j1);
                j1 = bitset.nextSetBit(j1 + 1);
            }
        }
        return null;
    }

    public static JSONObject a(int i1, int j1)
    {
        JSONObject jsonobject = new JSONObject();
        try
        {
            jsonobject.put("lat", i1);
            jsonobject.put("lng", j1);
            jsonobject.put("unit", "qd");
        }
        catch (Throwable throwable)
        {
            return jsonobject;
        }
        return jsonobject;
    }

    private static JSONObject a(TelephonyManager telephonymanager, String s1)
    {
        JSONObject jsonobject;
        jsonobject = new JSONObject();
        jsonobject.put("imei", s1.trim());
        if (telephonymanager.getSubscriberId() != null) goto _L2; else goto _L1
_L1:
        s1 = "";
_L9:
        jsonobject.put("subscriberId", s1);
        if (telephonymanager.getSimSerialNumber() != null) goto _L4; else goto _L3
_L3:
        s1 = "";
_L10:
        jsonobject.put("simSerialNumber", s1);
        jsonobject.put("dataState", telephonymanager.getDataState());
        jsonobject.put("networkType", a(telephonymanager.getNetworkType()));
        jsonobject.put("networkOperator", telephonymanager.getNetworkOperator());
        jsonobject.put("phoneType", b(telephonymanager.getPhoneType()));
        if (telephonymanager.getSimOperator() != null) goto _L6; else goto _L5
_L5:
        s1 = "";
_L11:
        jsonobject.put("simOperator", s1);
        if (telephonymanager.getSimOperatorName() != null) goto _L8; else goto _L7
_L7:
        s1 = "";
_L12:
        jsonobject.put("simOperatorName", s1);
        if (telephonymanager.getSimCountryIso() != null)
        {
            break MISSING_BLOCK_LABEL_196;
        }
        telephonymanager = "";
_L13:
        try
        {
            jsonobject.put("simCountryIso", telephonymanager);
        }
        // Misplaced declaration of an exception variable
        catch (TelephonyManager telephonymanager)
        {
            return null;
        }
        return jsonobject;
_L2:
        s1 = telephonymanager.getSubscriberId();
          goto _L9
_L4:
        s1 = telephonymanager.getSimSerialNumber();
          goto _L10
_L6:
        s1 = telephonymanager.getSimOperator();
          goto _L11
_L8:
        s1 = telephonymanager.getSimOperatorName();
          goto _L12
        telephonymanager = telephonymanager.getSimCountryIso();
          goto _L13
    }

    private static JSONObject a(Class class1, Object obj, Integer integer, String s1, String s2)
    {
        JSONObject jsonobject;
        jsonobject = new JSONObject();
        jsonobject.put("imei", s1);
        s1 = class1.getMethod((new StringBuilder()).append("getSubscriberId").append(s2).toString(), new Class[] {
            Integer.TYPE
        });
        if (s1.invoke(obj, new Object[] {
            integer
        }) != null) goto _L2; else goto _L1
_L1:
        s1 = "";
_L7:
        try
        {
            jsonobject.put("subscriberId", s1);
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        s1 = class1.getMethod((new StringBuilder()).append("getSimSerialNumber").append(s2).toString(), new Class[] {
            Integer.TYPE
        });
        if (s1.invoke(obj, new Object[] {
            integer
        }) != null) goto _L4; else goto _L3
_L3:
        s1 = "";
_L8:
        try
        {
            jsonobject.put("simSerialNumber", s1);
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        try
        {
            jsonobject.put("dataState", (Integer)class1.getMethod((new StringBuilder()).append("getDataState").append(s2).toString(), new Class[] {
                Integer.TYPE
            }).invoke(obj, new Object[] {
                integer
            }));
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        try
        {
            jsonobject.put("networkType", a(((Integer)class1.getMethod((new StringBuilder()).append("getNetworkType").append(s2).toString(), new Class[] {
                Integer.TYPE
            }).invoke(obj, new Object[] {
                integer
            })).intValue()));
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        try
        {
            jsonobject.put("networkOperator", (String)class1.getMethod((new StringBuilder()).append("getNetworkOperator").append(s2).toString(), new Class[] {
                Integer.TYPE
            }).invoke(obj, new Object[] {
                integer
            }));
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        try
        {
            jsonobject.put("phoneType", b(((Integer)class1.getMethod((new StringBuilder()).append("getPhoneType").append(s2).toString(), new Class[] {
                Integer.TYPE
            }).invoke(obj, new Object[] {
                integer
            })).intValue()));
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        s1 = class1.getMethod((new StringBuilder()).append("getSimOperator").append(s2).toString(), new Class[] {
            Integer.TYPE
        });
        if (s1.invoke(obj, new Object[] {
            integer
        }) != null) goto _L6; else goto _L5
_L5:
        s1 = "";
_L9:
        try
        {
            jsonobject.put("simOperator", s1);
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        class1 = class1.getMethod((new StringBuilder()).append("getSimOperatorName").append(s2).toString(), new Class[] {
            Integer.TYPE
        });
        if (class1.invoke(obj, new Object[] {
    integer
}) != null)
        {
            break MISSING_BLOCK_LABEL_592;
        }
        class1 = "";
_L10:
        try
        {
            jsonobject.put("simOperatorName", class1);
        }
        // Misplaced declaration of an exception variable
        catch (Class class1)
        {
            return jsonobject;
        }
        return jsonobject;
_L2:
        s1 = ((String)s1.invoke(obj, new Object[] {
            integer
        })).trim();
          goto _L7
_L4:
        s1 = ((String)s1.invoke(obj, new Object[] {
            integer
        })).trim();
          goto _L8
_L6:
        s1 = ((String)s1.invoke(obj, new Object[] {
            integer
        })).trim();
          goto _L9
        class1 = ((String)class1.invoke(obj, new Object[] {
            integer
        })).trim();
          goto _L10
    }

    static void a(Context context)
    {
        a = (TelephonyManager)context.getSystemService("phone");
    }

    public static boolean a()
    {
        if (!an.a(11)) goto _L2; else goto _L1
_L1:
        if (TextUtils.isEmpty(System.getProperty("http.proxyHost"))) goto _L4; else goto _L3
_L3:
        return true;
_L4:
        return false;
_L2:
        if (TextUtils.isEmpty(Proxy.getDefaultHost()))
        {
            return false;
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    private static Boolean b(String s1)
    {
        Integer integer = Integer.valueOf(s1.length());
        if (integer.intValue() > 10 && integer.intValue() < 20 && !a(s1.trim()).booleanValue())
        {
            return Boolean.valueOf(true);
        } else
        {
            return Boolean.valueOf(false);
        }
    }

    private static String b(int i1)
    {
        if (i1 >= 0 && i1 < f.length)
        {
            return f[i1];
        } else
        {
            return String.valueOf(i1);
        }
    }

    public static String b(Context context)
    {
        context = NetworkInterface.getNetworkInterfaces();
_L2:
        Enumeration enumeration;
        if (!context.hasMoreElements())
        {
            break MISSING_BLOCK_LABEL_70;
        }
        enumeration = ((NetworkInterface)context.nextElement()).getInetAddresses();
_L4:
        if (!enumeration.hasMoreElements()) goto _L2; else goto _L1
_L1:
        InetAddress inetaddress = (InetAddress)enumeration.nextElement();
        if (inetaddress.isLoopbackAddress() || !(inetaddress instanceof Inet4Address)) goto _L4; else goto _L3
_L3:
        context = inetaddress.getHostAddress().toString();
        return context;
        context;
        return null;
    }

    public static HttpHost b()
    {
        if (a())
        {
            return new HttpHost(Proxy.getDefaultHost(), Proxy.getDefaultPort());
        } else
        {
            return null;
        }
    }

    private static Map c()
    {
        HashMap hashmap = new HashMap();
        BufferedReader bufferedreader = new BufferedReader(new FileReader(new File("/proc/net/arp")));
        do
        {
            String s1 = bufferedreader.readLine();
            if (s1 == null)
            {
                break;
            }
            String as[] = s1.split("[ ]+");
            if (!as[0].matches("IP"))
            {
                String s2 = as[0];
                String s3 = as[3];
                if (!hashmap.containsKey(s2) && !s3.equals("00:00:00:00:00:00"))
                {
                    hashmap.put(s2, s3);
                }
            }
        } while (true);
        bufferedreader.close();
        return hashmap;
    }

    public static boolean c(Context context)
    {
        if (!an.b(context, "android.permission.ACCESS_NETWORK_STATE")) goto _L2; else goto _L1
_L1:
        Object obj;
        context = (ConnectivityManager)context.getSystemService("connectivity");
        obj = context.getActiveNetworkInfo();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_35;
        }
        return ((NetworkInfo) (obj)).isConnected();
        context = context.getNetworkInfo(0);
        if (context == null) goto _L4; else goto _L3
_L3:
        if (!context.getState().equals(android.net.NetworkInfo.State.UNKNOWN)) goto _L4; else goto _L2
_L2:
        if (SystemClock.elapsedRealtime() - d <= 0x493e0L) goto _L6; else goto _L5
_L5:
        d = SystemClock.elapsedRealtime();
        context = null;
        obj = context;
        Object obj1 = b();
        if (obj1 == null) goto _L8; else goto _L7
_L7:
        obj = context;
        context = new Socket(((HttpHost) (obj1)).getHostName(), ((HttpHost) (obj1)).getPort());
_L13:
        obj = context;
        obj1 = context;
        b = true;
        if (context != null)
        {
            try
            {
                context.close();
            }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
            // Misplaced declaration of an exception variable
            catch (Context context) { }
        }
_L6:
        return b;
_L4:
        return false;
_L8:
        obj = context;
        try
        {
            context = new Socket("www.talkingdata.net", 80);
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            obj1 = obj;
        }
        finally
        {
            obj1 = null;
        }
        b = false;
        if (obj == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        ((Socket) (obj)).close();
        continue; /* Loop/switch isn't completed */
        context;
        if (true) goto _L6; else goto _L9
_L9:
_L11:
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_181;
        }
        try
        {
            ((Socket) (obj1)).close();
        }
        catch (Exception exception) { }
        throw context;
        context;
        if (true) goto _L11; else goto _L10
_L10:
        if (true) goto _L13; else goto _L12
_L12:
    }

    public static boolean d(Context context)
    {
        if (!an.b(context, "android.permission.ACCESS_NETWORK_STATE"))
        {
            break MISSING_BLOCK_LABEL_42;
        }
        context = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_39;
        }
        boolean flag = context.isAvailable();
        if (flag)
        {
            return true;
        }
        return false;
        context;
        return false;
    }

    public static boolean e(Context context)
    {
        boolean flag;
        try
        {
            flag = ((WifiManager)context.getSystemService("wifi")).isWifiEnabled();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        return flag;
    }

    public static boolean f(Context context)
    {
        boolean flag = false;
        int i1;
        try
        {
            if (a == null)
            {
                a(context);
            }
            i1 = a.getSimState();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        if (i1 == 5)
        {
            flag = true;
        }
        return flag;
    }

    public static boolean g(Context context)
    {
        if (!an.b(context, "android.permission.ACCESS_NETWORK_STATE"))
        {
            break MISSING_BLOCK_LABEL_50;
        }
        context = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        boolean flag;
        if (1 != context.getType())
        {
            break MISSING_BLOCK_LABEL_47;
        }
        flag = context.isConnected();
        if (flag)
        {
            return true;
        }
        return false;
        context;
        return false;
    }

    public static boolean h(Context context)
    {
        boolean flag = false;
        int i1;
        try
        {
            if (a == null)
            {
                a(context);
            }
            i1 = a.getDataState();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return false;
        }
        if (i1 == 2)
        {
            flag = true;
        }
        return flag;
    }

    public static String i(Context context)
    {
        if (!c(context))
        {
            return "OFFLINE";
        }
        if (g(context))
        {
            return "WIFI";
        } else
        {
            return j(context);
        }
    }

    public static String j(Context context)
    {
        String s1 = e[0];
        try
        {
            if (a == null)
            {
                a(context);
            }
            context = a(a.getNetworkType());
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return s1;
        }
        return context;
    }

    public static String k(Context context)
    {
        try
        {
            if (a == null)
            {
                a(context);
            }
            context = a.getNetworkOperator();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static String l(Context context)
    {
        try
        {
            if (a == null)
            {
                a(context);
            }
            context = a.getSimOperator();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static String m(Context context)
    {
        try
        {
            if (a == null)
            {
                a(context);
            }
            context = a.getNetworkCountryIso();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static String n(Context context)
    {
        try
        {
            if (a == null)
            {
                a(context);
            }
            context = a.getSimCountryIso();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static String o(Context context)
    {
        if (!an.b(context, "android.permission.READ_PHONE_STATE") || !an.a(18))
        {
            break MISSING_BLOCK_LABEL_38;
        }
        if (a == null)
        {
            a(context);
        }
        context = a.getGroupIdLevel1();
        return context;
        context;
        return null;
    }

    public static String p(Context context)
    {
        try
        {
            if (a == null)
            {
                a(context);
            }
            context = a.getNetworkOperatorName();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static String q(Context context)
    {
        try
        {
            if (a == null)
            {
                a(context);
            }
            context = a.getSimOperatorName();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static JSONArray r(Context context)
    {
        JSONArray jsonarray = new JSONArray();
        try
        {
            JSONObject jsonobject = new JSONObject();
            jsonobject.put("type", "wifi");
            jsonobject.put("available", e(context));
            jsonobject.put("connected", g(context));
            jsonobject.put("current", v(context));
            jsonobject.put("scannable", w(context));
            jsonobject.put("configured", u(context));
            jsonarray.put(jsonobject);
        }
        catch (Throwable throwable) { }
        try
        {
            JSONObject jsonobject1 = new JSONObject();
            jsonobject1.put("type", "cellular");
            jsonobject1.put("available", f(context));
            jsonobject1.put("connected", h(context));
            jsonobject1.put("current", s(context));
            jsonobject1.put("scannable", t(context));
            jsonarray.put(jsonobject1);
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        if (jsonarray.length() > 0)
        {
            return jsonarray;
        } else
        {
            return null;
        }
    }

    public static JSONArray s(Context context)
    {
        if (!an.a)
        {
            return null;
        }
        JSONObject jsonobject;
        if (!an.b(context, "android.permission.ACCESS_COARSE_LOCATION") && !an.b(context, "android.permission.ACCESS_FINE_LOCATION"))
        {
            break MISSING_BLOCK_LABEL_264;
        }
        if (a == null)
        {
            a(context);
        }
        jsonobject = new JSONObject();
        if (!an.c && !an.d) goto _L2; else goto _L1
_L1:
        Object obj = a.getCellLocation();
        if (!(obj instanceof GsmCellLocation)) goto _L4; else goto _L3
_L3:
        obj = (GsmCellLocation)obj;
        if (obj == null) goto _L2; else goto _L5
_L5:
        jsonobject.put("systemId", ((GsmCellLocation) (obj)).getLac());
        jsonobject.put("networkId", ((GsmCellLocation) (obj)).getCid());
        if (an.a(9))
        {
            jsonobject.put("basestationId", ((GsmCellLocation) (obj)).getPsc());
        }
_L2:
        jsonobject.put("type", j(context));
        jsonobject.put("mcc", k(context));
        jsonobject.put("operator", p(context));
        jsonobject.put("country", m(context));
        context = new JSONArray();
        context.put(jsonobject);
        return context;
_L4:
        if (!(obj instanceof CdmaCellLocation)) goto _L2; else goto _L6
_L6:
        obj = (CdmaCellLocation)obj;
        if (obj == null) goto _L2; else goto _L7
_L7:
        jsonobject.put("systemId", ((CdmaCellLocation) (obj)).getSystemId());
        jsonobject.put("networkId", ((CdmaCellLocation) (obj)).getNetworkId());
        jsonobject.put("basestationId", ((CdmaCellLocation) (obj)).getBaseStationId());
        jsonobject.put("location", a(((CdmaCellLocation) (obj)).getBaseStationLatitude(), ((CdmaCellLocation) (obj)).getBaseStationLongitude()));
          goto _L2
        context;
        return null;
    }

    public static JSONArray t(Context context)
    {
        while (!an.a || !an.b(context, "android.permission.ACCESS_COARSE_LOCATION") && !an.b(context, "android.permission.ACCESS_FINE_LOCATION")) 
        {
            return null;
        }
        JSONArray jsonarray;
        if (a == null)
        {
            a(context);
        }
        jsonarray = new JSONArray();
        if (!an.a(17)) goto _L2; else goto _L1
_L1:
        context = a.getAllCellInfo();
        if (context == null) goto _L4; else goto _L3
_L3:
        Iterator iterator = context.iterator();
_L17:
        if (!iterator.hasNext()) goto _L4; else goto _L5
_L5:
        context = (CellInfo)iterator.next();
        JSONObject jsonobject1;
        jsonobject1 = new JSONObject();
        jsonobject1.put("registered", context.isRegistered());
        jsonobject1.put("ts", context.getTimeStamp());
        if (!(context instanceof CellInfoGsm)) goto _L7; else goto _L6
_L6:
        Object obj;
        int i1;
        int j1;
        int l1;
        int i2;
        context = (CellInfoGsm)context;
        obj = context.getCellIdentity();
        i2 = ((CellIdentityGsm) (obj)).getLac();
        l1 = ((CellIdentityGsm) (obj)).getCid();
        j1 = ((CellIdentityGsm) (obj)).getMcc();
        i1 = ((CellIdentityGsm) (obj)).getMnc();
        obj = context.getCellSignalStrength();
        int k1;
        k1 = -1;
        context = "GSM";
_L15:
        if (i2 == -1)
        {
            break MISSING_BLOCK_LABEL_195;
        }
        jsonobject1.put("systemId", i2);
        if (l1 == -1)
        {
            break MISSING_BLOCK_LABEL_212;
        }
        jsonobject1.put("networkId", l1);
        if (k1 == -1)
        {
            break MISSING_BLOCK_LABEL_229;
        }
        jsonobject1.put("basestationId", k1);
        if (j1 == -1)
        {
            break MISSING_BLOCK_LABEL_246;
        }
        jsonobject1.put("mcc", j1);
        if (i1 == -1)
        {
            break MISSING_BLOCK_LABEL_263;
        }
        jsonobject1.put("mnc", i1);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_293;
        }
        jsonobject1.put("asuLevel", ((CellSignalStrength) (obj)).getAsuLevel());
        jsonobject1.put("dbm", ((CellSignalStrength) (obj)).getDbm());
        jsonobject1.put("type", context);
        jsonarray.put(jsonobject1);
        continue; /* Loop/switch isn't completed */
_L7:
        if (!(context instanceof CellInfoCdma))
        {
            break MISSING_BLOCK_LABEL_463;
        }
        obj = (CellInfoCdma)context;
        context = ((CellInfoCdma) (obj)).getCellIdentity();
        i2 = context.getSystemId();
        l1 = context.getNetworkId();
        k1 = context.getBasestationId();
        obj = ((CellInfoCdma) (obj)).getCellSignalStrength();
        jsonobject1.put("cdmaDbm", ((CellSignalStrengthCdma) (obj)).getCdmaDbm());
        jsonobject1.put("cdmaDbm", ((CellSignalStrengthCdma) (obj)).getCdmaDbm());
        jsonobject1.put("cdmaEcio", ((CellSignalStrengthCdma) (obj)).getCdmaEcio());
        jsonobject1.put("evdoDbm", ((CellSignalStrengthCdma) (obj)).getEvdoDbm());
        jsonobject1.put("evdoEcio", ((CellSignalStrengthCdma) (obj)).getEvdoEcio());
        jsonobject1.put("evdoSnr", ((CellSignalStrengthCdma) (obj)).getEvdoSnr());
        jsonobject1.put("location", a(context.getLatitude(), context.getLongitude()));
        j1 = -1;
        i1 = -1;
        context = "CDMA";
        continue; /* Loop/switch isn't completed */
        if (!(context instanceof CellInfoWcdma))
        {
            break MISSING_BLOCK_LABEL_522;
        }
        context = (CellInfoWcdma)context;
        obj = context.getCellIdentity();
        i2 = ((CellIdentityWcdma) (obj)).getLac();
        l1 = ((CellIdentityWcdma) (obj)).getCid();
        k1 = ((CellIdentityWcdma) (obj)).getPsc();
        j1 = ((CellIdentityWcdma) (obj)).getMcc();
        i1 = ((CellIdentityWcdma) (obj)).getMnc();
        obj = context.getCellSignalStrength();
        context = "WCDMA";
        continue; /* Loop/switch isn't completed */
        if (!(context instanceof CellInfoLte)) goto _L9; else goto _L8
_L8:
        context = (CellInfoLte)context;
        obj = context.getCellIdentity();
        i2 = ((CellIdentityLte) (obj)).getTac();
        l1 = ((CellIdentityLte) (obj)).getPci();
        k1 = ((CellIdentityLte) (obj)).getCi();
        j1 = ((CellIdentityLte) (obj)).getMcc();
        i1 = ((CellIdentityLte) (obj)).getMnc();
        obj = context.getCellSignalStrength();
        context = "LTE";
        continue; /* Loop/switch isn't completed */
_L2:
        if (!an.a(5) || !an.c && !an.d) goto _L4; else goto _L10
_L10:
        context = a.getNeighboringCellInfo();
        if (context == null) goto _L4; else goto _L11
_L11:
        context = context.iterator();
_L13:
        if (!context.hasNext()) goto _L4; else goto _L12
_L12:
        obj = (NeighboringCellInfo)context.next();
        try
        {
            JSONObject jsonobject = new JSONObject();
            jsonobject.put("systemId", ((NeighboringCellInfo) (obj)).getLac());
            jsonobject.put("netId", ((NeighboringCellInfo) (obj)).getCid());
            jsonobject.put("basestationId", ((NeighboringCellInfo) (obj)).getPsc());
            jsonobject.put("asuLevel", ((NeighboringCellInfo) (obj)).getRssi());
            jsonobject.put("type", a(((NeighboringCellInfo) (obj)).getNetworkType()));
            jsonarray.put(jsonobject);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L13
_L4:
        try
        {
            i1 = jsonarray.length();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        if (i1 > 0)
        {
            context = jsonarray;
        } else
        {
            context = null;
        }
        return context;
_L9:
        context = null;
        obj = null;
        i1 = -1;
        j1 = -1;
        k1 = -1;
        l1 = -1;
        i2 = -1;
        if (true) goto _L15; else goto _L14
_L14:
        context;
        if (true) goto _L17; else goto _L16
_L16:
    }

    public static JSONArray u(Context context)
    {
        if (!an.a)
        {
            return null;
        }
        Object obj;
        if (!an.b(context, "android.permission.ACCESS_WIFI_STATE"))
        {
            break MISSING_BLOCK_LABEL_218;
        }
        obj = ((WifiManager)context.getSystemService("wifi")).getConfiguredNetworks();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_218;
        }
        context = new JSONArray();
        obj = ((List) (obj)).iterator();
_L1:
        WifiConfiguration wificonfiguration;
        if (!((Iterator) (obj)).hasNext())
        {
            break MISSING_BLOCK_LABEL_199;
        }
        wificonfiguration = (WifiConfiguration)((Iterator) (obj)).next();
        try
        {
            JSONObject jsonobject = new JSONObject();
            jsonobject.put("networkId", wificonfiguration.networkId);
            jsonobject.put("priority", wificonfiguration.priority);
            jsonobject.put("name", wificonfiguration.SSID);
            jsonobject.put("id", wificonfiguration.BSSID);
            jsonobject.put("allowedKeyManagement", a(wificonfiguration.allowedKeyManagement));
            jsonobject.put("allowedAuthAlgorithms", a(wificonfiguration.allowedAuthAlgorithms));
            jsonobject.put("allowedGroupCiphers", a(wificonfiguration.allowedGroupCiphers));
            jsonobject.put("allowedPairwiseCiphers", a(wificonfiguration.allowedPairwiseCiphers));
            context.put(jsonobject);
        }
        catch (Throwable throwable) { }
          goto _L1
        int i1 = context.length();
        if (i1 <= 0)
        {
            context = null;
        }
        return context;
        context;
        return null;
    }

    public static JSONArray v(Context context)
    {
        if (!an.a)
        {
            return null;
        }
        Object obj;
        WifiInfo wifiinfo;
        if (!an.b(context, "android.permission.ACCESS_WIFI_STATE"))
        {
            break MISSING_BLOCK_LABEL_287;
        }
        obj = (WifiManager)context.getSystemService("wifi");
        if (!((WifiManager) (obj)).isWifiEnabled())
        {
            break MISSING_BLOCK_LABEL_287;
        }
        wifiinfo = ((WifiManager) (obj)).getConnectionInfo();
        if (wifiinfo == null)
        {
            break MISSING_BLOCK_LABEL_287;
        }
        JSONObject jsonobject;
        String s1;
        if (wifiinfo.getBSSID() == null)
        {
            break MISSING_BLOCK_LABEL_287;
        }
        s1 = wifiinfo.getBSSID();
        context = new JSONArray();
        jsonobject = new JSONObject();
        jsonobject.put("name", wifiinfo.getSSID());
        jsonobject.put("id", s1);
        jsonobject.put("level", wifiinfo.getRssi());
        jsonobject.put("hidden", wifiinfo.getHiddenSSID());
        jsonobject.put("ip", wifiinfo.getIpAddress());
        jsonobject.put("speed", wifiinfo.getLinkSpeed());
        jsonobject.put("networkId", wifiinfo.getNetworkId());
        jsonobject.put("mac", wifiinfo.getMacAddress());
        obj = ((WifiManager) (obj)).getDhcpInfo();
        if (obj != null)
        {
            try
            {
                JSONObject jsonobject1 = new JSONObject();
                jsonobject1.put("dns1", ((DhcpInfo) (obj)).dns1);
                jsonobject1.put("dns2", ((DhcpInfo) (obj)).dns2);
                jsonobject1.put("gw", ((DhcpInfo) (obj)).gateway);
                jsonobject1.put("ip", ((DhcpInfo) (obj)).ipAddress);
                jsonobject1.put("mask", ((DhcpInfo) (obj)).netmask);
                jsonobject1.put("server", ((DhcpInfo) (obj)).serverAddress);
                jsonobject1.put("leaseDuration", ((DhcpInfo) (obj)).leaseDuration);
                jsonobject.put("dhcp", jsonobject1);
            }
            catch (Throwable throwable) { }
        }
        context.put(jsonobject);
        return context;
        context;
        return null;
    }

    public static JSONArray w(Context context)
    {
        if (!an.a || !an.c && !an.d)
        {
            return null;
        }
        Object obj;
        boolean flag;
        if (!an.b(context, "android.permission.ACCESS_WIFI_STATE"))
        {
            break MISSING_BLOCK_LABEL_272;
        }
        obj = (WifiManager)context.getSystemService("wifi");
        if (!((WifiManager) (obj)).isWifiEnabled() && !((WifiManager) (obj)).isScanAlwaysAvailable())
        {
            break MISSING_BLOCK_LABEL_272;
        }
        flag = an.b(context, "android.permission.CHANGE_WIFI_STATE");
        if (!flag) goto _L2; else goto _L1
_L1:
        Object obj1;
        obj1 = new Object();
        IntentFilter intentfilter = new IntentFilter("android.net.wifi.SCAN_RESULTS");
        context.registerReceiver(new t(obj1, context), intentfilter);
        ((WifiManager) (obj)).startScan();
        obj1;
        JVM INSTR monitorenter ;
        obj1.wait(2000L);
        obj1;
        JVM INSTR monitorexit ;
_L2:
        obj = ((WifiManager) (obj)).getScanResults();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_272;
        }
        context = new JSONArray();
        obj = ((List) (obj)).iterator();
_L3:
        JSONObject jsonobject;
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break; /* Loop/switch isn't completed */
            }
            obj1 = (ScanResult)((Iterator) (obj)).next();
        } while (((ScanResult) (obj1)).level < -85);
        jsonobject = new JSONObject();
        try
        {
            jsonobject.put("id", ((ScanResult) (obj1)).BSSID);
            jsonobject.put("name", ((ScanResult) (obj1)).SSID);
            jsonobject.put("level", ((ScanResult) (obj1)).level);
            jsonobject.put("freq", ((ScanResult) (obj1)).frequency);
            if (an.a(17))
            {
                jsonobject.put("ts", ((ScanResult) (obj1)).timestamp);
            }
            context.put(jsonobject);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1) { }
          goto _L3
        context;
        obj1;
        JVM INSTR monitorexit ;
        try
        {
            throw context;
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        if (true) goto _L2; else goto _L4
_L4:
        return context;
        context;
        return null;
    }

    public static JSONArray x(Context context)
    {
        JSONArray jsonarray = new JSONArray();
        Object obj;
        ArrayList arraylist;
        obj = (TelephonyManager)context.getSystemService("phone");
        String s1 = ((TelephonyManager) (obj)).getDeviceId();
        arraylist = new ArrayList();
        if (!b(s1.trim()).booleanValue())
        {
            break MISSING_BLOCK_LABEL_73;
        }
        arraylist.add(s1.trim());
        obj = a(((TelephonyManager) (obj)), s1);
        Object obj1;
        int i1;
        if (obj != null)
        {
            try
            {
                jsonarray.put(obj);
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                break; /* Loop/switch isn't completed */
            }
        }
        obj = (TelephonyManager)context.getSystemService("phone1");
        obj1 = ((TelephonyManager) (obj)).getDeviceId();
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_139;
        }
        if (!b(((String) (obj1))).booleanValue() || arraylist.contains(obj1))
        {
            break MISSING_BLOCK_LABEL_139;
        }
        arraylist.add(obj1);
        obj = a(((TelephonyManager) (obj)), ((String) (obj1)));
        if (obj != null)
        {
            try
            {
                jsonarray.put(obj);
            }
            catch (Throwable throwable1) { }
        }
        obj = (TelephonyManager)context.getSystemService("phone2");
        obj1 = ((TelephonyManager) (obj)).getDeviceId();
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_205;
        }
        if (!b(((String) (obj1))).booleanValue() || arraylist.contains(obj1))
        {
            break MISSING_BLOCK_LABEL_205;
        }
        arraylist.add(obj1);
        obj = a(((TelephonyManager) (obj)), ((String) (obj1)));
        if (obj != null)
        {
            try
            {
                jsonarray.put(obj);
            }
            catch (Throwable throwable) { }
        }
        obj = C(context);
        obj1 = B(context);
        if (obj1 != null)
        {
            obj = obj1;
        }
        obj1 = A(context);
        if (obj1 != null)
        {
            obj = obj1;
        }
        context = z(context);
        if (context != null)
        {
            obj = context;
        }
        if (obj == null) goto _L2; else goto _L1
_L1:
        if (((JSONArray) (obj)).length() <= 0) goto _L2; else goto _L3
_L3:
        i1 = 0;
_L4:
        if (i1 >= ((JSONArray) (obj)).length())
        {
            break; /* Loop/switch isn't completed */
        }
        context = ((JSONArray) (obj)).getJSONObject(i1);
        String s2 = context.getString("imei");
        if (!arraylist.contains(s2))
        {
            arraylist.add(s2);
            jsonarray.put(context);
        }
        i1++;
        continue; /* Loop/switch isn't completed */
        if (true) goto _L4; else goto _L2
_L2:
        return jsonarray;
    }

    public static Map y(Context context)
    {
        int i1;
        i1 = 1;
        if (!e(context))
        {
            break MISSING_BLOCK_LABEL_169;
        }
        Object obj = b(context);
        if (obj == null) goto _L2; else goto _L1
_L1:
        int j1;
        context = ((String) (obj)).substring(0, ((String) (obj)).lastIndexOf('.') + 1);
        j1 = Integer.parseInt(((String) (obj)).substring(((String) (obj)).lastIndexOf('.') + 1));
        DatagramSocket datagramsocket;
        byte abyte0[];
        if (j1 - 5 >= 1)
        {
            i1 = j1 - 5;
        }
          goto _L3
_L6:
        obj = InetAddress.getByName((new StringBuilder()).append(context).append(i1).toString());
        datagramsocket = new DatagramSocket();
        abyte0 = new byte[1];
        abyte0[0] = 1;
        try
        {
            datagramsocket.send(new DatagramPacket(abyte0, abyte0.length, ((InetAddress) (obj)), 80));
            Thread.sleep(200L);
            break; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        break MISSING_BLOCK_LABEL_169;
_L2:
        context = c();
        return context;
_L3:
        if (i1 >= j1 + 7) goto _L2; else goto _L4
_L4:
        if (i1 <= 254) goto _L6; else goto _L5
_L5:
        i1++;
          goto _L3
        return null;
    }

    private static JSONArray z(Context context)
    {
        JSONArray jsonarray;
        Object obj;
        TelephonyManager telephonymanager;
        String s1;
        Object obj1;
        try
        {
            jsonarray = new JSONArray();
            telephonymanager = (TelephonyManager)context.getSystemService("phone");
            context = Class.forName("com.android.internal.telephony.Phone");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        try
        {
            obj = context.getField("GEMINI_SIM_1");
            ((Field) (obj)).setAccessible(true);
            obj = (Integer)((Field) (obj)).get(null);
            context = context.getField("GEMINI_SIM_2");
            context.setAccessible(true);
            context = (Integer)context.get(null);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context = Integer.valueOf(1);
            obj = Integer.valueOf(0);
        }
        obj1 = android/telephony/TelephonyManager.getDeclaredMethod("getDeviceIdGemini", new Class[] {
            Integer.TYPE
        });
        s1 = ((String)((Method) (obj1)).invoke(telephonymanager, new Object[] {
            obj
        })).trim();
        obj1 = ((String)((Method) (obj1)).invoke(telephonymanager, new Object[] {
            context
        })).trim();
        if (b(s1).booleanValue())
        {
            jsonarray.put(a(android/telephony/TelephonyManager, telephonymanager, ((Integer) (obj)), s1, "Gemini"));
        }
        if (b(((String) (obj1))).booleanValue())
        {
            jsonarray.put(a(android/telephony/TelephonyManager, telephonymanager, context, ((String) (obj1)), "Gemini"));
        }
        return jsonarray;
    }

    static 
    {
        b = false;
    }
}
