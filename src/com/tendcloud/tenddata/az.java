// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URI;
import java.net.URL;
import java.security.MessageDigest;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpProtocolParams;

// Referenced classes of package com.tendcloud.tenddata:
//            ba, an, aj

public class az
{
    static class a extends SSLSocketFactory
    {

        SSLContext a;

        public Socket createSocket()
        {
            return a.getSocketFactory().createSocket();
        }

        public Socket createSocket(Socket socket, String s, int k, boolean flag)
        {
            return a.getSocketFactory().createSocket(socket, s, k, flag);
        }

        a(X509Certificate x509certificate)
        {
            super(null);
            a = SSLContext.getInstance("TLS");
            x509certificate = new b(x509certificate);
            a.init(null, new TrustManager[] {
                x509certificate
            }, null);
        }
    }

    static class b
        implements X509TrustManager
    {

        X509Certificate a;

        public void checkClientTrusted(X509Certificate ax509certificate[], String s)
        {
        }

        public void checkServerTrusted(X509Certificate ax509certificate[], String s)
        {
            int l = ax509certificate.length;
            for (int k = 0; k < l; k++)
            {
                if (ax509certificate[k].equals(a))
                {
                    return;
                }
            }

            throw new CertificateException("no trusted cert");
        }

        public X509Certificate[] getAcceptedIssuers()
        {
            return null;
        }

        b(X509Certificate x509certificate)
        {
            a = x509certificate;
        }
    }


    private static final int i = 60000;
    private static final int j = 60000;
    final String a;
    final int b;
    HttpClient c;
    String d;
    String e;
    String f;
    String g;
    Context h;

    public az(Context context, X509Certificate x509certificate, String s, String s1, int k)
    {
        f = "";
        g = null;
        a = s;
        b = k;
        d = s1;
        h = context;
        try
        {
            x509certificate = new a(x509certificate);
            x509certificate.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            x509certificate = new Scheme("https", x509certificate, k);
            c = f(s);
            c.getConnectionManager().getSchemeRegistry().register(x509certificate);
            if (ba.a())
            {
                break MISSING_BLOCK_LABEL_131;
            }
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_123;
        }
        f = PreferenceManager.getDefaultSharedPreferences(context).getString(an.c(s), "");
        e(a);
    }

    public az(String s, String s1, int k)
    {
        this(g(s), s1, null, k);
    }

    public az(String s, String s1, String s2, int k)
    {
        this(g(s), s1, s2, k);
    }

    public az(X509Certificate x509certificate, String s, String s1, int k)
    {
        this(null, x509certificate, s, s1, k);
    }

    private int a(String s, byte abyte0[], String s1)
    {
        int k;
        try
        {
            s = new HttpPost(a(s, s1));
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return 600;
        }
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_35;
        }
        if (abyte0.length > 0)
        {
            s.setEntity(new ByteArrayEntity(abyte0));
        }
        s = a().execute(s);
        s.getEntity().consumeContent();
        k = s.getStatusLine().getStatusCode();
        return k;
    }

    private void b()
    {
        if (!f.equalsIgnoreCase(e) && h != null)
        {
            PreferenceManager.getDefaultSharedPreferences(h).edit().putString(an.c(a), e).commit();
            f = e;
        }
    }

    private HttpClient f(String s)
    {
        Object obj = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(((org.apache.http.params.HttpParams) (obj)), 60000);
        HttpConnectionParams.setSoTimeout(((org.apache.http.params.HttpParams) (obj)), 60000);
        HttpConnectionParams.setTcpNoDelay(((org.apache.http.params.HttpParams) (obj)), true);
        HttpProtocolParams.setVersion(((org.apache.http.params.HttpParams) (obj)), new ProtocolVersion("HTTP", 1, 1));
        obj = new DefaultHttpClient(((org.apache.http.params.HttpParams) (obj)));
        ((DefaultHttpClient) (obj)).addRequestInterceptor(new aj(this, s));
        return ((HttpClient) (obj));
    }

    private static X509Certificate g(String s)
    {
        s = new ByteArrayInputStream(s.getBytes());
        try
        {
            s = (X509Certificate)CertificateFactory.getInstance("X.509").generateCertificate(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        finally
        {
            throw s;
        }
        return s;
    }

    public int a(String s, byte abyte0[])
    {
        int k;
        if (!"https".equalsIgnoreCase((new URL(s)).getProtocol()))
        {
            break MISSING_BLOCK_LABEL_29;
        }
        k = b(s, abyte0);
        return k;
        s;
        return 601;
    }

    public final String a(String s)
    {
        s = new HttpGet(a(s, e));
        s = a().execute(s);
        Object obj;
        StringBuilder stringbuilder;
        char ac[];
        if (s.getStatusLine().getStatusCode() != 200)
        {
            break MISSING_BLOCK_LABEL_140;
        }
        obj = new InputStreamReader(s.getEntity().getContent());
        stringbuilder = new StringBuilder();
        ac = new char[64];
_L3:
        int k = ((Reader) (obj)).read(ac);
        if (k == -1) goto _L2; else goto _L1
_L1:
        stringbuilder.append(ac, 0, k);
          goto _L3
        Exception exception;
        exception;
        try
        {
            s.getEntity().consumeContent();
            throw exception;
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
_L4:
        return null;
_L2:
        exception = stringbuilder.toString();
        s.getEntity().consumeContent();
        return exception;
        s.getEntity().consumeContent();
          goto _L4
    }

    public final String a(String s, File file)
    {
        s = new HttpGet(a(s, e));
        s = a().execute(s);
        InputStream inputstream;
        MessageDigest messagedigest;
        if (s.getStatusLine().getStatusCode() != 200)
        {
            break MISSING_BLOCK_LABEL_167;
        }
        inputstream = s.getEntity().getContent();
        file = new FileOutputStream(file);
        messagedigest = MessageDigest.getInstance("MD5");
        byte abyte0[] = new byte[4096];
_L3:
        int k = inputstream.read(abyte0);
        if (k == -1) goto _L2; else goto _L1
_L1:
        file.write(abyte0, 0, k);
        messagedigest.update(abyte0, 0, k);
          goto _L3
        Exception exception;
        exception;
        file.close();
        throw exception;
        file;
        try
        {
            s.getEntity().consumeContent();
            throw file;
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
_L4:
        return null;
_L2:
        file.close();
        file = an.a(messagedigest.digest());
        s.getEntity().consumeContent();
        return file;
        s.getEntity().consumeContent();
          goto _L4
    }

    protected URI a(String s, String s1)
    {
        s = new URL(s);
        if (ba.a())
        {
            return s.toURI();
        } else
        {
            return (new URL(s.getProtocol(), s1, s.getPort(), s.getFile())).toURI();
        }
    }

    public HttpClient a()
    {
        return c;
    }

    public int b(String s)
    {
        return b(s, null);
    }

    public int b(String s, byte abyte0[])
    {
        int k;
        if (g != null)
        {
            int l = a(s, abyte0, g);
            k = l;
            if (l == 600)
            {
                g = null;
                k = l;
            }
        } else
        {
            int j1;
            if (e != null)
            {
                int i1 = a(s, abyte0, e);
                k = i1;
                if (i1 != 600)
                {
                    g = e;
                    b();
                    k = i1;
                }
            } else
            {
                k = 600;
            }
            j1 = k;
            if (k == 600)
            {
                j1 = k;
                if (f.length() > 2)
                {
                    k = a(s, abyte0, f);
                    j1 = k;
                    if (k != 600)
                    {
                        g = f;
                        j1 = k;
                    }
                }
            }
            k = j1;
            if (j1 == 600)
            {
                int k1 = a(s, abyte0, d);
                k = k1;
                if (k1 != 600)
                {
                    g = d;
                    return k1;
                }
            }
        }
        return k;
    }

    public int c(String s)
    {
        return a(s, ((byte []) (null)));
    }

    public int d(String s)
    {
        s = new HttpGet(s);
        int k;
        try
        {
            s = a().execute(s);
            s.getEntity().consumeContent();
            k = s.getStatusLine().getStatusCode();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return 600;
        }
        return k;
    }

    protected void e(String s)
    {
        try
        {
            e = InetAddress.getByName(s).getHostAddress();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }
}
