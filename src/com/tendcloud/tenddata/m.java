// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import java.io.OutputStream;

// Referenced classes of package com.tendcloud.tenddata:
//            p, ab

final class m
{

    static final boolean e;
    final int a;
    final int b;
    final byte c[];
    int d;
    private final int f;
    private final int g;
    private int h;
    private boolean i;
    private int j;
    private int k;
    private final p l;
    private final int m[];
    private final ab n;
    private final int o;
    private final int p;
    private int q;
    private int r;

    m(int i1, int j1, int k1, int l1, int i2, int j2)
    {
        d = -1;
        h = -1;
        i = false;
        j = 0;
        k = 0;
        q = -1;
        c = new byte[a(i1, j1, k1, i2)];
        f = j1 + i1;
        g = k1 + i2;
        a = i2;
        b = l1;
        l = new p(i1);
        p = i1 + 1;
        m = new int[p];
        r = p;
        n = new ab(l1 - 1);
        if (j2 <= 0)
        {
            j2 = l1 / 4 + 4;
        }
        o = j2;
    }

    private static int a(int i1, int j1, int k1, int l1)
    {
        return j1 + i1 + (k1 + l1) + Math.min(i1 / 2 + 4096, 0x20000000);
    }

    static void a(int ai[], int i1)
    {
        int j1 = 0;
        while (j1 < ai.length) 
        {
            if (ai[j1] <= i1)
            {
                ai[j1] = 0;
            } else
            {
                ai[j1] = ai[j1] - i1;
            }
            j1++;
        }
    }

    private int g()
    {
        int i1 = c(4, 4);
        if (i1 != 0)
        {
            int j1 = r + 1;
            r = j1;
            if (j1 == 0x7fffffff)
            {
                j1 = 0x7fffffff - p;
                l.d(j1);
                a(m, j1);
                r = r - j1;
            }
            j1 = q + 1;
            q = j1;
            if (j1 == p)
            {
                q = 0;
            }
        }
        return i1;
    }

    private void h()
    {
        int i1 = (d + 1) - f & 0xfffffff0;
        int j1 = j;
        System.arraycopy(c, i1, c, 0, j1 - i1);
        d = d - i1;
        h = h - i1;
        j = j - i1;
    }

    private void i()
    {
        if (k > 0 && d < h)
        {
            d = d - k;
            int i1 = k;
            k = 0;
            a(i1);
            if (!e && k >= i1)
            {
                throw new AssertionError();
            }
        }
    }

    public int a(int i1, int j1)
    {
        return c[(d + i1) - j1] & 0xff;
    }

    public int a(int i1, int j1, int k1)
    {
        int l1 = d + i1;
        for (i1 = 0; i1 < k1 && c[l1 + i1] == c[(l1 - j1 - 1) + i1]; i1++) { }
        return i1;
    }

    public int a(byte abyte0[], int i1, int j1)
    {
        if (!e && i)
        {
            throw new AssertionError();
        }
        if (d >= c.length - g)
        {
            h();
        }
        int k1 = j1;
        if (j1 > c.length - j)
        {
            k1 = c.length - j;
        }
        System.arraycopy(abyte0, i1, c, j, k1);
        j = j + k1;
        if (j >= g)
        {
            h = j - g;
        }
        i();
        return k1;
    }

    public ab a()
    {
        int j2 = 3;
        n.c = 0;
        int k1 = a;
        int i1 = b;
        int j1 = g();
        if (j1 < k1)
        {
            if (j1 == 0)
            {
                return n;
            }
            int ai[];
            ab ab1;
            ab ab2;
            int l1;
            int i2;
            int k2;
            int l2;
            if (i1 > j1)
            {
                k1 = j1;
            } else
            {
                k1 = j1;
                j1 = i1;
            }
        } else
        {
            j1 = i1;
        }
        l.a(c, d);
        l1 = r - l.a();
        i2 = r - l.b();
        k2 = l.c();
        l.c(r);
        m[q] = k2;
        if (l1 < p && c[d - l1] == c[d])
        {
            n.a[0] = 2;
            n.b[0] = l1 - 1;
            n.c = 1;
            i1 = 2;
        } else
        {
            i1 = 0;
        }
        if (l1 != i2 && i2 < p && c[d - i2] == c[d])
        {
            ai = n.b;
            ab2 = n;
            i1 = ab2.c;
            ab2.c = i1 + 1;
            ai[i1] = i2 - 1;
            i1 = 3;
        } else
        {
            i2 = l1;
        }
        l1 = i1;
        if (n.c > 0)
        {
            for (; i1 < k1 && c[(d + i1) - i2] == c[d + i1]; i1++) { }
            n.a[n.c - 1] = i1;
            l1 = i1;
            if (i1 >= j1)
            {
                return n;
            }
        }
        if (l1 < 3)
        {
            i1 = j2;
        } else
        {
            i1 = l1;
        }
        l1 = o;
        i2 = k2;
        do
        {
            l2 = r - i2;
            if (l1 == 0 || l2 >= p)
            {
                return n;
            }
            ai = m;
            j2 = q;
            if (l2 > q)
            {
                i2 = p;
            } else
            {
                i2 = 0;
            }
            k2 = ai[i2 + (j2 - l2)];
            if (c[(d + i1) - l2] == c[d + i1] && c[d - l2] == c[d])
            {
                j2 = 0;
                do
                {
                    i2 = j2 + 1;
                    if (i2 >= k1)
                    {
                        break;
                    }
                    j2 = i2;
                } while (c[(d + i2) - l2] == c[d + i2]);
                if (i2 > i1)
                {
                    n.a[n.c] = i2;
                    n.b[n.c] = l2 - 1;
                    ab1 = n;
                    ab1.c = ab1.c + 1;
                    i1 = i2;
                    if (i2 >= j1)
                    {
                        return n;
                    }
                }
            }
            l1--;
            i2 = k2;
        } while (true);
    }

    public void a(int i1)
    {
        int j1 = i1;
        if (!e)
        {
            j1 = i1;
            if (i1 < 0)
            {
                throw new AssertionError();
            }
        }
        do
        {
            i1 = j1 - 1;
            if (j1 > 0)
            {
                if (g() != 0)
                {
                    l.a(c, d);
                    m[q] = l.c();
                    l.c(r);
                    j1 = i1;
                } else
                {
                    j1 = i1;
                }
            } else
            {
                return;
            }
        } while (true);
    }

    public void a(int i1, byte abyte0[])
    {
        if (!e && b())
        {
            throw new AssertionError();
        }
        if (!e && j != 0)
        {
            throw new AssertionError();
        }
        if (abyte0 != null)
        {
            i1 = Math.min(abyte0.length, i1);
            System.arraycopy(abyte0, abyte0.length - i1, c, 0, i1);
            j = j + i1;
            a(i1);
        }
    }

    public void a(OutputStream outputstream, int i1, int j1)
    {
        outputstream.write(c, (d + 1) - i1, j1);
    }

    public boolean a(ab ab1)
    {
        int j1 = Math.min(e(), a);
        for (int i1 = 0; i1 < ab1.c; i1++)
        {
            if (b(ab1.b[i1], j1) != ab1.a[i1])
            {
                return false;
            }
        }

        return true;
    }

    public int b(int i1, int j1)
    {
        int l1 = d;
        int k1;
        for (k1 = 0; k1 < j1 && c[d + k1] == c[(l1 - i1 - 1) + k1]; k1++) { }
        return k1;
    }

    public boolean b()
    {
        return d != -1;
    }

    public boolean b(int i1)
    {
        return d - i1 < h;
    }

    public int c(int i1)
    {
        return c[d - i1] & 0xff;
    }

    int c(int i1, int j1)
    {
        int k1;
label0:
        {
            if (!e && i1 < j1)
            {
                throw new AssertionError();
            }
            d = d + 1;
            int l1 = j - d;
            k1 = l1;
            if (l1 >= i1)
            {
                break label0;
            }
            if (l1 >= j1)
            {
                k1 = l1;
                if (i)
                {
                    break label0;
                }
            }
            k = k + 1;
            k1 = 0;
        }
        return k1;
    }

    public void c()
    {
        h = j - 1;
        i();
    }

    public void d()
    {
        h = j - 1;
        i = true;
        i();
    }

    public int e()
    {
        if (!e && !b())
        {
            throw new AssertionError();
        } else
        {
            return j - d;
        }
    }

    public int f()
    {
        return d;
    }

    static 
    {
        boolean flag;
        if (!com/tendcloud/tenddata/m.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        e = flag;
    }
}
