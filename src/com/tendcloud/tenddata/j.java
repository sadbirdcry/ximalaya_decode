// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import java.lang.reflect.Array;

// Referenced classes of package com.tendcloud.tenddata:
//            ai, i, ay, l, 
//            m, ab

public abstract class j
{
    class a
    {

        static final int a = 8;
        static final int b = 8;
        static final int c = 256;
        private static final int i = 32;
        final short d[] = new short[2];
        final short e[][];
        final short f[][];
        final short g[] = new short[256];
        final j h;
        private final int j[];
        private final int k[][];

        private void a(int i1)
        {
            int k1 = com.tendcloud.tenddata.l.a(d[0], 0);
            int j1;
            for (j1 = 0; j1 < 8; j1++)
            {
                k[i1][j1] = com.tendcloud.tenddata.l.b(e[i1], j1) + k1;
            }

            k1 = com.tendcloud.tenddata.l.a(d[0], 1);
            int l1 = com.tendcloud.tenddata.l.a(d[1], 0);
            for (; j1 < 16; j1++)
            {
                k[i1][j1] = k1 + l1 + com.tendcloud.tenddata.l.b(f[i1], j1 - 8);
            }

            l1 = com.tendcloud.tenddata.l.a(d[1], 1);
            for (; j1 < k[i1].length; j1++)
            {
                k[i1][j1] = k1 + l1 + com.tendcloud.tenddata.l.b(g, j1 - 8 - 8);
            }

        }

        void a()
        {
            com.tendcloud.tenddata.l.a(d);
            for (int i1 = 0; i1 < e.length; i1++)
            {
                com.tendcloud.tenddata.l.a(e[i1]);
            }

            for (int j1 = 0; j1 < e.length; j1++)
            {
                com.tendcloud.tenddata.l.a(f[j1]);
            }

            com.tendcloud.tenddata.l.a(g);
            for (int k1 = 0; k1 < j.length; k1++)
            {
                j[k1] = 0;
            }

        }

        void a(int i1, int j1)
        {
            i1 -= 2;
            int ai1[];
            if (i1 < 8)
            {
                j.a(h).a(d, 0, 0);
                j.a(h).a(e[j1], i1);
            } else
            {
                j.a(h).a(d, 0, 1);
                i1 -= 8;
                if (i1 < 8)
                {
                    j.a(h).a(d, 1, 0);
                    j.a(h).a(f[j1], i1);
                } else
                {
                    j.a(h).a(d, 1, 1);
                    j.a(h).a(g, i1 - 8);
                }
            }
            ai1 = j;
            ai1[j1] = ai1[j1] - 1;
        }

        int b(int i1, int j1)
        {
            return k[j1][i1 - 2];
        }

        void b()
        {
            for (int i1 = 0; i1 < j.length; i1++)
            {
                if (j[i1] <= 0)
                {
                    j[i1] = 32;
                    a(i1);
                }
            }

        }

        a(int i1, int k1)
        {
            h = j.this;
            super();
            e = (short[][])Array.newInstance(Short.TYPE, new int[] {
                16, 8
            });
            f = (short[][])Array.newInstance(Short.TYPE, new int[] {
                16, 8
            });
            i1 = 1 << i1;
            j = new int[i1];
            k1 = Math.max((k1 - 2) + 1, 16);
            k = (int[][])Array.newInstance(Integer.TYPE, new int[] {
                i1, k1
            });
        }
    }

    class c
    {

        static final boolean b;
        a a[];
        final j c;
        private final int d;
        private final int e;

        final int a(int i1, int j1)
        {
            return (i1 >> 8 - d) + ((e & j1) << d);
        }

        int a(int i1, int j1, int k1, int l1, ai ai1)
        {
            int i2 = com.tendcloud.tenddata.l.a(c.q[ai1.b()][c.n & l1], 0);
            k1 = a(k1, l1);
            if (ai1.g())
            {
                i1 = a[k1].a(i1);
            } else
            {
                i1 = a[k1].a(i1, j1);
            }
            return i1 + i2;
        }

        void a()
        {
            for (int i1 = 0; i1 < a.length; i1++)
            {
                a[i1].a();
            }

        }

        void b()
        {
            if (!b && c.F < 0)
            {
                throw new AssertionError();
            } else
            {
                a[0].b();
                return;
            }
        }

        void c()
        {
            if (!b && c.F < 0)
            {
                throw new AssertionError();
            } else
            {
                int i1 = a(c.z.c(c.F + 1), c.z.f() - c.F);
                a[i1].b();
                return;
            }
        }

        static 
        {
            boolean flag;
            if (!com/tendcloud/tenddata/j.desiredAssertionStatus())
            {
                flag = true;
            } else
            {
                flag = false;
            }
            b = flag;
        }

        c(int i1, int k1)
        {
            c = j.this;
            super();
            d = i1;
            e = (1 << k1) - 1;
            a = new a[1 << i1 + k1];
            for (i1 = 0; i1 < a.length; i1++)
            {
                a[i1] = new a(this, null);
            }

        }
    }

    private class c.a
    {

        final short a[];
        final c b;

        int a(int i1)
        {
            int j1 = 0;
            i1 |= 0x100;
            int k1;
            int l1;
            do
            {
                k1 = j1 + com.tendcloud.tenddata.l.a(a[i1 >>> 8], i1 >>> 7 & 1);
                l1 = i1 << 1;
                j1 = k1;
                i1 = l1;
            } while (l1 < 0x10000);
            return k1;
        }

        int a(int i1, int j1)
        {
            int k1 = 0;
            int l1 = 256;
            i1 |= 0x100;
            int i2;
            int j2;
            do
            {
                j1 <<= 1;
                i2 = k1 + com.tendcloud.tenddata.l.a(a[(j1 & l1) + l1 + (i1 >>> 8)], i1 >>> 7 & 1);
                j2 = i1 << 1;
                l1 &= ~(j1 ^ j2);
                k1 = i2;
                i1 = j2;
            } while (j2 < 0x10000);
            return i2;
        }

        void a()
        {
            com.tendcloud.tenddata.l.a(a);
        }

        void b()
        {
            int i1 = b.c.z.c(b.c.F) | 0x100;
            int j1;
            if (b.c.p.g())
            {
                do
                {
                    j.a(b.c).a(a, i1 >>> 8, i1 >>> 7 & 1);
                    j1 = i1 << 1;
                    i1 = j1;
                } while (j1 < 0x10000);
            } else
            {
                int l1 = b.c.z.c(b.c.o[0] + 1 + b.c.F);
                int k1 = 256;
                int i2;
                do
                {
                    l1 <<= 1;
                    j.a(b.c).a(a, (l1 & k1) + k1 + (i1 >>> 8), i1 >>> 7 & 1);
                    i2 = i1 << 1;
                    k1 &= ~(l1 ^ i2);
                    i1 = i2;
                } while (i2 < 0x10000);
            }
            b.c.p.c();
        }

        private c.a(c c1)
        {
            b = c1;
            super();
            a = new short[768];
        }

        c.a(c c1, b b1)
        {
            this(c1);
        }
    }


    static final boolean G;
    private static final int H = 8166;
    private static final int I = 128;
    private static final int J = 16;
    private static final int a = 0x1ffeef;
    static final int b = 16;
    static final int c = 2;
    static final int d = 273;
    static final int e = 4;
    static final int f = 64;
    static final int g = 4;
    static final int h = 14;
    static final int i = 128;
    static final int j = 4;
    static final int k = 16;
    static final int l = 15;
    static final int m = 4;
    final c A;
    final a B;
    final a C;
    final int D;
    int E;
    int F;
    private final l K;
    private int L;
    private int M;
    private final int N;
    private final int O[][];
    private final int P[][];
    private final int Q[] = new int[16];
    private int R;
    final int n;
    final int o[] = new int[4];
    final ai p = new ai();
    final short q[][];
    final short r[] = new short[12];
    final short s[] = new short[12];
    final short t[] = new short[12];
    final short u[] = new short[12];
    final short v[][];
    final short w[][];
    final short x[][] = {
        new short[2], new short[2], new short[4], new short[4], new short[8], new short[8], new short[16], new short[16], new short[32], new short[32]
    };
    final short y[] = new short[16];
    final m z;

    j(l l1, m m1, int i1, int j1, int k1, int i2, int j2)
    {
        q = (short[][])Array.newInstance(Short.TYPE, new int[] {
            12, 16
        });
        v = (short[][])Array.newInstance(Short.TYPE, new int[] {
            12, 16
        });
        w = (short[][])Array.newInstance(Short.TYPE, new int[] {
            4, 64
        });
        L = 0;
        M = 0;
        P = (int[][])Array.newInstance(Integer.TYPE, new int[] {
            4, 128
        });
        E = 0;
        F = -1;
        R = 0;
        n = (1 << k1) - 1;
        K = l1;
        z = m1;
        D = j2;
        A = new c(i1, j1);
        B = new a(k1, j2);
        C = new a(k1, j2);
        N = b(i2 - 1) + 1;
        i1 = N;
        O = (int[][])Array.newInstance(Integer.TYPE, new int[] {
            4, i1
        });
        b();
    }

    static final int a(int i1)
    {
        if (i1 < 6)
        {
            return i1 - 2;
        } else
        {
            return 3;
        }
    }

    public static j a(l l1, int i1, int j1, int k1, int i2, int j2, int k2, int l2)
    {
        if (Runtime.getRuntime().availableProcessors() <= 2)
        {
            return new i(l1, i1, j1, k1, i2, j2, k2, l2);
        } else
        {
            return new ay(l1, i1, j1, k1, i2, j2, k2, l2);
        }
    }

    static l a(j j1)
    {
        return j1.K;
    }

    private void a(int i1, int j1, int k1)
    {
        p.d();
        B.a(j1, k1);
        k1 = b(i1);
        K.a(w[a(j1)], k1);
        if (k1 >= 4)
        {
            j1 = (k1 >>> 1) - 1;
            int l1 = i1 - ((k1 & 1 | 2) << j1);
            if (k1 < 14)
            {
                K.c(x[k1 - 4], l1);
            } else
            {
                K.b(l1 >>> 4, j1 - 4);
                K.c(y, l1 & 0xf);
                M = M - 1;
            }
        }
        o[3] = o[2];
        o[2] = o[1];
        o[1] = o[0];
        o[0] = i1;
        L = L - 1;
    }

    public static int b(int i1)
    {
        if (i1 <= 4)
        {
            return i1;
        }
        int l1 = 31;
        int j1;
        int k1;
        int i2;
        if ((0xffff0000 & i1) == 0)
        {
            i2 = i1 << 16;
            l1 = 15;
        } else
        {
            i2 = i1;
        }
        j1 = l1;
        k1 = i2;
        if ((0xff000000 & i2) == 0)
        {
            k1 = i2 << 8;
            j1 = l1 - 8;
        }
        l1 = j1;
        i2 = k1;
        if ((0xf0000000 & k1) == 0)
        {
            i2 = k1 << 4;
            l1 = j1 - 4;
        }
        j1 = l1;
        k1 = i2;
        if ((0xc0000000 & i2) == 0)
        {
            k1 = i2 << 2;
            j1 = l1 - 2;
        }
        l1 = j1;
        if ((k1 & 0x80000000) == 0)
        {
            l1 = j1 - 1;
        }
        return (l1 << 1) + (i1 >>> l1 - 1 & 1);
    }

    private void b(int i1, int j1, int k1)
    {
        boolean flag = false;
        if (i1 == 0)
        {
            K.a(s, p.b(), 0);
            l l1 = K;
            short aword0[] = v[p.b()];
            if (j1 == 1)
            {
                i1 = ((flag) ? 1 : 0);
            } else
            {
                i1 = 1;
            }
            l1.a(aword0, k1, i1);
        } else
        {
            int i2 = o[i1];
            K.a(s, p.b(), 1);
            if (i1 == 1)
            {
                K.a(t, p.b(), 0);
            } else
            {
                K.a(t, p.b(), 1);
                K.a(u, p.b(), i1 - 2);
                if (i1 == 3)
                {
                    o[3] = o[2];
                }
                o[2] = o[1];
            }
            o[1] = o[0];
            o[0] = i2;
        }
        if (j1 == 1)
        {
            p.f();
            return;
        } else
        {
            C.a(j1, k1);
            p.e();
            return;
        }
    }

    private boolean i()
    {
        if (!G && F != -1)
        {
            throw new AssertionError();
        }
        if (!z.b(0))
        {
            return false;
        }
        c(1);
        K.a(q[p.b()], 0, 0);
        A.b();
        F = F - 1;
        if (!G && F != -1)
        {
            throw new AssertionError();
        }
        R = R + 1;
        if (!G && R != 1)
        {
            throw new AssertionError();
        } else
        {
            return true;
        }
    }

    private boolean j()
    {
        if (!z.b(F + 1))
        {
            return false;
        }
        int i1 = a();
        if (!G && F < 0)
        {
            throw new AssertionError();
        }
        int j1 = z.f() - F & n;
        if (E == -1)
        {
            if (!G && i1 != 1)
            {
                throw new AssertionError();
            }
            K.a(q[p.b()], j1, 0);
            A.c();
        } else
        {
            K.a(q[p.b()], j1, 1);
            if (E < 4)
            {
                if (!G && z.a(-F, o[E], i1) != i1)
                {
                    throw new AssertionError();
                }
                K.a(r, p.b(), 1);
                b(E, i1, j1);
            } else
            {
                if (!G && z.a(-F, E - 4, i1) != i1)
                {
                    throw new AssertionError();
                }
                K.a(r, p.b(), 0);
                a(E - 4, i1, j1);
            }
        }
        F = F - i1;
        R = R + i1;
        return true;
    }

    private void k()
    {
        L = 128;
        for (int i1 = 0; i1 < 4; i1++)
        {
            for (int k1 = 0; k1 < N; k1++)
            {
                O[i1][k1] = com.tendcloud.tenddata.l.b(w[i1], k1);
            }

            for (int l1 = 14; l1 < N; l1++)
            {
                int ai1[] = O[i1];
                int k2 = ai1[l1];
                ai1[l1] = com.tendcloud.tenddata.l.a((l1 >>> 1) - 1 - 4) + k2;
            }

            for (int i2 = 0; i2 < 4; i2++)
            {
                P[i1][i2] = O[i1][i2];
            }

        }

        int j2 = 4;
        int j1 = 4;
        for (; j2 < 14; j2++)
        {
            int j3 = x[j2 - 4].length;
            for (int l2 = 0; l2 < j3; l2++)
            {
                int k3 = com.tendcloud.tenddata.l.d(x[j2 - 4], j1 - ((j2 & 1 | 2) << (j2 >>> 1) - 1));
                for (int i3 = 0; i3 < 4; i3++)
                {
                    P[i3][j1] = O[i3][j2] + k3;
                }

                j1++;
            }

        }

        if (!G && j1 != 128)
        {
            throw new AssertionError();
        } else
        {
            return;
        }
    }

    private void l()
    {
        M = 16;
        for (int i1 = 0; i1 < 16; i1++)
        {
            Q[i1] = com.tendcloud.tenddata.l.d(y, i1);
        }

    }

    abstract int a();

    int a(int i1, int j1, int k1, int l1)
    {
        i1 = B.b(k1, l1) + i1;
        k1 = a(k1);
        if (j1 < 128)
        {
            return i1 + P[k1][j1];
        } else
        {
            l1 = b(j1);
            return i1 + (O[k1][l1] + Q[j1 & 0xf]);
        }
    }

    int a(int i1, int j1, ai ai1, int k1)
    {
        if (j1 == 0)
        {
            return com.tendcloud.tenddata.l.a(s[ai1.b()], 0) + com.tendcloud.tenddata.l.a(v[ai1.b()][k1], 1) + i1;
        }
        i1 = com.tendcloud.tenddata.l.a(s[ai1.b()], 1) + i1;
        if (j1 == 1)
        {
            return i1 + com.tendcloud.tenddata.l.a(t[ai1.b()], 0);
        } else
        {
            return i1 + (com.tendcloud.tenddata.l.a(t[ai1.b()], 1) + com.tendcloud.tenddata.l.a(u[ai1.b()], j1 - 2));
        }
    }

    int a(int i1, ai ai1)
    {
        return com.tendcloud.tenddata.l.a(r[ai1.b()], 0) + i1;
    }

    int a(int i1, ai ai1, int j1)
    {
        return com.tendcloud.tenddata.l.a(s[ai1.b()], 0) + i1 + com.tendcloud.tenddata.l.a(v[ai1.b()][j1], 0);
    }

    int a(ai ai1, int i1)
    {
        return com.tendcloud.tenddata.l.a(q[ai1.b()][i1], 1);
    }

    int b(int i1, int j1, ai ai1, int k1)
    {
        return a(b(a(ai1, k1), ai1), i1, ai1, k1) + C.b(j1, k1);
    }

    int b(int i1, ai ai1)
    {
        return com.tendcloud.tenddata.l.a(r[ai1.b()], 1) + i1;
    }

    public void b()
    {
        o[0] = 0;
        o[1] = 0;
        o[2] = 0;
        o[3] = 0;
        p.a();
        for (int i1 = 0; i1 < q.length; i1++)
        {
            com.tendcloud.tenddata.l.a(q[i1]);
        }

        com.tendcloud.tenddata.l.a(r);
        com.tendcloud.tenddata.l.a(s);
        com.tendcloud.tenddata.l.a(t);
        com.tendcloud.tenddata.l.a(u);
        for (int j1 = 0; j1 < v.length; j1++)
        {
            com.tendcloud.tenddata.l.a(v[j1]);
        }

        for (int k1 = 0; k1 < w.length; k1++)
        {
            com.tendcloud.tenddata.l.a(w[k1]);
        }

        for (int l1 = 0; l1 < x.length; l1++)
        {
            com.tendcloud.tenddata.l.a(x[l1]);
        }

        com.tendcloud.tenddata.l.a(y);
        A.a();
        B.a();
        C.a();
        L = 0;
        M = 0;
        R = R + (F + 1);
        F = -1;
    }

    public m c()
    {
        return z;
    }

    void c(int i1)
    {
        F = F + i1;
        z.a(i1);
    }

    public int d()
    {
        return R;
    }

    public void e()
    {
        R = 0;
    }

    public boolean f()
    {
        if (!z.b() && !i())
        {
            return false;
        }
        while (R <= 0x1ffeef && K.b() <= 8166) 
        {
            if (!j())
            {
                return false;
            }
        }
        return true;
    }

    ab g()
    {
        F = F + 1;
        ab ab = z.a();
        if (!G && !z.a(ab))
        {
            throw new AssertionError();
        } else
        {
            return ab;
        }
    }

    void h()
    {
        if (L <= 0)
        {
            k();
        }
        if (M <= 0)
        {
            l();
        }
        B.b();
        C.b();
    }

    static 
    {
        boolean flag;
        if (!com/tendcloud/tenddata/j.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        G = flag;
    }

    // Unreferenced inner class com/tendcloud/tenddata/j$b
    static class b
    {
    }

}
