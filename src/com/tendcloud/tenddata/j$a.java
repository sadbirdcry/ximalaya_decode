// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import java.lang.reflect.Array;

// Referenced classes of package com.tendcloud.tenddata:
//            j, l

class Instance
{

    static final int a = 8;
    static final int b = 8;
    static final int c = 256;
    private static final int i = 32;
    final short d[] = new short[2];
    final short e[][];
    final short f[][];
    final short g[] = new short[256];
    final j h;
    private final int j[];
    private final int k[][];

    private void a(int i1)
    {
        int k1 = l.a(d[0], 0);
        int j1;
        for (j1 = 0; j1 < 8; j1++)
        {
            k[i1][j1] = l.b(e[i1], j1) + k1;
        }

        k1 = l.a(d[0], 1);
        int l1 = l.a(d[1], 0);
        for (; j1 < 16; j1++)
        {
            k[i1][j1] = k1 + l1 + l.b(f[i1], j1 - 8);
        }

        l1 = l.a(d[1], 1);
        for (; j1 < k[i1].length; j1++)
        {
            k[i1][j1] = k1 + l1 + l.b(g, j1 - 8 - 8);
        }

    }

    void a()
    {
        l.a(d);
        for (int i1 = 0; i1 < e.length; i1++)
        {
            l.a(e[i1]);
        }

        for (int j1 = 0; j1 < e.length; j1++)
        {
            l.a(f[j1]);
        }

        l.a(g);
        for (int k1 = 0; k1 < j.length; k1++)
        {
            j[k1] = 0;
        }

    }

    void a(int i1, int j1)
    {
        i1 -= 2;
        int ai[];
        if (i1 < 8)
        {
            com.tendcloud.tenddata.j.a(h).a(d, 0, 0);
            com.tendcloud.tenddata.j.a(h).a(e[j1], i1);
        } else
        {
            com.tendcloud.tenddata.j.a(h).a(d, 0, 1);
            i1 -= 8;
            if (i1 < 8)
            {
                com.tendcloud.tenddata.j.a(h).a(d, 1, 0);
                com.tendcloud.tenddata.j.a(h).a(f[j1], i1);
            } else
            {
                com.tendcloud.tenddata.j.a(h).a(d, 1, 1);
                com.tendcloud.tenddata.j.a(h).a(g, i1 - 8);
            }
        }
        ai = j;
        ai[j1] = ai[j1] - 1;
    }

    int b(int i1, int j1)
    {
        return k[j1][i1 - 2];
    }

    void b()
    {
        for (int i1 = 0; i1 < j.length; i1++)
        {
            if (j[i1] <= 0)
            {
                j[i1] = 32;
                a(i1);
            }
        }

    }

    (j j1, int i1, int k1)
    {
        h = j1;
        super();
        e = (short[][])Array.newInstance(Short.TYPE, new int[] {
            16, 8
        });
        f = (short[][])Array.newInstance(Short.TYPE, new int[] {
            16, 8
        });
        i1 = 1 << i1;
        j = new int[i1];
        k1 = Math.max((k1 - 2) + 1, 16);
        k = (int[][])Array.newInstance(Integer.TYPE, new int[] {
            i1, k1
        });
    }
}
