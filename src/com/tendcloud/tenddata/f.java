// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.content.Context;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.Looper;

// Referenced classes of package com.tendcloud.tenddata:
//            ak, ah

public class f
{
    public static interface a
    {

        public abstract void a();
    }


    private static f b = null;
    private static final long c = 10000L;
    private static final int d = 10;
    private static final int e = 2;
    private Context a;
    private SensorManager f;
    private a g;
    private Handler h;
    private SensorEventListener i;

    private f(Context context)
    {
        a = null;
        g = null;
        h = new ak(this, Looper.getMainLooper());
        i = new ah(this);
        a = context;
        f = (SensorManager)context.getSystemService("sensor");
        f.registerListener(i, f.getDefaultSensor(1), 1);
        h.sendEmptyMessageDelayed(10, 10000L);
    }

    static SensorManager a(f f1)
    {
        return f1.f;
    }

    public static f a(Context context)
    {
        if (b == null && b == null)
        {
            b(context);
        }
        return b;
    }

    public static void a(float af[], float af1[])
    {
        int l = af.length;
        int j = 0;
        while (j < l) 
        {
            int k = 0;
            double d1;
            double d2;
            double d4;
            for (d1 = 0.0D; k < l; d1 += d2 * d4)
            {
                d2 = Math.cos((3.1415926535897931D * (double)j * (2D * (double)k + 1.0D)) / (double)(l * 2));
                d4 = af[k];
                k++;
            }

            double d3;
            if (j == 0)
            {
                d3 = 1.0D / Math.sqrt(2D);
            } else
            {
                d3 = 1.0D;
            }
            af1[j] = (float)(d3 * d1 * Math.sqrt(2D / (double)l));
            j++;
        }
    }

    static SensorEventListener b(f f1)
    {
        return f1.i;
    }

    private static void b(Context context)
    {
        com/tendcloud/tenddata/f;
        JVM INSTR monitorenter ;
        if (b == null)
        {
            b = new f(context);
        }
        com/tendcloud/tenddata/f;
        JVM INSTR monitorexit ;
        return;
        context;
        throw context;
    }

    public static void b(float af[], float af1[])
    {
        int l = af.length;
        for (int j = 0; j < l; j++)
        {
            int k = 0;
            double d1 = 0.0D;
            while (k < l) 
            {
                double d3 = Math.cos((3.1415926535897931D * (double)k * ((double)j * 2D + 1.0D)) / (double)(l * 2));
                double d4 = af[k];
                double d2;
                if (k == 0)
                {
                    d2 = 1.0D / Math.sqrt(2D);
                } else
                {
                    d2 = 1.0D;
                }
                d1 += d2 * (d4 * d3);
                k++;
            }
            af1[j] = (float)(Math.sqrt(2D / (double)l) * d1);
        }

    }

    static a c(f f1)
    {
        return f1.g;
    }

    static Context d(f f1)
    {
        return f1.a;
    }

    public void a(a a1)
    {
        g = a1;
    }

}
