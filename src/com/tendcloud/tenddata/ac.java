// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.os.SystemClock;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.util.Map;
import java.util.TreeMap;
import java.util.zip.GZIPOutputStream;

// Referenced classes of package com.tendcloud.tenddata:
//            az, ad, s, TCAgent, 
//            bm

public final class ac
{

    public static final String a = "tdcv3.talkingdata.net";
    private static String b;
    private static final String c = "http://tdcv3.talkingdata.net/g/d";
    private static final az d;

    public ac()
    {
    }

    static boolean a(bm bm)
    {
        TreeMap treemap = new TreeMap();
        int j;
        ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream(1024);
        GZIPOutputStream gzipoutputstream = new GZIPOutputStream(bytearrayoutputstream);
        (new ad(gzipoutputstream)).a(bm);
        gzipoutputstream.finish();
        gzipoutputstream.close();
        int i = bytearrayoutputstream.size();
        long l = SystemClock.elapsedRealtime();
        j = d.b("http://tdcv3.talkingdata.net/g/d", bytearrayoutputstream.toByteArray());
        long l1 = SystemClock.elapsedRealtime();
        treemap.put("to", "analytics");
        treemap.put("size", Integer.valueOf(i));
        treemap.put("latency", Long.valueOf(l1 - l));
        treemap.put("code", Integer.valueOf(j));
        if (j == 200)
        {
            TCAgent.onEvent(s.h(), "__tx.sdk.send", null, treemap);
            return true;
        }
        if (TCAgent.LOG_ON)
        {
            Log.w("TDLog", (new StringBuilder()).append("Server response code:").append(j).toString());
        }
        TCAgent.onEvent(s.h(), "__tx.sdk.send", null, treemap);
        return false;
        bm;
        treemap.put("error", bm.getClass().getName());
        TCAgent.onEvent(s.h(), "__tx.sdk.send", null, treemap);
        return false;
        bm;
        TCAgent.onEvent(s.h(), "__tx.sdk.send", null, treemap);
        throw bm;
    }

    static 
    {
        b = "211.151.164.164";
        d = new az("", "tdcv3.talkingdata.net", b, 443);
    }
}
