// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.tendcloud.tenddata:
//            s, e, bm, as, 
//            c, ba, z, w, 
//            a, bb, ax, aw, 
//            TCAgent, ac, ad

final class d
{

    static final int a = 30000;
    static final int b = 0x6ddd00;
    static long c = 0L;
    static long d = 0L;
    static int e = 0;
    private static final int f = 20480;
    private static Handler g = null;
    private static Handler h = null;
    private static boolean i = false;
    private static final HandlerThread j;

    d()
    {
    }

    static Handler a()
    {
        com/tendcloud/tenddata/d;
        JVM INSTR monitorenter ;
        Handler handler;
        if (h == null)
        {
            e();
        }
        handler = h;
        com/tendcloud/tenddata/d;
        JVM INSTR monitorexit ;
        return handler;
        Exception exception;
        exception;
        throw exception;
    }

    private static void a(bm bm1)
    {
        com.tendcloud.tenddata.e.a(s.h());
        Object obj = bm1.e;
        com.tendcloud.tenddata.e.b(bm1.f);
        com.tendcloud.tenddata.e.c(bm1.g);
        com.tendcloud.tenddata.e.d(bm1.h);
        obj = ((List) (obj)).iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break;
            }
            Object obj1 = (as)((Iterator) (obj)).next();
            switch (((as) (obj1)).a)
            {
            case 1: // '\001'
                s.b(false);
                break;

            case 2: // '\002'
                obj1 = ((as) (obj1)).b;
                if (((c) (obj1)).c == 1)
                {
                    com.tendcloud.tenddata.e.a(((c) (obj1)).a);
                } else
                if (((c) (obj1)).c == 3)
                {
                    com.tendcloud.tenddata.e.b(((c) (obj1)).a);
                    com.tendcloud.tenddata.e.c(((c) (obj1)).a);
                    com.tendcloud.tenddata.e.d(((c) (obj1)).a);
                }
                break;
            }
        } while (true);
        com.tendcloud.tenddata.e.b();
        if (bm1.i != null)
        {
            s.f();
        }
    }

    public static void b()
    {
        try
        {
            i = false;
            f();
            if (i)
            {
                g.sendEmptyMessageDelayed(0, 100L);
            }
            return;
        }
        catch (Throwable throwable)
        {
            return;
        }
    }

    static void c()
    {
        com/tendcloud/tenddata/d;
        JVM INSTR monitorenter ;
        long l;
        long l1;
        l = SystemClock.elapsedRealtime();
        l1 = d;
        l -= l1;
        if (l >= 0L) goto _L2; else goto _L1
_L1:
        com/tendcloud/tenddata/d;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (c <= 0L || l >= (long)e)
        {
            break MISSING_BLOCK_LABEL_138;
        }
        l = (long)e - l;
_L6:
        Object obj = s.h();
        if (ba.c(((Context) (obj)))) goto _L4; else goto _L3
_L3:
        Log.w("TDLog", "network is disabled.");
        s.p = true;
        if (l != 0L) goto _L1; else goto _L5
_L5:
        s.w();
          goto _L1
        obj;
        throw obj;
_L4:
label0:
        {
            if (!s.g || ba.g(((Context) (obj))))
            {
                break label0;
            }
            Log.w("TDLog", "wifi is not connected.");
            s.p = true;
        }
          goto _L1
        g.sendEmptyMessageDelayed(0, l);
        d = l + SystemClock.elapsedRealtime();
          goto _L1
        l = 0L;
          goto _L6
    }

    static z d()
    {
        Context context = s.h();
        z z1 = new z();
        Object aobj[] = w.k();
        Object obj;
        try
        {
            z1.a = aobj[0];
            z1.b = Integer.valueOf(aobj[1]).intValue();
            z1.d = aobj[2];
            z1.c = Float.valueOf(aobj[3]).floatValue();
        }
        catch (Exception exception3) { }
        aobj = w.m();
        z1.g = aobj[0];
        z1.h = aobj[1];
        try
        {
            int ai[] = w.n();
            z1.i = ai[0];
            z1.j = ai[1];
            z1.k = ai[2];
            z1.l = ai[3];
        }
        catch (Exception exception2) { }
        z1.m = w.o();
        obj = new DisplayMetrics();
        ((WindowManager)(WindowManager)context.getSystemService("window")).getDefaultDisplay().getMetrics(((DisplayMetrics) (obj)));
        z1.n = (float)((DisplayMetrics) (obj)).widthPixels / ((DisplayMetrics) (obj)).xdpi;
        z1.o = (float)((DisplayMetrics) (obj)).heightPixels / ((DisplayMetrics) (obj)).ydpi;
        z1.p = ((DisplayMetrics) (obj)).densityDpi;
        z1.q = Build.DISPLAY;
        z1.r = "unknown";
        try
        {
            z1.r = (String)Class.forName("android.os.SystemProperties").getDeclaredMethod("get", new Class[] {
                java/lang/String
            }).invoke(null, new Object[] {
                "gsm.version.baseband"
            });
        }
        catch (Exception exception1) { }
        obj = com.tendcloud.tenddata.a.d(context);
        if (obj != null)
        {
            z1.s = ((String) (obj));
        }
        obj = com.tendcloud.tenddata.a.g(context);
        if (obj != null)
        {
            z1.t = ((String) (obj));
        }
        try
        {
            z1.y = com.tendcloud.tenddata.a.f(context);
            z1.z = bb.g(context).toString();
            z1.A = com.tendcloud.tenddata.a.e(context);
            z1.B = com.tendcloud.tenddata.a.c(context);
        }
        catch (Exception exception)
        {
            return z1;
        }
        return z1;
    }

    private static void e()
    {
        if (!s.j)
        {
            e = 0x6ddd00;
        }
        if (g == null)
        {
            g = new ax(j.getLooper());
        }
        if (h == null)
        {
            h = new aw(j.getLooper());
        }
    }

    private static boolean f()
    {
        if (s.h() == null)
        {
            return false;
        }
        bm bm1 = g();
        if (bm1 == null)
        {
            return true;
        }
        s.i = false;
        if (TCAgent.LOG_ON)
        {
            Log.i("TDLog", "Post data to server...");
        }
        boolean flag = ac.a(bm1);
        c = SystemClock.elapsedRealtime();
        if (TCAgent.LOG_ON)
        {
            Log.i("TDLog", (new StringBuilder()).append("server return success:").append(flag).toString());
        }
        if (flag)
        {
            a(bm1);
            s.p = false;
        } else
        {
            s.w();
            s.p = true;
        }
        return flag;
    }

    private static bm g()
    {
        Object obj = s.h();
        bm bm1 = new bm();
        bm1.a = com.tendcloud.tenddata.a.b(((Context) (obj)));
        bm1.b = s.i();
        bm1.c = s.t();
        bm1.d = s.u();
        bm1.i = s.f;
        int k = bm1.a() + 3 + 0;
        Object obj1;
        boolean flag;
        int i1;
        if (s.j())
        {
            obj = new as();
            obj.a = 1;
            obj.c = d();
            bm1.e.add(obj);
            int l = ad.c(((as) (obj)).a);
            k += ((as) (obj)).c.a() + l;
            flag = true;
        } else
        {
            flag = false;
        }
        com.tendcloud.tenddata.e.a(s.h());
        bm1.h = com.tendcloud.tenddata.e.e("error_report");
        obj1 = com.tendcloud.tenddata.e.d();
        obj = new ArrayList();
        obj1 = ((List) (obj1)).iterator();
        i1 = 0;
label0:
        do
        {
label1:
            {
                if (((Iterator) (obj1)).hasNext())
                {
                    c c1 = (c)((Iterator) (obj1)).next();
                    int j1 = i1 + 1;
                    c1.h = com.tendcloud.tenddata.e.a(c1.a, bm1.f);
                    c1.i = com.tendcloud.tenddata.e.b(c1.a, bm1.g);
                    as as1;
                    boolean flag1;
                    int k1;
                    if (c1.i != null)
                    {
                        flag1 = true;
                    } else
                    {
                        flag1 = false;
                    }
                    as1 = new as();
                    as1.a = 2;
                    as1.b = c1;
                    i1 = c1.a();
                    if (i1 + k <= 20480 || j1 == 1)
                    {
                        break label1;
                    }
                    i = true;
                }
                bm1.f = com.tendcloud.tenddata.e.a(((List) (obj)));
                bm1.g = com.tendcloud.tenddata.e.b(((List) (obj)));
                if (bm1.h > 0L)
                {
                    for (obj = com.tendcloud.tenddata.e.e(bm1.h).iterator(); ((Iterator) (obj)).hasNext(); bm1.e.add(obj1))
                    {
                        obj1 = (as)((Iterator) (obj)).next();
                    }

                }
                break label0;
            }
            k1 = k + i1;
            ((List) (obj)).add(c1);
            if (SystemClock.elapsedRealtime() - c < 0x6ddd00L && c1.c == 2 && c1.h.size() == 0)
            {
                i1 = j1;
                k = k1;
                if (c1.i.size() == 0)
                {
                    continue;
                }
                if (!flag1)
                {
                    i1 = j1;
                    k = k1;
                    if (!s.i)
                    {
                        continue;
                    }
                }
            }
            bm1.e.add(as1);
            i1 = j1;
            k = k1;
        } while (true);
        com.tendcloud.tenddata.e.b();
        if (!flag && bm1.e.size() == 0)
        {
            return null;
        } else
        {
            return bm1;
        }
    }

    static 
    {
        c = 0L;
        d = 0L;
        e = 30000;
        j = new HandlerThread("ProcessingThread");
        j.start();
    }
}
