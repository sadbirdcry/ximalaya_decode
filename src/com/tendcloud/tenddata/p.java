// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;


// Referenced classes of package com.tendcloud.tenddata:
//            m

final class p
{

    static final int a[];
    private static final int b = 0xedb88320;
    private static final int c = 1024;
    private static final int d = 1023;
    private static final int e = 0x10000;
    private static final int f = 65535;
    private final int g;
    private final int h[] = new int[1024];
    private final int i[] = new int[0x10000];
    private final int j[];
    private int k;
    private int l;
    private int m;

    p(int i1)
    {
        k = 0;
        l = 0;
        m = 0;
        j = new int[a(i1)];
        g = j.length - 1;
    }

    static int a(int i1)
    {
        i1--;
        i1 |= i1 >>> 1;
        i1 |= i1 >>> 2;
        i1 |= i1 >>> 4;
        int j1 = (i1 | i1 >>> 8) >>> 1 | 0xffff;
        i1 = j1;
        if (j1 > 0x1000000)
        {
            i1 = j1 >>> 1;
        }
        return i1 + 1;
    }

    static int b(int i1)
    {
        return (0x10400 + a(i1)) / 256 + 4;
    }

    int a()
    {
        return h[k];
    }

    void a(byte abyte0[], int i1)
    {
        int j1 = a[abyte0[i1] & 0xff] ^ abyte0[i1 + 1] & 0xff;
        k = j1 & 0x3ff;
        j1 ^= (abyte0[i1 + 2] & 0xff) << 8;
        l = 0xffff & j1;
        m = (j1 ^ a[abyte0[i1 + 3] & 0xff] << 5) & g;
    }

    int b()
    {
        return i[l];
    }

    int c()
    {
        return j[m];
    }

    void c(int i1)
    {
        h[k] = i1;
        i[l] = i1;
        j[m] = i1;
    }

    void d(int i1)
    {
        com.tendcloud.tenddata.m.a(h, i1);
        com.tendcloud.tenddata.m.a(i, i1);
        com.tendcloud.tenddata.m.a(j, i1);
    }

    static 
    {
        a = new int[256];
        for (int i1 = 0; i1 < 256; i1++)
        {
            int k1 = 0;
            int j1 = i1;
            while (k1 < 8) 
            {
                if ((j1 & 1) != 0)
                {
                    j1 = j1 >>> 1 ^ 0xedb88320;
                } else
                {
                    j1 >>>= 1;
                }
                k1++;
            }
            a[i1] = j1;
        }

    }
}
