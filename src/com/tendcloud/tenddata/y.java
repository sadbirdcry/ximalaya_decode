// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.IntentFilter;
import android.os.ParcelUuid;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.tendcloud.tenddata:
//            an, o, h

public class y
{

    private static final String a = "STATE_OFF";
    private static final String b = "STATE_ON";
    private static final String c = "STATE_TURNING_OFF";
    private static final String d = "STATE_TURNING_ON";
    private static final String e = "UNKNOWN";
    private static final String f = "CLASSIC";
    private static final String g = "LOW_ENERGY";
    private static final String h = "DUAL_MODE";
    private static final String i = "UNKNOWN";
    private static final String j = "SCAN_MODE_CONNECTABLE";
    private static final String k = "SCAN_MODE_CONNECTABLE_DISCOVERABLE";
    private static final String l = "UNPAIRED";
    private static final String m = "PAIRING";
    private static final String n = "PAIRED";
    private static JSONArray o = new JSONArray();
    private static Context p = null;
    private static Object q = null;

    public y()
    {
    }

    static Context a()
    {
        return p;
    }

    private static String a(int i1)
    {
        switch (i1)
        {
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
        case 9: // '\t'
        default:
            return "EXCEPTION_CODE";

        case 10: // '\n'
            return "UNPAIRED";

        case 12: // '\f'
            return "PAIRED";

        case 11: // '\013'
            return "PAIRING";

        case 1: // '\001'
            return "CLASSIC";

        case 3: // '\003'
            return "DUAL_MODE";

        case 2: // '\002'
            return "LOW_ENERGY";

        case 0: // '\0'
            return "UNKNOWN";
        }
    }

    static JSONObject a(Context context, BluetoothDevice bluetoothdevice)
    {
        return b(context, bluetoothdevice);
    }

    public static boolean a(Context context)
    {
        if (!an.b(context, "android.permission.BLUETOOTH"))
        {
            break MISSING_BLOCK_LABEL_27;
        }
        context = BluetoothAdapter.getDefaultAdapter();
        if (context == null)
        {
            return false;
        }
        boolean flag = context.isEnabled();
        return flag;
        context;
        return false;
    }

    private static String b(int i1)
    {
        switch (i1)
        {
        case 14: // '\016'
        case 15: // '\017'
        case 16: // '\020'
        case 17: // '\021'
        case 18: // '\022'
        case 19: // '\023'
        case 22: // '\026'
        default:
            return "EXCEPTION_CODE";

        case 21: // '\025'
            return "SCAN_MODE_CONNECTABLE";

        case 23: // '\027'
            return "SCAN_MODE_CONNECTABLE_DISCOVERABLE";

        case 20: // '\024'
            return "UNKNOWN";

        case 10: // '\n'
            return "STATE_OFF";

        case 12: // '\f'
            return "STATE_ON";

        case 13: // '\r'
            return "STATE_TURNING_OFF";

        case 11: // '\013'
            return "STATE_TURNING_ON";
        }
    }

    static JSONArray b()
    {
        return o;
    }

    private static JSONObject b(Context context, BluetoothDevice bluetoothdevice)
    {
        JSONObject jsonobject = new JSONObject();
        if (!an.b(context, "android.permission.BLUETOOTH")) goto _L2; else goto _L1
_L1:
        if (bluetoothdevice == null) goto _L4; else goto _L3
_L3:
        jsonobject.put("address", bluetoothdevice.getAddress());
        jsonobject.put("name", bluetoothdevice.getName());
        if (an.a(18))
        {
            jsonobject.put("type", a(bluetoothdevice.getType()));
        }
        jsonobject.put("bondState", a(bluetoothdevice.getBondState()));
        if (!an.a(15)) goto _L4; else goto _L5
_L5:
        context = bluetoothdevice.getUuids();
        if (context == null) goto _L4; else goto _L6
_L6:
        int i1;
        try
        {
            bluetoothdevice = new StringBuffer();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        i1 = 0;
_L10:
        if (i1 >= context.length) goto _L8; else goto _L7
_L7:
        bluetoothdevice.append(context[i1].toString());
        if (i1 != context.length - 1)
        {
            bluetoothdevice.append("|");
        }
          goto _L9
_L8:
        jsonobject.put("uuids", bluetoothdevice.toString());
        return jsonobject;
_L2:
        jsonobject.put("error", "NO_PERMISSION");
        return jsonobject;
_L9:
        i1++;
        if (true) goto _L10; else goto _L4
_L4:
        return jsonobject;
    }

    public static boolean b(Context context)
    {
        p = context;
        if (!an.b(context, "android.permission.BLUETOOTH_ADMIN")) goto _L2; else goto _L1
_L1:
        BluetoothAdapter bluetoothadapter;
        bluetoothadapter = BluetoothAdapter.getDefaultAdapter();
        o = new JSONArray();
        if (!an.a(18) || bluetoothadapter == null) goto _L4; else goto _L3
_L3:
        boolean flag = bluetoothadapter.startLeScan(new o());
_L6:
        boolean flag1;
        flag1 = flag;
        if (flag)
        {
            break MISSING_BLOCK_LABEL_83;
        }
        context.registerReceiver(new h(), new IntentFilter("android.bluetooth.device.action.FOUND"));
        flag1 = bluetoothadapter.startDiscovery();
        return flag1;
        context;
_L2:
        return false;
_L4:
        flag = false;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public static void c(Context context)
    {
        try
        {
            if (!an.b(context, "android.permission.BLUETOOTH_ADMIN"))
            {
                break MISSING_BLOCK_LABEL_62;
            }
            context = BluetoothAdapter.getDefaultAdapter();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        if (context.isDiscovering() && an.a(18) && q != null && (q instanceof android.bluetooth.BluetoothAdapter.LeScanCallback))
        {
            context.stopLeScan((android.bluetooth.BluetoothAdapter.LeScanCallback)q);
        }
        context.cancelDiscovery();
    }

    public static JSONArray d(Context context)
    {
        return o;
    }

    public static JSONObject e(Context context)
    {
        Object obj;
        JSONObject jsonobject;
        jsonobject = new JSONObject();
        obj = jsonobject;
        if (!an.b(context, "android.permission.BLUETOOTH"))
        {
            break MISSING_BLOCK_LABEL_164;
        }
        obj = jsonobject;
        BluetoothAdapter bluetoothadapter;
        String s;
        try
        {
            bluetoothadapter = BluetoothAdapter.getDefaultAdapter();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            try
            {
                ((JSONObject) (obj)).put("error", "EXCEPTION");
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                return ((JSONObject) (obj));
            }
            return ((JSONObject) (obj));
        }
        if (bluetoothadapter == null)
        {
            break MISSING_BLOCK_LABEL_151;
        }
        obj = jsonobject;
        s = bluetoothadapter.getAddress();
        obj = jsonobject;
        if (!BluetoothAdapter.checkBluetoothAddress(s))
        {
            break MISSING_BLOCK_LABEL_123;
        }
        obj = jsonobject;
        jsonobject = b(context, bluetoothadapter.getRemoteDevice(s));
        context = jsonobject;
        if (jsonobject != null)
        {
            break MISSING_BLOCK_LABEL_76;
        }
        obj = jsonobject;
        context = new JSONObject();
        obj = context;
        context.put("name", bluetoothadapter.getName());
        obj = context;
        context.put("state", b(bluetoothadapter.getState()));
        obj = context;
        context.put("scanMode", b(bluetoothadapter.getScanMode()));
        return context;
        obj = jsonobject;
        jsonobject.put("error", "DEVICE_INVALID");
        return jsonobject;
        obj = jsonobject;
        jsonobject.put("error", "NOT_SUPPORT_BLUETOOTH");
        return jsonobject;
        obj = jsonobject;
        jsonobject.put("error", "NO_PERMISSION");
        return jsonobject;
    }

    public static JSONArray f(Context context)
    {
        Object obj;
        if (!an.b(context, "android.permission.BLUETOOTH"))
        {
            break MISSING_BLOCK_LABEL_103;
        }
        obj = BluetoothAdapter.getDefaultAdapter();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_103;
        }
        Object obj1 = ((BluetoothAdapter) (obj)).getBondedDevices();
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_103;
        }
        if (((Set) (obj1)).size() <= 0)
        {
            break MISSING_BLOCK_LABEL_103;
        }
        obj = new JSONArray();
        obj1 = ((Set) (obj1)).iterator();
_L2:
        JSONObject jsonobject;
        do
        {
            if (!((Iterator) (obj1)).hasNext())
            {
                break MISSING_BLOCK_LABEL_105;
            }
            jsonobject = b(context, (BluetoothDevice)((Iterator) (obj1)).next());
            if (jsonobject.has("uuids"))
            {
                jsonobject.remove("uuids");
            }
        } while (jsonobject == null);
        ((JSONArray) (obj)).put(jsonobject);
        if (true) goto _L2; else goto _L1
_L1:
        context;
        return null;
        return ((JSONArray) (obj));
    }

}
