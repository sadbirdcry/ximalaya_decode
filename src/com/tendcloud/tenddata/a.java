// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.tendcloud.tenddata:
//            an, ba, av

public class a
{

    static TelephonyManager a;
    static String b;
    private static final String c = "pref.deviceid.key";
    private static final String d = "00:00:00:00:00:00";
    private static final Pattern e = Pattern.compile("^([0-9A-F]{2}:){5}([0-9A-F]{2})$");
    private static final Pattern f = Pattern.compile("[0-3][0-9a-f]{24,32}");
    private static final Pattern g = Pattern.compile("[0-3][0-9a-f]{32}");
    private static final String h = ".tcookieid";
    private static String i = null;

    public a()
    {
    }

    public static final String a()
    {
        String s;
        if (!an.a(9))
        {
            break MISSING_BLOCK_LABEL_15;
        }
        s = Build.SERIAL;
        return s;
        Throwable throwable;
        throwable;
        return null;
    }

    private static String a(Context context, boolean flag)
    {
        if ("mounted".equals(Environment.getExternalStorageState()))
        {
            Object obj = Environment.getExternalStorageDirectory();
            String s;
            if (flag)
            {
                s = ".tcookieid";
            } else
            {
                s = (new StringBuilder()).append(".tcookieid").append(n(context)).toString();
            }
            obj = a(new File(((File) (obj)), s));
            s = ((String) (obj));
            if (an.b(((String) (obj))))
            {
                s = a(new File(Environment.getExternalStorageDirectory(), (new StringBuilder()).append(".tid").append(n(context)).toString()));
            }
            return s;
        } else
        {
            return "";
        }
    }

    private static String a(File file)
    {
        if (!file.exists() || !file.canRead())
        {
            break MISSING_BLOCK_LABEL_53;
        }
        file = new FileInputStream(file);
        byte abyte0[] = new byte[128];
        int i1 = file.read(abyte0);
        file.close();
        file = new String(abyte0, 0, i1);
        return file;
        file;
        return null;
    }

    public static void a(Context context)
    {
        a = (TelephonyManager)context.getSystemService("phone");
    }

    private static void a(Context context, String s)
    {
        File afile[] = (new File("/")).listFiles();
        int k1 = afile.length;
        for (int i1 = 0; i1 < k1; i1++)
        {
            File file = afile[i1];
            if (!file.isDirectory() || "/sdcard".equals(file.getAbsolutePath()))
            {
                continue;
            }
            if (file.canWrite() && !(new File(file, (new StringBuilder()).append(".tcookieid").append(n(context)).toString())).exists())
            {
                a(new File(file, ".tcookieid"), s);
            }
            if (file.listFiles() == null)
            {
                continue;
            }
            File afile1[] = file.listFiles();
            int l1 = afile1.length;
            for (int j1 = 0; j1 < l1; j1++)
            {
                File file1 = afile1[j1];
                if (file1.isDirectory() && file1.canWrite() && !(new File(file1, (new StringBuilder()).append(".tcookieid").append(n(context)).toString())).exists())
                {
                    a(new File(file1, ".tcookieid"), s);
                }
            }

        }

    }

    private static void a(Context context, String s, boolean flag)
    {
        File file = Environment.getExternalStorageDirectory();
        if (flag)
        {
            context = ".tcookieid";
        } else
        {
            context = (new StringBuilder()).append(".tcookieid").append(n(context)).toString();
        }
        a(new File(file, context), s);
    }

    private static void a(File file, String s)
    {
        try
        {
            FileOutputStream fileoutputstream = new FileOutputStream(file);
            fileoutputstream.write(s.getBytes());
            fileoutputstream.close();
            if (an.a(9))
            {
                file.getClass().getMethod("setReadable", new Class[] {
                    Boolean.TYPE, Boolean.TYPE
                }).invoke(file, new Object[] {
                    Boolean.valueOf(true), Boolean.valueOf(false)
                });
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (File file)
        {
            return;
        }
        Runtime.getRuntime().exec((new StringBuilder()).append("chmod 444 ").append(file.getAbsolutePath()).toString());
        return;
    }

    private static String b()
    {
        Object obj;
        File afile[];
        int i1;
        int k1;
        obj = null;
        afile = (new File("/")).listFiles();
        k1 = afile.length;
        i1 = 0;
_L9:
        Object obj1 = obj;
        if (i1 >= k1) goto _L2; else goto _L1
_L1:
        File file;
        file = afile[i1];
        obj1 = obj;
        if (!file.isDirectory())
        {
            break MISSING_BLOCK_LABEL_165;
        }
        obj1 = obj;
        if ("/sdcard".equals(file.getAbsolutePath()))
        {
            break MISSING_BLOCK_LABEL_165;
        }
        if (!file.canWrite()) goto _L4; else goto _L3
_L3:
        obj1 = a(new File(file, ".tcookieid"));
        obj = obj1;
        if (an.b(((String) (obj1)))) goto _L4; else goto _L2
_L2:
        return ((String) (obj1));
_L4:
        File afile1[];
        int j1;
        int l1;
        obj1 = obj;
        if (file.listFiles() == null)
        {
            break MISSING_BLOCK_LABEL_165;
        }
        afile1 = file.listFiles();
        l1 = afile1.length;
        j1 = 0;
_L6:
        obj1 = obj;
        if (j1 >= l1)
        {
            break MISSING_BLOCK_LABEL_165;
        }
        obj1 = afile1[j1];
        if (!((File) (obj1)).isDirectory())
        {
            break; /* Loop/switch isn't completed */
        }
        obj = a(new File(((File) (obj1)), ".tcookieid"));
        obj1 = obj;
        if (!an.b(((String) (obj)))) goto _L2; else goto _L5
_L5:
        j1++;
          goto _L6
        if (true) goto _L2; else goto _L7
_L7:
        i1++;
        obj = obj1;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public static String b(Context context)
    {
        com/tendcloud/tenddata/a;
        JVM INSTR monitorenter ;
        if (b == null)
        {
            b = j(context);
        }
        context = b;
        com/tendcloud/tenddata/a;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    private static void b(Context context, String s)
    {
        try
        {
            context = context.getSharedPreferences("tdid", 0);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return;
        }
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_36;
        }
        context = context.edit();
        context.putString("pref.deviceid.key", s);
        context.commit();
    }

    public static String c(Context context)
    {
        try
        {
            context = android.provider.Settings.Secure.getString(context.getContentResolver(), "android_id");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    private static boolean c()
    {
        if (!an.a(9)) goto _L2; else goto _L1
_L1:
        boolean flag = ((Boolean)android/os/Environment.getMethod("isExternalStorageRemovable", null).invoke(null, null)).booleanValue();
_L4:
        Throwable throwable;
        return !flag;
        throwable;
        flag = true;
        continue; /* Loop/switch isn't completed */
_L2:
        flag = true;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static String d(Context context)
    {
        if (!an.b(context, "android.permission.READ_PHONE_STATE")) goto _L2; else goto _L1
_L1:
        if (a == null)
        {
            a(context);
        }
        context = ba.x(context);
        if (context == null) goto _L4; else goto _L3
_L3:
        int i1 = context.length();
        if (i1 != 2) goto _L4; else goto _L5
_L5:
        context = context.getJSONObject(1).getString("imei");
_L7:
        Object obj;
        obj = context;
        if (context != null)
        {
            break MISSING_BLOCK_LABEL_64;
        }
        obj = a.getDeviceId();
        return ((String) (obj));
        context;
        context = null;
        continue; /* Loop/switch isn't completed */
        context;
_L2:
        return null;
_L4:
        context = null;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public static String e(Context context)
    {
        if (!an.b(context, "android.permission.READ_PHONE_STATE"))
        {
            break MISSING_BLOCK_LABEL_30;
        }
        if (a == null)
        {
            a(context);
        }
        context = a.getSimSerialNumber();
        return context;
        context;
        return null;
    }

    public static String f(Context context)
    {
        if (!an.b(context, "android.permission.READ_PHONE_STATE"))
        {
            break MISSING_BLOCK_LABEL_30;
        }
        if (a == null)
        {
            a(context);
        }
        context = a.getSubscriberId();
        return context;
        context;
        return null;
    }

    public static String g(Context context)
    {
        if (!an.a)
        {
            return null;
        }
        if (!an.b(context, "android.permission.ACCESS_WIFI_STATE")) goto _L2; else goto _L1
_L1:
        context = (WifiManager)context.getSystemService("wifi");
        if (!context.isWifiEnabled()) goto _L2; else goto _L3
_L3:
        context = context.getConnectionInfo();
        if (context == null) goto _L2; else goto _L4
_L4:
        String s;
        boolean flag;
        try
        {
            s = context.getMacAddress();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context = null;
            continue; /* Loop/switch isn't completed */
        }
        context = s;
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_98;
        }
        context = s;
        try
        {
            s = s.toUpperCase().trim();
        }
        catch (Throwable throwable)
        {
            continue; /* Loop/switch isn't completed */
        }
        context = s;
        if ("00:00:00:00:00:00".equals(s))
        {
            break MISSING_BLOCK_LABEL_96;
        }
        context = s;
        flag = e.matcher(s).matches();
        context = s;
        if (flag)
        {
            break MISSING_BLOCK_LABEL_98;
        }
        context = null;
_L6:
        return context;
_L2:
        context = null;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public static final String h(Context context)
    {
        try
        {
            context = ((Context) (Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient").getMethod("getAdvertisingIdInfo", new Class[] {
                android/content/Context
            }).invoke(null, new Object[] {
                context
            })));
            context = (String)Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient$Info").getMethod("getId", null).invoke(context, null);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static final String i(Context context)
    {
        String s1 = g(context);
        String s = s1;
        if (!TextUtils.isEmpty(s1))
        {
            s = String.valueOf(Long.parseLong(s1.replaceAll(":", ""), 16));
        }
        s1 = c(context);
        String s2 = d(context);
        String s3 = f(context);
        String s4 = e(context);
        String s5 = b(context);
        context = h(context);
        String s6 = a();
        return (new StringBuilder()).append(2).append("|").append(s).append("|").append(s1).append("|").append(s2).append("|").append(s3).append("|").append(s4).append("|").append(s5).append("|").append(context).append("|").append(s6).toString();
    }

    private static String j(Context context)
    {
        String s2;
        String s3;
        String s4;
        String as[];
        int i1;
        boolean flag;
        int j1;
        boolean flag1;
        flag = false;
        s2 = k(context);
        s3 = b();
        flag1 = c();
        s4 = a(context, flag1);
        as = new String[3];
        as[0] = s2;
        as[1] = s3;
        as[2] = s4;
        j1 = as.length;
        i1 = 0;
_L9:
        String s;
        if (i1 >= j1)
        {
            break MISSING_BLOCK_LABEL_241;
        }
        s = as[i1];
        if (an.b(s) || !g.matcher(s).matches()) goto _L2; else goto _L1
_L1:
        String s1 = s;
        if (!an.b(s)) goto _L4; else goto _L3
_L3:
        s1 = s;
        if (an.b(s2)) goto _L4; else goto _L5
_L5:
        s1 = s;
        if (Math.random() >= 0.98999999999999999D) goto _L4; else goto _L6
_L6:
        j1 = as.length;
        i1 = ((flag) ? 1 : 0);
_L10:
        s1 = s;
        if (i1 >= j1) goto _L4; else goto _L7
_L7:
        s1 = as[i1];
        if (an.b(s1) || !f.matcher(s1).matches()) goto _L8; else goto _L4
_L4:
        s = s1;
        if (an.b(s1))
        {
            s = l(context);
        }
        if (!s.equals(s2))
        {
            b(context, s);
        }
        if (!s.equals(s4))
        {
            a(context, s, flag1);
        }
        if (!s.equals(s3))
        {
            a(context, s);
        }
        return s;
_L2:
        i1++;
          goto _L9
_L8:
        i1++;
          goto _L10
        s = null;
          goto _L1
    }

    private static String k(Context context)
    {
        String s1 = av.b(context, "tdid", "pref.deviceid.key", null);
        String s = s1;
        if (an.b(s1))
        {
            s = PreferenceManager.getDefaultSharedPreferences(context).getString("pref.deviceid.key", null);
        }
        return s;
    }

    private static String l(Context context)
    {
        context = m(context);
        return (new StringBuilder()).append("3").append(an.c(context)).toString();
    }

    private static String m(Context context)
    {
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append(d(context)).append('-').append(g(context)).append('-').append(c(context));
        return stringbuilder.toString();
    }

    private static String n(Context context)
    {
        if (i == null)
        {
            Object obj = ((SensorManager)context.getSystemService("sensor")).getSensorList(-1);
            context = new Sensor[64];
            obj = ((List) (obj)).iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                Sensor sensor = (Sensor)((Iterator) (obj)).next();
                if (sensor.getType() < context.length && sensor.getType() >= 0)
                {
                    context[sensor.getType()] = sensor;
                }
            } while (true);
            obj = new StringBuffer();
            for (int i1 = 0; i1 < context.length; i1++)
            {
                if (context[i1] != null)
                {
                    ((StringBuffer) (obj)).append(i1).append('.').append(context[i1].getVendor()).append('-').append(context[i1].getName()).append('-').append(context[i1].getVersion()).append('\n');
                }
            }

            i = String.valueOf(((StringBuffer) (obj)).toString().hashCode());
        }
        return i;
    }

}
