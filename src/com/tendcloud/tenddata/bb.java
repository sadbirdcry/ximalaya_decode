// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

// Referenced classes of package com.tendcloud.tenddata:
//            an, ba

public class bb
{

    public bb()
    {
    }

    private static long a(String s)
    {
        long l = 0x3ffffffffffe5L;
        int j = s.length();
        for (int i = 0; i < j; i++)
        {
            l = l * 131L + (long)s.charAt(i);
        }

        return l;
    }

    public static List a(Context context)
    {
        ArrayList arraylist;
        arraylist = new ArrayList();
        if (!an.a)
        {
            return arraylist;
        }
        if (!an.b(context, "android.permission.ACCESS_FINE_LOCATION") && !an.b(context, "android.permission.ACCESS_COARSE_LOCATION") || !an.c && !an.d) goto _L2; else goto _L1
_L1:
        LocationManager locationmanager;
        Iterator iterator;
        locationmanager = (LocationManager)context.getSystemService("location");
        iterator = locationmanager.getProviders(true).iterator();
_L3:
        String s;
        if (!iterator.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        s = (String)iterator.next();
        Location location = locationmanager.getLastKnownLocation(s);
        if (location == null)
        {
            break MISSING_BLOCK_LABEL_109;
        }
        arraylist.add(location);
        PendingIntent pendingintent = PendingIntent.getBroadcast(context, 0, new Intent(), 0);
        locationmanager.requestLocationUpdates(s, 0x493e0L, 0.0F, pendingintent);
        locationmanager.removeUpdates(pendingintent);
        continue; /* Loop/switch isn't completed */
        Throwable throwable;
        throwable;
        if (true) goto _L3; else goto _L2
        context;
_L2:
        return arraylist;
    }

    public static String b(Context context)
    {
        context = a(context);
        StringBuffer stringbuffer = new StringBuffer();
        Iterator iterator = context.iterator();
        while (iterator.hasNext()) 
        {
            Location location = (Location)iterator.next();
            StringBuffer stringbuffer1 = stringbuffer.append(location.getLatitude()).append(',').append(location.getLongitude()).append(',');
            if (location.hasAltitude())
            {
                context = Double.valueOf(location.getAltitude());
            } else
            {
                context = "";
            }
            stringbuffer1 = stringbuffer1.append(context).append(',').append(location.getTime()).append(',');
            if (location.hasAccuracy())
            {
                context = Float.valueOf(location.getAccuracy());
            } else
            {
                context = "";
            }
            stringbuffer1 = stringbuffer1.append(context).append(',');
            if (location.hasBearing())
            {
                context = Float.valueOf(location.getBearing());
            } else
            {
                context = "";
            }
            stringbuffer1 = stringbuffer1.append(context).append(',');
            if (location.hasSpeed())
            {
                context = Float.valueOf(location.getSpeed());
            } else
            {
                context = "";
            }
            stringbuffer1.append(context).append(',').append(location.getProvider()).append(':');
        }
        return stringbuffer.toString();
    }

    public static JSONArray c(Context context)
    {
        Object obj = a(context);
        context = new JSONArray();
        for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext();)
        {
            Location location = (Location)((Iterator) (obj)).next();
            JSONObject jsonobject = new JSONObject();
            try
            {
                jsonobject.put("lat", location.getLatitude());
                jsonobject.put("lng", location.getLongitude());
                jsonobject.put("ts", location.getTime());
                if (an.a(17))
                {
                    jsonobject.put("elapsed", location.getElapsedRealtimeNanos());
                }
                if (location.hasAltitude())
                {
                    jsonobject.put("altitude", location.getAltitude());
                }
                if (location.hasAccuracy())
                {
                    jsonobject.put("accurate", location.getAccuracy());
                }
                if (location.hasBearing())
                {
                    jsonobject.put("bearing", location.getBearing());
                }
                if (location.hasSpeed())
                {
                    jsonobject.put("speed", location.getSpeed());
                }
                jsonobject.put("provider", location.getProvider());
                context.put(jsonobject);
            }
            catch (Throwable throwable) { }
        }

        return context;
    }

    public static JSONArray d(Context context)
    {
        boolean flag = false;
        JSONArray jsonarray = new JSONArray();
        Account aaccount[] = e(context);
        if (aaccount != null)
        {
            int j = aaccount.length;
            int i = 0;
            while (i < j) 
            {
                Object obj = aaccount[i];
                JSONArray jsonarray1;
                String s;
                JSONObject jsonobject1;
                try
                {
                    JSONObject jsonobject = new JSONObject();
                    jsonobject.put("type", ((Account) (obj)).type);
                    jsonobject.put("name", ((Account) (obj)).name);
                    jsonarray.put(jsonobject);
                }
                catch (Throwable throwable) { }
                i++;
            }
        }
        jsonarray1 = ba.x(context);
        i = ((flag) ? 1 : 0);
_L1:
        if (i >= jsonarray1.length())
        {
            break MISSING_BLOCK_LABEL_267;
        }
        obj = jsonarray1.getJSONObject(i);
        s = ((JSONObject) (obj)).getString("simSerialNumber");
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_257;
        }
        jsonobject1 = new JSONObject();
        jsonobject1.put("type", "sim");
        jsonobject1.put("name", s);
        jsonobject1.put("displayName", ((JSONObject) (obj)).optString("subscriberId"));
        jsonobject1.put("extra1", ((JSONObject) (obj)).optString("simOperator"));
        jsonobject1.put("extra2", ((JSONObject) (obj)).optString("simOperatorName"));
        jsonobject1.put("extra3", ba.o(context));
        jsonobject1.put("extra4", ((JSONObject) (obj)).optString("simCountryIso"));
        jsonobject1.put("extra6", ((JSONObject) (obj)).optString("imei"));
        jsonarray.put(jsonobject1);
        i++;
          goto _L1
        context;
        if (jsonarray.length() > 0)
        {
            return jsonarray;
        } else
        {
            return null;
        }
    }

    public static Account[] e(Context context)
    {
        if (!an.b(context, "android.permission.GET_ACCOUNTS"))
        {
            break MISSING_BLOCK_LABEL_21;
        }
        context = AccountManager.get(context).getAccounts();
        return context;
        context;
        return null;
    }

    public static Long[][] f(Context context)
    {
        Long along[][];
        along = new Long[3][];
        HashSet hashset;
        Object obj;
        Object obj1;
        Object obj2;
        Object obj3;
        try
        {
            obj1 = (ActivityManager)context.getSystemService("activity");
            obj = context.getPackageManager();
            hashset = new HashSet();
            hashset.add(context.getPackageName());
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return along;
        }
        obj2 = new HashSet();
        if (!an.b(context, "android.permission.GET_TASKS")) goto _L2; else goto _L1
_L1:
        context = ((ActivityManager) (obj1)).getRecentTasks(10, 1);
        if (context == null) goto _L2; else goto _L3
_L3:
        context = context.iterator();
_L6:
        if (!context.hasNext()) goto _L2; else goto _L4
_L4:
        obj3 = ((android.app.ActivityManager.RecentTaskInfo)context.next()).baseIntent.getComponent();
        if (obj3 == null) goto _L6; else goto _L5
_L5:
        obj3 = ((ComponentName) (obj3)).getPackageName();
        if (hashset.add(obj3))
        {
            ((Set) (obj2)).add(Long.valueOf(a(((String) (obj3)))));
        }
          goto _L6
_L10:
        obj1 = ((ActivityManager) (obj1)).getRunningAppProcesses();
        context = new HashSet();
        if (obj1 == null)
        {
            break; /* Loop/switch isn't completed */
        }
        obj1 = ((List) (obj1)).iterator();
_L8:
        if (!((Iterator) (obj1)).hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        obj2 = ((android.app.ActivityManager.RunningAppProcessInfo)((Iterator) (obj1)).next()).processName;
        try
        {
            if (((PackageManager) (obj)).getLaunchIntentForPackage(((String) (obj2))) != null)
            {
                context.add(Long.valueOf(a(((String) (obj2)))));
            }
        }
        // Misplaced declaration of an exception variable
        catch (Object obj2) { }
        if (true) goto _L8; else goto _L7
_L7:
        break; /* Loop/switch isn't completed */
_L2:
        try
        {
            along[0] = new Long[((Set) (obj2)).size()];
            along[0] = (Long[])((Set) (obj2)).toArray(along[0]);
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        if (true) goto _L10; else goto _L9
_L9:
        try
        {
            along[1] = new Long[context.size()];
            along[1] = (Long[])context.toArray(along[1]);
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        try
        {
            obj = ((PackageManager) (obj)).getInstalledApplications(0);
            context = new HashSet();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return along;
        }
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_384;
        }
        obj = ((List) (obj)).iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break;
            }
            obj1 = (ApplicationInfo)((Iterator) (obj)).next();
            if ((((ApplicationInfo) (obj1)).flags & 1) <= 0 && !hashset.contains(((ApplicationInfo) (obj1)).packageName))
            {
                context.add(Long.valueOf(a(((ApplicationInfo) (obj1)).packageName)));
            }
        } while (true);
        along[2] = new Long[context.size()];
        along[2] = (Long[])context.toArray(along[2]);
        return along;
    }

    public static List g(Context context)
    {
        ArrayList arraylist = new ArrayList();
        Object obj;
        context = context.getPackageManager();
        obj = context.getInstalledApplications(0);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_131;
        }
        obj = ((List) (obj)).iterator();
_L1:
        ApplicationInfo applicationinfo;
        if (!((Iterator) (obj)).hasNext())
        {
            break MISSING_BLOCK_LABEL_131;
        }
        applicationinfo = (ApplicationInfo)((Iterator) (obj)).next();
        arraylist.add(applicationinfo.packageName);
        arraylist.add(an.b(context.getApplicationLabel(applicationinfo).toString().getBytes()));
_L2:
        if ((applicationinfo.flags & 1) <= 0)
        {
            break MISSING_BLOCK_LABEL_117;
        }
        arraylist.add("1");
          goto _L1
        Throwable throwable;
        throwable;
        arraylist.add("");
          goto _L2
        arraylist.add("0");
          goto _L1
        context;
        return arraylist;
    }
}
