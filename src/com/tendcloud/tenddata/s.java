// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Process;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import java.io.Closeable;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.UUID;
import org.json.JSONArray;

// Referenced classes of package com.tendcloud.tenddata:
//            ao, bk, av, bb, 
//            g, al, ba, ae, 
//            TCAgent, a, an, d, 
//            e, bh, v, bi, 
//            n, r, w, bj, 
//            bn, bl, aq

public final class s
    implements ao
{
    private static final class a
    {

        String a;
        String b;
        long c;
        String d;
        String e;
        long f;
        Map g;

        public a()
        {
            g = null;
            f = System.currentTimeMillis();
        }
    }

    private static class b
        implements Thread.UncaughtExceptionHandler
    {

        private Thread.UncaughtExceptionHandler a;

        public void uncaughtException(Thread thread, Throwable throwable)
        {
            if (TCAgent.d)
            {
                s.a(throwable, false);
                Log.w("TDLog", (new StringBuilder()).append("UncaughtException in Thread ").append(thread.getName()).toString(), throwable);
            }
            if (a != null)
            {
                a.uncaughtException(thread, throwable);
            }
        }

        b()
        {
            a = Thread.getDefaultUncaughtExceptionHandler();
        }
    }


    private static final String A = "TDpref.apps_send_time.key";
    private static final String B = "TDpref.ip";
    private static final String C = "TDpref.last.sdk.check";
    private static final String D = "TD_APP_ID";
    private static final String E = "TD_CHANNEL_ID";
    private static final String F = "pref_longtime";
    private static final String G = "pref_shorttime";
    private static final long H = 30000L;
    private static final int I = 100;
    private static final int J = 5000;
    private static final int K = 0;
    private static final int L = 1;
    private static final int M = 2;
    private static final int N = 3;
    private static final int O = 4;
    private static final int P = 5;
    private static volatile boolean Q = false;
    private static String R = "TalkingData";
    private static Context S;
    private static String T;
    private static long U = 0L;
    private static boolean V = false;
    private static boolean W = false;
    protected static final int a = 0x1d4c0;
    protected static final int b = 6;
    protected static final int c = 7;
    protected static final int d = 8;
    protected static final int e = 9;
    static Long f[][];
    static boolean g = false;
    static Application h;
    static volatile boolean i = false;
    static boolean j = false;
    static FileChannel l;
    static n m;
    static al n;
    static boolean o = false;
    static boolean p = false;
    static Random q = new Random();
    private static final String r = "+V1.2.74";
    private static final String s = "Android+TD+V1.2.74";
    private static final String t = "TDpref.profile.key";
    private static final String u = "TDpref.session.key";
    private static final String v = "TDpref.lastactivity.key";
    private static final String w = "TDpref.start.key";
    private static final String x = "TDpref.init.key";
    private static final String y = "TDpref.actstart.key";
    private static final String z = "TDpref.end.key";
    aq k;

    public s()
    {
        k = new bk(this);
    }

    static String A()
    {
        return T;
    }

    private static void B()
    {
        Calendar calendar = Calendar.getInstance();
        int i1 = calendar.get(6);
        int j1 = calendar.get(11);
        av.a(S, "pref_longtime", "TDpref.apps_send_time.key", j1 + i1 * 100);
    }

    private static void C()
    {
        List list = bb.a(S);
        Object obj = null;
        Iterator iterator = list.iterator();
        while (iterator.hasNext()) 
        {
            Location location = (Location)iterator.next();
            Object obj1 = location;
            int i1;
            if (obj != null)
            {
                if (location.getTime() > ((Location) (obj)).getTime())
                {
                    obj1 = location;
                } else
                {
                    obj1 = obj;
                }
            }
            obj = obj1;
        }
        obj1 = new g();
        if (obj != null)
        {
            obj1.b = ((Location) (obj)).getLatitude();
            obj1.a = ((Location) (obj)).getLongitude();
        }
        n.c = ((g) (obj1));
        obj = n;
        if (ba.g(S))
        {
            i1 = 0;
        } else
        {
            i1 = 1;
        }
        obj.k = i1;
        n.l = ba.i(S);
        n.o = ba.k(S);
        n.n = ba.l(S);
        n.p = bb.b(S);
        n.t = ba.r(S).toString();
        obj = bb.d(h());
        if (j() || q.nextInt(100) > 90)
        {
            obj1 = n;
            if (obj == null)
            {
                obj = "";
            } else
            {
                obj = ((JSONArray) (obj)).toString();
            }
            obj1.u = ((String) (obj));
        }
    }

    private static void D()
    {
        Object obj = ba.x(h());
        if (obj != null && ((JSONArray) (obj)).length() == 2)
        {
            TreeMap treemap = new TreeMap();
            treemap.put("imeis", ((JSONArray) (obj)).toString());
            ae.a(new String[] {
                (new StringBuilder()).append("saveImeisInfo ").append(treemap.toString()).toString()
            });
            TCAgent.onEvent(h(), "__tx.env", null, treemap);
        }
        obj = com.tendcloud.tenddata.a.a();
        if (obj != null)
        {
            TreeMap treemap1 = new TreeMap();
            treemap1.put("SerialNo", obj);
            TCAgent.onEvent(h(), "__tx.env", null, treemap1);
        }
    }

    private static void E()
    {
        String s1 = an.a();
        if (s1 != null)
        {
            TreeMap treemap = new TreeMap();
            treemap.put("sysproperty", s1);
            TCAgent.onEvent(h(), "__tx.env", null, treemap);
        }
    }

    private static String a(Bundle bundle, String s1)
    {
label0:
        {
            if (bundle == null)
            {
                break label0;
            }
            Iterator iterator = bundle.keySet().iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
            } while (!((String)iterator.next()).equalsIgnoreCase(s1));
            return String.valueOf(bundle.get(s1));
        }
        return "";
    }

    private static String a(Throwable throwable)
    {
        int i1 = 50;
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append(throwable.toString());
        stringbuilder.append("\r\n");
        StackTraceElement astacktraceelement[] = throwable.getStackTrace();
        int j1;
        if (astacktraceelement.length <= 50)
        {
            i1 = astacktraceelement.length;
        }
        for (j1 = 0; j1 < i1; j1++)
        {
            stringbuilder.append((new StringBuilder()).append("\t").append(astacktraceelement[j1]).append("\r\n").toString());
        }

        throwable = throwable.getCause();
        if (throwable != null)
        {
            a(stringbuilder, astacktraceelement, throwable, 1);
        }
        return stringbuilder.toString();
    }

    public static void a(int i1, long l1)
    {
        Handler handler = com.tendcloud.tenddata.d.a();
        Message message = Message.obtain(handler, i1);
        handler.removeMessages(i1);
        handler.sendMessageDelayed(message, l1);
    }

    static void a(long l1)
    {
        av.a(S, "pref_longtime", "TDpref.start.key", l1);
    }

    private static void a(long l1, String s1, String s2, boolean flag)
    {
        if (s1 != null)
        {
            if (flag)
            {
                c(s1);
                c(l1);
                U = com.tendcloud.tenddata.e.a(k(), s1, l1, 0, s2, SystemClock.elapsedRealtime());
            }
            com.tendcloud.tenddata.d.a().removeMessages(7);
        }
        a(6, 100L);
    }

    private void a(Context context, String s1, int i1, boolean flag)
    {
        an.a(new bh(this, i1, s1, flag, context));
    }

    static void a(Message message)
    {
        Object obj;
        boolean flag1;
        boolean flag2;
        obj = null;
        flag1 = false;
        flag2 = true;
        if (message.what == 8) goto _L2; else goto _L1
_L1:
        o = true;
_L25:
        com.tendcloud.tenddata.e.a(h());
        message.what;
        JVM INSTR tableswitch 4 8: default 434
    //                   4 242
    //                   5 251
    //                   6 434
    //                   7 434
    //                   8 283;
           goto _L3 _L4 _L5 _L3 _L3 _L6
_L26:
        if (!j) goto _L8; else goto _L7
_L7:
        message.what;
        JVM INSTR tableswitch 0 9: default 439
    //                   0 301
    //                   1 334
    //                   2 439
    //                   3 351
    //                   4 439
    //                   5 439
    //                   6 463
    //                   7 472
    //                   8 439
    //                   9 368;
           goto _L8 _L9 _L10 _L8 _L11 _L8 _L8 _L12 _L13 _L8 _L14
_L29:
        com.tendcloud.tenddata.e.b();
        boolean flag;
        if (!flag) goto _L16; else goto _L15
_L15:
        if (l == null) goto _L18; else goto _L17
_L17:
        FileLock filelock = l.tryLock();
        obj = filelock;
_L33:
        if (obj == null) goto _L20; else goto _L19
_L19:
        com.tendcloud.tenddata.d.c();
        o = false;
_L20:
        if (obj != null)
        {
            try
            {
                ((FileLock) (obj)).release();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
_L16:
        if (flag1) goto _L22; else goto _L21
_L21:
        if (j) goto _L23; else goto _L22
_L22:
        if (!j && message.what == 8)
        {
            w();
        }
        a(8, 0x1d4c0 + (q.nextInt(60000) - 30000));
        return;
_L2:
        if (o || p) goto _L25; else goto _L24
_L24:
        a(8, 0x1d4c0L);
        return;
_L4:
        a a1;
        Throwable throwable;
        try
        {
            d(message);
        }
        // Misplaced declaration of an exception variable
        catch (Message message)
        {
            return;
        }
        flag = false;
          goto _L26
_L5:
        a1 = (a)message.obj;
        com.tendcloud.tenddata.e.a(a1.c, a1.d);
        d(a1.c);
        flag = false;
          goto _L26
_L6:
        if (g(h()))
        {
            d(System.currentTimeMillis());
        }
          goto _L27
_L9:
        if (android.os.Build.VERSION.SDK_INT >= 14 || !g(S)) goto _L8; else goto _L28
_L28:
        b(((Message) (null)));
        flag2 = true;
        flag1 = flag;
        flag = flag2;
          goto _L29
_L10:
        b(message);
        flag2 = flag;
        flag = flag1;
        flag1 = flag2;
          goto _L29
_L11:
        c(message);
        flag2 = flag;
        flag = flag1;
        flag1 = flag2;
          goto _L29
_L14:
        flag2 = flag;
        flag = flag1;
        E();
        flag1 = flag2;
          goto _L29
        obj;
        obj = null;
_L32:
        if (obj != null)
        {
            try
            {
                ((FileLock) (obj)).release();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
          goto _L16
        message;
_L31:
        if (obj != null)
        {
            try
            {
                ((FileLock) (obj)).release();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        throw message;
        message;
        if (true) goto _L31; else goto _L30
_L30:
        throwable;
          goto _L32
_L18:
        obj = null;
          goto _L33
_L3:
        flag = false;
          goto _L26
_L8:
        flag2 = flag;
        flag = flag1;
        flag1 = flag2;
          goto _L29
_L23:
        return;
_L27:
        flag = true;
        flag1 = true;
          goto _L26
_L12:
        flag = true;
        flag1 = flag2;
          goto _L29
_L13:
        boolean flag3 = true;
        flag1 = flag;
        flag = flag3;
          goto _L29
    }

    static void a(s s1, Context context, String s2, int i1, boolean flag)
    {
        s1.a(context, s2, i1, flag);
    }

    static void a(Closeable closeable)
    {
        if (closeable == null)
        {
            break MISSING_BLOCK_LABEL_10;
        }
        closeable.close();
        return;
        closeable;
    }

    private static void a(StringBuilder stringbuilder, StackTraceElement astacktraceelement[], Throwable throwable, int i1)
    {
        byte byte0 = 50;
        StackTraceElement astacktraceelement1[] = throwable.getStackTrace();
        int j1 = astacktraceelement1.length;
        int k1 = astacktraceelement.length;
        j1--;
        for (k1--; j1 >= 0 && k1 >= 0 && astacktraceelement1[j1].equals(astacktraceelement[k1]); j1--)
        {
            k1--;
        }

        if (j1 > 50)
        {
            j1 = byte0;
        }
        stringbuilder.append((new StringBuilder()).append("Caused by : ").append(throwable).append("\r\n").toString());
        for (int l1 = 0; l1 <= j1; l1++)
        {
            stringbuilder.append((new StringBuilder()).append("\t").append(astacktraceelement1[l1]).append("\r\n").toString());
        }

        while (i1 >= 5 || throwable.getCause() == null) 
        {
            return;
        }
        a(stringbuilder, astacktraceelement1, throwable, i1 + 1);
    }

    static void a(Throwable throwable, boolean flag)
    {
        b(throwable, flag);
    }

    public static void a(boolean flag)
    {
        g = flag;
    }

    static void b(long l1)
    {
        av.a(S, "pref_longtime", "TDpref.init.key", l1);
    }

    private static void b(Message message)
    {
        boolean flag1 = false;
        boolean flag = flag1;
        if (message != null)
        {
            flag = flag1;
            if (message.arg1 == 1)
            {
                flag = true;
            }
        }
        long l3 = System.currentTimeMillis();
        long l1 = o();
        long l2 = r();
        if (l2 > l1)
        {
            l1 = l2;
        }
        if (message == null)
        {
            message = null;
        } else
        {
            message = (String)message.obj;
        }
        if (l3 - l1 > 30000L)
        {
            e(l3);
            a(l3, message, "", flag);
            return;
        } else
        {
            a(l3, message, l(), flag);
            return;
        }
    }

    static void b(String s1)
    {
        av.a(S, "pref_longtime", "TDpref.session.key", s1);
    }

    private static void b(Throwable throwable, boolean flag)
    {
        a a1;
        StackTraceElement astacktraceelement[];
        StringBuilder stringbuilder;
        int i1;
        int j1;
        j1 = 0;
        if (!Q)
        {
            return;
        }
        a1 = new a();
        a1.c = System.currentTimeMillis();
        a1.d = a(throwable);
        for (; throwable.getCause() != null; throwable = throwable.getCause()) { }
        astacktraceelement = throwable.getStackTrace();
        stringbuilder = new StringBuilder();
        stringbuilder.append(throwable.getClass().getName()).append(":");
        throwable = h().getPackageName();
        i1 = 0;
_L2:
        String s1;
        int k1;
        if (j1 >= 3 || i1 >= astacktraceelement.length)
        {
            break MISSING_BLOCK_LABEL_254;
        }
        s1 = astacktraceelement[i1].getClassName();
        if (!s1.startsWith("java.") || throwable.startsWith("java."))
        {
            break; /* Loop/switch isn't completed */
        }
        k1 = j1;
_L5:
        i1++;
        j1 = k1;
        if (true) goto _L2; else goto _L1
_L1:
        if (!s1.startsWith("javax.")) goto _L4; else goto _L3
_L3:
        k1 = j1;
        if (!throwable.startsWith("javax.")) goto _L5; else goto _L4
_L4:
        if (!s1.startsWith("android.")) goto _L7; else goto _L6
_L6:
        k1 = j1;
        if (!throwable.startsWith("android.")) goto _L5; else goto _L7
_L7:
        if (!s1.startsWith("com.android.")) goto _L9; else goto _L8
_L8:
        k1 = j1;
        if (!throwable.startsWith("com.android.")) goto _L5; else goto _L9
_L9:
        stringbuilder.append(astacktraceelement[i1].toString()).append(":");
        k1 = j1 + 1;
          goto _L5
        a1.e = an.c(stringbuilder.toString());
        if (flag)
        {
            throwable = Message.obtain(com.tendcloud.tenddata.d.a(), 5, a1);
            com.tendcloud.tenddata.d.a().sendMessage(throwable);
            return;
        } else
        {
            com.tendcloud.tenddata.e.a(h());
            com.tendcloud.tenddata.e.a(a1.c, a1.d);
            d(a1.c);
            com.tendcloud.tenddata.e.b();
            return;
        }
    }

    static void b(boolean flag)
    {
        Context context = S;
        long l1;
        if (flag)
        {
            l1 = 1L;
        } else
        {
            l1 = 0L;
        }
        av.a(context, "pref_longtime", "TDpref.profile.key", l1);
    }

    static void c(long l1)
    {
        av.a(S, "pref_shorttime", "TDpref.actstart.key", l1);
    }

    private static void c(Message message)
    {
        boolean flag = true;
        long l1;
        if (message.arg1 != 1)
        {
            flag = false;
        }
        l1 = System.currentTimeMillis();
        if (U != -1L && flag)
        {
            com.tendcloud.tenddata.e.a(U, SystemClock.elapsedRealtime());
        }
        d(l1);
        com.tendcloud.tenddata.d.a().removeMessages(8);
        a(7, 5000L);
    }

    static void c(String s1)
    {
        av.a(S, "pref_shorttime", "TDpref.lastactivity.key", s1);
    }

    static boolean c(Context context)
    {
        return f(context);
    }

    static void d(long l1)
    {
        av.a(S, "pref_shorttime", "TDpref.end.key", l1);
    }

    private void d(Context context)
    {
        if (!n())
        {
            LocationManager locationmanager = (LocationManager)context.getApplicationContext().getSystemService("location");
            context = (WifiManager)context.getApplicationContext().getSystemService("wifi");
            try
            {
                an.a(locationmanager, k, "mService", "android.location.ILocationManager");
                an.a(context, k, "mService", "android.net.wifi.IWifiManager");
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                Log.e("TDLog", context.getMessage());
            }
            return;
        } else
        {
            ae.a(new String[] {
                "registerListenLocationMethodCalled : location method already called"
            });
            return;
        }
    }

    private static void d(Message message)
    {
        if (TextUtils.isEmpty(k()))
        {
            return;
        } else
        {
            message = (a)message.obj;
            com.tendcloud.tenddata.e.a(k(), ((a) (message)).a, ((a) (message)).b, ((a) (message)).f, ((a) (message)).g);
            return;
        }
    }

    static void d(String s1)
    {
        av.a(S, "pref_longtime", "TDpref.ip", s1);
    }

    private static void e(long l1)
    {
        long l2 = 0L;
        e(k());
        String s1 = UUID.randomUUID().toString();
        long l3 = r();
        int i1;
        if (0L != l3)
        {
            l2 = l1 - l3;
        }
        if (S != null && ba.c(S))
        {
            i1 = 1;
        } else
        {
            i1 = -1;
        }
        b(s1);
        a(l1);
        com.tendcloud.tenddata.e.a(s1, l1, l2, i1);
    }

    private void e(Context context)
    {
        if (!an.a(14))
        {
            break MISSING_BLOCK_LABEL_114;
        }
        if (!(context instanceof Activity)) goto _L2; else goto _L1
_L1:
        h = ((Activity)context).getApplication();
_L4:
        if (h != null && !V)
        {
            context = Class.forName("android.app.Application$ActivityLifecycleCallbacks");
            context = h.getClass().getMethod("registerActivityLifecycleCallbacks", new Class[] {
                context
            });
            v v1 = new v(this);
            context.invoke(h, new Object[] {
                v1
            });
            V = true;
            return;
        }
        break MISSING_BLOCK_LABEL_181;
_L2:
        if (context instanceof Application)
        {
            h = (Application)context;
        }
        if (true) goto _L4; else goto _L3
_L3:
        context;
        return;
        if (!V)
        {
            context = new bi(this);
            try
            {
                an.a(Class.forName("android.app.ActivityManagerNative"), context, "gDefault", "android.app.IActivityManager");
                V = true;
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                Log.e("TDLog", (new StringBuilder()).append("registerActivityLifecycleListener ").append(context.getMessage()).toString());
            }
        }
    }

    private static void e(String s1)
    {
        long l1 = o();
        long l2 = r();
        if (!TextUtils.isEmpty(s1))
        {
            l2 -= l1;
            l1 = l2;
            if (l2 < 500L)
            {
                l1 = -1000L;
            }
            com.tendcloud.tenddata.e.a(s1, (int)l1 / 1000);
        }
    }

    static void f()
    {
        B();
        f = (Long[][])null;
    }

    private static boolean f(Context context)
    {
        Object obj;
        obj = (ActivityManager)context.getSystemService("activity");
        context = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).processName;
        obj = ((ActivityManager) (obj)).getRunningAppProcesses();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_88;
        }
        obj = ((List) (obj)).iterator();
        boolean flag;
        do
        {
            android.app.ActivityManager.RunningAppProcessInfo runningappprocessinfo;
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break MISSING_BLOCK_LABEL_88;
                }
                runningappprocessinfo = (android.app.ActivityManager.RunningAppProcessInfo)((Iterator) (obj)).next();
            } while (Process.myPid() != runningappprocessinfo.pid);
            flag = runningappprocessinfo.processName.equals(context);
        } while (!flag);
        return true;
        context;
        return false;
    }

    public static int g()
    {
        long l1 = av.b(S, "pref_longtime", "TDpref.apps_send_time.key", 0L);
        Calendar calendar = Calendar.getInstance();
        int i1 = calendar.get(6);
        i1 = calendar.get(11) + i1 * 100;
        if (Math.abs(l1 / 100L - (long)(i1 / 100)) >= 1L)
        {
            return 2;
        }
        return l1 == (long)i1 ? 0 : 1;
    }

    private static boolean g(Context context)
    {
        if (an.b(context, "android.permission.GET_TASKS"))
        {
            boolean flag;
            try
            {
                flag = ((android.app.ActivityManager.RunningTaskInfo)((ActivityManager)context.getSystemService("activity")).getRunningTasks(1).get(0)).baseActivity.getPackageName().equals(context.getPackageName());
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                return false;
            }
            return flag;
        } else
        {
            return true;
        }
    }

    protected static Context h()
    {
        return S;
    }

    public static String i()
    {
        return T;
    }

    static boolean j()
    {
        return av.b(S, "pref_longtime", "TDpref.profile.key", 1L) != 0L;
    }

    static String k()
    {
        return av.b(S, "pref_longtime", "TDpref.session.key", null);
    }

    static String l()
    {
        return av.b(S, "pref_shorttime", "TDpref.lastactivity.key", "");
    }

    static void m()
    {
        S.getSharedPreferences("TD_CHANNEL_ID", 0).edit().putBoolean("location_called", true).commit();
    }

    static boolean n()
    {
        return S.getSharedPreferences("TD_CHANNEL_ID", 0).getBoolean("location_called", false);
    }

    static long o()
    {
        return av.b(S, "pref_longtime", "TDpref.start.key", 0L);
    }

    static long p()
    {
        return av.b(S, "pref_longtime", "TDpref.init.key", 0L);
    }

    static long q()
    {
        return av.b(S, "pref_shorttime", "TDpref.actstart.key", 0L);
    }

    static long r()
    {
        return av.b(S, "pref_shorttime", "TDpref.end.key", 0L);
    }

    static String s()
    {
        return av.b(S, "pref_longtime", "TDpref.ip", null);
    }

    static n t()
    {
        if (m != null)
        {
            return m;
        }
        if (S == null)
        {
            return null;
        } else
        {
            m = new n();
            m.a = S.getPackageName();
            m.b = com.tendcloud.tenddata.r.d(S);
            m.c = String.valueOf(com.tendcloud.tenddata.r.c(S));
            m.d = p();
            m.e = "Android+TD+V1.2.74";
            m.f = R;
            m.h = com.tendcloud.tenddata.r.e(S);
            m.i = com.tendcloud.tenddata.r.f(S);
            return m;
        }
    }

    static al u()
    {
        if (n == null)
        {
            if (S == null)
            {
                return null;
            }
            n = new al();
            n.s = com.tendcloud.tenddata.a.i(S);
            n.a = com.tendcloud.tenddata.w.f();
            n.b = String.valueOf(com.tendcloud.tenddata.w.g());
            n.d = Build.CPU_ABI;
            n.e = com.tendcloud.tenddata.w.a(S);
            n.f = com.tendcloud.tenddata.w.j();
            n.g = ba.p(S);
            n.h = com.tendcloud.tenddata.w.i();
            n.i = TimeZone.getDefault().getRawOffset() / 1000 / 60 / 60;
            n.j = (new StringBuilder()).append("Android+").append(android.os.Build.VERSION.RELEASE).toString();
        }
        C();
        return n;
    }

    public static void v()
    {
        Thread.setDefaultUncaughtExceptionHandler(new b());
    }

    public static void w()
    {
        int j1 = Calendar.getInstance().get(11);
        int i1 = 950;
        char c1 = i1;
        if (j1 >= 1)
        {
            c1 = i1;
            if (j1 <= 6)
            {
                c1 = '\310';
            }
        }
        i1 = q.nextInt(1000);
        if (i1 > c1)
        {
            return;
        }
        TreeMap treemap = new TreeMap();
        treemap.put("loc", bb.b(h()));
        treemap.put("net", ba.r(h()).toString());
        if (i1 % 2 == 0)
        {
            Long along[][] = bb.f(h());
            treemap.put("ruas", Arrays.toString(along[0]));
            treemap.put("ras", Arrays.toString(along[1]));
        }
        treemap.put("arp", ba.y(h()));
        TCAgent.onEvent(h(), "__tx.env", null, treemap);
    }

    static void x()
    {
        D();
    }

    static Context y()
    {
        return S;
    }

    static boolean z()
    {
        return Q;
    }

    public String a()
    {
        return "+V1.2.74";
    }

    public void a(Activity activity)
    {
        a(activity, activity.getLocalClassName(), true);
        if (!V)
        {
            e(activity);
        }
    }

    public void a(Activity activity, String s1, String s2)
    {
        if (!TextUtils.isEmpty(s1)) goto _L2; else goto _L1
_L1:
        if (TCAgent.LOG_ON)
        {
            Log.e("TDLog", "onResume# APP ID not allow empty. Please check it.");
        }
_L4:
        return;
_L2:
        if (Q)
        {
            break; /* Loop/switch isn't completed */
        }
        a(((Context) (activity)), s1, s2);
        if (Q)
        {
            break; /* Loop/switch isn't completed */
        }
        if (TCAgent.LOG_ON)
        {
            Log.e("TDLog", "onResume# SDK initialize failed.");
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        a(activity, activity.getLocalClassName(), true);
        if (!V)
        {
            e(activity);
            return;
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    void a(Activity activity, String s1, boolean flag)
    {
        if (W)
        {
            return;
        } else
        {
            W = false;
            a(((Context) (activity)), s1, 1, flag);
            return;
        }
    }

    public void a(Context context)
    {
        this;
        JVM INSTR monitorenter ;
        if (Q)
        {
            break MISSING_BLOCK_LABEL_125;
        }
        S = context.getApplicationContext();
        String s2;
        String s3;
        Bundle bundle = S.getPackageManager().getApplicationInfo(S.getPackageName(), 128).metaData;
        s3 = a(bundle, "TD_APP_ID");
        s2 = a(bundle, "TD_CHANNEL_ID");
        if (!TextUtils.isEmpty(s3)) goto _L2; else goto _L1
_L1:
        if (TCAgent.LOG_ON)
        {
            Log.e("TDLog", "TD_APP_ID not found in AndroidManifest.xml!");
        }
_L3:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        String s1;
        s1 = s2;
        if (s2 == null)
        {
            s1 = "TalkingData";
        }
        a(context, s3, s1);
          goto _L3
        context;
        if (!TCAgent.LOG_ON) goto _L3; else goto _L4
_L4:
        Log.e("TDLog", "init# Failed to load meta-data", context);
          goto _L3
        context;
        throw context;
        context = com.tendcloud.tenddata.d.a();
        context.sendMessage(Message.obtain(context, 0));
          goto _L3
    }

    public void a(Context context, String s1)
    {
        if (context != null)
        {
            S = context;
        }
    }

    public void a(Context context, String s1, String s2)
    {
        this;
        JVM INSTR monitorenter ;
        if (Q) goto _L2; else goto _L1
_L1:
        S = context.getApplicationContext();
        if (an.b(context, "android.permission.INTERNET")) goto _L4; else goto _L3
_L3:
        if (TCAgent.LOG_ON)
        {
            Log.e("TDLog", "init# stop working...application do not have permission to send data, you must add <uses-permission android:name=\"android.permission.INTERNET\"/> to your AndroidManifest.xml.");
        }
_L5:
        Q = true;
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L4:
        String s4;
        T = s1;
        s4 = an.a(S, "tdconfig.json");
        if (TCAgent.LOG_ON)
        {
            ae.a(new String[] {
                "TDLog", (new StringBuilder()).append("Channel id from assets json is: ").append(s4).toString()
            });
        }
        String s3;
        s3 = s2;
        if (s4 == null)
        {
            break MISSING_BLOCK_LABEL_128;
        }
        if (s4.equals(""))
        {
            s3 = s2;
        } else
        {
            s3 = s4;
        }
        R = an.a(s3);
        if (TCAgent.LOG_ON)
        {
            Log.i("TDLog", (new StringBuilder()).append("TalkingData App Analytics ").append(TCAgent.platform).append(" SDK Init:\n").append("SDK version is").append("+V1.2.74").append("\n").append("App ID is :").append(s1).append("\n").append("Channel is :").append(R).toString());
        }
        e(context);
        d(context);
        an.a(new bj(this, context));
          goto _L5
        context;
        throw context;
    }

    public void a(Context context, String s1, String s2, Map map)
    {
        context = new a();
        context.a = an.a(s1);
        if (s2 != null)
        {
            s2 = an.a(s2);
        }
        context.b = s2;
        context.g = map;
        an.a(new bn(this, context));
    }

    public void a(Context context, Throwable throwable)
    {
        if (throwable == null)
        {
            return;
        } else
        {
            an.a(new bl(this, throwable));
            return;
        }
    }

    public void a(String s1)
    {
        av.a(S, "pref_longtime", "TDpref.last.sdk.check", System.currentTimeMillis());
    }

    public String b(Context context)
    {
        this;
        JVM INSTR monitorenter ;
        context = com.tendcloud.tenddata.a.b(context);
        this;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public void b(Activity activity)
    {
        b(activity, activity.getLocalClassName(), true);
        if (!V)
        {
            e(activity);
        }
    }

    void b(Activity activity, String s1, boolean flag)
    {
        if ((activity.getChangingConfigurations() & 0x80) == 128)
        {
            W = true;
            return;
        } else
        {
            a(activity, s1, 3, flag);
            return;
        }
    }

    public void b(Context context, String s1)
    {
        a(context, s1, 1, true);
    }

    public boolean b()
    {
label0:
        {
            long l1 = av.b(S, "pref_longtime", "TDpref.last.sdk.check", System.currentTimeMillis());
            l1 = Math.abs((System.currentTimeMillis() - l1) / 0x5265c00L);
            if (l1 <= 7L)
            {
                float f1 = q.nextFloat();
                if ((float)l1 * f1 <= 2.0F)
                {
                    break label0;
                }
            }
            return true;
        }
        return false;
    }

    public String c()
    {
        return "https://u.talkingdata.net/ota/a/TD/android/ver";
    }

    public void c(Context context, String s1)
    {
        a(context, s1, 3, true);
    }

    public void c(boolean flag)
    {
        TCAgent.d = flag;
    }

    public String d()
    {
        return "https://u.talkingdata.net/ota/a/TD/android/sdk.zip";
    }

    public void e()
    {
    }

    static 
    {
        Q = false;
        V = false;
        W = false;
        i = false;
        j = true;
        v();
        o = false;
        p = false;
    }
}
