// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.content.Context;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.zip.CRC32;

public class aa
{

    private static final long j = 0x300000L;
    private static final String k = "td-cache";
    Context a;
    File b;
    RandomAccessFile c;
    String d;
    CRC32 e;
    Lock f;
    long g;
    long h;
    long i;

    public aa(Context context, String s)
    {
        e = new CRC32();
        f = new ReentrantLock();
        g = 0L;
        h = 0L;
        i = -1L;
        a = context;
        d = s;
        b = context.getDir("td-cache", 0);
        e();
        try
        {
            f();
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        if (c.length() > 0x300000L)
        {
            d();
        }
        return;
        context;
    }

    private boolean a(long l)
    {
        f.lock();
        int i1;
        c.seek(l);
        i1 = c.readByte();
        if (i1 != 31)
        {
            break MISSING_BLOCK_LABEL_165;
        }
        int j1;
        int k1;
        j1 = c.readInt();
        k1 = c.readShort();
        if (k1 < 0)
        {
            break MISSING_BLOCK_LABEL_246;
        }
        if (c.getFilePointer() + (long)k1 > c.length())
        {
            break MISSING_BLOCK_LABEL_246;
        }
        e.reset();
        i1 = 0;
_L2:
        if (i1 >= k1)
        {
            break; /* Loop/switch isn't completed */
        }
        e.update(c.read());
        i1++;
        if (true) goto _L2; else goto _L1
_L1:
        if (c.readByte() != 31 || j1 != (int)e.getValue())
        {
            break MISSING_BLOCK_LABEL_246;
        }
        h = c.getFilePointer();
        f.unlock();
        return true;
        if (i1 != 46)
        {
            break MISSING_BLOCK_LABEL_246;
        }
        i1 = c.readInt();
        j1 = c.readByte();
        if (i1 < 0)
        {
            break MISSING_BLOCK_LABEL_246;
        }
        if ((long)i1 >= c.length() || j1 != 46)
        {
            break MISSING_BLOCK_LABEL_246;
        }
        h = c.getFilePointer();
        g = i1;
        f.unlock();
        return false;
        Object obj;
        obj;
        h = 1L + l;
        f.unlock();
        return false;
        obj;
        f.unlock();
        throw obj;
    }

    private byte[] a(long l, boolean flag)
    {
        f.lock();
        int i1;
        c.seek(l);
        i1 = c.readByte();
        if (i1 != 31)
        {
            break MISSING_BLOCK_LABEL_157;
        }
        int j1;
        i1 = c.readInt();
        j1 = c.readShort();
        if (j1 < 0)
        {
            break MISSING_BLOCK_LABEL_243;
        }
        byte abyte0[];
        if (c.getFilePointer() + (long)j1 > c.length())
        {
            break MISSING_BLOCK_LABEL_243;
        }
        abyte0 = new byte[j1];
        c.readFully(abyte0);
        if (c.readByte() != 31)
        {
            break MISSING_BLOCK_LABEL_243;
        }
        e.reset();
        e.update(abyte0);
        if (i1 != (int)e.getValue())
        {
            break MISSING_BLOCK_LABEL_243;
        }
        h = c.getFilePointer();
        f.unlock();
        return abyte0;
        if (i1 != 46)
        {
            break MISSING_BLOCK_LABEL_243;
        }
        i1 = c.readInt();
        j1 = c.readByte();
        if (i1 < 0)
        {
            break MISSING_BLOCK_LABEL_243;
        }
        if ((long)i1 >= c.length() || j1 != 46)
        {
            break MISSING_BLOCK_LABEL_243;
        }
        h = c.getFilePointer();
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_230;
        }
        g = i1;
        f.unlock();
        return null;
        Object obj;
        obj;
        h = 1L + l;
        f.unlock();
        return null;
        obj;
        f.unlock();
        throw obj;
    }

    private void b(long l)
    {
        f.lock();
        c.seek(c.length());
        c.writeByte(46);
        c.writeInt((int)l);
        c.writeByte(46);
        f.unlock();
        return;
        Exception exception;
        exception;
        f.unlock();
        throw exception;
    }

    private void b(byte abyte0[])
    {
        f.lock();
        c.seek(c.length());
        c.writeByte(31);
        e.reset();
        e.update(abyte0);
        c.writeInt((int)e.getValue());
        c.writeShort(abyte0.length);
        c.write(abyte0);
        c.writeByte(31);
        f.unlock();
        return;
        abyte0;
        f.unlock();
        throw abyte0;
    }

    private void d()
    {
        Object obj;
        Object obj1;
        byte abyte0[];
        long l;
        if (g < i)
        {
            l = i;
        } else
        {
            l = g;
        }
        h = l;
        obj = new File(b, (new StringBuilder()).append(d).append(".tmp").toString());
        obj1 = new FileOutputStream(((File) (obj)));
        do
        {
            if (h >= c.length())
            {
                break MISSING_BLOCK_LABEL_131;
            }
            abyte0 = a(h, false);
        } while (abyte0 == null);
        ((FileOutputStream) (obj1)).write(abyte0);
        break MISSING_BLOCK_LABEL_67;
        obj;
        ((FileOutputStream) (obj1)).flush();
        ((FileOutputStream) (obj1)).close();
        c.close();
        throw obj;
        ((FileOutputStream) (obj1)).flush();
        ((FileOutputStream) (obj1)).close();
        c.close();
        obj1 = new File(b, d);
        ((File) (obj1)).delete();
        ((File) (obj)).renameTo(((File) (obj1)));
        e();
        g = 0L;
        h = 0L;
        return;
    }

    private void e()
    {
        c = new RandomAccessFile(new File(b, d), "rw");
    }

    private void f()
    {
        boolean flag = false;
        do
        {
            if (h >= c.length())
            {
                break;
            }
            if (i == -1L && c.length() - h < 0x300000L)
            {
                i = h;
            }
            long l = h;
            if (a(l) && !flag)
            {
                boolean flag1 = true;
                flag = flag1;
                if (g == 0L)
                {
                    g = l;
                    flag = flag1;
                }
            }
        } while (true);
    }

    public List a(int l)
    {
        LinkedList linkedlist = new LinkedList();
        h = g;
        c.seek(h);
_L2:
        byte abyte0[];
        if (h >= c.length())
        {
            break; /* Loop/switch isn't completed */
        }
        abyte0 = a(h, false);
        if (abyte0 == null)
        {
            break MISSING_BLOCK_LABEL_64;
        }
        linkedlist.add(abyte0);
        int i1 = linkedlist.size();
        if (i1 < l) goto _L2; else goto _L1
_L1:
        if (linkedlist.size() == 0)
        {
            g = h;
        }
        return linkedlist;
        IOException ioexception;
        ioexception;
        if (true) goto _L1; else goto _L3
_L3:
    }

    public void a()
    {
        b(h);
        g = h;
    }

    public void a(byte abyte0[])
    {
        b(abyte0);
    }

    public void b()
    {
        c.getFD().sync();
    }

    public void c()
    {
        b();
        c.close();
    }
}
