// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.os.Bundle;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Proxy;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.InflaterInputStream;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import org.json.JSONObject;

// Referenced classes of package com.tendcloud.tenddata:
//            bf, bc, aq

public class an
{

    public static boolean a = false;
    static final boolean b = false;
    public static boolean c = false;
    public static boolean d = false;
    static final boolean e;
    private static final String f = "UTF-8";
    private static String g = "ge";
    private static String h = "tp";
    private static String i = "rop";
    private static final ExecutorService j = Executors.newSingleThreadExecutor();
    private static final byte k = 61;
    private static final String l = "US-ASCII";
    private static final byte m[] = {
        65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 
        75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 
        85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 
        101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 
        111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 
        121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 
        56, 57, 43, 47
    };
    private static byte n[] = {
        1, 2, 3, 4, 5, 6, 7, 8
    };

    public an()
    {
    }

    public static String a()
    {
        Object obj;
        StringBuilder stringbuilder;
        obj = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec((new StringBuilder()).append(g).append(h).append(i).toString()).getInputStream(), "UTF-8"));
        stringbuilder = new StringBuilder();
_L1:
        String s = ((BufferedReader) (obj)).readLine();
label0:
        {
            if (s == null)
            {
                break label0;
            }
            try
            {
                stringbuilder.append(s).append("\n");
            }
            catch (Throwable throwable)
            {
                return null;
            }
        }
          goto _L1
        ((BufferedReader) (obj)).close();
        obj = stringbuilder.toString();
        return ((String) (obj));
    }

    public static String a(Context context, String s)
    {
        try
        {
            context = context.getAssets().open(s);
            s = new byte[context.available()];
            context.read(s);
            context.close();
            context = (new JSONObject(new String(s))).getString("td_channel_id");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    private static String a(Bundle bundle, String s)
    {
        if (bundle == null)
        {
            return null;
        }
        for (Iterator iterator = bundle.keySet().iterator(); iterator.hasNext();)
        {
            if (((String)iterator.next()).equalsIgnoreCase(s))
            {
                return String.valueOf(bundle.get(s));
            }
        }

        return null;
    }

    public static final String a(String s)
    {
        String s1 = s;
        if (s.length() > 256)
        {
            s1 = s.substring(0, 256);
        }
        return s1;
    }

    public static String a(byte abyte0[])
    {
        StringBuilder stringbuilder = new StringBuilder();
        int j1 = abyte0.length;
        for (int i1 = 0; i1 < j1; i1++)
        {
            int k1 = abyte0[i1] & 0xff;
            if (k1 < 16)
            {
                stringbuilder.append('0');
            }
            stringbuilder.append(Integer.toHexString(k1));
        }

        return stringbuilder.toString();
    }

    public static String a(byte abyte0[], int i1, int j1)
    {
        abyte0 = b(abyte0, i1, j1);
        String s;
        try
        {
            s = new String(abyte0, "US-ASCII");
        }
        catch (UnsupportedEncodingException unsupportedencodingexception)
        {
            return new String(abyte0);
        }
        return s;
    }

    public static void a(Class class1, aq aq, String s, String s1)
    {
        s = class1.getDeclaredField(s);
        s.setAccessible(true);
        Object obj = s.get(null);
        s1 = Class.forName(s1);
        aq = new bf(aq, obj);
        s.set(null, Proxy.newProxyInstance(class1.getClass().getClassLoader(), new Class[] {
            s1
        }, aq));
    }

    public static void a(Object obj, aq aq, String s, String s1)
    {
        s = obj.getClass().getDeclaredField(s);
        s.setAccessible(true);
        Object obj1 = s.get(obj);
        s1 = Class.forName(s1);
        aq = new bc(aq, obj1);
        s.set(obj, Proxy.newProxyInstance(obj.getClass().getClassLoader(), new Class[] {
            s1
        }, aq));
    }

    public static void a(Runnable runnable)
    {
        j.execute(runnable);
    }

    public static boolean a(int i1)
    {
        return android.os.Build.VERSION.SDK_INT >= i1;
    }

    public static boolean a(Context context)
    {
        if (b(context, "android.permission.GET_TASKS"))
        {
            ActivityManager activitymanager = c(context);
            if (b(context).contains(((android.app.ActivityManager.RunningTaskInfo)activitymanager.getRunningTasks(1).get(0)).baseActivity.getPackageName()))
            {
                return true;
            }
        }
        return false;
    }

    private static byte[] a(byte abyte0[], int i1, int j1, byte abyte1[], int k1)
    {
        int j2 = 0;
        byte abyte2[] = m;
        int l1;
        int i2;
        if (j1 > 0)
        {
            l1 = (abyte0[i1] << 24) >>> 8;
        } else
        {
            l1 = 0;
        }
        if (j1 > 1)
        {
            i2 = (abyte0[i1 + 1] << 24) >>> 16;
        } else
        {
            i2 = 0;
        }
        if (j1 > 2)
        {
            j2 = (abyte0[i1 + 2] << 24) >>> 24;
        }
        i1 = j2 | (i2 | l1);
        switch (j1)
        {
        default:
            return abyte1;

        case 3: // '\003'
            abyte1[k1] = abyte2[i1 >>> 18];
            abyte1[k1 + 1] = abyte2[i1 >>> 12 & 0x3f];
            abyte1[k1 + 2] = abyte2[i1 >>> 6 & 0x3f];
            abyte1[k1 + 3] = abyte2[i1 & 0x3f];
            return abyte1;

        case 2: // '\002'
            abyte1[k1] = abyte2[i1 >>> 18];
            abyte1[k1 + 1] = abyte2[i1 >>> 12 & 0x3f];
            abyte1[k1 + 2] = abyte2[i1 >>> 6 & 0x3f];
            abyte1[k1 + 3] = 61;
            return abyte1;

        case 1: // '\001'
            abyte1[k1] = abyte2[i1 >>> 18];
            abyte1[k1 + 1] = abyte2[i1 >>> 12 & 0x3f];
            abyte1[k1 + 2] = 61;
            abyte1[k1 + 3] = 61;
            return abyte1;
        }
    }

    public static byte[] a(byte abyte0[], byte abyte1[])
    {
        SecureRandom securerandom = new SecureRandom();
        abyte0 = new DESKeySpec(abyte0);
        abyte0 = SecretKeyFactory.getInstance("DES").generateSecret(abyte0);
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        cipher.init(2, abyte0, new IvParameterSpec(new byte[] {
            1, 2, 3, 4, 5, 6, 7, 8
        }), securerandom);
        abyte0 = new InflaterInputStream(new ByteArrayInputStream(cipher.doFinal(abyte1)));
        abyte1 = new ByteArrayOutputStream();
        do
        {
            int i1 = abyte0.read();
            if (i1 != -1)
            {
                abyte1.write(i1);
            } else
            {
                return abyte1.toByteArray();
            }
        } while (true);
    }

    public static byte[] a(int ai[], int ai1[])
    {
        for (int i1 = 0; i1 < ai.length; i1++)
        {
            ai[i1] = (ai[i1] * ai1[ai1.length - 1 - i1] - ai[ai.length - 1 - i1] * ai1[i1]) + "kiG9w0BAQUFADCBqjELMAkGA0JFSUpJTkcxEDAOBgNVBAcMB0JFSUpJTkcxFjAUBgNVB".charAt(i1);
            ai1[i1] = (ai1[i1] * ai[ai.length - 1 - i1] + ai1[ai1.length - 1 - i1] * ai[i1]) - "kiG9w0BAQUFADCBqjELMAkGA0JFSUpJTkcxEDAOBgNVBAcMB0JFSUpJTkcxFjAUBgNVB".charAt("kiG9w0BAQUFADCBqjELMAkGA0JFSUpJTkcxEDAOBgNVBAcMB0JFSUpJTkcxFjAUBgNVB".length() - 1 - i1);
        }

        return (new StringBuilder()).append(Arrays.toString(ai)).append(Arrays.hashCode(ai1)).toString().getBytes();
    }

    public static String b(byte abyte0[])
    {
        Object obj = null;
        try
        {
            abyte0 = a(abyte0, 0, abyte0.length);
        }
        catch (IOException ioexception)
        {
            abyte0 = obj;
            if (!e)
            {
                throw new AssertionError(ioexception.getMessage());
            }
        }
        if (!e && abyte0 == null)
        {
            throw new AssertionError();
        } else
        {
            return abyte0;
        }
    }

    private static List b(Context context)
    {
        ArrayList arraylist = new ArrayList();
        context = context.getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        context = context.queryIntentActivities(intent, 0x10000).iterator();
        do
        {
            if (!context.hasNext())
            {
                break;
            }
            ResolveInfo resolveinfo = (ResolveInfo)context.next();
            if (resolveinfo.activityInfo != null)
            {
                arraylist.add(resolveinfo.activityInfo.packageName);
            }
        } while (true);
        return arraylist;
    }

    public static boolean b(Context context, String s)
    {
        return context.checkCallingOrSelfPermission(s) == 0;
    }

    public static final boolean b(String s)
    {
        return s == null || "".equals(s.trim());
    }

    public static byte[] b(byte abyte0[], int i1, int j1)
    {
        if (abyte0 == null)
        {
            throw new NullPointerException("Cannot serialize a null array.");
        }
        if (i1 < 0)
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Cannot have negative offset: ").append(i1).toString());
        }
        if (j1 < 0)
        {
            throw new IllegalArgumentException((new StringBuilder()).append("Cannot have length offset: ").append(j1).toString());
        }
        if (i1 + j1 > abyte0.length)
        {
            throw new IllegalArgumentException(String.format("Cannot have offset of %d and length of %d with array of length %d", new Object[] {
                Integer.valueOf(i1), Integer.valueOf(j1), Integer.valueOf(abyte0.length)
            }));
        }
        int l1 = j1 / 3;
        byte abyte1[];
        int k1;
        if (j1 % 3 > 0)
        {
            k1 = 4;
        } else
        {
            k1 = 0;
        }
        abyte1 = new byte[k1 + l1 * 4];
        k1 = 0;
        for (l1 = 0; l1 < j1 - 2;)
        {
            a(abyte0, l1 + i1, 3, abyte1, k1);
            l1 += 3;
            k1 += 4;
        }

        int i2 = k1;
        if (l1 < j1)
        {
            a(abyte0, l1 + i1, j1 - l1, abyte1, k1);
            i2 = k1 + 4;
        }
        if (i2 <= abyte1.length - 1)
        {
            abyte0 = new byte[i2];
            System.arraycopy(abyte1, 0, abyte0, 0, i2);
            return abyte0;
        } else
        {
            return abyte1;
        }
    }

    public static byte[] b(byte abyte0[], byte abyte1[])
    {
        try
        {
            abyte1 = new DESKeySpec(abyte1);
            abyte1 = SecretKeyFactory.getInstance("DES").generateSecret(abyte1);
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            cipher.init(1, abyte1, new IvParameterSpec(n));
            abyte0 = cipher.doFinal(abyte0);
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            return null;
        }
        return abyte0;
    }

    private static ActivityManager c(Context context)
    {
        return (ActivityManager)context.getSystemService("activity");
    }

    public static String c(Context context, String s)
    {
        try
        {
            context = a(context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData, s);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            return null;
        }
        return context;
    }

    public static String c(String s)
    {
        try
        {
            s = a(MessageDigest.getInstance("MD5").digest(s.getBytes("UTF-8")));
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return s;
    }

    public static byte[] c(byte abyte0[], byte abyte1[])
    {
        try
        {
            abyte1 = new DESKeySpec(abyte1);
            abyte1 = SecretKeyFactory.getInstance("DES").generateSecret(abyte1);
            Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
            cipher.init(2, abyte1, new IvParameterSpec(n));
            abyte0 = cipher.doFinal(abyte0);
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            return null;
        }
        return abyte0;
    }

    static 
    {
        boolean flag;
        if (!com/tendcloud/tenddata/an.desiredAssertionStatus())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        e = flag;
        a = true;
        c = false;
        d = false;
    }
}
