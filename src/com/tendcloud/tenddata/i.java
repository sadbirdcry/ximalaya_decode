// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;


// Referenced classes of package com.tendcloud.tenddata:
//            j, m, ab, l

final class i extends j
{

    private static int H = 272;
    private static int a = 1;
    private ab I;

    i(l l, int k, int i1, int j1, int k1, int l1, int i2, 
            int j2)
    {
        super(l, new m(k1, Math.max(l1, a), H, i2, 273, j2), k, i1, j1, k1, i2);
        I = null;
    }

    private boolean a(int k, int l)
    {
        return k < l >>> 7;
    }

    int a()
    {
        if (F == -1)
        {
            I = g();
        }
        E = -1;
        int k3 = Math.min(z.e(), 273);
        if (k3 < 2)
        {
            return 1;
        }
        int k = 0;
        int i3 = 0;
        int l2 = 0;
        while (k < 4) 
        {
            int l1 = z.b(o[k], k3);
            int l;
            if (l1 < 2)
            {
                l = l2;
            } else
            {
                if (l1 >= D)
                {
                    E = k;
                    c(l1 - 1);
                    return l1;
                }
                l = l2;
                if (l1 > l2)
                {
                    i3 = k;
                    l = l1;
                }
            }
            k++;
            l2 = l;
        }
        int k2;
        int j3;
        if (I.c > 0)
        {
            k = I.a[I.c - 1];
            j3 = I.b[I.c - 1];
            int i1 = j3;
            int i2 = k;
            if (k >= D)
            {
                E = j3 + 4;
                c(k - 1);
                return k;
            }
            for (; I.c > 1 && i2 == I.a[I.c - 2] + 1 && a(I.b[I.c - 2], i1); i1 = I.b[I.c - 1])
            {
                ab ab1 = I;
                ab1.c = ab1.c - 1;
                i2 = I.a[I.c - 1];
            }

            j3 = i1;
            k = i2;
            if (i2 == 2)
            {
                j3 = i1;
                k = i2;
                if (i1 >= 128)
                {
                    k = 1;
                    j3 = i1;
                }
            }
        } else
        {
            j3 = 0;
            k = 0;
        }
        if (l2 >= 2 && (l2 + 1 >= k || l2 + 2 >= k && j3 >= 512 || l2 + 3 >= k && j3 >= 32768))
        {
            E = i3;
            c(l2 - 1);
            return l2;
        }
        if (k < 2 || k3 <= 2)
        {
            return 1;
        }
        I = g();
        if (I.c > 0)
        {
            int j1 = I.a[I.c - 1];
            int j2 = I.b[I.c - 1];
            if (j1 >= k && j2 < j3 || j1 == k + 1 && !a(j3, j2) || j1 > k + 1 || j1 + 1 >= k && k >= 3 && a(j2, j3))
            {
                return 1;
            }
        }
        k2 = Math.max(k - 1, 2);
        for (int k1 = 0; k1 < 4; k1++)
        {
            if (z.b(o[k1], k2) == k2)
            {
                return 1;
            }
        }

        E = j3 + 4;
        c(k - 2);
        return k;
    }

}
