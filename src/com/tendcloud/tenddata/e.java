// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.tendcloud.tenddata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.tendcloud.tenddata:
//            be, an, c, bg, 
//            s, ap, ae, r, 
//            as

final class e
{
    static final class a
        implements BaseColumns
    {

        public static final String a = "session_id";
        public static final String b = "start_time";
        public static final String c = "duration";
        public static final String d = "is_launch";
        public static final String e = "interval";
        public static final String f = "is_connected";
        public static final String g = "session";
        public static final String h[] = {
            "_id", "session_id", "start_time", "duration", "is_launch", "interval", "is_connected"
        };

        public static final void a(SQLiteDatabase sqlitedatabase)
        {
            sqlitedatabase.execSQL("CREATE TABLE session (_id INTEGER PRIMARY KEY autoincrement,session_id TEXT,start_time LONG,duration INTEGER,is_launch INTEGER,interval LONG, is_connected INTEGER)");
        }

        public static final void b(SQLiteDatabase sqlitedatabase)
        {
            sqlitedatabase.execSQL("DROP TABLE IF EXISTS session");
        }


        a()
        {
        }
    }

    static final class b
        implements BaseColumns
    {

        public static final String a = "name";
        public static final String b = "start_time";
        public static final String c = "duration";
        public static final String d = "session_id";
        public static final String e = "refer";
        public static final String f = "realtime";
        public static final String g = "activity";
        public static final String h[] = {
            "_id", "name", "start_time", "duration", "session_id", "refer", "realtime"
        };

        public static final void a(SQLiteDatabase sqlitedatabase)
        {
            sqlitedatabase.execSQL("CREATE TABLE activity (_id INTEGER PRIMARY KEY autoincrement,name TEXT,start_time LONG,duration INTEGER,session_id TEXT,refer TEXT,realtime LONG)");
        }

        public static final void b(SQLiteDatabase sqlitedatabase)
        {
            sqlitedatabase.execSQL("DROP TABLE IF EXISTS activity");
        }


        b()
        {
        }
    }

    static final class c
        implements BaseColumns
    {

        public static final String a = "error_time";
        public static final String b = "message";
        public static final String c = "repeat";
        public static final String d = "shorthashcode";
        public static final String e = "error_report";
        public static final String f[] = {
            "_id", "error_time", "message", "repeat", "shorthashcode"
        };

        public static final void a(SQLiteDatabase sqlitedatabase)
        {
            sqlitedatabase.execSQL("CREATE TABLE error_report (_id INTEGER PRIMARY KEY autoincrement,error_time LONG,message BLOB,repeat INTERGER,shorthashcode TEXT)");
        }

        public static final void b(SQLiteDatabase sqlitedatabase)
        {
            sqlitedatabase.execSQL("DROP TABLE IF EXISTS error_report");
        }


        c()
        {
        }
    }

    static final class d
        implements BaseColumns
    {

        public static final String a = "event_id";
        public static final String b = "event_label";
        public static final String c = "session_id";
        public static final String d = "occurtime";
        public static final String e = "paramap";
        public static final String f = "app_event";
        public static final String g[] = {
            "_id", "event_id", "event_label", "session_id", "occurtime", "paramap"
        };

        public static final void a(SQLiteDatabase sqlitedatabase)
        {
            sqlitedatabase.execSQL("CREATE TABLE app_event (_id INTEGER PRIMARY KEY autoincrement,event_id TEXT,event_label TEXT,session_id TEXT,occurtime LONG,paramap BLOB)");
        }

        public static final void b(SQLiteDatabase sqlitedatabase)
        {
            sqlitedatabase.execSQL("DROP TABLE IF EXISTS app_event");
        }


        d()
        {
        }
    }


    private static final int a = 5;
    private static final String b = "10";
    private static final String c = "TDtcagent.db";
    private static SQLiteDatabase d;
    private static int e;

    e()
    {
    }

    static long a(long l)
    {
        Object obj1 = null;
        Object obj = d.query("activity", b.h, "_id=?", new String[] {
            String.valueOf(l)
        }, null, null, "_id");
        if (!((Cursor) (obj)).moveToFirst() || ((Cursor) (obj)).isAfterLast())
        {
            break MISSING_BLOCK_LABEL_70;
        }
        l = ((Cursor) (obj)).getLong(6);
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        return l;
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
_L2:
        return 0L;
        obj;
        obj = null;
_L5:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        if (true) goto _L2; else goto _L1
_L1:
        obj;
_L4:
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw obj;
        Exception exception1;
        exception1;
        obj1 = obj;
        obj = exception1;
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
          goto _L5
    }

    static long a(long l, String s1)
    {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("error_time", Long.valueOf(l));
        be be1;
        StringBuffer stringbuffer;
        int i;
        try
        {
            be1 = new be();
            stringbuffer = new StringBuffer("");
            l = a(s1, be1, stringbuffer);
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            return 0L;
        }
        if (0L != l)
        {
            break MISSING_BLOCK_LABEL_97;
        }
        contentvalues.put("message", s1.getBytes("UTF-8"));
        contentvalues.put("repeat", Integer.valueOf(1));
        contentvalues.put("shorthashcode", stringbuffer.toString());
        return d.insert("error_report", null, contentvalues);
        contentvalues.put("repeat", Integer.valueOf(be1.b + 1));
        i = d.update("error_report", contentvalues, "_id=?", new String[] {
            String.valueOf(l)
        });
        return (long)i;
    }

    static long a(String s1, long l, long l1, int i)
    {
        try
        {
            ContentValues contentvalues = new ContentValues();
            contentvalues.put("session_id", s1);
            contentvalues.put("start_time", Long.valueOf(l));
            contentvalues.put("duration", Integer.valueOf(0));
            contentvalues.put("is_launch", Integer.valueOf(0));
            contentvalues.put("interval", Long.valueOf(l1));
            contentvalues.put("is_connected", Integer.valueOf(i));
            d.insert("session", null, contentvalues);
        }
        // Misplaced declaration of an exception variable
        catch (String s1) { }
        return 0L;
    }

    static long a(String s1, be be1, StringBuffer stringbuffer)
    {
        Cursor cursor = d.query("error_report", c.f, null, null, null, null, "_id");
        int i;
        s1 = s1.split("\r\n");
        i = s1.length;
        if (i >= 3) goto _L2; else goto _L1
_L1:
        long l;
        long l1 = 0L;
        l = l1;
        if (cursor != null)
        {
            cursor.close();
            l = l1;
        }
_L11:
        return l;
_L2:
        s1 = (new StringBuilder()).append(s1[0]).append("\r\n").append(s1[1]).append("\r\n").append(s1[2]).toString();
        stringbuffer.append(an.c(s1));
        if (!cursor.moveToFirst()) goto _L4; else goto _L3
_L3:
        if (cursor.isAfterLast()) goto _L4; else goto _L5
_L5:
        be1.a = cursor.getLong(1);
        be1.d = cursor.getBlob(2);
        be1.b = cursor.getInt(3);
        stringbuffer = new String(be1.d, "UTF-8");
        if (stringbuffer.length() < s1.length()) goto _L3; else goto _L6
_L6:
        stringbuffer = stringbuffer.split("\r\n");
        if (stringbuffer.length < 3) goto _L3; else goto _L7
_L7:
        if (!(new StringBuilder()).append(stringbuffer[0]).append("\r\n").append(stringbuffer[1]).append("\r\n").append(stringbuffer[2]).toString().equals(s1)) goto _L9; else goto _L8
_L8:
        long l2 = cursor.getLong(0);
        l = l2;
        if (cursor == null) goto _L11; else goto _L10
_L10:
        cursor.close();
        return l2;
_L9:
        cursor.moveToNext();
          goto _L3
        s1;
_L17:
        if (cursor != null)
        {
            cursor.close();
        }
_L13:
        return 0L;
_L4:
        if (cursor != null)
        {
            cursor.close();
        }
        if (true) goto _L13; else goto _L12
_L12:
        s1;
        cursor = null;
_L15:
        if (cursor != null)
        {
            cursor.close();
        }
        throw s1;
        s1;
        if (true) goto _L15; else goto _L14
_L14:
        s1;
        cursor = null;
        if (true) goto _L17; else goto _L16
_L16:
    }

    static long a(String s1, String s2, long l, int i, String s3, long l1)
    {
        try
        {
            ContentValues contentvalues = new ContentValues();
            contentvalues.put("session_id", s1);
            contentvalues.put("name", s2);
            contentvalues.put("start_time", Long.valueOf(l));
            contentvalues.put("duration", Integer.valueOf(i));
            contentvalues.put("refer", s3);
            contentvalues.put("realtime", Long.valueOf(l1));
            l = d.insert("activity", null, contentvalues);
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            return -1L;
        }
        return l;
    }

    static long a(List list)
    {
        int i = list.size();
        if (i != 0) goto _L2; else goto _L1
_L1:
        long l1 = 0L;
_L5:
        return l1;
_L2:
        long l;
        i--;
        l = 0L;
_L9:
        if (i < 0) goto _L4; else goto _L3
_L3:
        Object obj;
        long l2 = l;
        try
        {
            obj = new StringBuilder();
        }
        // Misplaced declaration of an exception variable
        catch (List list)
        {
            return l2;
        }
        l2 = l;
        ((StringBuilder) (obj)).append("SELECT MAX(_id) from activity where duration != 0 and session_id =?");
        obj = d.rawQuery(((StringBuilder) (obj)).toString(), new String[] {
            ((com.tendcloud.tenddata.c)list.get(i)).a
        });
        l1 = l;
        if (!((Cursor) (obj)).moveToFirst())
        {
            break MISSING_BLOCK_LABEL_130;
        }
        l1 = ((Cursor) (obj)).getLong(0);
label0:
        {
            l = l1;
            if (l == 0L)
            {
                break label0;
            }
            l1 = l;
            if (obj != null)
            {
                try
                {
                    ((Cursor) (obj)).close();
                }
                // Misplaced declaration of an exception variable
                catch (List list)
                {
                    return l;
                }
                return l;
            }
        }
          goto _L5
        l1 = l;
        if (obj == null) goto _L7; else goto _L6
_L6:
        l2 = l1;
        ((Cursor) (obj)).close();
          goto _L7
_L8:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_160;
        }
        l2 = l;
        ((Cursor) (obj)).close();
        l2 = l;
        throw list;
        list;
          goto _L8
_L4:
        return l;
_L7:
        i--;
        l = l1;
          goto _L9
        list;
        obj = null;
          goto _L8
    }

    static List a(String s1, long l)
    {
        Object obj;
        ArrayList arraylist;
        obj = null;
        arraylist = new ArrayList();
        s1 = d.query("activity", b.h, "session_id=? AND duration != 0 ", new String[] {
            s1
        }, null, null, "_id");
        if (s1.moveToFirst())
        {
            for (; !s1.isAfterLast(); s1.moveToNext())
            {
                obj = new bg();
                obj.a = s1.getString(1);
                obj.b = s1.getLong(2);
                obj.c = s1.getInt(3);
                obj.d = s1.getString(5);
                arraylist.add(obj);
            }

        }
          goto _L1
        obj;
_L7:
        if (s1 != null)
        {
            s1.close();
        }
_L3:
        return arraylist;
_L1:
        if (s1 == null) goto _L3; else goto _L2
_L2:
        s1.close();
        return arraylist;
        Exception exception;
        exception;
        s1 = ((String) (obj));
        obj = exception;
_L5:
        if (s1 != null)
        {
            s1.close();
        }
        throw obj;
        obj;
        if (true) goto _L5; else goto _L4
_L4:
        s1;
        s1 = null;
        if (true) goto _L7; else goto _L6
_L6:
    }

    private static Map a(byte abyte0[])
    {
        if (abyte0 == null || abyte0.length == 0)
        {
            return null;
        }
        HashMap hashmap;
        hashmap = new HashMap();
        abyte0 = new ByteArrayInputStream(abyte0);
        Object obj;
        Object obj1;
        DataInputStream datainputstream;
        String s1;
        int i;
        int j;
        int k;
        try
        {
            datainputstream = new DataInputStream(abyte0);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            datainputstream = null;
            obj1 = abyte0;
            abyte0 = datainputstream;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            datainputstream = null;
        }
        j = datainputstream.readInt();
        i = 0;
_L9:
        if (i >= j)
        {
            break MISSING_BLOCK_LABEL_124;
        }
        s1 = datainputstream.readUTF();
        k = datainputstream.readInt();
        if (k != 66) goto _L2; else goto _L1
_L1:
        obj = Double.valueOf(datainputstream.readDouble());
_L3:
        hashmap.put(s1, obj);
        i++;
        continue; /* Loop/switch isn't completed */
_L2:
        if (k != 88)
        {
            break MISSING_BLOCK_LABEL_114;
        }
        obj = datainputstream.readUTF();
          goto _L3
        s.a(abyte0);
        s.a(datainputstream);
        return null;
        s.a(abyte0);
        s.a(datainputstream);
        return hashmap;
        abyte0;
        abyte0 = null;
        obj = null;
_L7:
        s.a(((java.io.Closeable) (obj)));
        s.a(abyte0);
        return null;
        obj1;
        datainputstream = null;
        abyte0 = null;
_L5:
        s.a(abyte0);
        s.a(datainputstream);
        throw obj1;
        obj1;
        if (true) goto _L5; else goto _L4
_L4:
        obj1;
        obj1 = abyte0;
        abyte0 = datainputstream;
        if (true) goto _L7; else goto _L6
_L6:
        if (true) goto _L9; else goto _L8
_L8:
    }

    static void a()
    {
        d.setVersion(5);
        a.a(d);
        b.a(d);
        d.a(d);
        c.a(d);
    }

    static void a(long l, long l1)
    {
        l1 = (l1 - a(l)) / 1000L;
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("duration", Long.valueOf(l1));
        try
        {
            d.update("activity", contentvalues, "_id=?", new String[] {
                String.valueOf(l)
            });
            return;
        }
        catch (Exception exception)
        {
            return;
        }
    }

    static void a(Context context)
    {
        com/tendcloud/tenddata/e;
        JVM INSTR monitorenter ;
        boolean flag;
        if (d != null)
        {
            break MISSING_BLOCK_LABEL_144;
        }
        context = new File(context.getFilesDir(), "TDtcagent.db");
        flag = context.exists();
        if (!context.getParentFile().exists())
        {
            context.getParentFile().mkdirs();
        }
        d = SQLiteDatabase.openOrCreateDatabase(context, null);
        d.setLockingEnabled(true);
        d.setMaximumSize(0x7a1200L);
        e = 1;
        if (flag) goto _L2; else goto _L1
_L1:
        a();
_L4:
        com/tendcloud/tenddata/e;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (5 <= d.getVersion()) goto _L4; else goto _L3
_L3:
        d.execSQL("DROP TABLE IF EXISTS error_report");
        d.execSQL("DROP TABLE IF EXISTS app_event");
        d.execSQL("DROP TABLE IF EXISTS session");
        d.execSQL("DROP TABLE IF EXISTS activity");
        a();
          goto _L4
        context;
        throw context;
        e++;
          goto _L4
    }

    static void a(String s1)
    {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("is_launch", Integer.valueOf(1));
        d.update("session", contentvalues, "session_id=?", new String[] {
            s1
        });
    }

    static void a(String s1, int i)
    {
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("duration", Integer.valueOf(i));
        try
        {
            d.update("session", contentvalues, "session_id=?", new String[] {
                s1
            });
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            return;
        }
    }

    static boolean a(String s1, String s2, String s3, long l, Map map)
    {
        ContentValues contentvalues;
        contentvalues = new ContentValues();
        contentvalues.put("event_id", s2);
        contentvalues.put("event_label", s3);
        contentvalues.put("session_id", s1);
        contentvalues.put("occurtime", Long.valueOf(l));
        if (!s2.startsWith("__tx"))
        {
            break MISSING_BLOCK_LABEL_92;
        }
        contentvalues.put("paramap", a(map, false));
_L1:
        return d.insert("app_event", null, contentvalues) != -1L;
        try
        {
            contentvalues.put("paramap", a(map, true));
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            return false;
        }
          goto _L1
    }

    private static byte[] a(Map map, boolean flag)
    {
        int i;
        if (map == null || map.size() == 0)
        {
            return null;
        }
        i = map.size();
        ByteArrayOutputStream bytearrayoutputstream;
        DataOutputStream dataoutputstream;
        Object obj;
        try
        {
            bytearrayoutputstream = new ByteArrayOutputStream();
        }
        // Misplaced declaration of an exception variable
        catch (Map map)
        {
            map = null;
            bytearrayoutputstream = null;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            dataoutputstream = null;
            bytearrayoutputstream = null;
            continue; /* Loop/switch isn't completed */
        }
        dataoutputstream = new DataOutputStream(bytearrayoutputstream);
        dataoutputstream.writeInt(i);
        map = map.entrySet().iterator();
        i = 0;
_L11:
        if (!map.hasNext()) goto _L2; else goto _L1
_L1:
        obj = (java.util.Map.Entry)map.next();
        dataoutputstream.writeUTF((String)((java.util.Map.Entry) (obj)).getKey());
        obj = ((java.util.Map.Entry) (obj)).getValue();
        if (!(obj instanceof Number)) goto _L4; else goto _L3
_L3:
        dataoutputstream.writeInt(66);
        dataoutputstream.writeDouble(((Number)obj).doubleValue());
          goto _L5
_L2:
        map = bytearrayoutputstream.toByteArray();
        s.a(bytearrayoutputstream);
        s.a(dataoutputstream);
        return map;
_L4:
        dataoutputstream.writeInt(88);
        if (!flag) goto _L7; else goto _L6
_L6:
        dataoutputstream.writeUTF(an.a(obj.toString()));
          goto _L5
_L9:
        s.a(bytearrayoutputstream);
        s.a(map);
        return null;
_L7:
        try
        {
            dataoutputstream.writeUTF(obj.toString());
            break; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Map map)
        {
            map = dataoutputstream;
        }
        finally { }
        if (true) goto _L9; else goto _L8
_L8:
        s.a(bytearrayoutputstream);
        s.a(dataoutputstream);
        throw map;
        map;
        dataoutputstream = null;
        if (true) goto _L8; else goto _L10
_L10:
        break MISSING_BLOCK_LABEL_31;
        map;
        map = null;
        if (true) goto _L9; else goto _L5
_L5:
        i++;
        if (i != 10) goto _L11; else goto _L2
    }

    static long b(List list)
    {
        int i = list.size();
        if (i != 0) goto _L2; else goto _L1
_L1:
        long l = 0L;
_L6:
        return l;
_L2:
        i--;
_L10:
        if (i < 0) goto _L4; else goto _L3
_L3:
        Object obj;
        obj = new StringBuilder();
        ((StringBuilder) (obj)).append("SELECT MAX(_id) from app_event where session_id =?");
        Cursor cursor = null;
        obj = d.rawQuery(((StringBuilder) (obj)).toString(), new String[] {
            ((com.tendcloud.tenddata.c)list.get(i)).a
        });
        long l1;
        if (!((Cursor) (obj)).moveToFirst())
        {
            break MISSING_BLOCK_LABEL_116;
        }
        l1 = ((Cursor) (obj)).getLong(0);
        if (l1 == 0L)
        {
            break MISSING_BLOCK_LABEL_116;
        }
        l = l1;
        if (obj == null) goto _L6; else goto _L5
_L5:
        ((Cursor) (obj)).close();
        return l1;
        if (obj == null) goto _L8; else goto _L7
_L7:
        ((Cursor) (obj)).close();
          goto _L8
_L9:
        if (cursor == null)
        {
            break MISSING_BLOCK_LABEL_139;
        }
        cursor.close();
        throw list;
        list;
        cursor = ((Cursor) (obj));
          goto _L9
        list;
_L4:
        return 0L;
_L8:
        i--;
          goto _L10
        list;
          goto _L9
    }

    static List b(String s1, long l)
    {
        Object obj;
        Object obj1;
        ArrayList arraylist;
        obj1 = null;
        arraylist = new ArrayList();
        obj = obj1;
        StringBuilder stringbuilder = new StringBuilder();
        obj = obj1;
        stringbuilder.append("SELECT COUNT(_id), MAX(occurtime), event_id, event_label, paramap from app_event where session_id = ? group by event_id, event_label, paramap");
        obj = obj1;
        s1 = d.rawQuery(stringbuilder.toString(), new String[] {
            s1
        });
        obj = s1;
        if (!s1.moveToFirst()) goto _L2; else goto _L1
_L1:
        obj = s1;
        if (s1.isAfterLast()) goto _L2; else goto _L3
_L3:
        obj = s1;
        obj1 = new ap();
        obj = s1;
        obj1.c = s1.getInt(0);
        obj = s1;
        obj1.d = s1.getLong(1);
        obj = s1;
        obj1.a = s1.getString(2);
        obj = s1;
        obj1.b = s1.getString(3);
        obj = s1;
        obj1.e = null;
        obj = s1;
        obj1.e = a(s1.getBlob(4));
        obj = s1;
        arraylist.add(obj1);
        obj = s1;
        s1.moveToNext();
        if (true) goto _L1; else goto _L4
_L4:
        s1;
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
_L6:
        return arraylist;
_L2:
        if (s1 == null) goto _L6; else goto _L5
_L5:
        s1.close();
        return arraylist;
        s1;
        obj1 = null;
        obj = s1;
_L8:
        if (obj1 != null)
        {
            ((Cursor) (obj1)).close();
        }
        throw obj;
        obj;
        obj1 = s1;
        if (true) goto _L8; else goto _L7
_L7:
    }

    static void b()
    {
        com/tendcloud/tenddata/e;
        JVM INSTR monitorenter ;
        e--;
        e = Math.max(0, e);
        if (e == 0 && d != null)
        {
            d.close();
            d = null;
        }
        com/tendcloud/tenddata/e;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    static void b(long l)
    {
        d.delete("activity", "_id<=? AND duration != 0 ", new String[] {
            String.valueOf(l)
        });
    }

    static void b(String s1)
    {
        d.delete("session", "session_id=?", new String[] {
            s1
        });
    }

    static long c()
    {
        return DatabaseUtils.queryNumEntries(d, "session");
    }

    static void c(long l)
    {
        ae.a(new String[] {
            "Delete App Event Less Than Id", (new StringBuilder()).append("id:").append(l).toString()
        });
        d.delete("app_event", "_id<=? ", new String[] {
            String.valueOf(l)
        });
    }

    static void c(String s1)
    {
        d.delete("activity", "session_id=? ", new String[] {
            s1
        });
    }

    static List d()
    {
        Cursor cursor;
        ArrayList arraylist;
        cursor = null;
        arraylist = new ArrayList();
        Object obj = d.query("session", a.h, null, null, null, null, "_id", "10");
        cursor = ((Cursor) (obj));
        if (!cursor.moveToFirst()) goto _L2; else goto _L1
_L1:
        if (cursor.isAfterLast()) goto _L2; else goto _L3
_L3:
        obj = new com.tendcloud.tenddata.c();
        obj.a = cursor.getString(1);
        obj.b = cursor.getLong(2);
        obj.g = cursor.getInt(3);
        if (cursor.getInt(4) != 0) goto _L5; else goto _L4
_L4:
        obj.c = 1;
_L6:
        if (1 == ((com.tendcloud.tenddata.c) (obj)).c)
        {
            obj.j = cursor.getInt(5);
            if (((com.tendcloud.tenddata.c) (obj)).j < 0)
            {
                obj.j = 0;
            }
            obj.g = ((com.tendcloud.tenddata.c) (obj)).j / 1000;
        }
        obj.k = cursor.getInt(6);
        arraylist.add(obj);
        cursor.moveToNext();
          goto _L1
        obj;
_L10:
        if (cursor != null)
        {
            cursor.close();
        }
_L8:
        return arraylist;
_L5:
        Exception exception1;
        int i;
        if (((com.tendcloud.tenddata.c) (obj)).g != 0)
        {
            i = 3;
        } else
        {
            i = 2;
        }
        obj.c = i;
          goto _L6
        exception1;
_L9:
        if (cursor != null)
        {
            cursor.close();
        }
        throw exception1;
_L2:
        if (cursor == null) goto _L8; else goto _L7
_L7:
        cursor.close();
        return arraylist;
        exception1;
          goto _L9
        Exception exception;
        exception;
        exception = null;
          goto _L10
    }

    static void d(long l)
    {
        d.delete("error_report", "_id<=?", new String[] {
            String.valueOf(l)
        });
    }

    static void d(String s1)
    {
        d.delete("app_event", "session_id=? ", new String[] {
            s1
        });
    }

    static long e(String s1)
    {
        Object obj = null;
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append("SELECT MAX(_id) from ");
        stringbuilder.append(s1);
        s1 = d.rawQuery(stringbuilder.toString(), null);
        long l;
        if (!s1.moveToFirst() || s1.isAfterLast())
        {
            break MISSING_BLOCK_LABEL_74;
        }
        l = s1.getLong(0);
        if (s1 != null)
        {
            s1.close();
        }
        return l;
        if (s1 != null)
        {
            s1.close();
        }
_L2:
        return 0L;
        s1;
        s1 = ((String) (obj));
_L5:
        if (s1 != null)
        {
            s1.close();
        }
        if (true) goto _L2; else goto _L1
_L1:
        s1;
        String s2;
        s2 = null;
        obj = s1;
_L4:
        if (s2 != null)
        {
            s2.close();
        }
        throw obj;
        obj;
        s2 = s1;
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
          goto _L5
    }

    static List e(long l)
    {
        Object obj;
        Object obj2;
        obj2 = new ArrayList();
        obj = null;
        Object obj1;
        obj1 = new StringBuilder();
        ((StringBuilder) (obj1)).append("SELECT error_time,message,repeat, shorthashcode from error_report where _id<=?");
        obj1 = d.rawQuery(((StringBuilder) (obj1)).toString(), new String[] {
            String.valueOf(l)
        });
        obj = obj1;
        if (!((Cursor) (obj)).moveToFirst()) goto _L2; else goto _L1
_L1:
        if (s.h() == null) goto _L4; else goto _L3
_L3:
        obj1 = String.valueOf(r.c(s.h()));
_L5:
        for (; !((Cursor) (obj)).isAfterLast(); ((Cursor) (obj)).moveToNext())
        {
            as as1 = new as();
            as1.a = 3;
            be be1 = new be();
            be1.a = ((Cursor) (obj)).getLong(0);
            be1.d = ((Cursor) (obj)).getBlob(1);
            be1.b = ((Cursor) (obj)).getInt(2);
            be1.e = ((Cursor) (obj)).getString(3);
            be1.c = ((String) (obj1));
            as1.d = be1;
            ((List) (obj2)).add(as1);
        }

          goto _L2
        obj1;
_L10:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
_L7:
        return ((List) (obj2));
_L4:
        obj1 = "";
          goto _L5
_L2:
        if (obj == null) goto _L7; else goto _L6
_L6:
        ((Cursor) (obj)).close();
        return ((List) (obj2));
        obj;
        obj2 = null;
        obj1 = obj;
_L9:
        if (obj2 != null)
        {
            ((Cursor) (obj2)).close();
        }
        throw obj1;
        obj1;
        obj2 = obj;
        if (true) goto _L9; else goto _L8
_L8:
        Exception exception;
        exception;
          goto _L10
    }
}
