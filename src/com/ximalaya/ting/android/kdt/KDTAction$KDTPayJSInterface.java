// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.kdt;

import android.widget.Toast;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.kdt:
//            KDTAction

public class this._cls0
{

    final KDTAction this$0;

    public void appWXPay(String s)
    {
        Logger.log((new StringBuilder()).append("appWXPay param = ").append(s).toString());
        if (!KDTAction.access$400(KDTAction.this).isWXAppInstalled())
        {
            Toast.makeText(KDTAction.access$500(KDTAction.this), "\u8BF7\u5148\u5B89\u88C5\u5FAE\u4FE1", 0).show();
            return;
        }
        if (KDTAction.access$400(KDTAction.this).getWXAppSupportAPI() < 0x22000001)
        {
            Toast.makeText(KDTAction.access$500(KDTAction.this), "\u5FAE\u4FE1\u7248\u672C\u592A\u4F4E\uFF0C\u8BF7\u5148\u66F4\u65B0\u5FAE\u4FE1", 0).show();
            return;
        } else
        {
            String as[] = s.split("&");
            s = as[0].split("=");
            as = as[1].split("=");
            s = s[1];
            String s1 = as[1];
            class _cls1 extends MyAsyncTask
            {

                final KDTAction.KDTPayJSInterface this$1;

                public volatile Object doInBackground(Object aobj[])
                {
                    return doInBackground((String[])aobj);
                }

                public transient String doInBackground(String as1[])
                {
                    String s2 = as1[0];
                    as1 = as1[1];
                    return KDTAction.access$600(this$0, s2, as1);
                }

                public volatile void onPostExecute(Object obj)
                {
                    onPostExecute((String)obj);
                }

                public void onPostExecute(String s2)
                {
label0:
                    {
                        if (s2 != null)
                        {
                            s2 = JSON.parseObject(s2).getJSONObject("response");
                            if (s2 != null)
                            {
                                break label0;
                            }
                        }
                        return;
                    }
                    ((MyApplication)KDTAction.access$500(this$0).getApplicationContext()).c = 0;
                    PayReq payreq = new PayReq();
                    payreq.appId = s2.getString("appid");
                    payreq.partnerId = s2.getString("partnerid");
                    payreq.prepayId = s2.getString("prepayid");
                    payreq.nonceStr = s2.getString("noncestr");
                    payreq.timeStamp = s2.getString("timestamp");
                    payreq.packageValue = s2.getString("package");
                    payreq.sign = s2.getString("sign");
                    KDTAction.access$400(this$0).sendReq(payreq);
                }

            _cls1()
            {
                this$1 = KDTAction.KDTPayJSInterface.this;
                super();
            }
            }

            (new _cls1()).myexec(new String[] {
                s, s1
            });
            return;
        }
    }

    public _cls1()
    {
        this$0 = KDTAction.this;
        super();
    }
}
