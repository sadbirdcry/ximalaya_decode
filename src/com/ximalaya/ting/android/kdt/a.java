// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.kdt;

import com.ximalaya.ting.android.util.FreeFlowUtil;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

// Referenced classes of package com.ximalaya.ting.android.kdt:
//            b

public class a
{

    private String a;
    private String b;

    public a(String s, String s1)
        throws Exception
    {
        if ("".equals(s) || "".equals(s1))
        {
            throw new Exception("appId \u548C appSecret \u4E0D\u80FD\u4E3A\u7A7A");
        } else
        {
            a = s;
            b = s1;
            return;
        }
    }

    private String a(HashMap hashmap)
    {
        Object aobj[] = hashmap.keySet().toArray();
        String s = "";
        int i = 0;
        while (i < aobj.length) 
        {
            String s1 = (String)aobj[i];
            if (i == 0)
            {
                s = (new StringBuilder()).append(s).append(s1).append("=").append((String)hashmap.get(s1)).toString();
            } else
            {
                s = (new StringBuilder()).append(s).append("&").append(s1).append("=").append((String)hashmap.get(s1)).toString();
            }
            i++;
        }
        return s;
    }

    private HashMap a(String s)
    {
        HashMap hashmap = new HashMap();
        hashmap.put("app_id", a);
        hashmap.put("method", s);
        hashmap.put("timestamp", (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date()));
        hashmap.put("format", "json");
        hashmap.put("sign_method", "md5");
        hashmap.put("v", "1.0");
        return hashmap;
    }

    private HashMap c(String s, HashMap hashmap)
        throws Exception
    {
        s = a(s);
        String s1;
        for (Iterator iterator = hashmap.keySet().iterator(); iterator.hasNext(); s.put(s1, hashmap.get(s1)))
        {
            s1 = (String)iterator.next();
            if (s.containsKey(s1))
            {
                throw new Exception("\u53C2\u6570\u540D\u51B2\u7A81");
            }
        }

        s.put("sign", com.ximalaya.ting.android.kdt.b.a(b, s));
        return s;
    }

    public HttpResponse a(String s, HashMap hashmap)
        throws Exception
    {
        hashmap = (new StringBuilder()).append("http://open.koudaitong.com/api/entry?").append(b(s, hashmap)).toString();
        s = new DefaultHttpClient();
        FreeFlowUtil.setProxyForHttpClient(s);
        hashmap = new HttpGet(hashmap);
        hashmap.addHeader("User-Agent", "KdtApiSdk Client v0.1");
        return s.execute(hashmap);
    }

    public String b(String s, HashMap hashmap)
    {
        try
        {
            s = URLEncoder.encode(a(c(s, hashmap)), "UTF-8").replace("%3A", ":").replace("%2F", "/").replace("%26", "&").replace("%3D", "=").replace("%3F", "?");
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return "";
        }
        return s;
    }
}
