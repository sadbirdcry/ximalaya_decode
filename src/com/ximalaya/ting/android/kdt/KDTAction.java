// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.kdt;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.webkit.WebView;
import android.widget.Toast;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;
import java.io.PrintStream;
import java.util.HashMap;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.util.EntityUtils;

// Referenced classes of package com.ximalaya.ting.android.kdt:
//            a

public class KDTAction
{
    public class KDTJsInterface
    {

        Context mContext;
        final KDTAction this$0;

        public void checkAppLogin()
        {
            Object obj = UserInfoMannage.getInstance().getUser();
            if (obj != null)
            {
                outputAppUserInfo(KDTAction.getOutputAppUserInfoStr(((LoginInfoModel) (obj))));
                return;
            }
            obj = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
            ((Intent) (obj)).setFlags(0x10000000);
            if (!TextUtils.isEmpty(mCurrentUrl))
            {
                ((Intent) (obj)).putExtra("ExtraUrl", mCurrentUrl);
            }
            mContext.startActivity(((Intent) (obj)));
        }

        public void goAppHome()
        {
            mContext.startActivity(new Intent(mContext, com/ximalaya/ting/android/activity/MainTabActivity2));
        }

        public KDTJsInterface(Context context)
        {
            this$0 = KDTAction.this;
            super();
            mContext = context;
        }
    }

    public class KDTPayJSInterface
    {

        final KDTAction this$0;

        public void appWXPay(String s)
        {
            Logger.log((new StringBuilder()).append("appWXPay param = ").append(s).toString());
            if (!api.isWXAppInstalled())
            {
                Toast.makeText(mContext, "\u8BF7\u5148\u5B89\u88C5\u5FAE\u4FE1", 0).show();
                return;
            }
            if (api.getWXAppSupportAPI() < 0x22000001)
            {
                Toast.makeText(mContext, "\u5FAE\u4FE1\u7248\u672C\u592A\u4F4E\uFF0C\u8BF7\u5148\u66F4\u65B0\u5FAE\u4FE1", 0).show();
                return;
            } else
            {
                String as[] = s.split("&");
                s = as[0].split("=");
                as = as[1].split("=");
                s = s[1];
                String s1 = as[1];
                class _cls1 extends MyAsyncTask
                {

                    final KDTPayJSInterface this$1;

                    public volatile Object doInBackground(Object aobj[])
                    {
                        return doInBackground((String[])aobj);
                    }

                    public transient String doInBackground(String as1[])
                    {
                        String s2 = as1[0];
                        as1 = as1[1];
                        return getWXPayParamsFromKDT(s2, as1);
                    }

                    public volatile void onPostExecute(Object obj)
                    {
                        onPostExecute((String)obj);
                    }

                    public void onPostExecute(String s2)
                    {
label0:
                        {
                            if (s2 != null)
                            {
                                s2 = JSON.parseObject(s2).getJSONObject("response");
                                if (s2 != null)
                                {
                                    break label0;
                                }
                            }
                            return;
                        }
                        ((MyApplication)mContext.getApplicationContext()).c = 0;
                        PayReq payreq = new PayReq();
                        payreq.appId = s2.getString("appid");
                        payreq.partnerId = s2.getString("partnerid");
                        payreq.prepayId = s2.getString("prepayid");
                        payreq.nonceStr = s2.getString("noncestr");
                        payreq.timeStamp = s2.getString("timestamp");
                        payreq.packageValue = s2.getString("package");
                        payreq.sign = s2.getString("sign");
                        api.sendReq(payreq);
                    }

                _cls1()
                {
                    this$1 = KDTPayJSInterface.this;
                    super();
                }
                }

                (new _cls1()).myexec(new String[] {
                    s, s1
                });
                return;
            }
        }

        public KDTPayJSInterface()
        {
            this$0 = KDTAction.this;
            super();
        }
    }


    private static final String KDT_APP_ID = "69a2582e6657208f4a14";
    private static final String KDT_APP_SECRET = "e78ea02429256d4a2867b9c70f093b18";
    public static final String MY_ORDERS_URL = "http://wap.koudaitong.com/v2/showcase/usercenter?kdt_id=640794";
    public static boolean isMyOrdersShow = true;
    private IWXAPI api;
    private Context mContext;
    private String mCurrentUrl;
    private WebView mWebView;

    public KDTAction(WebView webview, Context context)
    {
        mWebView = webview;
        mContext = context;
        api = WXAPIFactory.createWXAPI(mContext, b.b, false);
    }

    private static String getOutputAppUserInfoStr(LoginInfoModel logininfomodel)
    {
        StringBuilder stringbuilder = new StringBuilder();
        StringBuilder stringbuilder1 = stringbuilder.append("{\"user_id\" : \"").append(logininfomodel.uid).append("\",").append("\"user_name\" : \"\",").append("\"nick_name\" : \"").append(logininfomodel.nickname).append("\",").append("\"gender\" : \"0\",").append("\"telephone\" : \"");
        String s;
        if (TextUtils.isEmpty(logininfomodel.mPhone))
        {
            s = "";
        } else
        {
            s = logininfomodel.mPhone;
        }
        stringbuilder1.append(s).append("\",").append("\"avatar\" : \"").append(logininfomodel.smallLogo).append("\",").append("\"extra\" : {}}");
        System.out.println((new StringBuilder()).append("\u767B\u9646\u4FE1\u606F").append(stringbuilder.toString()).toString());
        return stringbuilder.toString();
    }

    private String getWXPayParamsFromKDT(String s, String s1)
    {
        HashMap hashmap;
        Logger.log((new StringBuilder()).append("kdtId = ").append(s).append(", orderNum = ").append(s1).toString());
        hashmap = new HashMap();
        hashmap.put("kdt_id", s);
        hashmap.put("order_no", s1);
        hashmap.put("client_ip", NetworkUtils.getPhoneIP(mContext));
        int i;
        s = (new a("69a2582e6657208f4a14", "e78ea02429256d4a2867b9c70f093b18")).a("kdtpartner.pay.weixin.prepay", hashmap);
        i = s.getStatusLine().getStatusCode();
        Logger.log((new StringBuilder()).append("KDTAction response code = ").append(i).toString());
        if (i != 200) goto _L2; else goto _L1
_L1:
        s = EntityUtils.toString(s.getEntity());
_L5:
        Logger.log((new StringBuilder()).append("KDTAction result = ").append(s).toString());
        return s;
        s1;
        s = null;
_L3:
        Logger.log((new StringBuilder()).append("KDTAction exception: ").append(s1.getMessage()).toString());
        return s;
        s1;
        if (true) goto _L3; else goto _L2
_L2:
        s = null;
        if (true) goto _L5; else goto _L4
_L4:
    }

    private void outputAppUserInfo(final String userInfo)
    {
        mWebView.post(new _cls1());
    }

    public void outputAppUserInfo(LoginInfoModel logininfomodel)
    {
        outputAppUserInfo(getOutputAppUserInfoStr(logininfomodel));
    }

    public void setCurrentUrl(String s)
    {
        mCurrentUrl = s;
    }









    private class _cls1
        implements Runnable
    {

        final KDTAction this$0;
        final String val$userInfo;

        public void run()
        {
            mWebView.loadUrl((new StringBuilder()).append("javascript:outputAppUserInfo('").append(userInfo).append("')").toString());
        }

        _cls1()
        {
            this$0 = KDTAction.this;
            userInfo = s;
            super();
        }
    }

}
