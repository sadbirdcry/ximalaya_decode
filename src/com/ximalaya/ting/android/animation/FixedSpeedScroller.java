// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.animation;

import android.content.Context;
import android.view.animation.Interpolator;
import android.widget.Scroller;

public class FixedSpeedScroller extends Scroller
{

    private int mDuration;

    public FixedSpeedScroller(Context context)
    {
        super(context);
        mDuration = 800;
    }

    public FixedSpeedScroller(Context context, Interpolator interpolator)
    {
        super(context, interpolator);
        mDuration = 800;
    }

    public int getmDuration()
    {
        return mDuration;
    }

    public void setmDuration(int i)
    {
        mDuration = i;
    }

    public void startScroll(int i, int j, int k, int l)
    {
        super.startScroll(i, j, k, l, mDuration);
    }

    public void startScroll(int i, int j, int k, int l, int i1)
    {
        super.startScroll(i, j, k, l, mDuration);
    }
}
