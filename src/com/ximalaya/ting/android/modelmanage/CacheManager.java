// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.comment.CommentModelList;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoDetail;
import com.ximalaya.ting.android.model.sound.SoundInfoList;
import com.ximalaya.ting.android.model.sound.SoundLikeListModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

public class CacheManager
{

    public static final String CACHED_SOUND_COMMENTS = "sound_comments";
    public static final String CACHED_SOUND_DETAIL = "sound_detail";
    public static final String CACHED_SOUND_LIKE_LIST = "sound_like_list";
    private static final long max_saveTime = 0x9a7ec800L;

    public CacheManager()
    {
    }

    public static SoundInfoList getAlbumPlayList(Context context, long l)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        SharedPreferencesUtil sharedpreferencesutil;
        sharedpreferencesutil = SharedPreferencesUtil.getInstance(context);
        context = sharedpreferencesutil.getString("play_list_album");
        SoundInfoList soundinfolist = (SoundInfoList)JSON.parseObject(context, com/ximalaya/ting/android/model/sound/SoundInfoList);
        if (System.currentTimeMillis() - soundinfolist.updateAt < 0x9a7ec800L) goto _L2; else goto _L1
_L1:
        sharedpreferencesutil.removeByKey("play_list_album");
        context = null;
_L4:
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return context;
        context;
        sharedpreferencesutil.removeByKey("play_list_album");
        Logger.e("\u89E3\u6790json\u5F02\u5E38", "CacheManager.class , Method : getAlbumPlayList");
        context = null;
        continue; /* Loop/switch isn't completed */
_L2:
        if (soundinfolist == null)
        {
            break MISSING_BLOCK_LABEL_115;
        }
        if (soundinfolist.data.size() == 0)
        {
            break MISSING_BLOCK_LABEL_115;
        }
        context = soundinfolist;
        if (((SoundInfo)soundinfolist.data.get(0)).albumId == l)
        {
            continue; /* Loop/switch isn't completed */
        }
        sharedpreferencesutil.removeByKey("play_list_album");
        context = null;
        if (true) goto _L4; else goto _L3
_L3:
        context;
        throw context;
    }

    public static Object getObject(Context context, String s, Class class1)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        Object obj1 = null;
        String s1 = getString(context, s);
        Object obj = obj1;
        if (!Utilities.isBlank(s1))
        {
            obj = JSON.parseObject(s1, class1);
        }
_L2:
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return obj;
        class1;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", "CacheManager.class , Method : getObject");
        removeByKey(context, s);
        obj = obj1;
        if (true) goto _L2; else goto _L1
_L1:
        context;
        throw context;
    }

    public static List getObjectArray(Context context, String s, Class class1)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        Object obj = null;
        String s1 = getString(context, "sound_comments");
        List list = obj;
        if (!Utilities.isBlank(s1))
        {
            list = JSON.parseArray(s1, class1);
        }
_L2:
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return list;
        class1;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", "CacheManager.class , Method : getObjectArray");
        removeByKey(context, s);
        list = obj;
        if (true) goto _L2; else goto _L1
_L1:
        context;
        throw context;
    }

    public static CommentModelList getSoundComments(Context context)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        context = (CommentModelList)getObject(context, "sound_comments", com/ximalaya/ting/android/model/comment/CommentModelList);
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public static SoundInfoDetail getSoundDetail(Context context)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        context = (SoundInfoDetail)getObject(context, "sound_detail", com/ximalaya/ting/android/model/sound/SoundInfoDetail);
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    public static SoundLikeListModel getSoundLikeList(Context context)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        context = (SoundLikeListModel)getObject(context, "sound_like_list", com/ximalaya/ting/android/model/sound/SoundLikeListModel);
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    private static String getString(Context context, String s)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        context = SharedPreferencesUtil.getInstance(context).getString(s);
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    private static void removeByKey(Context context, String s)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        SharedPreferencesUtil.getInstance(context).removeByKey(s);
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return;
        context;
        throw context;
    }

    public static void savaObject(Context context, String s, Object obj)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        if (obj != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return;
_L2:
        Object obj1 = null;
        obj = JSON.toJSONString(obj);
_L4:
        if (!Utilities.isBlank(((String) (obj))))
        {
            saveString(context, s, ((String) (obj)));
        }
        if (true) goto _L1; else goto _L3
_L3:
        context;
        throw context;
        obj;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", "CacheManager.class , Method : savaObject");
        removeByKey(context, s);
        obj = obj1;
          goto _L4
    }

    public static void saveAlbumPlayList(Context context, SoundInfoList soundinfolist)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        if (soundinfolist != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return;
_L2:
        context = SharedPreferencesUtil.getInstance(context);
        try
        {
            soundinfolist.updateAt = System.currentTimeMillis();
            context.saveString("play_list_album", JSON.toJSONString(soundinfolist));
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Context context) { }
        Logger.e("\u89E3\u6790json\u5F02\u5E38", "CacheManager.class , Method : saveAlbumPlayList");
        if (true) goto _L1; else goto _L3
_L3:
        context;
        throw context;
    }

    public static void saveSoundComments(Context context, CommentModelList commentmodellist)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        savaObject(context, "sound_comments", commentmodellist);
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return;
        context;
        throw context;
    }

    public static void saveSoundDetail(Context context, SoundInfoDetail soundinfodetail)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        savaObject(context, "sound_detail", soundinfodetail);
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return;
        context;
        throw context;
    }

    public static void saveSoundLikeList(Context context, SoundLikeListModel soundlikelistmodel)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        savaObject(context, "sound_like_list", soundlikelistmodel);
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return;
        context;
        throw context;
    }

    private static void saveString(Context context, String s, String s1)
    {
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorenter ;
        context = SharedPreferencesUtil.getInstance(context);
        context.saveString(s, s1);
_L2:
        com/ximalaya/ting/android/modelmanage/CacheManager;
        JVM INSTR monitorexit ;
        return;
        s1;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", "CacheManager.class , Method : saveString");
        context.removeByKey(s);
        if (true) goto _L2; else goto _L1
_L1:
        context;
        throw context;
    }
}
