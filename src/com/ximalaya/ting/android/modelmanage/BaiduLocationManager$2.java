// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.Logger;
import org.apache.http.entity.StringEntity;

// Referenced classes of package com.ximalaya.ting.android.modelmanage:
//            PosInfo, UserInfoMannage, BaiduLocationManager, MyLocationManager

class this._cls0 extends Thread
{

    final BaiduLocationManager this$0;

    public void run()
    {
        String s;
        Object obj;
        LoginInfoModel logininfomodel;
        try
        {
            Logger.log("collectPosInfo start");
            s = (new StringBuilder()).append(e.ar).append("collectPosInfo").toString();
            obj = new PosInfo();
            logininfomodel = UserInfoMannage.getInstance().getUser();
        }
        catch (Exception exception)
        {
            Logger.log((new StringBuilder()).append("collectPosInfo exception=").append(exception.getMessage()).toString());
            return;
        }
        if (logininfomodel == null)
        {
            break MISSING_BLOCK_LABEL_54;
        }
        obj.uid = logininfomodel.uid;
        obj.deviceId = ((MyApplication)BaiduLocationManager.access$300(BaiduLocationManager.this).getApplicationContext()).i();
        obj.loginProvinceName = BaiduLocationManager.access$000(BaiduLocationManager.this);
        obj.loginCityName = BaiduLocationManager.access$100(BaiduLocationManager.this);
        obj.latitude = MyLocationManager.getInstance(BaiduLocationManager.access$300(BaiduLocationManager.this)).getLatitude();
        obj.longitude = MyLocationManager.getInstance(BaiduLocationManager.access$300(BaiduLocationManager.this)).getLongitude();
        obj.basestationList = BaiduLocationManager.access$400(BaiduLocationManager.this);
        obj.wifiList = BaiduLocationManager.access$500(BaiduLocationManager.this);
        obj = new StringEntity(JSON.toJSONString(obj), "UTF-8");
        f.a().a(BaiduLocationManager.access$300(BaiduLocationManager.this), s, null, ((org.apache.http.HttpEntity) (obj)), "application/json");
        return;
    }

    ()
    {
        this$0 = BaiduLocationManager.this;
        super();
    }
}
