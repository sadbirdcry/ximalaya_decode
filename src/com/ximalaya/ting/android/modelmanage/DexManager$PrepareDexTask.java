// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.io.File;

// Referenced classes of package com.ximalaya.ting.android.modelmanage:
//            DexManager

private class type extends MyAsyncTask
{

    Runnable runnable;
    final DexManager this$0;
    int type;

    protected transient File doInBackground(File afile[])
    {
        DexManager.access$400(DexManager.this, afile[0]);
        Logger.d("dex", (new StringBuilder()).append("doInBackground:").append(afile[0].getName()).toString());
        return afile[0];
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((File[])aobj);
    }

    protected void onCancelled()
    {
        super.onCancelled();
    }

    protected void onPostExecute(File file)
    {
        super.onPostExecute(file);
        String s = file.getName();
        if (s.endsWith(".dex") || s.endsWith(".") || s.endsWith(".jar"))
        {
            DexManager.access$500(DexManager.this, file, runnable, type);
        } else
        if (s.endsWith(".so"))
        {
            return;
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((File)obj);
    }

    public (Runnable runnable1, int i)
    {
        this$0 = DexManager.this;
        super();
        runnable = runnable1;
        type = i;
    }
}
