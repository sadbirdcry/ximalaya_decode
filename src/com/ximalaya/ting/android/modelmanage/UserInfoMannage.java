// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import java.util.HashSet;
import java.util.Iterator;

// Referenced classes of package com.ximalaya.ting.android.modelmanage:
//            ILoginStatusChangeListener, ScoreManage

public class UserInfoMannage
{

    private static UserInfoMannage userInfoManager = null;
    private HashSet mListeners;
    private LoginInfoModel user;

    private UserInfoMannage()
    {
    }

    public static UserInfoMannage getInstance()
    {
        if (userInfoManager != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/UserInfoMannage;
        JVM INSTR monitorenter ;
        userInfoManager = new UserInfoMannage();
        com/ximalaya/ting/android/modelmanage/UserInfoMannage;
        JVM INSTR monitorexit ;
_L2:
        return userInfoManager;
        Exception exception;
        exception;
        com/ximalaya/ting/android/modelmanage/UserInfoMannage;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static void gotoLogin(Context context)
    {
        context.startActivity(new Intent(context, com/ximalaya/ting/android/activity/login/LoginActivity));
    }

    public static boolean hasLogined()
    {
        return getInstance().getUser() != null && !TextUtils.isEmpty(getInstance().getUser().token);
    }

    public void addLoginStatusChangeListener(ILoginStatusChangeListener iloginstatuschangelistener)
    {
        if (mListeners == null)
        {
            mListeners = new HashSet();
        }
        mListeners.add(iloginstatuschangelistener);
    }

    public LoginInfoModel getUser()
    {
        return user;
    }

    public void removeLoginStatusChangeListener(ILoginStatusChangeListener iloginstatuschangelistener)
    {
        if (mListeners == null)
        {
            return;
        } else
        {
            mListeners.remove(iloginstatuschangelistener);
            return;
        }
    }

    public void setUser(LoginInfoModel logininfomodel)
    {
        if (MyApplication.b() == null)
        {
            return;
        }
        if (logininfomodel != null && !TextUtils.isEmpty(logininfomodel.token))
        {
            ((MyApplication)(MyApplication)MyApplication.b()).p();
        }
        if (user != null && logininfomodel == null)
        {
            if (mListeners != null)
            {
                for (Iterator iterator = mListeners.iterator(); iterator.hasNext(); ((ILoginStatusChangeListener)iterator.next()).onLogout()) { }
            }
        } else
        if (user == null && logininfomodel != null)
        {
            if (mListeners != null)
            {
                for (Iterator iterator1 = mListeners.iterator(); iterator1.hasNext(); ((ILoginStatusChangeListener)iterator1.next()).onLogin(logininfomodel)) { }
            }
        } else
        if (user != null && logininfomodel != null && !user.equals(logininfomodel) && mListeners != null)
        {
            for (Iterator iterator2 = mListeners.iterator(); iterator2.hasNext(); ((ILoginStatusChangeListener)iterator2.next()).onUserChange(user, logininfomodel)) { }
        }
        user = logininfomodel;
        ScoreManage.getInstance(MyApplication.b());
    }

}
