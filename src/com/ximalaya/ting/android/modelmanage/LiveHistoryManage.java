// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LiveHistoryManage
{
    public static interface HistoryUpdateListener
    {

        public abstract void update();
    }


    private static LiveHistoryManage instance;
    private Context appContext;
    private List listenerList;
    private List mSoundInfoList;
    private int maxLength;

    private LiveHistoryManage(Context context)
    {
        mSoundInfoList = new ArrayList();
        maxLength = 50;
        listenerList = new ArrayList();
        appContext = context;
    }

    public static LiveHistoryManage getInstance(Context context)
    {
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/LiveHistoryManage;
        JVM INSTR monitorenter ;
        if (instance == null)
        {
            instance = new LiveHistoryManage(context);
        }
        com/ximalaya/ting/android/modelmanage/LiveHistoryManage;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        context;
        com/ximalaya/ting/android/modelmanage/LiveHistoryManage;
        JVM INSTR monitorexit ;
        throw context;
    }

    private void saveHistoryRecord()
    {
        (new Thread(new _cls1())).start();
    }

    public void deleteAllSound()
    {
        mSoundInfoList.clear();
        for (Iterator iterator = listenerList.iterator(); iterator.hasNext(); ((HistoryUpdateListener)iterator.next()).update()) { }
        saveHistoryRecord();
    }

    public void deleteSound(int i)
    {
        if (i >= mSoundInfoList.size())
        {
            return;
        }
        mSoundInfoList.remove(i);
        for (Iterator iterator = listenerList.iterator(); iterator.hasNext(); ((HistoryUpdateListener)iterator.next()).update()) { }
        saveHistoryRecord();
    }

    public SoundInfo getHistorySound(int i)
    {
        Iterator iterator;
        if (mSoundInfoList.size() == 0)
        {
            return null;
        }
        iterator = mSoundInfoList.iterator();
_L4:
        if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        SoundInfo soundinfo = (SoundInfo)iterator.next();
        if (soundinfo.radioId != i) goto _L4; else goto _L3
_L3:
        return soundinfo;
_L2:
        soundinfo = null;
        if (true) goto _L3; else goto _L5
_L5:
    }

    public List getSoundInfoList()
    {
        return mSoundInfoList;
    }

    public void putSound(SoundInfo soundinfo)
    {
        Logger.log("LivePlay history putSound");
        if (soundinfo == null)
        {
            return;
        }
        Iterator iterator = mSoundInfoList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            if (((SoundInfo)iterator.next()).radioId != soundinfo.radioId)
            {
                continue;
            }
            iterator.remove();
            break;
        } while (true);
        if (mSoundInfoList.size() >= maxLength)
        {
            mSoundInfoList.remove(mSoundInfoList.size() - 1);
        }
        mSoundInfoList.add(0, soundinfo);
        for (soundinfo = listenerList.iterator(); soundinfo.hasNext(); ((HistoryUpdateListener)soundinfo.next()).update()) { }
        saveHistoryRecord();
    }

    public void removeHistoryUpdateListener(HistoryUpdateListener historyupdatelistener)
    {
        listenerList.remove(historyupdatelistener);
    }

    public void setOnHistoryUpdateListener(HistoryUpdateListener historyupdatelistener)
    {
        listenerList.add(historyupdatelistener);
    }

    public void updateHistoryList()
    {
        Object obj = SharedPreferencesUtil.getInstance(appContext).getString("live_history_listener");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            return;
        }
        try
        {
            obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/sound/SoundInfo);
            mSoundInfoList.clear();
            mSoundInfoList.addAll(((java.util.Collection) (obj)));
            return;
        }
        catch (Exception exception)
        {
            return;
        }
    }


    private class _cls1
        implements Runnable
    {

        final LiveHistoryManage this$0;
        final String val$historyString;

        public void run()
        {
            SharedPreferencesUtil.getInstance(appContext).saveString("live_history_listener", historyString);
        }

        _cls1()
        {
            this$0 = LiveHistoryManage.this;
            historyString = s;
            super();
        }
    }

}
