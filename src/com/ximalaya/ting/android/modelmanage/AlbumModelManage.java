// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.modelmanage:
//            UserInfoMannage

public class AlbumModelManage
{
    public static interface OnDataChangedCallback
    {

        public abstract void onDataChanged();
    }


    private static final int MAX_ALLOW_COUNT = 10;
    private static AlbumModelManage albumManage;
    private ArrayList mCallbacks;
    private Context mCon;
    private int maxLength;

    private AlbumModelManage()
    {
        maxLength = 0x7fffffff;
        mCon = MyApplication.b();
        mCallbacks = new ArrayList();
    }

    public static AlbumModelManage getInstance()
    {
        if (albumManage != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/AlbumModelManage;
        JVM INSTR monitorenter ;
        albumManage = new AlbumModelManage();
        com/ximalaya/ting/android/modelmanage/AlbumModelManage;
        JVM INSTR monitorexit ;
_L2:
        return albumManage;
        Exception exception;
        exception;
        com/ximalaya/ting/android/modelmanage/AlbumModelManage;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void addDataChangedCallback(OnDataChangedCallback ondatachangedcallback)
    {
        if (mCallbacks == null)
        {
            mCallbacks = new ArrayList();
        }
        if (mCallbacks.contains(ondatachangedcallback))
        {
            return;
        } else
        {
            mCallbacks.add(ondatachangedcallback);
            return;
        }
    }

    public boolean deleteAlbumInLocalAlbumList(AlbumModel albummodel)
    {
        Object obj;
        if (albummodel != null)
        {
            if (Utilities.isNotBlank(((String) (obj = SharedPreferencesUtil.getInstance(mCon).getString("COLLECT_ALLBUM")))) && ((List) (obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/album/AlbumModel))).remove(albummodel))
            {
                SharedPreferencesUtil.getInstance(mCon).saveString("COLLECT_ALLBUM", JSON.toJSONString(obj));
                if (mCallbacks != null)
                {
                    for (albummodel = mCallbacks.iterator(); albummodel.hasNext(); ((OnDataChangedCallback)albummodel.next()).onDataChanged()) { }
                }
                return true;
            }
        }
        return false;
    }

    public void deleteAllLocalAlbumList()
    {
        SharedPreferencesUtil.getInstance(mCon).removeByKey("COLLECT_ALLBUM");
        if (mCallbacks != null)
        {
            for (Iterator iterator = mCallbacks.iterator(); iterator.hasNext(); ((OnDataChangedCallback)iterator.next()).onDataChanged()) { }
        }
    }

    public boolean ensureLocalCollectAllow(final Context context, AlbumModel albummodel, final View view)
    {
        if (!albummodel.isFavorite)
        {
            if (isCountExceedAllow())
            {
                (new DialogBuilder(context)).setMessage(0x7f0901fd).setOkBtn(0x7f0901fb, new _cls1()).setCancelBtn(0x7f0901fc).showConfirm();
                return false;
            }
            if (!UserInfoMannage.hasLogined() && !isLocalCollectEnable(context))
            {
                context.startActivity(new Intent(context, com/ximalaya/ting/android/activity/login/LoginActivity));
                return false;
            }
        }
        return true;
    }

    public List getLocalCollectAlbumList()
    {
        List list = null;
        String s = SharedPreferencesUtil.getInstance(mCon).getString("COLLECT_ALLBUM");
        if (Utilities.isNotBlank(s))
        {
            list = JSON.parseArray(s, com/ximalaya/ting/android/model/album/AlbumModel);
        }
        return list;
    }

    public boolean isCountExceedAllow()
    {
        for (List list = albumManage.getLocalCollectAlbumList(); list == null || list.size() < 10;)
        {
            return false;
        }

        return true;
    }

    public AlbumModel isHadCollected(long l)
    {
        Object obj = null;
        Object obj1 = SharedPreferencesUtil.getInstance(mCon).getString("COLLECT_ALLBUM");
        AlbumModel albummodel = obj;
        if (Utilities.isNotBlank(((String) (obj1))))
        {
            obj1 = JSON.parseArray(((String) (obj1)), com/ximalaya/ting/android/model/album/AlbumModel);
            albummodel = new AlbumModel();
            albummodel.albumId = l;
            int i = ((List) (obj1)).indexOf(albummodel);
            albummodel = obj;
            if (-1 != i)
            {
                albummodel = (AlbumModel)((List) (obj1)).get(i);
            }
        }
        return albummodel;
    }

    public boolean isLocalCollectEnable(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean("allow_local_album_collect", true);
    }

    public void removeDataChangedCallback(OnDataChangedCallback ondatachangedcallback)
    {
        if (mCallbacks == null)
        {
            return;
        } else
        {
            mCallbacks.remove(ondatachangedcallback);
            return;
        }
    }

    public void saveAlbumModel(AlbumModel albummodel)
    {
        if (albummodel != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        obj = SharedPreferencesUtil.getInstance(mCon).getString("COLLECT_ALLBUM");
        if (!Utilities.isNotBlank(((String) (obj))))
        {
            break; /* Loop/switch isn't completed */
        }
        obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/album/AlbumModel);
        if (((List) (obj)).contains(albummodel))
        {
            continue; /* Loop/switch isn't completed */
        }
_L4:
        if (((List) (obj)).size() >= maxLength)
        {
            ((List) (obj)).remove(0);
        }
        ((List) (obj)).add(albummodel);
        SharedPreferencesUtil.getInstance(mCon).saveString("COLLECT_ALLBUM", JSON.toJSONString(obj));
        if (mCallbacks != null)
        {
            albummodel = mCallbacks.iterator();
            while (albummodel.hasNext()) 
            {
                ((OnDataChangedCallback)albummodel.next()).onDataChanged();
            }
        }
        if (true) goto _L1; else goto _L3
_L3:
        obj = new ArrayList();
          goto _L4
        if (true) goto _L1; else goto _L5
_L5:
    }

    public boolean updateLocalCollectAlbumLit(AlbumModel albummodel)
    {
        if (albummodel == null)
        {
            throw new NullPointerException();
        }
        Object obj;
        int i;
        obj = SharedPreferencesUtil.getInstance(mCon).getString("COLLECT_ALLBUM");
        if (!Utilities.isNotBlank(((String) (obj))))
        {
            break MISSING_BLOCK_LABEL_94;
        }
        obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/album/AlbumModel);
        i = ((List) (obj)).indexOf(albummodel);
        {
            if (-1 == i)
            {
                break MISSING_BLOCK_LABEL_81;
            }
            try
            {
                ((List) (obj)).set(i, albummodel);
            }
            // Misplaced declaration of an exception variable
            catch (AlbumModel albummodel)
            {
                return false;
            }
            albummodel = ((AlbumModel) (obj));
        }
        SharedPreferencesUtil.getInstance(mCon).saveString("COLLECT_ALLBUM", JSON.toJSONString(albummodel));
        return true;
        ((List) (obj)).add(albummodel);
        albummodel = ((AlbumModel) (obj));
        continue; /* Loop/switch isn't completed */
        obj = new ArrayList();
        ((List) (obj)).add(albummodel);
        albummodel = ((AlbumModel) (obj));
        if (true) goto _L2; else goto _L1
_L1:
        break MISSING_BLOCK_LABEL_114;
_L2:
        break MISSING_BLOCK_LABEL_63;
    }

    private class _cls1
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final AlbumModelManage this$0;
        final Context val$context;
        final View val$view;

        public void onExecute()
        {
            Intent intent = new Intent(context, com/ximalaya/ting/android/activity/login/LoginActivity);
            intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            context.startActivity(intent);
        }

        _cls1()
        {
            this$0 = AlbumModelManage.this;
            context = context1;
            view = view1;
            super();
        }
    }

}
