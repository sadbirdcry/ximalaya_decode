// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import java.util.Timer;

public class MyLocationManager
{
    public static interface LocationCallBack
    {

        public abstract void onCurrentLocation(Location location);
    }


    private static MyLocationManager instance;
    private static Context mContext;
    private final int MININSTANCE = 2;
    private final int MINTIME = 2000;
    private final int TIMEOUT_DURATION = 15000;
    private boolean isSingle;
    private final LocationListener locationListener = new _cls2();
    private LocationCallBack mCallback;
    private Location mLastLocation;
    private LocationManager mLocationManager;

    private MyLocationManager(Context context)
    {
        mLastLocation = null;
        mContext = context;
        try
        {
            mLocationManager = (LocationManager)mContext.getSystemService("location");
            if (mLocationManager != null)
            {
                mLastLocation = mLocationManager.getLastKnownLocation("gps");
                if (mLastLocation == null)
                {
                    mLastLocation = mLocationManager.getLastKnownLocation("network");
                }
            }
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            mLocationManager = null;
        }
    }

    public static MyLocationManager getInstance(Context context)
    {
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/MyLocationManager;
        JVM INSTR monitorenter ;
        instance = new MyLocationManager(context);
        com/ximalaya/ting/android/modelmanage/MyLocationManager;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        context;
        com/ximalaya/ting/android/modelmanage/MyLocationManager;
        JVM INSTR monitorexit ;
        throw context;
    }

    private boolean isZeroLocation(Location location)
    {
        return location != null && location.getLongitude() == 0.0D && location.getLatitude() == 0.0D;
    }

    private void updateLocation(Location location)
    {
        mLastLocation = location;
        if (mCallback != null)
        {
            mCallback.onCurrentLocation(location);
        }
        if (isSingle)
        {
            destoryLocationManager();
        }
    }

    public void destory()
    {
        com/ximalaya/ting/android/modelmanage/MyLocationManager;
        JVM INSTR monitorenter ;
        instance = null;
        com/ximalaya/ting/android/modelmanage/MyLocationManager;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        com/ximalaya/ting/android/modelmanage/MyLocationManager;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void destoryLocationManager()
    {
        if (mLocationManager != null)
        {
            mLocationManager.removeUpdates(locationListener);
        }
        mCallback = null;
    }

    public double getLatitude()
    {
        if (mLastLocation != null)
        {
            return mLastLocation.getLatitude();
        } else
        {
            return 0.0D;
        }
    }

    public double getLongitude()
    {
        if (mLastLocation != null)
        {
            return mLastLocation.getLongitude();
        } else
        {
            return 0.0D;
        }
    }

    public Location getMyLocation()
    {
        return mLastLocation;
    }

    public String getMyLocationStr()
    {
        if (mLastLocation != null && !isZeroLocation(mLastLocation))
        {
            return (new StringBuilder()).append(mLastLocation.getLongitude()).append(",").append(mLastLocation.getLatitude()).toString();
        } else
        {
            return "";
        }
    }

    public void requestUpdateLocation(boolean flag)
    {
        isSingle = flag;
        if (mLocationManager == null)
        {
            mLocationManager = (LocationManager)mContext.getSystemService("location");
        }
        if (mLocationManager == null)
        {
            return;
        }
        if (!mLocationManager.isProviderEnabled("gps")) goto _L2; else goto _L1
_L1:
        mLocationManager.requestLocationUpdates("gps", 2000L, 2.0F, locationListener);
_L4:
        (new Timer()).schedule(new _cls1(), 15000L);
        return;
_L2:
        try
        {
            if (mLocationManager.isProviderEnabled("network"))
            {
                mLocationManager.requestLocationUpdates("network", 2000L, 2.0F, locationListener);
            }
        }
        catch (Exception exception) { }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void setLocationCallBack(LocationCallBack locationcallback)
    {
        mCallback = locationcallback;
    }




/*
    static LocationCallBack access$202(MyLocationManager mylocationmanager, LocationCallBack locationcallback)
    {
        mylocationmanager.mCallback = locationcallback;
        return locationcallback;
    }

*/


    private class _cls2
        implements LocationListener
    {

        final MyLocationManager this$0;

        public void onLocationChanged(Location location)
        {
            updateLocation(location);
        }

        public void onProviderDisabled(String s)
        {
        }

        public void onProviderEnabled(String s)
        {
        }

        public void onStatusChanged(String s, int i, Bundle bundle)
        {
        }

        _cls2()
        {
            this$0 = MyLocationManager.this;
            super();
        }
    }


    private class _cls1 extends TimerTask
    {

        final MyLocationManager this$0;

        public void run()
        {
            if (mLocationManager != null && locationListener != null)
            {
                try
                {
                    mLocationManager.removeUpdates(locationListener);
                }
                catch (Exception exception)
                {
                    Logger.e(exception);
                }
            }
            mCallback = null;
        }

        _cls1()
        {
            this$0 = MyLocationManager.this;
            super();
        }
    }

}
