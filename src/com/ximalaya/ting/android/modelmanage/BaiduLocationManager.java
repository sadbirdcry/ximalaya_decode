// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.modelmanage:
//            BasestationInfo, WifiInfo

public class BaiduLocationManager
{

    private static BaiduLocationManager instance = null;
    private String PROVINCE;
    private Context appContext;
    private String city;
    private String province;

    private BaiduLocationManager()
    {
        PROVINCE = "province_code";
    }

    private void collectPosInfo()
    {
        (new _cls2()).start();
    }

    private List getBasestationInfos()
    {
        ArrayList arraylist = new ArrayList();
        Object obj = (TelephonyManager)appContext.getSystemService("phone");
        Object obj1 = ((TelephonyManager) (obj)).getNetworkOperator();
        if (TextUtils.isEmpty(((CharSequence) (obj1))))
        {
            return null;
        }
        int i = Integer.parseInt(((String) (obj1)).substring(0, 3));
        int j = Integer.parseInt(((String) (obj1)).substring(3));
        obj1 = (GsmCellLocation)((TelephonyManager) (obj)).getCellLocation();
        if (obj1 == null)
        {
            return null;
        }
        int k = ((GsmCellLocation) (obj1)).getLac();
        int l = ((GsmCellLocation) (obj1)).getCid();
        obj1 = new BasestationInfo();
        obj1.cellId = l;
        obj1.lac = k;
        obj1.mcc = (new StringBuilder()).append(i).append("").toString();
        obj1.mnc = (new StringBuilder()).append(j).append("").toString();
        arraylist.add(obj1);
        BasestationInfo basestationinfo;
        for (obj = ((TelephonyManager) (obj)).getNeighboringCellInfo().iterator(); ((Iterator) (obj)).hasNext(); arraylist.add(basestationinfo))
        {
            NeighboringCellInfo neighboringcellinfo = (NeighboringCellInfo)((Iterator) (obj)).next();
            basestationinfo = new BasestationInfo();
            basestationinfo.cellId = neighboringcellinfo.getCid();
            basestationinfo.lac = neighboringcellinfo.getLac();
            basestationinfo.mcc = (new StringBuilder()).append(i).append("").toString();
            basestationinfo.mnc = (new StringBuilder()).append(j).append("").toString();
            basestationinfo.signStrength = neighboringcellinfo.getRssi() * 2 - 113;
        }

        return arraylist;
    }

    public static BaiduLocationManager getInstance()
    {
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/BaiduLocationManager;
        JVM INSTR monitorenter ;
        if (instance == null)
        {
            instance = new BaiduLocationManager();
        }
        com/ximalaya/ting/android/modelmanage/BaiduLocationManager;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        Exception exception;
        exception;
        com/ximalaya/ting/android/modelmanage/BaiduLocationManager;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private List getWifiInfo()
    {
        ArrayList arraylist = new ArrayList();
        Object obj = (WifiManager)appContext.getSystemService("wifi");
        if (((WifiManager) (obj)).isWifiEnabled())
        {
            WifiInfo wifiinfo = ((WifiManager) (obj)).getConnectionInfo();
            com.ximalaya.ting.android.modelmanage.WifiInfo wifiinfo1 = new com.ximalaya.ting.android.modelmanage.WifiInfo();
            wifiinfo1.macAddress = wifiinfo.getMacAddress();
            wifiinfo1.ssid = wifiinfo.getSSID();
            wifiinfo1.signStrength = wifiinfo.getRssi();
            arraylist.add(wifiinfo1);
            com.ximalaya.ting.android.modelmanage.WifiInfo wifiinfo2;
            for (obj = ((WifiManager) (obj)).getScanResults().iterator(); ((Iterator) (obj)).hasNext(); arraylist.add(wifiinfo2))
            {
                ScanResult scanresult = (ScanResult)((Iterator) (obj)).next();
                wifiinfo2 = new com.ximalaya.ting.android.modelmanage.WifiInfo();
                wifiinfo2.macAddress = scanresult.BSSID;
                wifiinfo2.ssid = scanresult.SSID;
                wifiinfo2.signStrength = scanresult.level;
            }

        }
        return arraylist;
    }

    private void requestXimalayaLocation()
    {
        String s = (new StringBuilder()).append(e.ar).append("location").toString();
        RequestParams requestparams = new RequestParams();
        f.a().a(s, requestparams, null, new _cls1());
    }

    public int getSavedProvinceCode()
    {
        return SharedPreferencesUtil.getInstance(appContext).getInt(PROVINCE, 0);
    }

    public void requestLocationInfo(Context context)
    {
        appContext = context;
        requestXimalayaLocation();
    }

    public void saveProvinceCode(int i)
    {
        SharedPreferencesUtil.getInstance(appContext).saveInt(PROVINCE, i);
    }




/*
    static String access$002(BaiduLocationManager baidulocationmanager, String s)
    {
        baidulocationmanager.province = s;
        return s;
    }

*/



/*
    static String access$102(BaiduLocationManager baidulocationmanager, String s)
    {
        baidulocationmanager.city = s;
        return s;
    }

*/





    private class _cls2 extends Thread
    {

        final BaiduLocationManager this$0;

        public void run()
        {
            String s;
            Object obj;
            LoginInfoModel logininfomodel;
            try
            {
                Logger.log("collectPosInfo start");
                s = (new StringBuilder()).append(e.ar).append("collectPosInfo").toString();
                obj = new PosInfo();
                logininfomodel = UserInfoMannage.getInstance().getUser();
            }
            catch (Exception exception)
            {
                Logger.log((new StringBuilder()).append("collectPosInfo exception=").append(exception.getMessage()).toString());
                return;
            }
            if (logininfomodel == null)
            {
                break MISSING_BLOCK_LABEL_54;
            }
            obj.uid = logininfomodel.uid;
            obj.deviceId = ((MyApplication)appContext.getApplicationContext()).i();
            obj.loginProvinceName = province;
            obj.loginCityName = city;
            obj.latitude = MyLocationManager.getInstance(appContext).getLatitude();
            obj.longitude = MyLocationManager.getInstance(appContext).getLongitude();
            obj.basestationList = getBasestationInfos();
            obj.wifiList = getWifiInfo();
            obj = new StringEntity(JSON.toJSONString(obj), "UTF-8");
            f.a().a(appContext, s, null, ((org.apache.http.HttpEntity) (obj)), "application/json");
            return;
        }

        _cls2()
        {
            this$0 = BaiduLocationManager.this;
            super();
        }
    }


    private class _cls1 extends a
    {

        final BaiduLocationManager this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if ((s = JSON.parseObject(s)) == null)
            {
                continue; /* Loop/switch isn't completed */
            }
            int i;
            i = s.getIntValue("provinceCode");
            province = s.getString("provinceName");
            city = s.getString("cityName");
            collectPosInfo();
            Logger.log((new StringBuilder()).append("location ximalaya provinceCode = ").append(i).toString());
            if (i > 0)
            {
                try
                {
                    saveProvinceCode(i);
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    Logger.log((new StringBuilder()).append("location exception: ").append(s.getMessage()).toString());
                }
                return;
            }
            if (true) goto _L1; else goto _L3
_L3:
        }

        _cls1()
        {
            this$0 = BaiduLocationManager.this;
            super();
        }
    }

}
