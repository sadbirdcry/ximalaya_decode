// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.SharedPreferencesUserUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.modelmanage:
//            UserInfoMannage

public class HistoryManage
{
    public static interface HistoryUpdateListener
    {

        public abstract void update();
    }


    private static HistoryManage historyManage;
    private List listenerList;
    private Context mCon;
    private int maxLength;
    private SharedPreferences settings;
    private List tSoundInfoList;

    private HistoryManage(Context context)
    {
        tSoundInfoList = null;
        maxLength = 50;
        listenerList = new ArrayList();
        if (context != null)
        {
            mCon = context.getApplicationContext();
        } else
        {
            mCon = MyApplication.b();
        }
        settings = mCon.getSharedPreferences("history_listener_data", 0);
    }

    public static HistoryManage getInstance(Context context)
    {
        if (historyManage != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/HistoryManage;
        JVM INSTR monitorenter ;
        historyManage = new HistoryManage(context);
        com/ximalaya/ting/android/modelmanage/HistoryManage;
        JVM INSTR monitorexit ;
_L2:
        return historyManage;
        context;
        com/ximalaya/ting/android/modelmanage/HistoryManage;
        JVM INSTR monitorexit ;
        throw context;
    }

    private void saveHistoryRecord()
    {
        Iterator iterator = listenerList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            HistoryUpdateListener historyupdatelistener = (HistoryUpdateListener)iterator.next();
            if (historyupdatelistener != null)
            {
                historyupdatelistener.update();
            }
        } while (true);
        (new Thread(new _cls1())).start();
    }

    public void clearInfoList()
    {
        if (tSoundInfoList != null)
        {
            tSoundInfoList.clear();
            Iterator iterator = listenerList.iterator();
            while (iterator.hasNext()) 
            {
                HistoryUpdateListener historyupdatelistener = (HistoryUpdateListener)iterator.next();
                if (historyupdatelistener != null)
                {
                    historyupdatelistener.update();
                }
            }
        }
    }

    public void deleteAllSound()
    {
        if (tSoundInfoList == null)
        {
            return;
        } else
        {
            tSoundInfoList.clear();
            saveHistoryRecord();
            return;
        }
    }

    public void deleteSound(int i)
    {
        if (tSoundInfoList == null || i >= tSoundInfoList.size())
        {
            return;
        } else
        {
            tSoundInfoList.remove(i);
            saveHistoryRecord();
            return;
        }
    }

    public SoundInfo getHistoryInfo(long l)
    {
        if (tSoundInfoList == null || tSoundInfoList.size() == 0)
        {
            return null;
        }
        for (Iterator iterator = tSoundInfoList.iterator(); iterator.hasNext();)
        {
            SoundInfo soundinfo = (SoundInfo)iterator.next();
            if (soundinfo.trackId == l)
            {
                return soundinfo;
            }
        }

        return null;
    }

    public long getHistoryListenerPosition(long l)
    {
        return getHistoryListenerPosition(String.valueOf(l));
    }

    public long getHistoryListenerPosition(String s)
    {
        return settings.getLong(s, 0L);
    }

    public List getSoundInfoList()
    {
        if (tSoundInfoList == null || tSoundInfoList.size() == 0)
        {
            updateHistoryList();
        }
        return tSoundInfoList;
    }

    public void notifyDataSetChanged()
    {
        Iterator iterator = listenerList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            HistoryUpdateListener historyupdatelistener = (HistoryUpdateListener)iterator.next();
            if (historyupdatelistener != null)
            {
                historyupdatelistener.update();
            }
        } while (true);
    }

    public void putSound(SoundInfo soundinfo)
    {
        if (tSoundInfoList == null || soundinfo == null)
        {
            return;
        }
        Iterator iterator = tSoundInfoList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            SoundInfo soundinfo1 = (SoundInfo)iterator.next();
            if (soundinfo1.trackId == soundinfo.trackId || soundinfo1.albumId == soundinfo.albumId && soundinfo1.albumId > 0L)
            {
                iterator.remove();
            }
        } while (true);
        if (tSoundInfoList.size() >= maxLength)
        {
            tSoundInfoList.remove(tSoundInfoList.size() - 1);
        }
        tSoundInfoList.add(0, soundinfo);
        saveHistoryRecord();
    }

    public void removeHistoryListenerPosition(String s)
    {
        if (settings.contains(String.valueOf(s)))
        {
            SharedPreferencesUtil.apply(settings.edit().remove(s));
        }
    }

    public void removeHistoryListenerPositon(long l)
    {
        removeHistoryListenerPosition(String.valueOf(l));
    }

    public void removeHistoryUpdateListener(HistoryUpdateListener historyupdatelistener)
    {
        listenerList.remove(historyupdatelistener);
    }

    public void saveHistoryListenerPosition(long l, long l1)
    {
        saveHistoryListenerPosition(String.valueOf(l), l1);
    }

    public void saveHistoryListenerPosition(String s, long l)
    {
        SharedPreferencesUtil.apply(settings.edit().putLong(s, l));
    }

    public void setOnHistoryUpdateListener(HistoryUpdateListener historyupdatelistener)
    {
        listenerList.add(historyupdatelistener);
    }

    public void updateHistoryList()
    {
        if (mCon != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        String s = SharedPreferencesUtil.getInstance(mCon).getString("history_listener");
        obj = s;
        if (TextUtils.isEmpty(s))
        {
            obj = s;
            if (UserInfoMannage.hasLogined())
            {
                obj = SharedPreferencesUserUtil.getInstance(mCon, (new StringBuilder()).append(UserInfoMannage.getInstance().getUser().uid).append("").toString()).getString("history_listener");
            }
        }
        if (obj != null && !((String) (obj)).equals(""))
        {
            obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/sound/SoundInfo);
            if (tSoundInfoList != null)
            {
                break; /* Loop/switch isn't completed */
            }
            tSoundInfoList = ((List) (obj));
        }
_L4:
        if (tSoundInfoList == null)
        {
            tSoundInfoList = new ArrayList();
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        tSoundInfoList.clear();
        tSoundInfoList.addAll(((java.util.Collection) (obj)));
        obj = listenerList.iterator();
        while (((Iterator) (obj)).hasNext()) 
        {
            HistoryUpdateListener historyupdatelistener = (HistoryUpdateListener)((Iterator) (obj)).next();
            if (historyupdatelistener != null)
            {
                historyupdatelistener.update();
            }
        }
          goto _L4
        if (true) goto _L1; else goto _L5
_L5:
    }


    private class _cls1
        implements Runnable
    {

        final HistoryManage this$0;
        final String val$historyString;

        public void run()
        {
            SharedPreferencesUtil.getInstance(mCon).saveString("history_listener", historyString);
        }

        _cls1()
        {
            this$0 = HistoryManage.this;
            historyString = s;
            super();
        }
    }

}
