// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.model.AppAd;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.http.entity.StringEntity;

public class AdManager
{
    class PostAdDataTask extends MyAsyncTask
    {

        final AdManager this$0;

        public volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        public transient String doInBackground(Object aobj[])
        {
            String s;
            Context context;
            Object obj;
            obj = (String)aobj[0];
            context = (Context)aobj[1];
            s = (String)aobj[2];
            Logger.log((new StringBuilder()).append("AdManager PostAdDataTask adId=").append(s).append(", data=").append(((String) (obj))).toString());
            aobj = (new StringBuilder()).append(a.Q).append("adrecord/offlineAdRecord?ad=").append(s).toString();
            obj = new StringEntity(((String) (obj)), "UTF-8");
            aobj = f.a().a(context, ((String) (aobj)), null, ((org.apache.http.HttpEntity) (obj)), "application/json");
            if (((com.ximalaya.ting.android.b.n.a) (aobj)).b != 1)
            {
                break MISSING_BLOCK_LABEL_184;
            }
            aobj = ((com.ximalaya.ting.android.b.n.a) (aobj)).a;
_L3:
            if (TextUtils.isEmpty(((CharSequence) (aobj)))) goto _L2; else goto _L1
_L1:
            aobj = JSON.parseObject(((String) (aobj)));
            if (aobj != null)
            {
                try
                {
                    if (((JSONObject) (aobj)).getIntValue("status") == 1)
                    {
                        deleteAdFileById(s);
                    }
                }
                // Misplaced declaration of an exception variable
                catch (Object aobj[])
                {
                    Logger.log((new StringBuilder()).append("AdManager PostAdDataTask exception=").append(((Exception) (aobj)).getMessage()).toString());
                    return null;
                }
            }
_L2:
            return null;
            aobj = null;
              goto _L3
        }

        PostAdDataTask()
        {
            this$0 = AdManager.this;
            super();
        }
    }


    private static AdManager manage;
    private int mCommTopAdIndext;
    private List mCommTopList;

    private AdManager()
    {
        mCommTopAdIndext = 0;
    }

    private void deleteAdFileById(String s)
    {
        s = getAdFilePath(s);
        if (s == null)
        {
            return;
        } else
        {
            s.delete();
            return;
        }
    }

    private File getAdFilePath(String s)
    {
        if (!Environment.getExternalStorageState().equals("mounted"))
        {
            s = null;
        } else
        {
            File file = new File((new StringBuilder()).append("/sdcard/ting/ads/").append(s).toString());
            if (!file.getParentFile().getParentFile().exists())
            {
                file.getParentFile().getParentFile().mkdir();
            }
            s = file;
            if (!file.getParentFile().exists())
            {
                file.getParentFile().mkdir();
                return file;
            }
        }
        return s;
    }

    public static AdManager getInstance()
    {
        if (manage != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/AdManager;
        JVM INSTR monitorenter ;
        manage = new AdManager();
        com/ximalaya/ting/android/modelmanage/AdManager;
        JVM INSTR monitorexit ;
_L2:
        return manage;
        Exception exception;
        exception;
        com/ximalaya/ting/android/modelmanage/AdManager;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private String readAdContentFromAdFile(String s)
    {
        s = getAdFilePath(s);
        StringBuilder stringbuilder;
        s = new BufferedReader(new FileReader(s));
        stringbuilder = new StringBuilder();
_L1:
        String s1 = s.readLine();
label0:
        {
            if (s1 == null)
            {
                break label0;
            }
            try
            {
                stringbuilder.append(s1);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.log((new StringBuilder()).append("readFromAdIdFile exception: ").append(s.getMessage()).toString());
                return null;
            }
        }
          goto _L1
        s.close();
        s = stringbuilder.toString();
        return s;
    }

    private String readFromAdIdFile()
    {
        Object obj = new File("/sdcard/ting/ads/adId");
        StringBuilder stringbuilder;
        obj = new BufferedReader(new FileReader(((File) (obj))));
        stringbuilder = new StringBuilder();
_L1:
        String s = ((BufferedReader) (obj)).readLine();
label0:
        {
            if (s == null)
            {
                break label0;
            }
            try
            {
                stringbuilder.append(s);
            }
            catch (Exception exception)
            {
                Logger.log((new StringBuilder()).append("readFromAdIdFile exception: ").append(exception.getMessage()).toString());
                return null;
            }
        }
          goto _L1
        exception = stringbuilder.toString();
        return exception;
    }

    private void writeToAdIdFile(String s)
    {
        this;
        JVM INSTR monitorenter ;
        Object obj = new File("/sdcard/ting/ads/adId");
        Object obj1 = null;
        obj = new PrintWriter(new FileWriter(((File) (obj)), false));
        ((PrintWriter) (obj)).println(s);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_44;
        }
        ((PrintWriter) (obj)).close();
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
        s;
        obj = obj1;
_L3:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_58;
        }
        ((PrintWriter) (obj)).close();
        throw s;
        s;
        this;
        JVM INSTR monitorexit ;
        throw s;
        s;
        obj = null;
_L4:
        if (obj == null) goto _L2; else goto _L1
_L1:
        ((PrintWriter) (obj)).close();
          goto _L2
        s;
          goto _L3
        s;
          goto _L4
    }

    public void adrecord(AppAd appad, MyApplication myapplication)
    {
        if (myapplication == null || appad == null || appad.getLink() == null)
        {
            return;
        } else
        {
            f.a().a(getRealLink(appad.getLink(), myapplication), null, null, null, false);
            return;
        }
    }

    public void adrecordForShow(AppAd appad, MyApplication myapplication)
    {
        if (myapplication == null || appad == null || appad.getLink() == null)
        {
            return;
        } else
        {
            ThirdAdStatUtil.getInstance().execAfterDecorateUrl(true, appad.getILink(), new _cls1());
            return;
        }
    }

    public String getAdIdFromUrl(String s)
    {
        if (!TextUtils.isEmpty(s) && s.contains("ad")) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        if (!TextUtils.isEmpty(s = s.substring(s.indexOf('?') + 1)))
        {
            s = s.split("&");
            int j = s.length;
            int i = 0;
            while (i < j) 
            {
                String as[] = s[i].split("=");
                if ("ad".equals(as[0]))
                {
                    return as[1];
                }
                i++;
            }
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public String getAdRealJTUrl(String s, AdCollectData adcollectdata)
    {
        String s1;
        if (TextUtils.isEmpty(s))
        {
            s1 = null;
        } else
        {
            int i = s.indexOf("?jt=");
            boolean flag;
            if (i < 0)
            {
                i = s.indexOf("&jt=");
                flag = false;
            } else
            {
                flag = true;
            }
            s1 = s;
            if (i >= 0)
            {
                StringBuilder stringbuilder = new StringBuilder();
                if (flag)
                {
                    stringbuilder.append(s.substring(0, i)).append("?");
                } else
                {
                    stringbuilder.append(s.substring(0, i)).append("&");
                }
                stringbuilder.append("logType=");
                stringbuilder.append(adcollectdata.getLogType());
                stringbuilder.append("&time=");
                stringbuilder.append(adcollectdata.getTime());
                stringbuilder.append("&trackId=");
                stringbuilder.append(adcollectdata.getTrackId());
                stringbuilder.append("&android_id=");
                stringbuilder.append(adcollectdata.getAndroidId());
                stringbuilder.append("&adItemId=");
                stringbuilder.append(adcollectdata.getAdItemId());
                stringbuilder.append("&responseId=");
                stringbuilder.append(adcollectdata.getResponseId());
                stringbuilder.append("&adSource=");
                stringbuilder.append(adcollectdata.getAdSource());
                stringbuilder.append("&positionName=");
                stringbuilder.append(adcollectdata.getPositionName());
                if (flag)
                {
                    stringbuilder.append("&").append(s.substring(i + 1, s.length()));
                } else
                {
                    stringbuilder.append(s.substring(i, s.length()));
                }
                return stringbuilder.toString();
            }
        }
        return s1;
    }

    public AppAd getNextShowCommTopAd()
    {
        if (mCommTopList == null || mCommTopList.size() < 1)
        {
            return null;
        }
        AppAd appad;
        if (mCommTopAdIndext < mCommTopList.size())
        {
            appad = (AppAd)mCommTopList.get(mCommTopAdIndext);
        } else
        {
            mCommTopAdIndext = 0;
            appad = (AppAd)mCommTopList.get(mCommTopAdIndext);
        }
        mCommTopAdIndext = mCommTopAdIndext + 1;
        return appad;
    }

    public String getRealLink(String s, MyApplication myapplication)
    {
        s = new StringBuffer(s);
        s.append("&appid=0&device=android&android_id=");
        try
        {
            s.append(android.provider.Settings.Secure.getString(myapplication.getContentResolver(), "android_id"));
        }
        catch (Exception exception) { }
        myapplication = (TelephonyManager)myapplication.getSystemService("phone");
        if (myapplication != null)
        {
            myapplication = myapplication.getDeviceId();
            s.append("&native_device_id=");
            s.append(myapplication);
        }
        return s.toString();
    }

    public void initCommTopAd(MyApplication myapplication)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("appid", "0");
        requestparams.put("name", "comm_top");
        requestparams.put("version", myapplication.m());
        myapplication = (new StringBuilder()).append(a.Q).append("ting").toString();
        f.a().a(myapplication, requestparams, null, new _cls2(), false);
    }

    public void postOffineAdData(Context context)
    {
        Logger.log("AdManager postOffineAdData");
        Object obj = readFromAdIdFile();
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            return;
        }
        obj = JSON.parseArray(((String) (obj)));
        ArrayList arraylist = new ArrayList();
        int i = 0;
        while (i < ((JSONArray) (obj)).size()) 
        {
            String s = ((JSONArray) (obj)).getString(i);
            String s1 = readAdContentFromAdFile(s);
            Logger.log((new StringBuilder()).append("AdManager postOffineAdData adId=").append(s).append(", result=").append(s1).toString());
            if (!TextUtils.isEmpty(s1))
            {
                (new PostAdDataTask()).myexec(new Object[] {
                    s1, context, s
                });
            } else
            {
                arraylist.add(s);
            }
            i++;
        }
        for (context = arraylist.iterator(); context.hasNext(); ((JSONArray) (obj)).remove((String)context.next())) { }
        writeToAdIdFile(((JSONArray) (obj)).toJSONString());
    }

    public void saveOffineAdShowTime(String s, MyApplication myapplication)
    {
        File file;
        Logger.log((new StringBuilder()).append("AdManager saveOffineAdShowTime adId=").append(s).toString());
        file = getAdFilePath(s);
        if (file != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        StringBuilder stringbuilder;
        obj = new Date();
        obj = (new SimpleDateFormat("yyyyMMddHHmmssSSS")).format(((Date) (obj)));
        String s1 = ((TelephonyManager)myapplication.getSystemService("phone")).getDeviceId();
        myapplication = android.provider.Settings.Secure.getString(myapplication.getContentResolver(), "android_id");
        stringbuilder = new StringBuilder();
        String s2 = readAdContentFromAdFile(s);
        if (s2 != null && s2.contains("appid"))
        {
            stringbuilder.append(",");
        }
        stringbuilder.append("{\"appid\":0");
        stringbuilder.append((new StringBuilder()).append(",\"androidId\":\"").append(myapplication).append("\"").toString());
        stringbuilder.append((new StringBuilder()).append(",\"nativeDeviceId\":\"").append(s1).append("\"").toString());
        stringbuilder.append((new StringBuilder()).append(",\"createtime\":\"").append(((String) (obj))).append("\"}").toString());
        obj = null;
        myapplication = new PrintWriter(new FileWriter(file, true));
        myapplication.println(stringbuilder.toString());
        if (myapplication == null) goto _L4; else goto _L3
_L3:
        myapplication.close();
_L4:
        myapplication = readFromAdIdFile();
        if (TextUtils.isEmpty(myapplication))
        {
            writeToAdIdFile((new StringBuilder()).append("[\"").append(s).append("\"]").toString());
            return;
        }
        continue; /* Loop/switch isn't completed */
        s;
        myapplication = ((MyApplication) (obj));
_L6:
        if (myapplication != null)
        {
            myapplication.close();
        }
        throw s;
        if (myapplication.contains(s)) goto _L1; else goto _L5
_L5:
        myapplication = JSON.parseArray(myapplication);
        myapplication.add(s);
        writeToAdIdFile(myapplication.toJSONString());
        return;
        myapplication;
        myapplication = null;
_L7:
        if (myapplication == null) goto _L4; else goto _L3
        s;
          goto _L6
        Exception exception;
        exception;
          goto _L7
    }


/*
    static List access$002(AdManager admanager, List list)
    {
        admanager.mCommTopList = list;
        return list;
    }

*/


    private class _cls1
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final AdManager this$0;

        public void execute(String s)
        {
            f.a().a(s, null, null, null, false);
        }

        _cls1()
        {
            this$0 = AdManager.this;
            super();
        }
    }


    private class _cls2 extends com.ximalaya.ting.android.b.a
    {

        final AdManager this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if ((s = JSON.parseObject(s)).getIntValue("ret") != 0) goto _L1; else goto _L3
_L3:
            s = s.getString("data");
            mCommTopList = JSON.parseArray(s, com/ximalaya/ting/android/library/model/AppAd);
            return;
            s;
        }

        _cls2()
        {
            this$0 = AdManager.this;
            super();
        }
    }

}
