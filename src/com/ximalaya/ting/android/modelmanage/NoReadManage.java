// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.NoReadModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.modelmanage:
//            UserInfoMannage

public class NoReadManage
{
    public static interface NoReadUpdateListener
    {

        public abstract void update(NoReadModel noreadmodel);
    }

    class UpdateNoReadTask extends MyAsyncTask
    {

        final NoReadManage this$0;

        protected transient NoReadModel doInBackground(Object aobj[])
        {
            if ((Context)aobj[0] == null)
            {
                return null;
            }
            aobj = e.T;
            aobj = f.a().a(((String) (aobj)), null, null, null, false);
            if (((com.ximalaya.ting.android.b.n.a) (aobj)).b == 1)
            {
                aobj = ((com.ximalaya.ting.android.b.n.a) (aobj)).a;
            } else
            {
                aobj = null;
            }
            Logger.log((new StringBuilder()).append("result:").append(((String) (aobj))).toString());
            try
            {
                aobj = (NoReadModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/NoReadModel);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
                aobj = null;
            }
            return ((NoReadModel) (aobj));
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onCancelled()
        {
            super.onCancelled();
        }

        protected void onPostExecute(NoReadModel noreadmodel)
        {
            if (!isCancelled() && noreadmodel != null && noreadmodel.ret == 0) goto _L2; else goto _L1
_L1:
            return;
_L2:
            boolean flag;
label0:
            {
                boolean flag1 = false;
                if (noReadModel != null || noreadmodel.newEventCount <= 0)
                {
                    flag = flag1;
                    if (noReadModel == null)
                    {
                        break label0;
                    }
                    flag = flag1;
                    if (noreadmodel.newEventCount <= noReadModel.newEventCount)
                    {
                        break label0;
                    }
                }
                flag = true;
            }
            noReadModel = noreadmodel;
            noReadModel.newEventCountChanged = flag;
            noreadmodel = listenerList.iterator();
            while (noreadmodel.hasNext()) 
            {
                ((NoReadUpdateListener)noreadmodel.next()).update(noReadModel);
            }
            if (true) goto _L1; else goto _L3
_L3:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((NoReadModel)obj);
        }

        UpdateNoReadTask()
        {
            this$0 = NoReadManage.this;
            super();
        }
    }


    private static NoReadManage manage;
    private List listenerList;
    private UpdateNoReadTask mUpdateNoReadTask;
    private NoReadModel noReadModel;

    private NoReadManage()
    {
        noReadModel = null;
        listenerList = new ArrayList();
    }

    public static NoReadManage getInstance()
    {
        if (manage != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/NoReadManage;
        JVM INSTR monitorenter ;
        manage = new NoReadManage();
        com/ximalaya/ting/android/modelmanage/NoReadManage;
        JVM INSTR monitorexit ;
_L2:
        return manage;
        Exception exception;
        exception;
        com/ximalaya/ting/android/modelmanage/NoReadManage;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public NoReadModel getNoReadModel()
    {
        return noReadModel;
    }

    public void removeNoReadUpdateListenerListener(NoReadUpdateListener noreadupdatelistener)
    {
        listenerList.remove(noreadupdatelistener);
    }

    public void setOnNoReadUpdateListenerListener(NoReadUpdateListener noreadupdatelistener)
    {
        listenerList.add(noreadupdatelistener);
    }

    public void updateNoReadManageFromNet(Context context)
    {
        if (UserInfoMannage.getInstance().getUser() != null)
        {
            if (mUpdateNoReadTask != null && mUpdateNoReadTask.getStatus() != android.os.AsyncTask.Status.FINISHED)
            {
                mUpdateNoReadTask.cancel(true);
                mUpdateNoReadTask = null;
            }
            if (mUpdateNoReadTask == null || mUpdateNoReadTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
            {
                mUpdateNoReadTask = new UpdateNoReadTask();
                mUpdateNoReadTask.myexec(new Object[] {
                    context
                });
                return;
            }
        }
    }

    public void updateNoReadManageManual()
    {
        Iterator iterator = listenerList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            NoReadUpdateListener noreadupdatelistener = (NoReadUpdateListener)iterator.next();
            if (noReadModel != null)
            {
                noreadupdatelistener.update(noReadModel);
            }
        } while (true);
    }



/*
    static NoReadModel access$002(NoReadManage noreadmanage, NoReadModel noreadmodel)
    {
        noreadmanage.noReadModel = noreadmodel;
        return noreadmodel;
    }

*/

}
