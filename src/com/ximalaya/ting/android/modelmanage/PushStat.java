// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class PushStat
{
    private class PushClickStat
        implements Runnable
    {

        final PushStat this$0;

        public void run()
        {
            Object obj;
            obj = new RequestParams();
            ((RequestParams) (obj)).put("msgId", mMsgId);
            HashMap hashmap = new HashMap(1);
            hashmap.put("msgId", mMsgId);
            ((RequestParams) (obj)).put("signature", ToolUtil.genSignature(mContext, hashmap));
            obj = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("pns-portal/push/click").toString(), ((RequestParams) (obj)), null, null, false);
            if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
            {
                break MISSING_BLOCK_LABEL_133;
            }
            if (JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a).getIntValue("ret") == 0)
            {
                Logger.d("PushStat", "stat success");
            }
            return;
            Exception exception;
            exception;
            exception.printStackTrace();
            return;
        }

        private PushClickStat()
        {
            this$0 = PushStat.this;
            super();
        }

    }

    private class PushReceiveStat
        implements Runnable
    {

        private boolean mIsBatch;
        final PushStat this$0;

        public void run()
        {
            Object obj;
            obj = new RequestParams();
            ((RequestParams) (obj)).put("msgId", mMsgId);
            ((RequestParams) (obj)).put("provider", mProvider);
            HashMap hashmap = new HashMap(2);
            hashmap.put("msgId", mMsgId);
            hashmap.put("provider", mProvider);
            ((RequestParams) (obj)).put("signature", ToolUtil.genSignature(mContext, hashmap));
            obj = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("pns-portal/push/receive").toString(), ((RequestParams) (obj)), null, null, false);
            if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
            {
                break MISSING_BLOCK_LABEL_184;
            }
            if (JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a).getIntValue("ret") == 0)
            {
                Logger.d("PushStat", "stat success");
                makeStatSuccess();
            }
            if (!mIsBatch)
            {
                PushStat.statLocalRecords(mContext);
            }
            return;
            Exception exception;
            exception;
            exception.printStackTrace();
            return;
        }

        public PushReceiveStat()
        {
            this$0 = PushStat.this;
            super();
            mIsBatch = false;
        }

        public PushReceiveStat(boolean flag)
        {
            this$0 = PushStat.this;
            super();
            mIsBatch = flag;
        }
    }

    private static class StatModel
    {

        private long bid;
        public String mMsgId;
        public String provider;

        public boolean equals(Object obj)
        {
            while (!(obj instanceof StatModel) || ((StatModel)obj).bid != bid) 
            {
                return false;
            }
            return true;
        }


        private StatModel()
        {
        }

    }


    public static final int CLICK = 1;
    private static final String CLICK_PATH = "pns-portal/push/click";
    public static final int RECEIVE = 2;
    private static final String RECEIVE_PATH = "pns-portal/push/receive";
    private static List records;
    private Context mContext;
    private StatModel mModel;
    private String mMsgId;
    private String mProvider;

    public PushStat(Context context, long l, String s, String s1)
    {
        mContext = context;
        mMsgId = s;
        mProvider = s1;
        mModel = new StatModel();
        mModel.mMsgId = mMsgId;
        mModel.provider = mProvider;
    }

    private static void saveAll(Context context)
    {
        String s = JSON.toJSONString(records);
        SharedPreferencesUtil.getInstance(context).saveString("push_receive_stat_record", s);
    }

    public static void statLocalRecords(Context context)
    {
        if (records == null || records.size() <= 0)
        {
            return;
        }
        ArrayList arraylist = new ArrayList();
        StatModel statmodel;
        for (Iterator iterator = records.iterator(); iterator.hasNext(); arraylist.add(statmodel))
        {
            statmodel = (StatModel)iterator.next();
            (new PushStat(context, statmodel.bid, statmodel.mMsgId, statmodel.provider)).statReceive(true);
        }

        records.removeAll(arraylist);
        saveAll(context);
    }

    public void makeStatSuccess()
    {
        if (records == null)
        {
            Logger.log("PushStat", "error happend");
        } else
        if (records.size() > 0)
        {
            records.remove(mModel);
            saveAll(mContext);
            return;
        }
    }

    public void save()
    {
        records.add(mModel);
        saveAll(mContext);
    }

    public void statClick()
    {
        (new Thread(new PushClickStat())).start();
    }

    public void statReceive()
    {
        (new Thread(new PushReceiveStat(false))).start();
    }

    public void statReceive(boolean flag)
    {
        (new Thread(new PushReceiveStat(flag))).start();
    }

    static 
    {
        Object obj;
        obj = MyApplication.b();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_44;
        }
        records = new ArrayList();
        obj = SharedPreferencesUtil.getInstance(((Context) (obj))).getString("push_receive_stat_record");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            break MISSING_BLOCK_LABEL_44;
        }
        records = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/modelmanage/PushStat$StatModel);
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
    }



}
