// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.category.CategoryMenu;
import com.ximalaya.ting.android.util.FileOption;
import com.ximalaya.ting.android.util.Logger;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.modelmanage:
//            UserInfoMannage

public class FilterManage
{

    private static FilterManage filterManage;
    private boolean hasLoad;
    private List tPersonMenuList;
    private List tSoundMenuList;

    private FilterManage()
    {
        hasLoad = false;
    }

    public static FilterManage getInstance()
    {
        if (filterManage != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/UserInfoMannage;
        JVM INSTR monitorenter ;
        filterManage = new FilterManage();
        com/ximalaya/ting/android/modelmanage/UserInfoMannage;
        JVM INSTR monitorexit ;
_L2:
        return filterManage;
        Exception exception;
        exception;
        com/ximalaya/ting/android/modelmanage/UserInfoMannage;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public List getPersonMenuTag(Context context)
    {
        String s;
label0:
        {
            if (tPersonMenuList != null && tPersonMenuList.size() > 0)
            {
                return tPersonMenuList;
            }
            String s1 = SharedPreferencesUtil.getInstance(context).getString("category_person_menu");
            if (s1 != null)
            {
                s = s1;
                if (!"".equals(s1))
                {
                    break label0;
                }
            }
            s = FileOption.readAssetFileData(context, "data/category_person_menu");
        }
        try
        {
            tPersonMenuList = JSON.parseArray(JSON.parseObject(s).getJSONObject("data").getString("category_list"), com/ximalaya/ting/android/model/category/CategoryMenu);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Logger.log((new StringBuilder()).append(context.getMessage()).append(Logger.getLineInfo()).toString());
        }
        return tPersonMenuList;
    }

    public List getSoundMenuTag(Context context)
    {
        String s;
label0:
        {
            if (tSoundMenuList != null && tSoundMenuList.size() > 0)
            {
                return tSoundMenuList;
            }
            String s1 = SharedPreferencesUtil.getInstance(context).getString("category_tag_menu");
            if (s1 != null)
            {
                s = s1;
                if (!"".equals(s1))
                {
                    break label0;
                }
            }
            s = FileOption.readAssetFileData(context, "data/category_tag_menu");
        }
        try
        {
            tSoundMenuList = JSON.parseArray(JSON.parseObject(s).getJSONObject("data").getString("category_list"), com/ximalaya/ting/android/model/category/CategoryMenu);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Logger.log((new StringBuilder()).append(context.getMessage()).append(Logger.getLineInfo()).toString());
        }
        return tSoundMenuList;
    }

    public void updateMenuTag()
    {
        if (hasLoad)
        {
            return;
        } else
        {
            hasLoad = true;
            (new _cls1()).start();
            return;
        }
    }


/*
    static List access$002(FilterManage filtermanage, List list)
    {
        filtermanage.tSoundMenuList = list;
        return list;
    }

*/


/*
    static List access$102(FilterManage filtermanage, List list)
    {
        filtermanage.tPersonMenuList = list;
        return list;
    }

*/

    private class _cls1 extends Thread
    {

        final FilterManage this$0;

        public void run()
        {
            String s = e.x;
            if (MyApplication.b() != null)
            {
                Object obj = f.a().a(s, null, null, null, false);
                if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1)
                {
                    obj = ((com.ximalaya.ting.android.b.n.a) (obj)).a;
                } else
                {
                    obj = null;
                }
                SharedPreferencesUtil.getInstance(MyApplication.b()).saveString("category_tag_menu", ((String) (obj)));
                Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
                try
                {
                    obj = JSON.parseObject(((String) (obj)));
                    if ("0".equals(((JSONObject) (obj)).get("ret").toString()))
                    {
                        tSoundMenuList = JSON.parseArray(((JSONObject) (obj)).getJSONObject("data").getString("category_list"), com/ximalaya/ting/android/model/category/CategoryMenu);
                    }
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
                }
                obj = new RequestParams();
                ((RequestParams) (obj)).put("type", "user");
                obj = f.a().a(s, ((RequestParams) (obj)), null, null);
                SharedPreferencesUtil.getInstance(MyApplication.b()).saveString("category_person_menu", ((String) (obj)));
                Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
                try
                {
                    obj = JSON.parseObject(((String) (obj)));
                    if ("0".equals(((JSONObject) (obj)).get("ret").toString()))
                    {
                        tPersonMenuList = JSON.parseArray(((JSONObject) (obj)).getJSONObject("data").getString("category_list"), com/ximalaya/ting/android/model/category/CategoryMenu);
                        return;
                    }
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
                    return;
                }
            }
        }

        _cls1()
        {
            this$0 = FilterManage.this;
            super();
        }
    }

}
