// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import dalvik.system.DexClassLoader;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class DexManager
{
    private class LoadTask extends MyAsyncTask
    {

        private TaskWrapper mTaskWrapper;
        final DexManager this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            mTaskWrapper.run();
            return mTaskWrapper.getResult();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            if (mTaskWrapper.mCallback != null)
            {
                mTaskWrapper.mCallback.run();
            }
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        public LoadTask(TaskWrapper taskwrapper)
        {
            this$0 = DexManager.this;
            super();
            mTaskWrapper = taskwrapper;
        }
    }

    private class PrepareDexTask extends MyAsyncTask
    {

        Runnable runnable;
        final DexManager this$0;
        int type;

        protected transient File doInBackground(File afile[])
        {
            prepareDex(afile[0]);
            Logger.d("dex", (new StringBuilder()).append("doInBackground:").append(afile[0].getName()).toString());
            return afile[0];
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((File[])aobj);
        }

        protected void onCancelled()
        {
            super.onCancelled();
        }

        protected void onPostExecute(File file)
        {
            super.onPostExecute(file);
            String s = file.getName();
            if (s.endsWith(".dex") || s.endsWith(".") || s.endsWith(".jar"))
            {
                loadDex(file, runnable, type);
            } else
            if (s.endsWith(".so"))
            {
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((File)obj);
        }

        public PrepareDexTask(Runnable runnable1, int i)
        {
            this$0 = DexManager.this;
            super();
            runnable = runnable1;
            type = i;
        }
    }

    private class TaskWrapper
        implements Runnable
    {

        private Runnable mCallback;
        private List mDexNames;
        private String mResult;
        private int mTaskType;
        final DexManager this$0;

        public boolean equals(Object obj)
        {
            if (obj != null && (obj instanceof TaskWrapper))
            {
                if (((TaskWrapper) (obj = (TaskWrapper)obj)).mDexNames.size() > 0 && ((TaskWrapper) (obj)).mDexNames.size() == mDexNames.size())
                {
                    return mDexNames.equals(((TaskWrapper) (obj)).mDexNames);
                }
            }
            return false;
        }

        public String getResult()
        {
            return mResult;
        }

        public int hashCode()
        {
            Iterator iterator = mDexNames.iterator();
            int i;
            for (i = 1; iterator.hasNext(); i = ((String)iterator.next()).hashCode() + i * 31) { }
            return i;
        }

        public void run()
        {
            String s;
            for (Iterator iterator = mDexNames.iterator(); iterator.hasNext(); copyToInternalPath(s))
            {
                s = (String)iterator.next();
            }

            loadLibraryToClassPath(mDexNames);
            mResult = getMultiDexName(mDexNames);
        }



        public TaskWrapper(List list, int i, Runnable runnable)
        {
            this$0 = DexManager.this;
            super();
            mTaskType = 0;
            mDexNames = list;
            mTaskType = i;
            mCallback = runnable;
        }
    }


    public static final int BUF_SIZE = 8192;
    public static final String DEX_NAME_APPLINK = "AppLinkSDKAndroid-2-3-2_dex.jar";
    public static final String DEX_NAME_Dlna = "dlna_dex.jar";
    public static final String DEX_NAME_Dlna_lib = "dlna_lib_dex.jar";
    public static final String DEX_NAME_MSC = "Msc_dex.jar";
    public static final String DEX_NAME_SDL_LIB = "sdl_android_lib_dex.jar";
    public static final String DEX_NAME_SDL_UTILS = "mxsdlutils_dex.jar";
    public static final String DEX_NAME_Suicheting = "suicheting_dex.jar";
    public static final String DEX_NAME_Suicheting_ibuz = "ibluz-20150401a_dex.jar";
    public static final String DEX_NAME_Suicheting_lib = "bluetoothlibrary-20150627-a_dex.jar";
    public static final String DEX_NAME_Wifi_conn = "wifispeakersetupwizard_dex.jar";
    private static final String TAG = "DexManager";
    public static final int Type_Async = 2;
    public static final int Type_None = 0;
    public static final int Type_Sync = 1;
    private static DexManager sDexManager;
    private String Key_Dlna;
    private final String P_Dlna_Open = "P_Dlna_Open";
    private final String P_Doss_Open = "P_Doss_Open";
    private final String P_QSuicheting_Open = "P_QSuicheting_Open";
    private final String P_Suicheting_Open = "P_Suicheting_Open";
    private final String P_Tingshubao_Open = "P_Tingshubao_Open";
    private final String P_Ximao_Open = "P_Ximao_Open";
    private final String dexInAssert = "dex/";
    private boolean isDlnaFirstLoad;
    private Context mContext;
    private File mDexCachePath;
    private File mInternalDexPath;
    private HashMap mLoadedLibs;
    private SharedPreferencesUtil mSharedPreferencesUtil;

    private DexManager(Context context)
    {
        isDlnaFirstLoad = false;
        mContext = context;
        mSharedPreferencesUtil = SharedPreferencesUtil.getInstance(context);
        mLoadedLibs = new HashMap();
        mDexCachePath = context.getDir("outdex", 0);
        mInternalDexPath = context.getDir("dex", 0);
    }

    private void copyLoad(String s, Runnable runnable, int i)
    {
        File file = new File(mInternalDexPath, s);
        if (!file.exists())
        {
            (new PrepareDexTask(runnable, i)).myexec(new File[] {
                file
            });
            return;
        }
        if (s.endsWith("so"))
        {
            loadSo(file, runnable, i);
            return;
        } else
        {
            loadDex(file, runnable, i);
            return;
        }
    }

    private int copyToInternalPath(String s)
    {
        s = new File(mInternalDexPath, s);
        if (s.exists())
        {
            return 1;
        } else
        {
            return prepareDex(s);
        }
    }

    private void execute(TaskWrapper taskwrapper)
    {
        if (taskwrapper.mTaskType == 2)
        {
            (new LoadTask(taskwrapper)).myexec(new Void[0]);
            return;
        } else
        {
            taskwrapper.run();
            return;
        }
    }

    private File getFileByName(String s)
    {
        return new File(mInternalDexPath, s);
    }

    public static DexManager getInstance(Context context)
    {
        if (sDexManager != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/DexManager;
        JVM INSTR monitorenter ;
        sDexManager = new DexManager(context.getApplicationContext());
        com/ximalaya/ting/android/modelmanage/DexManager;
        JVM INSTR monitorexit ;
_L2:
        return sDexManager;
        context;
        com/ximalaya/ting/android/modelmanage/DexManager;
        JVM INSTR monitorexit ;
        throw context;
    }

    private boolean isNeedLoad(String s)
    {
        return true;
    }

    private void loadDex(File file, Runnable runnable, int i)
    {
        String s = file.getName();
        boolean flag = false;
        if (mLoadedLibs.containsKey(s))
        {
            flag = ((Boolean)mLoadedLibs.get(s)).booleanValue();
        }
        Logger.d("dex", (new StringBuilder()).append("loadDex start:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString());
        Logger.d("dex", (new StringBuilder()).append("loadDex start:").append(file.getName()).append(",isLoaded:").append(flag).toString());
        if (flag)
        {
            runRunnable(runnable, i);
            return;
        }
        if (file != null && file.exists())
        {
            loadDexInternal(file.getAbsolutePath(), runnable, i);
        }
        Logger.d("dex", (new StringBuilder()).append("loadDex end:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString());
    }

    private void loadDexInternal(String s, Runnable runnable, int i)
    {
        Logger.d("DexManager", (new StringBuilder()).append("loadDexInternal dexPath:").append(s).toString());
        ClassLoader classloader = getClass().getClassLoader();
        classloader;
        JVM INSTR monitorenter ;
        DexClassLoader dexclassloader = new DexClassLoader(s, mDexCachePath.getAbsolutePath(), null, classloader.getParent());
        Field field = java/lang/ClassLoader.getDeclaredField("parent");
        field.setAccessible(true);
        field.set(classloader, dexclassloader);
        synchronized (mLoadedLibs)
        {
            mLoadedLibs.put(s, Boolean.valueOf(true));
        }
_L2:
        classloader;
        JVM INSTR monitorexit ;
        runRunnable(runnable, i);
        Logger.d("dex", (new StringBuilder()).append("loadDex end:").append(s).append(",").append(System.currentTimeMillis()).toString());
        return;
        exception1;
        hashmap;
        JVM INSTR monitorexit ;
        try
        {
            throw exception1;
        }
        catch (Exception exception) { }
        finally { }
        if (true) goto _L2; else goto _L1
_L1:
        classloader;
        JVM INSTR monitorexit ;
        throw s;
    }

    private void loadLibraryToClassPath(List list)
    {
        list = getMultiDexName(list);
        if (!isLibraryLoaded(list))
        {
            loadDexInternal(list, null, 0);
        }
    }

    private void loadSo(File file, Runnable runnable, int i)
    {
        try
        {
            Logger.d("dex", (new StringBuilder()).append("loadSo:").append(file.getPath()).toString());
            System.load(file.getPath());
            runRunnable(runnable, i);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (File file)
        {
            file.printStackTrace();
        }
    }

    private int prepareDex(File file)
    {
        Object obj;
        Object obj2;
        obj2 = null;
        Exception exception = null;
        int i = 0;
        Logger.d("dex", (new StringBuilder()).append("prepareDex start:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString());
        Object obj1;
        int j;
        try
        {
            obj1 = new BufferedInputStream(mContext.getAssets().open((new StringBuilder()).append("dex/").append(file.getName()).toString()));
        }
        // Misplaced declaration of an exception variable
        catch (Object obj2)
        {
            obj = null;
            obj1 = exception;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            obj1 = null;
        }
        obj = new BufferedOutputStream(new FileOutputStream(file));
        obj2 = new byte[8192];
_L3:
        j = ((BufferedInputStream) (obj1)).read(((byte []) (obj2)), 0, 8192);
        if (j <= 0) goto _L2; else goto _L1
_L1:
        ((OutputStream) (obj)).write(((byte []) (obj2)), 0, j);
          goto _L3
        obj2;
_L9:
        ((Exception) (obj2)).printStackTrace();
        if (obj != null)
        {
            try
            {
                ((OutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        if (obj1 != null)
        {
            try
            {
                ((BufferedInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        file = (new StringBuilder()).append("prepareDex end:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString();
_L5:
        Logger.e("dex", file);
        return i;
_L2:
        ((OutputStream) (obj)).close();
        ((BufferedInputStream) (obj1)).close();
        i = 1;
        if (obj != null)
        {
            try
            {
                ((OutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        if (obj1 != null)
        {
            try
            {
                ((BufferedInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        file = (new StringBuilder()).append("prepareDex end:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString();
        if (true) goto _L5; else goto _L4
_L4:
        break MISSING_BLOCK_LABEL_89;
_L7:
        if (obj2 != null)
        {
            try
            {
                ((OutputStream) (obj2)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj2)
            {
                ((IOException) (obj2)).printStackTrace();
            }
        }
        if (obj1 != null)
        {
            try
            {
                ((BufferedInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((IOException) (obj1)).printStackTrace();
            }
        }
        Logger.e("dex", (new StringBuilder()).append("prepareDex end:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString());
        throw obj;
        obj;
        continue; /* Loop/switch isn't completed */
        exception;
        obj2 = obj;
        obj = exception;
        continue; /* Loop/switch isn't completed */
        exception;
        obj2 = obj;
        obj = exception;
        if (true) goto _L7; else goto _L6
_L6:
        break MISSING_BLOCK_LABEL_89;
        obj2;
        obj = null;
        if (true) goto _L9; else goto _L8
_L8:
    }

    private void prepareDexAndLoad(File file)
    {
        if (!file.exists())
        {
            prepareDex(file);
        }
    }

    private void runRunnable(Runnable runnable, int i)
    {
        this;
        JVM INSTR monitorenter ;
        if (runnable == null) goto _L2; else goto _L1
_L1:
        i;
        JVM INSTR tableswitch 0 2: default 32
    //                   0 32
    //                   1 35
    //                   2 49;
           goto _L2 _L2 _L3 _L4
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        runnable.run();
          goto _L2
        runnable;
        throw runnable;
_L4:
        (new Thread(runnable)).start();
          goto _L2
    }

    public void clear()
    {
        mLoadedLibs = null;
        sDexManager = null;
    }

    public void copyLoadDex(final List fileNames, final Runnable runnable, final int type)
    {
        (new _cls1()).myexec(new Void[0]);
    }

    public void copyLoadDexAync(String s, Runnable runnable)
    {
        if (!isNeedLoad(s))
        {
            copyLoadDexWithoutLoadAsyn(s, runnable);
            return;
        } else
        {
            copyLoad(s, runnable, 2);
            return;
        }
    }

    public void copyLoadDexNormal(String s)
    {
        if (!isNeedLoad(s))
        {
            copyLoadDexWithoutLoadSync(s, null);
            return;
        } else
        {
            copyLoad(s, null, 0);
            return;
        }
    }

    public void copyLoadDexSync(String s, Runnable runnable)
    {
        if (!isNeedLoad(s))
        {
            copyLoadDexWithoutLoadSync(s, runnable);
            return;
        } else
        {
            copyLoad(s, runnable, 1);
            return;
        }
    }

    public void copyLoadDexWithoutLoadAsyn(String s, Runnable runnable)
    {
        if (runnable != null)
        {
            (new Thread(runnable)).start();
        }
    }

    public void copyLoadDexWithoutLoadSync(String s, Runnable runnable)
    {
        if (runnable != null)
        {
            runnable.run();
        }
    }

    public String getMultiDexName(List list)
    {
        StringBuilder stringbuilder = new StringBuilder();
        int j = list.size();
        for (int i = 0; i < j; i++)
        {
            String s = (String)list.get(i);
            stringbuilder.append(mInternalDexPath).append(File.separator).append(s);
            if (i < j - 1)
            {
                stringbuilder.append(File.pathSeparator);
            }
        }

        return stringbuilder.toString();
    }

    public boolean isAppLinkOpen()
    {
        return true;
    }

    public boolean isDlnaInit()
    {
        boolean flag = false;
        synchronized (mLoadedLibs)
        {
            if (mLoadedLibs.containsKey(Key_Dlna))
            {
                flag = ((Boolean)mLoadedLibs.get(Key_Dlna)).booleanValue();
            }
        }
        return flag;
        exception;
        hashmap;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean isDlnaOpen()
    {
        return mSharedPreferencesUtil.getBoolean("P_Dlna_Open");
    }

    public boolean isDossOpen()
    {
        return mSharedPreferencesUtil.getBoolean("P_Doss_Open");
    }

    public boolean isLibraryLoaded(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return false;
        }
        Object obj = new ArrayList();
        boolean flag;
        if (s.contains(File.pathSeparator))
        {
            ((List) (obj)).addAll(Arrays.asList(s.split(File.pathSeparator)));
        } else
        {
            ((List) (obj)).add(s);
        }
        obj = getMultiDexName(((List) (obj)));
        s = mLoadedLibs;
        s;
        JVM INSTR monitorenter ;
        if (!mLoadedLibs.containsKey(obj))
        {
            break MISSING_BLOCK_LABEL_103;
        }
        flag = ((Boolean)mLoadedLibs.get(obj)).booleanValue();
        return flag;
        obj;
        s;
        JVM INSTR monitorexit ;
        throw obj;
        s;
        JVM INSTR monitorexit ;
        return false;
    }

    public boolean isNeedSysExit()
    {
        return isDlnaFirstLoad;
    }

    public boolean isQSuichetingOpen()
    {
        return mSharedPreferencesUtil.getBoolean("P_QSuicheting_Open");
    }

    public boolean isSdlOpen()
    {
        return true;
    }

    public boolean isSuichetingInit()
    {
        boolean flag = isLibraryLoaded("suicheting_dex.jar");
        Logger.d("DexManager", (new StringBuilder()).append("isSuichetingInit:").append(flag).toString());
        return flag;
    }

    public boolean isSuichetingOpen()
    {
        return mSharedPreferencesUtil.getBoolean("P_Suicheting_Open");
    }

    public boolean isTingshubaoOpen()
    {
        return mSharedPreferencesUtil.getBoolean("P_Tingshubao_Open");
    }

    public boolean isXimaoOpen()
    {
        return mSharedPreferencesUtil.getBoolean("P_Ximao_Open");
    }

    public void loadDexLibraryAsync(String s, Runnable runnable)
    {
        execute(new TaskWrapper(Arrays.asList(new String[] {
            s
        }), 2, runnable));
    }

    public void loadDexLibraryAsync(List list, Runnable runnable)
    {
        execute(new TaskWrapper(list, 2, runnable));
    }

    public void loadDexLibrarySync(String s)
    {
        execute(new TaskWrapper(Arrays.asList(new String[] {
            s
        }), 1, null));
    }

    public void loadDexLibrarySync(List list)
    {
        execute(new TaskWrapper(list, 1, null));
    }

    public void loadDlna(Runnable runnable, int i)
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add("wifispeakersetupwizard_dex.jar");
        arraylist.add("dlna_lib_dex.jar");
        arraylist.add("dlna_dex.jar");
        Key_Dlna = getMultiDexName(arraylist);
        sDexManager.copyLoadDex(arraylist, runnable, 2);
    }

    public void loadSuicheting(Runnable runnable)
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add("suicheting_dex.jar");
        loadDexLibraryAsync(arraylist, runnable);
    }

    public void setDossOpen(boolean flag)
    {
        mSharedPreferencesUtil.saveBoolean("P_Doss_Open", flag);
        if (flag)
        {
            setIsDlnaOpen(true);
        }
    }

    public void setIsDlnaOpen(boolean flag)
    {
        mSharedPreferencesUtil.saveBoolean("P_Dlna_Open", flag);
    }

    public void setIsQSuichetingOpen(boolean flag)
    {
        mSharedPreferencesUtil.saveBoolean("P_QSuicheting_Open", flag);
    }

    public void setIsSuichetingOpen(boolean flag)
    {
        mSharedPreferencesUtil.saveBoolean("P_Suicheting_Open", flag);
    }

    public void setIsTingshubaoOpen(boolean flag)
    {
        mSharedPreferencesUtil.saveBoolean("P_Tingshubao_Open", flag);
        if (flag)
        {
            setIsDlnaOpen(true);
        }
    }

    public void setIsXimaoOpen(boolean flag)
    {
        mSharedPreferencesUtil.saveBoolean("P_Ximao_Open", flag);
    }

    public void setLoadFalse(String s)
    {
        if (mLoadedLibs.containsKey(s))
        {
            mLoadedLibs.put(s, Boolean.valueOf(false));
        }
    }

    public void setSysExit(boolean flag)
    {
        isDlnaFirstLoad = flag;
    }








    private class _cls1 extends MyAsyncTask
    {

        final DexManager this$0;
        final List val$fileNames;
        final Runnable val$runnable;
        final int val$type;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            for (int i = 0; i < fileNames.size(); i++)
            {
                prepareDexAndLoad(getFileByName((String)fileNames.get(i)));
            }

            return null;
        }

        protected void onCancelled()
        {
            super.onCancelled();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            super.onPostExecute(void1);
            loadDexInternal(getMultiDexName(fileNames), runnable, type);
        }

        _cls1()
        {
            this$0 = DexManager.this;
            fileNames = list;
            runnable = runnable1;
            type = i;
            super();
        }
    }

}
