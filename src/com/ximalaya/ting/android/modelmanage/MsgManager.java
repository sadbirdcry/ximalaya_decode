// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.service.push.PushModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.List;

public class MsgManager
{

    private static final int COUNT_SVAE_IDS = 3;
    public static final String P_MSG_GOT_IDS = "P_MSG_GOT_IDS";
    private static final String TAG = "MsgManager";
    private static MsgManager manage;
    private List mMsgIds;
    private SharedPreferencesUtil mSharedPreferencesUtil;

    private MsgManager(Context context)
    {
        mSharedPreferencesUtil = SharedPreferencesUtil.getInstance(context);
        initMsgs();
    }

    public static MsgManager getInstance(Context context)
    {
        if (manage != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/MsgManager;
        JVM INSTR monitorenter ;
        manage = new MsgManager(context.getApplicationContext());
        com/ximalaya/ting/android/modelmanage/MsgManager;
        JVM INSTR monitorexit ;
_L2:
        return manage;
        context;
        com/ximalaya/ting/android/modelmanage/MsgManager;
        JVM INSTR monitorexit ;
        throw context;
    }

    private void initMsgs()
    {
        String s = mSharedPreferencesUtil.getString("P_MSG_GOT_IDS");
        Logger.d("MsgManager", (new StringBuilder()).append("initMsgs:msgId:").append(s).toString());
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        try
        {
            mMsgIds = JSON.parseArray(s, java/lang/Long);
            return;
        }
        catch (Exception exception)
        {
            mMsgIds = null;
        }
    }

    private void saveIds()
    {
        if (mMsgIds != null && mMsgIds.size() != 0)
        {
            String s;
            try
            {
                s = JSON.toJSONString(mMsgIds);
            }
            catch (Exception exception)
            {
                exception = null;
            }
            if (!TextUtils.isEmpty(s))
            {
                Logger.d("MsgManager", (new StringBuilder()).append("saveIds:msgId:").append(s).toString());
                mSharedPreferencesUtil.saveString("P_MSG_GOT_IDS", s);
                return;
            }
        }
    }

    public void addId(long l)
    {
        if (mMsgIds == null)
        {
            mMsgIds = new ArrayList();
        }
        if (mMsgIds.size() > 3)
        {
            mMsgIds.remove(0);
            mMsgIds.add(Long.valueOf(l));
        } else
        {
            mMsgIds.add(Long.valueOf(l));
        }
        saveIds();
    }

    public boolean isShowed(long l)
    {
        Logger.d("MsgManager", (new StringBuilder()).append("isShowed:id:").append(l).toString());
        if (mMsgIds == null || mMsgIds.size() == 0)
        {
            return false;
        } else
        {
            return mMsgIds.contains(Long.valueOf(l));
        }
    }

    public void sendCallBackMsg(Context context, PushModel pushmodel)
    {
        String s;
        RequestParams requestparams;
        ToolUtil.onEvent(context.getApplicationContext(), "PUSH_CLICK");
        s = e.ab;
        requestparams = new RequestParams();
        requestparams.put("msgid", pushmodel.bid);
        requestparams.put("deviceToken", ((MyApplication)context.getApplicationContext()).i());
        if (pushmodel.nType != 1 && pushmodel.nType != 3) goto _L2; else goto _L1
_L1:
        requestparams.put("msgtype", 0);
_L6:
        f.a().b(s, requestparams, null, new _cls1());
_L4:
        return;
_L2:
        if (pushmodel.nType != 2) goto _L4; else goto _L3
_L3:
        requestparams.put("msgtype", pushmodel.messageType);
        if (true) goto _L6; else goto _L5
_L5:
    }

    private class _cls1 extends a
    {

        final MsgManager this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            Logger.d("MsgManager", "Call back success!");
        }

        _cls1()
        {
            this$0 = MsgManager.this;
            super();
        }
    }

}
