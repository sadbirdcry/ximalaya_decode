// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;

public interface ILoginStatusChangeListener
{

    public abstract void onLogin(LoginInfoModel logininfomodel);

    public abstract void onLogout();

    public abstract void onUserChange(LoginInfoModel logininfomodel, LoginInfoModel logininfomodel1);
}
