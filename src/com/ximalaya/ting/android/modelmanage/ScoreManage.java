// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

// Referenced classes of package com.ximalaya.ting.android.modelmanage:
//            UserInfoMannage

public class ScoreManage
{
    private class ScoreAddControlMode
    {

        int requestCount;
        final ScoreManage this$0;
        int totalCount;

        private ScoreAddControlMode()
        {
            this$0 = ScoreManage.this;
            super();
        }

        ScoreAddControlMode(_cls1 _pcls1)
        {
            this();
        }
    }


    public static final int BEHAVIOR_DOWNLOAD_ALBUM = 1;
    public static final int BEHAVIOR_DOWNLOAD_WEIXIN_GROUP_INVITE = 7;
    public static final int BEHAVIOR_MSG_SHARE = 12;
    public static final int BEHAVIOR_QQ_FRIENT_SHARE = 10;
    public static final int BEHAVIOR_QZONE_SHARE = 3;
    public static final int BEHAVIOR_RENREN_SHARE = 11;
    public static final int BEHAVIOR_SINA_WEIBO_SHARE = 2;
    public static final int BEHAVIOR_T_WEIBO_SHARE = 9;
    public static final int BEHAVIOR_WEIXIN_FRIEND_INVITE = 4;
    public static final int BEHAVIOR_WEIXIN_FRIEND_SHARE = 8;
    public static final int BEHAVIOR_WEIXIN_GROUP_INVITE = 100;
    public static final int BEHAVIOR_WEIXIN_GROUP_SHARE = 1;
    public static final String P_INVITE_WX = "invite_wx";
    public static final String P_SHARE_QZONE = "share_qzone";
    public static final String P_SHARE_SINA_WB = "share_sina_wb";
    public static final String P_SHARE_WX_GROUP = "share_wx_group";
    public static final String TAG = com/ximalaya/ting/android/modelmanage/ScoreManage.getSimpleName();
    public static final String URL_BEHAVIOR_DEDUCT = "mobile/api1/point/query/deduct/rest";
    public static final String URL_BEHAVIOR_MULTI_DEDUCT = "mobile/api1/point/query/multi/earn/rest";
    public static final String URL_GET_BEHAVIOR_SCORE = "mobile/api1/point/config";
    public static final String URL_GET_SCORE_HOST = "mobile/api1/point/query";
    public static final String URL_GET_TOKEN = "mobile/api1/point/earn";
    private static UserInfoMannage mUserInfoMannage;
    private static ScoreManage scoreManage = null;
    public boolean isGetMyScore;
    public boolean isGetScoreConfig;
    private HashMap mBehaviorPointConfig;
    private HashMap mBehaviorTotalTimes;
    private int mCircylCount;
    private Context mContext;
    private SharedPreferencesUtil mSharedPreferencesUtil;
    private int score;
    private String token;
    private int tryGetMyScore;
    private int tryGetScoreConfig;
    private long uid;

    private ScoreManage(Context context)
    {
        score = -1;
        tryGetScoreConfig = 0;
        tryGetMyScore = 0;
        isGetScoreConfig = false;
        isGetMyScore = false;
        mCircylCount = 3;
        mContext = context.getApplicationContext();
        uid = mUserInfoMannage.getUser().uid;
        token = mUserInfoMannage.getUser().token;
        mSharedPreferencesUtil = SharedPreferencesUtil.getInstance(mContext);
    }

    private void addScore(final int behavior, final ScoreAddControlMode scoreControlM)
    {
        while (uid <= 0L || !UserInfoMannage.hasLogined()) 
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("behavior", behavior);
        f.a().a("mobile/api1/point/earn", requestparams, null, new _cls3());
    }

    public static ScoreManage getInstance(Context context)
    {
        mUserInfoMannage = UserInfoMannage.getInstance();
        if (mUserInfoMannage.getUser() == null)
        {
            scoreManage = null;
            return null;
        }
        if (scoreManage != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/ScoreManage;
        JVM INSTR monitorenter ;
        scoreManage = new ScoreManage(context);
        com/ximalaya/ting/android/modelmanage/ScoreManage;
        JVM INSTR monitorexit ;
_L2:
        return scoreManage;
        context;
        com/ximalaya/ting/android/modelmanage/ScoreManage;
        JVM INSTR monitorexit ;
        throw context;
    }

    private String getSignature(int i, String s)
    {
        Object obj;
        try
        {
            obj = mContext.getPackageManager().getApplicationInfo(mContext.getPackageName(), 128).metaData.getString("UMENG_CHANNEL");
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            Logger.d(TAG, "No channel id.");
            obj = null;
        }
        while (obj == null || ((String) (obj)).equals("") || uid <= 0L) 
        {
            return null;
        }
        StringBuilder stringbuilder = new StringBuilder();
        stringbuilder.append((new StringBuilder()).append("antiToken=").append(s).toString());
        stringbuilder.append((new StringBuilder()).append("&behavior=").append(i).toString());
        stringbuilder.append((new StringBuilder()).append("&channel=").append(((String) (obj))).toString());
        stringbuilder.append((new StringBuilder()).append("&device=").append(((MyApplication)(MyApplication)mContext.getApplicationContext()).h()).toString());
        stringbuilder.append((new StringBuilder()).append("&deviceId=").append(((MyApplication)(MyApplication)mContext.getApplicationContext()).i()).toString());
        stringbuilder.append((new StringBuilder()).append("&impl=").append(mContext.getPackageName()).toString());
        stringbuilder.append((new StringBuilder()).append("&token=").append(token).toString());
        stringbuilder.append((new StringBuilder()).append("&uid=").append(uid).toString());
        stringbuilder.append((new StringBuilder()).append("&version=").append(((MyApplication)(MyApplication)mContext.getApplicationContext()).m()).toString());
        stringbuilder.append("&e1996c7d6e0ff0664b28af93a2eeff8f8cae84b2402d158f7bb115b735a1663d");
        Logger.d(TAG, (new StringBuilder()).append("to md5 string is:").append(stringbuilder).toString());
        s = ToolUtil.md5(stringbuilder.toString().toLowerCase());
        Logger.d(TAG, (new StringBuilder()).append("result is:").append(s).toString());
        return s;
    }

    private void postScoreMain(int i, String s)
    {
        ScoreAddControlMode scoreaddcontrolmode = new ScoreAddControlMode(null);
        scoreaddcontrolmode.totalCount = 3;
        scoreaddcontrolmode.requestCount = 0;
        postScore(i, s, scoreaddcontrolmode);
    }

    public void addScoreMain(int i)
    {
        if (uid > 0L && UserInfoMannage.hasLogined())
        {
            ScoreAddControlMode scoreaddcontrolmode = new ScoreAddControlMode(null);
            scoreaddcontrolmode.totalCount = 3;
            scoreaddcontrolmode.requestCount = 0;
            if (scoreaddcontrolmode.requestCount < scoreaddcontrolmode.totalCount)
            {
                addScore(i, scoreaddcontrolmode);
                return;
            }
        }
    }

    public int getObtainScoreTotalTimes(int i)
    {
        if (!isGetScoreConfig)
        {
            initBehaviorScore();
        }
        if (mBehaviorPointConfig == null || mBehaviorTotalTimes == null)
        {
            initBehaviorScore();
            return -1;
        }
        try
        {
            i = ((Integer)mBehaviorTotalTimes.get(new Integer(i))).intValue();
        }
        catch (Exception exception)
        {
            initBehaviorScore();
            return -1;
        }
        return i;
    }

    public int getScore()
    {
        return score;
    }

    public int getShareScoreConfig(int i)
    {
        if (!isGetScoreConfig)
        {
            initBehaviorScore();
        }
        if (mBehaviorPointConfig == null || mBehaviorTotalTimes == null)
        {
            initBehaviorScore();
            return -1;
        }
        try
        {
            i = ((Integer)mBehaviorPointConfig.get(new Integer(i))).intValue();
        }
        catch (Exception exception)
        {
            initBehaviorScore();
            return -1;
        }
        return i;
    }

    public void initBehaviorScore()
    {
        if (isGetScoreConfig)
        {
            return;
        } else
        {
            mBehaviorPointConfig = new HashMap();
            mBehaviorTotalTimes = new HashMap();
            f.a().a("mobile/api1/point/config", new RequestParams(), null, new _cls1());
            return;
        }
    }

    public boolean isHasShareToday(String s)
    {
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        if (mSharedPreferencesUtil.getLong(s) >= 0L)
        {
            calendar1.setTimeInMillis(mSharedPreferencesUtil.getLong(s));
            if (calendar.get(1) == calendar1.get(1) && calendar.get(2) == calendar1.get(2) && calendar.get(5) == calendar1.get(5))
            {
                return true;
            }
        }
        return false;
    }

    public void onShareFinish(final int behavior, final ScoreAddControlMode scoreControlM)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("behaviors", behavior);
        f.a().a("mobile/api1/point/query/multi/earn/rest", requestparams, null, new _cls5());
    }

    public void onShareFinishMain(int i)
    {
        ScoreAddControlMode scoreaddcontrolmode = new ScoreAddControlMode(null);
        scoreaddcontrolmode.totalCount = 3;
        scoreaddcontrolmode.requestCount = 0;
        onShareFinish(i, scoreaddcontrolmode);
    }

    public void postScore(final int behavior, final String token, final ScoreAddControlMode scoreControlM)
    {
        String s = getSignature(behavior, token);
        if (s == null || s.equals(""))
        {
            return;
        } else
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("behavior", behavior);
            requestparams.put("signature", s);
            requestparams.put("antiToken", token);
            f.a().b("mobile/api1/point/earn", requestparams, null, new _cls4());
            return;
        }
    }

    public void saveShareTime(String s)
    {
        mSharedPreferencesUtil.saveLong(s, (new Date()).getTime());
    }

    public void setScore(int i)
    {
        score = i;
    }

    public void showToast(String s)
    {
        if (mContext != null)
        {
            Toast.makeText(mContext, s, 0).show();
        }
    }

    public void showToastTest(String s)
    {
    }

    public void updateScore()
    {
        f.a().a("mobile/api1/point/query", new RequestParams(), null, new _cls2());
    }







/*
    static int access$308(ScoreManage scoremanage)
    {
        int i = scoremanage.tryGetScoreConfig;
        scoremanage.tryGetScoreConfig = i + 1;
        return i;
    }

*/



/*
    static int access$408(ScoreManage scoremanage)
    {
        int i = scoremanage.tryGetMyScore;
        scoremanage.tryGetMyScore = i + 1;
        return i;
    }

*/



    private class _cls3 extends a
    {

        boolean isSuccess;
        final ScoreManage this$0;
        final int val$behavior;
        final ScoreAddControlMode val$scoreControlM;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onFinish()
        {
            super.onFinish();
            if (!isSuccess && scoreControlM.requestCount < scoreControlM.totalCount)
            {
                ScoreAddControlMode scoreaddcontrolmode = scoreControlM;
                scoreaddcontrolmode.requestCount = scoreaddcontrolmode.requestCount + 1;
                addScore(behavior, scoreControlM);
            }
        }

        public void onNetError(int i, String s)
        {
            Logger.d(ScoreManage.TAG, (new StringBuilder()).append("\u7F51\u7EDC\u9519\u8BEF:").append(s).toString());
        }

        public void onSuccess(String s)
        {
            String s1;
            String s2;
            boolean flag;
            Logger.d(ScoreManage.TAG, (new StringBuilder()).append("the success response is:").append(s).toString());
            s2 = "";
            flag = false;
            s1 = s2;
            JSONObject jsonobject = JSONObject.parseObject(s);
            int i;
            i = ((flag) ? 1 : 0);
            s = s2;
            s1 = s2;
            if (jsonobject.getIntValue("ret") != 0)
            {
                break MISSING_BLOCK_LABEL_85;
            }
            s1 = s2;
            isSuccess = true;
            s1 = s2;
            s = jsonobject.getString("token");
            s1 = s;
            try
            {
                i = jsonobject.getIntValue("rest");
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e(ScoreManage.TAG, (new StringBuilder()).append("json\u8F6C\u5316\u9519\u8BEF:").append(s.getMessage()).toString());
                s = s1;
                i = ((flag) ? 1 : 0);
            }
            if (i >= 0)
            {
                postScoreMain(behavior, s);
                return;
            } else
            {
                showToast("\u4EB2\uFF0C\u60A8\u7684\u5206\u4EAB\u83B7\u5F97\u79EF\u5206\u7684\u6B21\u6570\u6728\u6709\u4E86~");
                return;
            }
        }

        _cls3()
        {
            this$0 = ScoreManage.this;
            behavior = i;
            scoreControlM = scoreaddcontrolmode;
            super();
            isSuccess = false;
        }
    }


    private class _cls1 extends a
    {

        final ScoreManage this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
            Logger.d(ScoreManage.TAG, (new StringBuilder()).append("initBehaviorScore:the error response is:").append(s).toString());
            if (ScoreManage.scoreManage != null)
            {
                int i = ((get) (this)).get;
                if (tryGetScoreConfig < 3 && !isGetScoreConfig)
                {
                    initBehaviorScore();
                    return;
                }
            }
        }

        public void onSuccess(String s)
        {
            if (ScoreManage.scoreManage != null) goto _L2; else goto _L1
_L1:
            return;
_L2:
            Logger.d(ScoreManage.TAG, (new StringBuilder()).append("initBehaviorScore:the success response is:").append(s).toString());
            s = JSONObject.parseObject(s);
            if (s.getIntValue("ret") != 0) goto _L1; else goto _L3
_L3:
            s = JSONObject.parseArray(s.getString("data"), com/ximalaya/ting/android/model/score/ScoreConfig);
            if (s == null) goto _L1; else goto _L4
_L4:
            int i = 0;
            do
            {
                try
                {
                    if (i >= s.size())
                    {
                        break;
                    }
                    mBehaviorPointConfig.put(Integer.valueOf(((ScoreConfig)s.get(i)).behavior), Integer.valueOf(((ScoreConfig)s.get(i)).point));
                    mBehaviorTotalTimes.put(Integer.valueOf(((ScoreConfig)s.get(i)).behavior), Integer.valueOf(((ScoreConfig)s.get(i)).operatePerDay));
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    return;
                }
                i++;
            } while (true);
            isGetScoreConfig = true;
            return;
        }

        _cls1()
        {
            this$0 = ScoreManage.this;
            super();
        }
    }


    private class _cls5 extends a
    {

        boolean isSuccess;
        final ScoreManage this$0;
        final int val$behavior;
        final ScoreAddControlMode val$scoreControlM;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onFinish()
        {
            super.onFinish();
            if (!isSuccess && scoreControlM.requestCount < scoreControlM.totalCount)
            {
                ScoreAddControlMode scoreaddcontrolmode = scoreControlM;
                scoreaddcontrolmode.requestCount = scoreaddcontrolmode.requestCount + 1;
                onShareFinish(behavior, scoreControlM);
            }
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u65E0\u6CD5\u83B7\u53D6\u60A8\u7684\u79EF\u5206\u4FE1\u606F");
        }

        public void onSuccess(String s)
        {
            Logger.d(ScoreManage.TAG, (new StringBuilder()).append("\u83B7\u53D6\u79EF\u5206\u4FE1\u606F\u7B49\uFF1A").append(s).toString());
            s = JSONObject.parseObject(s);
            if (s.getIntValue("ret") == 0)
            {
                isSuccess = true;
                if (s.getIntValue((new StringBuilder()).append(behavior).append("").toString()) > 0)
                {
                    addScoreMain(behavior);
                    return;
                }
                break MISSING_BLOCK_LABEL_100;
            }
            try
            {
                showToast(s.getString("msg"));
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s) { }
        }

        _cls5()
        {
            this$0 = ScoreManage.this;
            behavior = i;
            scoreControlM = scoreaddcontrolmode;
            super();
            isSuccess = false;
        }
    }


    private class _cls4 extends a
    {

        boolean isSuccess;
        final ScoreManage this$0;
        final int val$behavior;
        final ScoreAddControlMode val$scoreControlM;
        final String val$token;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onFinish()
        {
            super.onFinish();
            if (!isSuccess && scoreControlM.requestCount < scoreControlM.totalCount)
            {
                ScoreAddControlMode scoreaddcontrolmode = scoreControlM;
                scoreaddcontrolmode.requestCount = scoreaddcontrolmode.requestCount + 1;
                postScore(behavior, token, scoreControlM);
            }
        }

        public void onNetError(int i, String s)
        {
            Logger.d(ScoreManage.TAG, (new StringBuilder()).append("\u7F51\u7EDC\u9519\u8BEF:").append(s).toString());
        }

        public void onSuccess(String s)
        {
            Logger.d(ScoreManage.TAG, (new StringBuilder()).append("the success response is:").append(s).toString());
            s = JSONObject.parseObject(s);
            if (s.getIntValue("ret") != 0) goto _L2; else goto _L1
_L1:
            int i = s.getIntValue("point");
            isSuccess = true;
            showToast((new StringBuilder()).append("\u83B7\u5F97\u79EF\u5206\uFF1A").append(i).toString());
            updateScore();
            behavior;
            JVM INSTR lookupswitch 5: default 220
        //                       1: 140
        //                       2: 190
        //                       3: 180
        //                       4: 200
        //                       100: 210;
               goto _L2 _L3 _L4 _L5 _L6 _L7
_L3:
            saveShareTime("share_wx_group");
            return;
_L5:
            try
            {
                saveShareTime("share_qzone");
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e(ScoreManage.TAG, (new StringBuilder()).append("json\u8F6C\u5316\u9519\u8BEF:").append(s.getMessage()).toString());
                return;
            }
_L4:
            saveShareTime("share_sina_wb");
            return;
_L6:
            saveShareTime("invite_wx");
            return;
_L7:
            saveShareTime("invite_wx");
            return;
_L2:
        }

        _cls4()
        {
            this$0 = ScoreManage.this;
            behavior = i;
            scoreControlM = scoreaddcontrolmode;
            token = s;
            super();
            isSuccess = false;
        }
    }


}
