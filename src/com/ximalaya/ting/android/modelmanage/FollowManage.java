// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.modelmanage;

import android.content.Context;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.follow.FollowGroup;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.List;

public class FollowManage
{
    public static interface FollowGroupUpdateListener
    {

        public abstract void update(List list);
    }


    private static FollowManage manage;
    public boolean isUpdate;
    private List listenerList;
    private List tFollowGroupList;

    private FollowManage()
    {
        isUpdate = false;
        listenerList = new ArrayList();
    }

    public static FollowManage getInstance()
    {
        if (manage != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/modelmanage/FollowManage;
        JVM INSTR monitorenter ;
        manage = new FollowManage();
        com/ximalaya/ting/android/modelmanage/FollowManage;
        JVM INSTR monitorexit ;
_L2:
        return manage;
        Exception exception;
        exception;
        com/ximalaya/ting/android/modelmanage/FollowManage;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public List getFollowGroupList(Context context)
    {
        if (tFollowGroupList != null && tFollowGroupList.size() > 0)
        {
            return tFollowGroupList;
        }
        context = SharedPreferencesUtil.getInstance(context).getString("following_group");
        try
        {
            tFollowGroupList = JSON.parseArray(JSON.parseObject(context).getString("data"), com/ximalaya/ting/android/model/follow/FollowGroup);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Logger.log((new StringBuilder()).append(context.getMessage()).append(Logger.getLineInfo()).toString());
        }
        return tFollowGroupList;
    }

    public void removeFollowGroupUpdateListener(FollowGroupUpdateListener followgroupupdatelistener)
    {
        listenerList.remove(followgroupupdatelistener);
    }

    public void setOnFollowGroupUpdateListener(FollowGroupUpdateListener followgroupupdatelistener)
    {
        listenerList.add(followgroupupdatelistener);
    }

    public void updateFollowGroup(final Context context)
    {
        (new _cls1()).myexec(new Void[0]);
    }



/*
    static List access$002(FollowManage followmanage, List list)
    {
        followmanage.tFollowGroupList = list;
        return list;
    }

*/


    private class _cls1 extends MyAsyncTask
    {

        final FollowManage this$0;
        final Context val$context;

        protected transient Boolean doInBackground(Void avoid[])
        {
            boolean flag;
            flag = true;
            if (UserInfoMannage.getInstance().getUser() == null)
            {
                return Boolean.valueOf(false);
            }
            avoid = e.D;
            RequestParams requestparams = new RequestParams();
            avoid = f.a().a(avoid, requestparams, null, null);
            Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
            JSONObject jsonobject = JSON.parseObject(avoid);
            if (!"true".equals(jsonobject.getString("res"))) goto _L2; else goto _L1
_L1:
            SharedPreferencesUtil.getInstance(context).saveString("following_group", avoid);
            tFollowGroupList = JSON.parseArray(jsonobject.getString("data"), com/ximalaya/ting/android/model/follow/FollowGroup);
            isUpdate = true;
_L4:
            return Boolean.valueOf(flag);
            avoid;
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
_L2:
            flag = false;
            if (true) goto _L4; else goto _L3
_L3:
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(Boolean boolean1)
        {
            if (boolean1.booleanValue())
            {
                for (boolean1 = listenerList.iterator(); boolean1.hasNext(); ((FollowGroupUpdateListener)boolean1.next()).update(tFollowGroupList)) { }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Boolean)obj);
        }

        _cls1()
        {
            this$0 = FollowManage.this;
            context = context1;
            super();
        }
    }

}
