// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.wxapi;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import cn.wemart.sdk.WemartWebView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.modelpay.PayResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.activity.web.WebWemartActivityNew;
import com.ximalaya.ting.android.library.util.Logger;

public class XMWXPayEntryActivity extends WebWemartActivityNew
    implements IWXAPIEventHandler
{

    private IWXAPI a;

    public XMWXPayEntryActivity()
    {
    }

    protected void a(BaseResp baseresp, int i)
    {
        Logger.log((new StringBuilder()).append("onPayFinish, errCode = ").append(baseresp.errCode).toString());
        if (baseresp.getType() == 5)
        {
            if (baseresp.errCode == 0)
            {
                if (i == 1 && (baseresp instanceof PayResp))
                {
                    baseresp = ((PayResp)baseresp).extData;
                    if (!TextUtils.isEmpty(baseresp))
                    {
                        try
                        {
                            Object obj1 = JSON.parseObject(baseresp);
                            Object obj = ((JSONObject) (obj1)).getString("amount");
                            String s = ((JSONObject) (obj1)).getString("mark");
                            baseresp = ((JSONObject) (obj1)).getString("order_num");
                            obj1 = ((JSONObject) (obj1)).getString("track_id");
                            Intent intent = new Intent();
                            intent.setAction("com.ximalaya.ting.android.pay.ACTION_PAY_FINISH");
                            intent.putExtra("amount", ((String) (obj)));
                            intent.putExtra("mark", s);
                            intent.putExtra("track_id", ((String) (obj1)));
                            sendBroadcast(intent);
                            obj = new Intent(this, com/ximalaya/ting/android/activity/web/WebActivityNew);
                            baseresp = (new StringBuilder()).append("{\"orderId\":\"").append(baseresp).append("\", \"status\":").append(true).append("}").toString();
                            ((Intent) (obj)).putExtra("ExtraUrl", (new StringBuilder()).append("javascript:nativeCall.paySuccess('").append(baseresp).append("')").toString());
                            startActivity(((Intent) (obj)));
                        }
                        // Misplaced declaration of an exception variable
                        catch (BaseResp baseresp) { }
                    }
                }
            } else
            {
                showToast("\u5FAE\u4FE1\u652F\u4ED8\u5931\u8D25");
            }
        }
        finish();
    }

    protected void b(BaseResp baseresp, int i)
    {
        if (baseresp.getType() == 5 && mWebView != null)
        {
            mWebView.a(baseresp.errCode);
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        a = WXAPIFactory.createWXAPI(this, b.b);
        a.handleIntent(getIntent(), this);
    }

    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        setIntent(intent);
        a.handleIntent(intent, this);
    }

    public void onReq(BaseReq basereq)
    {
    }

    public void onResp(BaseResp baseresp)
    {
        int i = ((MyApplication)getApplication()).c;
        i;
        JVM INSTR tableswitch 0 2: default 40
    //                   0 66
    //                   1 92
    //                   2 99;
           goto _L1 _L2 _L3 _L4
_L1:
        if (baseresp.getType() == 5 && baseresp.errCode == 0 && i == 0)
        {
            showToast("\u5FAE\u4FE1\u652F\u4ED8\u6210\u529F");
        }
_L6:
        return;
_L2:
        if (baseresp.getType() != 5 || baseresp.errCode != 0 || i != 0) goto _L6; else goto _L5
_L5:
        showToast("\u5FAE\u4FE1\u652F\u4ED8\u6210\u529F");
        return;
_L3:
        a(baseresp, i);
        return;
_L4:
        b(baseresp, i);
        return;
    }
}
