// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.wxapi;

import android.content.res.Resources;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;
import com.ximalaya.ting.android.model.auth.AuthInfo;
import java.io.IOException;

// Referenced classes of package com.ximalaya.ting.android.wxapi:
//            WXEntryActivity, b

class a
    implements Callback
{

    final WXEntryActivity a;

    a(WXEntryActivity wxentryactivity)
    {
        a = wxentryactivity;
        super();
    }

    public void onFailure(Request request, IOException ioexception)
    {
        a.showToast(a.getResources().getString(0x7f090099));
    }

    public void onResponse(Response response)
        throws IOException
    {
        if (!response.isSuccessful())
        {
            a.showToast(a.getResources().getString(0x7f090099));
        } else
        {
            response = response.body().string();
            if (!TextUtils.isEmpty(response))
            {
                try
                {
                    response = JSON.parseObject(response);
                    AuthInfo authinfo = new AuthInfo();
                    authinfo.access_token = response.getString("access_token");
                    authinfo.expires_in = response.getString("expires_in");
                    authinfo.openid = response.getString("openid");
                    authinfo.refreshToken = response.getString("refresh_token");
                    a.runOnUiThread(new b(this, authinfo));
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (Response response)
                {
                    response.printStackTrace();
                }
                a.showToast(a.getResources().getString(0x7f090099));
                return;
            }
        }
    }
}
