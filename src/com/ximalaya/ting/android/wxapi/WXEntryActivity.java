// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.wxapi;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.OkHttpClient;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.activity.share.BaseShareDialog;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.wxapi:
//            a

public class WXEntryActivity extends BaseActivity
    implements IWXAPIEventHandler
{

    private IWXAPI a;
    private ScoreManage b;
    private Context c;

    public WXEntryActivity()
    {
    }

    private boolean a()
    {
        b = ScoreManage.getInstance(c);
        return b != null;
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        a = WXAPIFactory.createWXAPI(this, b.b, false);
        a.handleIntent(getIntent(), this);
        c = getApplicationContext();
    }

    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        setIntent(intent);
        a.handleIntent(intent, this);
    }

    public void onReq(BaseReq basereq)
    {
    }

    public void onResp(BaseResp baseresp)
    {
        int i;
        i = ((MyApplication)getApplication()).b;
        a();
        baseresp.errCode;
        JVM INSTR tableswitch -4 0: default 56
    //                   -4 397
    //                   -3 56
    //                   -2 56
    //                   -1 56
    //                   0 61;
           goto _L1 _L2 _L1 _L1 _L1 _L3
_L1:
        finish();
_L5:
        return;
_L3:
        if (i == 0)
        {
            showToast("\u5206\u4EAB\u5230\u5FAE\u4FE1\u597D\u53CB\u6210\u529F");
            ToolUtil.onEvent(this, "SHARE_WEIXIN_FRIEND_SUCCESS");
            if (a())
            {
                ScoreManage.getInstance(getApplicationContext()).onShareFinishMain(8);
            }
            com.ximalaya.ting.android.model.share.ShareContentModel sharecontentmodel = ((MyApplication)getApplication()).d;
            if (sharecontentmodel != null)
            {
                BaseShareDialog.statShare(this, sharecontentmodel);
                ((MyApplication)getApplication()).d = null;
            }
        }
        if (i == 1)
        {
            showToast("\u5206\u4EAB\u5230\u5FAE\u4FE1\u670B\u53CB\u5708\u6210\u529F");
            ToolUtil.onEvent(this, "SHARE_WEIXIN_GROUP_SUCCESS");
            if (a())
            {
                ScoreManage.getInstance(getApplicationContext()).onShareFinishMain(1);
            }
            com.ximalaya.ting.android.model.share.ShareContentModel sharecontentmodel1 = ((MyApplication)getApplication()).d;
            if (sharecontentmodel1 != null)
            {
                BaseShareDialog.statShare(this, sharecontentmodel1);
                ((MyApplication)getApplication()).d = null;
            }
        }
        if (i == 2)
        {
            showToast("\u9080\u8BF7\u597D\u53CB\u6210\u529F");
            ToolUtil.onEvent(this, "SHARE_WEIXIN_FRIEND_SUCCESS");
            if (a())
            {
                ScoreManage.getInstance(getApplicationContext()).onShareFinishMain(4);
            }
        }
        if (i == 4)
        {
            showToast("\u4ECE\u670B\u53CB\u5708\u9080\u8BF7\u6210\u529F");
            ToolUtil.onEvent(this, "SHARE_WEIXIN_GROUP_SUCCESS");
            if (a())
            {
                ScoreManage.getInstance(getApplicationContext()).onShareFinishMain(4);
            }
        }
        if (a() && i == 3)
        {
            if (a())
            {
                ScoreManage.getInstance(getApplicationContext()).onShareFinishMain(7);
            }
            ToolUtil.onEvent(this, "SHARE_WEIXIN_GROUP_SUCCESS");
        }
        if (i != 5)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!(baseresp instanceof com.tencent.mm.sdk.modelmsg.SendAuth.Resp)) goto _L5; else goto _L4
_L4:
        baseresp = (com.tencent.mm.sdk.modelmsg.SendAuth.Resp)baseresp;
        baseresp = (new StringBuilder()).append("https://api.weixin.qq.com/sns/oauth2/access_token?grant_type=authorization_code&appid=").append(b.b).append("&secret=").append("f84ef3719ebd3d164cded5c7b324ade9").append("&code=").append(((com.tencent.mm.sdk.modelmsg.SendAuth.Resp) (baseresp)).code).toString();
        (new OkHttpClient()).newCall((new com.squareup.okhttp.Request.Builder()).url(baseresp).build()).enqueue(new a(this));
        return;
_L2:
        showToast("\u5206\u4EAB\u51FA\u9519");
        if (true) goto _L1; else goto _L6
_L6:
    }
}
