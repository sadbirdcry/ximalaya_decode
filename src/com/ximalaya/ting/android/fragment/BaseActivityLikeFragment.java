// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.MyCallback;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseFragment

public class BaseActivityLikeFragment extends BaseFragment
{

    public long createTime;
    public View homeButton;
    public LoginInfoModel loginInfoModel;
    public ImageView nextButton;
    public View retButton;
    public TextView topTextView;
    public View top_bar;
    private final int totalTime = 600;

    public BaseActivityLikeFragment()
    {
    }

    public boolean canGoon()
    {
        return isAdded() && !isRemoving() && !isDetached();
    }

    protected void doAfterAnimation(final MyCallback callback)
    {
        if (callback == null)
        {
            return;
        }
        long l = getAnimationLeftTime();
        if (l > 0L && fragmentBaseContainerView != null)
        {
            fragmentBaseContainerView.postDelayed(new _cls3(), l);
            return;
        } else
        {
            callback.execute();
            return;
        }
    }

    public void finish()
    {
        if (isAdded() && (getActivity() instanceof MainTabActivity2))
        {
            ((MainTabActivity2)getActivity()).removeFramentFromManageFragment(this);
        }
    }

    protected void finishFragment()
    {
label0:
        {
            Logger.d("backPress", getClass().getSimpleName());
            if (isAdded())
            {
                if (!(getActivity() instanceof MainTabActivity2))
                {
                    break label0;
                }
                ((MainTabActivity2)getActivity()).onBack();
            }
            return;
        }
        getActivity().finish();
    }

    public long getAnimationLeftTime()
    {
        return (600L + createTime) - System.currentTimeMillis();
    }

    public void hidePlayButton()
    {
        if (getActivity() != null && (getActivity() instanceof MainTabActivity2))
        {
            ((MainTabActivity2)getActivity()).hidePlayButton();
        }
    }

    protected void initCommon()
    {
        createTime = System.currentTimeMillis();
        retButton = findViewById(0x7f0a007b);
        if (retButton != null)
        {
            retButton.setOnClickListener(new _cls1());
        }
        topTextView = (TextView)findViewById(0x7f0a00ae);
        top_bar = findViewById(0x7f0a0066);
        if (findViewById(0x7f0a070e) != null)
        {
            homeButton = findViewById(0x7f0a070e);
            homeButton.setOnClickListener(new _cls2());
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        initCommon();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        super.onActivityResult(i, j, intent);
    }

    public boolean onBackPressed()
    {
        return false;
    }

    public void onDestroy()
    {
        mCon = null;
        super.onDestroy();
    }

    public void onPause()
    {
        super.onPause();
    }

    public void onRefresh()
    {
    }

    public void onResume()
    {
        super.onResume();
    }

    public void setTitleText(String s)
    {
label0:
        {
            if (topTextView != null)
            {
                if (!TextUtils.isEmpty(s))
                {
                    break label0;
                }
                topTextView.setText("\u65E0\u6807\u9898");
            }
            return;
        }
        topTextView.setText(s);
    }

    public void showPlayButton()
    {
        if (getActivity() != null && (getActivity() instanceof MainTabActivity2))
        {
            ((MainTabActivity2)getActivity()).showPlayButton();
        }
    }

    public void showToast(String s)
    {
        if (isAdded())
        {
            Toast.makeText(getActivity(), s, 0).show();
        }
    }

    public void startActivityForResult(Intent intent, int i)
    {
        if (getActivity() != null)
        {
            getActivity().startActivityForResult(intent, i);
        }
    }

    private class _cls3
        implements Runnable
    {

        final BaseActivityLikeFragment this$0;
        final MyCallback val$callback;

        public void run()
        {
            if (canGoon())
            {
                callback.execute();
            }
        }

        _cls3()
        {
            this$0 = BaseActivityLikeFragment.this;
            callback = mycallback;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final BaseActivityLikeFragment this$0;

        public void onClick(View view)
        {
            finishFragment();
        }

        _cls1()
        {
            this$0 = BaseActivityLikeFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final BaseActivityLikeFragment this$0;

        public void onClick(View view)
        {
            if (isAdded() && (getActivity() instanceof MainTabActivity2))
            {
                ((MainTabActivity2)getActivity()).clearAllFramentFromManageFragment();
            }
        }

        _cls2()
        {
            this$0 = BaseActivityLikeFragment.this;
            super();
        }
    }

}
