// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.model.category.CategoryTag;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryRecommendFragment

private static class mCtx extends BaseAdapter
{

    private Context mCtx;
    private List mDataList;

    public int getCount()
    {
        if (mDataList == null)
        {
            return 0;
        } else
        {
            return mDataList.size();
        }
    }

    public Object getItem(int i)
    {
        if (mDataList == null)
        {
            return null;
        } else
        {
            return (CategoryTag)mDataList.get(i);
        }
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        view = (RelativeLayout)LayoutInflater.from(mCtx).inflate(0x7f030051, null);
        ((TextView)view.findViewById(0x7f0a0175)).setText(((CategoryTag)mDataList.get(i)).tname);
        view.setTag(mDataList.get(i));
        return view;
    }

    public (List list, Context context)
    {
        mDataList = list;
        mCtx = context;
    }
}
