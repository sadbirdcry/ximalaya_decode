// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category.loader;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.category.detail.CategoryRecmdModel;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.ToolUtil;

public class CategoryRecommendLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "mobile/discovery/v2/category/recommends";
    private int mCategoryId;
    private String mContentType;
    private CategoryRecmdModel mData;

    public CategoryRecommendLoader(Context context, int i, String s)
    {
        super(context);
        mCategoryId = i;
        mContentType = s;
    }

    public void deliverResult(CategoryRecmdModel categoryrecmdmodel)
    {
        super.deliverResult(categoryrecmdmodel);
        mData = categoryrecmdmodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((CategoryRecmdModel)obj);
    }

    public CategoryRecmdModel loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("categoryId", mCategoryId);
        ((RequestParams) (obj)).put("scale", "2");
        ((RequestParams) (obj)).put("contentType", mContentType);
        ((RequestParams) (obj)).put("version", ToolUtil.getAppVersion(getContext()));
        obj = f.a().a("mobile/discovery/v2/category/recommends", ((RequestParams) (obj)), fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            try
            {
                if (JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a).getIntValue("ret") == 0)
                {
                    mData = (CategoryRecmdModel)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a, com/ximalaya/ting/android/model/category/detail/CategoryRecmdModel);
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
