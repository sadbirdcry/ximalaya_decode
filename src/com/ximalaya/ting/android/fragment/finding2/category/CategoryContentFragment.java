// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.shu.ReloadFragment;
import com.ximalaya.ting.android.fragment.finding2.category.loader.CategoryTagLoader;
import com.ximalaya.ting.android.fragment.search.WordAssociatedFragment;
import com.ximalaya.ting.android.library.view.GridViewWithHeaderAndFooter;
import com.ximalaya.ting.android.model.category.CategoryTag;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.MultiDirectionSlidingDrawer;
import com.ximalaya.ting.android.view.SlideView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryDetailFragment, CategoryRecommendFragment

public class CategoryContentFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.device.shu.ReloadFragment.Callback
{
    private static class Config
    {

        public static final int SORT_CLASSIC = 2;
        public static final int SORT_HOTTEST = 0;
        public static final int SORT_LATEST = 1;
        public static final int STATUS_ALL = 0;
        public static final int STATUS_COMPLETE = 1;
        public static final int STATUS_ON_PROGRESS = 2;
        public int mSortWay;
        public int mStatus;
        public int mTabPosition;

        private Config()
        {
        }

        Config(_cls1 _pcls1)
        {
            this();
        }
    }

    private static class TabAdapter extends FragmentStatePagerAdapter
    {

        private String from;
        private View fromBindView;
        private int mCategoryId;
        private Config mConfig;
        private String mContentType;
        private List mData;
        private SparseArray mFragmentRefs;
        private boolean mIsSerialized;

        public void destroyItem(ViewGroup viewgroup, int i, Object obj)
        {
            super.destroyItem(viewgroup, i, obj);
            mFragmentRefs.remove(i);
        }

        public int getCount()
        {
            if (mData == null)
            {
                return 0;
            } else
            {
                return mData.size();
            }
        }

        public List getData()
        {
            return mData;
        }

        public Fragment getFragmentAt(int i)
        {
            WeakReference weakreference = (WeakReference)mFragmentRefs.get(i);
            if (weakreference != null)
            {
                return (Fragment)weakreference.get();
            } else
            {
                return null;
            }
        }

        public Fragment getItem(int i)
        {
            Object obj;
            if (i == 0)
            {
                obj = CategoryRecommendFragment.newInstance(mCategoryId, mContentType, mIsSerialized, from);
                ((Fragment) (obj)).getArguments().putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(fromBindView));
            } else
            {
                obj = (CategoryTag)mData.get(i);
                obj = CategoryDetailFragment.newInstance((new StringBuilder()).append(mCategoryId).append("").toString(), mContentType, ((CategoryTag) (obj)).tname, mIsSerialized, false);
                ((CategoryDetailFragment)obj).setSortBy(CategoryContentFragment.getCategoryDetailSortByParams(mConfig));
                ((CategoryDetailFragment)obj).setBookStatus(mConfig.mStatus);
            }
            mFragmentRefs.put(i, new WeakReference(obj));
            return ((Fragment) (obj));
        }

        public CharSequence getPageTitle(int i)
        {
            return ((CategoryTag)mData.get(i)).tname;
        }

        public int getTagPosition(String s)
        {
            if (mData != null)
            {
                int j = mData.size();
                for (int i = 0; i != j; i++)
                {
                    if (TextUtils.equals(((CategoryTag)mData.get(i)).tname, s))
                    {
                        return i;
                    }
                }

            }
            return -1;
        }

        public void setConfig(Config config)
        {
            mConfig = config;
        }

        public TabAdapter(FragmentManager fragmentmanager, List list, int i, String s, boolean flag, View view)
        {
            super(fragmentmanager);
            mIsSerialized = false;
            mData = list;
            mCategoryId = i;
            mIsSerialized = flag;
            mContentType = s;
            mFragmentRefs = new SparseArray();
            fromBindView = view;
        }

        public TabAdapter(FragmentManager fragmentmanager, List list, int i, String s, boolean flag, View view, String s1)
        {
            super(fragmentmanager);
            mIsSerialized = false;
            mData = list;
            mCategoryId = i;
            mIsSerialized = flag;
            mContentType = s;
            mFragmentRefs = new SparseArray();
            fromBindView = view;
            from = s1;
        }
    }

    private class TabIndexAdapter extends BaseAdapter
    {

        private Context mContext;
        private List mData;
        final CategoryContentFragment this$0;

        public int getCount()
        {
            if (mData == null)
            {
                return 0;
            } else
            {
                return mData.size();
            }
        }

        public Object getItem(int i)
        {
            return mData.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            Object obj;
            boolean flag;
            if (view == null)
            {
                viewgroup = new TextView(mContext);
                viewgroup.setGravity(17);
                viewgroup.setTextSize(2, 14F);
                viewgroup.setSingleLine();
                viewgroup.setLayoutParams(new android.widget.AbsListView.LayoutParams(Utilities.dip2px(mContext, 110F), Utilities.dip2px(mContext, 30F)));
                view = viewgroup;
                obj = viewgroup;
                viewgroup = view;
            } else
            {
                viewgroup = (TextView)view;
                obj = view;
            }
            if (i == mTmpConfig.mTabPosition)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            CategoryContentFragment.setPulldownCheckedItemTextColor(viewgroup, flag);
            viewgroup.setText(((CategoryTag)mData.get(i)).tname);
            return ((View) (obj));
        }

        public TabIndexAdapter(Context context, List list)
        {
            this$0 = CategoryContentFragment.this;
            super();
            mContext = context;
            mData = list;
        }
    }


    private static String itemTitle;
    private TabAdapter adapter;
    private ImageView mBack;
    private int mCategoryId;
    private Config mConfig;
    private String mContentType;
    private boolean mIsSerialized;
    private View mLoadingView;
    private TextView mMoreBtn;
    private GridViewWithHeaderAndFooter mPulldownPanel;
    private boolean mPulldownPanelBuilded;
    private MultiDirectionSlidingDrawer mPulldownWidget;
    private SlideView mSlideView;
    private ArrayList mSortWayControls;
    private ArrayList mStatusControls;
    private TabIndexAdapter mTabIndexAdapter;
    private ArrayList mTabIndexGridChildren;
    private String mTitle;
    private Config mTmpConfig;
    private TextView mTopInfoBar;
    private TextView mWTitle;
    private ViewPager pager;
    private PagerSlidingTabStrip tabs;

    public CategoryContentFragment()
    {
    }

    private void applyConfig()
    {
        boolean flag;
        if (mConfig.mSortWay != mTmpConfig.mSortWay || mConfig.mStatus != mTmpConfig.mStatus || mConfig.mTabPosition != mTmpConfig.mTabPosition)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        mPulldownWidget.closePullDownPanel();
        if (flag)
        {
            mConfig.mSortWay = mTmpConfig.mSortWay;
            mConfig.mStatus = mTmpConfig.mStatus;
            mConfig.mTabPosition = mTmpConfig.mTabPosition;
            pager.setCurrentItem(mConfig.mTabPosition, false);
            adapter.setConfig(mConfig);
            setFragmentConfig(adapter.getFragmentAt(mConfig.mTabPosition));
            if (mConfig.mTabPosition - 1 >= 0)
            {
                setFragmentConfig(adapter.getFragmentAt(mConfig.mTabPosition - 1));
            }
            if (mConfig.mTabPosition + 1 < adapter.getData().size())
            {
                setFragmentConfig(adapter.getFragmentAt(mConfig.mTabPosition + 1));
            }
            return;
        } else
        {
            mTmpConfig.mSortWay = mConfig.mSortWay;
            mTmpConfig.mStatus = mConfig.mStatus;
            mTmpConfig.mTabPosition = mConfig.mTabPosition;
            return;
        }
    }

    private TextView buildHeader(String s)
    {
        TextView textview = new TextView(getActivity());
        textview.setText(s);
        textview.setCompoundDrawablesWithIntrinsicBounds(0x7f020241, 0, 0, 0);
        textview.setCompoundDrawablePadding(Utilities.dip2px(getActivity(), 5F));
        textview.setGravity(16);
        textview.setIncludeFontPadding(false);
        textview.setTextColor(getResources().getColor(0x7f0700c7));
        textview.setTextSize(2, 14F);
        return textview;
    }

    private void buildPulldownPanel(List list)
    {
        if (list == null || list.size() <= 0 || mPulldownPanelBuilded)
        {
            return;
        }
        mPulldownPanelBuilded = true;
        Object obj = buildHeader("\u7C7B\u522B:");
        Object obj3 = new android.widget.AbsListView.LayoutParams(-2, -2);
        ((TextView) (obj)).setPadding(0, Utilities.dip2px(getActivity(), 15F), 0, Utilities.dip2px(getActivity(), 10F));
        ((TextView) (obj)).setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj3)));
        mPulldownPanel.addHeaderView(((View) (obj)));
        int i = Utilities.dip2px(getActivity(), 28F);
        int j = Utilities.dip2px(getActivity(), 5F);
        Object obj2 = new android.widget.LinearLayout.LayoutParams(0, i);
        obj2.weight = 1.0F;
        obj2.rightMargin = j;
        Object obj1 = new android.widget.LinearLayout.LayoutParams(0, i);
        obj1.weight = 1.0F;
        obj1.leftMargin = j;
        obj1.rightMargin = j;
        obj = new android.widget.LinearLayout.LayoutParams(0, i);
        obj.weight = 1.0F;
        obj.leftMargin = j;
        if ("album".equals(mContentType))
        {
            Object obj4 = buildHeader("\u6392\u5E8F:");
            ((TextView) (obj4)).setPadding(0, Utilities.dip2px(getActivity(), 15F), 0, Utilities.dip2px(getActivity(), 10F));
            ((TextView) (obj4)).setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj3)));
            mPulldownPanel.addFooterView(((View) (obj4)));
            obj4 = new LinearLayout(getActivity());
            ((LinearLayout) (obj4)).setOrientation(0);
            TextView textview1 = buildSortWayItem("\u6700\u706B");
            setPulldownCheckedItemTextColor(textview1, true);
            textview1.setOnClickListener(new _cls4());
            mSortWayControls.add(textview1);
            ((LinearLayout) (obj4)).addView(textview1, ((android.view.ViewGroup.LayoutParams) (obj2)));
            textview1 = buildSortWayItem("\u6700\u8FD1\u66F4\u65B0");
            textview1.setOnClickListener(new _cls5());
            mSortWayControls.add(textview1);
            ((LinearLayout) (obj4)).addView(textview1, ((android.view.ViewGroup.LayoutParams) (obj1)));
            textview1 = buildSortWayItem("\u7ECF\u5178");
            textview1.setOnClickListener(new _cls6());
            mSortWayControls.add(textview1);
            ((LinearLayout) (obj4)).addView(textview1, ((android.view.ViewGroup.LayoutParams) (obj)));
            mPulldownPanel.addFooterView(((View) (obj4)));
        }
        if (mIsSerialized)
        {
            TextView textview = buildHeader("\u72B6\u6001:");
            textview.setPadding(0, Utilities.dip2px(getActivity(), 15F), 0, Utilities.dip2px(getActivity(), 10F));
            textview.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj3)));
            mPulldownPanel.addFooterView(textview);
            obj3 = new LinearLayout(getActivity());
            ((LinearLayout) (obj3)).setOrientation(0);
            textview = buildSortWayItem("\u5168\u90E8");
            textview.setOnClickListener(new _cls7());
            setPulldownCheckedItemTextColor(textview, true);
            mStatusControls.add(textview);
            ((LinearLayout) (obj3)).addView(textview, ((android.view.ViewGroup.LayoutParams) (obj2)));
            obj2 = buildSortWayItem("\u5B8C\u6574");
            ((TextView) (obj2)).setOnClickListener(new _cls8());
            mStatusControls.add(obj2);
            ((LinearLayout) (obj3)).addView(((View) (obj2)), ((android.view.ViewGroup.LayoutParams) (obj1)));
            obj1 = buildSortWayItem("\u8FDE\u8F7D\u4E2D");
            ((TextView) (obj1)).setOnClickListener(new _cls9());
            mStatusControls.add(obj1);
            ((LinearLayout) (obj3)).addView(((View) (obj1)), ((android.view.ViewGroup.LayoutParams) (obj)));
            mPulldownPanel.addFooterView(((View) (obj3)));
        }
        mTabIndexAdapter = new TabIndexAdapter(getActivity(), list);
        mPulldownPanel.setAdapter(mTabIndexAdapter);
        mPulldownPanel.setOnItemClickListener(new _cls10());
        getView().findViewById(0x7f0a02e1).setOnClickListener(new _cls11());
    }

    private TextView buildSortWayItem(String s)
    {
        TextView textview = new TextView(getActivity());
        textview.setGravity(17);
        textview.setTextSize(2, 14F);
        textview.setTextColor(Color.parseColor("#333333"));
        textview.setBackgroundColor(Color.parseColor("#f5f8fa"));
        textview.setSingleLine();
        textview.setText(s);
        return textview;
    }

    private void buildTabs(List list)
    {
        mMoreBtn.setVisibility(0);
        if (list != null && list.size() > 0)
        {
            buildPulldownPanel(list);
            adapter = new TabAdapter(getChildFragmentManager(), list, mCategoryId, mContentType, mIsSerialized, fragmentBaseContainerView, itemTitle);
            adapter.setConfig(mConfig);
            pager.setAdapter(adapter);
            tabs.setViewPager(pager);
        }
    }

    private void checkSelectedTabIndexGrid(int i)
    {
        int j = 0;
        while (j < mTabIndexGridChildren.size()) 
        {
            TextView textview = (TextView)mTabIndexGridChildren.get(j);
            boolean flag;
            if (i == j)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            setPulldownCheckedItemTextColor(textview, flag);
            j++;
        }
    }

    private void clickTabIndexGridItem(int i)
    {
        mTmpConfig.mTabPosition = i;
        checkSelectedTabIndexGrid(i);
        setSortAndStatusControlsStatus();
    }

    private static String getCategoryDetailSortByParams(Config config)
    {
        switch (config.mSortWay)
        {
        default:
            return "hot";

        case 0: // '\0'
            return "hot";

        case 1: // '\001'
            return "recent";

        case 2: // '\002'
            return "classic";
        }
    }

    private void highlightConfig()
    {
        setSortWay(mConfig.mSortWay);
        setStatus(mConfig.mStatus);
        Config config = mTmpConfig;
        Config config1 = mConfig;
        int i = pager.getCurrentItem();
        config1.mTabPosition = i;
        config.mTabPosition = i;
        clickTabIndexGridItem(mConfig.mTabPosition);
        mTabIndexAdapter.notifyDataSetChanged();
    }

    private void initTabs()
    {
        tabs.setDividerColor(0);
        tabs.setIndicatorColor(Color.parseColor("#f86442"));
        tabs.setUnderlineColor(Color.parseColor("#dcdcdc"));
        tabs.setUnderlineHeight(0);
        tabs.setDividerPadding(0);
        tabs.setDeactivateTextColor(Color.parseColor("#666666"));
        tabs.setActivateTextColor(Color.parseColor("#f86442"));
        tabs.setTabSwitch(true);
        tabs.setTextSize(15);
        tabs.setIndicatorHeight(Utilities.dip2px(getActivity(), 3F));
        tabs.setTabPaddingLeftRight(Utilities.dip2px(getActivity(), 17F));
        tabs.setDisallowInterceptTouchEventView((ViewGroup)tabs.getParent());
    }

    public static CategoryContentFragment newInstance(int i, String s, String s1, View view)
    {
        CategoryContentFragment categorycontentfragment = new CategoryContentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("category_id", i);
        bundle.putString("content_type", s);
        bundle.putString("title", s1);
        if (s1 != null)
        {
            itemTitle = s1;
        }
        if (view != null)
        {
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, (new StringBuilder()).append(s1).append(" \u66F4\u591A").toString()));
        }
        categorycontentfragment.setArguments(bundle);
        return categorycontentfragment;
    }

    public static CategoryContentFragment newInstance(int i, String s, String s1, View view, boolean flag)
    {
        CategoryContentFragment categorycontentfragment = new CategoryContentFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("category_id", i);
        bundle.putString("content_type", s);
        bundle.putString("title", s1);
        if (s1 != null)
        {
            itemTitle = s1;
        }
        if (view != null)
        {
            if (flag)
            {
                s = "\u66F4\u591A";
            } else
            {
                s = "";
            }
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, s));
        }
        categorycontentfragment.setArguments(bundle);
        return categorycontentfragment;
    }

    private void setFragmentConfig(Fragment fragment)
    {
        if (fragment != null && (fragment instanceof CategoryDetailFragment))
        {
            ((CategoryDetailFragment)fragment).setSortBy(getCategoryDetailSortByParams(mConfig));
            ((CategoryDetailFragment)fragment).setBookStatus(mConfig.mStatus);
            ((CategoryDetailFragment)fragment).syncConfig();
        }
    }

    private static void setPulldownCheckedItemTextColor(TextView textview, boolean flag)
    {
        if (flag)
        {
            textview.setTextColor(Color.parseColor("#ffffff"));
            textview.setBackgroundColor(Color.parseColor("#f86442"));
            return;
        } else
        {
            textview.setTextColor(Color.parseColor("#333333"));
            textview.setBackgroundColor(Color.parseColor("#f5f8fa"));
            return;
        }
    }

    private void setSortAndStatusControlsStatus()
    {
        if (mTmpConfig.mTabPosition == 0)
        {
            TextView textview;
            for (Iterator iterator = mSortWayControls.iterator(); iterator.hasNext(); textview.setEnabled(false))
            {
                textview = (TextView)iterator.next();
                textview.setTextColor(Color.parseColor("#cccccc"));
                textview.setBackgroundColor(Color.parseColor("#f5f8fa"));
            }

            TextView textview1;
            for (Iterator iterator1 = mStatusControls.iterator(); iterator1.hasNext(); textview1.setEnabled(false))
            {
                textview1 = (TextView)iterator1.next();
                textview1.setTextColor(Color.parseColor("#cccccc"));
                textview1.setBackgroundColor(Color.parseColor("#f5f8fa"));
            }

        } else
        {
            TextView textview2;
            for (Iterator iterator2 = mSortWayControls.iterator(); iterator2.hasNext(); textview2.setTextColor(Color.parseColor("#333333")))
            {
                textview2 = (TextView)iterator2.next();
                textview2.setEnabled(true);
            }

            setSortWay(mTmpConfig.mSortWay);
            TextView textview3;
            for (Iterator iterator3 = mStatusControls.iterator(); iterator3.hasNext(); textview3.setTextColor(Color.parseColor("#333333")))
            {
                textview3 = (TextView)iterator3.next();
                textview3.setEnabled(true);
            }

            setStatus(mTmpConfig.mStatus);
        }
    }

    private void setSortWay(int i)
    {
        mTmpConfig.mSortWay = i;
        int j = 0;
        while (j < mSortWayControls.size()) 
        {
            TextView textview = (TextView)mSortWayControls.get(j);
            boolean flag;
            if (j == i)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            setPulldownCheckedItemTextColor(textview, flag);
            j++;
        }
    }

    private void setStatus(int i)
    {
        mTmpConfig.mStatus = i;
        int j = 0;
        while (j < mStatusControls.size()) 
        {
            TextView textview = (TextView)mStatusControls.get(j);
            boolean flag;
            if (j == i)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            setPulldownCheckedItemTextColor(textview, flag);
            j++;
        }
    }

    private void togglePulldownPanel()
    {
        if (mPulldownWidget.isShowing())
        {
            mPulldownWidget.closePullDownPanel();
            return;
        } else
        {
            mPulldownWidget.openPullDownPanel();
            return;
        }
    }

    public void changeTagFragment(String s)
    {
        if (adapter != null)
        {
            int i = adapter.getTagPosition(s);
            if (i >= 0)
            {
                pager.setCurrentItem(i, true);
            }
        }
    }

    public void finish()
    {
        super.finish();
        if (getActivity() instanceof MainTabActivity2)
        {
            ((MainTabActivity2)getActivity()).showPlayButton();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        getView().findViewById(0x7f0a007b).setOnClickListener(this);
        ((TextView)getView().findViewById(0x7f0a0175)).setText(mTitle);
        getView().findViewById(0x7f0a02db).setOnClickListener(this);
        mMoreBtn.setOnClickListener(this);
        tabs.setOnPageChangeListener(new _cls1());
        mPulldownWidget.setCallback(new _cls2());
        doAfterAnimation(new _cls3());
    }

    public boolean onBackPressed()
    {
        if (mPulldownWidget != null && mPulldownWidget.isShowing())
        {
            togglePulldownPanel();
            return true;
        } else
        {
            return super.onBackPressed();
        }
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362523: 
            Bundle bundle = new Bundle();
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/search/WordAssociatedFragment, bundle);
            return;

        case 2131361915: 
            finish();
            return;

        case 2131362527: 
            togglePulldownPanel();
            break;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mConfig = new Config(null);
        mTmpConfig = new Config(null);
        bundle = getArguments();
        if (bundle != null)
        {
            mCategoryId = bundle.getInt("category_id");
            mContentType = bundle.getString("content_type");
            mTitle = bundle.getString("title");
            mIsSerialized = bundle.getBoolean("is_serialized");
        }
        mTabIndexGridChildren = new ArrayList();
        mSortWayControls = new ArrayList();
        mStatusControls = new ArrayList();
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        return new CategoryTagLoader(getActivity(), mCategoryId, mContentType);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f03009f, viewgroup, false);
        mSlideView = (SlideView)layoutinflater.findViewById(0x7f0a02da);
        mSlideView.setSlideRight(true);
        mWTitle = (TextView)layoutinflater.findViewById(0x7f0a0175);
        mBack = (ImageView)layoutinflater.findViewById(0x7f0a007b);
        tabs = (PagerSlidingTabStrip)layoutinflater.findViewById(0x7f0a02dc);
        pager = (ViewPager)layoutinflater.findViewById(0x7f0a0021);
        mPulldownWidget = (MultiDirectionSlidingDrawer)layoutinflater.findViewById(0x7f0a02cd);
        mPulldownWidget.disallowInterceptTouchEvent(true);
        mPulldownPanel = (GridViewWithHeaderAndFooter)layoutinflater.findViewById(0x7f0a02e0);
        mTopInfoBar = (TextView)layoutinflater.findViewById(0x7f0a02de);
        mMoreBtn = (TextView)layoutinflater.findViewById(0x7f0a02df);
        mLoadingView = layoutinflater.findViewById(0x7f0a012c);
        initTabs();
        fragmentBaseContainerView = layoutinflater;
        return layoutinflater;
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        if (mLoadingView != null)
        {
            mLoadingView.setVisibility(4);
        }
        if (list == null || list.size() <= 0)
        {
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a02e2);
            return;
        }
        if (loader instanceof CategoryTagLoader)
        {
            mIsSerialized = ((CategoryTagLoader)loader).isFinished();
        }
        buildTabs(list);
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void reload(View view)
    {
        getLoaderManager().restartLoader(0, null, this);
    }
















    private class _cls4
        implements android.view.View.OnClickListener
    {

        final CategoryContentFragment this$0;

        public void onClick(View view)
        {
            setSortWay(0);
        }

        _cls4()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final CategoryContentFragment this$0;

        public void onClick(View view)
        {
            setSortWay(1);
        }

        _cls5()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final CategoryContentFragment this$0;

        public void onClick(View view)
        {
            setSortWay(2);
        }

        _cls6()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.view.View.OnClickListener
    {

        final CategoryContentFragment this$0;

        public void onClick(View view)
        {
            setStatus(0);
        }

        _cls7()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls8
        implements android.view.View.OnClickListener
    {

        final CategoryContentFragment this$0;

        public void onClick(View view)
        {
            setStatus(1);
        }

        _cls8()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls9
        implements android.view.View.OnClickListener
    {

        final CategoryContentFragment this$0;

        public void onClick(View view)
        {
            setStatus(2);
        }

        _cls9()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls10
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CategoryContentFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            clickTabIndexGridItem(i);
            mTabIndexAdapter.notifyDataSetChanged();
        }

        _cls10()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls11
        implements android.view.View.OnClickListener
    {

        final CategoryContentFragment this$0;

        public void onClick(View view)
        {
            applyConfig();
        }

        _cls11()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final CategoryContentFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f, int j)
        {
        }

        public void onPageSelected(int i)
        {
            if (i == 0)
            {
                mSlideView.setForbidSlide(false);
                return;
            }
            if (adapter.getFragmentAt(i) instanceof CategoryDetailFragment)
            {
                ((CategoryDetailFragment)adapter.getFragmentAt(i)).reloadAd();
            }
            mSlideView.setForbidSlide(true);
        }

        _cls1()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.MultiDirectionSlidingDrawer.Callback
    {

        final CategoryContentFragment this$0;

        public void onCompletePullBack()
        {
            mTopInfoBar.setVisibility(4);
            tabs.setVisibility(0);
            mMoreBtn.setText("\u66F4\u591A");
            mMoreBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f02057f, 0);
            mSlideView.setSlide(true);
            if (getActivity() instanceof MainTabActivity2)
            {
                ((MainTabActivity2)getActivity()).showPlayButton();
            }
        }

        public void onCompletePullDown()
        {
            if (adapter != null)
            {
                buildPulldownPanel(adapter.getData());
                mTopInfoBar.setVisibility(0);
                tabs.setVisibility(4);
                mMoreBtn.setText("\u6536\u8D77");
                mMoreBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f02057d, 0);
                highlightConfig();
                mSlideView.setSlide(false);
                if (getActivity() instanceof MainTabActivity2)
                {
                    ((MainTabActivity2)getActivity()).hidePlayButton();
                    return;
                }
            }
        }

        public void onStartPullDown()
        {
        }

        _cls2()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }


    private class _cls3
        implements MyCallback
    {

        final CategoryContentFragment this$0;

        public void execute()
        {
            ((MyAsyncTaskLoader)getLoaderManager().initLoader(0, null, CategoryContentFragment.this)).setXDCSBindView(fragmentBaseContainerView, fragmentBaseContainerView);
        }

        _cls3()
        {
            this$0 = CategoryContentFragment.this;
            super();
        }
    }

}
