// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.model.category.CategoryTag;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryTagFragmentNew, CategoryDetailFragment

class this._cls0
    implements android.widget.stener
{

    final CategoryTagFragmentNew this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        adapterview = (CategoryTag)CategoryTagFragmentNew.access$1000(CategoryTagFragmentNew.this).get(i);
        if (((CategoryTag) (adapterview)).category_id == -2)
        {
            adapterview = new Intent(CategoryTagFragmentNew.access$1100(CategoryTagFragmentNew.this), com/ximalaya/ting/android/activity/web/WebActivityNew);
            adapterview.putExtra("ExtraUrl", e.ai);
            adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            ((MainTabActivity2)CategoryTagFragmentNew.access$1200(CategoryTagFragmentNew.this)).startActivity(adapterview);
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("categoryId", CategoryTagFragmentNew.access$1300(CategoryTagFragmentNew.this));
        bundle.putString("categoryName", CategoryTagFragmentNew.access$1400(CategoryTagFragmentNew.this));
        bundle.putString("contentType", CategoryTagFragmentNew.access$1500(CategoryTagFragmentNew.this));
        bundle.putString("categoryTitle", CategoryTagFragmentNew.access$1600(CategoryTagFragmentNew.this));
        bundle.putBoolean("isSerialized", CategoryTagFragmentNew.access$1700(CategoryTagFragmentNew.this));
        if (((CategoryTag) (adapterview)).category_id != -1)
        {
            bundle.putString("tagName", ((CategoryTag) (adapterview)).tname);
        }
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        if (CategoryTagFragmentNew.access$800(CategoryTagFragmentNew.this) != -1)
        {
            bundle.putInt("key", CategoryTagFragmentNew.access$800(CategoryTagFragmentNew.this));
        }
        if (CategoryTagFragmentNew.access$600(CategoryTagFragmentNew.this) != -1)
        {
            bundle.putInt("from", CategoryTagFragmentNew.access$600(CategoryTagFragmentNew.this));
        }
        startFragment(com/ximalaya/ting/android/fragment/finding2/category/CategoryDetailFragment, bundle);
    }

    ()
    {
        this$0 = CategoryTagFragmentNew.this;
        super();
    }
}
