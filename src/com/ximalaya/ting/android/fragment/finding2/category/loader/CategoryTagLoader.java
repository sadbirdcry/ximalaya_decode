// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category.loader;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.category.CategoryTag;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.List;

public class CategoryTagLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "mobile/discovery/v1/category/tagsWithoutCover";
    private int mCategoryId;
    private String mContentType;
    private List mData;
    private boolean mIsFinished;

    public CategoryTagLoader(Context context, int i, String s)
    {
        super(context);
        mIsFinished = false;
        mCategoryId = i;
        mContentType = s;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((List)obj);
    }

    public void deliverResult(List list)
    {
        super.deliverResult(list);
        mData = list;
    }

    public boolean isFinished()
    {
        return mIsFinished;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public List loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("categoryId", mCategoryId);
        ((RequestParams) (obj)).put("contentType", mContentType);
        obj = f.a().a("mobile/discovery/v1/category/tagsWithoutCover", ((RequestParams) (obj)), fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            try
            {
                obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
                if (((JSONObject) (obj)).getIntValue("ret") == 0)
                {
                    Object obj1 = ((JSONObject) (obj)).getString("list");
                    if (!TextUtils.isEmpty(((CharSequence) (obj1))))
                    {
                        mData = JSON.parseArray(((String) (obj1)), com/ximalaya/ting/android/model/category/CategoryTag);
                        obj1 = new CategoryTag();
                        obj1.category_id = -1;
                        obj1.tname = "\u63A8\u8350";
                        mData.add(0, obj1);
                    }
                    mIsFinished = ((JSONObject) (obj)).getBoolean("isFinished").booleanValue();
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        return mData;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
