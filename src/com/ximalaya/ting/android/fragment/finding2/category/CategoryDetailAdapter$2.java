// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryDetailAdapter

class val.model extends a
{

    final CategoryDetailAdapter this$0;
    final bumViewHolder val$holder;
    final CategoryAlbumModel val$model;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$holder.collect);
    }

    public void onFinish()
    {
        super.onFinish();
        val$holder.collect.setEnabled(true);
    }

    public void onNetError(int i, String s)
    {
        Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
        CategoryDetailAdapter.access$100(CategoryDetailAdapter.this, val$holder, val$model.isCCollected());
    }

    public void onStart()
    {
        super.onStart();
        val$holder.collect.setEnabled(false);
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            CategoryDetailAdapter.access$100(CategoryDetailAdapter.this, val$holder, val$model.isCCollected());
            return;
        }
        Object obj = null;
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = obj;
        }
        if (s != null && s.getIntValue("ret") == 0)
        {
            if (val$model.isCCollected())
            {
                s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
                val$model.setCCollected(false);
            } else
            {
                s = "\u6536\u85CF\u6210\u529F\uFF01";
                val$model.setCCollected(true);
            }
            CategoryDetailAdapter.access$100(CategoryDetailAdapter.this, val$holder, val$model.isCCollected());
            Toast.makeText(mContext, s, 0).show();
            return;
        }
        s = s.getString("msg");
        if (TextUtils.isEmpty(s))
        {
            s = "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
        }
        Toast.makeText(mContext, s, 0).show();
        CategoryDetailAdapter.access$100(CategoryDetailAdapter.this, val$holder, val$model.isCCollected());
    }

    bumViewHolder()
    {
        this$0 = final_categorydetailadapter;
        val$holder = bumviewholder;
        val$model = CategoryAlbumModel.this;
        super();
    }
}
