// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.os.Bundle;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumListModel;
import com.ximalaya.ting.android.util.MyCallback;
import java.io.Serializable;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryDetailFragment, CategoryDetailAdapter

class val.data
    implements MyCallback
{

    final CategoryDetailFragment this$0;
    final Object val$data;

    public void execute()
    {
        if (parseData(val$data))
        {
            if (!CategoryDetailFragment.access$500(CategoryDetailFragment.this) && (val$data instanceof CategoryAlbumListModel))
            {
                CategoryAlbumListModel categoryalbumlistmodel = (CategoryAlbumListModel)val$data;
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", (Serializable)categoryalbumlistmodel.getList());
                CategoryDetailFragment.access$2900(CategoryDetailFragment.this, 3, CategoryDetailFragment.access$2800(CategoryDetailFragment.this), null, bundle);
            }
            showFooterView(com.ximalaya.ting.android.fragment.w.HIDE_ALL);
            return;
        }
        if (CategoryDetailFragment.access$400(CategoryDetailFragment.this).getCount() > 0)
        {
            showFooterView(com.ximalaya.ting.android.fragment.w.HIDE_ALL);
            return;
        } else
        {
            showFooterView(com.ximalaya.ting.android.fragment.w.NO_DATA);
            return;
        }
    }

    ()
    {
        this$0 = final_categorydetailfragment;
        val$data = Object.this;
        super();
    }
}
