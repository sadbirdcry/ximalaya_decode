// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import com.ximalaya.ting.android.model.ad.CategoryAdModel;
import java.util.List;

public class CategoryAdLoader extends AsyncTaskLoader
{

    private String mCategoryId;
    private List mData;

    public CategoryAdLoader(Context context, String s)
    {
        super(context);
        mCategoryId = s;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((List)obj);
    }

    public void deliverResult(List list)
    {
        super.deliverResult(list);
        mData = list;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public List loadInBackground()
    {
        mData = CategoryAdModel.getListFromRemote(getContext(), mCategoryId);
        return mData;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
