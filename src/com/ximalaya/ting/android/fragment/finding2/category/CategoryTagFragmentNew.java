// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.FocusImageAdapter;
import com.ximalaya.ting.android.animation.FixedSpeedScroller;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceUtil;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.category.CategoryTag;
import com.ximalaya.ting.android.modelnew.FocusImageModelNew;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.MyGridView;
import com.ximalaya.ting.android.view.viewpager.ViewPagerInScroll;
import com.ximalaya.ting.android.view.viewpagerindicator.CirclePageIndicator;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CategoryTagFragmentNew extends BaseActivityLikeFragment
{
    private class CategoryAdapter extends BaseAdapter
    {

        private List tDataList;
        final CategoryTagFragmentNew this$0;

        public int getCount()
        {
            if (tDataList != null)
            {
                return tDataList.size();
            } else
            {
                return 0;
            }
        }

        public Object getItem(int i)
        {
            return null;
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            CategoryTag categorytag = (CategoryTag)tDataList.get(i);
            viewgroup = view;
            if (view == null)
            {
                viewgroup = View.inflate(mContext, 0x7f030053, null);
                view = new ViewHolder();
                view.cover = (ImageView)viewgroup.findViewById(0x7f0a01a3);
                view.label = (TextView)viewgroup.findViewById(0x7f0a01a4);
                markImageView(((ViewHolder) (view)).cover);
                android.widget.LinearLayout.LayoutParams layoutparams = (android.widget.LinearLayout.LayoutParams)((ViewHolder) (view)).cover.getLayoutParams();
                if (a.e)
                {
                    layoutparams.width = (ToolUtil.getScreenWidth(mContext) - ToolUtil.dp2px(mContext, 10F) * 7 - ToolUtil.dp2px(mContext, 5F) * 12) / 6;
                    ((ViewHolder) (view)).label.setSingleLine();
                } else
                {
                    layoutparams.width = (ToolUtil.getScreenWidth(mContext) - ToolUtil.dp2px(mContext, 10F) * 4 - ToolUtil.dp2px(mContext, 5F) * 6) / 3;
                }
                layoutparams.height = (int)((float)(layoutparams.width * 140) / 188F);
                viewgroup.setTag(view);
            }
            view = (ViewHolder)viewgroup.getTag();
            ((ViewHolder) (view)).label.setText(categorytag.tname);
            if (categorytag.category_id == -1)
            {
                ImageManager2.from(mContext).displayImage(((ViewHolder) (view)).cover, categorytag.cover_path, 0x7f0201b7, true);
                return viewgroup;
            }
            if (categorytag.category_id == -2)
            {
                ImageManager2.from(mContext).displayImage(((ViewHolder) (view)).cover, categorytag.cover_path, 0x7f0201b8, true);
                return viewgroup;
            } else
            {
                ImageManager2.from(mContext).displayImage(((ViewHolder) (view)).cover, categorytag.cover_path, 0x7f02011b, true);
                return viewgroup;
            }
        }

        public CategoryAdapter(List list)
        {
            this$0 = CategoryTagFragmentNew.this;
            super();
            tDataList = list;
        }
    }

    class ViewHolder
    {

        ImageView cover;
        TextView label;
        final CategoryTagFragmentNew this$0;

        ViewHolder()
        {
            this$0 = CategoryTagFragmentNew.this;
            super();
        }
    }


    public static final String SHOWFOCUS = "showFocus";
    private static long SWAP_TIME_SLICES = 5000L;
    private String categoryId;
    private String categoryName;
    private String contentType;
    private List dataList;
    private boolean isFakeData;
    private int key;
    private boolean loadingNextPage;
    private Runnable mAutoSwapRunnable;
    private Context mContext;
    private PagerAdapter mFocusAdapter;
    private View mFocusImageRoot;
    private ArrayList mFocusImages;
    private int mFocusIndex;
    private CirclePageIndicator mFocusIndicator;
    private ImageView mFocusLoading;
    private ViewPagerInScroll mFocusPager;
    private int mFrom;
    private MyGridView mGridView;
    private boolean mHasRecommendZones;
    private boolean mIsResumeDone;
    private boolean mIsSerialized;
    private LinearLayout mLoadingLayout;
    private ImageView mSearchIv;
    private CategoryAdapter mSoundsHotAdapter;
    private ViewGroup noNetLayout;
    private boolean showFocus;
    private String title;
    private int totalCount;

    public CategoryTagFragmentNew()
    {
        totalCount = 0;
        loadingNextPage = false;
        mHasRecommendZones = false;
        mFrom = -1;
        key = -1;
        showFocus = true;
        mIsSerialized = false;
        isFakeData = false;
        mFocusImages = new ArrayList();
        mFocusIndex = 0;
        mAutoSwapRunnable = new _cls2();
    }

    private void expandHitRect(final View view)
    {
        fragmentBaseContainerView.post(new _cls1());
    }

    private List getDefaultTags()
    {
        ArrayList arraylist = new ArrayList();
        CategoryTag categorytag = new CategoryTag();
        categorytag.tname = "\u5168\u90E8";
        categorytag.category_id = -1;
        arraylist.add(0, categorytag);
        return arraylist;
    }

    private void initData()
    {
        if (!loadingNextPage)
        {
            loadDataListData();
        }
        setTitleText(title);
        if (showFocus)
        {
            loadFocusImage();
            return;
        } else
        {
            removeFocusImgFromPrefAndHideView();
            return;
        }
    }

    private void initFocusImage()
    {
        mFocusImageRoot = findViewById(0x7f0a0071);
        mFocusPager = (ViewPagerInScroll)mFocusImageRoot.findViewById(0x7f0a017a);
        mFocusPager.setDisallowInterceptTouchEventView((ViewGroup)mFocusPager.getParent());
        mFocusIndicator = (CirclePageIndicator)mFocusImageRoot.findViewById(0x7f0a0230);
        mFocusLoading = (ImageView)mFocusImageRoot.findViewById(0x7f0a0275);
        Object obj = mFocusImageRoot.getLayoutParams();
        int i = ToolUtil.getScreenWidth(mActivity);
        int j = (int)((float)i * 0.46875F);
        obj.width = i;
        obj.height = j;
        mFocusImageRoot.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        try
        {
            obj = android/support/v4/view/ViewPager.getDeclaredField("mScroller");
            ((Field) (obj)).setAccessible(true);
            FixedSpeedScroller fixedspeedscroller = new FixedSpeedScroller(mFocusPager.getContext(), new DecelerateInterpolator());
            ((Field) (obj)).set(mFocusPager, fixedspeedscroller);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        mFocusAdapter = new FocusImageAdapter(this, mFocusImages, false);
        ((FocusImageAdapter)mFocusAdapter).setCycleScrollFlag(true);
        mFocusPager.setAdapter(mFocusAdapter);
        mFocusIndicator.setViewPager(mFocusPager);
        mFocusIndicator.setOnPageChangeListener(new _cls3());
        obj = new FocusImageModelNew();
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        updateFocusImageBar();
    }

    private void initListener()
    {
        noNetLayout.setOnClickListener(new _cls4());
        mSearchIv.setOnClickListener(new _cls5());
        mGridView.setOnItemClickListener(new _cls6());
    }

    private void initUi()
    {
        mLoadingLayout = (LinearLayout)findViewById(0x7f0a0073);
        initFocusImage();
        mGridView = (MyGridView)findViewById(0x7f0a0075);
        if (a.e)
        {
            mGridView.setNumColumns(6);
        } else
        {
            mGridView.setNumColumns(3);
        }
        dataList = new ArrayList();
        noNetLayout = (ViewGroup)findViewById(0x7f0a035b);
        mSoundsHotAdapter = new CategoryAdapter(dataList);
        mGridView.setAdapter(mSoundsHotAdapter);
        mSearchIv = (ImageView)findViewById(0x7f0a071a);
        mSearchIv.setVisibility(0);
        mSearchIv.setScaleType(android.widget.ImageView.ScaleType.CENTER);
        mSearchIv.setImageResource(0x7f020510);
        expandHitRect(mSearchIv);
        showReloadLayout(false);
    }

    private void loadDataListData()
    {
        (new _cls8()).myexec(new Void[0]);
    }

    private void loadFocusImage()
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("categoryId", categoryId);
        requestparams.put("version", ToolUtil.getAppVersion(getActivity()));
        f.a().a("mobile/discovery/v1/category/focusImage", requestparams, DataCollectUtil.getDataFromView(mFocusImageRoot), new _cls7(), true);
    }

    private void readFocusImgFromPrefAndDisplay()
    {
        Object obj = SharedPreferencesUtil.getInstance(mContext).getString("category_focus_img");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            mFocusImageRoot.setVisibility(8);
            return;
        }
        obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/modelnew/FocusImageModelNew);
        if (obj != null && ((List) (obj)).size() > 0)
        {
            mFocusImageRoot.setVisibility(0);
            mFocusImages.clear();
            mFocusImages.addAll(((java.util.Collection) (obj)));
            updateFocusImageBar();
            return;
        } else
        {
            mFocusImageRoot.setVisibility(8);
            return;
        }
    }

    private void removeFocusImgFromPrefAndHideView()
    {
        SharedPreferencesUtil.getInstance(mContext).removeByKey("category_focus_img");
        mFocusImageRoot.setVisibility(8);
    }

    private void saveFocusImgToPref(String s)
    {
        String s1 = SharedPreferencesUtil.getInstance(mContext).getString("category_focus_img");
        if (!TextUtils.isEmpty(s1) && !s1.equals(s))
        {
            SharedPreferencesUtil.getInstance(mContext).saveString("category_focus_img", s);
        }
    }

    private void showReloadLayout(boolean flag)
    {
        if (flag && (dataList == null || dataList.size() == 0))
        {
            noNetLayout.setVisibility(0);
            return;
        } else
        {
            noNetLayout.setVisibility(8);
            return;
        }
    }

    private void startAutoSwapFocusImage()
    {
        fragmentBaseContainerView.postDelayed(mAutoSwapRunnable, SWAP_TIME_SLICES);
    }

    private void stopAutoSwapFocusImage()
    {
        if (fragmentBaseContainerView != null && mAutoSwapRunnable != null)
        {
            fragmentBaseContainerView.removeCallbacks(mAutoSwapRunnable);
        }
    }

    private void updateFocusImageBar()
    {
        Logger.log((new StringBuilder()).append("updateFocusImageBar mFocusIndex =").append(mFocusIndex).toString());
        mFocusLoading.setVisibility(8);
        mFocusLoading.setImageBitmap(null);
        if (mFocusIndex == 0)
        {
            mFocusPager.setCurrentItem(0x3fffffff - 0x3fffffff % mFocusImages.size());
        }
        if (isFakeData)
        {
            mFocusIndicator.setPagerRealCount(mFocusImages.size() / 2);
        } else
        {
            mFocusIndicator.setPagerRealCount(mFocusImages.size());
        }
        isFakeData = false;
        mFocusAdapter.notifyDataSetChanged();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mContext = getActivity();
        if (getArguments() != null)
        {
            contentType = getArguments().getString("contentType");
            categoryName = getArguments().getString("categoryName");
            categoryId = getArguments().getString("categoryId");
            title = getArguments().getString("title");
            mIsSerialized = getArguments().getBoolean("isSerialized", false);
            if (getArguments().containsKey("from"))
            {
                mFrom = getArguments().getInt("from");
            } else
            {
                mFrom = -1;
            }
            if (getArguments().containsKey("key"))
            {
                key = getArguments().getInt("key");
            } else
            {
                key = -1;
            }
            if (getArguments().containsKey("showFocus"))
            {
                showFocus = getArguments().getBoolean("showFocus");
            } else
            {
                showFocus = true;
            }
        }
        initUi();
        initListener();
        initData();
        if (MyDeviceUtil.isFromDeviceDownload(mFrom))
        {
            mSearchIv.setVisibility(4);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03000a, viewgroup, false);
        mIsResumeDone = false;
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void onPause()
    {
        stopAutoSwapFocusImage();
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        if (mFrom == 5 && XiMaoBTManager.getInstance(mCon).getConnState() != com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTED)
        {
            getActivity().onBackPressed();
            return;
        } else
        {
            startAutoSwapFocusImage();
            ThirdAdStatUtil.getInstance().statFocusAd(mFocusImages, false);
            mIsResumeDone = true;
            return;
        }
    }




















/*
    static boolean access$2402(CategoryTagFragmentNew categorytagfragmentnew, boolean flag)
    {
        categorytagfragmentnew.isFakeData = flag;
        return flag;
    }

*/




/*
    static boolean access$2702(CategoryTagFragmentNew categorytagfragmentnew, boolean flag)
    {
        categorytagfragmentnew.loadingNextPage = flag;
        return flag;
    }

*/






/*
    static int access$302(CategoryTagFragmentNew categorytagfragmentnew, int i)
    {
        categorytagfragmentnew.mFocusIndex = i;
        return i;
    }

*/


/*
    static int access$308(CategoryTagFragmentNew categorytagfragmentnew)
    {
        int i = categorytagfragmentnew.mFocusIndex;
        categorytagfragmentnew.mFocusIndex = i + 1;
        return i;
    }

*/



/*
    static int access$3102(CategoryTagFragmentNew categorytagfragmentnew, int i)
    {
        categorytagfragmentnew.totalCount = i;
        return i;
    }

*/


/*
    static boolean access$3202(CategoryTagFragmentNew categorytagfragmentnew, boolean flag)
    {
        categorytagfragmentnew.mHasRecommendZones = flag;
        return flag;
    }

*/












    private class _cls1
        implements Runnable
    {

        final CategoryTagFragmentNew this$0;
        final View val$view;

        public void run()
        {
            if (isAdded())
            {
                Object obj = new Rect();
                View view1 = view;
                view1.getHitRect(((Rect) (obj)));
                int i = ToolUtil.dp2px(getActivity(), 10F);
                obj.right = ((Rect) (obj)).right + i;
                obj.left = ((Rect) (obj)).left - i;
                obj = new TouchDelegate(((Rect) (obj)), view1);
                if (android/view/View.isInstance(view1.getParent()))
                {
                    ((View)view1.getParent()).setTouchDelegate(((TouchDelegate) (obj)));
                }
            }
        }

        _cls1()
        {
            this$0 = CategoryTagFragmentNew.this;
            view = view1;
            super();
        }
    }


    private class _cls3
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final CategoryTagFragmentNew this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f1, int j)
        {
        }

        public void onPageSelected(int i)
        {
            mFocusIndex = i;
        }

        _cls3()
        {
            this$0 = CategoryTagFragmentNew.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final CategoryTagFragmentNew this$0;

        public void onClick(View view)
        {
            loadDataListData();
        }

        _cls4()
        {
            this$0 = CategoryTagFragmentNew.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final CategoryTagFragmentNew this$0;

        public void onClick(View view)
        {
            if (mFrom == 5)
            {
                setPlayPath("play_search");
                view = new Bundle();
                view.putInt("key", key);
                startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoSearchFragment, view);
                return;
            }
            if (mFrom == 7 || mFrom == 6)
            {
                view = new Bundle();
                view.putInt("key", key);
                view.putInt("from", mFrom);
                startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoSearchFragment, view);
                return;
            } else
            {
                setPlayPath("play_search");
                Bundle bundle = new Bundle();
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/search/WordAssociatedFragment, bundle);
                return;
            }
        }

        _cls5()
        {
            this$0 = CategoryTagFragmentNew.this;
            super();
        }
    }


    private class _cls6
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CategoryTagFragmentNew this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            adapterview = (CategoryTag)dataList.get(i);
            if (((CategoryTag) (adapterview)).category_id == -2)
            {
                adapterview = new Intent(tname, com/ximalaya/ting/android/activity/web/WebActivityNew);
                adapterview.putExtra("ExtraUrl", e.ai);
                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                ((MainTabActivity2)tname).startActivity(adapterview);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("categoryId", categoryId);
            bundle.putString("categoryName", categoryName);
            bundle.putString("contentType", contentType);
            bundle.putString("categoryTitle", title);
            bundle.putBoolean("isSerialized", mIsSerialized);
            if (((CategoryTag) (adapterview)).category_id != -1)
            {
                bundle.putString("tagName", ((CategoryTag) (adapterview)).tname);
            }
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            if (key != -1)
            {
                bundle.putInt("key", key);
            }
            if (mFrom != -1)
            {
                bundle.putInt("from", mFrom);
            }
            startFragment(com/ximalaya/ting/android/fragment/finding2/category/CategoryDetailFragment, bundle);
        }

        _cls6()
        {
            this$0 = CategoryTagFragmentNew.this;
            super();
        }
    }


    private class _cls8 extends MyAsyncTask
    {

        final CategoryTagFragmentNew this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            Object obj;
            Object obj1;
            Object obj2;
            Object obj3;
            avoid = (new StringBuilder()).append(a.u).append("mobile/discovery/v1/category/tags").toString();
            obj = new RequestParams();
            ((RequestParams) (obj)).put("categoryId", categoryId);
            ((RequestParams) (obj)).put("contentType", contentType);
            obj3 = f.a().a(avoid, ((RequestParams) (obj)), mGridView, mGridView, true);
            obj2 = null;
            obj1 = null;
            obj = obj1;
            avoid = obj2;
            if (((com.ximalaya.ting.android.b.n.a) (obj3)).b != 1)
            {
                break MISSING_BLOCK_LABEL_223;
            }
            obj = obj1;
            avoid = obj2;
            if (TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj3)).a))
            {
                break MISSING_BLOCK_LABEL_223;
            }
            avoid = obj2;
            String s;
            try
            {
                obj3 = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj3)).a);
            }
            catch (Exception exception)
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
                return avoid;
            }
            avoid = obj2;
            s = ((JSONObject) (obj3)).get("ret").toString();
            avoid = obj2;
            if (totalCount != 0)
            {
                break MISSING_BLOCK_LABEL_175;
            }
            avoid = obj2;
            totalCount = ((JSONObject) (obj3)).getIntValue("count");
            obj = obj1;
            avoid = obj2;
            if (!"0".equals(s))
            {
                break MISSING_BLOCK_LABEL_223;
            }
            avoid = obj2;
            obj = JSON.parseArray(((JSONObject) (obj3)).getString("list"), com/ximalaya/ting/android/model/category/CategoryTag);
            avoid = ((Void []) (obj));
            mHasRecommendZones = ((JSONObject) (obj3)).getBooleanValue("hasRecommendedZones");
            return ((List) (obj));
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(final List result)
        {
            if (mContext == null || !isAdded())
            {
                mLoadingLayout.setVisibility(4);
                mGridView.setVisibility(0);
                return;
            }
            loadingNextPage = false;
            if (result == null)
            {
                showReloadLayout(true);
                mLoadingLayout.setVisibility(4);
                mGridView.setVisibility(0);
                return;
            } else
            {
                class _cls1
                    implements MyCallback
                {

                    final _cls8 this$1;
                    final List val$result;

                    public void execute()
                    {
                        dataList.clear();
                        dataList.addAll(getDefaultTags());
                        dataList.addAll(result);
                        mSoundsHotAdapter.notifyDataSetChanged();
                        mLoadingLayout.setVisibility(4);
                        mGridView.setVisibility(0);
                    }

                _cls1()
                {
                    this$1 = _cls8.this;
                    result = list;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                showReloadLayout(false);
                return;
            }
        }

        protected void onPreExecute()
        {
            loadingNextPage = true;
            showReloadLayout(false);
            loginInfoModel = UserInfoMannage.getInstance().getUser();
            mLoadingLayout.setVisibility(0);
            mGridView.setVisibility(4);
        }

        _cls8()
        {
            this$0 = CategoryTagFragmentNew.this;
            super();
        }
    }


    private class _cls7 extends com.ximalaya.ting.android.b.a
    {

        final CategoryTagFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mFocusImageRoot);
        }

        public void onNetError(int i, String s)
        {
            readFocusImgFromPrefAndDisplay();
        }

        public void onStart()
        {
            super.onStart();
            if (mFocusImages.size() == 0)
            {
                mFocusLoading.setImageResource(0x7f02025e);
                mFocusLoading.setVisibility(0);
            }
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                readFocusImgFromPrefAndDisplay();
            } else
            {
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    readFocusImgFromPrefAndDisplay();
                    return;
                }
                if (s == null || s.getIntValue("ret") != 0)
                {
                    readFocusImgFromPrefAndDisplay();
                    return;
                }
                s = s.getString("list");
                if (TextUtils.isEmpty(s))
                {
                    removeFocusImgFromPrefAndHideView();
                    return;
                }
                List list = JSON.parseArray(s, com/ximalaya/ting/android/modelnew/FocusImageModelNew);
                if (list != null && list.size() > 0)
                {
                    mFocusImageRoot.setVisibility(0);
                    saveFocusImgToPref(s);
                    mFocusImages.clear();
                    mFocusImages.addAll(list);
                    if (list.size() == 1)
                    {
                        ((FocusImageAdapter)mFocusAdapter).setOnlyOnePageFlag(true);
                    }
                    if (list.size() == 2 || list.size() == 3)
                    {
                        isFakeData = true;
                        mFocusImages.addAll(list);
                    }
                    updateFocusImageBar();
                    if (mIsResumeDone)
                    {
                        ThirdAdStatUtil.getInstance().statFocusAd(mFocusImages, false);
                        return;
                    }
                } else
                {
                    removeFocusImgFromPrefAndHideView();
                    return;
                }
            }
        }

        _cls7()
        {
            this$0 = CategoryTagFragmentNew.this;
            super();
        }
    }

}
