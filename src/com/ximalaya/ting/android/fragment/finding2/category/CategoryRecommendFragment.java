// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.FocusImageAdapter;
import com.ximalaya.ting.android.animation.FixedSpeedScroller;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.fragment.finding2.category.loader.CategoryRecommendLoader;
import com.ximalaya.ting.android.fragment.subject.SubjectFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.ad.BannerAdController;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.category.CategoryTag;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumModel;
import com.ximalaya.ting.android.model.category.detail.CategoryRecmdModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdFocusImgGroup;
import com.ximalaya.ting.android.modelnew.FocusImageModelNew;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.MyGridView;
import com.ximalaya.ting.android.view.viewpager.ViewPagerInScroll;
import com.ximalaya.ting.android.view.viewpagerindicator.CirclePageIndicator;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryRecommendAdapter, CategoryDetailFragment

public class CategoryRecommendFragment extends BaseFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.ReloadFragment.Callback
{
    private static class CategoryTagAdapter extends BaseAdapter
    {

        private Context mCtx;
        private List mDataList;

        public int getCount()
        {
            if (mDataList == null)
            {
                return 0;
            } else
            {
                return mDataList.size();
            }
        }

        public Object getItem(int i)
        {
            if (mDataList == null)
            {
                return null;
            } else
            {
                return (CategoryTag)mDataList.get(i);
            }
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            view = (RelativeLayout)LayoutInflater.from(mCtx).inflate(0x7f030051, null);
            ((TextView)view.findViewById(0x7f0a0175)).setText(((CategoryTag)mDataList.get(i)).tname);
            view.setTag(mDataList.get(i));
            return view;
        }

        public CategoryTagAdapter(List list, Context context)
        {
            mDataList = list;
            mCtx = context;
        }
    }


    private static long SWAP_TIME_SLICES = 5000L;
    private static String mainTitle;
    private boolean isFakeData;
    private CategoryRecommendAdapter mAdapter;
    private Runnable mAutoSwapRunnable;
    private BannerAdController mBannerAdController;
    MyGridView mCategoryGrid;
    private int mCategoryId;
    private String mContentType;
    private CategoryRecmdModel mData;
    private PagerAdapter mFocusAdapter;
    private View mFocusImageRoot;
    private ArrayList mFocusImages;
    private int mFocusIndex;
    private CirclePageIndicator mFocusIndicator;
    private ImageView mFocusLoading;
    private ViewPagerInScroll mFocusPager;
    ViewGroup mFooterView;
    private LinearLayout mHeaderParent;
    private boolean mIsLoadedData;
    private boolean mIsResumeDone;
    private boolean mIsSerialized;
    private ListView mListView;
    private Loader mLoader;
    private View mLoadingView;
    private View mRanklistView;
    private ViewGroup mTingListArea;

    public CategoryRecommendFragment()
    {
        mIsLoadedData = false;
        mIsSerialized = false;
        mFocusImages = new ArrayList();
        isFakeData = false;
        mFocusIndex = 0;
        mAutoSwapRunnable = new _cls4();
    }

    private void addAd()
    {
        if (a.l)
        {
            mBannerAdController = new BannerAdController(getActivity(), mListView, "cata_banner");
            mBannerAdController.load(this);
            View view = mBannerAdController.getView();
            view.setLayoutParams(new android.widget.LinearLayout.LayoutParams(-1, ((ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 10F) * 2) * 170) / 720));
            mFooterView.addView(view);
        }
    }

    private void displayCategories(CategoryRecmdModel categoryrecmdmodel)
    {
        if (categoryrecmdmodel.getTags() != null && categoryrecmdmodel.getTags().getList() != null && !categoryrecmdmodel.getTags().getList().isEmpty())
        {
            mFooterView.findViewById(0x7f0a0444).setVisibility(8);
            mFooterView.findViewById(0x7f0a0276).setVisibility(0);
            mFooterView.findViewById(0x7f0a0439).setVisibility(0);
            mCategoryGrid.setVisibility(0);
            categoryrecmdmodel = new CategoryTagAdapter(categoryrecmdmodel.getTags().getList(), mCon);
            mCategoryGrid.setAdapter(categoryrecmdmodel);
        }
    }

    private void displayFocusImg(CategoryRecmdModel categoryrecmdmodel)
    {
        if (categoryrecmdmodel.getFocusImages() != null && categoryrecmdmodel.getFocusImages().getList() != null)
        {
            categoryrecmdmodel = categoryrecmdmodel.getFocusImages().getList();
            if (categoryrecmdmodel.isEmpty())
            {
                removeFocusImgFromPrefAndHideView();
            } else
            if (categoryrecmdmodel != null && categoryrecmdmodel.size() > 0)
            {
                mFocusImageRoot.setVisibility(0);
                saveFocusImgToPref(categoryrecmdmodel);
                mFocusImages.clear();
                mFocusImages.addAll(categoryrecmdmodel);
                if (categoryrecmdmodel.size() == 1)
                {
                    ((FocusImageAdapter)mFocusAdapter).setOnlyOnePageFlag(true);
                }
                if (categoryrecmdmodel.size() == 2 || categoryrecmdmodel.size() == 3)
                {
                    isFakeData = true;
                    mFocusImages.addAll(categoryrecmdmodel);
                }
                updateFocusImageBar();
                if (mIsResumeDone)
                {
                    ThirdAdStatUtil.getInstance().statFocusAd(mFocusImages, false);
                    return;
                }
            } else
            {
                removeFocusImgFromPrefAndHideView();
                return;
            }
            return;
        } else
        {
            readFocusImgFromPrefAndDisplay();
            return;
        }
    }

    private void initFocusImage()
    {
        mFocusImageRoot = LayoutInflater.from(mCon).inflate(0x7f03008b, null);
        mFocusImageRoot.setVisibility(8);
        mFocusPager = (ViewPagerInScroll)mFocusImageRoot.findViewById(0x7f0a017a);
        mFocusPager.setDisallowInterceptTouchEventView((ViewGroup)mFocusPager.getParent());
        mFocusIndicator = (CirclePageIndicator)mFocusImageRoot.findViewById(0x7f0a0230);
        mFocusLoading = (ImageView)mFocusImageRoot.findViewById(0x7f0a0275);
        mHeaderParent = new LinearLayout(mCon);
        mHeaderParent.setPadding(0, 0, 0, Utilities.dip2px(mCon, 10F));
        mHeaderParent.addView(mFocusImageRoot);
        mHeaderParent.setOrientation(1);
        mListView.addHeaderView(mHeaderParent);
        Object obj = mFocusImageRoot.getLayoutParams();
        int i = ToolUtil.getScreenWidth(mActivity);
        int j = (int)((float)i * 0.46875F);
        obj.width = i;
        obj.height = j;
        mFocusImageRoot.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        try
        {
            obj = android/support/v4/view/ViewPager.getDeclaredField("mScroller");
            ((Field) (obj)).setAccessible(true);
            FixedSpeedScroller fixedspeedscroller = new FixedSpeedScroller(mFocusPager.getContext(), new DecelerateInterpolator());
            ((Field) (obj)).set(mFocusPager, fixedspeedscroller);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        mFocusAdapter = new FocusImageAdapter(this, mFocusImages, false, mainTitle);
        ((FocusImageAdapter)mFocusAdapter).setCycleScrollFlag(true);
        mFocusPager.setAdapter(mFocusAdapter);
        mFocusIndicator.setViewPager(mFocusPager);
        mFocusIndicator.setOnPageChangeListener(new _cls3());
        obj = new FocusImageModelNew();
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        mFocusImages.add(obj);
        updateFocusImageBar();
    }

    private void initListener()
    {
        mCategoryGrid.setOnItemClickListener(new _cls1());
        mListView.setOnItemClickListener(new _cls2());
    }

    private void initViews()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        mLoadingView = findViewById(0x7f0a012c);
        initFocusImage();
        mRanklistView = LayoutInflater.from(mCon).inflate(0x7f030057, mHeaderParent, false);
        mRanklistView.setVisibility(8);
        mHeaderParent.addView(mRanklistView);
        mTingListArea = (ViewGroup)LayoutInflater.from(mCon).inflate(0x7f0301b8, mHeaderParent, false);
        mTingListArea.setBackgroundColor(0);
        mTingListArea.setPadding(0, ToolUtil.dp2px(mCon, 10F), 0, 0);
        mTingListArea.setVisibility(8);
        mHeaderParent.addView(mTingListArea);
        mFooterView = (ViewGroup)LayoutInflater.from(mCon).inflate(0x7f0300ff, null);
        ((TextView)mFooterView.findViewById(0x7f0a0158)).setText("\u66F4\u591A\u7C7B\u522B");
        mFooterView.findViewById(0x7f0a0439).setVisibility(8);
        mCategoryGrid = (MyGridView)mFooterView.findViewById(0x7f0a043a);
        mCategoryGrid.setVisibility(8);
        addAd();
        mListView.addFooterView(mFooterView);
        mAdapter = new CategoryRecommendAdapter(getActivity(), this, null);
        mListView.setAdapter(mAdapter);
    }

    private void loadData(boolean flag)
    {
        if (getUserVisibleHint() && getView() != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (!flag && mIsLoadedData)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!NetworkUtils.isNetworkAvaliable(getActivity()))
        {
            mLoadingView.setVisibility(8);
            showReload();
            return;
        }
        mLoadingView.setVisibility(0);
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(0, null, this);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(fragmentBaseContainerView, fragmentBaseContainerView);
            return;
        }
        if (!flag) goto _L1; else goto _L3
_L3:
        mLoader = getLoaderManager().restartLoader(0, null, this);
        ((MyAsyncTaskLoader)mLoader).setXDCSBindView(fragmentBaseContainerView, fragmentBaseContainerView);
        return;
        if (mData == null) goto _L1; else goto _L4
_L4:
        setDataForView(mData);
        return;
    }

    public static CategoryRecommendFragment newInstance(int i, String s, boolean flag, String s1)
    {
        mainTitle = s1;
        s1 = new CategoryRecommendFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("category_id", i);
        bundle.putString("content_type", s);
        bundle.putBoolean("is_serialized", flag);
        s1.setArguments(bundle);
        return s1;
    }

    private void readFocusImgFromPrefAndDisplay()
    {
        Object obj = SharedPreferencesUtil.getInstance(mCon).getString("category_focus_img");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            mFocusImageRoot.setVisibility(8);
            return;
        }
        obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/modelnew/FocusImageModelNew);
        if (obj != null && ((List) (obj)).size() > 0)
        {
            mFocusImageRoot.setVisibility(0);
            mFocusImages.clear();
            mFocusImages.addAll(((java.util.Collection) (obj)));
            updateFocusImageBar();
            return;
        } else
        {
            mFocusImageRoot.setVisibility(8);
            return;
        }
    }

    private void removeFocusImgFromPrefAndHideView()
    {
        SharedPreferencesUtil.getInstance(mCon).removeByKey("category_focus_img");
        mFocusImageRoot.setVisibility(8);
    }

    private void saveFocusImgToPref(List list)
    {
        String s = SharedPreferencesUtil.getInstance(mCon).getString("category_focus_img");
        list = JSON.toJSONString(list);
        if (!TextUtils.isEmpty(s) && !s.equals(list))
        {
            SharedPreferencesUtil.getInstance(mCon).saveString("category_focus_img", list);
        }
    }

    private void setDataForView(CategoryRecmdModel categoryrecmdmodel)
    {
        displayFocusImg(categoryrecmdmodel);
        ArrayList arraylist = new ArrayList();
        Object obj = null;
        CategoryAlbumModel categoryalbummodel = null;
        final String title = obj;
        if (categoryrecmdmodel.getCategoryContents() != null)
        {
            title = obj;
            if (categoryrecmdmodel.getCategoryContents().getRet() == 0)
            {
                title = obj;
                if (categoryrecmdmodel.getCategoryContents().getList() != null)
                {
                    title = obj;
                    if (categoryrecmdmodel.getCategoryContents().getList().size() > 0)
                    {
                        List list = categoryrecmdmodel.getCategoryContents().getList();
                        int i = 0;
                        do
                        {
                            title = categoryalbummodel;
                            if (i == list.size())
                            {
                                break;
                            }
                            title = (com.ximalaya.ting.android.model.category.detail.CategoryRecmdModel.CategoryRecmdContent)list.get(i);
                            if (title.getList() != null && title.getList().size() > 0)
                            {
                                if (title.getModuleType() != 2 && title.getModuleType() != 4)
                                {
                                    CategoryRecommendAdapter.RecmdAlbumTitleModel recmdalbumtitlemodel = new CategoryRecommendAdapter.RecmdAlbumTitleModel();
                                    recmdalbumtitlemodel.sortBy = title.getCalcDimension();
                                    recmdalbumtitlemodel.tagName = title.getTagName();
                                    recmdalbumtitlemodel.hasMore = title.isHasMore();
                                    recmdalbumtitlemodel.title = title.getTitle();
                                    arraylist.add(recmdalbumtitlemodel);
                                    arraylist.addAll(title.getList());
                                } else
                                if (title.getModuleType() == 2)
                                {
                                    categoryalbummodel = (CategoryAlbumModel)title.getList().get(0);
                                } else
                                if (title.getModuleType() == 4)
                                {
                                    setTingListData(title);
                                }
                            }
                            i++;
                        } while (true);
                    }
                }
            }
        }
        if (title != null)
        {
            mRanklistView.setVisibility(0);
            ((TextView)mRanklistView.findViewById(0x7f0a0175)).setText(title.getTitle());
            ((TextView)mRanklistView.findViewById(0x7f0a01ae)).setText(title.getSubtitle());
            final String key = title.getKey();
            final String type = title.getContentType();
            title = title.getTitle();
            mRanklistView.setOnClickListener(new _cls5());
        } else
        {
            mRanklistView.setVisibility(8);
        }
        mAdapter.setData(arraylist);
        displayCategories(categoryrecmdmodel);
    }

    private void setTingListData(final com.ximalaya.ting.android.model.category.detail.CategoryRecmdModel.CategoryRecmdContent group)
    {
        mTingListArea.setVisibility(0);
        ((TextView)mTingListArea.findViewById(0x7f0a0158)).setText(group.getTitle());
        View view = mTingListArea.findViewById(0x7f0a0444);
        ViewGroup viewgroup;
        int i;
        if (group.isHasMore())
        {
            i = 0;
        } else
        {
            i = 4;
        }
        view.setVisibility(i);
        mTingListArea.findViewById(0x7f0a0444).setOnClickListener(new _cls6());
        viewgroup = (ViewGroup)mTingListArea.findViewById(0x7f0a005a);
        if (group.getList() != null)
        {
            Iterator iterator = group.getList().iterator();
            while (iterator.hasNext()) 
            {
                final CategoryAlbumModel model = (CategoryAlbumModel)iterator.next();
                ViewGroup viewgroup1 = (ViewGroup)LayoutInflater.from(mCon).inflate(0x7f030140, viewgroup, false);
                viewgroup1.setOnClickListener(new _cls7());
                Object obj = (ImageView)viewgroup1.findViewById(0x7f0a0177);
                ImageManager2.from(mCon).displayImage(((ImageView) (obj)), model.getCoverPathSmall(), 0x7f0202e0);
                TextView textview = (TextView)viewgroup1.findViewById(0x7f0a0449);
                if (model.getTitle() == null)
                {
                    obj = "";
                } else
                {
                    obj = model.getTitle();
                }
                textview.setText(((CharSequence) (obj)));
                ((ImageView)viewgroup1.findViewById(0x7f0a04f1)).setVisibility(8);
                textview = (TextView)viewgroup1.findViewById(0x7f0a01ae);
                if (model.getSubtitle() == null)
                {
                    obj = "";
                } else
                {
                    obj = model.getSubtitle().replace("\r\n", " ");
                }
                textview.setText(((CharSequence) (obj)));
                obj = (TextView)viewgroup1.findViewById(0x7f0a044a);
                if (TextUtils.isEmpty(model.getFootnote()))
                {
                    ((TextView) (obj)).setVisibility(8);
                } else
                {
                    ((TextView) (obj)).setVisibility(0);
                    ((TextView) (obj)).setText(model.getFootnote());
                }
                viewgroup.addView(viewgroup1);
            }
        }
    }

    private void showReload()
    {
        if (mAdapter == null || mAdapter.getCount() == 0)
        {
            mLoadingView.setVisibility(8);
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
        }
    }

    private void startAutoSwapFocusImage()
    {
        fragmentBaseContainerView.postDelayed(mAutoSwapRunnable, SWAP_TIME_SLICES);
    }

    private void stopAutoSwapFocusImage()
    {
        if (fragmentBaseContainerView != null && mAutoSwapRunnable != null)
        {
            fragmentBaseContainerView.removeCallbacks(mAutoSwapRunnable);
        }
    }

    private void toAlbumFragment(long l, View view)
    {
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = l;
        Bundle bundle = new Bundle();
        bundle.putString("album", JSON.toJSONString(albummodel));
        bundle.putInt("from", 2);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
    }

    private void toCategoryDetailAll(CategoryRecommendAdapter.RecmdAlbumTitleModel recmdalbumtitlemodel, View view)
    {
        Bundle bundle = new Bundle();
        bundle.putString("categoryId", (new StringBuilder()).append(mCategoryId).append("").toString());
        bundle.putString("contentType", mContentType);
        bundle.putBoolean("isSerialized", mIsSerialized);
        String s;
        if (recmdalbumtitlemodel.tagName == null)
        {
            s = "";
        } else
        {
            s = recmdalbumtitlemodel.tagName;
        }
        bundle.putString("tagName", s);
        bundle.putString("title", recmdalbumtitlemodel.title);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, "\u66F4\u591A"));
        if (!TextUtils.isEmpty(recmdalbumtitlemodel.sortBy))
        {
            bundle.putString("sort_by", recmdalbumtitlemodel.sortBy);
        }
        startFragment(com/ximalaya/ting/android/fragment/finding2/category/CategoryDetailFragment, bundle);
    }

    private void toSubjectDetail(CategoryAlbumModel categoryalbummodel, View view)
    {
        Bundle bundle = new Bundle();
        bundle.putLong("subjectId", categoryalbummodel.getSpecialId());
        int i = 1;
        if (!TextUtils.isEmpty(categoryalbummodel.getContentType()))
        {
            i = Integer.valueOf(categoryalbummodel.getContentType()).intValue();
        }
        bundle.putInt("contentType", i);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/subject/SubjectFragment, bundle);
    }

    private void updateFocusImageBar()
    {
        Logger.log((new StringBuilder()).append("updateFocusImageBar mFocusIndex =").append(mFocusIndex).toString());
        mFocusLoading.setVisibility(8);
        mFocusLoading.setImageBitmap(null);
        if (mFocusIndex == 0)
        {
            mFocusPager.setCurrentItem(0x3fffffff - 0x3fffffff % mFocusImages.size());
        }
        if (isFakeData)
        {
            mFocusIndicator.setPagerRealCount(mFocusImages.size() / 2);
        } else
        {
            mFocusIndicator.setPagerRealCount(mFocusImages.size());
        }
        isFakeData = false;
        mFocusAdapter.notifyDataSetChanged();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mCategoryId = bundle.getInt("category_id");
            mContentType = bundle.getString("content_type");
            mIsSerialized = bundle.getBoolean("is_serialized", false);
        }
        initViews();
        initListener();
        loadData(false);
    }

    public void onClick(View view)
    {
        if (OneClickHelper.getInstance().onClick(view) && (view.getTag() instanceof CategoryRecommendAdapter.RecmdAlbumTitleModel))
        {
            toCategoryDetailAll((CategoryRecommendAdapter.RecmdAlbumTitleModel)view.getTag(), view);
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        return new CategoryRecommendLoader(getActivity(), mCategoryId, mContentType);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300a2, viewgroup, false);
        mIsResumeDone = false;
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void onLoadFinished(Loader loader, CategoryRecmdModel categoryrecmdmodel)
    {
label0:
        {
            mIsLoadedData = true;
            mLoadingView.setVisibility(8);
            if (loader != null)
            {
                if (categoryrecmdmodel == null)
                {
                    break label0;
                }
                setDataForView(categoryrecmdmodel);
            }
            return;
        }
        showReload();
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (CategoryRecmdModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onPause()
    {
        stopAutoSwapFocusImage();
        if (mBannerAdController != null)
        {
            mBannerAdController.stopSwapAd();
        }
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        startAutoSwapFocusImage();
        if (mBannerAdController != null)
        {
            mBannerAdController.swapAd();
        }
        ThirdAdStatUtil.getInstance().statFocusAd(mFocusImages, false);
        mIsResumeDone = true;
    }

    public void reload(View view)
    {
        mIsLoadedData = false;
        loadData(true);
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        loadData(false);
    }







/*
    static int access$302(CategoryRecommendFragment categoryrecommendfragment, int i)
    {
        categoryrecommendfragment.mFocusIndex = i;
        return i;
    }

*/


/*
    static int access$308(CategoryRecommendFragment categoryrecommendfragment)
    {
        int i = categoryrecommendfragment.mFocusIndex;
        categoryrecommendfragment.mFocusIndex = i + 1;
        return i;
    }

*/








    private class _cls3
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final CategoryRecommendFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f, int j)
        {
        }

        public void onPageSelected(int i)
        {
            mFocusIndex = i;
        }

        _cls3()
        {
            this$0 = CategoryRecommendFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CategoryRecommendFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (OneClickHelper.getInstance().onClick(view) && MainTabActivity2.isMainTabActivityAvaliable())
            {
                if (view.getTag() != null)
                {
                    if (!(view.getTag() instanceof CategoryTag));
                }
                MainTabActivity2.mainTabActivity.switchToTagFragment(((CategoryTag)view.getTag()).tname);
            }
        }

        _cls1()
        {
            this$0 = CategoryRecommendFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CategoryRecommendFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            int j;
label0:
            {
                if (OneClickHelper.getInstance().onClick(view))
                {
                    j = i - mListView.getHeaderViewsCount();
                    if (j >= 0 && j < mAdapter.getCount())
                    {
                        adapterview = ((AdapterView) (mAdapter.getData().get(j)));
                        if (!(adapterview instanceof CategoryRecommendAdapter.RecmdAlbumTitleModel))
                        {
                            break label0;
                        }
                    }
                }
                return;
            }
            i = j - 1;
            do
            {
label1:
                {
                    if (i >= 0)
                    {
                        Object obj = mAdapter.getData().get(i);
                        if (!(obj instanceof CategoryRecommendAdapter.RecmdAlbumTitleModel))
                        {
                            break label1;
                        }
                        DataCollectUtil.bindDataToView(DataCollectUtil.getDataFromView(view, ((CategoryRecommendAdapter.RecmdAlbumTitleModel)obj).title, (new StringBuilder()).append("").append(j - i).toString()), view);
                    }
                    adapterview = (CategoryAlbumModel)adapterview;
                    toAlbumFragment(adapterview.getAlbumId(), view);
                    return;
                }
                i--;
            } while (true);
        }

        _cls2()
        {
            this$0 = CategoryRecommendFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final CategoryRecommendFragment this$0;
        final String val$key;
        final String val$title;
        final String val$type;

        public void onClick(View view)
        {
            Bundle bundle = new Bundle();
            bundle.putString("key", key);
            bundle.putString("title", title);
            bundle.putString("type", type);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, title));
            startFragment(com/ximalaya/ting/android/fragment/finding2/rank/RankDetailFragment, bundle);
        }

        _cls5()
        {
            this$0 = CategoryRecommendFragment.this;
            key = s;
            title = s1;
            type = s2;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final CategoryRecommendFragment this$0;

        public void onClick(View view)
        {
            view = SubjectListFragmentNew.newInstance(mCategoryId);
            startFragment(view);
        }

        _cls6()
        {
            this$0 = CategoryRecommendFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.view.View.OnClickListener
    {

        final CategoryRecommendFragment this$0;
        final com.ximalaya.ting.android.model.category.detail.CategoryRecmdModel.CategoryRecmdContent val$group;
        final CategoryAlbumModel val$model;

        public void onClick(View view)
        {
            DataCollectUtil.bindDataToView(DataCollectUtil.getDataFromView(view, group.getTitle(), (new StringBuilder()).append("").append(group.getList().indexOf(model) + 1).toString()), view);
            toSubjectDetail(model, view);
        }

        _cls7()
        {
            this$0 = CategoryRecommendFragment.this;
            group = categoryrecmdcontent;
            model = categoryalbummodel;
            super();
        }
    }

}
