// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.adapter.FocusImageAdapter;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.modelnew.FocusImageModelNew;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryTagFragmentNew

class this._cls0 extends a
{

    final CategoryTagFragmentNew this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, CategoryTagFragmentNew.access$2200(CategoryTagFragmentNew.this));
    }

    public void onNetError(int i, String s)
    {
        CategoryTagFragmentNew.access$2000(CategoryTagFragmentNew.this);
    }

    public void onStart()
    {
        super.onStart();
        if (CategoryTagFragmentNew.access$1800(CategoryTagFragmentNew.this).size() == 0)
        {
            CategoryTagFragmentNew.access$1900(CategoryTagFragmentNew.this).setImageResource(0x7f02025e);
            CategoryTagFragmentNew.access$1900(CategoryTagFragmentNew.this).setVisibility(0);
        }
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            CategoryTagFragmentNew.access$2000(CategoryTagFragmentNew.this);
        } else
        {
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                CategoryTagFragmentNew.access$2000(CategoryTagFragmentNew.this);
                return;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                CategoryTagFragmentNew.access$2000(CategoryTagFragmentNew.this);
                return;
            }
            s = s.getString("list");
            if (TextUtils.isEmpty(s))
            {
                CategoryTagFragmentNew.access$2100(CategoryTagFragmentNew.this);
                return;
            }
            List list = JSON.parseArray(s, com/ximalaya/ting/android/modelnew/FocusImageModelNew);
            if (list != null && list.size() > 0)
            {
                CategoryTagFragmentNew.access$2200(CategoryTagFragmentNew.this).setVisibility(0);
                CategoryTagFragmentNew.access$2300(CategoryTagFragmentNew.this, s);
                CategoryTagFragmentNew.access$1800(CategoryTagFragmentNew.this).clear();
                CategoryTagFragmentNew.access$1800(CategoryTagFragmentNew.this).addAll(list);
                if (list.size() == 1)
                {
                    ((FocusImageAdapter)CategoryTagFragmentNew.access$200(CategoryTagFragmentNew.this)).setOnlyOnePageFlag(true);
                }
                if (list.size() == 2 || list.size() == 3)
                {
                    CategoryTagFragmentNew.access$2402(CategoryTagFragmentNew.this, true);
                    CategoryTagFragmentNew.access$1800(CategoryTagFragmentNew.this).addAll(list);
                }
                CategoryTagFragmentNew.access$2500(CategoryTagFragmentNew.this);
                if (CategoryTagFragmentNew.access$2600(CategoryTagFragmentNew.this))
                {
                    ThirdAdStatUtil.getInstance().statFocusAd(CategoryTagFragmentNew.access$1800(CategoryTagFragmentNew.this), false);
                    return;
                }
            } else
            {
                CategoryTagFragmentNew.access$2100(CategoryTagFragmentNew.this);
                return;
            }
        }
    }

    ()
    {
        this$0 = CategoryTagFragmentNew.this;
        super();
    }
}
