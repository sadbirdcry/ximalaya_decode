// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category.loader;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.category.detail.CategoryTrackListModel;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;

public class CategoryTrackLoader extends MyAsyncTaskLoader
{

    public static final String PATH = "mobile/discovery/v1/category/track";
    private String categoryId;
    private CategoryTrackListModel mData;
    private int pageId;
    private int pageSize;
    private String tagName;
    private int totalCount;

    public CategoryTrackLoader(Context context, String s, String s1, int i, int j)
    {
        super(context);
        pageId = i;
        pageSize = j;
        categoryId = s;
        tagName = s1;
    }

    public void deliverResult(CategoryTrackListModel categorytracklistmodel)
    {
        super.deliverResult(categorytracklistmodel);
        mData = categorytracklistmodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((CategoryTrackListModel)obj);
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public CategoryTrackListModel loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("pageId", pageId);
        ((RequestParams) (obj)).put("pageSize", pageSize);
        ((RequestParams) (obj)).put("categoryId", categoryId);
        if (TextUtils.isEmpty(tagName))
        {
            ((RequestParams) (obj)).put("tagName", "");
        } else
        {
            ((RequestParams) (obj)).put("tagName", tagName);
        }
        obj = f.a().a("mobile/discovery/v1/category/track", ((RequestParams) (obj)), fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_122;
        }
        obj = (CategoryTrackListModel)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a, com/ximalaya/ting/android/model/category/detail/CategoryTrackListModel);
        if (obj != null)
        {
            try
            {
                if (((CategoryTrackListModel) (obj)).ret == 0)
                {
                    mData = ((CategoryTrackListModel) (obj));
                }
            }
            catch (Exception exception)
            {
                Logger.e(exception);
            }
        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
