// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import com.ximalaya.ting.android.model.category.CategoryTag;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.lang.ref.WeakReference;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryContentFragment, CategoryRecommendFragment, CategoryDetailFragment

private static class from extends FragmentStatePagerAdapter
{

    private String from;
    private View fromBindView;
    private int mCategoryId;
    private mConfig mConfig;
    private String mContentType;
    private List mData;
    private SparseArray mFragmentRefs;
    private boolean mIsSerialized;

    public void destroyItem(ViewGroup viewgroup, int i, Object obj)
    {
        super.destroyItem(viewgroup, i, obj);
        mFragmentRefs.remove(i);
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public List getData()
    {
        return mData;
    }

    public Fragment getFragmentAt(int i)
    {
        WeakReference weakreference = (WeakReference)mFragmentRefs.get(i);
        if (weakreference != null)
        {
            return (Fragment)weakreference.get();
        } else
        {
            return null;
        }
    }

    public Fragment getItem(int i)
    {
        Object obj;
        if (i == 0)
        {
            obj = CategoryRecommendFragment.newInstance(mCategoryId, mContentType, mIsSerialized, from);
            ((Fragment) (obj)).getArguments().putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(fromBindView));
        } else
        {
            obj = (CategoryTag)mData.get(i);
            obj = CategoryDetailFragment.newInstance((new StringBuilder()).append(mCategoryId).append("").toString(), mContentType, ((CategoryTag) (obj)).tname, mIsSerialized, false);
            ((CategoryDetailFragment)obj).setSortBy(CategoryContentFragment.access$1300(mConfig));
            ((CategoryDetailFragment)obj).setBookStatus(mConfig.tus);
        }
        mFragmentRefs.put(i, new WeakReference(obj));
        return ((Fragment) (obj));
    }

    public CharSequence getPageTitle(int i)
    {
        return ((CategoryTag)mData.get(i)).tname;
    }

    public int getTagPosition(String s)
    {
        if (mData != null)
        {
            int j = mData.size();
            for (int i = 0; i != j; i++)
            {
                if (TextUtils.equals(((CategoryTag)mData.get(i)).tname, s))
                {
                    return i;
                }
            }

        }
        return -1;
    }

    public void setConfig(mData mdata)
    {
        mConfig = mdata;
    }

    public (FragmentManager fragmentmanager, List list, int i, String s, boolean flag, View view)
    {
        super(fragmentmanager);
        mIsSerialized = false;
        mData = list;
        mCategoryId = i;
        mIsSerialized = flag;
        mContentType = s;
        mFragmentRefs = new SparseArray();
        fromBindView = view;
    }

    public fromBindView(FragmentManager fragmentmanager, List list, int i, String s, boolean flag, View view, String s1)
    {
        super(fragmentmanager);
        mIsSerialized = false;
        mData = list;
        mCategoryId = i;
        mIsSerialized = flag;
        mContentType = s;
        mFragmentRefs = new SparseArray();
        fromBindView = view;
        from = s1;
    }
}
