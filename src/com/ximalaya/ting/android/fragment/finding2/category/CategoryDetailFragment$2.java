// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.ting.android.fragment.device.bluetooth.ximao.XiMaoDeviceAlbumSectionDownloadFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonDeviceAlbumSectionDownloadFragment;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukeDeviceAlbumSectionDownloadFragment;
import com.ximalaya.ting.android.fragment.web.WebFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.model.ad.CategoryAdModel;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumModel;
import com.ximalaya.ting.android.model.category.detail.CategoryTrackModel;
import com.ximalaya.ting.android.modelmanage.AdManager;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryDetailFragment, CategoryDetailAdapter

class this._cls0
    implements android.widget.stener
{

    final CategoryDetailFragment this$0;

    public void onItemClick(final AdapterView ad, View view, int i, long l)
    {
_L2:
        return;
        if (!OneClickHelper.getInstance().onClick(view) || CategoryDetailFragment.access$400(CategoryDetailFragment.this) == null || CategoryDetailFragment.access$400(CategoryDetailFragment.this).getCount() == 0) goto _L2; else goto _L1
_L1:
        i -= mListView.getHeaderViewsCount();
        if (i < 0 || i >= CategoryDetailFragment.access$400(CategoryDetailFragment.this).getCount())
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!CategoryDetailFragment.access$500(CategoryDetailFragment.this))
        {
            break; /* Loop/switch isn't completed */
        }
        ad = (CategoryTrackModel)CategoryDetailFragment.access$400(CategoryDetailFragment.this).getData().get(i);
        if (ad != null)
        {
            if (ad.getType() == 1)
            {
                ad = (CategoryAdModel)ad.getTag();
                if (ad.getLinkType() == 1 && ad.getClickType() == 1)
                {
                    view = new AdCollectData();
                    String s = AdManager.getInstance().getAdIdFromUrl(ad.getLink());
                    view.setAdItemId(s);
                    view.setAdSource("0");
                    view.setAndroidId(ToolUtil.getAndroidId(getActivity().getApplicationContext()));
                    view.setLogType("tingClick");
                    view.setPositionName("android_cata_list");
                    view.setResponseId(s);
                    view.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                    view.setTrackId("-1");
                    view = AdManager.getInstance().getAdRealJTUrl(ad.getLink(), view);
                    if (ad.getOpenlinkType() == 0)
                    {
                        ad = WebFragment.newInstance(view);
                        startFragment(ad);
                        return;
                    }
                    if (ad.getOpenlinkType() == 1)
                    {
                        ad.openInThirdBrowser(getActivity(), view);
                        return;
                    }
                }
            } else
            {
                CategoryDetailFragment.access$600(CategoryDetailFragment.this, i, CategoryDetailFragment.access$400(CategoryDetailFragment.this).getData(), DataCollectUtil.getDataFromView(view));
                return;
            }
        }
        if (true) goto _L2; else goto _L3
_L3:
        ad = (CategoryAlbumModel)CategoryDetailFragment.access$400(CategoryDetailFragment.this).getData().get(i);
        if (ad != null)
        {
            if (ad.getType() != 1)
            {
                continue; /* Loop/switch isn't completed */
            }
            ad = (CategoryAdModel)ad.getTag();
            if (ad.getLinkType() == 1 && ad.getClickType() == 1)
            {
                view = new AdCollectData();
                String s1 = AdManager.getInstance().getAdIdFromUrl(ad.getLink());
                view.setAdItemId(s1);
                view.setAdSource("0");
                view.setAndroidId(ToolUtil.getAndroidId(getActivity().getApplicationContext()));
                view.setLogType("tingClick");
                view.setPositionName("android_cata_list");
                view.setResponseId(s1);
                view.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                view.setTrackId("-1");
                view = AdManager.getInstance().getAdRealJTUrl(ad.getLink(), view);
                if (ad.getOpenlinkType() == 0)
                {
                    class _cls1
                        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
                    {

                        final CategoryDetailFragment._cls2 this$1;

                        public void execute(String s2)
                        {
                            s2 = WebFragment.newInstance(s2);
                            startFragment(s2);
                        }

            _cls1()
            {
                this$1 = CategoryDetailFragment._cls2.this;
                super();
            }
                    }

                    ThirdAdStatUtil.getInstance().execAfterDecorateUrl(view, new _cls1());
                    return;
                }
                if (ad.getOpenlinkType() == 1)
                {
                    class _cls2
                        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
                    {

                        final CategoryDetailFragment._cls2 this$1;
                        final CategoryAdModel val$ad;

                        public void execute(String s2)
                        {
                            ad.openInThirdBrowser(getActivity(), s2);
                        }

            _cls2()
            {
                this$1 = CategoryDetailFragment._cls2.this;
                ad = categoryadmodel;
                super();
            }
                    }

                    ThirdAdStatUtil.getInstance().execAfterDecorateUrl(view, new _cls2());
                    return;
                }
            }
        }
        continue; /* Loop/switch isn't completed */
        if (CategoryDetailFragment.access$700(CategoryDetailFragment.this) == 5 || CategoryDetailFragment.access$700(CategoryDetailFragment.this) == 7 || CategoryDetailFragment.access$700(CategoryDetailFragment.this) == 6) goto _L2; else goto _L4
_L4:
        if (CategoryDetailFragment.access$700(CategoryDetailFragment.this) != 8)
        {
            break; /* Loop/switch isn't completed */
        }
        if (ad != null)
        {
            view = new Bundle();
            view.putSerializable("data", ModelHelper.toAlbumModel(ad));
            startFragment(com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonDeviceAlbumSectionDownloadFragment, view);
            return;
        }
        if (true) goto _L2; else goto _L5
_L5:
        if (CategoryDetailFragment.access$700(CategoryDetailFragment.this) != 9)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!UserInfoMannage.hasLogined())
        {
            class _cls3
                implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
            {

                final CategoryDetailFragment._cls2 this$1;

                public void onExecute()
                {
                    Intent intent = new Intent(CategoryDetailFragment.access$800(this$0), com/ximalaya/ting/android/activity/login/LoginActivity);
                    CategoryDetailFragment.access$900(this$0).startActivity(intent);
                }

            _cls3()
            {
                this$1 = CategoryDetailFragment._cls2.this;
                super();
            }
            }

            (new DialogBuilder(CategoryDetailFragment.access$1000(CategoryDetailFragment.this))).setMessage("\u6DFB\u52A0\u529F\u80FD\u4EC5\u767B\u5F55\u7528\u6237\u624D\u80FD\u4F7F\u7528\u54E6\uFF01").setCancelBtn("\u7A0D\u540E\u518D\u8BF4").setOkBtn("\u53BB\u767B\u5F55", new _cls3()).showConfirm();
            return;
        }
        if (ad != null)
        {
            view = new Bundle();
            view.putSerializable("data", ModelHelper.toAlbumModel(ad));
            startFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeDeviceAlbumSectionDownloadFragment, view);
            return;
        }
        if (true) goto _L2; else goto _L6
_L6:
        if (CategoryDetailFragment.access$700(CategoryDetailFragment.this) == 10)
        {
            if (!UserInfoMannage.hasLogined())
            {
                class _cls4
                    implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                {

                    final CategoryDetailFragment._cls2 this$1;

                    public void onExecute()
                    {
                        Intent intent = new Intent(CategoryDetailFragment.access$1100(this$0), com/ximalaya/ting/android/activity/login/LoginActivity);
                        CategoryDetailFragment.access$1200(this$0).startActivity(intent);
                    }

            _cls4()
            {
                this$1 = CategoryDetailFragment._cls2.this;
                super();
            }
                }

                (new DialogBuilder(CategoryDetailFragment.access$1300(CategoryDetailFragment.this))).setMessage("\u6DFB\u52A0\u529F\u80FD\u4EC5\u767B\u5F55\u7528\u6237\u624D\u80FD\u4F7F\u7528\u54E6\uFF01").setCancelBtn("\u7A0D\u540E\u518D\u8BF4").setOkBtn("\u53BB\u767B\u5F55", new _cls4()).showConfirm();
                return;
            }
            if (ad != null)
            {
                view = new Bundle();
                view.putSerializable("data", ModelHelper.toAlbumModel(ad));
                startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/ximao/XiMaoDeviceAlbumSectionDownloadFragment, view);
                return;
            }
        } else
        {
            CategoryDetailFragment.access$1400(CategoryDetailFragment.this, ad.getAlbumId(), view, (new StringBuilder()).append(i + 1).append("").toString());
            return;
        }
        if (true) goto _L2; else goto _L7
_L7:
    }

    ionDownloadFragment()
    {
        this$0 = CategoryDetailFragment.this;
        super();
    }
}
