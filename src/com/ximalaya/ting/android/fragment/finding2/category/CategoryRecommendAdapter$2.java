// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryRecommendAdapter

final class val.model extends a
{

    final Context val$context;
    final ewHolder val$holder;
    final CategoryAlbumModel val$model;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$holder.collectBtn);
    }

    public void onNetError(int i, String s)
    {
        Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
        if (((CategoryAlbumModel)val$holder.collectBtn.getTag(0x7f090000)).getCId() == val$model.getCId())
        {
            CategoryRecommendAdapter.setCollectStatus(val$holder, val$model.isCCollected());
        }
    }

    public void onSuccess(String s)
    {
        boolean flag = true;
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
_L4:
        return;
_L2:
        s = JSON.parseObject(s);
        if (s == null) goto _L4; else goto _L3
_L3:
        int i = s.getIntValue("ret");
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_149;
        }
        s = val$model;
        if (val$model.isCCollected())
        {
            flag = false;
        }
        s.setCCollected(flag);
        if (((CategoryAlbumModel)val$holder.collectBtn.getTag(0x7f090000)).getCId() == val$model.getCId())
        {
            CategoryRecommendAdapter.setCollectStatus(val$holder, val$model.isCCollected());
        }
        if (val$model.isCCollected())
        {
            s = "\u6536\u85CF\u6210\u529F\uFF01";
        } else
        {
            s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
        }
        try
        {
            Toast.makeText(val$context, s, 0).show();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
        return;
        if (i != 791)
        {
            break MISSING_BLOCK_LABEL_164;
        }
        val$model.setCCollected(true);
        if (((CategoryAlbumModel)val$holder.collectBtn.getTag(0x7f090000)).getCId() == val$model.getCId())
        {
            CategoryRecommendAdapter.setCollectStatus(val$holder, val$model.isCCollected());
        }
        if (s.getString("msg") != null)
        {
            break MISSING_BLOCK_LABEL_232;
        }
        s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
_L6:
        Toast.makeText(val$context, s, 0).show();
        return;
        s = s.getString("msg");
        if (true) goto _L6; else goto _L5
_L5:
    }

    ewHolder()
    {
        val$context = context1;
        val$holder = ewholder;
        val$model = categoryalbummodel;
        super();
    }
}
