// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ListView;
import com.ximalaya.ting.android.c.b;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryDetailFragment, CategoryDetailAdapter

class this._cls0
    implements android.support.v4.app.cks
{

    final CategoryDetailFragment this$0;

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        if (i == 3)
        {
            return new b(getActivity(), (List)bundle.getSerializable("data"));
        } else
        {
            return null;
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        if (CategoryDetailFragment.access$100(CategoryDetailFragment.this) == 1 && CategoryDetailFragment.access$400(CategoryDetailFragment.this) != null && CategoryDetailFragment.access$400(CategoryDetailFragment.this).getData() != null)
        {
            CategoryDetailFragment.access$400(CategoryDetailFragment.this).getData().clear();
        }
        CategoryDetailFragment.access$3000(CategoryDetailFragment.this, list);
        if (CategoryDetailFragment.access$3100(CategoryDetailFragment.this))
        {
            CategoryDetailFragment.access$3200(CategoryDetailFragment.this).setVisibility(0);
        }
        if (list == null)
        {
            loader = new ArrayList();
        } else
        {
            loader = list;
        }
        if (CategoryDetailFragment.access$400(CategoryDetailFragment.this) == null)
        {
            if (CategoryDetailFragment.access$700(CategoryDetailFragment.this) == 5 || CategoryDetailFragment.access$700(CategoryDetailFragment.this) == 7 || CategoryDetailFragment.access$700(CategoryDetailFragment.this) == 6)
            {
                CategoryDetailFragment.access$402(CategoryDetailFragment.this, new CategoryDetailAdapter(getActivity(), CategoryDetailFragment.this, loader, true, true));
                mListView.setAdapter(CategoryDetailFragment.access$400(CategoryDetailFragment.this));
            } else
            {
                CategoryDetailFragment.access$402(CategoryDetailFragment.this, new CategoryDetailAdapter(getActivity(), CategoryDetailFragment.this, loader, true));
                mListView.setAdapter(CategoryDetailFragment.access$400(CategoryDetailFragment.this));
            }
        } else
        {
            CategoryDetailFragment.access$400(CategoryDetailFragment.this).addData(loader);
            CategoryDetailFragment.access$400(CategoryDetailFragment.this).notifyDataSetChanged();
        }
        CategoryDetailFragment.access$400(CategoryDetailFragment.this).notifyDataSetChanged();
    }

    public void onLoaderReset(Loader loader)
    {
    }

    ()
    {
        this$0 = CategoryDetailFragment.this;
        super();
    }
}
