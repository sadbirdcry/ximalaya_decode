// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.fragment.device.DeviceBindingListFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.MyDeviceUtil;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukeContentFragment;
import com.ximalaya.ting.android.fragment.device.doss.DossContentFragment;
import com.ximalaya.ting.android.fragment.device.ximao.KeyInfo;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoContentFragment;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoSearchFragment;
import com.ximalaya.ting.android.fragment.finding2.category.loader.CategoryAlbumLoader;
import com.ximalaya.ting.android.fragment.finding2.category.loader.CategoryTrackLoader;
import com.ximalaya.ting.android.fragment.finding2.recommend.RecommendListFragment;
import com.ximalaya.ting.android.fragment.finding2.recommend.adapter.RecommendItemHolder;
import com.ximalaya.ting.android.fragment.search.WordAssociatedFragment;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.model.ad.CategoryAdModel;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumListModel;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumModel;
import com.ximalaya.ting.android.model.category.detail.CategorySubFieldListModel;
import com.ximalaya.ting.android.model.category.detail.CategoryTrackListModel;
import com.ximalaya.ting.android.model.category.detail.CategoryTrackModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.AdManager;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.category:
//            CategoryDetailAdapter

public class CategoryDetailFragment extends BaseListFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.ReloadFragment.Callback, CategoryDetailAdapter.OnBindingListener, com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{

    public static final String FROM = "from";
    public static final int FROM_DOSS = 7;
    public static final int FROM_DOSS_DOWNLOAD = 8;
    public static final int FROM_DOWNLOAD_ALBUM = 4;
    public static final int FROM_FINDING_CATEGORY = 3;
    public static final int FROM_HOT_ANCHOR = 1;
    public static final int FROM_RECOMMEND_ALBUM = 2;
    public static final int FROM_SHUKE_DOWNLOAD = 9;
    public static final int FROM_TINGSHUBAO = 6;
    public static final int FROM_XIMAO = 5;
    public static final int FROM_XIMAO_DOWNLOAD = 10;
    public static final String KEY = "key";
    private static final int LOAD_AD = 4;
    private static final int LOAD_ALBUM_COLLECT_STATUS = 3;
    private static final int LOAD_DATA_ALBUM = 0;
    private static final int LOAD_DATA_TRACK = 1;
    public static final String TYPE_CLASSIC = "classic";
    public static final String TYPE_HOT = "hot";
    public static final String TYPE_RECENT = "recent";
    private int PAGE_SIZE;
    private RecommendItemHolder h1;
    private RecommendItemHolder h2;
    private RecommendItemHolder h3;
    private RelativeLayout headSelectorLayout;
    private RelativeLayout headSelectorLayoutFloat;
    private RelativeLayout listviewContainer;
    private android.support.v4.app.LoaderManager.LoaderCallbacks mAdLoaderCallback;
    private CategoryDetailAdapter mAdapter;
    protected List mAds;
    private int mBookStatus;
    private String mCategoryId;
    private android.support.v4.app.LoaderManager.LoaderCallbacks mCollectStatusCallbacks;
    private View mContentTitleLayout;
    private TextView mContentTitleTv;
    private View mFilter;
    private View mFilterMask;
    private PopupWindow mFilterWindow;
    private int mFrom;
    private RadioGroup mHeaderRadioGroup;
    private RadioGroup mHeaderRadioGroupFloat;
    private boolean mIsDataLoaded;
    private boolean mIsFistResumed;
    private boolean mIsLoading;
    private boolean mIsSerialized;
    private boolean mIsTrack;
    private boolean mIsloadingAd;
    private int mKey;
    private Loader mLoader;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private int mPageId;
    private boolean mPullToRefresh;
    private ReloadFragment mReloadFragment;
    private boolean mShowHeaders;
    private String mSortBy;
    private View mSubfieldContent;
    private View mSubfieldHeader;
    private View mSubfieldMoreBtn;
    private TextView mSubfieldTitle;
    private LinearLayout mSubfieldView;
    private CategorySubFieldListModel mSubfields;
    private String mTagName;
    private String mTagTitle;
    private int mTotalCount;

    public CategoryDetailFragment()
    {
        mIsTrack = false;
        mShowHeaders = true;
        mPageId = 1;
        PAGE_SIZE = 20;
        mKey = -1;
        mIsFistResumed = false;
        mPullToRefresh = false;
        mAdLoaderCallback = new _cls6();
        mIsDataLoaded = false;
        mIsloadingAd = false;
        mBookStatus = 0;
        mCollectStatusCallbacks = new _cls11();
        mReloadFragment = null;
    }

    private void bindData(boolean flag, RecommendItemHolder recommenditemholder, RecmdItemModel recmditemmodel)
    {
        ImageManager2.from(mCon).displayImage(recommenditemholder.cover, getCoverPath(recmditemmodel), 0x7f0202e0);
        TextView textview = recommenditemholder.name;
        String s;
        if (recmditemmodel.getTrackTitle() == null)
        {
            s = "";
        } else
        {
            s = recmditemmodel.getTrackTitle();
        }
        textview.setText(s);
        if (TextUtils.isEmpty(recmditemmodel.getTitle()))
        {
            recommenditemholder.desc.setVisibility(8);
        } else
        {
            recommenditemholder.desc.setVisibility(0);
            recommenditemholder.desc.setText(recmditemmodel.getTitle());
        }
        if (mIsTrack)
        {
            recommenditemholder.album_edge.setVisibility(8);
            recommenditemholder.ic_play.setVisibility(0);
        } else
        {
            ImageView imageview = recommenditemholder.complete;
            int i;
            if (recmditemmodel.getIsFinished() == 2)
            {
                i = 0;
            } else
            {
                i = 8;
            }
            imageview.setVisibility(i);
            recommenditemholder.ic_play.setVisibility(8);
            recommenditemholder.album_edge.setVisibility(0);
        }
        recommenditemholder.cover.setTag(recmditemmodel);
        recommenditemholder.cover.setOnClickListener(new _cls7());
    }

    private void bindXimaoAlbum(CategoryAlbumModel categoryalbummodel)
    {
        if (XiMaoBTManager.getInstance(mCon).getConnState() == com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTED)
        {
            if (mKey > 0 && mKey <= 3)
            {
                categoryalbummodel = new KeyInfo(mKey, categoryalbummodel.getTitle(), categoryalbummodel.getAlbumId(), categoryalbummodel.getTracks());
                XiMaoComm.bindingAlbum(mCon, mKey, mActivity, categoryalbummodel);
            } else
            {
                showToast("\u7ED1\u5B9A\u5931\u8D25");
            }
        }
        mAdapter.notifyDataSetChanged();
        goBackFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment);
    }

    private CategoryAlbumModel convertAdModelToAlbum(CategoryAdModel categoryadmodel)
    {
        CategoryAlbumModel categoryalbummodel = new CategoryAlbumModel();
        categoryalbummodel.setType(1);
        categoryalbummodel.setTitle(categoryadmodel.getName());
        categoryalbummodel.setCoverMiddle(categoryadmodel.getCover());
        categoryalbummodel.setIntro(categoryadmodel.getDescription());
        categoryalbummodel.setTag(categoryadmodel);
        if (getUserVisibleHint())
        {
            if (!TextUtils.isEmpty(categoryadmodel.getThirdStatUrl()))
            {
                ThirdAdStatUtil.getInstance().thirdAdStatRequest(categoryadmodel.getThirdStatUrl());
            }
            categoryadmodel = AdManager.getInstance().getAdIdFromUrl(categoryadmodel.getLink());
            if (!TextUtils.isEmpty(categoryadmodel))
            {
                AdCollectData adcollectdata = new AdCollectData();
                adcollectdata.setAdItemId(categoryadmodel);
                adcollectdata.setAdSource("0");
                adcollectdata.setAndroidId(ToolUtil.getAndroidId(mCon.getApplicationContext()));
                adcollectdata.setLogType("tingShow");
                adcollectdata.setPositionName("android_cata_list");
                adcollectdata.setResponseId(categoryadmodel);
                adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                adcollectdata.setTrackId("-1");
                DataCollectUtil.getInstance(mCon.getApplicationContext()).statOnlineAd(adcollectdata);
            }
        }
        return categoryalbummodel;
    }

    private CategoryTrackModel convertAdModelToTrack(CategoryAdModel categoryadmodel)
    {
        CategoryTrackModel categorytrackmodel = new CategoryTrackModel();
        categorytrackmodel.setType(1);
        categorytrackmodel.setTitle(categoryadmodel.getName());
        categorytrackmodel.setCoverSmall(categoryadmodel.getCover());
        categorytrackmodel.setNickname(categoryadmodel.getDescription());
        categorytrackmodel.setTag(categoryadmodel);
        if (getUserVisibleHint())
        {
            if (!TextUtils.isEmpty(categoryadmodel.getThirdStatUrl()))
            {
                ThirdAdStatUtil.getInstance().thirdAdStatRequest(categoryadmodel.getThirdStatUrl());
            }
            categoryadmodel = AdManager.getInstance().getAdIdFromUrl(categoryadmodel.getLink());
            if (!TextUtils.isEmpty(categoryadmodel))
            {
                AdCollectData adcollectdata = new AdCollectData();
                adcollectdata.setAdItemId(categoryadmodel);
                adcollectdata.setAdSource("0");
                adcollectdata.setAndroidId(ToolUtil.getAndroidId(mCon.getApplicationContext()));
                adcollectdata.setLogType("tingShow");
                adcollectdata.setPositionName("android_cata_list");
                adcollectdata.setResponseId(categoryadmodel);
                adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                adcollectdata.setTrackId("-1");
                DataCollectUtil.getInstance(mCon.getApplicationContext()).statOnlineAd(adcollectdata);
            }
        }
        return categorytrackmodel;
    }

    private void defaultParams()
    {
        if (TextUtils.isEmpty(mSortBy))
        {
            mSortBy = "hot";
        }
        if (TextUtils.isEmpty(mTagName))
        {
            mTagName = "";
        }
        if (!mIsSerialized)
        {
            mBookStatus = 0;
        }
    }

    private void expandFilter()
    {
        if (mFilterWindow == null)
        {
            mFilterWindow = new PopupWindow(mActivity);
            RelativeLayout relativelayout = (RelativeLayout)LayoutInflater.from(mActivity).inflate(0x7f03003b, null);
            ((RadioGroup)relativelayout.findViewById(0x7f0a014a)).setOnCheckedChangeListener(new _cls8());
            mFilterWindow.setContentView(relativelayout);
            mFilterWindow.setWidth(-1);
            mFilterWindow.setHeight(-2);
            mFilterWindow.setBackgroundDrawable(new BitmapDrawable());
            mFilterWindow.setOutsideTouchable(true);
            mFilterWindow.setOnDismissListener(new _cls9());
            mFilterWindow.setFocusable(true);
        }
        int i = ToolUtil.dp2px(mActivity, 70F);
        mFilterWindow.showAtLocation(mFilter, 48, 0, i);
    }

    private String getCoverPath(RecmdItemModel recmditemmodel)
    {
        if (mIsTrack)
        {
            return recmditemmodel.getCoverMiddle();
        } else
        {
            return recmditemmodel.getCoverLarge();
        }
    }

    private String getSortCondition(int i)
    {
        if (i == 0x7f0a0080)
        {
            return "hot";
        }
        if (i == 0x7f0a0081)
        {
            return "recent";
        }
        if (i == 0x7f0a014b)
        {
            return "classic";
        } else
        {
            return "hot";
        }
    }

    private void gotoPlay(int i, List list, String s)
    {
        Object obj = new ArrayList();
        list = list.iterator();
        do
        {
            if (!list.hasNext())
            {
                break;
            }
            Object obj1 = list.next();
            if (obj1 instanceof CategoryTrackModel)
            {
                ((List) (obj)).add((CategoryTrackModel)obj1);
            }
        } while (true);
        list = ModelHelper.toSoundInfoListForCategoryTrack(((List) (obj)));
        obj = new HashMap();
        if (TextUtils.isEmpty(mTagName))
        {
            ((HashMap) (obj)).put("tagName", "");
        } else
        {
            ((HashMap) (obj)).put("tagName", mTagName);
        }
        ((HashMap) (obj)).put("categoryId", mCategoryId);
        PlayTools.gotoPlay(20, "mobile/discovery/v1/category/track", mPageId + 1, ((HashMap) (obj)), list, i, mActivity, true, s);
    }

    private void hideSubfield()
    {
        if (headSelectorLayout != null)
        {
            headSelectorLayout.setPadding(0, ToolUtil.dp2px(mCon, 10F), 0, ToolUtil.dp2px(mCon, 10F));
        }
        mSubfieldContent.setVisibility(8);
        mSubfieldHeader.setVisibility(8);
        mContentTitleLayout.setVisibility(8);
    }

    private void initData()
    {
        if (getUserVisibleHint() && getView() != null)
        {
            if (!mIsDataLoaded)
            {
                ((PullToRefreshListView)mListView).toRefreshing();
                return;
            }
            if (mAdapter != null)
            {
                if (mIsTrack)
                {
                    insertOrReplaceAdForTrack(mAdapter.getData());
                } else
                {
                    insertOrReplaceAdForAlbum(mAdapter.getData());
                }
                mAdapter.notifyDataSetChanged();
                return;
            }
        }
    }

    private void initDefaultSort()
    {
        if (TextUtils.isEmpty(mSortBy)) goto _L2; else goto _L1
_L1:
        int i = 0x7f0a0080;
        if (!mSortBy.equals("recent")) goto _L4; else goto _L3
_L3:
        i = 0x7f0a0081;
_L6:
        mHeaderRadioGroup.check(i);
        mHeaderRadioGroupFloat.check(i);
_L2:
        return;
_L4:
        if (mSortBy.equals("classic"))
        {
            i = 0x7f0a014b;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    private void initHeadData(RadioGroup radiogroup)
    {
        RadioButton radiobutton = (RadioButton)radiogroup.findViewById(0x7f0a0080);
        radiobutton.setText("\u6700\u706B");
        radiobutton.setTextSize(2, 15F);
        radiobutton = (RadioButton)radiogroup.findViewById(0x7f0a0081);
        radiobutton.setText("\u6700\u65B0");
        radiobutton.setTextSize(2, 15F);
        radiogroup = (RadioButton)radiogroup.findViewById(0x7f0a014b);
        radiogroup.setText("\u7ECF\u5178");
        radiogroup.setTextSize(2, 15F);
    }

    private void initHeader()
    {
        mSubfieldView = (LinearLayout)LayoutInflater.from(getActivity()).inflate(0x7f0301b7, null);
        mSubfieldContent = mSubfieldView.findViewById(0x7f0a06a1);
        mSubfieldContent.setVisibility(8);
        mSubfieldHeader = mSubfieldView.findViewById(0x7f0a06a0);
        mSubfieldHeader.setVisibility(8);
        mSubfieldTitle = (TextView)mSubfieldView.findViewById(0x7f0a0158);
        mSubfieldTitle.setText("\u7CBE\u9009\u4E13\u8F91");
        mSubfieldMoreBtn = mSubfieldView.findViewById(0x7f0a0444);
        h1 = RecommendItemHolder.findView(mSubfieldView.findViewById(0x7f0a06a2));
        h2 = RecommendItemHolder.findView(mSubfieldView.findViewById(0x7f0a06a3));
        h3 = RecommendItemHolder.findView(mSubfieldView.findViewById(0x7f0a06a4));
        resetLayoutWidthAndHeight(h1, h2, h3);
        mContentTitleLayout = LayoutInflater.from(getActivity()).inflate(0x7f030106, null);
        mContentTitleLayout.findViewById(0x7f0a0444).setVisibility(8);
        mContentTitleTv = (TextView)mContentTitleLayout.findViewById(0x7f0a0158);
        TextView textview = mContentTitleTv;
        String s;
        if (mIsTrack)
        {
            s = "\u70ED\u95E8\u58F0\u97F3";
        } else
        {
            s = "\u70ED\u95E8\u4E13\u8F91";
        }
        textview.setText(s);
        mContentTitleLayout.setVisibility(8);
        mSubfieldView.addView(mContentTitleLayout);
        mListView.addHeaderView(mSubfieldView);
    }

    private void initListener()
    {
        findViewById(0x7f0a02e5).setOnClickListener(this);
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls1());
        mListView.setOnItemClickListener(new _cls2());
        ((PullToRefreshListView)mListView).setMyScrollListener2(new _cls3());
        if (mIsSerialized)
        {
            mFilter.setOnClickListener(this);
        }
        mFooterViewLoading.setOnClickListener(new _cls4());
        mSubfieldMoreBtn.setOnClickListener(this);
        registerListener();
    }

    private void initRadioCheckHeader()
    {
        headSelectorLayout = (RelativeLayout)LayoutInflater.from(getActivity()).inflate(0x7f030091, null);
        mHeaderRadioGroup = (RadioGroup)headSelectorLayout.findViewById(0x7f0a0448);
        mListView.addHeaderView(headSelectorLayout);
        initHeadData(mHeaderRadioGroup);
        headSelectorLayoutFloat = (RelativeLayout)LayoutInflater.from(getActivity()).inflate(0x7f030091, null);
        ((PullToRefreshListView)mListView).setFloatHeadView(headSelectorLayoutFloat);
        listviewContainer.addView(headSelectorLayoutFloat);
        mHeaderRadioGroupFloat = (RadioGroup)headSelectorLayoutFloat.findViewById(0x7f0a0448);
        initHeadData(mHeaderRadioGroupFloat);
    }

    private void initRadioGroupListener(RadioGroup radiogroup)
    {
        radiogroup.setOnCheckedChangeListener(new _cls5());
    }

    private void initView()
    {
        if (mShowHeaders)
        {
            if (TextUtils.isEmpty(mTagName))
            {
                if (mFrom == 4)
                {
                    setTitleText("\u70ED\u95E8");
                } else
                {
                    setTitleText("\u5168\u90E8");
                }
            } else
            {
                setTitleText(mTagName);
            }
        } else
        {
            findViewById(0x7f0a0066).setVisibility(8);
        }
        listviewContainer = (RelativeLayout)findViewById(0x7f0a005a);
        if (!mIsTrack && mShowHeaders)
        {
            initRadioCheckHeader();
            initDefaultSort();
            initRadioGroupListener(mHeaderRadioGroup);
            initRadioGroupListener(mHeaderRadioGroupFloat);
        }
        initHeader();
        mFilter = findViewById(0x7f0a02e4);
        if (mIsSerialized)
        {
            mFilter.setVisibility(0);
        } else
        {
            mFilter.setVisibility(8);
        }
        if (mFrom == 5 || mFrom == 7 || mFrom == 6)
        {
            mAdapter = new CategoryDetailAdapter(getActivity(), this, new ArrayList(), true, true);
            mListView.setAdapter(mAdapter);
            return;
        }
        if (mIsTrack)
        {
            mAdapter = new CategoryDetailAdapter(getActivity(), this, new ArrayList(), false);
            mListView.setAdapter(mAdapter);
            return;
        } else
        {
            mAdapter = new CategoryDetailAdapter(getActivity(), this, new ArrayList(), true);
            mListView.setAdapter(mAdapter);
            return;
        }
    }

    private void insertOrReplaceAdForAlbum(List list)
    {
        if (mAds != null && mAds.size() > 0 && list != null && list.size() > 0)
        {
            ArrayList arraylist = new ArrayList();
            Iterator iterator = mAds.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                CategoryAdModel categoryadmodel = (CategoryAdModel)iterator.next();
                if (categoryadmodel.getPosition() >= 0 && categoryadmodel.getPosition() < list.size())
                {
                    if (((CategoryAlbumModel)list.get(categoryadmodel.getPosition())).getType() == 1)
                    {
                        list.set(categoryadmodel.getPosition(), convertAdModelToAlbum(categoryadmodel));
                    } else
                    {
                        list.add(categoryadmodel.getPosition(), convertAdModelToAlbum(categoryadmodel));
                    }
                    arraylist.add(categoryadmodel);
                }
            } while (true);
            mAds.removeAll(arraylist);
        }
    }

    private void insertOrReplaceAdForTrack(List list)
    {
        if (mAds != null && mAds.size() > 0 && list != null && list.size() > 0)
        {
            ArrayList arraylist = new ArrayList();
            Iterator iterator = mAds.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                CategoryAdModel categoryadmodel = (CategoryAdModel)iterator.next();
                if (categoryadmodel.getPosition() >= 0 && categoryadmodel.getPosition() < list.size())
                {
                    if (((CategoryTrackModel)list.get(categoryadmodel.getPosition())).getType() == 1)
                    {
                        list.set(categoryadmodel.getPosition(), convertAdModelToTrack(categoryadmodel));
                    } else
                    {
                        list.add(categoryadmodel.getPosition(), convertAdModelToTrack(categoryadmodel));
                    }
                    arraylist.add(categoryadmodel);
                }
            } while (true);
            mAds.removeAll(arraylist);
        }
    }

    private boolean isShowSubfiled()
    {
        return mSubfields != null && mSubfields.getList() != null && mSubfields.getList().size() >= 3;
    }

    private void loadAd()
    {
        if (mIsloadingAd)
        {
            return;
        } else
        {
            Bundle bundle = new Bundle();
            bundle.putString("category_id", mCategoryId);
            getLoaderManager().restartLoader(4, bundle, mAdLoaderCallback);
            return;
        }
    }

    private void loadDataListData(View view)
    {
        int i = 1;
        if (!mIsLoading)
        {
            mIsLoading = true;
            mIsDataLoaded = true;
            if (!mIsTrack)
            {
                i = 0;
            }
            startLoader(i, this, view);
        }
    }

    public static CategoryDetailFragment newInstance(String s, String s1, String s2, boolean flag, boolean flag1)
    {
        Bundle bundle = new Bundle();
        bundle.putString("categoryId", s);
        bundle.putString("contentType", s1);
        bundle.putBoolean("isSerialized", flag);
        bundle.putString("tagName", s2);
        bundle.putBoolean("showHeaders", flag1);
        s = new CategoryDetailFragment();
        s.setArguments(bundle);
        return s;
    }

    private void parseSubfield(CategorySubFieldListModel categorysubfieldlistmodel)
    {
label0:
        {
label1:
            {
                if (categorysubfieldlistmodel == null || categorysubfieldlistmodel.getList() == null || categorysubfieldlistmodel.getList().size() < 3)
                {
                    break label0;
                }
                if (isShowSubfiled())
                {
                    showSubfield();
                    TextView textview = mSubfieldTitle;
                    String s;
                    if (TextUtils.isEmpty(categorysubfieldlistmodel.getTitle()))
                    {
                        s = "\u7CBE\u9009\u4E13\u8F91";
                    } else
                    {
                        s = categorysubfieldlistmodel.getTitle();
                    }
                    textview.setText(s);
                    bindData(categorysubfieldlistmodel.isTrack(), h1, (RecmdItemModel)categorysubfieldlistmodel.getList().get(0));
                    bindData(categorysubfieldlistmodel.isTrack(), h2, (RecmdItemModel)categorysubfieldlistmodel.getList().get(1));
                    bindData(categorysubfieldlistmodel.isTrack(), h3, (RecmdItemModel)categorysubfieldlistmodel.getList().get(2));
                    if (categorysubfieldlistmodel.getCount() <= 3 || categorysubfieldlistmodel.isTrack())
                    {
                        break label1;
                    }
                    mSubfieldMoreBtn.setVisibility(0);
                }
                return;
            }
            mSubfieldMoreBtn.setVisibility(8);
            return;
        }
        hideSubfield();
    }

    private void registerListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls12();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void resetLayoutWidthAndHeight(RecommendItemHolder recommenditemholder, RecommendItemHolder recommenditemholder1, RecommendItemHolder recommenditemholder2)
    {
        int i = (ToolUtil.getScreenWidth(getActivity()) - ToolUtil.dp2px(getActivity(), 40F)) / 3;
        android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)recommenditemholder.cover.getLayoutParams();
        layoutparams.height = i;
        recommenditemholder.cover.setLayoutParams(layoutparams);
        recommenditemholder = (android.widget.RelativeLayout.LayoutParams)recommenditemholder1.cover.getLayoutParams();
        recommenditemholder.height = i;
        recommenditemholder1.cover.setLayoutParams(recommenditemholder);
        recommenditemholder = (android.widget.RelativeLayout.LayoutParams)recommenditemholder2.cover.getLayoutParams();
        recommenditemholder.height = i;
        recommenditemholder2.cover.setLayoutParams(recommenditemholder);
    }

    private void setPlayListParam()
    {
        HashMap hashmap = new HashMap();
        if (TextUtils.isEmpty(mTagName))
        {
            hashmap.put("tagName", "");
        } else
        {
            hashmap.put("tagName", mTagName);
        }
        hashmap.put("categoryId", mCategoryId);
        mAdapter.setDataSource("mobile/discovery/v1/category/track");
        mAdapter.setPageId(mPageId + 1);
        mAdapter.setParams(hashmap);
    }

    private void showMask()
    {
        if (mFilterMask == null)
        {
            mFilterMask = new View(mCon);
            mFilterMask.setLayoutParams(new android.view.ViewGroup.LayoutParams(-1, -1));
            mFilterMask.setBackgroundColor(Color.parseColor("#000000"));
            mFilterMask.getBackground().setAlpha((int)153.59999999999999D);
        }
        if (mFilterMask.getParent() != null && (mFilterMask.getParent() instanceof ViewGroup))
        {
            ((ViewGroup)mFilterMask.getParent()).removeView(mFilterMask);
        }
        listviewContainer.addView(mFilterMask);
    }

    private void showReload()
    {
        if (mAdapter == null || mAdapter.getCount() == 0)
        {
            mReloadFragment = ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
        }
    }

    private void showSubfield()
    {
        mSubfieldContent.setVisibility(0);
        mSubfieldHeader.setVisibility(0);
        mSubfieldView.findViewById(0x7f0a06a5).setVisibility(0);
        if (headSelectorLayout != null)
        {
            headSelectorLayout.setPadding(0, ToolUtil.dp2px(mCon, 10F), 0, 0);
        }
    }

    private void startLoader(int i, android.support.v4.app.LoaderManager.LoaderCallbacks loadercallbacks, View view)
    {
        startLoader(i, loadercallbacks, view, null);
    }

    private void startLoader(int i, android.support.v4.app.LoaderManager.LoaderCallbacks loadercallbacks, View view, Bundle bundle)
    {
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(i, bundle, loadercallbacks);
        } else
        {
            mLoader = getLoaderManager().restartLoader(i, bundle, loadercallbacks);
        }
        if (i == 0)
        {
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
        } else
        if (i == 1)
        {
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
            return;
        }
    }

    private void toAlbumFragment(long l, View view)
    {
        Bundle bundle;
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = l;
        bundle = new Bundle();
        bundle.putString("album", JSON.toJSONString(albummodel));
        mFrom;
        JVM INSTR tableswitch 2 3: default 64
    //                   2 104
    //                   3 98;
           goto _L1 _L2 _L3
_L1:
        int i = 0;
_L5:
        bundle.putInt("from", i);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
        return;
_L3:
        i = 2;
        continue; /* Loop/switch isn't completed */
_L2:
        i = 6;
        if (true) goto _L5; else goto _L4
_L4:
    }

    private void toAlbumFragment(long l, View view, String s)
    {
        Bundle bundle;
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = l;
        bundle = new Bundle();
        bundle.putString("album", JSON.toJSONString(albummodel));
        mFrom;
        JVM INSTR tableswitch 2 3: default 64
    //                   2 124
    //                   3 118;
           goto _L1 _L2 _L3
_L2:
        break MISSING_BLOCK_LABEL_124;
_L1:
        int i = 0;
_L4:
        bundle.putInt("from", i);
        String s1;
        if (TextUtils.isEmpty(mTagName))
        {
            s1 = mTagTitle;
        } else
        {
            s1 = mTagName;
        }
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, s1, s));
        startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
        return;
_L3:
        i = 2;
          goto _L4
        i = 6;
          goto _L4
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(this);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    public void doCollect(Context context, CategoryAlbumModel categoryalbummodel)
    {
        mFrom;
        JVM INSTR tableswitch 5 7: default 32
    //                   5 105
    //                   6 33
    //                   7 33;
           goto _L1 _L2 _L3 _L3
_L1:
        return;
_L3:
        if ((context = MyDeviceManager.getInstance(mActivity)).isConn())
        {
            context.bindDevice(mKey, ModelHelper.toAlbumModel(categoryalbummodel), getActivity());
            if (mFrom == 6)
            {
                goBackFragment(com/ximalaya/ting/android/fragment/device/DeviceBindingListFragment);
                return;
            }
            if (mFrom == 7)
            {
                goBackFragment(com/ximalaya/ting/android/fragment/device/doss/DossContentFragment);
                goBackFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeContentFragment);
                return;
            }
        }
          goto _L1
_L2:
        bindXimaoAlbum(categoryalbummodel);
        return;
    }

    public int getBookStatus()
    {
        return mBookStatus;
    }

    public String getSortBy()
    {
        return mSortBy;
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    protected void loadMoreData(View view)
    {
        if (mPageId * PAGE_SIZE < mTotalCount)
        {
            mPageId = mPageId + 1;
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
            loadDataListData(view);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getArguments();
        if (bundle != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mCategoryId = bundle.getString("categoryId");
        mIsSerialized = bundle.getBoolean("isSerialized", false);
        mTagName = bundle.getString("tagName");
        mTagTitle = bundle.getString("title");
        mIsTrack = "track".equals(bundle.getString("contentType"));
        mFrom = bundle.getInt("from");
        if (getArguments().containsKey("key"))
        {
            mKey = getArguments().getInt("key");
        } else
        {
            mKey = -1;
        }
        if (android.os.Build.VERSION.SDK_INT < 12)
        {
            break; /* Loop/switch isn't completed */
        }
        mSortBy = bundle.getString("sort_by", "");
_L4:
        initView();
        initListener();
        initData();
        if (MyDeviceUtil.isFromDeviceDownload(mFrom))
        {
            fragmentBaseContainerView.findViewById(0x7f0a02e5).setVisibility(4);
            mListView.removeHeaderView(mSubfieldView);
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        mSortBy = bundle.getString("sort_by");
        if (mSortBy == null)
        {
            mSortBy = "";
        }
          goto _L4
        if (true) goto _L1; else goto _L5
_L5:
    }

    public void onClick(View view)
    {
        if (OneClickHelper.getInstance().onClick(view))
        {
            switch (view.getId())
            {
            default:
                return;

            case 2131362532: 
                expandFilter();
                showMask();
                return;

            case 2131362533: 
                if (mFrom == 5)
                {
                    setPlayPath("play_search");
                    view = new Bundle();
                    view.putInt("key", mKey);
                    startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoSearchFragment, view);
                    return;
                }
                if (mFrom == 7 || mFrom == 6)
                {
                    view = new Bundle();
                    view.putInt("key", mKey);
                    view.putInt("from", mFrom);
                    startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoSearchFragment, view);
                    return;
                } else
                {
                    setPlayPath("play_search");
                    Bundle bundle = new Bundle();
                    bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                    startFragment(com/ximalaya/ting/android/fragment/search/WordAssociatedFragment, bundle);
                    return;
                }

            case 2131362884: 
                break;
            }
            if (mSubfields != null)
            {
                Bundle bundle1 = new Bundle();
                bundle1.putInt("loader_type", 2);
                String s;
                if (mSubfields == null)
                {
                    s = "\u7CBE\u9009\u4E13\u8F91";
                } else
                {
                    s = mSubfields.getTitle();
                }
                bundle1.putString("title", s);
                bundle1.putString("categoryId", mCategoryId);
                bundle1.putString("tagName", mTagName);
                bundle1.putString("subfieldName", mSubfields.getTitle());
                bundle1.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/finding2/recommend/RecommendListFragment, bundle1);
                return;
            }
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mShowHeaders = bundle.getBoolean("showHeaders", true);
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        switch (i)
        {
        default:
            return null;

        case 0: // '\0'
            defaultParams();
            return new CategoryAlbumLoader(getActivity(), mCategoryId, mTagName, mSortBy, mBookStatus, mPageId, PAGE_SIZE);

        case 1: // '\001'
            return new CategoryTrackLoader(getActivity(), mCategoryId, mTagName, mPageId, PAGE_SIZE);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        if (mShowHeaders)
        {
            fragmentBaseContainerView = layoutinflater.inflate(0x7f0300a0, viewgroup, false);
        } else
        {
            fragmentBaseContainerView = layoutinflater.inflate(0x7f0300a1, viewgroup, false);
        }
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        unRegisterListener();
        mIsFistResumed = false;
    }

    public void onLoadFinished(Loader loader, final Object data)
    {
        ((PullToRefreshListView)mListView).onRefreshComplete();
        ReloadFragment.hide(getChildFragmentManager(), mReloadFragment);
        if (loader != null && data != null)
        {
            doAfterAnimation(new _cls10());
        } else
        {
            showReload();
        }
        mIsLoading = false;
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onPlayCanceled()
    {
    }

    public void onRefresh()
    {
        ((PullToRefreshListView)mListView).toRefreshing();
    }

    public void onResume()
    {
        super.onResume();
        if (mFrom == 5 || mFrom == 7 || mFrom == 6)
        {
            mAdapter.setBindingListener(this);
        }
        if (mFrom == 5 && XiMaoBTManager.getInstance(mCon).getConnState() != com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTED)
        {
            getActivity().onBackPressed();
        } else
        if (mIsFistResumed)
        {
            if (getUserVisibleHint())
            {
                loadAd();
                return;
            }
        } else
        {
            mIsFistResumed = true;
            return;
        }
    }

    public void onSoundChanged(int i)
    {
        if (mIsTrack && mAdapter != null)
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
    }

    protected boolean parseData(Object obj)
    {
        boolean flag = true;
        if (obj instanceof CategoryAlbumListModel)
        {
            CategoryAlbumListModel categoryalbumlistmodel = (CategoryAlbumListModel)obj;
            mTotalCount = categoryalbumlistmodel.getTotalCount();
            TextView textview = mContentTitleTv;
            if (TextUtils.isEmpty(categoryalbumlistmodel.getTitle()))
            {
                obj = "\u70ED\u95E8\u4E13\u8F91";
            } else
            {
                obj = categoryalbumlistmodel.getTitle();
            }
            textview.setText(((CharSequence) (obj)));
            if (categoryalbumlistmodel.getList() != null && !categoryalbumlistmodel.getList().isEmpty())
            {
                flag = true;
            } else
            {
                flag = false;
            }
            if (categoryalbumlistmodel.getSubfields() == null || categoryalbumlistmodel.getSubfields().isEmpty())
            {
                obj = null;
            } else
            {
                obj = (CategorySubFieldListModel)categoryalbumlistmodel.getSubfields().get(0);
            }
            mSubfields = ((CategorySubFieldListModel) (obj));
            if (mPageId == 1)
            {
                parseSubfield(mSubfields);
            }
            return flag;
        }
        if (obj instanceof CategoryTrackListModel)
        {
            if (mPageId == 1 && mAdapter != null && mAdapter.getData() != null)
            {
                mAdapter.getData().clear();
            }
            CategoryTrackListModel categorytracklistmodel = (CategoryTrackListModel)obj;
            TextView textview1 = mContentTitleTv;
            Object obj1;
            if (TextUtils.isEmpty(categorytracklistmodel.getTitle()))
            {
                obj1 = "\u70ED\u95E8\u58F0\u97F3";
            } else
            {
                obj1 = categorytracklistmodel.getTitle();
            }
            textview1.setText(((CharSequence) (obj1)));
            if (categorytracklistmodel.getSubfields() == null || categorytracklistmodel.getSubfields().isEmpty())
            {
                obj1 = null;
            } else
            {
                obj1 = (CategorySubFieldListModel)categorytracklistmodel.getSubfields().get(0);
            }
            mSubfields = ((CategorySubFieldListModel) (obj1));
            if (mPageId == 1)
            {
                parseSubfield(mSubfields);
            }
            obj1 = categorytracklistmodel.getList();
            mTotalCount = ((CategoryTrackListModel)obj).getTotalCount();
            insertOrReplaceAdForTrack(((List) (obj1)));
            if (isShowSubfiled())
            {
                mContentTitleLayout.setVisibility(0);
            }
            obj = obj1;
            if (obj1 == null)
            {
                obj = new ArrayList();
            }
            if (mAdapter == null)
            {
                mAdapter = new CategoryDetailAdapter(getActivity(), this, ((List) (obj)), false);
                mListView.setAdapter(mAdapter);
            } else
            {
                mAdapter.addData(((List) (obj)));
                mAdapter.notifyDataSetChanged();
            }
            setPlayListParam();
            if (obj == null || ((List) (obj)).isEmpty())
            {
                flag = false;
            }
            return flag;
        } else
        {
            return false;
        }
    }

    public void reload(View view)
    {
        ((PullToRefreshListView)mListView).toRefreshing();
    }

    public void reloadAd()
    {
        if (mAdapter != null && mAdapter.getCount() > 0)
        {
            loadAd();
        }
    }

    public void setBookStatus(int i)
    {
        mBookStatus = i;
    }

    public void setSortBy(String s)
    {
        mSortBy = s;
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        initData();
    }

    public void syncConfig()
    {
        loadDataListData(mListView);
    }





/*
    static int access$102(CategoryDetailFragment categorydetailfragment, int i)
    {
        categorydetailfragment.mPageId = i;
        return i;
    }

*/










/*
    static String access$1802(CategoryDetailFragment categorydetailfragment, String s)
    {
        categorydetailfragment.mSortBy = s;
        return s;
    }

*/





/*
    static boolean access$202(CategoryDetailFragment categorydetailfragment, boolean flag)
    {
        categorydetailfragment.mPullToRefresh = flag;
        return flag;
    }

*/


/*
    static boolean access$2102(CategoryDetailFragment categorydetailfragment, boolean flag)
    {
        categorydetailfragment.mIsloadingAd = flag;
        return flag;
    }

*/




/*
    static int access$2402(CategoryDetailFragment categorydetailfragment, int i)
    {
        categorydetailfragment.mBookStatus = i;
        return i;
    }

*/












/*
    static CategoryDetailAdapter access$402(CategoryDetailFragment categorydetailfragment, CategoryDetailAdapter categorydetailadapter)
    {
        categorydetailfragment.mAdapter = categorydetailadapter;
        return categorydetailadapter;
    }

*/






    private class _cls6
        implements android.support.v4.app.LoaderManager.LoaderCallbacks
    {

        final CategoryDetailFragment this$0;

        public Loader onCreateLoader(int i, Bundle bundle)
        {
            mIsloadingAd = true;
            bundle = bundle.getString("category_id");
            return new CategoryAdLoader(getActivity(), bundle);
        }

        public volatile void onLoadFinished(Loader loader, Object obj)
        {
            onLoadFinished(loader, (List)obj);
        }

        public void onLoadFinished(Loader loader, List list)
        {
            mIsloadingAd = false;
            mAds = list;
            if (mPullToRefresh)
            {
                mPullToRefresh = false;
                loadDataListData(mListView);
                return;
            } else
            {
                initData();
                return;
            }
        }

        public void onLoaderReset(Loader loader)
        {
        }

        _cls6()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls11
        implements android.support.v4.app.LoaderManager.LoaderCallbacks
    {

        final CategoryDetailFragment this$0;

        public Loader onCreateLoader(int i, Bundle bundle)
        {
            if (i == 3)
            {
                return new b(getActivity(), (List)bundle.getSerializable("data"));
            } else
            {
                return null;
            }
        }

        public volatile void onLoadFinished(Loader loader, Object obj)
        {
            onLoadFinished(loader, (List)obj);
        }

        public void onLoadFinished(Loader loader, List list)
        {
            if (mPageId == 1 && mAdapter != null && mAdapter.getData() != null)
            {
                mAdapter.getData().clear();
            }
            insertOrReplaceAdForAlbum(list);
            if (isShowSubfiled())
            {
                mContentTitleLayout.setVisibility(0);
            }
            if (list == null)
            {
                loader = new ArrayList();
            } else
            {
                loader = list;
            }
            if (mAdapter == null)
            {
                if (mFrom == 5 || mFrom == 7 || mFrom == 6)
                {
                    mAdapter = new CategoryDetailAdapter(getActivity(), CategoryDetailFragment.this, loader, true, true);
                    mListView.setAdapter(mAdapter);
                } else
                {
                    mAdapter = new CategoryDetailAdapter(getActivity(), CategoryDetailFragment.this, loader, true);
                    mListView.setAdapter(mAdapter);
                }
            } else
            {
                mAdapter.addData(loader);
                mAdapter.notifyDataSetChanged();
            }
            mAdapter.notifyDataSetChanged();
        }

        public void onLoaderReset(Loader loader)
        {
        }

        _cls11()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.view.View.OnClickListener
    {

        final CategoryDetailFragment this$0;

        public void onClick(View view)
        {
            RecmdItemModel recmditemmodel;
label0:
            {
                if (OneClickHelper.getInstance().onClick(view))
                {
                    recmditemmodel = (RecmdItemModel)view.getTag();
                    if (!mIsTrack)
                    {
                        break label0;
                    }
                    PlayTools.gotoPlay(0, ModelHelper.toSoundInfo(recmditemmodel), getActivity(), DataCollectUtil.getDataFromView(view));
                }
                return;
            }
            toAlbumFragment(recmditemmodel.getAlbumId(), view);
        }

        _cls7()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls8
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final CategoryDetailFragment this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            if (i == 0x7f0a0080)
            {
                ToolUtil.onEvent(mCon, "CLICK_BOOK_TYPE_ALL");
                mBookStatus = 0;
            } else
            if (i == 0x7f0a0081)
            {
                ToolUtil.onEvent(mCon, "CLICK_BOOK_TYPE_FINISHED");
                mBookStatus = 1;
            } else
            if (i == 0x7f0a014b)
            {
                ToolUtil.onEvent(mCon, "CLICK_BOOK_TYPE_WRITTING");
                mBookStatus = 2;
            } else
            {
                mBookStatus = -1;
            }
            mPageId = 1;
            if (mAdapter != null && mAdapter.getData() != null)
            {
                mAdapter.getData().clear();
                mAdapter.notifyDataSetChanged();
            }
            mFilterWindow.dismiss();
            ((PullToRefreshListView)mListView).toRefreshing();
        }

        _cls8()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls9
        implements android.widget.PopupWindow.OnDismissListener
    {

        final CategoryDetailFragment this$0;

        public void onDismiss()
        {
            listviewContainer.removeView(mFilterMask);
        }

        _cls9()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final CategoryDetailFragment this$0;

        public void onRefresh()
        {
            if (!mIsLoading)
            {
                mPageId = 1;
                mPullToRefresh = true;
                loadAd();
            }
        }

        _cls1()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CategoryDetailFragment this$0;

        public void onItemClick(final AdapterView ad, View view, int i, long l)
        {
_L2:
            return;
            if (!OneClickHelper.getInstance().onClick(view) || mAdapter == null || mAdapter.getCount() == 0) goto _L2; else goto _L1
_L1:
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= mAdapter.getCount())
            {
                continue; /* Loop/switch isn't completed */
            }
            if (!mIsTrack)
            {
                break; /* Loop/switch isn't completed */
            }
            ad = (CategoryTrackModel)mAdapter.getData().get(i);
            if (ad != null)
            {
                if (ad.getType() == 1)
                {
                    ad = (CategoryAdModel)ad.getTag();
                    if (ad.getLinkType() == 1 && ad.getClickType() == 1)
                    {
                        view = new AdCollectData();
                        String s = AdManager.getInstance().getAdIdFromUrl(ad.getLink());
                        view.setAdItemId(s);
                        view.setAdSource("0");
                        view.setAndroidId(ToolUtil.getAndroidId(getActivity().getApplicationContext()));
                        view.setLogType("tingClick");
                        view.setPositionName("android_cata_list");
                        view.setResponseId(s);
                        view.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                        view.setTrackId("-1");
                        view = AdManager.getInstance().getAdRealJTUrl(ad.getLink(), view);
                        if (ad.getOpenlinkType() == 0)
                        {
                            ad = WebFragment.newInstance(view);
                            startFragment(ad);
                            return;
                        }
                        if (ad.getOpenlinkType() == 1)
                        {
                            ad.openInThirdBrowser(getActivity(), view);
                            return;
                        }
                    }
                } else
                {
                    gotoPlay(i, mAdapter.getData(), DataCollectUtil.getDataFromView(view));
                    return;
                }
            }
            if (true) goto _L2; else goto _L3
_L3:
            ad = (CategoryAlbumModel)mAdapter.getData().get(i);
            if (ad != null)
            {
                if (ad.getType() != 1)
                {
                    continue; /* Loop/switch isn't completed */
                }
                ad = (CategoryAdModel)ad.getTag();
                if (ad.getLinkType() == 1 && ad.getClickType() == 1)
                {
                    view = new AdCollectData();
                    String s1 = AdManager.getInstance().getAdIdFromUrl(ad.getLink());
                    view.setAdItemId(s1);
                    view.setAdSource("0");
                    view.setAndroidId(ToolUtil.getAndroidId(getActivity().getApplicationContext()));
                    view.setLogType("tingClick");
                    view.setPositionName("android_cata_list");
                    view.setResponseId(s1);
                    view.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                    view.setTrackId("-1");
                    view = AdManager.getInstance().getAdRealJTUrl(ad.getLink(), view);
                    if (ad.getOpenlinkType() == 0)
                    {
                        class _cls1
                            implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
                        {

                            final _cls2 this$1;

                            public void execute(String s2)
                            {
                                s2 = WebFragment.newInstance(s2);
                                startFragment(s2);
                            }

                _cls1()
                {
                    this$1 = _cls2.this;
                    super();
                }
                        }

                        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(view, new _cls1());
                        return;
                    }
                    if (ad.getOpenlinkType() == 1)
                    {
                        class _cls2
                            implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
                        {

                            final _cls2 this$1;
                            final CategoryAdModel val$ad;

                            public void execute(String s2)
                            {
                                ad.openInThirdBrowser(getActivity(), s2);
                            }

                _cls2()
                {
                    this$1 = _cls2.this;
                    ad = categoryadmodel;
                    super();
                }
                        }

                        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(view, new _cls2());
                        return;
                    }
                }
            }
            continue; /* Loop/switch isn't completed */
            if (mFrom == 5 || mFrom == 7 || mFrom == 6) goto _L2; else goto _L4
_L4:
            if (mFrom != 8)
            {
                break; /* Loop/switch isn't completed */
            }
            if (ad != null)
            {
                view = new Bundle();
                view.putSerializable("data", ModelHelper.toAlbumModel(ad));
                startFragment(com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonDeviceAlbumSectionDownloadFragment, view);
                return;
            }
            if (true) goto _L2; else goto _L5
_L5:
            if (mFrom != 9)
            {
                break; /* Loop/switch isn't completed */
            }
            if (!UserInfoMannage.hasLogined())
            {
                (new DialogBuilder(getAdRealJTUrl)).setMessage("\u6DFB\u52A0\u529F\u80FD\u4EC5\u767B\u5F55\u7528\u6237\u624D\u80FD\u4F7F\u7528\u54E6\uFF01").setCancelBtn("\u7A0D\u540E\u518D\u8BF4").setOkBtn("\u53BB\u767B\u5F55", new _cls3()).showConfirm();
                return;
            }
            if (ad != null)
            {
                view = new Bundle();
                view.putSerializable("data", ModelHelper.toAlbumModel(ad));
                startFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeDeviceAlbumSectionDownloadFragment, view);
                return;
            }
            if (true) goto _L2; else goto _L6
_L6:
            if (mFrom == 10)
            {
                if (!UserInfoMannage.hasLogined())
                {
                    (new DialogBuilder(getAdRealJTUrl)).setMessage("\u6DFB\u52A0\u529F\u80FD\u4EC5\u767B\u5F55\u7528\u6237\u624D\u80FD\u4F7F\u7528\u54E6\uFF01").setCancelBtn("\u7A0D\u540E\u518D\u8BF4").setOkBtn("\u53BB\u767B\u5F55", new _cls4()).showConfirm();
                    return;
                }
                if (ad != null)
                {
                    view = new Bundle();
                    view.putSerializable("data", ModelHelper.toAlbumModel(ad));
                    startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/ximao/XiMaoDeviceAlbumSectionDownloadFragment, view);
                    return;
                }
            } else
            {
                toAlbumFragment(ad.getAlbumId(), view, (new StringBuilder()).append(i + 1).append("").toString());
                return;
            }
            if (true) goto _L2; else goto _L7
_L7:
        }

        _cls2()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnScrollListner
    {

        final CategoryDetailFragment this$0;

        public void onMyScrollStateChanged(AbsListView abslistview, int i)
        {
            int j = abslistview.getCount();
            if (j > 5)
            {
                j -= 5;
            } else
            {
                j--;
            }
            if (mListView.getLastVisiblePosition() > j && i == 0 && !mIsLoading)
            {
                loadMoreData(mListView);
            }
        }

        _cls3()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final CategoryDetailFragment this$0;

        public void onClick(View view)
        {
            loadDataListData(mFooterViewLoading);
        }

        _cls4()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final CategoryDetailFragment this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            if (radiogroup == mHeaderRadioGroupFloat)
            {
                if (mHeaderRadioGroup.getCheckedRadioButtonId() != i)
                {
                    ((RadioButton)mHeaderRadioGroup.findViewById(i)).setChecked(true);
                }
            } else
            {
                if (mHeaderRadioGroupFloat.getCheckedRadioButtonId() != i)
                {
                    ((RadioButton)mHeaderRadioGroupFloat.findViewById(i)).setChecked(true);
                }
                mSortBy = getSortCondition(i);
                hideSubfield();
                mPageId = 1;
                if (mAdapter != null && mAdapter.getData() != null)
                {
                    mAdapter.getData().clear();
                }
                mAdapter.notifyDataSetChanged();
                ((PullToRefreshListView)mListView).toRefreshing();
                if (mSortBy == "recent")
                {
                    ToolUtil.onEvent(mCon, "CLICK_ALBUM_TAB_NEW");
                }
                if (mSortBy == "classic")
                {
                    ToolUtil.onEvent(mCon, "CLICK_ALBUM_TAB_CLASSIC");
                    return;
                }
            }
        }

        _cls5()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls12 extends OnPlayerStatusUpdateListenerProxy
    {

        final CategoryDetailFragment this$0;

        public void onPlayStateChange()
        {
            if (mIsTrack && mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        _cls12()
        {
            this$0 = CategoryDetailFragment.this;
            super();
        }
    }


    private class _cls10
        implements MyCallback
    {

        final CategoryDetailFragment this$0;
        final Object val$data;

        public void execute()
        {
            if (parseData(data))
            {
                if (!mIsTrack && (data instanceof CategoryAlbumListModel))
                {
                    CategoryAlbumListModel categoryalbumlistmodel = (CategoryAlbumListModel)data;
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", (Serializable)categoryalbumlistmodel.getList());
                    startLoader(3, mCollectStatusCallbacks, null, bundle);
                }
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
            if (mAdapter.getCount() > 0)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            } else
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
        }

        _cls10()
        {
            this$0 = CategoryDetailFragment.this;
            data = obj;
            super();
        }
    }

}
