// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.BaseListSoundsAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumModel;
import com.ximalaya.ting.android.model.category.detail.CategoryTrackModel;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

public class CategoryDetailAdapter extends BaseListSoundsAdapter
{
    public class AlbumViewHolder
    {

        public ImageView adIndicator;
        public View adLayout;
        public ImageView arrow;
        public LinearLayout collect;
        public TextView collectTxt;
        public ImageView complete;
        public ImageView cover;
        public TextView name;
        public TextView playCount;
        public TextView soundsCount;
        public TextView tags;
        final CategoryDetailAdapter this$0;

        public AlbumViewHolder()
        {
            this$0 = CategoryDetailAdapter.this;
            super();
        }
    }

    public static interface OnBindingListener
    {

        public abstract void doCollect(Context context, CategoryAlbumModel categoryalbummodel);
    }


    private boolean isAlbum;
    private boolean isBind;
    private OnBindingListener mBindingListener;
    private BaseFragment mFragment;

    public CategoryDetailAdapter(Activity activity, BaseFragment basefragment, List list, boolean flag)
    {
        super(activity, list);
        isBind = false;
        isAlbum = flag;
        mFragment = basefragment;
        isBind = false;
    }

    public CategoryDetailAdapter(Activity activity, BaseFragment basefragment, List list, boolean flag, boolean flag1)
    {
        super(activity, list);
        isBind = false;
        isAlbum = flag;
        mFragment = basefragment;
        isBind = flag1;
    }

    private View getAlbumView(int i, View view, ViewGroup viewgroup)
    {
        CategoryAlbumModel categoryalbummodel = (CategoryAlbumModel)mData.get(i);
        String s;
        boolean flag;
        if (view == null)
        {
            view = View.inflate(mContext, 0x7f0301b0, null);
            viewgroup = new AlbumViewHolder();
            viewgroup.cover = (ImageView)view.findViewById(0x7f0a0177);
            mFragment.markImageView(((AlbumViewHolder) (viewgroup)).cover);
            viewgroup.name = (TextView)view.findViewById(0x7f0a0449);
            viewgroup.playCount = (TextView)view.findViewById(0x7f0a021a);
            viewgroup.soundsCount = (TextView)view.findViewById(0x7f0a026b);
            viewgroup.collect = (LinearLayout)view.findViewById(0x7f0a0154);
            viewgroup.collectTxt = (TextView)view.findViewById(0x7f0a0155);
            viewgroup.arrow = (ImageView)view.findViewById(0x7f0a0153);
            viewgroup.complete = (ImageView)view.findViewById(0x7f0a014d);
            viewgroup.tags = (TextView)view.findViewById(0x7f0a044a);
            viewgroup.adIndicator = (ImageView)view.findViewById(0x7f0a0255);
            viewgroup.adLayout = view.findViewById(0x7f0a050d);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (AlbumViewHolder)view.getTag();
        }
        ImageManager2.from(mContext).displayImage(((AlbumViewHolder) (viewgroup)).cover, categoryalbummodel.getCoverMiddle(), 0x7f0202e0);
        setCollectStatus(viewgroup, categoryalbummodel.isCCollected());
        if (categoryalbummodel.getIsFinished() == 2)
        {
            if (((AlbumViewHolder) (viewgroup)).complete.getVisibility() != 0)
            {
                ((AlbumViewHolder) (viewgroup)).complete.setVisibility(0);
            }
        } else
        {
            ((AlbumViewHolder) (viewgroup)).complete.setVisibility(8);
        }
        if (categoryalbummodel.getPlaysCounts() > 0L)
        {
            if (((AlbumViewHolder) (viewgroup)).playCount.getVisibility() != 0)
            {
                ((AlbumViewHolder) (viewgroup)).playCount.setVisibility(0);
            }
            ((AlbumViewHolder) (viewgroup)).playCount.setText(StringUtil.getFriendlyNumStr(categoryalbummodel.getPlaysCounts()));
        } else
        {
            ((AlbumViewHolder) (viewgroup)).playCount.setVisibility(8);
        }
        if (categoryalbummodel.getTracks() > 0)
        {
            if (((AlbumViewHolder) (viewgroup)).soundsCount.getVisibility() != 0)
            {
                ((AlbumViewHolder) (viewgroup)).soundsCount.setVisibility(0);
            }
            ((AlbumViewHolder) (viewgroup)).soundsCount.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(categoryalbummodel.getTracks())).append("\u96C6").toString());
        } else
        {
            ((AlbumViewHolder) (viewgroup)).soundsCount.setVisibility(8);
        }
        ((AlbumViewHolder) (viewgroup)).collect.setTag(0x7f090000, categoryalbummodel);
        if (categoryalbummodel.getType() != 1)
        {
            TextView textview = ((AlbumViewHolder) (viewgroup)).name;
            if (categoryalbummodel.getTitle() == null)
            {
                s = "";
            } else
            {
                s = categoryalbummodel.getTitle();
            }
            textview.setText(s);
            textview = ((AlbumViewHolder) (viewgroup)).tags;
            if (categoryalbummodel.getIntro() == null)
            {
                s = "";
            } else
            {
                s = categoryalbummodel.getIntro();
            }
            textview.setText(s);
        }
        if (isBind)
        {
            if (MyDeviceManager.getInstance(mContext).isAlbumBind(categoryalbummodel.getAlbumId()))
            {
                setCollectStatus(viewgroup, true);
                ((AlbumViewHolder) (viewgroup)).collectTxt.setText("");
                ((AlbumViewHolder) (viewgroup)).collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0201c6, 0, 0);
            } else
            {
                setCollectStatus(viewgroup, false);
                ((AlbumViewHolder) (viewgroup)).collectTxt.setText("");
                ((AlbumViewHolder) (viewgroup)).collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0201c5, 0, 0);
            }
            ((AlbumViewHolder) (viewgroup)).collect.setTag(0x7f090000, categoryalbummodel);
            ((AlbumViewHolder) (viewgroup)).collect.setOnClickListener(new _cls1());
        } else
        {
            ((AlbumViewHolder) (viewgroup)).collect.setVisibility(8);
            ((AlbumViewHolder) (viewgroup)).arrow.setVisibility(0);
        }
        if (categoryalbummodel.getType() == 1)
        {
            ((AlbumViewHolder) (viewgroup)).tags.setVisibility(8);
            ((AlbumViewHolder) (viewgroup)).name.setVisibility(8);
            ((AlbumViewHolder) (viewgroup)).adLayout.setVisibility(0);
            textview = (TextView)((AlbumViewHolder) (viewgroup)).adLayout.findViewById(0x7f0a0695);
            if (categoryalbummodel.getTitle() == null)
            {
                s = "";
            } else
            {
                s = categoryalbummodel.getTitle();
            }
            textview.setText(s);
            textview = (TextView)((AlbumViewHolder) (viewgroup)).adLayout.findViewById(0x7f0a0696);
            if (categoryalbummodel.getIntro() == null)
            {
                s = "";
            } else
            {
                s = categoryalbummodel.getIntro();
            }
            textview.setText(s);
            ((AlbumViewHolder) (viewgroup)).collect.setVisibility(8);
            ((AlbumViewHolder) (viewgroup)).arrow.setVisibility(4);
            ((AlbumViewHolder) (viewgroup)).adIndicator.setVisibility(0);
        } else
        {
            ((AlbumViewHolder) (viewgroup)).tags.setVisibility(0);
            ((AlbumViewHolder) (viewgroup)).name.setVisibility(0);
            ((AlbumViewHolder) (viewgroup)).adLayout.setVisibility(8);
            if (isBind)
            {
                ((AlbumViewHolder) (viewgroup)).collect.setVisibility(0);
            }
            ((AlbumViewHolder) (viewgroup)).adIndicator.setVisibility(4);
        }
        viewgroup = mContext;
        if (i + 1 == mData.size())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        ViewUtil.buildAlbumItemSpace(viewgroup, view, false, flag, 81);
        return view;
    }

    private void setCollectStatus(AlbumViewHolder albumviewholder, boolean flag)
    {
        if (flag)
        {
            albumviewholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ee, 0, 0);
            albumviewholder.collectTxt.setText("\u5DF2\u6536\u85CF");
            albumviewholder.collectTxt.setTextColor(Color.parseColor("#999999"));
            return;
        } else
        {
            albumviewholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ed, 0, 0);
            albumviewholder.collectTxt.setText("\u6536\u85CF");
            albumviewholder.collectTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    private void subscribeAlbum(final CategoryAlbumModel model, final AlbumViewHolder holder)
    {
        if (UserInfoMannage.hasLogined())
        {
            String s;
            RequestParams requestparams;
            if (model.isCCollected())
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.getCId()).toString());
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(holder.collect), new _cls2());
        } else
        {
            final AlbumModel am = ModelHelper.toAlbumModel(model);
            if (AlbumModelManage.getInstance().ensureLocalCollectAllow(mContext, am, holder.collect))
            {
                (new _cls3()).myexec(new Void[0]);
                return;
            }
        }
    }

    protected void bindData(Object obj, final com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder holder)
    {
        final CategoryTrackModel model;
label0:
        {
            if (obj instanceof CategoryTrackModel)
            {
                model = (CategoryTrackModel)obj;
                ImageManager2.from(mContext).displayImage(holder.cover, model.getCoverSmall(), 0x7f0202de);
                holder.likeCount.setVisibility(8);
                TextView textview = holder.title;
                if (model.getTitle() == null)
                {
                    obj = "";
                } else
                {
                    obj = model.getTitle();
                }
                textview.setText(((CharSequence) (obj)));
                if (TextUtils.isEmpty(model.getNickname()))
                {
                    holder.owner.setText("");
                } else
                if (model.getType() == 1)
                {
                    holder.owner.setText(model.getNickname());
                } else
                {
                    holder.owner.setText((new StringBuilder()).append("by ").append(model.getNickname()).toString());
                }
                if (holder.position + 1 == mData.size())
                {
                    holder.border.setVisibility(8);
                } else
                {
                    holder.border.setVisibility(0);
                }
                if (model.getType() != 1)
                {
                    break label0;
                }
                holder.origin.setVisibility(8);
                holder.playCount.setVisibility(8);
                holder.likeCount.setVisibility(8);
                holder.duration.setVisibility(8);
                holder.commentCount.setVisibility(8);
                holder.createTime.setVisibility(8);
                holder.cover.setEnabled(false);
                holder.playFlag.setVisibility(4);
                holder.btn.setVisibility(8);
                holder.adIndicator.setVisibility(0);
            }
            return;
        }
        holder.cover.setEnabled(true);
        holder.playFlag.setVisibility(0);
        holder.btn.setVisibility(0);
        holder.adIndicator.setVisibility(8);
        holder.playCount.setText(StringUtil.getFriendlyNumStr(model.getPlaysCounts()));
        if (model.getPlaysCounts() > 0L)
        {
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(model.getFavoritesCounts()));
            holder.likeCount.setVisibility(0);
        } else
        {
            holder.likeCount.setVisibility(8);
        }
        if (model.getDuration() > 0.0D)
        {
            holder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)model.getDuration())).toString());
            holder.duration.setVisibility(0);
        } else
        {
            holder.duration.setVisibility(8);
        }
        if (model.getUpdatedAt() > 0L)
        {
            holder.createTime.setVisibility(0);
            holder.createTime.setText(ToolUtil.convertTime(model.getUpdatedAt()));
        } else
        {
            holder.createTime.setVisibility(8);
        }
        holder.origin.setVisibility(8);
        if (isPlaying(model.getTrackId()))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        if (isDownload(model.getTrackId()))
        {
            holder.btn.setImageResource(0x7f0200f1);
            holder.btn.setEnabled(false);
        } else
        {
            holder.btn.setImageResource(0x7f0201fe);
            holder.btn.setEnabled(true);
        }
        holder.btn.setOnClickListener(new _cls4());
        holder.cover.setOnClickListener(new _cls5());
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (isAlbum)
        {
            view = getAlbumView(i, view, viewgroup);
        } else
        {
            viewgroup = super.getView(i, view, viewgroup);
            View view1 = viewgroup.findViewById(0x7f0a00a8);
            view = viewgroup;
            if (view1.getVisibility() != 0)
            {
                view1.setVisibility(0);
                return viewgroup;
            }
        }
        return view;
    }

    public void removeBindingListener()
    {
        mBindingListener = null;
    }

    public void setBindingListener(OnBindingListener onbindinglistener)
    {
        mBindingListener = onbindinglistener;
    }





    private class _cls1
        implements android.view.View.OnClickListener
    {

        final CategoryDetailAdapter this$0;

        public void onClick(View view)
        {
            view = (CategoryAlbumModel)view.getTag(0x7f090000);
            if (view == null)
            {
                return;
            }
            if (mBindingListener != null)
            {
                mBindingListener.doCollect(mContext, view);
            }
            notifyDataSetChanged();
        }

        _cls1()
        {
            this$0 = CategoryDetailAdapter.this;
            super();
        }
    }


    private class _cls2 extends a
    {

        final CategoryDetailAdapter this$0;
        final AlbumViewHolder val$holder;
        final CategoryAlbumModel val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, holder.collect);
        }

        public void onFinish()
        {
            super.onFinish();
            holder.collect.setEnabled(true);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            setCollectStatus(holder, model.isCCollected());
        }

        public void onStart()
        {
            super.onStart();
            holder.collect.setEnabled(false);
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                setCollectStatus(holder, model.isCCollected());
                return;
            }
            Object obj = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = obj;
            }
            if (s != null && s.getIntValue("ret") == 0)
            {
                if (model.isCCollected())
                {
                    s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
                    model.setCCollected(false);
                } else
                {
                    s = "\u6536\u85CF\u6210\u529F\uFF01";
                    model.setCCollected(true);
                }
                setCollectStatus(holder, model.isCCollected());
                Toast.makeText(mContext, s, 0).show();
                return;
            }
            s = s.getString("msg");
            if (TextUtils.isEmpty(s))
            {
                s = "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
            }
            Toast.makeText(mContext, s, 0).show();
            setCollectStatus(holder, model.isCCollected());
        }

        _cls2()
        {
            this$0 = CategoryDetailAdapter.this;
            holder = albumviewholder;
            model = categoryalbummodel;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final CategoryDetailAdapter this$0;
        final AlbumModel val$am;
        final AlbumViewHolder val$holder;
        final CategoryAlbumModel val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            CategoryAlbumModel categoryalbummodel = model;
            boolean flag;
            if (!model.isCCollected())
            {
                flag = true;
            } else
            {
                flag = false;
            }
            categoryalbummodel.setCCollected(flag);
            if (!model.isCCollected())
            {
                avoid.deleteAlbumInLocalAlbumList(am);
            } else
            {
                avoid.saveAlbumModel(am);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            void1 = (CategoryAlbumModel)holder.collect.getTag(0x7f090000);
            if (void1 != null && void1.getAlbumId() == model.getAlbumId())
            {
                setCollectStatus(holder, model.isCCollected());
            }
        }

        _cls3()
        {
            this$0 = CategoryDetailAdapter.this;
            model = categoryalbummodel;
            am = albummodel;
            holder = albumviewholder;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final CategoryDetailAdapter this$0;
        final com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder val$holder;
        final CategoryTrackModel val$model;

        public void onClick(View view)
        {
            DownloadTask downloadtask = new DownloadTask(ModelHelper.toSoundInfo(model));
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            view = downloadtools.goDownload(downloadtask, mContext.getApplicationContext(), view);
            downloadtools.release();
            if (view == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls4()
        {
            this$0 = CategoryDetailAdapter.this;
            model = categorytrackmodel;
            holder = viewholder;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final CategoryDetailAdapter this$0;
        final com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder val$holder;
        final CategoryTrackModel val$model;

        public void onClick(View view)
        {
            playSound(holder.playFlag, toSoundInfo.indexOf(model), ModelHelper.toSoundInfo(model), ModelHelper.toSoundInfoListForCategoryTrack(Arrays.asList(toSoundInfo.toArray(new CategoryTrackModel[0]))));
        }

        _cls5()
        {
            this$0 = CategoryDetailAdapter.this;
            holder = viewholder;
            model = categorytrackmodel;
            super();
        }
    }

}
