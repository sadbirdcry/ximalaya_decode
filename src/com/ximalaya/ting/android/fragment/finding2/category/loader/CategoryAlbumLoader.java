// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.category.loader;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumListModel;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;

public class CategoryAlbumLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "mobile/discovery/v1/category/album";
    private String calcDimension;
    private String categoryId;
    private CategoryAlbumListModel mData;
    private int pageId;
    private int pageSize;
    private int status;
    private String tagName;

    public CategoryAlbumLoader(Context context, String s, String s1, String s2, int i, int j, int k)
    {
        super(context);
        calcDimension = "hot";
        status = 0;
        pageId = j;
        pageSize = k;
        categoryId = s;
        tagName = s1;
        calcDimension = s2;
        status = i;
    }

    public void deliverResult(CategoryAlbumListModel categoryalbumlistmodel)
    {
        super.deliverResult(categoryalbumlistmodel);
        mData = categoryalbumlistmodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((CategoryAlbumListModel)obj);
    }

    public CategoryAlbumListModel loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("pageId", pageId);
        ((RequestParams) (obj)).put("pageSize", pageSize);
        ((RequestParams) (obj)).put("categoryId", categoryId);
        ((RequestParams) (obj)).put("calcDimension", calcDimension);
        ((RequestParams) (obj)).put("status", status);
        if (TextUtils.isEmpty(tagName))
        {
            ((RequestParams) (obj)).put("tagName", "");
        } else
        {
            ((RequestParams) (obj)).put("tagName", tagName);
        }
        obj = f.a().a("mobile/discovery/v1/category/album", ((RequestParams) (obj)), fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_142;
        }
        obj = (CategoryAlbumListModel)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a, com/ximalaya/ting/android/model/category/detail/CategoryAlbumListModel);
        if (obj != null)
        {
            try
            {
                if (((CategoryAlbumListModel) (obj)).ret == 0)
                {
                    mData = ((CategoryAlbumListModel) (obj));
                }
            }
            catch (Exception exception)
            {
                Logger.e(exception);
            }
        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
