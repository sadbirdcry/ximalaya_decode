// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.recommend;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import com.ximalaya.ting.android.fragment.finding2.recommend.adapter.RecommendItemListAdapter;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemGroup;
import com.ximalaya.ting.android.util.MyCallback;
import java.io.Serializable;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.recommend:
//            RecommendListFragment

class val.data
    implements MyCallback
{

    final RecommendListFragment this$0;
    final RecmdItemGroup val$data;

    public void execute()
    {
        RecommendListFragment.access$702(RecommendListFragment.this, val$data.hasMore());
        if (val$data.getList() != null && val$data.getList().size() > 0)
        {
            Bundle bundle = new Bundle();
            bundle.putSerializable("data", (Serializable)val$data.getList());
            if (RecommendListFragment.access$800(RecommendListFragment.this) == null)
            {
                RecommendListFragment.access$802(RecommendListFragment.this, getLoaderManager().initLoader(999, bundle, RecommendListFragment.access$900(RecommendListFragment.this)));
            } else
            {
                RecommendListFragment.access$802(RecommendListFragment.this, getLoaderManager().restartLoader(999, bundle, RecommendListFragment.access$900(RecommendListFragment.this)));
            }
            showFooterView(com.ximalaya.ting.android.fragment.iew.HIDE_ALL);
            return;
        }
        if (RecommendListFragment.access$000(RecommendListFragment.this).getCount() > 0)
        {
            showFooterView(com.ximalaya.ting.android.fragment.iew.HIDE_ALL);
            return;
        } else
        {
            showFooterView(com.ximalaya.ting.android.fragment.iew.NO_DATA);
            return;
        }
    }

    tAdapter()
    {
        this$0 = final_recommendlistfragment;
        val$data = RecmdItemGroup.this;
        super();
    }
}
