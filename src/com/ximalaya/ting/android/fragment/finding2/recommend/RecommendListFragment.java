// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.recommend;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.fragment.finding2.recommend.adapter.RecommendItemListAdapter;
import com.ximalaya.ting.android.fragment.finding2.recommend.loader.RecommendListLoader;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemGroup;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;

public class RecommendListFragment extends BaseListFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, com.ximalaya.ting.android.fragment.ReloadFragment.Callback
{

    public static final String BUNDLE_LOADER_TYPE = "loader_type";
    public static final String BUNDLE_TITLE = "title";
    private static final int LOAD_COLLECT_STATUS = 999;
    private static final int PAGE_SIZE = 20;
    private RecommendItemListAdapter mAdapter;
    private String mCategoryId;
    private android.support.v4.app.LoaderManager.LoaderCallbacks mCollectStatusCallbacks;
    private boolean mHasMore;
    private boolean mIsLoading;
    private Loader mLoader;
    private int mLoaderType;
    private int mPageId;
    private Loader mStatusLoader;
    private String mSubfieldName;
    private String mTagName;
    private String mTitle;

    public RecommendListFragment()
    {
        mPageId = 1;
        mHasMore = true;
        mCollectStatusCallbacks = new _cls5();
    }

    private void initListener()
    {
        mListView.setOnItemClickListener(new _cls1());
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls2());
        ((PullToRefreshListView)mListView).setMyScrollListener2(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
    }

    private boolean isGuessLike()
    {
        return mLoaderType == 1 || mLoaderType == 3;
    }

    private void loadData(View view)
    {
        if (mIsLoading)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            return;
        }
        if (!NetworkUtils.isNetworkAvaliable(getActivity()))
        {
            showReload();
            return;
        }
        mIsLoading = true;
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(mLoaderType, null, this);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
            return;
        } else
        {
            mLoader = getLoaderManager().restartLoader(mLoaderType, null, this);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
            return;
        }
    }

    private void loadMoreData(View view)
    {
        if (mHasMore)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
            loadData(view);
        }
    }

    private void showReload()
    {
        if (mAdapter == null || mAdapter.getCount() == 0)
        {
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
        }
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mLoaderType = bundle.getInt("loader_type");
            mTitle = bundle.getString("title");
            mCategoryId = bundle.getString("categoryId");
            mTagName = bundle.getString("tagName");
            mSubfieldName = bundle.getString("subfieldName");
        }
        if (!TextUtils.isEmpty(mTitle)) goto _L2; else goto _L1
_L1:
        if (mLoaderType != 0) goto _L4; else goto _L3
_L3:
        mTitle = "\u5C0F\u7F16\u63A8\u8350";
_L2:
        mAdapter = new RecommendItemListAdapter(getActivity(), this, new ArrayList(), isGuessLike());
        mListView.setAdapter(mAdapter);
        setTitleText(mTitle);
        initListener();
        ((PullToRefreshListView)mListView).toRefreshing();
        return;
_L4:
        if (isGuessLike())
        {
            mTitle = "\u731C\u4F60\u559C\u6B22";
        } else
        if (mLoaderType == 2)
        {
            mTitle = "\u7CBE\u9009\u4E13\u8F91";
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        if (i == 2)
        {
            return new RecommendListLoader(getActivity(), i, mPageId, 20, mCategoryId, mTagName, mSubfieldName);
        } else
        {
            return new RecommendListLoader(getActivity(), i, mPageId, 20);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300cf, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onLoadFinished(Loader loader, final RecmdItemGroup data)
    {
        ((PullToRefreshListView)mListView).onRefreshComplete();
        if (loader != null)
        {
            if (data != null && data.getRet() == 0)
            {
                doAfterAnimation(new _cls6());
            } else
            {
                showReload();
            }
        }
        mIsLoading = false;
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (RecmdItemGroup)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onResume()
    {
        super.onResume();
    }

    public void reload(View view)
    {
        ((PullToRefreshListView)mListView).toRefreshing();
    }



/*
    static RecommendItemListAdapter access$002(RecommendListFragment recommendlistfragment, RecommendItemListAdapter recommenditemlistadapter)
    {
        recommendlistfragment.mAdapter = recommenditemlistadapter;
        return recommenditemlistadapter;
    }

*/





/*
    static int access$302(RecommendListFragment recommendlistfragment, int i)
    {
        recommendlistfragment.mPageId = i;
        return i;
    }

*/


/*
    static int access$308(RecommendListFragment recommendlistfragment)
    {
        int i = recommendlistfragment.mPageId;
        recommendlistfragment.mPageId = i + 1;
        return i;
    }

*/






/*
    static boolean access$702(RecommendListFragment recommendlistfragment, boolean flag)
    {
        recommendlistfragment.mHasMore = flag;
        return flag;
    }

*/



/*
    static Loader access$802(RecommendListFragment recommendlistfragment, Loader loader)
    {
        recommendlistfragment.mStatusLoader = loader;
        return loader;
    }

*/


    private class _cls5
        implements android.support.v4.app.LoaderManager.LoaderCallbacks
    {

        final RecommendListFragment this$0;

        public Loader onCreateLoader(int i, Bundle bundle)
        {
            if (i == 999)
            {
                return new b(getActivity(), (List)bundle.getSerializable("data"));
            } else
            {
                return null;
            }
        }

        public volatile void onLoadFinished(Loader loader, Object obj)
        {
            onLoadFinished(loader, (List)obj);
        }

        public void onLoadFinished(Loader loader, List list)
        {
            if (list == null)
            {
                return;
            }
            if (mPageId == 1 && mAdapter != null && mAdapter.getData() != null)
            {
                mAdapter.getData().clear();
            }
            if (mHasMore)
            {
                int i = ((onLoadFinished) (this)).onLoadFinished;
            }
            if (mAdapter == null || mAdapter.getData() == null)
            {
                mAdapter = new RecommendItemListAdapter(getActivity(), RecommendListFragment.this, list, isGuessLike());
                mListView.setAdapter(mAdapter);
                return;
            } else
            {
                mAdapter.addData(list);
                mAdapter.notifyDataSetChanged();
                return;
            }
        }

        public void onLoaderReset(Loader loader)
        {
        }

        _cls5()
        {
            this$0 = RecommendListFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final RecommendListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (OneClickHelper.getInstance().onClick(view))
            {
                i -= mListView.getHeaderViewsCount();
                if (i >= 0 && i < mAdapter.getCount())
                {
                    adapterview = mAdapter.getItem(i);
                    AlbumModel albummodel = new AlbumModel();
                    albummodel.albumId = adapterview.getAlbumId();
                    Bundle bundle = new Bundle();
                    bundle.putString("album", JSON.toJSONString(albummodel));
                    if (isGuessLike())
                    {
                        bundle.putInt("from", 12);
                        bundle.putString("rec_src", adapterview.getRecSrc());
                        bundle.putString("rec_track", adapterview.getRecTrack());
                    } else
                    {
                        bundle.putInt("from", 6);
                    }
                    bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, mTitle, (new StringBuilder()).append(i + 1).append("").toString()));
                    startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
                }
            }
        }

        _cls1()
        {
            this$0 = RecommendListFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final RecommendListFragment this$0;

        public void onRefresh()
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            mPageId = 1;
            loadData(mListView);
        }

        _cls2()
        {
            this$0 = RecommendListFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnScrollListner
    {

        final RecommendListFragment this$0;

        public void onMyScrollStateChanged(AbsListView abslistview, int i)
        {
            int j = abslistview.getCount();
            if (j > 5)
            {
                j -= 5;
            } else
            {
                j--;
            }
            if (mListView.getLastVisiblePosition() > j && i == 0 && !mIsLoading)
            {
                loadMoreData(mListView);
            }
        }

        _cls3()
        {
            this$0 = RecommendListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final RecommendListFragment this$0;

        public void onClick(View view)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            if (!mIsLoading)
            {
                loadMoreData(mFooterViewLoading);
            }
        }

        _cls4()
        {
            this$0 = RecommendListFragment.this;
            super();
        }
    }


    private class _cls6
        implements MyCallback
    {

        final RecommendListFragment this$0;
        final RecmdItemGroup val$data;

        public void execute()
        {
            mHasMore = data.hasMore();
            if (data.getList() != null && data.getList().size() > 0)
            {
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", (Serializable)data.getList());
                if (mStatusLoader == null)
                {
                    mStatusLoader = getLoaderManager().initLoader(999, bundle, mCollectStatusCallbacks);
                } else
                {
                    mStatusLoader = getLoaderManager().restartLoader(999, bundle, mCollectStatusCallbacks);
                }
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
            if (mAdapter.getCount() > 0)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            } else
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
        }

        _cls6()
        {
            this$0 = RecommendListFragment.this;
            data = recmditemgroup;
            super();
        }
    }

}
