// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.recommend.loader;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemGroup;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;

public class RecommendListLoader extends MyAsyncTaskLoader
{

    private static final String PATH_CATEGORY_SUBFIELD = "mobile/discovery/v1/category/subfield/albums";
    private static final String PATH_EDITOR_RECOMMEND = "mobile/discovery/v1/recommend/editor";
    private static final String PATH_GUESS_LIKE = "mobile/discovery/v1/recommend/guessYouLike";
    private static final String PATH_GUESS_LIKE_UNLOGIN = "mobile/discovery/v1/recommend/guessYouLike/unlogin";
    public static final int TYPE_CATEGORY_SUBFIELD = 2;
    public static final int TYPE_EDITOR_RECOMMEND = 0;
    public static final int TYPE_GUESS_LIKE = 1;
    public static final int TYPE_GUESS_LIKE_UNLOGIN = 3;
    private String categoryId;
    private RecmdItemGroup mData;
    private int pageId;
    private int pageSize;
    private String subfieldName;
    private String tagName;
    private int type;

    public RecommendListLoader(Context context, int i, int j, int k)
    {
        super(context);
        pageId = j;
        pageSize = k;
        type = i;
    }

    public RecommendListLoader(Context context, int i, int j, int k, String s, String s1, String s2)
    {
        this(context, i, j, k);
        categoryId = s;
        tagName = s1;
        subfieldName = s2;
    }

    private RecmdItemGroup loadFromPref()
    {
        Object obj;
        obj = SharedPreferencesUtil.getInstance(getContext()).getString("recommend_guess_like");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            break MISSING_BLOCK_LABEL_37;
        }
        obj = (RecmdItemGroup)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/finding2/recommend/RecmdItemGroup);
        return ((RecmdItemGroup) (obj));
        Exception exception;
        exception;
        Logger.e(exception);
        return null;
    }

    private RecmdItemGroup parseData(com.ximalaya.ting.android.b.n.a a)
    {
        if (a.b != 1 || TextUtils.isEmpty(a.a))
        {
            break MISSING_BLOCK_LABEL_38;
        }
        a = (RecmdItemGroup)JSON.parseObject(a.a, com/ximalaya/ting/android/model/finding2/recommend/RecmdItemGroup);
        return a;
        a;
        Logger.e(a);
        return null;
    }

    public void deliverResult(RecmdItemGroup recmditemgroup)
    {
        super.deliverResult(recmditemgroup);
        mData = recmditemgroup;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((RecmdItemGroup)obj);
    }

    public RecmdItemGroup loadInBackground()
    {
        RequestParams requestparams;
        requestparams = new RequestParams();
        requestparams.put("pageId", pageId);
        requestparams.put("pageSize", pageSize);
        if (type != 1) goto _L2; else goto _L1
_L1:
        LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
        if (logininfomodel != null)
        {
            requestparams.put("uid", (new StringBuilder()).append("").append(logininfomodel.uid).toString());
            requestparams.put("token", logininfomodel.token);
        }
        mData = parseData(f.a().a("mobile/discovery/v1/recommend/guessYouLike", requestparams, fromBindView, toBindView, true));
_L4:
        fromBindView = null;
        toBindView = null;
        return mData;
_L2:
        if (type == 3)
        {
            mData = parseData(f.a().a("mobile/discovery/v1/recommend/guessYouLike/unlogin", requestparams, fromBindView, toBindView, true));
        } else
        if (type == 0)
        {
            mData = parseData(f.a().a("mobile/discovery/v1/recommend/editor", requestparams, fromBindView, toBindView, true));
        } else
        if (type == 2)
        {
            requestparams.put("categoryId", categoryId);
            requestparams.put("subfieldName", subfieldName);
            if (TextUtils.isEmpty(tagName))
            {
                requestparams.put("tagName", "");
            } else
            {
                requestparams.put("tagName", tagName);
            }
            mData = parseData(f.a().a("mobile/discovery/v1/category/subfield/albums", requestparams, fromBindView, toBindView, true));
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
