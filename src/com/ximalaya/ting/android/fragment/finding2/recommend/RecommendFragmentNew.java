// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.recommend;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.adapter.FocusImageAdapter;
import com.ximalaya.ting.android.animation.FixedSpeedScroller;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.finding2.category.CategoryContentFragment;
import com.ximalaya.ting.android.fragment.finding2.rank.RankDetailFragment;
import com.ximalaya.ting.android.fragment.finding2.recommend.adapter.RecommendItemHolder;
import com.ximalaya.ting.android.fragment.finding2.recommend.loader.RecommendLoader;
import com.ximalaya.ting.android.fragment.subject.SubjectFragment;
import com.ximalaya.ting.android.fragment.subject.SubjectListFragmentNew;
import com.ximalaya.ting.android.fragment.userspace.NoticePageFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.model.ad.AliBannerAd;
import com.ximalaya.ting.android.model.ad.BannerAdController;
import com.ximalaya.ting.android.model.ad.GoogleBannerAd;
import com.ximalaya.ting.android.model.ad.IThirdAd;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdEntrancesGroup;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdEntrancesItem;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdFocusImgGroup;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdHotGroupList;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdHotGroupModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemGroup;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdMainModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdOtherGroup;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdOtherItem;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdSpecialGroup;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdSpecialModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.modelmanage.ILoginStatusChangeListener;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.modelnew.FocusImageModelNew;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import com.ximalaya.ting.android.transaction.b.a;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.util.ViewUtil;
import com.ximalaya.ting.android.view.viewpager.ViewPagerInScroll;
import com.ximalaya.ting.android.view.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.recommend:
//            RecommendListFragment

public class RecommendFragmentNew extends BaseFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener
{
    private class RecommendSectionAdapter extends BaseAdapter
    {

        final RecommendFragmentNew this$0;

        public int getCount()
        {
            if (mData == null || mData.getHotRecommends() == null || mData.getHotRecommends().getList() == null)
            {
                return 0;
            } else
            {
                return mData.getHotRecommends().getList().size();
            }
        }

        public Object getItem(int i)
        {
            if (mData == null || mData.getHotRecommends() == null || mData.getHotRecommends().getList() == null)
            {
                return null;
            } else
            {
                return mData.getHotRecommends().getList().get(i);
            }
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            boolean flag = false;
            Object obj;
            RecmdHotGroupModel recmdhotgroupmodel;
            TextView textview;
            if (view == null)
            {
                view = View.inflate(getActivity(), 0x7f0301b7, null);
                viewgroup = new ViewHolder(null);
                viewgroup.h1 = RecommendItemHolder.findView(view.findViewById(0x7f0a06a2));
                viewgroup.h2 = RecommendItemHolder.findView(view.findViewById(0x7f0a06a3));
                viewgroup.h3 = RecommendItemHolder.findView(view.findViewById(0x7f0a06a4));
                viewgroup.title = (TextView)view.findViewById(0x7f0a0158);
                viewgroup.moreBtn = view.findViewById(0x7f0a0444);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (ViewHolder)view.getTag();
            }
            resetLayoutWidthAndHeight(((ViewHolder) (viewgroup)).h1, ((ViewHolder) (viewgroup)).h2, ((ViewHolder) (viewgroup)).h3);
            recmdhotgroupmodel = (RecmdHotGroupModel)mData.getHotRecommends().getList().get(i);
            bindData(2, false, recmdhotgroupmodel.isTrack(), ((ViewHolder) (viewgroup)).h1, (RecmdItemModel)recmdhotgroupmodel.getList().get(0), recmdhotgroupmodel.getTitle(), "1");
            bindData(2, false, recmdhotgroupmodel.isTrack(), ((ViewHolder) (viewgroup)).h2, (RecmdItemModel)recmdhotgroupmodel.getList().get(1), recmdhotgroupmodel.getTitle(), "2");
            bindData(2, false, recmdhotgroupmodel.isTrack(), ((ViewHolder) (viewgroup)).h3, (RecmdItemModel)recmdhotgroupmodel.getList().get(2), recmdhotgroupmodel.getTitle(), "3");
            textview = ((ViewHolder) (viewgroup)).title;
            if (recmdhotgroupmodel.getTitle() == null)
            {
                obj = "";
            } else
            {
                obj = recmdhotgroupmodel.getTitle();
            }
            textview.setText(((CharSequence) (obj)));
            obj = ((ViewHolder) (viewgroup)).moreBtn;
            if (recmdhotgroupmodel.isHasMore())
            {
                i = ((flag) ? 1 : 0);
            } else
            {
                i = 8;
            }
            ((View) (obj)).setVisibility(i);
            ((ViewHolder) (viewgroup)).moreBtn.setTag(recmdhotgroupmodel);
            ((ViewHolder) (viewgroup)).moreBtn.setOnClickListener(RecommendFragmentNew.this);
            return view;
        }

        private RecommendSectionAdapter()
        {
            this$0 = RecommendFragmentNew.this;
            super();
        }

        RecommendSectionAdapter(_cls1 _pcls1)
        {
            this();
        }
    }

    private class ViewHolder
    {

        RecommendItemHolder h1;
        RecommendItemHolder h2;
        RecommendItemHolder h3;
        View moreBtn;
        final RecommendFragmentNew this$0;
        TextView title;

        private ViewHolder()
        {
            this$0 = RecommendFragmentNew.this;
            super();
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private static final String DISCOVERY_H5_AUDIO = "html5.audio";
    private static final String DISCOVERY_H5_MALL = "html5.mall";
    private static final String DISCOVERY_H5_PREFIX = "html5.";
    private static final String MORE_GUESSLIKE = "more_guesslike";
    private static final String MORE_RECOMMEND = "recommend";
    private static final String MORE_SPECIAL = "special";
    private static long SWAP_TIME_SLICES = 5000L;
    private Loader guessLikeLoader;
    private LayoutInflater inflater;
    boolean is_new_pushed_message;
    boolean is_pushed_message_clicked;
    private RecommendSectionAdapter mAdapter;
    private Runnable mAutoSwapRunnable;
    private BannerAdController mBannerAdController;
    private RecmdMainModel mData;
    private PagerAdapter mFocusAdapter;
    private View mFocusImageRoot;
    private ArrayList mFocusImages;
    private int mFocusIndex;
    private CirclePageIndicator mFocusIndicator;
    private ImageView mFocusLoading;
    private ViewPagerInScroll mFocusPager;
    private LinearLayout mFooterView;
    private android.support.v4.app.LoaderManager.LoaderCallbacks mGuessLikeCallback;
    private RecmdItemGroup mGuessLikeData;
    private View mGuessLikeLayout;
    private View mGuessLikeMoreBtn;
    private TextView mGuessLikeTitle;
    private LinearLayout mHeaderView;
    private boolean mIsLoadedData;
    private boolean mIsLoadedGuessLike;
    private boolean mIsResumeDone;
    private ListView mListView;
    private ImageView mLiveEntryImg;
    private View mLiveEntryLayout;
    private TextView mLiveEntryTxt;
    private View mLoadingView;
    private ILoginStatusChangeListener mLoginListener;
    private View mMoreCategoryLayout;
    private TextView mNotice;
    private View mNoticeLayout;
    private LinearLayout mOtherContainer;
    private View mOtherLayout;
    private TextView mOtherTitle;
    private View mRecommendLayout;
    private View mRecommendMoreBtn;
    private TextView mRecommendTitle;
    private LinearLayout mSpecialContainer;
    private View mSpecialLayout;
    private TextView mSpecialTitle;
    private IThirdAd mThirdBannerAd;

    public RecommendFragmentNew()
    {
        mFocusImages = new ArrayList();
        mIsLoadedData = false;
        mIsLoadedGuessLike = false;
        mFocusIndex = 0;
        mAutoSwapRunnable = new _cls5();
        mGuessLikeCallback = new _cls9();
    }

    private void addThirdBannerAd()
    {
        if (mThirdBannerAd != null) goto _L2; else goto _L1
_L1:
        mThirdBannerAd = (new a()).a(0).a(getActivity());
        if (mThirdBannerAd == null) goto _L2; else goto _L3
_L3:
        Object obj;
        obj = new android.widget.LinearLayout.LayoutParams(-2, -2);
        obj.gravity = 1;
        mFooterView.addView(mThirdBannerAd.getView(), ((android.view.ViewGroup.LayoutParams) (obj)));
        mThirdBannerAd.loadAd();
        obj = new AdCollectData();
        if (!(mThirdBannerAd instanceof AliBannerAd)) goto _L5; else goto _L4
_L4:
        ((AdCollectData) (obj)).setAdSource("3");
_L7:
        ((AdCollectData) (obj)).setAndroidId(ToolUtil.getAndroidId(mCon.getApplicationContext()));
        ((AdCollectData) (obj)).setLogType("tingShow");
        ((AdCollectData) (obj)).setPositionName("find_banner");
        ((AdCollectData) (obj)).setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
        ((AdCollectData) (obj)).setTrackId("-1");
        DataCollectUtil.getInstance(mCon.getApplicationContext()).statOnlineAd(((AdCollectData) (obj)));
_L2:
        return;
_L5:
        if (mThirdBannerAd instanceof GoogleBannerAd)
        {
            ((AdCollectData) (obj)).setAdSource("2");
        }
        if (true) goto _L7; else goto _L6
_L6:
    }

    private void bindData(final int from, final boolean isGuessLike, final boolean isTrack, RecommendItemHolder recommenditemholder, RecmdItemModel recmditemmodel, final String xdcsTitle, final String xdcsPosition)
    {
        ImageManager2.from(getActivity()).displayImage(recommenditemholder.cover, getCoverPath(isTrack, recmditemmodel), 0x7f0202e0);
        if (isGuessLike)
        {
            TextView textview = recommenditemholder.name;
            String s;
            if (recmditemmodel.getTitle() == null)
            {
                s = "";
            } else
            {
                s = recmditemmodel.getTitle();
            }
            textview.setText(s);
        } else
        {
            TextView textview1 = recommenditemholder.name;
            String s1;
            if (TextUtils.isEmpty(recmditemmodel.getTrackTitle()))
            {
                s1 = "";
            } else
            {
                s1 = recmditemmodel.getTrackTitle();
            }
            textview1.setText(s1);
        }
        if (isGuessLike)
        {
            if (TextUtils.isEmpty(recmditemmodel.getTags()))
            {
                recommenditemholder.desc.setVisibility(8);
            } else
            {
                recommenditemholder.desc.setVisibility(0);
                recommenditemholder.desc.setText(StringUtil.getFirstTag(recmditemmodel.getTags()));
            }
        } else
        {
            TextView textview2 = recommenditemholder.desc;
            String s2;
            if (recmditemmodel.getTitle() == null)
            {
                s2 = "";
            } else
            {
                s2 = recmditemmodel.getTitle();
            }
            textview2.setText(s2);
        }
        if (isTrack)
        {
            recommenditemholder.album_edge.setVisibility(8);
            recommenditemholder.ic_play.setVisibility(0);
        } else
        {
            ImageView imageview = recommenditemholder.complete;
            int i;
            if (recmditemmodel.getIsFinished() == 2)
            {
                i = 0;
            } else
            {
                i = 8;
            }
            imageview.setVisibility(i);
            recommenditemholder.ic_play.setVisibility(8);
            recommenditemholder.album_edge.setVisibility(0);
        }
        recommenditemholder.cover.setTag(recmditemmodel);
        recommenditemholder.cover.setOnClickListener(new _cls10());
    }

    private void clearAdRef()
    {
        mThirdBannerAd = null;
    }

    private void clearRef()
    {
        mSpecialContainer.removeAllViews();
        mSpecialContainer = null;
        clearAdRef();
    }

    private void filterInvalidHotRecommends(RecmdMainModel recmdmainmodel)
    {
        if (recmdmainmodel != null && recmdmainmodel.getHotRecommends() != null && recmdmainmodel.getHotRecommends().getList() != null)
        {
            recmdmainmodel = recmdmainmodel.getHotRecommends().getList().iterator();
            while (recmdmainmodel.hasNext()) 
            {
                RecmdHotGroupModel recmdhotgroupmodel = (RecmdHotGroupModel)recmdmainmodel.next();
                if (recmdhotgroupmodel.getList() == null || recmdhotgroupmodel.getList().size() < 3)
                {
                    recmdmainmodel.remove();
                }
            }
        }
    }

    private String getCoverPath(boolean flag, RecmdItemModel recmditemmodel)
    {
        if (flag)
        {
            return recmditemmodel.getCoverMiddle();
        } else
        {
            return recmditemmodel.getCoverLarge();
        }
    }

    private HashMap getDiscoveryUpdatePref()
    {
        HashMap hashmap;
        HashMap hashmap1;
        try
        {
            hashmap = SharedPreferencesUtil.getInstance(mCon).getHashMapByKey("recommend_discovery_update");
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            exception = null;
        }
        hashmap1 = hashmap;
        if (hashmap == null)
        {
            hashmap1 = new HashMap();
        }
        return hashmap1;
    }

    private void initDefaultFocusImg()
    {
        FocusImageModelNew focusimagemodelnew = new FocusImageModelNew();
        mFocusImages.add(focusimagemodelnew);
        updateFocusImageBar();
    }

    private void initFocusImage(LayoutInflater layoutinflater)
    {
        mFocusImageRoot = layoutinflater.inflate(0x7f03008b, null);
        mHeaderView.addView(mFocusImageRoot);
        mFocusImageRoot.setVisibility(8);
        mFocusPager = (ViewPagerInScroll)mFocusImageRoot.findViewById(0x7f0a017a);
        mFocusPager.setDisallowInterceptTouchEventView((ViewGroup)mFocusPager.getParent());
        mFocusIndicator = (CirclePageIndicator)mFocusImageRoot.findViewById(0x7f0a0230);
        mFocusLoading = (ImageView)mFocusImageRoot.findViewById(0x7f0a0275);
        layoutinflater = mFocusImageRoot.getLayoutParams();
        int i = ToolUtil.getScreenWidth(mActivity);
        int j = (int)((float)i * 0.46875F);
        layoutinflater.width = i;
        layoutinflater.height = j;
        mFocusImageRoot.setLayoutParams(layoutinflater);
        layoutinflater = new FixedSpeedScroller(mFocusPager.getContext(), new DecelerateInterpolator());
        ViewUtil.setViewPagerScroller(mFocusPager, layoutinflater);
        mFocusAdapter = new FocusImageAdapter(this, mFocusImages);
        ((FocusImageAdapter)mFocusAdapter).setCycleScrollFlag(true);
        mFocusPager.setAdapter(mFocusAdapter);
        mFocusIndicator.setViewPager(mFocusPager);
        mFocusIndicator.setOnPageChangeListener(new _cls2());
        initDefaultFocusImg();
        mFocusLoading.setImageResource(0x7f02025e);
        mFocusLoading.setVisibility(0);
    }

    private void initListener()
    {
        mGuessLikeMoreBtn.setTag("more_guesslike");
        mRecommendMoreBtn.setTag("recommend");
        mSpecialLayout.findViewById(0x7f0a0444).setTag("special");
        mRecommendMoreBtn.setOnClickListener(this);
        mGuessLikeMoreBtn.setOnClickListener(this);
        mSpecialLayout.findViewById(0x7f0a0444).setOnClickListener(this);
    }

    private void initViews()
    {
        if (PackageUtil.isMeizu() && getActivity() != null)
        {
            findViewById(0x7f0a00dc).setPadding(0, 0, 0, Utilities.dip2px(getActivity(), 70F));
        }
        mListView = (ListView)findViewById(0x7f0a00dc);
        mHeaderView = new LinearLayout(mCon);
        mHeaderView.setOrientation(1);
        initFocusImage(inflater);
        mGuessLikeLayout = inflater.inflate(0x7f0301b7, null);
        android.view.ViewGroup.LayoutParams layoutparams;
        View view;
        if (mIsLoadedData && mData != null && needShowGuessLike(mGuessLikeData))
        {
            mGuessLikeLayout.setVisibility(0);
        } else
        {
            mGuessLikeLayout.setVisibility(8);
        }
        mGuessLikeTitle = (TextView)mGuessLikeLayout.findViewById(0x7f0a0158);
        mGuessLikeMoreBtn = mGuessLikeLayout.findViewById(0x7f0a0444);
        mHeaderView.addView(mGuessLikeLayout);
        mRecommendLayout = inflater.inflate(0x7f0301b7, null);
        if (mIsLoadedData && mData != null && needShowEditorRecommend(mData.getEditorRecommendAlbums()))
        {
            mRecommendLayout.setVisibility(0);
        } else
        {
            mRecommendLayout.setVisibility(8);
        }
        mRecommendTitle = (TextView)mRecommendLayout.findViewById(0x7f0a0158);
        mRecommendMoreBtn = mRecommendLayout.findViewById(0x7f0a0444);
        mHeaderView.addView(mRecommendLayout);
        mSpecialLayout = inflater.inflate(0x7f0301b8, null);
        mSpecialTitle = (TextView)mSpecialLayout.findViewById(0x7f0a0158);
        mSpecialLayout.setVisibility(8);
        mSpecialContainer = (LinearLayout)mSpecialLayout.findViewById(0x7f0a005a);
        mHeaderView.addView(mSpecialLayout);
        mOtherLayout = inflater.inflate(0x7f0301b8, null);
        mOtherTitle = (TextView)mOtherLayout.findViewById(0x7f0a0158);
        mOtherLayout.setVisibility(8);
        mOtherLayout.findViewById(0x7f0a0444).setVisibility(8);
        mOtherContainer = (LinearLayout)mOtherLayout.findViewById(0x7f0a005a);
        mHeaderView.addView(mOtherLayout);
        mListView.addHeaderView(mHeaderView);
        mMoreCategoryLayout = inflater.inflate(0x7f030055, null);
        mMoreCategoryLayout.setOnClickListener(this);
        mLiveEntryLayout = inflater.inflate(0x7f0301b5, null);
        mLiveEntryTxt = (TextView)mLiveEntryLayout.findViewById(0x7f0a069e);
        mLiveEntryImg = (ImageView)mLiveEntryLayout.findViewById(0x7f0a01a8);
        mLiveEntryLayout.setOnClickListener(this);
        view = new View(mCon);
        view.setBackgroundResource(0x7f070010);
        layoutparams = new android.view.ViewGroup.LayoutParams(-1, Utilities.dip2px(getActivity(), 10F));
        mFooterView = new LinearLayout(mCon);
        mFooterView.setOrientation(1);
        mFooterView.addView(mMoreCategoryLayout);
        if (!TingMediaPlayer.getTingMediaPlayer(getActivity().getApplicationContext()).isUseSystemPlayer())
        {
            mFooterView.addView(view, layoutparams);
            mFooterView.addView(mLiveEntryLayout);
        }
        view = new View(mCon);
        view.setBackgroundResource(0x7f070010);
        mFooterView.addView(view, layoutparams);
        mListView.addFooterView(mFooterView);
        mLoadingView = findViewById(0x7f0a012c);
        mAdapter = new RecommendSectionAdapter(null);
        mListView.setAdapter(mAdapter);
        if (!mIsLoadedData && mData != null)
        {
            parseEditorRecommend(mData);
            parseGuessLike(mData.getGuessYouLikeAlbums());
            parseSpecialColumn(mData);
            parseDiscoveryColumn(mData);
            return;
        } else
        {
            mLoadingView.setVisibility(0);
            return;
        }
    }

    private void loadData()
    {
        if (!getUserVisibleHint() || getView() == null)
        {
            return;
        }
        if (mIsLoadedData) goto _L2; else goto _L1
_L1:
        getLoaderManager().initLoader(0x7f0a0029, null, this);
_L4:
        loadGuessLike();
        return;
_L2:
        mLoadingView.setVisibility(8);
        if (mData != null)
        {
            setDataForView(mData);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void loadGuessLike()
    {
        if (UserInfoMannage.hasLogined())
        {
            if (!mIsLoadedGuessLike || loginJustNow())
            {
                mIsLoadedGuessLike = true;
                if (guessLikeLoader == null)
                {
                    guessLikeLoader = getLoaderManager().initLoader(0x7f0a002a, null, mGuessLikeCallback);
                } else
                {
                    guessLikeLoader = getLoaderManager().restartLoader(0x7f0a002a, null, mGuessLikeCallback);
                }
                ((MyAsyncTaskLoader)guessLikeLoader).setXDCSBindView(mGuessLikeLayout, mGuessLikeLayout);
                ((MyApplication)mActivity.getApplication()).e = 0;
                return;
            } else
            {
                parseGuessLike(mGuessLikeData);
                return;
            }
        }
        if (!mIsLoadedGuessLike)
        {
            mIsLoadedGuessLike = true;
            if (guessLikeLoader == null)
            {
                guessLikeLoader = getLoaderManager().initLoader(0x7f0a002b, null, mGuessLikeCallback);
            } else
            {
                guessLikeLoader = getLoaderManager().restartLoader(0x7f0a002b, null, mGuessLikeCallback);
            }
            ((MyAsyncTaskLoader)guessLikeLoader).setXDCSBindView(mGuessLikeLayout, mGuessLikeLayout);
            return;
        } else
        {
            parseGuessLike(mGuessLikeData);
            return;
        }
    }

    private boolean loginJustNow()
    {
        return mActivity != null && ((MyApplication)mActivity.getApplication()).e == 1;
    }

    private boolean needShowEditorRecommend(RecmdItemGroup recmditemgroup)
    {
        return recmditemgroup != null && recmditemgroup.getRet() == 0 && recmditemgroup.getList() != null && recmditemgroup.getList().size() >= 3;
    }

    public static boolean needShowGuessLike(RecmdItemGroup recmditemgroup)
    {
        for (LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser(); recmditemgroup != null && recmditemgroup.getUid() != 0L && UserInfoMannage.hasLogined() && recmditemgroup.getUid() != logininfomodel.getUid().longValue() || recmditemgroup == null || recmditemgroup.getRet() != 0 || recmditemgroup.getList() == null || recmditemgroup.getList().size() < 3;)
        {
            return false;
        }

        return true;
    }

    public static RecommendFragmentNew newInstance()
    {
        return new RecommendFragmentNew();
    }

    private void parseDiscoveryColumn(RecmdMainModel recmdmainmodel)
    {
        RecmdOtherGroup recmdothergroup = recmdmainmodel.getDiscoveryColumns();
        if (recmdothergroup != null && recmdothergroup.getRet() == 0 && recmdothergroup.getList() != null && !recmdothergroup.getList().isEmpty())
        {
            if (mOtherLayout.getVisibility() != 0)
            {
                mOtherLayout.setVisibility(0);
            }
            TextView textview = mOtherTitle;
            if (TextUtils.isEmpty(recmdothergroup.getTitle()))
            {
                recmdmainmodel = "\u53D1\u73B0\u65B0\u5947";
            } else
            {
                recmdmainmodel = recmdothergroup.getTitle();
            }
            textview.setText(recmdmainmodel);
            updateDiscoveryColumnList(recmdothergroup.getList());
            return;
        } else
        {
            mOtherLayout.setVisibility(8);
            return;
        }
    }

    private void parseEditorRecommend(RecmdMainModel recmdmainmodel)
    {
        RecmdItemGroup recmditemgroup = recmdmainmodel.getEditorRecommendAlbums();
        if (needShowEditorRecommend(recmditemgroup))
        {
            if (mRecommendLayout.getVisibility() != 0)
            {
                mRecommendLayout.setVisibility(0);
            }
            recmdmainmodel = RecommendItemHolder.findView(mRecommendLayout.findViewById(0x7f0a06a2));
            Object obj = RecommendItemHolder.findView(mRecommendLayout.findViewById(0x7f0a06a3));
            RecommendItemHolder recommenditemholder = RecommendItemHolder.findView(mRecommendLayout.findViewById(0x7f0a06a4));
            bindData(6, false, false, recmdmainmodel, (RecmdItemModel)recmditemgroup.getList().get(0), recmditemgroup.getTitle(), "1");
            bindData(6, false, false, ((RecommendItemHolder) (obj)), (RecmdItemModel)recmditemgroup.getList().get(1), recmditemgroup.getTitle(), "2");
            bindData(6, false, false, recommenditemholder, (RecmdItemModel)recmditemgroup.getList().get(2), recmditemgroup.getTitle(), "3");
            resetLayoutWidthAndHeight(recmdmainmodel, ((RecommendItemHolder) (obj)), recommenditemholder);
            obj = mRecommendTitle;
            int i;
            if (TextUtils.isEmpty(recmditemgroup.getTitle()))
            {
                recmdmainmodel = "\u5C0F\u7F16\u63A8\u8350";
            } else
            {
                recmdmainmodel = recmditemgroup.getTitle();
            }
            ((TextView) (obj)).setText(recmdmainmodel);
            recmdmainmodel = mRecommendMoreBtn;
            if (recmditemgroup.isHasMore() || recmditemgroup.getList().size() > 3)
            {
                i = 0;
            } else
            {
                i = 8;
            }
            recmdmainmodel.setVisibility(i);
            mRecommendLayout.findViewById(0x7f0a06a5).setVisibility(0);
            return;
        } else
        {
            mRecommendLayout.setVisibility(8);
            return;
        }
    }

    private void parseFocusImage(RecmdMainModel recmdmainmodel)
    {
        while (recmdmainmodel.getFocusImages() == null || recmdmainmodel.getFocusImages().getRet() != 0 || recmdmainmodel.getFocusImages().getList() == null || recmdmainmodel.getFocusImages().getList().size() <= 0) 
        {
            return;
        }
        mFocusImages.clear();
        mFocusImages.addAll(recmdmainmodel.getFocusImages().getList());
        mFocusImageRoot.setVisibility(0);
        updateFocusImageBar();
    }

    private void parseGuessLike(RecmdItemGroup recmditemgroup)
    {
        if (mLoadingView.getVisibility() == 0)
        {
            return;
        }
        if (needShowGuessLike(recmditemgroup))
        {
            if (mGuessLikeLayout.getVisibility() != 0)
            {
                mGuessLikeLayout.setVisibility(0);
            }
            Object obj1 = mGuessLikeTitle;
            Object obj;
            RecommendItemHolder recommenditemholder;
            int i;
            if (TextUtils.isEmpty(recmditemgroup.getTitle()))
            {
                obj = "\u731C\u4F60\u559C\u6B22";
            } else
            {
                obj = recmditemgroup.getTitle();
            }
            ((TextView) (obj1)).setText(((CharSequence) (obj)));
            obj = RecommendItemHolder.findView(mGuessLikeLayout.findViewById(0x7f0a06a2));
            obj1 = RecommendItemHolder.findView(mGuessLikeLayout.findViewById(0x7f0a06a3));
            recommenditemholder = RecommendItemHolder.findView(mGuessLikeLayout.findViewById(0x7f0a06a4));
            bindData(12, true, false, ((RecommendItemHolder) (obj)), (RecmdItemModel)recmditemgroup.getList().get(0), recmditemgroup.getTitle(), "1");
            bindData(12, true, false, ((RecommendItemHolder) (obj1)), (RecmdItemModel)recmditemgroup.getList().get(1), recmditemgroup.getTitle(), "2");
            bindData(12, true, false, recommenditemholder, (RecmdItemModel)recmditemgroup.getList().get(2), recmditemgroup.getTitle(), "3");
            resetLayoutWidthAndHeight(((RecommendItemHolder) (obj)), ((RecommendItemHolder) (obj1)), recommenditemholder);
            obj = mGuessLikeMoreBtn;
            if (recmditemgroup.getList().size() > 3 || recmditemgroup.isHasMore())
            {
                i = 0;
            } else
            {
                i = 8;
            }
            ((View) (obj)).setVisibility(i);
            mGuessLikeLayout.findViewById(0x7f0a06a5).setVisibility(0);
            return;
        } else
        {
            mGuessLikeLayout.setVisibility(8);
            return;
        }
    }

    private void parseLiveEntrance(RecmdMainModel recmdmainmodel)
    {
_L2:
        return;
        if (recmdmainmodel == null || recmdmainmodel.getEntrances() == null || recmdmainmodel.getEntrances().getRet() != 0 || recmdmainmodel.getEntrances().getList() == null || recmdmainmodel.getEntrances().getList().size() <= 0) goto _L2; else goto _L1
_L1:
        RecmdEntrancesItem recmdentrancesitem;
        recmdmainmodel = recmdmainmodel.getEntrances().getList().iterator();
        do
        {
            if (!recmdmainmodel.hasNext())
            {
                continue; /* Loop/switch isn't completed */
            }
            recmdentrancesitem = (RecmdEntrancesItem)recmdmainmodel.next();
        } while (!"live".equals(recmdentrancesitem.getEntranceType()));
        break; /* Loop/switch isn't completed */
        if (true) goto _L2; else goto _L3
_L3:
        mLiveEntryTxt.setText(recmdentrancesitem.getTitle());
        ImageManager2.from(getActivity()).displayImage(mLiveEntryImg, recmdentrancesitem.getCoverPath(), -1);
        return;
    }

    private void parseSpecialColumn(RecmdMainModel recmdmainmodel)
    {
        RecmdSpecialGroup recmdspecialgroup = recmdmainmodel.getSpecialColumn();
        if (recmdspecialgroup != null && recmdspecialgroup.getRet() == 0 && recmdspecialgroup.getList() != null && !recmdspecialgroup.getList().isEmpty())
        {
            if (mSpecialLayout.getVisibility() != 0)
            {
                mSpecialLayout.setVisibility(0);
            }
            TextView textview = mSpecialTitle;
            String s;
            if (TextUtils.isEmpty(recmdspecialgroup.getTitle()))
            {
                s = "\u7279\u8272\u542C\u5355";
            } else
            {
                s = recmdspecialgroup.getTitle();
            }
            textview.setText(s);
            updateSpecialList(recmdspecialgroup.getList());
            if (recmdmainmodel.getDiscoveryColumns() != null && recmdmainmodel.getDiscoveryColumns().getRet() == 0 && recmdmainmodel.getDiscoveryColumns().getList() != null && !recmdmainmodel.getDiscoveryColumns().getList().isEmpty())
            {
                mSpecialLayout.findViewById(0x7f0a06a5).setVisibility(0);
            }
            return;
        } else
        {
            mSpecialLayout.setVisibility(8);
            return;
        }
    }

    private void resetLayoutWidthAndHeight(RecommendItemHolder recommenditemholder, RecommendItemHolder recommenditemholder1, RecommendItemHolder recommenditemholder2)
    {
        if (canGoon() && getActivity() != null && !getActivity().isFinishing())
        {
            int i = (ToolUtil.getScreenWidth(getActivity()) - ToolUtil.dp2px(getActivity(), 40F)) / 3;
            android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)recommenditemholder.cover.getLayoutParams();
            layoutparams.height = i;
            recommenditemholder.cover.setLayoutParams(layoutparams);
            recommenditemholder = (android.widget.RelativeLayout.LayoutParams)recommenditemholder1.cover.getLayoutParams();
            recommenditemholder.height = i;
            recommenditemholder1.cover.setLayoutParams(recommenditemholder);
            recommenditemholder = (android.widget.RelativeLayout.LayoutParams)recommenditemholder2.cover.getLayoutParams();
            recommenditemholder.height = i;
            recommenditemholder2.cover.setLayoutParams(recommenditemholder);
        }
    }

    private void saveUpdatedToPref(RecmdOtherItem recmdotheritem)
    {
        HashMap hashmap = getDiscoveryUpdatePref();
        String s = (String)hashmap.get((new StringBuilder()).append(recmdotheritem.getId()).append("").toString());
        if (TextUtils.isEmpty(s) || Long.valueOf(s).longValue() < recmdotheritem.getContentUpdatedAt())
        {
            hashmap.put((new StringBuilder()).append(recmdotheritem.getId()).append("").toString(), (new StringBuilder()).append(recmdotheritem.getContentUpdatedAt()).append("").toString());
            SharedPreferencesUtil.getInstance(mCon).saveHashMap("recommend_discovery_update", hashmap);
        }
    }

    private void setDataForView(RecmdMainModel recmdmainmodel)
    {
        if (recmdmainmodel != null && recmdmainmodel.ret == 0 && canGoon())
        {
            parseFocusImage(recmdmainmodel);
            parseEditorRecommend(recmdmainmodel);
            parseSpecialColumn(recmdmainmodel);
            parseDiscoveryColumn(mData);
            parseLiveEntrance(recmdmainmodel);
            if (mAdapter == null)
            {
                mAdapter = new RecommendSectionAdapter(null);
                mListView.setAdapter(mAdapter);
            } else
            {
                mAdapter.notifyDataSetChanged();
            }
            if (mMoreCategoryLayout.getVisibility() != 0)
            {
                mMoreCategoryLayout.setVisibility(0);
            }
            if (!TingMediaPlayer.getTingMediaPlayer(getActivity().getApplicationContext()).isUseSystemPlayer() && mLiveEntryLayout.getVisibility() != 0)
            {
                mLiveEntryLayout.setVisibility(0);
                return;
            }
        }
    }

    private void startAutoSwapFocusImage()
    {
        if (mFocusAdapter != null)
        {
            fragmentBaseContainerView.postDelayed(mAutoSwapRunnable, SWAP_TIME_SLICES);
        }
    }

    private void stopAutoSwapFocusImage()
    {
        if (fragmentBaseContainerView != null)
        {
            fragmentBaseContainerView.removeCallbacks(mAutoSwapRunnable);
        }
    }

    private void toAlbumFragment(RecmdItemModel recmditemmodel, boolean flag, int i, View view)
    {
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = recmditemmodel.getAlbumId();
        Bundle bundle = new Bundle();
        bundle.putString("album", JSON.toJSONString(albummodel));
        bundle.putInt("from", i);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        bundle.putLong("track_id_to_play", recmditemmodel.getTrackId());
        if (flag)
        {
            bundle.putString("rec_src", recmditemmodel.getRecSrc());
            bundle.putString("rec_track", recmditemmodel.getRecTrack());
        }
        startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
    }

    private void toRankDetail(RecmdSpecialModel recmdspecialmodel, View view, int i)
    {
        Bundle bundle = new Bundle();
        bundle.putString("key", recmdspecialmodel.getKey());
        bundle.putString("title", recmdspecialmodel.getTitle());
        bundle.putString("type", recmdspecialmodel.getContentType());
        bundle.putString("position", (new StringBuilder()).append(i).append("").toString());
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/finding2/rank/RankDetailFragment, bundle);
    }

    private void toSubjectDetail(RecmdSpecialModel recmdspecialmodel, View view, int i)
    {
        Bundle bundle = new Bundle();
        bundle.putLong("subjectId", recmdspecialmodel.getSpecialId());
        int j = 1;
        if (!TextUtils.isEmpty(recmdspecialmodel.getContentType()))
        {
            j = Integer.valueOf(recmdspecialmodel.getContentType()).intValue();
        }
        bundle.putInt("contentType", j);
        bundle.putString("title", "\u7CBE\u54C1\u542C\u5355");
        bundle.putString("position", (new StringBuilder()).append(i).append("").toString());
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/subject/SubjectFragment, bundle);
    }

    private void updateDiscoveryColumnList(List list)
    {
        int j = list.size();
        if (j == 0)
        {
            mOtherLayout.setVisibility(8);
        } else
        {
            mOtherContainer.removeAllViews();
            HashMap hashmap = getDiscoveryUpdatePref();
            int i = 0;
            while (i < j) 
            {
                RecmdOtherItem recmdotheritem = (RecmdOtherItem)list.get(i);
                ViewGroup viewgroup = (ViewGroup)inflater.inflate(0x7f030140, mOtherContainer, false);
                viewgroup.setPadding(0, Utilities.dip2px(mActivity, 5F), 0, 0);
                Object obj = viewgroup.getLayoutParams();
                obj.height = Utilities.dip2px(mActivity, 70F);
                viewgroup.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
                obj = (ImageView)viewgroup.findViewById(0x7f0a0177);
                ((ImageView) (obj)).setBackgroundResource(0);
                final TextView name = (android.widget.RelativeLayout.LayoutParams)((ImageView) (obj)).getLayoutParams();
                int k = Utilities.dip2px(mActivity, 59F);
                name.width = k;
                name.height = k;
                ((ImageView) (obj)).setLayoutParams(name);
                markImageView(((ImageView) (obj)));
                TextView textview;
                if ("activity".equals(recmdotheritem.getContentType()))
                {
                    ImageManager2.from(mActivity).displayImage(((ImageView) (obj)), recmdotheritem.getCoverPath(), 0x7f020238);
                } else
                if ("xzone".equals(recmdotheritem.getContentType()))
                {
                    ImageManager2.from(mActivity).displayImage(((ImageView) (obj)), recmdotheritem.getCoverPath(), 0x7f020243);
                } else
                if ("html5.mall".equals(recmdotheritem.getContentType()))
                {
                    ImageManager2.from(mActivity).displayImage(((ImageView) (obj)), recmdotheritem.getCoverPath(), 0x7f02023f);
                } else
                if ("html5.audio".equals(recmdotheritem.getContentType()))
                {
                    ImageManager2.from(mActivity).displayImage(((ImageView) (obj)), recmdotheritem.getCoverPath(), 0x7f020237);
                } else
                {
                    ImageManager2.from(mActivity).displayImage(((ImageView) (obj)), recmdotheritem.getCoverPath(), 0x7f0202e0);
                }
                name = (TextView)viewgroup.findViewById(0x7f0a0449);
                name.setTextSize(17F);
                if (recmdotheritem.getTitle() == null)
                {
                    obj = "";
                } else
                {
                    obj = recmdotheritem.getTitle();
                }
                name.setText(((CharSequence) (obj)));
                obj = (android.widget.RelativeLayout.LayoutParams)name.getLayoutParams();
                obj.leftMargin = Utilities.dip2px(mActivity, 13F);
                obj.topMargin = Utilities.dip2px(mActivity, 6F);
                name.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
                ((ImageView)viewgroup.findViewById(0x7f0a04f1)).setVisibility(8);
                textview = (TextView)viewgroup.findViewById(0x7f0a01ae);
                textview.setTextSize(12F);
                textview.setMaxLines(1);
                if (recmdotheritem.getSubtitle() == null)
                {
                    obj = "";
                } else
                {
                    obj = recmdotheritem.getSubtitle();
                }
                textview.setText(((CharSequence) (obj)));
                obj = (android.widget.RelativeLayout.LayoutParams)textview.getLayoutParams();
                obj.topMargin = Utilities.dip2px(mActivity, 6F);
                textview.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
                if (TextUtils.isEmpty(recmdotheritem.getSubtitle()))
                {
                    textview.setVisibility(8);
                }
                viewgroup.findViewById(0x7f0a044a).setVisibility(8);
                if (i == j - 1)
                {
                    viewgroup.findViewById(0x7f0a00a8).setVisibility(8);
                }
                if (("html5".equals(recmdotheritem.getContentType()) || recmdotheritem.getContentType().startsWith("html5.")) && recmdotheritem.getContentUpdatedAt() > 0L)
                {
                    obj = (String)hashmap.get((new StringBuilder()).append(recmdotheritem.getId()).append("").toString());
                    if (TextUtils.isEmpty(((CharSequence) (obj))) || Long.valueOf(((String) (obj))).longValue() < recmdotheritem.getContentUpdatedAt())
                    {
                        name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f0204f6, 0);
                    }
                }
                viewgroup.setOnClickListener(new _cls3());
                if ("html5.mall".equals(recmdotheritem.getContentType()))
                {
                    MyDeviceManager.getInstance(mCon).sharePicUrl = recmdotheritem.getSharePic();
                }
                viewgroup.setTag(recmdotheritem);
                mOtherContainer.addView(viewgroup);
                i++;
            }
        }
    }

    private void updateFocusImageBar()
    {
        if (mIsResumeDone)
        {
            ThirdAdStatUtil.getInstance().statFocusAd(mFocusImages, true);
        }
        mFocusLoading.setImageBitmap(null);
        mFocusLoading.setVisibility(8);
        if (mFocusIndex == 0)
        {
            mFocusPager.setCurrentItem(0x3fffffff - 0x3fffffff % mFocusImages.size());
        } else
        {
            mFocusPager.setCurrentItem(mFocusIndex);
        }
        mFocusIndicator.setPagerRealCount(mFocusImages.size());
        mFocusAdapter.notifyDataSetChanged();
    }

    private void updateSpecialList(List list)
    {
        int j = list.size();
        if (j == 0)
        {
            mSpecialLayout.setVisibility(8);
        } else
        {
            mSpecialContainer.removeAllViews();
            int i = 0;
            while (i < j) 
            {
                RecmdSpecialModel recmdspecialmodel = (RecmdSpecialModel)list.get(i);
                ViewGroup viewgroup = (ViewGroup)inflater.inflate(0x7f030140, mSpecialContainer, false);
                Object obj = (ImageView)viewgroup.findViewById(0x7f0a0177);
                markImageView(((ImageView) (obj)));
                ImageManager2.from(mActivity).displayImage(((ImageView) (obj)), recmdspecialmodel.getCoverPath(), 0x7f0202e0);
                TextView textview = (TextView)viewgroup.findViewById(0x7f0a0449);
                if (recmdspecialmodel.getTitle() == null)
                {
                    obj = "";
                } else
                {
                    obj = recmdspecialmodel.getTitle();
                }
                textview.setText(((CharSequence) (obj)));
                ((ImageView)viewgroup.findViewById(0x7f0a04f1)).setVisibility(8);
                textview = (TextView)viewgroup.findViewById(0x7f0a01ae);
                if (recmdspecialmodel.getSubtitle() == null)
                {
                    obj = "";
                } else
                {
                    obj = recmdspecialmodel.getSubtitle().replace("\r\n", " ");
                }
                textview.setText(((CharSequence) (obj)));
                obj = (TextView)viewgroup.findViewById(0x7f0a044a);
                if (TextUtils.isEmpty(recmdspecialmodel.getFootnote()))
                {
                    ((TextView) (obj)).setVisibility(8);
                } else
                {
                    ((TextView) (obj)).setVisibility(0);
                    ((TextView) (obj)).setText(recmdspecialmodel.getFootnote());
                }
                if (i == j - 1)
                {
                    obj = viewgroup.getLayoutParams();
                    obj.height = Utilities.dip2px(getActivity(), 83F);
                    viewgroup.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
                    viewgroup.findViewById(0x7f0a00a8).setVisibility(8);
                }
                viewgroup.setOnClickListener(new _cls4());
                viewgroup.setTag(recmdspecialmodel);
                mSpecialContainer.addView(viewgroup);
                i++;
            }
        }
    }

    public void forceUpdateGuessLike()
    {
        if (isAdded())
        {
            mIsLoadedGuessLike = false;
            loadGuessLike();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        inflater = LayoutInflater.from(getActivity());
        initViews();
        initListener();
        loadData();
        if (com.ximalaya.ting.android.a.l)
        {
            mBannerAdController = new BannerAdController(getActivity(), mFooterView, "find_banner");
            mBannerAdController.load(this);
            bundle = mBannerAdController.getView();
            android.widget.LinearLayout.LayoutParams layoutparams = new android.widget.LinearLayout.LayoutParams(-1, ((ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 10F) * 2) * 170) / 720);
            mFooterView.addView(bundle, layoutparams);
        }
    }

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    public void onClick(View view)
    {
        if (OneClickHelper.getInstance().onClick(view))
        {
            switch (view.getId())
            {
            default:
                return;

            case 2131362215: 
                fragmentBaseContainerView.post(new _cls8());
                return;

            case 2131363485: 
                fragmentBaseContainerView.post(new _cls6());
                return;

            case 2131363487: 
                com.ximalaya.ting.android.a.a = false;
                SharedPreferencesUtil.getInstance(mActivity).saveBoolean("is_pushed_message_clicked", true);
                mNoticeLayout.postDelayed(new _cls7(), 500L);
                Toast.makeText(getActivity(), "notice", 0).show();
                startFragment(new NoticePageFragment());
                return;

            case 2131362884: 
                break;
            }
            if (view.getTag() != null)
            {
                if (view.getTag() instanceof RecmdHotGroupModel)
                {
                    RecmdHotGroupModel recmdhotgroupmodel = (RecmdHotGroupModel)view.getTag();
                    startFragment(CategoryContentFragment.newInstance((int)recmdhotgroupmodel.getCategoryId(), recmdhotgroupmodel.getContentType(), recmdhotgroupmodel.getTitle(), view, true));
                    return;
                }
                if (view.getTag().equals("more_guesslike"))
                {
                    Bundle bundle = new Bundle();
                    if (UserInfoMannage.hasLogined())
                    {
                        bundle.putInt("loader_type", 1);
                    } else
                    {
                        bundle.putInt("loader_type", 3);
                    }
                    if (mGuessLikeData != null)
                    {
                        bundle.putString("title", mGuessLikeData.getTitle());
                    }
                    bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, "\u66F4\u591A"));
                    startFragment(com/ximalaya/ting/android/fragment/finding2/recommend/RecommendListFragment, bundle);
                    return;
                }
                if (view.getTag().equals("recommend"))
                {
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("loader_type", 0);
                    if (mData != null && mData.getEditorRecommendAlbums() != null)
                    {
                        bundle1.putString("title", mData.getEditorRecommendAlbums().getTitle());
                    }
                    bundle1.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, "\u66F4\u591A"));
                    startFragment(com/ximalaya/ting/android/fragment/finding2/recommend/RecommendListFragment, bundle1);
                    return;
                }
                if (view.getTag().equals("special"))
                {
                    view = new Bundle();
                    view.putString("headermore", "\u66F4\u591A");
                    startFragment(com/ximalaya/ting/android/fragment/subject/SubjectListFragmentNew, view);
                    return;
                }
            }
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        switch (i)
        {
        default:
            return null;

        case 2131361832: 
            return new RecommendLoader(getActivity(), fragmentBaseContainerView, fragmentBaseContainerView);

        case 2131361833: 
            return new RecommendLoader(getActivity(), true);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300d0, viewgroup, false);
        mIsResumeDone = false;
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        if (mFocusAdapter != null)
        {
            ((FocusImageAdapter)mFocusAdapter).destory();
        }
        if (mLoginListener != null)
        {
            UserInfoMannage.getInstance().removeLoginStatusChangeListener(mLoginListener);
            mLoginListener = null;
        }
    }

    public void onLoadFinished(Loader loader, RecmdMainModel recmdmainmodel)
    {
        mLoadingView.setVisibility(8);
        if (loader != null)
        {
            if (loader.getId() == 0x7f0a0029 && !mIsLoadedData)
            {
                mIsLoadedData = true;
                getLoaderManager().restartLoader(0x7f0a0028, null, this);
            }
            if (recmdmainmodel != null)
            {
                filterInvalidHotRecommends(recmdmainmodel);
                mData = recmdmainmodel;
                setDataForView(mData);
            }
            parseGuessLike(mGuessLikeData);
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (RecmdMainModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onPause()
    {
        stopAutoSwapFocusImage();
        if (mBannerAdController != null)
        {
            mBannerAdController.stopSwapAd();
        }
        super.onPause();
        if (mThirdBannerAd != null)
        {
            mThirdBannerAd.pause();
        }
    }

    public void onResume()
    {
        super.onResume();
        startAutoSwapFocusImage();
        ThirdAdStatUtil.getInstance().statFocusAd(mFocusImages, true);
        mIsResumeDone = true;
        if (mBannerAdController != null)
        {
            mBannerAdController.swapAd();
        }
        if (mThirdBannerAd != null)
        {
            mThirdBannerAd.resume();
        }
        if (mLoginListener == null)
        {
            mLoginListener = new _cls1();
            UserInfoMannage.getInstance().addLoginStatusChangeListener(mLoginListener);
        }
    }

    public void onStart()
    {
        super.onStart();
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        loadData();
    }










/*
    static RecmdItemGroup access$1502(RecommendFragmentNew recommendfragmentnew, RecmdItemGroup recmditemgroup)
    {
        recommendfragmentnew.mGuessLikeData = recmditemgroup;
        return recmditemgroup;
    }

*/







/*
    static int access$202(RecommendFragmentNew recommendfragmentnew, int i)
    {
        recommendfragmentnew.mFocusIndex = i;
        return i;
    }

*/


/*
    static int access$208(RecommendFragmentNew recommendfragmentnew)
    {
        int i = recommendfragmentnew.mFocusIndex;
        recommendfragmentnew.mFocusIndex = i + 1;
        return i;
    }

*/










    private class _cls9
        implements android.support.v4.app.LoaderManager.LoaderCallbacks
    {

        final RecommendFragmentNew this$0;

        public Loader onCreateLoader(int i, Bundle bundle)
        {
            switch (i)
            {
            default:
                return null;

            case 2131361834: 
                return new RecommendListLoader(getActivity(), 1, 1, 3);

            case 2131361835: 
                return new RecommendListLoader(getActivity(), 3, 1, 3);
            }
        }

        public void onLoadFinished(Loader loader, RecmdItemGroup recmditemgroup)
        {
            if (loader != null)
            {
                if (recmditemgroup != null)
                {
                    mGuessLikeData = recmditemgroup;
                    loader = UserInfoMannage.getInstance().getUser();
                    if (loader != null)
                    {
                        mGuessLikeData.setUid(loader.getUid().longValue());
                    }
                }
                parseGuessLike(mGuessLikeData);
            }
        }

        public volatile void onLoadFinished(Loader loader, Object obj)
        {
            onLoadFinished(loader, (RecmdItemGroup)obj);
        }

        public void onLoaderReset(Loader loader)
        {
        }

        _cls9()
        {
            this$0 = RecommendFragmentNew.this;
            super();
        }
    }


    private class _cls10
        implements android.view.View.OnClickListener
    {

        final RecommendFragmentNew this$0;
        final int val$from;
        final boolean val$isGuessLike;
        final boolean val$isTrack;
        final String val$xdcsPosition;
        final String val$xdcsTitle;

        public void onClick(View view)
        {
            RecmdItemModel recmditemmodel;
            if (OneClickHelper.getInstance().onClick(view))
            {
                if ((recmditemmodel = (RecmdItemModel)view.getTag()) != null)
                {
                    if (isTrack)
                    {
                        PlayTools.gotoPlay(0, ModelHelper.toSoundInfo(recmditemmodel), getActivity(), DataCollectUtil.getDataFromView(view, xdcsTitle, xdcsPosition));
                        return;
                    } else
                    {
                        DataCollectUtil.bindDataToView(DataCollectUtil.getDataFromView(view, xdcsTitle, xdcsPosition), view);
                        toAlbumFragment(recmditemmodel, isGuessLike, from, view);
                        return;
                    }
                }
            }
        }

        _cls10()
        {
            this$0 = RecommendFragmentNew.this;
            isTrack = flag;
            xdcsTitle = s;
            xdcsPosition = s1;
            isGuessLike = flag1;
            from = i;
            super();
        }
    }


    private class _cls2
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final RecommendFragmentNew this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f, int j)
        {
        }

        public void onPageSelected(int i)
        {
            mFocusIndex = i;
            if (mFocusImages != null)
            {
                if (mFocusImages.size() > i);
            }
        }

        _cls2()
        {
            this$0 = RecommendFragmentNew.this;
            super();
        }
    }



    private class _cls4
        implements android.view.View.OnClickListener
    {

        final RecommendFragmentNew this$0;
        final int val$pos;

        public void onClick(View view)
        {
            RecmdSpecialModel recmdspecialmodel;
            if (OneClickHelper.getInstance().onClick(view))
            {
                if ((recmdspecialmodel = (RecmdSpecialModel)view.getTag()) != null)
                {
                    if (recmdspecialmodel.getColumnType() == 1)
                    {
                        toSubjectDetail(recmdspecialmodel, view, pos);
                        return;
                    }
                    if (recmdspecialmodel.getColumnType() == 2)
                    {
                        toRankDetail(recmdspecialmodel, view, pos);
                        return;
                    }
                }
            }
        }

        _cls4()
        {
            this$0 = RecommendFragmentNew.this;
            pos = i;
            super();
        }
    }


    private class _cls8
        implements Runnable
    {

        final RecommendFragmentNew this$0;

        public void run()
        {
            if (MainTabActivity2.isMainTabActivityAvaliable())
            {
                MainTabActivity2.mainTabActivity.switchToCategoryFragment();
            }
        }

        _cls8()
        {
            this$0 = RecommendFragmentNew.this;
            super();
        }
    }


    private class _cls6
        implements Runnable
    {

        final RecommendFragmentNew this$0;

        public void run()
        {
            if (MainTabActivity2.isMainTabActivityAvaliable())
            {
                MainTabActivity2.mainTabActivity.switchToLiveFragment(mLiveEntryTxt.getText().toString());
            }
        }

        _cls6()
        {
            this$0 = RecommendFragmentNew.this;
            super();
        }
    }


    private class _cls7
        implements Runnable
    {

        final RecommendFragmentNew this$0;

        public void run()
        {
            if (canGoon())
            {
                mNoticeLayout.setVisibility(8);
            }
        }

        _cls7()
        {
            this$0 = RecommendFragmentNew.this;
            super();
        }
    }


    private class _cls1
        implements ILoginStatusChangeListener
    {

        final RecommendFragmentNew this$0;

        public void onLogin(LoginInfoModel logininfomodel)
        {
            loadGuessLike();
        }

        public void onLogout()
        {
            loadGuessLike();
        }

        public void onUserChange(LoginInfoModel logininfomodel, LoginInfoModel logininfomodel1)
        {
            loadGuessLike();
        }

        _cls1()
        {
            this$0 = RecommendFragmentNew.this;
            super();
        }
    }

}
