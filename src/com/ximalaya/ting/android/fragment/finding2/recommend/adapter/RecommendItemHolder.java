// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.recommend.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class RecommendItemHolder
{

    public ImageView album_edge;
    public ImageView complete;
    public View content;
    public ImageView cover;
    public TextView desc;
    public ImageView ic_play;
    public TextView name;

    public RecommendItemHolder()
    {
    }

    public static RecommendItemHolder findView(View view)
    {
        RecommendItemHolder recommenditemholder = new RecommendItemHolder();
        recommenditemholder.complete = (ImageView)view.findViewById(0x7f0a014d);
        recommenditemholder.cover = (ImageView)view.findViewById(0x7f0a0177);
        recommenditemholder.name = (TextView)view.findViewById(0x7f0a0449);
        recommenditemholder.desc = (TextView)view.findViewById(0x7f0a044a);
        recommenditemholder.ic_play = (ImageView)view.findViewById(0x7f0a069b);
        recommenditemholder.album_edge = (ImageView)view.findViewById(0x7f0a069a);
        recommenditemholder.content = view;
        return recommenditemholder;
    }

    public static View getView(Context context)
    {
        context = View.inflate(context, 0x7f0301b3, null);
        context.setTag(findView(context));
        return context;
    }
}
