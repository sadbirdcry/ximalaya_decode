// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.recommend;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.fragment.finding2.recommend.adapter.RecommendItemListAdapter;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.recommend:
//            RecommendListFragment

class this._cls0
    implements android.widget.istener
{

    final RecommendListFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        if (OneClickHelper.getInstance().onClick(view))
        {
            i -= mListView.getHeaderViewsCount();
            if (i >= 0 && i < RecommendListFragment.access$000(RecommendListFragment.this).getCount())
            {
                adapterview = RecommendListFragment.access$000(RecommendListFragment.this).getItem(i);
                AlbumModel albummodel = new AlbumModel();
                albummodel.albumId = adapterview.getAlbumId();
                Bundle bundle = new Bundle();
                bundle.putString("album", JSON.toJSONString(albummodel));
                if (RecommendListFragment.access$100(RecommendListFragment.this))
                {
                    bundle.putInt("from", 12);
                    bundle.putString("rec_src", adapterview.getRecSrc());
                    bundle.putString("rec_track", adapterview.getRecTrack());
                } else
                {
                    bundle.putInt("from", 6);
                }
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, RecommendListFragment.access$200(RecommendListFragment.this), (new StringBuilder()).append(i + 1).append("").toString()));
                startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
            }
        }
    }

    tAdapter()
    {
        this$0 = RecommendListFragment.this;
        super();
    }
}
