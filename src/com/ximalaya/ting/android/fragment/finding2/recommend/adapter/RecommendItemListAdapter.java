// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.recommend.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemModel;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.ArrayList;
import java.util.List;

public class RecommendItemListAdapter extends BaseAdapter
{
    protected static class ViewHolder
    {

        View arrow;
        View collectBtn;
        TextView collectTxt;
        ImageView complete;
        ImageView cover;
        TextView desc;
        TextView name;
        TextView playCount;
        TextView soundCount;

        protected ViewHolder()
        {
        }
    }


    private Context mContext;
    private List mData;
    private BaseFragment mFragment;
    private LayoutInflater mInflater;
    private boolean mIsGuessLike;

    public RecommendItemListAdapter(Context context, BaseFragment basefragment, List list)
    {
        this(context, basefragment, list, false);
    }

    public RecommendItemListAdapter(Context context, BaseFragment basefragment, List list, boolean flag)
    {
        mData = new ArrayList();
        mIsGuessLike = false;
        mFragment = basefragment;
        mInflater = LayoutInflater.from(context);
        mData = list;
        mContext = context;
        mIsGuessLike = flag;
    }

    public static void doCollect(final Context context, final RecmdItemModel model, final ViewHolder holder)
    {
        if (UserInfoMannage.hasLogined())
        {
            String s;
            RequestParams requestparams;
            if (model.isCCollected())
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.getCId()).toString());
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(holder.collectBtn), new _cls4());
        } else
        {
            final AlbumModel m = new AlbumModel();
            m.albumId = model.getCId();
            m.isFavorite = model.isCCollected();
            if (AlbumModelManage.getInstance().ensureLocalCollectAllow(context, m, holder.collectBtn))
            {
                (new _cls5()).myexec(new Void[0]);
                return;
            }
        }
    }

    public static void doCollect(final Context context, final RecmdItemModel model, final ViewHolder holder, int i, boolean flag)
    {
        if (UserInfoMannage.hasLogined())
        {
            String s;
            String s1;
            RequestParams requestparams;
            if (model.isCCollected())
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.getCId()).toString());
            requestparams.add("position", (new StringBuilder()).append(i).append("").toString());
            if (flag)
            {
                s1 = "\u731C\u4F60\u559C\u6B22";
            } else
            {
                s1 = "\u5C0F\u7F16\u63A8\u8350";
            }
            requestparams.add("title", s1);
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(holder.collectBtn), new _cls2());
        } else
        {
            final AlbumModel m = new AlbumModel();
            m.albumId = model.getCId();
            m.isFavorite = model.isCCollected();
            if (AlbumModelManage.getInstance().ensureLocalCollectAllow(context, m, holder.collectBtn))
            {
                (new _cls3()).myexec(new Void[0]);
                return;
            }
        }
    }

    public static void setCollectStatus(ViewHolder viewholder, boolean flag)
    {
        if (flag)
        {
            viewholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ee, 0, 0);
            viewholder.collectTxt.setText("\u5DF2\u6536\u85CF");
            viewholder.collectTxt.setTextColor(Color.parseColor("#999999"));
            return;
        } else
        {
            viewholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ed, 0, 0);
            viewholder.collectTxt.setText("\u6536\u85CF");
            viewholder.collectTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    public void addData(List list)
    {
        if (list == null)
        {
            return;
        }
        if (mData == null)
        {
            mData = list;
        } else
        {
            mData.addAll(list);
        }
        notifyDataSetChanged();
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public List getData()
    {
        return mData;
    }

    public RecmdItemModel getItem(int i)
    {
        return (RecmdItemModel)mData.get(i);
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(final int position, View view, final ViewGroup h)
    {
        boolean flag1 = true;
        Object obj;
        final RecmdItemModel model;
        TextView textview;
        int i;
        boolean flag;
        if (view == null)
        {
            view = mInflater.inflate(0x7f0301b0, h, false);
            h = new ViewHolder();
            h.cover = (ImageView)view.findViewById(0x7f0a0177);
            mFragment.markImageView(((ViewHolder) (h)).cover);
            h.name = (TextView)view.findViewById(0x7f0a0449);
            h.desc = (TextView)view.findViewById(0x7f0a044a);
            h.playCount = (TextView)view.findViewById(0x7f0a021a);
            h.soundCount = (TextView)view.findViewById(0x7f0a026b);
            h.complete = (ImageView)view.findViewById(0x7f0a014d);
            h.collectBtn = view.findViewById(0x7f0a0154);
            h.collectTxt = (TextView)view.findViewById(0x7f0a0155);
            h.arrow = view.findViewById(0x7f0a0153);
            view.setTag(h);
        } else
        {
            h = (ViewHolder)view.getTag();
        }
        model = (RecmdItemModel)mData.get(position);
        ImageManager2.from(mContext).displayImage(((ViewHolder) (h)).cover, model.getCoverMiddle(), 0x7f0202e0);
        textview = ((ViewHolder) (h)).name;
        if (model.getTitle() == null)
        {
            obj = "";
        } else
        {
            obj = model.getTitle();
        }
        textview.setText(((CharSequence) (obj)));
        obj = ((ViewHolder) (h)).complete;
        if (model.getIsFinished() == 2)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        ((ImageView) (obj)).setVisibility(i);
        if (model.getPlaysCounts() > 0L)
        {
            ((ViewHolder) (h)).playCount.setVisibility(0);
            ((ViewHolder) (h)).playCount.setText(StringUtil.getFriendlyNumStr(model.getPlaysCounts()));
        } else
        {
            ((ViewHolder) (h)).playCount.setVisibility(8);
        }
        if (model.getTracks() > 0)
        {
            ((ViewHolder) (h)).soundCount.setVisibility(0);
            ((ViewHolder) (h)).soundCount.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(model.getTracks())).append("\u96C6").toString());
        } else
        {
            ((ViewHolder) (h)).soundCount.setVisibility(8);
        }
        if (mIsGuessLike)
        {
            TextView textview1 = ((ViewHolder) (h)).desc;
            if (model.getRecReason() == null)
            {
                obj = "";
            } else
            {
                obj = model.getRecReason();
            }
            textview1.setText(((CharSequence) (obj)));
        } else
        {
            TextView textview2 = ((ViewHolder) (h)).desc;
            String s;
            if (model.getIntro() == null)
            {
                s = "";
            } else
            {
                s = model.getIntro();
            }
            textview2.setText(s);
        }
        setCollectStatus(h, model.isCCollected());
        ((ViewHolder) (h)).collectBtn.setTag(0x7f090000, model);
        ((ViewHolder) (h)).arrow.setVisibility(8);
        ((ViewHolder) (h)).collectBtn.setOnClickListener(new _cls1());
        h = mContext;
        if (position == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (position + 1 != mData.size())
        {
            flag1 = false;
        }
        ViewUtil.buildAlbumItemSpace(h, view, flag, flag1, 81);
        return view;
    }

    public void setData(List list)
    {
        mData = list;
        notifyDataSetChanged();
    }

    public void setList(List list)
    {
        if (list == null)
        {
            return;
        }
        if (mData == null)
        {
            mData = new ArrayList();
        }
        mData.clear();
        mData.addAll(list);
        notifyDataSetChanged();
    }



    private class _cls4 extends a
    {

        final Context val$context;
        final ViewHolder val$holder;
        final RecmdItemModel val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, holder.collectBtn);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            if (((RecmdItemModel)holder.collectBtn.getTag(0x7f090000)).getCId() == model.getCId())
            {
                RecommendItemListAdapter.setCollectStatus(holder, model.isCCollected());
            }
        }

        public void onSuccess(String s)
        {
            boolean flag = true;
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
_L4:
            return;
_L2:
            s = JSON.parseObject(s);
            if (s == null) goto _L4; else goto _L3
_L3:
            int i = s.getIntValue("ret");
            if (i != 0)
            {
                break MISSING_BLOCK_LABEL_149;
            }
            s = model;
            if (model.isCCollected())
            {
                flag = false;
            }
            s.setCCollected(flag);
            if (((RecmdItemModel)holder.collectBtn.getTag(0x7f090000)).getCId() == model.getCId())
            {
                RecommendItemListAdapter.setCollectStatus(holder, model.isCCollected());
            }
            if (model.isCCollected())
            {
                s = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            try
            {
                Toast.makeText(context, s, 0).show();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
            return;
            if (i != 791)
            {
                break MISSING_BLOCK_LABEL_164;
            }
            model.setCCollected(true);
            if (((RecmdItemModel)holder.collectBtn.getTag(0x7f090000)).getCId() == model.getCId())
            {
                RecommendItemListAdapter.setCollectStatus(holder, model.isCCollected());
            }
            if (s.getString("msg") != null)
            {
                break MISSING_BLOCK_LABEL_232;
            }
            s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
_L6:
            Toast.makeText(context, s, 0).show();
            return;
            s = s.getString("msg");
            if (true) goto _L6; else goto _L5
_L5:
        }

        _cls4()
        {
            context = context1;
            holder = viewholder;
            model = recmditemmodel;
            super();
        }
    }


    private class _cls5 extends MyAsyncTask
    {

        final Context val$context;
        final ViewHolder val$holder;
        final AlbumModel val$m;
        final RecmdItemModel val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            RecmdItemModel recmditemmodel = model;
            boolean flag;
            if (!model.isCCollected())
            {
                flag = true;
            } else
            {
                flag = false;
            }
            recmditemmodel.setCCollected(flag);
            m.coverSmall = model.getCoverMiddle();
            m.playTimes = (int)model.getPlaysCounts();
            m.tracks = model.getTracks();
            m.lastUptrackAt = model.getLastUptrackAt();
            m.lastUptrackTitle = model.getLastUptrackTitle();
            m.title = model.getTitle();
            if (!model.isCCollected())
            {
                avoid.deleteAlbumInLocalAlbumList(m);
            } else
            {
                avoid.saveAlbumModel(m);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            if (((RecmdItemModel)holder.collectBtn.getTag(0x7f090000)).getCId() == model.getCId())
            {
                RecommendItemListAdapter.setCollectStatus(holder, model.isCCollected());
            }
            if (model.isCCollected())
            {
                void1 = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                void1 = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            Toast.makeText(context, void1, 0).show();
        }

        _cls5()
        {
            model = recmditemmodel;
            m = albummodel;
            holder = viewholder;
            context = context1;
            super();
        }
    }


    private class _cls2 extends a
    {

        final Context val$context;
        final ViewHolder val$holder;
        final RecmdItemModel val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, holder.collectBtn);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            if (((RecmdItemModel)holder.collectBtn.getTag(0x7f090000)).getCId() == model.getCId())
            {
                RecommendItemListAdapter.setCollectStatus(holder, model.isCCollected());
            }
        }

        public void onSuccess(String s)
        {
            boolean flag = true;
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
_L4:
            return;
_L2:
            s = JSON.parseObject(s);
            if (s == null) goto _L4; else goto _L3
_L3:
            int i = s.getIntValue("ret");
            if (i != 0)
            {
                break MISSING_BLOCK_LABEL_149;
            }
            s = model;
            if (model.isCCollected())
            {
                flag = false;
            }
            s.setCCollected(flag);
            if (((RecmdItemModel)holder.collectBtn.getTag(0x7f090000)).getCId() == model.getCId())
            {
                RecommendItemListAdapter.setCollectStatus(holder, model.isCCollected());
            }
            if (model.isCCollected())
            {
                s = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            try
            {
                Toast.makeText(context, s, 0).show();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
            return;
            if (i != 791)
            {
                break MISSING_BLOCK_LABEL_164;
            }
            model.setCCollected(true);
            if (((RecmdItemModel)holder.collectBtn.getTag(0x7f090000)).getCId() == model.getCId())
            {
                RecommendItemListAdapter.setCollectStatus(holder, model.isCCollected());
            }
            if (s.getString("msg") != null)
            {
                break MISSING_BLOCK_LABEL_232;
            }
            s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
_L6:
            Toast.makeText(context, s, 0).show();
            return;
            s = s.getString("msg");
            if (true) goto _L6; else goto _L5
_L5:
        }

        _cls2()
        {
            context = context1;
            holder = viewholder;
            model = recmditemmodel;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final Context val$context;
        final ViewHolder val$holder;
        final AlbumModel val$m;
        final RecmdItemModel val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            RecmdItemModel recmditemmodel = model;
            boolean flag;
            if (!model.isCCollected())
            {
                flag = true;
            } else
            {
                flag = false;
            }
            recmditemmodel.setCCollected(flag);
            m.coverSmall = model.getCoverMiddle();
            m.playTimes = (int)model.getPlaysCounts();
            m.tracks = model.getTracks();
            m.lastUptrackAt = model.getLastUptrackAt();
            m.lastUptrackTitle = model.getLastUptrackTitle();
            m.title = model.getTitle();
            if (!model.isCCollected())
            {
                avoid.deleteAlbumInLocalAlbumList(m);
            } else
            {
                avoid.saveAlbumModel(m);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            if (((RecmdItemModel)holder.collectBtn.getTag(0x7f090000)).getCId() == model.getCId())
            {
                RecommendItemListAdapter.setCollectStatus(holder, model.isCCollected());
            }
            if (model.isCCollected())
            {
                void1 = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                void1 = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            Toast.makeText(context, void1, 0).show();
        }

        _cls3()
        {
            model = recmditemmodel;
            m = albummodel;
            holder = viewholder;
            context = context1;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final RecommendItemListAdapter this$0;
        final ViewHolder val$h;
        final RecmdItemModel val$model;
        final int val$position;

        public void onClick(View view)
        {
            RecommendItemListAdapter.doCollect(mContext, model, h, position + 1, mIsGuessLike);
        }

        _cls1()
        {
            this$0 = RecommendItemListAdapter.this;
            model = recmditemmodel;
            h = viewholder;
            position = i;
            super();
        }
    }

}
