// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.recommend.loader;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.util.ToolUtilLib;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdMainModel;
import com.ximalaya.ting.android.player.Logger;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.FileOption;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.MD5;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.File;

public class RecommendLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "mobile/discovery/v1/recommends";
    private View fromBindView;
    private RecmdMainModel mData;
    private boolean mLoadFromLocal;
    private View toBindView;

    public RecommendLoader(Context context, View view, View view1)
    {
        super(context);
        mLoadFromLocal = false;
        fromBindView = view;
        toBindView = view1;
    }

    public RecommendLoader(Context context, boolean flag)
    {
        super(context);
        mLoadFromLocal = flag;
    }

    private String readInfoFromAssets()
    {
        return FileOption.readAssetFileData(getContext(), "data/header_data.txt");
    }

    private String readInfoFromCache()
    {
        Object obj = SharedPreferencesUtil.getInstance(getContext()).getString("finding_header_info");
        if (TextUtils.isEmpty(((CharSequence) (obj)))) goto _L2; else goto _L1
_L1:
        JSONObject jsonobject = JSON.parseObject(((String) (obj)));
        if (jsonobject == null) goto _L2; else goto _L3
_L3:
        int i = jsonobject.getIntValue("ret");
        if (i != 0) goto _L2; else goto _L4
_L4:
        Object obj1 = obj;
        if (obj == null)
        {
            obj1 = readInfoFromAssets();
        }
        return ((String) (obj1));
        obj;
        ((Exception) (obj)).printStackTrace();
_L2:
        obj = null;
        if (true) goto _L4; else goto _L5
_L5:
    }

    private void saveToTemp(String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            FileUtils.writeStr2File(s, (new File(getContext().getCacheDir(), MD5.md5((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/discovery/v1/recommends").toString()))).getAbsolutePath());
        }
    }

    public void deliverResult(RecmdMainModel recmdmainmodel)
    {
        super.deliverResult(recmdmainmodel);
        mData = recmdmainmodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((RecmdMainModel)obj);
    }

    public RecmdMainModel loadInBackground()
    {
        if (!mLoadFromLocal) goto _L2; else goto _L1
_L1:
        String s = FileUtils.readStrFromFile((new File(getContext().getCacheDir(), MD5.md5((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/discovery/v1/recommends").toString()))).getAbsolutePath());
        Object obj = s;
        if (TextUtils.isEmpty(s))
        {
            obj = FileUtils.readAssetFileData(getContext(), "finding/recommend.json");
        }
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            try
            {
                obj = (RecmdMainModel)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/finding2/recommend/RecmdMainModel);
            }
            catch (Exception exception)
            {
                Logger.e(exception);
                exception = null;
            }
            if (obj != null && ((RecmdMainModel) (obj)).ret == 0)
            {
                mData = ((RecmdMainModel) (obj));
            }
        }
_L4:
        return mData;
_L2:
        Object obj1;
        obj1 = new RequestParams();
        ((RequestParams) (obj1)).put("scale", "2");
        ((RequestParams) (obj1)).put("includeActivity", "true");
        ((RequestParams) (obj1)).put("includeSpecial", "true");
        ((RequestParams) (obj1)).put("channel", ToolUtilLib.getChannel(getContext().getApplicationContext()));
        ((RequestParams) (obj1)).put("version", ToolUtil.getAppVersion(getContext()));
        obj1 = f.a().a("mobile/discovery/v1/recommends", ((RequestParams) (obj1)), fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj1)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj1)).a))
        {
            continue; /* Loop/switch isn't completed */
        }
        RecmdMainModel recmdmainmodel = (RecmdMainModel)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj1)).a, com/ximalaya/ting/android/model/finding2/recommend/RecmdMainModel);
        if (recmdmainmodel != null)
        {
            try
            {
                if (recmdmainmodel.ret == 0)
                {
                    mData = recmdmainmodel;
                    saveToTemp(((com.ximalaya.ting.android.b.n.a) (obj1)).a);
                }
            }
            catch (Exception exception1)
            {
                exception1.printStackTrace();
            }
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
