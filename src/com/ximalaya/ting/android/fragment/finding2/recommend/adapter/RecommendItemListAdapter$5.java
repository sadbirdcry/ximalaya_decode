// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.recommend.adapter;

import android.content.Context;
import android.view.View;
import android.widget.Toast;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemModel;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.recommend.adapter:
//            RecommendItemListAdapter

final class val.context extends MyAsyncTask
{

    final Context val$context;
    final ewHolder val$holder;
    final AlbumModel val$m;
    final RecmdItemModel val$model;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        avoid = AlbumModelManage.getInstance();
        RecmdItemModel recmditemmodel = val$model;
        boolean flag;
        if (!val$model.isCCollected())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        recmditemmodel.setCCollected(flag);
        val$m.coverSmall = val$model.getCoverMiddle();
        val$m.playTimes = (int)val$model.getPlaysCounts();
        val$m.tracks = val$model.getTracks();
        val$m.lastUptrackAt = val$model.getLastUptrackAt();
        val$m.lastUptrackTitle = val$model.getLastUptrackTitle();
        val$m.title = val$model.getTitle();
        if (!val$model.isCCollected())
        {
            avoid.deleteAlbumInLocalAlbumList(val$m);
        } else
        {
            avoid.saveAlbumModel(val$m);
        }
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        if (((RecmdItemModel)val$holder.collectBtn.getTag(0x7f090000)).getCId() == val$model.getCId())
        {
            RecommendItemListAdapter.setCollectStatus(val$holder, val$model.isCCollected());
        }
        if (val$model.isCCollected())
        {
            void1 = "\u6536\u85CF\u6210\u529F\uFF01";
        } else
        {
            void1 = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
        }
        Toast.makeText(val$context, void1, 0).show();
    }

    ewHolder()
    {
        val$model = recmditemmodel;
        val$m = albummodel;
        val$holder = ewholder;
        val$context = context1;
        super();
    }
}
