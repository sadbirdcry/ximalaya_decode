// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.rank.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.finding2.rank.RankAlbumModel;
import com.ximalaya.ting.android.model.finding2.rank.RankAnchorModel;
import com.ximalaya.ting.android.model.finding2.rank.RankTrackModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class RankDetailAdapter extends BaseAdapter
{
    private static class TrackViewHolder
    {

        public ImageButton btn;
        public ImageView cover;
        public TextView createTime;
        public TextView owner;
        public ImageView playFlag;
        public TextView rankNum;
        public TextView title;

        private TrackViewHolder()
        {
        }

        TrackViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }

    protected static class ViewHolder
    {

        ImageView complete;
        ImageView cover;
        TextView desc;
        TextView name;
        TextView rankNum;
        TextView soundCounts;

        protected ViewHolder()
        {
        }
    }


    private Context mContext;
    private List mData;
    private String mDataSourceUrl;
    private BaseFragment mFragment;
    private LayoutInflater mInflater;
    protected int mPageId;
    protected HashMap mParams;

    public RankDetailAdapter(Activity activity, BaseFragment basefragment, List list)
    {
        mContext = activity;
        mInflater = LayoutInflater.from(mContext);
        mFragment = basefragment;
        mData = list;
    }

    private View getOtherConvertView(Object obj, View view, int i)
    {
        boolean flag1 = true;
        ViewHolder viewholder;
        boolean flag;
        if (view == null)
        {
            view = mInflater.inflate(0x7f0301a0, null, false);
            viewholder = new ViewHolder();
            viewholder.rankNum = (TextView)view.findViewById(0x7f0a0658);
            viewholder.cover = (ImageView)view.findViewById(0x7f0a0177);
            mFragment.markImageView(viewholder.cover);
            viewholder.name = (TextView)view.findViewById(0x7f0a0449);
            viewholder.desc = (TextView)view.findViewById(0x7f0a044a);
            viewholder.soundCounts = (TextView)view.findViewById(0x7f0a026b);
            viewholder.complete = (ImageView)view.findViewById(0x7f0a014d);
            view.setTag(viewholder);
        } else
        {
            viewholder = (ViewHolder)view.getTag();
        }
        refreshRankPosition(viewholder.rankNum, i + 1);
        if (obj instanceof RankAlbumModel)
        {
            refreshAlbum(viewholder, (RankAlbumModel)obj);
        } else
        if (obj instanceof RankAnchorModel)
        {
            refreshAnchor(viewholder, (RankAnchorModel)obj);
        }
        obj = mContext;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (i + 1 != mData.size())
        {
            flag1 = false;
        }
        ViewUtil.buildAlbumItemSpace(((Context) (obj)), view, flag, flag1, 81);
        return view;
    }

    private View getTrackConvertView(RankTrackModel ranktrackmodel, View view, int i)
    {
        Object obj;
        View view1;
        if (view == null)
        {
            view1 = mInflater.inflate(0x7f0301a1, null, false);
            view = new TrackViewHolder(null);
            view.cover = (ImageView)view1.findViewById(0x7f0a0177);
            view.title = (TextView)view1.findViewById(0x7f0a0218);
            view.owner = (TextView)view1.findViewById(0x7f0a0219);
            view.btn = (ImageButton)view1.findViewById(0x7f0a04b0);
            view.createTime = (TextView)view1.findViewById(0x7f0a0216);
            view.playFlag = (ImageView)view1.findViewById(0x7f0a0022);
            view.rankNum = (TextView)view1.findViewById(0x7f0a0658);
            view1.setTag(view);
            obj = view;
        } else
        {
            obj = (TrackViewHolder)view.getTag();
            view1 = view;
        }
        refreshRankPosition(((TrackViewHolder) (obj)).rankNum, i + 1);
        refreshTrack(((TrackViewHolder) (obj)), ranktrackmodel);
        return view1;
    }

    private void refreshAlbum(ViewHolder viewholder, RankAlbumModel rankalbummodel)
    {
        ImageManager2.from(mContext).displayImage(viewholder.cover, rankalbummodel.getCoverMiddle(), 0x7f0202e0);
        TextView textview = viewholder.name;
        Object obj;
        int i;
        if (rankalbummodel.getTitle() == null)
        {
            obj = "";
        } else
        {
            obj = rankalbummodel.getTitle();
        }
        textview.setText(((CharSequence) (obj)));
        viewholder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        if (rankalbummodel.getTracks() > 0L)
        {
            viewholder.soundCounts.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(rankalbummodel.getTracks())).append("\u96C6").toString());
            viewholder.soundCounts.setVisibility(0);
        } else
        {
            viewholder.soundCounts.setVisibility(8);
        }
        obj = viewholder.complete;
        if (rankalbummodel.getIsFinished() == 2)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        ((ImageView) (obj)).setVisibility(i);
        if (viewholder.desc.getVisibility() != 0)
        {
            viewholder.desc.setVisibility(0);
        }
        if (rankalbummodel.getIsFinished() == 2)
        {
            viewholder.complete.setVisibility(0);
        } else
        {
            viewholder.complete.setVisibility(8);
        }
        viewholder.desc.setText(StringUtil.getFriendlyTag(rankalbummodel.getTags()));
    }

    private void refreshAnchor(ViewHolder viewholder, RankAnchorModel rankanchormodel)
    {
        ImageManager2.from(mContext).displayImage(viewholder.cover, rankanchormodel.getMiddleLogo(), 0x7f0202e0);
        TextView textview = viewholder.name;
        Object obj;
        if (rankanchormodel.getNickname() == null)
        {
            obj = "";
        } else
        {
            obj = rankanchormodel.getNickname();
        }
        textview.setText(((CharSequence) (obj)));
        if (rankanchormodel.isVerified())
        {
            viewholder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f02003a, 0);
        } else
        {
            viewholder.name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
        if (rankanchormodel.getFollowersCounts() > 0L)
        {
            viewholder.soundCounts.setCompoundDrawablesWithIntrinsicBounds(0x7f020313, 0, 0, 0);
            viewholder.soundCounts.setVisibility(0);
            viewholder.soundCounts.setText(StringUtil.getFriendlyNumStr(rankanchormodel.getFollowersCounts()));
        } else
        {
            viewholder.soundCounts.setVisibility(8);
        }
        obj = viewholder.desc;
        if (rankanchormodel.getPersonDescribe() == null)
        {
            viewholder = "";
        } else
        {
            viewholder = rankanchormodel.getPersonDescribe();
        }
        ((TextView) (obj)).setText(viewholder);
    }

    private void refreshRankPosition(TextView textview, int i)
    {
        textview.setText((new StringBuilder()).append(i).append("").toString());
        if (i == 1)
        {
            textview.setTextColor(Color.parseColor("#F86442"));
        } else
        {
            if (i == 2)
            {
                textview.setTextColor(Color.parseColor("#f79100"));
                return;
            }
            if (i == 3)
            {
                textview.setTextColor(Color.parseColor("#9ebc0c"));
                return;
            }
            if (i > 3)
            {
                textview.setTextColor(Color.parseColor("#999999"));
                return;
            }
        }
    }

    private void refreshTrack(final TrackViewHolder holder, final RankTrackModel info)
    {
        final SoundInfo sInfo = ModelHelper.toSoundInfo(info);
        b.a().a(sInfo);
        Object obj = holder.title;
        final Object soundList;
        if (info.getTitle() == null)
        {
            soundList = "";
        } else
        {
            soundList = info.getTitle();
        }
        ((TextView) (obj)).setText(((CharSequence) (soundList)));
        if (isPlaying(info.getTrackId()))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        if (info.getUpdatedAt() > 0L)
        {
            holder.createTime.setVisibility(0);
            holder.createTime.setText(ToolUtil.convertTime(info.getUpdatedAt()));
        } else
        {
            holder.createTime.setVisibility(4);
        }
        if (TextUtils.isEmpty(info.getNickname()))
        {
            holder.owner.setText("");
        } else
        {
            holder.owner.setText((new StringBuilder()).append("by ").append(info.getNickname()).toString());
        }
        ImageManager2.from(mContext).displayImage(holder.cover, info.getCoverSmall(), 0x7f0202de);
        if (isDownload(info.getTrackId()))
        {
            holder.btn.setImageResource(0x7f0200f1);
            holder.btn.setEnabled(false);
        } else
        {
            holder.btn.setImageResource(0x7f0201fe);
            holder.btn.setEnabled(true);
        }
        holder.btn.setOnClickListener(new _cls1());
        soundList = new ArrayList();
        obj = mData.iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break;
            }
            Object obj1 = ((Iterator) (obj)).next();
            if (obj1 instanceof RankTrackModel)
            {
                ((List) (soundList)).add(ModelHelper.toSoundInfo((RankTrackModel)obj1));
            }
        } while (true);
        holder.cover.setOnClickListener(new _cls2());
    }

    public void addData(List list)
    {
        mData.addAll(list);
        notifyDataSetChanged();
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public List getData()
    {
        return mData;
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        Object obj = mData.get(i);
        if (!(obj instanceof RankTrackModel)) goto _L2; else goto _L1
_L1:
        viewgroup = getTrackConvertView((RankTrackModel)obj, view, i);
_L4:
        return viewgroup;
_L2:
        if (obj instanceof RankAlbumModel)
        {
            break; /* Loop/switch isn't completed */
        }
        viewgroup = view;
        if (!(obj instanceof RankAnchorModel)) goto _L4; else goto _L3
_L3:
        return getOtherConvertView(obj, view, i);
    }

    public com.ximalaya.ting.android.a.a.a goDownLoad(SoundInfo soundinfo, View view)
    {
        Object obj = null;
        DownloadTask downloadtask = new DownloadTask(soundinfo);
        soundinfo = obj;
        if (downloadtask != null)
        {
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            soundinfo = downloadtools.goDownload(downloadtask, (MyApplication)mContext.getApplicationContext(), view);
            downloadtools.release();
        }
        return soundinfo;
    }

    public boolean isDownload(long l)
    {
        if (mContext != null) goto _L2; else goto _L1
_L1:
        Object obj;
        return false;
_L2:
        if ((obj = DownloadHandler.getInstance(mContext.getApplicationContext()).downloadList) == null || ((List) (obj)).size() <= 0) goto _L1; else goto _L3
_L3:
        obj = ((List) (obj)).iterator();
_L7:
        if (!((Iterator) (obj)).hasNext()) goto _L5; else goto _L4
_L4:
        long l1 = ((DownloadTask)((Iterator) (obj)).next()).trackId;
        if (l1 != l) goto _L7; else goto _L6
_L6:
        boolean flag = true;
_L9:
        return flag;
        Exception exception;
        exception;
        flag = false;
        continue; /* Loop/switch isn't completed */
_L5:
        flag = false;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public boolean isPlaying(long l)
    {
        return PlayListControl.getPlayListManager().getCurSound() != null && l == PlayListControl.getPlayListManager().getCurSound().trackId;
    }

    public void playSound(View view, int i, SoundInfo soundinfo, List list)
    {
        ImageView imageview = (ImageView)view;
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        SoundInfo soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
        if (localmediaservice == null || soundinfo == null)
        {
            return;
        }
        if (soundinfo1 == null)
        {
            PlayTools.gotoPlay(13, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
            imageview.setImageResource(0x7f02024f);
            return;
        }
        switch (localmediaservice.getPlayServiceState())
        {
        default:
            return;

        case 0: // '\0'
            PlayTools.gotoPlay(13, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
            imageview.setImageResource(0x7f02024f);
            return;

        case 1: // '\001'
        case 3: // '\003'
            if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.trackId == soundinfo1.trackId)
            {
                localmediaservice.pause();
                imageview.setImageResource(0x7f020250);
                return;
            } else
            {
                PlayTools.gotoPlay(13, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
                return;
            }

        case 2: // '\002'
            break;
        }
        if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.trackId == soundinfo1.trackId)
        {
            localmediaservice.start();
            imageview.setImageResource(0x7f02024f);
            return;
        } else
        {
            PlayTools.gotoPlay(13, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
            return;
        }
    }

    public void setData(List list)
    {
        mData = list;
        notifyDataSetChanged();
    }

    public void setDataSource(String s)
    {
        mDataSourceUrl = s;
    }

    public void setPageId(int i)
    {
        mPageId = i;
    }

    public void setParams(HashMap hashmap)
    {
        mParams = hashmap;
    }

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final RankDetailAdapter this$0;
        final TrackViewHolder val$holder;
        final SoundInfo val$sInfo;

        public void onClick(View view)
        {
            if (goDownLoad(sInfo, view) == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls1()
        {
            this$0 = RankDetailAdapter.this;
            sInfo = soundinfo;
            holder = trackviewholder;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final RankDetailAdapter this$0;
        final TrackViewHolder val$holder;
        final RankTrackModel val$info;
        final SoundInfo val$sInfo;
        final List val$soundList;

        public void onClick(View view)
        {
            playSound(holder.playFlag, getData().indexOf(info), sInfo, soundList);
        }

        _cls2()
        {
            this$0 = RankDetailAdapter.this;
            holder = trackviewholder;
            info = ranktrackmodel;
            sInfo = soundinfo;
            soundList = list;
            super();
        }
    }

}
