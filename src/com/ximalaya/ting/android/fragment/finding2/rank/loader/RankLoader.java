// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.rank.loader;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.ToolUtilLib;
import com.ximalaya.ting.android.model.finding2.rank.RankModel;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.ToolUtil;

public class RankLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "mobile/discovery/v2/rankingList/group";
    private RankModel mData;

    public RankLoader(Context context)
    {
        super(context);
    }

    public void deliverResult(RankModel rankmodel)
    {
        super.deliverResult(rankmodel);
        mData = rankmodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((RankModel)obj);
    }

    public RankModel loadInBackground()
    {
        Object obj;
        obj = new RequestParams();
        ((RequestParams) (obj)).put("scale", "2");
        ((RequestParams) (obj)).put("includeActivity", "true");
        ((RequestParams) (obj)).put("includeSpecial", "true");
        ((RequestParams) (obj)).put("channel", ToolUtilLib.getChannel(getContext().getApplicationContext()));
        ((RequestParams) (obj)).put("version", ToolUtil.getAppVersion(getContext()));
        obj = f.a().a("mobile/discovery/v2/rankingList/group", ((RequestParams) (obj)), fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_128;
        }
        if (JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a).getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_128;
        }
        obj = RankModel.getInstance(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        return ((RankModel) (obj));
        Exception exception;
        exception;
        Logger.e(exception);
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
