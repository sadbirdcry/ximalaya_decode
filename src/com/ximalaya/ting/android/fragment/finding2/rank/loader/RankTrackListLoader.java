// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.rank.loader;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.finding2.rank.RankTrackListModel;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;

public class RankTrackListLoader extends MyAsyncTaskLoader
{

    public static final String PATH = "mobile/discovery/v1/rankingList/track";
    private String key;
    private RankTrackListModel mData;
    private int pageId;
    private int pageSize;

    public RankTrackListLoader(Context context, String s, int i, int j)
    {
        super(context);
        key = s;
        pageId = i;
        pageSize = j;
    }

    public void deliverResult(RankTrackListModel ranktracklistmodel)
    {
        super.deliverResult(ranktracklistmodel);
        mData = ranktracklistmodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((RankTrackListModel)obj);
    }

    public RankTrackListModel loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("key", key);
        ((RequestParams) (obj)).put("pageId", pageId);
        ((RequestParams) (obj)).put("pageSize", pageSize);
        obj = f.a().a("mobile/discovery/v1/rankingList/track", ((RequestParams) (obj)), fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            try
            {
                obj = (RankTrackListModel)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a, com/ximalaya/ting/android/model/finding2/rank/RankTrackListModel);
                if (((RankTrackListModel) (obj)).ret == 0)
                {
                    mData = ((RankTrackListModel) (obj));
                }
            }
            catch (Exception exception)
            {
                Logger.e(exception);
            }
        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
