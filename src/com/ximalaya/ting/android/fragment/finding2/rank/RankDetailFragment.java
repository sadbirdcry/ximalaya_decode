// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.rank;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.fragment.finding2.rank.adapter.RankDetailAdapter;
import com.ximalaya.ting.android.fragment.finding2.rank.loader.RankAlbumListLoader;
import com.ximalaya.ting.android.fragment.finding2.rank.loader.RankAnchorListLoader;
import com.ximalaya.ting.android.fragment.finding2.rank.loader.RankTrackListLoader;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.finding2.rank.RankAlbumListModel;
import com.ximalaya.ting.android.model.finding2.rank.RankAlbumModel;
import com.ximalaya.ting.android.model.finding2.rank.RankAnchorListModel;
import com.ximalaya.ting.android.model.finding2.rank.RankAnchorModel;
import com.ximalaya.ting.android.model.finding2.rank.RankTrackListModel;
import com.ximalaya.ting.android.model.finding2.rank.RankTrackModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class RankDetailFragment extends BaseListFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, com.ximalaya.ting.android.fragment.ReloadFragment.Callback, com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{

    private static final int LOAD_ALBUM = 0;
    private static final int LOAD_ANCHOR = 1;
    private static final int LOAD_TRACK = 2;
    private static final int PAGE_SIZE = 40;
    private RankDetailAdapter mAdapter;
    private boolean mHasMore;
    private boolean mIsLoading;
    private String mKey;
    private Loader mLoader;
    private int mLoaderType;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private int mPageId;
    private String mTitle;

    public RankDetailFragment()
    {
        mPageId = 1;
        mHasMore = true;
    }

    private void gotoPlay(int i, List list, View view)
    {
        Object obj = new ArrayList();
        list = list.iterator();
        do
        {
            if (!list.hasNext())
            {
                break;
            }
            Object obj1 = list.next();
            if (obj1 instanceof RankTrackModel)
            {
                ((List) (obj)).add((RankTrackModel)obj1);
            }
        } while (true);
        list = ModelHelper.toSoundInfoListForRank(((List) (obj)));
        obj = new HashMap();
        ((HashMap) (obj)).put("key", mKey);
        PlayTools.gotoPlay(31, "mobile/discovery/v1/rankingList/track", mPageId, ((HashMap) (obj)), list, i, mActivity, true, DataCollectUtil.getDataFromView(view));
    }

    private void initListener()
    {
        registerListener();
        mListView.setOnItemClickListener(new _cls1());
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls2());
        ((PullToRefreshListView)mListView).setMyScrollListener2(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
    }

    private void loadData(View view)
    {
        if (mIsLoading)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            return;
        }
        if (!NetworkUtils.isNetworkAvaliable(getActivity()))
        {
            showReload();
            return;
        }
        mIsLoading = true;
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(mLoaderType, null, this);
        } else
        {
            mLoader = getLoaderManager().restartLoader(mLoaderType, null, this);
        }
        switch (mLoaderType)
        {
        default:
            return;

        case 0: // '\0'
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
            return;

        case 1: // '\001'
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
            return;

        case 2: // '\002'
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
            break;
        }
    }

    private void loadMoreData(View view)
    {
        if (mHasMore)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
            loadData(view);
        }
    }

    private void parseData(Object obj)
    {
        boolean flag;
        boolean flag3;
        boolean flag4;
        flag3 = true;
        flag4 = true;
        flag = true;
        if (mPageId == 1 && mAdapter != null && mAdapter.getData() != null)
        {
            mAdapter.getData().clear();
        }
        mLoaderType;
        JVM INSTR tableswitch 0 2: default 76
    //                   0 77
    //                   1 185
    //                   2 295;
           goto _L1 _L2 _L3 _L4
_L1:
        return;
_L2:
        if (obj instanceof RankAlbumListModel)
        {
            obj = (RankAlbumListModel)obj;
            if (((RankAlbumListModel) (obj)).getMaxPageId() <= ((RankAlbumListModel) (obj)).getPageId())
            {
                flag = false;
            }
            mHasMore = flag;
            if (mHasMore)
            {
                mPageId = mPageId + 1;
            }
            if (mAdapter == null)
            {
                mAdapter = new RankDetailAdapter(getActivity(), this, ((RankAlbumListModel) (obj)).getList());
                mListView.setAdapter(mAdapter);
                return;
            } else
            {
                mAdapter.addData(((RankAlbumListModel) (obj)).getList());
                mAdapter.notifyDataSetChanged();
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (obj instanceof RankAnchorListModel)
        {
            obj = (RankAnchorListModel)obj;
            boolean flag1;
            if (((RankAnchorListModel) (obj)).getMaxPageId() > ((RankAnchorListModel) (obj)).getPageId())
            {
                flag1 = flag3;
            } else
            {
                flag1 = false;
            }
            mHasMore = flag1;
            if (mHasMore)
            {
                mPageId = mPageId + 1;
            }
            if (mAdapter == null)
            {
                mAdapter = new RankDetailAdapter(getActivity(), this, ((RankAnchorListModel) (obj)).getList());
                mListView.setAdapter(mAdapter);
                return;
            } else
            {
                mAdapter.addData(((RankAnchorListModel) (obj)).getList());
                mAdapter.notifyDataSetChanged();
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L4:
        if (obj instanceof RankTrackListModel)
        {
            obj = (RankTrackListModel)obj;
            boolean flag2;
            if (((RankTrackListModel) (obj)).getMaxPageId() > ((RankTrackListModel) (obj)).getPageId())
            {
                flag2 = flag4;
            } else
            {
                flag2 = false;
            }
            mHasMore = flag2;
            if (mHasMore)
            {
                mPageId = mPageId + 1;
            }
            if (mAdapter == null)
            {
                mAdapter = new RankDetailAdapter(getActivity(), this, ((RankTrackListModel) (obj)).getList());
                mListView.setAdapter(mAdapter);
            } else
            {
                mAdapter.addData(((RankTrackListModel) (obj)).getList());
                mAdapter.notifyDataSetChanged();
            }
            setPlayListParam();
            return;
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    private void registerListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls6();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void setPlayListParam()
    {
        HashMap hashmap = new HashMap();
        hashmap.put("key", mKey);
        mAdapter.setDataSource("mobile/discovery/v1/rankingList/track");
        mAdapter.setPageId(mPageId);
        mAdapter.setParams(hashmap);
    }

    private void showReload()
    {
        if (mAdapter == null || mAdapter.getCount() == 0)
        {
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
        }
    }

    private void toAlbum(Object obj, View view)
    {
        if (obj instanceof RankAlbumModel)
        {
            obj = (RankAlbumModel)obj;
            Bundle bundle = new Bundle();
            AlbumModel albummodel = new AlbumModel();
            albummodel.albumId = ((RankAlbumModel) (obj)).getAlbumId();
            bundle.putString("album", JSON.toJSONString(albummodel));
            bundle.putInt("from", 6);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
        }
    }

    private void toAnchor(Object obj, View view)
    {
        if (obj instanceof RankAnchorModel)
        {
            obj = (RankAnchorModel)obj;
            Bundle bundle = new Bundle();
            bundle.putLong("toUid", ((RankAnchorModel) (obj)).getUid());
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
        }
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(this);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mKey = bundle.getString("key");
            mTitle = bundle.getString("title");
            bundle = bundle.getString("type");
            if ("album".equals(bundle))
            {
                mLoaderType = 0;
                mAdapter = new RankDetailAdapter(getActivity(), this, new ArrayList());
                mListView.setAdapter(mAdapter);
            } else
            if ("anchor".equals(bundle))
            {
                mLoaderType = 1;
                mAdapter = new RankDetailAdapter(getActivity(), this, new ArrayList());
                mListView.setAdapter(mAdapter);
            } else
            if ("track".equals(bundle))
            {
                mLoaderType = 2;
                mAdapter = new RankDetailAdapter(getActivity(), this, new ArrayList());
                mListView.setAdapter(mAdapter);
            }
        }
        if (TextUtils.isEmpty(mTitle))
        {
            bundle = "\u699C\u5355";
        } else
        {
            bundle = mTitle;
        }
        setTitleText(bundle);
        initListener();
        ((PullToRefreshListView)mListView).toRefreshing();
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        switch (i)
        {
        default:
            return null;

        case 2: // '\002'
            return new RankTrackListLoader(getActivity(), mKey, mPageId, 40);

        case 0: // '\0'
            return new RankAlbumListLoader(getActivity(), mKey, mPageId, 40);

        case 1: // '\001'
            return new RankAnchorListLoader(getActivity(), mKey, mPageId, 40);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300cf, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        unRegisterListener();
    }

    public void onLoadFinished(Loader loader, final Object data)
    {
        ((PullToRefreshListView)mListView).onRefreshComplete();
        if (loader != null)
        {
            if (data != null)
            {
                doAfterAnimation(new _cls5());
            } else
            {
                showReload();
            }
        }
        mIsLoading = false;
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onPlayCanceled()
    {
    }

    public void onSoundChanged(int i)
    {
        if (mLoaderType == 2 && mAdapter != null)
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
    }

    public void reload(View view)
    {
        ((PullToRefreshListView)mListView).toRefreshing();
    }







/*
    static int access$502(RankDetailFragment rankdetailfragment, int i)
    {
        rankdetailfragment.mPageId = i;
        return i;
    }

*/





    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final RankDetailFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (!OneClickHelper.getInstance().onClick(view)) goto _L2; else goto _L1
_L1:
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= mAdapter.getCount()) goto _L2; else goto _L3
_L3:
            adapterview = ((AdapterView) (mAdapter.getData().get(i)));
            mLoaderType;
            JVM INSTR tableswitch 0 2: default 92
        //                       0 93
        //                       1 103
        //                       2 113;
               goto _L2 _L4 _L5 _L6
_L2:
            return;
_L4:
            toAlbum(adapterview, view);
            return;
_L5:
            toAnchor(adapterview, view);
            return;
_L6:
            gotoPlay(i, mAdapter.getData(), view);
            return;
        }

        _cls1()
        {
            this$0 = RankDetailFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final RankDetailFragment this$0;

        public void onRefresh()
        {
            mPageId = 1;
            loadData(mListView);
        }

        _cls2()
        {
            this$0 = RankDetailFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnScrollListner
    {

        final RankDetailFragment this$0;

        public void onMyScrollStateChanged(AbsListView abslistview, int i)
        {
            int j = abslistview.getCount();
            if (j > 5)
            {
                j -= 5;
            } else
            {
                j--;
            }
            if (mListView.getLastVisiblePosition() > j && i == 0 && !mIsLoading)
            {
                loadMoreData(mListView);
            }
        }

        _cls3()
        {
            this$0 = RankDetailFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final RankDetailFragment this$0;

        public void onClick(View view)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            if (!mIsLoading)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
                loadMoreData(mFooterViewLoading);
            }
        }

        _cls4()
        {
            this$0 = RankDetailFragment.this;
            super();
        }
    }


    private class _cls6 extends OnPlayerStatusUpdateListenerProxy
    {

        final RankDetailFragment this$0;

        public void onPlayStateChange()
        {
            if (mLoaderType == 2 && mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        _cls6()
        {
            this$0 = RankDetailFragment.this;
            super();
        }
    }


    private class _cls5
        implements MyCallback
    {

        final RankDetailFragment this$0;
        final Object val$data;

        public void execute()
        {
            parseData(data);
            if (mAdapter.getCount() > 0)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            } else
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
        }

        _cls5()
        {
            this$0 = RankDetailFragment.this;
            data = obj;
            super();
        }
    }

}
