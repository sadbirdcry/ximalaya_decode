// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.rank;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.ting.android.fragment.finding2.rank.adapter.RankGroupAdapter;
import com.ximalaya.ting.android.model.finding2.rank.RankItemModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.finding2.rank:
//            RankFragment, RankDetailFragment

class this._cls0
    implements android.widget.temClickListener
{

    final RankFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
label0:
        {
            if (OneClickHelper.getInstance().onClick(view))
            {
                i -= RankFragment.access$300(RankFragment.this).getHeaderViewsCount();
                if (i >= 0 && i < RankFragment.access$400(RankFragment.this).getCount())
                {
                    adapterview = (RankItemModel)RankFragment.access$400(RankFragment.this).getData().get(i);
                    if (!adapterview.isTitleView())
                    {
                        break label0;
                    }
                }
            }
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString("key", adapterview.getKey());
        bundle.putString("title", adapterview.getTitle());
        bundle.putString("type", adapterview.getContentType());
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/finding2/rank/RankDetailFragment, bundle);
    }

    ent()
    {
        this$0 = RankFragment.this;
        super();
    }
}
