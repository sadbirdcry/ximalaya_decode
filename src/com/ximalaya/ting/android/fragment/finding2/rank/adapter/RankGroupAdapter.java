// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.rank.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.finding2.rank.RankItemModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

public class RankGroupAdapter extends BaseAdapter
{
    protected static class ViewHolder
    {

        View border;
        ImageView cover;
        TextView firstTitle;
        TextView secondTitle;
        TextView title;

        protected ViewHolder()
        {
        }
    }


    private Context mContext;
    private List mData;
    private BaseFragment mFragment;
    private LayoutInflater mInflater;

    public RankGroupAdapter(Context context, BaseFragment basefragment, List list)
    {
        mInflater = LayoutInflater.from(context);
        mData = list;
        mContext = context;
        mFragment = basefragment;
    }

    private View getContentView(int i, View view, ViewGroup viewgroup)
    {
        Object obj;
        RankItemModel rankitemmodel;
        TextView textview;
        boolean flag;
        boolean flag1;
        if (view == null || view.getTag() == null)
        {
            view = mInflater.inflate(0x7f0301a2, viewgroup, false);
            viewgroup = new ViewHolder();
            viewgroup.cover = (ImageView)view.findViewById(0x7f0a0659);
            mFragment.markImageView(((ViewHolder) (viewgroup)).cover);
            viewgroup.title = (TextView)view.findViewById(0x7f0a0175);
            viewgroup.firstTitle = (TextView)view.findViewById(0x7f0a065a);
            viewgroup.secondTitle = (TextView)view.findViewById(0x7f0a065b);
            viewgroup.border = view.findViewById(0x7f0a00a8);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        obj = mContext;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (i + 1 == mData.size() || nextIsTitle(i))
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        ViewUtil.buildAlbumItemSpace(((Context) (obj)), view, flag, flag1, 81);
        rankitemmodel = (RankItemModel)mData.get(i);
        ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).cover, rankitemmodel.getCoverPath(), 0x7f0202e0);
        textview = ((ViewHolder) (viewgroup)).title;
        if (rankitemmodel.getTitle() == null)
        {
            obj = "";
        } else
        {
            obj = rankitemmodel.getTitle();
        }
        textview.setText(((CharSequence) (obj)));
        obj = rankitemmodel.getFirstKResults();
        if (obj != null && ((List) (obj)).size() > 0)
        {
            ((ViewHolder) (viewgroup)).firstTitle.setText(getFriendlyRank(1, ((com.ximalaya.ting.android.model.finding2.rank.RankItemModel.RankFirstResults)((List) (obj)).get(0)).getTitle()));
            if (rankitemmodel.getFirstKResults().size() > 1)
            {
                ((ViewHolder) (viewgroup)).secondTitle.setText(getFriendlyRank(2, ((com.ximalaya.ting.android.model.finding2.rank.RankItemModel.RankFirstResults)((List) (obj)).get(1)).getTitle()));
                return view;
            } else
            {
                ((ViewHolder) (viewgroup)).secondTitle.setText("");
                return view;
            }
        } else
        {
            ((ViewHolder) (viewgroup)).firstTitle.setText("");
            ((ViewHolder) (viewgroup)).secondTitle.setText("");
            return view;
        }
    }

    private String getFriendlyRank(int i, String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return "";
        } else
        {
            return (new StringBuilder()).append(i).append(" ").append(s).toString();
        }
    }

    private View getTitleView(int i, View view, ViewGroup viewgroup)
    {
        view = mInflater.inflate(0x7f030106, viewgroup, false);
        view.findViewById(0x7f0a0444).setVisibility(8);
        ((TextView)view.findViewById(0x7f0a0158)).setText(((RankItemModel)mData.get(i)).getTitleText());
        if (i != 0)
        {
            view.findViewById(0x7f0a0276).setVisibility(0);
            return view;
        } else
        {
            view.findViewById(0x7f0a0276).setVisibility(8);
            return view;
        }
    }

    private boolean nextIsTitle(int i)
    {
        return i + 1 < mData.size() && ((RankItemModel)mData.get(i + 1)).isTitleView();
    }

    public void addData(List list)
    {
        if (mData == null)
        {
            mData = list;
        } else
        {
            mData.addAll(list);
        }
        notifyDataSetChanged();
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public List getData()
    {
        return mData;
    }

    public Object getItem(int i)
    {
        if (mData == null)
        {
            return null;
        } else
        {
            return (RankItemModel)mData.get(i);
        }
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (((RankItemModel)mData.get(i)).isTitleView())
        {
            return getTitleView(i, view, viewgroup);
        } else
        {
            return getContentView(i, view, viewgroup);
        }
    }

    public void setData(List list)
    {
        mData = list;
        notifyDataSetChanged();
    }
}
