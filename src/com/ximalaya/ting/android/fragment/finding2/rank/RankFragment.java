// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.finding2.rank;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ListView;
import com.ximalaya.ting.android.adapter.FocusImageAdapter;
import com.ximalaya.ting.android.animation.FixedSpeedScroller;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.fragment.finding2.rank.adapter.RankGroupAdapter;
import com.ximalaya.ting.android.fragment.finding2.rank.loader.RankLoader;
import com.ximalaya.ting.android.model.finding2.rank.RankGroupModel;
import com.ximalaya.ting.android.model.finding2.rank.RankItemModel;
import com.ximalaya.ting.android.model.finding2.rank.RankModel;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.util.ViewUtil;
import com.ximalaya.ting.android.view.viewpager.ViewPagerInScroll;
import com.ximalaya.ting.android.view.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import java.util.List;

public class RankFragment extends BaseFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, com.ximalaya.ting.android.fragment.ReloadFragment.Callback
{

    private static final int SWAP_FOCUS_INTERNAL = 5000;
    private RankGroupAdapter mAdapter;
    private List mDataList;
    private FocusImageAdapter mFocusImagesAdapter;
    private View mFocusImagesHeader;
    private CirclePageIndicator mFocusImagesPagerIndicator;
    private ViewPagerInScroll mFocusViewPager;
    private boolean mIsLoadedData;
    private ListView mListView;
    private Loader mLoader;
    private View mLoadingView;
    private Runnable mSwapFocusImagesTask;

    public RankFragment()
    {
        mIsLoadedData = false;
        mSwapFocusImagesTask = new _cls1();
    }

    private void clearRef()
    {
        mListView.setAdapter(null);
        mListView = null;
        fragmentBaseContainerView = null;
    }

    private void initListener()
    {
        mListView.setOnItemClickListener(new _cls2());
    }

    private void initViews()
    {
        if (PackageUtil.isMeizu() && getActivity() != null)
        {
            findViewById(0x7f0a005c).setPadding(0, 0, 0, Utilities.dip2px(getActivity(), 70F));
        }
        mListView = (ListView)findViewById(0x7f0a005c);
        mLoadingView = findViewById(0x7f0a012c);
        mFocusImagesHeader = View.inflate(getActivity(), 0x7f03008b, null);
        mFocusViewPager = (ViewPagerInScroll)mFocusImagesHeader.findViewById(0x7f0a017a);
        mFocusImagesPagerIndicator = (CirclePageIndicator)mFocusImagesHeader.findViewById(0x7f0a0230);
        mListView.addHeaderView(mFocusImagesHeader);
        mFocusViewPager.setDisallowInterceptTouchEventView((ViewGroup)mFocusViewPager.getParent());
        FixedSpeedScroller fixedspeedscroller = new FixedSpeedScroller(getActivity(), new DecelerateInterpolator());
        ViewUtil.setViewPagerScroller(mFocusViewPager, fixedspeedscroller);
        mAdapter = new RankGroupAdapter(getActivity(), this, null);
        mListView.setAdapter(mAdapter);
    }

    private void loadData(boolean flag)
    {
        if (getUserVisibleHint() && getView() != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (!flag && mIsLoadedData)
        {
            continue; /* Loop/switch isn't completed */
        }
        mIsLoadedData = true;
        if (!NetworkUtils.isNetworkAvaliable(getActivity()))
        {
            mLoadingView.setVisibility(8);
            showReload();
            return;
        }
        mLoadingView.setVisibility(0);
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(0x7f0a002e, null, this);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(fragmentBaseContainerView, fragmentBaseContainerView);
            return;
        }
        if (!flag) goto _L1; else goto _L3
_L3:
        mLoader = getLoaderManager().restartLoader(0x7f0a002e, null, this);
        ((MyAsyncTaskLoader)mLoader).setXDCSBindView(fragmentBaseContainerView, fragmentBaseContainerView);
        return;
        if (mDataList == null || mDataList.size() <= 0) goto _L1; else goto _L4
_L4:
        setDataForView(mDataList);
        return;
    }

    public static RankFragment newInstance()
    {
        return new RankFragment();
    }

    private void setDataForView(List list)
    {
        int i = 0;
        ArrayList arraylist = new ArrayList();
        if (list.size() > 1)
        {
            for (; i != list.size(); i++)
            {
                RankGroupModel rankgroupmodel = (RankGroupModel)list.get(i);
                if (rankgroupmodel.getList() != null && rankgroupmodel.getList().size() > 0)
                {
                    RankItemModel rankitemmodel = new RankItemModel();
                    rankitemmodel.setTitleView(true);
                    rankitemmodel.setTitleText(rankgroupmodel.getTitle());
                    arraylist.add(rankitemmodel);
                    arraylist.addAll(rankgroupmodel.getList());
                }
            }

        } else
        if (list.size() == 1 && ((RankGroupModel)list.get(0)).getList() != null)
        {
            arraylist.addAll(((RankGroupModel)list.get(0)).getList());
        }
        mAdapter.setData(arraylist);
    }

    private void showReload()
    {
        if (mAdapter == null || mAdapter.getCount() == 0)
        {
            mLoadingView.setVisibility(8);
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
        }
    }

    private void stopSwapFocusImages()
    {
        if (mFocusViewPager != null)
        {
            mFocusViewPager.removeCallbacks(mSwapFocusImagesTask);
        }
    }

    private void swapFocusImages()
    {
        if (mFocusViewPager != null)
        {
            mFocusViewPager.removeCallbacks(mSwapFocusImagesTask);
            mFocusViewPager.postDelayed(mSwapFocusImagesTask, 5000L);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initViews();
        initListener();
        loadData(false);
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        return new RankLoader(getActivity());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300ce, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        if (mFocusImagesAdapter != null)
        {
            mFocusImagesAdapter.destory();
        }
    }

    public void onLoadFinished(Loader loader, RankModel rankmodel)
    {
label0:
        {
            boolean flag = true;
            mLoadingView.setVisibility(8);
            if (loader != null)
            {
                if (rankmodel != null && rankmodel.getFocusImages() != null && rankmodel.getFocusImages().size() > 0)
                {
                    int i = ToolUtil.getScreenWidth(getActivity());
                    loader = new android.widget.AbsListView.LayoutParams(i, (int)((float)i * 0.46875F));
                    mFocusImagesHeader.setLayoutParams(loader);
                    mFocusImagesAdapter = new FocusImageAdapter(this, rankmodel.getFocusImages(), false);
                    mFocusImagesAdapter.setCycleScrollFlag(true);
                    loader = mFocusImagesAdapter;
                    if (rankmodel.getFocusImages().size() != 1)
                    {
                        flag = false;
                    }
                    loader.setOnlyOnePageFlag(flag);
                    mFocusViewPager.setAdapter(mFocusImagesAdapter);
                    mFocusImagesPagerIndicator.setViewPager(mFocusViewPager);
                    mFocusImagesPagerIndicator.setPagerRealCount(rankmodel.getFocusImages().size());
                    swapFocusImages();
                } else
                {
                    mFocusImagesHeader.setVisibility(8);
                }
                if (rankmodel == null || rankmodel.getDatas() == null || rankmodel.getDatas().size() <= 0)
                {
                    break label0;
                }
                setDataForView(rankmodel.getDatas());
            }
            return;
        }
        showReload();
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (RankModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onResume()
    {
        super.onResume();
        swapFocusImages();
    }

    public void onStop()
    {
        super.onStop();
        stopSwapFocusImages();
    }

    public void reload(View view)
    {
        mIsLoadedData = false;
        loadData(true);
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        loadData(false);
    }






    private class _cls1
        implements Runnable
    {

        final RankFragment this$0;

        public void run()
        {
            if (mFocusViewPager != null && mFocusImagesAdapter != null && mFocusImagesAdapter.getCount() > 1)
            {
                int i = mFocusViewPager.getCurrentItem();
                int j = mFocusImagesAdapter.getCount();
                mFocusViewPager.setCurrentItem((i + 1) % j, true);
                mFocusViewPager.postDelayed(mSwapFocusImagesTask, 5000L);
            }
        }

        _cls1()
        {
            this$0 = RankFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final RankFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
label0:
            {
                if (OneClickHelper.getInstance().onClick(view))
                {
                    i -= mListView.getHeaderViewsCount();
                    if (i >= 0 && i < mAdapter.getCount())
                    {
                        adapterview = (RankItemModel)mAdapter.getData().get(i);
                        if (!adapterview.isTitleView())
                        {
                            break label0;
                        }
                    }
                }
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("key", adapterview.getKey());
            bundle.putString("title", adapterview.getTitle());
            bundle.putString("type", adapterview.getContentType());
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/finding2/rank/RankDetailFragment, bundle);
        }

        _cls2()
        {
            this$0 = RankFragment.this;
            super();
        }
    }

}
