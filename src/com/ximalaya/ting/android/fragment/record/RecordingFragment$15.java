// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.record;

import android.os.Handler;
import android.os.Message;
import com.ximalaya.ting.android.transaction.d.g;

// Referenced classes of package com.ximalaya.ting.android.fragment.record:
//            RecordingFragment

class this._cls0
    implements com.ximalaya.ting.android.transaction.d._cls5
{

    final RecordingFragment this$0;

    public void onError(Exception exception, int i, int j)
    {
        exception.printStackTrace();
        if (2 == i)
        {
            RecordingFragment.access$5700(RecordingFragment.this, "\u521D\u59CB\u5316\u5F55\u97F3\u673A\u5931\u8D25\uFF0C\u53EF\u80FD\u6709\u5176\u4ED6\u5E94\u7528\u6B63\u5728\u4F7F\u7528\u9EA6\u514B\u98CE");
            return;
        }
        if (1 == i)
        {
            RecordingFragment.access$5700(RecordingFragment.this, "\u4FDD\u5B58\u5F55\u97F3\u6570\u636E\u5931\u8D25\uFF0CSD\u5361\u53EF\u80FD\u4E0D\u53EF\u7528");
            return;
        } else
        {
            RecordingFragment.access$5700(RecordingFragment.this, "\u5F55\u97F3\u51FA\u73B0\u9519\u8BEF");
            return;
        }
    }

    public void onPause()
    {
        RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(2).sendToTarget();
    }

    public void onPauseHandleData()
    {
        RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(6).sendToTarget();
    }

    public void onRecording(g g1)
    {
        RecordingFragment.access$1314(RecordingFragment.this, g1.c);
        Message message = RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(5);
        message.arg1 = (int)((float)(1000L * RecordingFragment.access$1300(RecordingFragment.this)) / RecordingFragment.access$5600(RecordingFragment.this));
        message.sendToTarget();
        g.a(g1);
        if (RecordingFragment.access$1300(RecordingFragment.this) >= RecordingFragment.access$4500(RecordingFragment.this))
        {
            RecordingFragment.access$4700(RecordingFragment.this);
        }
    }

    public void onResumeHandleData()
    {
        RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(7).sendToTarget();
    }

    public void onStart()
    {
        RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(1).sendToTarget();
    }

    public void onStop()
    {
        RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(2).sendToTarget();
    }

    ()
    {
        this$0 = RecordingFragment.this;
        super();
    }
}
