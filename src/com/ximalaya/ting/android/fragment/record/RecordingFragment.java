// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.record;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.userspace.SoundListFragmentNew;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.library.view.record.MyRadioGroup;
import com.ximalaya.ting.android.library.view.record.WaveformController;
import com.ximalaya.ting.android.library.view.record.WaveformView;
import com.ximalaya.ting.android.library.view.seekbar.VerticalSeekBar;
import com.ximalaya.ting.android.model.record.BgSound;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.d.b;
import com.ximalaya.ting.android.transaction.d.d;
import com.ximalaya.ting.android.transaction.d.f;
import com.ximalaya.ting.android.transaction.d.k;
import com.ximalaya.ting.android.transaction.d.n;
import com.ximalaya.ting.android.transaction.d.o;
import com.ximalaya.ting.android.transaction.d.p;
import com.ximalaya.ting.android.transaction.d.r;
import com.ximalaya.ting.android.transaction.d.s;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Session;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.fragment.record:
//            GuidePageFragment

public class RecordingFragment extends BaseFragment
{
    private class TelListener extends b
    {

        private static final String TAG = "TelListener";
        final RecordingFragment this$0;

        protected void onCallHangup(String s1)
        {
            Logger.e("TelListener", (new StringBuilder()).append("onCallHangup ").append(s1).toString());
        }

        protected void onCallRinging(String s1)
        {
            Logger.e("TelListener", (new StringBuilder()).append("onCallRinging ").append(s1).toString());
            if (mRecording)
            {
                doActionPauseRecord();
                return;
            }
            if (!mHeadSetOn) goto _L2; else goto _L1
_L1:
            mMixPlayback.d();
_L4:
            mBgPlayer.i();
            return;
_L2:
            if (mPcmPlayback != null)
            {
                mPcmPlayback.b();
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        protected void onCalling(String s1)
        {
            Logger.e("TelListener", (new StringBuilder()).append("onCalling ").append(s1).toString());
        }

        public TelListener(Context context)
        {
            this$0 = RecordingFragment.this;
            super(context);
        }
    }


    private static final int MSG_PAUSE_HANDLE_RECORD = 6;
    private static final int MSG_PAUSE_RECORD = 2;
    private static final int MSG_RESUME_HANDLE_RECORD = 7;
    private static final int MSG_SHOW_TOAST = 8;
    private static final int MSG_START_PLAY = 3;
    private static final int MSG_START_RECORD = 1;
    private static final int MSG_STOP_PLAY = 4;
    private static final int MSG_UPDATE_CURR_TIME = 9;
    private static final int MSG_UPDATE_RECORD_TIME = 5;
    private static final String TAG = "RecordingFragment";
    private static HashMap sLabels;
    private TextView mActTitle;
    private long mActivityId;
    private RadioButton mAddLocalBg;
    private List mAmps;
    private int mAudioFormat;
    private AudioManager mAudioManager;
    private int mAudioSource;
    private View mBgContainer;
    private String mBgPath;
    private f mBgPlayer;
    private boolean mBgSoundChange;
    private MyRadioGroup mBgSoundGroup;
    private d mBgSoundManager;
    private TextView mBgTitle;
    private float mBgVol;
    private VerticalSeekBar mBgVolume;
    private ImageView mBtnBack;
    private View mBtnCut;
    private View mBtnCutCancel;
    private View mBtnCutOK;
    private View mBtnFeedback;
    private View mBtnGuide;
    private ImageView mBtnMore;
    private ImageView mBtnPreListen;
    private ImageView mBtnRecord;
    private View mBtnReset;
    private View mBtnSave;
    private float mBytePerSeconds;
    private r mCacheManager;
    private int mChannels;
    private boolean mCheckFromUser;
    private boolean mCheckHeadSet;
    private ViewGroup mControlBar;
    private WaveformController mController;
    private BgSound mCurrBg;
    private PopupWindow mCutConfirm;
    private View mCutConfirmContent;
    private float mCutPercent;
    private CutTask mCutTask;
    private String mEncodeFileDir;
    private String mEncodeFileName;
    private Fragment mFragment;
    private FragmentManager mFragmentManager;
    private List mGuideImages;
    private int mGuideIndex;
    private GuidePageFragment mGuidePage;
    private Handler mHandler;
    private IntentFilter mHeadSetFilter;
    private boolean mHeadSetOn;
    private BroadcastReceiver mHeadSetReceiver;
    private ImageView mLeftScale;
    private LoadBgTask mLoadBgTask;
    private float mMaxBgVol;
    private long mMaxLength;
    private k mMixPlayback;
    private PopupWindow mMoreMenu;
    private View mMoreMenuContent;
    private List mOperations;
    private o mPcmPlayback;
    private p mPcmRecorder;
    private com.ximalaya.ting.android.transaction.d.o.a mPlaybackListener;
    private Map mPositions;
    private SharedPreferencesUtil mPreference;
    private TextView mRecCurrTime;
    private ImageView mRecState;
    private s mRecordFile;
    private String mRecordName;
    private String mRecordPath;
    private TextView mRecordTime;
    private boolean mRecording;
    private boolean mRegisted;
    private ImageView mRightScale;
    private int mSampleRate;
    private List mSelectedBg;
    private RadioButton mSetNoBg;
    private TelListener mTelListener;
    private long mTotal;
    private TextView mVolumeLabel;
    private WaveformView mWaveformView;

    public RecordingFragment()
    {
        mBgSoundChange = false;
        mRecording = false;
        mMaxBgVol = 0.5F;
        mAmps = new ArrayList();
        mSelectedBg = new ArrayList();
        mSampleRate = 44100;
        mChannels = 16;
        mAudioFormat = 2;
        mAudioSource = 1;
        mCheckFromUser = true;
        mBgVol = 1.0F;
        mOperations = new ArrayList();
        mPositions = new HashMap();
        mRegisted = false;
        mGuideImages = Arrays.asList(new Integer[] {
            Integer.valueOf(0x7f020494), Integer.valueOf(0x7f020495), Integer.valueOf(0x7f020496), Integer.valueOf(0x7f020497), Integer.valueOf(0x7f020498), Integer.valueOf(0x7f020499), Integer.valueOf(0x7f02049a)
        });
        mHandler = new _cls1();
        mPlaybackListener = new _cls2();
        mHeadSetReceiver = new _cls3();
    }

    private boolean checkHeadSet()
    {
        if (mAudioManager != null)
        {
            mHeadSetOn = mAudioManager.isWiredHeadsetOn();
        }
        return mHeadSetOn;
    }

    private void clearCache()
    {
        if (mRecordFile != null)
        {
            mRecordFile.c();
        }
    }

    private void computeUISize()
    {
        int i = ToolUtil.getScreenHeight(mActivity);
        int j = ToolUtil.getStatusBarHeight(mCon);
        int l = ToolUtil.dp2px(mCon, 50F);
        int i1 = getResources().getDimensionPixelSize(0x7f080003);
        int j1 = ToolUtil.dp2px(mCon, 50F);
        int k1 = getResources().getDimensionPixelSize(0x7f080004);
        int l1 = getResources().getDimensionPixelSize(0x7f080006);
        j = ToolUtil.dp2px(mCon, 40F) + (i - j - l - i1 - j1 - k1 - 0 - l1);
        l = ToolUtil.dp2px(mCon, 225F);
        i = j;
        if (j > l)
        {
            i = l;
        }
        j = (int)((float)(i * 130) / 350F);
        android.view.ViewGroup.LayoutParams layoutparams = mLeftScale.getLayoutParams();
        ViewGroup viewgroup;
        if (layoutparams == null)
        {
            layoutparams = new android.view.ViewGroup.LayoutParams(j, i);
        } else
        {
            layoutparams.width = j;
            layoutparams.height = i;
        }
        mLeftScale.setLayoutParams(layoutparams);
        mLeftScale.postInvalidate();
        layoutparams = mRightScale.getLayoutParams();
        if (layoutparams == null)
        {
            layoutparams = new android.view.ViewGroup.LayoutParams(j, i);
        } else
        {
            layoutparams.width = j;
            layoutparams.height = i;
        }
        mRightScale.setLayoutParams(layoutparams);
        mRightScale.postInvalidate();
        viewgroup = (ViewGroup)mBgContainer.findViewById(0x7f0a0508);
        layoutparams = viewgroup.getLayoutParams();
        if (layoutparams == null)
        {
            layoutparams = new android.view.ViewGroup.LayoutParams(-2, i + ToolUtil.dp2px(mCon, 5F));
        } else
        {
            layoutparams.height = i + ToolUtil.dp2px(mCon, 5F);
        }
        viewgroup.setLayoutParams(layoutparams);
        viewgroup.postInvalidate();
    }

    private void doActionMixRecord()
    {
        if (mMixPlayback != null)
        {
            mMixPlayback.d();
            mMixPlayback.a(0L);
        }
        mBgPlayer.h();
        mBgPlayer.a(mBgVol * mMaxBgVol, mBgVol * mMaxBgVol);
        mPcmRecorder.b();
        if (mCurrBg != null)
        {
            mBgPlayer.a(mCurrBg, getLastPlayPosition(mCurrBg));
            mBgPlayer.g();
        }
        mController.startRecord();
        if (mOperations.size() > 0)
        {
            n n1 = (n)mOperations.get(mOperations.size() - 1);
            if (n1.d == 3)
            {
                mOperations.remove(n1);
            }
        }
        n n2 = new n();
        n2.d = 1;
        n2.a = mRecordFile.i();
        n2.e = mCurrBg;
        int i;
        if (mCurrBg == null)
        {
            i = 0;
        } else
        {
            i = getLastPlayPosition(mCurrBg);
        }
        n2.g = i;
        n2.b = mBgVol * mMaxBgVol;
        mOperations.add(n2);
    }

    private void doActionPauseRecord()
    {
        mRecording = false;
        mBgPlayer.h();
        if (mCurrBg != null)
        {
            setLastPlayPosition(mCurrBg, mBgPlayer.d());
        }
        mPcmRecorder.c();
        mController.startEdit();
        n n1 = new n();
        n1.d = 3;
        n1.a = mRecordFile.i();
        mOperations.add(n1);
    }

    private void doActionRecord()
    {
        mRecording = true;
        if (mHeadSetOn)
        {
            doActionMixRecord();
            return;
        } else
        {
            doActionSingleRecord();
            return;
        }
    }

    private void doActionSingleRecord()
    {
        mPcmPlayback.b();
        mPcmPlayback.a(0L);
        mBgPlayer.h();
        mBgPlayer.a(mBgVol * mMaxBgVol, mBgVol * mMaxBgVol);
        mPcmRecorder.b();
        if (mCurrBg != null)
        {
            mBgPlayer.a(mCurrBg, getLastPlayPosition(mCurrBg));
            mBgPlayer.g();
        }
        mController.startRecord();
    }

    private void doActionStopRecord()
    {
        mRecording = false;
        mBgPlayer.h();
        if (mCurrBg != null)
        {
            setLastPlayPosition(mCurrBg, mBgPlayer.d());
        }
        mPcmRecorder.d();
        mController.stopRecord();
        n n1 = new n();
        n1.d = 3;
        n1.a = mRecordFile.i();
        mOperations.add(n1);
    }

    private String getFileNameFromPath(String s1)
    {
        if (TextUtils.isEmpty(s1))
        {
            return "";
        }
        int i = s1.lastIndexOf("/");
        int l = s1.lastIndexOf(".");
        int j;
        if (i == -1)
        {
            i = 0;
        } else
        {
            i++;
        }
        j = l;
        if (l == -1)
        {
            j = s1.length();
        }
        return s1.substring(i, j).trim();
    }

    private void goSoundListFragment()
    {
        if (MainTabActivity2.mainTabActivity != null)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("DEST_FLAG", 1);
            MainTabActivity2.mainTabActivity.startFragment(com/ximalaya/ting/android/fragment/userspace/SoundListFragmentNew, bundle);
        }
    }

    private void handlePauseHandleData()
    {
        if (!mFragment.isAdded())
        {
            return;
        } else
        {
            mBtnRecord.setImageResource(0x7f0200ff);
            mBtnPreListen.setEnabled(true);
            mBtnCut.setVisibility(0);
            return;
        }
    }

    private void handlePauseRecord()
    {
        if (!mFragment.isAdded())
        {
            return;
        } else
        {
            mRecState.setVisibility(8);
            mRecCurrTime.setText("00:00");
            mRecCurrTime.setVisibility(0);
            mBtnRecord.setImageResource(0x7f0200ff);
            mBtnPreListen.setEnabled(true);
            mBtnPreListen.setImageResource(0x7f0200fd);
            mBtnCut.setVisibility(0);
            mBtnReset.setVisibility(0);
            mBtnSave.setVisibility(0);
            return;
        }
    }

    private void handleShowToast(Message message)
    {
        if (!mFragment.isAdded())
        {
            return;
        } else
        {
            Toast.makeText(mCon, (String)message.obj, 1).show();
            return;
        }
    }

    private void handleStartHandleData()
    {
        if (!mFragment.isAdded())
        {
            return;
        } else
        {
            mBtnRecord.setImageResource(0x7f020100);
            mBtnPreListen.setImageResource(0x7f0200fe);
            mBtnCut.setVisibility(8);
            mCutPercent = 1.0F;
            return;
        }
    }

    private void handleStartPlay()
    {
        if (!mFragment.isAdded())
        {
            return;
        } else
        {
            mBtnPreListen.setImageResource(0x7f0200fc);
            return;
        }
    }

    private void handleStartRecord()
    {
        if (!mFragment.isAdded())
        {
            return;
        } else
        {
            mRecState.setImageResource(0x7f02048b);
            mRecState.setVisibility(0);
            mRecCurrTime.setVisibility(8);
            mBtnRecord.setImageResource(0x7f020100);
            mBtnPreListen.setEnabled(false);
            mBtnPreListen.setImageResource(0x7f0200fe);
            mBtnCut.setVisibility(8);
            mBtnReset.setVisibility(8);
            mBtnSave.setVisibility(8);
            mCutPercent = 1.0F;
            return;
        }
    }

    private void handleStopPlay()
    {
        if (!mFragment.isAdded())
        {
            return;
        } else
        {
            mBtnPreListen.setImageResource(0x7f0200fd);
            mRecCurrTime.setText("00:00");
            return;
        }
    }

    private void handleUpdateCurrTime(Message message)
    {
        float f1 = (float)message.arg1 / 1000F;
        mRecCurrTime.setText(ToolUtil.toTime(f1));
    }

    private void handleUpdateRecordTime(Message message)
    {
        if (!mFragment.isAdded())
        {
            return;
        } else
        {
            float f1 = (float)message.arg1 / 1000F;
            mRecordTime.setText(ToolUtil.toTime(f1));
            return;
        }
    }

    private void hideGuide()
    {
        try
        {
            FragmentTransaction fragmenttransaction = mFragmentManager.beginTransaction();
            fragmenttransaction.remove(mGuidePage);
            fragmenttransaction.commitAllowingStateLoss();
            mGuidePage.setOnPagerChangeListener(null);
            mGuidePage.setCloseListener(null);
            mGuidePage = null;
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void init()
    {
        initCacheDirs();
        mPreference = SharedPreferencesUtil.getInstance(mCon);
        mBgSoundManager = d.a();
        mBgSoundManager.a(mBgPath);
        mAudioManager = (AudioManager)mCon.getSystemService("audio");
        mBtnMore.setImageResource(0x7f0200fa);
        mBtnMore.setScaleType(android.widget.ImageView.ScaleType.CENTER);
        mBtnMore.setVisibility(0);
        initBgSound();
        mWaveformView.setPointerDrawable(0x7f02049c);
        mController = mWaveformView.getController();
        mController.setAmps(mAmps);
        String s1;
        String s2;
        int j;
        s1 = mRecordPath;
        s2 = mRecordName;
        j = mSampleRate;
        int i;
        byte byte0;
        if (mChannels == 12)
        {
            i = 2;
        } else
        {
            i = 1;
        }
        if (mAudioFormat == 2)
        {
            byte0 = 16;
        } else
        {
            byte0 = 8;
        }
        try
        {
            mRecordFile = new s(s1, s2, j, i, byte0);
        }
        catch (IOException ioexception)
        {
            ioexception.printStackTrace();
        }
        mMaxLength = (long)(3600F * mRecordFile.a());
        mBgPlayer = new f(mCon);
        mBytePerSeconds = mRecordFile.a();
        mPcmRecorder = new p(mRecordFile);
        mPcmRecorder.a(mAudioSource, mSampleRate, mChannels, mAudioFormat);
        i = (int)getResources().getDimension(0x7f080003);
        mPcmRecorder.a(i - ToolUtil.dp2px(mCon, 20F));
        mPcmRecorder.a(mAmps);
        mBgVolume.setProgress(30);
        mBgVol = 0.3F;
        mBtnMore.setOnClickListener(new _cls6());
        mBgVolume.setOnSeekBarChangeListener(new _cls7());
        mBgPlayer.a(new _cls8());
        mBtnReset.setOnClickListener(new _cls9());
        mBtnSave.setOnClickListener(new _cls10());
        mBtnRecord.setOnClickListener(new _cls11());
        mBtnPreListen.setOnClickListener(new _cls12());
        mWaveformView.setClickListener(new _cls13());
        mWaveformView.setOnCutListener(new _cls14());
        mPcmRecorder.a(new _cls15());
        mAddLocalBg.setOnClickListener(new _cls16());
        mBgSoundGroup.setOnChildStatusChangeListener(new _cls17());
        if (!mPreference.contains("REC_FIRST_USE"))
        {
            showGuide();
        }
        return;
    }

    private void initBeforeCheck()
    {
        mActTitle.setText("\u5F55\u97F3");
        mBtnBack.setOnClickListener(new _cls5());
    }

    private void initBgSound()
    {
        if (mLoadBgTask == null || mLoadBgTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mLoadBgTask = new LoadBgTask(null);
            mLoadBgTask.myexec(new String[0]);
        }
    }

    private boolean initCacheDirs()
    {
        mBgPath = mCacheManager.d();
        mRecordPath = mCacheManager.e();
        mEncodeFileDir = mCacheManager.g();
        mRecordName = (new StringBuilder()).append(System.currentTimeMillis()).append(".pcm").toString();
        mEncodeFileName = (new StringBuilder()).append("record").append(System.currentTimeMillis()).append(".mp3").toString();
        return true;
    }

    private void initCutConfirm()
    {
        if (mCutConfirmContent == null)
        {
            mCutConfirmContent = LayoutInflater.from(mCon).inflate(0x7f0301a3, mBgSoundGroup, false);
            mBtnCutOK = mCutConfirmContent.findViewById(0x7f0a065c);
            mBtnCutCancel = mCutConfirmContent.findViewById(0x7f0a065d);
            android.view.ViewGroup.LayoutParams layoutparams1 = mCutConfirmContent.getLayoutParams();
            android.view.ViewGroup.LayoutParams layoutparams = layoutparams1;
            if (layoutparams1 == null)
            {
                layoutparams = new android.view.ViewGroup.LayoutParams(-1, -1);
            }
            layoutparams.height = mBgContainer.getHeight() + (int)getResources().getDimension(0x7f080006);
            mCutConfirmContent.setLayoutParams(layoutparams);
            mCutConfirm = new PopupWindow(mCutConfirmContent, -1, layoutparams.height);
            mCutConfirm.setOutsideTouchable(true);
            mCutConfirm.setFocusable(false);
            mBtnCutOK.setOnClickListener(new _cls22());
            mBtnCutCancel.setOnClickListener(new _cls23());
        }
    }

    private void initGuidePage()
    {
        mGuidePage = new GuidePageFragment();
        mGuidePage.setImages(mGuideImages);
        mGuidePage.setOnPagerChangeListener(new _cls18());
        mGuidePage.setCloseListener(new _cls19());
    }

    private void initMoreMenu()
    {
        if (mMoreMenuContent != null)
        {
            return;
        } else
        {
            mMoreMenuContent = LayoutInflater.from(mCon).inflate(0x7f0301a5, mBgSoundGroup, false);
            mBtnGuide = mMoreMenuContent.findViewById(0x7f0a0661);
            mBtnFeedback = mMoreMenuContent.findViewById(0x7f0a05a1);
            mMoreMenu = new PopupWindow(mMoreMenuContent, -2, -2);
            mMoreMenu.setBackgroundDrawable(new BitmapDrawable());
            mMoreMenu.setOutsideTouchable(true);
            mMoreMenu.setFocusable(true);
            mBtnGuide.setOnClickListener(new _cls20());
            mBtnFeedback.setOnClickListener(new _cls21());
            return;
        }
    }

    private List loadBgFromAsset()
        throws IOException
    {
        mBgSoundChange = true;
        AssetManager assetmanager = mCon.getAssets();
        String as[] = assetmanager.list("bg_sound");
        if (as == null || as.length == 0)
        {
            return null;
        }
        ArrayList arraylist = new ArrayList();
        int j = as.length;
        for (int i = 0; i < j; i++)
        {
            String s1 = as[i];
            FileUtils.copyAssetsToFile(assetmanager, (new StringBuilder()).append("bg_sound").append(File.separator).append(s1).toString(), (new StringBuilder()).append(mBgPath).append(s1).toString());
            BgSound bgsound = new BgSound();
            bgsound.type = 3;
            bgsound.candelete = false;
            bgsound.path = (new StringBuilder()).append(mBgPath).append(s1).toString();
            bgsound.title = getFileNameFromPath(bgsound.path);
            bgsound.songLink = (new StringBuilder()).append("file://").append(bgsound.path).toString();
            arraylist.add(bgsound);
        }

        mPreference.saveString("BG_SOUND_CACHE", JSON.toJSONString(arraylist));
        return arraylist;
    }

    private void showGuide()
    {
        try
        {
            if (mGuidePage == null)
            {
                initGuidePage();
            }
            mGuideIndex = 0;
            if (mGuidePage != null && !mGuidePage.isAdded())
            {
                FragmentTransaction fragmenttransaction = mFragmentManager.beginTransaction();
                fragmenttransaction.add(0x7f0a00f1, mGuidePage);
                fragmenttransaction.commitAllowingStateLoss();
            }
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void showMessage(String s1)
    {
        if (TextUtils.isEmpty(s1))
        {
            return;
        } else
        {
            Message message = mHandler.obtainMessage(8);
            message.obj = s1;
            message.sendToTarget();
            return;
        }
    }

    private void showToastDelay(final String msg, long l)
    {
        mHandler.postDelayed(new _cls4(), l);
    }

    public int getLastPlayPosition(BgSound bgsound)
    {
        int i = 0;
        if (mPositions.keySet().contains(bgsound))
        {
            i = ((Integer)mPositions.get(bgsound)).intValue();
        }
        Logger.e("RecordingFragment", (new StringBuilder()).append("getLastPlayPosition ").append(bgsound.title).append("/").append(i).toString());
        return i;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mFragment = this;
        initBeforeCheck();
        mCacheManager = r.a();
        if (!mCacheManager.b())
        {
            showToastDelay("SD\u5361\u4E0D\u53EF\u7528\u6216\u4E0D\u53EF\u5199\uFF0C\u8BF7\u68C0\u67E5SD\u5361\u662F\u5426\u5DF2\u51C6\u5907\u597D\uFF01", 1000L);
            return;
        }
        if (!mCacheManager.c())
        {
            showToastDelay("\u521B\u5EFA\u7F13\u5B58\u76EE\u5F55\u5931\u8D25\uFF0C\u8BF7\u68C0\u67E5SD\u5361\u662F\u5426\u5DF2\u51C6\u5907\u597D\uFF01", 1000L);
            return;
        }
        mHeadSetFilter = new IntentFilter();
        mHeadSetFilter.addAction("android.intent.action.HEADSET_PLUG");
        mActivity.registerReceiver(mHeadSetReceiver, mHeadSetFilter);
        mRegisted = true;
        mTelListener = new TelListener(mActivity);
        mTelListener.registe();
        bundle = LocalMediaService.getInstance();
        mFragmentManager = getFragmentManager();
        if (bundle != null)
        {
            bundle.pause();
        }
        computeUISize();
        init();
        bundle = getArguments();
        if (bundle != null && bundle.containsKey("activity_id"))
        {
            mActivityId = bundle.getLong("activity_id");
        }
        mPreference.saveBoolean("REC_FIRST_USE", false);
    }

    public boolean onBackPressed()
    {
        if (mGuidePage != null && mGuidePage.isAdded())
        {
            hideGuide();
            return true;
        }
        if (mRecordFile != null && mRecordFile.i() > 0L)
        {
            DialogBuilder dialogbuilder = new DialogBuilder(mActivity);
            dialogbuilder.setOutsideTouchCancel(false);
            dialogbuilder.setTitle("\u6E29\u99A8\u63D0\u793A").setMessage("\u76F4\u63A5\u9000\u51FA\u5C06\u4E22\u5931\u5F55\u97F3\u6570\u636E\uFF0C\u662F\u5426\u5B58\u5728\u8349\u7A3F\u7BB1\u7559\u7740\u4E0B\u6B21\u4F7F\u7528\uFF1F").setCancelBtn("\u9000\u51FA", new _cls25()).setOkBtn("\u5B58\u8349\u7A3F\u7BB1", new _cls24()).showConfirm();
            return true;
        } else
        {
            return super.onBackPressed();
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030028, viewgroup, false);
        mBtnBack = (ImageView)findViewById(0x7f0a007b);
        mActTitle = (TextView)findViewById(0x7f0a00ae);
        mBtnMore = (ImageView)findViewById(0x7f0a0710);
        mWaveformView = (WaveformView)findViewById(0x7f0a00f5);
        mBgTitle = (TextView)findViewById(0x7f0a00f7);
        mControlBar = (ViewGroup)findViewById(0x7f0a00f8);
        mBtnPreListen = (ImageView)findViewById(0x7f0a00f9);
        mBtnPreListen.setEnabled(false);
        mRecordTime = (TextView)findViewById(0x7f0a00fa);
        mRecCurrTime = (TextView)findViewById(0x7f0a00fc);
        mRecState = (ImageView)findViewById(0x7f0a00fb);
        mBtnCut = findViewById(0x7f0a00fd);
        mBgContainer = findViewById(0x7f0a00fe);
        mBgSoundGroup = (MyRadioGroup)mBgContainer.findViewById(0x7f0a0505);
        mAddLocalBg = (RadioButton)mBgContainer.findViewById(0x7f0a0504);
        mBgVolume = (VerticalSeekBar)mBgContainer.findViewById(0x7f0a0509);
        mLeftScale = (ImageView)mBgContainer.findViewById(0x7f0a050a);
        mRightScale = (ImageView)mBgContainer.findViewById(0x7f0a050b);
        mVolumeLabel = (TextView)mBgContainer.findViewById(0x7f0a0507);
        mBtnRecord = (ImageView)findViewById(0x7f0a00f3);
        mBtnReset = findViewById(0x7f0a00f4);
        mBtnSave = findViewById(0x7f0a00f0);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        if (mTelListener != null)
        {
            mTelListener.unRegiste();
        }
        if (mActivity != null && mRegisted)
        {
            mActivity.unregisterReceiver(mHeadSetReceiver);
        }
        if (mPreference != null)
        {
            mPreference.removeByKey("REC_RECORDING");
        }
        if (mPcmRecorder != null)
        {
            mPcmRecorder.d();
        }
        if (mBgPlayer != null)
        {
            mBgPlayer.i();
            mBgPlayer.j();
        }
        if (mMixPlayback != null)
        {
            mMixPlayback.a();
            mMixPlayback = null;
        }
        if (mPcmPlayback != null)
        {
            mPcmPlayback.b();
            mPcmPlayback = null;
        }
        if (mHandler != null)
        {
            mHandler.removeCallbacksAndMessages(null);
        }
        if (mBgSoundChange)
        {
            mPreference.saveString("BG_SOUND_CACHE", JSON.toJSONString(mSelectedBg));
        }
        if (mMixPlayback != null)
        {
            mMixPlayback.a();
        }
        super.onDestroy();
    }

    public void onPause()
    {
        mWaveformView.onPause();
        super.onPause();
    }

    public void onResume()
    {
        mWaveformView.onResume();
        if (Session.getSession().get("BG_SOUND_CACHE_CHANGE") != null)
        {
            Session.getSession().remove("BG_SOUND_CACHE_CHANGE");
            initBgSound();
        }
        super.onResume();
    }

    public void setActivityId(long l)
    {
        mActivityId = l;
    }

    public void setLastPlayPosition(BgSound bgsound, int i)
    {
        Logger.e("RecordingFragment", (new StringBuilder()).append("setLastPlayPosition ").append(bgsound.title).append("/").append(i).toString());
        mPositions.put(bgsound, Integer.valueOf(i));
    }

    static 
    {
        sLabels = new HashMap();
        sLabels.put("SweetDreams", "\u6292\u60C5");
        sLabels.put("Murder", "\u60CA\u609A");
        sLabels.put("Honey Trap", "\u6B22\u4E50");
        sLabels.put("You Are Haneulmalraria", "\u5E7D\u9759");
        sLabels.put("Execution", "\u6050\u6016");
    }








/*
    static long access$1302(RecordingFragment recordingfragment, long l)
    {
        recordingfragment.mTotal = l;
        return l;
    }

*/


/*
    static long access$1314(RecordingFragment recordingfragment, long l)
    {
        l = recordingfragment.mTotal + l;
        recordingfragment.mTotal = l;
        return l;
    }

*/












/*
    static float access$2202(RecordingFragment recordingfragment, float f1)
    {
        recordingfragment.mBgVol = f1;
        return f1;
    }

*/









/*
    static k access$2902(RecordingFragment recordingfragment, k k1)
    {
        recordingfragment.mMixPlayback = k1;
        return k1;
    }

*/




/*
    static o access$3002(RecordingFragment recordingfragment, o o1)
    {
        recordingfragment.mPcmPlayback = o1;
        return o1;
    }

*/














/*
    static boolean access$4102(RecordingFragment recordingfragment, boolean flag)
    {
        recordingfragment.mCheckHeadSet = flag;
        return flag;
    }

*/

















/*
    static float access$5502(RecordingFragment recordingfragment, float f1)
    {
        recordingfragment.mCutPercent = f1;
        return f1;
    }

*/





/*
    static boolean access$5802(RecordingFragment recordingfragment, boolean flag)
    {
        recordingfragment.mCheckFromUser = flag;
        return flag;
    }

*/



/*
    static BgSound access$5902(RecordingFragment recordingfragment, BgSound bgsound)
    {
        recordingfragment.mCurrBg = bgsound;
        return bgsound;
    }

*/






/*
    static RadioButton access$6202(RecordingFragment recordingfragment, RadioButton radiobutton)
    {
        recordingfragment.mSetNoBg = radiobutton;
        return radiobutton;
    }

*/



/*
    static int access$6402(RecordingFragment recordingfragment, int i)
    {
        recordingfragment.mGuideIndex = i;
        return i;
    }

*/






/*
    static CutTask access$6802(RecordingFragment recordingfragment, CutTask cuttask)
    {
        recordingfragment.mCutTask = cuttask;
        return cuttask;
    }

*/


















    private class _cls2
        implements com.ximalaya.ting.android.transaction.d.o.a
    {

        final RecordingFragment this$0;

        public void onPlayerPaused()
        {
            mHandler.obtainMessage(4).sendToTarget();
        }

        public void onPlayerResume()
        {
            mHandler.obtainMessage(3).sendToTarget();
        }

        public void onPlayerStart()
        {
            mHandler.obtainMessage(3).sendToTarget();
        }

        public void onPlayerStopped()
        {
            mHandler.obtainMessage(4).sendToTarget();
        }

        public void onProgressUpdate(float f1)
        {
            mWaveformView.updatePlayerProgress(f1);
            Message message = mHandler.obtainMessage(9);
            message.arg1 = (int)(((float)(1000L * mTotal) * f1) / mRecordFile.a());
            message.sendToTarget();
        }

        _cls2()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls3 extends BroadcastReceiver
    {

        final RecordingFragment this$0;

        public void onReceive(Context context, Intent intent)
        {
            if (!mRecording)
            {
                return;
            } else
            {
                showToast("\u4EB2~\u5F55\u97F3\u8FC7\u7A0B\u4E2D\u8BF7\u4E0D\u8981\u62D4\u63D2\u8033\u673A\u54E6\uFF01");
                return;
            }
        }

        _cls3()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final RecordingFragment this$0;

        public void onClick(View view)
        {
            initMoreMenu();
            mMoreMenu.showAsDropDown(mBtnMore, 0, 0);
        }

        _cls6()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.widget.SeekBar.OnSeekBarChangeListener
    {

        final RecordingFragment this$0;

        public void onProgressChanged(SeekBar seekbar, int i, boolean flag)
        {
            mVolumeLabel.setText((new StringBuilder()).append("\u914D\u4E50\u97F3\u91CF:").append(i).append("%").toString());
            mBgVol = (float)i / 100F;
            if (mRecording)
            {
                mBgPlayer.a(mMaxBgVol * mBgVol, mMaxBgVol * mBgVol);
                return;
            } else
            {
                mBgPlayer.a(mBgVol, mBgVol);
                return;
            }
        }

        public void onStartTrackingTouch(SeekBar seekbar)
        {
        }

        public void onStopTrackingTouch(SeekBar seekbar)
        {
            mBgVol = (float)seekbar.getProgress() / 100F;
            if (mRecording)
            {
                mBgPlayer.a(mMaxBgVol * mBgVol, mMaxBgVol * mBgVol);
                if (mHeadSetOn)
                {
                    seekbar = new n();
                    seekbar.d = 2;
                    seekbar.a = mRecordFile.i();
                    seekbar.b = mBgVol * mMaxBgVol;
                    seekbar.c = mBgVol * mMaxBgVol;
                    mOperations.add(seekbar);
                }
                return;
            } else
            {
                mBgPlayer.a(mBgVol, mBgVol);
                return;
            }
        }

        _cls7()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls8
        implements com.ximalaya.ting.android.transaction.d.h.a
    {

        final RecordingFragment this$0;

        public void onComplete()
        {
        }

        public void onError(Exception exception, int i, int j)
        {
            Logger.e("RecordingFragment", (new StringBuilder()).append("bg onError what: ").append(i).append(", extra: ").append(j).toString());
            if (exception != null)
            {
                exception.printStackTrace();
            }
        }

        public void onPause()
        {
        }

        public void onStart()
        {
            mBgPlayer.e().duration = (float)mBgPlayer.c() / 1000F;
        }

        public void onStop()
        {
        }

        _cls8()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }




    private class _cls11
        implements android.view.View.OnClickListener
    {

        final RecordingFragment this$0;

        public void onClick(View view)
        {
            if (!mCheckHeadSet)
            {
                mPreference.saveBoolean("REC_RECORDING", true);
                findViewById(0x7f0a00f6).setVisibility(8);
                findViewById(0x7f0a00aa).setVisibility(4);
                checkHeadSet();
                if (mHeadSetOn)
                {
                    mMixPlayback = new k(mCon, mRecordFile, mOperations);
                    mMixPlayback.a(mPlaybackListener);
                    mPcmRecorder.a(a.g, 0);
                } else
                {
                    try
                    {
                        mPcmPlayback = new o();
                        mPcmPlayback.a(mRecordFile);
                        mPcmPlayback.a(mPlaybackListener);
                    }
                    // Misplaced declaration of an exception variable
                    catch (View view)
                    {
                        view.printStackTrace();
                    }
                }
                mCheckHeadSet = true;
            }
            if (!mRecording && mTotal >= mMaxLength)
            {
                showToast("\u4EB2\uFF0C\u6700\u957F\u53EA\u80FD\u5F55\u97F3\u4E00\u5C0F\u65F6\u54E6~");
                return;
            }
            if (!mRecording)
            {
                showToast("\u5F00\u59CB\u5F55\u97F3\uFF0C\u6234\u4E0A\u8033\u673A\u6548\u679C\u66F4\u597D\u54E6\uFF01");
                doActionRecord();
                return;
            } else
            {
                doActionPauseRecord();
                return;
            }
        }

        _cls11()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls12
        implements android.view.View.OnClickListener
    {

        final RecordingFragment this$0;

        public void onClick(View view)
        {
            mBgPlayer.i();
            if (mHeadSetOn)
            {
                if (mMixPlayback.b())
                {
                    mMixPlayback.d();
                } else
                {
                    mMixPlayback.c();
                }
            } else
            if (mPcmPlayback.f())
            {
                mPcmPlayback.b();
            } else
            {
                try
                {
                    mRecordFile.d();
                    mPcmPlayback.a();
                }
                // Misplaced declaration of an exception variable
                catch (View view)
                {
                    view.printStackTrace();
                }
            }
            if (mWaveformView.getMode() != 3 && mWaveformView.getMode() != 2)
            {
                mController.startPlay();
            }
        }

        _cls12()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls13
        implements com.ximalaya.ting.android.library.view.record.WaveformView.ClickListener
    {

        final RecordingFragment this$0;

        public void onClick(float f1)
        {
            if (mHeadSetOn)
            {
                mMixPlayback.d();
                mMixPlayback.a((long)((float)mRecordFile.i() * f1), mRecordFile.i());
                return;
            } else
            {
                mPcmPlayback.b();
                mPcmPlayback.a((long)((float)mRecordFile.i() * f1), mRecordFile.i());
                return;
            }
        }

        _cls13()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }




    private class _cls16
        implements android.view.View.OnClickListener
    {

        final RecordingFragment this$0;

        public void onClick(View view)
        {
            startActivity(new Intent(mCon, com/ximalaya/ting/android/activity/recording/RecLocalBgAct));
        }

        _cls16()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls17
        implements com.ximalaya.ting.android.library.view.record.MyRadioGroup.OnChildStatusChangeListener
    {

        private long mLastCheckId;
        final RecordingFragment this$0;

        public void onCheck(RadioGroup radiogroup, View view)
        {
_L2:
            mLastCheckId = view.getId();
            return;
            if (view == mSetNoBg || (long)view.getId() != mLastCheckId || mBgPlayer == null) goto _L2; else goto _L1
_L1:
            switch (mBgPlayer.f())
            {
            case -1: 
            case 0: // '\0'
                int i;
                if (mHeadSetOn)
                {
                    if (mMixPlayback != null && mMixPlayback.b())
                    {
                        mMixPlayback.d();
                    }
                } else
                if (mPcmPlayback != null && mPcmPlayback.f())
                {
                    mPcmPlayback.b();
                }
                i = getLastPlayPosition(mCurrBg);
                mBgPlayer.a(mCurrBg, i);
                mBgPlayer.g();
                if (mRecording && mHeadSetOn)
                {
                    radiogroup = new n();
                    radiogroup.d = 1;
                    radiogroup.e = mCurrBg;
                    radiogroup.g = i;
                    radiogroup.a = mRecordFile.i();
                    radiogroup.b = mBgVol * mMaxBgVol;
                    radiogroup.c = mBgVol * mMaxBgVol;
                    mOperations.add(radiogroup);
                }
                break;

            case 2: // '\002'
                mBgPlayer.h();
                setLastPlayPosition(mCurrBg, mBgPlayer.d());
                if (mRecording && mHeadSetOn)
                {
                    radiogroup = new n();
                    radiogroup.d = 1;
                    radiogroup.e = null;
                    radiogroup.a = mRecordFile.i();
                    mOperations.add(radiogroup);
                }
                break;

            case 1: // '\001'
            case 3: // '\003'
            case 4: // '\004'
            case 5: // '\005'
                if (mHeadSetOn)
                {
                    if (mMixPlayback != null && mMixPlayback.b())
                    {
                        mMixPlayback.d();
                    }
                } else
                if (mPcmPlayback != null && mPcmPlayback.f())
                {
                    mPcmPlayback.b();
                }
                mBgPlayer.g();
                if (mRecording && mHeadSetOn)
                {
                    radiogroup = new n();
                    radiogroup.d = 1;
                    radiogroup.e = mCurrBg;
                    radiogroup.g = getLastPlayPosition(mCurrBg);
                    radiogroup.a = mRecordFile.i();
                    radiogroup.b = mBgVol * mMaxBgVol;
                    radiogroup.c = mBgVol * mMaxBgVol;
                    mOperations.add(radiogroup);
                }
                break;
            }
            if (true) goto _L2; else goto _L3
_L3:
        }

        public void onCheckChange(RadioGroup radiogroup, int i)
        {
            if (mCheckFromUser) goto _L2; else goto _L1
_L1:
            mCheckFromUser = true;
_L4:
            return;
_L2:
            i = radiogroup.indexOfChild(radiogroup.findViewById(i));
            if (i != 0)
            {
                break; /* Loop/switch isn't completed */
            }
            if (mRecording && mCurrBg != null)
            {
                setLastPlayPosition(mCurrBg, mBgPlayer.d());
                mCurrBg.duration = (float)mBgPlayer.c() / 1000F;
            }
            mBgPlayer.i();
            mCurrBg = null;
            mBgTitle.setText("\u914D\u4E50:\u65E0");
            if (mRecording && mHeadSetOn)
            {
                radiogroup = new n();
                radiogroup.d = 1;
                radiogroup.e = null;
                radiogroup.a = mRecordFile.i();
                mOperations.add(radiogroup);
                return;
            }
            if (true) goto _L4; else goto _L3
_L3:
            radiogroup = (BgSound)mSelectedBg.get(i - 1);
            if (radiogroup != null)
            {
                if (mCurrBg != null && mRecording)
                {
                    setLastPlayPosition(mCurrBg, mBgPlayer.d());
                }
                mCurrBg = radiogroup;
                if (mRecording)
                {
                    mBgPlayer.a(mBgVol * mMaxBgVol, mBgVol * mMaxBgVol);
                    mBgPlayer.a(mCurrBg, getLastPlayPosition(mCurrBg));
                } else
                {
                    mBgPlayer.a(mBgVol, mBgVol);
                    mBgPlayer.a(mCurrBg);
                }
                if (mHeadSetOn)
                {
                    if (mMixPlayback != null && mMixPlayback.b())
                    {
                        mMixPlayback.d();
                    }
                } else
                if (mPcmPlayback != null && mPcmPlayback.f())
                {
                    mPcmPlayback.b();
                }
                mBgPlayer.g();
                mCurrBg.duration = (float)mBgPlayer.c() / 1000F;
                mBgTitle.setText((new StringBuilder()).append("\u914D\u4E50:").append(((BgSound) (radiogroup)).title).toString());
                if (mRecording && mHeadSetOn)
                {
                    radiogroup = new n();
                    radiogroup.d = 1;
                    radiogroup.e = mCurrBg;
                    radiogroup.g = getLastPlayPosition(mCurrBg);
                    radiogroup.b = mBgVol * mMaxBgVol;
                    radiogroup.a = mRecordFile.i();
                    mOperations.add(radiogroup);
                    return;
                }
            }
            if (true) goto _L4; else goto _L5
_L5:
        }

        _cls17()
        {
            this$0 = RecordingFragment.this;
            super();
            mLastCheckId = -1L;
        }
    }



    private class _cls22
        implements android.view.View.OnClickListener
    {

        final RecordingFragment this$0;

        public void onClick(View view)
        {
            if (mCutTask == null || mCutTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
            {
                if (mHeadSetOn)
                {
                    mMixPlayback.d();
                } else
                {
                    mPcmPlayback.b();
                }
                mCutTask = new CutTask(null);
                mCutTask.myexec(new Void[0]);
            }
            mCutConfirm.dismiss();
        }

        _cls22()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls23
        implements android.view.View.OnClickListener
    {

        final RecordingFragment this$0;

        public void onClick(View view)
        {
            if (mHeadSetOn)
            {
                mMixPlayback.a(0L);
                mMixPlayback.b();
            } else
            {
                mPcmPlayback.a(0L);
                mPcmPlayback.f();
            }
            mBtnCut.setVisibility(0);
            view = ToolUtil.toTime((float)mRecordFile.i() / mRecordFile.a());
            mRecCurrTime.setText(view);
            mRecordTime.setText(view);
            mController.startEdit();
            mCutConfirm.dismiss();
        }

        _cls23()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls18
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final RecordingFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
            if (mGuideIndex == mGuideImages.size() - 1 && i == 1)
            {
                hideGuide();
            }
        }

        public void onPageScrolled(int i, float f1, int j)
        {
        }

        public void onPageSelected(int i)
        {
            mGuideIndex = i;
        }

        _cls18()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls19
        implements GuidePageFragment.CloseListener
    {

        final RecordingFragment this$0;

        public void close()
        {
            hideGuide();
        }

        _cls19()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls20
        implements android.view.View.OnClickListener
    {

        final RecordingFragment this$0;

        public void onClick(View view)
        {
            showGuide();
            mMoreMenu.dismiss();
        }

        _cls20()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }


    private class _cls21
        implements android.view.View.OnClickListener
    {

        final RecordingFragment this$0;

        public void onClick(View view)
        {
            startActivity(new Intent(mCon, com/ximalaya/ting/android/activity/setting/UmengFeedbackActivity));
            mMoreMenu.dismiss();
        }

        _cls21()
        {
            this$0 = RecordingFragment.this;
            super();
        }
    }




}
