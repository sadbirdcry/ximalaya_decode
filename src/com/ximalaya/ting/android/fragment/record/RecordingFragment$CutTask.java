// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.record;

import android.app.ProgressDialog;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.library.view.record.WaveformController;
import com.ximalaya.ting.android.library.view.record.WaveformView;
import com.ximalaya.ting.android.transaction.d.k;
import com.ximalaya.ting.android.transaction.d.n;
import com.ximalaya.ting.android.transaction.d.o;
import com.ximalaya.ting.android.transaction.d.s;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.record:
//            RecordingFragment

private class <init> extends MyAsyncTask
{

    ProgressDialog pd;
    final RecordingFragment this$0;

    protected transient Integer doInBackground(Void avoid[])
    {
        if (RecordingFragment.access$5500(RecordingFragment.this) == 1.0F)
        {
            return Integer.valueOf(0);
        }
        long l1;
        l1 = RecordingFragment.access$1400(RecordingFragment.this).i();
        RecordingFragment.access$1400(RecordingFragment.this).a(0L, (long)((float)RecordingFragment.access$1400(RecordingFragment.this).i() * RecordingFragment.access$5500(RecordingFragment.this)));
        synchronized (RecordingFragment.access$3100(RecordingFragment.this))
        {
            int i = (int)((float)RecordingFragment.access$3100(RecordingFragment.this).size() * RecordingFragment.access$5500(RecordingFragment.this));
            ArrayList arraylist = new ArrayList(i);
            arraylist.addAll(RecordingFragment.access$3100(RecordingFragment.this).subList(0, i));
            RecordingFragment.access$3100(RecordingFragment.this).clear();
            RecordingFragment.access$3100(RecordingFragment.this).addAll(arraylist);
        }
        try
        {
            RecordingFragment.access$4800(RecordingFragment.this).cutFinish();
            RecordingFragment.access$5502(RecordingFragment.this, 1.0F);
            RecordingFragment.access$1302(RecordingFragment.this, RecordingFragment.access$1400(RecordingFragment.this).i());
            if (RecordingFragment.access$2500(RecordingFragment.this))
            {
                avoid = RecordingFragment.access$2600(RecordingFragment.this).iterator();
                do
                {
                    if (!avoid.hasNext())
                    {
                        break;
                    }
                    if (((n)avoid.next()).a >= RecordingFragment.access$1300(RecordingFragment.this))
                    {
                        avoid.remove();
                    }
                } while (true);
                break MISSING_BLOCK_LABEL_267;
            }
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            avoid.printStackTrace();
            return Integer.valueOf(0);
        }
          goto _L1
        exception;
        avoid;
        JVM INSTR monitorexit ;
        throw exception;
        if (RecordingFragment.access$2600(RecordingFragment.this).size() > 0 && ((n)RecordingFragment.access$2600(RecordingFragment.this).get(RecordingFragment.access$2600(RecordingFragment.this).size() - 1)).d != 3)
        {
            avoid = new n();
            avoid.d = 3;
            avoid.a = RecordingFragment.access$1400(RecordingFragment.this).i();
            RecordingFragment.access$2600(RecordingFragment.this).add(avoid);
        }
        if (RecordingFragment.access$2900(RecordingFragment.this) != null)
        {
            RecordingFragment.access$2900(RecordingFragment.this).a(0L);
        }
_L5:
        if (RecordingFragment.access$5900(RecordingFragment.this) == null) goto _L3; else goto _L2
_L2:
        int i1;
        int j = (int)((float)((l1 - RecordingFragment.access$1400(RecordingFragment.this).i()) * 1000L) / RecordingFragment.access$1400(RecordingFragment.this).a());
        i1 = getLastPlayPosition(RecordingFragment.access$5900(RecordingFragment.this)) - j;
        int l;
        l = i1;
        if (i1 < 0)
        {
            l = 0;
        }
        setLastPlayPosition(RecordingFragment.access$5900(RecordingFragment.this), l);
_L3:
        avoid = RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(5);
        avoid.arg1 = (int)((float)(RecordingFragment.access$1300(RecordingFragment.this) * 1000L) / RecordingFragment.access$5600(RecordingFragment.this));
        avoid.sendToTarget();
        return Integer.valueOf(1);
_L1:
        if (RecordingFragment.access$3000(RecordingFragment.this) == null) goto _L5; else goto _L4
_L4:
        RecordingFragment.access$3000(RecordingFragment.this).a(0L);
          goto _L5
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        super.onPostExecute(integer);
        pd.cancel();
        RecordingFragment.access$5300(RecordingFragment.this).setVisibility(0);
        RecordingFragment.access$1100(RecordingFragment.this).updatePlayerProgress(0.0F);
        RecordingFragment.access$4800(RecordingFragment.this).startEdit();
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        pd = new MyProgressDialog(RecordingFragment.access$7800(RecordingFragment.this));
        pd.setMessage("\u6B63\u5728\u526A\u5207\uFF0C\u8BF7\u7A0D\u5019...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }

    private I()
    {
        this$0 = RecordingFragment.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
