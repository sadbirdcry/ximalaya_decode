// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.record;

import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.ximalaya.ting.android.model.record.BgSound;
import com.ximalaya.ting.android.transaction.d.f;
import com.ximalaya.ting.android.transaction.d.k;
import com.ximalaya.ting.android.transaction.d.n;
import com.ximalaya.ting.android.transaction.d.o;
import com.ximalaya.ting.android.transaction.d.s;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.record:
//            RecordingFragment

class mLastCheckId
    implements com.ximalaya.ting.android.library.view.record.tatusChangeListener
{

    private long mLastCheckId;
    final RecordingFragment this$0;

    public void onCheck(RadioGroup radiogroup, View view)
    {
_L2:
        mLastCheckId = view.getId();
        return;
        if (view == RecordingFragment.access$6200(RecordingFragment.this) || (long)view.getId() != mLastCheckId || RecordingFragment.access$2400(RecordingFragment.this) == null) goto _L2; else goto _L1
_L1:
        switch (RecordingFragment.access$2400(RecordingFragment.this).f())
        {
        case -1: 
        case 0: // '\0'
            int i;
            if (RecordingFragment.access$2500(RecordingFragment.this))
            {
                if (RecordingFragment.access$2900(RecordingFragment.this) != null && RecordingFragment.access$2900(RecordingFragment.this).b())
                {
                    RecordingFragment.access$2900(RecordingFragment.this).d();
                }
            } else
            if (RecordingFragment.access$3000(RecordingFragment.this) != null && RecordingFragment.access$3000(RecordingFragment.this).f())
            {
                RecordingFragment.access$3000(RecordingFragment.this).b();
            }
            i = getLastPlayPosition(RecordingFragment.access$5900(RecordingFragment.this));
            RecordingFragment.access$2400(RecordingFragment.this).a(RecordingFragment.access$5900(RecordingFragment.this), i);
            RecordingFragment.access$2400(RecordingFragment.this).g();
            if (RecordingFragment.access$1500(RecordingFragment.this) && RecordingFragment.access$2500(RecordingFragment.this))
            {
                radiogroup = new n();
                radiogroup.d = 1;
                radiogroup.e = RecordingFragment.access$5900(RecordingFragment.this);
                radiogroup.g = i;
                radiogroup.a = RecordingFragment.access$1400(RecordingFragment.this).i();
                radiogroup.b = RecordingFragment.access$2200(RecordingFragment.this) * RecordingFragment.access$2300(RecordingFragment.this);
                radiogroup.c = RecordingFragment.access$2200(RecordingFragment.this) * RecordingFragment.access$2300(RecordingFragment.this);
                RecordingFragment.access$2600(RecordingFragment.this).add(radiogroup);
            }
            break;

        case 2: // '\002'
            RecordingFragment.access$2400(RecordingFragment.this).h();
            setLastPlayPosition(RecordingFragment.access$5900(RecordingFragment.this), RecordingFragment.access$2400(RecordingFragment.this).d());
            if (RecordingFragment.access$1500(RecordingFragment.this) && RecordingFragment.access$2500(RecordingFragment.this))
            {
                radiogroup = new n();
                radiogroup.d = 1;
                radiogroup.e = null;
                radiogroup.a = RecordingFragment.access$1400(RecordingFragment.this).i();
                RecordingFragment.access$2600(RecordingFragment.this).add(radiogroup);
            }
            break;

        case 1: // '\001'
        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
            if (RecordingFragment.access$2500(RecordingFragment.this))
            {
                if (RecordingFragment.access$2900(RecordingFragment.this) != null && RecordingFragment.access$2900(RecordingFragment.this).b())
                {
                    RecordingFragment.access$2900(RecordingFragment.this).d();
                }
            } else
            if (RecordingFragment.access$3000(RecordingFragment.this) != null && RecordingFragment.access$3000(RecordingFragment.this).f())
            {
                RecordingFragment.access$3000(RecordingFragment.this).b();
            }
            RecordingFragment.access$2400(RecordingFragment.this).g();
            if (RecordingFragment.access$1500(RecordingFragment.this) && RecordingFragment.access$2500(RecordingFragment.this))
            {
                radiogroup = new n();
                radiogroup.d = 1;
                radiogroup.e = RecordingFragment.access$5900(RecordingFragment.this);
                radiogroup.g = getLastPlayPosition(RecordingFragment.access$5900(RecordingFragment.this));
                radiogroup.a = RecordingFragment.access$1400(RecordingFragment.this).i();
                radiogroup.b = RecordingFragment.access$2200(RecordingFragment.this) * RecordingFragment.access$2300(RecordingFragment.this);
                radiogroup.c = RecordingFragment.access$2200(RecordingFragment.this) * RecordingFragment.access$2300(RecordingFragment.this);
                RecordingFragment.access$2600(RecordingFragment.this).add(radiogroup);
            }
            break;
        }
        if (true) goto _L2; else goto _L3
_L3:
    }

    public void onCheckChange(RadioGroup radiogroup, int i)
    {
        if (RecordingFragment.access$5800(RecordingFragment.this)) goto _L2; else goto _L1
_L1:
        RecordingFragment.access$5802(RecordingFragment.this, true);
_L4:
        return;
_L2:
        i = radiogroup.indexOfChild(radiogroup.findViewById(i));
        if (i != 0)
        {
            break; /* Loop/switch isn't completed */
        }
        if (RecordingFragment.access$1500(RecordingFragment.this) && RecordingFragment.access$5900(RecordingFragment.this) != null)
        {
            setLastPlayPosition(RecordingFragment.access$5900(RecordingFragment.this), RecordingFragment.access$2400(RecordingFragment.this).d());
            RecordingFragment.access$5900(RecordingFragment.this).duration = (float)RecordingFragment.access$2400(RecordingFragment.this).c() / 1000F;
        }
        RecordingFragment.access$2400(RecordingFragment.this).i();
        RecordingFragment.access$5902(RecordingFragment.this, null);
        RecordingFragment.access$6000(RecordingFragment.this).setText("\u914D\u4E50:\u65E0");
        if (RecordingFragment.access$1500(RecordingFragment.this) && RecordingFragment.access$2500(RecordingFragment.this))
        {
            radiogroup = new n();
            radiogroup.d = 1;
            radiogroup.e = null;
            radiogroup.a = RecordingFragment.access$1400(RecordingFragment.this).i();
            RecordingFragment.access$2600(RecordingFragment.this).add(radiogroup);
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        radiogroup = (BgSound)RecordingFragment.access$6100(RecordingFragment.this).get(i - 1);
        if (radiogroup != null)
        {
            if (RecordingFragment.access$5900(RecordingFragment.this) != null && RecordingFragment.access$1500(RecordingFragment.this))
            {
                setLastPlayPosition(RecordingFragment.access$5900(RecordingFragment.this), RecordingFragment.access$2400(RecordingFragment.this).d());
            }
            RecordingFragment.access$5902(RecordingFragment.this, radiogroup);
            if (RecordingFragment.access$1500(RecordingFragment.this))
            {
                RecordingFragment.access$2400(RecordingFragment.this).a(RecordingFragment.access$2200(RecordingFragment.this) * RecordingFragment.access$2300(RecordingFragment.this), RecordingFragment.access$2200(RecordingFragment.this) * RecordingFragment.access$2300(RecordingFragment.this));
                RecordingFragment.access$2400(RecordingFragment.this).a(RecordingFragment.access$5900(RecordingFragment.this), getLastPlayPosition(RecordingFragment.access$5900(RecordingFragment.this)));
            } else
            {
                RecordingFragment.access$2400(RecordingFragment.this).a(RecordingFragment.access$2200(RecordingFragment.this), RecordingFragment.access$2200(RecordingFragment.this));
                RecordingFragment.access$2400(RecordingFragment.this).a(RecordingFragment.access$5900(RecordingFragment.this));
            }
            if (RecordingFragment.access$2500(RecordingFragment.this))
            {
                if (RecordingFragment.access$2900(RecordingFragment.this) != null && RecordingFragment.access$2900(RecordingFragment.this).b())
                {
                    RecordingFragment.access$2900(RecordingFragment.this).d();
                }
            } else
            if (RecordingFragment.access$3000(RecordingFragment.this) != null && RecordingFragment.access$3000(RecordingFragment.this).f())
            {
                RecordingFragment.access$3000(RecordingFragment.this).b();
            }
            RecordingFragment.access$2400(RecordingFragment.this).g();
            RecordingFragment.access$5900(RecordingFragment.this).duration = (float)RecordingFragment.access$2400(RecordingFragment.this).c() / 1000F;
            RecordingFragment.access$6000(RecordingFragment.this).setText((new StringBuilder()).append("\u914D\u4E50:").append(((BgSound) (radiogroup)).title).toString());
            if (RecordingFragment.access$1500(RecordingFragment.this) && RecordingFragment.access$2500(RecordingFragment.this))
            {
                radiogroup = new n();
                radiogroup.d = 1;
                radiogroup.e = RecordingFragment.access$5900(RecordingFragment.this);
                radiogroup.g = getLastPlayPosition(RecordingFragment.access$5900(RecordingFragment.this));
                radiogroup.b = RecordingFragment.access$2200(RecordingFragment.this) * RecordingFragment.access$2300(RecordingFragment.this);
                radiogroup.a = RecordingFragment.access$1400(RecordingFragment.this).i();
                RecordingFragment.access$2600(RecordingFragment.this).add(radiogroup);
                return;
            }
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    ildStatusChangeListener()
    {
        this$0 = RecordingFragment.this;
        super();
        mLastCheckId = -1L;
    }
}
