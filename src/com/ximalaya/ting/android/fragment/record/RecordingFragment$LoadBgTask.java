// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.record;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.widget.RadioButton;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.record.MyRadioGroup;
import com.ximalaya.ting.android.model.record.BgSound;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.record:
//            RecordingFragment

private class <init> extends MyAsyncTask
{

    final RecordingFragment this$0;

    protected transient Integer doInBackground(String as[])
    {
        as = RecordingFragment.access$4200(RecordingFragment.this).getString("BG_SOUND_CACHE");
        if (TextUtils.isEmpty(as))
        {
            break MISSING_BLOCK_LABEL_57;
        }
        as = JSON.parseArray(as, com/ximalaya/ting/android/model/record/BgSound);
        if (as == null)
        {
            break MISSING_BLOCK_LABEL_57;
        }
        RecordingFragment.access$6100(RecordingFragment.this).clear();
        RecordingFragment.access$6100(RecordingFragment.this).addAll(as);
        if (RecordingFragment.access$6100(RecordingFragment.this).size() != 0)
        {
            break MISSING_BLOCK_LABEL_98;
        }
        as = RecordingFragment.access$7900(RecordingFragment.this);
        if (as != null)
        {
            try
            {
                RecordingFragment.access$6100(RecordingFragment.this).addAll(as);
            }
            // Misplaced declaration of an exception variable
            catch (String as[])
            {
                as.printStackTrace();
                return Integer.valueOf(-1);
            }
        }
        return Integer.valueOf(0);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((String[])aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        super.onPostExecute(integer);
        if (RecordingFragment.access$000(RecordingFragment.this).isAdded() && !RecordingFragment.access$8000(RecordingFragment.this).isFinishing()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        RecordingFragment.access$8100(RecordingFragment.this).removeAllViews();
        RecordingFragment.access$6202(RecordingFragment.this, (RadioButton)LayoutInflater.from(mCon).inflate(0x7f0301b9, RecordingFragment.access$8100(RecordingFragment.this), false));
        RecordingFragment.access$6200(RecordingFragment.this).setText("\u65E0\u80CC\u666F");
        RecordingFragment.access$8100(RecordingFragment.this).addView(RecordingFragment.access$6200(RecordingFragment.this));
        if (RecordingFragment.access$5900(RecordingFragment.this) == null || !RecordingFragment.access$6100(RecordingFragment.this).contains(RecordingFragment.access$5900(RecordingFragment.this)))
        {
            RecordingFragment.access$8100(RecordingFragment.this).check(RecordingFragment.access$6200(RecordingFragment.this).getId());
        }
        if (RecordingFragment.access$6100(RecordingFragment.this).size() != 0)
        {
            int i = 0;
            while (i < RecordingFragment.access$6100(RecordingFragment.this).size()) 
            {
                BgSound bgsound = (BgSound)RecordingFragment.access$6100(RecordingFragment.this).get(i);
                RadioButton radiobutton = (RadioButton)LayoutInflater.from(mCon).inflate(0x7f0301b9, RecordingFragment.access$8100(RecordingFragment.this), false);
                if (TextUtils.isEmpty(bgsound.label) && RecordingFragment.access$8200().containsKey(bgsound.title))
                {
                    bgsound.label = (String)RecordingFragment.access$8200().get(bgsound.title);
                }
                if (TextUtils.isEmpty(bgsound.label))
                {
                    integer = bgsound.title;
                } else
                {
                    integer = bgsound.label;
                }
                radiobutton.setText(integer);
                RecordingFragment.access$8100(RecordingFragment.this).addView(radiobutton);
                if (bgsound.equals(RecordingFragment.access$5900(RecordingFragment.this)))
                {
                    RecordingFragment.access$5802(RecordingFragment.this, false);
                    RecordingFragment.access$8100(RecordingFragment.this).check(radiobutton.getId());
                }
                i++;
            }
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
    }

    private ()
    {
        this$0 = RecordingFragment.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
