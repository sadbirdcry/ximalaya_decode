// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.record;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.view.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GuidePageFragment extends BaseFragment
{
    public static interface CloseListener
    {

        public abstract void close();
    }

    private static class ViewPagerAdapter extends PagerAdapter
    {

        private ArrayList mChildViews;
        private Context mContext;
        private List mImages;
        private ViewPager mViewPager;

        public void destory()
        {
            for (Iterator iterator = mChildViews.iterator(); iterator.hasNext(); ((View)iterator.next()).setOnClickListener(null)) { }
            mViewPager = null;
        }

        public void destroyItem(ViewGroup viewgroup, int i, Object obj)
        {
            obj = (ImageView)mChildViews.get(i);
            ((ImageView) (obj)).setImageResource(0);
            viewgroup.removeView(((View) (obj)));
        }

        public int getCount()
        {
            return mChildViews.size();
        }

        public Object instantiateItem(ViewGroup viewgroup, int i)
        {
            ImageView imageview = (ImageView)mChildViews.get(i);
            imageview.setImageResource(((Integer)mImages.get(i)).intValue());
            viewgroup.addView(imageview);
            return imageview;
        }

        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }

        public void notifyDataSetChanged()
        {
            mChildViews.clear();
            class _cls1
                implements android.view.View.OnClickListener
            {

                final ViewPagerAdapter this$0;

                public void onClick(View view)
                {
                    int i = mViewPager.getCurrentItem() + 1;
                    if (i <= mImages.size() - 1)
                    {
                        mViewPager.setCurrentItem(i);
                    }
                }

                _cls1()
                {
                    this$0 = ViewPagerAdapter.this;
                    super();
                }
            }

            ImageView imageview;
            for (Iterator iterator = mImages.iterator(); iterator.hasNext(); imageview.setOnClickListener(new _cls1()))
            {
                ((Integer)iterator.next()).intValue();
                imageview = new ImageView(mContext);
                imageview.setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
                mChildViews.add(imageview);
            }

            super.notifyDataSetChanged();
        }



        public ViewPagerAdapter(ViewPager viewpager, List list)
        {
            mViewPager = viewpager;
            mContext = viewpager.getContext();
            mChildViews = new ArrayList();
            mImages = list;
        }
    }


    private ViewPagerAdapter mAdapter;
    private boolean mCanCacel;
    private ImageView mClose;
    private CloseListener mCloseListener;
    private List mImages;
    private CirclePageIndicator mIndicator;
    private android.support.v4.view.ViewPager.OnPageChangeListener mPagerListener;
    private boolean mShow;
    private ViewPager mViewPager;

    public GuidePageFragment()
    {
        mShow = false;
        mCanCacel = false;
        mImages = new ArrayList();
    }

    public boolean isShowing()
    {
        return mShow;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mAdapter = new ViewPagerAdapter(mViewPager, mImages);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setAdapter(mAdapter);
        mIndicator.setViewPager(mViewPager);
        mIndicator.setCentered(true);
        mClose.setOnClickListener(new _cls1());
        mAdapter.notifyDataSetChanged();
        if (mPagerListener != null)
        {
            mIndicator.setOnPageChangeListener(mPagerListener);
        }
        mShow = true;
    }

    public boolean onBackPressed()
    {
        return true;
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f03009a, viewgroup, false);
        mViewPager = (ViewPager)layoutinflater.findViewById(0x7f0a02c4);
        mIndicator = (CirclePageIndicator)layoutinflater.findViewById(0x7f0a02c5);
        mIndicator.setFillColor(Color.parseColor("#ffffff"));
        mIndicator.setPageColor(Color.parseColor("#7fffffff"));
        mClose = (ImageView)layoutinflater.findViewById(0x7f0a013d);
        mClose.setVisibility(0);
        return layoutinflater;
    }

    public void onDestroyView()
    {
        mShow = false;
        mIndicator.setOnPageChangeListener(null);
        mAdapter.destory();
        super.onDestroyView();
    }

    public void onResume()
    {
        super.onResume();
        mViewPager.setCurrentItem(0);
    }

    public void removePagerChangeListener(android.support.v4.view.ViewPager.OnPageChangeListener onpagechangelistener)
    {
        if (mIndicator != null)
        {
            mIndicator.setOnPageChangeListener(null);
        }
    }

    public void setCloseListener(CloseListener closelistener)
    {
        mCloseListener = closelistener;
    }

    public void setImages(List list)
    {
        mImages.clear();
        if (list != null)
        {
            mImages.addAll(list);
        }
        if (mAdapter != null)
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void setOnPagerChangeListener(android.support.v4.view.ViewPager.OnPageChangeListener onpagechangelistener)
    {
        mPagerListener = onpagechangelistener;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final GuidePageFragment this$0;

        public void onClick(View view)
        {
            if (mCloseListener != null)
            {
                mCloseListener.close();
                return;
            }
            try
            {
                view = getFragmentManager().beginTransaction();
                view.remove(GuidePageFragment.this);
                view.commitAllowingStateLoss();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (View view)
            {
                view.printStackTrace();
            }
        }

        _cls1()
        {
            this$0 = GuidePageFragment.this;
            super();
        }
    }

}
