// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.record;

import android.os.Handler;
import android.os.Message;
import com.ximalaya.ting.android.library.view.record.WaveformView;
import com.ximalaya.ting.android.transaction.d.s;

// Referenced classes of package com.ximalaya.ting.android.fragment.record:
//            RecordingFragment

class this._cls0
    implements com.ximalaya.ting.android.transaction.d._cls2
{

    final RecordingFragment this$0;

    public void onPlayerPaused()
    {
        RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(4).sendToTarget();
    }

    public void onPlayerResume()
    {
        RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(3).sendToTarget();
    }

    public void onPlayerStart()
    {
        RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(3).sendToTarget();
    }

    public void onPlayerStopped()
    {
        RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(4).sendToTarget();
    }

    public void onProgressUpdate(float f)
    {
        RecordingFragment.access$1100(RecordingFragment.this).updatePlayerProgress(f);
        Message message = RecordingFragment.access$1200(RecordingFragment.this).obtainMessage(9);
        message.arg1 = (int)(((float)(1000L * RecordingFragment.access$1300(RecordingFragment.this)) * f) / RecordingFragment.access$1400(RecordingFragment.this).a());
        message.sendToTarget();
    }

    ()
    {
        this$0 = RecordingFragment.this;
        super();
    }
}
