// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.RewardModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            ScrollTabHolderListFragment, SoundDetailAdapter

public class PlayingSoundDetailFragment extends ScrollTabHolderListFragment
{
    public class PayFinishReceiver extends BroadcastReceiver
    {

        final PlayingSoundDetailFragment this$0;

        public void onReceive(Context context, Intent intent)
        {
            if ("com.ximalaya.ting.android.pay.ACTION_PAY_FINISH".equals(intent.getAction()))
            {
                context = intent.getStringExtra("track_id");
                if (String.valueOf(mTrackId).equals(context) && UserInfoMannage.hasLogined())
                {
                    context = new com.ximalaya.ting.android.model.RewardModel.RewardItemModel();
                    context.setAmount(intent.getStringExtra("amount"));
                    context.setMark(intent.getStringExtra("mark"));
                    context.setNickname(UserInfoMannage.getInstance().getUser().getNickname());
                    context.setSmallLogo(UserInfoMannage.getInstance().getUser().getSmallLogo());
                    context.setUid(UserInfoMannage.getInstance().getUser().getUid().longValue());
                    insertRewardData(context);
                }
            }
        }

        public PayFinishReceiver()
        {
            this$0 = PlayingSoundDetailFragment.this;
            super();
        }
    }

    private class RewardInfoLoader extends MyAsyncTask
    {

        private long mTrackId;
        final PlayingSoundDetailFragment this$0;

        protected transient RewardModel doInBackground(Void avoid[])
        {
            return RewardModel.getInstanceFromRemote(mTrackId);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(RewardModel rewardmodel)
        {
            super.onPostExecute(rewardmodel);
            if (getActivity() == null || !isAdded() || isRemoving())
            {
                return;
            }
            if (rewardmodel != null)
            {
                setupRewardView(rewardmodel);
                return;
            } else
            {
                mRewardView.setVisibility(8);
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((RewardModel)obj);
        }

        public RewardInfoLoader(long l)
        {
            this$0 = PlayingSoundDetailFragment.this;
            super();
            mTrackId = l;
        }
    }


    private static final String HIGHLIGHT_PAYER = "\u571F\u8C6A";
    private static final int MAX_NUM_OF_REWARDS = 6;
    private SoundDetailAdapter mAdapter;
    private long mAlbumId;
    private TextView mEmptyView;
    private long mPayeeId;
    private PayFinishReceiver mReceiver;
    private ImageButton mRewardBtn;
    private RewardModel mRewardModel;
    private TextView mRewardNum;
    private View mRewardView;
    private LinearLayout mRewardedContainer;
    private long mTrackId;

    public PlayingSoundDetailFragment()
    {
    }

    public static PlayingSoundDetailFragment getInstance(int i, long l, long l1, long l2)
    {
        PlayingSoundDetailFragment playingsounddetailfragment = new PlayingSoundDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", i);
        bundle.putLong("track_id", l);
        bundle.putLong("payee_id", l1);
        bundle.putLong("album_id", l2);
        playingsounddetailfragment.setArguments(bundle);
        return playingsounddetailfragment;
    }

    private void insertRewardData(com.ximalaya.ting.android.model.RewardModel.RewardItemModel rewarditemmodel)
    {
        if (mRewardModel != null)
        {
            if (mRewardModel.getRewords() != null)
            {
                mRewardModel.getRewords().add(0, rewarditemmodel);
            } else
            {
                mRewardModel.setRewords(new ArrayList());
                mRewardModel.getRewords().add(rewarditemmodel);
            }
            setupRewardView(mRewardModel);
        }
    }

    private void loadRewardData(long l)
    {
        (new RewardInfoLoader(l)).myexec(new Void[0]);
    }

    private void setupRewardView(RewardModel rewardmodel)
    {
        if (!rewardmodel.getIsOpen())
        {
            mRewardView.setVisibility(8);
            return;
        }
        mRewardModel = rewardmodel;
        mRewardView.setVisibility(0);
        mRewardNum.setText((new StringBuilder()).append(rewardmodel.getNumOfRewards()).append("\u4EBA\u6253\u8D4F").toString());
        mRewardedContainer.removeAllViews();
        if (rewardmodel.getRewords() != null)
        {
            mRewardedContainer.setVisibility(0);
            mRewardedContainer.setOnClickListener(new _cls1());
            int i = 0;
            while (i < 6 && i < rewardmodel.getRewords().size()) 
            {
                Object obj = (com.ximalaya.ting.android.model.RewardModel.RewardItemModel)rewardmodel.getRewords().get(i);
                ViewGroup viewgroup = (ViewGroup)View.inflate(getActivity(), 0x7f0301c0, null);
                RoundedImageView roundedimageview = (RoundedImageView)viewgroup.findViewById(0x7f0a06b7);
                TextView textview = (TextView)viewgroup.findViewById(0x7f0a06b8);
                if ("\u571F\u8C6A".equals(((com.ximalaya.ting.android.model.RewardModel.RewardItemModel) (obj)).getMark()))
                {
                    textview.setTextColor(Color.parseColor("#ff9249"));
                } else
                {
                    textview.setTextColor(Color.parseColor("#aaaaaa"));
                }
                textview.setText((new StringBuilder()).append("\uFFE5").append(((com.ximalaya.ting.android.model.RewardModel.RewardItemModel) (obj)).getAmount()).toString());
                ImageManager2.from(getActivity()).displayImage(roundedimageview, ((com.ximalaya.ting.android.model.RewardModel.RewardItemModel) (obj)).getSmallLogo(), 0x7f0202df);
                obj = new android.widget.LinearLayout.LayoutParams(-2, -2);
                if (i < Math.min(6, rewardmodel.getRewords().size()) - 1)
                {
                    obj.rightMargin = Utilities.dip2px(getActivity(), 15F);
                }
                mRewardedContainer.addView(viewgroup, ((android.view.ViewGroup.LayoutParams) (obj)));
                i++;
            }
        } else
        {
            mRewardedContainer.setVisibility(8);
        }
        mRewardBtn.setOnClickListener(new _cls2());
    }

    public void clear()
    {
        mAdapter.setModel(null);
        android.widget.AbsListView.LayoutParams layoutparams = new android.widget.AbsListView.LayoutParams(-1, getActivity().getResources().getDisplayMetrics().heightPixels - Utilities.dip2px(getActivity(), 108F));
        mEmptyView.setLayoutParams(layoutparams);
        mEmptyView.setVisibility(0);
        mRewardView.setVisibility(8);
    }

    public View getRewardView()
    {
        return mRewardView;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mRewardView = View.inflate(getActivity(), 0x7f0301bf, null);
        mRewardBtn = (ImageButton)mRewardView.findViewById(0x7f0a06b4);
        mRewardNum = (TextView)mRewardView.findViewById(0x7f0a06b5);
        mRewardedContainer = (LinearLayout)mRewardView.findViewById(0x7f0a06b6);
        mListView.addFooterView(mRewardView);
        mEmptyView = new TextView(getActivity());
        mEmptyView.setText(0x7f0901d0);
        mEmptyView.setGravity(17);
        mEmptyView.setVisibility(8);
        bundle = new android.widget.AbsListView.LayoutParams(-1, getActivity().getResources().getDisplayMetrics().heightPixels - Utilities.dip2px(getActivity(), 108F));
        mEmptyView.setLayoutParams(bundle);
        mListView.addFooterView(mEmptyView);
        mAdapter = new SoundDetailAdapter(this, null);
        mListView.setAdapter(mAdapter);
        if (mReceiver == null)
        {
            mReceiver = new PayFinishReceiver();
            bundle = new IntentFilter("com.ximalaya.ting.android.pay.ACTION_PAY_FINISH");
            getActivity().registerReceiver(mReceiver, bundle);
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        if (getArguments() == null)
        {
            mTrackId = getArguments().getLong("track_id");
            mPayeeId = getArguments().getLong("payee_id");
            mAlbumId = getArguments().getLong("album_id");
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        return layoutinflater.inflate(0x7f0300bf, viewgroup, false);
    }

    public void onDetach()
    {
        super.onDetach();
        if (mReceiver != null)
        {
            getActivity().unregisterReceiver(mReceiver);
            mReceiver = null;
        }
    }

    public void setData(SoundDetailAdapter.Model model, long l, long l1, long l2)
    {
        mAdapter.setModel(model);
        mTrackId = l;
        mPayeeId = l1;
        mAlbumId = l2;
        loadRewardData(mTrackId);
        model = new android.widget.AbsListView.LayoutParams(-1, 0);
        mEmptyView.setLayoutParams(model);
        mEmptyView.setVisibility(8);
    }







    private class _cls1
        implements android.view.View.OnClickListener
    {

        final PlayingSoundDetailFragment this$0;

        public void onClick(View view)
        {
            view = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
            view.putExtra("ExtraUrl", (new StringBuilder()).append(a.U).append("ting-shang-mobile-web/v1/rewardOrders?trackId=").append(mTrackId).toString());
            startActivity(view);
        }

        _cls1()
        {
            this$0 = PlayingSoundDetailFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final PlayingSoundDetailFragment this$0;

        public void onClick(View view)
        {
            DataCollectUtil.getInstance(getActivity().getApplicationContext()).statDashang("click_dashang", mPayeeId, mTrackId, mAlbumId);
            if (UserInfoMannage.hasLogined())
            {
                long l = UserInfoMannage.getInstance().getUser().uid;
                view = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
                view.putExtra("ExtraUrl", (new StringBuilder()).append(a.U).append("ting-shang-mobile-web/1/user/rewardSelect?payerId=").append(l).append("&").append("payeeId=").append(mPayeeId).append("&trackId=").append(mTrackId).toString());
                startActivity(view);
                return;
            } else
            {
                view = new Intent(getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity);
                getActivity().startActivity(view);
                return;
            }
        }

        _cls2()
        {
            this$0 = PlayingSoundDetailFragment.this;
            super();
        }
    }

}
