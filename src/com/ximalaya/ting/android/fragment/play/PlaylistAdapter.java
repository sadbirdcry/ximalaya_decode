// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.BaseListSoundsAdapter;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

public class PlaylistAdapter extends BaseListSoundsAdapter
{
    class PlaylistViewHolder
    {

        ImageButton downloadBtn;
        ImageView newItemFlag;
        View playingFlag;
        final PlaylistAdapter this$0;
        TextView title;

        PlaylistViewHolder()
        {
            this$0 = PlaylistAdapter.this;
            super();
        }
    }


    private Context mContext;
    private LayoutInflater mInflater;

    public PlaylistAdapter(Activity activity, List list)
    {
        super(activity, list);
        mInflater = LayoutInflater.from(activity);
        mContext = activity;
    }

    protected void bindData(SoundInfo soundinfo, com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder viewholder)
    {
    }

    protected volatile void bindData(Object obj, com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((SoundInfo)obj, viewholder);
    }

    public View getView(int i, View view, final ViewGroup holder)
    {
        final SoundInfo info;
        if (view == null)
        {
            PlaylistViewHolder playlistviewholder = new PlaylistViewHolder();
            view = mInflater.inflate(0x7f030150, holder, false);
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ToolUtil.dp2px(mContext, 48F)));
            playlistviewholder.title = (TextView)view.findViewById(0x7f0a00e6);
            playlistviewholder.downloadBtn = (ImageButton)view.findViewById(0x7f0a02d1);
            playlistviewholder.playingFlag = view.findViewById(0x7f0a0510);
            playlistviewholder.newItemFlag = (ImageView)view.findViewById(0x7f0a0511);
            view.setTag(playlistviewholder);
            holder = playlistviewholder;
        } else
        {
            holder = (PlaylistViewHolder)view.getTag();
        }
        info = (SoundInfo)mData.get(i);
        if (isPlaying(info.trackId))
        {
            ((PlaylistViewHolder) (holder)).title.setTextColor(mContext.getResources().getColor(0x7f07007c));
            ((PlaylistViewHolder) (holder)).playingFlag.setVisibility(0);
        } else
        {
            ((PlaylistViewHolder) (holder)).title.setTextColor(mContext.getResources().getColor(0x7f07007b));
            ((PlaylistViewHolder) (holder)).playingFlag.setVisibility(4);
        }
        ((PlaylistViewHolder) (holder)).title.setText(info.title);
        if (DownloadHandler.getInstance(mContext).isDownloadCompleted(info) || DownloadHandler.getInstance(mContext).isDownloading(info))
        {
            ((PlaylistViewHolder) (holder)).downloadBtn.setImageResource(0x7f0200f1);
            ((PlaylistViewHolder) (holder)).downloadBtn.setClickable(false);
            ((PlaylistViewHolder) (holder)).downloadBtn.setEnabled(false);
        } else
        {
            ((PlaylistViewHolder) (holder)).downloadBtn.setImageResource(0x7f0201fe);
            ((PlaylistViewHolder) (holder)).downloadBtn.setClickable(true);
            ((PlaylistViewHolder) (holder)).downloadBtn.setEnabled(true);
        }
        ((PlaylistViewHolder) (holder)).downloadBtn.setOnClickListener(new _cls1());
        return view;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final PlaylistAdapter this$0;
        final PlaylistViewHolder val$holder;
        final SoundInfo val$info;

        public void onClick(View view)
        {
            Object obj = new DownloadTask(info);
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            obj = downloadtools.goDownload(((DownloadTask) (obj)), mContext.getApplicationContext(), view);
            downloadtools.release();
            if (obj != null && ((com.ximalaya.ting.android.a.a.a) (obj)).b() == 1)
            {
                if (view instanceof ImageView)
                {
                    ((ImageView)view).setImageResource(0x7f0200f1);
                }
                info.setIsNew(false);
                holder.newItemFlag.setVisibility(4);
            }
        }

        _cls1()
        {
            this$0 = PlaylistAdapter.this;
            info = soundinfo;
            holder = playlistviewholder;
            super();
        }
    }

}
