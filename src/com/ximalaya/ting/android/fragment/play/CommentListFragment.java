// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.ximalaya.ting.android.model.comment.CommentModel;
import com.ximalaya.ting.android.model.comment.CommentModelList;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            ScrollTabHolderListFragment, SoundCommentsAdapter

public class CommentListFragment extends ScrollTabHolderListFragment
{
    public static interface OnActivityCreateCallback
    {

        public abstract void onActivityCreate();
    }


    private OnActivityCreateCallback mCallback;
    private SoundCommentsAdapter mCommentAdapter;
    private CommentModelList mCommentModelList;
    private List mComments;
    private View mFooterView;
    private android.widget.AdapterView.OnItemClickListener mOnItemClickListener;

    public CommentListFragment()
    {
    }

    private void ensureListViewEnoughtHeight()
    {
        if (getActivity() != null && !getActivity().isFinishing())
        {
            int i = mCommentAdapter.getCount();
            if (i < 7 && i > 0)
            {
                mFooterView.setVisibility(0);
                android.widget.AbsListView.LayoutParams layoutparams = new android.widget.AbsListView.LayoutParams(-1, (7 - i) * Utilities.dip2px(getActivity(), 67F));
                mFooterView.setLayoutParams(layoutparams);
            }
        }
    }

    public static CommentListFragment getInstance(int i)
    {
        CommentListFragment commentlistfragment = new CommentListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", i);
        commentlistfragment.setArguments(bundle);
        return commentlistfragment;
    }

    public void addData(CommentModel commentmodel)
    {
        mCommentAdapter.addData(commentmodel);
        mListView.post(new _cls3());
    }

    public void addData(List list)
    {
        mCommentAdapter.addData(list);
        mListView.post(new _cls2());
    }

    public ListView getListView()
    {
        return mListView;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        if (mCallback != null)
        {
            mCallback.onActivityCreate();
        }
        mCommentAdapter = new SoundCommentsAdapter(getActivity(), this, mComments, mCommentModelList);
        mFooterView = View.inflate(getActivity(), 0x7f0301fa, null);
        mFooterView.setVisibility(8);
        mListView.addFooterView(mFooterView, null, false);
        mListView.setAdapter(mCommentAdapter);
        mListView.setOnItemClickListener(new _cls1());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        return layoutinflater.inflate(0x7f0300bf, viewgroup, false);
    }

    public void removeData(CommentModel commentmodel)
    {
        mCommentAdapter.removeData(commentmodel);
    }

    public void setData(CommentModelList commentmodellist)
    {
        mCommentAdapter.setCommentList(commentmodellist);
    }

    public void setOnActivityCreateCallback(OnActivityCreateCallback onactivitycreatecallback)
    {
        mCallback = onactivitycreatecallback;
    }

    public void setOnItemClickListener(android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        mOnItemClickListener = onitemclicklistener;
    }



    private class _cls3
        implements Runnable
    {

        final CommentListFragment this$0;

        public void run()
        {
            ensureListViewEnoughtHeight();
        }

        _cls3()
        {
            this$0 = CommentListFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final CommentListFragment this$0;

        public void run()
        {
            ensureListViewEnoughtHeight();
        }

        _cls2()
        {
            this$0 = CommentListFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CommentListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (mOnItemClickListener != null)
            {
                mOnItemClickListener.onItemClick(adapterview, view, i, l);
            }
        }

        _cls1()
        {
            this$0 = CommentListFragment.this;
            super();
        }
    }

}
