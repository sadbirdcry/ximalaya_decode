// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import com.ximalaya.ting.android.model.sound.SoundInfoDetail;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayingSoundInfo

public class PlayingSoundInfoLoader extends AsyncTaskLoader
{

    private PlayingSoundInfo mData;
    private SoundInfoDetail mInfo;

    public PlayingSoundInfoLoader(Context context, SoundInfoDetail soundinfodetail)
    {
        super(context);
        mInfo = soundinfodetail;
    }

    public void deliverResult(PlayingSoundInfo playingsoundinfo)
    {
        super.deliverResult(playingsoundinfo);
        mData = playingsoundinfo;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((PlayingSoundInfo)obj);
    }

    public PlayingSoundInfo loadInBackground()
    {
        mData = PlayingSoundInfo.getFromRemote(mInfo);
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
