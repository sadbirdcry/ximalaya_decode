// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.library.view.flowlayout.FlowLayout;
import com.ximalaya.ting.android.util.Utilities;

public class SoundDetailAdapter extends BaseAdapter
{
    static class Model
    {

        public long activityId;
        public String activityName;
        public String intro;
        public String lrc;
        public ImageButton rewardBtn;
        public LinearLayout rewardContainer;
        public TextView rewardNum;
        public String richIntro;
        public String tags;

        Model()
        {
        }
    }


    private BaseFragment mBaseFragment;
    private Context mContext;
    private android.widget.AbsListView.LayoutParams mLayoutParams;
    private Model mModel;

    public SoundDetailAdapter(BaseFragment basefragment, Model model)
    {
        mBaseFragment = basefragment;
        mContext = mBaseFragment.getActivity();
        mModel = model;
        mLayoutParams = new android.widget.AbsListView.LayoutParams(-1, mContext.getResources().getDisplayMetrics().heightPixels - Utilities.dip2px(mContext, 107F));
    }

    public int getCount()
    {
        return mModel != null ? 1 : 0;
    }

    public Object getItem(int i)
    {
        return Integer.valueOf(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        view = View.inflate(mContext, 0x7f030112, null);
        TextView textview = (TextView)view.findViewById(0x7f0a0466);
        viewgroup = (FlowLayout)view.findViewById(0x7f0a0468);
        if (mModel == null)
        {
            textview.setText("");
            ((TextView)view.findViewById(0x7f0a046c)).setText("");
            viewgroup.removeAllViews();
            ((TextView)view.findViewById(0x7f0a046a)).setText("");
            return view;
        }
        if (!TextUtils.isEmpty(mModel.activityName))
        {
            view.findViewById(0x7f0a0464).setVisibility(0);
            textview.setOnClickListener(new _cls1());
            textview.setText((new StringBuilder()).append("#").append(mModel.activityName).append("#").toString());
        } else
        {
            ((TextView)view.findViewById(0x7f0a0466)).setVisibility(8);
            view.findViewById(0x7f0a0464).setVisibility(8);
        }
        if (!TextUtils.isEmpty(mModel.lrc))
        {
            view.findViewById(0x7f0a046b).setVisibility(0);
            ((TextView)view.findViewById(0x7f0a046c)).setText(Html.fromHtml(mModel.lrc));
        } else
        {
            view.findViewById(0x7f0a046b).setVisibility(8);
        }
        if (!TextUtils.isEmpty(mModel.tags))
        {
            view.findViewById(0x7f0a0467).setVisibility(0);
            String as[] = mModel.tags.split(",");
            int j = as.length;
            for (i = 0; i < j; i++)
            {
                final String tag = as[i];
                TextView textview1 = new TextView(mContext);
                textview1.setBackgroundResource(0x7f020581);
                textview1.setTextSize(13F);
                textview1.setTextColor(Color.parseColor("#999999"));
                textview1.setText(tag);
                com.ximalaya.ting.android.library.view.flowlayout.FlowLayout.LayoutParams layoutparams = new com.ximalaya.ting.android.library.view.flowlayout.FlowLayout.LayoutParams(-2, -2);
                layoutparams.leftMargin = Utilities.dip2px(mContext, 10F);
                layoutparams.bottomMargin = Utilities.dip2px(mContext, 10F);
                viewgroup.addView(textview1, layoutparams);
                textview1.setOnClickListener(new _cls2());
            }

        } else
        {
            view.findViewById(0x7f0a0467).setVisibility(8);
        }
        if (!TextUtils.isEmpty(mModel.richIntro))
        {
            ((TextView)view.findViewById(0x7f0a046a)).setText(Html.fromHtml(mModel.richIntro));
            view.findViewById(0x7f0a0469).setVisibility(0);
            return view;
        }
        if (!TextUtils.isEmpty(mModel.intro))
        {
            ((TextView)view.findViewById(0x7f0a046a)).setText(mModel.intro);
            view.findViewById(0x7f0a0469).setVisibility(0);
            return view;
        } else
        {
            view.findViewById(0x7f0a0469).setVisibility(8);
            return view;
        }
    }

    public void setModel(Model model)
    {
        mModel = model;
        notifyDataSetChanged();
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SoundDetailAdapter this$0;

        public void onClick(View view)
        {
            Bundle bundle = new Bundle();
            bundle.putString("ExtraUrl", (new StringBuilder()).append(a.S).append("activity-web/activity/").append(mModel.activityId).toString());
            bundle.putInt("web_view_type", 8);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            mBaseFragment.startFragment(com/ximalaya/ting/android/fragment/web/WebFragment, bundle);
        }

        _cls1()
        {
            this$0 = SoundDetailAdapter.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final SoundDetailAdapter this$0;
        final String val$tag;

        public void onClick(View view)
        {
            TagRelativeAlbumListFragment tagrelativealbumlistfragment = TagRelativeAlbumListFragment.getInstance(tag);
            Bundle bundle = tagrelativealbumlistfragment.getArguments();
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            tagrelativealbumlistfragment.setArguments(bundle);
            mBaseFragment.startFragment(tagrelativealbumlistfragment);
        }

        _cls2()
        {
            this$0 = SoundDetailAdapter.this;
            tag = s;
            super();
        }
    }

}
