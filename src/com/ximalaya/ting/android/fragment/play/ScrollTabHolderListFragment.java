// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import com.ximalaya.ting.android.fragment.ScrollTabHolder;
import com.ximalaya.ting.android.fragment.ScrollTabHolderFragment;
import com.ximalaya.ting.android.util.Utilities;

public class ScrollTabHolderListFragment extends ScrollTabHolderFragment
    implements android.widget.AbsListView.OnScrollListener
{

    protected static final String ARG_POSITION = "position";
    private int mHeaderHeight;
    protected ListView mListView;
    private android.widget.AbsListView.OnScrollListener mOnScrollListenerDelegate;
    private View mPlaceholderView;
    protected int mPosition;

    public ScrollTabHolderListFragment()
    {
    }

    public void adjustScroll(int i)
    {
        if (i == Utilities.dip2px(getActivity(), 53F) && mListView.getFirstVisiblePosition() >= 1)
        {
            return;
        } else
        {
            mListView.setSelectionFromTop(1, i);
            return;
        }
    }

    public ListView getListView()
    {
        return mListView;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mListView = (ListView)getView().findViewById(0x7f0a0025);
        if (mListView == null)
        {
            throw new IllegalArgumentException("Must have a ListView with list_view id");
        } else
        {
            mListView.setHeaderDividersEnabled(false);
            mListView.setOnScrollListener(this);
            mPlaceholderView = LayoutInflater.from(getActivity()).inflate(0x7f0301fa, mListView, false);
            bundle = new android.widget.AbsListView.LayoutParams(-1, mHeaderHeight);
            mPlaceholderView.setLayoutParams(bundle);
            mListView.addHeaderView(mPlaceholderView, null, false);
            return;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mPosition = getArguments().getInt("position");
    }

    public void onScroll(AbsListView abslistview, int i, int j, int k)
    {
        if (mScrollTabHolder != null)
        {
            mScrollTabHolder.onScroll(abslistview, i, j, k, mPosition);
        }
        if (mOnScrollListenerDelegate != null)
        {
            mOnScrollListenerDelegate.onScroll(abslistview, i, j, k);
        }
    }

    public void onScrollStateChanged(AbsListView abslistview, int i)
    {
        if (mOnScrollListenerDelegate != null)
        {
            mOnScrollListenerDelegate.onScrollStateChanged(abslistview, i);
        }
    }

    public void setHeaderHeight(int i)
    {
        mHeaderHeight = i;
        if (mPlaceholderView != null)
        {
            android.widget.AbsListView.LayoutParams layoutparams = new android.widget.AbsListView.LayoutParams(-1, mHeaderHeight);
            mPlaceholderView.setLayoutParams(layoutparams);
        }
    }

    public void setOnScrollListener(android.widget.AbsListView.OnScrollListener onscrolllistener)
    {
        mOnScrollListenerDelegate = onscrolllistener;
    }
}
