// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.text.TextUtils;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            ImageViewer

class this._cls0 extends MyAsyncTask
{

    final ImageViewer this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Long[])aobj);
    }

    protected transient List doInBackground(Long along[])
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("trackId", along[0].longValue());
        along = f.a().a((new StringBuilder()).append(a.M).append("mobile/track/images").toString(), requestparams, false);
        if (((com.ximalaya.ting.android.b.>) (along)).> != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.>) (along)).>))
        {
            break MISSING_BLOCK_LABEL_113;
        }
        along = JSON.parseObject(((com.ximalaya.ting.android.b.>) (along)).>);
        if (along.getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_113;
        }
        along = along.getString("data");
        if (TextUtils.isEmpty(along))
        {
            break MISSING_BLOCK_LABEL_113;
        }
        along = JSON.parseArray(along, java/lang/String);
        return along;
        along;
        along.printStackTrace();
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        super.onPostExecute(list);
        ImageViewer.access$000(ImageViewer.this).setVisibility(8);
        if (ImageViewer.access$100(ImageViewer.this) == null || !ImageViewer.access$100(ImageViewer.this).isShowing())
        {
            return;
        }
        if (list != null && list.size() > 0)
        {
            setData(list);
        } else
        {
            CustomToast.showToast(ImageViewer.access$200(ImageViewer.this), "\u6CA1\u6709\u56FE\u7247\u6216\u83B7\u53D6\u56FE\u7247\u5931\u8D25", 1);
        }
        ImageViewer.access$302(ImageViewer.this, null);
    }

    protected void onPreExecute()
    {
        ImageViewer.access$000(ImageViewer.this).setVisibility(0);
    }

    ()
    {
        this$0 = ImageViewer.this;
        super();
    }
}
