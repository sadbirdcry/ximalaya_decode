// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.app.ProgressDialog;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.comment.CommentModel;
import com.ximalaya.ting.android.model.comment.CommentModelList;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoDetail;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerFragment, PlayerViewPagerAdapter, CommentListFragment

private class <init> extends MyAsyncTask
{

    private ProgressDialog pd;
    private long position;
    final PlayerFragment this$0;

    protected transient Integer doInBackground(Object aobj[])
    {
        try
        {
            String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/comment/delete").toString();
            UserInfoMannage.getInstance().getUser();
            long l = ((Long)aobj[0]).longValue();
            long l1 = ((Long)aobj[1]).longValue();
            position = ((Long)aobj[2]).longValue();
            RequestParams requestparams = new RequestParams();
            requestparams.put("trackId", (new StringBuilder()).append("").append(l).toString());
            requestparams.put("commentId", (new StringBuilder()).append("").append(l1).toString());
            if (JSON.parseObject(f.a().b(s, requestparams, (View)aobj[3], fragmentBaseContainerView)).getIntValue("ret") == 0)
            {
                return Integer.valueOf(3);
            }
        }
        // Misplaced declaration of an exception variable
        catch (Object aobj[])
        {
            ((Exception) (aobj)).printStackTrace();
            return Integer.valueOf(2);
        }
        return Integer.valueOf(4);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (PlayerFragment.access$13200(PlayerFragment.this) == null || !isAdded())
        {
            return;
        }
        pd.dismiss();
        switch (integer.intValue())
        {
        default:
            return;

        case 2: // '\002'
            showToast("\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5");
            return;

        case 3: // '\003'
            showToast("\u5220\u9664\u6210\u529F\uFF01");
            integer = PlayerFragment.access$11500(PlayerFragment.this);
            integer.totalCount = ((CommentModelList) (integer)).totalCount - 1;
            if (PlayListControl.getPlayListManager().getCurSound() != null)
            {
                integer = PlayListControl.getPlayListManager().getCurSound();
                integer.comments_counts = ((SoundInfo) (integer)).comments_counts - 1;
            }
            if (PlayerFragment.access$8500(PlayerFragment.this) != null)
            {
                integer = PlayerFragment.access$8500(PlayerFragment.this);
                integer.comments = ((SoundInfoDetail) (integer)).comments - 1;
            }
            PlayerFragment.access$12000(PlayerFragment.this);
            integer = (CommentModel)PlayerFragment.access$11600(PlayerFragment.this).remove((int)position);
            PlayerFragment.access$100(PlayerFragment.this).getCommentFragment().setData(PlayerFragment.access$11500(PlayerFragment.this));
            PlayerFragment.access$100(PlayerFragment.this).getCommentFragment().removeData(integer);
            PlayerFragment.access$11700(PlayerFragment.this);
            PlayerFragment.access$11800(PlayerFragment.this);
            return;

        case 4: // '\004'
            showToast("\u5220\u9664\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5");
            return;
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        pd = new MyProgressDialog(PlayerFragment.access$1600(PlayerFragment.this));
        pd.setTitle("\u8BF7\u7B49\u5F85");
        pd.setMessage("\u6B63\u5728\u5220\u9664\u8BC4\u8BBA...");
        pd.show();
    }

    private ()
    {
        this$0 = PlayerFragment.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
