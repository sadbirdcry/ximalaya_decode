// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.view.View;
import android.widget.ImageView;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.fragment.device.MyDeviceUtil;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerFragment

class this._cls0
    implements android.view.er
{

    final PlayerFragment this$0;

    public void onClick(View view)
    {
        view = SharedPreferencesUtil.getInstance(PlayerFragment.access$1600(PlayerFragment.this));
        if (!view.getBoolean("new_feature_more_clicked"))
        {
            PlayerFragment.access$8100(PlayerFragment.this).setVisibility(8);
            view.saveBoolean("new_feature_more_clicked", true);
        }
        if (PlayerFragment.access$8200(PlayerFragment.this) == null)
        {
            view = new ArrayList();
            view.add("\u8F6C\u91C7");
            view.add(PlayerFragment.access$8300(PlayerFragment.this));
            view.add("\u67E5\u770B\u6240\u5C5E\u4E13\u8F91");
            if (MyDeviceUtil.isShowEnterance(PlayerFragment.access$3300(PlayerFragment.this), "DLNA\u5165\u53E3\u5F00\u5173"))
            {
                view.add("\u4ECE\u97F3\u7BB1\u64AD\u653E");
            }
            view.add("\u8BBE\u4E3A\u624B\u673A\u94C3\u58F0");
            view.add("\u4E3E\u62A5");
            class _cls1
                implements android.widget.AdapterView.OnItemClickListener
            {

                final PlayerFragment._cls42 this$1;

                public void onItemClick(AdapterView adapterview, View view1, int i, long l)
                {
                    if (PlayerFragment.access$8200(this$0) != null)
                    {
                        PlayerFragment.access$8200(this$0).dismiss();
                    }
                    break MISSING_BLOCK_LABEL_26;
_L9:
                    do
                    {
                        return;
                    } while (PlayerFragment.access$1700(this$0) == null || i < 0 || i >= PlayerFragment.access$8200(this$0).getSelections().size());
                    if (((String)PlayerFragment.access$8200(this$0).getSelections().get(i)).equals("\u8F6C\u91C7"))
                    {
                        PlayerFragment.access$8400(this$0);
                        return;
                    }
                    if (!((String)PlayerFragment.access$8200(this$0).getSelections().get(i)).equals(PlayerFragment.access$8300(this$0))) goto _L2; else goto _L1
_L1:
                    ToolUtil.onEvent(PlayerFragment.access$1600(this$0), "Nowplaying_PlayMode");
                    SharedPreferencesUtil.getInstance(PlayerFragment.access$3300(this$0)).getInt("play_mode", 0);
                    JVM INSTR tableswitch 0 2: default 204
                //                               0 286
                //                               1 294
                //                               2 302;
                       goto _L3 _L4 _L5 _L6
_L3:
                    adapterview = "\u987A\u5E8F";
                    i = 0;
_L7:
                    view1 = getString(0x7f0901a8, new Object[] {
                        adapterview
                    });
                    PlayerFragment.access$8200(this$0).setSelection(1, view1);
                    showToast(getString(0x7f0901a9, new Object[] {
                        adapterview
                    }));
                    SharedPreferencesUtil.getInstance(null).saveInt("play_mode", i);
                    return;
_L4:
                    adapterview = "\u5355\u66F2";
                    i = 1;
                    continue; /* Loop/switch isn't completed */
_L5:
                    i = 2;
                    adapterview = "\u968F\u673A";
                    continue; /* Loop/switch isn't completed */
_L6:
                    i = 3;
                    adapterview = "\u5FAA\u73AF";
                    if (true) goto _L7; else goto _L2
_L2:
                    if (((String)PlayerFragment.access$8200(this$0).getSelections().get(i)).equals("\u67E5\u770B\u6240\u5C5E\u4E13\u8F91"))
                    {
                        if (!NetworkUtils.isNetworkAvaliable(PlayerFragment.access$3300(this$0)))
                        {
                            showToast("\u8FDE\u63A5\u7F51\u7EDC\u5931\u8D25");
                            return;
                        }
                        if (PlayerFragment.access$8500(this$0) == null)
                        {
                            showToast(getString(0x7f0901e4));
                            return;
                        }
                        if (!PlayerFragment.access$8500(this$0).hasAlbum())
                        {
                            showToast("\u4EB2\uFF0C\u6CA1\u6709\u6240\u5728\u4E13\u8F91");
                            return;
                        } else
                        {
                            adapterview = new AlbumModel();
                            adapterview.uid = PlayerFragment.access$8500(this$0).uid;
                            adapterview.albumId = PlayerFragment.access$8500(this$0).albumId;
                            adapterview = JSON.toJSONString(adapterview);
                            view1 = new Bundle();
                            view1.putString("album", adapterview);
                            view1.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(PlayerFragment.access$4200(this$0)));
                            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, view1);
                            return;
                        }
                    }
                    if (((String)PlayerFragment.access$8200(this$0).getSelections().get(i)).equals("\u4ECE\u97F3\u7BB1\u64AD\u653E"))
                    {
                        PlayerFragment.access$8602(this$0, true);
                        class _cls1 extends TimerTask
                        {

                            final _cls1 this$2;

                            public void run()
                            {
                                PlayerFragment.access$8602(this$0, false);
                            }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                        }

                        (new Timer()).schedule(new _cls1(), 60000L);
                        if (MyDeviceManager.getInstance(PlayerFragment.access$3300(this$0)).isDlnaInit())
                        {
                            CustomToast.showToast(PlayerFragment.access$3300(this$0), "\u6B63\u5728\u67E5\u627E\u5468\u56F4\u7684WiFi\u97F3\u7BB1", 0);
                            DlnaManager.getInstance(PlayerFragment.access$3300(this$0)).startScanThread(15);
                            PlayerFragment.access$8700(this$0);
                            return;
                        } else
                        {
                            class _cls2
                                implements Runnable
                            {

                                final _cls1 this$2;

                                public void run()
                                {
                                    DexManager.getInstance(PlayerFragment.access$3300(this$0)).setIsDlnaOpen(true);
                                    CustomToast.showToast(PlayerFragment.access$3300(this$0), "\u6B63\u5728\u67E5\u627E\u5468\u56F4\u7684WiFi\u97F3\u7BB1", 0);
                                    ((MainTabActivity2)getActivity()).tingshubaoBind();
                                    DlnaManager.getInstance(PlayerFragment.access$3300(this$0)).startScanThread(15);
                                    PlayerFragment.access$8700(this$0);
                                }

                            _cls2()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                            }

                            DexManager.getInstance(PlayerFragment.access$3300(this$0)).loadDlna(new _cls2(), 1);
                            return;
                        }
                    }
                    if (((String)PlayerFragment.access$8200(this$0).getSelections().get(i)).equals("\u8BBE\u4E3A\u624B\u673A\u94C3\u58F0"))
                    {
                        PlayerFragment.access$8800(this$0);
                        return;
                    }
                    if (((String)PlayerFragment.access$8200(this$0).getSelections().get(i)).equals("\u4E3E\u62A5"))
                    {
                        if (UserInfoMannage.hasLogined())
                        {
                            adapterview = new Intent(PlayerFragment.access$1600(this$0), com/ximalaya/ting/android/activity/report/ReportActivity);
                            adapterview.putExtra("report_type", 0);
                            adapterview.putExtra("track_id", PlayerFragment.access$1700(this$0).trackId);
                            adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(PlayerFragment.access$4200(this$0)));
                            PlayerFragment.access$1600(this$0).startActivity(adapterview);
                            return;
                        } else
                        {
                            adapterview = new Intent(PlayerFragment.access$1600(this$0), com/ximalaya/ting/android/activity/login/LoginActivity);
                            adapterview.setFlags(0x20000000);
                            PlayerFragment.access$1600(this$0).startActivity(adapterview);
                            return;
                        }
                    }
                    if (true) goto _L9; else goto _L8
_L8:
                }

            _cls1()
            {
                this$1 = PlayerFragment._cls42.this;
                super();
            }
            }

            class _cls2
                implements com.ximalaya.ting.android.library.view.dialog.MenuDialog.ExtraCallback
            {

                final PlayerFragment._cls42 this$1;

                public void execute(String s, com.ximalaya.ting.android.library.view.dialog.MenuDialog.ViewHolder viewholder)
                {
                    if ("\u8BBE\u4E3A\u624B\u673A\u94C3\u58F0".equals(s))
                    {
                        if (!SharedPreferencesUtil.getInstance(PlayerFragment.access$1600(this$0)).getBoolean("new_feature_ringtone"))
                        {
                            viewholder.icon.setVisibility(0);
                            return;
                        } else
                        {
                            viewholder.icon.setVisibility(8);
                            return;
                        }
                    } else
                    {
                        viewholder.icon.setVisibility(8);
                        return;
                    }
                }

            _cls2()
            {
                this$1 = PlayerFragment._cls42.this;
                super();
            }
            }

            PlayerFragment.access$8202(PlayerFragment.this, new MenuDialog(getActivity(), view, a.e, new _cls1(), new _cls2()));
        }
        PlayerFragment.access$8200(PlayerFragment.this).setSelection(1, PlayerFragment.access$8300(PlayerFragment.this));
        PlayerFragment.access$8200(PlayerFragment.this).show();
    }

    _cls2()
    {
        this$0 = PlayerFragment.this;
        super();
    }
}
