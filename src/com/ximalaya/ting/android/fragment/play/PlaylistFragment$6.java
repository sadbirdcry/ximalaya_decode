// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.Playlist;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshListView2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlaylistFragment, PlaylistAdapter

class this._cls0 extends MyAsyncTask
{

    final PlaylistFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        return PlayListControl.getPlayListManager().getPlaylist().loadMore();
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        if (PlaylistFragment.access$300(PlaylistFragment.this))
        {
            PlaylistFragment.access$600(PlaylistFragment.this).onRefreshComplete();
            if (list != null && list.size() > 0)
            {
                Collections.reverse(list);
                ArrayList arraylist = new ArrayList(list);
                arraylist.addAll(PlayListControl.getPlayListManager().getPlaylist().getData());
                PlayListControl.getPlayListManager().updatePlaylist(arraylist);
                PlaylistFragment.access$200(PlaylistFragment.this).setData(arraylist);
                mListView.setSelection(list.size());
            }
        } else
        {
            if (list != null && list.size() > 0)
            {
                PlayListControl.getPlayListManager().getPlaylist().append(list);
                PlaylistFragment.access$200(PlaylistFragment.this).addData(list);
            }
            showFooterView(com.ximalaya.ting.android.fragment.oterView.HIDE_ALL);
        }
        PlaylistFragment.access$702(PlaylistFragment.this, false);
        PlaylistFragment.access$500(PlaylistFragment.this).setEnabled(true);
    }

    protected void onPreExecute()
    {
        PlaylistFragment.access$500(PlaylistFragment.this).setEnabled(false);
    }

    iew()
    {
        this$0 = PlaylistFragment.this;
        super();
    }
}
