// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.view.View;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerFragment

class this._cls0
    implements android.view.er
{

    final PlayerFragment this$0;

    public void onClick(View view)
    {
        if (mBoundService == null) goto _L2; else goto _L1
_L1:
        ToolUtil.onEvent(PlayerFragment.access$1600(PlayerFragment.this), "Nowplaying_Paly");
        if (!PlayerFragment.access$6700(PlayerFragment.this) || PlayerFragment.access$7500(PlayerFragment.this).getLinkedDeviceModel() == null) goto _L4; else goto _L3
_L3:
        PlayerFragment.access$7500(PlayerFragment.this).getNowPlayState();
        JVM INSTR tableswitch 0 1: default 76
    //                   0 77
    //                   1 114;
           goto _L2 _L5 _L6
_L2:
        return;
_L5:
        Logger.d("WiFi", "mPlayOrPauseBtn change mNowPlayState 1");
        PlayerFragment.access$7500(PlayerFragment.this).setNowPlayState(1);
        PlayerFragment.access$7500(PlayerFragment.this).play();
        PlayerFragment.access$7600(PlayerFragment.this, 1);
        return;
_L6:
        Logger.d("WiFi", "mPlayOrPauseBtn change mNowPlayState 0");
        PlayerFragment.access$7500(PlayerFragment.this).setNowPlayState(0);
        PlayerFragment.access$7500(PlayerFragment.this).pause();
        PlayerFragment.access$7600(PlayerFragment.this, 0);
        return;
_L4:
        switch (mBoundService.getMediaPlayerState())
        {
        case 1: // '\001'
        case 9: // '\t'
        default:
            return;

        case 0: // '\0'
        case 6: // '\006'
            class _cls1
                implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
            {

                final PlayerFragment._cls34 this$1;

                public void onExecute()
                {
                    mBoundService.doPlay();
                }

            _cls1()
            {
                this$1 = PlayerFragment._cls34.this;
                super();
            }
            }

            PlayerFragment.access$7700(PlayerFragment.this, new _cls1());
            return;

        case 3: // '\003'
        case 5: // '\005'
        case 7: // '\007'
        case 11: // '\013'
            class _cls2
                implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
            {

                final PlayerFragment._cls34 this$1;

                public void onExecute()
                {
                    mBoundService.start();
                }

            _cls2()
            {
                this$1 = PlayerFragment._cls34.this;
                super();
            }
            }

            PlayerFragment.access$7700(PlayerFragment.this, new _cls2());
            return;

        case 4: // '\004'
        case 10: // '\n'
            mBoundService.pause();
            return;

        case 2: // '\002'
        case 8: // '\b'
            class _cls3
                implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
            {

                final PlayerFragment._cls34 this$1;

                public void onExecute()
                {
                    PlayerFragment.access$6600(this$0);
                    mBoundService.restart();
                }

            _cls3()
            {
                this$1 = PlayerFragment._cls34.this;
                super();
            }
            }

            PlayerFragment.access$7700(PlayerFragment.this, new _cls3());
            return;
        }
    }

    _cls3()
    {
        this$0 = PlayerFragment.this;
        super();
    }
}
