// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.model.sound.SoundInfoDetail;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            ScrollTabHolderListFragment, SoundRelativeAlbumModel, SoundRelativeAlbumAdapter, SoundRelativeAlbumLoader

public class SoundRelativeAlbumFragment extends ScrollTabHolderListFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, com.ximalaya.ting.android.fragment.ReloadFragment.Callback
{

    private static final String ID = "track_id";
    private static final int LOAD_DATA = 1;
    private SoundRelativeAlbumAdapter mAdapter;
    private SoundRelativeAlbumModel mAlbum;
    private TextView mEmptyView;
    private Loader mLoader;
    private ProgressBar mProgressBar;
    private long mTrackId;

    public SoundRelativeAlbumFragment()
    {
    }

    private boolean checkSameBaseAlbum(SoundRelativeAlbumModel soundrelativealbummodel, SoundRelativeAlbumModel soundrelativealbummodel1)
    {
        return soundrelativealbummodel != null && soundrelativealbummodel1 != null && soundrelativealbummodel.getAlbumId() == soundrelativealbummodel1.getAlbumId() && soundrelativealbummodel.isBelongsToAlbum() && soundrelativealbummodel1.isBelongsToAlbum();
    }

    public static SoundRelativeAlbumFragment getInstance(int i, SoundRelativeAlbumModel soundrelativealbummodel, long l)
    {
        SoundRelativeAlbumFragment soundrelativealbumfragment = new SoundRelativeAlbumFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("position", i);
        bundle.putLong("track_id", l);
        bundle.putString("album", JSON.toJSONString(soundrelativealbummodel));
        soundrelativealbumfragment.setArguments(bundle);
        return soundrelativealbumfragment;
    }

    private void loadData()
    {
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(1, null, this);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(mListView, mListView);
            return;
        } else
        {
            mLoader = getLoaderManager().restartLoader(1, null, this);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(mListView, mListView);
            return;
        }
    }

    public void clear()
    {
        if (mAdapter != null)
        {
            mAdapter.clear();
        }
        android.widget.AbsListView.LayoutParams layoutparams = new android.widget.AbsListView.LayoutParams(-1, getActivity().getResources().getDisplayMetrics().heightPixels - Utilities.dip2px(getActivity(), 108F));
        mEmptyView.setLayoutParams(layoutparams);
        mEmptyView.setVisibility(0);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mEmptyView = new TextView(getActivity());
        mEmptyView.setText(0x7f0901d0);
        mEmptyView.setGravity(17);
        mEmptyView.setVisibility(8);
        bundle = new android.widget.AbsListView.LayoutParams(-1, getActivity().getResources().getDisplayMetrics().heightPixels - Utilities.dip2px(getActivity(), 108F));
        mEmptyView.setLayoutParams(bundle);
        mListView.addFooterView(mEmptyView);
        bundle = null;
        if (mAlbum != null)
        {
            ArrayList arraylist = new ArrayList();
            bundle = arraylist;
            if (mAlbum.getAlbumId() > 0L)
            {
                mAlbum.setBelongsToAlbum(true);
                arraylist.add(mAlbum);
                bundle = arraylist;
            }
        }
        mAdapter = new SoundRelativeAlbumAdapter(getActivity(), bundle);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new _cls1());
        loadData();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mTrackId = getArguments().getLong("track_id");
        bundle = getArguments().getString("album");
        if (!TextUtils.isEmpty(getArguments().getString(bundle)))
        {
            mAlbum = (SoundRelativeAlbumModel)JSON.parseObject(bundle, com/ximalaya/ting/android/fragment/play/SoundRelativeAlbumModel);
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        if (mProgressBar != null)
        {
            mProgressBar.setVisibility(0);
        }
        return new SoundRelativeAlbumLoader(getActivity(), mTrackId);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        layoutinflater = layoutinflater.inflate(0x7f0300bf, viewgroup, false);
        mProgressBar = (ProgressBar)layoutinflater.findViewById(0x7f0a0093);
        return layoutinflater;
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        if (mProgressBar != null)
        {
            mProgressBar.setVisibility(8);
        }
        if (list != null && list.size() > 0)
        {
            if (checkSameBaseAlbum(mAdapter.getModelAt(0), (SoundRelativeAlbumModel)list.get(0)))
            {
                mAdapter.clear();
            }
            mAdapter.addData(list);
            loader = new android.widget.AbsListView.LayoutParams(-1, 0);
            mEmptyView.setLayoutParams(loader);
            mEmptyView.setVisibility(8);
            return;
        } else
        {
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
            return;
        }
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void reload(View view)
    {
        loadData();
    }

    public void updateData(SoundInfoDetail soundinfodetail, String s)
    {
        if (soundinfodetail == null)
        {
            if (mAdapter == null)
            {
                mAdapter = new SoundRelativeAlbumAdapter(getActivity(), null);
                return;
            } else
            {
                mAdapter.setData(null);
                return;
            }
        }
        mTrackId = soundinfodetail.trackId;
        if (mAlbum == null)
        {
            mAlbum = new SoundRelativeAlbumModel();
        }
        mAlbum.setAlbumId(soundinfodetail.albumId);
        mAlbum.setCoverMiddle(soundinfodetail.albumImage);
        mAlbum.setTitle(soundinfodetail.albumTitle);
        soundinfodetail = new ArrayList();
        if (mAdapter == null)
        {
            mAdapter = new SoundRelativeAlbumAdapter(getActivity(), soundinfodetail);
        } else
        {
            if (mAlbum.getAlbumId() > 0L)
            {
                mAlbum.setBelongsToAlbum(true);
                soundinfodetail.add(mAlbum);
            }
            mAdapter.setData(soundinfodetail);
        }
        DataCollectUtil.bindDataToView(s, mListView);
        loadData();
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final SoundRelativeAlbumFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            adapterview = new AlbumModel();
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= mAdapter.getCount())
            {
                return;
            }
            SoundRelativeAlbumModel soundrelativealbummodel = mAdapter.getModelAt(i);
            adapterview.albumId = soundrelativealbummodel.getAlbumId();
            Bundle bundle = new Bundle();
            bundle.putString("album", JSONObject.toJSONString(adapterview));
            if (!TextUtils.isEmpty(soundrelativealbummodel.getRecSrc()))
            {
                bundle.putString("rec_src", soundrelativealbummodel.getRecSrc());
            }
            if (!TextUtils.isEmpty(soundrelativealbummodel.getRecTrack()))
            {
                bundle.putString("rec_track", soundrelativealbummodel.getRecTrack());
            }
            bundle.putInt("from", 10);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
        }

        _cls1()
        {
            this$0 = SoundRelativeAlbumFragment.this;
            super();
        }
    }

}
