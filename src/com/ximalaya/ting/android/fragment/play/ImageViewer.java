// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.viewpager.HackyViewPager;
import com.ximalaya.ting.android.view.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import java.util.List;
import uk.co.senab.photoview.PhotoView;

public class ImageViewer
    implements android.view.View.OnClickListener
{
    private class ImageAdapter extends PagerAdapter
    {

        final ImageViewer this$0;

        public void destroyItem(ViewGroup viewgroup, int i, Object obj)
        {
            viewgroup.removeView((View)obj);
            mProgressBar.setVisibility(8);
        }

        public int getCount()
        {
            return mUrls.size();
        }

        public int getItemPosition(Object obj)
        {
            return -2;
        }

        public Object instantiateItem(ViewGroup viewgroup, int i)
        {
            mProgressBar.setVisibility(0);
            iv = new PhotoView(mContext);
            class _cls1
                implements uk.co.senab.photoview.PhotoViewAttacher.OnViewTapListener
            {

                final ImageAdapter this$1;

                public void onViewTap(View view, float f, float f1)
                {
                    dismiss();
                }

                _cls1()
                {
                    this$1 = ImageAdapter.this;
                    super();
                }
            }

            ((PhotoView)iv).setOnViewTapListener(new _cls1());
            if (isCenterInside)
            {
                iv.setScaleType(android.widget.ImageView.ScaleType.FIT_CENTER);
                mHackyView.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(-2, -2));
            }
            class _cls2
                implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
            {

                final ImageAdapter this$1;

                public void onCompleteDisplay(String s, Bitmap bitmap)
                {
                    mProgressBar.setVisibility(8);
                }

                _cls2()
                {
                    this$1 = ImageAdapter.this;
                    super();
                }
            }

            ImageManager2.from(mContext).displayImage(iv, (String)mUrls.get(i), -1, new _cls2());
            viewgroup.addView(iv);
            return iv;
        }

        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }

        private ImageAdapter()
        {
            this$0 = ImageViewer.this;
            super();
        }

        ImageAdapter(_cls1 _pcls1)
        {
            this();
        }
    }


    private boolean isCenterInside;
    private ImageView iv;
    private View mContent;
    private Context mContext;
    private HackyViewPager mHackyView;
    private ImageAdapter mImageAdapter;
    private CirclePageIndicator mIndicator;
    private boolean mIsHasDismissAnimation;
    private MyAsyncTask mLoaderAsyncTask;
    private RelativeLayout mMainLayout;
    private PopupWindow mPopupWindow;
    private ProgressBar mProgressBar;
    private long mTrackId;
    private List mUrls;
    private ViewPager mViewPager;
    private int type;

    public ImageViewer(Context context)
    {
        type = 0;
        mUrls = new ArrayList();
        isCenterInside = false;
        mIsHasDismissAnimation = false;
        mContext = context;
        init();
    }

    private void init()
    {
        if (mPopupWindow == null)
        {
            mPopupWindow = new PopupWindow(mContext);
            mContent = View.inflate(mContext, 0x7f03014d, null);
            mPopupWindow.setContentView(mContent);
            mMainLayout = (RelativeLayout)mContent.findViewById(0x7f0a0056);
            mMainLayout.setOnClickListener(this);
            mViewPager = (ViewPager)mContent.findViewById(0x7f0a050c);
            mViewPager.setOnClickListener(this);
            mIndicator = (CirclePageIndicator)mContent.findViewById(0x7f0a0230);
            mProgressBar = (ProgressBar)mContent.findViewById(0x7f0a00ba);
            mHackyView = (HackyViewPager)mContent.findViewById(0x7f0a050c);
            mImageAdapter = new ImageAdapter(null);
            mViewPager.setAdapter(mImageAdapter);
            mIndicator.setViewPager(mViewPager);
            mPopupWindow.setWidth(-1);
            mPopupWindow.setHeight(-1);
            mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
            mPopupWindow.setOutsideTouchable(true);
            mPopupWindow.setFocusable(true);
        }
    }

    public void dismiss()
    {
        if (mLoaderAsyncTask != null)
        {
            mLoaderAsyncTask.cancel(true);
        }
        mViewPager.removeAllViews();
        iv = null;
        if (mIsHasDismissAnimation)
        {
            AnimatorSet animatorset = (AnimatorSet)AnimatorInflater.loadAnimator(mContext, 0x7f040010);
            animatorset.setTarget(mPopupWindow);
            animatorset.start();
        }
        if (mPopupWindow.isShowing())
        {
            mPopupWindow.dismiss();
        }
    }

    public boolean isShowing()
    {
        return mPopupWindow.isShowing();
    }

    public void onClick(View view)
    {
        dismiss();
        switch (view.getId())
        {
        default:
            return;

        case 2131361878: 
            dismiss();
            return;

        case 2131363084: 
            dismiss();
            break;
        }
    }

    public void setBackgroundAlpha(int i)
    {
        mMainLayout.getBackground().setAlpha(i);
    }

    public void setData(long l)
    {
        mTrackId = l;
        mLoaderAsyncTask = (new _cls1()).myexec(new Long[] {
            Long.valueOf(l)
        });
    }

    public void setData(List list)
    {
        if (list == null || list.size() == 0)
        {
            return;
        } else
        {
            mUrls.clear();
            mUrls.addAll(list);
            mImageAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void setIndicatorVisible(boolean flag)
    {
        if (flag)
        {
            mIndicator.setVisibility(0);
            return;
        } else
        {
            mIndicator.setVisibility(8);
            return;
        }
    }

    public void setIsHasDismissAnimation(boolean flag)
    {
        mIsHasDismissAnimation = flag;
        if (flag)
        {
            mPopupWindow.setAnimationStyle(0x7f0b0029);
        }
    }

    public void setPictureCenterInside()
    {
        isCenterInside = true;
    }

    public void show(View view)
    {
        show(view, 0);
    }

    public void show(View view, int i)
    {
        mMainLayout.getBackground().setAlpha(255);
        if (!mPopupWindow.isShowing())
        {
            mPopupWindow.showAtLocation(view, 17, 0, 0);
        }
        if (i + 1 <= mUrls.size())
        {
            mViewPager.setCurrentItem(i);
        }
    }





/*
    static MyAsyncTask access$302(ImageViewer imageviewer, MyAsyncTask myasynctask)
    {
        imageviewer.mLoaderAsyncTask = myasynctask;
        return myasynctask;
    }

*/




/*
    static ImageView access$602(ImageViewer imageviewer, ImageView imageview)
    {
        imageviewer.iv = imageview;
        return imageview;
    }

*/



    private class _cls1 extends MyAsyncTask
    {

        final ImageViewer this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Long[])aobj);
        }

        protected transient List doInBackground(Long along[])
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("trackId", along[0].longValue());
            along = f.a().a((new StringBuilder()).append(a.M).append("mobile/track/images").toString(), requestparams, false);
            if (((com.ximalaya.ting.android.b.n.a) (along)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (along)).a))
            {
                break MISSING_BLOCK_LABEL_113;
            }
            along = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (along)).a);
            if (along.getIntValue("ret") != 0)
            {
                break MISSING_BLOCK_LABEL_113;
            }
            along = along.getString("data");
            if (TextUtils.isEmpty(along))
            {
                break MISSING_BLOCK_LABEL_113;
            }
            along = JSON.parseArray(along, java/lang/String);
            return along;
            along;
            along.printStackTrace();
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            mProgressBar.setVisibility(8);
            if (mPopupWindow == null || !mPopupWindow.isShowing())
            {
                return;
            }
            if (list != null && list.size() > 0)
            {
                setData(list);
            } else
            {
                CustomToast.showToast(mContext, "\u6CA1\u6709\u56FE\u7247\u6216\u83B7\u53D6\u56FE\u7247\u5931\u8D25", 1);
            }
            mLoaderAsyncTask = null;
        }

        protected void onPreExecute()
        {
            mProgressBar.setVisibility(0);
        }

        _cls1()
        {
            this$0 = ImageViewer.this;
            super();
        }
    }

}
