// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoDetail;
import com.ximalaya.ting.android.util.ApiUtil;

public class SoundInfoDetailLoader extends AsyncTaskLoader
{

    private SoundInfoDetail mData;
    private View mFromView;
    private SoundInfo mSoundInfo;
    private View mToView;

    public SoundInfoDetailLoader(Context context, SoundInfo soundinfo, View view, View view1)
    {
        super(context);
        mSoundInfo = soundinfo;
        mFromView = view;
        mToView = view1;
    }

    public void deliverResult(SoundInfoDetail soundinfodetail)
    {
        super.deliverResult(soundinfodetail);
        mData = soundinfodetail;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((SoundInfoDetail)obj);
    }

    public SoundInfoDetail loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append(mSoundInfo.trackId).append("").toString());
        obj = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/track/detail").toString(), ((RequestParams) (obj)), mFromView, mToView, false);
        try
        {
            if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
            {
                mData = (SoundInfoDetail)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a, com/ximalaya/ting/android/model/sound/SoundInfoDetail);
            }
            if (mData != null && mData.userInfo == null)
            {
                mData.userInfo = new com.ximalaya.ting.android.model.sound.SoundInfoDetail.UserInfo();
                mData.userInfo.uid = mSoundInfo.uid;
                mData.userInfo.nickname = mSoundInfo.nickname;
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
