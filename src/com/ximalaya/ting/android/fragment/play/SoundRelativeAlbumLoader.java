// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            SoundRelativeAlbumModel

public class SoundRelativeAlbumLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "rec-association/recommend/album";
    private List mData;
    private long mTrackId;

    public SoundRelativeAlbumLoader(Context context, long l)
    {
        super(context);
        mTrackId = l;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((List)obj);
    }

    public void deliverResult(List list)
    {
        super.deliverResult(list);
        mData = list;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public List loadInBackground()
    {
        Object obj;
        obj = new RequestParams();
        ((RequestParams) (obj)).put("trackId", mTrackId);
        obj = f.a().a((new StringBuilder()).append(a.G).append("rec-association/recommend/album").toString(), ((RequestParams) (obj)), fromBindView, toBindView, false);
        fromBindView = null;
        toBindView = null;
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_195;
        }
        obj = JSONObject.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        if (((JSONObject) (obj)).getInteger("ret").intValue() != 0)
        {
            break MISSING_BLOCK_LABEL_195;
        }
        mData = new ArrayList();
        Object obj1 = ((JSONObject) (obj)).getString("baseAlbum");
        if (!TextUtils.isEmpty(((CharSequence) (obj1))))
        {
            obj1 = (SoundRelativeAlbumModel)JSON.parseObject(((String) (obj1)), com/ximalaya/ting/android/fragment/play/SoundRelativeAlbumModel);
            ((SoundRelativeAlbumModel) (obj1)).setBelongsToAlbum(true);
            mData.add(obj1);
        }
        obj = ((JSONObject) (obj)).getString("albums");
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            mData.addAll(SoundRelativeAlbumModel.getModelListFromJson(((String) (obj))));
        }
        obj = mData;
        return ((List) (obj));
        Exception exception;
        exception;
        exception.printStackTrace();
        return null;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData != null)
        {
            deliverResult(mData);
            return;
        } else
        {
            forceLoad();
            return;
        }
    }
}
