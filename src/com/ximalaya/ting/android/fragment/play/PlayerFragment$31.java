// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.widget.SeekBar;
import android.widget.TextView;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.view.seekbar.MySeekBar;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerFragment

class this._cls0
    implements android.widget.hangeListener
{

    final PlayerFragment this$0;

    public void onProgressChanged(SeekBar seekbar, int i, boolean flag)
    {
        if (flag)
        {
            seekbar = LocalMediaService.getInstance();
            if (seekbar != null)
            {
                int j = seekbar.getDuration();
                PlayerFragment.access$7300(PlayerFragment.this, i, j);
            }
        }
    }

    public void onStartTrackingTouch(SeekBar seekbar)
    {
        PlayerFragment.access$7102(PlayerFragment.this, true);
    }

    public void onStopTrackingTouch(SeekBar seekbar)
    {
        PlayerFragment.mIsSetSeekBar = false;
        PlayerFragment.access$6600(PlayerFragment.this);
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.seekToProgress(seekbar.getProgress(), seekbar.getMax());
            if (PlayerFragment.access$6700(PlayerFragment.this) && PlayerFragment.access$6800(PlayerFragment.this))
            {
                PlayerFragment.access$6900(PlayerFragment.this, seekbar);
            }
        }
        PlayerFragment.access$7000(PlayerFragment.this).setCanSeek(false);
        PlayerFragment.access$7102(PlayerFragment.this, false);
        PlayerFragment.access$7200(PlayerFragment.this).setVisibility(4);
    }

    ()
    {
        this$0 = PlayerFragment.this;
        super();
    }
}
