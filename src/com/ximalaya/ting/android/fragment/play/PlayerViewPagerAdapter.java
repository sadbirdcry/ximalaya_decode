// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.util.SparseArrayCompat;
import com.ximalaya.ting.android.fragment.ScrollTabHolder;
import com.ximalaya.ting.android.model.sound.SoundInfoDetail;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            CommentListFragment, PlayingSoundDetailFragment, SoundRelativeAlbumModel, SoundRelativeAlbumFragment

public class PlayerViewPagerAdapter extends FragmentPagerAdapter
{

    private static final int COUNT = 3;
    static final int ITEM_COMMENT = 1;
    static final int ITEM_DETAIL = 0;
    static final int ITEM_RELATIVE = 2;
    private CommentListFragment.OnActivityCreateCallback mCallback;
    private android.widget.AdapterView.OnItemClickListener mCommentItemClickListener;
    private CommentListFragment mCommentListFragment;
    private PlayingSoundDetailFragment mDetailInfoFragment;
    private int mHeaderHeight;
    private ScrollTabHolder mListener;
    private SoundRelativeAlbumFragment mRelativeAlbumFragment;
    private SparseArrayCompat mScrollTabHolders;
    private SoundInfoDetail mSoundInfo;

    public PlayerViewPagerAdapter(FragmentManager fragmentmanager, SoundInfoDetail soundinfodetail)
    {
        super(fragmentmanager);
        mScrollTabHolders = new SparseArrayCompat();
        mSoundInfo = soundinfodetail;
    }

    public CommentListFragment getCommentFragment()
    {
        return mCommentListFragment;
    }

    public int getCount()
    {
        return 3;
    }

    public PlayingSoundDetailFragment getDetailFragment()
    {
        return mDetailInfoFragment;
    }

    public Fragment getItem(int i)
    {
        long l3 = 0L;
        SoundRelativeAlbumModel soundrelativealbummodel;
        switch (i)
        {
        default:
            return null;

        case 1: // '\001'
            mCommentListFragment = CommentListFragment.getInstance(i);
            mScrollTabHolders.put(i, mCommentListFragment);
            if (mListener != null)
            {
                mCommentListFragment.setScrollTabHolder(mListener);
            }
            mCommentListFragment.setOnItemClickListener(mCommentItemClickListener);
            mCommentListFragment.setOnActivityCreateCallback(mCallback);
            mCommentListFragment.setHeaderHeight(mHeaderHeight);
            return mCommentListFragment;

        case 0: // '\0'
            long l;
            long l2;
            if (mSoundInfo == null)
            {
                l = 0L;
            } else
            {
                l = mSoundInfo.trackId;
            }
            if (mSoundInfo == null)
            {
                l2 = 0L;
            } else
            {
                l2 = mSoundInfo.uid;
            }
            if (mSoundInfo != null)
            {
                l3 = mSoundInfo.albumId;
            }
            mDetailInfoFragment = PlayingSoundDetailFragment.getInstance(i, l, l2, l3);
            mDetailInfoFragment.setHeaderHeight(mHeaderHeight);
            mScrollTabHolders.put(i, mDetailInfoFragment);
            if (mListener != null)
            {
                mDetailInfoFragment.setScrollTabHolder(mListener);
            }
            return mDetailInfoFragment;

        case 2: // '\002'
            soundrelativealbummodel = new SoundRelativeAlbumModel();
            break;
        }
        if (mSoundInfo != null)
        {
            soundrelativealbummodel.setAlbumId(mSoundInfo.albumId);
            soundrelativealbummodel.setCoverSmall(mSoundInfo.albumImage);
            soundrelativealbummodel.setTitle(mSoundInfo.albumTitle);
        }
        long l1;
        if (mSoundInfo == null)
        {
            l1 = -1L;
        } else
        {
            l1 = mSoundInfo.trackId;
        }
        mRelativeAlbumFragment = SoundRelativeAlbumFragment.getInstance(i, soundrelativealbummodel, l1);
        mRelativeAlbumFragment.setHeaderHeight(mHeaderHeight);
        mScrollTabHolders.put(i, mRelativeAlbumFragment);
        if (mListener != null)
        {
            mRelativeAlbumFragment.setScrollTabHolder(mListener);
        }
        return mRelativeAlbumFragment;
    }

    public SparseArrayCompat getScrollTabHolders()
    {
        return mScrollTabHolders;
    }

    public SoundRelativeAlbumFragment getSoundRelativeAlbumFragment()
    {
        return mRelativeAlbumFragment;
    }

    public void setCommentListItemClickListener(android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        mCommentItemClickListener = onitemclicklistener;
    }

    public void setHeaderHeight(int i)
    {
        mHeaderHeight = i;
        if (mCommentListFragment != null)
        {
            mCommentListFragment.setHeaderHeight(i);
        }
        if (mDetailInfoFragment != null)
        {
            mDetailInfoFragment.setHeaderHeight(i);
        }
        if (mRelativeAlbumFragment != null)
        {
            mRelativeAlbumFragment.setHeaderHeight(i);
        }
    }

    public void setOnActivityCreateCallback(CommentListFragment.OnActivityCreateCallback onactivitycreatecallback)
    {
        mCallback = onactivitycreatecallback;
    }

    public void setTabHolderScrollingContent(ScrollTabHolder scrolltabholder)
    {
        mListener = scrolltabholder;
    }
}
