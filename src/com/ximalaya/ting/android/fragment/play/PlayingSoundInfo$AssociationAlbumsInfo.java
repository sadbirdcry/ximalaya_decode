// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import com.ximalaya.ting.android.model.album.IAlbum;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayingSoundInfo

public static class 
    implements IAlbum
{

    public long albumId;
    public String coverMiddle;
    public String coverSmall;
    public String intro;
    public String recSrc;
    public String recTrack;
    public String title;
    public long tracks;
    public long uid;
    public long updatedAt;

    public String getIAlbumCoverUrl()
    {
        return coverSmall;
    }

    public long getIAlbumId()
    {
        return albumId;
    }

    public long getIAlbumLastUpdateAt()
    {
        return updatedAt;
    }

    public long getIAlbumPlayCount()
    {
        return 0L;
    }

    public String getIAlbumTag()
    {
        return null;
    }

    public String getIAlbumTitle()
    {
        return title;
    }

    public long getIAlbumTrackCount()
    {
        return tracks;
    }

    public String getIIntro()
    {
        return intro;
    }

    public boolean isIAlbumCollect()
    {
        return false;
    }

    public void setIAlbumCollect(boolean flag)
    {
    }

    public ()
    {
    }
}
