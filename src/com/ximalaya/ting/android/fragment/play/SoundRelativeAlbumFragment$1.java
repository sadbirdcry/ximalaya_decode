// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.DataCollectUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            SoundRelativeAlbumFragment, SoundRelativeAlbumAdapter, SoundRelativeAlbumModel

class this._cls0
    implements android.widget.er
{

    final SoundRelativeAlbumFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        adapterview = new AlbumModel();
        i -= mListView.getHeaderViewsCount();
        if (i < 0 || i >= SoundRelativeAlbumFragment.access$000(SoundRelativeAlbumFragment.this).getCount())
        {
            return;
        }
        SoundRelativeAlbumModel soundrelativealbummodel = SoundRelativeAlbumFragment.access$000(SoundRelativeAlbumFragment.this).getModelAt(i);
        adapterview.albumId = soundrelativealbummodel.getAlbumId();
        Bundle bundle = new Bundle();
        bundle.putString("album", JSONObject.toJSONString(adapterview));
        if (!TextUtils.isEmpty(soundrelativealbummodel.getRecSrc()))
        {
            bundle.putString("rec_src", soundrelativealbummodel.getRecSrc());
        }
        if (!TextUtils.isEmpty(soundrelativealbummodel.getRecTrack()))
        {
            bundle.putString("rec_track", soundrelativealbummodel.getRecTrack());
        }
        bundle.putInt("from", 10);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
    }

    ()
    {
        this$0 = SoundRelativeAlbumFragment.this;
        super();
    }
}
