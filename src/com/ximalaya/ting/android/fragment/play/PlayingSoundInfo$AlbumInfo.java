// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import com.ximalaya.ting.android.model.album.IAlbum;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayingSoundInfo

public static class 
    implements IAlbum
{

    public long albumId;
    public long categoryId;
    public String coverLarge;
    public String coverOrigin;
    public String coverSmall;
    public String coverWebLarge;
    public long createdAt;
    public boolean hasNew;
    public String intro;
    public boolean isFavorite;
    public long playTimes;
    public long playsCounts;
    public int serialState;
    public int serializeStatus;
    public long shares;
    public int status;
    public String tags;
    public String title;
    public long tracks;
    public long uid;
    public long updateAt;

    public String getIAlbumCoverUrl()
    {
        return coverSmall;
    }

    public long getIAlbumId()
    {
        return albumId;
    }

    public long getIAlbumLastUpdateAt()
    {
        return updateAt;
    }

    public long getIAlbumPlayCount()
    {
        return playsCounts;
    }

    public String getIAlbumTag()
    {
        return tags;
    }

    public String getIAlbumTitle()
    {
        return title;
    }

    public long getIAlbumTrackCount()
    {
        return tracks;
    }

    public String getIIntro()
    {
        return intro;
    }

    public boolean isIAlbumCollect()
    {
        return isFavorite;
    }

    public void setIAlbumCollect(boolean flag)
    {
        isFavorite = flag;
    }

    public ()
    {
    }
}
