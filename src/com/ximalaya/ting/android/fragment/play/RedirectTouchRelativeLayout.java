// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.Context;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.RelativeLayout;
import com.nineoldandroids.view.ViewHelper;
import com.ximalaya.ting.android.util.PackageUtil;

public class RedirectTouchRelativeLayout extends RelativeLayout
{
    public static interface TouchUpCallback
    {

        public abstract void touchUp();
    }


    private float mDownX;
    private float mDownY;
    private int mHeaderMaxTranslation;
    private View mHeaderView;
    private boolean mIsScroll;
    private float mLastX;
    private float mLastY;
    private Matrix mMatrix;
    private int mTouchSlop;
    private TouchUpCallback mTouchUpCallback;
    private boolean mVerticalScrollInHeader;
    private View mViewPager;

    public RedirectTouchRelativeLayout(Context context)
    {
        super(context);
        init(context);
    }

    public RedirectTouchRelativeLayout(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        init(context);
    }

    public RedirectTouchRelativeLayout(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        init(context);
    }

    private void init(Context context)
    {
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public boolean dispatchTouchEvent(MotionEvent motionevent)
    {
        float f1;
        float f2;
        boolean flag;
        MotionEvent motionevent1 = MotionEvent.obtain(motionevent);
        int i = motionevent.getAction();
        float f = ViewHelper.getTranslationY(mHeaderView);
        if (PackageUtil.isPostHB())
        {
            if (mMatrix == null)
            {
                mMatrix = new Matrix();
            } else
            {
                mMatrix.reset();
            }
            mMatrix.setTranslate(0.0F, -f);
            motionevent1.transform(mMatrix);
        } else
        {
            motionevent1.setLocation(motionevent1.getX(), motionevent1.getY() - f);
        }
        f1 = motionevent.getX();
        f2 = motionevent.getY();
        if (f2 > (float)mHeaderView.getTop() + f && f2 < f + (float)mHeaderView.getBottom())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        i;
        JVM INSTR tableswitch 0 3: default 140
    //                   0 226
    //                   1 353
    //                   2 256
    //                   3 353;
           goto _L1 _L2 _L3 _L4 _L3
_L1:
        if (!mVerticalScrollInHeader && i == 1 && mIsScroll)
        {
            motionevent1.setAction(3);
        }
        mHeaderView.dispatchTouchEvent(motionevent1);
        break MISSING_BLOCK_LABEL_176;
_L2:
        mDownX = f1;
        mLastX = f1;
        mDownY = f2;
        mLastX = f2;
        mVerticalScrollInHeader = false;
        continue; /* Loop/switch isn't completed */
_L4:
        if (Math.abs(f1 - mDownX) > (float)mTouchSlop || Math.abs(f2 - mDownY) > (float)mTouchSlop)
        {
            mIsScroll = true;
            if (flag && Math.abs(f1 - mDownX) > Math.abs(f2 - mDownY))
            {
                mVerticalScrollInHeader = true;
            }
        } else
        {
            mIsScroll = false;
        }
        mLastX = f1;
        mLastY = f2;
        continue; /* Loop/switch isn't completed */
_L3:
        if (Math.abs(f1 - mDownX) > (float)mTouchSlop || Math.abs(f2 - mDownY) > (float)mTouchSlop)
        {
            mIsScroll = true;
        } else
        {
            mIsScroll = false;
        }
        if (mTouchUpCallback != null)
        {
            mTouchUpCallback.touchUp();
        }
        continue; /* Loop/switch isn't completed */
        if (flag && !mIsScroll || mVerticalScrollInHeader)
        {
            return true;
        }
        mViewPager.dispatchTouchEvent(motionevent);
        return true;
        if (true) goto _L1; else goto _L5
_L5:
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        mHeaderView = findViewById(0x7f0a00c8);
        mViewPager = findViewById(0x7f0a017a);
    }

    public void removeTouchUpCallback()
    {
        mTouchUpCallback = null;
    }

    public void setHeaderMaxTranslation(int i)
    {
        mHeaderMaxTranslation = i;
    }

    public void setTouchUpCallback(TouchUpCallback touchupcallback)
    {
        mTouchUpCallback = touchupcallback;
    }
}
