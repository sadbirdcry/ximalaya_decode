// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerFragment, PlaylistFragment

class this._cls0
    implements android.view.er
{

    final PlayerFragment this$0;

    public void onClick(View view)
    {
        if (PlayerFragment.access$1700(PlayerFragment.this) == null)
        {
            return;
        }
        ToolUtil.onEvent(PlayerFragment.access$1600(PlayerFragment.this), "Nowplaying_ZhuanJi");
        FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
        fragmenttransaction.setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020);
        if (PlayerFragment.access$9600(PlayerFragment.this) == null)
        {
            PlayerFragment.access$9602(PlayerFragment.this, PlaylistFragment.getInstance());
            Bundle bundle = new Bundle();
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            PlayerFragment.access$9600(PlayerFragment.this).setArguments(bundle);
            class _cls1
                implements com.ximalaya.ting.android.view.SlideView.OnFinishListener
            {

                final PlayerFragment._cls50 this$1;

                public boolean onFinish()
                {
                    if (PlayerFragment.access$9600(this$0) != null && PlayerFragment.access$9600(this$0).isAdded())
                    {
                        getChildFragmentManager().beginTransaction().remove(PlayerFragment.access$9600(this$0)).commitAllowingStateLoss();
                        PlayerFragment.access$9602(this$0, null);
                        return true;
                    } else
                    {
                        return false;
                    }
                }

            _cls1()
            {
                this$1 = PlayerFragment._cls50.this;
                super();
            }
            }

            PlayerFragment.access$9600(PlayerFragment.this).setOnFinishListener(new _cls1());
            fragmenttransaction.add(0x7f0a0372, PlayerFragment.access$9600(PlayerFragment.this));
            fragmenttransaction.addToBackStack(null);
        } else
        {
            fragmenttransaction.show(PlayerFragment.access$9600(PlayerFragment.this));
        }
        fragmenttransaction.commitAllowingStateLoss();
    }

    _cls1()
    {
        this$0 = PlayerFragment.this;
        super();
    }
}
