// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerFragment

class shareList extends MyAsyncTask
{

    boolean isFirstLike;
    boolean isShare;
    boolean paramFavorite;
    String shareList;
    String str;
    final PlayerFragment this$0;
    long trackId;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected transient String doInBackground(Object aobj[])
    {
        Object obj;
        trackId = ((Long)aobj[0]).longValue();
        isShare = ((Boolean)aobj[1]).booleanValue();
        paramFavorite = ((Boolean)aobj[2]).booleanValue();
        isFirstLike = ((Boolean)aobj[3]).booleanValue();
        obj = UserInfoMannage.getInstance().getUser();
        if (obj == null) goto _L2; else goto _L1
_L1:
        if (((LoginInfoModel) (obj)).bindStatus == null) goto _L2; else goto _L3
_L3:
        List list;
        list = ((LoginInfoModel) (obj)).bindStatus;
        if (!isShare)
        {
            break MISSING_BLOCK_LABEL_247;
        }
        aobj = list.iterator();
_L8:
        Object obj1;
        do
        {
            if (!((Iterator) (aobj)).hasNext())
            {
                break MISSING_BLOCK_LABEL_247;
            }
            obj1 = (ThirdPartyUserInfo)((Iterator) (aobj)).next();
        } while (((ThirdPartyUserInfo) (obj1)).isExpired());
        if (!((ThirdPartyUserInfo) (obj1)).getIdentity().equals("1")) goto _L5; else goto _L4
_L4:
        if (!"".equals(shareList)) goto _L7; else goto _L6
_L6:
        shareList = "tSina";
          goto _L8
_L7:
        try
        {
            shareList = (new StringBuilder()).append(shareList).append("tSina").toString();
        }
        // Misplaced declaration of an exception variable
        catch (Object aobj[])
        {
            return "";
        }
          goto _L8
_L5:
        if (!((ThirdPartyUserInfo) (obj1)).getIdentity().equals("2")) goto _L8; else goto _L9
_L9:
        if (!"".equals(shareList))
        {
            break MISSING_BLOCK_LABEL_218;
        }
        shareList = "qzone";
          goto _L8
        shareList = (new StringBuilder()).append(shareList).append("qzone").toString();
          goto _L8
        if (!isFirstLike || paramFavorite) goto _L2; else goto _L10
_L10:
        aobj = ApiUtil.getApiHost();
        int i = 0;
_L17:
        if (i >= list.size()) goto _L12; else goto _L11
_L11:
        aobj = (new StringBuilder()).append(((String) (aobj))).append("mobile/sync/").append(((ThirdPartyUserInfo)list.get(i)).thirdpartyId).append("/set").toString();
        obj1 = new RequestParams();
        ((RequestParams) (obj1)).put("type", "favorite");
        ((RequestParams) (obj1)).put("isChecked", String.valueOf(isShare));
        obj1 = f.a().b(((String) (aobj)), ((RequestParams) (obj1)), PlayerFragment.access$8900(PlayerFragment.this), null);
        if (obj1 == null) goto _L14; else goto _L13
_L13:
        obj1 = JSON.parseObject(((String) (obj1)));
        if (obj1 == null) goto _L14; else goto _L15
_L15:
        if (((JSONObject) (obj1)).getInteger("ret").intValue() == 0)
        {
            ((ThirdPartyUserInfo)list.get(i)).setWebFavorite(isShare);
        }
          goto _L14
_L12:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_432;
        }
        obj.bindStatus = list;
        PlayerFragment.access$11100(PlayerFragment.this);
_L2:
        aobj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/favorite/track").toString();
        obj = new RequestParams();
        ((RequestParams) (obj)).put("trackId", String.valueOf(trackId));
        boolean flag;
        if (!paramFavorite)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        ((RequestParams) (obj)).put("favorite", String.valueOf(flag));
        if (!"".equals(shareList))
        {
            ((RequestParams) (obj)).put("shareList", String.valueOf(shareList));
        }
        aobj = f.a().b(((String) (aobj)), ((RequestParams) (obj)), PlayerFragment.access$8900(PlayerFragment.this), null);
        if (aobj == null)
        {
            break MISSING_BLOCK_LABEL_578;
        }
        aobj = JSON.parseObject(((String) (aobj)));
        if (((JSONObject) (aobj)).getInteger("ret").intValue() == 0)
        {
            return "0";
        }
        aobj = ((JSONObject) (aobj)).getString("msg");
        return ((String) (aobj));
        return "";
_L14:
        i++;
        if (true) goto _L17; else goto _L16
_L16:
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((String)obj);
    }

    protected void onPostExecute(String s)
    {
        if ("0".equals(s))
        {
            if (!paramFavorite)
            {
                paramFavorite = true;
                str = "\u8D5E\u6210\u529F";
            } else
            {
                paramFavorite = false;
                str = "\u53D6\u6D88\u8D5E\u6210\u529F";
            }
            DownloadHandler.getInstance(PlayerFragment.access$1600(PlayerFragment.this).getApplicationContext()).updateFavorited(trackId, paramFavorite, false);
            if (PlayerFragment.access$1600(PlayerFragment.this) != null && isAdded())
            {
                if (PlayerFragment.access$8900(PlayerFragment.this) != null)
                {
                    if (paramFavorite)
                    {
                        PlayerFragment.access$8900(PlayerFragment.this).setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020346, 0, 0);
                    } else
                    {
                        PlayerFragment.access$8900(PlayerFragment.this).setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020345, 0, 0);
                    }
                    onSoundLiked(paramFavorite);
                }
                PlayerFragment.access$1502(PlayerFragment.this, true);
                PlayerFragment.access$11200(PlayerFragment.this);
            }
        } else
        if ("".equals(s))
        {
            str = "\u7F51\u7EDC\u8FDE\u63A5\u5931\u8D25";
        } else
        {
            str = s;
        }
        Toast.makeText(PlayerFragment.access$3300(PlayerFragment.this).getApplicationContext(), str, 0).show();
    }

    nfo()
    {
        this$0 = PlayerFragment.this;
        super();
        str = "";
        shareList = "";
    }
}
