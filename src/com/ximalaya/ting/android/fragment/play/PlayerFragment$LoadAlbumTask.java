// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.app.Activity;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoList;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerFragment

class this._cls0 extends MyAsyncTask
{

    final PlayerFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        avoid = PlayListControl.getPlayListManager().getCurSound();
        if (avoid != null)
        {
            if ((avoid = CommonRequest.getAlbumList(PlayerFragment.access$1600(PlayerFragment.this), ((SoundInfo) (avoid)).trackId, ((SoundInfo) (avoid)).albumId, PlayerFragment.access$1700(PlayerFragment.this).uid, ((SoundInfo) (avoid)).albumName, ((SoundInfo) (avoid)).albumCoverPath, ((SoundInfo) (avoid)).recSrc, ((SoundInfo) (avoid)).recTrack, fragmentBaseContainerView, null)) != null)
            {
                return ((SoundInfoList) (avoid)).data;
            }
        }
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        while (PlayerFragment.access$1600(PlayerFragment.this) == null || ((Activity)PlayerFragment.access$1600(PlayerFragment.this)).isFinishing() || list == null) 
        {
            return;
        }
        if (!list.equals(PlayerFragment.access$1800(PlayerFragment.this)) && PlayerFragment.access$1900(PlayerFragment.this))
        {
            PlayListControl.getPlayListManager().updatePlaylist(list);
        }
        PlayerFragment.access$2000(PlayerFragment.this);
    }

    protected void onPreExecute()
    {
    }

    ()
    {
        this$0 = PlayerFragment.this;
        super();
    }
}
