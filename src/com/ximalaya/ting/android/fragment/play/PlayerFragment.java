// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.media.MediaPlayer;
import android.media.RemoteControlClient;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.activity.share.BaseShareDialog;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.dialog.CallingRingtoneDownloadDialog;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.BaseCurrentPlayingModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceUtil;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import com.ximalaya.ting.android.fragment.device.dlna.model.LinkedDeviceModel;
import com.ximalaya.ting.android.fragment.device.doss.DossUtils;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.fragment.userspace.PlayListHostoryFragment;
import com.ximalaya.ting.android.library.model.AppAd;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.ad.AbstractSoundAdModel;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.model.ad.BdSoundAdModel;
import com.ximalaya.ting.android.model.ad.XMSoundAd;
import com.ximalaya.ting.android.model.comment.CommentModel;
import com.ximalaya.ting.android.model.comment.CommentModelList;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.setting.ShareSettingModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoDetail;
import com.ximalaya.ting.android.model.sound.SoundInfoList;
import com.ximalaya.ting.android.modelmanage.CacheManager;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.service.play.Playlist;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.RingtoneUtil;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.BlurableImageView;
import com.ximalaya.ting.android.view.emotion.EmotionSelector;
import com.ximalaya.ting.android.view.seekbar.MySeekBar;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import org.xmlpull.v1.XmlPullParserException;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerViewPagerAdapter, CommentListFragment, RedirectTouchRelativeLayout, PlayingSoundDetailFragment, 
//            SoundRelativeAlbumFragment, PlaylistFragment, ImageViewer

public class PlayerFragment extends BaseFragment
    implements android.view.View.OnClickListener, android.view.ViewTreeObserver.OnGlobalLayoutListener, com.ximalaya.ting.android.fragment.device.dlna.DlnaManager.OnDeviceChangedListener, com.ximalaya.ting.android.fragment.device.dlna.DlnaManager.OnkeyEventListener, com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener, com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener, com.ximalaya.ting.android.service.play.TingMediaPlayer.SoundAdCallback
{
    private static final class FooterView extends Enum
    {

        private static final FooterView $VALUES[];
        public static final FooterView HIDE_ALL;
        public static final FooterView LOADING;
        public static final FooterView MORE;
        public static final FooterView NO_CONNECTION;
        public static final FooterView NO_DATA;

        public static FooterView valueOf(String s)
        {
            return (FooterView)Enum.valueOf(com/ximalaya/ting/android/fragment/play/PlayerFragment$FooterView, s);
        }

        public static FooterView[] values()
        {
            return (FooterView[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterView("MORE", 0);
            LOADING = new FooterView("LOADING", 1);
            NO_CONNECTION = new FooterView("NO_CONNECTION", 2);
            HIDE_ALL = new FooterView("HIDE_ALL", 3);
            NO_DATA = new FooterView("NO_DATA", 4);
            $VALUES = (new FooterView[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA
            });
        }

        private FooterView(String s, int i)
        {
            super(s, i);
        }
    }

    class LikeTask extends MyAsyncTask
    {

        boolean isFirstLike;
        boolean isShare;
        boolean paramFavorite;
        String shareList;
        String str;
        final PlayerFragment this$0;
        long trackId;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected transient String doInBackground(Object aobj[])
        {
            Object obj;
            trackId = ((Long)aobj[0]).longValue();
            isShare = ((Boolean)aobj[1]).booleanValue();
            paramFavorite = ((Boolean)aobj[2]).booleanValue();
            isFirstLike = ((Boolean)aobj[3]).booleanValue();
            obj = UserInfoMannage.getInstance().getUser();
            if (obj == null) goto _L2; else goto _L1
_L1:
            if (((LoginInfoModel) (obj)).bindStatus == null) goto _L2; else goto _L3
_L3:
            List list;
            list = ((LoginInfoModel) (obj)).bindStatus;
            if (!isShare)
            {
                break MISSING_BLOCK_LABEL_247;
            }
            aobj = list.iterator();
_L8:
            Object obj1;
            do
            {
                if (!((Iterator) (aobj)).hasNext())
                {
                    break MISSING_BLOCK_LABEL_247;
                }
                obj1 = (ThirdPartyUserInfo)((Iterator) (aobj)).next();
            } while (((ThirdPartyUserInfo) (obj1)).isExpired());
            if (!((ThirdPartyUserInfo) (obj1)).getIdentity().equals("1")) goto _L5; else goto _L4
_L4:
            if (!"".equals(shareList)) goto _L7; else goto _L6
_L6:
            shareList = "tSina";
              goto _L8
_L7:
            try
            {
                shareList = (new StringBuilder()).append(shareList).append("tSina").toString();
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                return "";
            }
              goto _L8
_L5:
            if (!((ThirdPartyUserInfo) (obj1)).getIdentity().equals("2")) goto _L8; else goto _L9
_L9:
            if (!"".equals(shareList))
            {
                break MISSING_BLOCK_LABEL_218;
            }
            shareList = "qzone";
              goto _L8
            shareList = (new StringBuilder()).append(shareList).append("qzone").toString();
              goto _L8
            if (!isFirstLike || paramFavorite) goto _L2; else goto _L10
_L10:
            aobj = ApiUtil.getApiHost();
            int i = 0;
_L17:
            if (i >= list.size()) goto _L12; else goto _L11
_L11:
            aobj = (new StringBuilder()).append(((String) (aobj))).append("mobile/sync/").append(((ThirdPartyUserInfo)list.get(i)).thirdpartyId).append("/set").toString();
            obj1 = new RequestParams();
            ((RequestParams) (obj1)).put("type", "favorite");
            ((RequestParams) (obj1)).put("isChecked", String.valueOf(isShare));
            obj1 = f.a().b(((String) (aobj)), ((RequestParams) (obj1)), mLikeBtn, null);
            if (obj1 == null) goto _L14; else goto _L13
_L13:
            obj1 = JSON.parseObject(((String) (obj1)));
            if (obj1 == null) goto _L14; else goto _L15
_L15:
            if (((JSONObject) (obj1)).getInteger("ret").intValue() == 0)
            {
                ((ThirdPartyUserInfo)list.get(i)).setWebFavorite(isShare);
            }
              goto _L14
_L12:
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_432;
            }
            obj.bindStatus = list;
            saveFirstLike();
_L2:
            aobj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/favorite/track").toString();
            obj = new RequestParams();
            ((RequestParams) (obj)).put("trackId", String.valueOf(trackId));
            boolean flag;
            if (!paramFavorite)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            ((RequestParams) (obj)).put("favorite", String.valueOf(flag));
            if (!"".equals(shareList))
            {
                ((RequestParams) (obj)).put("shareList", String.valueOf(shareList));
            }
            aobj = f.a().b(((String) (aobj)), ((RequestParams) (obj)), mLikeBtn, null);
            if (aobj == null)
            {
                break MISSING_BLOCK_LABEL_578;
            }
            aobj = JSON.parseObject(((String) (aobj)));
            if (((JSONObject) (aobj)).getInteger("ret").intValue() == 0)
            {
                return "0";
            }
            aobj = ((JSONObject) (aobj)).getString("msg");
            return ((String) (aobj));
            return "";
_L14:
            i++;
            if (true) goto _L17; else goto _L16
_L16:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if ("0".equals(s))
            {
                if (!paramFavorite)
                {
                    paramFavorite = true;
                    str = "\u8D5E\u6210\u529F";
                } else
                {
                    paramFavorite = false;
                    str = "\u53D6\u6D88\u8D5E\u6210\u529F";
                }
                DownloadHandler.getInstance(mContext.getApplicationContext()).updateFavorited(trackId, paramFavorite, false);
                if (mContext != null && isAdded())
                {
                    if (mLikeBtn != null)
                    {
                        if (paramFavorite)
                        {
                            mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020346, 0, 0);
                        } else
                        {
                            mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020345, 0, 0);
                        }
                        onSoundLiked(paramFavorite);
                    }
                    mNeedUpdate = true;
                    updateUI();
                }
            } else
            if ("".equals(s))
            {
                str = "\u7F51\u7EDC\u8FDE\u63A5\u5931\u8D25";
            } else
            {
                str = s;
            }
            Toast.makeText(mAppContext.getApplicationContext(), str, 0).show();
        }

        LikeTask()
        {
            this$0 = PlayerFragment.this;
            super();
            str = "";
            shareList = "";
        }
    }

    class LoadAlbumTask extends MyAsyncTask
    {

        final PlayerFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = PlayListControl.getPlayListManager().getCurSound();
            if (avoid != null)
            {
                if ((avoid = CommonRequest.getAlbumList(mContext, ((SoundInfo) (avoid)).trackId, ((SoundInfo) (avoid)).albumId, mSoundInfo.uid, ((SoundInfo) (avoid)).albumName, ((SoundInfo) (avoid)).albumCoverPath, ((SoundInfo) (avoid)).recSrc, ((SoundInfo) (avoid)).recTrack, fragmentBaseContainerView, null)) != null)
                {
                    return ((SoundInfoList) (avoid)).data;
                }
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            while (mContext == null || ((Activity)mContext).isFinishing() || list == null) 
            {
                return;
            }
            if (!list.equals(mAlbumSounds) && isNeedUpdatePlaylist())
            {
                PlayListControl.getPlayListManager().updatePlaylist(list);
            }
            updateControlButtons();
        }

        protected void onPreExecute()
        {
        }

        LoadAlbumTask()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }

    class LoadShareSetting extends MyAsyncTask
    {

        final PlayerFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/sync/get").toString();
            RequestParams requestparams = new RequestParams();
            avoid = f.a().a(avoid, requestparams, null, null);
            if (avoid == null)
            {
                break MISSING_BLOCK_LABEL_96;
            }
            JSONObject jsonobject;
            jsonobject = JSON.parseObject(avoid);
            if (jsonobject.getInteger("ret").intValue() != 0)
            {
                break MISSING_BLOCK_LABEL_89;
            }
            parseData(avoid);
            SharedPreferencesUtil.getInstance(mContext).saveString("share_setting", avoid);
            return "0";
            try
            {
                return jsonobject.getString("msg");
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                return "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
            }
            return "\u7F51\u7EDC\u8BBF\u95EE\u5F02\u5E38";
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            mIsLoadShareSetting = false;
            if (mContext == null || !isAdded())
            {
                return;
            }
            if (!"0".equals(s))
            {
                s = SharedPreferencesUtil.getInstance(mContext).getString("share_setting");
                parseData(s);
            }
            refreshShareSetting();
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            mIsLoadShareSetting = true;
        }

        LoadShareSetting()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }

    class LoadSoundDetailTask extends MyAsyncTask
    {

        final PlayerFragment this$0;

        protected transient Integer doInBackground(String as[])
        {
            as = new RequestParams();
            as.put("trackId", (new StringBuilder()).append(mSoundInfo.trackId).append("").toString());
            as = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/track/detail").toString(), as, fragmentBaseContainerView, mHeader, false);
            if (((com.ximalaya.ting.android.b.n.a) (as)).b == 1)
            {
                as = ((com.ximalaya.ting.android.b.n.a) (as)).a;
            } else
            {
                as = null;
            }
            if (Utilities.isBlank(as))
            {
                return Integer.valueOf(1);
            }
            try
            {
                mSoundInfoDetail = (SoundInfoDetail)JSON.parseObject(as, com/ximalaya/ting/android/model/sound/SoundInfoDetail);
                if (mSoundInfoDetail != null && mSoundInfoDetail.userInfo == null)
                {
                    mSoundInfoDetail.userInfo = new com.ximalaya.ting.android.model.sound.SoundInfoDetail.UserInfo();
                    mSoundInfoDetail.userInfo.uid = mSoundInfo.uid;
                    mSoundInfoDetail.userInfo.nickname = mSoundInfo.nickname;
                }
            }
            // Misplaced declaration of an exception variable
            catch (String as[])
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(as.getMessage()).append(Logger.getLineInfo()).toString());
            }
            if (mSoundInfoDetail == null)
            {
                return Integer.valueOf(2);
            } else
            {
                return Integer.valueOf(3);
            }
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (mContext == null || !isAdded())
            {
                return;
            }
            if (integer.intValue() == 3 && mSoundInfoDetail != null)
            {
                if (mSoundInfoDetail.ret == 0)
                {
                    updateOwnerInfoBar();
                    long l = mSoundInfo.uid;
                    ModelHelper.updateSoundInfo(mSoundInfoDetail, mSoundInfo);
                    HistoryManage.getInstance(getActivity()).putSound(mSoundInfo);
                    int i;
                    if (isSortedPlaylist() || mSoundInfo.albumId == -1L)
                    {
                        mSoundInfo.uid = l;
                        mSoundInfo.actualAlbumId = mSoundInfoDetail.albumId;
                    } else
                    {
                        mSoundInfo.albumId = mSoundInfoDetail.albumId;
                        if (PlayListControl.getPlayListManager().getCurSound() != null)
                        {
                            PlayListControl.getPlayListManager().getCurSound().albumId = mSoundInfoDetail.albumId;
                        }
                    }
                    updateSoundInfoBar();
                    updateSoundCoverImage(mSoundInfo.coverLarge);
                    buildViewPager(mSoundInfoDetail);
                    updateDownloadBtnStatus(mSoundInfo.trackId);
                    TingMediaPlayer.getTingMediaPlayer(mAppContext).updateNotificationBigView();
                } else
                {
                    Toast.makeText(mContext, mSoundInfoDetail.msg, 0).show();
                }
            }
            i = PlayListControl.getPlayListManager().listType;
            if (mPlayedSoundAlbumId != mSoundInfo.albumId || mPlayedSoundAlbumId == -1L && mSoundInfo.albumId == -1L || i != mPlaySource)
            {
                integer = PlayListControl.getPlayListManager().getPlaylist().getData();
                if (mSoundInfo.albumId > 0L && PlayListControl.getPlayListManager().listType != 1)
                {
                    loadSoundsData();
                } else
                if (integer == null || integer.equals(mAlbumSounds));
            }
            mPlaySource = i;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            mSoundInfoDetail = null;
        }

        LoadSoundDetailTask()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }

    public class PayFinishReceiver extends BroadcastReceiver
    {

        final PlayerFragment this$0;

        public void onReceive(Context context, Intent intent)
        {
            if ("com.ximalaya.ting.android.pay.ACTION_PAY_SHARE".equals(intent.getAction()) && mSoundInfo != null)
            {
                (new BaseShareDialog(getActivity(), mSoundInfo, mShareBtn)).show();
            }
        }

        public PayFinishReceiver()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }

    class SendCommentTask extends MyAsyncTask
    {

        ProgressDialog pd;
        final PlayerFragment this$0;

        protected transient CommentModel doInBackground(Object aobj[])
        {
            LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
            if (logininfomodel == null || mSoundInfo == null)
            {
                return null;
            }
            String s = (String)aobj[0];
            Object obj = (String)aobj[1];
            String s1 = (String)aobj[2];
            String s2 = (String)aobj[3];
            String s3 = (String)aobj[4];
            int i;
            if (!TextUtils.isEmpty(((CharSequence) (obj))) && !"null".equals(obj))
            {
                i = Integer.parseInt(((String) (obj)));
            } else
            {
                i = 1;
            }
            aobj = ApiUtil.getApiHost();
            if (i == 1)
            {
                aobj = (new StringBuilder()).append(((String) (aobj))).append("mobile/comment/create").toString();
            } else
            {
                aobj = (new StringBuilder()).append(((String) (aobj))).append("mobile/track/relay").toString();
            }
            obj = new RequestParams();
            ((RequestParams) (obj)).add("uid", (new StringBuilder()).append("").append(logininfomodel.uid).toString());
            ((RequestParams) (obj)).add("token", logininfomodel.token);
            ((RequestParams) (obj)).add("trackId", (new StringBuilder()).append("").append(mSoundInfo.trackId).toString());
            ((RequestParams) (obj)).add("content", s);
            if (Utilities.isNotBlank(s1))
            {
                ((RequestParams) (obj)).add("parentId", s1);
            }
            if (Utilities.isNotBlank(s3))
            {
                ((RequestParams) (obj)).add("second", s3);
            }
            if (Utilities.isNotBlank(s2))
            {
                ((RequestParams) (obj)).add("shareList", s2);
            }
            aobj = f.a().a(((String) (aobj)), ((RequestParams) (obj)), null);
            if (((com.ximalaya.ting.android.b.n.a) (aobj)).b == 1)
            {
                aobj = ((com.ximalaya.ting.android.b.n.a) (aobj)).a;
            } else
            {
                aobj = null;
            }
            Logger.log((new StringBuilder()).append("resultJosn=").append(((String) (aobj))).toString());
            if (!Utilities.isNotBlank(((String) (aobj))))
            {
                break MISSING_BLOCK_LABEL_353;
            }
            aobj = (CommentModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/comment/CommentModel);
            return ((CommentModel) (aobj));
            aobj;
            ((Exception) (aobj)).printStackTrace();
            return null;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(CommentModel commentmodel)
        {
            if (mContext == null || !isAdded())
            {
                return;
            }
            if (pd != null)
            {
                pd.cancel();
                pd = null;
            }
            if (commentmodel == null)
            {
                Toast.makeText(mContext, "\u56DE\u590D\u5931\u8D25", 1).show();
                return;
            }
            if (commentmodel.ret != 0) goto _L2; else goto _L1
_L1:
            if (mCommentType != 1) goto _L4; else goto _L3
_L3:
            showToast("\u56DE\u590D\u6210\u529F");
            if (mCommentList != null)
            {
                CommentModelList commentmodellist = mCommentList;
                commentmodellist.totalCount = commentmodellist.totalCount + 1;
            }
            if (PlayListControl.getPlayListManager().getCurSound() != null)
            {
                SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
                soundinfo.comments_counts = soundinfo.comments_counts + 1;
            }
            if (mSoundInfoDetail != null)
            {
                SoundInfoDetail soundinfodetail = mSoundInfoDetail;
                soundinfodetail.comments = soundinfodetail.comments + 1;
            }
            updateOwnerInfoBar();
            mComments.add(0, commentmodel);
            if (viewPagerAdapter != null)
            {
                viewPagerAdapter.getCommentFragment().setData(mCommentList);
                viewPagerAdapter.getCommentFragment().addData(commentmodel);
                showNoCommentViewOrNot();
            } else
            {
                updateSoundDetailData();
            }
_L5:
            mEmotionSelector.setText("");
            mEmotionSelector.setVisibility(8);
            mSyncBar.setVisibility(8);
            mBottomMenuBar.setVisibility(0);
            return;
_L4:
            if (mCommentType == 2)
            {
                showToast("\u8F6C\u91C7\u6210\u529F");
                if (PlayListControl.getPlayListManager().getCurSound() != null)
                {
                    commentmodel = PlayListControl.getPlayListManager().getCurSound();
                    commentmodel.shares_counts = ((SoundInfo) (commentmodel)).shares_counts + 1;
                }
                if (mSoundInfoDetail != null)
                {
                    commentmodel = mSoundInfoDetail;
                    commentmodel.shares = ((SoundInfoDetail) (commentmodel)).shares + 1;
                }
            }
            if (true) goto _L5; else goto _L2
_L2:
            Toast.makeText(mAppContext, commentmodel.msg, 1).show();
            return;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((CommentModel)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            pd = new MyProgressDialog(mContext);
            if (mCommentType != 1) goto _L2; else goto _L1
_L1:
            pd.setTitle("\u56DE\u590D");
            pd.setMessage("\u6B63\u5728\u5E2E\u60A8\u52AA\u529B\u56DE\u590D\u4E2D...");
_L4:
            pd.show();
            return;
_L2:
            if (mCommentType == 2)
            {
                pd.setTitle("\u8F6C\u91C7");
                pd.setMessage("\u6B63\u5728\u5E2E\u60A8\u52AA\u529B\u8F6C\u91C7\u4E2D...");
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        SendCommentTask()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }

    public class WifiControlHandler extends Handler
    {

        final PlayerFragment this$0;

        public void handleMessage(Message message)
        {
            Logger.d("WiFi", (new StringBuilder()).append("WifiControlHandler:handleMessage:msg:").append(message.what).append(",").append(message.arg1).toString());
            message.what;
            JVM INSTR tableswitch 2 24: default 148
        //                       2 148
        //                       3 242
        //                       4 221
        //                       5 187
        //                       6 204
        //                       7 263
        //                       8 273
        //                       9 148
        //                       10 148
        //                       11 148
        //                       12 148
        //                       13 148
        //                       14 148
        //                       15 148
        //                       16 148
        //                       17 163
        //                       18 148
        //                       19 148
        //                       20 148
        //                       21 148
        //                       22 148
        //                       23 148
        //                       24 284;
               goto _L1 _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L1 _L1 _L1 _L1 _L1 _L1 _L1 _L1 _L8 _L1 _L1 _L1 _L1 _L1 _L1 _L9
_L1:
            Logger.d("tuisong", "handleMessage");
            onUpdatePlayBtn();
            return;
_L8:
            Logger.d("doss", "\u6536\u5230\u6B4C\u66F2\u53D8\u5316\u547D\u4EE4");
            onDeviceSoundTransition((BaseCurrentPlayingModel)message.obj);
            continue; /* Loop/switch isn't completed */
_L4:
            Logger.d("doss", "\u4E0A\u4E00\u9996");
            onDeviceChangePre();
            continue; /* Loop/switch isn't completed */
_L5:
            Logger.d("doss", "\u4E0B\u4E00\u9996");
            onDeviceChangeNext();
            continue; /* Loop/switch isn't completed */
_L3:
            Logger.d("doss", "\u6536\u5230PLAYING");
            getDlnaManager().setNowPlayState(1);
            continue; /* Loop/switch isn't completed */
_L2:
            Logger.d("doss", "\u6536\u5230PAUSE");
            getDlnaManager().setNowPlayState(0);
            continue; /* Loop/switch isn't completed */
_L6:
            onDeviceChangeSound();
            continue; /* Loop/switch isn't completed */
_L7:
            change2Bendi(true);
            continue; /* Loop/switch isn't completed */
_L9:
            message = (String)message.obj;
            Logger.d("tuisong", (new StringBuilder()).append("ARG_STORAGEMEDIA_CHANGE:").append(message).toString());
            if (!message.equals("SONGLIST-NETWORK"))
            {
                Logger.d("doss", "\u6536\u5230\u64AD\u653E\u65B9\u5F0F\u5207\u6362");
                if (DlnaManager.getInstance(mAppContext).getLinkedDeviceModel() != null && DlnaManager.getInstance(mAppContext).getLinkedDeviceModel().getNowDeviceItem().getDlnaType() == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss)
                {
                    change2Bendi(true);
                    getDlnaManager().setNowPlayState(0);
                }
            }
            if (true) goto _L1; else goto _L10
_L10:
        }

        public WifiControlHandler()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }

    public class WifiMsg
    {

        public static final int ARG_CHANNEL_PLAY = 8;
        public static final int ARG_MEDIA_ADD = 22;
        public static final int ARG_SEEKBAR_FINISH = 1;
        public static final int ARG_SOUND_CHANNEL_NEXT = 16;
        public static final int ARG_SOUND_CHANNEL_PLAY = 7;
        public static final int ARG_SOUND_CHANNEL_PRE = 15;
        public static final int ARG_SOUND_FRESH = 2;
        public static final int ARG_SOUND_NEXT = 6;
        public static final int ARG_SOUND_PAUSE = 3;
        public static final int ARG_SOUND_PLAY = 4;
        public static final int ARG_SOUND_PLAYLISTCHANGE = 18;
        public static final int ARG_SOUND_PRE = 5;
        public static final int ARG_SOUND_TRANSITIONING = 17;
        public static final int ARG_STORAGEMEDIA_CHANGE = 24;
        public static final int ARG_TASK_RENEW = 21;
        public static final int ARG_TASK_STATUS = 20;
        public static final int WHAT = 7;
        final PlayerFragment this$0;

        public WifiMsg()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private static final int ADS_SWAP_TIME = 5000;
    private static final int GOBINDWEIBO_REQUESTCODE = 1114;
    private static final int GOSELECTCAREPERSON_REQUESTCODE = 175;
    private static final int MAX_COMMENT_CHAR_COUNT = 140;
    private static final int OPERAION_ALARM = 1;
    private static final int OPERATION_CANCEL = 2;
    private static final int OPERATION_TIMER_SHUTDOWN = 0;
    public static final String TAG = "WiFi";
    public static boolean mIsSetSeekBar = true;
    public static boolean mIsShowAds = true;
    private ViewGroup adLayout;
    public IActionCallBack callBack;
    private int handlerCount;
    private boolean isDeviceControl;
    private boolean isDoLikeForToast;
    private boolean isSearching;
    private boolean isStatDashangShow;
    private int lastBindWeibo;
    private int likeCount;
    private ImageView mAdImageView;
    private ImageView mAdLogo;
    private ArrayList mAlbumSounds;
    private Context mAppContext;
    private Runnable mAutoSwapAdsTask;
    private ImageView mBackBtn;
    private ImageView mBindQzoneImg;
    private ImageView mBindRennImg;
    private ImageView mBindSinaImg;
    private ImageView mBindTqqImg;
    private BlurableImageView mBlurCoverBg;
    private RelativeLayout mBottomMenuBar;
    public LocalMediaService mBoundService;
    private int mBufferPercent;
    private ImageView mCarePeopleImg;
    private ImageView mCloseAds;
    private List mCommTopAdList;
    private TextView mCommentBtn;
    private Queue mCommentFragmentCreatedWorkQueue;
    private CommentModelList mCommentList;
    private TextView mCommentTips;
    private int mCommentType;
    private List mComments;
    private ServiceConnection mConnection;
    private Context mContext;
    private int mCoverImageIndex;
    private DeleteCommentTask mDeleteCommentTask;
    private ImageView mDeviceBtn;
    private MenuDialog mDeviceMenuDialog;
    private TextView mDownloadBtn;
    private String mDurationFormatText;
    private EmotionSelector mEmotionSelector;
    private boolean mEnableCache;
    private ImageView mExitTimerBtn;
    private TextView mFollow;
    private View mFollowContainer;
    private ImageView mFollowIcon;
    private RelativeLayout mFootView;
    private RelativeLayout mFooterViewLoading;
    private boolean mForbidProgressUpdate;
    private Handler mHandler;
    private View mHeader;
    private int mHeaderHeight;
    private ImageViewer mImageViewer;
    private boolean mIsBindQQ;
    private boolean mIsBindQZone;
    private ImageView mIsBindQzoneImg;
    private boolean mIsBindRenn;
    private ImageView mIsBindRennImg;
    private boolean mIsBindSina;
    private ImageView mIsBindSinaImg;
    private ImageView mIsBindTqqImg;
    private boolean mIsBound;
    private boolean mIsChangeForAd;
    private boolean mIsCommentFragmentCreated;
    private boolean mIsLoadShareSetting;
    private boolean mIsLoading;
    private boolean mIsShareQQ;
    private boolean mIsShareQZone;
    private boolean mIsShareRenn;
    private boolean mIsShareSina;
    protected boolean mIsSoundAdShowing;
    private boolean mIsStopSwapAd;
    private TextView mLikeBtn;
    private LikeTask mLikeTask;
    private View mListHeader;
    private LoadAlbumTask mLoadAlbumTask;
    private LoadCommentTask mLoadCommentTask;
    private LoadShareSetting mLoadShareSetting;
    private LoadSoundDetailTask mLoadSoundDetailTask;
    private int mLocalProcess;
    private int mMinHeaderTranslation;
    private TextView mMoreBtn;
    private LinearLayout mMovingTimeBar;
    private ImageView mMyCarePeoples;
    private boolean mNeedUpdate;
    private ImageView mNextSoundBtn;
    private LinearLayout mNoCommentView;
    private AppAd mNowAd;
    private int mNowAdNum;
    private View mOwnerClickArea;
    private TextView mOwnerFunCountLabel;
    private TextView mOwnerFunsCount;
    private TextView mOwnerPosition;
    private TextView mOwnerSoundCount;
    private TextView mOwnerSoundCountLabel;
    private int mPageId;
    private int mPageSize;
    private long mParentId;
    private TextView mPlayCount;
    private PlayListHostoryFragment mPlayHistoryFragment;
    private TextView mPlayListBtn;
    private TextView mPlayModeBtn;
    private MenuDialog mPlayMoreDiaolog;
    private Button mPlayOrPauseBtn;
    private int mPlaySource;
    private long mPlayedSoundAlbumId;
    private PlaylistFragment mPlaylistFragment;
    private ImageView mPreSoundBtn;
    private TextView mProgressLabel;
    private RadioGroup mRadioGroup;
    private PayFinishReceiver mReceiver;
    private RedirectTouchRelativeLayout mRedirectView;
    private Resources mResources;
    private View mRightSpace;
    private MySeekBar mSeekBar;
    private SendCommentTask mSendCommentTask;
    private TextView mShareBtn;
    private List mShareSettingList;
    private TextView mShowShareToastTv;
    private AbstractSoundAdModel mSoundAd;
    private ImageView mSoundCover;
    private SoundInfo mSoundInfo;
    private SoundInfoDetail mSoundInfoDetail;
    private ImageView mSoundOwnerFlag;
    private ImageView mSoundOwnerIcon;
    private TextView mSoundOwnerName;
    private TextView mSoundSourceType;
    private TextView mSoundTitle;
    private LinearLayout mSyncBar;
    private ImageView mTopCommAdImageView;
    private ViewPager mViewPager;
    private ProgressBar mWaittingProgressBar;
    private WifiControlHandler mWifiControlHandler;
    private ImageView newFeatureImg;
    private TextView topTextView;
    private PlayerViewPagerAdapter viewPagerAdapter;

    public PlayerFragment()
    {
        mNeedUpdate = false;
        mEnableCache = false;
        mBoundService = null;
        mIsBound = false;
        mIsLoading = false;
        mPageId = 1;
        mPageSize = 15;
        mBufferPercent = 0;
        mForbidProgressUpdate = false;
        mDurationFormatText = null;
        mIsBindQQ = false;
        mIsBindSina = false;
        mIsBindQZone = false;
        mIsBindRenn = false;
        mIsShareQQ = false;
        mIsShareSina = false;
        mIsShareQZone = false;
        mIsShareRenn = false;
        likeCount = 0;
        mHandler = new Handler(Looper.getMainLooper());
        isDoLikeForToast = false;
        mCommentFragmentCreatedWorkQueue = new LinkedList();
        isSearching = false;
        isDeviceControl = false;
        mAlbumSounds = new ArrayList();
        mConnection = new _cls55();
        handlerCount = 0;
        mIsStopSwapAd = true;
        mAutoSwapAdsTask = new _cls58();
        mComments = new ArrayList();
        mCommentType = 1;
        mIsLoadShareSetting = false;
        callBack = new _cls63();
        mWifiControlHandler = new WifiControlHandler();
    }

    private void afterDeviceControl()
    {
        freshDeviceBtn();
        getDlnaManager().setRegister(this);
    }

    private void buildTabRadioGroup()
    {
        RadioButton radiobutton = (RadioButton)mHeader.findViewById(0x7f0a0080);
        RadioButton radiobutton1 = (RadioButton)mHeader.findViewById(0x7f0a0081);
        RadioButton radiobutton2 = (RadioButton)mHeader.findViewById(0x7f0a014b);
        radiobutton.setTextSize(13F);
        radiobutton1.setTextSize(13F);
        radiobutton2.setTextSize(13F);
        Object obj = getResources().getXml(0x7f02040d);
        android.content.res.XmlResourceParser xmlresourceparser = getResources().getXml(0x7f02040d);
        android.content.res.XmlResourceParser xmlresourceparser1 = getResources().getXml(0x7f02040d);
        int i;
        try
        {
            radiobutton.setTextColor(ColorStateList.createFromXml(getResources(), ((org.xmlpull.v1.XmlPullParser) (obj))));
            radiobutton1.setTextColor(ColorStateList.createFromXml(getResources(), xmlresourceparser));
            radiobutton2.setTextColor(ColorStateList.createFromXml(getResources(), xmlresourceparser1));
        }
        catch (XmlPullParserException xmlpullparserexception)
        {
            xmlpullparserexception.printStackTrace();
        }
        catch (IOException ioexception)
        {
            ioexception.printStackTrace();
        }
        radiobutton.setBackgroundResource(0x7f020478);
        radiobutton1.setBackgroundResource(0x7f020475);
        radiobutton2.setBackgroundResource(0x7f02047b);
        mHeader.setBackgroundColor(-1);
        obj = (new StringBuilder()).append("\u8BC4\u8BBA(");
        if (mCommentList == null)
        {
            i = 0;
        } else
        {
            i = mCommentList.totalCount;
        }
        radiobutton1.setText(((StringBuilder) (obj)).append(i).append(")").toString());
        radiobutton.setText("\u8BE6\u60C5");
        radiobutton2.setText("\u76F8\u5173\u63A8\u8350");
    }

    private void buildViewPager(SoundInfoDetail soundinfodetail)
    {
        if (viewPagerAdapter == null)
        {
            viewPagerAdapter = new PlayerViewPagerAdapter(getChildFragmentManager(), soundinfodetail);
            viewPagerAdapter.setHeaderHeight(mHeaderHeight);
            viewPagerAdapter.setOnActivityCreateCallback(new _cls1());
            viewPagerAdapter.setTabHolderScrollingContent(new _cls2());
            viewPagerAdapter.setCommentListItemClickListener(new _cls3());
            mViewPager.setAdapter(viewPagerAdapter);
        }
        updateRelativeAlbumData(soundinfodetail);
        updateSoundDetailFragment();
        if (!mIsCommentFragmentCreated)
        {
            mCommentFragmentCreatedWorkQueue.add(new _cls4());
            return;
        } else
        {
            updateSoundCommentsData(fragmentBaseContainerView);
            return;
        }
    }

    private void changeSeekBar(int i)
    {
        Logger.d("WIFI", (new StringBuilder()).append("changeSeekBar is:").append(i).toString());
        LocalMediaService localmediaservice;
        if (!isDlnaInit() || !isDlnaDeviceLinked())
        {
            if ((localmediaservice = LocalMediaService.getInstance()) != null)
            {
                updateMoveTime(i, localmediaservice.getDuration());
                mMovingTimeBar.postDelayed(new _cls64(), 1000L);
                return;
            }
        }
    }

    private void clearDlnaListener()
    {
        if (!isDlnaInit())
        {
            return;
        } else
        {
            getDlnaManager().removeDeviceChangedListener(this);
            getDlnaManager().removeOnkeyEventListener(this);
            return;
        }
    }

    private void clickCommentItem(final int pt)
    {
        if (mComments.size() != 0 && viewPagerAdapter.getCommentFragment().getListView() != null)
        {
            Object obj = new ArrayList();
            final MenuDialog mCommentMenuDialog = new MenuDialog(getActivity(), ((List) (obj)));
            pt -= viewPagerAdapter.getCommentFragment().getListView().getHeaderViewsCount();
            if (pt >= 0 && pt < mComments.size())
            {
                final CommentModel comment = (CommentModel)mComments.get(pt);
                mParentId = comment.id;
                final SoundInfo sound = PlayListControl.getPlayListManager().getCurSound();
                LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
                if (UserInfoMannage.hasLogined() && logininfomodel != null && logininfomodel.uid == comment.uid)
                {
                    ((List) (obj)).add("\u5220\u9664");
                    obj = new _cls21();
                } else
                {
                    ((List) (obj)).add("\u67E5\u770B\u8D44\u6599");
                    ((List) (obj)).add("\u56DE\u590D");
                    ((List) (obj)).add("\u4E3E\u62A5\u8BC4\u8BBA");
                    obj = new _cls22();
                }
                mCommentMenuDialog.setOnItemClickListener(((android.widget.AdapterView.OnItemClickListener) (obj)));
                mCommentMenuDialog.show();
                return;
            }
        }
    }

    private void closeShareToast()
    {
        mShowShareToastTv.setVisibility(8);
    }

    private void doBindService()
    {
        mContext.bindService(new Intent(mContext, com/ximalaya/ting/android/service/play/LocalMediaService), mConnection, 1);
    }

    private void doLikeAction(final boolean isFirstLike, final SoundInfo info)
    {
        if (info.is_favorited)
        {
            (new DialogBuilder(mContext)).setMessage("\u786E\u5B9A\u8981\u53D6\u6D88\u8D5E\uFF1F").setOkBtn(new _cls44()).setCancelBtn(new _cls43()).showConfirm();
            return;
        } else
        {
            ToolUtil.onEvent(mContext, "Nowplaying_Follow");
            mLikeTask = new LikeTask();
            mLikeTask.myexec(new Object[] {
                Long.valueOf(info.trackId), Boolean.valueOf(true), Boolean.valueOf(info.is_favorited), Boolean.valueOf(isFirstLike)
            });
            return;
        }
    }

    private void doSetAlarm(DownloadTask downloadtask)
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mContext);
        String s = sharedpreferencesutil.getString("alarm_ringtone_location");
        String s1 = sharedpreferencesutil.getString("alarm_ringtone_download_url");
        String s2 = sharedpreferencesutil.getString("alarm_ringtone_title");
        if (isStrEquals(s, downloadtask.downloadLocation) && isStrEquals(s1, downloadtask.downLoadUrl) && isStrEquals(s2, downloadtask.title))
        {
            return;
        } else
        {
            sharedpreferencesutil.saveString("alarm_ringtone_location", downloadtask.downloadLocation);
            sharedpreferencesutil.saveString("alarm_ringtone_download_url", downloadtask.downLoadUrl);
            sharedpreferencesutil.saveString("alarm_ringtone_title", downloadtask.title);
            return;
        }
    }

    private void doSetCallingRingtone()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mContext);
        long l = sharedpreferencesutil.getLong("calling_ringtone_trackid");
        sharedpreferencesutil.saveBoolean("new_feature_ringtone", true);
        if (l == mSoundInfo.trackId)
        {
            File file = new File(StorageUtils.getRingtoneDir(), (new StringBuilder()).append(String.valueOf(l)).append(".mp3").toString());
            if (!file.exists())
            {
                doStartDownloadRingtone();
                return;
            } else
            {
                RingtoneUtil.setMyRingtone(getActivity(), Uri.fromFile(file).toString(), RingtoneUtil.buildMediaInfo(mSoundInfo.title, "\u559C\u9A6C\u62C9\u96C5\u94C3\u58F0", mSoundInfo.nickname));
                Toast.makeText(getActivity(), (new StringBuilder()).append("\u5DF2\u5C07\"").append(mSoundInfo.title).append("\"\u8BBE\u4E3A\u624B\u673A\u94C3\u58F0").toString(), 0).show();
                return;
            }
        } else
        {
            doStartDownloadRingtone();
            return;
        }
    }

    private void doShareQQ(boolean flag)
    {
        if (flag)
        {
            mIsShareQQ = true;
            mBindTqqImg.setImageResource(0x7f02058c);
            mIsBindTqqImg.setVisibility(0);
            return;
        } else
        {
            mIsShareQQ = false;
            mBindTqqImg.setImageResource(0x7f02058d);
            mIsBindTqqImg.setVisibility(4);
            return;
        }
    }

    private void doShareQzone(boolean flag)
    {
        if (flag)
        {
            mIsShareQZone = true;
            mBindQzoneImg.setImageResource(0x7f02045d);
            mIsBindQzoneImg.setVisibility(0);
            return;
        } else
        {
            mIsShareQZone = false;
            mBindQzoneImg.setImageResource(0x7f02045e);
            mIsBindQzoneImg.setVisibility(4);
            return;
        }
    }

    private void doShareRenn(boolean flag)
    {
        if (flag)
        {
            mIsShareRenn = true;
            mBindRennImg.setImageResource(0x7f0204d8);
            mIsBindRennImg.setVisibility(0);
            return;
        } else
        {
            mIsShareRenn = false;
            mBindRennImg.setImageResource(0x7f0204d9);
            mIsBindRennImg.setVisibility(4);
            return;
        }
    }

    private void doShareSina(boolean flag)
    {
        if (flag)
        {
            mIsShareSina = true;
            mBindSinaImg.setImageResource(0x7f020556);
            mIsBindSinaImg.setVisibility(0);
            return;
        } else
        {
            mIsShareSina = false;
            mBindSinaImg.setImageResource(0x7f020557);
            mIsBindSinaImg.setVisibility(4);
            return;
        }
    }

    private void doStartDownloadRingtone()
    {
        CallingRingtoneDownloadDialog callingringtonedownloaddialog = new CallingRingtoneDownloadDialog(getActivity());
        callingringtonedownloaddialog.setDownloadInfo(mSoundInfo);
        callingringtonedownloaddialog.show();
    }

    private void doTransmit()
    {
        if (UserInfoMannage.hasLogined())
        {
            mCommentType = 2;
            initLoginUserBindData();
            loadDefaultShareSetting();
            if (mLoadShareSetting == null || mLoadShareSetting.getStatus() == android.os.AsyncTask.Status.FINISHED)
            {
                mLoadShareSetting = new LoadShareSetting();
                mLoadShareSetting.myexec(new Void[0]);
            }
            mBottomMenuBar.setVisibility(8);
            mEmotionSelector.setHint("\u8F6C\u91C7\u5230\u6211\u7684\u4E3B\u9875,\u5199\u4E00\u53E5\u8BC4\u8BBA");
            mEmotionSelector.setVisibility(0);
            mSyncBar.setVisibility(0);
            return;
        } else
        {
            Intent intent = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
            intent.setFlags(0x20000000);
            startActivity(intent);
            return;
        }
    }

    private void doUnbindService()
    {
        if (mIsBound)
        {
            mContext.unbindService(mConnection);
            mIsBound = false;
        }
    }

    private void downloadFile(String s)
    {
        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(s, new _cls62());
    }

    private void followUser(final long toUid, final View v)
    {
        if (!NetworkUtils.isNetworkAvaliable(mAppContext))
        {
            showToast("\u8FDE\u63A5\u7F51\u7EDC\u5931\u8D25");
            setConcernBtnStatus(mSoundInfo.is_favorited);
        } else
        if (v.getTag() != null && (v.getTag() instanceof Boolean))
        {
            final boolean isFollow = ((Boolean)v.getTag()).booleanValue();
            if (UserInfoMannage.hasLogined())
            {
                ToolUtil.onEvent(mContext, "Account_Follow");
                if (isFollow)
                {
                    (new DialogBuilder(mContext)).setMessage("\u786E\u5B9A\u8981\u53D6\u6D88\u5173\u6CE8\uFF1F").setOkBtn(new _cls46()).setCancelBtn(new _cls45()).showConfirm();
                    return;
                } else
                {
                    setFollowRequest(toUid, isFollow, v);
                    return;
                }
            } else
            {
                v = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                v.setFlags(0x20000000);
                startActivity(v);
                return;
            }
        }
    }

    private void forbidSeek()
    {
        if (mWaittingProgressBar != null)
        {
            mWaittingProgressBar.setVisibility(0);
        }
        if (mSeekBar != null)
        {
            mSeekBar.setCanSeek(false);
        }
    }

    private void freshDeviceBtn()
    {
        while (mActivity == null || !isDlnaInit()) 
        {
            return;
        }
        getDlnaManager().setDeviceChangedListener(this);
        mActivity.runOnUiThread(new _cls69());
    }

    private DlnaManager getDlnaManager()
    {
        return DlnaManager.getInstance(mActivity);
    }

    private String getDurationFormatText(int i)
    {
        if (i <= 0)
        {
            return "00:00";
        }
        if (Utilities.isBlank(mDurationFormatText))
        {
            initDurationFormatText(i);
        }
        return mDurationFormatText;
    }

    private String getPlayModeStr()
    {
        switch (SharedPreferencesUtil.getInstance(mAppContext).getInt("play_mode", 0))
        {
        default:
            return getResources().getString(0x7f0901a8, new Object[] {
                "\u5FAA\u73AF"
            });

        case 0: // '\0'
            return getResources().getString(0x7f0901a8, new Object[] {
                "\u987A\u5E8F"
            });

        case 1: // '\001'
            return getResources().getString(0x7f0901a8, new Object[] {
                "\u5355\u66F2"
            });

        case 2: // '\002'
            return getResources().getString(0x7f0901a8, new Object[] {
                "\u968F\u673A"
            });
        }
    }

    private SoundDetailAdapter.Model getSoundDetail()
    {
        SoundDetailAdapter.Model model = null;
        if (mSoundInfoDetail != null)
        {
            model = new SoundDetailAdapter.Model();
            model.tags = mSoundInfoDetail.tags;
            model.intro = mSoundInfoDetail.intro;
            model.richIntro = mSoundInfoDetail.richIntro;
            model.lrc = mSoundInfoDetail.lyric;
            model.activityName = mSoundInfoDetail.activityName;
            model.activityId = mSoundInfoDetail.activityId;
        }
        return model;
    }

    private int getSoundIndex(long l, List list)
    {
        if (list != null && list.size() != 0) goto _L2; else goto _L1
_L1:
        int j = -1;
_L4:
        return j;
_L2:
        int i = 0;
label0:
        do
        {
label1:
            {
                if (i >= list.size())
                {
                    break label1;
                }
                j = i;
                if (((SoundInfo)list.get(i)).trackId == l)
                {
                    break label0;
                }
                i++;
            }
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
        return -1;
    }

    private void goToWeb(String s)
    {
        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(s, new _cls11());
    }

    private void gotoHomePage(long l, View view)
    {
        Bundle bundle = new Bundle();
        bundle.putLong("toUid", l);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
    }

    private void initAdData()
    {
        while (!mIsShowAds || !mIsChangeForAd) 
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("appid", "0");
        requestparams.put("device", "android");
        requestparams.put("name", "comm_top");
        requestparams.put("trackid", mSoundInfo.trackId);
        requestparams.put("version", ((MyApplication)(MyApplication)getActivity().getApplication()).m());
        String s = (new StringBuilder()).append(a.Q).append("ting/direct").toString();
        f.a().a(s, requestparams, null, new _cls7(), false);
    }

    private void initAdsHeader()
    {
        adLayout = (ViewGroup)View.inflate(getActivity(), 0x7f03014e, null);
        Object obj = new android.widget.AbsListView.LayoutParams(-1, -2);
        adLayout.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        mTopCommAdImageView = (ImageView)adLayout.findViewById(0x7f0a050e);
        mTopCommAdImageView.setVisibility(8);
        obj = new android.widget.RelativeLayout.LayoutParams(-1, ((ToolUtil.getScreenWidth(getActivity()) - ToolUtil.dp2px(getActivity(), 10F) * 2) * 190) / 640);
        mTopCommAdImageView.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        mCloseAds = (ImageView)adLayout.findViewById(0x7f0a04ff);
        mCloseAds.setOnClickListener(new _cls9());
        mTopCommAdImageView.setOnClickListener(new _cls10());
    }

    private void initBottomMenuBar()
    {
        mBottomMenuBar = (RelativeLayout)findViewById(0x7f0a0185);
        mCommentBtn = (TextView)findViewById(0x7f0a0189);
        mDownloadBtn = (TextView)findViewById(0x7f0a0188);
        mLikeBtn = (TextView)findViewById(0x7f0a0187);
        mShareBtn = (TextView)findViewById(0x7f0a018a);
        mMoreBtn = (TextView)findViewById(0x7f0a018b);
        newFeatureImg = (ImageView)findViewById(0x7f0a018c);
        mSyncBar = (LinearLayout)findViewById(0x7f0a018d);
        mCommentTips = (TextView)findViewById(0x7f0a0198);
        mEmotionSelector = (EmotionSelector)findViewById(0x7f0a0199);
        mBindSinaImg = (ImageView)findViewById(0x7f0a018f);
        mIsBindSinaImg = (ImageView)findViewById(0x7f0a0190);
        mBindTqqImg = (ImageView)findViewById(0x7f0a0191);
        mIsBindTqqImg = (ImageView)findViewById(0x7f0a0192);
        mBindQzoneImg = (ImageView)findViewById(0x7f0a0193);
        mIsBindQzoneImg = (ImageView)findViewById(0x7f0a0194);
        mBindRennImg = (ImageView)findViewById(0x7f0a0195);
        mIsBindRennImg = (ImageView)findViewById(0x7f0a0196);
        mCarePeopleImg = (ImageView)findViewById(0x7f0a0197);
        mEmotionSelector.setOnSendButtonClickListener(new _cls13());
        mEmotionSelector.setOnEmotionTextChange(new _cls14());
        mBindSinaImg.setOnClickListener(new _cls15());
        mBindTqqImg.setOnClickListener(new _cls16());
        mBindQzoneImg.setOnClickListener(new _cls17());
        mBindRennImg.setOnClickListener(new _cls18());
        mCarePeopleImg.setOnClickListener(new _cls19());
        mLikeBtn.setOnClickListener(new _cls20());
        setNewFeatruePointVisible();
    }

    private void initDlnaListener()
    {
        getDlnaManager().setOnkeyEventListener(this);
        freshDeviceBtn();
    }

    private void initDurationFormatText(int i)
    {
        if (i > 0)
        {
            mDurationFormatText = ToolUtil.toTime((float)i / 1000F);
        }
    }

    private void initLisnteners()
    {
        mRedirectView.setTouchUpCallback(new _cls25());
        mBackBtn.setOnClickListener(new _cls26());
        mViewPager.setOnPageChangeListener(new _cls27());
        mRadioGroup.setOnCheckedChangeListener(new _cls28());
        fragmentBaseContainerView.findViewById(0x7f0a0310).setOnClickListener(new _cls29());
        if (mSeekBar != null)
        {
            mSeekBar.setProgressNumberFormat(new _cls30());
            mSeekBar.setOnSeekBarChangeListener(new _cls31());
        }
        if (mPlayModeBtn != null)
        {
            mPlayModeBtn.setOnClickListener(new _cls32());
        }
        if (mPreSoundBtn != null)
        {
            mPreSoundBtn.setOnClickListener(new _cls33());
        }
        if (mPlayOrPauseBtn != null)
        {
            mPlayOrPauseBtn.setOnClickListener(new _cls34());
        }
        if (mNextSoundBtn != null)
        {
            mNextSoundBtn.setOnClickListener(new _cls35());
        }
        if (mFollowContainer != null)
        {
            mFollowContainer.setOnClickListener(new _cls36());
        }
        if (mSoundOwnerIcon != null)
        {
            mSoundOwnerIcon.setOnClickListener(new _cls37());
        }
        if (mOwnerClickArea != null)
        {
            mOwnerClickArea.setOnClickListener(new _cls38());
        }
        if (mDownloadBtn != null)
        {
            mDownloadBtn.setOnClickListener(new _cls39());
        }
        if (mShareBtn != null)
        {
            mShareBtn.setOnClickListener(new _cls40());
        }
        if (mCommentBtn != null)
        {
            mCommentBtn.setOnClickListener(new _cls41());
        }
        if (mMoreBtn != null)
        {
            mMoreBtn.setOnClickListener(new _cls42());
        }
    }

    private void initListFootView()
    {
        mFootView = (RelativeLayout)View.inflate(mContext, 0x7f0301fb, null);
        mNoCommentView = (LinearLayout)LayoutInflater.from(mContext).inflate(0x7f03007c, viewPagerAdapter.getCommentFragment().getListView(), false);
        mNoCommentView.setBackgroundColor(-1);
        ((TextView)mNoCommentView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u8FD9\u6761\u58F0\u97F3\u8FD8\u6CA1\u6709\u8BC4\u8BBA\u54E6");
        ((TextView)mNoCommentView.findViewById(0x7f0a0231)).setText("\u8FD8\u7B49\u4EC0\u4E48\uFF0C\u8D76\u7D27\u53BB\u62A2\u6C99\u53D1\u5427");
        Button button = (Button)mNoCommentView.findViewById(0x7f0a0232);
        button.setText("\u53BB\u8BC4\u8BBA");
        button.setOnClickListener(new _cls12());
        mNoCommentView.setVisibility(8);
        viewPagerAdapter.getCommentFragment().getListView().addFooterView(mNoCommentView);
        viewPagerAdapter.getCommentFragment().getListView().addFooterView(mFootView);
        showFooterView(FooterView.LOADING, viewPagerAdapter.getCommentFragment().getListView(), mFootView);
    }

    private void initLoginUserBindData()
    {
        Object obj;
        if (UserInfoMannage.hasLogined())
        {
            if (((LoginInfoModel) (obj = UserInfoMannage.getInstance().getUser())).bindStatus != null && ((LoginInfoModel) (obj)).bindStatus != null)
            {
                obj = ((LoginInfoModel) (obj)).bindStatus.iterator();
                while (((Iterator) (obj)).hasNext()) 
                {
                    ThirdPartyUserInfo thirdpartyuserinfo = (ThirdPartyUserInfo)((Iterator) (obj)).next();
                    if (!thirdpartyuserinfo.isExpired())
                    {
                        if (thirdpartyuserinfo.getIdentity().equals("1"))
                        {
                            mIsBindSina = true;
                        } else
                        if (thirdpartyuserinfo.getIdentity().equals("2"))
                        {
                            mIsBindQQ = true;
                        } else
                        if (thirdpartyuserinfo.getIdentity().equals("3"))
                        {
                            mIsBindRenn = true;
                        }
                    }
                }
            }
        }
    }

    private void initPlayerHeader()
    {
        mListHeader = mHeader.findViewById(0x7f0a0027);
        mAdImageView = (ImageView)mListHeader.findViewById(0x7f0a0120);
        final View soundCoverSection = (ViewGroup)mListHeader.findViewById(0x7f0a052c);
        final int screenWidth = mContext.getResources().getDisplayMetrics().widthPixels;
        int i = mContext.getResources().getDisplayMetrics().heightPixels;
        android.view.ViewGroup.LayoutParams layoutparams = soundCoverSection.getLayoutParams();
        layoutparams.height = i - Utilities.dip2px(mContext, 143F);
        soundCoverSection.setLayoutParams(layoutparams);
        mPlayListBtn = (TextView)mListHeader.findViewById(0x7f0a00cc);
        soundCoverSection = mListHeader.findViewById(0x7f0a05fa);
        mSoundCover = (ImageView)mListHeader.findViewById(0x7f0a00e4);
        mSoundCover.setTag(0x7f0a003a, Boolean.valueOf(true));
        mSoundCover.getViewTreeObserver().addOnPreDrawListener(new _cls48());
        mBlurCoverBg = (BlurableImageView)mListHeader.findViewById(0x7f0a052d);
        mBlurCoverBg.setTag(0x7f0a0024, Integer.valueOf(0));
        mBlurCoverBg.setBackgroundColor(Color.parseColor("#b3202332"));
        mBlurCoverBg.setTag(0x7f0a003b, Boolean.valueOf(true));
        mSoundTitle = (TextView)mListHeader.findViewById(0x7f0a00e6);
        mExitTimerBtn = (ImageView)mListHeader.findViewById(0x7f0a0537);
        mPlayCount = (TextView)mListHeader.findViewById(0x7f0a0151);
        mPlayModeBtn = (TextView)mListHeader.findViewById(0x7f0a0616);
        mPreSoundBtn = (ImageView)mListHeader.findViewById(0x7f0a0615);
        mPlayOrPauseBtn = (Button)mListHeader.findViewById(0x7f0a0523);
        mNextSoundBtn = (ImageView)mListHeader.findViewById(0x7f0a0524);
        mMovingTimeBar = (LinearLayout)mListHeader.findViewById(0x7f0a0533);
        mProgressLabel = (TextView)mListHeader.findViewById(0x7f0a0534);
        mSeekBar = (MySeekBar)mListHeader.findViewById(0x7f0a0528);
        mWaittingProgressBar = (ProgressBar)mListHeader.findViewById(0x7f0a0529);
        mRightSpace = mListHeader.findViewById(0x7f0a0527);
        mShowShareToastTv = (TextView)findViewById(0x7f0a0622);
        mShowShareToastTv.setOnClickListener(new _cls49());
        mSoundOwnerIcon = (ImageView)mListHeader.findViewById(0x7f0a0623);
        mOwnerClickArea = mListHeader.findViewById(0x7f0a06d7);
        mSoundOwnerName = (TextView)mListHeader.findViewById(0x7f0a0624);
        mSoundOwnerFlag = (ImageView)mListHeader.findViewById(0x7f0a0625);
        mSoundSourceType = (TextView)mListHeader.findViewById(0x7f0a06d8);
        mOwnerSoundCount = (TextView)mListHeader.findViewById(0x7f0a0627);
        mOwnerSoundCountLabel = (TextView)mListHeader.findViewById(0x7f0a0626);
        mOwnerFunCountLabel = (TextView)mListHeader.findViewById(0x7f0a0628);
        mOwnerFunsCount = (TextView)mListHeader.findViewById(0x7f0a0629);
        mOwnerPosition = (TextView)mListHeader.findViewById(0x7f0a062a);
        mFollow = (TextView)mListHeader.findViewById(0x7f0a01b9);
        mFollowIcon = (ImageView)mListHeader.findViewById(0x7f0a06da);
        mFollowContainer = mListHeader.findViewById(0x7f0a06d9);
        if (android.os.Build.VERSION.SDK_INT < 11)
        {
            mListHeader.findViewById(0x7f0a0526).setVisibility(8);
            mListHeader.findViewById(0x7f0a0527).setVisibility(8);
            mSeekBar.setThumbOffset(0);
        }
        markImageView(mBlurCoverBg);
        mPlayListBtn.setOnClickListener(new _cls50());
        mExitTimerBtn.setOnClickListener(new _cls51());
        mSoundCover.setOnClickListener(new _cls52());
        mAdLogo.setOnClickListener(new _cls53());
        mIsChangeForAd = true;
        mDeviceBtn = (ImageView)findViewById(0x7f0a047d);
        mDeviceBtn.setOnClickListener(this);
        mDeviceBtn.setVisibility(8);
    }

    private void initPlayerProgressBar()
    {
        while (mSeekBar == null || !SharedPreferencesUtil.getInstance(mContext).getBoolean("historySwitch", true) || mSoundInfo == null || PlayListControl.getPlayListManager() == null || PlayListControl.getPlayListManager().details != null) 
        {
            return;
        }
    }

    private boolean isDlnaDeviceLinked()
    {
        return isDlnaInit() && DlnaManager.getInstance(mActivity).isDeviceLinked();
    }

    private boolean isDlnaInit()
    {
        return MyDeviceManager.getInstance(mActivity).isDlnaInit();
    }

    private boolean isFirstLike()
    {
        return SharedPreferencesUtil.getInstance(mAppContext).getBoolean("isFirstLike", true);
    }

    private boolean isNeedUpdatePlaylist()
    {
        return PlayListControl.getPlayListManager().listType == 10;
    }

    private boolean isSortedPlaylist()
    {
        return PlayListControl.getPlayListManager().listType == 1;
    }

    private boolean isStrEquals(String s, String s1)
    {
        boolean flag = false;
        if (s == null)
        {
            if (s1 == null)
            {
                flag = true;
            }
        } else
        if (s1 != null)
        {
            return s.equals(s1);
        }
        return flag;
    }

    private void likeSound()
    {
        if (mSoundInfo == null)
        {
            return;
        }
        if (!UserInfoMannage.hasLogined())
        {
            startActivity(new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity));
            return;
        }
        if (mLikeTask != null && mLikeTask.getStatus() != android.os.AsyncTask.Status.FINISHED)
        {
            showToast("\u6B63\u5728\u53D1\u9001\u8BF7\u6C42\uFF0C\u8BF7\u7A0D\u5019...");
            return;
        }
        final boolean isFirstLike;
        if (!mSoundInfo.is_favorited)
        {
            mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020346, 0, 0);
            isDoLikeForToast = true;
        } else
        {
            mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020345, 0, 0);
        }
        ToolUtil.onEvent(mContext, "Nowplaying_Like");
        isFirstLike = isFirstLike();
        if (mSoundInfo != null && isFirstLike && !mSoundInfo.is_favorited)
        {
            LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
            if (logininfomodel != null && logininfomodel.bindStatus != null && logininfomodel.bindStatus.size() > 0)
            {
                (new DialogBuilder(mContext)).setMessage("\u5206\u4EAB\u4F60\u559C\u6B22\u7684\u58F0\u97F3\uFF0C\u70B9\u51FB\u540C\u6B65\u5206\u4EAB\u5230\u65B0\u6D6A/QQ\u5FAE\u535A\u3002\u4F60\u4E5F\u53EF\u4EE5\u5728\u5206\u4EAB\u8BBE\u7F6E\u91CC\u5173\u95ED\u540C\u6B65\u5F00\u5173\u54E6\uFF01").setOkBtn("\u540C\u6B65", new _cls24()).setCancelBtn(new _cls23()).showConfirm();
                return;
            } else
            {
                doLikeAction(isFirstLike, mSoundInfo);
                return;
            }
        } else
        {
            doLikeAction(isFirstLike, mSoundInfo);
            return;
        }
    }

    private void loadDefaultShareSetting()
    {
        parseData(SharedPreferencesUtil.getInstance(mContext).getString("share_setting"));
        refreshShareSetting();
    }

    private void loadSoundsData()
    {
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null)
        {
            SoundInfoList soundinfolist = CacheManager.getAlbumPlayList(mContext, soundinfo.albumId);
            if (soundinfolist != null && soundinfolist.data.size() > 0 && soundinfolist.data.contains(soundinfo))
            {
                if (!mAlbumSounds.equals(soundinfolist.data))
                {
                    mAlbumSounds.clear();
                    mAlbumSounds.addAll(soundinfolist.data);
                    if (isNeedUpdatePlaylist())
                    {
                        PlayListControl.getPlayListManager().updatePlaylist(soundinfolist.data);
                    }
                    updateControlButtons();
                }
            } else
            if (mLoadAlbumTask == null || mLoadAlbumTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
            {
                mLoadAlbumTask = new LoadAlbumTask();
                mLoadAlbumTask.myexec(new Void[0]);
                return;
            }
        }
    }

    private void makeAdVisible()
    {
        mTopCommAdImageView.setVisibility(0);
        mCloseAds.setVisibility(0);
        adLayout.setVisibility(0);
        startAutoSwapAds();
    }

    private boolean moreDataAvailable()
    {
        return mCommentList == null || mPageId <= mCommentList.maxPageId;
    }

    private void onDeviceChangeNext()
    {
        mBoundService.playNext(true);
    }

    private void onDeviceChangePre()
    {
        mBoundService.playPrev(true);
    }

    private void onDeviceChangeSound()
    {
        Logger.d("tuisong", "onDeviceChangeSound IN");
        change2Bendi(true);
    }

    private void onDeviceSoundTransition(BaseCurrentPlayingModel basecurrentplayingmodel)
    {
        Logger.d("tuisong", "onDeviceSoundTransition IN");
        break MISSING_BLOCK_LABEL_9;
        if ((!isDlnaInit() || isDlnaDeviceLinked()) && !TextUtils.isEmpty(basecurrentplayingmodel.getCurrentPlayList()) && basecurrentplayingmodel.getCurrentPage() != -1 && basecurrentplayingmodel.getCurrentTrackNum() != -1)
        {
            Logger.d("tuisong", (new StringBuilder()).append("").append(basecurrentplayingmodel.getCurrentPlayList()).append(":").append(basecurrentplayingmodel.getCurrentPage()).append(":").append(basecurrentplayingmodel.getCurrentTrackNum()).toString());
            if (!TextUtils.isEmpty(basecurrentplayingmodel.getCurrentPlayList()) && !basecurrentplayingmodel.getCurrentPlayList().equals(DossUtils.getPlayListName(MyApplication.b())))
            {
                Logger.d("doss", "\u4E0D\u662F\u672C\u673A\u63A8\u9001\u7684\u64AD\u653E\u5217\u8868\uFF0C\u5207\u56DE\u5230\u672C\u5730");
                change2Bendi(true);
                return;
            }
            if (mBoundService != null)
            {
                int i = PlayListControl.getPlayListManager().getPlaylist().getCurrPosition();
                int j = (basecurrentplayingmodel.getCurrentTrackNum() - 1) + DossUtils.mDeviceStartNum;
                Logger.d("doss", (new StringBuilder()).append("\u6536\u5230\u7684\u4F4D\u7F6E\uFF1A").append(basecurrentplayingmodel.getCurrentTrackNum()).toString());
                Logger.d("doss", (new StringBuilder()).append("\u8BB0\u5F55\u7684\u8BBE\u5907\u5185\u5F00\u59CB\u4F4D\u7F6E\uFF1A").append(DossUtils.mDeviceStartNum).toString());
                Logger.d("doss", (new StringBuilder()).append("\u8F6C\u6362\u540E\u5B9E\u9645\u6536\u5230\u7684\u4F4D\u7F6E\uFF1A").append(j).toString());
                Logger.d("doss", (new StringBuilder()).append("\u5F53\u524D\u64AD\u653E\u7684\u6B4C\u66F2\uFF1A").append(i).toString());
                if (i != j)
                {
                    Logger.d("doss", (new StringBuilder()).append("\u6536\u5230\u8DF3\u8F6C\u547D\u4EE4\uFF0C\u8DF3\u8F6C\u5230\u6B4C\u66F2").append(j).toString());
                    isDeviceControl = true;
                    mBoundService.doPlay(j);
                    return;
                } else
                {
                    Logger.d("doss", "\u4E0D\u9700\u8981\u8DF3\u8F6C");
                    return;
                }
            }
        }
        return;
    }

    private void onDlnaSeekBarFinish()
    {
        mIsSetSeekBar = true;
        Logger.log("onDlnaSeekBarFinish unForbidSeek");
        unForbidSeek();
    }

    private void onUpdatePlayBtn()
    {
        Logger.d("tuisong", "onUpdatePlayBtn IN");
        setPlayerBtnState(getDlnaManager().getNowPlayState());
        if (getDlnaManager().getNowPlayState() == 1)
        {
            if (MyApplication.a() != null && (MyApplication.a() instanceof MainTabActivity2))
            {
                ((MainTabActivity2)MyApplication.a()).startPlayAnimation();
            }
        } else
        if (MyApplication.a() != null && (MyApplication.a() instanceof MainTabActivity2))
        {
            ((MainTabActivity2)MyApplication.a()).stopPlayAnimation();
            return;
        }
    }

    private void openDevicesSelect()
    {
        if (MyDeviceManager.getInstance(mAppContext).isDlnaInit())
        {
            DlnaManager.getInstance(mAppContext).startScanThread(-1);
        }
        final ArrayList devices = new ArrayList();
        devices.addAll((ArrayList)getDlnaManager().getAllDevices());
        ArrayList arraylist = new ArrayList();
        for (int i = 0; i < devices.size(); i++)
        {
            arraylist.add(DeviceUtil.getDeviceItemName((BaseDeviceItem)devices.get(i)));
        }

        arraylist.add("\u672C\u5730");
        if (mDeviceMenuDialog == null)
        {
            mDeviceMenuDialog = new _cls66(mActivity, arraylist, a.e, new _cls65(), 1);
        } else
        {
            mDeviceMenuDialog.setSelections(arraylist);
        }
        mDeviceMenuDialog.setHeaderTitle("\u9009\u62E9\u64AD\u653E\u8BBE\u5907");
        mDeviceMenuDialog.show();
        if (!isDlnaDeviceLinked())
        {
            List list = mDeviceMenuDialog.getSelections();
            if (list != null && list.size() > 0)
            {
                mDeviceMenuDialog.showListNewIcon(list.size() - 1);
            }
        }
    }

    private void parseData(String s)
    {
        if (s != null)
        {
            try
            {
                mShareSettingList = JSON.parseArray(JSON.parseObject(s).get("data").toString(), com/ximalaya/ting/android/model/setting/ShareSettingModel);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return;
            }
        } else
        {
            mShareSettingList = new ArrayList();
            s = new ShareSettingModel();
            s.thirdpartyId = 0;
            s.relay = true;
            s.webAlbum = true;
            s.webComment = true;
            s.webTrack = true;
            mShareSettingList.add(s);
            return;
        }
    }

    private void refreshShareSetting()
    {
        if (mShareSettingList != null && mShareSettingList.size() > 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Iterator iterator = mShareSettingList.iterator();
_L6:
        if (!iterator.hasNext()) goto _L1; else goto _L3
_L3:
        ShareSettingModel sharesettingmodel;
        boolean flag;
        sharesettingmodel = (ShareSettingModel)iterator.next();
        switch (mCommentType)
        {
        default:
            flag = sharesettingmodel.relay;
            break;

        case 1: // '\001'
            break MISSING_BLOCK_LABEL_119;
        }
_L4:
        switch (sharesettingmodel.thirdpartyId)
        {
        case 1: // '\001'
            doShareSina(flag);
            break;

        case 2: // '\002'
            if (sharesettingmodel.thirdpartyName.equals("tQQ"))
            {
                doShareQQ(flag);
            }
            if (sharesettingmodel.thirdpartyName.equals("qzone"))
            {
                doShareQzone(flag);
            }
            break;

        case 3: // '\003'
            doShareRenn(flag);
            break;
        }
        continue; /* Loop/switch isn't completed */
        flag = sharesettingmodel.webComment;
          goto _L4
        if (true) goto _L6; else goto _L5
_L5:
    }

    private void registerReceiver()
    {
        if (getActivity() != null && mReceiver == null)
        {
            mReceiver = new PayFinishReceiver();
            IntentFilter intentfilter = new IntentFilter("com.ximalaya.ting.android.pay.ACTION_PAY_SHARE");
            getActivity().registerReceiver(mReceiver, intentfilter);
        }
    }

    private void removeAdsHeader()
    {
        stopAutoSwapAds();
        adLayout.removeAllViews();
    }

    private void restoreUiToDefault()
    {
        mRightSpace.setBackgroundResource(0x7f070049);
        TextView textview = mSoundOwnerName;
        String s;
        if (mSoundInfo == null || Utilities.isBlank(mSoundInfo.nickname))
        {
            s = "\u559C\u9A6C\u62C9\u96C5";
        } else
        {
            s = mSoundInfo.nickname;
        }
        textview.setText(s);
        mSoundOwnerIcon.setImageResource(0x7f0202df);
        mOwnerSoundCountLabel.setVisibility(4);
        mOwnerFunCountLabel.setVisibility(4);
        mOwnerSoundCount.setText("");
        mOwnerFunsCount.setText("");
        mOwnerPosition.setText("");
        mComments.clear();
        if (viewPagerAdapter != null)
        {
            if (viewPagerAdapter.getCommentFragment() != null)
            {
                viewPagerAdapter.getCommentFragment().addData(mComments);
            }
            if (viewPagerAdapter.getDetailFragment() != null)
            {
                viewPagerAdapter.getDetailFragment().clear();
            }
            if (viewPagerAdapter.getSoundRelativeAlbumFragment() != null)
            {
                viewPagerAdapter.getSoundRelativeAlbumFragment().clear();
            }
        }
        setCommentCount(0);
        showNoCommentViewOrNot();
    }

    private void saveFirstLike()
    {
        SharedPreferencesUtil.getInstance(mAppContext).saveBoolean("isFirstLike", false);
    }

    private void setBlurBackground(String s, Drawable drawable)
    {
        android.graphics.Bitmap bitmap = ImageManager2.from(getActivity()).getFromMemCache((new StringBuilder()).append(s).append("/blur").toString());
        if (bitmap != null)
        {
            mBlurCoverBg.setImageBitmap(bitmap);
        } else
        {
            if (drawable == null)
            {
                mBlurCoverBg.setImageDrawable(new ColorDrawable(Color.parseColor("#b3202332")));
                mBlurCoverBg.setResourceUrl(null);
                return;
            }
            if (drawable instanceof BitmapDrawable)
            {
                mBlurCoverBg.blur(drawable, (new StringBuilder()).append(s).append("/blur").toString(), true);
                return;
            }
            if (drawable instanceof TransitionDrawable)
            {
                drawable = (TransitionDrawable)drawable;
                drawable = drawable.getDrawable(drawable.getNumberOfLayers() - 1);
                if (drawable instanceof BitmapDrawable)
                {
                    mBlurCoverBg.blur(drawable, (new StringBuilder()).append(s).append("/blur").toString(), true);
                    return;
                } else
                {
                    mBlurCoverBg.setImageDrawable(new ColorDrawable(Color.parseColor("#b3202332")));
                    mBlurCoverBg.setResourceUrl(null);
                    return;
                }
            }
        }
    }

    private void setCommentCount(int i)
    {
        ((RadioButton)mHeader.findViewById(0x7f0a0081)).setText("\u8BC4\u8BBA");
    }

    private void setCommentListViewListener()
    {
        viewPagerAdapter.getCommentFragment().setOnScrollListener(new _cls5());
        mFootView.setOnClickListener(new _cls6());
    }

    private void setConcernBtnStatus(boolean flag)
    {
        if (flag)
        {
            mFollow.setText(0x7f090199);
            mFollowContainer.setBackgroundResource(0x7f020265);
            mFollowIcon.setVisibility(8);
            return;
        } else
        {
            mFollow.setText(0x7f090198);
            mFollowContainer.setBackgroundResource(0x7f020262);
            mFollowIcon.setVisibility(0);
            return;
        }
    }

    private void setDlnaSeek(SeekBar seekbar)
    {
        double d = mSoundInfo.duration;
        getDlnaManager().setSeekBar(seekbar.getProgress(), seekbar.getMax(), d, new _cls70());
    }

    private void setFollowRequest(final long toUid, final boolean isFollow, final View v)
    {
        (new _cls47()).myexec(new Void[0]);
    }

    private void setNewFeatruePointVisible()
    {
        if (!SharedPreferencesUtil.getInstance(mContext).getBoolean("new_feature_more_clicked"))
        {
            newFeatureImg.setVisibility(0);
            return;
        } else
        {
            newFeatureImg.setVisibility(8);
            return;
        }
    }

    private void setPlayerBtnState(int i)
    {
        if (!isDlnaDeviceLinked())
        {
            return;
        }
        switch (i)
        {
        default:
            return;

        case 0: // '\0'
            mPlayOrPauseBtn.setBackgroundResource(0x7f020406);
            Logger.d("WIFI", "handlePlayKey setPlayerBtnState play");
            return;

        case 1: // '\001'
            mPlayOrPauseBtn.setBackgroundResource(0x7f0203de);
            break;
        }
        Logger.d("WIFI", "handlePlayKey setPlayerBtnState pause");
    }

    private void showAdImage()
    {
        if (mCommTopAdList == null || mCommTopAdList.size() == 0)
        {
            return;
        }
        _cls8 _lcls8;
        if (mNowAdNum == 0)
        {
            _lcls8 = new _cls8();
        } else
        {
            _lcls8 = null;
        }
        ImageManager2.from(mCon).displayImage(mTopCommAdImageView, ((AppAd)mCommTopAdList.get(mNowAdNum % mCommTopAdList.size())).cover, -1, _lcls8);
        mNowAd = (AppAd)mCommTopAdList.get(mNowAdNum % mCommTopAdList.size());
        mNowAdNum = mNowAdNum + 1;
    }

    private void showFooterView(FooterView footerview, ListView listview, View view)
    {
        listview.setFooterDividersEnabled(false);
        view.setVisibility(0);
        if (footerview == FooterView.MORE)
        {
            view.setClickable(true);
            view.findViewById(0x7f0a073b).setVisibility(0);
            ((TextView)mFootView.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
            view.findViewById(0x7f0a073c).setVisibility(8);
        } else
        {
            if (footerview == FooterView.LOADING)
            {
                view.setClickable(false);
                view.findViewById(0x7f0a073b).setVisibility(0);
                view.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFootView.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerview == FooterView.NO_CONNECTION)
            {
                view.setClickable(true);
                view.findViewById(0x7f0a073b).setVisibility(0);
                view.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)view.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == FooterView.NO_DATA)
            {
                view.setClickable(true);
                view.findViewById(0x7f0a073b).setVisibility(0);
                view.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFootView.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerview == FooterView.HIDE_ALL)
            {
                view.setClickable(true);
                view.findViewById(0x7f0a073c).setVisibility(8);
                view.findViewById(0x7f0a073b).setVisibility(8);
                view.setVisibility(8);
                return;
            }
        }
    }

    private void showNoCommentView(boolean flag)
    {
        if (!flag)
        {
            mNoCommentView.findViewById(0x7f0a011a).setVisibility(8);
            mNoCommentView.findViewById(0x7f0a0231).setVisibility(8);
            mNoCommentView.findViewById(0x7f0a0232).setVisibility(8);
            mNoCommentView.setVisibility(8);
            return;
        } else
        {
            mNoCommentView.findViewById(0x7f0a011a).setVisibility(0);
            mNoCommentView.findViewById(0x7f0a0231).setVisibility(0);
            mNoCommentView.findViewById(0x7f0a0232).setVisibility(0);
            mNoCommentView.setVisibility(0);
            return;
        }
    }

    private void showNoCommentViewOrNot()
    {
label0:
        {
            if (mNoCommentView != null)
            {
                if (mComments.size() <= 0)
                {
                    break label0;
                }
                showNoCommentView(false);
            }
            return;
        }
        showNoCommentView(true);
    }

    private void showNoWifiConfirm(com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback)
    {
        if (dialogcallback == null)
        {
            return;
        }
        int i = NetworkUtils.getNetType(mContext);
        boolean flag = SharedPreferencesUtil.getInstance(mContext).getBoolean("is_download_enabled_in_3g", false);
        if (i == 0 && !flag && mBoundService != null && mBoundService.isPlayFromNetwork())
        {
            NetworkUtils.showChangeNetWorkSetConfirm(dialogcallback, null);
            return;
        } else
        {
            dialogcallback.onExecute();
            return;
        }
    }

    private void showOperationChooser()
    {
        final MenuDialog dialog = new MenuDialog(getActivity(), 0x7f0c0029);
        dialog.setOnItemClickListener(new _cls54());
        dialog.show();
    }

    private void showShareToast()
    {
        mShowShareToastTv.setVisibility(0);
        mShowShareToastTv.postDelayed(new _cls56(), 10000L);
    }

    private void showSoundAdPic(AbstractSoundAdModel abstractsoundadmodel, boolean flag)
    {
        AdCollectData adcollectdata = new AdCollectData();
        adcollectdata.setAdItemId((new StringBuilder()).append("").append(abstractsoundadmodel.getAdId()).toString());
        SoundInfo soundinfo;
        if (abstractsoundadmodel instanceof XMSoundAd)
        {
            adcollectdata.setAdSource("0");
        } else
        if (abstractsoundadmodel instanceof BdSoundAdModel)
        {
            adcollectdata.setAdSource("1");
        }
        adcollectdata.setAndroidId(ToolUtil.getAndroidId(mAppContext.getApplicationContext()));
        if (flag)
        {
            adcollectdata.setLogType("soundLogoClick");
        } else
        {
            adcollectdata.setLogType("soundShow");
        }
        adcollectdata.setPositionName("sound_patch");
        adcollectdata.setResponseId((new StringBuilder()).append(abstractsoundadmodel.getResponseId()).append("").toString());
        adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
        soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null)
        {
            adcollectdata.setTrackId((new StringBuilder()).append("").append(soundinfo.trackId).toString());
        } else
        {
            adcollectdata.setTrackId("-1");
        }
        DataCollectUtil.getInstance(mCon.getApplicationContext()).statOnlineAd(adcollectdata);
        mIsSoundAdShowing = true;
        ImageManager2.from(getActivity()).displayImage(mSoundCover, abstractsoundadmodel.getICover(), -1);
        if (TextUtils.isEmpty(abstractsoundadmodel.getICover()))
        {
            mAdLogo.setVisibility(8);
        } else
        {
            mAdLogo.setVisibility(0);
            mAdLogo.setImageResource(0x7f020162);
        }
        if (!TextUtils.isEmpty(abstractsoundadmodel.getITitle()))
        {
            mSoundTitle.setText(abstractsoundadmodel.getITitle());
        }
        abstractsoundadmodel.onPicAdShow(flag);
    }

    private void showWaitingViews(boolean flag)
    {
        if (mWaittingProgressBar != null)
        {
            ProgressBar progressbar = mWaittingProgressBar;
            int i;
            if (flag)
            {
                i = 0;
            } else
            {
                i = 8;
            }
            progressbar.setVisibility(i);
        }
    }

    private void startAutoSwapAds()
    {
        mIsStopSwapAd = false;
        if (handlerCount > 0)
        {
            return;
        } else
        {
            handlerCount = handlerCount + 1;
            mHandler.postDelayed(mAutoSwapAdsTask, 5000L);
            return;
        }
    }

    private void statDashangShow()
    {
        View view;
        if (viewPagerAdapter != null && viewPagerAdapter.getDetailFragment() != null)
        {
            if ((view = viewPagerAdapter.getDetailFragment().getRewardView()) != null && view.isShown() && view.getVisibility() == 0 && !isStatDashangShow)
            {
                Logger.log("dashang test");
                isStatDashangShow = true;
                DataCollectUtil.getInstance(getActivity().getApplicationContext()).statDashang("show_dashang", mSoundInfo.uid, mSoundInfo.trackId, mSoundInfo.albumId);
                return;
            }
        }
    }

    private void stopAutoSwapAds()
    {
        mIsStopSwapAd = true;
        handlerCount = 0;
        mHandler.removeCallbacks(mAutoSwapAdsTask);
    }

    private void toSound(long l)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.trackId = l;
        PlayTools.gotoPlayWithoutUrl(12, soundinfo, mActivity, true, null);
    }

    private void tuisong(BaseDeviceItem basedeviceitem)
    {
        if (isDlnaInit() && (getDlnaManager().getLinkedDeviceModel() == null || getDlnaManager().getLinkedDeviceModel().getNowDeviceItem() == null || !getDlnaManager().getLinkedDeviceModel().getNowDeviceItem().equals(basedeviceitem)))
        {
            initDlnaListener();
            getDlnaManager().tuisongDevice(basedeviceitem);
            basedeviceitem = getDlnaManager().getLinkedDeviceModel();
            if (basedeviceitem.isValid())
            {
                if (basedeviceitem.getNowDeviceItem().getDlnaType() == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao)
                {
                    getDlnaManager().stop();
                }
                mBoundService.pause();
                try
                {
                    Thread.sleep(50L);
                }
                // Misplaced declaration of an exception variable
                catch (BaseDeviceItem basedeviceitem)
                {
                    basedeviceitem.printStackTrace();
                }
                if (mSoundInfo != null)
                {
                    basedeviceitem = new Intent();
                    basedeviceitem.addFlags(0x10000000);
                    basedeviceitem.setAction("ximalaya.action.player.CHANGE_SOUND");
                    basedeviceitem.putExtra("position1", (new Long(mSoundInfo.history_listener)).intValue());
                    basedeviceitem.putExtra("duration1", (new Long(mSoundInfo.history_duration)).intValue());
                    basedeviceitem.putExtra("trackId1", mSoundInfo.albumId);
                    basedeviceitem.putExtra("trackId2", mSoundInfo.albumId);
                    basedeviceitem.putExtra("ACTION_EXTRA_SOUNDINFO", JSON.toJSONString(mSoundInfo));
                    getActivity().sendBroadcast(basedeviceitem);
                }
                basedeviceitem = new ActionModel();
                ((ActionModel) (basedeviceitem)).result.put("callback", callBack);
                getDlnaManager().playSound(basedeviceitem);
                if (mActivity != null)
                {
                    (new Handler()).postDelayed(new _cls68(), 1000L);
                    return;
                }
            }
        }
    }

    private void unForbidSeek()
    {
        if (mWaittingProgressBar != null)
        {
            mWaittingProgressBar.setVisibility(8);
        }
        if (mSeekBar != null)
        {
            mSeekBar.setCanSeek(true);
        }
    }

    private void unregisterReceiver()
    {
        if (getActivity() != null && mReceiver != null)
        {
            getActivity().unregisterReceiver(mReceiver);
        }
    }

    private void updateBufferingProgress(int i)
    {
        this;
        JVM INSTR monitorenter ;
        if (!isDlnaInit()) goto _L2; else goto _L1
_L1:
        boolean flag = isDlnaDeviceLinked();
        if (!flag) goto _L2; else goto _L3
_L3:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (PlayListControl.getPlayListManager().curPlaySrc == null || !isAdded()) goto _L3; else goto _L4
_L4:
        if (!PlayListControl.getPlayListManager().curPlaySrc.contains("http"))
        {
            i = 100;
        }
        if (i < mBufferPercent || mSeekBar == null || mSoundInfo == null) goto _L3; else goto _L5
_L5:
        if (mSeekBar.getMax() != LocalMediaService.getInstance().getDuration())
        {
            mSeekBar.setMax(LocalMediaService.getInstance().getDuration());
        }
        int j = (mSeekBar.getMax() * i) / 100;
        mSeekBar.setSecondaryProgress(j);
        mBufferPercent = i;
        if (i != 100)
        {
            break MISSING_BLOCK_LABEL_165;
        }
        mRightSpace.setBackgroundColor(getResources().getColor(0x7f070047));
          goto _L3
        Exception exception;
        exception;
        throw exception;
        mRightSpace.setBackgroundColor(getResources().getColor(0x7f070049));
          goto _L3
    }

    private void updateControlButtons()
    {
        int i = PlayListControl.getPlayListManager().curIndex;
        int j = PlayListControl.getPlayListManager().getSize();
        if (i == 0)
        {
            mPreSoundBtn.setEnabled(false);
            if (j == 1)
            {
                mNextSoundBtn.setEnabled(false);
            } else
            {
                mNextSoundBtn.setEnabled(true);
            }
        } else
        if (i == j - 1)
        {
            mNextSoundBtn.setEnabled(false);
            if (i > 0)
            {
                mPreSoundBtn.setEnabled(true);
            } else
            {
                mPreSoundBtn.setEnabled(false);
            }
        } else
        {
            mPreSoundBtn.setEnabled(true);
            mNextSoundBtn.setEnabled(true);
        }
        updatePlayPauseSwitchButton();
    }

    private void updateDownloadBtnStatus(long l)
    {
        if (DownloadHandler.getInstance(getActivity()).isInDownloadList(l))
        {
            mDownloadBtn.setText(0x7f0901e5);
            return;
        } else
        {
            mDownloadBtn.setText(0x7f0901e6);
            return;
        }
    }

    private void updateLikeView()
    {
        if (mSoundInfo != null && mSoundInfo.is_favorited)
        {
            mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020346, 0, 0);
            if (likeCount % 5 == 0 && isDoLikeForToast)
            {
                likeCount = 0;
                showShareToast();
            }
            likeCount = likeCount + 1;
            isDoLikeForToast = false;
            return;
        } else
        {
            mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020345, 0, 0);
            return;
        }
    }

    private void updateModeView()
    {
        switch (SharedPreferencesUtil.getInstance(mAppContext).getInt("play_mode", 0))
        {
        default:
            mPlayModeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020087, 0, 0);
            mPlayModeBtn.setText(0x7f090194);
            return;

        case 0: // '\0'
            mPlayModeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020087, 0, 0);
            mPlayModeBtn.setText(0x7f090194);
            return;

        case 1: // '\001'
            mPlayModeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f02008a, 0, 0);
            mPlayModeBtn.setText(0x7f090195);
            return;

        case 2: // '\002'
            mPlayModeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020089, 0, 0);
            mPlayModeBtn.setText(0x7f090196);
            return;

        case 3: // '\003'
            mPlayModeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020088, 0, 0);
            break;
        }
        mPlayModeBtn.setText(0x7f090197);
    }

    private void updateMoveTime(int i, int j)
    {
        Logger.d("doss", (new StringBuilder()).append("updateMoveTime IN: ").append(j).toString());
        if (i >= 0 && j >= 0 && i <= j)
        {
            int k = ToolUtil.getScreenWidth(mContext);
            if (j > 0 && mMovingTimeBar != null && mProgressLabel != null)
            {
                mProgressLabel.setVisibility(0);
                mProgressLabel.setText((new StringBuilder()).append(ToolUtil.toTime((float)i / 1000F)).append("/").append(getDurationFormatText(j)).toString());
                if (mProgressLabel.getWidth() > 0)
                {
                    float f1 = (float)i / (float)j;
                    mMovingTimeBar.setPadding((int)((float)(k - mProgressLabel.getWidth()) * f1), 0, 0, 0);
                    return;
                }
            }
        }
    }

    private void updateOwnerInfoBar()
    {
label0:
        {
            if (mSoundInfoDetail != null)
            {
                if (mSoundInfoDetail.images != null && mSoundInfoDetail.images.size() > 1)
                {
                    mCoverImageIndex = mSoundInfoDetail.images.indexOf(mSoundInfoDetail.coverLarge);
                }
                Object obj;
                View view;
                if (mSoundInfoDetail.userInfo.isVerified)
                {
                    mSoundOwnerFlag.setVisibility(0);
                } else
                {
                    mSoundOwnerFlag.setVisibility(8);
                }
                if (mSoundInfoDetail.userSource == 1)
                {
                    obj = "\u539F\u521B";
                } else
                {
                    obj = "\u91C7\u96C6";
                }
                mSoundOwnerName.setText(mSoundInfoDetail.userInfo.nickname);
                mSoundSourceType.setText(((CharSequence) (obj)));
                mSoundOwnerIcon.setTag(0x7f0a0037, Boolean.valueOf(true));
                ImageManager2.from(mAppContext).displayImage(mSoundOwnerIcon, mSoundInfoDetail.userInfo.smallLogo, 0x7f0202df);
                mOwnerSoundCountLabel.setVisibility(0);
                mOwnerSoundCount.setText(StringUtil.getFriendlyNumStr(mSoundInfoDetail.userInfo.tracks));
                mOwnerFunCountLabel.setVisibility(0);
                mOwnerFunsCount.setText(StringUtil.getFriendlyNumStr(mSoundInfoDetail.userInfo.followers));
                mOwnerPosition.setText(mSoundInfoDetail.userInfo.personDescribe);
                obj = UserInfoMannage.getInstance().getUser();
                if (UserInfoMannage.hasLogined() && (obj == null || ((LoginInfoModel) (obj)).uid == mSoundInfoDetail.uid))
                {
                    break label0;
                }
                mFollowContainer.setVisibility(0);
                setConcernBtnStatus(mSoundInfoDetail.userInfo.isFollowed);
                view = mFollowContainer;
                if (mSoundInfoDetail.userInfo.isFollowed)
                {
                    obj = Boolean.valueOf(true);
                } else
                {
                    obj = Boolean.valueOf(false);
                }
                view.setTag(obj);
            }
            return;
        }
        mFollowContainer.setVisibility(4);
    }

    private void updatePlayControlBar()
    {
        updateControlButtons();
        updateLikeView();
        updateSeekBarView();
    }

    private void updatePlayPauseSwitchButton()
    {
        while (this == null || !isAdded() || isDlnaInit() && DlnaManager.getInstance(mActivity).isLinkedDeviceValid() || mBoundService == null || mBoundService.getMediaPlayerState() != 4) 
        {
            return;
        }
        mPlayOrPauseBtn.setBackgroundResource(0x7f0203de);
        mPlayOrPauseBtn.setContentDescription("\u6682\u505C");
    }

    private void updateProgress(int i, int j)
    {
        while (isDlnaInit() && isDlnaDeviceLinked() || mSoundInfo == null || mSeekBar == null || j <= 0) 
        {
            return;
        }
        if (mSeekBar.getMax() != LocalMediaService.getInstance().getDuration())
        {
            mSeekBar.setMax(LocalMediaService.getInstance().getDuration());
        }
        mSeekBar.setProgress(i);
    }

    private void updateRelativeAlbumData(SoundInfoDetail soundinfodetail)
    {
        if (viewPagerAdapter.getSoundRelativeAlbumFragment() != null)
        {
            viewPagerAdapter.getSoundRelativeAlbumFragment().updateData(soundinfodetail, DataCollectUtil.getDataFromView(fragmentBaseContainerView));
        }
    }

    private void updateRemoteControlClientMetadata(SoundInfo soundinfo)
    {
        if (PackageUtil.isPostICS())
        {
            MyApplication myapplication = (MyApplication)getActivity().getApplication();
            final android.media.RemoteControlClient.MetadataEditor editor = myapplication.n();
            if (editor != null && soundinfo != null)
            {
                editor = editor.editMetadata(true);
                editor.putString(1, soundinfo.albumName);
                editor.putString(2, soundinfo.nickname);
                editor.putString(7, soundinfo.title);
                editor.putLong(9, Double.doubleToLongBits(soundinfo.duration));
                ImageManager2.from(myapplication).downloadBitmap(soundinfo.coverLarge, new _cls61());
                return;
            }
        }
    }

    private void updateSeekBarView()
    {
        while (isDlnaInit() && isDlnaDeviceLinked() || PlayListControl.getPlayListManager().listType == 1 || !UserInfoMannage.hasLogined()) 
        {
            return;
        }
        initPlayerProgressBar();
    }

    private void updateSoundCommentsData(View view)
    {
        if (!NetworkUtils.isNetworkAvaliable(mAppContext))
        {
            showToast("\u8FDE\u63A5\u7F51\u7EDC\u5931\u8D25");
            showFooterView(FooterView.NO_CONNECTION, viewPagerAdapter.getCommentFragment().getListView(), mFootView);
        } else
        {
            if (mEnableCache && !mNeedUpdate)
            {
                CommentModelList commentmodellist = CacheManager.getSoundComments(mContext);
                if (commentmodellist != null && commentmodellist.trackId == mSoundInfo.trackId)
                {
                    mCommentList = commentmodellist;
                    mPageId = commentmodellist.pageId;
                    mComments.clear();
                    mComments.addAll(commentmodellist.list);
                    viewPagerAdapter.getCommentFragment().setData(mCommentList);
                    viewPagerAdapter.getCommentFragment().addData(mComments);
                    showNoCommentViewOrNot();
                    showFooterView(FooterView.HIDE_ALL, viewPagerAdapter.getCommentFragment().getListView(), mFootView);
                    mNeedUpdate = false;
                    return;
                }
            }
            if (!mIsLoading && (mLoadCommentTask == null || mLoadCommentTask.getStatus() == android.os.AsyncTask.Status.FINISHED))
            {
                mLoadCommentTask = new LoadCommentTask();
                mLoadCommentTask.myexec(new Object[] {
                    view
                });
                return;
            }
        }
    }

    private void updateSoundCoverImage(String s)
    {
        mIsSoundAdShowing = false;
        if (mSoundAd != null && !TextUtils.isEmpty(mSoundAd.getICover()))
        {
            ImageManager2.from(getActivity()).displayImage(mAdLogo, mSoundAd.getILogo(), -1);
            mAdLogo.setVisibility(0);
        } else
        {
            mAdLogo.setVisibility(8);
        }
        if (mSoundInfo != null && Utilities.isNotBlank(mSoundInfo.title))
        {
            mSoundTitle.setText(mSoundInfo.title);
        } else
        {
            mSoundTitle.setText("\u6B63\u5728\u64AD\u653E");
        }
        if (Utilities.isNotBlank(s))
        {
            com.ximalaya.ting.android.util.ImageManager2.Options options = new com.ximalaya.ting.android.util.ImageManager2.Options();
            options.fadeAlways = true;
            options.noPlaceholder = true;
            options.errorResId = 0x7f0201b4;
            ImageManager2.from(mAppContext).displayImage(mSoundCover, s, options, new _cls59());
            return;
        } else
        {
            mSoundCover.setImageResource(0x7f0202e1);
            mSoundCover.setTag(null);
            return;
        }
    }

    private void updateSoundDetailData()
    {
        if (!mEnableCache || mNeedUpdate) goto _L2; else goto _L1
_L1:
        SoundInfoDetail soundinfodetail = CacheManager.getSoundDetail(mContext);
        if (soundinfodetail == null || soundinfodetail.trackId != mSoundInfo.trackId) goto _L2; else goto _L3
_L3:
        mSoundInfoDetail = soundinfodetail;
        updateOwnerInfoBar();
        buildViewPager(soundinfodetail);
_L5:
        return;
_L2:
        if ((mLoadSoundDetailTask == null || mLoadSoundDetailTask.getStatus() == android.os.AsyncTask.Status.FINISHED) && mSoundInfo.trackId != 0L)
        {
            mLoadSoundDetailTask = new LoadSoundDetailTask();
            mLoadSoundDetailTask.myexec(new String[0]);
            return;
        }
        if (true) goto _L5; else goto _L4
_L4:
    }

    private void updateSoundDetailFragment()
    {
        if (viewPagerAdapter.getDetailFragment() != null)
        {
            viewPagerAdapter.getDetailFragment().setData(getSoundDetail(), mSoundInfo.trackId, mSoundInfoDetail.uid, mSoundInfo.albumId);
        }
    }

    private void updateSoundInfoBar()
    {
        if (mSoundInfo == null)
        {
            return;
        }
        if (mSoundInfo != null && Utilities.isNotBlank(mSoundInfo.title))
        {
            mSoundTitle.setText(mSoundInfo.title);
        } else
        {
            mSoundTitle.setText("\u6B63\u5728\u64AD\u653E");
        }
        if (mSoundInfo.plays_counts > 0)
        {
            mPlayCount.setVisibility(0);
            mPlayCount.setText(StringUtil.getFriendlyNumStr(mSoundInfo.plays_counts));
        } else
        {
            mPlayCount.setVisibility(8);
        }
        if (mSoundInfo.is_favorited)
        {
            mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020346, 0, 0);
            return;
        } else
        {
            mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020345, 0, 0);
            return;
        }
    }

    private void updateUI()
    {
        if (mSoundInfo == null)
        {
            return;
        }
        updateSoundInfoBar();
        updatePlayControlBar();
        if (ToolUtil.isConnectToNetwork(getActivity()))
        {
            updateSoundDetailData();
        } else
        {
            updateSoundCoverImage(mSoundInfo.coverLarge);
        }
        mNeedUpdate = false;
    }

    public void change2Bendi(final boolean needPause)
    {
        Logger.d("tuisong", (new StringBuilder()).append("change2Bendi:").append(needPause).toString());
        getActivity().runOnUiThread(new _cls67());
    }

    public int getScrollY(AbsListView abslistview)
    {
        int i = 0;
        View view = abslistview.getChildAt(0);
        if (view == null)
        {
            return 0;
        }
        int j = abslistview.getFirstVisiblePosition();
        int k = view.getTop();
        if (j >= 1)
        {
            i = mHeaderHeight;
        }
        k = -k;
        return i + (view.getHeight() * j + k);
    }

    public void initBufferingProgress()
    {
        this;
        JVM INSTR monitorenter ;
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice == null)
        {
            break MISSING_BLOCK_LABEL_18;
        }
        updateBufferingProgress(localmediaservice.getBufferredPercent());
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mContext = getActivity();
        mSoundInfo = PlayListControl.getPlayListManager().getCurSound();
        mAppContext = mContext.getApplicationContext();
        mResources = getResources();
        initPlayerHeader();
        initBottomMenuBar();
        initLisnteners();
        fragmentBaseContainerView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        doBindService();
        mNeedUpdate = false;
        restoreUiToDefault();
        updateUI();
        initAdsHeader();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        super.onActivityResult(i, j, intent);
        if (i != 1114) goto _L2; else goto _L1
_L1:
        if (j != 0) goto _L4; else goto _L3
_L3:
        if (lastBindWeibo != 12) goto _L6; else goto _L5
_L5:
        mIsBindSina = true;
_L9:
        if (mLoadShareSetting == null || mLoadShareSetting.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mLoadShareSetting = new LoadShareSetting();
            mLoadShareSetting.myexec(new Void[0]);
        }
_L4:
        return;
_L6:
        if (lastBindWeibo == 13)
        {
            mIsBindQQ = true;
        } else
        if (lastBindWeibo == 14)
        {
            mIsBindRenn = true;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        if (i != 175 || j != 86) goto _L4; else goto _L7
_L7:
        mEmotionSelector.setText((new StringBuilder()).append("@").append(intent.getStringExtra("name")).append(":").toString());
        return;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public void onAdFetchFail()
    {
        Logger.log("onAdFetchFail unForbidSeek");
        unForbidSeek();
        showWaitingViews(false);
        mSoundAd = null;
        updateSoundCoverImage(mSoundInfo.coverLarge);
    }

    public void onAdFetched(List list)
    {
        forbidSeek();
        if (list != null && list.size() > 0)
        {
            mSoundAd = (AbstractSoundAdModel)list.get(0);
            return;
        } else
        {
            mSoundAd = null;
            updateSoundCoverImage(mSoundInfo.coverLarge);
            return;
        }
    }

    public boolean onBackPressed()
    {
        if (mPlayHistoryFragment != null && mPlayHistoryFragment.isVisible())
        {
            getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).remove(mPlayHistoryFragment).commitAllowingStateLoss();
            return true;
        }
        if (mPlaylistFragment != null && mPlaylistFragment.isVisible())
        {
            getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).hide(mPlaylistFragment).commitAllowingStateLoss();
            return true;
        }
        if (mImageViewer != null && mImageViewer.isShowing())
        {
            mImageViewer.dismiss();
            return true;
        }
        if (mEmotionSelector.getVisibility() == 0)
        {
            mEmotionSelector.setVisibility(8);
            mEmotionSelector.setText("");
            mSyncBar.setVisibility(8);
            mBottomMenuBar.setVisibility(0);
            return true;
        } else
        {
            return false;
        }
    }

    public void onBind(String s)
    {
        DataCollectUtil.bindDataToView(s, fragmentBaseContainerView);
    }

    public void onBufferUpdated(int i)
    {
        if (this == null || !isAdded() || isDlnaInit() && isDlnaDeviceLinked())
        {
            return;
        } else
        {
            updateBufferingProgress(i);
            return;
        }
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        case 2131362080: 
        default:
            return;

        case 2131362941: 
            openDevicesSelect();
            break;
        }
    }

    public void onCompletePlayAd(MediaPlayer mediaplayer, List list)
    {
        Logger.log("onCompletePlayAd unForbidSeek");
        updateSoundCoverImage(mSoundInfo.coverLarge);
        unForbidSeek();
        showWaitingViews(false);
        if (mSeekBar != null)
        {
            mSeekBar.setText("00:00");
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        if (ToolUtil.isUseSmartbarAsTab())
        {
            setHasOptionsMenu(true);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f03018f, null);
        fragmentBaseContainerView = layoutinflater;
        mViewPager = (ViewPager)layoutinflater.findViewById(0x7f0a017a);
        mViewPager.setOffscreenPageLimit(3);
        mHeader = layoutinflater.findViewById(0x7f0a00c8);
        mRadioGroup = (RadioGroup)mHeader.findViewById(0x7f0a0621);
        mRadioGroup.setBackgroundColor(-1);
        mBackBtn = (ImageView)mHeader.findViewById(0x7f0a0536);
        mAdLogo = (ImageView)mHeader.findViewById(0x7f0a05fb);
        mRedirectView = (RedirectTouchRelativeLayout)layoutinflater.findViewById(0x7f0a061e);
        return layoutinflater;
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    public void onDestroyView()
    {
        if (mBoundService != null)
        {
            mBoundService.removeOnPlayerUpdateListener(this);
            mBoundService.removeOnPlayServiceUpdateListener(this);
            mBoundService.removeSoundPatchAdCallback(this);
        }
        doUnbindService();
        unregisterReceiver();
        if (mEnableCache && mNeedUpdate)
        {
            CacheManager.saveSoundDetail(mContext, mSoundInfoDetail);
            mCommentList.list = mComments;
            CacheManager.saveSoundComments(mContext, mCommentList);
        }
        if (android.os.Build.VERSION.SDK_INT >= 16)
        {
            fragmentBaseContainerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        } else
        {
            fragmentBaseContainerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
        clearDlnaListener();
        mRedirectView.removeTouchUpCallback();
        super.onDestroyView();
    }

    public void onDeviceChanged()
    {
        getActivity().runOnUiThread(new _cls71());
    }

    public void onErroredPlayAd(MediaPlayer mediaplayer, List list)
    {
        Logger.log("unForbidSeek", "onErroredPlayAd");
        updateSoundCoverImage(mSoundInfo.coverLarge);
        Logger.log("onErroredPlayAd unForbidSeek");
        unForbidSeek();
        showWaitingViews(false);
        if (mSeekBar != null)
        {
            mSeekBar.setText("00:00");
        }
    }

    public void onGlobalLayout()
    {
        Rect rect = new Rect();
        fragmentBaseContainerView.getWindowVisibleDisplayFrame(rect);
        if (Math.abs(fragmentBaseContainerView.getRootView().getHeight() - (rect.bottom - rect.top)) > 100)
        {
            mEmotionSelector.setEmotionSelectorIcon(0x7f02020a);
            return;
        } else
        {
            mEmotionSelector.setEmotionSelectorIcon(0x7f0202ff);
            return;
        }
    }

    public void onKeyAciton(KeyEvent keyevent)
    {
        while (mWifiControlHandler == null || keyevent.getDeviceItem() == null || !keyevent.getDeviceItem().equals(DlnaManager.getInstance(mAppContext).getLinkedDeviceModel().getNowDeviceItem())) 
        {
            return;
        }
        Message message = new Message();
        message.what = keyevent.getEventKey();
        message.obj = keyevent.getT();
        mWifiControlHandler.sendMessage(message);
    }

    public void onLogoPlayFinished()
    {
        Logger.log("onLogoPlayFinished unForbidSeek");
        unForbidSeek();
    }

    public void onPause()
    {
        stopAutoSwapAds();
        isStatDashangShow = false;
        super.onPause();
        mSoundTitle.setSelected(false);
    }

    public void onPausePlayAd(MediaPlayer mediaplayer)
    {
        if (this == null || !isAdded() || !isDlnaDeviceLinked())
        {
            return;
        } else
        {
            mPlayOrPauseBtn.setBackgroundResource(0x7f020406);
            mPlayOrPauseBtn.setContentDescription("\u5F00\u59CB\u64AD\u653E");
            return;
        }
    }

    public void onPlayCanceled()
    {
        if (this != null)
        {
            if (isAdded());
        }
    }

    public void onPlayCompleted()
    {
        if (this == null || !isAdded() || !isDlnaDeviceLinked())
        {
            return;
        } else
        {
            mPlayOrPauseBtn.setBackgroundResource(0x7f020406);
            mPlayOrPauseBtn.setContentDescription("\u5F00\u59CB\u64AD\u653E");
            return;
        }
    }

    public void onPlayPaused()
    {
        if (!isDlnaInit() || !getDlnaManager().isLinkedDeviceValid())
        {
            if (this != null && isAdded())
            {
                mPlayOrPauseBtn.setBackgroundResource(0x7f020406);
                mPlayOrPauseBtn.setContentDescription("\u5F00\u59CB\u64AD\u653E");
            }
            if (mSoundAd != null)
            {
                showSoundAdPic(mSoundAd, false);
                return;
            }
        }
    }

    public void onPlayProgressUpdate(int i, int j)
    {
        if (this == null || !isAdded() || isDlnaInit() && isDlnaDeviceLinked())
        {
            return;
        } else
        {
            updatePlayProgress(i, j);
            return;
        }
    }

    public void onPlayStarted()
    {
        if (this != null && isAdded())
        {
            mPlayOrPauseBtn.setBackgroundResource(0x7f0203de);
            mPlayOrPauseBtn.setContentDescription("\u6682\u505C");
            Logger.log("onPlayStarted unForbidSeek");
            unForbidSeek();
            if (mIsSoundAdShowing)
            {
                updateSoundCoverImage(mSoundInfo.coverLarge);
                if (mSoundAd != null)
                {
                    mSoundAd.onPicAdClose();
                }
            }
            if (isDlnaInit() && isDlnaDeviceLinked())
            {
                LocalMediaService.getInstance().pause();
            }
        }
    }

    public void onPlayerBuffering(boolean flag)
    {
label0:
        {
            if (this != null && isAdded())
            {
                if (!flag)
                {
                    break label0;
                }
                Logger.log("onPlayerBuffering ForbidSeek");
                forbidSeek();
            }
            return;
        }
        Logger.log("onPlayerBuffering unForbidSeek");
        unForbidSeek();
    }

    public void onPrepareOptionsMenu(Menu menu)
    {
        if (ToolUtil.isUseSmartbarAsTab())
        {
            getActivity().getActionBar().removeAllTabs();
        }
        super.onPrepareOptionsMenu(menu);
    }

    public void onResume()
    {
        super.onResume();
        Logger.d("tuisong", "onResume IN");
        if (isDlnaInit())
        {
            if (getDlnaManager().isLinkedDeviceValid())
            {
                onUpdatePlayBtn();
            }
            freshDeviceBtn();
        }
        if (mSoundTitle != null)
        {
            mSoundTitle.postDelayed(new _cls57(), 1000L);
        }
        if (mSoundInfo == null) goto _L2; else goto _L1
_L1:
        if (mSoundInfo.equals(PlayListControl.getPlayListManager().getCurSound())) goto _L4; else goto _L3
_L3:
        onSoundChanged(PlayListControl.getPlayListManager().curIndex, true);
_L2:
        registerReceiver();
        statDashangShow();
        return;
_L4:
        if (mIsShowAds)
        {
            initAdData();
            mIsStopSwapAd = false;
            if (mCommTopAdList != null && mCommTopAdList.size() > 0)
            {
                startAutoSwapAds();
            }
        } else
        {
            removeAdsHeader();
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    public void onResumePlayAd(MediaPlayer mediaplayer)
    {
        if (this != null && isAdded())
        {
            mPlayOrPauseBtn.setBackgroundResource(0x7f0203de);
            mPlayOrPauseBtn.setContentDescription("\u6682\u505C");
        }
    }

    public void onSoundChanged(int i)
    {
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null && soundinfo.category != 0)
        {
            return;
        } else
        {
            onSoundChanged(i, false);
            initAdData();
            return;
        }
    }

    public void onSoundChanged(int i, boolean flag)
    {
        SoundInfo soundinfo;
        mIsChangeForAd = true;
        soundinfo = PlayListControl.getPlayListManager().getCurSound();
        break MISSING_BLOCK_LABEL_12;
        if (soundinfo != null && this != null && isAdded() && (mSoundInfo == null || soundinfo.trackId != mSoundInfo.trackId) && i >= 0)
        {
            if (mSoundInfo != null)
            {
                mPlayedSoundAlbumId = mSoundInfo.albumId;
            }
            mSoundInfoDetail = null;
            mSoundInfo = PlayListControl.getPlayListManager().getCurSound();
            restoreUiToDefault();
            mBufferPercent = 0;
            mNeedUpdate = true;
            mPageId = 1;
            if (mAppContext != null && NetworkUtils.getNetType(mAppContext) == -1 && PlayListControl.getPlayListManager().curPlaySrc.contains("http"))
            {
                CustomToast.showToast(mAppContext, "\u7F51\u7EDC\u4E0D\u7ED9\u529B", 1000);
                return;
            }
            updateUI();
            if (!flag)
            {
                forbidSeek();
                return;
            }
        }
        return;
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (mContext != null && isAdded() && mSoundInfo != null && soundinfo != null && soundinfo.trackId == mSoundInfo.trackId)
        {
            if (mLikeBtn != null)
            {
                if (soundinfo.is_favorited)
                {
                    mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020346, 0, 0);
                    mSoundInfo.is_favorited = true;
                    mSoundInfo.favorites_counts = soundinfo.favorites_counts;
                } else
                {
                    mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f020345, 0, 0);
                    mSoundInfo.is_favorited = false;
                    mSoundInfo.favorites_counts = soundinfo.favorites_counts;
                }
            }
            mNeedUpdate = true;
            mNeedUpdate = false;
        }
    }

    public void onSoundLiked(boolean flag)
    {
        if (mContext != null && mSoundInfo != null)
        {
            LocalMediaService localmediaservice;
            if (flag)
            {
                mSoundInfo.is_favorited = true;
                SoundInfo soundinfo = mSoundInfo;
                soundinfo.favorites_counts = soundinfo.favorites_counts + 1;
            } else
            {
                mSoundInfo.is_favorited = false;
                SoundInfo soundinfo1 = mSoundInfo;
                soundinfo1.favorites_counts = soundinfo1.favorites_counts - 1;
            }
            PlayListControl.getPlayListManager().updateCurSoundInfo(PlayListControl.getPlayListManager().curIndex, mSoundInfo);
            localmediaservice = LocalMediaService.getInstance();
            if (localmediaservice != null)
            {
                localmediaservice.updateOnSoundInfoModified(PlayListControl.getPlayListManager().curIndex, mSoundInfo);
                return;
            }
        }
    }

    public void onSoundPrepared(int i)
    {
        Logger.d("PlayStatistics", "onSoundPrepared IN");
        if (isDlnaInit() && isDlnaDeviceLinked() && PlayListControl.getPlayListManager().getCurSound().category == 1)
        {
            Logger.d("PlayStatistics", "PLAY_TYPE_LIVE IN");
            CustomToast.showToast(mAppContext, "\u60A8\u6B63\u5728\u4F7F\u7528WIFI\u8BBE\u5907\u8FDB\u884C\u64AD\u653E\uFF0C\u6B63\u5728\u4E3A\u60A8\u5207\u56DE\u672C\u5730\u64AD\u653E", 0);
            change2Bendi(false);
        }
        if (!isDlnaInit() || !isDlnaDeviceLinked()) goto _L2; else goto _L1
_L1:
        if (!isDeviceControl) goto _L4; else goto _L3
_L3:
        isDeviceControl = false;
_L6:
        return;
_L4:
        ActionModel actionmodel = new ActionModel();
        actionmodel.result.put("callback", callBack);
        getDlnaManager().playSound(actionmodel);
        if (mActivity != null)
        {
            (new Handler()).postDelayed(new _cls60(), 1000L);
        }
        initDurationFormatText(i);
        return;
_L2:
        if (this != null && isAdded())
        {
            initDurationFormatText(i);
            if (mWaittingProgressBar != null)
            {
                mWaittingProgressBar.setVisibility(8);
                return;
            }
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void onStartFetchAd()
    {
        forbidSeek();
        showWaitingViews(true);
        mSoundAd = null;
    }

    public void onStartPlayAd(MediaPlayer mediaplayer, List list, int i)
    {
        mSoundAd = (AbstractSoundAdModel)list.get(i);
        showSoundAdPic((AbstractSoundAdModel)list.get(i), false);
        showWaitingViews(false);
        if (mSeekBar != null)
        {
            mSeekBar.setProgress(0);
            mSeekBar.setText("\u5E7F\u544A");
        }
        if (this != null && isAdded())
        {
            mPlayOrPauseBtn.setBackgroundResource(0x7f0203de);
            mPlayOrPauseBtn.setContentDescription("\u6682\u505C");
        }
    }

    public void onStartPlayLogo()
    {
        forbidSeek();
    }

    public void setDlnaSeekbarProgress(int i, int j)
    {
        if (i != 0)
        {
            Logger.d("position", "setSeekbarProgress IN");
            if (isDlnaInit() && getDlnaManager().getLinkedDeviceModel() != null && getDlnaManager().getLinkedDeviceModel().isValid())
            {
                BaseDeviceItem basedeviceitem = getDlnaManager().getLinkedDeviceModel().getNowDeviceItem();
                if (basedeviceitem == null || mForbidProgressUpdate)
                {
                    Logger.d("position", "setSeekbarProgress OUT#1");
                    return;
                }
                Logger.log("setDlnaSeekbarProgress unForbidSeek");
                unForbidSeek();
                mSeekBar.setMax(i * 1000);
                mSeekBar.setProgress(j * 1000);
                if (j > 10 && i > 10 && j >= i - 2 && basedeviceitem != null)
                {
                    mBoundService.playNext(true);
                }
                mLocalProcess = j * 1000;
                return;
            }
        }
    }

    public void updatePlayProgress(int i, int j)
    {
        if (i > j || mForbidProgressUpdate)
        {
            return;
        } else
        {
            initBufferingProgress();
            updateProgress(i, j);
            return;
        }
    }








/*
    static boolean access$10202(PlayerFragment playerfragment, boolean flag)
    {
        playerfragment.mIsBound = flag;
        return flag;
    }

*/






/*
    static int access$10702(PlayerFragment playerfragment, int i)
    {
        playerfragment.handlerCount = i;
        return i;
    }

*/









/*
    static int access$11308(PlayerFragment playerfragment)
    {
        int i = playerfragment.mPageId;
        playerfragment.mPageId = i + 1;
        return i;
    }

*/




/*
    static CommentModelList access$11502(PlayerFragment playerfragment, CommentModelList commentmodellist)
    {
        playerfragment.mCommentList = commentmodellist;
        return commentmodellist;
    }

*/








/*
    static boolean access$1202(PlayerFragment playerfragment, boolean flag)
    {
        playerfragment.mIsLoading = flag;
        return flag;
    }

*/








/*
    static int access$12602(PlayerFragment playerfragment, int i)
    {
        playerfragment.mPlaySource = i;
        return i;
    }

*/




/*
    static boolean access$12902(PlayerFragment playerfragment, boolean flag)
    {
        playerfragment.mIsLoadShareSetting = flag;
        return flag;
    }

*/




















/*
    static boolean access$1502(PlayerFragment playerfragment, boolean flag)
    {
        playerfragment.mNeedUpdate = flag;
        return flag;
    }

*/









/*
    static List access$2102(PlayerFragment playerfragment, List list)
    {
        playerfragment.mCommTopAdList = list;
        return list;
    }

*/






/*
    static boolean access$2602(PlayerFragment playerfragment, boolean flag)
    {
        playerfragment.mIsChangeForAd = flag;
        return flag;
    }

*/












/*
    static int access$3502(PlayerFragment playerfragment, int i)
    {
        playerfragment.mCommentType = i;
        return i;
    }

*/



/*
    static SendCommentTask access$3602(PlayerFragment playerfragment, SendCommentTask sendcommenttask)
    {
        playerfragment.mSendCommentTask = sendcommenttask;
        return sendcommenttask;
    }

*/






/*
    static boolean access$402(PlayerFragment playerfragment, boolean flag)
    {
        playerfragment.mIsCommentFragmentCreated = flag;
        return flag;
    }

*/









/*
    static int access$4702(PlayerFragment playerfragment, int i)
    {
        playerfragment.lastBindWeibo = i;
        return i;
    }

*/










/*
    static DeleteCommentTask access$5402(PlayerFragment playerfragment, DeleteCommentTask deletecommenttask)
    {
        playerfragment.mDeleteCommentTask = deletecommenttask;
        return deletecommenttask;
    }

*/









/*
    static LoadShareSetting access$6102(PlayerFragment playerfragment, LoadShareSetting loadsharesetting)
    {
        playerfragment.mLoadShareSetting = loadsharesetting;
        return loadsharesetting;
    }

*/




/*
    static LikeTask access$6302(PlayerFragment playerfragment, LikeTask liketask)
    {
        playerfragment.mLikeTask = liketask;
        return liketask;
    }

*/










/*
    static boolean access$7102(PlayerFragment playerfragment, boolean flag)
    {
        playerfragment.mForbidProgressUpdate = flag;
        return flag;
    }

*/





/*
    static PlayListHostoryFragment access$7402(PlayerFragment playerfragment, PlayListHostoryFragment playlisthostoryfragment)
    {
        playerfragment.mPlayHistoryFragment = playlisthostoryfragment;
        return playlisthostoryfragment;
    }

*/









/*
    static int access$802(PlayerFragment playerfragment, int i)
    {
        playerfragment.mMinHeaderTranslation = i;
        return i;
    }

*/




/*
    static MenuDialog access$8202(PlayerFragment playerfragment, MenuDialog menudialog)
    {
        playerfragment.mPlayMoreDiaolog = menudialog;
        return menudialog;
    }

*/





/*
    static SoundInfoDetail access$8502(PlayerFragment playerfragment, SoundInfoDetail soundinfodetail)
    {
        playerfragment.mSoundInfoDetail = soundinfodetail;
        return soundinfodetail;
    }

*/



/*
    static boolean access$8602(PlayerFragment playerfragment, boolean flag)
    {
        playerfragment.isSearching = flag;
        return flag;
    }

*/










/*
    static int access$9302(PlayerFragment playerfragment, int i)
    {
        playerfragment.mHeaderHeight = i;
        return i;
    }

*/





/*
    static PlaylistFragment access$9602(PlayerFragment playerfragment, PlaylistFragment playlistfragment)
    {
        playerfragment.mPlaylistFragment = playlistfragment;
        return playlistfragment;
    }

*/





/*
    static ImageViewer access$9902(PlayerFragment playerfragment, ImageViewer imageviewer)
    {
        playerfragment.mImageViewer = imageviewer;
        return imageviewer;
    }

*/

    private class _cls55
        implements ServiceConnection
    {

        final PlayerFragment this$0;

        public void onServiceConnected(ComponentName componentname, IBinder ibinder)
        {
            mBoundService = ((com.ximalaya.ting.android.service.play.LocalMediaService.LocalBinder)ibinder).getService();
            mBoundService.setOnPlayerStatusUpdateListener(PlayerFragment.this);
            mBoundService.setOnPlayServiceUpdateListener(PlayerFragment.this);
            mBoundService.addSoundPatchAdCallback(PlayerFragment.this);
            mIsBound = true;
            updatePlayPauseSwitchButton();
        }

        public void onServiceDisconnected(ComponentName componentname)
        {
            mBoundService.removeSoundPatchAdCallback(PlayerFragment.this);
            mBoundService = null;
        }

        _cls55()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls58
        implements Runnable
    {

        final PlayerFragment this$0;

        public void run()
        {
            if (!PlayerFragment.mIsShowAds)
            {
                return;
            }
            if (mIsStopSwapAd || mTopCommAdImageView == null)
            {
                handlerCount = 0;
                return;
            }
            if (isAdded() && isResumed())
            {
                showAdImage();
            }
            mHandler.postDelayed(this, 5000L);
        }

        _cls58()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls63
        implements IActionCallBack
    {

        final PlayerFragment this$0;

        public void onFailed()
        {
        }

        public void onSuccess(ActionModel actionmodel)
        {
        }

        _cls63()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls1
        implements CommentListFragment.OnActivityCreateCallback
    {

        final PlayerFragment this$0;

        public void onActivityCreate()
        {
            viewPagerAdapter.getCommentFragment().getListView().addHeaderView(adLayout);
            initListFootView();
            setCommentListViewListener();
            mIsCommentFragmentCreated = true;
            for (; !mCommentFragmentCreatedWorkQueue.isEmpty(); ((Runnable)mCommentFragmentCreatedWorkQueue.poll()).run()) { }
        }

        _cls1()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls2
        implements ScrollTabHolder
    {

        final PlayerFragment this$0;

        public void adjustScroll(int i)
        {
        }

        public ListView getListView()
        {
            return null;
        }

        public void onScroll(AbsListView abslistview, int i, int j, int k, int l)
        {
            if (mViewPager.getCurrentItem() == l)
            {
                i = getScrollY(abslistview);
                ViewHelper.setTranslationY(mHeader, Math.max(-i, mMinHeaderTranslation));
            }
        }

        _cls2()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final PlayerFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            clickCommentItem(i);
        }

        _cls3()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final PlayerFragment this$0;

        public void run()
        {
            updateSoundCommentsData(fragmentBaseContainerView);
        }

        _cls4()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls64
        implements Runnable
    {

        final PlayerFragment this$0;

        public void run()
        {
            mProgressLabel.setVisibility(4);
        }

        _cls64()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls21
        implements android.widget.AdapterView.OnItemClickListener
    {

        final PlayerFragment this$0;
        final CommentModel val$comment;
        final MenuDialog val$mCommentMenuDialog;
        final int val$pt;
        final SoundInfo val$sound;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i;
            JVM INSTR tableswitch 0 1: default 24
        //                       0 32
        //                       1 24;
               goto _L1 _L2 _L1
_L1:
            mCommentMenuDialog.dismiss();
            return;
_L2:
            if (mDeleteCommentTask == null || mDeleteCommentTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
            {
                mDeleteCommentTask = new DeleteCommentTask(null);
                mDeleteCommentTask.myexec(new Object[] {
                    Long.valueOf(sound.trackId), Long.valueOf(comment.id), Long.valueOf(pt), view
                });
            } else
            {
                showToast("\u5C1A\u6709\u5176\u5B83\u5220\u9664\u8BC4\u8BBA\u4EFB\u52A1\u672A\u6267\u884C\u5B8C\u6BD5\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5");
            }
            if (true) goto _L1; else goto _L3
_L3:
        }

        _cls21()
        {
            this$0 = PlayerFragment.this;
            sound = soundinfo;
            comment = commentmodel;
            pt = i;
            mCommentMenuDialog = menudialog;
            super();
        }
    }


    private class _cls22
        implements android.widget.AdapterView.OnItemClickListener
    {

        final PlayerFragment this$0;
        final CommentModel val$comment;
        final MenuDialog val$mCommentMenuDialog;
        final SoundInfo val$sound;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i;
            JVM INSTR tableswitch 0 2: default 28
        //                       0 36
        //                       1 54
        //                       2 214;
               goto _L1 _L2 _L3 _L4
_L1:
            mCommentMenuDialog.dismiss();
            return;
_L2:
            gotoHomePage(comment.uid, view);
            continue; /* Loop/switch isn't completed */
_L3:
            mCommentType = 1;
            mEmotionSelector.setVisibility(0);
            mSyncBar.setVisibility(0);
            initLoginUserBindData();
            loadDefaultShareSetting();
            if (mLoadShareSetting == null || mLoadShareSetting.getStatus() == android.os.AsyncTask.Status.FINISHED)
            {
                mLoadShareSetting = new LoadShareSetting();
                mLoadShareSetting.myexec(new Void[0]);
            }
            mEmotionSelector.setText((new StringBuilder()).append("@").append(comment.nickname).append(":").toString());
            mBottomMenuBar.setVisibility(8);
            continue; /* Loop/switch isn't completed */
_L4:
            if (UserInfoMannage.hasLogined())
            {
                adapterview = new Intent(mContext, com/ximalaya/ting/android/activity/report/ReportActivity);
                adapterview.putExtra("report_type", 1);
                adapterview.putExtra("track_id", sound.trackId);
                adapterview.putExtra("comment_id", comment.id);
                startActivity(adapterview);
            } else
            {
                adapterview = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                startActivity(adapterview);
            }
            if (true) goto _L1; else goto _L5
_L5:
        }

        _cls22()
        {
            this$0 = PlayerFragment.this;
            comment = commentmodel;
            sound = soundinfo;
            mCommentMenuDialog = menudialog;
            super();
        }
    }


    private class _cls44
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final PlayerFragment this$0;
        final SoundInfo val$info;
        final boolean val$isFirstLike;

        public void onExecute()
        {
            mLikeTask = new LikeTask();
            mLikeTask.myexec(new Object[] {
                Long.valueOf(info.trackId), Boolean.valueOf(true), Boolean.valueOf(info.is_favorited), Boolean.valueOf(isFirstLike)
            });
        }

        _cls44()
        {
            this$0 = PlayerFragment.this;
            info = soundinfo;
            isFirstLike = flag;
            super();
        }
    }


    private class _cls43
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final PlayerFragment this$0;

        public void onExecute()
        {
            if (mSoundInfo.is_favorited)
            {
                mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f02007d, 0, 0);
                return;
            } else
            {
                mLikeBtn.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f02007c, 0, 0);
                return;
            }
        }

        _cls43()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls62
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final PlayerFragment this$0;

        public void execute(final String url)
        {
            if (getActivity().isFinishing())
            {
                return;
            } else
            {
                class _cls1
                    implements Runnable
                {

                    final _cls62 this$1;
                    final String val$url;

                    public void run()
                    {
                        Object obj = new AdCollectData();
                        ((AdCollectData) (obj)).setAdItemId((new StringBuilder()).append("").append(mNowAd.getAdid()).toString());
                        ((AdCollectData) (obj)).setAdSource("0");
                        ((AdCollectData) (obj)).setAndroidId(ToolUtil.getAndroidId(mAppContext));
                        ((AdCollectData) (obj)).setLogType("tingClick");
                        ((AdCollectData) (obj)).setPositionName("comm_top");
                        ((AdCollectData) (obj)).setResponseId((new StringBuilder()).append("").append(mNowAd.getAdid()).toString());
                        ((AdCollectData) (obj)).setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                        Object obj1 = PlayListControl.getPlayListManager().getCurSound();
                        if (obj1 != null)
                        {
                            ((AdCollectData) (obj)).setTrackId((new StringBuilder()).append("").append(((SoundInfo) (obj1)).trackId).toString());
                        } else
                        {
                            ((AdCollectData) (obj)).setTrackId("-1");
                        }
                        obj = AdManager.getInstance().getAdRealJTUrl(url, ((AdCollectData) (obj)));
                        obj1 = new Intent(getActivity(), com/ximalaya/ting/android/library/service/DownloadService);
                        ((Intent) (obj1)).putExtra("download_url", ((String) (obj)));
                        getActivity().startService(((Intent) (obj1)));
                    }

                _cls1()
                {
                    this$1 = _cls62.this;
                    url = s;
                    super();
                }
                }

                getActivity().runOnUiThread(new _cls1());
                return;
            }
        }

        _cls62()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls46
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final PlayerFragment this$0;
        final boolean val$isFollow;
        final long val$toUid;
        final View val$v;

        public void onExecute()
        {
            setFollowRequest(toUid, isFollow, v);
        }

        _cls46()
        {
            this$0 = PlayerFragment.this;
            toUid = l;
            isFollow = flag;
            v = view;
            super();
        }
    }


    private class _cls45
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final PlayerFragment this$0;
        final View val$v;

        public void onExecute()
        {
            v.setTag(Boolean.valueOf(true));
        }

        _cls45()
        {
            this$0 = PlayerFragment.this;
            v = view;
            super();
        }
    }


    private class _cls69
        implements Runnable
    {

        final PlayerFragment this$0;

        public void run()
        {
            mDeviceBtn.setVisibility(8);
            if (isSearching)
            {
                openDevicesSelect();
            }
            if (getDlnaManager().getLinkedDeviceModel() == null)
            {
                mDeviceBtn.setImageResource(0x7f0201e2);
                return;
            } else
            {
                mDeviceBtn.setImageResource(0x7f0201e1);
                return;
            }
        }

        _cls69()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls11
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final PlayerFragment this$0;

        public void execute(final String url)
        {
            if (getActivity().isFinishing())
            {
                return;
            } else
            {
                class _cls1
                    implements Runnable
                {

                    final _cls11 this$1;
                    final String val$url;

                    public void run()
                    {
                        Object obj = new AdCollectData();
                        ((AdCollectData) (obj)).setAdItemId((new StringBuilder()).append("").append(mNowAd.getAdid()).toString());
                        ((AdCollectData) (obj)).setAdSource("0");
                        ((AdCollectData) (obj)).setAndroidId(ToolUtil.getAndroidId(mAppContext));
                        ((AdCollectData) (obj)).setLogType("tingClick");
                        ((AdCollectData) (obj)).setPositionName("comm_top");
                        ((AdCollectData) (obj)).setResponseId((new StringBuilder()).append("").append(mNowAd.getAdid()).toString());
                        ((AdCollectData) (obj)).setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                        Object obj1 = PlayListControl.getPlayListManager().getCurSound();
                        if (obj1 != null)
                        {
                            ((AdCollectData) (obj)).setTrackId((new StringBuilder()).append("").append(((SoundInfo) (obj1)).trackId).toString());
                        } else
                        {
                            ((AdCollectData) (obj)).setTrackId("-1");
                        }
                        obj = AdManager.getInstance().getAdRealJTUrl(url, ((AdCollectData) (obj)));
                        obj1 = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
                        ((Intent) (obj1)).putExtra("ExtraUrl", ((String) (obj)));
                        getActivity().startActivity(((Intent) (obj1)));
                    }

                _cls1()
                {
                    this$1 = _cls11.this;
                    url = s;
                    super();
                }
                }

                getActivity().runOnUiThread(new _cls1());
                return;
            }
        }

        _cls11()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls7 extends com.ximalaya.ting.android.b.a
    {

        List listTemp;
        final PlayerFragment this$0;

        private void initAdsForTest()
        {
            AppAd appad = new AppAd();
            appad.setName("Android \u8BC4\u8BBA\u9876\u90E8");
            appad.cover = "http://fdfs.test.ximalaya.com/group1/M01/15/4F/wKgDplONmRSAQkQqAAITteyBm0U269.jpg";
            appad.link = "http://ad.test.ximalaya.com/adrecord?ad=1080&jt=http%3A%2F%2Fwww.ximalaya.com";
            appad.setIsAuto(false);
            appad.setDescription("Android \u8BC4\u8BBA\u9876\u90E8");
            appad.setLinkType(1);
            appad.setDisplayType(1);
            mCommTopAdList.add(appad);
            AppAd appad1 = new AppAd();
            appad1.setName("Android \u8BC4\u8BBA\u9876\u90E8");
            appad1.cover = "http://fdfs.test.ximalaya.com/group1/M01/15/4F/wKgDplONmRSAQkQqAAITteyBm0U269.jpg";
            appad1.link = "http://ad.test.ximalaya.com/adrecord?ad=1080&jt=http%3A%2F%2Fwww.ximalaya.com";
            appad.setIsAuto(false);
            appad.setDescription("Android \u8BC4\u8BBA\u9876\u90E8");
            appad.setLinkType(1);
            appad.setDisplayType(1);
            mCommTopAdList.add(appad1);
            if (mSoundInfo.trackId % 2L == 0L)
            {
                AppAd appad2 = new AppAd();
                appad2.setName("Android \u8BC4\u8BBA\u9876\u90E8");
                appad2.cover = "http://fdfs.test.ximalaya.com/group1/M01/15/4F/wKgDplONmRSAQkQqAAITteyBm0U269.jpg";
                appad2.link = "http://ad.test.ximalaya.com/adrecord?ad=1080&jt=http%3A%2F%2Fwww.ximalaya.com";
                appad.setIsAuto(false);
                appad.setDescription("Android \u8BC4\u8BBA\u9876\u90E8");
                appad.setLinkType(1);
                appad.setDisplayType(1);
                mCommTopAdList.add(appad2);
            }
        }

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onFinish()
        {
            super.onFinish();
            if (!canGoon())
            {
                return;
            }
            if (mCommTopAdList == null)
            {
                mCommTopAdList = new ArrayList();
            }
            if (mCommTopAdList != null && mCommTopAdList.size() != 0)
            {
                adLayout.setVisibility(0);
                mTopCommAdImageView.setVisibility(0);
                mCloseAds.setVisibility(0);
                showAdImage();
                for (int i = 0; i < mCommTopAdList.size(); i++)
                {
                    AppAd appad = (AppAd)mCommTopAdList.get(i);
                    ThirdAdStatUtil.getInstance().thirdAdStatRequest(appad.getThirdStatUrl());
                    AdCollectData adcollectdata = new AdCollectData();
                    adcollectdata.setAdItemId((new StringBuilder()).append(appad.getAdid()).append("").toString());
                    adcollectdata.setAdSource("0");
                    adcollectdata.setAndroidId(ToolUtil.getAndroidId(mCon));
                    adcollectdata.setLogType("tingShow");
                    adcollectdata.setPositionName("comm_top");
                    adcollectdata.setResponseId((new StringBuilder()).append(appad.getAdid()).append("").toString());
                    adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                    adcollectdata.setTrackId("-1");
                    DataCollectUtil.getInstance(mCon.getApplicationContext()).statOnlineAd(adcollectdata);
                }

            } else
            {
                stopAutoSwapAds();
                adLayout.setVisibility(8);
                mTopCommAdImageView.setVisibility(8);
                mCloseAds.setVisibility(8);
            }
            mIsChangeForAd = false;
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s))
            {
                try
                {
                    s = JSON.parseObject(s);
                    if (s.getIntValue("ret") == 0)
                    {
                        listTemp = JSON.parseArray(s.getString("data"), com/ximalaya/ting/android/library/model/AppAd);
                    }
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    listTemp = new ArrayList();
                }
                if (mCommTopAdList == null)
                {
                    mCommTopAdList = new ArrayList();
                }
                mCommTopAdList.clear();
                if (listTemp != null)
                {
                    mCommTopAdList.addAll(listTemp);
                    return;
                }
            }
        }

        _cls7()
        {
            this$0 = PlayerFragment.this;
            super();
            listTemp = null;
        }
    }


    private class _cls9
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            PlayerFragment.mIsShowAds = false;
            removeAdsHeader();
        }

        _cls9()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls10
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mCommTopAdList != null && mCommTopAdList.size() != 0 && mNowAd != null)
            {
                if (mNowAd.getILinkType() == 1 || mNowAd.getILinkType() == 0)
                {
                    goToWeb(mNowAd.getILink());
                    return;
                }
                if (mNowAd.getILinkType() == 2)
                {
                    downloadFile(mNowAd.getILink());
                    return;
                }
            }
        }

        _cls10()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls13
        implements com.ximalaya.ting.android.view.emotion.EmotionSelector.OnSendButtonClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view, CharSequence charsequence)
        {
            view = charsequence.toString();
            if (mCommentType == 1 && TextUtils.isEmpty(view))
            {
                showToast("\u8BF7\u8F93\u5165\u8BC4\u8BBA");
            } else
            if (ToolUtil.isConnectToNetwork(mContext))
            {
                if (mSendCommentTask == null || mSendCommentTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
                {
                    mSendCommentTask = new SendCommentTask();
                    charsequence = new StringBuilder();
                    if (mIsShareQQ)
                    {
                        charsequence.append("tQQ");
                    }
                    if (mIsShareQZone)
                    {
                        if (!TextUtils.isEmpty(charsequence.toString()) && !charsequence.toString().endsWith(","))
                        {
                            charsequence.append(",");
                        }
                        charsequence.append("qzone");
                    }
                    if (mIsShareSina)
                    {
                        if (!TextUtils.isEmpty(charsequence.toString()) && !charsequence.toString().endsWith(","))
                        {
                            charsequence.append(",");
                        }
                        charsequence.append("tSina");
                    }
                    if (mIsShareRenn)
                    {
                        if (!TextUtils.isEmpty(charsequence.toString()) && !charsequence.toString().endsWith(","))
                        {
                            charsequence.append(",");
                        }
                        charsequence.append("renren");
                    }
                    if (mCommentType == 1)
                    {
                        mSendCommentTask.myexec(new Object[] {
                            view, "1", null, charsequence.toString(), null, mCommentBtn
                        });
                        return;
                    }
                    if (mCommentType == 2)
                    {
                        mSendCommentTask.myexec(new Object[] {
                            view, "2", (new StringBuilder()).append("").append(mParentId).toString(), charsequence.toString(), null, mMoreBtn
                        });
                        return;
                    } else
                    {
                        mSendCommentTask.myexec(new Object[] {
                            view, "2", null, charsequence.toString(), null, mMoreBtn
                        });
                        return;
                    }
                }
            } else
            {
                showToast("\u6CA1\u6709\u53EF\u7528\u7F51\u7EDC");
                return;
            }
        }

        _cls13()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls14
        implements com.ximalaya.ting.android.view.emotion.EmotionSelector.OnTextChangeListener
    {

        final PlayerFragment this$0;

        public void afterTextChanged(Editable editable)
        {
            if (editable.length() > 140)
            {
                mCommentTips.setText(String.format(mResources.getString(0x7f0900f5), new Object[] {
                    Integer.valueOf(0)
                }));
                return;
            } else
            {
                mCommentTips.setText(String.format(mResources.getString(0x7f0900f5), new Object[] {
                    Integer.valueOf(140 - editable.length())
                }));
                return;
            }
        }

        public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
        {
        }

        public void onTextChanged(CharSequence charsequence, int i, int j, int k)
        {
        }

        _cls14()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls15
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mIsShareSina)
            {
                doShareSina(false);
            } else
            {
                if (mIsBindSina)
                {
                    doShareSina(true);
                    return;
                }
                if (!mIsBindSina)
                {
                    doShareSina(false);
                    view = new Intent(mContext, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
                    lastBindWeibo = 12;
                    view.putExtra("lgflag", lastBindWeibo);
                    startActivityForResult(view, 1114);
                    return;
                }
            }
        }

        _cls15()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls16
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mIsShareQQ)
            {
                doShareQQ(false);
            } else
            {
                if (mIsBindQQ)
                {
                    doShareQQ(true);
                    return;
                }
                if (!mIsBindQQ)
                {
                    doShareQQ(false);
                    view = new Intent(mContext, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
                    lastBindWeibo = 13;
                    view.putExtra("lgflag", lastBindWeibo);
                    startActivityForResult(view, 1114);
                    return;
                }
            }
        }

        _cls16()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls17
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mIsShareQZone)
            {
                doShareQzone(false);
            } else
            {
                if (mIsBindQQ)
                {
                    doShareQzone(true);
                    return;
                }
                if (!mIsBindQQ)
                {
                    doShareQzone(false);
                    view = new Intent(mContext, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
                    lastBindWeibo = 13;
                    view.putExtra("lgflag", lastBindWeibo);
                    startActivityForResult(view, 1114);
                    return;
                }
            }
        }

        _cls17()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls18
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mIsShareRenn)
            {
                doShareRenn(false);
            } else
            {
                if (mIsBindRenn)
                {
                    doShareRenn(true);
                    return;
                }
                if (!mIsBindRenn)
                {
                    doShareRenn(false);
                    view = new Intent(mContext, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
                    lastBindWeibo = 14;
                    view.putExtra("lgflag", lastBindWeibo);
                    startActivityForResult(view, 1114);
                    return;
                }
            }
        }

        _cls18()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls19
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            view = new Intent(mContext, com/ximalaya/ting/android/activity/comments/CarePersonListAct);
            view.putExtra("title", "\u8054\u7CFB\u4EBA");
            startActivityForResult(view, 175);
        }

        _cls19()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls20
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            likeSound();
        }

        _cls20()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls25
        implements RedirectTouchRelativeLayout.TouchUpCallback
    {

        final PlayerFragment this$0;

        public void touchUp()
        {
            statDashangShow();
        }

        _cls25()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls26
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            view = getActivity();
            if (view instanceof MainTabActivity2)
            {
                ((MainTabActivity2)view).onbackPlayFragment();
            }
        }

        _cls26()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls27
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final PlayerFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f1, int j)
        {
        }

        public void onPageSelected(int i)
        {
            mRadioGroup.check((new int[] {
                0x7f0a0080, 0x7f0a0081, 0x7f0a014b
            })[i]);
            ((ScrollTabHolder)viewPagerAdapter.getScrollTabHolders().valueAt(i)).adjustScroll((int)((float)mHeader.getHeight() + ViewHelper.getTranslationY(mHeader)));
        }

        _cls27()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls28
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final PlayerFragment this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            int j;
            boolean flag;
            flag = false;
            j = ((flag) ? 1 : 0);
            i;
            JVM INSTR lookupswitch 3: default 40
        //                       2131361920: 43
        //                       2131361921: 55
        //                       2131362123: 60;
               goto _L1 _L2 _L3 _L4
_L2:
            break; /* Loop/switch isn't completed */
_L1:
            j = ((flag) ? 1 : 0);
_L6:
            mViewPager.setCurrentItem(j);
            return;
_L3:
            j = 1;
            continue; /* Loop/switch isn't completed */
_L4:
            j = 2;
            ToolUtil.onEvent(mContext, "Nowplaying_Related");
            if (true) goto _L6; else goto _L5
_L5:
        }

        _cls28()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls29
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mEmotionSelector != null)
            {
                mEmotionSelector.dismiss();
                mSyncBar.setVisibility(8);
                mBottomMenuBar.setVisibility(0);
                findViewById(0x7f0a0310).setVisibility(8);
            }
        }

        _cls29()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls30
        implements com.ximalaya.ting.android.view.seekbar.MySeekBar.ProgressNubmerFormat
    {

        final PlayerFragment this$0;

        public String format(int i, int j)
        {
            return ToolUtil.toTime((float)i / 1000F);
        }

        public String getFormatString(int i, int j)
        {
            return "00:00";
        }

        _cls30()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls31
        implements android.widget.SeekBar.OnSeekBarChangeListener
    {

        final PlayerFragment this$0;

        public void onProgressChanged(SeekBar seekbar, int i, boolean flag)
        {
            if (flag)
            {
                seekbar = LocalMediaService.getInstance();
                if (seekbar != null)
                {
                    int j = seekbar.getDuration();
                    updateMoveTime(i, j);
                }
            }
        }

        public void onStartTrackingTouch(SeekBar seekbar)
        {
            mForbidProgressUpdate = true;
        }

        public void onStopTrackingTouch(SeekBar seekbar)
        {
            PlayerFragment.mIsSetSeekBar = false;
            forbidSeek();
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            if (localmediaservice != null)
            {
                localmediaservice.seekToProgress(seekbar.getProgress(), seekbar.getMax());
                if (isDlnaInit() && isDlnaDeviceLinked())
                {
                    setDlnaSeek(seekbar);
                }
            }
            mSeekBar.setCanSeek(false);
            mForbidProgressUpdate = false;
            mProgressLabel.setVisibility(4);
        }

        _cls31()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls32
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            mPlayHistoryFragment = new PlayListHostoryFragment();
            view = new Bundle();
            view.putBoolean("exit_when_play", true);
            mPlayHistoryFragment.setArguments(view);
            class _cls1
                implements com.ximalaya.ting.android.view.SlideRightOutView.OnFinishListener
            {

                final _cls32 this$1;

                public boolean onFinish()
                {
                    getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).remove(mPlayHistoryFragment).commitAllowingStateLoss();
                    return true;
                }

                _cls1()
                {
                    this$1 = _cls32.this;
                    super();
                }
            }

            mPlayHistoryFragment.setOnFinishCallback(new _cls1());
            getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).add(0x7f0a0372, mPlayHistoryFragment).commitAllowingStateLoss();
        }

        _cls32()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls33
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mBoundService != null)
            {
                ToolUtil.onEvent(mContext, "Nowplaying_BackOne");
                mBoundService.playPrev(true);
            }
        }

        _cls33()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls34
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mBoundService == null) goto _L2; else goto _L1
_L1:
            ToolUtil.onEvent(mContext, "Nowplaying_Paly");
            if (!isDlnaInit() || getDlnaManager().getLinkedDeviceModel() == null) goto _L4; else goto _L3
_L3:
            getDlnaManager().getNowPlayState();
            JVM INSTR tableswitch 0 1: default 76
        //                       0 77
        //                       1 114;
               goto _L2 _L5 _L6
_L2:
            return;
_L5:
            Logger.d("WiFi", "mPlayOrPauseBtn change mNowPlayState 1");
            getDlnaManager().setNowPlayState(1);
            getDlnaManager().play();
            setPlayerBtnState(1);
            return;
_L6:
            Logger.d("WiFi", "mPlayOrPauseBtn change mNowPlayState 0");
            getDlnaManager().setNowPlayState(0);
            getDlnaManager().pause();
            setPlayerBtnState(0);
            return;
_L4:
            switch (mBoundService.getMediaPlayerState())
            {
            case 1: // '\001'
            case 9: // '\t'
            default:
                return;

            case 0: // '\0'
            case 6: // '\006'
                class _cls1
                    implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                {

                    final _cls34 this$1;

                    public void onExecute()
                    {
                        mBoundService.doPlay();
                    }

                _cls1()
                {
                    this$1 = _cls34.this;
                    super();
                }
                }

                showNoWifiConfirm(new _cls1());
                return;

            case 3: // '\003'
            case 5: // '\005'
            case 7: // '\007'
            case 11: // '\013'
                class _cls2
                    implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                {

                    final _cls34 this$1;

                    public void onExecute()
                    {
                        mBoundService.start();
                    }

                _cls2()
                {
                    this$1 = _cls34.this;
                    super();
                }
                }

                showNoWifiConfirm(new _cls2());
                return;

            case 4: // '\004'
            case 10: // '\n'
                mBoundService.pause();
                return;

            case 2: // '\002'
            case 8: // '\b'
                class _cls3
                    implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                {

                    final _cls34 this$1;

                    public void onExecute()
                    {
                        forbidSeek();
                        mBoundService.restart();
                    }

                _cls3()
                {
                    this$1 = _cls34.this;
                    super();
                }
                }

                showNoWifiConfirm(new _cls3());
                return;
            }
        }

        _cls34()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls35
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mBoundService != null)
            {
                ToolUtil.onEvent(mContext, "Nowplaying_NextOne");
                mBoundService.playNext(true);
            }
        }

        _cls35()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls36
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mSoundInfo != null)
            {
                followUser(mSoundInfo.uid, mFollowContainer);
            }
        }

        _cls36()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls37
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mSoundInfo == null)
            {
                return;
            } else
            {
                gotoHomePage(mSoundInfo.uid, view);
                return;
            }
        }

        _cls37()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls38
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mSoundInfo == null)
            {
                return;
            } else
            {
                gotoHomePage(mSoundInfo.uid, view);
                return;
            }
        }

        _cls38()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls39
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mSoundInfo == null || mAppContext == null)
            {
                return;
            } else
            {
                DownloadTask downloadtask = new DownloadTask(mSoundInfo);
                DownLoadTools downloadtools = DownLoadTools.getInstance();
                downloadtools.goDownload(downloadtask, mAppContext, view);
                downloadtools.release();
                return;
            }
        }

        _cls39()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls40
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            ToolUtil.onEvent(mContext, "Nowplaying_Share");
            if (mSoundInfo != null)
            {
                (new BaseShareDialog(getActivity(), mSoundInfo, mShareBtn)).show();
                return;
            } else
            {
                showToast(getActivity().getString(0x7f0901c4));
                return;
            }
        }

        _cls40()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls41
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (UserInfoMannage.hasLogined())
            {
                mCommentType = 1;
                mEmotionSelector.setHint("\u5728\u6B64\u5904\u5199\u4E0A\u60A8\u7684\u8BC4\u8BBA");
                mEmotionSelector.setVisibility(0);
                mSyncBar.setVisibility(0);
                initLoginUserBindData();
                loadDefaultShareSetting();
                if (mLoadShareSetting == null || mLoadShareSetting.getStatus() == android.os.AsyncTask.Status.FINISHED)
                {
                    mLoadShareSetting = new LoadShareSetting();
                    mLoadShareSetting.myexec(new Void[0]);
                }
                mBottomMenuBar.setVisibility(8);
                findViewById(0x7f0a0310).setVisibility(0);
                return;
            } else
            {
                view = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                view.setFlags(0x20000000);
                startActivity(view);
                return;
            }
        }

        _cls41()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls42
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            view = SharedPreferencesUtil.getInstance(mContext);
            if (!view.getBoolean("new_feature_more_clicked"))
            {
                newFeatureImg.setVisibility(8);
                view.saveBoolean("new_feature_more_clicked", true);
            }
            if (mPlayMoreDiaolog == null)
            {
                view = new ArrayList();
                view.add("\u8F6C\u91C7");
                view.add(getPlayModeStr());
                view.add("\u67E5\u770B\u6240\u5C5E\u4E13\u8F91");
                if (MyDeviceUtil.isShowEnterance(mAppContext, "DLNA\u5165\u53E3\u5F00\u5173"))
                {
                    view.add("\u4ECE\u97F3\u7BB1\u64AD\u653E");
                }
                view.add("\u8BBE\u4E3A\u624B\u673A\u94C3\u58F0");
                view.add("\u4E3E\u62A5");
                class _cls1
                    implements android.widget.AdapterView.OnItemClickListener
                {

                    final _cls42 this$1;

                    public void onItemClick(AdapterView adapterview, View view1, int i, long l)
                    {
                        if (mPlayMoreDiaolog != null)
                        {
                            mPlayMoreDiaolog.dismiss();
                        }
                        break MISSING_BLOCK_LABEL_26;
_L9:
                        do
                        {
                            return;
                        } while (mSoundInfo == null || i < 0 || i >= mPlayMoreDiaolog.getSelections().size());
                        if (((String)mPlayMoreDiaolog.getSelections().get(i)).equals("\u8F6C\u91C7"))
                        {
                            doTransmit();
                            return;
                        }
                        if (!((String)mPlayMoreDiaolog.getSelections().get(i)).equals(getPlayModeStr())) goto _L2; else goto _L1
_L1:
                        ToolUtil.onEvent(mContext, "Nowplaying_PlayMode");
                        SharedPreferencesUtil.getInstance(mAppContext).getInt("play_mode", 0);
                        JVM INSTR tableswitch 0 2: default 204
                    //                                   0 286
                    //                                   1 294
                    //                                   2 302;
                           goto _L3 _L4 _L5 _L6
_L3:
                        adapterview = "\u987A\u5E8F";
                        i = 0;
_L7:
                        view1 = getString(0x7f0901a8, new Object[] {
                            adapterview
                        });
                        mPlayMoreDiaolog.setSelection(1, view1);
                        showToast(getString(0x7f0901a9, new Object[] {
                            adapterview
                        }));
                        SharedPreferencesUtil.getInstance(null).saveInt("play_mode", i);
                        return;
_L4:
                        adapterview = "\u5355\u66F2";
                        i = 1;
                        continue; /* Loop/switch isn't completed */
_L5:
                        i = 2;
                        adapterview = "\u968F\u673A";
                        continue; /* Loop/switch isn't completed */
_L6:
                        i = 3;
                        adapterview = "\u5FAA\u73AF";
                        if (true) goto _L7; else goto _L2
_L2:
                        if (((String)mPlayMoreDiaolog.getSelections().get(i)).equals("\u67E5\u770B\u6240\u5C5E\u4E13\u8F91"))
                        {
                            if (!NetworkUtils.isNetworkAvaliable(mAppContext))
                            {
                                showToast("\u8FDE\u63A5\u7F51\u7EDC\u5931\u8D25");
                                return;
                            }
                            if (mSoundInfoDetail == null)
                            {
                                showToast(getString(0x7f0901e4));
                                return;
                            }
                            if (!mSoundInfoDetail.hasAlbum())
                            {
                                showToast("\u4EB2\uFF0C\u6CA1\u6709\u6240\u5728\u4E13\u8F91");
                                return;
                            } else
                            {
                                adapterview = new AlbumModel();
                                adapterview.uid = mSoundInfoDetail.uid;
                                adapterview.albumId = mSoundInfoDetail.albumId;
                                adapterview = JSON.toJSONString(adapterview);
                                view1 = new Bundle();
                                view1.putString("album", adapterview);
                                view1.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(mMoreBtn));
                                startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, view1);
                                return;
                            }
                        }
                        if (((String)mPlayMoreDiaolog.getSelections().get(i)).equals("\u4ECE\u97F3\u7BB1\u64AD\u653E"))
                        {
                            isSearching = true;
                            class _cls1 extends TimerTask
                            {

                                final _cls1 this$2;

                                public void run()
                                {
                                    isSearching = false;
                                }

                                _cls1()
                                {
                                    this$2 = _cls1.this;
                                    super();
                                }
                            }

                            (new Timer()).schedule(new _cls1(), 60000L);
                            if (MyDeviceManager.getInstance(mAppContext).isDlnaInit())
                            {
                                CustomToast.showToast(mAppContext, "\u6B63\u5728\u67E5\u627E\u5468\u56F4\u7684WiFi\u97F3\u7BB1", 0);
                                DlnaManager.getInstance(mAppContext).startScanThread(15);
                                freshDeviceBtn();
                                return;
                            } else
                            {
                                class _cls2
                                    implements Runnable
                                {

                                    final _cls1 this$2;

                                    public void run()
                                    {
                                        DexManager.getInstance(mAppContext).setIsDlnaOpen(true);
                                        CustomToast.showToast(mAppContext, "\u6B63\u5728\u67E5\u627E\u5468\u56F4\u7684WiFi\u97F3\u7BB1", 0);
                                        ((MainTabActivity2)getActivity()).tingshubaoBind();
                                        DlnaManager.getInstance(mAppContext).startScanThread(15);
                                        freshDeviceBtn();
                                    }

                                _cls2()
                                {
                                    this$2 = _cls1.this;
                                    super();
                                }
                                }

                                DexManager.getInstance(mAppContext).loadDlna(new _cls2(), 1);
                                return;
                            }
                        }
                        if (((String)mPlayMoreDiaolog.getSelections().get(i)).equals("\u8BBE\u4E3A\u624B\u673A\u94C3\u58F0"))
                        {
                            doSetCallingRingtone();
                            return;
                        }
                        if (((String)mPlayMoreDiaolog.getSelections().get(i)).equals("\u4E3E\u62A5"))
                        {
                            if (UserInfoMannage.hasLogined())
                            {
                                adapterview = new Intent(mContext, com/ximalaya/ting/android/activity/report/ReportActivity);
                                adapterview.putExtra("report_type", 0);
                                adapterview.putExtra("track_id", mSoundInfo.trackId);
                                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(mMoreBtn));
                                mContext.startActivity(adapterview);
                                return;
                            } else
                            {
                                adapterview = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                                adapterview.setFlags(0x20000000);
                                mContext.startActivity(adapterview);
                                return;
                            }
                        }
                        if (true) goto _L9; else goto _L8
_L8:
                    }

                _cls1()
                {
                    this$1 = _cls42.this;
                    super();
                }
                }

                class _cls2
                    implements com.ximalaya.ting.android.library.view.dialog.MenuDialog.ExtraCallback
                {

                    final _cls42 this$1;

                    public void execute(String s, com.ximalaya.ting.android.library.view.dialog.MenuDialog.ViewHolder viewholder)
                    {
                        if ("\u8BBE\u4E3A\u624B\u673A\u94C3\u58F0".equals(s))
                        {
                            if (!SharedPreferencesUtil.getInstance(mContext).getBoolean("new_feature_ringtone"))
                            {
                                viewholder.icon.setVisibility(0);
                                return;
                            } else
                            {
                                viewholder.icon.setVisibility(8);
                                return;
                            }
                        } else
                        {
                            viewholder.icon.setVisibility(8);
                            return;
                        }
                    }

                _cls2()
                {
                    this$1 = _cls42.this;
                    super();
                }
                }

                mPlayMoreDiaolog = new MenuDialog(getActivity(), view, a.e, new _cls1(), new _cls2());
            }
            mPlayMoreDiaolog.setSelection(1, getPlayModeStr());
            mPlayMoreDiaolog.show();
        }

        _cls42()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls12
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mCommentBtn != null)
            {
                mCommentBtn.performClick();
            }
        }

        _cls12()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls48
        implements android.view.ViewTreeObserver.OnPreDrawListener
    {

        final PlayerFragment this$0;
        final int val$screenWidth;
        final View val$soundCoverSection;

        public boolean onPreDraw()
        {
            mSoundCover.getViewTreeObserver().removeOnPreDrawListener(this);
            int i = (int)(0.59999999999999998D * (double)screenWidth);
            int j = soundCoverSection.getHeight() - Utilities.dip2px(mContext, 10F);
            if (i > j)
            {
                i = j;
            }
            Object obj = mSoundCover.getLayoutParams();
            obj.width = i;
            obj.height = i;
            mSoundCover.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
            obj = mHeader.findViewById(0x7f0a0620);
            mHeaderHeight = mHeader.getHeight() - Utilities.dip2px(mContext, 2.0F);
            if (viewPagerAdapter != null)
            {
                viewPagerAdapter.setHeaderHeight(mHeaderHeight);
            }
            mMinHeaderTranslation = ((View) (obj)).getHeight() - mHeaderHeight;
            ((RedirectTouchRelativeLayout)findViewById(0x7f0a061e)).setHeaderMaxTranslation(-mMinHeaderTranslation);
            return false;
        }

        _cls48()
        {
            this$0 = PlayerFragment.this;
            screenWidth = i;
            soundCoverSection = view;
            super();
        }
    }



    private class _cls50
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mSoundInfo == null)
            {
                return;
            }
            ToolUtil.onEvent(mContext, "Nowplaying_ZhuanJi");
            FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
            fragmenttransaction.setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020);
            if (mPlaylistFragment == null)
            {
                mPlaylistFragment = PlaylistFragment.getInstance();
                Bundle bundle = new Bundle();
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                mPlaylistFragment.setArguments(bundle);
                class _cls1
                    implements com.ximalaya.ting.android.view.SlideView.OnFinishListener
                {

                    final _cls50 this$1;

                    public boolean onFinish()
                    {
                        if (mPlaylistFragment != null && mPlaylistFragment.isAdded())
                        {
                            getChildFragmentManager().beginTransaction().remove(mPlaylistFragment).commitAllowingStateLoss();
                            mPlaylistFragment = null;
                            return true;
                        } else
                        {
                            return false;
                        }
                    }

                _cls1()
                {
                    this$1 = _cls50.this;
                    Object();
                }
                }

                mPlaylistFragment.setOnFinishListener(new _cls1());
                fragmenttransaction.add(0x7f0a0372, mPlaylistFragment);
                fragmenttransaction.addToBackStack(null);
            } else
            {
                fragmenttransaction.show(mPlaylistFragment);
            }
            fragmenttransaction.commitAllowingStateLoss();
        }

        _cls50()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls51
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            showOperationChooser();
        }

        _cls51()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls52
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mIsSoundAdShowing)
            {
                if (mSoundAd != null)
                {
                    mSoundAd.onClickAd(getActivity());
                }
                return;
            }
            if (mSoundInfo != null)
            {
                mImageViewer = new ImageViewer(mContext);
                mImageViewer.setData(mSoundInfo.trackId);
                mImageViewer.show(mSoundCover);
                return;
            } else
            {
                showToast("\u5C01\u9762\u6B63\u5728\u52A0\u8F7D\u4E2D\uFF0C\u8BF7\u7A0D\u5019\uFF01");
                return;
            }
        }

        _cls52()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls53
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (mSoundCover != null)
            {
                if (mIsSoundAdShowing)
                {
                    updateSoundCoverImage(mSoundInfo.coverLarge);
                    if (mSoundAd != null)
                    {
                        mSoundAd.onPicAdClose();
                    }
                } else
                if (mSoundAd != null)
                {
                    showSoundAdPic(mSoundAd, true);
                    return;
                }
            }
        }

        _cls53()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls24
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final PlayerFragment this$0;
        final boolean val$isFirstLike;

        public void onExecute()
        {
            mLikeTask = new LikeTask();
            mLikeTask.myexec(new Object[] {
                Long.valueOf(mSoundInfo.trackId), Boolean.valueOf(true), Boolean.valueOf(mSoundInfo.is_favorited), Boolean.valueOf(isFirstLike)
            });
        }

        _cls24()
        {
            this$0 = PlayerFragment.this;
            isFirstLike = flag;
            super();
        }
    }


    private class _cls23
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final PlayerFragment this$0;
        final boolean val$isFirstLike;

        public void onExecute()
        {
            mLikeTask = new LikeTask();
            mLikeTask.myexec(new Object[] {
                Long.valueOf(mSoundInfo.trackId), Boolean.valueOf(false), Boolean.valueOf(mSoundInfo.is_favorited), Boolean.valueOf(isFirstLike)
            });
        }

        _cls23()
        {
            this$0 = PlayerFragment.this;
            isFirstLike = flag;
            super();
        }
    }


    private class _cls66 extends MenuDialog
    {

        final PlayerFragment this$0;

        protected void onStop()
        {
            if (MyDeviceManager.getInstance(mAppContext).isDlnaInit())
            {
                DlnaManager.getInstance(mAppContext).stopScanThread();
            }
            isSearching = false;
            super.onStop();
        }

        _cls66(Activity activity, List list, boolean flag, android.widget.AdapterView.OnItemClickListener onitemclicklistener, int i)
        {
            this$0 = PlayerFragment.this;
            super(activity, list, flag, onitemclicklistener, i);
        }
    }


    private class _cls65
        implements android.widget.AdapterView.OnItemClickListener
    {

        final PlayerFragment this$0;
        final ArrayList val$devices;

        public void onItemClick(final AdapterView deviceItem, View view, int i, long l)
        {
            if (i > mDeviceMenuDialog.getSelections().size() - 1 || i < 0)
            {
                return;
            }
            if (mDeviceMenuDialog != null)
            {
                mDeviceMenuDialog.showListNewIcon(i);
                mDeviceMenuDialog.dismiss();
            }
            TingMediaPlayer.getTingMediaPlayer(mContext).mediaplayer.setVolume(0.0F, 0.0F);
            if (mDeviceMenuDialog.getSelections().size() - 1 == i)
            {
                change2Bendi(false);
                return;
            }
            if (i < devices.size())
            {
                freshDeviceBtn();
            }
            deviceItem = new ArrayList();
            deviceItem.addAll((ArrayList)getDlnaManager().getAllDevices());
            deviceItem = (BaseDeviceItem)deviceItem.get(i);
            (new Timer()).schedule(new _cls1(), 500L);
        }

        _cls65()
        {
            this$0 = PlayerFragment.this;
            devices = arraylist;
            super();
        }
    }


    private class _cls5
        implements android.widget.AbsListView.OnScrollListener
    {

        final PlayerFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || !moreDataAvailable())
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        showFooterView(FooterView.LOADING, viewPagerAdapter.getCommentFragment().getListView(), mFootView);
                        mNeedUpdate = true;
                        updateSoundCommentsData(viewPagerAdapter.getCommentFragment().getListView());
                    }
                }
                return;
            }
            showFooterView(FooterView.HIDE_ALL, viewPagerAdapter.getCommentFragment().getListView(), mFootView);
        }

        _cls5()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final PlayerFragment this$0;

        public void onClick(View view)
        {
            if (!mIsLoading)
            {
                showFooterView(FooterView.LOADING, viewPagerAdapter.getCommentFragment().getListView(), mFootView);
                updateSoundCommentsData(mFootView);
            }
        }

        _cls6()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls70
        implements IActionCallBack
    {

        final PlayerFragment this$0;

        public void onFailed()
        {
            onDlnaSeekBarFinish();
        }

        public void onSuccess(ActionModel actionmodel)
        {
            onDlnaSeekBarFinish();
        }

        _cls70()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls47 extends MyAsyncTask
    {

        ProgressDialog pd;
        final PlayerFragment this$0;
        final boolean val$isFollow;
        final long val$toUid;
        final View val$v;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            return CommonRequest.doSetGroup(mContext, (new StringBuilder()).append(toUid).append("").toString(), isFollow, v, v);
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if (mContext == null || !isAdded())
            {
                return;
            }
            pd.dismiss();
            if (s == null)
            {
                boolean flag;
                if (!isFollow)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                setConcernBtnStatus(flag);
                v.setTag(Boolean.valueOf(flag));
                return;
            } else
            {
                setConcernBtnStatus(isFollow);
                v.setTag(Boolean.valueOf(isFollow));
                showToast(s);
                return;
            }
        }

        protected void onPreExecute()
        {
            pd.setTitle("\u8BF7\u7A0D\u5019...");
            pd.show();
        }

        _cls47()
        {
            this$0 = PlayerFragment.this;
            toUid = l;
            isFollow = flag;
            v = view;
            super();
            pd = new MyProgressDialog(mContext, 0);
        }
    }


    private class _cls8
        implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
    {

        final PlayerFragment this$0;

        public void onCompleteDisplay(String s, Bitmap bitmap)
        {
            makeAdVisible();
            startAutoSwapAds();
        }

        _cls8()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls54
        implements android.widget.AdapterView.OnItemClickListener
    {

        final PlayerFragment this$0;
        final MenuDialog val$dialog;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (dialog != null)
            {
                dialog.dismiss();
            }
            switch (i)
            {
            default:
                return;

            case 0: // '\0'
                ToolUtil.onEvent(mContext, "Nowplaying_DingShi");
                startActivity(new Intent(mContext, com/ximalaya/ting/android/activity/setting/PlanTerminateActivity));
                return;

            case 1: // '\001'
                adapterview = new Intent(mContext, com/ximalaya/ting/android/activity/setting/WakeUpSettingActivity);
                break;
            }
            adapterview.putExtra("from", 1);
            startActivity(adapterview);
        }

        _cls54()
        {
            this$0 = PlayerFragment.this;
            dialog = menudialog;
            super();
        }
    }


    private class _cls56
        implements Runnable
    {

        final PlayerFragment this$0;

        public void run()
        {
            mShowShareToastTv.setVisibility(8);
        }

        _cls56()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls68
        implements Runnable
    {

        final PlayerFragment this$0;

        public void run()
        {
            afterDeviceControl();
        }

        _cls68()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls61
        implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
    {

        final PlayerFragment this$0;
        final android.media.RemoteControlClient.MetadataEditor val$editor;

        public void onCompleteDisplay(String s, Bitmap bitmap)
        {
            if (bitmap != null)
            {
                android.media.RemoteControlClient.MetadataEditor metadataeditor = editor;
                if (bitmap.getConfig() == null)
                {
                    s = android.graphics.Bitmap.Config.ARGB_8888;
                } else
                {
                    s = bitmap.getConfig();
                }
                metadataeditor.putBitmap(100, bitmap.copy(s, false));
                editor.apply();
            }
        }

        _cls61()
        {
            this$0 = PlayerFragment.this;
            editor = metadataeditor;
            super();
        }
    }


    private class _cls59
        implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
    {

        final PlayerFragment this$0;

        public void onCompleteDisplay(String s, Bitmap bitmap)
        {
            setBlurBackground(s, mSoundCover.getDrawable());
        }

        _cls59()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls67
        implements Runnable
    {

        final PlayerFragment this$0;
        final boolean val$needPause;

        public void run()
        {
            getDlnaManager().change2Bendi();
            freshDeviceBtn();
            updatePlayPauseSwitchButton();
            TingMediaPlayer.getTingMediaPlayer(mContext).mediaplayer.setVolume(1.0F, 1.0F);
            if (mLocalProcess != 0)
            {
                Logger.d("tuisong", (new StringBuilder()).append("change2Bendi").append(mLocalProcess).toString());
                changeSeekBar(mLocalProcess);
                LocalMediaService localmediaservice = LocalMediaService.getInstance();
                if (localmediaservice != null)
                {
                    mBoundService.pause();
                    Logger.d("tuisong", "change2Bendi setProgress");
                    Logger.d("tuisong", (new StringBuilder()).append("change2Bendi ").append(mSoundInfo.duration).toString());
                    localmediaservice.seekToProgress(mLocalProcess / 1000, (int)mSoundInfo.duration);
                }
            }
            if (mDeviceMenuDialog != null)
            {
                List list = mDeviceMenuDialog.getSelections();
                class _cls1
                    implements Runnable
                {

                    final _cls67 this$1;

                    public void run()
                    {
                        class _cls1
                            implements Runnable
                        {

                            final _cls1 this$2;

                            public void run()
                            {
                                mBoundService.pause();
                                if (MyApplication.a() != null && (MyApplication.a() instanceof MainTabActivity2))
                                {
                                    ((MainTabActivity2)MyApplication.a()).stopPlayAnimation();
                                }
                            }

                                _cls1()
                                {
                                    this$2 = _cls1.this;
                                    super();
                                }
                        }

                        try
                        {
                            Thread.sleep(2000L);
                        }
                        catch (InterruptedException interruptedexception)
                        {
                            interruptedexception.printStackTrace();
                        }
                        getActivity().runOnUiThread(new _cls1());
                    }

                _cls1()
                {
                    this$1 = _cls67.this;
                    super();
                }
                }

                if (list == null)
                {
                    mDeviceMenuDialog.showListNewIcon(0);
                } else
                {
                    mDeviceMenuDialog.showListNewIcon(list.size());
                }
            }
            if (needPause)
            {
                (new Thread(new _cls1())).start();
                Logger.d("tuisong", "Pause");
                return;
            } else
            {
                Logger.d("tuisong", "Start");
                mBoundService.start();
                return;
            }
        }

        _cls67()
        {
            this$0 = PlayerFragment.this;
            needPause = flag;
            super();
        }
    }


    private class _cls71
        implements Runnable
    {

        final PlayerFragment this$0;

        public void run()
        {
            ArrayList arraylist1;
            boolean flag;
            flag = false;
            arraylist1 = new ArrayList();
            arraylist1.addAll((ArrayList)getDlnaManager().getAllDevices());
            if (arraylist1 != null && arraylist1.size() != 0) goto _L2; else goto _L1
_L1:
            change2Bendi(true);
_L4:
            freshDeviceBtn();
            return;
_L2:
            ArrayList arraylist;
            int j;
            int k;
            if (MyDeviceManager.getInstance(mAppContext).isDlnaInit() && DlnaManager.getInstance(mAppContext).getLinkedDeviceModel() != null && DlnaManager.getInstance(mAppContext).getLinkedDeviceModel().getNowDeviceItem() != null && !arraylist1.contains(DlnaManager.getInstance(mAppContext).getLinkedDeviceModel().getNowDeviceItem()))
            {
                change2Bendi(true);
            }
            arraylist = new ArrayList();
            for (int i = 0; i < arraylist1.size(); i++)
            {
                arraylist.add(DeviceUtil.getDeviceItemName((BaseDeviceItem)arraylist1.get(i)));
            }

            arraylist.add("\u672C\u5730");
            if (mDeviceMenuDialog == null)
            {
                continue; /* Loop/switch isn't completed */
            }
            k = mDeviceMenuDialog.getListNewIconPosition();
            if (k >= 0)
            {
                break; /* Loop/switch isn't completed */
            }
            j = 0;
_L5:
            String s = (String)mDeviceMenuDialog.getSelections().get(j);
            mDeviceMenuDialog.setSelections(arraylist);
            mDeviceMenuDialog.showListNewIcon(mDeviceMenuDialog.getSelections().size() - 1);
            j = ((flag) ? 1 : 0);
            while (j < arraylist.size()) 
            {
                if (((String)arraylist.get(j)).equals(s))
                {
                    mDeviceMenuDialog.showListNewIcon(j);
                }
                j++;
            }
            if (true) goto _L4; else goto _L3
_L3:
            j = k;
            if (k >= mDeviceMenuDialog.getSelections().size())
            {
                j = mDeviceMenuDialog.getSelections().size() - 1;
            }
              goto _L5
            if (true) goto _L4; else goto _L6
_L6:
        }

        _cls71()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls57
        implements Runnable
    {

        final PlayerFragment this$0;

        public void run()
        {
            if (isVisible())
            {
                mSoundTitle.setSelected(true);
            }
        }

        _cls57()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }


    private class _cls60
        implements Runnable
    {

        final PlayerFragment this$0;

        public void run()
        {
            afterDeviceControl();
        }

        _cls60()
        {
            this$0 = PlayerFragment.this;
            super();
        }
    }

}
