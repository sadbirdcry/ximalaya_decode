// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.Playlist;
import com.ximalaya.ting.android.view.SlideView;
import com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshListView2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlaylistAdapter

public class PlaylistFragment extends BaseListFragment
    implements android.view.View.OnClickListener
{

    private PlaylistAdapter mAdapter;
    private boolean mIsDesc;
    private boolean mIsLoad;
    private com.ximalaya.ting.android.view.SlideView.OnFinishListener mOnFinishListener;
    private PullToRefreshListView2 mRefreshableListView;
    private SlideView mRootView;
    private TextView mSortBtn;

    public PlaylistFragment()
    {
        mIsLoad = false;
    }

    public static PlaylistFragment getInstance()
    {
        return new PlaylistFragment();
    }

    private int getPlayingPosition()
    {
        SoundInfo soundinfo;
        int i;
        soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo == null)
        {
            return 0;
        }
        i = 0;
_L3:
        if (i >= mAdapter.getData().size())
        {
            break MISSING_BLOCK_LABEL_67;
        }
        if (soundinfo.trackId != ((SoundInfo)mAdapter.getData().get(i)).trackId) goto _L2; else goto _L1
_L1:
        return i;
_L2:
        i++;
          goto _L3
        i = 0;
          goto _L1
    }

    private void loadMore()
    {
        if (!mIsLoad)
        {
            mIsLoad = true;
            (new _cls6()).myexec(new Void[0]);
        }
    }

    private void setPlayingPosition()
    {
        int k = getPlayingPosition();
        int i = ((ListView)mRefreshableListView.getRefreshableView()).getFirstVisiblePosition();
        int l = getResources().getDisplayMetrics().heightPixels;
        View view = mListView.getChildAt(0);
        if (view != null)
        {
            int i1 = view.getHeight();
            if (i1 != 0)
            {
                l /= i1;
                if (k < i || k >= i + l)
                {
                    ListView listview = (ListView)mRefreshableListView.getRefreshableView();
                    int j = k;
                    if (k - 3 > 0)
                    {
                        j = k - 3;
                    }
                    listview.setSelection(j);
                }
            }
        }
    }

    private void setSortStatus()
    {
        if (mIsDesc)
        {
            mSortBtn.setCompoundDrawablesWithIntrinsicBounds(0x7f0202c6, 0, 0, 0);
            mRefreshableListView.setMode(com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.PULL_FROM_START);
            return;
        } else
        {
            mSortBtn.setCompoundDrawablesWithIntrinsicBounds(0x7f0202c5, 0, 0, 0);
            mRefreshableListView.setMode(com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.DISABLED);
            return;
        }
    }

    private void toggleOrder()
    {
        ArrayList arraylist = new ArrayList(PlayListControl.getPlayListManager().getPlaylist().getData());
        Collections.reverse(arraylist);
        PlayListControl.getPlayListManager().updatePlaylist(arraylist);
        Collections.reverse(mAdapter.getData());
        mAdapter.notifyDataSetChanged();
        boolean flag;
        if (!mIsDesc)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        mIsDesc = flag;
        PlayListControl.getPlayListManager().getPlaylist().setIsReverse(mIsDesc);
        setSortStatus();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mRootView.findViewById(0x7f0a007b).setOnClickListener(this);
        mRootView.findViewById(0x7f0a0373).setOnClickListener(this);
        mRootView.setOnFinishListener(new _cls1());
        bundle = new ArrayList(PlayListControl.getPlayListManager().getPlaylist().getData());
        mAdapter = new PlaylistAdapter(getActivity(), bundle);
        mListView.setAdapter(mAdapter);
        mListView.post(new _cls2());
        mIsDesc = PlayListControl.getPlayListManager().getPlaylist().isReverse();
        setSortStatus();
        mListView.setOnItemClickListener(new _cls3());
        mListView.setOnScrollListener(new _cls4());
        mRefreshableListView.setOnRefreshListener(new _cls5());
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131361915: 
            getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).hide(this).commitAllowingStateLoss();
            return;

        case 2131362675: 
            toggleOrder();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mRootView = (SlideView)layoutinflater.inflate(0x7f0300c8, viewgroup, false);
        mRefreshableListView = (PullToRefreshListView2)mRootView.findViewById(0x7f0a0025);
        mListView = (ListView)mRefreshableListView.getRefreshableView();
        mSortBtn = (TextView)mRootView.findViewById(0x7f0a0373);
        return mRootView;
    }

    public void onHiddenChanged(boolean flag)
    {
        super.onHiddenChanged(flag);
        if (!flag)
        {
            if (PlayListControl.getPlayListManager().getPlaylist() != null)
            {
                List list = PlayListControl.getPlayListManager().getPlaylist().getData();
                if (list != null && list.size() > 0)
                {
                    mAdapter.setData(new ArrayList(list));
                }
            }
            setPlayingPosition();
        }
    }

    public void setOnFinishListener(com.ximalaya.ting.android.view.SlideView.OnFinishListener onfinishlistener)
    {
        mOnFinishListener = onfinishlistener;
    }









/*
    static boolean access$702(PlaylistFragment playlistfragment, boolean flag)
    {
        playlistfragment.mIsLoad = flag;
        return flag;
    }

*/

    private class _cls6 extends MyAsyncTask
    {

        final PlaylistFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            return PlayListControl.getPlayListManager().getPlaylist().loadMore();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (mIsDesc)
            {
                mRefreshableListView.onRefreshComplete();
                if (list != null && list.size() > 0)
                {
                    Collections.reverse(list);
                    ArrayList arraylist = new ArrayList(list);
                    arraylist.addAll(PlayListControl.getPlayListManager().getPlaylist().getData());
                    PlayListControl.getPlayListManager().updatePlaylist(arraylist);
                    mAdapter.setData(arraylist);
                    mListView.setSelection(list.size());
                }
            } else
            {
                if (list != null && list.size() > 0)
                {
                    PlayListControl.getPlayListManager().getPlaylist().append(list);
                    mAdapter.addData(list);
                }
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            }
            mIsLoad = false;
            mSortBtn.setEnabled(true);
        }

        protected void onPreExecute()
        {
            mSortBtn.setEnabled(false);
        }

        _cls6()
        {
            this$0 = PlaylistFragment.this;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.view.SlideView.OnFinishListener
    {

        final PlaylistFragment this$0;

        public boolean onFinish()
        {
            if (mOnFinishListener != null)
            {
                return mOnFinishListener.onFinish();
            } else
            {
                return false;
            }
        }

        _cls1()
        {
            this$0 = PlaylistFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final PlaylistFragment this$0;

        public void run()
        {
            setPlayingPosition();
        }

        _cls2()
        {
            this$0 = PlaylistFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final PlaylistFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).hide(PlaylistFragment.this).commitAllowingStateLoss();
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= mAdapter.getData().size())
            {
                return;
            } else
            {
                adapterview = mAdapter.getData();
                PlayTools.gotoPlay(PlayListControl.getPlayListManager().listType, PlayListControl.getPlayListManager().getPlaylist().getDataSource(), PlayListControl.getPlayListManager().getPlaylist().getPageId(), PlayListControl.getPlayListManager().getPlaylist().getParams(), adapterview, i, getActivity().getApplicationContext(), true, DataCollectUtil.getDataFromView(view));
                return;
            }
        }

        _cls3()
        {
            this$0 = PlaylistFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AbsListView.OnScrollListener
    {

        final PlaylistFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (!mIsDesc && i == 0 && PlayListControl.getPlayListManager().getPlaylist().hasMore() && abslistview.getLastVisiblePosition() - 1 >= mAdapter.getCount())
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.MORE);
                loadMore();
            }
        }

        _cls4()
        {
            this$0 = PlaylistFragment.this;
            super();
        }
    }


    private class _cls5
        implements com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.OnRefreshListener
    {

        final PlaylistFragment this$0;

        public void onRefresh(PullToRefreshBase pulltorefreshbase)
        {
            loadMore();
        }

        _cls5()
        {
            this$0 = PlaylistFragment.this;
            super();
        }
    }

}
