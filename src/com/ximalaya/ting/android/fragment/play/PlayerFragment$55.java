// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.ximalaya.ting.android.service.play.LocalMediaService;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerFragment

class this._cls0
    implements ServiceConnection
{

    final PlayerFragment this$0;

    public void onServiceConnected(ComponentName componentname, IBinder ibinder)
    {
        mBoundService = ((com.ximalaya.ting.android.service.play.LocalBinder)ibinder).getService();
        mBoundService.setOnPlayerStatusUpdateListener(PlayerFragment.this);
        mBoundService.setOnPlayServiceUpdateListener(PlayerFragment.this);
        mBoundService.addSoundPatchAdCallback(PlayerFragment.this);
        PlayerFragment.access$10202(PlayerFragment.this, true);
        PlayerFragment.access$10300(PlayerFragment.this);
    }

    public void onServiceDisconnected(ComponentName componentname)
    {
        mBoundService.removeSoundPatchAdCallback(PlayerFragment.this);
        mBoundService = null;
    }

    ocalBinder()
    {
        this$0 = PlayerFragment.this;
        super();
    }
}
