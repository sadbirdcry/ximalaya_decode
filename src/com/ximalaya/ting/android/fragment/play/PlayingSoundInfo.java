// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.album.IAlbum;
import com.ximalaya.ting.android.model.sound.SoundInfoDetail;

public class PlayingSoundInfo
{
    public static class AlbumInfo
        implements IAlbum
    {

        public long albumId;
        public long categoryId;
        public String coverLarge;
        public String coverOrigin;
        public String coverSmall;
        public String coverWebLarge;
        public long createdAt;
        public boolean hasNew;
        public String intro;
        public boolean isFavorite;
        public long playTimes;
        public long playsCounts;
        public int serialState;
        public int serializeStatus;
        public long shares;
        public int status;
        public String tags;
        public String title;
        public long tracks;
        public long uid;
        public long updateAt;

        public String getIAlbumCoverUrl()
        {
            return coverSmall;
        }

        public long getIAlbumId()
        {
            return albumId;
        }

        public long getIAlbumLastUpdateAt()
        {
            return updateAt;
        }

        public long getIAlbumPlayCount()
        {
            return playsCounts;
        }

        public String getIAlbumTag()
        {
            return tags;
        }

        public String getIAlbumTitle()
        {
            return title;
        }

        public long getIAlbumTrackCount()
        {
            return tracks;
        }

        public String getIIntro()
        {
            return intro;
        }

        public boolean isIAlbumCollect()
        {
            return isFavorite;
        }

        public void setIAlbumCollect(boolean flag)
        {
            isFavorite = flag;
        }

        public AlbumInfo()
        {
        }
    }

    public static class AssociationAlbumsInfo
        implements IAlbum
    {

        public long albumId;
        public String coverMiddle;
        public String coverSmall;
        public String intro;
        public String recSrc;
        public String recTrack;
        public String title;
        public long tracks;
        public long uid;
        public long updatedAt;

        public String getIAlbumCoverUrl()
        {
            return coverSmall;
        }

        public long getIAlbumId()
        {
            return albumId;
        }

        public long getIAlbumLastUpdateAt()
        {
            return updatedAt;
        }

        public long getIAlbumPlayCount()
        {
            return 0L;
        }

        public String getIAlbumTag()
        {
            return null;
        }

        public String getIAlbumTitle()
        {
            return title;
        }

        public long getIAlbumTrackCount()
        {
            return tracks;
        }

        public String getIIntro()
        {
            return intro;
        }

        public boolean isIAlbumCollect()
        {
            return false;
        }

        public void setIAlbumCollect(boolean flag)
        {
        }

        public AssociationAlbumsInfo()
        {
        }
    }

    public static class CommentInfo
    {

        public String content;
        public long createdAt;
        public long id;
        public String nickname;
        public long parentId;
        public String smallHeader;
        public long track_id;
        public long uid;

        public CommentInfo()
        {
        }
    }

    public static class CommentsInfo
    {

        public CommentInfo list[];
        public int maxPageId;
        public int pageId;
        public int pageSize;
        public long totalCount;

        public CommentsInfo()
        {
        }
    }

    public static class OtherInfo
    {

        public long followerNum;

        public OtherInfo()
        {
        }
    }

    public static class TrackRewardInfo
    {

        public boolean isOPen;
        public long numOfRewards;
        public long uid;

        public TrackRewardInfo()
        {
        }
    }


    private static final String PATH = "mobile/track/playpage/";
    public AlbumInfo albumInfo;
    public AssociationAlbumsInfo associationAlbumsInfo[];
    public CommentsInfo commentInfo;
    public OtherInfo otherInfo;
    public TrackRewardInfo trackRewardInfo;

    public PlayingSoundInfo()
    {
    }

    public static PlayingSoundInfo getFromRemote(SoundInfoDetail soundinfodetail)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("albumId", soundinfodetail.albumId);
        requestparams.put("trackUid", soundinfodetail.userInfo.uid);
        soundinfodetail = f.a().a((new StringBuilder()).append(a.u).append("mobile/track/playpage/").append(soundinfodetail.trackId).toString(), requestparams, false);
        if (((com.ximalaya.ting.android.b.n.a) (soundinfodetail)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (soundinfodetail)).a))
        {
            break MISSING_BLOCK_LABEL_106;
        }
        soundinfodetail = (PlayingSoundInfo)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (soundinfodetail)).a, com/ximalaya/ting/android/fragment/play/PlayingSoundInfo);
        return soundinfodetail;
        soundinfodetail;
        soundinfodetail.printStackTrace();
        return null;
    }
}
