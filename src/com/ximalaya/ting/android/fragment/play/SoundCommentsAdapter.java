// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.comment.CommentModel;
import com.ximalaya.ting.android.model.comment.CommentModelList;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.util.ArrayList;
import java.util.List;

public class SoundCommentsAdapter extends BaseAdapter
{
    class ViewHolder
    {

        TextView commentContent;
        TextView commentOrder;
        TextView commentTime;
        TextView likeCount;
        final SoundCommentsAdapter this$0;
        RoundedImageView userIcon;
        TextView userName;

        ViewHolder()
        {
            this$0 = SoundCommentsAdapter.this;
            super();
        }
    }


    private CommentModelList mCommentModelList;
    private List mComments;
    private Context mContext;
    private BaseFragment mFra;
    private LayoutInflater mInflater;

    public SoundCommentsAdapter(Context context, BaseFragment basefragment, List list, CommentModelList commentmodellist)
    {
        mContext = context;
        mFra = basefragment;
        mComments = list;
        mCommentModelList = commentmodellist;
        mInflater = LayoutInflater.from(context);
    }

    public void addData(CommentModel commentmodel)
    {
        if (mComments == null)
        {
            mComments = new ArrayList();
        }
        mComments.add(0, commentmodel);
        notifyDataSetChanged();
    }

    public void addData(List list)
    {
        if (mComments == null)
        {
            mComments = new ArrayList(list);
        } else
        {
            mComments.clear();
            mComments.addAll(list);
        }
        notifyDataSetChanged();
    }

    public int getCount()
    {
        if (mComments == null)
        {
            return 0;
        } else
        {
            return mComments.size();
        }
    }

    public Object getItem(int i)
    {
        return mComments.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (view == null)
        {
            view = View.inflate(mContext, 0x7f030060, null);
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, -2));
            viewgroup = new ViewHolder();
            viewgroup.userIcon = (RoundedImageView)view.findViewById(0x7f0a01dd);
            viewgroup.userName = (TextView)view.findViewById(0x7f0a01de);
            viewgroup.commentTime = (TextView)view.findViewById(0x7f0a01df);
            viewgroup.commentOrder = (TextView)view.findViewById(0x7f0a01e0);
            viewgroup.commentContent = (TextView)view.findViewById(0x7f0a01e1);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        if (viewgroup == null)
        {
            return view;
        }
        final CommentModel comment = (CommentModel)mComments.get(i);
        ((ViewHolder) (viewgroup)).userIcon.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).userIcon, comment.smallHeader, 0x7f0202df);
        ((ViewHolder) (viewgroup)).userName.setText(comment.nickname);
        ((ViewHolder) (viewgroup)).commentTime.setText(ToolUtil.convertTime(comment.createdAt));
        if (mCommentModelList != null)
        {
            ((ViewHolder) (viewgroup)).commentOrder.setText((new StringBuilder()).append(mCommentModelList.totalCount - i).append("\u697C").toString());
        }
        ((ViewHolder) (viewgroup)).commentContent.setText(EmotionUtil.getInstance().convertEmotion(comment.content));
        ((ViewHolder) (viewgroup)).userIcon.setOnClickListener(new _cls1());
        return view;
    }

    public void removeData(CommentModel commentmodel)
    {
        while (mComments == null || !mComments.remove(commentmodel)) 
        {
            return;
        }
        notifyDataSetChanged();
    }

    public void setCommentList(CommentModelList commentmodellist)
    {
        mCommentModelList = commentmodellist;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SoundCommentsAdapter this$0;
        final CommentModel val$comment;

        public void onClick(View view)
        {
            Bundle bundle = new Bundle();
            bundle.putLong("toUid", comment.uid);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            mFra.startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
        }

        _cls1()
        {
            this$0 = SoundCommentsAdapter.this;
            comment = commentmodel;
            super();
        }
    }

}
