// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.view.View;
import com.ximalaya.ting.android.model.RewardModel;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayingSoundDetailFragment

private class mTrackId extends MyAsyncTask
{

    private long mTrackId;
    final PlayingSoundDetailFragment this$0;

    protected transient RewardModel doInBackground(Void avoid[])
    {
        return RewardModel.getInstanceFromRemote(mTrackId);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(RewardModel rewardmodel)
    {
        super.onPostExecute(rewardmodel);
        if (getActivity() == null || !isAdded() || isRemoving())
        {
            return;
        }
        if (rewardmodel != null)
        {
            PlayingSoundDetailFragment.access$300(PlayingSoundDetailFragment.this, rewardmodel);
            return;
        } else
        {
            PlayingSoundDetailFragment.access$400(PlayingSoundDetailFragment.this).setVisibility(8);
            return;
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((RewardModel)obj);
    }

    public Y(long l)
    {
        this$0 = PlayingSoundDetailFragment.this;
        super();
        mTrackId = l;
    }
}
