// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import com.alibaba.fastjson.JSONObject;
import java.util.List;

public class SoundRelativeAlbumModel
{

    private long albumId;
    private boolean belongsToAlbum;
    private String coverMiddle;
    private String coverSmall;
    private String intro;
    private long playsCounts;
    private String recSrc;
    private String recTrack;
    private String title;
    private long tracks;
    private long updatedAt;

    public SoundRelativeAlbumModel()
    {
        belongsToAlbum = false;
    }

    public static List getModelListFromJson(String s)
    {
        return JSONObject.parseArray(s, com/ximalaya/ting/android/fragment/play/SoundRelativeAlbumModel);
    }

    public boolean equals(Object obj)
    {
        while (!(obj instanceof SoundRelativeAlbumModel) || ((SoundRelativeAlbumModel)obj).albumId != albumId) 
        {
            return false;
        }
        return true;
    }

    public long getAlbumId()
    {
        return albumId;
    }

    public String getCoverMiddle()
    {
        return coverMiddle;
    }

    public String getCoverSmall()
    {
        return coverSmall;
    }

    public String getIntro()
    {
        return intro;
    }

    public long getPlaysCounts()
    {
        return playsCounts;
    }

    public String getRecSrc()
    {
        return recSrc;
    }

    public String getRecTrack()
    {
        return recTrack;
    }

    public String getTitle()
    {
        return title;
    }

    public long getTracks()
    {
        return tracks;
    }

    public long getUpdatedAt()
    {
        return updatedAt;
    }

    public boolean isBelongsToAlbum()
    {
        return belongsToAlbum;
    }

    public void setAlbumId(long l)
    {
        albumId = l;
    }

    public void setBelongsToAlbum(boolean flag)
    {
        belongsToAlbum = flag;
    }

    public void setCoverMiddle(String s)
    {
        coverMiddle = s;
    }

    public void setCoverSmall(String s)
    {
        coverSmall = s;
    }

    public void setIntro(String s)
    {
        intro = s;
    }

    public void setPlaysCounts(long l)
    {
        playsCounts = l;
    }

    public void setRecSrc(String s)
    {
        recSrc = s;
    }

    public void setRecTrack(String s)
    {
        recTrack = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTracks(long l)
    {
        tracks = l;
    }

    public void setUpdatedAt(long l)
    {
        updatedAt = l;
    }
}
