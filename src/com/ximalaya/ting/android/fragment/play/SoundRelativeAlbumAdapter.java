// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            SoundRelativeAlbumModel

public class SoundRelativeAlbumAdapter extends BaseAdapter
{
    class ViewHolder
    {

        public View border;
        public ImageView cover;
        public TextView headerInfo;
        public TextView lastUpdated;
        public TextView playsCount;
        public TextView soundsCount;
        final SoundRelativeAlbumAdapter this$0;
        public TextView title;

        ViewHolder()
        {
            this$0 = SoundRelativeAlbumAdapter.this;
            super();
        }
    }


    private Context mContext;
    private List mData;
    private LayoutInflater mLayoutInflater;

    public SoundRelativeAlbumAdapter(Context context, List list)
    {
        mLayoutInflater = LayoutInflater.from(context);
        mData = list;
        mContext = context;
    }

    public void addData(List list)
    {
        if (mData == null)
        {
            mData = new ArrayList(list);
        } else
        {
            mData.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void clear()
    {
        if (mData != null)
        {
            mData.clear();
        }
        notifyDataSetChanged();
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public SoundRelativeAlbumModel getModelAt(int i)
    {
        if (mData == null || mData.size() <= i)
        {
            return null;
        } else
        {
            return (SoundRelativeAlbumModel)mData.get(i);
        }
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        SoundRelativeAlbumModel soundrelativealbummodel;
        if (view == null)
        {
            view = mLayoutInflater.inflate(0x7f03013d, viewgroup, false);
            viewgroup = new ViewHolder();
            viewgroup.cover = (ImageView)view.findViewById(0x7f0a0177);
            viewgroup.title = (TextView)view.findViewById(0x7f0a0175);
            viewgroup.soundsCount = (TextView)view.findViewById(0x7f0a026b);
            viewgroup.lastUpdated = (TextView)view.findViewById(0x7f0a04ea);
            viewgroup.headerInfo = (TextView)view.findViewById(0x7f0a04e9);
            viewgroup.playsCount = (TextView)view.findViewById(0x7f0a0151);
            viewgroup.border = view.findViewById(0x7f0a00a8);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        soundrelativealbummodel = (SoundRelativeAlbumModel)mData.get(i);
        if (viewgroup == null)
        {
            return view;
        }
        ((ViewHolder) (viewgroup)).title.setText(soundrelativealbummodel.getTitle());
        ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).cover, soundrelativealbummodel.getCoverMiddle(), 0x7f0202e0);
        Object obj = null;
        if (mData.size() >= 1)
        {
            obj = (SoundRelativeAlbumModel)mData.get(0);
        }
        TextView textview;
        if (i == 0 && obj != null && ((SoundRelativeAlbumModel) (obj)).isBelongsToAlbum())
        {
            ((ViewHolder) (viewgroup)).headerInfo.setText(0x7f0901f9);
            ((ViewHolder) (viewgroup)).headerInfo.setVisibility(0);
        } else
        if (i == 1 && obj != null && ((SoundRelativeAlbumModel) (obj)).isBelongsToAlbum())
        {
            ((ViewHolder) (viewgroup)).headerInfo.setText(0x7f0901fa);
            ((ViewHolder) (viewgroup)).headerInfo.setVisibility(0);
        } else
        {
            ((ViewHolder) (viewgroup)).headerInfo.setVisibility(8);
        }
        if (soundrelativealbummodel.getTracks() > 0L)
        {
            ((ViewHolder) (viewgroup)).soundsCount.setVisibility(0);
            ((ViewHolder) (viewgroup)).soundsCount.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(soundrelativealbummodel.getTracks())).append("\u96C6").toString());
        } else
        {
            ((ViewHolder) (viewgroup)).soundsCount.setVisibility(8);
        }
        if (soundrelativealbummodel.getPlaysCounts() > 0L)
        {
            ((ViewHolder) (viewgroup)).playsCount.setVisibility(0);
            ((ViewHolder) (viewgroup)).playsCount.setText(StringUtil.getFriendlyNumStr(soundrelativealbummodel.getPlaysCounts()));
        } else
        {
            ((ViewHolder) (viewgroup)).playsCount.setVisibility(8);
        }
        textview = ((ViewHolder) (viewgroup)).lastUpdated;
        if (soundrelativealbummodel.getIntro() == null)
        {
            obj = "";
        } else
        {
            obj = soundrelativealbummodel.getIntro();
        }
        textview.setText(((CharSequence) (obj)));
        if (i + 1 == mData.size())
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(8);
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(0);
            return view;
        }
    }

    public void setData(List list)
    {
        if (mData == null)
        {
            mData = new ArrayList(list);
        } else
        {
            mData.clear();
            mData.addAll(list);
        }
        notifyDataSetChanged();
    }
}
