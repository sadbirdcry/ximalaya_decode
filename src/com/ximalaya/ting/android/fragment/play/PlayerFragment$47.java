// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.play;

import android.app.ProgressDialog;
import android.view.View;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.play:
//            PlayerFragment

class this._cls0 extends MyAsyncTask
{

    ProgressDialog pd;
    final PlayerFragment this$0;
    final boolean val$isFollow;
    final long val$toUid;
    final View val$v;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient String doInBackground(Void avoid[])
    {
        return CommonRequest.doSetGroup(PlayerFragment.access$1600(PlayerFragment.this), (new StringBuilder()).append(val$toUid).append("").toString(), val$isFollow, val$v, val$v);
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((String)obj);
    }

    protected void onPostExecute(String s)
    {
        if (PlayerFragment.access$1600(PlayerFragment.this) == null || !isAdded())
        {
            return;
        }
        pd.dismiss();
        if (s == null)
        {
            boolean flag;
            if (!val$isFollow)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            PlayerFragment.access$9100(PlayerFragment.this, flag);
            val$v.setTag(Boolean.valueOf(flag));
            return;
        } else
        {
            PlayerFragment.access$9100(PlayerFragment.this, val$isFollow);
            val$v.setTag(Boolean.valueOf(val$isFollow));
            showToast(s);
            return;
        }
    }

    protected void onPreExecute()
    {
        pd.setTitle("\u8BF7\u7A0D\u5019...");
        pd.show();
    }

    alog()
    {
        this$0 = final_playerfragment;
        val$toUid = l;
        val$isFollow = flag;
        val$v = View.this;
        super();
        pd = new MyProgressDialog(PlayerFragment.access$1600(PlayerFragment.this), 0);
    }
}
