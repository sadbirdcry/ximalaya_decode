// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.search;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.search.AssociateModel;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.List;

public class SearchHistoryHotFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks
{
    private static class HistoryLoader extends MyAsyncTaskLoader
    {

        private List mData;

        public volatile void deliverResult(Object obj)
        {
            deliverResult((List)obj);
        }

        public void deliverResult(List list)
        {
            super.deliverResult(list);
            mData = list;
        }

        public volatile Object loadInBackground()
        {
            return loadInBackground();
        }

        public List loadInBackground()
        {
            String s = SharedPreferencesUtil.getInstance(getContext()).getString("history_search_word");
            if (!TextUtils.isEmpty(s))
            {
                try
                {
                    mData = JSON.parseArray(s, com/ximalaya/ting/android/model/search/AssociateModel);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
            }
            return mData;
        }

        protected void onStartLoading()
        {
            super.onStartLoading();
            if (mData != null)
            {
                deliverResult(mData);
                return;
            } else
            {
                forceLoad();
                return;
            }
        }

        public HistoryLoader(Context context)
        {
            super(context);
        }
    }

    private static class HotLoader extends MyAsyncTaskLoader
    {

        private List mData;

        public volatile void deliverResult(Object obj)
        {
            deliverResult((List)obj);
        }

        public void deliverResult(List list)
        {
            super.deliverResult(list);
            mData = list;
        }

        public volatile Object loadInBackground()
        {
            return loadInBackground();
        }

        public List loadInBackground()
        {
            Object obj;
            obj = new RequestParams();
            ((RequestParams) (obj)).put("size", "9");
            obj = f.a().a((new StringBuilder()).append(a.u).append("s/hot_search_key").toString(), ((RequestParams) (obj)), null, null);
            fromBindView = null;
            toBindView = null;
            if (TextUtils.isEmpty(((CharSequence) (obj))))
            {
                break MISSING_BLOCK_LABEL_202;
            }
            obj = JSON.parseObject(((String) (obj)));
            if (obj == null)
            {
                return null;
            }
            obj = ((JSONObject) (obj)).getString("list");
            if (TextUtils.isEmpty(((CharSequence) (obj))))
            {
                return null;
            }
            obj = JSON.parseArray(((String) (obj)), java/lang/String);
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_202;
            }
            if (((List) (obj)).size() <= 0)
            {
                break MISSING_BLOCK_LABEL_202;
            }
            mData = new ArrayList(9);
            int i = 0;
_L2:
            if (i >= 9)
            {
                break; /* Loop/switch isn't completed */
            }
            if (i >= ((List) (obj)).size())
            {
                break; /* Loop/switch isn't completed */
            }
            String s = (String)((List) (obj)).get(i);
            AssociateModel associatemodel = new AssociateModel();
            associatemodel.title = s;
            mData.add(associatemodel);
            i++;
            if (true) goto _L2; else goto _L1
_L1:
            obj = mData;
            return ((List) (obj));
            Exception exception;
            exception;
            exception.printStackTrace();
            return null;
        }

        protected void onStartLoading()
        {
            super.onStartLoading();
            if (mData != null)
            {
                deliverResult(mData);
                return;
            } else
            {
                forceLoad();
                return;
            }
        }

        public HotLoader(Context context)
        {
            super(context);
        }
    }

    public static interface OnItemClickListener
    {

        public abstract void onClearHistory(View view);

        public abstract void onItemClick(View view, AssociateModel associatemodel);

        public abstract void onLoadedHistoryData(List list);
    }


    private static final int LOAD_HISTORY = 10;
    private static final int LOAD_HOT = 11;
    private TextView mClearHistoryBtn;
    private View mHistoryLayout;
    private ViewGroup mHistorySection;
    private ViewGroup mHotSection;
    private LayoutInflater mInflater;
    private OnItemClickListener mListener;
    private View mNoHistoryHint;

    public SearchHistoryHotFragment()
    {
    }

    private View buildChildView(final AssociateModel model)
    {
        final TextView item = (TextView)mInflater.inflate(0x7f030138, null);
        item.setText(model.title);
        item.setOnClickListener(new _cls2());
        model = new FrameLayout(getActivity());
        model.addView(item, new android.widget.FrameLayout.LayoutParams(-2, -1));
        return model;
    }

    public static SearchHistoryHotFragment getInstance()
    {
        return new SearchHistoryHotFragment();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mInflater = LayoutInflater.from(getActivity());
        getLoaderManager().initLoader(10, null, this);
        getLoaderManager().initLoader(11, null, this);
        mClearHistoryBtn.setOnClickListener(new _cls1());
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setRetainInstance(false);
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        switch (i)
        {
        default:
            return null;

        case 10: // '\n'
            return new HistoryLoader(getActivity());

        case 11: // '\013'
            return new HotLoader(getActivity());
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300d6, viewgroup, false);
        mHistorySection = (ViewGroup)findViewById(0x7f0a0387);
        mHotSection = (ViewGroup)findViewById(0x7f0a0388);
        mNoHistoryHint = findViewById(0x7f0a0386);
        mClearHistoryBtn = (TextView)findViewById(0x7f0a0385);
        mHistoryLayout = findViewById(0x7f0a0384);
        return fragmentBaseContainerView;
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        if (loader == null) goto _L2; else goto _L1
_L1:
        loader.getId();
        JVM INSTR tableswitch 10 11: default 32
    //                   10 33
    //                   11 332;
           goto _L2 _L3 _L4
_L2:
        return;
_L3:
        if (list != null && list.size() > 0)
        {
            if (mListener != null)
            {
                mListener.onLoadedHistoryData(list);
            }
            loader = null;
            for (int i = 0; i < list.size(); i++)
            {
                if (i % 3 == 0)
                {
                    loader = new LinearLayout(getActivity());
                    mHistorySection.addView(loader);
                }
                android.widget.LinearLayout.LayoutParams layoutparams = new android.widget.LinearLayout.LayoutParams(0, -2);
                layoutparams.weight = 1.0F;
                layoutparams.bottomMargin = Utilities.dip2px(getActivity(), 10F);
                layoutparams.rightMargin = Utilities.dip2px(getActivity(), 10F);
                loader.addView(buildChildView((AssociateModel)list.get(i)), layoutparams);
            }

            if (list.size() % 3 != 0)
            {
                int i1 = list.size();
                for (int j = 0; j < 3 - i1 % 3; j++)
                {
                    list = new View(getActivity());
                    android.widget.LinearLayout.LayoutParams layoutparams1 = new android.widget.LinearLayout.LayoutParams(0, -2);
                    layoutparams1.bottomMargin = Utilities.dip2px(getActivity(), 10F);
                    layoutparams1.rightMargin = Utilities.dip2px(getActivity(), 10F);
                    layoutparams1.weight = 1.0F;
                    loader.addView(list, layoutparams1);
                }

            }
            mClearHistoryBtn.setVisibility(0);
            mHistoryLayout.setVisibility(0);
            return;
        } else
        {
            mHistoryLayout.setVisibility(8);
            mHistorySection.setVisibility(8);
            mNoHistoryHint.setVisibility(0);
            mClearHistoryBtn.setVisibility(8);
            return;
        }
_L4:
        if (list != null && list.size() > 0)
        {
            loader = null;
            for (int k = 0; k < list.size(); k++)
            {
                if (k % 3 == 0)
                {
                    loader = new LinearLayout(getActivity());
                    mHotSection.addView(loader);
                }
                android.widget.LinearLayout.LayoutParams layoutparams2 = new android.widget.LinearLayout.LayoutParams(0, -2);
                layoutparams2.weight = 1.0F;
                layoutparams2.bottomMargin = Utilities.dip2px(getActivity(), 10F);
                layoutparams2.rightMargin = Utilities.dip2px(getActivity(), 10F);
                loader.addView(buildChildView((AssociateModel)list.get(k)), layoutparams2);
            }

            if (list.size() % 3 != 0)
            {
                int j1 = list.size();
                int l = 0;
                while (l < 3 - j1 % 3) 
                {
                    list = new View(getActivity());
                    android.widget.LinearLayout.LayoutParams layoutparams3 = new android.widget.LinearLayout.LayoutParams(0, -2);
                    layoutparams3.weight = 1.0F;
                    loader.addView(list, layoutparams3);
                    l++;
                }
            }
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void setOnItemClickListener(OnItemClickListener onitemclicklistener)
    {
        mListener = onitemclicklistener;
    }






    private class _cls2
        implements android.view.View.OnClickListener
    {

        final SearchHistoryHotFragment this$0;
        final TextView val$item;
        final AssociateModel val$model;

        public void onClick(View view)
        {
            if (mListener != null)
            {
                mListener.onItemClick(item, model);
            }
        }

        _cls2()
        {
            this$0 = SearchHistoryHotFragment.this;
            item = textview;
            model = associatemodel;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SearchHistoryHotFragment this$0;

        public void onClick(View view)
        {
            if (mListener != null)
            {
                mListener.onClearHistory(view);
            }
            mHistorySection.setVisibility(8);
            mNoHistoryHint.setVisibility(0);
            mClearHistoryBtn.setVisibility(8);
            mHistoryLayout.setVisibility(8);
        }

        _cls1()
        {
            this$0 = SearchHistoryHotFragment.this;
            super();
        }
    }

}
