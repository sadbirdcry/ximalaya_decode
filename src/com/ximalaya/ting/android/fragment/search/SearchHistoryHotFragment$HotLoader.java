// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.search;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.search.AssociateModel;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.search:
//            SearchHistoryHotFragment

private static class  extends MyAsyncTaskLoader
{

    private List mData;

    public volatile void deliverResult(Object obj)
    {
        deliverResult((List)obj);
    }

    public void deliverResult(List list)
    {
        super.deliverResult(list);
        mData = list;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public List loadInBackground()
    {
        Object obj;
        obj = new RequestParams();
        ((RequestParams) (obj)).put("size", "9");
        obj = f.a().a((new StringBuilder()).append(a.u).append("s/hot_search_key").toString(), ((RequestParams) (obj)), null, null);
        fromBindView = null;
        toBindView = null;
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            break MISSING_BLOCK_LABEL_202;
        }
        obj = JSON.parseObject(((String) (obj)));
        if (obj == null)
        {
            return null;
        }
        obj = ((JSONObject) (obj)).getString("list");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            return null;
        }
        obj = JSON.parseArray(((String) (obj)), java/lang/String);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_202;
        }
        if (((List) (obj)).size() <= 0)
        {
            break MISSING_BLOCK_LABEL_202;
        }
        mData = new ArrayList(9);
        int i = 0;
_L2:
        if (i >= 9)
        {
            break; /* Loop/switch isn't completed */
        }
        if (i >= ((List) (obj)).size())
        {
            break; /* Loop/switch isn't completed */
        }
        String s = (String)((List) (obj)).get(i);
        AssociateModel associatemodel = new AssociateModel();
        associatemodel.title = s;
        mData.add(associatemodel);
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        obj = mData;
        return ((List) (obj));
        Exception exception;
        exception;
        exception.printStackTrace();
        return null;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData != null)
        {
            deliverResult(mData);
            return;
        } else
        {
            forceLoad();
            return;
        }
    }

    public (Context context)
    {
        super(context);
    }
}
