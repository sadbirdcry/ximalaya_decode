// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.search;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.search.AssociateModel;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.search:
//            SearchHistoryHotFragment

private static class  extends MyAsyncTaskLoader
{

    private List mData;

    public volatile void deliverResult(Object obj)
    {
        deliverResult((List)obj);
    }

    public void deliverResult(List list)
    {
        super.deliverResult(list);
        mData = list;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public List loadInBackground()
    {
        String s = SharedPreferencesUtil.getInstance(getContext()).getString("history_search_word");
        if (!TextUtils.isEmpty(s))
        {
            try
            {
                mData = JSON.parseArray(s, com/ximalaya/ting/android/model/search/AssociateModel);
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        return mData;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData != null)
        {
            deliverResult(mData);
            return;
        } else
        {
            forceLoad();
            return;
        }
    }

    public (Context context)
    {
        super(context);
    }
}
