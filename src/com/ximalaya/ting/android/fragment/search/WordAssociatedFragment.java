// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.search;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.AssocicateAdapterNew;
import com.ximalaya.ting.android.adapter.SearchAlbumAdapterNew;
import com.ximalaya.ting.android.adapter.SearchAllAdapterNew;
import com.ximalaya.ting.android.adapter.SearchPersonAdapter;
import com.ximalaya.ting.android.adapter.SearchSoundAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.search.AssociateKeywordModel;
import com.ximalaya.ting.android.model.search.AssociateModel;
import com.ximalaya.ting.android.model.search.SearchAlbum;
import com.ximalaya.ting.android.model.search.SearchPerson;
import com.ximalaya.ting.android.model.search.SearchSound;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.search:
//            SearchHistoryHotFragment

public class WordAssociatedFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener, com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
{
    public static class SearchModelDivider
    {

        public int count;
        public String type;

        public SearchModelDivider()
        {
        }
    }


    public static final String CONDITION_PLAY = "play";
    public static final String CONDITION_RECENT = "recent";
    public static final String CONDITION_RELATION = "relation";
    public static final int HISTORY_COUNT = 6;
    public static final int HOT_COUNT = 9;
    public static final String SCOPE_ALBUM = "album";
    public static final String SCOPE_ALL = "all";
    public static final String SCOPE_TRACK = "track";
    public static final String SCOPE_USER = "user";
    private BaseAdapter mAdapter;
    private boolean mCheckSpell;
    private View mClearSearchText;
    private TextView mCorrectionTxt;
    private List mDataList;
    private TextView mFooterView;
    private boolean mFromUser;
    private boolean mHasSearched;
    private SearchHistoryHotFragment mHistoryHotFra;
    private List mHistoryList;
    private boolean mIsLoading;
    private String mKeyWord;
    private ListView mListView;
    private int mPageId;
    private int mPageSize;
    private RadioGroup mScopeGroup;
    private Button mSearchButton;
    private EditText mSearchText;
    private String mSort;
    private RadioButton mSortBtn1;
    private RadioButton mSortBtn2;
    private RadioButton mSortBtn3;
    private RadioGroup mSortHeader;
    private int mTotal;

    public WordAssociatedFragment()
    {
        mDataList = new ArrayList();
        mHistoryList = new ArrayList();
        mPageId = 1;
        mPageSize = 15;
        mSort = null;
        mCheckSpell = true;
    }

    private void addHistoryHotFrag()
    {
        if (getFragmentManager() != null)
        {
            FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
            if (mHistoryHotFra == null)
            {
                mHistoryHotFra = SearchHistoryHotFragment.getInstance();
                mHistoryHotFra.setOnItemClickListener(new _cls13());
                fragmenttransaction.add(0x7f0a012a, mHistoryHotFra, "history_hot");
                fragmenttransaction.commitAllowingStateLoss();
                doAfterAnimation(new _cls14());
                return;
            }
        }
    }

    private void decorateSort(RequestParams requestparams)
    {
        if (TextUtils.isEmpty(mSort))
        {
            mSort = "relation";
        }
        if (!TextUtils.isEmpty(mSort))
        {
            requestparams.add("condition", mSort);
        }
    }

    private int getRadioButtonId(String s)
    {
        if ("all".equals(s))
        {
            s = "0";
        } else
        if ("album".equals(s))
        {
            s = "1";
        } else
        if ("user".equals(s))
        {
            s = "2";
        } else
        if ("track".equals(s))
        {
            s = "3";
        } else
        {
            s = "0";
        }
        return mScopeGroup.findViewWithTag(s).getId();
    }

    private String getSearchScope()
    {
        if (mScopeGroup == null)
        {
            return "all";
        }
        switch (Integer.parseInt(mScopeGroup.findViewById(mScopeGroup.getCheckedRadioButtonId()).getTag().toString()))
        {
        default:
            return "all";

        case 0: // '\0'
            return "all";

        case 1: // '\001'
            return "album";

        case 2: // '\002'
            return "user";

        case 3: // '\003'
            return "track";
        }
    }

    private String getSearchWord()
    {
        return mSearchText.getEditableText().toString().trim();
    }

    private String getSortForAlbum(int i)
    {
        switch (i)
        {
        default:
            return "";

        case 2131361920: 
            return "relation";

        case 2131361921: 
            ToolUtil.onEvent(mCon, "CLICK_SEARCH_ALBUM_NEW");
            return "recent";

        case 2131362123: 
            ToolUtil.onEvent(mCon, "CLICK_SEARCH_ALBUM_PLAY");
            break;
        }
        return "play";
    }

    private String getSortForPerson(int i)
    {
        switch (i)
        {
        default:
            return "";

        case 2131361920: 
            return "relation";

        case 2131361921: 
            ToolUtil.onEvent(mCon, "CLICK_SEARCH_USER_FUNS");
            return "fans";

        case 2131362123: 
            ToolUtil.onEvent(mCon, "CLICK_SEARCH_USER_SOUNDS");
            break;
        }
        return "voice";
    }

    private String getSortForSound(int i)
    {
        switch (i)
        {
        default:
            return "";

        case 2131361920: 
            return "relation";

        case 2131361921: 
            ToolUtil.onEvent(mCon, "CLICK_SEARCH_SOUND_NEW");
            return "recent";

        case 2131362123: 
            ToolUtil.onEvent(mCon, "CLICK_SEARCH_SOUND_PLAY");
            break;
        }
        return "play";
    }

    private String getSortStr(int i)
    {
        switch (Integer.parseInt(mScopeGroup.findViewById(mScopeGroup.getCheckedRadioButtonId()).getTag().toString()))
        {
        default:
            return null;

        case 1: // '\001'
            return getSortForAlbum(i);

        case 2: // '\002'
            return getSortForPerson(i);

        case 3: // '\003'
            return getSortForSound(i);
        }
    }

    private void hideSoftInput()
    {
        if (mSearchText != null)
        {
            mSearchText.clearFocus();
        }
        if (mCon != null)
        {
            ((InputMethodManager)mCon.getSystemService("input_method")).hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
        }
    }

    private void initListener()
    {
        mSearchButton.setOnClickListener(this);
        mClearSearchText.setOnClickListener(this);
        mCorrectionTxt.setOnClickListener(this);
        mSearchText.setOnFocusChangeListener(new _cls1());
        mSearchText.setOnKeyListener(new _cls2());
        mSearchText.addTextChangedListener(new _cls3());
        mFooterView.setOnClickListener(new _cls4());
        mListView.setOnTouchListener(new _cls5());
        mListView.setOnScrollListener(new _cls6());
        mListView.setOnItemClickListener(new _cls7());
        mListView.setOnItemLongClickListener(new _cls8());
        mScopeGroup.setOnCheckedChangeListener(new _cls9());
        mSortHeader.setOnCheckedChangeListener(new _cls10());
    }

    private void initSortHeader()
    {
        String s = getSearchScope();
        if ("all".equals(s))
        {
            mSortHeader.setVisibility(8);
            mListView.setHeaderDividersEnabled(false);
            TextView textview = mCorrectionTxt;
            int i;
            if (mCheckSpell)
            {
                i = 0;
            } else
            {
                i = 8;
            }
            textview.setVisibility(i);
        }
        if ("album".equals(s))
        {
            mSortBtn1.setText("\u76F8\u5173\u5EA6");
            mSortBtn2.setText("\u6700\u65B0\u4E0A\u4F20");
            mSortBtn2.setVisibility(0);
            mSortBtn3.setText("\u6700\u591A\u64AD\u653E");
            mSortHeader.setVisibility(0);
            mCorrectionTxt.setVisibility(8);
        }
        if ("user".equals(s))
        {
            mSortBtn1.setText("\u76F8\u5173\u5EA6");
            mSortBtn2.setText("\u6700\u591A\u7C89\u4E1D");
            mSortBtn2.setVisibility(0);
            mSortBtn3.setText("\u6700\u591A\u58F0\u97F3");
            mSortHeader.setVisibility(0);
            mCorrectionTxt.setVisibility(8);
        }
        if ("track".equals(s))
        {
            mSortBtn1.setText("\u76F8\u5173\u5EA6");
            mSortBtn2.setText("\u6700\u65B0\u4E0A\u4F20");
            mSortBtn2.setVisibility(0);
            mSortBtn3.setText("\u6700\u591A\u64AD\u653E");
            mSortHeader.setVisibility(0);
            mCorrectionTxt.setVisibility(8);
        }
    }

    private void initUi()
    {
        mListView = (ListView)findViewById(0x7f0a0129);
        mFooterView = (TextView)View.inflate(mCon, 0x7f0301ca, null);
        mFooterView.setGravity(17);
        mListView.addFooterView(mFooterView, null, true);
        mSearchButton = (Button)findViewById(0x7f0a00e0);
        mSearchButton.setText("\u53D6\u6D88");
        mSearchText = (EditText)findViewById(0x7f0a00df);
        mScopeGroup = (RadioGroup)findViewById(0x7f0a0122);
        mClearSearchText = findViewById(0x7f0a00e1);
        mClearSearchText.setVisibility(4);
        String s = SharedPreferencesUtil.getInstance(mCon).getString("history_search_scope");
        mScopeGroup.check(getRadioButtonId(s));
        mSortHeader = (RadioGroup)findViewById(0x7f0a0127);
        mSortBtn1 = (RadioButton)mSortHeader.findViewById(0x7f0a0080);
        mSortBtn2 = (RadioButton)mSortHeader.findViewById(0x7f0a0081);
        mSortBtn3 = (RadioButton)mSortHeader.findViewById(0x7f0a014b);
    }

    private void loadRSSStatus(final List data)
    {
        if (!UserInfoMannage.hasLogined())
        {
            return;
        }
        LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
        RequestParams requestparams = new RequestParams();
        StringBuilder stringbuilder = new StringBuilder();
        for (Iterator iterator = data.iterator(); iterator.hasNext(); stringbuilder.append(((SearchAlbum)iterator.next()).id).append(",")) { }
        requestparams.add("uid", (new StringBuilder()).append("").append(logininfomodel.uid).toString());
        requestparams.add("album_ids", stringbuilder.toString());
        f.a().a("m/album_subscribe_status", requestparams, null, new _cls16());
    }

    private void loadRssStatusLocal(final List data)
    {
        (new _cls15()).myexec(new Void[0]);
    }

    private void loadSearch(AssociateModel associatemodel, View view)
    {
        loadSearch(associatemodel, view, mCheckSpell);
    }

    private void loadSearch(final AssociateModel as, View view, boolean flag)
    {
        if (as != null && !TextUtils.isEmpty(as.title)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        RequestParams requestparams;
        String s;
        LoginInfoModel logininfomodel;
        if ("all".equals(as.type))
        {
            mPageSize = 3;
        } else
        {
            mPageSize = 20;
        }
        s = (new StringBuilder()).append(a.P).append("front/v1").toString();
        requestparams = new RequestParams();
        requestparams.add("kw", as.title);
        requestparams.add("page", (new StringBuilder()).append("").append(mPageId).toString());
        requestparams.add("rows", (new StringBuilder()).append("").append(mPageSize).toString());
        requestparams.add("spellchecker", (new StringBuilder()).append(flag).append("").toString());
        logininfomodel = UserInfoMannage.getInstance().getUser();
        if (logininfomodel != null)
        {
            requestparams.add("uid", (new StringBuilder()).append("").append(logininfomodel.uid).toString());
        }
        if (!TextUtils.isEmpty(as.type))
        {
            break; /* Loop/switch isn't completed */
        }
        requestparams.add("core", "all");
_L4:
        if (view != null)
        {
            f.a().a(s, requestparams, DataCollectUtil.getDataFromView(view), new _cls11());
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        requestparams.add("core", as.type);
        if (!"all".equals(as.type))
        {
            decorateSort(requestparams);
        }
          goto _L4
        if (true) goto _L1; else goto _L5
_L5:
    }

    private void loadSuggestData(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        mDataList.clear();
        if (!(mAdapter instanceof AssocicateAdapterNew))
        {
            mAdapter = new AssocicateAdapterNew(mActivity, mDataList);
            mListView.setAdapter(mAdapter);
        }
        String s1 = (new StringBuilder()).append(a.P).append("suggest").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.add("kw", s);
        f.a().a(s1, requestparams, DataCollectUtil.getDataFromView(mSearchText), new _cls12());
    }

    private void onSearchResultClick(Object obj, View view)
    {
        if (obj != null)
        {
            if (obj instanceof SearchAlbum)
            {
                Object obj1 = (SearchAlbum)obj;
                obj = new AlbumModel();
                obj.albumId = ((SearchAlbum) (obj1)).id;
                obj1 = new Bundle();
                ((Bundle) (obj1)).putString("album", JSON.toJSONString(obj));
                ((Bundle) (obj1)).putInt("from", 8);
                ((Bundle) (obj1)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj1)));
                return;
            }
            if (obj instanceof SearchSound)
            {
                obj = (SearchSound)obj;
                SoundInfo soundinfo = new SoundInfo();
                soundinfo.trackId = ((SearchSound) (obj)).id;
                PlayTools.gotoPlayWithoutUrl(11, soundinfo, mActivity, true, DataCollectUtil.getDataFromView(view));
                return;
            }
            if (obj instanceof SearchPerson)
            {
                obj = (SearchPerson)obj;
                Bundle bundle = new Bundle();
                bundle.putLong("toUid", ((SearchPerson) (obj)).uid);
                bundle.putInt("from", 2);
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
                return;
            }
            if (obj instanceof SearchModelDivider)
            {
                obj = (SearchModelDivider)obj;
                DataCollectUtil.bindDataToView(DataCollectUtil.getDataFromView(view), view);
                search(mKeyWord, ((SearchModelDivider) (obj)).type, true, view, mCheckSpell);
                return;
            }
        }
    }

    private void onSearchResultClick(Object obj, View view, int i, boolean flag)
    {
        if (obj != null)
        {
            if (obj instanceof SearchAlbum)
            {
                Object obj1 = (SearchAlbum)obj;
                obj = new AlbumModel();
                obj.albumId = ((SearchAlbum) (obj1)).id;
                obj1 = new Bundle();
                ((Bundle) (obj1)).putString("album", JSON.toJSONString(obj));
                ((Bundle) (obj1)).putInt("from", 8);
                if (flag)
                {
                    obj = DataCollectUtil.getDataFromView(view, 1, (new StringBuilder()).append(i).append("").toString());
                } else
                {
                    obj = DataCollectUtil.getDataFromView(view, i / 20 + 1, (new StringBuilder()).append(i % 20 + 1).append("").toString());
                }
                ((Bundle) (obj1)).putString("xdcs_data_bundle", ((String) (obj)));
                startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj1)));
                return;
            }
            if (obj instanceof SearchSound)
            {
                obj = (SearchSound)obj;
                SoundInfo soundinfo = new SoundInfo();
                soundinfo.trackId = ((SearchSound) (obj)).id;
                android.app.Activity activity = mActivity;
                if (flag)
                {
                    obj = DataCollectUtil.getDataFromView(view, 1, (new StringBuilder()).append(i - 2).append("").toString());
                } else
                {
                    obj = DataCollectUtil.getDataFromView(view, i / 20 + 1, (new StringBuilder()).append(i % 20 + 1).append("").toString());
                }
                PlayTools.gotoPlayWithoutUrl(11, soundinfo, activity, true, ((String) (obj)));
                return;
            }
            if (obj instanceof SearchPerson)
            {
                obj = (SearchPerson)obj;
                Bundle bundle = new Bundle();
                bundle.putLong("toUid", ((SearchPerson) (obj)).uid);
                bundle.putInt("from", 2);
                if (flag)
                {
                    obj = DataCollectUtil.getDataFromView(view, 1, (new StringBuilder()).append(i - 1).append("").toString());
                } else
                {
                    obj = DataCollectUtil.getDataFromView(view, i / 20 + 1, (new StringBuilder()).append(i % 20 + 1).append("").toString());
                }
                bundle.putString("xdcs_data_bundle", ((String) (obj)));
                startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
                return;
            }
            if (obj instanceof SearchModelDivider)
            {
                obj = (SearchModelDivider)obj;
                DataCollectUtil.bindDataToView(DataCollectUtil.getDataFromView(view, i / 20 + 1, (new StringBuilder()).append(i % 20 + 1).append("").toString()), view);
                search(mKeyWord, ((SearchModelDivider) (obj)).type, true, view, mCheckSpell);
                return;
            }
        }
    }

    private void onSuggestItemClick(AssociateKeywordModel associatekeywordmodel, View view)
    {
        if (associatekeywordmodel.isAlbum())
        {
            AlbumModel albummodel = new AlbumModel();
            albummodel.albumId = associatekeywordmodel.getId();
            Bundle bundle = new Bundle();
            bundle.putString("album", JSON.toJSONString(albummodel));
            bundle.putInt("from", 8);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
            search(associatekeywordmodel.getKeyword(), "album", false, view);
            return;
        } else
        {
            search(associatekeywordmodel.getKeyword(), null, true, view);
            return;
        }
    }

    private void openSoftInput()
    {
        mSearchText.requestFocus();
        if (mCon != null)
        {
            ((InputMethodManager)mCon.getSystemService("input_method")).showSoftInput(mSearchText, 0);
        }
    }

    private void parseSearchAlbum(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        s = s.getJSONObject("response");
        if (s == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        mTotal = s.getIntValue("numFound");
        int i = s.getIntValue("start");
        s = s.getString("docs");
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/search/SearchAlbum);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null || s.size() == 0)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        mFooterView.setVisibility(8);
        mListView.setFooterDividersEnabled(false);
        if (mPageId == 1 || i == 0)
        {
            mDataList.clear();
        }
        mDataList.addAll(s);
        mAdapter.notifyDataSetChanged();
        mPageId = mPageId + 1;
    }

    private void parseSearchAll(String s)
    {
        Object obj1;
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            obj1 = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            obj1 = null;
        }
        if (obj1 == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        if (!mCheckSpell) goto _L2; else goto _L1
_L1:
        s = ((JSONObject) (obj1)).getString("sq");
        if (TextUtils.isEmpty(s) || s.equals(mSearchText)) goto _L2; else goto _L3
_L3:
        mCorrectionTxt.setText(Html.fromHtml((new StringBuilder()).append("\u5DF2\u7EA0\u9519\u4E3A\u641C\u7D22\u201C<font color=\"#fb633a\">").append(s).append("</font>\u201D\uFF1B<font color=\"#008bff\">\u53D6\u6D88\u7EA0\u9519</font>").toString()));
_L14:
        int j;
        s = ((JSONObject) (obj1)).getJSONObject("track");
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_554;
        }
        j = s.getIntValue("numFound");
        s = s.getString("docs");
        if (TextUtils.isEmpty(s)) goto _L5; else goto _L4
_L4:
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/search/SearchSound);
_L15:
        Object obj = ((JSONObject) (obj1)).getJSONObject("album");
        if (obj == null) goto _L7; else goto _L6
_L6:
        int k;
        k = ((JSONObject) (obj)).getIntValue("numFound");
        obj = ((JSONObject) (obj)).getString("docs");
        if (TextUtils.isEmpty(((CharSequence) (obj)))) goto _L9; else goto _L8
_L8:
        obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/search/SearchAlbum);
_L16:
        obj1 = ((JSONObject) (obj1)).getJSONObject("user");
        if (obj1 == null) goto _L11; else goto _L10
_L10:
        int i;
        i = ((JSONObject) (obj1)).getIntValue("numFound");
        obj1 = ((JSONObject) (obj1)).getString("docs");
        if (TextUtils.isEmpty(((CharSequence) (obj1)))) goto _L13; else goto _L12
_L12:
        obj1 = JSON.parseArray(((String) (obj1)), com/ximalaya/ting/android/model/search/SearchPerson);
_L17:
        mDataList.clear();
        if (obj != null && ((List) (obj)).size() > 0)
        {
            SearchModelDivider searchmodeldivider = new SearchModelDivider();
            searchmodeldivider.type = "album";
            searchmodeldivider.count = k;
            mDataList.add(searchmodeldivider);
            mDataList.addAll(((java.util.Collection) (obj)));
        }
        if (obj1 != null && ((List) (obj1)).size() > 0)
        {
            obj = new SearchModelDivider();
            obj.type = "user";
            obj.count = i;
            mDataList.add(obj);
            mDataList.addAll(((java.util.Collection) (obj1)));
        }
        if (s != null && s.size() > 0)
        {
            obj = new SearchModelDivider();
            obj.type = "track";
            obj.count = j;
            mDataList.add(obj);
            mDataList.addAll(s);
        }
        if (mDataList.size() == 0)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        } else
        {
            mFooterView.setVisibility(8);
            mListView.setFooterDividersEnabled(false);
            mAdapter.notifyDataSetChanged();
            return;
        }
_L2:
        mCorrectionTxt.setVisibility(8);
          goto _L14
        s;
        s.printStackTrace();
_L5:
        s = null;
          goto _L15
        obj;
        ((Exception) (obj)).printStackTrace();
_L9:
        obj = null;
          goto _L16
        obj1;
        ((Exception) (obj1)).printStackTrace();
_L13:
        obj1 = null;
          goto _L17
_L11:
        obj1 = null;
        i = 0;
          goto _L17
_L7:
        k = 0;
        obj = null;
          goto _L16
        j = 0;
        s = null;
          goto _L15
    }

    private void parseSearchPerson(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        s = s.getJSONObject("response");
        if (s == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        mTotal = s.getIntValue("numFound");
        int i = s.getIntValue("start");
        s = s.getString("docs");
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/search/SearchPerson);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null || s.size() == 0)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        mFooterView.setVisibility(8);
        mListView.setFooterDividersEnabled(false);
        if (mPageId == 1 || i == 0)
        {
            mDataList.clear();
        }
        mDataList.addAll(s);
        mAdapter.notifyDataSetChanged();
        mPageId = mPageId + 1;
    }

    private void parseSearchSound(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        s = s.getJSONObject("response");
        if (s == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        mTotal = s.getIntValue("numFound");
        int i = s.getIntValue("start");
        s = s.getString("docs");
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/search/SearchSound);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null || s.size() == 0)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        mFooterView.setVisibility(8);
        mListView.setFooterDividersEnabled(false);
        if (mPageId == 1 || i == 0)
        {
            mDataList.clear();
        }
        mDataList.addAll(s);
        mAdapter.notifyDataSetChanged();
        mPageId = mPageId + 1;
    }

    private void registePlayerCallback()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayerStatusUpdateListener(this);
            localmediaservice.setOnPlayServiceUpdateListener(this);
        }
    }

    private void removeHistoryHotFragment()
    {
        FragmentManager fragmentmanager = getChildFragmentManager();
        if (fragmentmanager != null && fragmentmanager.findFragmentByTag("history_hot") != null)
        {
            FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
            fragmenttransaction.remove(fragmentmanager.findFragmentByTag("history_hot"));
            fragmenttransaction.commitAllowingStateLoss();
            mHistoryHotFra = null;
        }
    }

    private void saveHistory()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCon);
        if (mHistoryList != null)
        {
            sharedpreferencesutil.saveString("history_search_word", JSON.toJSONString(mHistoryList));
        }
        sharedpreferencesutil.saveString("history_search_scope", getSearchScope());
    }

    private void unregistePlayerCallback()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayerUpdateListener(this);
            localmediaservice.removeOnPlayServiceUpdateListener(this);
        }
    }

    void clearHistory()
    {
        if (mHistoryList != null)
        {
            mHistoryList.clear();
            saveHistory();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initUi();
        registePlayerCallback();
        initListener();
        bundle = null;
        Bundle bundle1 = getArguments();
        if (bundle1 != null)
        {
            bundle = bundle1.getString("key");
        }
        if (!TextUtils.isEmpty(bundle))
        {
            mSearchText.setText(bundle);
            search(bundle, "all", true, mSearchButton);
            return;
        } else
        {
            addHistoryHotFrag();
            return;
        }
    }

    public void onBufferUpdated(int i)
    {
    }

    public void onClick(View view)
    {
        if (view.getId() != 0x7f0a00e0) goto _L2; else goto _L1
_L1:
        if (TextUtils.isEmpty(getSearchWord())) goto _L4; else goto _L3
_L3:
        mHasSearched = true;
        mCheckSpell = true;
        search(getSearchWord(), getSearchScope(), true, view);
_L6:
        return;
_L4:
        finishFragment();
        return;
_L2:
        if (view.getId() == 0x7f0a00e1)
        {
            mSearchText.setText("");
            mSortHeader.setVisibility(8);
            addHistoryHotFrag();
            return;
        }
        if (view.getId() == 0x7f0a0121)
        {
            mCorrectionTxt.setVisibility(8);
            search(mKeyWord, null, true, mScopeGroup, false);
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030034, null);
        mCorrectionTxt = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0121);
        getActivity().getWindow().setSoftInputMode(51);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        unregistePlayerCallback();
        getActivity().getWindow().setSoftInputMode(35);
        super.onDestroyView();
    }

    public void onLogoPlayFinished()
    {
    }

    public void onPause()
    {
        hideSoftInput();
        saveHistory();
        super.onPause();
    }

    public void onPlayCanceled()
    {
        if ((mAdapter instanceof SearchAllAdapterNew) || (mAdapter instanceof SearchSoundAdapter))
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onPlayCompleted()
    {
        if ((mAdapter instanceof SearchAllAdapterNew) || (mAdapter instanceof SearchSoundAdapter))
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onPlayPaused()
    {
        if ((mAdapter instanceof SearchAllAdapterNew) || (mAdapter instanceof SearchSoundAdapter))
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onPlayProgressUpdate(int i, int j)
    {
    }

    public void onPlayStarted()
    {
        if ((mAdapter instanceof SearchAllAdapterNew) || (mAdapter instanceof SearchSoundAdapter))
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onPlayerBuffering(boolean flag)
    {
    }

    public void onResume()
    {
        super.onResume();
    }

    public void onSoundChanged(int i)
    {
        if ((mAdapter instanceof SearchAllAdapterNew) || (mAdapter instanceof SearchSoundAdapter))
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if ((mAdapter instanceof SearchAllAdapterNew) || (mAdapter instanceof SearchSoundAdapter))
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundPrepared(int i)
    {
    }

    public void onStartPlayLogo()
    {
    }

    void search(String s, String s1, boolean flag, View view)
    {
        search(s, s1, flag, view, true);
    }

    void search(String s, String s1, boolean flag, View view, boolean flag1)
    {
        if (TextUtils.isEmpty(s))
        {
            showToast("\u8BF7\u8F93\u5165\u641C\u7D22\u5173\u952E\u8BCD\uFF01");
        } else
        {
            mHasSearched = true;
            mCheckSpell = flag1;
            hideSoftInput();
            Object obj = mHistoryList.iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                AssociateModel associatemodel = (AssociateModel)((Iterator) (obj)).next();
                if (!s.equals(associatemodel.title))
                {
                    continue;
                }
                mHistoryList.remove(associatemodel);
                break;
            } while (true);
            obj = s1;
            if (TextUtils.isEmpty(s1))
            {
                obj = "all";
            }
            s1 = new AssociateModel();
            s1.type = ((String) (obj));
            s1.title = s;
            mHistoryList.add(0, s1);
            if (mHistoryList.size() > 6)
            {
                mHistoryList = mHistoryList.subList(0, 6);
            }
            mKeyWord = s;
            saveHistory();
            if (flag)
            {
                mScopeGroup.check(getRadioButtonId(((String) (obj))));
                if ("album".equals(obj))
                {
                    s = new SearchAlbumAdapterNew(mActivity, mDataList);
                } else
                if ("user".equals(obj))
                {
                    s = new SearchPersonAdapter(mActivity, mDataList);
                } else
                if ("track".equals(obj))
                {
                    s = new SearchSoundAdapter(mActivity, mDataList);
                } else
                {
                    s = new SearchAllAdapterNew(mActivity, mDataList);
                }
                mPageId = 1;
                mDataList.clear();
                mListView.setAdapter(s);
                mAdapter = s;
                mScopeGroup.setVisibility(0);
                loadSearch(s1, view, flag1);
                removeHistoryHotFragment();
                return;
            }
        }
    }







/*
    static List access$1202(WordAssociatedFragment wordassociatedfragment, List list)
    {
        wordassociatedfragment.mHistoryList = list;
        return list;
    }

*/






/*
    static int access$1602(WordAssociatedFragment wordassociatedfragment, int i)
    {
        wordassociatedfragment.mPageId = i;
        return i;
    }

*/





/*
    static boolean access$1902(WordAssociatedFragment wordassociatedfragment, boolean flag)
    {
        wordassociatedfragment.mIsLoading = flag;
        return flag;
    }

*/









/*
    static String access$2602(WordAssociatedFragment wordassociatedfragment, String s)
    {
        wordassociatedfragment.mSort = s;
        return s;
    }

*/







/*
    static boolean access$302(WordAssociatedFragment wordassociatedfragment, boolean flag)
    {
        wordassociatedfragment.mHasSearched = flag;
        return flag;
    }

*/










/*
    static boolean access$702(WordAssociatedFragment wordassociatedfragment, boolean flag)
    {
        wordassociatedfragment.mFromUser = flag;
        return flag;
    }

*/



    private class _cls13
        implements SearchHistoryHotFragment.OnItemClickListener
    {

        final WordAssociatedFragment this$0;

        public void onClearHistory(View view)
        {
            clearHistory();
        }

        public void onItemClick(View view, AssociateModel associatemodel)
        {
            mSearchText.setText(associatemodel.title);
            search(associatemodel.title, associatemodel.type, true, view);
        }

        public void onLoadedHistoryData(List list)
        {
            mHistoryList = list;
        }

        _cls13()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls14
        implements MyCallback
    {

        final WordAssociatedFragment this$0;

        public void execute()
        {
            openSoftInput();
        }

        _cls14()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnFocusChangeListener
    {

        final WordAssociatedFragment this$0;

        public void onFocusChange(View view, boolean flag)
        {
            if (flag && TextUtils.isEmpty(getSearchWord()))
            {
                addHistoryHotFrag();
            }
        }

        _cls1()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnKeyListener
    {

        final WordAssociatedFragment this$0;

        public boolean onKey(View view, int i, KeyEvent keyevent)
        {
            if (i == 66 && keyevent.getAction() == 0)
            {
                search(getSearchWord(), getSearchScope(), true, view);
                return true;
            } else
            {
                return false;
            }
        }

        _cls2()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls3
        implements TextWatcher
    {

        final WordAssociatedFragment this$0;

        public void afterTextChanged(Editable editable)
        {
            mHasSearched = false;
            if (editable.length() == 0)
            {
                mSearchButton.setText("\u53D6\u6D88");
                mClearSearchText.setVisibility(4);
            } else
            {
                mSearchButton.setText("\u641C\u7D22");
                mClearSearchText.setVisibility(0);
                removeHistoryHotFragment();
            }
            if (!mFromUser)
            {
                mSortHeader.setVisibility(8);
                mScopeGroup.setVisibility(8);
                loadSuggestData(editable.toString());
                return;
            } else
            {
                mFromUser = false;
                return;
            }
        }

        public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
        {
        }

        public void onTextChanged(CharSequence charsequence, int i, int j, int k)
        {
        }

        _cls3()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final WordAssociatedFragment this$0;

        public void onClick(View view)
        {
            mDataList.clear();
            mHistoryList.clear();
            mAdapter.notifyDataSetChanged();
            mFooterView.setText("\u65E0\u5386\u53F2\u8BB0\u5F55");
        }

        _cls4()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnTouchListener
    {

        final WordAssociatedFragment this$0;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            hideSoftInput();
            return false;
        }

        _cls5()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.widget.AbsListView.OnScrollListener
    {

        final WordAssociatedFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0 && ((mAdapter instanceof SearchAlbumAdapterNew) || (mAdapter instanceof SearchPersonAdapter) || (mAdapter instanceof SearchSoundAdapter)))
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || (mPageId - 1) * mPageSize > mTotal)
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        mFooterView.setVisibility(0);
                        mFooterView.setText("\u52A0\u8F7D\u66F4\u591A\u7ED3\u679C...");
                        abslistview = new AssociateModel();
                        abslistview.title = getSearchWord();
                        abslistview.type = getSearchScope();
                        loadSearch(abslistview, mListView);
                    }
                }
                return;
            }
            mFooterView.setVisibility(8);
        }

        _cls6()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.widget.AdapterView.OnItemClickListener
    {

        final WordAssociatedFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i >= 0 && i < mDataList.size()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            android.widget.ListAdapter listadapter = mListView.getAdapter();
            adapterview = listadapter;
            if (listadapter instanceof WrapperListAdapter)
            {
                adapterview = ((WrapperListAdapter)listadapter).getWrappedAdapter();
            }
            if (!(adapterview instanceof AssocicateAdapterNew))
            {
                continue; /* Loop/switch isn't completed */
            }
            adapterview = ((AdapterView) (mDataList.get(i)));
            if (!(adapterview instanceof AssociateKeywordModel)) goto _L1; else goto _L3
_L3:
            adapterview = (AssociateKeywordModel)adapterview;
            if (!"".equals(adapterview.getKeyword()) && !adapterview.isAlbum())
            {
                mFromUser = true;
                mSearchText.setText(adapterview.getKeyword());
            }
            mPageId = 1;
            mDataList.clear();
            mAdapter.notifyDataSetChanged();
            onSuggestItemClick(adapterview, view);
            return;
            if (!(adapterview instanceof SearchAllAdapterNew) && !(adapterview instanceof SearchAlbumAdapterNew) && !(adapterview instanceof SearchPersonAdapter) && !(adapterview instanceof SearchSoundAdapter) || mDataList == null || mDataList.size() < 0) goto _L1; else goto _L4
_L4:
            boolean flag;
            if (adapterview instanceof SearchAllAdapterNew)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            adapterview = ((AdapterView) (mDataList.get(i)));
            onSearchResultClick(adapterview, view, i, flag);
            return;
        }

        _cls7()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls8
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final WordAssociatedFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            android.widget.ListAdapter listadapter = mListView.getAdapter();
            adapterview = listadapter;
            if (listadapter instanceof WrapperListAdapter)
            {
                adapterview = ((WrapperListAdapter)listadapter).getWrappedAdapter();
            }
            if (adapterview instanceof SearchAllAdapterNew)
            {
                adapterview = (SearchAllAdapterNew)adapterview;
                if (adapterview.isSoundItem(i))
                {
                    adapterview.handleItemLongClick((SearchSound)adapterview.getData().get(i), view);
                    return true;
                }
            } else
            if (adapterview instanceof SearchSoundAdapter)
            {
                adapterview = (SearchSoundAdapter)adapterview;
                adapterview.handleItemLongClick((SearchSound)adapterview.getData().get(i), view);
                return true;
            }
            return false;
        }

        _cls8()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls9
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final WordAssociatedFragment this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            mSortHeader.setVisibility(8);
            mSortBtn1.setChecked(true);
            mSort = "";
            if (!TextUtils.isEmpty(mSearchText.getText().toString().trim()))
            {
                radiogroup = new AssociateModel();
                radiogroup.title = getSearchWord();
                radiogroup.type = getSearchScope();
                search(((AssociateModel) (radiogroup)).title, ((AssociateModel) (radiogroup)).type, true, mScopeGroup, mCheckSpell);
            }
        }

        _cls9()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls10
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final WordAssociatedFragment this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            mSort = getSortStr(i);
            radiogroup = new AssociateModel();
            radiogroup.title = getSearchWord();
            radiogroup.type = getSearchScope();
            mPageId = 1;
            mDataList.clear();
            mAdapter.notifyDataSetChanged();
            loadSearch(radiogroup, mSortHeader);
        }

        _cls10()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }


    private class _cls16 extends com.ximalaya.ting.android.b.a
    {

        final WordAssociatedFragment this$0;
        final List val$data;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (isAdded() && !TextUtils.isEmpty(s))
            {
                SearchAlbum searchalbum = null;
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = searchalbum;
                }
                if (s != null && s.getIntValue("ret") == 0)
                {
                    s = s.getJSONObject("status");
                    if (s != null)
                    {
                        int i = 0;
                        while (i < data.size()) 
                        {
                            searchalbum = (SearchAlbum)data.get(i);
                            boolean flag;
                            if (s.getIntValue((new StringBuilder()).append("").append(searchalbum.id).toString()) == 1)
                            {
                                flag = true;
                            } else
                            {
                                flag = false;
                            }
                            searchalbum.isFavorite = flag;
                            i++;
                        }
                        mAdapter.notifyDataSetChanged();
                        return;
                    }
                }
            }
        }

        _cls16()
        {
            this$0 = WordAssociatedFragment.this;
            data = list;
            super();
        }
    }


    private class _cls15 extends MyAsyncTask
    {

        final WordAssociatedFragment this$0;
        final List val$data;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            if (data != null && data.size() > 0)
            {
                avoid = data.iterator();
                while (avoid.hasNext()) 
                {
                    SearchAlbum searchalbum = (SearchAlbum)avoid.next();
                    boolean flag;
                    if (AlbumModelManage.getInstance().isHadCollected(searchalbum.id) != null)
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                    searchalbum.isFavorite = flag;
                }
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            mAdapter.notifyDataSetChanged();
        }

        _cls15()
        {
            this$0 = WordAssociatedFragment.this;
            data = list;
            super();
        }
    }


    private class _cls11 extends com.ximalaya.ting.android.b.a
    {

        final WordAssociatedFragment this$0;
        final AssociateModel val$as;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
            DataCollectUtil.bindDataToView(aheader, mListView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01");
        }

        public void onStart()
        {
            super.onStart();
            mIsLoading = true;
            mFooterView.setVisibility(0);
            mFooterView.setText("\u6B63\u5728\u641C\u7D22...");
        }

        public void onSuccess(String s)
        {
            if (canGoon())
            {
                initSortHeader();
                if (TextUtils.isEmpty(s))
                {
                    mFooterView.setVisibility(0);
                    mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
                    return;
                }
                if ("all".equals(as.type) && "all".equals(getSearchScope()))
                {
                    parseSearchAll(s);
                    return;
                }
                if ("album".equals(as.type) && "album".equals(getSearchScope()))
                {
                    parseSearchAlbum(s);
                    return;
                }
                if ("user".equals(as.type) && "user".equals(getSearchScope()))
                {
                    parseSearchPerson(s);
                    return;
                }
                if ("track".equals(as.type) && "track".equals(getSearchScope()))
                {
                    parseSearchSound(s);
                    return;
                }
            }
        }

        _cls11()
        {
            this$0 = WordAssociatedFragment.this;
            as = associatemodel;
            super();
        }
    }


    private class _cls12 extends com.ximalaya.ting.android.b.a
    {

        final WordAssociatedFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mListView);
        }

        public void onNetError(int i, String s)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u7F51\u7EDC\u4E0D\u7ED9\u529B\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5");
            mListView.setFooterDividersEnabled(false);
        }

        public void onSuccess(String s)
        {
            while (!canGoon() || mHasSearched || TextUtils.isEmpty(s)) 
            {
                return;
            }
            s = (AssociateSuggestModel)JSON.parseObject(s, com/ximalaya/ting/android/model/search/AssociateSuggestModel);
            if (s != null) goto _L2; else goto _L1
_L1:
            mAdapter.notifyDataSetChanged();
            mFooterView.setVisibility(8);
            s = mListView;
_L4:
            s.setFooterDividersEnabled(false);
            return;
_L2:
            try
            {
                mDataList.clear();
                if (s.getAlbumResultList() == null)
                {
                    break MISSING_BLOCK_LABEL_187;
                }
                for (Iterator iterator = s.getAlbumResultList().iterator(); iterator.hasNext(); ((AssociateKeywordModel)iterator.next()).setAlbum(true)) { }
                break MISSING_BLOCK_LABEL_170;
            }
            // Misplaced declaration of an exception variable
            catch (String s) { }
            finally
            {
                mAdapter.notifyDataSetChanged();
                mFooterView.setVisibility(8);
                mListView.setFooterDividersEnabled(false);
                throw s;
            }
            s.printStackTrace();
            mAdapter.notifyDataSetChanged();
            mFooterView.setVisibility(8);
            s = mListView;
            continue; /* Loop/switch isn't completed */
            mDataList.addAll(s.getAlbumResultList());
            if (s.getQueryResultList() == null)
            {
                break MISSING_BLOCK_LABEL_282;
            }
            for (Iterator iterator1 = s.getQueryResultList().iterator(); iterator1.hasNext(); ((AssociateKeywordModel)iterator1.next()).setAlbum(false)) { }
            break MISSING_BLOCK_LABEL_265;
            mDataList.addAll(s.getQueryResultList());
            mAdapter.notifyDataSetChanged();
            mFooterView.setVisibility(8);
            s = mListView;
            if (true) goto _L4; else goto _L3
_L3:
        }

        _cls12()
        {
            this$0 = WordAssociatedFragment.this;
            super();
        }
    }

}
