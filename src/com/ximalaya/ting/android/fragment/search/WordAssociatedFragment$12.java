// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.search;

import android.text.TextUtils;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.search.AssociateKeywordModel;
import com.ximalaya.ting.android.model.search.AssociateSuggestModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.util.Iterator;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.search:
//            WordAssociatedFragment

class this._cls0 extends a
{

    final WordAssociatedFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, WordAssociatedFragment.access$2000(WordAssociatedFragment.this));
    }

    public void onNetError(int i, String s)
    {
        WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setVisibility(0);
        WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setText("\u7F51\u7EDC\u4E0D\u7ED9\u529B\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5");
        WordAssociatedFragment.access$2000(WordAssociatedFragment.this).setFooterDividersEnabled(false);
    }

    public void onSuccess(String s)
    {
        while (!canGoon() || WordAssociatedFragment.access$300(WordAssociatedFragment.this) || TextUtils.isEmpty(s)) 
        {
            return;
        }
        s = (AssociateSuggestModel)JSON.parseObject(s, com/ximalaya/ting/android/model/search/AssociateSuggestModel);
        if (s != null) goto _L2; else goto _L1
_L1:
        WordAssociatedFragment.access$1300(WordAssociatedFragment.this).notifyDataSetChanged();
        WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setVisibility(8);
        s = WordAssociatedFragment.access$2000(WordAssociatedFragment.this);
_L4:
        s.setFooterDividersEnabled(false);
        return;
_L2:
        try
        {
            WordAssociatedFragment.access$1100(WordAssociatedFragment.this).clear();
            if (s.getAlbumResultList() == null)
            {
                break MISSING_BLOCK_LABEL_187;
            }
            for (Iterator iterator = s.getAlbumResultList().iterator(); iterator.hasNext(); ((AssociateKeywordModel)iterator.next()).setAlbum(true)) { }
            break MISSING_BLOCK_LABEL_170;
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
        finally
        {
            WordAssociatedFragment.access$1300(WordAssociatedFragment.this).notifyDataSetChanged();
            WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setVisibility(8);
            WordAssociatedFragment.access$2000(WordAssociatedFragment.this).setFooterDividersEnabled(false);
            throw s;
        }
        s.printStackTrace();
        WordAssociatedFragment.access$1300(WordAssociatedFragment.this).notifyDataSetChanged();
        WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setVisibility(8);
        s = WordAssociatedFragment.access$2000(WordAssociatedFragment.this);
        continue; /* Loop/switch isn't completed */
        WordAssociatedFragment.access$1100(WordAssociatedFragment.this).addAll(s.getAlbumResultList());
        if (s.getQueryResultList() == null)
        {
            break MISSING_BLOCK_LABEL_282;
        }
        for (Iterator iterator1 = s.getQueryResultList().iterator(); iterator1.hasNext(); ((AssociateKeywordModel)iterator1.next()).setAlbum(false)) { }
        break MISSING_BLOCK_LABEL_265;
        WordAssociatedFragment.access$1100(WordAssociatedFragment.this).addAll(s.getQueryResultList());
        WordAssociatedFragment.access$1300(WordAssociatedFragment.this).notifyDataSetChanged();
        WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setVisibility(8);
        s = WordAssociatedFragment.access$2000(WordAssociatedFragment.this);
        if (true) goto _L4; else goto _L3
_L3:
    }

    I()
    {
        this$0 = WordAssociatedFragment.this;
        super();
    }
}
