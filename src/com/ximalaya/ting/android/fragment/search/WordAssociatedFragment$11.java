// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.search;

import android.text.TextUtils;
import android.widget.TextView;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.search.AssociateModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.search:
//            WordAssociatedFragment

class val.as extends a
{

    final WordAssociatedFragment this$0;
    final AssociateModel val$as;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        DataCollectUtil.bindDataToView(aheader, WordAssociatedFragment.access$2000(WordAssociatedFragment.this));
    }

    public void onFinish()
    {
        super.onFinish();
        WordAssociatedFragment.access$1902(WordAssociatedFragment.this, false);
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01");
    }

    public void onStart()
    {
        super.onStart();
        WordAssociatedFragment.access$1902(WordAssociatedFragment.this, true);
        WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setVisibility(0);
        WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setText("\u6B63\u5728\u641C\u7D22...");
    }

    public void onSuccess(String s)
    {
        if (canGoon())
        {
            WordAssociatedFragment.access$2900(WordAssociatedFragment.this);
            if (TextUtils.isEmpty(s))
            {
                WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setVisibility(0);
                WordAssociatedFragment.access$1400(WordAssociatedFragment.this).setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
                return;
            }
            if ("all".equals(val$as.type) && "all".equals(WordAssociatedFragment.access$200(WordAssociatedFragment.this)))
            {
                WordAssociatedFragment.access$3000(WordAssociatedFragment.this, s);
                return;
            }
            if ("album".equals(val$as.type) && "album".equals(WordAssociatedFragment.access$200(WordAssociatedFragment.this)))
            {
                WordAssociatedFragment.access$3100(WordAssociatedFragment.this, s);
                return;
            }
            if ("user".equals(val$as.type) && "user".equals(WordAssociatedFragment.access$200(WordAssociatedFragment.this)))
            {
                WordAssociatedFragment.access$3200(WordAssociatedFragment.this, s);
                return;
            }
            if ("track".equals(val$as.type) && "track".equals(WordAssociatedFragment.access$200(WordAssociatedFragment.this)))
            {
                WordAssociatedFragment.access$3300(WordAssociatedFragment.this, s);
                return;
            }
        }
    }

    I()
    {
        this$0 = final_wordassociatedfragment;
        val$as = AssociateModel.this;
        super();
    }
}
