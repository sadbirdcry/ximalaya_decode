// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.search;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.WrapperListAdapter;
import com.ximalaya.ting.android.adapter.SearchAllAdapterNew;
import com.ximalaya.ting.android.adapter.SearchSoundAdapter;
import com.ximalaya.ting.android.model.search.SearchSound;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.search:
//            WordAssociatedFragment

class this._cls0
    implements android.widget.ckListener
{

    final WordAssociatedFragment this$0;

    public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
    {
        android.widget.ListAdapter listadapter = WordAssociatedFragment.access$2000(WordAssociatedFragment.this).getAdapter();
        adapterview = listadapter;
        if (listadapter instanceof WrapperListAdapter)
        {
            adapterview = ((WrapperListAdapter)listadapter).getWrappedAdapter();
        }
        if (adapterview instanceof SearchAllAdapterNew)
        {
            adapterview = (SearchAllAdapterNew)adapterview;
            if (adapterview.isSoundItem(i))
            {
                adapterview.handleItemLongClick((SearchSound)adapterview.getData().get(i), view);
                return true;
            }
        } else
        if (adapterview instanceof SearchSoundAdapter)
        {
            adapterview = (SearchSoundAdapter)adapterview;
            adapterview.handleItemLongClick((SearchSound)adapterview.getData().get(i), view);
            return true;
        }
        return false;
    }

    ()
    {
        this$0 = WordAssociatedFragment.this;
        super();
    }
}
