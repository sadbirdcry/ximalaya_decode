// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            ManageFragment, FragmentCallback

public class BaseFragment extends Fragment
{
    private class ViewPauseRunnable
        implements Runnable
    {

        final BaseFragment this$0;
        private ImageView view;

        public void run()
        {
            if (!mIsResumed)
            {
                String s;
                if (view.getTag() != null && (view.getTag() instanceof Integer))
                {
                    s = ((Integer)view.getTag()).toString();
                } else
                {
                    s = (String)view.getTag();
                }
                if (!TextUtils.isEmpty(s))
                {
                    view.setTag(0x7f0a0039, Boolean.valueOf(true));
                    Log.e("", "Xm set null ");
                    view.setImageBitmap(null);
                    return;
                }
            }
        }

        public ViewPauseRunnable(ImageView imageview)
        {
            this$0 = BaseFragment.this;
            super();
            view = imageview;
        }
    }


    private static final String TAG = com/ximalaya/ting/android/fragment/BaseFragment.getSimpleName();
    public View fragmentBaseContainerView;
    protected Activity mActivity;
    protected FragmentCallback mCallback;
    public Context mCon;
    private List mImageViews;
    private boolean mIsResumed;

    public BaseFragment()
    {
        fragmentBaseContainerView = null;
        mImageViews = new ArrayList();
        mIsResumed = false;
    }

    public boolean canGoon()
    {
        return isAdded() && !isRemoving() && !isDetached();
    }

    public View findViewById(int i)
    {
        if (fragmentBaseContainerView != null)
        {
            return fragmentBaseContainerView.findViewById(i);
        } else
        {
            return getActivity().findViewById(i);
        }
    }

    public ManageFragment getManageFragment()
    {
        if (getActivity() != null && (getActivity() instanceof MainTabActivity2))
        {
            return ((MainTabActivity2)getActivity()).getManageFragment();
        }
        if (MainTabActivity2.isMainTabActivityAvaliable())
        {
            MainTabActivity2.mainTabActivity.getManageFragment();
        }
        return null;
    }

    public void goBackFragment(int i)
    {
        int k;
        if (getManageFragment() != null)
        {
            if ((k = getManageFragment().mStacks.size()) >= i)
            {
                for (int j = 2; j <= i; j++)
                {
                    Fragment fragment = (Fragment)((SoftReference)getManageFragment().mStacks.get(k - j)).get();
                    if (fragment != null)
                    {
                        removeFramentFromManageFragment(fragment);
                    }
                }

                removeTopFramentFromManageFragment();
                return;
            }
        }
    }

    public void goBackFragment(Class class1)
    {
        if (getManageFragment() != null)
        {
            break MISSING_BLOCK_LABEL_112;
        }
_L2:
        return;
_L5:
        int i;
        i++;
_L6:
        int j = getManageFragment().mStacks.size();
        if (j <= 0 || j < i || j - 1 - i < 0 || j - 1 - i >= j) goto _L2; else goto _L1
_L1:
        Fragment fragment = (Fragment)((SoftReference)getManageFragment().mStacks.get(j - 1 - i)).get();
        if (fragment == null) goto _L2; else goto _L3
_L3:
        if (!fragment.getClass().getSimpleName().equals(class1.getSimpleName())) goto _L5; else goto _L4
_L4:
        goBackFragment(i);
        return;
        i = 0;
          goto _L6
    }

    public void goToFindingPage()
    {
        if (getActivity() != null && (getActivity() instanceof MainTabActivity2))
        {
            ((MainTabActivity2)getActivity()).goToFindingPage();
        }
    }

    public final void markImageView(ImageView imageview)
    {
        while (imageview == null || mImageViews.contains(imageview)) 
        {
            return;
        }
        mImageViews.add(imageview);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mCon = getActivity().getApplicationContext();
        mActivity = getActivity();
        bundle = getArguments();
        if (bundle != null)
        {
            bundle = bundle.getString("xdcs_data_bundle");
            MyLogger.getLogger().d((new StringBuilder()).append("xdcs data = ").append(bundle).toString());
            DataCollectUtil.bindDataToView(bundle, fragmentBaseContainerView);
        }
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        super.onActivityResult(i, j, intent);
    }

    public boolean onBackPressed()
    {
        if (getChildFragmentManager().getBackStackEntryCount() > 1)
        {
            getChildFragmentManager().popBackStack(getChildFragmentManager().getBackStackEntryAt(getChildFragmentManager().getBackStackEntryCount() - 1).getId(), 1);
            return true;
        } else
        {
            return false;
        }
    }

    public void onDestroy()
    {
        super.onDestroy();
        mCon = null;
        mActivity = null;
    }

    public void onDestroyView()
    {
        onViewDestory();
        mCon = null;
        mActivity = null;
        super.onDestroyView();
    }

    public void onDetach()
    {
        super.onDetach();
        try
        {
            Field field = android/support/v4/app/Fragment.getDeclaredField("mChildFragmentManager");
            field.setAccessible(true);
            field.set(this, null);
            return;
        }
        catch (NoSuchFieldException nosuchfieldexception)
        {
            throw new RuntimeException(nosuchfieldexception);
        }
        catch (IllegalAccessException illegalaccessexception)
        {
            throw new RuntimeException(illegalaccessexception);
        }
    }

    public void onPause()
    {
        super.onPause();
        mIsResumed = false;
    }

    public void onRefresh()
    {
    }

    public void onResume()
    {
        super.onResume();
        mIsResumed = true;
    }

    protected final void onViewDestory()
    {
        do
        {
            if (mImageViews.size() <= 0)
            {
                break;
            }
            ImageView imageview = (ImageView)mImageViews.remove(0);
            if (imageview != null)
            {
                imageview.setImageBitmap(null);
                imageview.setBackgroundDrawable(null);
            }
        } while (true);
    }

    public final void onViewPause()
    {
        Iterator iterator = mImageViews.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            ImageView imageview = (ImageView)iterator.next();
            if (imageview != null)
            {
                imageview.postDelayed(new ViewPauseRunnable(imageview), 1000L);
            }
        } while (true);
    }

    public final void onViewResume()
    {
        Iterator iterator = mImageViews.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            ImageView imageview = (ImageView)iterator.next();
            if (imageview != null)
            {
                Object obj = imageview.getTag();
                if (obj instanceof String)
                {
                    obj = (String)obj;
                    if (!TextUtils.isEmpty(((CharSequence) (obj))))
                    {
                        int j = ((Integer)imageview.getTag(0x7f0a0038)).intValue();
                        int i = j;
                        if (j == 0)
                        {
                            i = -1;
                        }
                        Log.e("", "Xm set recover ");
                        imageview.setTag(0x7f0a003a, Boolean.valueOf(false));
                        ImageManager2.from(getActivity()).displayImage(imageview, ((String) (obj)), i);
                        imageview.setTag(0x7f0a0039, null);
                    }
                }
            }
        } while (true);
    }

    public void releaseViewTreeImageView()
    {
        if (getView() != null)
        {
            getView().post(new _cls1());
        }
    }

    public void removeFramentFromManageFragment(Fragment fragment)
    {
        if (getActivity() != null && (getActivity() instanceof MainTabActivity2))
        {
            ((MainTabActivity2)getActivity()).removeFramentFromManageFragment(fragment);
        }
    }

    public final boolean removeMarkedImageView(ImageView imageview)
    {
        return mImageViews.remove(imageview);
    }

    public void removeTopFramentFromManageFragment()
    {
        if (getActivity() != null && (getActivity() instanceof MainTabActivity2))
        {
            ((MainTabActivity2)getActivity()).removeTopFramentFromManageFragment();
        }
    }

    public void restoreViewTreeImageView()
    {
        if (getView() != null && getActivity() != null)
        {
            getView().post(new _cls2());
        }
    }

    public void setFragmentCallback(FragmentCallback fragmentcallback)
    {
        mCallback = fragmentcallback;
    }

    protected void setPlayPath(String s)
    {
        if (mActivity != null)
        {
            ((MyApplication)(MyApplication)mActivity.getApplication()).a = s;
        }
    }

    public void showToast(String s)
    {
        if (isAdded())
        {
            CustomToast.showToast(getActivity(), s, 0);
        }
    }

    public void startActivityForResult(Intent intent, int i)
    {
        if (getActivity() != null)
        {
            getActivity().startActivityForResult(intent, i);
        }
    }

    public void startFragment(Fragment fragment)
    {
        if (getActivity() != null && (getActivity() instanceof MainTabActivity2))
        {
            ((MainTabActivity2)getActivity()).startFragment(fragment);
        }
    }

    public void startFragment(Class class1, Bundle bundle)
    {
        if (getActivity() != null && (getActivity() instanceof MainTabActivity2))
        {
            Bundle bundle1 = bundle;
            if (bundle == null)
            {
                bundle1 = new Bundle();
            }
            ((MainTabActivity2)getActivity()).startFragment(class1, bundle1);
        }
    }



    private class _cls1
        implements Runnable
    {

        final BaseFragment this$0;

        public void run()
        {
            ViewUtil.releaseViewTreeImageView(getView());
        }

        _cls1()
        {
            this$0 = BaseFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final BaseFragment this$0;

        public void run()
        {
            ViewUtil.restoreViewTreeImageView(getActivity(), getView());
        }

        _cls2()
        {
            this$0 = BaseFragment.this;
            super();
        }
    }

}
