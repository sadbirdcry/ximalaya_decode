// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.tab;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.umeng.analytics.MobclickAgent;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.homepage.MeDetialActivity;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.activity.recording.RecordingActivity;
import com.ximalaya.ting.android.activity.setting.BindActivity;
import com.ximalaya.ting.android.activity.setting.SettingActivity;
import com.ximalaya.ting.android.activity.setting.UmengFeedbackActivity;
import com.ximalaya.ting.android.activity.share.BaseShareDialog;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.carlink.carlife.CarlifeSettingAct;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.device.MyDevicesFragment;
import com.ximalaya.ting.android.fragment.setting.FindFriendSettingFragment;
import com.ximalaya.ting.android.fragment.ting.CollectFragment;
import com.ximalaya.ting.android.fragment.userspace.AlbumListFragment;
import com.ximalaya.ting.android.fragment.userspace.ManageCenterFragment;
import com.ximalaya.ting.android.fragment.userspace.MyAttentionFragment;
import com.ximalaya.ting.android.fragment.userspace.MyLikedSoundsFragment;
import com.ximalaya.ting.android.fragment.userspace.MyScoreFragment;
import com.ximalaya.ting.android.fragment.userspace.NewsCenterFragment;
import com.ximalaya.ting.android.fragment.userspace.PlayListHostoryFragment;
import com.ximalaya.ting.android.fragment.userspace.SoundListFragmentNew;
import com.ximalaya.ting.android.fragment.zone.ZoneFragment;
import com.ximalaya.ting.android.kdt.KDTAction;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.NoReadModel;
import com.ximalaya.ting.android.model.personal_info.HomePageModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.modelmanage.NoReadManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.SharedPreferencesUserUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.StretchScrollView;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MySpaceFragmentNew extends BaseFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.modelmanage.NoReadManage.NoReadUpdateListener
{
    class GetHomePageInfo extends MyAsyncTask
    {

        HomePageModel homeModel2;
        final MySpaceFragmentNew this$0;

        protected transient Integer doInBackground(String as[])
        {
            Object obj = null;
            as = f.a().a(e.E, null, fragmentBaseContainerView, fragmentBaseContainerView, false);
            com.ximalaya.ting.android.b.n.a a1;
            if (((com.ximalaya.ting.android.b.n.a) (as)).b == 1)
            {
                as = ((com.ximalaya.ting.android.b.n.a) (as)).a;
            } else
            {
                as = null;
            }
            a1 = f.a().a(e.T, null, fragmentBaseContainerView, null, false);
            if (a1.b == 1)
            {
                obj = a1.a;
            }
            if (Utilities.isBlank(as))
            {
                return Integer.valueOf(1);
            }
            obj = (NoReadModel)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/NoReadModel);
            homeModel2 = (HomePageModel)JSON.parseObject(as, com/ximalaya/ting/android/model/personal_info/HomePageModel);
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_131;
            }
            if (((NoReadModel) (obj)).ret == 0)
            {
                homeModel2.favoriteAlbumIsUpdate = ((NoReadModel) (obj)).favoriteAlbumIsUpdate;
            }
            if (UserInfoMannage.hasLogined())
            {
                UserInfoMannage.getInstance().getUser().isVerified = homeModel2.isVerified();
            }
_L1:
            if (homeModel2 == null)
            {
                return Integer.valueOf(2);
            } else
            {
                return Integer.valueOf(3);
            }
            as;
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(as.getMessage()).append(Logger.getLineInfo()).toString());
              goto _L1
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (isAdded() && mCon != null && integer.intValue() == 3 && homeModel2 != null)
            {
                if (homeModel2.ret == 0)
                {
                    updateMyspaceView(homeModel2);
                    refreshLoginInfo(homeModel2);
                    return;
                }
                if (mCon != null && homeModel != null && Utilities.isNotBlank(homeModel.msg))
                {
                    Toast.makeText(mCon, homeModel.msg, 0).show();
                    return;
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        GetHomePageInfo()
        {
            this$0 = MySpaceFragmentNew.this;
            super();
        }
    }

    class UpdateHeadTask extends MyAsyncTask
    {

        String backgroundLogo;
        BaseModel bm;
        int flag;
        String largeLogo;
        String middleLogo;
        ProgressDialog pd;
        String smallLogo;
        final MySpaceFragmentNew this$0;

        protected transient Integer doInBackground(Object aobj[])
        {
            Object obj = null;
            flag = ((Integer)aobj[0]).intValue();
            String s;
            ImageView imageview;
            if (flag == 2)
            {
                s = e.p;
            } else
            if (flag == 1)
            {
                s = e.o;
            } else
            {
                s = "";
            }
            if (flag == 2)
            {
                imageview = bacImg;
            } else
            if (flag == 1)
            {
                imageview = headImg;
            } else
            {
                imageview = null;
            }
            try
            {
                RequestParams requestparams = new RequestParams();
                requestparams.put("uid", (new StringBuilder()).append(mLoginInfoModel.uid).append("").toString());
                requestparams.put("token", mLoginInfoModel.token);
                requestparams.put("image", (File)aobj[1]);
                aobj = f.a().b(s, requestparams, imageview, imageview);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                aobj = null;
            }
            if (Utilities.isBlank(((String) (aobj))))
            {
                return Integer.valueOf(1);
            }
            try
            {
                aobj = JSON.parseObject(((String) (aobj)));
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
                aobj = obj;
            }
            if (aobj != null)
            {
                bm = new BaseModel();
                bm.ret = ((JSONObject) (aobj)).getInteger("ret").intValue();
                if (bm.ret != -1)
                {
                    bm.msg = ((JSONObject) (aobj)).getString("msg");
                    if (bm.ret == 0)
                    {
                        largeLogo = ((JSONObject) (aobj)).getString("mobileLargeLogo");
                        smallLogo = ((JSONObject) (aobj)).getString("mobileSmallLogo");
                        middleLogo = ((JSONObject) (aobj)).getString("mobileMiddleLogo");
                        backgroundLogo = ((JSONObject) (aobj)).getString("backgroundLogo");
                    }
                }
                return Integer.valueOf(3);
            } else
            {
                return Integer.valueOf(2);
            }
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (isAdded() && mCon != null) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if (pd != null)
            {
                pd.cancel();
                pd = null;
            }
            if (integer.intValue() != 3)
            {
                break MISSING_BLOCK_LABEL_164;
            }
            if (bm == null || bm.ret != 0)
            {
                continue; /* Loop/switch isn't completed */
            }
            if (flag == 2)
            {
                ImageManager2.from(mCon).displayImage(bacImg, backgroundLogo, -1, true);
                return;
            }
            if (flag != 1) goto _L1; else goto _L3
_L3:
            ImageManager2.from(mCon).displayImage(headImg, middleLogo, -1, true);
            return;
            if (bm == null) goto _L1; else goto _L4
_L4:
            Toast.makeText(mCon, bm.msg, 1).show();
            return;
            Context context = mCon;
            StringBuilder stringbuilder = (new StringBuilder()).append("\u4FEE\u6539");
            if (flag == 2)
            {
                integer = "\u80CC\u666F\u56FE";
            } else
            {
                integer = "\u5934\u50CF";
            }
            Toast.makeText(context, stringbuilder.append(integer).append("\u5931\u8D25").toString(), 1).show();
            return;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            pd = new MyProgressDialog(getActivity());
            class _cls1
                implements android.content.DialogInterface.OnKeyListener
            {

                final UpdateHeadTask this$1;

                public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
                {
                    if (i == 4)
                    {
                        dialoginterface.dismiss();
                        return true;
                    } else
                    {
                        return false;
                    }
                }

                _cls1()
                {
                    this$1 = UpdateHeadTask.this;
                    super();
                }
            }

            pd.setOnKeyListener(new _cls1());
            pd.setCanceledOnTouchOutside(true);
            pd.setTitle("\u4E0A\u4F20");
            ProgressDialog progressdialog = pd;
            StringBuilder stringbuilder = new StringBuilder();
            String s;
            if (flag == 2)
            {
                s = "\u80CC\u666F\u56FE";
            } else
            {
                s = "\u5934\u50CF";
            }
            progressdialog.setMessage(stringbuilder.append(s).append("\u4E0A\u4F20\u4E2D").toString());
            pd.show();
        }

        UpdateHeadTask()
        {
            this$0 = MySpaceFragmentNew.this;
            super();
            pd = null;
        }
    }


    private static final int BackgroundImageTopHideDp = 40;
    private static final int EDIT_INFO = 2563;
    private static final String FIRST_IN = "first_in";
    private static final int IMAGE_FROM_CAMERA = 161;
    private static final int IMAGE_FROM_PHOTOS = 4066;
    private static final String IMAGE_UNSPECIFIED = "image/*";
    private static final String MySpaceFragment_HomeModel = "MySpaceFragment_HomeModel";
    private static final int PHOTORESOULT = 2803;
    private static final int UPDATE_BG = 2;
    private static final int UPDATE_HEAD = 1;
    private ImageView bacImg;
    private View bindLayout;
    private ImageView collectAlbumsUpdateFlag;
    private int curUpdate;
    private volatile boolean firstShow;
    private View freeFlowBar;
    private Handler handler;
    private ImageView headImg;
    private HomePageModel homeModel;
    private View introLayout;
    private TextView introTextView;
    private MenuDialog mChangeAvatarDialog;
    private LoginInfoModel mLoginInfoModel;
    private ImageView mNewsCenter_rightArrow;
    private ImageView mailBindImg;
    private RelativeLayout myZoneLayout;
    private View myspaceView;
    private ImageView newFansFlag;
    private TextView newFriendsNumTextView;
    private TextView nickNameTextView;
    private ImageView noUploadSoundFlagImg;
    private TextView numFansTextView;
    private TextView numFocusPeopleTextView;
    private TextView numNewsTextView;
    private TextView numPublishAlbumTextView;
    private TextView numPublishSoundTextView;
    private ImageView phoneBindImg;
    private PopupWindow popup;
    private ImageView qqBindImg;
    private ImageView renrenBindImg;
    private ImageView settingImg;
    private ImageView shareImg;
    private ImageView sinaBindImg;
    private View sounds_layout;
    private ImageView unicomFlag;
    private ImageView unicomImg;
    private TextView unicomTxt;

    public MySpaceFragmentNew()
    {
        curUpdate = -1;
        firstShow = true;
        handler = new Handler();
    }

    private void getFromCamera()
    {
        try
        {
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            intent.putExtra("output", Uri.fromFile(getTempHeadFile()));
            startActivityForResult(intent, 161);
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void getFromPhotos()
    {
        Intent intent = new Intent("android.intent.action.PICK", null);
        intent.setDataAndType(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, 4066);
    }

    private File getTempHeadFile()
    {
        File file;
        if (Environment.getExternalStorageState().equals("mounted"))
        {
            file = new File(Environment.getExternalStorageDirectory(), "/ting/images");
        } else
        {
            file = mCon.getCacheDir();
        }
        if (file.exists()) goto _L2; else goto _L1
_L1:
        file.mkdirs();
_L4:
        file = new File(file, "head.jpg");
        if (!file.exists())
        {
            try
            {
                file.createNewFile();
            }
            catch (IOException ioexception)
            {
                ioexception.printStackTrace();
                return file;
            }
        }
        return file;
_L2:
        if (file.isFile())
        {
            file.deleteOnExit();
            file.mkdirs();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void initData()
    {
        mLoginInfoModel = UserInfoMannage.getInstance().getUser();
        KDTAction.isMyOrdersShow = Boolean.parseBoolean(MobclickAgent.getConfigParams(getActivity(), "myorders_show"));
    }

    private void initViews()
    {
        if (PackageUtil.isMeizu() && getActivity() != null)
        {
            findViewById(0x7f0a057a).setPadding(0, 0, 0, Utilities.dip2px(getActivity(), 70F));
        }
        numPublishSoundTextView = (TextView)findViewById(0x7f0a05af);
        numPublishAlbumTextView = (TextView)findViewById(0x7f0a05b2);
        numFocusPeopleTextView = (TextView)findViewById(0x7f0a05b4);
        numFansTextView = (TextView)findViewById(0x7f0a05b7);
        noUploadSoundFlagImg = (ImageView)findViewById(0x7f0a05b0);
        numNewsTextView = (TextView)findViewById(0x7f0a0580);
        mNewsCenter_rightArrow = (ImageView)findViewById(0x7f0a0581);
        newFriendsNumTextView = (TextView)findViewById(0x7f0a0583);
        collectAlbumsUpdateFlag = (ImageView)findViewById(0x7f0a058b);
        bacImg = (ImageView)findViewById(0x7f0a05a4);
        headImg = (ImageView)findViewById(0x7f0a05a6);
        nickNameTextView = (TextView)findViewById(0x7f0a05a7);
        introTextView = (TextView)findViewById(0x7f0a05aa);
        newFansFlag = (ImageView)findViewById(0x7f0a05b8);
        qqBindImg = (ImageView)findViewById(0x7f0a059c);
        sinaBindImg = (ImageView)findViewById(0x7f0a059d);
        phoneBindImg = (ImageView)findViewById(0x7f0a059e);
        mailBindImg = (ImageView)findViewById(0x7f0a059f);
        renrenBindImg = (ImageView)findViewById(0x7f0a05a0);
        freeFlowBar = findViewById(0x7f0a0594);
        myZoneLayout = (RelativeLayout)findViewById(0x7f0a0590);
        bindLayout = findViewById(0x7f0a0598);
        shareImg = (ImageView)findViewById(0x7f0a05ab);
        settingImg = (ImageView)findViewById(0x7f0a05a5);
        introLayout = findViewById(0x7f0a05a8);
        sounds_layout = findViewById(0x7f0a05ac);
        unicomImg = (ImageView)findViewById(0x7f0a0595);
        unicomFlag = (ImageView)findViewById(0x7f0a0597);
        unicomTxt = (TextView)findViewById(0x7f0a0596);
        final int width = ToolUtil.dp2px(mCon, 40F);
        myspaceView = findViewById(0x7f0a057a);
        ((StretchScrollView)myspaceView).setMaxScrollHeight(width);
        if (android.os.Build.VERSION.SDK_INT < 9)
        {
            findViewById(0x7f0a05a1).setVisibility(8);
        }
        SharedPreferencesUtil sharedpreferencesutil;
        boolean flag;
        if (FreeFlowUtil.getInstance().getOrderStatus() == 1)
        {
            unicomFlag.setVisibility(8);
        } else
        {
            unicomFlag.setVisibility(0);
        }
        unicomTxt.setText("\u514D\u6D41\u91CF\u670D\u52A1");
        flag = FreeFlowUtil.getInstance().isChinaUnicomUser();
        if (!FreeFlowUtil.unicomFreeFlowSwitchOn() || !flag)
        {
            freeFlowBar.setVisibility(8);
        } else
        {
            freeFlowBar.setVisibility(0);
        }
        sharedpreferencesutil = SharedPreferencesUtil.getInstance(getActivity());
        if (!sharedpreferencesutil.getBoolean("first_in"))
        {
            width = 1;
        } else
        {
            width = 0;
        }
        if (width != 0 && flag && FreeFlowUtil.unicomFreeFlowSwitchOn())
        {
            sharedpreferencesutil.saveBoolean("first_in", true);
            width = ToolUtil.dp2px(getActivity(), 200F);
            final int height = ToolUtil.dp2px(getActivity(), 50F);
            handler.post(new _cls1());
            myspaceView.setOnTouchListener(new _cls2());
        }
    }

    private void initViewsListener()
    {
        findViewById(0x7f0a057b).setOnClickListener(this);
        findViewById(0x7f0a05ad).setOnClickListener(this);
        findViewById(0x7f0a05b1).setOnClickListener(this);
        findViewById(0x7f0a05b3).setOnClickListener(this);
        findViewById(0x7f0a05b5).setOnClickListener(this);
        findViewById(0x7f0a057e).setOnClickListener(this);
        findViewById(0x7f0a0582).setOnClickListener(this);
        findViewById(0x7f0a0585).setOnClickListener(this);
        findViewById(0x7f0a0587).setOnClickListener(this);
        findViewById(0x7f0a0589).setOnClickListener(this);
        findViewById(0x7f0a058e).setOnClickListener(this);
        findViewById(0x7f0a0592).setOnClickListener(this);
        findViewById(0x7f0a0598).setOnClickListener(this);
        findViewById(0x7f0a0594).setOnClickListener(this);
        if (mLoginInfoModel != null)
        {
            findViewById(0x7f0a05a4).setOnClickListener(this);
        }
        findViewById(0x7f0a05a9).setOnClickListener(this);
        findViewById(0x7f0a05a6).setOnClickListener(this);
        findViewById(0x7f0a05b9).setOnClickListener(this);
        findViewById(0x7f0a05a1).setOnClickListener(this);
        findViewById(0x7f0a05a2).setOnClickListener(this);
        findViewById(0x7f0a058c).setOnClickListener(this);
        shareImg.setOnClickListener(this);
        settingImg.setOnClickListener(this);
        myZoneLayout.setOnClickListener(this);
        NoReadManage.getInstance().setOnNoReadUpdateListenerListener(this);
    }

    private LoginInfoModel parseLoginfo(String s)
    {
label0:
        {
            try
            {
                if (!"0".equals(JSON.parseObject(s).get("ret").toString()))
                {
                    break label0;
                }
                s = (LoginInfoModel)JSON.parseObject(s, com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
                return null;
            }
            return s;
        }
        return null;
    }

    private void readCacheFromLocal()
    {
        String s;
        if (mLoginInfoModel == null || !firstShow)
        {
            break MISSING_BLOCK_LABEL_109;
        }
        firstShow = false;
        s = SharedPreferencesUserUtil.getInstance(mCon, (new StringBuilder()).append(mLoginInfoModel.uid).append("").toString()).getString("MySpaceFragment_HomeModel");
        if (!Utilities.isNotBlank(s))
        {
            break MISSING_BLOCK_LABEL_109;
        }
        homeModel = (HomePageModel)JSON.parseObject(s, com/ximalaya/ting/android/model/personal_info/HomePageModel);
        if (UserInfoMannage.hasLogined())
        {
            UserInfoMannage.getInstance().getUser().isVerified = homeModel.isVerified();
        }
        updateMyspaceView(homeModel);
        return;
        Exception exception;
        exception;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
        return;
    }

    private void refreshLoginInfo(HomePageModel homepagemodel)
    {
        Object obj = UserInfoMannage.getInstance();
        if (obj != null && ((UserInfoMannage) (obj)).getUser() != null)
        {
            setPersonalInfo(((UserInfoMannage) (obj)).getUser(), homepagemodel);
        }
        obj = SharedPreferencesUtil.getInstance(getActivity());
        Object obj1 = ((SharedPreferencesUtil) (obj)).getString("loginforesult");
        if (obj1 != null && !"".equals(obj1))
        {
            obj1 = parseLoginfo(((String) (obj1)));
            if (obj1 != null)
            {
                setPersonalInfo(((LoginInfoModel) (obj1)), homepagemodel);
                ((SharedPreferencesUtil) (obj)).saveString("loginforesult", JSON.toJSONString(obj1));
            }
        }
    }

    private void refreshLoginInfo(List list)
    {
        Object obj = UserInfoMannage.getInstance();
        if (obj != null && ((UserInfoMannage) (obj)).getUser() != null)
        {
            setBindStatus(((UserInfoMannage) (obj)).getUser(), list);
        }
        obj = SharedPreferencesUtil.getInstance(getActivity());
        Object obj1 = ((SharedPreferencesUtil) (obj)).getString("loginforesult");
        if (obj1 != null && !"".equals(obj1))
        {
            obj1 = parseLoginfo(((String) (obj1)));
            if (obj1 != null)
            {
                setBindStatus(((LoginInfoModel) (obj1)), list);
                ((SharedPreferencesUtil) (obj)).saveString("loginforesult", JSON.toJSONString(obj1));
            }
        }
    }

    private void removeOtherRecording(List list, int i)
    {
        int j = i;
        if (i < list.size()) goto _L2; else goto _L1
_L1:
        return;
_L4:
        j++;
_L2:
        if (j >= list.size())
        {
            continue; /* Loop/switch isn't completed */
        }
        RecordingModel recordingmodel = (RecordingModel)list.get(j);
        if (recordingmodel.uid != mLoginInfoModel.uid)
        {
            list.remove(recordingmodel);
            removeOtherRecording(list, j);
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (true) goto _L1; else goto _L5
_L5:
    }

    private void saveSomething()
    {
        if (mLoginInfoModel != null)
        {
            SharedPreferencesUserUtil sharedpreferencesuserutil = SharedPreferencesUserUtil.getInstance(mCon, (new StringBuilder()).append(mLoginInfoModel.uid).append("").toString());
            if (homeModel != null)
            {
                sharedpreferencesuserutil.saveString("MySpaceFragment_HomeModel", JSON.toJSONString(homeModel));
            }
        }
    }

    private void setBindStatus(LoginInfoModel logininfomodel, List list)
    {
        if (logininfomodel != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        list = list.iterator();
_L5:
        if (!list.hasNext()) goto _L1; else goto _L3
_L3:
        ThirdPartyUserInfo thirdpartyuserinfo = (ThirdPartyUserInfo)list.next();
        if (logininfomodel == null || logininfomodel.bindStatus == null) goto _L1; else goto _L4
_L4:
        Iterator iterator = logininfomodel.bindStatus.iterator();
        while (iterator.hasNext()) 
        {
            ThirdPartyUserInfo thirdpartyuserinfo1 = (ThirdPartyUserInfo)iterator.next();
            if (thirdpartyuserinfo.thirdpartyId == thirdpartyuserinfo1.thirdpartyId)
            {
                if (!TextUtils.isEmpty(thirdpartyuserinfo.nickname))
                {
                    thirdpartyuserinfo1.nickname = thirdpartyuserinfo.nickname;
                }
                if (!TextUtils.isEmpty(thirdpartyuserinfo.thirdpartyUid))
                {
                    thirdpartyuserinfo1.thirdpartyUid = thirdpartyuserinfo.thirdpartyUid;
                }
                thirdpartyuserinfo1.isExpired = thirdpartyuserinfo.isExpired;
            }
        }
          goto _L5
    }

    private void setPersonalInfo(LoginInfoModel logininfomodel, HomePageModel homepagemodel)
    {
        if (!TextUtils.isEmpty(homepagemodel.nickname))
        {
            logininfomodel.nickname = homepagemodel.nickname;
        }
        if (!TextUtils.isEmpty(homepagemodel.mobileLargeLogo))
        {
            logininfomodel.mobileLargeLogo = homepagemodel.mobileLargeLogo;
        }
        if (!TextUtils.isEmpty(homepagemodel.mobileMiddleLogo))
        {
            logininfomodel.mobileMiddleLogo = homepagemodel.mobileMiddleLogo;
        }
        if (!TextUtils.isEmpty(homepagemodel.mobileSmallLogo))
        {
            logininfomodel.mobileSmallLogo = homepagemodel.mobileSmallLogo;
        }
    }

    private void showSelectDialog()
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add("\u4ECE\u76F8\u518C\u9009\u62E9");
        arraylist.add("\u62CD\u7167");
        if (mChangeAvatarDialog == null)
        {
            mChangeAvatarDialog = new MenuDialog(getActivity(), arraylist);
        } else
        {
            mChangeAvatarDialog.setSelections(arraylist);
        }
        mChangeAvatarDialog.setOnItemClickListener(new _cls5());
        mChangeAvatarDialog.show();
    }

    private void updateBindInfo()
    {
        initData();
        if (mLoginInfoModel != null && mLoginInfoModel.mPhone != null)
        {
            phoneBindImg.setImageResource(0x7f0203e5);
        } else
        {
            phoneBindImg.setImageResource(0x7f0203e6);
        }
        if (mLoginInfoModel != null && mLoginInfoModel.email != null && mLoginInfoModel.isVEmail)
        {
            mailBindImg.setImageResource(0x7f02032f);
        } else
        {
            mailBindImg.setImageResource(0x7f020330);
        }
        (new _cls3()).myexec(new Void[0]);
    }

    private void updateHomePageInfo()
    {
        (new GetHomePageInfo()).myexec(new String[0]);
    }

    private void updateMyspaceView(HomePageModel homepagemodel)
    {
        homeModel = homepagemodel;
        nickNameTextView.setText(homeModel.nickname);
        TextView textview;
        int i;
        if (homeModel.isVerified)
        {
            nickNameTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(0x7f02003a), null);
        } else
        {
            nickNameTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
        textview = introTextView;
        if (TextUtils.isEmpty(homeModel.personalSignature))
        {
            homepagemodel = getString(0x7f09016f);
        } else
        {
            homepagemodel = homeModel.personalSignature;
        }
        textview.setText(homepagemodel);
        homepagemodel = myZoneLayout;
        if (a.o && homeModel != null && homeModel.getZoneId() > 0L)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        homepagemodel.setVisibility(i);
        markImageView(bacImg);
        headImg.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mCon).displayImage(bacImg, homeModel.backgroundLogo, -1, true);
        ImageManager2.from(mCon).displayImage(headImg, homeModel.mobileMiddleLogo, 0x7f02036d, true);
        numPublishSoundTextView.setText((new StringBuilder()).append(homeModel.tracks).append("").toString());
        numPublishAlbumTextView.setText((new StringBuilder()).append(homeModel.albums).append("").toString());
        numFocusPeopleTextView.setText((new StringBuilder()).append(homeModel.followings).append("").toString());
        numFansTextView.setText((new StringBuilder()).append(homeModel.followers).append("").toString());
        if (homeModel.noReadFollowers > 0)
        {
            newFansFlag.setVisibility(0);
        } else
        {
            newFansFlag.setVisibility(8);
        }
        if (homeModel.favoriteAlbumIsUpdate)
        {
            collectAlbumsUpdateFlag.setVisibility(0);
        } else
        {
            collectAlbumsUpdateFlag.setVisibility(8);
        }
        if (homeModel.leters + homeModel.messages + homeModel.newZoneCommentCount > 0)
        {
            numNewsTextView.setVisibility(0);
            numNewsTextView.setText((new StringBuilder()).append(homeModel.leters + homeModel.messages + homeModel.newZoneCommentCount).append("").toString());
        } else
        {
            numNewsTextView.setVisibility(8);
        }
        if (homeModel.newThirdRegisters > 0)
        {
            newFriendsNumTextView.setVisibility(0);
            TextView textview1 = newFriendsNumTextView;
            if (homeModel.newThirdRegisters > 99)
            {
                homepagemodel = "N";
            } else
            {
                homepagemodel = (new StringBuilder()).append("").append(homeModel.newThirdRegisters).toString();
            }
            textview1.setText(homepagemodel);
            return;
        } else
        {
            newFriendsNumTextView.setVisibility(8);
            return;
        }
    }

    private void updateNoUploadSoundFlag()
    {
        (new _cls4()).myexec(new String[0]);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initData();
        initViews();
        initViewsListener();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (i != 161) goto _L2; else goto _L1
_L1:
        if (j == -1)
        {
            startPhotoZoom(Uri.fromFile(getTempHeadFile()));
        }
_L4:
        return;
_L2:
        if (i != 4066)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == -1)
        {
            startPhotoZoom(intent.getData());
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (i != 2803)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (j != -1) goto _L4; else goto _L5
_L5:
        (new UpdateHeadTask()).myexec(new Object[] {
            Integer.valueOf(curUpdate), getTempHeadFile()
        });
        return;
        if (i != 2563 || j != -1) goto _L4; else goto _L6
_L6:
        String s = intent.getStringExtra("nickName");
        intent = intent.getStringExtra("intro");
        nickNameTextView.setText(s);
        introTextView.setText(intent);
        return;
    }

    public void onClick(View view)
    {
        int i;
        int j;
label0:
        {
label1:
            {
                i = 0;
                if (OneClickHelper.getInstance().onClick(view))
                {
                    j = view.getId();
                    if (UserInfoMannage.getInstance().getUser() != null)
                    {
                        break label0;
                    }
                    if (j != 0x7f0a0587)
                    {
                        break label1;
                    }
                    (new Bundle()).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                    startFragment(com/ximalaya/ting/android/fragment/userspace/PlayListHostoryFragment, null);
                }
                return;
            }
            if (j == 0x7f0a0594)
            {
                FreeFlowUtil.getInstance().gotoOrderPage();
                return;
            }
            if (j == 0x7f0a05a5 || j == 0x7f0a05a2)
            {
                view = new Intent(getActivity(), com/ximalaya/ting/android/activity/setting/SettingActivity);
                view.setFlags(0x20000000);
                view.addFlags(0x20000);
                startActivity(view);
                return;
            }
            if (j == 0x7f0a05a1)
            {
                startActivity(new Intent(mCon, com/ximalaya/ting/android/activity/setting/UmengFeedbackActivity));
                return;
            }
        }
          goto _L1
_L27:
        Object obj = NoReadManage.getInstance().getNoReadModel();
        j;
        JVM INSTR lookupswitch 25: default 384
    //                   2131363195: 426
    //                   2131363198: 828
    //                   2131363202: 765
    //                   2131363205: 1184
    //                   2131363207: 839
    //                   2131363209: 869
    //                   2131363212: 1173
    //                   2131363214: 927
    //                   2131363216: 1077
    //                   2131363218: 957
    //                   2131363220: 1143
    //                   2131363224: 987
    //                   2131363233: 1152
    //                   2131363234: 1038
    //                   2131363236: 452
    //                   2131363237: 1038
    //                   2131363238: 513
    //                   2131363240: 464
    //                   2131363241: 464
    //                   2131363243: 525
    //                   2131363245: 564
    //                   2131363249: 594
    //                   2131363251: 632
    //                   2131363253: 670
    //                   2131363257: 741;
           goto _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11 _L12 _L13 _L14 _L15 _L16 _L17 _L16 _L18 _L19 _L19 _L20 _L21 _L22 _L23 _L24 _L25
_L2:
        break; /* Loop/switch isn't completed */
_L6:
        break MISSING_BLOCK_LABEL_1184;
_L28:
        NoReadManage.getInstance().updateNoReadManageManual();
        return;
_L1:
        if (j == 0x7f0a058c || j == 0x7f0a0585 || j == 0x7f0a0589) goto _L27; else goto _L26
_L26:
        startActivity(new Intent(getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity));
        return;
_L3:
        if (homeModel != null)
        {
            i = homeModel.albums;
        }
        startFragment(ManageCenterFragment.newInstance(i));
          goto _L28
_L17:
        curUpdate = 2;
        showSelectDialog();
          goto _L28
_L19:
        obj = new Intent(mCon, com/ximalaya/ting/android/activity/homepage/MeDetialActivity);
        ((Intent) (obj)).putExtra("flag", "user");
        ((Intent) (obj)).putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startActivityForResult(((Intent) (obj)), 2563);
          goto _L28
_L18:
        curUpdate = 1;
        showSelectDialog();
          goto _L28
_L20:
        if (homeModel != null)
        {
            (new BaseShareDialog(mActivity, homeModel, view)).show();
        } else
        {
            showToast("\u4EB2\uFF0C\u6CA1\u6709\u4E3B\u64AD\u4FE1\u606F\u54E6~");
        }
          goto _L28
_L21:
        obj = new Bundle();
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/userspace/SoundListFragmentNew, ((Bundle) (obj)));
          goto _L28
_L22:
        obj = new Bundle();
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        ((Bundle) (obj)).putBoolean("showCollect", false);
        startFragment(com/ximalaya/ting/android/fragment/userspace/AlbumListFragment, ((Bundle) (obj)));
          goto _L28
_L23:
        obj = new Bundle();
        ((Bundle) (obj)).putInt("flag", 0);
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/userspace/MyAttentionFragment, ((Bundle) (obj)));
          goto _L28
_L24:
        newFansFlag.setVisibility(8);
        if (homeModel != null)
        {
            homeModel.noReadFollowers = 0;
        }
        if (obj != null)
        {
            obj.noReadFollowers = 0;
        }
        obj = new Bundle();
        ((Bundle) (obj)).putInt("flag", 1);
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/userspace/MyAttentionFragment, ((Bundle) (obj)));
          goto _L28
_L25:
        startActivity(new Intent(getActivity().getApplicationContext(), com/ximalaya/ting/android/activity/recording/RecordingActivity));
          goto _L28
_L5:
        newFriendsNumTextView.setVisibility(8);
        if (homeModel != null)
        {
            homeModel.newThirdRegisters = 0;
        }
        if (obj != null)
        {
            obj.newThirdRegisters = 0;
        }
        obj = new Bundle();
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/setting/FindFriendSettingFragment, ((Bundle) (obj)));
          goto _L28
_L4:
        startFragment(com/ximalaya/ting/android/fragment/userspace/NewsCenterFragment, null);
          goto _L28
_L7:
        obj = new Bundle();
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/userspace/PlayListHostoryFragment, ((Bundle) (obj)));
          goto _L28
_L8:
        ToolUtil.onEvent(getActivity(), "Account_MyCollect");
        collectAlbumsUpdateFlag.setVisibility(8);
        if (obj != null)
        {
            obj.favoriteAlbumIsUpdate = false;
        }
        obj = new Bundle();
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/ting/CollectFragment, ((Bundle) (obj)));
          goto _L28
_L10:
        Bundle bundle = new Bundle();
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/userspace/MyLikedSoundsFragment, bundle);
          goto _L28
_L12:
        Bundle bundle1 = new Bundle();
        bundle1.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/userspace/MyScoreFragment, bundle1);
          goto _L28
_L14:
        Intent intent = new Intent(getActivity(), com/ximalaya/ting/android/activity/setting/BindActivity);
        intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        intent.setFlags(0x20000000);
        intent.addFlags(0x20000);
        startActivity(intent);
          goto _L28
_L16:
        view = new Intent(getActivity(), com/ximalaya/ting/android/activity/setting/SettingActivity);
        view.setFlags(0x20000000);
        view.addFlags(0x20000);
        startActivity(view);
          goto _L28
_L11:
        if (homeModel.getZoneId() > 0L)
        {
            ToolUtil.onEvent(mCon, "zone_mine");
            Bundle bundle2 = new Bundle();
            bundle2.putLong("zoneId", homeModel.getZoneId());
            bundle2.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment, bundle2);
        }
          goto _L28
_L13:
        FreeFlowUtil.getInstance().gotoOrderPage();
          goto _L28
_L15:
        startActivity(new Intent(mCon, com/ximalaya/ting/android/activity/setting/UmengFeedbackActivity));
          goto _L28
_L9:
        startFragment(com/ximalaya/ting/android/fragment/device/MyDevicesFragment, null);
          goto _L28
        startActivity(new Intent(mCon, com/ximalaya/ting/android/carlink/carlife/CarlifeSettingAct));
          goto _L28
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030173, null, false);
        DataCollectUtil.bindViewIdDataToView(fragmentBaseContainerView);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        saveSomething();
        NoReadManage.getInstance().removeNoReadUpdateListenerListener(this);
        super.onDestroyView();
    }

    public void onPause()
    {
        if (popup != null)
        {
            popup.dismiss();
        }
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        if (UserInfoMannage.getInstance().getUser() == null)
        {
            shareImg.setVisibility(8);
            introLayout.setVisibility(8);
            sounds_layout.setVisibility(8);
            nickNameTextView.setText("\u767B\u5F55");
            nickNameTextView.setOnClickListener(this);
            bacImg.setOnClickListener(null);
            bacImg.setImageResource(0x7f0205ac);
            headImg.setImageResource(0x7f02036d);
            bacImg.setTag(null);
            headImg.setTag(null);
        } else
        {
            shareImg.setVisibility(0);
            introLayout.setVisibility(0);
            introLayout.setOnClickListener(this);
            sounds_layout.setVisibility(0);
            nickNameTextView.setOnClickListener(null);
            bacImg.setOnClickListener(this);
        }
        if (((MainTabActivity2)getActivity()).mTabHost.getCurrentTab() == 4 && UserInfoMannage.getInstance().getUser() != null)
        {
            readCacheFromLocal();
            updateHomePageInfo();
            updateBindInfo();
            updateNoUploadSoundFlag();
        }
    }

    public void startPhotoZoom(Uri uri)
    {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 640);
        intent.putExtra("outputY", 640);
        intent.putExtra("scale", true);
        intent.putExtra("output", Uri.fromFile(getTempHeadFile()));
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", android.graphics.Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        startActivityForResult(intent, 2803);
    }

    public void update(NoReadModel noreadmodel)
    {
        if (!isAdded() || noreadmodel == null)
        {
            return;
        }
        if (noreadmodel.noReadFollowers > 0)
        {
            newFansFlag.setVisibility(0);
        } else
        {
            newFansFlag.setVisibility(8);
        }
        if (noreadmodel.favoriteAlbumIsUpdate)
        {
            collectAlbumsUpdateFlag.setVisibility(0);
        } else
        {
            collectAlbumsUpdateFlag.setVisibility(8);
        }
        if (noreadmodel.leters + noreadmodel.messages + noreadmodel.newZoneCommentCount > 0)
        {
            numNewsTextView.setVisibility(0);
            numNewsTextView.setText((new StringBuilder()).append(noreadmodel.leters + noreadmodel.messages + noreadmodel.newZoneCommentCount).append("").toString());
        } else
        {
            numNewsTextView.setVisibility(8);
        }
        if (noreadmodel.newThirdRegisters > 0)
        {
            newFriendsNumTextView.setVisibility(0);
            TextView textview = newFriendsNumTextView;
            if (noreadmodel.newThirdRegisters > 99)
            {
                noreadmodel = "N";
            } else
            {
                noreadmodel = (new StringBuilder()).append("").append(noreadmodel.newThirdRegisters).toString();
            }
            textview.setText(noreadmodel);
            return;
        } else
        {
            newFriendsNumTextView.setVisibility(8);
            return;
        }
    }










/*
    static MenuDialog access$1502(MySpaceFragmentNew myspacefragmentnew, MenuDialog menudialog)
    {
        myspacefragmentnew.mChangeAvatarDialog = menudialog;
        return menudialog;
    }

*/










/*
    static PopupWindow access$302(MySpaceFragmentNew myspacefragmentnew, PopupWindow popupwindow)
    {
        myspacefragmentnew.popup = popupwindow;
        return popupwindow;
    }

*/







    private class _cls1
        implements Runnable
    {

        final MySpaceFragmentNew this$0;
        final int val$height;
        final int val$width;

        public void run()
        {
            ((ScrollView)myspaceView).fullScroll(130);
            class _cls1
                implements Runnable
            {

                final _cls1 this$1;

                public void run()
                {
                    if (
// JavaClassFileOutputException: get_constant: invalid tag

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            handler.postDelayed(new _cls1(), 500L);
        }

        _cls1()
        {
            this$0 = MySpaceFragmentNew.this;
            width = i;
            height = j;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnTouchListener
    {

        final MySpaceFragmentNew this$0;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            if (popup != null)
            {
                popup.dismiss();
            }
            return false;
        }

        _cls2()
        {
            this$0 = MySpaceFragmentNew.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AdapterView.OnItemClickListener
    {

        final MySpaceFragmentNew this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i;
            JVM INSTR tableswitch 0 1: default 24
        //                       0 84
        //                       1 44;
               goto _L1 _L2 _L3
_L1:
            mChangeAvatarDialog.dismiss();
            mChangeAvatarDialog = null;
            return;
_L3:
            if (Environment.getExternalStorageState().equals("mounted"))
            {
                getFromCamera();
            } else
            {
                Toast.makeText(mCon, "\u624B\u673A\u6CA1\u6709SD\u5361", 0).show();
            }
            continue; /* Loop/switch isn't completed */
_L2:
            getFromPhotos();
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls5()
        {
            this$0 = MySpaceFragmentNew.this;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        SharedPreferencesUtil sp;
        final MySpaceFragmentNew this$0;

        protected transient ResponseFindBindStatusInfo doInBackground(Void avoid[])
        {
            Object obj;
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/auth/bindStatus").toString();
            obj = new RequestParams();
            obj = f.a().a(avoid, ((RequestParams) (obj)), bindLayout, bindLayout);
            avoid = new ResponseFindBindStatusInfo();
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_117;
            }
            JSONObject jsonobject = JSON.parseObject(((String) (obj)));
            if (jsonobject.getInteger("ret").intValue() != 0)
            {
                break MISSING_BLOCK_LABEL_207;
            }
            avoid.list = JSON.parseArray(jsonobject.get("data").toString(), com/ximalaya/ting/android/model/personal_setting/ThirdPartyUserInfo);
            avoid.ret = 0;
            sp.saveString("BINDSETTINGINFO", ((String) (obj)));
            return avoid;
            Object obj1;
            try
            {
                obj1 = sp.getString("BINDSETTINGINFO");
            }
            catch (Exception exception)
            {
                avoid.ret = -1;
                avoid.msg = "";
                return avoid;
            }
            if (obj1 == null)
            {
                break MISSING_BLOCK_LABEL_196;
            }
            if ("".equals(obj1))
            {
                break MISSING_BLOCK_LABEL_196;
            }
            obj1 = JSON.parseObject(((String) (obj1)));
            if (((JSONObject) (obj1)).getInteger("ret").intValue() != 0)
            {
                break MISSING_BLOCK_LABEL_207;
            }
            avoid.list = JSON.parseArray(((JSONObject) (obj1)).get("data").toString(), com/ximalaya/ting/android/model/personal_setting/ThirdPartyUserInfo);
            avoid.ret = 0;
            return avoid;
            avoid.ret = -1;
            avoid.msg = "";
            return avoid;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(ResponseFindBindStatusInfo responsefindbindstatusinfo)
        {
            if (responsefindbindstatusinfo.ret == 0 && responsefindbindstatusinfo.list.size() > 0)
            {
                refreshLoginInfo(responsefindbindstatusinfo.list);
                responsefindbindstatusinfo = responsefindbindstatusinfo.list.iterator();
                do
                {
                    if (!responsefindbindstatusinfo.hasNext())
                    {
                        break;
                    }
                    ThirdPartyUserInfo thirdpartyuserinfo = (ThirdPartyUserInfo)responsefindbindstatusinfo.next();
                    switch (Integer.valueOf(thirdpartyuserinfo.thirdpartyId).intValue())
                    {
                    case 1: // '\001'
                        if (thirdpartyuserinfo.isExpired)
                        {
                            sinaBindImg.setImageResource(0x7f020555);
                        } else
                        {
                            sinaBindImg.setImageResource(0x7f020554);
                        }
                        break;

                    case 2: // '\002'
                        if (thirdpartyuserinfo.isExpired)
                        {
                            qqBindImg.setImageResource(0x7f02045c);
                        } else
                        {
                            qqBindImg.setImageResource(0x7f02045b);
                        }
                        break;

                    case 3: // '\003'
                        if (thirdpartyuserinfo.isExpired)
                        {
                            renrenBindImg.setImageResource(0x7f0204db);
                        } else
                        {
                            renrenBindImg.setImageResource(0x7f0204da);
                        }
                        break;
                    }
                } while (true);
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((ResponseFindBindStatusInfo)obj);
        }

        protected void onPreExecute()
        {
            sp = SharedPreferencesUtil.getInstance(getActivity());
        }

        _cls3()
        {
            this$0 = MySpaceFragmentNew.this;
            super();
            sp = null;
        }
    }


    private class _cls4 extends MyAsyncTask
    {

        List rm;
        final MySpaceFragmentNew this$0;

        protected transient Integer doInBackground(String as[])
        {
            if (getActivity() != null && !getActivity().isFinishing())
            {
                rm = z.a(getActivity()).b();
            }
            if (rm != null && rm.size() > 0)
            {
                removeOtherRecording(rm, 0);
                return Integer.valueOf(1);
            } else
            {
                return Integer.valueOf(0);
            }
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (!isAdded() || mCon == null)
            {
                return;
            }
            if (integer.intValue() == 1 && rm.size() > 0)
            {
                noUploadSoundFlagImg.setVisibility(0);
                return;
            } else
            {
                noUploadSoundFlagImg.setVisibility(8);
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        _cls4()
        {
            this$0 = MySpaceFragmentNew.this;
            super();
        }
    }

}
