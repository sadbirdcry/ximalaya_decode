// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.tab;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.ad.NoPageAd;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.List;

public class PlayerCommentBannerAdLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "ting/direct";
    private List mData;
    private long mTrackId;

    public PlayerCommentBannerAdLoader(Context context, long l)
    {
        super(context);
        mTrackId = l;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((List)obj);
    }

    public void deliverResult(List list)
    {
        super.deliverResult(list);
        mData = list;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public List loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("appid", "0");
        ((RequestParams) (obj)).put("device", "android");
        ((RequestParams) (obj)).put("name", "comm_top");
        ((RequestParams) (obj)).put("trackid", mTrackId);
        ((RequestParams) (obj)).put("version", ((MyApplication)getContext().getApplicationContext()).m());
        obj = f.a().a((new StringBuilder()).append(a.Q).append("ting/direct").toString(), ((RequestParams) (obj)), null, null, false);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1)
        {
            try
            {
                if (!TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
                {
                    obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
                    if (((JSONObject) (obj)).getInteger("ret").intValue() == 0)
                    {
                        obj = ((JSONObject) (obj)).getString("data");
                        if (!TextUtils.isEmpty(((CharSequence) (obj))))
                        {
                            mData = NoPageAd.getListFromJSON(((String) (obj)));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        return mData;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
