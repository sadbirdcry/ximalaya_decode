// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.tab;

import android.widget.ImageView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.thirdBind.ResponseFindBindStatusInfo;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.tab:
//            MySpaceFragmentNew

class sp extends MyAsyncTask
{

    SharedPreferencesUtil sp;
    final MySpaceFragmentNew this$0;

    protected transient ResponseFindBindStatusInfo doInBackground(Void avoid[])
    {
        Object obj;
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/auth/bindStatus").toString();
        obj = new RequestParams();
        obj = f.a().a(avoid, ((RequestParams) (obj)), MySpaceFragmentNew.access$600(MySpaceFragmentNew.this), MySpaceFragmentNew.access$600(MySpaceFragmentNew.this));
        avoid = new ResponseFindBindStatusInfo();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_117;
        }
        JSONObject jsonobject = JSON.parseObject(((String) (obj)));
        if (jsonobject.getInteger("ret").intValue() != 0)
        {
            break MISSING_BLOCK_LABEL_207;
        }
        avoid.list = JSON.parseArray(jsonobject.get("data").toString(), com/ximalaya/ting/android/model/personal_setting/ThirdPartyUserInfo);
        avoid.ret = 0;
        sp.saveString("BINDSETTINGINFO", ((String) (obj)));
        return avoid;
        Object obj1;
        try
        {
            obj1 = sp.getString("BINDSETTINGINFO");
        }
        catch (Exception exception)
        {
            avoid.ret = -1;
            avoid.msg = "";
            return avoid;
        }
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_196;
        }
        if ("".equals(obj1))
        {
            break MISSING_BLOCK_LABEL_196;
        }
        obj1 = JSON.parseObject(((String) (obj1)));
        if (((JSONObject) (obj1)).getInteger("ret").intValue() != 0)
        {
            break MISSING_BLOCK_LABEL_207;
        }
        avoid.list = JSON.parseArray(((JSONObject) (obj1)).get("data").toString(), com/ximalaya/ting/android/model/personal_setting/ThirdPartyUserInfo);
        avoid.ret = 0;
        return avoid;
        avoid.ret = -1;
        avoid.msg = "";
        return avoid;
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(ResponseFindBindStatusInfo responsefindbindstatusinfo)
    {
        if (responsefindbindstatusinfo.ret == 0 && responsefindbindstatusinfo.list.size() > 0)
        {
            MySpaceFragmentNew.access$700(MySpaceFragmentNew.this, responsefindbindstatusinfo.list);
            responsefindbindstatusinfo = responsefindbindstatusinfo.list.iterator();
            do
            {
                if (!responsefindbindstatusinfo.hasNext())
                {
                    break;
                }
                ThirdPartyUserInfo thirdpartyuserinfo = (ThirdPartyUserInfo)responsefindbindstatusinfo.next();
                switch (Integer.valueOf(thirdpartyuserinfo.thirdpartyId).intValue())
                {
                case 1: // '\001'
                    if (thirdpartyuserinfo.isExpired)
                    {
                        MySpaceFragmentNew.access$900(MySpaceFragmentNew.this).setImageResource(0x7f020555);
                    } else
                    {
                        MySpaceFragmentNew.access$900(MySpaceFragmentNew.this).setImageResource(0x7f020554);
                    }
                    break;

                case 2: // '\002'
                    if (thirdpartyuserinfo.isExpired)
                    {
                        MySpaceFragmentNew.access$800(MySpaceFragmentNew.this).setImageResource(0x7f02045c);
                    } else
                    {
                        MySpaceFragmentNew.access$800(MySpaceFragmentNew.this).setImageResource(0x7f02045b);
                    }
                    break;

                case 3: // '\003'
                    if (thirdpartyuserinfo.isExpired)
                    {
                        MySpaceFragmentNew.access$1000(MySpaceFragmentNew.this).setImageResource(0x7f0204db);
                    } else
                    {
                        MySpaceFragmentNew.access$1000(MySpaceFragmentNew.this).setImageResource(0x7f0204da);
                    }
                    break;
                }
            } while (true);
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((ResponseFindBindStatusInfo)obj);
    }

    protected void onPreExecute()
    {
        sp = SharedPreferencesUtil.getInstance(getActivity());
    }

    atusInfo()
    {
        this$0 = MySpaceFragmentNew.this;
        super();
        sp = null;
    }
}
