// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.tab;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.ad.NoPageAd;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

public class BannerAdLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "ting";
    private List mData;
    private String mName;

    public BannerAdLoader(Context context, String s)
    {
        super(context);
        mName = s;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((List)obj);
    }

    public void deliverResult(List list)
    {
        super.deliverResult(list);
        mData = list;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public List loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("name", mName);
        ((RequestParams) (obj)).put("version", ToolUtil.getAppVersion(getContext()));
        ((RequestParams) (obj)).put("network", NetworkUtils.getNetworkClass(getContext()));
        ((RequestParams) (obj)).put("operator", NetworkUtils.getOperator(getContext()));
        obj = f.a().a((new StringBuilder()).append(a.Q).append("ting").toString(), ((RequestParams) (obj)), null, null, false);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1)
        {
            try
            {
                if (!TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
                {
                    obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
                    if (((JSONObject) (obj)).getInteger("ret").intValue() == 0)
                    {
                        obj = ((JSONObject) (obj)).getString("data");
                        if (!TextUtils.isEmpty(((CharSequence) (obj))))
                        {
                            mData = NoPageAd.getListFromJSON(((String) (obj)));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        return mData;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
