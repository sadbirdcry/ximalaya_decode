// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.tab;

import android.app.ProgressDialog;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import java.io.File;

// Referenced classes of package com.ximalaya.ting.android.fragment.tab:
//            MySpaceFragmentNew

class pd extends MyAsyncTask
{

    String backgroundLogo;
    BaseModel bm;
    int flag;
    String largeLogo;
    String middleLogo;
    ProgressDialog pd;
    String smallLogo;
    final MySpaceFragmentNew this$0;

    protected transient Integer doInBackground(Object aobj[])
    {
        Object obj = null;
        flag = ((Integer)aobj[0]).intValue();
        String s;
        android.widget.ImageView imageview;
        if (flag == 2)
        {
            s = e.p;
        } else
        if (flag == 1)
        {
            s = e.o;
        } else
        {
            s = "";
        }
        if (flag == 2)
        {
            imageview = MySpaceFragmentNew.access$1600(MySpaceFragmentNew.this);
        } else
        if (flag == 1)
        {
            imageview = MySpaceFragmentNew.access$1700(MySpaceFragmentNew.this);
        } else
        {
            imageview = null;
        }
        try
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("uid", (new StringBuilder()).append(MySpaceFragmentNew.access$1800(MySpaceFragmentNew.this).uid).append("").toString());
            requestparams.put("token", MySpaceFragmentNew.access$1800(MySpaceFragmentNew.this).token);
            requestparams.put("image", (File)aobj[1]);
            aobj = f.a().b(s, requestparams, imageview, imageview);
        }
        // Misplaced declaration of an exception variable
        catch (Object aobj[])
        {
            aobj = null;
        }
        if (Utilities.isBlank(((String) (aobj))))
        {
            return Integer.valueOf(1);
        }
        try
        {
            aobj = JSON.parseObject(((String) (aobj)));
        }
        // Misplaced declaration of an exception variable
        catch (Object aobj[])
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
            aobj = obj;
        }
        if (aobj != null)
        {
            bm = new BaseModel();
            bm.ret = ((JSONObject) (aobj)).getInteger("ret").intValue();
            if (bm.ret != -1)
            {
                bm.msg = ((JSONObject) (aobj)).getString("msg");
                if (bm.ret == 0)
                {
                    largeLogo = ((JSONObject) (aobj)).getString("mobileLargeLogo");
                    smallLogo = ((JSONObject) (aobj)).getString("mobileSmallLogo");
                    middleLogo = ((JSONObject) (aobj)).getString("mobileMiddleLogo");
                    backgroundLogo = ((JSONObject) (aobj)).getString("backgroundLogo");
                }
            }
            return Integer.valueOf(3);
        } else
        {
            return Integer.valueOf(2);
        }
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (isAdded() && mCon != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (pd != null)
        {
            pd.cancel();
            pd = null;
        }
        if (integer.intValue() != 3)
        {
            break MISSING_BLOCK_LABEL_164;
        }
        if (bm == null || bm.ret != 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (flag == 2)
        {
            ImageManager2.from(mCon).displayImage(MySpaceFragmentNew.access$1600(MySpaceFragmentNew.this), backgroundLogo, -1, true);
            return;
        }
        if (flag != 1) goto _L1; else goto _L3
_L3:
        ImageManager2.from(mCon).displayImage(MySpaceFragmentNew.access$1700(MySpaceFragmentNew.this), middleLogo, -1, true);
        return;
        if (bm == null) goto _L1; else goto _L4
_L4:
        Toast.makeText(mCon, bm.msg, 1).show();
        return;
        android.content.Context context = mCon;
        StringBuilder stringbuilder = (new StringBuilder()).append("\u4FEE\u6539");
        if (flag == 2)
        {
            integer = "\u80CC\u666F\u56FE";
        } else
        {
            integer = "\u5934\u50CF";
        }
        Toast.makeText(context, stringbuilder.append(integer).append("\u5931\u8D25").toString(), 1).show();
        return;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        pd = new MyProgressDialog(getActivity());
        class _cls1
            implements android.content.DialogInterface.OnKeyListener
        {

            final MySpaceFragmentNew.UpdateHeadTask this$1;

            public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
            {
                if (i == 4)
                {
                    dialoginterface.dismiss();
                    return true;
                } else
                {
                    return false;
                }
            }

            _cls1()
            {
                this$1 = MySpaceFragmentNew.UpdateHeadTask.this;
                super();
            }
        }

        pd.setOnKeyListener(new _cls1());
        pd.setCanceledOnTouchOutside(true);
        pd.setTitle("\u4E0A\u4F20");
        ProgressDialog progressdialog = pd;
        StringBuilder stringbuilder = new StringBuilder();
        String s;
        if (flag == 2)
        {
            s = "\u80CC\u666F\u56FE";
        } else
        {
            s = "\u5934\u50CF";
        }
        progressdialog.setMessage(stringbuilder.append(s).append("\u4E0A\u4F20\u4E2D").toString());
        pd.show();
    }

    _cls1()
    {
        this$0 = MySpaceFragmentNew.this;
        super();
        pd = null;
    }
}
