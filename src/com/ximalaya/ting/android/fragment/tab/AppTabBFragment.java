// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.tab;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.download.DownloadFragmentGroup;
import com.ximalaya.ting.android.util.DataCollectUtil;

public class AppTabBFragment extends BaseFragment
{

    private RelativeLayout container_layout;
    private View decorView;
    public DownloadFragmentGroup downloadFragmentGroup;
    private FragmentManager manager;

    public AppTabBFragment()
    {
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (downloadFragmentGroup == null)
        {
            return;
        } else
        {
            downloadFragmentGroup.onActivityResult(i, j, intent);
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        container_layout = (RelativeLayout)layoutinflater.inflate(0x7f030042, viewgroup, false);
        manager = getChildFragmentManager();
        downloadFragmentGroup = new DownloadFragmentGroup(manager);
        downloadFragmentGroup.setArguments(getArguments());
        pushFragments(downloadFragmentGroup);
        DataCollectUtil.bindViewIdDataToView(container_layout);
        return container_layout;
    }

    public void onPause()
    {
        super.onPause();
        if (downloadFragmentGroup != null)
        {
            downloadFragmentGroup.onPause();
        }
    }

    public void onResume()
    {
        super.onResume();
        setPlayPath("play_download");
        if (downloadFragmentGroup != null)
        {
            downloadFragmentGroup.onResume();
        }
    }

    public void pushFragments(Fragment fragment)
    {
        FragmentTransaction fragmenttransaction = manager.beginTransaction();
        fragmenttransaction.add(0x7f0a016d, fragment);
        fragmenttransaction.addToBackStack(null);
        fragmenttransaction.commitAllowingStateLoss();
    }

    public void setCurrPage(int i)
    {
        if (downloadFragmentGroup != null)
        {
            downloadFragmentGroup.setCurrentFragment(i, false);
        }
    }
}
