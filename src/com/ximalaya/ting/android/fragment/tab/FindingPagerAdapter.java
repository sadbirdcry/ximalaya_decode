// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.tab;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.SparseArray;
import com.ximalaya.ting.android.fragment.activities.ActivityListFragment;
import com.ximalaya.ting.android.fragment.finding2.rank.RankFragment;
import com.ximalaya.ting.android.fragment.finding2.recommend.RecommendFragmentNew;
import com.ximalaya.ting.android.fragment.findings.CategoryFragment;
import com.ximalaya.ting.android.fragment.findings.ComingSoonFragment;
import com.ximalaya.ting.android.fragment.findings.FindingHotStationCategoryFragment;
import com.ximalaya.ting.android.fragment.livefm.LiveFMFragment;
import com.ximalaya.ting.android.fragment.subject.SubjectListFragmentNew;
import com.ximalaya.ting.android.fragment.web.WebFragment;
import com.ximalaya.ting.android.fragment.zone.ZoneEntryFragmentNew;
import com.ximalaya.ting.android.model.finding.FindingTabModel;
import java.lang.ref.WeakReference;
import java.util.List;

public class FindingPagerAdapter extends FragmentStatePagerAdapter
{

    private List mData;
    private SparseArray mFragments;

    public FindingPagerAdapter(FragmentManager fragmentmanager, List list)
    {
        super(fragmentmanager);
        mData = list;
        int i;
        if (list == null)
        {
            i = 0;
        } else
        {
            i = list.size();
        }
        mFragments = new SparseArray(i);
    }

    public int getCategoryPosition()
    {
        if (mData != null && !mData.isEmpty()) goto _L2; else goto _L1
_L1:
        int j = -1;
_L4:
        return j;
_L2:
        int i = 0;
label0:
        do
        {
label1:
            {
                if (i == mData.size())
                {
                    break label1;
                }
                j = i;
                if (2 == ((FindingTabModel)mData.get(i)).getType())
                {
                    break label0;
                }
                i++;
            }
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
        return -1;
    }

    public int getCount()
    {
        return mData.size();
    }

    public Fragment getFragmentAt(int i)
    {
        WeakReference weakreference = (WeakReference)mFragments.get(i);
        if (weakreference != null)
        {
            return (Fragment)weakreference.get();
        } else
        {
            return null;
        }
    }

    public Fragment getFragmentByType(int i)
    {
        if (mData == null) goto _L2; else goto _L1
_L1:
        int j = 0;
_L5:
        if (j >= mData.size())
        {
            break MISSING_BLOCK_LABEL_62;
        }
        if (((FindingTabModel)mData.get(j)).getType() != i) goto _L4; else goto _L3
_L3:
        if (j != -1)
        {
            return getFragmentAt(j);
        }
          goto _L2
_L4:
        j++;
          goto _L5
_L2:
        return null;
        j = -1;
          goto _L3
    }

    public Fragment getItem(int i)
    {
        Object obj = (FindingTabModel)mData.get(i);
        ((FindingTabModel) (obj)).getType();
        JVM INSTR tableswitch 2 10: default 68
    //                   2 117
    //                   3 159
    //                   4 98
    //                   5 124
    //                   6 135
    //                   7 105
    //                   8 143
    //                   9 151
    //                   10 90;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10
_L1:
        obj = ComingSoonFragment.newInstance();
_L12:
        mFragments.put(i, new WeakReference(obj));
        return ((Fragment) (obj));
_L10:
        obj = ZoneEntryFragmentNew.newInstance(true);
        continue; /* Loop/switch isn't completed */
_L4:
        obj = RankFragment.newInstance();
        continue; /* Loop/switch isn't completed */
_L7:
        obj = WebFragment.newInstance(((FindingTabModel) (obj)).getUrl(), false);
        continue; /* Loop/switch isn't completed */
_L2:
        obj = CategoryFragment.newInstance();
        continue; /* Loop/switch isn't completed */
_L5:
        obj = new LiveFMFragment();
        continue; /* Loop/switch isn't completed */
_L6:
        obj = FindingHotStationCategoryFragment.newInstance(true);
        continue; /* Loop/switch isn't completed */
_L8:
        obj = SubjectListFragmentNew.newInstance(true);
        continue; /* Loop/switch isn't completed */
_L9:
        obj = ActivityListFragment.newInstance(true);
        continue; /* Loop/switch isn't completed */
_L3:
        obj = RecommendFragmentNew.newInstance();
        if (true) goto _L12; else goto _L11
_L11:
    }

    public int getLivePostion()
    {
        if (mData != null && !mData.isEmpty()) goto _L2; else goto _L1
_L1:
        int j = -1;
_L4:
        return j;
_L2:
        int i = 0;
label0:
        do
        {
label1:
            {
                if (i == mData.size())
                {
                    break label1;
                }
                j = i;
                if (5 == ((FindingTabModel)mData.get(i)).getType())
                {
                    break label0;
                }
                i++;
            }
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
        return -1;
    }

    public CharSequence getPageTitle(int i)
    {
        return ((FindingTabModel)mData.get(i)).getTitle();
    }

    public Parcelable saveState()
    {
        return null;
    }
}
