// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.tab;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.finding2.recommend.RecommendFragmentNew;
import com.ximalaya.ting.android.fragment.search.WordAssociatedFragment;
import com.ximalaya.ting.android.model.finding.FindingTabModel;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.tab:
//            FindingPagerAdapter

public class FindingFragmentV3 extends BaseFragment
    implements android.view.View.OnClickListener
{
    static class TabGridViewAdapter extends BaseAdapter
    {

        private Context mContext;
        private List mData;
        private int mSelectedItem;

        public int getCount()
        {
            if (mData == null)
            {
                return 0;
            } else
            {
                return mData.size();
            }
        }

        public Object getItem(int i)
        {
            return mData.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            if (view == null)
            {
                view = new TextView(mContext);
                view.setGravity(17);
                view.setLayoutParams(new android.widget.AbsListView.LayoutParams(Utilities.dip2px(mContext, 70F), Utilities.dip2px(mContext, 28F)));
            } else
            {
                view = (TextView)view;
            }
            if (i == mSelectedItem)
            {
                view.setTextColor(Color.parseColor("#ffffff"));
                view.setBackgroundColor(Color.parseColor("#f86442"));
            } else
            {
                view.setTextColor(Color.parseColor("#707788"));
                view.setBackgroundColor(Color.parseColor("#f5f8fa"));
            }
            view.setText(((FindingTabModel)mData.get(i)).getTitle());
            return view;
        }

        public void setData(List list)
        {
            mData = list;
            notifyDataSetChanged();
        }

        public void setSelectedItem(int i)
        {
            mSelectedItem = i;
            notifyDataSetChanged();
        }

        public TabGridViewAdapter(Context context, List list)
        {
            mData = list;
            mContext = context;
        }
    }


    private static final int LOAD_TAB_FROM_LOCAL = 2;
    private static final int LOAD_TAB_FROM_SERVER = 1;
    public static String recommendLiveEntryText = null;
    private FindingPagerAdapter adapter;
    private TabGridViewAdapter mAllTabAdapter;
    private android.support.v4.app.LoaderManager.LoaderCallbacks mLoadTabCallback;
    private List mMata;
    private int mPrimaryPosition;
    private TextView mTopInfoBar;
    private ViewPager pager;
    private PagerSlidingTabStrip tabs;

    public FindingFragmentV3()
    {
        mLoadTabCallback = new _cls1();
    }

    private void buildTabs(List list)
    {
        mMata = list;
        if (list == null || list.size() <= 0) goto _L2; else goto _L1
_L1:
        if (!TingMediaPlayer.getTingMediaPlayer(getActivity().getApplicationContext()).isUseSystemPlayer()) goto _L4; else goto _L3
_L3:
        int i = 0;
_L7:
        if (i >= list.size())
        {
            break MISSING_BLOCK_LABEL_180;
        }
        if (((FindingTabModel)list.get(i)).getType() != 5) goto _L6; else goto _L5
_L5:
        if (i >= 0)
        {
            list.remove(i);
        }
_L4:
        adapter = new FindingPagerAdapter(getChildFragmentManager(), list);
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);
        if (mPrimaryPosition > 0 && mPrimaryPosition < list.size())
        {
            pager.setCurrentItem(mPrimaryPosition, false);
            tabs.a(mPrimaryPosition);
        }
        mAllTabAdapter = new TabGridViewAdapter(getActivity(), list);
_L2:
        return;
_L6:
        i++;
          goto _L7
        i = -1;
          goto _L5
    }

    private void clearViewRef()
    {
        if (pager != null)
        {
            pager.setAdapter(null);
            pager = null;
        }
        adapter = null;
        tabs = null;
    }

    private static boolean isTabsEqual(List list, List list1)
    {
        if (list == null || list1 == null || list.size() == 0 || list.size() != list1.size()) goto _L2; else goto _L1
_L1:
        int i = 0;
_L5:
        boolean flag;
        if (i == list1.size())
        {
            break; /* Loop/switch isn't completed */
        }
        int j = 0;
        flag = false;
        for (; j != list.size(); j++)
        {
            if (((FindingTabModel)list.get(j)).equals(list1.get(i)))
            {
                flag = true;
            }
        }

        if (flag) goto _L3; else goto _L2
_L2:
        return false;
_L3:
        i++;
        if (true) goto _L5; else goto _L4
_L4:
        return true;
    }

    public static FindingFragmentV3 newInstance()
    {
        return new FindingFragmentV3();
    }

    private void saveState()
    {
        if (pager != null)
        {
            mPrimaryPosition = pager.getCurrentItem();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        tabs.setShouldExpand(true);
        tabs.setDividerColor(0);
        tabs.setIndicatorColor(Color.parseColor("#f86442"));
        tabs.setUnderlineColor(Color.parseColor("#dcdcdc"));
        tabs.setUnderlineHeight(0);
        tabs.setDividerPadding(0);
        tabs.setDeactivateTextColor(Color.parseColor("#666666"));
        tabs.setActivateTextColor(Color.parseColor("#f86442"));
        tabs.setTabSwitch(true);
        tabs.setTextSize(15);
        tabs.setIndicatorHeight(Utilities.dip2px(getActivity(), 3F));
        tabs.setTabPaddingLeftRight(Utilities.dip2px(getActivity(), 17F));
        findViewById(0x7f0a02db).setOnClickListener(this);
        findViewById(0x7f0a0359).setOnClickListener(this);
        getLoaderManager().initLoader(2, null, mLoadTabCallback);
    }

    public boolean onBackPressed()
    {
        if (adapter != null)
        {
            android.support.v4.app.Fragment fragment = adapter.getFragmentAt(pager.getCurrentItem());
            if (fragment != null && (fragment instanceof BaseFragment))
            {
                return ((BaseFragment)fragment).onBackPressed();
            }
        }
        return super.onBackPressed();
    }

    public void onClick(View view)
    {
        Bundle bundle;
        switch (view.getId())
        {
        case 2131362649: 
        default:
            return;

        case 2131362523: 
            bundle = new Bundle();
            break;
        }
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/search/WordAssociatedFragment, bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300ba, viewgroup, false);
        tabs = (PagerSlidingTabStrip)layoutinflater.findViewById(0x7f0a02dc);
        pager = (ViewPager)layoutinflater.findViewById(0x7f0a0021);
        mTopInfoBar = (TextView)layoutinflater.findViewById(0x7f0a02de);
        DataCollectUtil.bindViewIdDataToView(layoutinflater);
        return layoutinflater;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        saveState();
        clearViewRef();
    }

    public void switchToCategoryFragment()
    {
        if (pager != null && adapter != null)
        {
            int i = adapter.getCategoryPosition();
            if (i >= 0)
            {
                pager.setCurrentItem(i, true);
            }
        }
    }

    public void switchToLiveFragment(String s)
    {
        if (pager != null && adapter != null)
        {
            int i = adapter.getLivePostion();
            if (i >= 0)
            {
                recommendLiveEntryText = s;
                pager.setCurrentItem(i, true);
            }
        }
    }

    public void updateGuessLikeData()
    {
        if (adapter != null)
        {
            android.support.v4.app.Fragment fragment = adapter.getFragmentByType(3);
            if (fragment instanceof RecommendFragmentNew)
            {
                ((RecommendFragmentNew)fragment).forceUpdateGuessLike();
            }
        }
    }






    private class _cls1
        implements android.support.v4.app.LoaderManager.LoaderCallbacks
    {

        final FindingFragmentV3 this$0;

        public Loader onCreateLoader(int i, Bundle bundle)
        {
            switch (i)
            {
            default:
                return new c(getActivity());

            case 2: // '\002'
                return new c(getActivity(), true);

            case 1: // '\001'
                return new c(getActivity());
            }
        }

        public volatile void onLoadFinished(Loader loader, Object obj)
        {
            onLoadFinished(loader, (List)obj);
        }

        public void onLoadFinished(Loader loader, List list)
        {
            if (loader.getId() == 2)
            {
                getLoaderManager().restartLoader(1, null, mLoadTabCallback);
            } else
            if (FindingFragmentV3.isTabsEqual(mMata, list))
            {
                return;
            }
            if (list != null)
            {
                buildTabs(list);
            }
        }

        public void onLoaderReset(Loader loader)
        {
        }

        _cls1()
        {
            this$0 = FindingFragmentV3.this;
            super();
        }
    }

}
