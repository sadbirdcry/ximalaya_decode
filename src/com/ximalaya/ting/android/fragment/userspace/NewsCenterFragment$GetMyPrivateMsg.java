// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.widget.LinearLayout;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.adapter.PrivateMsgAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.message.PrivateMsgModelList;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            NewsCenterFragment

class this._cls0 extends MyAsyncTask
{

    PrivateMsgModelList pmml;
    final NewsCenterFragment this$0;

    protected transient Integer doInBackground(String as[])
    {
        as = new RequestParams();
        as.put("uid", (new StringBuilder()).append(loginInfoModel.uid).append("").toString());
        as.put("token", loginInfoModel.token);
        as.put("pageSize", (new StringBuilder()).append(NewsCenterFragment.access$400(NewsCenterFragment.this)).append("").toString());
        as.put("isDown", (new StringBuilder()).append(NewsCenterFragment.access$500(NewsCenterFragment.this)).append("").toString());
        as.put("key", NewsCenterFragment.access$200(NewsCenterFragment.this));
        as = f.a().b(e.i, as, NewsCenterFragment.access$300(NewsCenterFragment.this), NewsCenterFragment.access$300(NewsCenterFragment.this));
        if (Utilities.isBlank(as))
        {
            return Integer.valueOf(1);
        }
        try
        {
            pmml = (PrivateMsgModelList)JSON.parseObject(as, com/ximalaya/ting/android/model/message/PrivateMsgModelList);
            if (pmml != null)
            {
                return Integer.valueOf(3);
            }
        }
        // Misplaced declaration of an exception variable
        catch (String as[])
        {
            return Integer.valueOf(2);
        }
        return Integer.valueOf(2);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((String[])aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (canGoon())
        {
            if (NewsCenterFragment.access$300(NewsCenterFragment.this).isRefreshing())
            {
                NewsCenterFragment.access$300(NewsCenterFragment.this).onRefreshComplete();
            }
            if (integer.intValue() == 3)
            {
                if (NewsCenterFragment.access$100(NewsCenterFragment.this) == null)
                {
                    NewsCenterFragment.access$102(NewsCenterFragment.this, new PrivateMsgAdapter(NewsCenterFragment.this));
                    long l = getAnimationLeftTime();
                    integer = NewsCenterFragment.access$300(NewsCenterFragment.this);
                    class _cls1
                        implements Runnable
                    {

                        final NewsCenterFragment.GetMyPrivateMsg this$1;

                        public void run()
                        {
                            NewsCenterFragment.access$300(this$0).setAdapter(NewsCenterFragment.access$100(this$0));
                        }

            _cls1()
            {
                this$1 = NewsCenterFragment.GetMyPrivateMsg.this;
                super();
            }
                    }

                    _cls1 _lcls1 = new _cls1();
                    if (l <= 0L)
                    {
                        l = 0L;
                    }
                    integer.postDelayed(_lcls1, l);
                    NewsCenterFragment.access$600(NewsCenterFragment.this).setVisibility(8);
                    if (pmml != null)
                    {
                        NewsCenterFragment.access$100(NewsCenterFragment.this).pmmList = pmml.list;
                        return;
                    }
                } else
                if (NewsCenterFragment.access$100(NewsCenterFragment.this) != null && NewsCenterFragment.access$200(NewsCenterFragment.this).equals("0"))
                {
                    if (pmml != null)
                    {
                        NewsCenterFragment.access$100(NewsCenterFragment.this).pmmList = pmml.list;
                        NewsCenterFragment.access$100(NewsCenterFragment.this).notifyDataSetChanged();
                        return;
                    }
                } else
                {
                    NewsCenterFragment.access$100(NewsCenterFragment.this).pmmList.remove(NewsCenterFragment.access$100(NewsCenterFragment.this).pmmList.size() - 1);
                    if (pmml.list != null)
                    {
                        NewsCenterFragment.access$100(NewsCenterFragment.this).pmmList.addAll(pmml.list);
                    }
                    NewsCenterFragment.access$100(NewsCenterFragment.this).notifyDataSetChanged();
                    return;
                }
            } else
            {
                if (NewsCenterFragment.access$100(NewsCenterFragment.this) == null)
                {
                    NewsCenterFragment.access$700(NewsCenterFragment.this);
                    return;
                }
                if (NewsCenterFragment.access$100(NewsCenterFragment.this) != null && NewsCenterFragment.access$200(NewsCenterFragment.this).equals("0"))
                {
                    NewsCenterFragment.access$800(NewsCenterFragment.this, getString(0x7f09009b));
                    return;
                } else
                {
                    NewsCenterFragment.access$100(NewsCenterFragment.this).pmmList.remove(NewsCenterFragment.access$100(NewsCenterFragment.this).pmmList.size() - 1);
                    NewsCenterFragment.access$800(NewsCenterFragment.this, getString(0x7f09009b));
                    return;
                }
            }
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    _cls1()
    {
        this$0 = NewsCenterFragment.this;
        super();
    }
}
