// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.h;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            SoundListFragmentNew

class this._cls1 extends h
{

    final this._cls1 this$1;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, mListView);
    }

    public void onFinish()
    {
    }

    public void onNetError(int i, String s)
    {
        cess._mth4102(this._cls1.this, true);
    }

    public void onStart()
    {
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            cess._mth4102(this._cls1.this, true);
            return;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null || s.getIntValue("ret") != 0)
        {
            cess._mth4102(this._cls1.this, true);
            return;
        }
        int i = s.getIntValue("processState");
        int j = s.getIntValue("status");
        cess._mth4200(this._cls1.this).processState = i;
        cess._mth4200(this._cls1.this).status = j;
        double d;
        try
        {
            d = s.getDoubleValue("duration");
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            d = 0.0D;
        }
        if (d > 0.0D)
        {
            cess._mth4200(this._cls1.this).duration = d;
        }
        if (i == 2)
        {
            cess._mth4200(this._cls1.this).playUrl32 = s.getString("playUrl32");
            cess._mth4200(this._cls1.this).playUrl64 = s.getString("playUrl64");
            if (j == 1 || j == 2)
            {
                cess._mth4102(this._cls1.this, false);
                return;
            } else
            {
                cess._mth4102(this._cls1.this, true);
                return;
            }
        }
        if (i == 3)
        {
            cess._mth4102(this._cls1.this, false);
            return;
        } else
        {
            cess._mth4102(this._cls1.this, true);
            return;
        }
    }

    ()
    {
        this$1 = this._cls1.this;
        super();
    }
}
