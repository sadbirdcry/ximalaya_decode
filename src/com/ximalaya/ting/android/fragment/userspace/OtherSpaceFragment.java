// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.activity.homepage.TalkViewAct;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.activity.share.BaseShareDialog;
import com.ximalaya.ting.android.adapter.OtherSpaceAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.zone.ZoneFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.album.AlbumCollection;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.model.personal_info.HomePageModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.model.sound.RecordingModelList;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            MyAttentionFragment, MyLikedSoundsFragment

public class OtherSpaceFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{
    public static final class FooterView extends Enum
    {

        private static final FooterView $VALUES[];
        public static final FooterView HIDE_ALL;
        public static final FooterView LOADING;
        public static final FooterView MORE;
        public static final FooterView NO_CONNECTION;
        public static final FooterView NO_DATA;
        public static final FooterView REQUEST_FAILED;

        public static FooterView valueOf(String s)
        {
            return (FooterView)Enum.valueOf(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment$FooterView, s);
        }

        public static FooterView[] values()
        {
            return (FooterView[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterView("MORE", 0);
            LOADING = new FooterView("LOADING", 1);
            NO_CONNECTION = new FooterView("NO_CONNECTION", 2);
            HIDE_ALL = new FooterView("HIDE_ALL", 3);
            NO_DATA = new FooterView("NO_DATA", 4);
            REQUEST_FAILED = new FooterView("REQUEST_FAILED", 5);
            $VALUES = (new FooterView[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA, REQUEST_FAILED
            });
        }

        private FooterView(String s, int i)
        {
            super(s, i);
        }
    }

    class GetHomePageInfo extends MyAsyncTask
    {

        HomePageModel homeModel;
        final OtherSpaceFragment this$0;

        protected transient Integer doInBackground(String as[])
        {
            Object obj = new RequestParams();
            ((RequestParams) (obj)).put("toUid", (new StringBuilder()).append(toUid).append("").toString());
            as = null;
            obj = f.a().a(e.F, ((RequestParams) (obj)), fragmentBaseContainerView, headerView, false);
            if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1)
            {
                as = ((com.ximalaya.ting.android.b.n.a) (obj)).a;
            }
            if (Utilities.isBlank(as))
            {
                return Integer.valueOf(1);
            }
            homeModel = (HomePageModel)JSON.parseObject(as, com/ximalaya/ting/android/model/personal_info/HomePageModel);
            if (mInfoModel == null)
            {
                break MISSING_BLOCK_LABEL_272;
            }
            Object obj1 = new RequestParams();
            ((RequestParams) (obj1)).put("uids", (new StringBuilder()).append(homeModel.uid).append("").toString());
            obj1 = f.a().a(e.X, ((RequestParams) (obj1)), headerView, headerView, false);
            if (((com.ximalaya.ting.android.b.n.a) (obj1)).b == 1)
            {
                as = ((com.ximalaya.ting.android.b.n.a) (obj1)).a;
            }
            if (!Utilities.isNotBlank(as))
            {
                break MISSING_BLOCK_LABEL_272;
            }
            as = JSON.parseObject(as);
            if (as == null)
            {
                break MISSING_BLOCK_LABEL_272;
            }
            if (as.getIntValue("ret") != 0)
            {
                break MISSING_BLOCK_LABEL_272;
            }
            as = as.getJSONArray("data");
            if (as != null)
            {
                try
                {
                    if (as.size() > 0)
                    {
                        as = as.getJSONObject(0);
                        if (homeModel.uid == as.getLongValue("uid"))
                        {
                            homeModel.isFollowed = as.getBooleanValue("isFollow");
                        }
                    }
                }
                // Misplaced declaration of an exception variable
                catch (String as[])
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(as.getMessage()).append(Logger.getLineInfo()).toString());
                }
            }
            if (homeModel == null)
            {
                return Integer.valueOf(2);
            } else
            {
                return Integer.valueOf(3);
            }
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (canGoon())
            {
                progressBar.setVisibility(8);
                if (integer.intValue() == 3 && homeModel != null)
                {
                    if (homeModel.ret == 0)
                    {
                        initHomePage(homeModel);
                        return;
                    } else
                    {
                        Toast.makeText(mCon, homeModel.msg, 0).show();
                        return;
                    }
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            progressBar.setVisibility(0);
        }

        GetHomePageInfo()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }

    class GetMySoundList extends MyAsyncTask
    {

        List rm;
        final OtherSpaceFragment this$0;

        protected transient Integer doInBackground(String as[])
        {
            int i;
            as = null;
            i = 0;
            isLoadingNetSound = true;
            Object obj = e.B;
            obj = (new StringBuilder()).append(((String) (obj))).append("/").append(toUid).append("/").append(rmList.pageId).append("/").append(rmList.pageSize).toString();
            obj = f.a().a(((String) (obj)), null, fragmentBaseContainerView, mListview, false);
            if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1)
            {
                as = ((com.ximalaya.ting.android.b.n.a) (obj)).a;
            }
            if (Utilities.isEmpty(as))
            {
                return Integer.valueOf(1);
            }
            Object obj1;
            int j;
            obj1 = JSON.parseObject(as);
            j = ((JSONObject) (obj1)).getIntValue("ret");
            if (j != 0)
            {
                break MISSING_BLOCK_LABEL_576;
            }
            RecordingModelList recordingmodellist = rmList;
            recordingmodellist;
            JVM INSTR monitorenter ;
            rmList.maxPageId = ((JSONObject) (obj1)).getInteger("maxPageId").intValue();
            rmList.totalCount = ((JSONObject) (obj1)).getInteger("totalCount").intValue();
            rm = JSON.parseArray(((JSONObject) (obj1)).getString("list"), com/ximalaya/ting/android/model/sound/RecordingModel);
            if (rm == null || rm.size() == 0)
            {
                return Integer.valueOf(3);
            }
            break MISSING_BLOCK_LABEL_284;
            as;
            recordingmodellist;
            JVM INSTR monitorexit ;
            try
            {
                throw as;
            }
            // Misplaced declaration of an exception variable
            catch (String as[])
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(as.getMessage()).append(Logger.getLineInfo()).toString());
            }
            return Integer.valueOf(1);
            if (UserInfoMannage.getInstance().getUser() == null) goto _L2; else goto _L1
_L1:
            obj1 = new StringBuffer();
            RecordingModel recordingmodel;
            for (Iterator iterator = rm.iterator(); iterator.hasNext(); ((StringBuffer) (obj1)).append((new StringBuilder()).append(recordingmodel.trackId).append(",").toString()))
            {
                recordingmodel = (RecordingModel)iterator.next();
            }

            obj1 = ((StringBuffer) (obj1)).substring(0, ((StringBuffer) (obj1)).length() - 1);
            RequestParams requestparams = new RequestParams();
            requestparams.put("trackIds", ((String) (obj1)));
            obj1 = f.a().a(e.W, requestparams, mListview, mListview, false);
            if (((com.ximalaya.ting.android.b.n.a) (obj1)).b == 1)
            {
                as = ((com.ximalaya.ting.android.b.n.a) (obj1)).a;
            }
            as = JSON.parseObject(as);
            if (as.getIntValue("ret") != 0) goto _L2; else goto _L3
_L3:
            as = as.getJSONArray("data");
            if (as == null) goto _L2; else goto _L4
_L4:
            if (i >= as.size())
            {
                break; /* Loop/switch isn't completed */
            }
            obj1 = new RecordingModel();
            JSONObject jsonobject = as.getJSONObject(i);
            obj1.trackId = jsonobject.getLongValue("trackId");
            ((RecordingModel)rm.get(rm.indexOf(obj1))).isRelay = jsonobject.getBooleanValue("isRelay");
            ((RecordingModel)rm.get(rm.indexOf(obj1))).isLike = jsonobject.getBooleanValue("isLike");
            i++;
            if (true) goto _L4; else goto _L2
_L2:
            recordingmodellist;
            JVM INSTR monitorexit ;
            return Integer.valueOf(3);
            synchronized (rmList)
            {
                rmList.msg = ((JSONObject) (obj1)).getString("msg");
                rmList.ret = j;
            }
            return Integer.valueOf(2);
            exception;
            as;
            JVM INSTR monitorexit ;
            throw exception;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            isLoadingNetSound = false;
            if (canGoon())
            {
                showFooterView(FooterView.HIDE_ALL);
                switch (integer.intValue())
                {
                default:
                    return;

                case 1: // '\001'
                    Toast.makeText(mCon, "\u8BF7\u6C42\u58F0\u97F3\u4FE1\u606F\u6709\u8BEF,\u8BF7\u91CD\u8BD5", 0).show();
                    otherSpaceAdapter.notifyDataSetChanged();
                    return;

                case 2: // '\002'
                    Toast.makeText(mCon, rmList.msg, 0).show();
                    otherSpaceAdapter.notifyDataSetChanged();
                    return;

                case 3: // '\003'
                    break;
                }
                if (rm != null && otherSpaceAdapter != null)
                {
                    long l = getAnimationLeftTime();
                    integer = mListview;
                    class _cls1
                        implements Runnable
                    {

                        final GetMySoundList this$1;

                        public void run()
                        {
                            if (!canGoon())
                            {
                                return;
                            }
                            HashMap hashmap;
                            if (otherSpaceAdapter.getData() == null)
                            {
                                otherSpaceAdapter.setData(rm);
                            } else
                            {
                                otherSpaceAdapter.getData().addAll(rm);
                            }
                            otherSpaceAdapter.setDataSource(e.B);
                            otherSpaceAdapter.setPageId(rmList.pageId);
                            hashmap = new HashMap();
                            hashmap.put("uid", (new StringBuilder()).append(toUid).append("").toString());
                            otherSpaceAdapter.setParams(hashmap);
                            otherSpaceAdapter.notifyDataSetChanged();
                        }

                _cls1()
                {
                    this$1 = GetMySoundList.this;
                    super();
                }
                    }

                    _cls1 _lcls1 = new _cls1();
                    if (l <= 0L)
                    {
                        l = 0L;
                    }
                    integer.postDelayed(_lcls1, l);
                    return;
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        GetMySoundList()
        {
            this$0 = OtherSpaceFragment.this;
            super();
            rm = null;
        }
    }

    class GetOtherAlbumList extends MyAsyncTask
    {

        AlbumCollection ac;
        final OtherSpaceFragment this$0;

        protected transient Integer doInBackground(String as[])
        {
            Context context = mCon;
            long l;
            if (mInfoModel == null)
            {
                l = 0L;
            } else
            {
                l = mInfoModel.uid;
            }
            if (mInfoModel == null)
            {
                as = null;
            } else
            {
                as = mInfoModel.token;
            }
            as = CommonRequest.doGetMyOrOtherAlbums(context, l, as, toUid, pageId_album, 2, fragmentBaseContainerView, include_Album);
            if (Utilities.isNotBlank(as))
            {
                try
                {
                    ac = (AlbumCollection)JSON.parseObject(as, com/ximalaya/ting/android/model/album/AlbumCollection);
                }
                // Misplaced declaration of an exception variable
                catch (String as[])
                {
                    Logger.e(as);
                }
                if (ac == null)
                {
                    return Integer.valueOf(2);
                } else
                {
                    return Integer.valueOf(3);
                }
            } else
            {
                return Integer.valueOf(1);
            }
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (canGoon()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            switch (integer.intValue())
            {
            default:
                break;

            case 1: // '\001'
            case 2: // '\002'
                break;

            case 3: // '\003'
                break;
            }
            break; /* Loop/switch isn't completed */
            return;
            if (true) goto _L1; else goto _L3
_L3:
            if (ac.ret != 0) goto _L1; else goto _L4
_L4:
            long l = getAnimationLeftTime();
            integer = mListview;
            class _cls1
                implements Runnable
            {

                final GetOtherAlbumList this$1;

                public void run()
                {
                    if (!canGoon())
                    {
                        return;
                    } else
                    {
                        initAlbumTowItem(ac.list, ac.totalCount);
                        return;
                    }
                }

                _cls1()
                {
                    this$1 = GetOtherAlbumList.this;
                    super();
                }
            }

            _cls1 _lcls1 = new _cls1();
            if (l <= 0L)
            {
                l = 0L;
            }
            integer.postDelayed(_lcls1, l);
            return;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            showFooterView(FooterView.LOADING);
        }

        GetOtherAlbumList()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    public static final String FROM = "from";
    public static final int FROM_FOCUS = 3;
    public static final int FROM_HOT_ANCHOR = 1;
    public static final int FROM_SEARCH = 2;
    private static final String TAG = "OtherSpaceFragment";
    private final int NickNameViewMoveDistance = 85;
    private ImageView arrowImg;
    private ImageView bacImg;
    private ImageView focusOrCancelBtn;
    private ImageView headImg;
    private View headerView;
    private View include_Album;
    private TextView introAnimTxt;
    private TextView introTxt;
    private boolean isAniamtionFinished;
    private boolean isHeadImgShowing;
    private boolean isLoadingNetSound;
    private View mFooterViewLoading;
    public int mFrom;
    private HomePageModel mHomeModel;
    private LoginInfoModel mInfoModel;
    private BounceListView mListview;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private RadioSound mRadioModel;
    private TextView nicknameTxt;
    private OtherSpaceAdapter otherSpaceAdapter;
    private int pageId_album;
    private ImageView playIcon;
    private TextView playNumTxt;
    private ImageView privateMsgImg;
    private View programDividerView;
    private TextView programNameTxt;
    private View programView;
    private ProgressBar progressBar;
    private ImageView radioCover;
    private RecordingModelList rmList;
    private ImageView shareImg;
    private TextView soundsCountTxt;
    private long toUid;
    private ImageView zoneBtnIv;

    public OtherSpaceFragment()
    {
        rmList = new RecordingModelList();
        isLoadingNetSound = false;
        isHeadImgShowing = true;
        pageId_album = 1;
        isAniamtionFinished = true;
    }

    private void doCollect(final Context context, final AlbumModel model, final View container)
    {
        if (UserInfoMannage.hasLogined())
        {
            String s;
            RequestParams requestparams;
            if (model.isFavorite)
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.albumId).toString());
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(container), new _cls17());
        } else
        if (AlbumModelManage.getInstance().ensureLocalCollectAllow(context, model, container))
        {
            (new _cls18()).myexec(new Void[0]);
            return;
        }
    }

    private void findViews()
    {
        mListview = (BounceListView)findViewById(0x7f0a00c7);
        headerView = LayoutInflater.from(mCon).inflate(0x7f030185, null);
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(mCon).inflate(0x7f0301fb, null);
        progressBar = (ProgressBar)findViewById(0x7f0a0091);
        soundsCountTxt = (TextView)headerView.findViewById(0x7f0a05f0);
        include_Album = headerView.findViewById(0x7f0a05ef);
        arrowImg = (ImageView)headerView.findViewById(0x7f0a05e2);
        nicknameTxt = (TextView)headerView.findViewById(0x7f0a05a7);
        introTxt = (TextView)headerView.findViewById(0x7f0a05aa);
        zoneBtnIv = (ImageView)headerView.findViewById(0x7f0a05e6);
        privateMsgImg = (ImageView)headerView.findViewById(0x7f0a05e4);
        shareImg = (ImageView)headerView.findViewById(0x7f0a05e5);
        headerView.findViewById(0x7f0a007b).setOnClickListener(this);
        headerView.findViewById(0x7f0a05e8).setOnClickListener(this);
        headerView.findViewById(0x7f0a05ea).setOnClickListener(this);
        headerView.findViewById(0x7f0a05eb).setOnClickListener(this);
        headerView.findViewById(0x7f0a05e7).setOnClickListener(this);
        headerView.findViewById(0x7f0a05a4).setOnClickListener(this);
        shareImg.setOnClickListener(this);
        privateMsgImg.setOnClickListener(this);
        zoneBtnIv.setOnClickListener(this);
        programDividerView = headerView.findViewById(0x7f0a05ed);
        programView = headerView.findViewById(0x7f0a05ee);
        radioCover = (ImageView)programView.findViewById(0x7f0a0177);
        playIcon = (ImageView)programView.findViewById(0x7f0a0022);
        programNameTxt = (TextView)programView.findViewById(0x7f0a0218);
        playNumTxt = (TextView)programView.findViewById(0x7f0a021a);
    }

    private int getPlaySource()
    {
        switch (mFrom)
        {
        default:
            return 4;

        case 1: // '\001'
            return 21;

        case 2: // '\002'
            return 11;

        case 3: // '\003'
            return 12;
        }
    }

    private void initAlbumTowItem(List list, int i)
    {
        if (list != null && list.size() > 0)
        {
            ((TextView)headerView.findViewById(0x7f0a072f)).setText(Html.fromHtml((new StringBuilder()).append("\u53D1\u5E03\u7684\u4E13\u8F91    <small>(").append(i).append(")</small>").toString()));
            View view = headerView.findViewById(0x7f0a0734);
            Resources resources = mCon.getResources();
            String s = resources.getResourcePackageName(0x7f0a003d);
            ArrayList arraylist = new ArrayList(list.size());
            int j = 0;
            while (j < list.size()) 
            {
                final AlbumModel info = (AlbumModel)list.get(j);
                View view1 = headerView.findViewById(resources.getIdentifier((new StringBuilder()).append("include").append(j).toString(), "id", s));
                ImageView imageview = (ImageView)view1.findViewById(0x7f0a0023);
                TextView textview = (TextView)view1.findViewById(0x7f0a00ea);
                TextView textview1 = (TextView)view1.findViewById(0x7f0a0152);
                TextView textview2 = (TextView)view1.findViewById(0x7f0a014f);
                final View container = view1.findViewById(0x7f0a0154);
                Object obj = (TextView)view1.findViewById(0x7f0a0155);
                obj = (ImageView)view1.findViewById(0x7f0a014d);
                obj = (TextView)view1.findViewById(0x7f0a0151);
                setCollectStatus(container, info.isFavorite);
                arraylist.add(container);
                container.setOnClickListener(new _cls14());
                ImageManager2.from(mCon).displayImage(imageview, info.coverLarge, 0x7f0202e0);
                textview.setText(info.title);
                textview1.setText((new StringBuilder()).append(info.tracks).append("\u96C6").toString());
                ((TextView) (obj)).setText(StringUtil.getFriendlyNumStr(info.playTimes));
                long l;
                if (info.lastUptrackAt == 0L)
                {
                    l = info.updatedAt;
                } else
                {
                    l = info.lastUptrackAt;
                }
                textview2.setText(getString(0x7f0901e7, new Object[] {
                    ToolUtil.convertTime(l)
                }));
                view1.setOnClickListener(new _cls15());
                j++;
            }
            if (UserInfoMannage.hasLogined())
            {
                loadRSSStatus(list, arraylist);
            } else
            {
                loadRssStatusLocal(list, arraylist);
            }
            if (i <= 2)
            {
                headerView.findViewById(0x7f0a0733).setVisibility(8);
                view.setVisibility(8);
            } else
            {
                headerView.findViewById(0x7f0a0733).setVisibility(0);
                view.setVisibility(0);
                view.setOnClickListener(new _cls16());
            }
            if (list.size() < 2)
            {
                list = headerView.findViewById(0x7f0a0732);
                headerView.findViewById(0x7f0a0731).setVisibility(8);
                list.setVisibility(8);
            } else
            {
                list = headerView.findViewById(0x7f0a0732);
                headerView.findViewById(0x7f0a0731).setVisibility(0);
                list.setVisibility(0);
            }
            include_Album.setVisibility(0);
            return;
        } else
        {
            include_Album.setVisibility(8);
            return;
        }
    }

    private void initAnimationTextView()
    {
        introAnimTxt = (TextView)headerView.findViewById(0x7f0a05e3);
        TextView textview = introAnimTxt;
        String s;
        int i;
        int j;
        if (TextUtils.isEmpty(mHomeModel.personalSignature))
        {
            s = getString(0x7f09016f);
        } else
        {
            s = mHomeModel.personalSignature;
        }
        textview.setText(s);
        i = introTxt.getTop();
        j = introAnimTxt.getTop();
        introAnimTxt.offsetTopAndBottom(i - j);
    }

    private void initData()
    {
        mInfoModel = UserInfoMannage.getInstance().getUser();
        toUid = getArguments().getLong("toUid", 0L);
    }

    private void initHomePage(HomePageModel homepagemodel)
    {
        mHomeModel = homepagemodel;
        bacImg = (ImageView)headerView.findViewById(0x7f0a05a4);
        markImageView(bacImg);
        ImageManager2.from(mCon).displayImage(bacImg, mHomeModel.backgroundLogo, -1);
        headImg = (ImageView)headerView.findViewById(0x7f0a05a6);
        headImg.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mCon).displayImage(headImg, mHomeModel.mobileMiddleLogo, 0x7f0201b5);
        headImg.setOnClickListener(new _cls7());
        nicknameTxt.setText(mHomeModel.nickname);
        TextView textview;
        if (mHomeModel.isVerified)
        {
            nicknameTxt.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(0x7f0200c0), null);
        } else
        {
            nicknameTxt.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
        textview = introTxt;
        if (TextUtils.isEmpty(mHomeModel.personalSignature))
        {
            homepagemodel = getString(0x7f09016f);
        } else
        {
            homepagemodel = mHomeModel.personalSignature;
        }
        textview.setText(homepagemodel);
        focusOrCancelBtn = (ImageView)headerView.findViewById(0x7f0a05e7);
        if (mHomeModel.isFollowed)
        {
            focusOrCancelBtn.setImageResource(0x7f020061);
            focusOrCancelBtn.setTag(Boolean.valueOf(true));
        } else
        {
            focusOrCancelBtn.setImageResource(0x7f020077);
            focusOrCancelBtn.setTag(Boolean.valueOf(false));
        }
        if (a.o && mHomeModel != null && mHomeModel.getZoneId() > 0L)
        {
            focusOrCancelBtn.setLayoutParams(new android.widget.LinearLayout.LayoutParams(-2, -1));
            zoneBtnIv.setVisibility(0);
        } else
        {
            zoneBtnIv.setVisibility(8);
        }
        initAnimationTextView();
        ((TextView)headerView.findViewById(0x7f0a05e9)).setText(StringUtil.getFriendlyNumStr(mHomeModel.followings));
        ((TextView)headerView.findViewById(0x7f0a05b7)).setText(StringUtil.getFriendlyNumStr(mHomeModel.followers));
        ((TextView)headerView.findViewById(0x7f0a05ec)).setText(StringUtil.getFriendlyNumStr(mHomeModel.favorites));
        if (mHomeModel.isHasLive() && !TingMediaPlayer.getTingMediaPlayer(getActivity().getApplicationContext()).isUseSystemPlayer())
        {
            loadRadioData();
            return;
        } else
        {
            programView.setVisibility(8);
            programDividerView.setVisibility(8);
            return;
        }
    }

    private void initListener()
    {
        programView.setOnClickListener(new _cls5());
        playIcon.setOnClickListener(new _cls6());
    }

    private void initViews()
    {
        mListview.addHeaderView(headerView);
        programView.setVisibility(8);
        programDividerView.setVisibility(8);
        View view = new View(getActivity());
        view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, getResources().getDimensionPixelSize(0x7f080009)));
        mListview.addFooterView(view, null, false);
        mListview.addFooterView(mFooterViewLoading);
        showFooterView(FooterView.HIDE_ALL);
        otherSpaceAdapter = new OtherSpaceAdapter(this, null);
        mListview.setAdapter(otherSpaceAdapter);
        mListview.setOnScrollListener(new _cls2());
        mListview.setOnItemClickListener(new _cls3());
        mListview.setOnItemLongClickListener(new _cls4());
    }

    private void loadRSSStatus(final List data, final List containers)
    {
        if (!UserInfoMannage.hasLogined())
        {
            return;
        }
        LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
        RequestParams requestparams = new RequestParams();
        StringBuilder stringbuilder = new StringBuilder();
        for (Iterator iterator = data.iterator(); iterator.hasNext(); stringbuilder.append(((AlbumModel)iterator.next()).albumId).append(",")) { }
        requestparams.add("uid", (new StringBuilder()).append("").append(logininfomodel.uid).toString());
        requestparams.add("album_ids", stringbuilder.toString());
        f.a().a("m/album_subscribe_status", requestparams, null, new _cls13());
    }

    private void loadRadioData()
    {
        String s = (new StringBuilder()).append(a.u).append("mobile/others/live").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("toUid", toUid);
        f.a().a(s, requestparams, DataCollectUtil.getDataFromView(programView), new _cls8());
    }

    private void loadRssStatusLocal(final List data, final List containers)
    {
        (new _cls12()).myexec(new Void[0]);
    }

    private void playSound(ImageView imageview, SoundInfo soundinfo, String s)
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        SoundInfo soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
        if (localmediaservice == null || soundinfo == null)
        {
            return;
        }
        if (soundinfo1 == null)
        {
            PlayTools.gotoPlay(32, soundinfo, getActivity(), false, s);
            imageview.setImageResource(0x7f020250);
            return;
        }
        switch (localmediaservice.getPlayServiceState())
        {
        default:
            return;

        case 0: // '\0'
            PlayTools.gotoPlay(32, soundinfo, getActivity(), false, s);
            imageview.setImageResource(0x7f02024f);
            return;

        case 1: // '\001'
        case 3: // '\003'
            if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.radioId == soundinfo1.radioId)
            {
                localmediaservice.pause();
                imageview.setImageResource(0x7f020250);
                return;
            } else
            {
                PlayTools.gotoPlay(32, soundinfo, getActivity(), false, s);
                imageview.setImageResource(0x7f02024f);
                return;
            }

        case 2: // '\002'
            break;
        }
        if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.radioId == soundinfo1.radioId)
        {
            localmediaservice.start();
            imageview.setImageResource(0x7f02024f);
            return;
        } else
        {
            PlayTools.gotoPlay(32, soundinfo, getActivity(), false, s);
            imageview.setImageResource(0x7f02024f);
            return;
        }
    }

    private void registerListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls1();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void sendPrivateMessage()
    {
        if (mHomeModel == null)
        {
            return;
        }
        Intent intent = new Intent(getActivity(), com/ximalaya/ting/android/activity/homepage/TalkViewAct);
        if (!UserInfoMannage.hasLogined())
        {
            startActivity(new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity));
            return;
        }
        ToolUtil.onEvent(getActivity(), "Account_Privateletter");
        if (mHomeModel == null)
        {
            showToast("\u7528\u6237\u4FE1\u606F\u6709\u8BEF\uFF01");
            return;
        } else
        {
            intent.putExtra("title", mHomeModel.nickname);
            intent.putExtra("toUid", mHomeModel.uid);
            intent.putExtra("meHeadUrl", mHomeModel.smallLogo);
            startActivity(intent);
            return;
        }
    }

    private void setCollectStatus(View view, boolean flag)
    {
        view = (TextView)view.findViewById(0x7f0a0155);
        if (flag)
        {
            view.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ee, 0, 0);
            view.setText("\u5DF2\u6536\u85CF");
            view.setTextColor(Color.parseColor("#999999"));
            return;
        } else
        {
            view.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ed, 0, 0);
            view.setText("\u6536\u85CF");
            view.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    private void setFocusNetData(final boolean isFollow, final View v)
    {
        (new _cls10()).myexec(new Void[0]);
    }

    private void setFocusState(final View v)
    {
        if (v.getTag() == null || !(v.getTag() instanceof Boolean))
        {
            return;
        }
        final boolean isFollow = ((Boolean)v.getTag()).booleanValue();
        if (UserInfoMannage.hasLogined())
        {
            ToolUtil.onEvent(getActivity(), "Account_Follow");
            if (isFollow)
            {
                (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u8981\u53D6\u6D88\u5173\u6CE8\uFF1F").setOkBtn(new _cls9()).showConfirm();
                return;
            } else
            {
                setFocusNetData(isFollow, v);
                return;
            }
        } else
        {
            v = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
            v.setFlags(0x20000000);
            startActivity(v);
            return;
        }
    }

    private void startHeaderAnimation()
    {
        isAniamtionFinished = false;
        int i = ToolUtil.dp2px(getActivity(), 85F);
        int j = introTxt.getTop() - introAnimTxt.getTop();
        AnimatorSet animatorset = new AnimatorSet();
        AnimatorSet animatorset1 = new AnimatorSet();
        ObjectAnimator objectanimator;
        ObjectAnimator objectanimator1;
        ObjectAnimator objectanimator2;
        ObjectAnimator objectanimator3;
        if (isHeadImgShowing)
        {
            animatorset.playTogether(new Animator[] {
                ObjectAnimator.ofFloat(headImg, "alpha", new float[] {
                    1.0F, 0.0F
                }), ObjectAnimator.ofFloat(headImg, "rotation", new float[] {
                    0.0F, 180F
                }), ObjectAnimator.ofFloat(headImg, "translationY", new float[] {
                    0.0F, (float)(-i)
                })
            });
            objectanimator3 = ObjectAnimator.ofFloat(nicknameTxt, "translationY", new float[] {
                0.0F, (float)(-i)
            });
            objectanimator2 = ObjectAnimator.ofFloat(introAnimTxt, "translationY", new float[] {
                (float)j, (float)(-i + j)
            });
            objectanimator = ObjectAnimator.ofFloat(introAnimTxt, "alpha", new float[] {
                0.0F, 1.0F
            });
            objectanimator1 = ObjectAnimator.ofFloat(arrowImg, "rotation", new float[] {
                0.0F, 180F
            });
        } else
        {
            animatorset.playTogether(new Animator[] {
                ObjectAnimator.ofFloat(headImg, "alpha", new float[] {
                    0.0F, 1.0F
                }), ObjectAnimator.ofFloat(headImg, "rotation", new float[] {
                    180F, 0.0F
                }), ObjectAnimator.ofFloat(headImg, "translationY", new float[] {
                    (float)(-i), 0.0F
                })
            });
            objectanimator3 = ObjectAnimator.ofFloat(nicknameTxt, "translationY", new float[] {
                (float)(-i), 0.0F
            });
            objectanimator2 = ObjectAnimator.ofFloat(introAnimTxt, "translationY", new float[] {
                (float)(-i + j), (float)j
            });
            objectanimator = ObjectAnimator.ofFloat(introAnimTxt, "alpha", new float[] {
                1.0F, 0.0F
            });
            objectanimator1 = ObjectAnimator.ofFloat(arrowImg, "rotation", new float[] {
                180F, 0.0F
            });
        }
        animatorset1.playTogether(new Animator[] {
            objectanimator2, objectanimator
        });
        objectanimator3.addListener(new _cls11());
        animatorset.setDuration(500L);
        animatorset1.setDuration(500L);
        objectanimator3.setDuration(500L);
        objectanimator1.setDuration(500L);
        animatorset.start();
        objectanimator3.start();
        objectanimator1.start();
        animatorset1.start();
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(this);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void updatePlayIcon()
    {
        if (mRadioModel == null)
        {
            return;
        }
        if (ToolUtil.isLivePlaying(mRadioModel.getRadioId()))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                playIcon.setImageResource(0x7f020250);
                return;
            } else
            {
                playIcon.setImageResource(0x7f02024f);
                return;
            }
        } else
        {
            playIcon.setImageResource(0x7f020250);
            return;
        }
    }

    private void updateSoundView()
    {
        programView.setVisibility(0);
        programDividerView.setVisibility(0);
        ImageManager2.from(mActivity).displayImage(radioCover, mRadioModel.getRadioCoverSmall(), -1);
        programNameTxt.setText((new StringBuilder()).append("\u6B63\u5728\u76F4\u64AD:").append(mRadioModel.getProgramName()).toString());
        playNumTxt.setText(StringUtil.getFriendlyNumStr(mRadioModel.getRadioPlayCount()));
        updatePlayIcon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        registerListener();
        initData();
        findViews();
        initViews();
        initListener();
        requestNetData();
    }

    public void onClick(View view)
    {
        if (!OneClickHelper.getInstance().onClick(view)) goto _L2; else goto _L1
_L1:
        view.getId();
        JVM INSTR lookupswitch 9: default 96
    //                   2131361915: 97
    //                   2131363236: 375
    //                   2131363300: 300
    //                   2131363301: 265
    //                   2131363302: 311
    //                   2131363303: 305
    //                   2131363304: 102
    //                   2131363306: 159
    //                   2131363307: 216;
           goto _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11
_L2:
        return;
_L3:
        finishFragment();
        return;
_L9:
        if (mHomeModel != null)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("flag", 0);
            bundle.putLong("toUid", mHomeModel.uid);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/MyAttentionFragment, bundle);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L10:
        if (mHomeModel != null)
        {
            Bundle bundle1 = new Bundle();
            bundle1.putInt("flag", 1);
            bundle1.putLong("toUid", mHomeModel.uid);
            bundle1.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/MyAttentionFragment, bundle1);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L11:
        if (mHomeModel != null)
        {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("toUid", mHomeModel.uid);
            bundle2.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/MyLikedSoundsFragment, bundle2);
            return;
        }
        if (true) goto _L2; else goto _L6
_L6:
        if (mHomeModel != null)
        {
            (new BaseShareDialog(mActivity, mHomeModel, view)).show();
            return;
        } else
        {
            showToast("\u4EB2\uFF0C\u6CA1\u6709\u4E3B\u64AD\u4FE1\u606F\u54E6~");
            return;
        }
_L5:
        sendPrivateMessage();
        return;
_L8:
        setFocusState(view);
        return;
_L7:
        if (mHomeModel.getZoneId() > 0L)
        {
            ToolUtil.onEvent(mCon, "zone_main");
            Bundle bundle3 = new Bundle();
            bundle3.putLong("zoneId", mHomeModel.getZoneId());
            bundle3.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment, bundle3);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L4:
        if (headImg != null && isAniamtionFinished)
        {
            startHeaderAnimation();
            return;
        }
        if (true) goto _L2; else goto _L12
_L12:
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mFrom = bundle.getInt("from");
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03001e, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        unRegisterListener();
        mListview.setAdapter(null);
        if (otherSpaceAdapter != null)
        {
            otherSpaceAdapter.releseData();
            otherSpaceAdapter = null;
        }
        super.onDestroyView();
    }

    public void onPlayCanceled()
    {
    }

    public void onSoundChanged(int i)
    {
        if (otherSpaceAdapter != null)
        {
            otherSpaceAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (i < 0 || soundinfo == null || i >= otherSpaceAdapter.getData().size() || ((RecordingModel)otherSpaceAdapter.getData().get(i)).trackId != soundinfo.trackId || otherSpaceAdapter == null)
        {
            return;
        } else
        {
            RecordingModel recordingmodel = (RecordingModel)otherSpaceAdapter.getData().get(i);
            recordingmodel.isLike = soundinfo.is_favorited;
            recordingmodel.likes = soundinfo.favorites_counts;
            otherSpaceAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void requestNetData()
    {
        if (mInfoModel == null)
        {
            mInfoModel = UserInfoMannage.getInstance().getUser();
        }
        (new GetHomePageInfo()).myexec(new String[0]);
        (new GetMySoundList()).myexec(new String[0]);
        (new GetOtherAlbumList()).myexec(new String[0]);
    }

    public void setSoundCount()
    {
        if (rmList != null && soundsCountTxt != null)
        {
            soundsCountTxt.setText(Html.fromHtml((new StringBuilder()).append("\u53D1\u5E03\u7684\u58F0\u97F3    <small>(").append(StringUtil.getFriendlyNumStr(rmList.totalCount)).append(")</small>").toString()));
        }
        if (rmList == null || rmList.totalCount == 0)
        {
            showFooterView(FooterView.NO_DATA);
        }
    }

    public void showFooterView(FooterView footerview)
    {
        if (mListview != null && mFooterViewLoading != null)
        {
            mListview.setFooterDividersEnabled(false);
            mFooterViewLoading.setVisibility(0);
            if (footerview == FooterView.MORE)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                return;
            }
            if (footerview == FooterView.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerview == FooterView.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == FooterView.REQUEST_FAILED)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerview == FooterView.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073a).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
            if (footerview == FooterView.NO_DATA)
            {
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                return;
            }
        }
    }












/*
    static boolean access$1702(OtherSpaceFragment otherspacefragment, boolean flag)
    {
        otherspacefragment.isHeadImgShowing = flag;
        return flag;
    }

*/


/*
    static boolean access$1802(OtherSpaceFragment otherspacefragment, boolean flag)
    {
        otherspacefragment.isAniamtionFinished = flag;
        return flag;
    }

*/












/*
    static boolean access$302(OtherSpaceFragment otherspacefragment, boolean flag)
    {
        otherspacefragment.isLoadingNetSound = flag;
        return flag;
    }

*/






/*
    static RadioSound access$702(OtherSpaceFragment otherspacefragment, RadioSound radiosound)
    {
        otherspacefragment.mRadioModel = radiosound;
        return radiosound;
    }

*/



    private class _cls17 extends com.ximalaya.ting.android.b.a
    {

        final OtherSpaceFragment this$0;
        final View val$container;
        final Context val$context;
        final AlbumModel val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, container);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            setCollectStatus(container, model.isFavorite);
        }

        public void onSuccess(String s)
        {
            boolean flag = true;
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
_L4:
            return;
_L2:
            int i;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return;
            }
            if (s == null) goto _L4; else goto _L3
_L3:
            i = s.getIntValue("ret");
            if (i != 0)
            {
                break MISSING_BLOCK_LABEL_107;
            }
            s = model;
            if (model.isFavorite)
            {
                flag = false;
            }
            s.isFavorite = flag;
            setCollectStatus(container, model.isFavorite);
            if (model.isFavorite)
            {
                s = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            Toast.makeText(context, s, 0).show();
            return;
            if (i != 791)
            {
                break MISSING_BLOCK_LABEL_122;
            }
            model.isFavorite = true;
            setCollectStatus(container, model.isFavorite);
            if (s.getString("msg") != null)
            {
                break MISSING_BLOCK_LABEL_165;
            }
            s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
_L6:
            Toast.makeText(context, s, 0).show();
            return;
            s = s.getString("msg");
            if (true) goto _L6; else goto _L5
_L5:
        }

        _cls17()
        {
            this$0 = OtherSpaceFragment.this;
            context = context1;
            container = view;
            model = albummodel;
            super();
        }
    }


    private class _cls18 extends MyAsyncTask
    {

        final OtherSpaceFragment this$0;
        final View val$container;
        final AlbumModel val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            AlbumModel albummodel = model;
            boolean flag;
            if (!model.isFavorite)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            albummodel.isFavorite = flag;
            if (!model.isFavorite)
            {
                avoid.deleteAlbumInLocalAlbumList(model);
            } else
            {
                avoid.saveAlbumModel(model);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            setCollectStatus(container, model.isFavorite);
        }

        _cls18()
        {
            this$0 = OtherSpaceFragment.this;
            model = albummodel;
            container = view;
            super();
        }
    }


    private class _cls14
        implements android.view.View.OnClickListener
    {

        final OtherSpaceFragment this$0;
        final View val$container;
        final AlbumModel val$info;

        public void onClick(View view)
        {
            doCollect(getActivity(), info, container);
        }

        _cls14()
        {
            this$0 = OtherSpaceFragment.this;
            info = albummodel;
            container = view;
            super();
        }
    }


    private class _cls15
        implements android.view.View.OnClickListener
    {

        final OtherSpaceFragment this$0;
        final AlbumModel val$info;

        public void onClick(View view)
        {
            Bundle bundle;
            bundle = new Bundle();
            bundle.putString("album", JSON.toJSONString(info));
            mFrom;
            JVM INSTR tableswitch 1 3: default 56
        //                       1 88
        //                       2 98
        //                       3 77;
               goto _L1 _L2 _L3 _L4
_L1:
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
            return;
_L4:
            bundle.putInt("from", 7);
            continue; /* Loop/switch isn't completed */
_L2:
            bundle.putInt("from", 4);
            continue; /* Loop/switch isn't completed */
_L3:
            bundle.putInt("from", 8);
            if (true) goto _L1; else goto _L5
_L5:
        }

        _cls15()
        {
            this$0 = OtherSpaceFragment.this;
            info = albummodel;
            super();
        }
    }


    private class _cls16
        implements android.view.View.OnClickListener
    {

        final OtherSpaceFragment this$0;

        public void onClick(View view)
        {
            Bundle bundle;
            if (mHomeModel == null)
            {
                Toast.makeText(mCon, "\u7A0D\u7B49\u6570\u636E\u521D\u59CB\u5316\u5B8C\u6BD5...", 0).show();
                return;
            }
            bundle = new Bundle();
            bundle.putLong("toUid", mHomeModel.uid);
            mFrom;
            JVM INSTR tableswitch 1 3: default 84
        //                       1 115
        //                       2 125
        //                       3 105;
               goto _L1 _L2 _L3 _L4
_L1:
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/AlbumListFragment, bundle);
            return;
_L4:
            bundle.putInt("from", 1);
            continue; /* Loop/switch isn't completed */
_L2:
            bundle.putInt("from", 1);
            continue; /* Loop/switch isn't completed */
_L3:
            bundle.putInt("from", 1);
            if (true) goto _L1; else goto _L5
_L5:
        }

        _cls16()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.view.View.OnClickListener
    {

        final OtherSpaceFragment this$0;

        public void onClick(View view)
        {
            Logger.d("OtherSpaceFragment", (new StringBuilder()).append("The big logo is:").append(mHomeModel.mobileLargeLogo).toString());
            if (mHomeModel != null && mHomeModel.mobileLargeLogo != null)
            {
                view = new Intent(mCon, com/ximalaya/ting/android/activity/homepage/OtherDetailActivity);
                view.putExtra("flag", "other");
                view.putExtra("homepage", JSON.toJSONString(mHomeModel));
                startActivity(view);
            }
        }

        _cls7()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final OtherSpaceFragment this$0;

        public void onClick(View view)
        {
            if (mRadioModel == null)
            {
                return;
            } else
            {
                PlayTools.gotoPlay(32, ModelHelper.toSoundInfo(mRadioModel), getActivity(), true, DataCollectUtil.getDataFromView(programView));
                updatePlayIcon();
                return;
            }
        }

        _cls5()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final OtherSpaceFragment this$0;

        public void onClick(View view)
        {
            if (mRadioModel == null)
            {
                return;
            } else
            {
                playSound(playIcon, ModelHelper.toSoundInfo(mRadioModel), DataCollectUtil.getDataFromView(view));
                return;
            }
        }

        _cls6()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AbsListView.OnScrollListener
    {

        final OtherSpaceFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0 && rmList != null && !isLoadingNetSound && otherSpaceAdapter != null && abslistview.getLastVisiblePosition() - 1 >= otherSpaceAdapter.getCount() && rmList.maxPageId > rmList.pageId)
            {
                abslistview = rmList;
                abslistview.pageId = ((RecordingModelList) (abslistview)).pageId + 1;
                showFooterView(FooterView.LOADING);
                (new GetMySoundList()).myexec(new String[0]);
            }
        }

        _cls2()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final OtherSpaceFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListview.getHeaderViewsCount();
            Object obj;
            if (i >= 0 && i < otherSpaceAdapter.getCount())
            {
                if ((obj = (RecordingModel)otherSpaceAdapter.getItem(i)) != null && 2 == ((RecordingModel) (obj)).processState)
                {
                    adapterview = ModelHelper.recordingModelToSoundInfoList(otherSpaceAdapter.getData());
                    SoundInfo soundinfo = new SoundInfo();
                    soundinfo.trackId = ((RecordingModel) (obj)).trackId;
                    i = adapterview.indexOf(soundinfo);
                    int j = getPlaySource();
                    obj = new HashMap();
                    ((HashMap) (obj)).put("uid", (new StringBuilder()).append(toUid).append("").toString());
                    PlayTools.gotoPlay(j, e.B, rmList.pageId, ((HashMap) (obj)), adapterview, i, getActivity(), true, DataCollectUtil.getDataFromView(view));
                    return;
                }
            }
        }

        _cls3()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final OtherSpaceFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListview.getHeaderViewsCount();
            if (i < 0 || i >= otherSpaceAdapter.getCount())
            {
                return false;
            } else
            {
                adapterview = (RecordingModel)otherSpaceAdapter.getData().get(i);
                otherSpaceAdapter.handleItemLongClick(adapterview, view);
                return true;
            }
        }

        _cls4()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    private class _cls13 extends com.ximalaya.ting.android.b.a
    {

        final OtherSpaceFragment this$0;
        final List val$containers;
        final List val$data;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
_L2:
            return;
            if (!isAdded() || TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            AlbumModel albummodel = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = albummodel;
            }
            if (s != null && s.getIntValue("ret") == 0)
            {
                s = s.getJSONObject("status");
                if (s != null)
                {
                    int i = 0;
                    while (i < data.size()) 
                    {
                        albummodel = (AlbumModel)data.get(i);
                        boolean flag;
                        if (s.getIntValue((new StringBuilder()).append("").append(albummodel.albumId).toString()) == 1)
                        {
                            flag = true;
                        } else
                        {
                            flag = false;
                        }
                        albummodel.isFavorite = flag;
                        setCollectStatus((View)containers.get(i), albummodel.isFavorite);
                        i++;
                    }
                }
            }
            if (true) goto _L2; else goto _L3
_L3:
        }

        _cls13()
        {
            this$0 = OtherSpaceFragment.this;
            data = list;
            containers = list1;
            super();
        }
    }


    private class _cls8 extends com.ximalaya.ting.android.b.a
    {

        final OtherSpaceFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, programView);
        }

        public void onNetError(int i, String s)
        {
            Logger.log((new StringBuilder()).append("loadRadioData: statusCode=").append(i).append(", errorMessage=").append(s).toString());
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            JSONObject jsonobject;
            return;
_L2:
            if ((jsonobject = JSON.parseObject(s)) == null) goto _L1; else goto _L3
_L3:
            if (jsonobject.getInteger("ret").intValue() != 0) goto _L1; else goto _L4
_L4:
            mRadioModel = (RadioSound)JSON.parseObject(s, com/ximalaya/ting/android/model/livefm/RadioSound);
            if (mRadioModel != null)
            {
                mRadioModel.setCategory(1);
                mRadioModel.setRname(mHomeModel.nickname);
                updateSoundView();
                return;
            }
              goto _L1
            s;
            Logger.log((new StringBuilder()).append("exception=").append(s.getMessage()).toString());
            return;
        }

        _cls8()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    private class _cls12 extends MyAsyncTask
    {

        final OtherSpaceFragment this$0;
        final List val$containers;
        final List val$data;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            if (data != null && data.size() > 0)
            {
                avoid = data.iterator();
                while (avoid.hasNext()) 
                {
                    AlbumModel albummodel = (AlbumModel)avoid.next();
                    boolean flag;
                    if (AlbumModelManage.getInstance().isHadCollected(albummodel.albumId) != null)
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                    albummodel.isFavorite = flag;
                }
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            for (int i = 0; i < data.size(); i++)
            {
                void1 = (AlbumModel)data.get(i);
                setCollectStatus((View)containers.get(i), ((AlbumModel) (void1)).isFavorite);
            }

        }

        _cls12()
        {
            this$0 = OtherSpaceFragment.this;
            data = list;
            containers = list1;
            super();
        }
    }


    private class _cls1 extends OnPlayerStatusUpdateListenerProxy
    {

        final OtherSpaceFragment this$0;

        public void onPlayStateChange()
        {
            otherSpaceAdapter.notifyDataSetChanged();
            updatePlayIcon();
        }

        _cls1()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }


    private class _cls10 extends MyAsyncTask
    {

        final OtherSpaceFragment this$0;
        final boolean val$isFollow;
        final View val$v;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            return CommonRequest.doSetGroup(mCon, (new StringBuilder()).append(toUid).append("").toString(), isFollow, v, v);
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if (!canGoon())
            {
                return;
            }
            progressBar.setVisibility(8);
            if (s == null)
            {
                boolean flag;
                if (!isFollow)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                if (flag)
                {
                    ((ImageView)v).setImageResource(0x7f020061);
                } else
                {
                    ((ImageView)v).setImageResource(0x7f020077);
                }
                v.setTag(Boolean.valueOf(flag));
                return;
            } else
            {
                showToast(s);
                return;
            }
        }

        protected void onPreExecute()
        {
            progressBar.setVisibility(0);
        }

        _cls10()
        {
            this$0 = OtherSpaceFragment.this;
            isFollow = flag;
            v = view;
            super();
        }
    }


    private class _cls9
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final OtherSpaceFragment this$0;
        final boolean val$isFollow;
        final View val$v;

        public void onExecute()
        {
            setFocusNetData(isFollow, v);
        }

        _cls9()
        {
            this$0 = OtherSpaceFragment.this;
            isFollow = flag;
            v = view;
            super();
        }
    }


    private class _cls11
        implements com.nineoldandroids.animation.Animator.AnimatorListener
    {

        final OtherSpaceFragment this$0;

        public void onAnimationCancel(Animator animator)
        {
        }

        public void onAnimationEnd(Animator animator)
        {
            if (isHeadImgShowing)
            {
                isHeadImgShowing = false;
                introAnimTxt.setVisibility(0);
                introTxt.setVisibility(4);
            } else
            {
                isHeadImgShowing = true;
                introTxt.setVisibility(0);
                introAnimTxt.setVisibility(4);
            }
            isAniamtionFinished = true;
        }

        public void onAnimationRepeat(Animator animator)
        {
        }

        public void onAnimationStart(Animator animator)
        {
            introAnimTxt.setVisibility(0);
            introTxt.setVisibility(4);
        }

        _cls11()
        {
            this$0 = OtherSpaceFragment.this;
            super();
        }
    }

}
