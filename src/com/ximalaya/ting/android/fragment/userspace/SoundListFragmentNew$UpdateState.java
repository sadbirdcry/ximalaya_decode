// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.app.Activity;
import android.support.v4.app.Fragment;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.util.DataCollectUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            SoundListFragmentNew

class mRecordingModel extends Thread
{

    private static final String TAG = "SLFN.UpdateState";
    private boolean mContinue;
    private RecordingModel mRecordingModel;
    private boolean mStop;
    final SoundListFragmentNew this$0;

    public void run()
    {
        String s;
        RequestParams requestparams;
        f f1;
        s = (new StringBuilder()).append("mobile/track/").append(mRecordingModel.trackId).toString();
        requestparams = new RequestParams();
        f1 = f.a();
_L5:
        if (!SoundListFragmentNew.access$3800(SoundListFragmentNew.this) && !mStop && mContinue && SoundListFragmentNew.access$3900(SoundListFragmentNew.this) != null && !SoundListFragmentNew.access$4000(SoundListFragmentNew.this).isFinishing() && SoundListFragmentNew.access$1900(SoundListFragmentNew.this).isAdded()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        class _cls1 extends h
        {

            final SoundListFragmentNew.UpdateState this$1;

            public void onBindXDCS(Header aheader[])
            {
                DataCollectUtil.bindDataToView(aheader, mListView);
            }

            public void onFinish()
            {
            }

            public void onNetError(int i, String s1)
            {
                mContinue = true;
            }

            public void onStart()
            {
            }

            public void onSuccess(String s1)
            {
                if (TextUtils.isEmpty(s1))
                {
                    mContinue = true;
                    return;
                }
                try
                {
                    s1 = JSON.parseObject(s1);
                }
                // Misplaced declaration of an exception variable
                catch (String s1)
                {
                    s1.printStackTrace();
                    s1 = null;
                }
                if (s1 == null || s1.getIntValue("ret") != 0)
                {
                    mContinue = true;
                    return;
                }
                int i = s1.getIntValue("processState");
                int j = s1.getIntValue("status");
                mRecordingModel.processState = i;
                mRecordingModel.status = j;
                double d;
                try
                {
                    d = s1.getDoubleValue("duration");
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    d = 0.0D;
                }
                if (d > 0.0D)
                {
                    mRecordingModel.duration = d;
                }
                if (i == 2)
                {
                    mRecordingModel.playUrl32 = s1.getString("playUrl32");
                    mRecordingModel.playUrl64 = s1.getString("playUrl64");
                    if (j == 1 || j == 2)
                    {
                        mContinue = false;
                        return;
                    } else
                    {
                        mContinue = true;
                        return;
                    }
                }
                if (i == 3)
                {
                    mContinue = false;
                    return;
                } else
                {
                    mContinue = true;
                    return;
                }
            }

            _cls1()
            {
                this$1 = SoundListFragmentNew.UpdateState.this;
                super();
            }
        }

        f1.a(s, requestparams, DataCollectUtil.getDataFromView(mListView), false, new _cls1());
        if (SoundListFragmentNew.access$4300(SoundListFragmentNew.this) == null || SoundListFragmentNew.access$4400(SoundListFragmentNew.this).isFinishing() || !SoundListFragmentNew.access$1900(SoundListFragmentNew.this).isAdded()) goto _L1; else goto _L3
_L3:
        class _cls2
            implements Runnable
        {

            final SoundListFragmentNew.UpdateState this$1;

            public void run()
            {
                SoundListFragmentNew.access$1800(this$0).notifyDataSetChanged();
            }

            _cls2()
            {
                this$1 = SoundListFragmentNew.UpdateState.this;
                super();
            }
        }

        SoundListFragmentNew.access$4500(SoundListFragmentNew.this).runOnUiThread(new _cls2());
        try
        {
            Thread.sleep(2000L);
        }
        catch (InterruptedException interruptedexception)
        {
            interruptedexception.printStackTrace();
        }
        if (true) goto _L5; else goto _L4
_L4:
    }

    public void stopUpdate()
    {
        mStop = true;
        StringBuilder stringbuilder = (new StringBuilder()).append("Stop upadte ");
        String s;
        if (mRecordingModel == null)
        {
            s = "";
        } else
        {
            s = mRecordingModel.title;
        }
        Logger.e("SLFN.UpdateState", stringbuilder.append(s).toString());
    }


/*
    static boolean access$4102(_cls2 _pcls2, boolean flag)
    {
        _pcls2.mContinue = flag;
        return flag;
    }

*/


    public mContinue(RecordingModel recordingmodel)
    {
        this$0 = SoundListFragmentNew.this;
        super();
        mContinue = true;
        mStop = false;
        mRecordingModel = recordingmodel;
    }
}
