// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.activity.recording.RecordingActivity;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.ToolUtilLib;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            AlbumListFragment, RewardStatusModel

public class ManageCenterFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener
{

    private static final int LOAD_REWARD_STATUS = 1;
    private int mNumOfAlbum;
    private TextView mNumOfAlbumTxt;
    private TextView mRecordBtn;
    private TextView mRewardStatusTxt;

    public ManageCenterFragment()
    {
    }

    public static ManageCenterFragment newInstance(int i)
    {
        ManageCenterFragment managecenterfragment = new ManageCenterFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("num_of_album", i);
        managecenterfragment.setArguments(bundle);
        return managecenterfragment;
    }

    public void onActivityCreated(Bundle bundle)
    {
label0:
        {
            super.onActivityCreated(bundle);
            setTitleText("\u7BA1\u7406\u4E2D\u5FC3");
            mNumOfAlbumTxt.setText((new StringBuilder()).append("\u4E13\u8F91(").append(mNumOfAlbum).append(")").toString());
            mRecordBtn.setText("\u5F55\u97F3");
            getView().findViewById(0x7f0a035d).setOnClickListener(this);
            getView().findViewById(0x7f0a0367).setOnClickListener(this);
            getView().findViewById(0x7f0a0360).setOnClickListener(this);
            getView().findViewById(0x7f0a035f).setOnClickListener(this);
            getView().findViewById(0x7f0a0364).setOnClickListener(this);
            getView().findViewById(0x7f0a0363).setOnClickListener(this);
            getView().findViewById(0x7f0a0368).setOnClickListener(this);
            mRecordBtn.setOnClickListener(this);
            if (UserInfoMannage.hasLogined())
            {
                if (!UserInfoMannage.getInstance().getUser().isVerified)
                {
                    break label0;
                }
                getView().findViewById(0x7f0a0368).setVisibility(8);
                getLoaderManager().initLoader(1, null, this);
            }
            return;
        }
        getView().findViewById(0x7f0a0360).setVisibility(8);
        getView().findViewById(0x7f0a0363).setVisibility(8);
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 8: default 80
    //                   2131362653: 81
    //                   2131362655: 268
    //                   2131362656: 134
    //                   2131362659: 495
    //                   2131362660: 357
    //                   2131362663: 104
    //                   2131362664: 387
    //                   2131363609: 474;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9
_L1:
        return;
_L2:
        view = new Bundle();
        view.putBoolean("showCollect", false);
        startFragment(com/ximalaya/ting/android/fragment/userspace/AlbumListFragment, view);
        return;
_L7:
        view = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
        view.putExtra("ExtraUrl", a.D);
        startActivity(view);
        return;
_L4:
        if (UserInfoMannage.hasLogined())
        {
            view = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
            view.putExtra("ExtraUrl", (new StringBuilder()).append(a.U).append("ting-shang-mobile-web/v1/anchor/myReward?payeeId=").append(UserInfoMannage.getInstance().getUser().uid).append("&channel=").append(ToolUtilLib.getChannel(getActivity())).append("&version=").append(ToolUtil.getAppVersion(getActivity())).append("&device=android&impl=").append(ToolUtil.getPackageName(getActivity())).toString());
            startActivity(view);
            return;
        } else
        {
            view = new Intent(getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity);
            getActivity().startActivity(view);
            return;
        }
_L3:
        if (UserInfoMannage.hasLogined())
        {
            view = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
            view.putExtra("ExtraUrl", (new StringBuilder()).append(a.T).append("revenue/user/").append(UserInfoMannage.getInstance().getUser().uid).toString());
            startActivity(view);
            return;
        } else
        {
            view = new Intent(getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity);
            getActivity().startActivity(view);
            return;
        }
_L6:
        view = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
        view.putExtra("ExtraUrl", a.V);
        startActivity(view);
        return;
_L8:
        if (UserInfoMannage.hasLogined())
        {
            long l = UserInfoMannage.getInstance().getUser().uid;
            view = UserInfoMannage.getInstance().getUser().token;
            Intent intent = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
            intent.putExtra("ExtraUrl", (new StringBuilder()).append(a.U).append("api/verify?uid=").append(l).append("&token=").append(view).toString());
            startActivity(intent);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L9:
        startActivity(new Intent(getActivity().getApplicationContext(), com/ximalaya/ting/android/activity/recording/RecordingActivity));
        return;
_L5:
        if (UserInfoMannage.hasLogined())
        {
            long l1 = UserInfoMannage.getInstance().getUser().uid;
            view = UserInfoMannage.getInstance().getUser().token;
            Intent intent1 = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
            intent1.putExtra("ExtraUrl", (new StringBuilder()).append(a.W).append("?uid=").append(l1).append("&token=").append(view).toString());
            startActivity(intent1);
            return;
        }
        if (true) goto _L1; else goto _L10
_L10:
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        if (getArguments() != null)
        {
            mNumOfAlbum = getArguments().getInt("num_of_album");
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        return new RewardStatusModel.RewardStatusLoader(getActivity());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300c3, viewgroup, false);
        mNumOfAlbumTxt = (TextView)layoutinflater.findViewById(0x7f0a035e);
        mRewardStatusTxt = (TextView)layoutinflater.findViewById(0x7f0a0362);
        mRecordBtn = (TextView)layoutinflater.findViewById(0x7f0a0719);
        mRecordBtn.setVisibility(0);
        fragmentBaseContainerView = layoutinflater;
        return layoutinflater;
    }

    public void onLoadFinished(Loader loader, RewardStatusModel rewardstatusmodel)
    {
        if (rewardstatusmodel != null)
        {
            if (rewardstatusmodel.getRewardStatus() == 1)
            {
                mRewardStatusTxt.setText("\u5DF2\u5F00\u901A");
                return;
            }
            if (rewardstatusmodel.getRewardStatus() == 3)
            {
                mRewardStatusTxt.setVisibility(8);
                return;
            } else
            {
                mRewardStatusTxt.setText("\u672A\u5F00\u901A");
                return;
            }
        } else
        {
            mRewardStatusTxt.setVisibility(8);
            return;
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (RewardStatusModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }
}
