// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.activity.homepage.TalkViewAct;
import com.ximalaya.ting.android.adapter.PrivateMsgAdapter;
import com.ximalaya.ting.android.model.message.PrivateMsgModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.DataCollectUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            PrivateMsgFragment

class this._cls0
    implements android.widget.ckListener
{

    final PrivateMsgFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        PrivateMsgFragment.access$302(PrivateMsgFragment.this, i);
        Intent intent = new Intent(getActivity(), com/ximalaya/ting/android/activity/homepage/TalkViewAct);
        adapterview = PrivateMsgFragment.access$400(PrivateMsgFragment.this).getItem(i);
        if (adapterview != null)
        {
            if (i == 2)
            {
                intent.putExtra("isSystemMsg", true);
            }
            if (i == 2 || i == 3)
            {
                intent.putExtra("isForbidTalk", true);
                intent.putExtra("isOfficialAccount", true);
            }
            intent.putExtra("title", adapterview.getLinkmanNickname());
            intent.putExtra("toUid", adapterview.getLinkmanUid());
            if (loginInfoModel == null)
            {
                adapterview = "";
            } else
            {
                adapterview = loginInfoModel.smallLogo;
            }
            intent.putExtra("meHeadUrl", adapterview);
            intent.putExtra("position", i);
            intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startActivityForResult(intent, 2575);
        }
    }

    ()
    {
        this$0 = PrivateMsgFragment.this;
        super();
    }
}
