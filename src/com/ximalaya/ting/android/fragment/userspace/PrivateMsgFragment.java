// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.adapter.PrivateMsgAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.message.PrivateMsgModel;
import com.ximalaya.ting.android.model.message.PrivateMsgModelList;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

public class PrivateMsgFragment extends BaseActivityLikeFragment
{
    class DelPrivateMsgListByUid extends MyAsyncTask
    {

        BaseModel rr;
        final PrivateMsgFragment this$0;

        protected transient Integer doInBackground(Object aobj[])
        {
            Object obj;
            obj = new RequestParams();
            ((RequestParams) (obj)).put("toUid", (String)aobj[0]);
            aobj = (View)aobj[1];
            obj = f.a().b(e.m, ((RequestParams) (obj)), ((View) (aobj)), private_msg);
            if (Utilities.isBlank(((String) (obj))))
            {
                return Integer.valueOf(1);
            }
            aobj = null;
            obj = JSON.parseObject(((String) (obj)));
            aobj = ((Object []) (obj));
_L1:
            Exception exception;
            if (aobj != null)
            {
                rr = new BaseModel();
                rr.ret = ((JSONObject) (aobj)).getInteger("ret").intValue();
                if (rr.ret != -1)
                {
                    rr.msg = ((JSONObject) (aobj)).getString("msg");
                }
                return Integer.valueOf(3);
            } else
            {
                return Integer.valueOf(2);
            }
            exception;
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
              goto _L1
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (!canGoon())
            {
                return;
            }
            if (integer.intValue() == 3)
            {
                if (rr.ret == 0)
                {
                    if (curClickPos_byPrivateMsg - 2 != 0 && curClickPos_byPrivateMsg - 2 != 1)
                    {
                        if (pma.pmmList.size() >= curClickPos_byPrivateMsg - 1)
                        {
                            pma.pmmList.remove(curClickPos_byPrivateMsg - 2);
                        }
                        pma.notifyDataSetChanged();
                        return;
                    }
                    if (pma.pmmList.size() >= curClickPos_byPrivateMsg - 1)
                    {
                        ((PrivateMsgModel)pma.pmmList.get(curClickPos_byPrivateMsg - 2)).setContent("");
                    }
                    ((TextView)(TextView)private_msg.getChildAt(curClickPos_byPrivateMsg).findViewById(0x7f0a056d)).setText("");
                    return;
                } else
                {
                    Toast.makeText(mCon, rr.msg, 0).show();
                    return;
                }
            } else
            {
                Toast.makeText(mCon, "\u5220\u9664\u5931\u8D25", 0).show();
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        DelPrivateMsgListByUid()
        {
            this$0 = PrivateMsgFragment.this;
            super();
        }
    }

    class GetMyPrivateMsg extends MyAsyncTask
    {

        PrivateMsgModelList pmml;
        final PrivateMsgFragment this$0;

        protected transient Integer doInBackground(String as[])
        {
            as = new RequestParams();
            as.put("uid", (new StringBuilder()).append(loginInfoModel.uid).append("").toString());
            as.put("token", loginInfoModel.token);
            as.put("pageSize", (new StringBuilder()).append(msg_list_request_count).append("").toString());
            as.put("isDown", (new StringBuilder()).append(privateMsg_isDown).append("").toString());
            as.put("key", privateMsg_key);
            as = f.a().b(e.i, as, private_msg, private_msg);
            if (Utilities.isBlank(as))
            {
                return Integer.valueOf(1);
            }
            try
            {
                pmml = (PrivateMsgModelList)JSON.parseObject(as, com/ximalaya/ting/android/model/message/PrivateMsgModelList);
                if (pmml != null)
                {
                    return Integer.valueOf(3);
                }
            }
            // Misplaced declaration of an exception variable
            catch (String as[])
            {
                return Integer.valueOf(2);
            }
            return Integer.valueOf(2);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (canGoon())
            {
                if (private_msg.isRefreshing())
                {
                    private_msg.onRefreshComplete();
                }
                if (integer.intValue() == 3)
                {
                    if (pma == null)
                    {
                        long l = getAnimationLeftTime();
                        integer = private_msg;
                        class _cls1
                            implements Runnable
                        {

                            final GetMyPrivateMsg this$1;

                            public void run()
                            {
                                private_msg.setAdapter(pma);
                            }

                _cls1()
                {
                    this$1 = GetMyPrivateMsg.this;
                    super();
                }
                        }

                        _cls1 _lcls1 = new _cls1();
                        if (l <= 0L)
                        {
                            l = 0L;
                        }
                        integer.postDelayed(_lcls1, l);
                        ll_progress.setVisibility(8);
                        if (pmml != null)
                        {
                            pma.pmmList = pmml.list;
                            return;
                        }
                    } else
                    if (pma != null && privateMsg_key.equals("0"))
                    {
                        if (pmml != null)
                        {
                            pma.pmmList = pmml.list;
                            pma.notifyDataSetChanged();
                            return;
                        }
                    } else
                    {
                        pma.pmmList.remove(pma.pmmList.size() - 1);
                        if (pmml.list != null)
                        {
                            pma.pmmList.addAll(pmml.list);
                        }
                        pma.notifyDataSetChanged();
                        return;
                    }
                } else
                {
                    if (pma == null)
                    {
                        firstRequestFail();
                        return;
                    }
                    if (pma != null && privateMsg_key.equals("0"))
                    {
                        makeToast(getString(0x7f09009b));
                        return;
                    } else
                    {
                        pma.pmmList.remove(pma.pmmList.size() - 1);
                        makeToast(getString(0x7f09009b));
                        return;
                    }
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        GetMyPrivateMsg()
        {
            this$0 = PrivateMsgFragment.this;
            super();
        }
    }


    private final int GO_PRIVATE_ACT = 2575;
    private final int GO_SEND_COMMENT = 175;
    private int curClickPos_byPrivateMsg;
    private LinearLayout ll_progress;
    private int msg_list_request_count;
    private View networkError;
    private PrivateMsgAdapter pma;
    private boolean privateMsg_isDown;
    private String privateMsg_key;
    private PullToRefreshListView private_msg;

    public PrivateMsgFragment()
    {
        msg_list_request_count = 37;
        privateMsg_isDown = false;
        privateMsg_key = "0";
    }

    private void findViews()
    {
        networkError = findViewById(0x7f0a0079);
        private_msg = (PullToRefreshListView)findViewById(0x7f0a00da);
        ll_progress = (LinearLayout)findViewById(0x7f0a0073);
    }

    private void firstRequestFail()
    {
        ll_progress.setVisibility(8);
        networkError.setVisibility(0);
        private_msg.setVisibility(8);
    }

    private void initData()
    {
    }

    private void initViews()
    {
        ((TextView)findViewById(0x7f0a00ae)).setText("\u79C1\u4FE1");
        networkError.setOnClickListener(new _cls1());
        private_msg.setOnItemLongClickListener(new _cls2());
        private_msg.setOnItemClickListener(new _cls3());
        private_msg.setOnRefreshListener(new _cls4());
        private_msg.setMyScrollListener2(new _cls5());
    }

    private void makeToast(String s)
    {
        Toast.makeText(mCon, s, 1).show();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initData();
        findViews();
        initViews();
        (new GetMyPrivateMsg()).myexec(new String[0]);
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (i != 175) goto _L2; else goto _L1
_L1:
        if (j == -1)
        {
            Toast.makeText(mCon, "\u56DE\u590D\u6210\u529F", 1).show();
        }
_L4:
        return;
_L2:
        if (i != 2575) goto _L4; else goto _L3
_L3:
        if (j != -1)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (pma == null) goto _L4; else goto _L5
_L5:
        String s = null;
        if (intent != null)
        {
            s = intent.getStringExtra("lastMsg");
        }
        intent = pma.getItem(curClickPos_byPrivateMsg);
        if (intent != null)
        {
            intent.setNoReadCount(0);
            if (s != null)
            {
                intent.setContent(s);
            }
        }
        pma.notifyDataSetChanged();
        return;
        if (j != 22) goto _L4; else goto _L6
_L6:
        finish();
        return;
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030023, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        if (pma != null)
        {
            pma.releseData();
        }
        super.onDestroyView();
    }






/*
    static int access$302(PrivateMsgFragment privatemsgfragment, int i)
    {
        privatemsgfragment.curClickPos_byPrivateMsg = i;
        return i;
    }

*/




/*
    static String access$502(PrivateMsgFragment privatemsgfragment, String s)
    {
        privatemsgfragment.privateMsg_key = s;
        return s;
    }

*/




/*
    static boolean access$702(PrivateMsgFragment privatemsgfragment, boolean flag)
    {
        privatemsgfragment.privateMsg_isDown = flag;
        return flag;
    }

*/



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final PrivateMsgFragment this$0;

        public void onClick(View view)
        {
            ll_progress.setVisibility(0);
            private_msg.setVisibility(0);
            networkError.setVisibility(8);
            private_msg.toRefreshing();
        }

        _cls1()
        {
            this$0 = PrivateMsgFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final PrivateMsgFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, final View arg1, final int arg2, long l)
        {
            if (arg2 != 0 && arg2 != 1)
            {
                class _cls1
                    implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                {

                    final _cls2 this$1;
                    final View val$arg1;
                    final int val$arg2;

                    public void onExecute()
                    {
                        curClickPos_byPrivateMsg = arg2;
                        if (pma.getItem(arg2) != null)
                        {
                            (new DelPrivateMsgListByUid()).myexec(new Object[] {
                                (new StringBuilder()).append(pma.getItem(arg2).getLinkmanUid()).append("").toString(), arg1
                            });
                        }
                    }

                _cls1()
                {
                    this$1 = _cls2.this;
                    arg2 = i;
                    arg1 = view;
                    super();
                }
                }

                (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u5220\u9664\u4E0E\u6B64\u4EBA\u7684\u5168\u90E8\u79C1\u4FE1\uFF1F").setOkBtn(new _cls1()).showConfirm();
                return true;
            } else
            {
                return false;
            }
        }

        _cls2()
        {
            this$0 = PrivateMsgFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final PrivateMsgFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            curClickPos_byPrivateMsg = i;
            Intent intent = new Intent(getActivity(), com/ximalaya/ting/android/activity/homepage/TalkViewAct);
            adapterview = pma.getItem(i);
            if (adapterview != null)
            {
                if (i == 2)
                {
                    intent.putExtra("isSystemMsg", true);
                }
                if (i == 2 || i == 3)
                {
                    intent.putExtra("isForbidTalk", true);
                    intent.putExtra("isOfficialAccount", true);
                }
                intent.putExtra("title", adapterview.getLinkmanNickname());
                intent.putExtra("toUid", adapterview.getLinkmanUid());
                if (loginInfoModel == null)
                {
                    adapterview = "";
                } else
                {
                    adapterview = loginInfoModel.smallLogo;
                }
                intent.putExtra("meHeadUrl", adapterview);
                intent.putExtra("position", i);
                intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startActivityForResult(intent, 2575);
            }
        }

        _cls3()
        {
            this$0 = PrivateMsgFragment.this;
            super();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final PrivateMsgFragment this$0;

        public void onRefresh()
        {
            privateMsg_key = "0";
            (new GetMyPrivateMsg()).myexec(new String[0]);
        }

        _cls4()
        {
            this$0 = PrivateMsgFragment.this;
            super();
        }
    }


    private class _cls5
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnScrollListner
    {

        final PrivateMsgFragment this$0;

        public void onMyScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0 && pma != null && private_msg.getLastVisiblePosition() == pma.getCount() && pma.getCount() - 3 != 0 && (pma.getCount() - 3) % msg_list_request_count == 0)
            {
                privateMsg_key = (new StringBuilder()).append(((PrivateMsgModel)pma.pmmList.get(pma.pmmList.size() - 1)).getUpdatedAt()).append("").toString();
                privateMsg_isDown = true;
                abslistview = new PrivateMsgModel();
                abslistview.flag = false;
                pma.pmmList.add(abslistview);
                pma.notifyDataSetChanged();
                private_msg.setSelection(pma.getCount() - 1);
                (new GetMyPrivateMsg()).myexec(new String[0]);
            }
        }

        _cls5()
        {
            this$0 = PrivateMsgFragment.this;
            super();
        }
    }

}
