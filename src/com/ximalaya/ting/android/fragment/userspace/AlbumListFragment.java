// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.userspace.AlbumListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.album.AlbumCollection;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AlbumListFragment extends BaseListFragment
{
    class GetMyAlbumList extends MyAsyncTask
    {

        AlbumCollection ac;
        final AlbumListFragment this$0;

        protected transient Integer doInBackground(String as[])
        {
            android.content.Context context = mCon;
            long l;
            if (loginInfoModel == null)
            {
                l = 0L;
            } else
            {
                l = loginInfoModel.uid;
            }
            if (loginInfoModel == null)
            {
                as = null;
            } else
            {
                as = loginInfoModel.token;
            }
            as = CommonRequest.doGetMyOrOtherAlbums(context, l, as, toUid, pageId, pageSize, mListView, mListView);
            if (Utilities.isNotBlank(as))
            {
                try
                {
                    ac = (AlbumCollection)JSON.parseObject(as, com/ximalaya/ting/android/model/album/AlbumCollection);
                }
                // Misplaced declaration of an exception variable
                catch (String as[])
                {
                    as.printStackTrace();
                }
                if (ac == null)
                {
                    return Integer.valueOf(2);
                } else
                {
                    return Integer.valueOf(3);
                }
            } else
            {
                return Integer.valueOf(1);
            }
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (!canGoon())
            {
                return;
            }
            ((PullToRefreshListView)mListView).onRefreshComplete();
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            switch (integer.intValue())
            {
            default:
                return;

            case 1: // '\001'
            case 2: // '\002'
                Toast.makeText(mCon, getString(0x7f09009b), 0).show();
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
                return;

            case 3: // '\003'
                break;
            }
            if (ac.ret == 0)
            {
                maxPageId = ac.maxPageId;
                if (aoa != null && pageId == 1)
                {
                    list.clear();
                    list.addAll(ac.list);
                    aoa.notifyDataSetChanged();
                    if (list.size() == 0)
                    {
                        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                    }
                } else
                if (aoa != null && pageId > 1)
                {
                    list.addAll(ac.list);
                    aoa.notifyDataSetChanged();
                }
                if (UserInfoMannage.hasLogined())
                {
                    loadRSSStatus(ac.list);
                } else
                {
                    loadRSSStatusFromLocal(ac.list);
                }
            } else
            {
                Toast.makeText(mCon, ac.msg, 0).show();
            }
            ac.list = null;
            ac = null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        GetMyAlbumList()
        {
            this$0 = AlbumListFragment.this;
            super();
        }
    }


    public static final String FROM = "from";
    public static final int FROM_COLLECTION = 10;
    public static final int FROM_DISCOVERY_CATEGORY = 11;
    public static final int FROM_DISCOVERY_FOCUS = 12;
    public static final int FROM_FEED = 9;
    public static final int FROM_FOCUS = 3;
    public static final int FROM_HOT_ANCHOR = 1;
    public static final int FROM_RECOMMEND_ALBUM = 6;
    public static final int FROM_SEARCH = 2;
    public static final int FROM_SUBJECT = 5;
    private AlbumListAdapter aoa;
    public List list;
    private int mFrom;
    private boolean mShowCollect;
    private volatile int maxPageId;
    private volatile int pageId;
    private volatile int pageSize;
    private long toUid;

    public AlbumListFragment()
    {
        pageId = 1;
        pageSize = 30;
        mShowCollect = true;
    }

    private void findViews()
    {
        list = new ArrayList();
        aoa = new AlbumListAdapter(mActivity, list, mShowCollect);
        mListView.setAdapter(aoa);
    }

    private void initData()
    {
        long l = 0L;
        int i;
        boolean flag;
        if (getArguments() != null)
        {
            l = getArguments().getLong("toUid", 0L);
        }
        toUid = l;
        if (getArguments() == null)
        {
            i = -1;
        } else
        {
            i = getArguments().getInt("from");
        }
        mFrom = i;
        if (getArguments() == null)
        {
            flag = true;
        } else
        {
            flag = getArguments().getBoolean("showCollect", true);
        }
        mShowCollect = flag;
    }

    private void initViews()
    {
        setTitleText("\u4E13\u8F91");
        ((PullToRefreshListView)mListView).setMyScrollListener2(new _cls1());
        mListView.setOnItemClickListener(new _cls2());
        mFooterViewLoading.setOnClickListener(new _cls3());
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls4());
        long l = getAnimationLeftTime();
        if (l > 0L)
        {
            fragmentBaseContainerView.postDelayed(new _cls5(), l);
            return;
        } else
        {
            ((PullToRefreshListView)mListView).toRefreshing();
            return;
        }
    }

    private void loadRSSStatus(final List data)
    {
        if (!UserInfoMannage.hasLogined())
        {
            return;
        }
        LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
        RequestParams requestparams = new RequestParams();
        StringBuilder stringbuilder = new StringBuilder();
        for (Iterator iterator = data.iterator(); iterator.hasNext(); stringbuilder.append(((AlbumModel)iterator.next()).albumId).append(",")) { }
        requestparams.add("uid", (new StringBuilder()).append("").append(logininfomodel.uid).toString());
        requestparams.add("album_ids", stringbuilder.toString());
        f.a().a("m/album_subscribe_status", requestparams, null, new _cls7());
    }

    private void loadRSSStatusFromLocal(final List data)
    {
        (new _cls6()).myexec(new Void[0]);
    }

    public void onActivityCreated(Bundle bundle)
    {
        mListView = (ListView)findViewById(0x7f0a0065);
        super.onActivityCreated(bundle);
        initData();
        findViews();
        initViews();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030005, null);
        return fragmentBaseContainerView;
    }




/*
    static int access$102(AlbumListFragment albumlistfragment, int i)
    {
        albumlistfragment.pageId = i;
        return i;
    }

*/


/*
    static int access$108(AlbumListFragment albumlistfragment)
    {
        int i = albumlistfragment.pageId;
        albumlistfragment.pageId = i + 1;
        return i;
    }

*/




/*
    static int access$302(AlbumListFragment albumlistfragment, int i)
    {
        albumlistfragment.maxPageId = i;
        return i;
    }

*/





    private class _cls1
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnScrollListner
    {

        final AlbumListFragment this$0;

        public void onMyScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0 && aoa != null && pageId < maxPageId && abslistview.getLastVisiblePosition() - 1 == aoa.getCount())
            {
                int i = 
// JavaClassFileOutputException: get_constant: invalid tag

        _cls1()
        {
            this$0 = AlbumListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final AlbumListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (aoa == null) goto _L2; else goto _L1
_L1:
            adapterview = new Bundle();
            adapterview.putString("album", JSON.toJSONString(aoa.getItem(i - 1)));
            mFrom;
            JVM INSTR tableswitch 1 12: default 108
        //                       1 140
        //                       2 150
        //                       3 129
        //                       4 108
        //                       5 191
        //                       6 201
        //                       7 108
        //                       8 108
        //                       9 212
        //                       10 161
        //                       11 171
        //                       12 181;
               goto _L3 _L4 _L5 _L6 _L3 _L7 _L8 _L3 _L3 _L9 _L10 _L11 _L12
_L3:
            adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, adapterview);
_L2:
            return;
_L6:
            adapterview.putInt("from", 7);
            continue; /* Loop/switch isn't completed */
_L4:
            adapterview.putInt("from", 4);
            continue; /* Loop/switch isn't completed */
_L5:
            adapterview.putInt("from", 8);
            continue; /* Loop/switch isn't completed */
_L10:
            adapterview.putInt("from", 1);
            continue; /* Loop/switch isn't completed */
_L11:
            adapterview.putInt("from", 2);
            continue; /* Loop/switch isn't completed */
_L12:
            adapterview.putInt("from", 3);
            continue; /* Loop/switch isn't completed */
_L7:
            adapterview.putInt("from", 5);
            continue; /* Loop/switch isn't completed */
_L8:
            adapterview.putInt("from", 6);
            continue; /* Loop/switch isn't completed */
_L9:
            adapterview.putInt("from", 9);
            if (true) goto _L3; else goto _L13
_L13:
        }

        _cls2()
        {
            this$0 = AlbumListFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final AlbumListFragment this$0;

        public void onClick(View view)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            ((PullToRefreshListView)mListView).toRefreshing();
        }

        _cls3()
        {
            this$0 = AlbumListFragment.this;
            super();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final AlbumListFragment this$0;

        public void onRefresh()
        {
            pageId = 1;
            (new GetMyAlbumList()).myexec(new String[0]);
        }

        _cls4()
        {
            this$0 = AlbumListFragment.this;
            super();
        }
    }


    private class _cls5
        implements Runnable
    {

        final AlbumListFragment this$0;

        public void run()
        {
            if (canGoon())
            {
                ((PullToRefreshListView)mListView).toRefreshing();
            }
        }

        _cls5()
        {
            this$0 = AlbumListFragment.this;
            super();
        }
    }


    private class _cls7 extends a
    {

        final AlbumListFragment this$0;
        final List val$data;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (isAdded() && !TextUtils.isEmpty(s))
            {
                AlbumModel albummodel = null;
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = albummodel;
                }
                if (s != null && s.getIntValue("ret") == 0)
                {
                    s = s.getJSONObject("status");
                    if (s != null)
                    {
                        int i = 0;
                        while (i < data.size()) 
                        {
                            albummodel = (AlbumModel)data.get(i);
                            boolean flag;
                            if (s.getIntValue((new StringBuilder()).append("").append(albummodel.albumId).toString()) == 1)
                            {
                                flag = true;
                            } else
                            {
                                flag = false;
                            }
                            albummodel.isFavorite = flag;
                            i++;
                        }
                        aoa.notifyDataSetChanged();
                        return;
                    }
                }
            }
        }

        _cls7()
        {
            this$0 = AlbumListFragment.this;
            data = list1;
            super();
        }
    }


    private class _cls6 extends MyAsyncTask
    {

        final AlbumListFragment this$0;
        final List val$data;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            int i = 0;
            while (i < data.size()) 
            {
                avoid = (AlbumModel)data.get(i);
                boolean flag;
                if (AlbumModelManage.getInstance().isHadCollected(((AlbumModel) (avoid)).albumId) != null)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                avoid.isFavorite = flag;
                i++;
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            aoa.notifyDataSetChanged();
        }

        _cls6()
        {
            this$0 = AlbumListFragment.this;
            data = list1;
            super();
        }
    }

}
