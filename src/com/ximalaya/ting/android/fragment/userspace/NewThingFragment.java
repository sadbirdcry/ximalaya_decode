// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.adapter.NewThingsAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.feed.BaseFeedModel;
import com.ximalaya.ting.android.model.feed.ChildFeedModel;
import com.ximalaya.ting.android.model.feed.MsgFeedCollection;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public class NewThingFragment extends BaseActivityLikeFragment
{
    class DelNewThing extends MyAsyncTask
    {

        BaseModel rr;
        final NewThingFragment this$0;

        protected transient Integer doInBackground(Object aobj[])
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("id", (String)aobj[0]);
            requestparams.put("feedType", (String)aobj[1]);
            aobj = f.a().b(e.g, requestparams, (View)aobj[2], null);
            try
            {
                rr = (BaseModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/BaseModel);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
            }
            if (rr == null)
            {
                return Integer.valueOf(1);
            }
            if (rr.ret != 0)
            {
                return Integer.valueOf(2);
            } else
            {
                return Integer.valueOf(3);
            }
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        DelNewThing()
        {
            this$0 = NewThingFragment.this;
            super();
        }
    }

    class GetNewThings extends MyAsyncTask
    {

        MsgFeedCollection mfc;
        final NewThingFragment this$0;

        protected transient Integer doInBackground(Object aobj[])
        {
            if (loginInfoModel == null)
            {
                return Integer.valueOf(2);
            }
            RequestParams requestparams = new RequestParams();
            requestparams.put("pageSize", (new StringBuilder()).append(msg_list_request_count).append("").toString());
            if (type_newThings != 0)
            {
                requestparams.put("timeLine", (new StringBuilder()).append(timeLine_newThings).append("").toString());
            } else
            {
                requestparams.put("timeLine", "0");
            }
            requestparams.put("type", (new StringBuilder()).append(type_newThings).append("").toString());
            aobj = f.a().a(e.c, requestparams, (View)aobj[0], new_somthing, false);
            if (((com.ximalaya.ting.android.b.n.a) (aobj)).b == 1)
            {
                aobj = ((com.ximalaya.ting.android.b.n.a) (aobj)).a;
            } else
            {
                aobj = null;
            }
            try
            {
                mfc = (MsgFeedCollection)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/feed/MsgFeedCollection);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
            }
            if (mfc == null)
            {
                return Integer.valueOf(1);
            }
            if (mfc.ret != 0)
            {
                return Integer.valueOf(2);
            } else
            {
                return Integer.valueOf(3);
            }
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (canGoon()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            new_somthing.onRefreshComplete();
            if (integer.intValue() != 3)
            {
                break; /* Loop/switch isn't completed */
            }
            if (mfc == null || mfc.getBaseFeedList() == null || mfc.getBaseFeedList().size() <= 0)
            {
                showEmptyView(true);
            } else
            {
                showEmptyView(false);
                showDataRight();
            }
            new_somthing.setVisibility(0);
            if (mfc != null)
            {
                if (nta == null)
                {
                    nta = new NewThingsAdapter(progressBar, getActivity(), mfc, loginInfoModel.uid, loginInfoModel.token);
                    ll_progress.setVisibility(8);
                    long l = 300L;
                    integer = new_somthing;
                    class _cls1
                        implements Runnable
                    {

                        final GetNewThings this$1;

                        public void run()
                        {
                            if (nta != null && new_somthing != null)
                            {
                                new_somthing.setAdapter(nta);
                                nta.notifyDataSetChanged();
                            }
                        }

                _cls1()
                {
                    this$1 = GetNewThings.this;
                    super();
                }
                    }

                    _cls1 _lcls1 = new _cls1();
                    if (300L <= 0L)
                    {
                        l = 0L;
                    }
                    integer.postDelayed(_lcls1, l);
                    return;
                }
                if (nta != null && type_newThings == 0)
                {
                    nta.setMfc(mfc);
                    new_somthing.onRefreshComplete();
                    nta.clearShowList();
                    nta.notifyDataSetChanged();
                    return;
                }
                if (nta != null && type_newThings == -1)
                {
                    int i = nta.getCount() - 1;
                    if (nta.getMfc() != null && nta.getMfc().getBaseFeedList() != null && mfc.getBaseFeedList() != null && i < nta.getMfc().getBaseFeedList().size())
                    {
                        nta.getMfc().getBaseFeedList().remove(i);
                        nta.getMfc().getBaseFeedList().addAll(mfc.getBaseFeedList());
                        nta.notifyDataSetChanged();
                        return;
                    }
                }
            }
            if (true) goto _L1; else goto _L3
_L3:
            if (integer.intValue() != 1)
            {
                continue; /* Loop/switch isn't completed */
            }
            if (nta == null)
            {
                firstRequestFail();
                return;
            }
            if (nta != null && type_newThings == 0)
            {
                new_somthing.onRefreshComplete();
                makeToast(getString(0x7f09009b));
                return;
            }
            if (nta != null && type_newThings == -1)
            {
                nta.getMfc().getBaseFeedList().remove(nta.getCount() - 1);
                nta.notifyDataSetChanged();
                makeToast(getString(0x7f09009b));
                return;
            }
            continue; /* Loop/switch isn't completed */
            if (integer.intValue() != 2) goto _L1; else goto _L4
_L4:
            if (nta != null && type_newThings == 0)
            {
                new_somthing.onRefreshComplete();
                return;
            }
            if (nta != null && type_newThings == -1)
            {
                nta.getMfc().getBaseFeedList().remove(nta.getCount() - 1);
                nta.notifyDataSetChanged();
                return;
            }
            if (true) goto _L1; else goto _L5
_L5:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        GetNewThings()
        {
            this$0 = NewThingFragment.this;
            super();
            mfc = null;
        }
    }


    private final int GO_SEND_COMMENT = 175;
    private boolean canSaveData;
    private int curClickPos_byNewThing;
    private View curClickView;
    private LinearLayout ll_progress;
    LoginInfoModel loginInfoModel;
    private LinearLayout mEmptyView;
    GetNewThings mGetNewThings;
    private int msg_list_request_count;
    private boolean needUpdate;
    private View networkError;
    private PullToRefreshListView new_somthing;
    private List new_thingIdList;
    private NewThingsAdapter nta;
    private ProgressBar progressBar;
    private double timeLine_newThings;
    private View txt_aginRequest;
    private int type_newThings;

    public NewThingFragment()
    {
        msg_list_request_count = 37;
        type_newThings = 0;
        canSaveData = true;
        needUpdate = true;
    }

    private void findViews()
    {
        networkError = findViewById(0x7f0a0079);
        txt_aginRequest = findViewById(0x7f0a00c9);
        ll_progress = (LinearLayout)findViewById(0x7f0a0073);
        new_somthing = (PullToRefreshListView)findViewById(0x7f0a00ca);
        progressBar = (ProgressBar)findViewById(0x7f0a0091);
        mEmptyView = (LinearLayout)LayoutInflater.from(mActivity).inflate(0x7f03007c, new_somthing, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u5173\u6CE8\u7684\u4EBA\u54E6");
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setText("\u60F3\u4E86\u89E3\u5927\u5BB6\u90FD\u5728\u559C\u9A6C\u62C9\u96C5\u73A9\u4EC0\u4E48\uFF1F");
        Button button = (Button)mEmptyView.findViewById(0x7f0a0232);
        button.setText("\u53BB\u5173\u6CE8\u611F\u5174\u8DA3\u7684\u4EBA");
        button.setOnClickListener(new _cls1());
        new_somthing.addFooterView(mEmptyView);
    }

    private void firstRequestFail()
    {
        ll_progress.setVisibility(8);
        networkError.setVisibility(0);
        new_somthing.setVisibility(8);
        mEmptyView.setVisibility(8);
    }

    private String[] getNewThingItems(View view, int i)
    {
        boolean flag = false;
        String as[] = nta.getItem(i);
        curClickPos_byNewThing = i;
        curClickView = view;
        i = view.findViewById(0x7f0a0566).getVisibility();
        view = new ArrayList();
        new_thingIdList = new ArrayList();
        if (i == 8)
        {
            view.add("\u5C55\u5F00");
            new_thingIdList.add("-1");
        } else
        {
            view.add("\u6536\u8D77");
            new_thingIdList.add("-2");
        }
        if (as != null)
        {
            if (as.getType().equals("fc"))
            {
                String s = as.getNickName();
                long l = as.getUid().longValue();
                String s2 = ((ChildFeedModel)as.getChildFeedList().get(0)).getToNickName();
                if (s.equals(s2))
                {
                    view.add((new StringBuilder()).append("\u67E5\u770B").append(s).append("\u7684\u8D44\u6599").toString());
                    new_thingIdList.add((new StringBuilder()).append(as.getUid()).append("").toString());
                } else
                {
                    view.add((new StringBuilder()).append("\u67E5\u770B").append(s).append("\u7684\u8D44\u6599").toString());
                    new_thingIdList.add((new StringBuilder()).append(l).append("").toString());
                    view.add((new StringBuilder()).append("\u67E5\u770B").append(s2).append("\u7684\u8D44\u6599").toString());
                    new_thingIdList.add((new StringBuilder()).append(((ChildFeedModel)as.getChildFeedList().get(0)).toUid).append("").toString());
                }
                view.add("\u56DE\u590D");
                new_thingIdList.add("-5");
            } else
            {
                String s1 = as.getNickName();
                as = (new StringBuilder()).append(as.getUid()).append("").toString();
                view.add((new StringBuilder()).append("\u67E5\u770B").append(s1).append("\u7684\u8D44\u6599").toString());
                new_thingIdList.add(as);
            }
        }
        view.add("\u5220\u9664");
        new_thingIdList.add("-3");
        as = new String[view.size()];
        for (i = ((flag) ? 1 : 0); i < view.size(); i++)
        {
            as[i] = (String)view.get(i);
        }

        return as;
    }

    private void getNewThings(View view)
    {
        if (mGetNewThings == null || mGetNewThings.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mGetNewThings = new GetNewThings();
            mGetNewThings.myexec(new Object[] {
                view
            });
        }
    }

    private void initData()
    {
    }

    private void initViews()
    {
        txt_aginRequest.setOnClickListener(new _cls2());
        new_somthing.setOnRefreshListener(new _cls3());
        new_somthing.setMyScrollListener2(new _cls4());
        new_somthing.setOnItemClickListener(new _cls5());
    }

    private void makeToast(String s)
    {
        Toast.makeText(mCon, s, 1).show();
    }

    private void saveSomthing()
    {
    }

    private void showDataRight()
    {
        ll_progress.setVisibility(8);
        networkError.setVisibility(8);
        mEmptyView.setVisibility(8);
    }

    private void showEmptyView(boolean flag)
    {
        if (flag)
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(0);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(0);
            mEmptyView.findViewById(0x7f0a0232).setVisibility(0);
            mEmptyView.setVisibility(0);
            return;
        } else
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0232).setVisibility(8);
            mEmptyView.setVisibility(8);
            return;
        }
    }

    public void loadData()
    {
        if (needUpdate)
        {
            needUpdate = false;
            new_somthing.toRefreshing();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        if (loginInfoModel == null)
        {
            Toast.makeText(mCon, "\u6CA1\u6709\u67E5\u5230\u767B\u5F55\u4FE1\u606F~", 0).show();
            return;
        } else
        {
            setTitleText("\u65B0\u9C9C\u4E8B");
            initData();
            findViews();
            initViews();
            return;
        }
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (i == 175)
        {
            System.out.println("onActivityResultonActivityResultonActivityResultonActivityResult");
            if (j == -1)
            {
                Toast.makeText(getActivity(), "\u56DE\u590D\u6210\u529F", 1).show();
            }
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03001f, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void onPause()
    {
        super.onPause();
        if (canSaveData)
        {
            saveSomthing();
        }
    }

    public void onRefresh()
    {
        super.onRefresh();
        new_somthing.toRefreshing();
    }

    public void onResume()
    {
        super.onResume();
        onRefresh();
    }





/*
    static int access$102(NewThingFragment newthingfragment, int i)
    {
        newthingfragment.type_newThings = i;
        return i;
    }

*/








/*
    static double access$202(NewThingFragment newthingfragment, double d)
    {
        newthingfragment.timeLine_newThings = d;
        return d;
    }

*/






/*
    static NewThingsAdapter access$602(NewThingFragment newthingfragment, NewThingsAdapter newthingsadapter)
    {
        newthingfragment.nta = newthingsadapter;
        return newthingsadapter;
    }

*/




    private class _cls1
        implements android.view.View.OnClickListener
    {

        final NewThingFragment this$0;

        public void onClick(View view)
        {
            (new Bundle()).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/findings/FindingHotStationCategoryFragment, null);
        }

        _cls1()
        {
            this$0 = NewThingFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final NewThingFragment this$0;

        public void onClick(View view)
        {
            type_newThings = 0;
            getNewThings(view);
        }

        _cls2()
        {
            this$0 = NewThingFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final NewThingFragment this$0;

        public void onRefresh()
        {
            type_newThings = 0;
            getNewThings(new_somthing);
        }

        _cls3()
        {
            this$0 = NewThingFragment.this;
            super();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnScrollListner
    {

        final NewThingFragment this$0;

        public void onMyScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0 && nta != null && nta.getCount() != 0 && new_somthing.getLastVisiblePosition() == nta.getCount() && nta.getCount() % msg_list_request_count == 0)
            {
                type_newThings = -1;
                timeLine_newThings = ((BaseFeedModel)nta.getMfc().getBaseFeedList().get(nta.getCount() - 1)).getTimeLine().doubleValue();
                nta.getMfc().getBaseFeedList().add((new BaseFeedModel()).setFlag(false));
                nta.notifyDataSetChanged();
                new_somthing.setSelection(nta.getCount() - 1);
                getNewThings(new_somthing);
            }
        }

        _cls4()
        {
            this$0 = NewThingFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AdapterView.OnItemClickListener
    {

        final NewThingFragment this$0;

        public void onItemClick(final AdapterView dialog, final View view, int i, long l)
        {
            if (nta != null)
            {
                if ((dialog = nta.getItem(i)) == null || !((BaseFeedModel) (dialog)).type.equals("ft"))
                {
                    dialog = new MenuDialog(getActivity(), Arrays.asList(getNewThingItems(view, i)));
                    class _cls1
                        implements android.widget.AdapterView.OnItemClickListener
                    {

                        final _cls5 this$1;
                        final MenuDialog val$dialog;
                        final View val$view;

                        public void onItemClick(AdapterView adapterview, View view1, int j, long l1)
                        {
                            if (dialog != null)
                            {
                                dialog.dismiss();
                            }
                            adapterview = (String)new_thingIdList.get(j);
                            if (adapterview.equals("-1") || adapterview.equals("-2") || adapterview.equals("-3") || adapterview.equals("-5")) goto _L2; else goto _L1
_L1:
                            if (!adapterview.equals((new StringBuilder()).append(loginInfoModel.uid).append("").toString())) goto _L4; else goto _L3
_L3:
                            return;
_L4:
                            view1 = new Bundle();
                            view1.putLong("toUid", Long.valueOf(adapterview).longValue());
                            view1.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                            startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, view1);
                            return;
_L2:
                            if (adapterview.equals("-1"))
                            {
                                ((com.ximalaya.ting.android.adapter.NewThingsAdapter.Careone_commentHolder)curClickView.getTag()).tb_careone_arrow.performClick();
                                return;
                            }
                            if (adapterview.equals("-2"))
                            {
                                ((com.ximalaya.ting.android.adapter.NewThingsAdapter.Careone_commentHolder)curClickView.getTag()).tb_careone_arrow.performClick();
                                return;
                            }
                            if (adapterview.equals("-3"))
                            {
                                adapterview = nta.getItem(curClickPos_byNewThing);
                                (new DelNewThing()).myexec(new Object[] {
                                    adapterview.getId(), adapterview.getType(), view
                                });
                                nta.getMfc().getBaseFeedList().remove(curClickPos_byNewThing - 1);
                                if (nta.showList != null)
                                {
                                    nta.showList.remove((new StringBuilder()).append(curClickPos_byNewThing - 1).append("").toString());
                                }
                                nta.notifyDataSetChanged();
                                return;
                            }
                            if (adapterview.equals("-5"))
                            {
                                adapterview = nta.getItem(curClickPos_byNewThing);
                                view1 = new Intent(mCon, com/ximalaya/ting/android/activity/comments/Send_CommentAct);
                                view1.putExtra("trackId", ((ChildFeedModel)adapterview.getChildFeedList().get(0)).toTid);
                                view1.putExtra("parentId", ((ChildFeedModel)adapterview.getChildFeedList().get(0)).getCid());
                                view1.putExtra("nickName", adapterview.getNickName());
                                startActivityForResult(view1, 175);
                                return;
                            }
                            if (true) goto _L3; else goto _L5
_L5:
                        }

                _cls1()
                {
                    this$1 = _cls5.this;
                    dialog = menudialog;
                    view = view1;
                    super();
                }
                    }

                    dialog.setOnItemClickListener(new _cls1());
                    dialog.show();
                    return;
                }
            }
        }

        _cls5()
        {
            this$0 = NewThingFragment.this;
            super();
        }
    }

}
