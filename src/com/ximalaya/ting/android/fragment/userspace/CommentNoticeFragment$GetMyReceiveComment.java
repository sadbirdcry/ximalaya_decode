// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.adapter.NoticeAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.message.CommentListInCommentNotice;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            CommentNoticeFragment

class this._cls0 extends MyAsyncTask
{

    CommentListInCommentNotice cicn;
    final CommentNoticeFragment this$0;

    protected transient Integer doInBackground(Object aobj[])
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("pageSize", (new StringBuilder()).append(CommentNoticeFragment.access$1200(CommentNoticeFragment.this)).append("").toString());
        requestparams.put("key", CommentNoticeFragment.access$200(CommentNoticeFragment.this));
        requestparams.put("isDown", (new StringBuilder()).append(CommentNoticeFragment.access$300(CommentNoticeFragment.this)).append("").toString());
        aobj = f.a().b(e.d, requestparams, (View)aobj[0], CommentNoticeFragment.access$000(CommentNoticeFragment.this));
        if (Utilities.isNotBlank(((String) (aobj))))
        {
            try
            {
                cicn = (CommentListInCommentNotice)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/message/CommentListInCommentNotice);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[]) { }
            if (cicn != null)
            {
                if (cicn.ret == 0)
                {
                    return Integer.valueOf(3);
                } else
                {
                    return Integer.valueOf(2);
                }
            }
        }
        return Integer.valueOf(1);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        while (!canGoon() || CommentNoticeFragment.access$400(CommentNoticeFragment.this).getCheckedRadioButtonId() != 0x7f0a007d) 
        {
            return;
        }
        class _cls1
            implements Runnable
        {

            final CommentNoticeFragment.GetMyReceiveComment this$1;

            public void run()
            {
                CommentNoticeFragment.access$000(this$0).setAdapter(CommentNoticeFragment.access$100(this$0));
            }

            _cls1()
            {
                this$1 = CommentNoticeFragment.GetMyReceiveComment.this;
                super();
            }
        }

        _cls1 _lcls1;
        if (cicn == null || cicn.list == null || cicn.list.size() <= 0)
        {
            CommentNoticeFragment.access$1100(CommentNoticeFragment.this, true);
        } else
        {
            CommentNoticeFragment.access$1100(CommentNoticeFragment.this, false);
        }
        if (CommentNoticeFragment.access$000(CommentNoticeFragment.this).isRefreshing())
        {
            CommentNoticeFragment.access$000(CommentNoticeFragment.this).onRefreshComplete();
        }
        if (integer.intValue() != 3) goto _L2; else goto _L1
_L1:
        cicn.sendType = "\u6536\u5230\u7684";
        if (CommentNoticeFragment.access$100(CommentNoticeFragment.this) != null) goto _L4; else goto _L3
_L3:
        CommentNoticeFragment.access$102(CommentNoticeFragment.this, new NoticeAdapter(getActivity(), cicn));
        long l = getAnimationLeftTime();
        integer = CommentNoticeFragment.access$000(CommentNoticeFragment.this);
        _lcls1 = new _cls1();
        if (l <= 0L)
        {
            l = 0L;
        }
        integer.postDelayed(_lcls1, l);
        CommentNoticeFragment.access$500(CommentNoticeFragment.this).setVisibility(8);
_L6:
        CommentNoticeFragment.access$802(CommentNoticeFragment.this, false);
        return;
_L4:
        if (CommentNoticeFragment.access$100(CommentNoticeFragment.this) != null && CommentNoticeFragment.access$200(CommentNoticeFragment.this).equals("0"))
        {
            if (cicn != null)
            {
                CommentNoticeFragment.access$100(CommentNoticeFragment.this).setCicn(cicn);
            }
            CommentNoticeFragment.access$100(CommentNoticeFragment.this).notifyDataSetChanged();
        } else
        {
            CommentNoticeFragment.access$100(CommentNoticeFragment.this).getCicn().list.remove(CommentNoticeFragment.access$100(CommentNoticeFragment.this).getCount() - 1);
            if (cicn != null && cicn.list != null)
            {
                cicn.list.addAll(0, CommentNoticeFragment.access$100(CommentNoticeFragment.this).getCicn().list);
                CommentNoticeFragment.access$100(CommentNoticeFragment.this).setCicn(cicn);
                CommentNoticeFragment.access$100(CommentNoticeFragment.this).notifyDataSetChanged();
            }
        }
        continue; /* Loop/switch isn't completed */
_L2:
        if (CommentNoticeFragment.access$100(CommentNoticeFragment.this) == null)
        {
            CommentNoticeFragment.access$1300(CommentNoticeFragment.this);
        } else
        if (CommentNoticeFragment.access$100(CommentNoticeFragment.this) != null && CommentNoticeFragment.access$200(CommentNoticeFragment.this).equals("0"))
        {
            CommentNoticeFragment.access$1400(CommentNoticeFragment.this, getString(0x7f09009b));
        } else
        {
            CommentNoticeFragment.access$100(CommentNoticeFragment.this).getCicn().list.remove(CommentNoticeFragment.access$100(CommentNoticeFragment.this).getCount() - 1);
            CommentNoticeFragment.access$1400(CommentNoticeFragment.this, getString(0x7f09009b));
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        CommentNoticeFragment.access$802(CommentNoticeFragment.this, true);
        CommentNoticeFragment.access$1100(CommentNoticeFragment.this, false);
    }

    _cls1()
    {
        this$0 = CommentNoticeFragment.this;
        super();
    }
}
