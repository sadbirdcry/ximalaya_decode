// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.model.album.AlbumCollection;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.bounceview.BounceListView;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            OtherSpaceFragment

class this._cls0 extends MyAsyncTask
{

    AlbumCollection ac;
    final OtherSpaceFragment this$0;

    protected transient Integer doInBackground(String as[])
    {
        android.content.Context context = mCon;
        long l;
        if (OtherSpaceFragment.access$2000(OtherSpaceFragment.this) == null)
        {
            l = 0L;
        } else
        {
            l = OtherSpaceFragment.access$2000(OtherSpaceFragment.this).uid;
        }
        if (OtherSpaceFragment.access$2000(OtherSpaceFragment.this) == null)
        {
            as = null;
        } else
        {
            as = OtherSpaceFragment.access$2000(OtherSpaceFragment.this).token;
        }
        as = CommonRequest.doGetMyOrOtherAlbums(context, l, as, OtherSpaceFragment.access$600(OtherSpaceFragment.this), OtherSpaceFragment.access$2200(OtherSpaceFragment.this), 2, fragmentBaseContainerView, OtherSpaceFragment.access$2300(OtherSpaceFragment.this));
        if (Utilities.isNotBlank(as))
        {
            try
            {
                ac = (AlbumCollection)JSON.parseObject(as, com/ximalaya/ting/android/model/album/AlbumCollection);
            }
            // Misplaced declaration of an exception variable
            catch (String as[])
            {
                Logger.e(as);
            }
            if (ac == null)
            {
                return Integer.valueOf(2);
            } else
            {
                return Integer.valueOf(3);
            }
        } else
        {
            return Integer.valueOf(1);
        }
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((String[])aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (canGoon()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        switch (integer.intValue())
        {
        default:
            break;

        case 1: // '\001'
        case 2: // '\002'
            break;

        case 3: // '\003'
            break;
        }
        break; /* Loop/switch isn't completed */
        return;
        if (true) goto _L1; else goto _L3
_L3:
        if (ac.ret != 0) goto _L1; else goto _L4
_L4:
        long l = getAnimationLeftTime();
        integer = OtherSpaceFragment.access$400(OtherSpaceFragment.this);
        class _cls1
            implements Runnable
        {

            final OtherSpaceFragment.GetOtherAlbumList this$1;

            public void run()
            {
                if (!canGoon())
                {
                    return;
                } else
                {
                    OtherSpaceFragment.access$2400(this$0, ac.list, ac.totalCount);
                    return;
                }
            }

            _cls1()
            {
                this$1 = OtherSpaceFragment.GetOtherAlbumList.this;
                super();
            }
        }

        _cls1 _lcls1 = new _cls1();
        if (l <= 0L)
        {
            l = 0L;
        }
        integer.postDelayed(_lcls1, l);
        return;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        showFooterView();
    }

    _cls1()
    {
        this$0 = OtherSpaceFragment.this;
        super();
    }
}
