// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.util.Log;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;

public class RewardStatusModel
{
    public static class RewardStatusLoader extends AsyncTaskLoader
    {

        private RewardStatusModel mData;

        public void deliverResult(RewardStatusModel rewardstatusmodel)
        {
            super.deliverResult(rewardstatusmodel);
            mData = rewardstatusmodel;
        }

        public volatile void deliverResult(Object obj)
        {
            deliverResult((RewardStatusModel)obj);
        }

        public RewardStatusModel loadInBackground()
        {
            com.ximalaya.ting.android.b.n.a a1 = f.a().a((new StringBuilder()).append(a.u).append("mobile/anchor_backend").toString(), null, false);
            if (a1.b == 1 && !TextUtils.isEmpty(a1.a))
            {
                mData = RewardStatusModel.getInstance(a1.a);
            }
            return mData;
        }

        public volatile Object loadInBackground()
        {
            return loadInBackground();
        }

        protected void onStartLoading()
        {
            super.onStartLoading();
            if (mData == null)
            {
                forceLoad();
                return;
            } else
            {
                deliverResult(mData);
                return;
            }
        }

        public RewardStatusLoader(Context context)
        {
            super(context);
        }
    }


    public static final int STATUS_HIDE = 3;
    public static final int STATUS_NO_OPEN = 0;
    public static final int STATUS_OPEN = 1;
    public static final int STATUS_STOP = 2;
    private int rewardStatus;

    public RewardStatusModel()
    {
    }

    public static RewardStatusModel getInstance(String s)
    {
        if (JSONObject.parseObject(s).getIntValue("ret") != 0 || TextUtils.isEmpty(s))
        {
            break MISSING_BLOCK_LABEL_42;
        }
        s = (RewardStatusModel)JSONObject.parseObject(s, com/ximalaya/ting/android/fragment/userspace/RewardStatusModel);
        return s;
        s;
        Log.e("ManageCenterFragment", s.toString());
        return null;
    }

    public int getRewardStatus()
    {
        return rewardStatus;
    }

    public void setRewardStatus(int i)
    {
        rewardStatus = i;
    }
}
