// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.app.ProgressDialog;
import android.view.View;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.adapter.NoticeAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.message.CommentListInCommentNotice;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            CommentNoticeFragment

class pos extends MyAsyncTask
{

    ProgressDialog pd;
    int pos;
    BaseModel rr;
    final CommentNoticeFragment this$0;

    protected transient Integer doInBackground(Object aobj[])
    {
        pos = Integer.valueOf((String)aobj[3]).intValue();
        RequestParams requestparams = new RequestParams();
        String s;
        if ((String)aobj[0] == null)
        {
            s = "";
        } else
        {
            s = (String)aobj[0];
        }
        requestparams.put("commentId", s);
        if ((String)aobj[1] == null)
        {
            s = "";
        } else
        {
            s = (String)aobj[1];
        }
        requestparams.put("trackId", s);
        if ((String)aobj[2] == null)
        {
            s = "";
        } else
        {
            s = (String)aobj[2];
        }
        requestparams.put("messageId", s);
        aobj = f.a().b(e.q, requestparams, (View)aobj[4], null);
        if (Utilities.isNotBlank(((String) (aobj))))
        {
            rr = (BaseModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/BaseModel);
            if (rr != null)
            {
                if (rr.ret == 0)
                {
                    return Integer.valueOf(3);
                } else
                {
                    return Integer.valueOf(2);
                }
            }
        }
        return Integer.valueOf(1);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (canGoon())
        {
            if (pd != null)
            {
                pd.cancel();
                pd = null;
            }
            if (integer.intValue() == 3)
            {
                if (pos != 0)
                {
                    CommentNoticeFragment.access$100(CommentNoticeFragment.this).getCicn().list.remove(pos - 1);
                    CommentNoticeFragment.access$100(CommentNoticeFragment.this).notifyDataSetChanged();
                    return;
                }
            } else
            if (integer.intValue() == 2)
            {
                Toast.makeText(mCon, rr.msg, 0).show();
                return;
            } else
            {
                Toast.makeText(mCon, "\u5220\u9664\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5~", 0).show();
                return;
            }
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        pd = new MyProgressDialog(getActivity());
        pd.setMessage("\u6B63\u5728\u5220\u9664...");
        pd.show();
    }

    Y()
    {
        this$0 = CommentNoticeFragment.this;
        super();
        pos = 0;
    }
}
