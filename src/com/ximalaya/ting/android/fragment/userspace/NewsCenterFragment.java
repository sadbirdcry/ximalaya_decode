// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.umeng.analytics.MobclickAgent;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.activity.comments.CarePersonListAct;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.adapter.PrivateMsgAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.zone.ZoneMessageFragment;
import com.ximalaya.ting.android.kdt.KDTAction;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.NoReadModel;
import com.ximalaya.ting.android.model.message.PrivateMsgModel;
import com.ximalaya.ting.android.model.message.PrivateMsgModelList;
import com.ximalaya.ting.android.model.personal_info.HomePageModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.NoReadManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.SharedPreferencesUserUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            NoticePageFragment, CommentNoticeFragment

public class NewsCenterFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.modelmanage.NoReadManage.NoReadUpdateListener
{
    class DelPrivateMsgListByUid extends MyAsyncTask
    {

        BaseModel rr;
        final NewsCenterFragment this$0;

        protected transient Integer doInBackground(Object aobj[])
        {
            Object obj;
            obj = new RequestParams();
            ((RequestParams) (obj)).put("toUid", (String)aobj[0]);
            aobj = (View)aobj[1];
            obj = f.a().b(e.m, ((RequestParams) (obj)), ((View) (aobj)), private_msg);
            if (Utilities.isBlank(((String) (obj))))
            {
                return Integer.valueOf(1);
            }
            aobj = null;
            obj = JSON.parseObject(((String) (obj)));
            aobj = ((Object []) (obj));
_L1:
            Exception exception;
            if (aobj != null)
            {
                rr = new BaseModel();
                rr.ret = ((JSONObject) (aobj)).getInteger("ret").intValue();
                if (rr.ret != -1)
                {
                    rr.msg = ((JSONObject) (aobj)).getString("msg");
                }
                return Integer.valueOf(3);
            } else
            {
                return Integer.valueOf(2);
            }
            exception;
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
              goto _L1
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (!canGoon())
            {
                return;
            }
            if (integer.intValue() == 3)
            {
                if (rr.ret == 0)
                {
                    if (curClickPos_byPrivateMsg - 2 != 0 && curClickPos_byPrivateMsg - 2 != 1)
                    {
                        if (pmaAdapter.pmmList.size() >= curClickPos_byPrivateMsg - 1)
                        {
                            pmaAdapter.pmmList.remove(curClickPos_byPrivateMsg - 2);
                        }
                        pmaAdapter.notifyDataSetChanged();
                        return;
                    }
                    if (pmaAdapter.pmmList.size() >= curClickPos_byPrivateMsg - 1)
                    {
                        ((PrivateMsgModel)pmaAdapter.pmmList.get(curClickPos_byPrivateMsg - 2)).setContent("");
                    }
                    ((TextView)(TextView)private_msg.getChildAt(curClickPos_byPrivateMsg).findViewById(0x7f0a056d)).setText("");
                    return;
                } else
                {
                    Toast.makeText(mCon, rr.msg, 0).show();
                    return;
                }
            } else
            {
                Toast.makeText(mCon, "\u5220\u9664\u5931\u8D25", 0).show();
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        DelPrivateMsgListByUid()
        {
            this$0 = NewsCenterFragment.this;
            super();
        }
    }

    class GetHomePageInfo extends MyAsyncTask
    {

        HomePageModel homeModel2;
        final NewsCenterFragment this$0;

        protected transient Integer doInBackground(String as[])
        {
            Object obj = null;
            as = f.a().a(e.E, null, fragmentBaseContainerView, fragmentBaseContainerView, false);
            com.ximalaya.ting.android.b.n.a a1;
            if (((com.ximalaya.ting.android.b.n.a) (as)).b == 1)
            {
                as = ((com.ximalaya.ting.android.b.n.a) (as)).a;
            } else
            {
                as = null;
            }
            a1 = f.a().a(e.T, null, fragmentBaseContainerView, null, false);
            if (a1.b == 1)
            {
                obj = a1.a;
            }
            if (Utilities.isBlank(as))
            {
                return Integer.valueOf(1);
            }
            obj = (NoReadModel)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/NoReadModel);
            homeModel2 = (HomePageModel)JSON.parseObject(as, com/ximalaya/ting/android/model/personal_info/HomePageModel);
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_131;
            }
            if (((NoReadModel) (obj)).ret == 0)
            {
                homeModel2.favoriteAlbumIsUpdate = ((NoReadModel) (obj)).favoriteAlbumIsUpdate;
            }
            if (UserInfoMannage.hasLogined())
            {
                UserInfoMannage.getInstance().getUser().isVerified = homeModel2.isVerified();
            }
_L1:
            if (homeModel2 == null)
            {
                return Integer.valueOf(2);
            } else
            {
                return Integer.valueOf(3);
            }
            as;
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(as.getMessage()).append(Logger.getLineInfo()).toString());
              goto _L1
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (isAdded() && mCon != null && integer.intValue() == 3 && homeModel2 != null)
            {
                if (homeModel2.ret == 0)
                {
                    updateMyspaceView(homeModel2);
                    return;
                }
                if (mCon != null && homeModel != null && Utilities.isNotBlank(homeModel.msg))
                {
                    Toast.makeText(mCon, homeModel.msg, 0).show();
                    return;
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        GetHomePageInfo()
        {
            this$0 = NewsCenterFragment.this;
            super();
        }
    }

    class GetMyPrivateMsg extends MyAsyncTask
    {

        PrivateMsgModelList pmml;
        final NewsCenterFragment this$0;

        protected transient Integer doInBackground(String as[])
        {
            as = new RequestParams();
            as.put("uid", (new StringBuilder()).append(loginInfoModel.uid).append("").toString());
            as.put("token", loginInfoModel.token);
            as.put("pageSize", (new StringBuilder()).append(msg_list_request_count).append("").toString());
            as.put("isDown", (new StringBuilder()).append(privateMsg_isDown).append("").toString());
            as.put("key", privateMsg_key);
            as = f.a().b(e.i, as, private_msg, private_msg);
            if (Utilities.isBlank(as))
            {
                return Integer.valueOf(1);
            }
            try
            {
                pmml = (PrivateMsgModelList)JSON.parseObject(as, com/ximalaya/ting/android/model/message/PrivateMsgModelList);
                if (pmml != null)
                {
                    return Integer.valueOf(3);
                }
            }
            // Misplaced declaration of an exception variable
            catch (String as[])
            {
                return Integer.valueOf(2);
            }
            return Integer.valueOf(2);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (canGoon())
            {
                if (private_msg.isRefreshing())
                {
                    private_msg.onRefreshComplete();
                }
                if (integer.intValue() == 3)
                {
                    if (pmaAdapter == null)
                    {
                        pmaAdapter = new PrivateMsgAdapter(NewsCenterFragment.this);
                        long l = getAnimationLeftTime();
                        integer = private_msg;
                        class _cls1
                            implements Runnable
                        {

                            final GetMyPrivateMsg this$1;

                            public void run()
                            {
                                private_msg.setAdapter(pmaAdapter);
                            }

                _cls1()
                {
                    this$1 = GetMyPrivateMsg.this;
                    super();
                }
                        }

                        _cls1 _lcls1 = new _cls1();
                        if (l <= 0L)
                        {
                            l = 0L;
                        }
                        integer.postDelayed(_lcls1, l);
                        ll_progress.setVisibility(8);
                        if (pmml != null)
                        {
                            pmaAdapter.pmmList = pmml.list;
                            return;
                        }
                    } else
                    if (pmaAdapter != null && privateMsg_key.equals("0"))
                    {
                        if (pmml != null)
                        {
                            pmaAdapter.pmmList = pmml.list;
                            pmaAdapter.notifyDataSetChanged();
                            return;
                        }
                    } else
                    {
                        pmaAdapter.pmmList.remove(pmaAdapter.pmmList.size() - 1);
                        if (pmml.list != null)
                        {
                            pmaAdapter.pmmList.addAll(pmml.list);
                        }
                        pmaAdapter.notifyDataSetChanged();
                        return;
                    }
                } else
                {
                    if (pmaAdapter == null)
                    {
                        firstRequestFail();
                        return;
                    }
                    if (pmaAdapter != null && privateMsg_key.equals("0"))
                    {
                        makeToast(getString(0x7f09009b));
                        return;
                    } else
                    {
                        pmaAdapter.pmmList.remove(pmaAdapter.pmmList.size() - 1);
                        makeToast(getString(0x7f09009b));
                        return;
                    }
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        GetMyPrivateMsg()
        {
            this$0 = NewsCenterFragment.this;
            super();
        }
    }


    private static final String MySpaceFragment_HomeModel = "MySpaceFragment_HomeModel";
    private final int GO_PRIVATE_ACT = 2575;
    private final int GO_SEND_COMMENT = 175;
    private final Context context;
    private int curClickPos_byPrivateMsg;
    private volatile boolean firstShow;
    private HomePageModel homeModel;
    private LayoutInflater inflater;
    private LinearLayout ll_progress;
    private View mComment;
    private ImageView mCommentArrow;
    private LinearLayout mHeaderView;
    private LoginInfoModel mLoginInfoModel;
    private TextView mNewCommentNum;
    private TextView mNewNoticeNum;
    private TextView mNewZoneMessageNum;
    private View mNotice;
    private ImageView mNoticeArrow;
    private LinearLayout mRl_comment;
    private LinearLayout mRl_notice;
    private LinearLayout mRl_zoneMessage;
    private ImageView mSendMessage;
    private ImageView mZoneArrow;
    private View mZoneMessage;
    private int msg_list_request_count;
    private View networkError;
    private PrivateMsgAdapter pmaAdapter;
    private boolean privateMsg_isDown;
    private String privateMsg_key;
    private PullToRefreshListView private_msg;

    public NewsCenterFragment()
    {
        context = mCon;
        firstShow = true;
        msg_list_request_count = 37;
        privateMsg_isDown = false;
        privateMsg_key = "0";
    }

    private void firstRequestFail()
    {
        ll_progress.setVisibility(8);
        networkError.setVisibility(0);
        private_msg.setVisibility(8);
    }

    private void initData()
    {
        mLoginInfoModel = UserInfoMannage.getInstance().getUser();
        KDTAction.isMyOrdersShow = Boolean.parseBoolean(MobclickAgent.getConfigParams(getActivity(), "myorders_show"));
    }

    private void initView()
    {
        networkError = findViewById(0x7f0a0079);
        ll_progress = (LinearLayout)findViewById(0x7f0a0073);
        private_msg = (PullToRefreshListView)findViewById(0x7f0a00da);
        mHeaderView = new LinearLayout(mCon);
        mHeaderView.setOrientation(1);
        mSendMessage = (ImageView)findViewById(0x7f0a0714);
        mComment = inflater.inflate(0x7f030178, null);
        mRl_comment = (LinearLayout)mComment.findViewById(0x7f0a05c2);
        mNewCommentNum = (TextView)mComment.findViewById(0x7f0a05c3);
        mNewCommentNum.setVisibility(8);
        mCommentArrow = (ImageView)mComment.findViewById(0x7f0a05c4);
        mCommentArrow.setVisibility(8);
        mHeaderView.addView(mComment);
        mZoneMessage = inflater.inflate(0x7f03017a, null);
        mRl_zoneMessage = (LinearLayout)mZoneMessage.findViewById(0x7f0a05c8);
        mNewZoneMessageNum = (TextView)mZoneMessage.findViewById(0x7f0a05c9);
        mNewZoneMessageNum.setVisibility(8);
        mZoneArrow = (ImageView)mZoneMessage.findViewById(0x7f0a05ca);
        mZoneArrow.setVisibility(8);
        mHeaderView.addView(mZoneMessage);
        private_msg.addHeaderView(mHeaderView);
    }

    private void initViewListener()
    {
        networkError.setOnClickListener(this);
        mRl_comment.setOnClickListener(this);
        mRl_zoneMessage.setOnClickListener(this);
        mSendMessage.setOnClickListener(this);
        private_msg.setOnItemLongClickListener(new _cls1());
        private_msg.setOnItemClickListener(new _cls2());
        private_msg.setOnRefreshListener(new _cls3());
        private_msg.setMyScrollListener2(new _cls4());
        NoReadManage.getInstance().setOnNoReadUpdateListenerListener(this);
    }

    private void makeToast(String s)
    {
        Toast.makeText(mCon, s, 1).show();
    }

    private void readCacheFromLocal()
    {
        String s;
        if (mLoginInfoModel == null || !firstShow)
        {
            break MISSING_BLOCK_LABEL_109;
        }
        firstShow = false;
        s = SharedPreferencesUserUtil.getInstance(mCon, (new StringBuilder()).append(mLoginInfoModel.uid).append("").toString()).getString("MySpaceFragment_HomeModel");
        if (!Utilities.isNotBlank(s))
        {
            break MISSING_BLOCK_LABEL_109;
        }
        homeModel = (HomePageModel)JSON.parseObject(s, com/ximalaya/ting/android/model/personal_info/HomePageModel);
        if (UserInfoMannage.hasLogined())
        {
            UserInfoMannage.getInstance().getUser().isVerified = homeModel.isVerified();
        }
        updateMyspaceView(homeModel);
        return;
        Exception exception;
        exception;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
        return;
    }

    private void updateHomePageInfo()
    {
        (new GetHomePageInfo()).myexec(new String[0]);
    }

    private void updateMyspaceView(HomePageModel homepagemodel)
    {
        homeModel = homepagemodel;
        if (homeModel.messages <= 0)
        {
            mNewCommentNum.setVisibility(8);
            mCommentArrow.setVisibility(0);
        } else
        {
            mCommentArrow.setVisibility(8);
            mNewCommentNum.setVisibility(0);
            TextView textview = mNewCommentNum;
            if (homeModel.messages > 99)
            {
                homepagemodel = "N";
            } else
            {
                homepagemodel = (new StringBuilder()).append("").append(homeModel.messages).toString();
            }
            textview.setText(homepagemodel);
        }
        if (homeModel.newZoneCommentCount <= 0)
        {
            mZoneArrow.setVisibility(0);
            mNewZoneMessageNum.setVisibility(8);
            return;
        }
        mZoneArrow.setVisibility(8);
        mNewZoneMessageNum.setVisibility(0);
        TextView textview1 = mNewZoneMessageNum;
        if (homeModel.newZoneCommentCount > 99)
        {
            homepagemodel = "N";
        } else
        {
            homepagemodel = (new StringBuilder()).append("").append(homeModel.newZoneCommentCount).toString();
        }
        textview1.setText(homepagemodel);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        inflater = LayoutInflater.from(getActivity());
        initData();
        initView();
        initViewListener();
        (new GetMyPrivateMsg()).myexec(new String[0]);
        (new GetHomePageInfo()).myexec(new String[0]);
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (i != 175) goto _L2; else goto _L1
_L1:
        if (j == -1)
        {
            Toast.makeText(mCon, "\u56DE\u590D\u6210\u529F", 1).show();
        }
_L4:
        return;
_L2:
        if (i != 2575) goto _L4; else goto _L3
_L3:
        if (j != -1)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (pmaAdapter == null) goto _L4; else goto _L5
_L5:
        String s = null;
        if (intent != null)
        {
            s = intent.getStringExtra("lastMsg");
        }
        intent = pmaAdapter.getItem(curClickPos_byPrivateMsg);
        if (intent != null)
        {
            intent.setNoReadCount(0);
            if (s != null)
            {
                intent.setContent(s);
            }
        }
        pmaAdapter.notifyDataSetChanged();
        return;
        if (j != 22) goto _L4; else goto _L6
_L6:
        finish();
        return;
    }

    public void onClick(View view)
    {
        if (!OneClickHelper.getInstance().onClick(view)) goto _L2; else goto _L1
_L1:
        int i;
        i = view.getId();
        if (0x7f0a0079 == i)
        {
            ll_progress.setVisibility(0);
            private_msg.setVisibility(0);
            networkError.setVisibility(8);
            private_msg.toRefreshing();
        }
        if (UserInfoMannage.getInstance().getUser() != null) goto _L4; else goto _L3
_L3:
        i;
        JVM INSTR lookupswitch 4: default 104
    //                   2131363266: 111
    //                   2131363269: 132
    //                   2131363272: 111
    //                   2131363604: 111;
           goto _L5 _L6 _L7 _L6 _L6
_L5:
        NoReadManage.getInstance().updateNoReadManageManual();
_L2:
        return;
_L6:
        startActivity(new Intent(getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity));
        continue; /* Loop/switch isn't completed */
_L7:
        startFragment(new NoticePageFragment());
        if (true) goto _L5; else goto _L4
_L4:
        Object obj = NoReadManage.getInstance().getNoReadModel();
        switch (i)
        {
        case 2131363266: 
            new Bundle();
            mNewCommentNum.setVisibility(8);
            if (homeModel != null)
            {
                homeModel.messages = 0;
            }
            if (obj != null)
            {
                obj.messages = 0;
            }
            ToolUtil.onEvent(getActivity(), "Account_MyComment");
            obj = new Bundle();
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/CommentNoticeFragment, ((Bundle) (obj)));
            break;

        case 2131363272: 
            mNewZoneMessageNum.setVisibility(8);
            if (homeModel != null)
            {
                homeModel.newZoneCommentCount = 0;
            }
            if (obj != null)
            {
                obj.newZoneCommentCount = 0;
            }
            obj = new Bundle();
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/ZoneMessageFragment, ((Bundle) (obj)));
            break;

        case 2131363269: 
            if (a.a)
            {
                a.a = false;
            }
            startFragment(new NoticePageFragment());
            break;

        case 2131363604: 
            view = new Intent(getActivity().getApplicationContext(), com/ximalaya/ting/android/activity/comments/CarePersonListAct);
            view.putExtra("title", "\u5199\u79C1\u4FE1");
            LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
            if (logininfomodel != null)
            {
                view.putExtra("meHeadUrl", logininfomodel.getSmallLogo());
            }
            startActivity(view);
            break;
        }
        if (true) goto _L5; else goto _L8
_L8:
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030023, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        NoReadManage.getInstance().removeNoReadUpdateListenerListener(this);
        if (pmaAdapter != null)
        {
            pmaAdapter.releseData();
        }
        super.onDestroyView();
    }

    public void onResume()
    {
        super.onResume();
        readCacheFromLocal();
        updateHomePageInfo();
        (new GetMyPrivateMsg()).myexec(new String[0]);
    }

    public void update(NoReadModel noreadmodel)
    {
        if (!isAdded() || noreadmodel == null)
        {
            return;
        }
        if (noreadmodel.messages <= 0)
        {
            mNewCommentNum.setVisibility(8);
            mCommentArrow.setVisibility(0);
        } else
        {
            mCommentArrow.setVisibility(8);
            mNewCommentNum.setVisibility(0);
            TextView textview1 = mNewCommentNum;
            String s;
            if (noreadmodel.messages > 99)
            {
                s = "N";
            } else
            {
                s = (new StringBuilder()).append("").append(noreadmodel.messages).toString();
            }
            textview1.setText(s);
        }
        if (noreadmodel.newZoneCommentCount <= 0)
        {
            mNewZoneMessageNum.setVisibility(8);
            mZoneArrow.setVisibility(0);
            return;
        }
        mZoneArrow.setVisibility(8);
        mNewZoneMessageNum.setVisibility(0);
        TextView textview = mNewZoneMessageNum;
        if (noreadmodel.newZoneCommentCount > 99)
        {
            noreadmodel = "N";
        } else
        {
            noreadmodel = (new StringBuilder()).append("").append(noreadmodel.newZoneCommentCount).toString();
        }
        textview.setText(noreadmodel);
    }



/*
    static int access$002(NewsCenterFragment newscenterfragment, int i)
    {
        newscenterfragment.curClickPos_byPrivateMsg = i;
        return i;
    }

*/




/*
    static PrivateMsgAdapter access$102(NewsCenterFragment newscenterfragment, PrivateMsgAdapter privatemsgadapter)
    {
        newscenterfragment.pmaAdapter = privatemsgadapter;
        return privatemsgadapter;
    }

*/



/*
    static String access$202(NewsCenterFragment newscenterfragment, String s)
    {
        newscenterfragment.privateMsg_key = s;
        return s;
    }

*/





/*
    static boolean access$502(NewsCenterFragment newscenterfragment, boolean flag)
    {
        newscenterfragment.privateMsg_isDown = flag;
        return flag;
    }

*/





    private class _cls1
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final NewsCenterFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, final View arg1, final int arg2, long l)
        {
            if (l != -1L && arg2 > 3)
            {
                class _cls1
                    implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                {

                    final _cls1 this$1;
                    final View val$arg1;
                    final int val$arg2;

                    public void onExecute()
                    {
                        curClickPos_byPrivateMsg = arg2;
                        if (pmaAdapter.getItem(arg2) != null)
                        {
                            (new DelPrivateMsgListByUid()).myexec(new Object[] {
                                (new StringBuilder()).append(pmaAdapter.getItem(arg2).getLinkmanUid()).append("").toString(), arg1
                            });
                        }
                    }

                _cls1()
                {
                    this$1 = _cls1.this;
                    arg2 = i;
                    arg1 = view;
                    super();
                }
                }

                (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u5220\u9664\u4E0E\u6B64\u4EBA\u7684\u5168\u90E8\u79C1\u4FE1\uFF1F").setOkBtn(new _cls1()).showConfirm();
                return true;
            } else
            {
                return false;
            }
        }

        _cls1()
        {
            this$0 = NewsCenterFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final NewsCenterFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            curClickPos_byPrivateMsg = i;
            Intent intent = new Intent(getActivity(), com/ximalaya/ting/android/activity/homepage/TalkViewAct);
            adapterview = pmaAdapter.getItem(i);
            if (adapterview != null)
            {
                if (i == 2)
                {
                    intent.putExtra("isSystemMsg", true);
                }
                if (i == 2 || i == 3)
                {
                    intent.putExtra("isForbidTalk", true);
                    intent.putExtra("isOfficialAccount", true);
                }
                intent.putExtra("title", adapterview.getLinkmanNickname());
                intent.putExtra("toUid", adapterview.getLinkmanUid());
                if (loginInfoModel == null)
                {
                    adapterview = "";
                } else
                {
                    adapterview = loginInfoModel.smallLogo;
                }
                intent.putExtra("meHeadUrl", adapterview);
                intent.putExtra("position", i);
                intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startActivityForResult(intent, 2575);
            }
        }

        _cls2()
        {
            this$0 = NewsCenterFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final NewsCenterFragment this$0;

        public void onRefresh()
        {
            privateMsg_key = "0";
            (new GetMyPrivateMsg()).myexec(new String[0]);
            (new GetHomePageInfo()).myexec(new String[0]);
        }

        _cls3()
        {
            this$0 = NewsCenterFragment.this;
            super();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnScrollListner
    {

        final NewsCenterFragment this$0;

        public void onMyScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0 && pmaAdapter != null && private_msg.getLastVisiblePosition() == pmaAdapter.getCount() && pmaAdapter.getCount() - 3 != 0 && (pmaAdapter.getCount() - 3) % msg_list_request_count == 0)
            {
                privateMsg_key = (new StringBuilder()).append(((PrivateMsgModel)pmaAdapter.pmmList.get(pmaAdapter.pmmList.size() - 1)).getUpdatedAt()).append("").toString();
                privateMsg_isDown = true;
                abslistview = new PrivateMsgModel();
                abslistview.flag = false;
                pmaAdapter.pmmList.add(abslistview);
                pmaAdapter.notifyDataSetChanged();
                private_msg.setSelection(pmaAdapter.getCount() - 1);
                (new GetMyPrivateMsg()).myexec(new String[0]);
            }
        }

        _cls4()
        {
            this$0 = NewsCenterFragment.this;
            super();
        }
    }

}
