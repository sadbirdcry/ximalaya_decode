// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.adapter.BaseListSoundsAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.holder.SoundItemHolderNew;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.transaction.d.z;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class SoundListFragmentNew extends BaseListFragment
{
    class DeleteTask extends MyAsyncTask
    {

        private RecordingModel model;
        final SoundListFragmentNew this$0;

        protected transient Integer doInBackground(RecordingModel arecordingmodel[])
        {
            model = arecordingmodel[0];
            arecordingmodel = new RequestParams();
            arecordingmodel.put("trackId", (new StringBuilder()).append("").append(model.trackId).toString());
            class _cls1ResultWrapper
            {

                public String errorCode;
                public String msg;
                public int ret;
                final DeleteTask this$1;
                public long trackId;

                _cls1ResultWrapper()
                {
                    this$1 = DeleteTask.this;
                    super();
                    ret = -1;
                }
            }

            final _cls1ResultWrapper wrapper = new _cls1ResultWrapper();
            class _cls1 extends h
            {

                final DeleteTask this$1;
                final _cls1ResultWrapper val$wrapper;

                public void onBindXDCS(Header aheader[])
                {
                    DataCollectUtil.bindDataToView(aheader, mListView);
                }

                public void onFinish()
                {
                }

                public void onNetError(int i, String s)
                {
                }

                public void onStart()
                {
                }

                public void onSuccess(String s)
                {
                    if (!TextUtils.isEmpty(s))
                    {
                        Object obj = null;
                        try
                        {
                            s = JSON.parseObject(s);
                        }
                        // Misplaced declaration of an exception variable
                        catch (String s)
                        {
                            s.printStackTrace();
                            s = obj;
                        }
                        if (s != null)
                        {
                            wrapper.ret = s.getIntValue("ret");
                            wrapper.msg = s.getString("msg");
                            wrapper.errorCode = s.getString("errorCode");
                            wrapper.trackId = s.getLongValue("trackId");
                            return;
                        }
                    }
                }

                _cls1()
                {
                    this$1 = DeleteTask.this;
                    wrapper = _pcls1resultwrapper;
                    super();
                }
            }

            f.a().a("mobile/track/delete", arecordingmodel, DataCollectUtil.getDataFromView(mListView), false, new _cls1());
            Logger.e("SoundListFragmentNew", (new StringBuilder()).append("Delete sound result ").append(wrapper.ret).append("/").append(wrapper.msg).append("/").append(wrapper.errorCode).toString());
            return Integer.valueOf(wrapper.ret);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((RecordingModel[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (integer.intValue() != 0)
            {
                showToast("\u5220\u9664\u58F0\u97F3\u5931\u8D25");
                return;
            }
            showToast("\u5220\u9664\u58F0\u97F3\u6210\u529F");
            integer = getThread(model);
            if (integer != null && integer.get() != null)
            {
                ((UpdateState)integer.get()).stopUpdate();
            }
            mThreads.remove(model);
            mUploaded.remove(model);
            mAdapter.notifyDataSetChanged();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        DeleteTask()
        {
            this$0 = SoundListFragmentNew.this;
            super();
        }
    }

    private class LoadNetTask extends MyAsyncTask
    {

        final SoundListFragmentNew this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            mErrMsg = null;
            avoid = new RequestParams();
            avoid.put("pageId", (new StringBuilder()).append("").append(mPageId).toString());
            avoid.put("pageSize", (new StringBuilder()).append("").append(mPageSize).toString());
            final ArrayList data = new ArrayList();
            class _cls1 extends h
            {

                final LoadNetTask this$1;
                final List val$data;

                public void onBindXDCS(Header aheader[])
                {
                    DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
                }

                public void onFinish()
                {
                }

                public void onNetError(int i, String s)
                {
                }

                public void onStart()
                {
                }

                public void onSuccess(String s)
                {
                    if (!TextUtils.isEmpty(s))
                    {
                        try
                        {
                            s = JSON.parseObject(s);
                        }
                        // Misplaced declaration of an exception variable
                        catch (String s)
                        {
                            s.printStackTrace();
                            s = null;
                        }
                        if (s != null)
                        {
                            if (s.getIntValue("ret") != 0)
                            {
                                mErrMsg = s.getString("msg");
                                return;
                            }
                            mTotal = s.getIntValue("totalCount");
                            s = s.getString("list");
                            if (!TextUtils.isEmpty(s))
                            {
                                try
                                {
                                    s = JSON.parseArray(s, com/ximalaya/ting/android/model/sound/RecordingModel);
                                }
                                // Misplaced declaration of an exception variable
                                catch (String s)
                                {
                                    s.printStackTrace();
                                    s = null;
                                }
                                if (s != null && s.size() > 0)
                                {
                                    data.addAll(s);
                                    return;
                                }
                            }
                        }
                    }
                }

                _cls1()
                {
                    this$1 = LoadNetTask.this;
                    data = list;
                    h();
                }
            }

            f.a().a(e.A, avoid, DataCollectUtil.getDataFromView(fragmentBaseContainerView), false, new _cls1());
            return data;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(final List result)
        {
            if (!isCancelled() && mFragment.isAdded() && !
// JavaClassFileOutputException: get_constant: invalid tag

        protected void onPreExecute()
        {
            onPreExecute();
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
            mFooterViewLoading.setOnClickListener(null);
        }

        private LoadNetTask()
        {
            this$0 = SoundListFragmentNew.this;
            super();
        }

        LoadNetTask(_cls1 _pcls1)
        {
            this();
        }

    }

    class PlayStatusListener
        implements com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
    {

        final SoundListFragmentNew this$0;

        public void onBufferUpdated(int i)
        {
        }

        public void onLogoPlayFinished()
        {
        }

        public void onPlayCompleted()
        {
            mAdapter.notifyDataSetChanged();
        }

        public void onPlayPaused()
        {
            mAdapter.notifyDataSetChanged();
        }

        public void onPlayProgressUpdate(int i, int j)
        {
        }

        public void onPlayStarted()
        {
            mAdapter.notifyDataSetChanged();
        }

        public void onPlayerBuffering(boolean flag)
        {
        }

        public void onSoundPrepared(int i)
        {
        }

        public void onStartPlayLogo()
        {
        }

        PlayStatusListener()
        {
            this$0 = SoundListFragmentNew.this;
            super();
        }
    }

    class PlayerListener
        implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
    {

        final SoundListFragmentNew this$0;

        public void onPlayCanceled()
        {
            mAdapter.notifyDataSetChanged();
        }

        public void onSoundChanged(int i)
        {
            mAdapter.notifyDataSetChanged();
        }

        public void onSoundInfoChanged(int i, SoundInfo soundinfo)
        {
        }

        PlayerListener()
        {
            this$0 = SoundListFragmentNew.this;
            super();
        }
    }

    private class SoundAdapter extends BaseListSoundsAdapter
        implements android.widget.AdapterView.OnItemClickListener
    {

        private static final String TAG = "SoundListFragmentNew.SoundAdapter";
        final SoundListFragmentNew this$0;

        private com.ximalaya.ting.android.a.a.a goDownLoad(RecordingModel recordingmodel, View view)
        {
            Object obj = null;
            DownloadTask downloadtask = new DownloadTask(recordingmodel);
            recordingmodel = obj;
            if (downloadtask != null)
            {
                DownLoadTools downloadtools = DownLoadTools.getInstance();
                recordingmodel = downloadtools.goDownload(downloadtask, (MyApplication)mContext.getApplicationContext(), view);
                downloadtools.release();
            }
            return recordingmodel;
        }

        private void parseLocalSound(RecordingModel recordingmodel, SoundItemHolderNew sounditemholdernew)
        {
            sounditemholdernew.relayContainer.setVisibility(8);
            sounditemholdernew.playCount.setVisibility(8);
            sounditemholdernew.likeCount.setVisibility(8);
            sounditemholdernew.commentCount.setVisibility(8);
            sounditemholdernew.playIcon.setVisibility(8);
            sounditemholdernew.download.setVisibility(8);
            sounditemholdernew.status.setVisibility(8);
            sounditemholdernew.soundName.setText(recordingmodel.title);
            sounditemholdernew.userName.setText(mUser.nickname);
            sounditemholdernew.publicTime.setText(ToolUtil.convertTime(recordingmodel.createdAt));
            sounditemholdernew.origin.setText("\u539F\u521B");
            sounditemholdernew.alltime.setText(ToolUtil.toTime(recordingmodel.duration));
            int j;
            if (recordingmodel.covers != null && recordingmodel.covers.length > 0)
            {
                Object obj;
                int i;
                try
                {
                    obj = recordingmodel.covers[0];
                    android.graphics.BitmapFactory.Options options = new Options();
                    options.inSampleSize = 4;
                    obj = BitmapFactory.decodeFile(((String) (obj)), options);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    exception = null;
                }
                if (obj != null)
                {
                    sounditemholdernew.soundCover.setImageBitmap(((android.graphics.Bitmap) (obj)));
                }
            } else
            {
                sounditemholdernew.soundCover.setImageDrawable(getResources().getDrawable(0x7f0202df));
            }
            i = recordingmodel.processState;
            j = recordingmodel.processState & 0xff;
            switch (i & 0xff00)
            {
            default:
                sounditemholdernew.statusContainer.setVisibility(8);
                return;

            case 25600: 
                sounditemholdernew.statusContainer.setVisibility(0);
                sounditemholdernew.statusIcon.setImageResource(0x7f020076);
                sounditemholdernew.statusName.setText("\u6B63\u5728\u4E0A\u4F20");
                sounditemholdernew.progress.setVisibility(0);
                sounditemholdernew.progress.setProgress(0);
                return;

            case 25856: 
                sounditemholdernew.statusContainer.setVisibility(0);
                sounditemholdernew.statusIcon.setImageResource(0x7f020076);
                if (j == 0)
                {
                    sounditemholdernew.statusName.setText("\u4E0A\u4F20\u58F0\u97F3");
                } else
                {
                    sounditemholdernew.statusName.setText((new StringBuilder()).append("\u4E0A\u4F20\u5C01\u9762").append(j).toString());
                }
                sounditemholdernew.progress.setVisibility(0);
                sounditemholdernew.progress.setProgress(recordingmodel.uploadPercent);
                return;

            case 26368: 
                sounditemholdernew.statusContainer.setVisibility(0);
                sounditemholdernew.statusIcon.setImageResource(0x7f020076);
                sounditemholdernew.statusName.setText("\u4E0A\u4F20\u5931\u8D25");
                sounditemholdernew.progress.setVisibility(8);
                return;

            case 26624: 
                sounditemholdernew.statusContainer.setVisibility(0);
                sounditemholdernew.statusIcon.setImageResource(0x7f020076);
                sounditemholdernew.statusName.setText("\u4E0A\u4F20\u6210\u529F");
                sounditemholdernew.progress.setVisibility(8);
                return;
            }
        }

        private void parseNetSound(RecordingModel recordingmodel, SoundItemHolderNew sounditemholdernew)
        {
            sounditemholdernew.progress.setVisibility(8);
            sounditemholdernew.playCount.setVisibility(0);
            sounditemholdernew.likeCount.setVisibility(0);
            sounditemholdernew.commentCount.setVisibility(0);
            sounditemholdernew.playIcon.setVisibility(0);
            sounditemholdernew.download.setVisibility(0);
            sounditemholdernew.soundName.setText(recordingmodel.title);
            sounditemholdernew.userName.setText(mUser.nickname);
            sounditemholdernew.publicTime.setText(ToolUtil.convertTime(recordingmodel.createdAt));
            sounditemholdernew.alltime.setText(ToolUtil.toTime(recordingmodel.duration));
            sounditemholdernew.soundCover.setImageResource(0x7f0202de);
            if (recordingmodel.isRelay)
            {
                sounditemholdernew.origin.setText("\u8F6C\u91C7");
                sounditemholdernew.origin.setTextColor(mCon.getResources().getColor(0x7f070037));
                sounditemholdernew.relayContainer.setVisibility(0);
                sounditemholdernew.relayHead.setTag(0x7f0a0037, Boolean.valueOf(true));
                ImageManager2.from(mCon).displayImage(sounditemholdernew.relayHead, mUser.smallLogo, 0x7f0202df);
                sounditemholdernew.relayName.setText("\u6211");
                sounditemholdernew.relayContent.setText("\u8F6C\u91C7\u4E86\u4E00\u4E2A\u8282\u76EE");
            } else
            {
                sounditemholdernew.origin.setText("\u539F\u521B");
                sounditemholdernew.origin.setTextColor(mContext.getResources().getColor(0x7f070036));
                sounditemholdernew.relayContainer.setVisibility(8);
            }
            if (isPlaying(recordingmodel.trackId))
            {
                if (LocalMediaService.getInstance().isPlaying())
                {
                    sounditemholdernew.playIcon.setImageResource(0x7f02024f);
                } else
                {
                    sounditemholdernew.playIcon.setImageResource(0x7f020250);
                }
            } else
            {
                sounditemholdernew.playIcon.setImageResource(0x7f020250);
            }
            sounditemholdernew.soundCover.setTag(0x7f0a0037, Boolean.valueOf(true));
            ImageManager2.from(mCon).displayImage(sounditemholdernew.soundCover, recordingmodel.coverSmall, 0x7f0202de);
            sounditemholdernew.playCount.setText((new StringBuilder()).append("").append(recordingmodel.playtimes).toString());
            sounditemholdernew.likeCount.setText((new StringBuilder()).append("").append(recordingmodel.likes).toString());
            sounditemholdernew.commentCount.setText((new StringBuilder()).append("").append(recordingmodel.comments).toString());
            if (recordingmodel.processState == 2)
            {
                if (recordingmodel.status == 0)
                {
                    sounditemholdernew.statusContainer.setVisibility(8);
                    sounditemholdernew.status.setVisibility(0);
                    sounditemholdernew.status.setText("\u5F85\u5BA1\u6838");
                    WeakReference weakreference = getThread(recordingmodel);
                    if (weakreference == null || weakreference.get() == null || !((UpdateState)weakreference.get()).isAlive())
                    {
                        Object obj = new UpdateState(recordingmodel);
                        ((UpdateState) (obj)).start();
                        obj = new WeakReference(obj);
                        mThreads.put(recordingmodel, obj);
                    }
                } else
                if (recordingmodel.status == 2)
                {
                    sounditemholdernew.statusContainer.setVisibility(0);
                    sounditemholdernew.status.setVisibility(0);
                    sounditemholdernew.status.setText("\u5BA1\u6838\u672A\u901A\u8FC7");
                    sounditemholdernew.statusIcon.setImageResource(0x7f020075);
                } else
                {
                    sounditemholdernew.status.setVisibility(8);
                    sounditemholdernew.statusContainer.setVisibility(8);
                }
            } else
            if (recordingmodel.processState == 3)
            {
                sounditemholdernew.status.setVisibility(8);
                sounditemholdernew.statusContainer.setVisibility(0);
                sounditemholdernew.statusName.setText("\u8F6C\u7801\u5931\u8D25");
                sounditemholdernew.statusIcon.setImageResource(0x7f020075);
            } else
            {
                sounditemholdernew.status.setVisibility(8);
                sounditemholdernew.statusContainer.setVisibility(0);
                sounditemholdernew.statusName.setText("\u6B63\u5728\u8F6C\u7801");
                sounditemholdernew.statusIcon.setImageResource(0x7f020075);
                WeakReference weakreference1 = getThread(recordingmodel);
                if (weakreference1 == null || weakreference1.get() == null || !((UpdateState)weakreference1.get()).isAlive())
                {
                    Object obj1 = new UpdateState(recordingmodel);
                    ((UpdateState) (obj1)).start();
                    obj1 = new WeakReference(obj1);
                    mThreads.put(recordingmodel, obj1);
                }
            }
            if (recordingmodel.isLike)
            {
                sounditemholdernew.likeCount.setCompoundDrawablesWithIntrinsicBounds(0x7f020090, 0, 0, 0);
            } else
            {
                sounditemholdernew.likeCount.setCompoundDrawablesWithIntrinsicBounds(0x7f020092, 0, 0, 0);
            }
            if (isDownload(recordingmodel.trackId))
            {
                sounditemholdernew.download.setImageResource(0x7f0200f1);
                sounditemholdernew.download.setEnabled(false);
            } else
            {
                sounditemholdernew.download.setEnabled(true);
                sounditemholdernew.download.setImageResource(0x7f0201fe);
            }
            sounditemholdernew.playIcon.setTag(sounditemholdernew);
            sounditemholdernew.download.setTag(sounditemholdernew);
        }

        protected void bindData(RecordingModel recordingmodel, com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder viewholder)
        {
        }

        protected volatile void bindData(Object obj, com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder viewholder)
        {
            bindData((RecordingModel)obj, viewholder);
        }

        public int getCount()
        {
            if (mIsUploaded)
            {
                return mUploaded.size();
            } else
            {
                return mDrafts.size();
            }
        }

        public RecordingModel getItem(int i)
        {
            if (mIsUploaded)
            {
                return (RecordingModel)mUploaded.get(i);
            } else
            {
                return (RecordingModel)mDrafts.get(i);
            }
        }

        public volatile Object getItem(int i)
        {
            return getItem(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            viewgroup = view;
            if (view == null)
            {
                viewgroup = SoundItemHolderNew.getView(mCon);
                view = (SoundItemHolderNew)viewgroup.getTag();
                class _cls1
                    implements android.view.View.OnClickListener
                {

                    final SoundAdapter this$1;

                    public void onClick(View view1)
                    {
                        if (mIsUploaded && mUploaded.size() != 0)
                        {
                            view1 = ((View) (view1.getTag()));
                            if (view1 != null && (view1 instanceof SoundItemHolderNew))
                            {
                                view1 = (SoundItemHolderNew)view1;
                                int j = ((SoundItemHolderNew) (view1)).position;
                                if (j >= 0 && j <= mUploaded.size())
                                {
                                    RecordingModel recordingmodel = (RecordingModel)mUploaded.get(j);
                                    if (recordingmodel.processState == 2)
                                    {
                                        playSound(((SoundItemHolderNew) (view1)).playIcon, ((SoundItemHolderNew) (view1)).position, ModelHelper.toSoundInfo(recordingmodel), ModelHelper.recordingModelToSoundInfoList(mUploaded));
                                        return;
                                    }
                                }
                            }
                        }
                    }

                _cls1()
                {
                    this$1 = SoundAdapter.this;
                    super();
                }
                }

                ((SoundItemHolderNew) (view)).playIcon.setOnClickListener(new _cls1());
                class _cls2
                    implements android.view.View.OnClickListener
                {

                    final SoundAdapter this$1;

                    public void onClick(View view1)
                    {
                        if (mIsUploaded && mUploaded.size() != 0)
                        {
                            Object obj = view1.getTag();
                            if (obj != null && (obj instanceof SoundItemHolderNew))
                            {
                                obj = (SoundItemHolderNew)obj;
                                int j = ((SoundItemHolderNew) (obj)).position;
                                if (j >= 0 && j <= mUploaded.size())
                                {
                                    RecordingModel recordingmodel = (RecordingModel)mUploaded.get(j);
                                    if (!isDownload(recordingmodel.trackId) && goDownLoad(recordingmodel, view1) == com.ximalaya.ting.android.a.a.a.a)
                                    {
                                        ((SoundItemHolderNew) (obj)).download.setImageResource(0x7f0200f1);
                                        return;
                                    }
                                }
                            }
                        }
                    }

                _cls2()
                {
                    this$1 = SoundAdapter.this;
                    super();
                }
                }

                ((SoundItemHolderNew) (view)).download.setOnClickListener(new _cls2());
            }
            view = (SoundItemHolderNew)viewgroup.getTag();
            view.position = i;
            if (mIsUploaded)
            {
                if (mUploaded.size() > 0 && i >= 0 && i < mUploaded.size())
                {
                    parseNetSound((RecordingModel)mUploaded.get(i), view);
                }
            } else
            if (mDrafts.size() > 0 && i >= 0 && i < mDrafts.size())
            {
                parseLocalSound((RecordingModel)mDrafts.get(i), view);
                return viewgroup;
            }
            return viewgroup;
        }

        public void notifyDataSetChanged()
        {
            boolean flag2 = true;
            boolean flag = true;
            if (mIsUploaded)
            {
                SoundListFragmentNew soundlistfragmentnew = SoundListFragmentNew.this;
                if (mUploaded.size() != 0)
                {
                    flag = false;
                }
                soundlistfragmentnew.showEmptyView(flag);
                Collections.sort(mUploaded);
            } else
            {
                SoundListFragmentNew soundlistfragmentnew1 = SoundListFragmentNew.this;
                boolean flag1;
                if (mDrafts.size() == 0)
                {
                    flag1 = flag2;
                } else
                {
                    flag1 = false;
                }
                soundlistfragmentnew1.showEmptyView(flag1);
            }
            super.notifyDataSetChanged();
        }

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            adapterview = ((AdapterView) (view.getTag()));
            if (adapterview != null && (adapterview instanceof SoundItemHolderNew)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            adapterview = (SoundItemHolderNew)adapterview;
            if (!mIsUploaded)
            {
                continue; /* Loop/switch isn't completed */
            }
            if (mUploaded.size() != 0 && ((SoundItemHolderNew) (adapterview)).position >= 0 && ((SoundItemHolderNew) (adapterview)).position <= mUploaded.size())
            {
                RecordingModel recordingmodel = (RecordingModel)mUploaded.get(((SoundItemHolderNew) (adapterview)).position);
                if (recordingmodel != null)
                {
                    if (recordingmodel.processState != 2)
                    {
                        showToast("\u4EB2\uFF0C\u8FD8\u5728\u8F6C\u7801\u54E6\uFF0C\u6682\u65F6\u4E0D\u80FD\u64AD\u653E\uFF01");
                        return;
                    } else
                    {
                        List list = ModelHelper.recordingModelToSoundInfoList(mUploaded);
                        PlayTools.gotoPlay(0, null, 0, null, list, Math.max(((SoundItemHolderNew) (adapterview)).position - (mUploaded.size() - list.size()), 0), 
// JavaClassFileOutputException: get_constant: invalid tag


        public SoundAdapter(Activity activity)
        {
            this$0 = SoundListFragmentNew.this;
            super(activity, null);
            mContext = activity;
        }
    }

    class UpdateState extends Thread
    {

        private static final String TAG = "SLFN.UpdateState";
        private boolean mContinue;
        private RecordingModel mRecordingModel;
        private boolean mStop;
        final SoundListFragmentNew this$0;

        public void run()
        {
            String s;
            RequestParams requestparams;
            f f1;
            s = (new StringBuilder()).append("mobile/track/").append(mRecordingModel.trackId).toString();
            requestparams = new RequestParams();
            f1 = f.a();
_L5:
            if (!mAllStop && !mStop && mContinue && e != null && !e.isFinishing() && mFragment.isAdded()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            class _cls1 extends h
            {

                final UpdateState this$1;

                public void onBindXDCS(Header aheader[])
                {
                    DataCollectUtil.bindDataToView(aheader, mListView);
                }

                public void onFinish()
                {
                }

                public void onNetError(int i, String s1)
                {
                    mContinue = true;
                }

                public void onStart()
                {
                }

                public void onSuccess(String s1)
                {
                    if (TextUtils.isEmpty(s1))
                    {
                        mContinue = true;
                        return;
                    }
                    try
                    {
                        s1 = JSON.parseObject(s1);
                    }
                    // Misplaced declaration of an exception variable
                    catch (String s1)
                    {
                        s1.printStackTrace();
                        s1 = null;
                    }
                    if (s1 == null || s1.getIntValue("ret") != 0)
                    {
                        mContinue = true;
                        return;
                    }
                    int i = s1.getIntValue("processState");
                    int j = s1.getIntValue("status");
                    mRecordingModel.processState = i;
                    mRecordingModel.status = j;
                    double d;
                    try
                    {
                        d = s1.getDoubleValue("duration");
                    }
                    catch (Exception exception)
                    {
                        exception.printStackTrace();
                        d = 0.0D;
                    }
                    if (d > 0.0D)
                    {
                        mRecordingModel.duration = d;
                    }
                    if (i == 2)
                    {
                        mRecordingModel.playUrl32 = s1.getString("playUrl32");
                        mRecordingModel.playUrl64 = s1.getString("playUrl64");
                        if (j == 1 || j == 2)
                        {
                            mContinue = false;
                            return;
                        } else
                        {
                            mContinue = true;
                            return;
                        }
                    }
                    if (i == 3)
                    {
                        mContinue = false;
                        return;
                    } else
                    {
                        mContinue = true;
                        return;
                    }
                }

                _cls1()
                {
                    this$1 = UpdateState.this;
                    h();
                }
            }

            f1.a(s, requestparams, DataCollectUtil.getDataFromView(mListView), false, new _cls1());
            if (e == null || e.isFinishing() || !mFragment.isAdded()) goto _L1; else goto _L3
_L3:
            class _cls2
                implements Runnable
            {

                final UpdateState this$1;

                public void run()
                {
                    mAdapter.notifyDataSetChanged();
                }

                _cls2()
                {
                    this$1 = UpdateState.this;
                    Object();
                }
            }

            e.runOnUiThread(new _cls2());
            try
            {
                Thread.sleep(2000L);
            }
            catch (InterruptedException interruptedexception)
            {
                interruptedexception.printStackTrace();
            }
            if (true) goto _L5; else goto _L4
_L4:
        }

        public void stopUpdate()
        {
            mStop = true;
            StringBuilder stringbuilder = (new StringBuilder()).append("Stop upadte ");
            String s;
            if (mRecordingModel == null)
            {
                s = "";
            } else
            {
                s = mRecordingModel.title;
            }
            Logger.e("SLFN.UpdateState", stringbuilder.append(s).toString());
        }


/*
        static boolean access$4102(UpdateState updatestate, boolean flag)
        {
            updatestate.mContinue = flag;
            return flag;
        }

*/


        public UpdateState(RecordingModel recordingmodel)
        {
            this$0 = SoundListFragmentNew.this;
            super();
            mContinue = true;
            mStop = false;
            mRecordingModel = recordingmodel;
        }
    }


    public static final String DEST_FLAG = "DEST_FLAG";
    public static final int DEST_INDEX_DRAFTS = 1;
    public static final int DEST_INDEX_UPLOADED = 0;
    private static final String TAG = "SoundListFragmentNew";
    private SoundAdapter mAdapter;
    private boolean mAllStop;
    private RecordingModel mCurrentModel;
    private DeleteTask mDeleteTask;
    private List mDrafts;
    private TextView mEmptyView;
    private String mErrMsg;
    private boolean mFirst;
    private Fragment mFragment;
    private boolean mFroceLoad;
    private boolean mIsUploaded;
    private LoadLocalTask mLoadLocalTask;
    private LoadNetTask mLoadNetTask;
    private LocalMediaService mMediaService;
    private int mPageId;
    private int mPageSize;
    private PlayStatusListener mPlayStatusListener;
    private PlayerListener mPlayerListener;
    private android.view.View.OnClickListener mReloadListener;
    private RadioGroup mSoundType;
    private HashMap mThreads;
    private int mTotal;
    private MenuDialog mUploadDialog;
    private UploadListener mUploadListener;
    private z mUploadManager;
    private List mUploaded;
    private LoginInfoModel mUser;

    public SoundListFragmentNew()
    {
        mIsUploaded = true;
        mPageId = 1;
        mPageSize = 15;
        mDrafts = new ArrayList();
        mUploaded = new ArrayList();
        mFroceLoad = false;
        mAllStop = false;
        mFirst = true;
        mThreads = new HashMap();
        mReloadListener = new _cls1();
    }

    private void deleteSound(RecordingModel recordingmodel)
    {
        if (mDeleteTask == null || mDeleteTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mDeleteTask = new DeleteTask();
            mDeleteTask.myexec(new RecordingModel[] {
                recordingmodel
            });
        }
    }

    private WeakReference getThread(RecordingModel recordingmodel)
    {
        for (Iterator iterator = mThreads.entrySet().iterator(); iterator.hasNext();)
        {
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            if (((RecordingModel)entry.getKey()).equals(recordingmodel))
            {
                return (WeakReference)entry.getValue();
            }
        }

        return null;
    }

    private void initUI()
    {
        ((RadioButton)mSoundType.getChildAt(0)).setText("\u5DF2\u4E0A\u4F20");
        ((RadioButton)mSoundType.getChildAt(1)).setVisibility(8);
        ((RadioButton)mSoundType.getChildAt(2)).setText("\u8349\u7A3F\u7BB1");
        setTitleText("\u6211\u7684\u58F0\u97F3");
        mSoundType.setOnCheckedChangeListener(new _cls3());
        mAdapter = new SoundAdapter(mActivity);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(mAdapter);
        mListView.setOnItemLongClickListener(new _cls4());
        mListView.setOnScrollListener(new _cls5());
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls6());
    }

    private void initUploadDialog()
    {
        if (mUploadDialog != null)
        {
            return;
        } else
        {
            ArrayList arraylist = new ArrayList();
            arraylist.add("\u4E0A\u4F20");
            arraylist.add("\u7F16\u8F91");
            arraylist.add("\u5220\u9664");
            mUploadDialog = new MenuDialog(mActivity, arraylist, a.e);
            mUploadDialog.setOnItemClickListener(new _cls7());
            return;
        }
    }

    private void loadData()
    {
        if (mFirst)
        {
            loadNetSound(false);
            loadLocalSound(false);
            mFirst = false;
            return;
        }
        if (mIsUploaded)
        {
            loadNetSound(true);
            return;
        } else
        {
            loadLocalSound(true);
            return;
        }
    }

    private void loadLocalSound(boolean flag)
    {
        if (mDrafts.size() != 0 && !mFroceLoad)
        {
            ((PullToRefreshListView)mListView).onRefreshComplete();
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            mAdapter.notifyDataSetChanged();
        } else
        if (mLoadLocalTask == null || mLoadLocalTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mLoadLocalTask = new LoadLocalTask(null);
            mLoadLocalTask.myexec(new Void[0]);
            return;
        }
    }

    private void loadNetSound(boolean flag)
    {
        if (mUploaded.size() != 0 && !mFroceLoad)
        {
            ((PullToRefreshListView)mListView).onRefreshComplete();
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            mAdapter.notifyDataSetChanged();
        } else
        if (mLoadNetTask == null || mLoadNetTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mLoadNetTask = new LoadNetTask(null);
            mLoadNetTask.myexec(new Void[0]);
            return;
        }
    }

    private void showEmptyView(boolean flag)
    {
        if (!flag)
        {
            mEmptyView.setVisibility(8);
            return;
        }
        String s;
        if (mIsUploaded)
        {
            s = "\u4EB2~\u4F60\u8FD8\u6CA1\u6709\u4E0A\u4F20\u58F0\u97F3\u54E6\uFF01";
        } else
        {
            s = "\u4EB2~\u4F60\u8FD8\u6CA1\u6709\u5F55\u97F3\u8349\u7A3F\u54E6\uFF01";
        }
        mEmptyView.setText(s);
        mEmptyView.setVisibility(0);
    }

    public void onActivityCreated(Bundle bundle)
    {
        onActivityCreated(bundle);
        mFragment = this;
        mUploadManager = z.a(mCon);
        mUser = UserInfoMannage.getInstance().getUser();
        mMediaService = LocalMediaService.getInstance();
        initUI();
        mPlayStatusListener = new PlayStatusListener();
        mPlayerListener = new PlayerListener();
        mUploadListener = new UploadListener();
        if (mMediaService != null)
        {
            mMediaService.setOnPlayerStatusUpdateListener(mPlayStatusListener);
            mMediaService.setOnPlayServiceUpdateListener(mPlayerListener);
        }
        mUploadManager.a(mUploadListener);
        mSoundType.post(new _cls2());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030030, viewgroup, false);
        mListView = (ListView)findViewById(0x7f0a0118);
        mSoundType = (RadioGroup)findViewById(0x7f0a0119);
        mEmptyView = (TextView)findViewById(0x7f0a011a);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        onDestroyView();
        if (mMediaService != null)
        {
            mMediaService.removeOnPlayerUpdateListener(mPlayStatusListener);
            mMediaService.removeOnPlayServiceUpdateListener(mPlayerListener);
        }
        mUploadManager.b(mUploadListener);
        mAllStop = true;
    }

    public void onResume()
    {
        onResume();
        mFroceLoad = true;
        loadData();
    }


/*
    static boolean access$002(SoundListFragmentNew soundlistfragmentnew, boolean flag)
    {
        soundlistfragmentnew.mFroceLoad = flag;
        return flag;
    }

*/




/*
    static int access$1002(SoundListFragmentNew soundlistfragmentnew, int i)
    {
        soundlistfragmentnew.mPageId = i;
        return i;
    }

*/


/*
    static int access$1008(SoundListFragmentNew soundlistfragmentnew)
    {
        int i = soundlistfragmentnew.mPageId;
        soundlistfragmentnew.mPageId = i + 1;
        return i;
    }

*/




/*
    static RecordingModel access$1402(SoundListFragmentNew soundlistfragmentnew, RecordingModel recordingmodel)
    {
        soundlistfragmentnew.mCurrentModel = recordingmodel;
        return recordingmodel;
    }

*/











/*
    static String access$2302(SoundListFragmentNew soundlistfragmentnew, String s)
    {
        soundlistfragmentnew.mErrMsg = s;
        return s;
    }

*/









/*
    static boolean access$302(SoundListFragmentNew soundlistfragmentnew, boolean flag)
    {
        soundlistfragmentnew.mIsUploaded = flag;
        return flag;
    }

*/





















/*
    static int access$902(SoundListFragmentNew soundlistfragmentnew, int i)
    {
        soundlistfragmentnew.mTotal = i;
        return i;
    }

*/

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SoundListFragmentNew this$0;

        public void onClick(View view)
        {
            mFroceLoad = true;
            loadData();
        }

        _cls1()
        {
            this$0 = SoundListFragmentNew.this;
            Object();
        }
    }


    private class _cls3
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final SoundListFragmentNew this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            if (radiogroup.indexOfChild(radiogroup.findViewById(i)) == 0)
            {
                mIsUploaded = true;
                loadNetSound(true);
                return;
            } else
            {
                mIsUploaded = false;
                mFroceLoad = false;
                loadLocalSound(true);
                return;
            }
        }

        _cls3()
        {
            this$0 = SoundListFragmentNew.this;
            Object();
        }
    }



    private class _cls5
        implements android.widget.AbsListView.OnScrollListener
    {

        final SoundListFragmentNew this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0 && mIsUploaded)
            {
                int j = abslistview.getLastVisiblePosition();
                i = abslistview.getCount();
                if (i > 5)
                {
                    i -= 5;
                } else
                {
                    i--;
                }
                if (j >= i && mUploaded.size() < mTotal)
                {
                    mFroceLoad = true;
                    loadData();
                    return;
                }
            }
        }

        _cls5()
        {
            this$0 = SoundListFragmentNew.this;
            Object();
        }
    }


    private class _cls6
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final SoundListFragmentNew this$0;

        public void onRefresh()
        {
            mPageId = 1;
            mFroceLoad = true;
            loadData();
        }

        _cls6()
        {
            this$0 = SoundListFragmentNew.this;
            Object();
        }
    }



    private class _cls2
        implements Runnable
    {

        final SoundListFragmentNew this$0;

        public void run()
        {
            Bundle bundle = getArguments();
            if (bundle == null || !bundle.containsKey("DEST_FLAG")) goto _L2; else goto _L1
_L1:
            int i;
            int j;
            j = bundle.getInt("DEST_FLAG");
            i = 0x7f0a0080;
            j;
            JVM INSTR tableswitch 1 1: default 52
        //                       1 64;
               goto _L3 _L4
_L3:
            mSoundType.check(i);
_L2:
            return;
_L4:
            i = 0x7f0a014b;
            if (true) goto _L3; else goto _L5
_L5:
        }

        _cls2()
        {
            this$0 = SoundListFragmentNew.this;
            Object();
        }
    }

}
