// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;


// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            OtherSpaceFragment

public static final class  extends Enum
{

    private static final REQUEST_FAILED $VALUES[];
    public static final REQUEST_FAILED HIDE_ALL;
    public static final REQUEST_FAILED LOADING;
    public static final REQUEST_FAILED MORE;
    public static final REQUEST_FAILED NO_CONNECTION;
    public static final REQUEST_FAILED NO_DATA;
    public static final REQUEST_FAILED REQUEST_FAILED;

    public static  valueOf(String s)
    {
        return ()Enum.valueOf(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment$FooterView, s);
    }

    public static [] values()
    {
        return ([])$VALUES.clone();
    }

    static 
    {
        MORE = new <init>("MORE", 0);
        LOADING = new <init>("LOADING", 1);
        NO_CONNECTION = new <init>("NO_CONNECTION", 2);
        HIDE_ALL = new <init>("HIDE_ALL", 3);
        NO_DATA = new <init>("NO_DATA", 4);
        REQUEST_FAILED = new <init>("REQUEST_FAILED", 5);
        $VALUES = (new .VALUES[] {
            MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA, REQUEST_FAILED
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
