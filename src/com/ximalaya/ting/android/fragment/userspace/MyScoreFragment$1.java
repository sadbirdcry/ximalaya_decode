// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.support.v4.app.FragmentActivity;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.share.ScoreTask;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            MyScoreFragment

class this._cls0 extends a
{

    final MyScoreFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, MyScoreFragment.access$500(MyScoreFragment.this));
    }

    public void onNetError(int i, String s)
    {
        if (getActivity() != null && !getActivity().isFinishing())
        {
            showToast(getActivity().getString(0x7f0901f8));
        }
        Logger.d("MyScoreFragment", (new StringBuilder()).append("the error response is:").append(s).toString());
        if (MyScoreFragment.access$100(MyScoreFragment.this) != null && MyScoreFragment.access$100(MyScoreFragment.this).getScore() >= 0)
        {
            MyScoreFragment.access$200(MyScoreFragment.this, MyScoreFragment.access$100(MyScoreFragment.this).getScore());
        }
    }

    public void onSuccess(String s)
    {
        Logger.d("MyScoreFragment", (new StringBuilder()).append("the success response is:").append(s).toString());
        s = JSONObject.parseObject(s);
        if (s.getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_125;
        }
        int i = s.getIntValue("userPoint");
        MyScoreFragment.access$200(MyScoreFragment.this, i);
        if (MyScoreFragment.access$300(MyScoreFragment.this).size() == 0)
        {
            break MISSING_BLOCK_LABEL_125;
        }
        if (MyScoreFragment.access$300(MyScoreFragment.this).get(0) == null)
        {
            return;
        }
        try
        {
            ((ScoreTask)MyScoreFragment.access$300(MyScoreFragment.this).get(0)).restTimes = s.getIntValue("restTimes");
            MyScoreFragment.access$400(MyScoreFragment.this).notifyDataSetChanged();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            showToast("\u6570\u636E\u8F6C\u5316\u9519\u8BEF");
        }
    }

    skAdapter()
    {
        this$0 = MyScoreFragment.this;
        super();
    }
}
