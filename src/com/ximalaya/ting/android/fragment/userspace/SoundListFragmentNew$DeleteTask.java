// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            SoundListFragmentNew

class this._cls0 extends MyAsyncTask
{

    private RecordingModel model;
    final SoundListFragmentNew this$0;

    protected transient Integer doInBackground(RecordingModel arecordingmodel[])
    {
        model = arecordingmodel[0];
        arecordingmodel = new RequestParams();
        arecordingmodel.put("trackId", (new StringBuilder()).append("").append(model.trackId).toString());
        class _cls1ResultWrapper
        {

            public String errorCode;
            public String msg;
            public int ret;
            final SoundListFragmentNew.DeleteTask this$1;
            public long trackId;

            _cls1ResultWrapper()
            {
                this$1 = SoundListFragmentNew.DeleteTask.this;
                super();
                ret = -1;
            }
        }

        final _cls1ResultWrapper wrapper = new _cls1ResultWrapper();
        class _cls1 extends h
        {

            final SoundListFragmentNew.DeleteTask this$1;
            final _cls1ResultWrapper val$wrapper;

            public void onBindXDCS(Header aheader[])
            {
                DataCollectUtil.bindDataToView(aheader, mListView);
            }

            public void onFinish()
            {
            }

            public void onNetError(int i, String s)
            {
            }

            public void onStart()
            {
            }

            public void onSuccess(String s)
            {
                if (!TextUtils.isEmpty(s))
                {
                    Object obj = null;
                    try
                    {
                        s = JSON.parseObject(s);
                    }
                    // Misplaced declaration of an exception variable
                    catch (String s)
                    {
                        s.printStackTrace();
                        s = obj;
                    }
                    if (s != null)
                    {
                        wrapper.ret = s.getIntValue("ret");
                        wrapper.msg = s.getString("msg");
                        wrapper.errorCode = s.getString("errorCode");
                        wrapper.trackId = s.getLongValue("trackId");
                        return;
                    }
                }
            }

            _cls1()
            {
                this$1 = SoundListFragmentNew.DeleteTask.this;
                wrapper = _pcls1resultwrapper;
                super();
            }
        }

        f.a().a("mobile/track/delete", arecordingmodel, DataCollectUtil.getDataFromView(mListView), false, new _cls1());
        Logger.e("SoundListFragmentNew", (new StringBuilder()).append("Delete sound result ").append(wrapper.ret).append("/").append(wrapper.msg).append("/").append(wrapper.errorCode).toString());
        return Integer.valueOf(wrapper.ret);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((RecordingModel[])aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (integer.intValue() != 0)
        {
            showToast("\u5220\u9664\u58F0\u97F3\u5931\u8D25");
            return;
        }
        showToast("\u5220\u9664\u58F0\u97F3\u6210\u529F");
        integer = SoundListFragmentNew.access$3000(SoundListFragmentNew.this, model);
        if (integer != null && integer.get() != null)
        {
            (()integer.get()).stopUpdate();
        }
        SoundListFragmentNew.access$3100(SoundListFragmentNew.this).remove(model);
        SoundListFragmentNew.access$600(SoundListFragmentNew.this).remove(model);
        SoundListFragmentNew.access$1800(SoundListFragmentNew.this).notifyDataSetChanged();
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
    }

    r()
    {
        this$0 = SoundListFragmentNew.this;
        super();
    }
}
