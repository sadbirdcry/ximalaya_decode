// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.adapter.MyLikedSoundsAdapter;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundModel;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public class MyLikedSoundsFragment extends BaseActivityLikeFragment
    implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{
    private static final class FooterView extends Enum
    {

        private static final FooterView $VALUES[];
        public static final FooterView HIDE_ALL;
        public static final FooterView LOADING;
        public static final FooterView MORE;
        public static final FooterView NO_CONNECTION;
        public static final FooterView NO_DATA;

        public static FooterView valueOf(String s)
        {
            return (FooterView)Enum.valueOf(com/ximalaya/ting/android/fragment/userspace/MyLikedSoundsFragment$FooterView, s);
        }

        public static FooterView[] values()
        {
            return (FooterView[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterView("MORE", 0);
            LOADING = new FooterView("LOADING", 1);
            NO_CONNECTION = new FooterView("NO_CONNECTION", 2);
            HIDE_ALL = new FooterView("HIDE_ALL", 3);
            NO_DATA = new FooterView("NO_DATA", 4);
            $VALUES = (new FooterView[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA
            });
        }

        private FooterView(String s, int i)
        {
            super(s, i);
        }
    }


    private List dataList;
    private boolean loadingNextPage;
    private MyAsyncTask mDataLoadTask;
    private LinearLayout mEmptyView;
    private RelativeLayout mFooterViewLoading;
    private Handler mHandler;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private PullToRefreshListView mPullToRefreshExpandableListView;
    private MyLikedSoundsAdapter mSoundsHotAdapter;
    private ViewGroup noNetLayout;
    private int pageId;
    private int pageSize;
    private long toUid;
    private int totalCount;

    public MyLikedSoundsFragment()
    {
        totalCount = 0;
        pageId = 1;
        pageSize = 15;
        loadingNextPage = false;
        mHandler = new _cls1();
    }

    private void initData()
    {
        long l = 0L;
        if (getArguments() != null)
        {
            l = getArguments().getLong("toUid", 0L);
        }
        toUid = l;
        showEmptyView(false);
        mPullToRefreshExpandableListView.toRefreshing();
    }

    private void initListener()
    {
        mPullToRefreshExpandableListView.setOnRefreshListener(new _cls2());
        noNetLayout.setOnClickListener(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
        mPullToRefreshExpandableListView.setOnScrollListener(new _cls5());
        mPullToRefreshExpandableListView.setOnItemClickListener(new _cls6());
        mPullToRefreshExpandableListView.setOnItemLongClickListener(new _cls7());
    }

    private void initUi()
    {
        mPullToRefreshExpandableListView = (PullToRefreshListView)findViewById(0x7f0a0095);
        dataList = new ArrayList();
        noNetLayout = (ViewGroup)findViewById(0x7f0a035b);
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(mCon).inflate(0x7f0301fb, null);
        mPullToRefreshExpandableListView.addFooterView(mFooterViewLoading);
        mEmptyView = (LinearLayout)LayoutInflater.from(mActivity).inflate(0x7f03007c, mPullToRefreshExpandableListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u8D5E\u8FC7\u58F0\u97F3\u54E6");
        mEmptyView.findViewById(0x7f0a0231).setVisibility(8);
        mEmptyView.findViewById(0x7f0a0232).setVisibility(8);
        mPullToRefreshExpandableListView.addFooterView(mEmptyView);
        mSoundsHotAdapter = new MyLikedSoundsAdapter(getActivity(), dataList, mHandler);
        mPullToRefreshExpandableListView.setAdapter(mSoundsHotAdapter);
        showFooterView(FooterView.HIDE_ALL);
        setTitleText("\u8D5E\u8FC7\u7684");
    }

    private void loadDataListData(final View view)
    {
        if (ToolUtil.isConnectToNetwork(mCon))
        {
            mDataLoadTask = (new _cls8()).myexec(new Void[0]);
            return;
        } else
        {
            showEmptyView(false);
            mPullToRefreshExpandableListView.onRefreshComplete();
            showFooterView(FooterView.NO_CONNECTION);
            showReloadLayout(true);
            return;
        }
    }

    private boolean moreDataAvailable()
    {
        return pageId * pageSize < totalCount;
    }

    private void parseData(List list)
    {
        loadingNextPage = false;
        mPullToRefreshExpandableListView.onRefreshComplete();
        if (list == null || list.size() <= 0)
        {
            showEmptyView(true);
        } else
        {
            showEmptyView(false);
        }
        if (list == null)
        {
            showReloadLayout(true);
            if (dataList.size() > 0)
            {
                showFooterView(FooterView.NO_DATA);
            }
            return;
        }
        mSoundsHotAdapter.setDataSource(e.O);
        mSoundsHotAdapter.setPageId(pageId);
        if (pageId == 1)
        {
            dataList.clear();
            dataList.addAll(list);
            mSoundsHotAdapter.notifyDataSetChanged();
        } else
        {
            dataList.addAll(list);
            mSoundsHotAdapter.notifyDataSetChanged();
        }
        if (moreDataAvailable())
        {
            showFooterView(FooterView.MORE);
        } else
        {
            showFooterView(FooterView.HIDE_ALL);
        }
        pageId = pageId + 1;
        showReloadLayout(false);
    }

    private void registerListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls9();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void showEmptyView(boolean flag)
    {
        if (flag)
        {
            mEmptyView.setVisibility(0);
            return;
        } else
        {
            mEmptyView.setVisibility(8);
            return;
        }
    }

    private void showFooterView(FooterView footerview)
    {
        mPullToRefreshExpandableListView.setFooterDividersEnabled(false);
        mFooterViewLoading.setVisibility(0);
        if (footerview == FooterView.MORE)
        {
            mFooterViewLoading.setClickable(true);
            mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
            ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
            mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
        } else
        {
            if (footerview == FooterView.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerview == FooterView.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == FooterView.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerview == FooterView.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }

    private void showReloadLayout(boolean flag)
    {
        if (flag && (dataList == null || dataList.size() == 0))
        {
            noNetLayout.setVisibility(0);
            showFooterView(FooterView.HIDE_ALL);
            return;
        } else
        {
            noNetLayout.setVisibility(8);
            return;
        }
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(this);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initUi();
        initListener();
        initData();
        registerListener();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (canGoon() && i == 1 && j == 1)
        {
            i = intent.getIntExtra("position", -1);
            long l = intent.getLongExtra("trackId", 0L);
            if (dataList != null && i < dataList.size() && i >= 0 && ((SoundModel)dataList.get(i)).trackId == l)
            {
                ((SoundModel)dataList.get(i)).isRelay = true;
                if (mSoundsHotAdapter != null)
                {
                    mSoundsHotAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03001d, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        if (mSoundsHotAdapter != null)
        {
            mSoundsHotAdapter.cleanData();
        }
        unRegisterListener();
        super.onDestroyView();
    }

    public void onPlayCanceled()
    {
    }

    public void onSoundChanged(int i)
    {
        if (mSoundsHotAdapter != null)
        {
            mSoundsHotAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (i < 0 || soundinfo == null || i >= dataList.size() || ((SoundModel)dataList.get(i)).trackId != soundinfo.trackId || mSoundsHotAdapter == null)
        {
            return;
        } else
        {
            SoundModel soundmodel = (SoundModel)dataList.get(i);
            soundmodel.isLike = soundinfo.is_favorited;
            soundmodel.likes = soundinfo.favorites_counts;
            mSoundsHotAdapter.notifyDataSetChanged();
            return;
        }
    }



/*
    static boolean access$002(MyLikedSoundsFragment mylikedsoundsfragment, boolean flag)
    {
        mylikedsoundsfragment.loadingNextPage = flag;
        return flag;
    }

*/




/*
    static int access$102(MyLikedSoundsFragment mylikedsoundsfragment, int i)
    {
        mylikedsoundsfragment.pageId = i;
        return i;
    }

*/













/*
    static int access$802(MyLikedSoundsFragment mylikedsoundsfragment, int i)
    {
        mylikedsoundsfragment.totalCount = i;
        return i;
    }

*/


    private class _cls1 extends Handler
    {

        final MyLikedSoundsFragment this$0;

        public void handleMessage(Message message)
        {
            switch (message.what)
            {
            default:
                return;

            case 2147418114: 
                message = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                break;
            }
            startActivity(message);
            finish();
        }

        _cls1()
        {
            this$0 = MyLikedSoundsFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final MyLikedSoundsFragment this$0;

        public void onRefresh()
        {
            if (!loadingNextPage)
            {
                pageId = 1;
                loadDataListData(mPullToRefreshExpandableListView);
            }
        }

        _cls2()
        {
            this$0 = MyLikedSoundsFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final MyLikedSoundsFragment this$0;

        public void onClick(View view)
        {
            mPullToRefreshExpandableListView.toRefreshing();
            loadDataListData(noNetLayout);
        }

        _cls3()
        {
            this$0 = MyLikedSoundsFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final MyLikedSoundsFragment this$0;

        public void onClick(View view)
        {
            if (!loadingNextPage)
            {
                showFooterView(FooterView.LOADING);
                loadDataListData(mFooterViewLoading);
            }
        }

        _cls4()
        {
            this$0 = MyLikedSoundsFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AbsListView.OnScrollListener
    {

        final MyLikedSoundsFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            mPullToRefreshExpandableListView.onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0)
            {
                i = abslistview.getCount();
                if (i > 5)
                {
                    i -= 5;
                } else
                {
                    i--;
                }
                if (abslistview.getLastVisiblePosition() > i && (pageId - 1) * pageSize < totalCount && (mDataLoadTask == null || mDataLoadTask.getStatus() != android.os.AsyncTask.Status.RUNNING) && !loadingNextPage)
                {
                    showFooterView(FooterView.LOADING);
                    loadDataListData(mPullToRefreshExpandableListView);
                }
            }
        }

        _cls5()
        {
            this$0 = MyLikedSoundsFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.widget.AdapterView.OnItemClickListener
    {

        final MyLikedSoundsFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            adapterview = ModelHelper.soundModelToSoundInfoList(mSoundsHotAdapter.list);
            PlayTools.gotoPlay(6, e.O, pageId, null, adapterview, i - 1, getActivity(), true, DataCollectUtil.getDataFromView(view));
        }

        _cls6()
        {
            this$0 = MyLikedSoundsFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final MyLikedSoundsFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mPullToRefreshExpandableListView.getHeaderViewsCount();
            if (i < 0 || i >= mSoundsHotAdapter.getCount())
            {
                return false;
            } else
            {
                mSoundsHotAdapter.handleItemLongClick((Likeable)mSoundsHotAdapter.getData().get(i), view);
                return true;
            }
        }

        _cls7()
        {
            this$0 = MyLikedSoundsFragment.this;
            super();
        }
    }


    private class _cls8 extends MyAsyncTask
    {

        final MyLikedSoundsFragment this$0;
        final View val$view;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = e.O;
            Object obj = new RequestParams();
            if (loginInfoModel != null)
            {
                ((RequestParams) (obj)).put("uid", (new StringBuilder()).append("").append(loginInfoModel.uid).toString());
                ((RequestParams) (obj)).put("token", loginInfoModel.token);
            }
            if (0L != toUid)
            {
                ((RequestParams) (obj)).put("toUid", (new StringBuilder()).append(toUid).append("").toString());
                avoid = e.P;
            }
            ((RequestParams) (obj)).put("pageId", (new StringBuilder()).append(pageId).append("").toString());
            ((RequestParams) (obj)).put("pageSize", (new StringBuilder()).append(pageSize).append("").toString());
            obj = f.a().a(avoid, ((RequestParams) (obj)), view, mPullToRefreshExpandableListView);
            Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
            avoid = null;
            try
            {
                obj = JSON.parseObject(((String) (obj)));
                String s = ((JSONObject) (obj)).get("ret").toString();
                if (totalCount == 0)
                {
                    totalCount = ((JSONObject) (obj)).getIntValue("totalCount");
                }
                if ("0".equals(s))
                {
                    avoid = JSON.parseArray(((JSONObject) (obj)).get("list").toString(), com/ximalaya/ting/android/model/sound/SoundModel);
                }
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
                return null;
            }
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(final List result)
        {
            if (!canGoon())
            {
                return;
            } else
            {
                class _cls1
                    implements MyCallback
                {

                    final _cls8 this$1;
                    final List val$result;

                    public void execute()
                    {
                        parseData(result);
                    }

                _cls1()
                {
                    this$1 = _cls8.this;
                    result = list;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                return;
            }
        }

        protected void onPreExecute()
        {
            loadingNextPage = true;
            showReloadLayout(false);
        }

        _cls8()
        {
            this$0 = MyLikedSoundsFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls9 extends OnPlayerStatusUpdateListenerProxy
    {

        final MyLikedSoundsFragment this$0;

        public void onPlayStateChange()
        {
            mSoundsHotAdapter.notifyDataSetChanged();
        }

        _cls9()
        {
            this$0 = MyLikedSoundsFragment.this;
            super();
        }
    }

}
