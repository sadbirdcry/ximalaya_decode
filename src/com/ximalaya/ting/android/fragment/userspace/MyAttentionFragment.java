// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.adapter.UserListAdapter;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.findings.FindingHotStationCategoryFragment;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_info.UserList;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public class MyAttentionFragment extends BaseListFragment
{
    class LoadTask extends MyAsyncTask
    {

        final MyAttentionFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            long l = 0L;
            if (flag != 0) goto _L2; else goto _L1
_L1:
            android.content.Context context = mCon;
            if (loginInfoModel != null)
            {
                try
                {
                    l = loginInfoModel.uid;
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                    return null;
                }
            }
_L13:
            if (loginInfoModel != null) goto _L4; else goto _L3
_L3:
            avoid = null;
_L7:
            avoid = CommonRequest.doGetFollowings(context, l, avoid, toUid, mPageId, mPageSize, mListView, mListView);
_L10:
            userList = (UserList)JSON.parseObject(avoid, com/ximalaya/ting/android/model/personal_info/UserList);
            if (userList == null || userList.list != null) goto _L6; else goto _L5
_L5:
            userList.list = new ArrayList();
            return null;
_L4:
            avoid = loginInfoModel.token;
              goto _L7
_L2:
            if (flag != 1)
            {
                break MISSING_BLOCK_LABEL_285;
            }
            context = mCon;
            if (loginInfoModel != null) goto _L9; else goto _L8
_L8:
            if (loginInfoModel != null)
            {
                break MISSING_BLOCK_LABEL_271;
            }
            avoid = null;
_L11:
            avoid = CommonRequest.doGetFollowers(context, l, avoid, toUid, mPageId, mPageSize, mListView, mListView);
              goto _L10
_L9:
            l = loginInfoModel.uid;
              goto _L8
            avoid = loginInfoModel.token;
              goto _L11
label0:
            {
                if (flag != 2)
                {
                    break label0;
                }
                avoid = CommonRequest.doGetLovers(mCon, trackId, mPageId, mPageSize, mListView, mListView);
            }
              goto _L10
            avoid = null;
              goto _L10
_L6:
            return null;
            if (true) goto _L13; else goto _L12
_L12:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            if (canGoon())
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                if (userList != null && userList.ret == 0)
                {
                    if (mPageId == 1)
                    {
                        long l = getAnimationLeftTime();
                        class _cls1
                            implements Runnable
                        {

                            final LoadTask this$1;

                            public void run()
                            {
                                if (isAdded())
                                {
                                    if (itemAdapter != null)
                                    {
                                        mUserList.clear();
                                        mUserList.addAll(userList.list);
                                        itemAdapter.notifyDataSetChanged();
                                    }
                                    onRefreshOk();
                                }
                            }

                _cls1()
                {
                    this$1 = LoadTask.this;
                    super();
                }
                        }

                        if (l > 0L)
                        {
                            mListView.postDelayed(new _cls1(), l);
                        } else
                        {
                            mUserList.clear();
                            mUserList.addAll(userList.list);
                            itemAdapter.notifyDataSetChanged();
                            onRefreshOk();
                        }
                    } else
                    {
                        mUserList.addAll(userList.list);
                        itemAdapter.notifyDataSetChanged();
                        onRefreshOk();
                    }
                    int i = this.access$000;
                    if (userList.totalCount != 0 && flag == 2)
                    {
                        topTextView.setText((new StringBuilder()).append(userList.totalCount).append("\u4EBA\u8D5E\u8FC7").toString());
                        return;
                    }
                } else
                {
                    showToast("\u5237\u65B0\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5");
                    onRefreshOk();
                    return;
                }
            }
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        LoadTask()
        {
            this$0 = MyAttentionFragment.this;
            super();
        }
    }


    public static final int FLAG_CARE_FRIEND = 3;
    public static final int FLAG_CARE_FRIENDS = 0;
    public static final int FLAG_FANS = 1;
    public static final int FLAG_LOVERS = 2;
    private int flag;
    private UserListAdapter itemAdapter;
    private ProgressDialog mDialog;
    private LinearLayout mEmptyView;
    private LoadTask mLoadTask;
    private int mPageId;
    private int mPageSize;
    private List mUserList;
    private long toUid;
    private long trackId;
    private UserList userList;

    public MyAttentionFragment()
    {
        mUserList = new ArrayList();
        mPageId = 1;
        mPageSize = 15;
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        flag = bundle.getInt("flag", 0);
        toUid = bundle.getLong("toUid", 0L);
        trackId = bundle.getLong("trackId", 0L);
        mPageId = 1;
        ((PullToRefreshListView)mListView).toRefreshing();
    }

    private void initUI()
    {
        mEmptyView = (LinearLayout)LayoutInflater.from(mActivity).inflate(0x7f03007c, mListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setVisibility(8);
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setVisibility(8);
        ((Button)mEmptyView.findViewById(0x7f0a0232)).setVisibility(8);
        mListView.addFooterView(mEmptyView);
        itemAdapter = new UserListAdapter(this, mUserList, loginInfoModel);
        mListView.setAdapter(itemAdapter);
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls2());
        mListView.setOnScrollListener(new _cls3());
        mListView.setOnItemClickListener(new _cls4());
    }

    private boolean isLoading()
    {
        return mLoadTask != null && mLoadTask.getStatus() == android.os.AsyncTask.Status.RUNNING;
    }

    private void loadData()
    {
        if (mLoadTask == null || mLoadTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mLoadTask = new LoadTask();
            mLoadTask.myexec(new Void[0]);
        }
    }

    private void onRefreshOk()
    {
        ((PullToRefreshListView)mListView).onRefreshComplete();
        showEmptyView();
    }

    private void showEmptyView()
    {
        if (mUserList != null && mUserList.size() != 0) goto _L2; else goto _L1
_L1:
        flag;
        JVM INSTR tableswitch 0 2: default 48
    //                   0 189
    //                   1 211
    //                   2 230;
           goto _L3 _L4 _L5 _L6
_L3:
        final Object fclazz;
        Object obj;
        Object obj1;
        Object obj2;
        boolean flag1;
        boolean flag2;
        flag1 = false;
        fclazz = null;
        flag2 = false;
        obj = null;
        obj1 = null;
        obj2 = null;
_L7:
        TextView textview = (TextView)mEmptyView.findViewById(0x7f0a011a);
        TextView textview1 = (TextView)mEmptyView.findViewById(0x7f0a0231);
        Button button = (Button)mEmptyView.findViewById(0x7f0a0232);
        textview.setVisibility(0);
        textview.setText(((CharSequence) (obj2)));
        textview1.setVisibility(8);
        if (flag2)
        {
            textview1.setVisibility(0);
            textview1.setText(((CharSequence) (obj1)));
        }
        button.setVisibility(8);
        if (flag1)
        {
            button.setVisibility(0);
            button.setText(((CharSequence) (obj)));
            button.setOnClickListener(new _cls1());
        }
        mEmptyView.setVisibility(0);
        return;
_L4:
        flag2 = true;
        obj = "\u53BB\u770B\u770B\u63A8\u8350\u7535\u53F0";
        obj1 = "\u60F3\u5B9A\u5236\u4F60\u7684\u4E13\u5C5E\u7535\u53F0\u5417\uFF1F";
        obj2 = "\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u5173\u6CE8\u7684\u4EBA\u54E6";
        flag1 = true;
        fclazz = com/ximalaya/ting/android/fragment/findings/FindingHotStationCategoryFragment;
        continue; /* Loop/switch isn't completed */
_L5:
        flag1 = false;
        fclazz = null;
        flag2 = false;
        obj = null;
        obj1 = null;
        obj2 = "\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u7C89\u4E1D\u54E6";
        continue; /* Loop/switch isn't completed */
_L6:
        flag1 = false;
        fclazz = null;
        flag2 = false;
        obj = null;
        obj1 = null;
        obj2 = "\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u8D5E\u8FC7\u4EBA\u54E6";
        if (true) goto _L7; else goto _L2
_L2:
        mEmptyView.setVisibility(8);
        return;
    }

    private void updateUI()
    {
        switch (flag)
        {
        default:
            return;

        case 0: // '\0'
            topTextView.setText(0x7f090154);
            return;

        case 1: // '\001'
            topTextView.setText(0x7f090155);
            return;

        case 2: // '\002'
            topTextView.setText(0x7f090156);
            break;
        }
    }

    public void dismissConcertProgressDialog()
    {
        if (mDialog != null)
        {
            mDialog.dismiss();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        mListView = (PullToRefreshListView)findViewById(0x7f0a0500);
        super.onActivityCreated(bundle);
        initUI();
        initData();
        updateUI();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030171, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        itemAdapter.cleanData();
        itemAdapter = null;
        super.onDestroyView();
    }

    public void showConcertProgressDialog()
    {
        mDialog = new MyProgressDialog(getActivity());
        mDialog.setCanceledOnTouchOutside(false);
        mDialog.setTitle("\u6B63\u5728\u5904\u7406...");
        mDialog.show();
    }



/*
    static int access$002(MyAttentionFragment myattentionfragment, int i)
    {
        myattentionfragment.mPageId = i;
        return i;
    }

*/


/*
    static int access$008(MyAttentionFragment myattentionfragment)
    {
        int i = myattentionfragment.mPageId;
        myattentionfragment.mPageId = i + 1;
        return i;
    }

*/





/*
    static UserList access$202(MyAttentionFragment myattentionfragment, UserList userlist)
    {
        myattentionfragment.userList = userlist;
        return userlist;
    }

*/








    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final MyAttentionFragment this$0;

        public void onRefresh()
        {
            mPageId = 1;
            loadData();
        }

        _cls2()
        {
            this$0 = MyAttentionFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AbsListView.OnScrollListener
    {

        final MyAttentionFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (userList == null || abslistview.getLastVisiblePosition() <= i || (mPageId - 1) * mPageSize >= userList.totalCount)
                    {
                        break label0;
                    }
                    if (!isLoading())
                    {
                        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
                        loadData();
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls3()
        {
            this$0 = MyAttentionFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemClickListener
    {

        final MyAttentionFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (i <= mUserList.size())
            {
                adapterview = new Bundle();
                adapterview.putLong("toUid", ((UserInfoModel)mUserList.get(i - mListView.getHeaderViewsCount())).getUid().longValue());
                adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, adapterview);
            }
        }

        _cls4()
        {
            this$0 = MyAttentionFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final MyAttentionFragment this$0;
        final Class val$fclazz;

        public void onClick(View view)
        {
            (new Bundle()).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(fclazz, null);
        }

        _cls1()
        {
            this$0 = MyAttentionFragment.this;
            fclazz = class1;
            super();
        }
    }

}
