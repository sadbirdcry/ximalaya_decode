// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.adapter.userspace.AlbumListAdapter;
import com.ximalaya.ting.android.model.album.AlbumCollection;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            AlbumListFragment

class this._cls0 extends MyAsyncTask
{

    AlbumCollection ac;
    final AlbumListFragment this$0;

    protected transient Integer doInBackground(String as[])
    {
        android.content.Context context = mCon;
        long l;
        if (loginInfoModel == null)
        {
            l = 0L;
        } else
        {
            l = loginInfoModel.uid;
        }
        if (loginInfoModel == null)
        {
            as = null;
        } else
        {
            as = loginInfoModel.token;
        }
        as = CommonRequest.doGetMyOrOtherAlbums(context, l, as, AlbumListFragment.access$000(AlbumListFragment.this), AlbumListFragment.access$100(AlbumListFragment.this), AlbumListFragment.access$200(AlbumListFragment.this), mListView, mListView);
        if (Utilities.isNotBlank(as))
        {
            try
            {
                ac = (AlbumCollection)JSON.parseObject(as, com/ximalaya/ting/android/model/album/AlbumCollection);
            }
            // Misplaced declaration of an exception variable
            catch (String as[])
            {
                as.printStackTrace();
            }
            if (ac == null)
            {
                return Integer.valueOf(2);
            } else
            {
                return Integer.valueOf(3);
            }
        } else
        {
            return Integer.valueOf(1);
        }
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((String[])aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (!canGoon())
        {
            return;
        }
        ((PullToRefreshListView)mListView).onRefreshComplete();
        showFooterView(com.ximalaya.ting.android.fragment.ALL);
        switch (integer.intValue())
        {
        default:
            return;

        case 1: // '\001'
        case 2: // '\002'
            Toast.makeText(mCon, getString(0x7f09009b), 0).show();
            showFooterView(com.ximalaya.ting.android.fragment.NNECTION);
            return;

        case 3: // '\003'
            break;
        }
        if (ac.ret == 0)
        {
            AlbumListFragment.access$302(AlbumListFragment.this, ac.maxPageId);
            if (AlbumListFragment.access$400(AlbumListFragment.this) != null && AlbumListFragment.access$100(AlbumListFragment.this) == 1)
            {
                list.clear();
                list.addAll(ac.list);
                AlbumListFragment.access$400(AlbumListFragment.this).notifyDataSetChanged();
                if (list.size() == 0)
                {
                    showFooterView(com.ximalaya.ting.android.fragment.TA);
                }
            } else
            if (AlbumListFragment.access$400(AlbumListFragment.this) != null && AlbumListFragment.access$100(AlbumListFragment.this) > 1)
            {
                list.addAll(ac.list);
                AlbumListFragment.access$400(AlbumListFragment.this).notifyDataSetChanged();
            }
            if (UserInfoMannage.hasLogined())
            {
                AlbumListFragment.access$500(AlbumListFragment.this, ac.list);
            } else
            {
                AlbumListFragment.access$600(AlbumListFragment.this, ac.list);
            }
        } else
        {
            Toast.makeText(mCon, ac.msg, 0).show();
        }
        ac.list = null;
        ac = null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    ()
    {
        this$0 = AlbumListFragment.this;
        super();
    }
}
