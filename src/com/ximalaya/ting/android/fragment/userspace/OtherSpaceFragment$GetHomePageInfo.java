// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.widget.ProgressBar;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.personal_info.HomePageModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            OtherSpaceFragment

class this._cls0 extends MyAsyncTask
{

    HomePageModel homeModel;
    final OtherSpaceFragment this$0;

    protected transient Integer doInBackground(String as[])
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("toUid", (new StringBuilder()).append(OtherSpaceFragment.access$600(OtherSpaceFragment.this)).append("").toString());
        as = null;
        obj = f.a().a(e.F, ((RequestParams) (obj)), fragmentBaseContainerView, OtherSpaceFragment.access$1900(OtherSpaceFragment.this), false);
        if (((com.ximalaya.ting.android.b.) (obj)). == 1)
        {
            as = ((com.ximalaya.ting.android.b.) (obj)).;
        }
        if (Utilities.isBlank(as))
        {
            return Integer.valueOf(1);
        }
        homeModel = (HomePageModel)JSON.parseObject(as, com/ximalaya/ting/android/model/personal_info/HomePageModel);
        if (OtherSpaceFragment.access$2000(OtherSpaceFragment.this) == null)
        {
            break MISSING_BLOCK_LABEL_272;
        }
        Object obj1 = new RequestParams();
        ((RequestParams) (obj1)).put("uids", (new StringBuilder()).append(homeModel.uid).append("").toString());
        obj1 = f.a().a(e.X, ((RequestParams) (obj1)), OtherSpaceFragment.access$1900(OtherSpaceFragment.this), OtherSpaceFragment.access$1900(OtherSpaceFragment.this), false);
        if (((com.ximalaya.ting.android.b.) (obj1)). == 1)
        {
            as = ((com.ximalaya.ting.android.b.) (obj1)).;
        }
        if (!Utilities.isNotBlank(as))
        {
            break MISSING_BLOCK_LABEL_272;
        }
        as = JSON.parseObject(as);
        if (as == null)
        {
            break MISSING_BLOCK_LABEL_272;
        }
        if (as.getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_272;
        }
        as = as.getJSONArray("data");
        if (as != null)
        {
            try
            {
                if (as.size() > 0)
                {
                    as = as.getJSONObject(0);
                    if (homeModel.uid == as.getLongValue("uid"))
                    {
                        homeModel.isFollowed = as.getBooleanValue("isFollow");
                    }
                }
            }
            // Misplaced declaration of an exception variable
            catch (String as[])
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(as.getMessage()).append(Logger.getLineInfo()).toString());
            }
        }
        if (homeModel == null)
        {
            return Integer.valueOf(2);
        } else
        {
            return Integer.valueOf(3);
        }
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((String[])aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (canGoon())
        {
            OtherSpaceFragment.access$1400(OtherSpaceFragment.this).setVisibility(8);
            if (integer.intValue() == 3 && homeModel != null)
            {
                if (homeModel.ret == 0)
                {
                    OtherSpaceFragment.access$2100(OtherSpaceFragment.this, homeModel);
                    return;
                } else
                {
                    Toast.makeText(mCon, homeModel.msg, 0).show();
                    return;
                }
            }
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        OtherSpaceFragment.access$1400(OtherSpaceFragment.this).setVisibility(0);
    }

    ()
    {
        this$0 = OtherSpaceFragment.this;
        super();
    }
}
