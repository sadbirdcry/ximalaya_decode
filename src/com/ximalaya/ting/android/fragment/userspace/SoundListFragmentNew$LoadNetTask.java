// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.widget.RelativeLayout;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            SoundListFragmentNew

private class <init> extends MyAsyncTask
{

    final SoundListFragmentNew this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        SoundListFragmentNew.access$2302(SoundListFragmentNew.this, null);
        avoid = new RequestParams();
        avoid.put("pageId", (new StringBuilder()).append("").append(SoundListFragmentNew.access$1000(SoundListFragmentNew.this)).toString());
        avoid.put("pageSize", (new StringBuilder()).append("").append(SoundListFragmentNew.access$2400(SoundListFragmentNew.this)).toString());
        final ArrayList data = new ArrayList();
        class _cls1 extends h
        {

            final SoundListFragmentNew.LoadNetTask this$1;
            final List val$data;

            public void onBindXDCS(Header aheader[])
            {
                DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
            }

            public void onFinish()
            {
            }

            public void onNetError(int i, String s)
            {
            }

            public void onStart()
            {
            }

            public void onSuccess(String s)
            {
                if (!TextUtils.isEmpty(s))
                {
                    try
                    {
                        s = JSON.parseObject(s);
                    }
                    // Misplaced declaration of an exception variable
                    catch (String s)
                    {
                        s.printStackTrace();
                        s = null;
                    }
                    if (s != null)
                    {
                        if (s.getIntValue("ret") != 0)
                        {
                            SoundListFragmentNew.access$2302(this$0, s.getString("msg"));
                            return;
                        }
                        SoundListFragmentNew.access$902(this$0, s.getIntValue("totalCount"));
                        s = s.getString("list");
                        if (!TextUtils.isEmpty(s))
                        {
                            try
                            {
                                s = JSON.parseArray(s, com/ximalaya/ting/android/model/sound/RecordingModel);
                            }
                            // Misplaced declaration of an exception variable
                            catch (String s)
                            {
                                s.printStackTrace();
                                s = null;
                            }
                            if (s != null && s.size() > 0)
                            {
                                data.addAll(s);
                                return;
                            }
                        }
                    }
                }
            }

            _cls1()
            {
                this$1 = SoundListFragmentNew.LoadNetTask.this;
                data = list;
                super();
            }
        }

        f.a().a(e.A, avoid, DataCollectUtil.getDataFromView(fragmentBaseContainerView), false, new _cls1());
        return data;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(final List result)
    {
        if (!isCancelled() && SoundListFragmentNew.access$1900(SoundListFragmentNew.this).isAdded() && !SoundListFragmentNew.access$2500(SoundListFragmentNew.this).isFinishing() && SoundListFragmentNew.access$300(SoundListFragmentNew.this))
        {
            ((PullToRefreshListView)mListView).onRefreshComplete();
            showFooterView(com.ximalaya.ting.android.fragment.ALL);
            if (SoundListFragmentNew.access$900(SoundListFragmentNew.this) == 0)
            {
                SoundListFragmentNew.access$2100(SoundListFragmentNew.this, true);
                return;
            }
            SoundListFragmentNew.access$2100(SoundListFragmentNew.this, false);
            if (result == null)
            {
                showFooterView(com.ximalaya.ting.android.fragment.NNECTION);
                mFooterViewLoading.setOnClickListener(SoundListFragmentNew.access$2600(SoundListFragmentNew.this));
                return;
            }
            if (result.size() != 0)
            {
                class _cls2
                    implements MyCallback
                {

                    final SoundListFragmentNew.LoadNetTask this$1;
                    final List val$result;

                    public void execute()
                    {
                        if (SoundListFragmentNew.access$1000(this$0) == 1)
                        {
                            SoundListFragmentNew.access$600(this$0).clear();
                        }
                        SoundListFragmentNew.access$600(this$0).addAll(result);
                        SoundListFragmentNew.access$1800(this$0).notifyDataSetChanged();
                        int _tmp = SoundListFragmentNew.access$1008(this$0);
                    }

            _cls2()
            {
                this$1 = SoundListFragmentNew.LoadNetTask.this;
                result = list;
                super();
            }
                }

                SoundListFragmentNew.access$2700(SoundListFragmentNew.this, new _cls2());
                return;
            }
        }
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        showFooterView(com.ximalaya.ting.android.fragment.NG);
        mFooterViewLoading.setOnClickListener(null);
    }

    private _cls2()
    {
        this$0 = SoundListFragmentNew.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
