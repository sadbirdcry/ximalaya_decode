// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.share.ScoreTask;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.List;

public class MyScoreFragment extends BaseActivityLikeFragment
{
    class TaskAdapter extends BaseAdapter
    {

        Context mContext;
        ArrayList mmTasks;
        final MyScoreFragment this$0;

        public void freshData(ArrayList arraylist)
        {
            mmTasks = arraylist;
            if (mmTasks == null)
            {
                mmTasks = new ArrayList(0);
            }
        }

        public int getCount()
        {
            return mmTasks.size();
        }

        public Object getItem(int i)
        {
            return mmTasks.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            viewgroup = view;
            if (view == null)
            {
                viewgroup = LayoutInflater.from(mContext).inflate(0x7f03016d, (RelativeLayout)view, true);
                class _cls1
                    implements android.view.View.OnClickListener
                {

                    final TaskAdapter this$1;

                    public void onClick(View view1)
                    {
                        openShareDialog();
                    }

                _cls1()
                {
                    this$1 = TaskAdapter.this;
                    super();
                }
                }

                viewgroup.setOnClickListener(new _cls1());
            }
            ScoreTask scoretask = (ScoreTask)mmTasks.get(i);
            if (scoretask == null)
            {
                view = null;
            } else
            {
                TextView textview = (TextView)viewgroup.findViewById(0x7f0a0572);
                TextView textview1 = (TextView)viewgroup.findViewById(0x7f0a0571);
                if (scoretask.operatePerDay > 1)
                {
                    view = (new StringBuilder()).append("\u524D").append(scoretask.operatePerDay).append("\u6B21").toString();
                } else
                {
                    view = "\u9996\u6B21";
                }
                textview1.setText(Html.fromHtml((new StringBuilder()).append("\u6BCF\u5929").append(view).append("\u53D1\u9001\u5230\u5FAE\u4FE1\u670B\u53CB\u5708\u6216\u8005\u5FAE\u4FE1\u7FA4\u53D1\u5B8C\u8FD4\u56DE\u5373\u5F97\u5206").toString()));
                if (scoretask.score < 0)
                {
                    textview.setText("\u672A\u83B7\u53D6");
                } else
                {
                    textview.setText((new StringBuilder()).append("+").append(scoretask.score).append("\u5206").toString());
                }
                view = viewgroup;
                if (scoretask.restTimes == 0)
                {
                    textview.setText("");
                    return viewgroup;
                }
            }
            return view;
        }

        public TaskAdapter(ArrayList arraylist, Context context)
        {
            this$0 = MyScoreFragment.this;
            super();
            mmTasks = arraylist;
            mContext = context;
            if (mmTasks == null)
            {
                mmTasks = new ArrayList(0);
            }
        }
    }


    private static final String TAG = "MyScoreFragment";
    private TaskAdapter mAdapter;
    private ListView mListView;
    private MenuDialog mMenuDialog;
    private ScoreManage mScoreManage;
    private TextView mScoreTv;
    private ArrayList tasks;

    public MyScoreFragment()
    {
        tasks = new ArrayList();
    }

    private String buildTransaction(String s)
    {
        if (s == null)
        {
            return String.valueOf(System.currentTimeMillis());
        } else
        {
            return (new StringBuilder()).append(s).append(System.currentTimeMillis()).toString();
        }
    }

    private void initData()
    {
        mScoreManage = ScoreManage.getInstance(mCon);
        if (mScoreManage == null)
        {
            return;
        } else
        {
            ScoreTask scoretask = new ScoreTask();
            scoretask.drawableImg = 0x7f020429;
            scoretask.title = "\u63A8\u8350\u7ED9\u5FAE\u4FE1\u670B\u53CB\u4EEC";
            scoretask.comment = "\u6BCF\u5929\u7B2C\u4E00\u6B21\u53D1\u9001\u5230\u5FAE\u4FE1\u670B\u53CB\u5708\u6216\u8005\u5FAE\u4FE1\u7FA4\u53D1\u5B8C\u8FD4\u56DE\u5373\u5F97\u5206";
            scoretask.score = mScoreManage.getShareScoreConfig(4);
            scoretask.operatePerDay = mScoreManage.getObtainScoreTotalTimes(4);
            scoretask.restTimes = -1;
            tasks.clear();
            tasks.add(scoretask);
            return;
        }
    }

    private void initView()
    {
        mListView = (ListView)fragmentBaseContainerView.findViewById(0x7f0a056e);
        RelativeLayout relativelayout = (RelativeLayout)View.inflate(mCon, 0x7f03016c, null);
        mScoreTv = (TextView)relativelayout.findViewById(0x7f0a056f);
        RelativeLayout relativelayout1 = (RelativeLayout)View.inflate(mCon, 0x7f03016b, null);
        mListView.addHeaderView(relativelayout);
        mListView.addFooterView(relativelayout1);
        mListView.setDividerHeight(0);
        mAdapter = new TaskAdapter(tasks, mCon);
        mListView.setAdapter(mAdapter);
    }

    private void loadData()
    {
        if (!UserInfoMannage.hasLogined())
        {
            showToast("\u60A8\u5F53\u524D\u672A\u767B\u5F55\uFF0C\u8BF7\u767B\u5F55\u540E\u8FDB\u5165");
            if (isAdded() && mActivity != null)
            {
                Intent intent = new Intent(mActivity, com/ximalaya/ting/android/activity/login/LoginActivity);
                intent.setFlags(0x20000000);
                intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(fragmentBaseContainerView));
                mActivity.startActivity(intent);
            }
            return;
        } else
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("behavior", 4);
            f.a().a("mobile/api1/point/query/deduct/rest", requestparams, DataCollectUtil.getDataFromView(mListView), new _cls1());
            return;
        }
    }

    private void openShareDialog()
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add("\u5FAE\u4FE1\u597D\u53CB");
        arraylist.add("\u5FAE\u4FE1\u670B\u53CB\u5708");
        if (mMenuDialog == null)
        {
            mMenuDialog = new MenuDialog(getActivity(), arraylist, a.e, new _cls2());
        } else
        {
            mMenuDialog.setSelections(arraylist);
        }
        mMenuDialog.setHeaderTitle("\u5206\u4EAB\u5230");
        mMenuDialog.show();
    }

    private void setScore(int i)
    {
        if (i < 0)
        {
            mScoreTv.setText("\u672A\u83B7\u53D6");
        } else
        {
            mScoreTv.setText((new StringBuilder()).append("\u6211\u7684\u5206\u6570:").append(i).toString());
            mScoreManage = ScoreManage.getInstance(mCon);
            if (mScoreManage != null)
            {
                mScoreManage.setScore(i);
                return;
            }
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        mActivity = getActivity();
        mCon = mActivity.getApplicationContext();
        super.onActivityCreated(bundle);
        initView();
        initData();
        if (UserInfoMannage.hasLogined())
        {
            mScoreManage = ScoreManage.getInstance(mCon);
            loadData();
        }
        setTitleText("\u6211\u7684\u79EF\u5206");
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03016a, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void sendWXShare(boolean flag)
    {
        Object obj1 = new WXWebpageObject();
        Object obj = new WXMediaMessage();
        if (flag)
        {
            ((MyApplication)(MyApplication)mActivity.getApplication()).b = 4;
            obj1.webpageUrl = "http://m.ximalaya.com/?src=myscore_invite_weixingroup";
        } else
        {
            ((MyApplication)(MyApplication)mActivity.getApplication()).b = 2;
            obj1.webpageUrl = "http://m.ximalaya.com/?src=myscore_invite_weixin";
        }
        obj.title = "\u559C\u9A6C\u62C9\u96C5";
        obj.description = "\u8FD9\u662F\u4E2A\u6253\u53D1\u65E0\u804A\u7684\u795E\u5668\uFF0C\u6211\u6700\u8FD1\u5929\u5929\u7528\uFF0C\u771F8\u9519\uFF0C\u4F60\u8BD5\u8BD5\u770B";
        obj.thumbData = ToolUtil.imageZoom32(BitmapFactory.decodeResource(mActivity.getResources(), 0x7f020598));
        obj.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj1));
        obj1 = new com.tencent.mm.sdk.modelmsg.SendMessageToWX.Req();
        obj1.transaction = buildTransaction("webpage");
        obj1.message = ((WXMediaMessage) (obj));
        if (flag)
        {
            obj1.scene = 1;
        } else
        {
            obj1.scene = 0;
        }
        obj = WXAPIFactory.createWXAPI(mCon, b.b, false);
        ((IWXAPI) (obj)).registerApp(b.b);
        if (!((IWXAPI) (obj)).sendReq(((com.tencent.mm.sdk.modelbase.BaseReq) (obj1))))
        {
            showToast("\u5206\u4EAB\u5931\u8D25\uFF0C\u8BF7\u5148\u5B89\u88C5\u5FAE\u4FE1");
        }
    }









/*
    static MenuDialog access$602(MyScoreFragment myscorefragment, MenuDialog menudialog)
    {
        myscorefragment.mMenuDialog = menudialog;
        return menudialog;
    }

*/

    private class _cls1 extends com.ximalaya.ting.android.b.a
    {

        final MyScoreFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mListView);
        }

        public void onNetError(int i, String s)
        {
            if (getActivity() != null && !getActivity().isFinishing())
            {
                showToast(getActivity().getString(0x7f0901f8));
            }
            Logger.d("MyScoreFragment", (new StringBuilder()).append("the error response is:").append(s).toString());
            if (mScoreManage != null && mScoreManage.getScore() >= 0)
            {
                setScore(mScoreManage.getScore());
            }
        }

        public void onSuccess(String s)
        {
            Logger.d("MyScoreFragment", (new StringBuilder()).append("the success response is:").append(s).toString());
            s = JSONObject.parseObject(s);
            if (s.getIntValue("ret") != 0)
            {
                break MISSING_BLOCK_LABEL_125;
            }
            int i = s.getIntValue("userPoint");
            setScore(i);
            if (tasks.size() == 0)
            {
                break MISSING_BLOCK_LABEL_125;
            }
            if (tasks.get(0) == null)
            {
                return;
            }
            try
            {
                ((ScoreTask)tasks.get(0)).restTimes = s.getIntValue("restTimes");
                mAdapter.notifyDataSetChanged();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                showToast("\u6570\u636E\u8F6C\u5316\u9519\u8BEF");
            }
        }

        _cls1()
        {
            this$0 = MyScoreFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final MyScoreFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (mMenuDialog != null)
            {
                mMenuDialog.dismiss();
                mMenuDialog = null;
            }
            switch (i)
            {
            default:
                return;

            case 0: // '\0'
                sendWXShare(false);
                return;

            case 1: // '\001'
                sendWXShare(true);
                break;
            }
        }

        _cls2()
        {
            this$0 = MyScoreFragment.this;
            super();
        }
    }

}
