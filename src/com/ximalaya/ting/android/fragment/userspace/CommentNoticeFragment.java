// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.adapter.NoticeAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.message.BaseCommentModel;
import com.ximalaya.ting.android.model.message.CommentListInCommentNotice;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

public class CommentNoticeFragment extends BaseActivityLikeFragment
{
    class DelMySendComment extends MyAsyncTask
    {

        ProgressDialog pd;
        int pos;
        BaseModel rr;
        final CommentNoticeFragment this$0;

        protected transient Integer doInBackground(Object aobj[])
        {
            pos = Integer.valueOf((String)aobj[3]).intValue();
            RequestParams requestparams = new RequestParams();
            String s;
            if ((String)aobj[0] == null)
            {
                s = "";
            } else
            {
                s = (String)aobj[0];
            }
            requestparams.put("commentId", s);
            if ((String)aobj[1] == null)
            {
                s = "";
            } else
            {
                s = (String)aobj[1];
            }
            requestparams.put("trackId", s);
            if ((String)aobj[2] == null)
            {
                s = "";
            } else
            {
                s = (String)aobj[2];
            }
            requestparams.put("messageId", s);
            aobj = f.a().b(e.q, requestparams, (View)aobj[4], null);
            if (Utilities.isNotBlank(((String) (aobj))))
            {
                rr = (BaseModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/BaseModel);
                if (rr != null)
                {
                    if (rr.ret == 0)
                    {
                        return Integer.valueOf(3);
                    } else
                    {
                        return Integer.valueOf(2);
                    }
                }
            }
            return Integer.valueOf(1);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (canGoon())
            {
                if (pd != null)
                {
                    pd.cancel();
                    pd = null;
                }
                if (integer.intValue() == 3)
                {
                    if (pos != 0)
                    {
                        noticeAdapter.getCicn().list.remove(pos - 1);
                        noticeAdapter.notifyDataSetChanged();
                        return;
                    }
                } else
                if (integer.intValue() == 2)
                {
                    Toast.makeText(mCon, rr.msg, 0).show();
                    return;
                } else
                {
                    Toast.makeText(mCon, "\u5220\u9664\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5~", 0).show();
                    return;
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            pd = new MyProgressDialog(getActivity());
            pd.setMessage("\u6B63\u5728\u5220\u9664...");
            pd.show();
        }

        DelMySendComment()
        {
            this$0 = CommentNoticeFragment.this;
            super();
            pos = 0;
        }
    }

    class GetMyReceiveComment extends MyAsyncTask
    {

        CommentListInCommentNotice cicn;
        final CommentNoticeFragment this$0;

        protected transient Integer doInBackground(Object aobj[])
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("pageSize", (new StringBuilder()).append(msg_list_request_count).append("").toString());
            requestparams.put("key", notice_key);
            requestparams.put("isDown", (new StringBuilder()).append(notice_isDown).append("").toString());
            aobj = f.a().b(e.d, requestparams, (View)aobj[0], noticeListView);
            if (Utilities.isNotBlank(((String) (aobj))))
            {
                try
                {
                    cicn = (CommentListInCommentNotice)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/message/CommentListInCommentNotice);
                }
                // Misplaced declaration of an exception variable
                catch (Object aobj[]) { }
                if (cicn != null)
                {
                    if (cicn.ret == 0)
                    {
                        return Integer.valueOf(3);
                    } else
                    {
                        return Integer.valueOf(2);
                    }
                }
            }
            return Integer.valueOf(1);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            while (!canGoon() || rg_commentNotice.getCheckedRadioButtonId() != 0x7f0a007d) 
            {
                return;
            }
            class _cls1
                implements Runnable
            {

                final GetMyReceiveComment this$1;

                public void run()
                {
                    noticeListView.setAdapter(noticeAdapter);
                }

                _cls1()
                {
                    this$1 = GetMyReceiveComment.this;
                    super();
                }
            }

            _cls1 _lcls1;
            if (cicn == null || cicn.list == null || cicn.list.size() <= 0)
            {
                showEmptyView(true);
            } else
            {
                showEmptyView(false);
            }
            if (noticeListView.isRefreshing())
            {
                noticeListView.onRefreshComplete();
            }
            if (integer.intValue() != 3) goto _L2; else goto _L1
_L1:
            cicn.sendType = "\u6536\u5230\u7684";
            if (noticeAdapter != null) goto _L4; else goto _L3
_L3:
            noticeAdapter = new NoticeAdapter(getActivity(), cicn);
            long l = getAnimationLeftTime();
            integer = noticeListView;
            _lcls1 = new _cls1();
            if (l <= 0L)
            {
                l = 0L;
            }
            integer.postDelayed(_lcls1, l);
            ll_progress.setVisibility(8);
_L6:
            isReceiveTaskLoading = false;
            return;
_L4:
            if (noticeAdapter != null && notice_key.equals("0"))
            {
                if (cicn != null)
                {
                    noticeAdapter.setCicn(cicn);
                }
                noticeAdapter.notifyDataSetChanged();
            } else
            {
                noticeAdapter.getCicn().list.remove(noticeAdapter.getCount() - 1);
                if (cicn != null && cicn.list != null)
                {
                    cicn.list.addAll(0, noticeAdapter.getCicn().list);
                    noticeAdapter.setCicn(cicn);
                    noticeAdapter.notifyDataSetChanged();
                }
            }
            continue; /* Loop/switch isn't completed */
_L2:
            if (noticeAdapter == null)
            {
                firstRequestFail();
            } else
            if (noticeAdapter != null && notice_key.equals("0"))
            {
                makeToast(getString(0x7f09009b));
            } else
            {
                noticeAdapter.getCicn().list.remove(noticeAdapter.getCount() - 1);
                makeToast(getString(0x7f09009b));
            }
            if (true) goto _L6; else goto _L5
_L5:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            isReceiveTaskLoading = true;
            showEmptyView(false);
        }

        GetMyReceiveComment()
        {
            this$0 = CommentNoticeFragment.this;
            super();
        }
    }

    class GetMySendComments extends MyAsyncTask
    {

        CommentListInCommentNotice cicn;
        final CommentNoticeFragment this$0;

        protected transient Integer doInBackground(Object aobj[])
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("pageSize", (new StringBuilder()).append(msg_list_request_count).append("").toString());
            requestparams.put("key", notice_key);
            requestparams.put("isDown", (new StringBuilder()).append(notice_isDown).append("").toString());
            aobj = f.a().b(e.e, requestparams, (View)aobj[0], noticeListView);
            if (Utilities.isNotBlank(((String) (aobj))))
            {
                try
                {
                    cicn = (CommentListInCommentNotice)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/message/CommentListInCommentNotice);
                }
                // Misplaced declaration of an exception variable
                catch (Object aobj[])
                {
                    ((Exception) (aobj)).printStackTrace();
                }
                if (cicn != null)
                {
                    if (cicn.ret == 0)
                    {
                        return Integer.valueOf(3);
                    } else
                    {
                        return Integer.valueOf(2);
                    }
                }
            }
            return Integer.valueOf(1);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            while (!canGoon() || rg_commentNotice.getCheckedRadioButtonId() != 0x7f0a007e) 
            {
                return;
            }
            if (noticeListView.isRefreshing())
            {
                noticeListView.onRefreshComplete();
            }
            if (integer.intValue() == 3)
            {
                if (cicn == null || cicn.list == null || cicn.list.size() <= 0)
                {
                    showEmptyView(true);
                    return;
                }
                showEmptyView(false);
                cicn.sendType = "\u53D1\u51FA\u7684";
                if (noticeAdapter == null)
                {
                    noticeAdapter = new NoticeAdapter(getActivity(), cicn);
                    noticeListView.setAdapter(noticeAdapter);
                    ll_progress.setVisibility(8);
                } else
                if (noticeAdapter != null && notice_key.equals("0"))
                {
                    if (cicn != null)
                    {
                        noticeAdapter.setCicn(cicn);
                    }
                    noticeAdapter.notifyDataSetChanged();
                } else
                {
                    noticeAdapter.getCicn().list.remove(noticeAdapter.getCount() - 1);
                    cicn.list.addAll(0, noticeAdapter.getCicn().list);
                    noticeAdapter.setCicn(cicn);
                    noticeAdapter.notifyDataSetChanged();
                }
            } else
            if (noticeAdapter == null)
            {
                firstRequestFail();
            } else
            if (noticeAdapter != null && notice_key.equals("0"))
            {
                makeToast(getString(0x7f09009b));
            } else
            {
                noticeAdapter.getCicn().list.remove(noticeAdapter.getCount() - 1);
                makeToast(getString(0x7f09009b));
            }
            isSendTaskLoading = false;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            isSendTaskLoading = true;
            showEmptyView(false);
        }

        GetMySendComments()
        {
            this$0 = CommentNoticeFragment.this;
            super();
        }
    }


    private final int GO_SEND_COMMENT = 175;
    private volatile boolean isReceiveTaskLoading;
    private volatile boolean isSendTaskLoading;
    private LinearLayout ll_progress;
    private LinearLayout mEmptyView;
    private int msg_list_request_count;
    private View networkError;
    private NoticeAdapter noticeAdapter;
    private long noticeArray[];
    private PullToRefreshListView noticeListView;
    private boolean notice_isDown;
    private String notice_key;
    private RadioGroup rg_commentNotice;

    public CommentNoticeFragment()
    {
        msg_list_request_count = 37;
        notice_isDown = false;
        notice_key = "0";
        isSendTaskLoading = false;
        isReceiveTaskLoading = false;
    }

    private void findViews()
    {
        rg_commentNotice = (RadioGroup)findViewById(0x7f0a007c);
        networkError = findViewById(0x7f0a0079);
        noticeListView = (PullToRefreshListView)findViewById(0x7f0a007a);
        ll_progress = (LinearLayout)findViewById(0x7f0a0073);
        mEmptyView = (LinearLayout)LayoutInflater.from(mActivity).inflate(0x7f03007c, noticeListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u8BC4\u8BBA\u54E6");
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setVisibility(8);
        ((Button)mEmptyView.findViewById(0x7f0a0232)).setVisibility(8);
        noticeListView.addFooterView(mEmptyView);
    }

    private void firstRequestFail()
    {
        ll_progress.setVisibility(8);
        networkError.setVisibility(0);
        noticeListView.setVisibility(8);
    }

    private String[] getNoticeNeedItem(BaseCommentModel basecommentmodel)
    {
        if (NoticeAdapter.curType.equals("\u53D1\u51FA\u7684"))
        {
            String s = (new StringBuilder()).append("\u67E5\u770B").append(basecommentmodel.getToNickname()).append("\u7684\u8D44\u6599").toString();
            noticeArray = new long[3];
            noticeArray[0] = basecommentmodel.getToUid().longValue();
            noticeArray[1] = -1L;
            noticeArray[2] = -2L;
            return (new String[] {
                s, "\u56DE\u590D", "\u5220\u9664"
            });
        }
        if (NoticeAdapter.curType.equals("\u6536\u5230\u7684"))
        {
            if (basecommentmodel.uid.longValue() == loginInfoModel.uid)
            {
                String s1 = (new StringBuilder()).append("\u67E5\u770B").append(basecommentmodel.getNickname()).append("\u7684\u8D44\u6599").toString();
                noticeArray = new long[3];
                noticeArray[0] = basecommentmodel.getUid().longValue();
                noticeArray[1] = -1L;
                noticeArray[2] = -2L;
                return (new String[] {
                    s1, "\u56DE\u590D", "\u5220\u9664"
                });
            } else
            {
                String s2 = (new StringBuilder()).append("\u67E5\u770B").append(basecommentmodel.getNickname()).append("\u7684\u8D44\u6599").toString();
                noticeArray = new long[2];
                noticeArray[0] = basecommentmodel.getUid().longValue();
                noticeArray[1] = -1L;
                return (new String[] {
                    s2, "\u56DE\u590D"
                });
            }
        } else
        {
            return null;
        }
    }

    private void initData()
    {
        ll_progress.setVisibility(0);
        NoticeAdapter.curType = "\u6536\u5230\u7684";
        noticeListView.toRefreshing();
    }

    private void initViews()
    {
        initCommon();
        rg_commentNotice.setOnCheckedChangeListener(new _cls1());
        networkError.setOnClickListener(new _cls2());
        noticeListView.setOnScrollListener(new _cls3());
        noticeListView.setOnRefreshListener(new _cls4());
        noticeListView.setOnItemClickListener(new _cls5());
    }

    private void makeToast(String s)
    {
        Toast.makeText(mCon, s, 1).show();
    }

    private void showEmptyView(boolean flag)
    {
        String s;
        if (NoticeAdapter.curType == "\u53D1\u51FA\u7684")
        {
            s = "\u4EB2~ \u8FD8\u6CA1\u6709\u4F60\u7684\u8BC4\u8BBA\u54E6";
        } else
        if (NoticeAdapter.curType == "\u6536\u5230\u7684")
        {
            s = "\u4EB2~ \u8FD8\u6CA1\u6709\u522B\u4EBA\u7684\u8BC4\u8BBA\u54E6";
        } else
        {
            s = "\u4EB2~ \u8FD8\u6CA1\u6709\u8BC4\u8BBA\u54E6";
        }
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText(s);
        if (flag)
        {
            mEmptyView.setVisibility(0);
            return;
        } else
        {
            mEmptyView.setVisibility(8);
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        findViews();
        initViews();
        initData();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        noticeListView.toRefreshing();
        if (i == 175 && j == -1)
        {
            Toast.makeText(mCon, "\u56DE\u590D\u6210\u529F", 0).show();
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03000c, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        if (noticeAdapter != null)
        {
            noticeAdapter.setDataNull();
            noticeAdapter = null;
        }
        super.onDestroyView();
    }





/*
    static NoticeAdapter access$102(CommentNoticeFragment commentnoticefragment, NoticeAdapter noticeadapter)
    {
        commentnoticefragment.noticeAdapter = noticeadapter;
        return noticeadapter;
    }

*/







/*
    static String access$202(CommentNoticeFragment commentnoticefragment, String s)
    {
        commentnoticefragment.notice_key = s;
        return s;
    }

*/



/*
    static boolean access$302(CommentNoticeFragment commentnoticefragment, boolean flag)
    {
        commentnoticefragment.notice_isDown = flag;
        return flag;
    }

*/






/*
    static boolean access$702(CommentNoticeFragment commentnoticefragment, boolean flag)
    {
        commentnoticefragment.isSendTaskLoading = flag;
        return flag;
    }

*/



/*
    static boolean access$802(CommentNoticeFragment commentnoticefragment, boolean flag)
    {
        commentnoticefragment.isReceiveTaskLoading = flag;
        return flag;
    }

*/


    private class _cls1
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final CommentNoticeFragment this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            noticeListView.setSelectionAfterHeaderView();
            if (noticeAdapter != null)
            {
                noticeAdapter.setCicn(null);
                noticeAdapter.notifyDataSetChanged();
            }
            i;
            JVM INSTR tableswitch 2131361917 2131361918: default 64
        //                       2131361917 88
        //                       2131361918 144;
               goto _L1 _L2 _L3
_L1:
            if (!noticeListView.isRefreshing())
            {
                noticeListView.toRefreshing();
            }
            return;
_L2:
            notice_key = "0";
            notice_isDown = false;
            NoticeAdapter.curType = "\u6536\u5230\u7684";
            (new GetMyReceiveComment()).myexec(new Object[] {
                rg_commentNotice
            });
            continue; /* Loop/switch isn't completed */
_L3:
            notice_key = "0";
            notice_isDown = false;
            NoticeAdapter.curType = "\u53D1\u51FA\u7684";
            (new GetMySendComments()).myexec(new Object[] {
                rg_commentNotice
            });
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls1()
        {
            this$0 = CommentNoticeFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final CommentNoticeFragment this$0;

        public void onClick(View view)
        {
            ll_progress.setVisibility(0);
            networkError.setVisibility(8);
            noticeListView.setVisibility(0);
            noticeListView.toRefreshing();
        }

        _cls2()
        {
            this$0 = CommentNoticeFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AbsListView.OnScrollListener
    {

        final CommentNoticeFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            noticeListView.onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0 && noticeAdapter != null && noticeAdapter.getCicn() != null)
            {
                i = abslistview.getCount();
                if (i > 5)
                {
                    i -= 5;
                } else
                {
                    i--;
                }
                if (abslistview.getLastVisiblePosition() > i && noticeAdapter.getCicn().maxPageId > noticeAdapter.getCicn().pageId && !noticeListView.isRefreshing() && noticeAdapter.getCicn().list != null && (!NoticeAdapter.curType.equals("\u53D1\u51FA\u7684") || !isSendTaskLoading) && (!NoticeAdapter.curType.equals("\u6536\u5230\u7684") || !isReceiveTaskLoading))
                {
                    notice_key = (new StringBuilder()).append(((BaseCommentModel)noticeAdapter.getCicn().list.get(noticeAdapter.getCount() - 1)).getId()).append("").toString();
                    notice_isDown = true;
                    abslistview = new BaseCommentModel();
                    abslistview.flag = false;
                    noticeAdapter.getCicn().list.add(abslistview);
                    noticeAdapter.notifyDataSetChanged();
                    noticeListView.setSelection(noticeAdapter.getCount() - 1);
                    if (NoticeAdapter.curType.equals("\u53D1\u51FA\u7684"))
                    {
                        (new GetMySendComments()).myexec(new Object[] {
                            noticeListView
                        });
                        return;
                    }
                    if (NoticeAdapter.curType.equals("\u6536\u5230\u7684"))
                    {
                        (new GetMyReceiveComment()).myexec(new Object[] {
                            noticeListView
                        });
                        return;
                    }
                }
            }
        }

        _cls3()
        {
            this$0 = CommentNoticeFragment.this;
            super();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final CommentNoticeFragment this$0;

        public void onRefresh()
        {
            if ((!NoticeAdapter.curType.equals("\u53D1\u51FA\u7684") || !isSendTaskLoading) && (!NoticeAdapter.curType.equals("\u6536\u5230\u7684") || !isReceiveTaskLoading))
            {
                notice_isDown = false;
                if (NoticeAdapter.curType.equals("\u53D1\u51FA\u7684"))
                {
                    (new GetMySendComments()).myexec(new Object[] {
                        noticeListView
                    });
                    return;
                }
                if (NoticeAdapter.curType.equals("\u6536\u5230\u7684"))
                {
                    (new GetMyReceiveComment()).myexec(new Object[] {
                        noticeListView
                    });
                    return;
                }
            }
        }

        _cls4()
        {
            this$0 = CommentNoticeFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CommentNoticeFragment this$0;

        public void onItemClick(final AdapterView dialog, final View viewInList, final int position, long l)
        {
label0:
            {
                if (position != 0 && noticeAdapter != null)
                {
                    dialog = noticeAdapter.getItem(position - 1);
                    if (dialog != null)
                    {
                        break label0;
                    }
                }
                return;
            }
            dialog = new MenuDialog(getActivity(), Arrays.asList(getNoticeNeedItem(dialog)));
            class _cls1
                implements android.widget.AdapterView.OnItemClickListener
            {

                final _cls5 this$1;
                final MenuDialog val$dialog;
                final int val$position;
                final View val$viewInList;

                public void onItemClick(AdapterView adapterview, View view, int i, long l1)
                {
                    if (dialog != null)
                    {
                        dialog.dismiss();
                    }
                    l1 = noticeArray[i];
                    if (l1 != -1L) goto _L2; else goto _L1
_L1:
                    if (noticeAdapter != null) goto _L4; else goto _L3
_L3:
                    return;
_L4:
                    adapterview = noticeAdapter.getItem(position - 1);
                    view = new Intent(mCon, com/ximalaya/ting/android/activity/comments/Send_CommentAct);
                    view.putExtra("trackId", (new StringBuilder()).append(adapterview.getSicm().getTrackId()).append("").toString());
                    view.putExtra("parentId", adapterview.getSicm().getPcommentId());
                    if (!NoticeAdapter.curType.equals("\u53D1\u51FA\u7684")) goto _L6; else goto _L5
_L5:
                    view.putExtra("nickName", adapterview.getToNickname());
_L7:
                    startActivityForResult(view, 175);
                    return;
_L6:
                    if (NoticeAdapter.curType.equals("\u6536\u5230\u7684"))
                    {
                        view.putExtra("nickName", adapterview.getNickname());
                    }
                    if (true) goto _L7; else goto _L2
_L2:
                    if (l1 == -2L)
                    {
                        if (noticeAdapter != null)
                        {
                            adapterview = noticeAdapter.getItem(position - 1);
                            view = adapterview.getSicm();
                            (new DelMySendComment()).myexec(new Object[] {
                                (new StringBuilder()).append(view.getCommentId()).append("").toString(), (new StringBuilder()).append(view.getTrackId()).append("").toString(), (new StringBuilder()).append(adapterview.getId()).append("").toString(), (new StringBuilder()).append(position).append("").toString(), viewInList
                            });
                            return;
                        }
                    } else
                    {
                        adapterview = new Bundle();
                        adapterview.putLong("toUid", l1);
                        adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                        startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, adapterview);
                        return;
                    }
                    if (true) goto _L3; else goto _L8
_L8:
                }

                _cls1()
                {
                    this$1 = _cls5.this;
                    dialog = menudialog;
                    position = i;
                    viewInList = view;
                    super();
                }
            }

            dialog.setOnItemClickListener(new _cls1());
            dialog.show();
        }

        _cls5()
        {
            this$0 = CommentNoticeFragment.this;
            super();
        }
    }

}
