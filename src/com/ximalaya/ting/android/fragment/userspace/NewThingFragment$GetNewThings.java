// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.view.View;
import android.widget.LinearLayout;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.adapter.NewThingsAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.feed.MsgFeedCollection;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.userspace:
//            NewThingFragment

class mfc extends MyAsyncTask
{

    MsgFeedCollection mfc;
    final NewThingFragment this$0;

    protected transient Integer doInBackground(Object aobj[])
    {
        if (loginInfoModel == null)
        {
            return Integer.valueOf(2);
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("pageSize", (new StringBuilder()).append(NewThingFragment.access$000(NewThingFragment.this)).append("").toString());
        if (NewThingFragment.access$100(NewThingFragment.this) != 0)
        {
            requestparams.put("timeLine", (new StringBuilder()).append(NewThingFragment.access$200(NewThingFragment.this)).append("").toString());
        } else
        {
            requestparams.put("timeLine", "0");
        }
        requestparams.put("type", (new StringBuilder()).append(NewThingFragment.access$100(NewThingFragment.this)).append("").toString());
        aobj = f.a().a(e.c, requestparams, (View)aobj[0], NewThingFragment.access$300(NewThingFragment.this), false);
        if (((com.ximalaya.ting.android.b.ent) (aobj)).ent == 1)
        {
            aobj = ((com.ximalaya.ting.android.b.ent) (aobj)).ent;
        } else
        {
            aobj = null;
        }
        try
        {
            mfc = (MsgFeedCollection)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/feed/MsgFeedCollection);
        }
        // Misplaced declaration of an exception variable
        catch (Object aobj[])
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
        }
        if (mfc == null)
        {
            return Integer.valueOf(1);
        }
        if (mfc.ret != 0)
        {
            return Integer.valueOf(2);
        } else
        {
            return Integer.valueOf(3);
        }
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (canGoon()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        NewThingFragment.access$300(NewThingFragment.this).onRefreshComplete();
        if (integer.intValue() != 3)
        {
            break; /* Loop/switch isn't completed */
        }
        if (mfc == null || mfc.getBaseFeedList() == null || mfc.getBaseFeedList().size() <= 0)
        {
            NewThingFragment.access$400(NewThingFragment.this, true);
        } else
        {
            NewThingFragment.access$400(NewThingFragment.this, false);
            NewThingFragment.access$500(NewThingFragment.this);
        }
        NewThingFragment.access$300(NewThingFragment.this).setVisibility(0);
        if (mfc != null)
        {
            if (NewThingFragment.access$600(NewThingFragment.this) == null)
            {
                NewThingFragment.access$602(NewThingFragment.this, new NewThingsAdapter(NewThingFragment.access$700(NewThingFragment.this), getActivity(), mfc, loginInfoModel.uid, loginInfoModel.token));
                NewThingFragment.access$800(NewThingFragment.this).setVisibility(8);
                long l = 300L;
                integer = NewThingFragment.access$300(NewThingFragment.this);
                class _cls1
                    implements Runnable
                {

                    final NewThingFragment.GetNewThings this$1;

                    public void run()
                    {
                        if (NewThingFragment.access$600(this$0) != null && NewThingFragment.access$300(this$0) != null)
                        {
                            NewThingFragment.access$300(this$0).setAdapter(NewThingFragment.access$600(this$0));
                            NewThingFragment.access$600(this$0).notifyDataSetChanged();
                        }
                    }

            _cls1()
            {
                this$1 = NewThingFragment.GetNewThings.this;
                super();
            }
                }

                _cls1 _lcls1 = new _cls1();
                if (300L <= 0L)
                {
                    l = 0L;
                }
                integer.postDelayed(_lcls1, l);
                return;
            }
            if (NewThingFragment.access$600(NewThingFragment.this) != null && NewThingFragment.access$100(NewThingFragment.this) == 0)
            {
                NewThingFragment.access$600(NewThingFragment.this).setMfc(mfc);
                NewThingFragment.access$300(NewThingFragment.this).onRefreshComplete();
                NewThingFragment.access$600(NewThingFragment.this).clearShowList();
                NewThingFragment.access$600(NewThingFragment.this).notifyDataSetChanged();
                return;
            }
            if (NewThingFragment.access$600(NewThingFragment.this) != null && NewThingFragment.access$100(NewThingFragment.this) == -1)
            {
                int i = NewThingFragment.access$600(NewThingFragment.this).getCount() - 1;
                if (NewThingFragment.access$600(NewThingFragment.this).getMfc() != null && NewThingFragment.access$600(NewThingFragment.this).getMfc().getBaseFeedList() != null && mfc.getBaseFeedList() != null && i < NewThingFragment.access$600(NewThingFragment.this).getMfc().getBaseFeedList().size())
                {
                    NewThingFragment.access$600(NewThingFragment.this).getMfc().getBaseFeedList().remove(i);
                    NewThingFragment.access$600(NewThingFragment.this).getMfc().getBaseFeedList().addAll(mfc.getBaseFeedList());
                    NewThingFragment.access$600(NewThingFragment.this).notifyDataSetChanged();
                    return;
                }
            }
        }
        if (true) goto _L1; else goto _L3
_L3:
        if (integer.intValue() != 1)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (NewThingFragment.access$600(NewThingFragment.this) == null)
        {
            NewThingFragment.access$900(NewThingFragment.this);
            return;
        }
        if (NewThingFragment.access$600(NewThingFragment.this) != null && NewThingFragment.access$100(NewThingFragment.this) == 0)
        {
            NewThingFragment.access$300(NewThingFragment.this).onRefreshComplete();
            NewThingFragment.access$1000(NewThingFragment.this, getString(0x7f09009b));
            return;
        }
        if (NewThingFragment.access$600(NewThingFragment.this) != null && NewThingFragment.access$100(NewThingFragment.this) == -1)
        {
            NewThingFragment.access$600(NewThingFragment.this).getMfc().getBaseFeedList().remove(NewThingFragment.access$600(NewThingFragment.this).getCount() - 1);
            NewThingFragment.access$600(NewThingFragment.this).notifyDataSetChanged();
            NewThingFragment.access$1000(NewThingFragment.this, getString(0x7f09009b));
            return;
        }
        continue; /* Loop/switch isn't completed */
        if (integer.intValue() != 2) goto _L1; else goto _L4
_L4:
        if (NewThingFragment.access$600(NewThingFragment.this) != null && NewThingFragment.access$100(NewThingFragment.this) == 0)
        {
            NewThingFragment.access$300(NewThingFragment.this).onRefreshComplete();
            return;
        }
        if (NewThingFragment.access$600(NewThingFragment.this) != null && NewThingFragment.access$100(NewThingFragment.this) == -1)
        {
            NewThingFragment.access$600(NewThingFragment.this).getMfc().getBaseFeedList().remove(NewThingFragment.access$600(NewThingFragment.this).getCount() - 1);
            NewThingFragment.access$600(NewThingFragment.this).notifyDataSetChanged();
            return;
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    _cls1()
    {
        this$0 = NewThingFragment.this;
        super();
        mfc = null;
    }
}
