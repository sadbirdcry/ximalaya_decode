// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.userspace;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.ting.HistoryFragment;
import com.ximalaya.ting.android.view.SlideRightOutView;

public class PlayListHostoryFragment extends BaseActivityLikeFragment
{

    public static final String FROM = "from";
    public static final int FROM_FEED = 1;
    private boolean mExitWhenPlay;
    private com.ximalaya.ting.android.view.SlideRightOutView.OnFinishListener mOnFinishCallback;
    private SlideRightOutView mSlideOutView;

    public PlayListHostoryFragment()
    {
    }

    private void initViews()
    {
        setTitleText("\u64AD\u653E\u5386\u53F2");
        HistoryFragment historyfragment = new HistoryFragment();
        historyfragment.setOnFinishCallback(new _cls3());
        Bundle bundle = new Bundle();
        bundle.putBoolean("exit_when_play", mExitWhenPlay);
        historyfragment.setArguments(bundle);
        bundle = getArguments();
        if (bundle != null && bundle.getInt("from") == 1 && findViewById(0x7f0a0066) != null)
        {
            findViewById(0x7f0a0066).setVisibility(8);
            ((SlideRightOutView)findViewById(0x7f0a0055)).setSlide(false);
        }
        getChildFragmentManager().beginTransaction().add(0x7f0a00cb, historyfragment).commit();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initViews();
        mSlideOutView.setOnFinishListener(new _cls1());
        retButton.setOnClickListener(new _cls2());
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mExitWhenPlay = bundle.getBoolean("exit_when_play", false);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030020, null);
        mSlideOutView = (SlideRightOutView)fragmentBaseContainerView;
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void setOnFinishCallback(com.ximalaya.ting.android.view.SlideRightOutView.OnFinishListener onfinishlistener)
    {
        mOnFinishCallback = onfinishlistener;
    }



    private class _cls3
        implements com.ximalaya.ting.android.view.SlideRightOutView.OnFinishListener
    {

        final PlayListHostoryFragment this$0;

        public boolean onFinish()
        {
            getFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020).remove(PlayListHostoryFragment.this).commitAllowingStateLoss();
            return false;
        }

        _cls3()
        {
            this$0 = PlayListHostoryFragment.this;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.view.SlideRightOutView.OnFinishListener
    {

        final PlayListHostoryFragment this$0;

        public boolean onFinish()
        {
            if (mOnFinishCallback != null)
            {
                return mOnFinishCallback.onFinish();
            } else
            {
                return false;
            }
        }

        _cls1()
        {
            this$0 = PlayListHostoryFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final PlayListHostoryFragment this$0;

        public void onClick(View view)
        {
            boolean flag = false;
            if (mOnFinishCallback != null)
            {
                flag = mOnFinishCallback.onFinish();
            }
            if (!flag)
            {
                finishFragment();
            }
        }

        _cls2()
        {
            this$0 = PlayListHostoryFragment.this;
            super();
        }
    }

}
