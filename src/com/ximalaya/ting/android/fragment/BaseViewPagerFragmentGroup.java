// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseFragment, GroupFragmentAdapter

public class BaseViewPagerFragmentGroup extends BaseFragment
    implements android.support.v4.view.ViewPager.OnPageChangeListener
{
    public class ClassInfo
    {

        Bundle bundle;
        Class className;
        FragmentManager fragmentManager;
        final BaseViewPagerFragmentGroup this$0;

        public ClassInfo()
        {
            this$0 = BaseViewPagerFragmentGroup.this;
            super();
            bundle = null;
        }
    }


    private int initSelectedPage;
    public String mCurrentFragmentTag;
    private List mFragmentClassInfoList;
    public GroupFragmentAdapter mGroupFragmentAdapter;
    private ViewPager pager;
    private RelativeLayout titleBarView;

    public BaseViewPagerFragmentGroup()
    {
        mFragmentClassInfoList = new ArrayList();
    }

    public void addFragment(Class class1)
    {
        ClassInfo classinfo = new ClassInfo();
        classinfo.className = class1;
        mFragmentClassInfoList.add(classinfo);
        mGroupFragmentAdapter.notifyDataSetChanged();
    }

    public void addFragment(Class class1, Bundle bundle)
    {
        ClassInfo classinfo = new ClassInfo();
        classinfo.className = class1;
        classinfo.bundle = bundle;
        mFragmentClassInfoList.add(classinfo);
        mGroupFragmentAdapter.notifyDataSetChanged();
    }

    public void addFragment(Class class1, FragmentManager fragmentmanager)
    {
        ClassInfo classinfo = new ClassInfo();
        classinfo.className = class1;
        classinfo.fragmentManager = fragmentmanager;
        mFragmentClassInfoList.add(classinfo);
        mGroupFragmentAdapter.notifyDataSetChanged();
    }

    public Fragment getCurrentFragment()
    {
        return mGroupFragmentAdapter.getFragment(getCurrentNum());
    }

    public int getCurrentNum()
    {
        if (pager != null)
        {
            return pager.getCurrentItem();
        } else
        {
            return 0;
        }
    }

    public void init()
    {
        if (mFragmentClassInfoList == null)
        {
            mFragmentClassInfoList = new ArrayList();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        pager.setCurrentItem(initSelectedPage, false);
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        if (getArguments() != null)
        {
            initSelectedPage = getArguments().getInt("page", 0);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mCon = getActivity().getApplicationContext();
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030049, viewgroup, false);
        titleBarView = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a00de);
        pager = (ViewPager)fragmentBaseContainerView.findViewById(0x7f0a017a);
        pager.setOffscreenPageLimit(3);
        mGroupFragmentAdapter = new GroupFragmentAdapter(getChildFragmentManager(), mFragmentClassInfoList);
        pager.setAdapter(mGroupFragmentAdapter);
        pager.setOnPageChangeListener(this);
        init();
        return fragmentBaseContainerView;
    }

    public void onPageScrollStateChanged(int i)
    {
    }

    public void onPageScrolled(int i, float f, int j)
    {
    }

    public void onPageSelected(int i)
    {
    }

    public void onPause()
    {
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
    }

    public void setCurrentFragment(int i)
    {
        if (pager != null)
        {
            pager.setCurrentItem(i, true);
        }
    }

    public void setCurrentFragment(int i, boolean flag)
    {
        if (pager != null)
        {
            pager.setCurrentItem(i, flag);
        }
    }

    public void setTitleBarView(View view)
    {
        android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, (int)getResources().getDimension(0x7f080025));
        titleBarView.addView(view, layoutparams);
    }

    public void setTitleBarView(View view, int i)
    {
        android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, ToolUtil.dp2px(getActivity(), i));
        titleBarView.addView(view, layoutparams);
    }
}
