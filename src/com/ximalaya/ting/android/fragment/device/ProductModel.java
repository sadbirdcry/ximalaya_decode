// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;


public class ProductModel
{

    public long createdAt;
    public int forDevice;
    public int id;
    public boolean isShow;
    public String logo;
    public int orderNum;
    public String subtitle;
    public String title;
    public long updatedAt;
    public String url;

    public ProductModel()
    {
        url = "0";
        forDevice = 1;
    }

    public ProductModel(int i, String s, String s1, String s2, String s3, int j, boolean flag, 
            long l, long l1, int k)
    {
        url = "0";
        forDevice = 1;
        id = i;
        title = s;
        subtitle = s1;
        logo = s2;
        url = s3;
        orderNum = j;
        isShow = flag;
        createdAt = l;
        updatedAt = l1;
        forDevice = k;
    }

    public long getCreatedAt()
    {
        return createdAt;
    }

    public int getForDevice()
    {
        return forDevice;
    }

    public int getId()
    {
        return id;
    }

    public String getLogo()
    {
        return logo;
    }

    public int getOrderNum()
    {
        return orderNum;
    }

    public String getSubtitle()
    {
        return subtitle;
    }

    public String getTitle()
    {
        return title;
    }

    public long getUpdatedAt()
    {
        return updatedAt;
    }

    public String getUrl()
    {
        if (url == null)
        {
            return "0";
        } else
        {
            return url;
        }
    }

    public boolean isShow()
    {
        return isShow;
    }

    public void setCreatedAt(long l)
    {
        createdAt = l;
    }

    public void setForDevice(int i)
    {
        forDevice = i;
    }

    public void setId(int i)
    {
        id = i;
    }

    public void setLogo(String s)
    {
        logo = s;
    }

    public void setOrderNum(int i)
    {
        orderNum = i;
    }

    public void setShow(boolean flag)
    {
        isShow = flag;
    }

    public void setSubtitle(String s)
    {
        subtitle = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setUpdatedAt(long l)
    {
        updatedAt = l;
    }

    public void setUrl(String s)
    {
        url = s;
    }
}
