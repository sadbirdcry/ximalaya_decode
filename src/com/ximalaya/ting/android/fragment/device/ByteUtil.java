// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import java.io.PrintStream;
import java.nio.charset.Charset;

public class ByteUtil
{

    public ByteUtil()
    {
    }

    public static byte[] getBytes(char c)
    {
        return (new byte[] {
            (byte)c, (byte)(c >> 8)
        });
    }

    public static byte[] getBytes(double d)
    {
        return getBytes(Double.doubleToLongBits(d));
    }

    public static byte[] getBytes(float f)
    {
        return getBytes(Float.floatToIntBits(f));
    }

    public static byte[] getBytes(int i)
    {
        return (new byte[] {
            (byte)(i & 0xff), (byte)((0xff00 & i) >> 8), (byte)((0xff0000 & i) >> 16), (byte)((0xff000000 & i) >> 24)
        });
    }

    public static byte[] getBytes(long l)
    {
        return (new byte[] {
            (byte)(int)(l & 255L), (byte)(int)(l >> 8 & 255L), (byte)(int)(l >> 16 & 255L), (byte)(int)(l >> 24 & 255L), (byte)(int)(l >> 32 & 255L), (byte)(int)(l >> 40 & 255L), (byte)(int)(l >> 48 & 255L), (byte)(int)(l >> 56 & 255L)
        });
    }

    public static byte[] getBytes(String s)
    {
        return getBytes(s, "GBK");
    }

    public static byte[] getBytes(String s, String s1)
    {
        return s.getBytes(Charset.forName(s1));
    }

    public static byte[] getBytes(short word0)
    {
        return (new byte[] {
            (byte)(word0 & 0xff), (byte)((0xff00 & word0) >> 8)
        });
    }

    public static char getChar(byte abyte0[])
    {
        return (char)(abyte0[0] & 0xff | 0xff00 & abyte0[1] << 8);
    }

    public static double getDouble(byte abyte0[])
    {
        long l = getLong(abyte0);
        System.out.println(l);
        return Double.longBitsToDouble(l);
    }

    public static float getFloat(byte abyte0[])
    {
        return Float.intBitsToFloat(getInt(abyte0));
    }

    public static int getInt(byte abyte0[])
    {
        return abyte0[0] & 0xff | 0xff00 & abyte0[1] << 8 | 0xff0000 & abyte0[2] << 16 | 0xff000000 & abyte0[3] << 24;
    }

    public static long getLong(byte abyte0[])
    {
        return 255L & (long)abyte0[0] | 65280L & (long)abyte0[1] << 8 | 0xff0000L & (long)abyte0[2] << 16 | 0xff000000L & (long)abyte0[3] << 24 | 0xff00000000L & (long)abyte0[4] << 32 | 0xff0000000000L & (long)abyte0[5] << 40 | 0xff000000000000L & (long)abyte0[6] << 48 | 0xff00000000000000L & (long)abyte0[7] << 56;
    }

    public static short getShort(byte abyte0[])
    {
        return (short)(abyte0[0] & 0xff | 0xff00 & abyte0[1] << 8);
    }

    public static String getString(byte abyte0[])
    {
        return getString(abyte0, "GBK");
    }

    public static String getString(byte abyte0[], String s)
    {
        return new String(abyte0, Charset.forName(s));
    }
}
