// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.content.Context;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            MyDeviceManager, ProductModel

public class MyDeviceUtil
{

    public MyDeviceUtil()
    {
    }

    public static boolean check(String s, String s1)
    {
        boolean flag1 = false;
        s = s.split("\\.");
        s1 = s1.split("\\.");
        int i = 0;
        do
        {
label0:
            {
                boolean flag = flag1;
                if (i < s.length)
                {
                    flag = flag1;
                    if (i < s1.length)
                    {
                        if (Integer.parseInt(s1[i]) < Integer.parseInt(s[i]))
                        {
                            break label0;
                        }
                        flag = true;
                    }
                }
                return flag;
            }
            i++;
        } while (true);
    }

    public static boolean isFromDeviceDownload(int i)
    {
        return i == 8 || i == 9 || i == 10 || i == 5 || i == 6 || i == 7;
    }

    public static boolean isShowEnterance(Context context, String s)
    {
        String s1;
        boolean flag;
        boolean flag1;
        flag1 = false;
        s1 = ToolUtil.getAppVersion(context);
        context = MyDeviceManager.getInstance(context).getProductModels();
        flag = flag1;
        if (context == null) goto _L2; else goto _L1
_L1:
        int i = 0;
_L7:
        flag = flag1;
        if (i >= context.size()) goto _L2; else goto _L3
_L3:
        ProductModel productmodel;
        String s2;
        productmodel = (ProductModel)context.get(i);
        s2 = productmodel.getUrl();
        if (productmodel.title == null || !productmodel.title.equals(s) || productmodel.getForDevice() != 1 || !check(s2, s1) || !productmodel.isShow()) goto _L5; else goto _L4
_L4:
        flag = true;
_L2:
        return flag;
_L5:
        i++;
        if (true) goto _L7; else goto _L6
_L6:
    }
}
