// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.che;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.che:
//            MyBluetoothManager2

public class CheTingSettingFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    boolean isOnTouch;
    private ImageView mBackImg;
    private TextView mSetting1Info;
    private SwitchButton mSetting1Switch;
    private RelativeLayout mSetting2;
    private TextView mSetting2Info;
    private SwitchButton mSetting2Switch;
    private TextView mSetting3Info;
    private SwitchButton mSetting3Switch;
    private RelativeLayout mSetting4;
    private TextView mSetting4Info;
    private SwitchButton mSetting4Switch;
    private TextView top_tv;

    public CheTingSettingFragment()
    {
        isOnTouch = false;
    }

    private void initView()
    {
        top_tv = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        top_tv.setText("\u8BBE\u7F6E");
        mBackImg.setOnClickListener(this);
        mSetting1Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a02fe);
        mSetting1Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a02ff);
        mSetting2Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a030d);
        mSetting2 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a030a);
        mSetting2Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a030e);
        mSetting4Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a0303);
        mSetting4 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a0300);
        mSetting4Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0304);
        mSetting3Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a0308);
        mSetting3Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0309);
        mSetting2.setOnClickListener(this);
        mSetting2Switch.setVisibility(8);
        mSetting2Info.setVisibility(0);
        mSetting4Info.setVisibility(8);
        mSetting4Switch.setVisibility(0);
        mSetting4Switch.setChecked(MyBluetoothManager2.isAutoPlaying(mCon));
        mSetting4Switch.setOnCheckedChangeListener(new _cls1());
        mSetting1Info.setVisibility(8);
        mSetting1Switch.setVisibility(0);
        if (MyBluetoothManager2.getInstance(mCon).isWarning())
        {
            mSetting1Switch.setChecked(true);
        } else
        {
            mSetting1Switch.setChecked(false);
        }
        mSetting1Switch.setOnCheckedChangeListener(new _cls2());
        mSetting3Info.setVisibility(8);
        mSetting3Switch.setVisibility(0);
        if (MyBluetoothManager2.getInstance(mCon).isContinue())
        {
            mSetting3Switch.setChecked(true);
        } else
        {
            mSetting3Switch.setChecked(false);
        }
        mSetting3Switch.setOnCheckedChangeListener(new _cls3());
    }

    public void init()
    {
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initView();
        init();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362570: 
            MyBluetoothManager2.goIntroduction(getActivity());
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300a5, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onResume()
    {
        super.onResume();
        if (MyBluetoothManager2.getInstance(mCon) == null || !MyBluetoothManager2.getInstance(mCon).judgeConn())
        {
            getActivity().onBackPressed();
        }
    }

    public void showDebugToast(String s)
    {
        CustomToast.showToast(mCon, s, 0);
    }

    public void showToast(String s)
    {
        CustomToast.showToast(mCon, s, 0);
    }

    private class _cls1
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final CheTingSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            if (flag)
            {
                MyBluetoothManager2.getInstance(mCon).turnOnPlayKeySending();
                MyBluetoothManager2.getInstance(mCon).setAutoPlaying(true);
                return;
            } else
            {
                MyBluetoothManager2.getInstance(mCon).turnOffPlayKeySending();
                MyBluetoothManager2.getInstance(mCon).setAutoPlaying(false);
                return;
            }
        }

        _cls1()
        {
            this$0 = CheTingSettingFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final CheTingSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            if (flag)
            {
                MyBluetoothManager2.getInstance(mCon).turnOnVoicePrompt();
                MyBluetoothManager2.getInstance(mCon).setWarning(true);
                return;
            } else
            {
                MyBluetoothManager2.getInstance(mCon).turnOffVoicePrompt();
                MyBluetoothManager2.getInstance(mCon).setWarning(false);
                return;
            }
        }

        _cls2()
        {
            this$0 = CheTingSettingFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final CheTingSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            if (flag)
            {
                MyBluetoothManager2.getInstance(mCon).setContinue(true);
                return;
            } else
            {
                MyBluetoothManager2.getInstance(mCon).setContinue(false);
                return;
            }
        }

        _cls3()
        {
            this$0 = CheTingSettingFragment.this;
            super();
        }
    }

}
