// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.che;

import android.bluetooth.BluetoothDevice;
import com.chipsguide.lib.bluetooth.interfaces.callbacks.OnBluetoothDeviceDiscoveryListener;
import com.chipsguide.lib.bluetooth.managers.BluetoothDeviceManager;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.che:
//            CheTingFragment

class this._cls0
    implements OnBluetoothDeviceDiscoveryListener
{

    final CheTingFragment this$0;

    public void onBluetoothDeviceDiscoveryFinished()
    {
        Logger.d("WIFI", (new StringBuilder()).append("onBluetoothDeviceDiscoveryFinished\uFF1Astate:").append(getNowState()).append(";mIsOnConning=").append(CheTingFragment.access$400(CheTingFragment.this)).toString());
        class _cls1
            implements Runnable
        {

            final CheTingFragment._cls6 this$1;

            public void run()
            {
                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
                if (CheTingFragment.access$400(this$0))
                {
                    return;
                } else
                {
                    class _cls1
                        implements Runnable
                    {

                        final _cls1 this$2;

                        public void run()
                        {
                            showToast("\u641C\u7D22\u5B8C\u6210");
                            break MISSING_BLOCK_LABEL_15;
                            if (getNowState() != MyBluetoothManager2.STATE.CONNECTED && getNowState() == MyBluetoothManager2.STATE.CONNECTING)
                            {
                                setNowState(MyBluetoothManager2.STATE.FAILED);
                            }
                            CheTingFragment.access$200(this$0);
                            return;
                        }

                        _cls1()
                        {
                            this$2 = _cls1.this;
                            super();
                        }
                    }

                    CheTingFragment.access$600(this$0).runOnUiThread(new _cls1());
                    return;
                }
            }

            _cls1()
            {
                this$1 = CheTingFragment._cls6.this;
                super();
            }
        }

        (new Thread(new _cls1())).start();
    }

    public void onBluetoothDeviceDiscoveryFound(BluetoothDevice bluetoothdevice)
    {
        Logger.d("WIFI", (new StringBuilder()).append("Found only one").append(bluetoothdevice.getName()).toString());
        showToast("\u53D1\u73B0\u8BBE\u5907");
        CheTingFragment.access$700(CheTingFragment.this).cancelDiscovery();
        if (CheTingFragment.access$800(CheTingFragment.this, bluetoothdevice) && getNowState() == STATE.CONNECTING)
        {
            CheTingFragment.access$402(CheTingFragment.this, true);
            CheTingFragment.access$900(CheTingFragment.this, bluetoothdevice);
        }
    }

    public void onBluetoothDeviceDiscoveryStarted()
    {
        Logger.d("WIFI", "DiscoveryStarted");
    }

    _cls2.STATE()
    {
        this$0 = CheTingFragment.this;
        super();
    }
}
