// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.che;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.text.TextUtils;
import com.chipsguide.lib.bluetooth.extend.devices.BluetoothDeviceFmSenderManager;
import com.chipsguide.lib.bluetooth.managers.BluetoothDeviceManager;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.modelmanage.DexManager;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.download.DownloadInfoManager;
import com.ximalaya.ting.android.util.CustomToast;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.che:
//            CheTingFragment

public class MyBluetoothManager2
{
    public class BatteryReceiver extends BroadcastReceiver
    {

        final MyBluetoothManager2 this$0;

        public void onReceive(Context context, Intent intent)
        {
            if ("android.intent.action.BATTERY_LOW".equals(intent.getAction()))
            {
                sendMsgDeviceButteryLow();
            }
        }

        public BatteryReceiver()
        {
            this$0 = MyBluetoothManager2.this;
            super();
        }
    }

    public static final class STATE extends Enum
    {

        private static final STATE $VALUES[];
        public static final STATE CONNECTED;
        public static final STATE CONNECTING;
        public static final STATE FAILED;
        public static final STATE START;

        public static STATE valueOf(String s)
        {
            return (STATE)Enum.valueOf(com/ximalaya/ting/android/fragment/device/che/MyBluetoothManager2$STATE, s);
        }

        public static STATE[] values()
        {
            return (STATE[])$VALUES.clone();
        }

        static 
        {
            START = new STATE("START", 0);
            CONNECTING = new STATE("CONNECTING", 1);
            CONNECTED = new STATE("CONNECTED", 2);
            FAILED = new STATE("FAILED", 3);
            $VALUES = (new STATE[] {
                START, CONNECTING, CONNECTED, FAILED
            });
        }

        private STATE(String s, int i)
        {
            super(s, i);
        }
    }

    public static interface onBtStateChangeListener
    {

        public abstract void onBtConnect();

        public abstract void onBtDisconnect();
    }


    public static boolean IS_SUICHETING_OPEN = false;
    private static final String TAG = "suicheting";
    private static MyBluetoothManager2 mMyBluetoothManager2;
    private final float INIT_VALUE = 87.5F;
    protected final String P_INIT_VALUE_KEY = "P_INIT_VALUE_KEY";
    private String hardwareBuyUrl;
    private String hardwareShoppingUrl;
    private boolean isChangeAlbum;
    private volatile boolean isSetAlbumListener;
    private boolean isSuichetingOpen;
    private boolean isSuichetingPlaying;
    private BatteryReceiver mBatteryReceiver;
    private BluetoothDevice mBluetoothDevice;
    private BluetoothDeviceFmSenderManager mBluetoothDeviceFmSenderManager;
    private BluetoothDeviceManager mBluetoothDeviceManager;
    private Context mContext;
    private boolean mIsBtConnStateInit;
    private boolean mIsSuichetingInit;
    private STATE mNowState;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private SharedPreferencesUtil mSharedPreferencesUtil;

    private MyBluetoothManager2(Context context)
    {
        mNowState = STATE.START;
        isSetAlbumListener = false;
        isSuichetingOpen = false;
        isSuichetingPlaying = false;
        isChangeAlbum = true;
        mIsBtConnStateInit = false;
        mIsSuichetingInit = false;
        Logger.e("clear", "MyBluetoothManager2 init");
        Context context1 = context;
        if (context == null)
        {
            context1 = MyApplication.b();
        }
        mContext = context1.getApplicationContext();
        mSharedPreferencesUtil = SharedPreferencesUtil.getInstance(context1);
    }

    private BluetoothDevice getConnDevice()
    {
        BluetoothDevice bluetoothdevice;
        BluetoothDevice bluetoothdevice1;
        if (mBluetoothDeviceManager.getBluetoothDeviceConnectedA2dp() != null)
        {
            bluetoothdevice = mBluetoothDeviceManager.getBluetoothDeviceConnectedA2dp();
        } else
        {
            bluetoothdevice = null;
        }
        if (mBluetoothDeviceManager.getBluetoothDeviceConnectedSpp() != null)
        {
            bluetoothdevice1 = mBluetoothDeviceManager.getBluetoothDeviceConnectedSpp();
        } else
        {
            bluetoothdevice1 = null;
        }
        if (bluetoothdevice1 != null && bluetoothdevice != null)
        {
            return bluetoothdevice;
        } else
        {
            return null;
        }
    }

    public static MyBluetoothManager2 getInstance(Context context)
    {
        if (mMyBluetoothManager2 != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/fragment/device/che/MyBluetoothManager2;
        JVM INSTR monitorenter ;
        if (mMyBluetoothManager2 == null)
        {
            mMyBluetoothManager2 = new MyBluetoothManager2(context);
        }
        com/ximalaya/ting/android/fragment/device/che/MyBluetoothManager2;
        JVM INSTR monitorexit ;
_L2:
        if (context != null)
        {
            mMyBluetoothManager2.mContext = context;
        }
        return mMyBluetoothManager2;
        context;
        com/ximalaya/ting/android/fragment/device/che/MyBluetoothManager2;
        JVM INSTR monitorexit ;
        throw context;
    }

    public static void goIntroduction(Activity activity)
    {
        String s = MyDeviceManager.getInstance(activity.getApplicationContext()).getSuichetingIntro();
        if (!TextUtils.isEmpty(s))
        {
            Intent intent = new Intent(activity, com/ximalaya/ting/android/activity/web/WebActivityNew);
            intent.putExtra("ExtraUrl", s);
            activity.startActivity(intent);
            return;
        } else
        {
            CustomToast.showToast(activity, "\u656C\u8BF7\u671F\u5F85", 0);
            return;
        }
    }

    public static boolean isAutoPlaying(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean("autoPlaying", true);
    }

    private boolean isInitFmSenderManager()
    {
        if (isSuichetingCanUse() && mBluetoothDeviceManager != null)
        {
            mBluetoothDeviceFmSenderManager = mBluetoothDeviceManager.getBluetoothDeviceFmSenderManager();
            if (mBluetoothDeviceFmSenderManager != null)
            {
                return true;
            }
        }
        return false;
    }

    private boolean isSuichetingCanUse()
    {
        if (isSuichetingOpen)
        {
            return true;
        }
        if (IS_SUICHETING_OPEN && DexManager.getInstance(mContext).isSuichetingOpen())
        {
            isSuichetingOpen = true;
        }
        return isSuichetingOpen;
    }

    private void onConnectDevice()
    {
        if (!isSuichetingCanUse())
        {
            return;
        } else
        {
            setBtConnStateListener();
            setBatteryListener();
            setChangeAlbumListener(MyApplication.b());
            return;
        }
    }

    private void setAlbumListener(final Context context)
    {
        Logger.d("suicheting", (new StringBuilder()).append("isSetAlbumListener:").append(isSetAlbumListener).toString());
        while (!isSuichetingCanUse() || isSetAlbumListener || mBluetoothDevice == null || mBluetoothDeviceFmSenderManager == null) 
        {
            return;
        }
        DownloadInfoManager.getInstance(context.getApplicationContext()).doGetDownloadList();
        MyApplication.a().runOnUiThread(new _cls3());
    }

    private void setBatteryListener()
    {
        if (MainTabActivity2.isMainTabActivityAvaliable() && mBatteryReceiver == null)
        {
            mBatteryReceiver = new BatteryReceiver();
            if (MainTabActivity2.mainTabActivity != null)
            {
                MainTabActivity2.mainTabActivity.registerReceiver(mBatteryReceiver, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
                return;
            }
        }
    }

    private void setBtConnStateListener()
    {
        Logger.d("WIFI", "setBtConnStateListener");
        while (!isSuichetingCanUse() || mIsBtConnStateInit || mBluetoothDeviceManager == null) 
        {
            return;
        }
        mIsBtConnStateInit = true;
        (new Thread(new _cls4())).start();
    }

    private void showWarning()
    {
        if (MyApplication.a() == null)
        {
            return;
        } else
        {
            MyApplication.a().runOnUiThread(new _cls8());
            return;
        }
    }

    private void startSppConnLib(BluetoothDevice bluetoothdevice)
    {
        mBluetoothDeviceManager.connect(bluetoothdevice);
    }

    private void unRegisterPlayListener()
    {
        while (!isSuichetingCanUse() || mOnPlayerStatusUpdateListener == null || LocalMediaService.getInstance() == null) 
        {
            return;
        }
        LocalMediaService.getInstance().removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        mOnPlayerStatusUpdateListener = null;
    }

    public void checkDevice()
    {
        if (!isSuichetingCanUse())
        {
            return;
        } else
        {
            tryConnect(true, false, null, true);
            return;
        }
    }

    public void closeSppService()
    {
        if (mBluetoothDeviceManager == null);
    }

    public BluetoothDevice getBluetoothDevice()
    {
        return mBluetoothDevice;
    }

    public BluetoothDeviceFmSenderManager getBluetoothDeviceFmSenderManager()
    {
        if (mBluetoothDeviceFmSenderManager == null && mBluetoothDeviceManager != null)
        {
            mBluetoothDeviceFmSenderManager = mBluetoothDeviceManager.getBluetoothDeviceFmSenderManager();
        }
        return mBluetoothDeviceFmSenderManager;
    }

    public BluetoothDeviceManager getBluetoothDeviceManager(Context context)
    {
        if (mBluetoothDeviceManager == null)
        {
            try
            {
                mBluetoothDeviceManager = BluetoothDeviceManager.getInstance(context.getApplicationContext()).setBluetoothDevice(3).setBluetoothDeviceMacAddressFilterPrefix("C9:32:").build();
                setBtConnStateListener();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context.printStackTrace();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                context.printStackTrace();
            }
        }
        return mBluetoothDeviceManager;
    }

    public String getHardwareBuyUrl()
    {
        return hardwareBuyUrl;
    }

    public String getHardwareShoppingUrl()
    {
        return hardwareShoppingUrl;
    }

    public STATE getNowState()
    {
        this;
        JVM INSTR monitorenter ;
        STATE state = mNowState;
        this;
        JVM INSTR monitorexit ;
        return state;
        Exception exception;
        exception;
        throw exception;
    }

    public void getPlayKeySendingStatus()
    {
        if (mMyBluetoothManager2 != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
            if (mBluetoothDeviceFmSenderManager != null)
            {
                mBluetoothDeviceFmSenderManager.getPlayKeySendingStatus();
            }
        }
    }

    public void getVoicePromptStatus()
    {
        if (mMyBluetoothManager2 != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
            if (mBluetoothDeviceFmSenderManager != null)
            {
                mBluetoothDeviceFmSenderManager.getVoicePromptStatus();
            }
        }
    }

    public void initSuicheting()
    {
        while (!isSuichetingCanUse() || !isSuichetingOpen || mIsSuichetingInit) 
        {
            return;
        }
        mIsSuichetingInit = true;
        mBluetoothDeviceManager = getBluetoothDeviceManager(mContext);
        mBluetoothDeviceFmSenderManager = getBluetoothDeviceFmSenderManager();
        setBtConnStateListener();
    }

    public boolean isContinue()
    {
        boolean flag = SharedPreferencesUtil.getInstance(mContext).getBoolean("isContinue", true);
        if (flag)
        {
            Logger.d("suicheting", "\u65AD\u5F00\u84DD\u7259\u7EE7\u7EED\u64AD\u653E");
            return flag;
        } else
        {
            Logger.d("suicheting", "\u65AD\u5F00\u84DD\u7259\u505C\u6B62\u64AD\u653E");
            return flag;
        }
    }

    public boolean isContinuePlaying()
    {
        return isSuichetingPlaying && isContinue();
    }

    public boolean isSuichetingPlaying()
    {
        if (android.os.Build.VERSION.SDK_INT < 11)
        {
            return isSuichetingPlaying;
        }
        BluetoothAdapter bluetoothadapter;
        boolean flag;
        try
        {
            bluetoothadapter = BluetoothAdapter.getDefaultAdapter();
        }
        catch (Exception exception)
        {
            return isSuichetingPlaying;
        }
        if (bluetoothadapter == null)
        {
            break MISSING_BLOCK_LABEL_39;
        }
        return bluetoothadapter.getProfileConnectionState(2) == 2 && isSuichetingPlaying;
        flag = isSuichetingPlaying;
        return flag;
    }

    public boolean isWarning()
    {
        boolean flag = SharedPreferencesUtil.getInstance(mContext).getBoolean("isWarning", true);
        Logger.d("suicheting", (new StringBuilder()).append("\u5F53\u524D\u8BB0\u5F55\u7684\u63D0\u793A\u97F3\u72B6\u6001").append(flag).toString());
        return flag;
    }

    public boolean judgeConn()
    {
        BluetoothDevice bluetoothdevice = getConnDevice();
        Logger.d("WIFI", (new StringBuilder()).append("judgeConn:").append(bluetoothdevice).toString());
        return bluetoothdevice != null && MyDeviceManager.isSuichetingDevice(bluetoothdevice);
    }

    public boolean judgeConn(BluetoothDeviceManager bluetoothdevicemanager, Context context, BluetoothDevice bluetoothdevice)
    {
        if (!isSuichetingCanUse())
        {
            return false;
        }
        Logger.d("suicheting", "BluetoothReciever judgeConn");
        if (bluetoothdevicemanager == null)
        {
            return false;
        }
        Context context1;
        if (bluetoothdevicemanager.getBluetoothDeviceConnectedA2dp() != null)
        {
            Logger.d("device", "A2dp");
            context = bluetoothdevicemanager.getBluetoothDeviceConnectedA2dp();
        } else
        {
            context = null;
        }
        if (bluetoothdevicemanager.getBluetoothDeviceConnectedSpp() != null)
        {
            Logger.d("device", "Spp");
            bluetoothdevicemanager = bluetoothdevicemanager.getBluetoothDeviceConnectedSpp();
        } else
        {
            bluetoothdevicemanager = null;
        }
        if (context != null)
        {
            context1 = context;
            if (bluetoothdevicemanager == null)
            {
                startSppConnLib(bluetoothdevice);
                context1 = context;
            }
        } else
        {
            context1 = null;
        }
        Logger.d("suicheting", (new StringBuilder()).append("judgeConn mTempDevice:").append(context1).toString());
        if (context1 != null)
        {
            sendMsgStarted();
            setBluetoothDevice(context1);
            return true;
        } else
        {
            Logger.d("WIFI", "BluetoothReciever");
            return false;
        }
    }

    public void registerPlayListener()
    {
        if (isSuichetingCanUse())
        {
            if (mOnPlayerStatusUpdateListener != null)
            {
                Logger.d("suicheting", "mOnPlayerStatusUpdateListener return");
                return;
            }
            if (LocalMediaService.getInstance() == null)
            {
                Logger.d("suicheting", "LocalMediaService return");
                return;
            }
            mOnPlayerStatusUpdateListener = new _cls1();
            if (LocalMediaService.getInstance() != null)
            {
                Logger.d("suicheting", "setOnPlayerStatusUpdateListener");
                LocalMediaService.getInstance().setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
                return;
            }
        }
    }

    public void release()
    {
        mBluetoothDeviceManager.release();
    }

    public void releaseBtManager()
    {
        if (isSuichetingCanUse())
        {
            unRegisterPlayListener();
            if (mBatteryReceiver != null && MainTabActivity2.mainTabActivity != null)
            {
                MainTabActivity2.mainTabActivity.unregisterReceiver(mBatteryReceiver);
                mBatteryReceiver = null;
            }
            if (mBluetoothDeviceFmSenderManager != null)
            {
                mBluetoothDeviceFmSenderManager.setOnBluetoothDeviceFmSenderAlbumStatusChangedListener(null);
            }
            mNowState = STATE.START;
            mBluetoothDevice = null;
            isSetAlbumListener = false;
            mIsBtConnStateInit = false;
            mIsSuichetingInit = false;
            if (mBluetoothDeviceManager != null)
            {
                mBluetoothDeviceManager.setOnBluetoothDeviceConnectionStateChangedListener(null);
                if (MainTabActivity2.isMainTabActivityAvaliable())
                {
                    MainTabActivity2.mainTabActivity.runOnUiThread(new _cls6());
                    return;
                }
            }
        }
    }

    public void scanConnDeviceSync(final Context context)
    {
        while (!isSuichetingCanUse() || mBluetoothDevice != null) 
        {
            return;
        }
        if (mBluetoothDeviceManager == null)
        {
            getBluetoothDeviceManager(context);
        }
        (new Thread(new _cls5())).start();
    }

    public void sendMsgChangeAlbumFail()
    {
        while (mBluetoothDevice == null || getBluetoothDeviceFmSenderManager() == null) 
        {
            return;
        }
        Logger.d("suicheting", "sendMsgChangeAlbumFail");
        mBluetoothDeviceFmSenderManager.sendMessage(4);
    }

    public void sendMsgConnNetFail()
    {
        while (mBluetoothDevice == null || getBluetoothDeviceFmSenderManager() == null) 
        {
            return;
        }
        Logger.d("suicheting", "sendMsgConnNetFail");
        mBluetoothDeviceFmSenderManager.sendMessage(1);
    }

    public void sendMsgDeviceButteryLow()
    {
        while (mBluetoothDevice == null || getBluetoothDeviceFmSenderManager() == null) 
        {
            return;
        }
        Logger.d("suicheting", "sendMsgDeviceButteryLow");
        mBluetoothDeviceFmSenderManager.sendMessage(2);
    }

    public void sendMsgLastSong()
    {
        while (mBluetoothDevice == null || getBluetoothDeviceFmSenderManager() == null) 
        {
            return;
        }
        Logger.d("suicheting", "sendMsgLastSong");
        mBluetoothDeviceFmSenderManager.sendMessage(3);
    }

    public void sendMsgStarted()
    {
        while (mBluetoothDevice == null || getBluetoothDeviceFmSenderManager() == null) 
        {
            return;
        }
        Logger.d("suicheting", "sendMsgStarted");
        mBluetoothDeviceFmSenderManager.sendMessage(5);
    }

    public void setAutoPlaying(boolean flag)
    {
        SharedPreferencesUtil.getInstance(mContext).saveBoolean("autoPlaying", flag);
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothdevice)
    {
        mBluetoothDevice = bluetoothdevice;
        if (bluetoothdevice != null)
        {
            onConnectDevice();
        }
    }

    public void setBluetoothDeviceFmSenderManager(BluetoothDeviceFmSenderManager bluetoothdevicefmsendermanager)
    {
        mBluetoothDeviceFmSenderManager = bluetoothdevicefmsendermanager;
    }

    public void setBluetoothDeviceManager(BluetoothDeviceManager bluetoothdevicemanager)
    {
        mBluetoothDeviceManager = bluetoothdevicemanager;
        if (bluetoothdevicemanager != null)
        {
            setBtConnStateListener();
        }
    }

    public void setChangeAlbumListener(final Context context)
    {
        while (context == null || !isSuichetingCanUse() || isChangeAlbum) 
        {
            return;
        }
        getBluetoothDeviceManager(context);
        if (mBluetoothDeviceFmSenderManager == null)
        {
            Logger.d("suicheting", "setChangeAlbumListener mBluetoothDeviceFmSenderManager == null");
            (new Thread(new _cls2())).start();
            return;
        } else
        {
            Logger.d("suicheting", "setChangeAlbumListener mBluetoothDeviceFmSenderManager == null");
            setAlbumListener(context);
            return;
        }
    }

    public void setContinue(boolean flag)
    {
        SharedPreferencesUtil.getInstance(mContext).saveBoolean("isContinue", flag);
    }

    public void setHardwareBuyUrl(String s)
    {
        hardwareBuyUrl = s;
    }

    public void setHardwareShoppingUrl(String s)
    {
        hardwareShoppingUrl = s;
    }

    public void setInitFrequency()
    {
label0:
        {
            if (mBluetoothDeviceFmSenderManager != null)
            {
                if (!mSharedPreferencesUtil.getBoolean("P_INIT_VALUE_KEY"))
                {
                    break label0;
                }
                mBluetoothDeviceFmSenderManager.getStatus();
            }
            return;
        }
        if (mBluetoothDeviceFmSenderManager != null)
        {
            mBluetoothDeviceFmSenderManager.setFrequency(0x155cc);
            Logger.d("suicheting", "setFrequency:87500.0");
        }
        mSharedPreferencesUtil.saveBoolean("P_INIT_VALUE_KEY", true);
    }

    public void setNowState(STATE state)
    {
        this;
        JVM INSTR monitorenter ;
        mNowState = state;
        this;
        JVM INSTR monitorexit ;
        return;
        state;
        throw state;
    }

    public void setOnBluetoothDeviceFmSenderVoicePromptStatusListener(com.chipsguide.lib.bluetooth.extend.devices.BluetoothDeviceFmSenderManager.OnBluetoothDeviceFmSenderVoicePromptStatusListener onbluetoothdevicefmsendervoicepromptstatuslistener)
    {
        if (mMyBluetoothManager2 != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
            if (mBluetoothDeviceFmSenderManager != null)
            {
                mBluetoothDeviceFmSenderManager.setOnBluetoothDeviceFmSenderVoicePromptStatusListener(onbluetoothdevicefmsendervoicepromptstatuslistener);
            }
        }
    }

    public void setSuichetingPlaying(boolean flag)
    {
        isSuichetingPlaying = flag;
    }

    public void setWarning(boolean flag)
    {
        SharedPreferencesUtil.getInstance(mContext).saveBoolean("isWarning", flag);
    }

    public void startSppService()
    {
        if (mBluetoothDeviceManager == null);
    }

    public void tryConnect(boolean flag, boolean flag1, BaseFragment basefragment)
    {
        if (!isSuichetingCanUse())
        {
            return;
        }
        try
        {
            tryConnect(flag, flag1, basefragment, false);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (BaseFragment basefragment)
        {
            basefragment.printStackTrace();
        }
    }

    public void tryConnect(final boolean isSlient, final boolean removeTop, final BaseFragment fra, final boolean isCheckDevice)
    {
        if (isSuichetingCanUse()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int i;
        i = android.os.Build.VERSION.SDK_INT;
        Logger.d("suicheting", "CheTingIntroFragment tryConnect");
        if (BluetoothAdapter.getDefaultAdapter() != null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!isSlient)
        {
            showWarning();
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        if (BluetoothAdapter.getDefaultAdapter().isEnabled())
        {
            break; /* Loop/switch isn't completed */
        }
        if (!isSlient)
        {
            showWarning();
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
        BluetoothDevice bluetoothdevice = MyDeviceManager.getInstance(mContext).getConnectedDevice(MyApplication.a());
        Logger.d("suicheting", (new StringBuilder()).append("tryConnect mBluetoothDevice:").append(mBluetoothDevice).toString());
        if (bluetoothdevice != null)
        {
            if (!MyDeviceManager.isSuichetingDevice(bluetoothdevice))
            {
                continue; /* Loop/switch isn't completed */
            }
            mBluetoothDevice = bluetoothdevice;
            if (isCheckDevice)
            {
                isSuichetingPlaying = true;
                registerPlayListener();
                return;
            }
            if (fra != null)
            {
                if (removeTop)
                {
                    fra.removeTopFramentFromManageFragment();
                }
                fra.startFragment(com/ximalaya/ting/android/fragment/device/che/CheTingFragment, null);
                return;
            }
            continue; /* Loop/switch isn't completed */
        }
        break MISSING_BLOCK_LABEL_161;
        if (isSlient) goto _L1; else goto _L5
_L5:
        showWarning();
        return;
        if (i >= 11)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!isSlient)
        {
            showWarning();
            return;
        }
        if (true) goto _L1; else goto _L6
_L6:
        if (BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(2) == 2)
        {
            BluetoothAdapter.getDefaultAdapter().getProfileProxy(MyApplication.b(), new _cls7(), 2);
            return;
        }
        if (!isSlient)
        {
            showWarning();
            return;
        }
        if (true) goto _L1; else goto _L7
_L7:
    }

    public void turnOffPlayKeySending()
    {
        if (mMyBluetoothManager2 != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
            if (mBluetoothDeviceFmSenderManager != null)
            {
                Logger.d("suicheting", "\u5173\u95ED\u81EA\u52A8\u64AD\u653E");
                mBluetoothDeviceFmSenderManager.turnOffPlayKeySending();
            }
        }
    }

    public void turnOffVoicePrompt()
    {
        if (mMyBluetoothManager2 != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
            if (mBluetoothDeviceFmSenderManager != null)
            {
                Logger.d("suicheting", "\u5173\u95ED\u63D0\u793A\u97F3");
                mBluetoothDeviceFmSenderManager.turnOffVoicePrompt();
            }
        }
    }

    public void turnOnPlayKeySending()
    {
        if (mMyBluetoothManager2 != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
            if (mBluetoothDeviceFmSenderManager != null)
            {
                Logger.d("suicheting", "\u6253\u5F00\u81EA\u52A8\u64AD\u653E");
                mBluetoothDeviceFmSenderManager.turnOnPlayKeySending();
            }
        }
    }

    public void turnOnVoicePrompt()
    {
        if (mMyBluetoothManager2 != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
            if (mBluetoothDeviceFmSenderManager != null)
            {
                Logger.d("suicheting", "\u6253\u5F00\u63D0\u793A\u97F3");
                mBluetoothDeviceFmSenderManager.turnOnVoicePrompt();
            }
        }
    }

    static 
    {
        IS_SUICHETING_OPEN = true;
    }






/*
    static BluetoothDeviceFmSenderManager access$202(MyBluetoothManager2 mybluetoothmanager2, BluetoothDeviceFmSenderManager bluetoothdevicefmsendermanager)
    {
        mybluetoothmanager2.mBluetoothDeviceFmSenderManager = bluetoothdevicefmsendermanager;
        return bluetoothdevicefmsendermanager;
    }

*/




/*
    static boolean access$502(MyBluetoothManager2 mybluetoothmanager2, boolean flag)
    {
        mybluetoothmanager2.isSetAlbumListener = flag;
        return flag;
    }

*/


/*
    static boolean access$602(MyBluetoothManager2 mybluetoothmanager2, boolean flag)
    {
        mybluetoothmanager2.isSuichetingPlaying = flag;
        return flag;
    }

*/


/*
    static BluetoothDevice access$702(MyBluetoothManager2 mybluetoothmanager2, BluetoothDevice bluetoothdevice)
    {
        mybluetoothmanager2.mBluetoothDevice = bluetoothdevice;
        return bluetoothdevice;
    }

*/



/*
    static BluetoothDeviceManager access$802(MyBluetoothManager2 mybluetoothmanager2, BluetoothDeviceManager bluetoothdevicemanager)
    {
        mybluetoothmanager2.mBluetoothDeviceManager = bluetoothdevicemanager;
        return bluetoothdevicemanager;
    }

*/


/*
    static MyBluetoothManager2 access$902(MyBluetoothManager2 mybluetoothmanager2)
    {
        mMyBluetoothManager2 = mybluetoothmanager2;
        return mybluetoothmanager2;
    }

*/

    private class _cls3
        implements Runnable
    {

        final MyBluetoothManager2 this$0;
        final Context val$context;

        public void run()
        {
            class _cls1
                implements com.chipsguide.lib.bluetooth.extend.devices.BluetoothDeviceFmSenderManager.OnBluetoothDeviceFmSenderAlbumStatusChangedListener
            {

                final _cls3 this$1;

                public void onBluetoothDeviceFmSenderAlbumStatusChanged(int i)
                {
                    boolean flag = false;
                    i;
                    JVM INSTR tableswitch 1 2: default 24
                //                               1 39
                //                               2 60;
                       goto _L1 _L2 _L3
_L1:
                    if (!flag)
                    {
                        sendMsgChangeAlbumFail();
                    }
                    return;
_L2:
                    flag = DownloadInfoManager.getInstance(context.getApplicationContext()).toPlayNextAlbum(false);
                    continue; /* Loop/switch isn't completed */
_L3:
                    flag = DownloadInfoManager.getInstance(context.getApplicationContext()).toPlayNextAlbum(true);
                    if (true) goto _L1; else goto _L4
_L4:
                }

                _cls1()
                {
                    this$1 = _cls3.this;
                    super();
                }
            }

            mBluetoothDeviceFmSenderManager.setOnBluetoothDeviceFmSenderAlbumStatusChangedListener(new _cls1());
            isSetAlbumListener = true;
            Logger.d("suicheting", "isSetAlbumListener set!:true");
        }

        _cls3()
        {
            this$0 = MyBluetoothManager2.this;
            context = context1;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final MyBluetoothManager2 this$0;

        public void run()
        {
            class _cls1
                implements OnBluetoothDeviceConnectionStateChangedListener
            {

                final _cls4 this$1;

                public void onBluetoothDeviceConnectionStateChanged(BluetoothDevice bluetoothdevice, int i)
                {
                    i;
                    JVM INSTR tableswitch 0 1: default 24
                //                               0 25
                //                               1 24;
                       goto _L1 _L2 _L1
_L1:
                    return;
_L2:
                    if (MyApplication.a() != null)
                    {
                        class _cls1 extends TimerTask
                        {

                            final _cls1 this$2;

                            public void run()
                            {
                                isSuichetingPlaying = false;
                            }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                        }

                        (new Timer()).schedule(new _cls1(), 5000L);
                        class _cls2
                            implements Runnable
                        {

                            final _cls1 this$2;

                            public void run()
                            {
                                mBluetoothDevice = null;
                                LocalMediaService localmediaservice;
                                if (!isContinue())
                                {
                                    if ((localmediaservice = LocalMediaService.getInstance()) != null)
                                    {
                                        localmediaservice.pause();
                                        return;
                                    }
                                }
                            }

                            _cls2()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                        }

                        MyApplication.a().runOnUiThread(new _cls2());
                        return;
                    }
                    if (true) goto _L1; else goto _L3
_L3:
                }

                _cls1()
                {
                    this$1 = _cls4.this;
                    super();
                }
            }

            mBluetoothDeviceManager.setOnBluetoothDeviceConnectionStateChangedListener(new _cls1());
        }

        _cls4()
        {
            this$0 = MyBluetoothManager2.this;
            super();
        }
    }


    private class _cls8
        implements Runnable
    {

        final MyBluetoothManager2 this$0;

        public void run()
        {
            (new DialogBuilder(MyApplication.a())).setMessage("\u5F53\u524D\u6CA1\u6709\u8FDE\u63A5\u968F\u8F66\u542C\uFF0C\u8BF7\u6309\u7167\u8BF4\u660E\u8FDE\u63A5\u968F\u8F66\u542C\u540E\u518D\u8BD5").showWarning();
        }

        _cls8()
        {
            this$0 = MyBluetoothManager2.this;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
    {

        final MyBluetoothManager2 this$0;

        public void onBufferUpdated(int i)
        {
        }

        public void onLogoPlayFinished()
        {
        }

        public void onPlayCompleted()
        {
        }

        public void onPlayPaused()
        {
        }

        public void onPlayProgressUpdate(int i, int j)
        {
        }

        public void onPlayStarted()
        {
            Logger.d("suicheting", "onPlayStarted IN");
            if (MyBluetoothManager2.getInstance(mContext).isSuichetingPlaying() && MyDeviceManager.getInstance(mContext).isSuiCheTingFirstUsed())
            {
                MyDeviceManager.getInstance(mContext).setIsSuiCheTingFirstUsed(false);
                MyDeviceManager.getInstance(mContext).shareSuiCheTing(4);
            }
            unRegisterPlayListener();
        }

        public void onPlayerBuffering(boolean flag)
        {
        }

        public void onSoundPrepared(int i)
        {
        }

        public void onStartPlayLogo()
        {
        }

        _cls1()
        {
            this$0 = MyBluetoothManager2.this;
            super();
        }
    }


    private class _cls6
        implements Runnable
    {

        final MyBluetoothManager2 this$0;

        public void run()
        {
            try
            {
                mBluetoothDeviceManager.release();
                mBluetoothDeviceManager = null;
                MyBluetoothManager2.mMyBluetoothManager2 = null;
                mBluetoothDeviceFmSenderManager = null;
                return;
            }
            catch (Exception exception)
            {
                return;
            }
        }

        _cls6()
        {
            this$0 = MyBluetoothManager2.this;
            super();
        }
    }


    private class _cls5
        implements Runnable
    {

        final MyBluetoothManager2 this$0;
        final Context val$context;

        public void run()
        {
            Logger.d("suicheting", "BluetoothReciever ACTION_ACL_CONNECTED Runnable");
            int i = 0;
            do
            {
label0:
                {
                    if (i < 40)
                    {
                        try
                        {
                            Thread.sleep(100L);
                        }
                        catch (InterruptedException interruptedexception)
                        {
                            interruptedexception.printStackTrace();
                        }
                        if (!judgeConn(mBluetoothDeviceManager, context, null))
                        {
                            break label0;
                        }
                    }
                    return;
                }
                i++;
            } while (true);
        }

        _cls5()
        {
            this$0 = MyBluetoothManager2.this;
            context = context1;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final MyBluetoothManager2 this$0;
        final Context val$context;

        public void run()
        {
            try
            {
                Thread.sleep(1000L);
            }
            catch (InterruptedException interruptedexception)
            {
                interruptedexception.printStackTrace();
            }
            do
            {
label0:
                {
                    if (mBluetoothDeviceFmSenderManager == null)
                    {
                        if (!isInitFmSenderManager())
                        {
                            break label0;
                        }
                        setAlbumListener(context);
                    }
                    return;
                }
                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException interruptedexception1)
                {
                    interruptedexception1.printStackTrace();
                }
            } while (true);
        }

        _cls2()
        {
            this$0 = MyBluetoothManager2.this;
            context = context1;
            super();
        }
    }


    private class _cls7
        implements android.bluetooth.BluetoothProfile.ServiceListener
    {

        final MyBluetoothManager2 this$0;
        final BaseFragment val$fra;
        final boolean val$isCheckDevice;
        final boolean val$isSlient;
        final boolean val$removeTop;

        public void onServiceConnected(int i, BluetoothProfile bluetoothprofile)
        {
            List list;
            list = bluetoothprofile.getConnectedDevices();
            BluetoothAdapter.getDefaultAdapter().closeProfileProxy(i, bluetoothprofile);
            if (list == null || list.size() == 0) goto _L2; else goto _L1
_L1:
            bluetoothprofile = (BluetoothDevice)list.get(0);
            if (!MyDeviceManager.isSuichetingDevice(bluetoothprofile)) goto _L4; else goto _L3
_L3:
            mBluetoothDevice = bluetoothprofile;
            if (!isCheckDevice) goto _L6; else goto _L5
_L5:
            setBluetoothDevice(bluetoothprofile);
            isSuichetingPlaying = true;
            registerPlayListener();
_L8:
            return;
_L6:
            if (fra != null)
            {
                if (removeTop)
                {
                    fra.removeTopFramentFromManageFragment();
                }
                fra.startFragment(com/ximalaya/ting/android/fragment/device/che/CheTingFragment, null);
                return;
            }
            continue; /* Loop/switch isn't completed */
_L4:
            if (!isSlient)
            {
                showWarning();
                return;
            }
            continue; /* Loop/switch isn't completed */
_L2:
            if (!isSlient)
            {
                showWarning();
                return;
            }
            if (true) goto _L8; else goto _L7
_L7:
        }

        public void onServiceDisconnected(int i)
        {
        }

        _cls7()
        {
            this$0 = MyBluetoothManager2.this;
            isCheckDevice = flag;
            fra = basefragment;
            removeTop = flag1;
            isSlient = flag2;
            super();
        }
    }

}
