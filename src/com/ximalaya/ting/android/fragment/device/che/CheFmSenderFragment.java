// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.che;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.chipsguide.lib.bluetooth.extend.devices.BluetoothDeviceFmSenderManager;
import com.chipsguide.lib.bluetooth.managers.BluetoothDeviceManager;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.view.dial.CircleProgressBar;
import java.math.BigDecimal;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.che:
//            MyBluetoothManager2, CheTingSettingFragment

public class CheFmSenderFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    private static final String LOADING_STRING = "\u8F7D\u5165\u4E2D..";
    protected static final String TAG = "suicheting";
    Runnable frequenceSet;
    boolean isOnTouch;
    private ImageView mBackImg;
    private BluetoothDeviceFmSenderManager mBluetoothDeviceFmSenderManager;
    private BluetoothDeviceManager mBluetoothDeviceManager;
    private TextView mBtnRight;
    private CircleProgressBar mCircleProgressBar;
    private RelativeLayout mContentView;
    private TextView mHzlabel;
    private boolean mInitStartValue;
    private boolean mIsCanSet;
    private MyBluetoothManager2 mMyBluetoothManager2;
    private float mRateValue;
    private ScrollView mScrollView;
    private ImageView mTurnDownIv;
    private ImageView mTurnUpIv;
    private TextView mValueTv;
    private TextView top_tv;

    public CheFmSenderFragment()
    {
        mInitStartValue = true;
        isOnTouch = false;
        frequenceSet = new _cls6();
    }

    public static float add(float f, float f1)
    {
        return (new BigDecimal(Float.toString(f))).add(new BigDecimal(Float.toString(f1))).floatValue();
    }

    private void freshValueShow()
    {
        mValueTv.setText((new StringBuilder()).append(mRateValue).append("").toString());
        mHzlabel.setText("MHz");
    }

    private void initListener()
    {
        if (mMyBluetoothManager2.getBluetoothDeviceFmSenderManager() != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
        } else
        {
            mBluetoothDeviceFmSenderManager = BluetoothDeviceFmSenderManager.getInstance();
            mMyBluetoothManager2.setBluetoothDeviceFmSenderManager(mBluetoothDeviceFmSenderManager);
        }
        if (mBluetoothDeviceFmSenderManager != null)
        {
            mBluetoothDeviceFmSenderManager.setOnBluetoothDeviceFmSenderStatusChangedListener(new _cls7());
            mBluetoothDeviceFmSenderManager.setOnBluetoothDeviceFmSenderVoicePromptStatusListener(new _cls8());
            mBluetoothDeviceFmSenderManager.setOnBluetoothDeviceFmSenderPlayKeySendingStatusListener(new _cls9());
            return;
        } else
        {
            Logger.d("suicheting", "mBluetoothDeviceFmSenderManager\u4E3A\u7A7A\uFF0C\u65E0\u6CD5\u8BBE\u5B9AListenter");
            return;
        }
    }

    private void initView()
    {
        top_tv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        top_tv.setText("\u968F\u8F66\u542C");
        mBackImg.setOnClickListener(this);
        mBtnRight = (TextView)mContentView.findViewById(0x7f0a071c);
        mBtnRight.setOnClickListener(this);
        mBtnRight.setText("\u8BBE\u7F6E");
        mBtnRight.setVisibility(0);
        hidePlayButton();
        mScrollView = (ScrollView)findViewById(0x7f0a0098);
        mCircleProgressBar = (CircleProgressBar)findViewById(0x7f0a00a5);
        mCircleProgressBar.setOnTouchListener(new _cls2());
        mValueTv = (TextView)findViewById(0x7f0a00a0);
        mHzlabel = (TextView)findViewById(0x7f0a00a1);
        mValueTv.setText("\u8F7D\u5165\u4E2D..");
        mHzlabel.setText("");
        mTurnUpIv = (ImageView)findViewById(0x7f0a00a2);
        mTurnUpIv.setOnClickListener(this);
        mTurnDownIv = (ImageView)findViewById(0x7f0a00a3);
        mTurnDownIv.setOnClickListener(this);
        mTurnDownIv.setOnTouchListener(new _cls3());
        mTurnUpIv.setOnTouchListener(new _cls4());
        mCircleProgressBar.setDialListener(new _cls5());
        initListener();
    }

    private void removeFrequencyDelayed()
    {
        if (!mIsCanSet)
        {
            mCircleProgressBar.removeCallbacks(frequenceSet);
            mIsCanSet = true;
        }
    }

    public static float sub(float f, float f1)
    {
        return (new BigDecimal(Float.toString(f))).subtract(new BigDecimal(Float.toString(f1))).floatValue();
    }

    private void turnDown()
    {
        if ((double)mRateValue <= 87.5D)
        {
            mRateValue = 108F;
        } else
        {
            mRateValue = sub(mRateValue, 0.1F);
        }
        freshValueShow();
        mCircleProgressBar.setValue(mRateValue);
    }

    private void turnUp()
    {
        if (mRateValue >= 108F)
        {
            mRateValue = 87.5F;
        } else
        {
            mRateValue = add(mRateValue, 0.1F);
        }
        freshValueShow();
        mCircleProgressBar.setValue(mRateValue);
    }

    public void init()
    {
        mContentView.findViewById(0x7f0a0317).setOnClickListener(this);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        Logger.d("suicheting", "CheTingFragment:onActivityCreated");
        mMyBluetoothManager2 = MyBluetoothManager2.getInstance(mCon);
        mBluetoothDeviceManager = mMyBluetoothManager2.getBluetoothDeviceManager(mCon);
        if (mMyBluetoothManager2.getBluetoothDeviceFmSenderManager() != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
        } else
        {
            mBluetoothDeviceFmSenderManager = mBluetoothDeviceManager.getBluetoothDeviceFmSenderManager();
            mMyBluetoothManager2.setBluetoothDeviceFmSenderManager(mBluetoothDeviceFmSenderManager);
        }
        initView();
        mMyBluetoothManager2.startSppService();
        Logger.d("suicheting", "\u5F00\u59CB\u521B\u5EFAmanager");
        if (mMyBluetoothManager2.getBluetoothDeviceFmSenderManager() != null)
        {
            mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
        } else
        {
            mBluetoothDeviceFmSenderManager = BluetoothDeviceFmSenderManager.getInstance();
            mMyBluetoothManager2.setBluetoothDeviceFmSenderManager(mBluetoothDeviceFmSenderManager);
        }
        initListener();
        (new Thread(new _cls1())).start();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131361954: 
            turnUp();
            setFrequency();
            return;

        case 2131361955: 
            Logger.d("onTouch", "onClick");
            turnDown();
            isOnTouch = false;
            setFrequency();
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;

        case 2131363612: 
            mMyBluetoothManager2.getVoicePromptStatus();
            break;
        }
        mMyBluetoothManager2.getPlayKeySendingStatus();
        startFragment(com/ximalaya/ting/android/fragment/device/che/CheTingSettingFragment, null);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f030016, viewgroup, false);
        mMyBluetoothManager2 = MyBluetoothManager2.getInstance(mCon);
        mBluetoothDeviceManager = mMyBluetoothManager2.getBluetoothDeviceManager(mActivity);
        return mContentView;
    }

    public void onResume()
    {
        super.onResume();
        if (mMyBluetoothManager2 == null || !mMyBluetoothManager2.judgeConn())
        {
            getActivity().onBackPressed();
        } else
        {
            mMyBluetoothManager2.startSppService();
            if (mBluetoothDeviceFmSenderManager != null)
            {
                mInitStartValue = true;
                mBluetoothDeviceFmSenderManager.getStatus();
                return;
            }
        }
    }

    public void onStop()
    {
        super.onStop();
        if (mMyBluetoothManager2 != null)
        {
            mMyBluetoothManager2.closeSppService();
        }
    }

    protected void setFrequency()
    {
        Logger.d("suicheting", "setFrequency");
        if (mBluetoothDeviceFmSenderManager != null)
        {
            mInitStartValue = false;
            mBluetoothDeviceFmSenderManager.setFrequency((int)(mRateValue * 1000F));
            Logger.d("suicheting", (new StringBuilder()).append("setFrequency:").append(mRateValue).toString());
        }
    }



/*
    static boolean access$002(CheFmSenderFragment chefmsenderfragment, boolean flag)
    {
        chefmsenderfragment.mInitStartValue = flag;
        return flag;
    }

*/





/*
    static float access$1102(CheFmSenderFragment chefmsenderfragment, float f)
    {
        chefmsenderfragment.mRateValue = f;
        return f;
    }

*/











    private class _cls6
        implements Runnable
    {

        final CheFmSenderFragment this$0;

        public void run()
        {
            setFrequency();
        }

        _cls6()
        {
            this$0 = CheFmSenderFragment.this;
            super();
        }
    }


    private class _cls7
        implements com.chipsguide.lib.bluetooth.extend.devices.BluetoothDeviceFmSenderManager.OnBluetoothDeviceFmSenderStatusChangedListener
    {

        final CheFmSenderFragment this$0;

        public void onBluetoothDeviceFmSenderStatusChanged(boolean flag, int i)
        {
            Logger.d("suicheting", (new StringBuilder()).append("onBluetoothDeviceFmSenderStatusChanged:").append(flag).append(",").append(i).toString());
            if (mInitStartValue)
            {
                float f = i;
                mCircleProgressBar.setValue(f / 1000F);
                mRateValue = f / 1000F;
                mValueTv.setText((new StringBuilder()).append(mRateValue).append("").toString());
                mHzlabel.setText("MHz");
                mInitStartValue = false;
            }
        }

        _cls7()
        {
            this$0 = CheFmSenderFragment.this;
            super();
        }
    }


    private class _cls8
        implements com.chipsguide.lib.bluetooth.extend.devices.BluetoothDeviceFmSenderManager.OnBluetoothDeviceFmSenderVoicePromptStatusListener
    {

        final CheFmSenderFragment this$0;

        public void OnBluetoothDeviceFmSenderVoicePromptStatus(boolean flag)
        {
            Logger.d("suicheting", (new StringBuilder()).append("\u5F53\u524D\u5B9E\u9645\u63D0\u793A\u97F3\u72B6\u6001").append(flag).toString());
            mMyBluetoothManager2.setWarning(flag);
        }

        _cls8()
        {
            this$0 = CheFmSenderFragment.this;
            super();
        }
    }


    private class _cls9
        implements com.chipsguide.lib.bluetooth.extend.devices.BluetoothDeviceFmSenderManager.OnBluetoothDeviceFmSenderPlayKeySendingStatusListener
    {

        final CheFmSenderFragment this$0;

        public void OnBluetoothDeviceFmSenderPlayKeySendingStatus(boolean flag)
        {
            Logger.d("suicheting", (new StringBuilder()).append("\u5F53\u524D\u5B9E\u9645\u81EA\u52A8\u64AD\u653E\u72B6\u6001").append(flag).toString());
            mMyBluetoothManager2.setAutoPlaying(flag);
        }

        _cls9()
        {
            this$0 = CheFmSenderFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnTouchListener
    {

        final CheFmSenderFragment this$0;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            motionevent.getAction();
            JVM INSTR tableswitch 0 1: default 28
        //                       0 40
        //                       1 61;
               goto _L1 _L2 _L3
_L1:
            return mCircleProgressBar.onTouchEvent(motionevent);
_L2:
            mScrollView.requestDisallowInterceptTouchEvent(true);
            removeFrequencyDelayed();
            continue; /* Loop/switch isn't completed */
_L3:
            mScrollView.requestDisallowInterceptTouchEvent(false);
            setFrequency();
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls2()
        {
            this$0 = CheFmSenderFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnTouchListener
    {

        final CheFmSenderFragment this$0;
        Timer timer;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            switch (motionevent.getAction())
            {
            default:
                return false;

            case 0: // '\0'
                Logger.d("onTouch", "ACTION_DOWN");
                timer = new Timer();
                isOnTouch = true;
                timer.schedule(new _cls1(), 500L, 100L);
                return false;

            case 1: // '\001'
                Logger.d("onTouch", "ACTION_UP");
                break;
            }
            if (timer != null)
            {
                timer.cancel();
                timer = null;
            }
            isOnTouch = false;
            setFrequency();
            return false;
        }

        _cls3()
        {
            this$0 = CheFmSenderFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnTouchListener
    {

        final CheFmSenderFragment this$0;
        Timer timer;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            switch (motionevent.getAction())
            {
            default:
                return false;

            case 0: // '\0'
                Logger.d("onTouch", "ACTION_DOWN");
                timer = new Timer();
                isOnTouch = true;
                timer.schedule(new _cls1(), 500L, 100L);
                return false;

            case 1: // '\001'
                Logger.d("onTouch", "ACTION_UP");
                break;
            }
            if (timer != null)
            {
                timer.cancel();
                timer = null;
            }
            isOnTouch = false;
            setFrequency();
            return false;
        }

        _cls4()
        {
            this$0 = CheFmSenderFragment.this;
            super();
        }
    }


    private class _cls5
        implements com.ximalaya.ting.android.view.dial.CircleProgressBar.DialListener
    {

        final CheFmSenderFragment this$0;

        public void onValueAt(float f)
        {
            mRateValue = f;
            freshValueShow();
        }

        _cls5()
        {
            this$0 = CheFmSenderFragment.this;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final CheFmSenderFragment this$0;

        public void run()
        {
            mInitStartValue = true;
            while (mValueTv.getText().toString().equals("\u8F7D\u5165\u4E2D..")) 
            {
                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
                mBluetoothDeviceFmSenderManager.getStatus();
                mMyBluetoothManager2.getVoicePromptStatus();
                mMyBluetoothManager2.getPlayKeySendingStatus();
            }
        }

        _cls1()
        {
            this$0 = CheFmSenderFragment.this;
            super();
        }
    }

}
