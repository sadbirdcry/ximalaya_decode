// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.che;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.che:
//            MyBluetoothManager2

public class CheUseTipFragment extends BaseFragment
    implements android.view.View.OnClickListener
{

    protected static final String TAG = "WIFI";
    private RelativeLayout mContentView;

    public CheUseTipFragment()
    {
    }

    public void init()
    {
        mContentView.findViewById(0x7f0a0317).setOnClickListener(this);
        MyBluetoothManager2.getInstance(mActivity).setInitFrequency();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        Logger.d("WIFI", "CheTingFragment:onActivityCreated");
        init();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        case 2131362583: 
        default:
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300a9, viewgroup, false);
        return mContentView;
    }
}
