// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.che;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.che:
//            MyBluetoothManager2

public class CheTingIntroFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    protected static final String TAG = "suicheting";
    private LinearLayout goBlueTooth;
    private LinearLayout mBackHome;
    private ImageView mBackImg;
    private TextView mBtnRight;
    private RelativeLayout mContentView;
    private LinearLayout mSetting;
    private TextView top_tv;

    public CheTingIntroFragment()
    {
    }

    private void expandHitRect(final View view)
    {
        mContentView.post(new _cls1());
    }

    private void initView()
    {
        top_tv = (TextView)(TextView)mContentView.findViewById(0x7f0a00ae);
        top_tv.setText("\u968F\u8F66\u542C");
        android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)top_tv.getLayoutParams();
        layoutparams.addRule(13);
        top_tv.setLayoutParams(layoutparams);
        goBlueTooth = (LinearLayout)mContentView.findViewById(0x7f0a02f7);
        goBlueTooth.setOnClickListener(this);
        expandHitRect(goBlueTooth);
        mBtnRight = (TextView)mContentView.findViewById(0x7f0a071c);
        mBtnRight.setVisibility(0);
        mBtnRight.setText("\u8BF4\u660E\u4E66");
        mBtnRight.setOnClickListener(this);
        mBackHome = (LinearLayout)mContentView.findViewById(0x7f0a02fa);
        mBackHome.setOnClickListener(this);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(this);
        mSetting = (LinearLayout)mContentView.findViewById(0x7f0a02f1);
        mSetting.setOnClickListener(this);
        expandHitRect(mSetting);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initView();
        hidePlayButton();
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 5: default 56
    //                   2131362545: 76
    //                   2131362551: 96
    //                   2131362554: 57
    //                   2131363611: 68
    //                   2131363612: 120;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        return;
_L4:
        if (!ToolUtil.isFastClick())
        {
            goToFindingPage();
            return;
        }
        continue; /* Loop/switch isn't completed */
_L5:
        getActivity().onBackPressed();
        return;
_L2:
        if (!ToolUtil.isFastClick())
        {
            MyBluetoothManager2.getInstance(mCon).tryConnect(false, false, this);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (getActivity() != null)
        {
            getActivity().startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
            return;
        }
        if (true) goto _L1; else goto _L6
_L6:
        MyBluetoothManager2.goIntroduction(getActivity());
        return;
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300a4, viewgroup, false);
        return mContentView;
    }

    public void onResume()
    {
        super.onResume();
        Logger.d("suicheting", "CheTingIntroFragment onResume");
        hidePlayButton();
    }

    private class _cls1
        implements Runnable
    {

        final CheTingIntroFragment this$0;
        final View val$view;

        public void run()
        {
            if (isAdded())
            {
                Object obj = new Rect();
                View view1 = view;
                view1.getHitRect(((Rect) (obj)));
                int i = ToolUtil.dp2px(getActivity(), 100F);
                obj.right = ((Rect) (obj)).right + i;
                obj.left = ((Rect) (obj)).left - i;
                obj.top = ((Rect) (obj)).top + i;
                obj.bottom = ((Rect) (obj)).bottom - i;
                obj = new TouchDelegate(((Rect) (obj)), view1);
                if (android/view/View.isInstance(view1.getParent()))
                {
                    ((View)view1.getParent()).setTouchDelegate(((TouchDelegate) (obj)));
                }
            }
        }

        _cls1()
        {
            this$0 = CheTingIntroFragment.this;
            view = view1;
            super();
        }
    }

}
