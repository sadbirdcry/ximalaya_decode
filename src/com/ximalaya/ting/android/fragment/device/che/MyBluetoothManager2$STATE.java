// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.che;


// Referenced classes of package com.ximalaya.ting.android.fragment.device.che:
//            MyBluetoothManager2

public static final class  extends Enum
{

    private static final FAILED $VALUES[];
    public static final FAILED CONNECTED;
    public static final FAILED CONNECTING;
    public static final FAILED FAILED;
    public static final FAILED START;

    public static  valueOf(String s)
    {
        return ()Enum.valueOf(com/ximalaya/ting/android/fragment/device/che/MyBluetoothManager2$STATE, s);
    }

    public static [] values()
    {
        return ([])$VALUES.clone();
    }

    static 
    {
        START = new <init>("START", 0);
        CONNECTING = new <init>("CONNECTING", 1);
        CONNECTED = new <init>("CONNECTED", 2);
        FAILED = new <init>("FAILED", 3);
        $VALUES = (new .VALUES[] {
            START, CONNECTING, CONNECTED, FAILED
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
