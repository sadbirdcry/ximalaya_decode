// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.che;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.chipsguide.lib.bluetooth.extend.devices.BluetoothDeviceFmSenderManager;
import com.chipsguide.lib.bluetooth.managers.BluetoothDeviceManager;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.ManageFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.web.WebFragment;
import com.ximalaya.ting.android.library.util.Logger;
import java.lang.ref.SoftReference;
import java.util.List;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.che:
//            MyBluetoothManager2, CheFmSenderFragment

public class CheTingFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    class ConningDialog extends Dialog
    {

        final CheTingFragment this$0;

        protected void onCreate(Bundle bundle)
        {
            super.onCreate(bundle);
            requestWindowFeature(1);
            setCancelable(false);
        }

        public ConningDialog(Context context)
        {
            this$0 = CheTingFragment.this;
            super(context, 0x7f0b0027);
        }
    }


    public static final int REQUEST_ENABLE_BT = 10;
    protected static final String TAG = "WIFI";
    public static final String TARGET_DEVICE = "AD_995";
    public boolean isBtEnable;
    private boolean isRetryed;
    private boolean isStartConningTvAnim;
    private FragmentActivity mActivity;
    private ImageView mBackImg;
    private BluetoothDeviceFmSenderManager mBluetoothDeviceFmSenderManager;
    private BluetoothDeviceManager mBluetoothDeviceManager;
    private BluetoothAdapter mBtAdapter;
    private TextView mBtDisConn;
    private TextView mConnBtn;
    private TextView mConnTipsTv;
    private RelativeLayout mConningLayout;
    private RelativeLayout mContentView;
    private RelativeLayout mFailedLayout;
    private boolean mIs2Fm;
    private volatile boolean mIsOnConning;
    private boolean mIsStart;
    private MyBluetoothManager2 mMyBluetoothManager2;
    private TextView mSetting;
    private RelativeLayout mStartLayout;
    private BluetoothDevice mTempDevice;
    private Timer mTimer;
    private TextView mTopTv;
    Timer timer;

    public CheTingFragment()
    {
        mIsStart = false;
        isRetryed = false;
        timer = null;
        isStartConningTvAnim = false;
    }

    private void cancelConningTvAnim()
    {
        if (!isStartConningTvAnim)
        {
            return;
        } else
        {
            isStartConningTvAnim = false;
            mTimer.cancel();
            return;
        }
    }

    private void connDevice(BluetoothDevice bluetoothdevice)
    {
        while (bluetoothdevice == null || mBluetoothDeviceManager == null) 
        {
            return;
        }
        mTempDevice = bluetoothdevice;
        mBluetoothDeviceManager.connect(bluetoothdevice);
    }

    private void fresh2Connected()
    {
        if (timer != null)
        {
            timer.cancel();
            timer = null;
        }
        isRetryed = false;
        Logger.d("WIFI", (new StringBuilder()).append("fresh2Connected:mTempDevice:").append(mTempDevice).toString());
        if (mBluetoothDeviceManager != null)
        {
            mBluetoothDeviceManager.cancelDiscovery();
        }
        if (mTempDevice != null)
        {
            mMyBluetoothManager2.setBluetoothDevice(mTempDevice);
        }
        if (!mIs2Fm)
        {
            setNowState(MyBluetoothManager2.STATE.CONNECTED);
            toFmSenderActivity();
        }
        cancelConningTvAnim();
    }

    private void fresh2Connecting()
    {
        mStartLayout.setVisibility(8);
        mConningLayout.setVisibility(0);
        mFailedLayout.setVisibility(8);
        startConningTvAnim();
    }

    private void fresh2Failed()
    {
        mStartLayout.setVisibility(8);
        mConningLayout.setVisibility(8);
        mFailedLayout.setVisibility(0);
    }

    private void fresh2Start()
    {
        if (!isAdded())
        {
            return;
        } else
        {
            mStartLayout.setVisibility(0);
            mConningLayout.setVisibility(8);
            mFailedLayout.setVisibility(8);
            cancelConningTvAnim();
            return;
        }
    }

    private void freshView()
    {
        Logger.d("WIFI", (new StringBuilder()).append("freshView:state").append(getNowState()).toString());
        static class _cls11
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$che$MyBluetoothManager2$STATE[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$che$MyBluetoothManager2$STATE = new int[MyBluetoothManager2.STATE.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$che$MyBluetoothManager2$STATE[MyBluetoothManager2.STATE.START.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$che$MyBluetoothManager2$STATE[MyBluetoothManager2.STATE.CONNECTING.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$che$MyBluetoothManager2$STATE[MyBluetoothManager2.STATE.CONNECTED.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$che$MyBluetoothManager2$STATE[MyBluetoothManager2.STATE.FAILED.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        switch (_cls11..SwitchMap.com.ximalaya.ting.android.fragment.device.che.MyBluetoothManager2.STATE[getNowState().ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            fresh2Start();
            return;

        case 2: // '\002'
            fresh2Connecting();
            return;

        case 3: // '\003'
            fresh2Connected();
            return;

        case 4: // '\004'
            fresh2Failed();
            break;
        }
    }

    private BluetoothDevice getA2dpConnDevice()
    {
        BluetoothDevice bluetoothdevice = null;
        if (mBluetoothDeviceManager.getBluetoothDeviceConnectedA2dp() != null)
        {
            bluetoothdevice = mBluetoothDeviceManager.getBluetoothDeviceConnectedA2dp();
        }
        return bluetoothdevice;
    }

    private BluetoothDevice getConnDevice()
    {
        if (mBluetoothDeviceManager != null)
        {
            BluetoothDevice bluetoothdevice;
            BluetoothDevice bluetoothdevice1;
            if (mBluetoothDeviceManager.getBluetoothDeviceConnectedA2dp() != null)
            {
                bluetoothdevice = mBluetoothDeviceManager.getBluetoothDeviceConnectedA2dp();
            } else
            {
                bluetoothdevice = null;
            }
            if (mBluetoothDeviceManager.getBluetoothDeviceConnectedSpp() != null)
            {
                bluetoothdevice1 = mBluetoothDeviceManager.getBluetoothDeviceConnectedSpp();
            } else
            {
                bluetoothdevice1 = null;
            }
            if (bluetoothdevice1 != null && bluetoothdevice != null)
            {
                return bluetoothdevice;
            }
        }
        return null;
    }

    private void initBtListener()
    {
        if (mBluetoothDeviceManager == null)
        {
            try
            {
                Thread.sleep(200L);
            }
            catch (InterruptedException interruptedexception)
            {
                interruptedexception.printStackTrace();
            }
        }
        if (mBluetoothDeviceManager == null)
        {
            getActivity().onBackPressed();
            return;
        } else
        {
            mBluetoothDeviceManager.setOnBluetoothDeviceDiscoveryListener(new _cls6());
            mBluetoothDeviceManager.setOnBluetoothDeviceConnectionStateChangedListener(new _cls7());
            mBluetoothDeviceManager.setOnBluetoothDeviceManagerReadyListener(new _cls8());
            return;
        }
    }

    private void initView()
    {
        mBtDisConn = (TextView)mContentView.findViewById(0x7f0a071c);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mTopTv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mBtDisConn.setOnClickListener(this);
        mBackImg.setOnClickListener(this);
        mTopTv.setOnClickListener(this);
        mSetting = (TextView)mContentView.findViewById(0x7f0a0573);
        mSetting.setOnClickListener(this);
        mConningLayout = (RelativeLayout)mContentView.findViewById(0x7f0a031f);
        mFailedLayout = (RelativeLayout)mContentView.findViewById(0x7f0a0324);
        mStartLayout = (RelativeLayout)mContentView.findViewById(0x7f0a0092);
        mConnBtn = (TextView)(TextView)mContentView.findViewById(0x7f0a031c);
        ((TextView)(TextView)mContentView.findViewById(0x7f0a00ae)).setText("\u968F\u8F66\u542C");
        mConnTipsTv = (TextView)(TextView)mContentView.findViewById(0x7f0a0322);
        if (isAdded())
        {
            WebFragment webfragment = new WebFragment();
            Bundle bundle = new Bundle();
            bundle.putCharSequence("ExtraUrl", mMyBluetoothManager2.getHardwareShoppingUrl());
            bundle.putBoolean("WEB_TITLE_BAR_VISIBLE", false);
            webfragment.setArguments(bundle);
        }
        ((TextView)(TextView)mContentView.findViewById(0x7f0a031b)).setOnClickListener(this);
        ((TextView)(TextView)mContentView.findViewById(0x7f0a0329)).setOnClickListener(this);
        mConnBtn.setOnClickListener(this);
    }

    private boolean isOurDevice(BluetoothDevice bluetoothdevice)
    {
        return MyDeviceManager.isSuichetingDevice(bluetoothdevice);
    }

    private boolean judgeConn()
    {
        mTempDevice = getConnDevice();
        Logger.d("WIFI", (new StringBuilder()).append("judgeConn:").append(mTempDevice).toString());
        if (mTempDevice != null && isOurDevice(mTempDevice))
        {
            setNowState(MyBluetoothManager2.STATE.CONNECTED);
            mActivity.runOnUiThread(new _cls3());
            return true;
        } else
        {
            return false;
        }
    }

    private void onBack()
    {
        _cls11..SwitchMap.com.ximalaya.ting.android.fragment.device.che.MyBluetoothManager2.STATE[getNowState().ordinal()];
        JVM INSTR tableswitch 1 4: default 40
    //                   1 45
    //                   2 55
    //                   3 72
    //                   4 82;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        freshView();
        return;
_L2:
        getActivity().onBackPressed();
        continue; /* Loop/switch isn't completed */
_L3:
        setNowState(MyBluetoothManager2.STATE.START);
        getActivity().onBackPressed();
        continue; /* Loop/switch isn't completed */
_L4:
        getActivity().onBackPressed();
        continue; /* Loop/switch isn't completed */
_L5:
        setNowState(MyBluetoothManager2.STATE.START);
        getActivity().onBackPressed();
        if (true) goto _L1; else goto _L6
_L6:
    }

    private void startConningTvAnim()
    {
        if (isStartConningTvAnim)
        {
            return;
        } else
        {
            mTimer = new Timer();
            isStartConningTvAnim = true;
            mTimer.schedule(new _cls9(), 500L, 500L);
            return;
        }
    }

    private void startDiscoverDevice()
    {
        if (mBluetoothDeviceManager != null)
        {
            Logger.d("WIFI", "startDiscoverDevice");
            mBluetoothDeviceManager.startDiscovery();
        }
    }

    private void startSuicheting()
    {
        if (!mIsStart)
        {
            mIsStart = true;
            mBluetoothDeviceManager = mMyBluetoothManager2.getBluetoothDeviceManager(mActivity);
            mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
            mMyBluetoothManager2.startSppService();
            Logger.d("WIFI", (new StringBuilder()).append("init:2").append(mBluetoothDeviceManager).toString());
            isBtEnable = mBtAdapter.isEnabled();
            initBtListener();
            if (judgeConn())
            {
                if (!mIs2Fm)
                {
                    toFmSenderActivity();
                    return;
                }
            } else
            {
                setNowState(MyBluetoothManager2.STATE.START);
                mIs2Fm = false;
                return;
            }
        }
    }

    private void toBuy()
    {
        startDiscoverDevice();
    }

    private void toConn()
    {
        startSuicheting();
        if (!isAdded())
        {
            return;
        }
        if (mBluetoothDeviceManager == null)
        {
            MyApplication.a().runOnUiThread(new _cls4());
            return;
        }
        setNowState(MyBluetoothManager2.STATE.CONNECTING);
        BluetoothDevice bluetoothdevice = mBluetoothDeviceManager.getBluetoothDeviceConnectedA2dp();
        if (isOurDevice(bluetoothdevice))
        {
            mIsOnConning = true;
            connDevice(bluetoothdevice);
        }
        freshView();
    }

    private void toFmSenderActivity()
    {
        mIs2Fm = true;
        mMyBluetoothManager2.setChangeAlbumListener(mActivity);
        int i;
        if (getManageFragment() != null)
        {
            if ((i = getManageFragment().mStacks.size()) >= 1 && ((Fragment)((SoftReference)getManageFragment().mStacks.get(i - 1)).get() instanceof CheTingFragment))
            {
                removeTopFramentFromManageFragment();
                startFragment(com/ximalaya/ting/android/fragment/device/che/CheFmSenderFragment, null);
                return;
            }
        }
    }

    private void toGetConnedDevice(final int opType)
    {
        (new Thread(new _cls5())).start();
    }

    public MyBluetoothManager2.STATE getNowState()
    {
        return mMyBluetoothManager2.getNowState();
    }

    public void init()
    {
        getActivity().runOnUiThread(new _cls1());
        mMyBluetoothManager2 = MyBluetoothManager2.getInstance(mActivity);
        mBluetoothDeviceManager = mMyBluetoothManager2.getBluetoothDeviceManager(mActivity);
        mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
        Logger.d("WIFI", (new StringBuilder()).append("init:1").append(mBluetoothDeviceManager).toString());
        initView();
    }

    public void onActivityCreated(Bundle bundle)
    {
        XiMaoBTManager.getInstance(mCon).cancel();
        super.onActivityCreated(bundle);
        mActivity = getActivity();
        init();
        toConn();
    }

    public void onBtDisable()
    {
        isBtEnable = false;
    }

    public void onBtEnable()
    {
        isBtEnable = true;
        toConn();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362587: 
            toBuy();
            return;

        case 2131362588: 
            toConn();
            return;

        case 2131362601: 
            toConn();
            return;

        case 2131363611: 
            onBack();
            return;

        case 2131363187: 
            startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f03016e, viewgroup, false);
        return mContentView;
    }

    public void onDestroy()
    {
        cancelConningTvAnim();
        super.onDestroy();
    }

    public void onResume()
    {
        super.onResume();
        Logger.d("WIFI", "CheTingFragment:onResume");
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new _cls2());
        mIs2Fm = false;
    }

    public void onStop()
    {
        super.onStop();
    }

    public void setNowState(MyBluetoothManager2.STATE state)
    {
        mMyBluetoothManager2.setNowState(state);
    }

    public void setReconnectTimeout()
    {
        if (timer != null)
        {
            return;
        } else
        {
            timer = new Timer();
            _cls10 _lcls10 = new _cls10();
            timer.schedule(_lcls10, 10000L);
            return;
        }
    }

    public void showToast(String s)
    {
        if (isAdded())
        {
            Toast.makeText(getActivity(), s, 0).show();
        }
    }


/*
    static BluetoothAdapter access$002(CheTingFragment chetingfragment, BluetoothAdapter bluetoothadapter)
    {
        chetingfragment.mBtAdapter = bluetoothadapter;
        return bluetoothadapter;
    }

*/




/*
    static BluetoothDeviceFmSenderManager access$1002(CheTingFragment chetingfragment, BluetoothDeviceFmSenderManager bluetoothdevicefmsendermanager)
    {
        chetingfragment.mBluetoothDeviceFmSenderManager = bluetoothdevicefmsendermanager;
        return bluetoothdevicefmsendermanager;
    }

*/




/*
    static boolean access$1202(CheTingFragment chetingfragment, boolean flag)
    {
        chetingfragment.isRetryed = flag;
        return flag;
    }

*/








/*
    static boolean access$402(CheTingFragment chetingfragment, boolean flag)
    {
        chetingfragment.mIsOnConning = flag;
        return flag;
    }

*/






    private class _cls6
        implements OnBluetoothDeviceDiscoveryListener
    {

        final CheTingFragment this$0;

        public void onBluetoothDeviceDiscoveryFinished()
        {
            Logger.d("WIFI", (new StringBuilder()).append("onBluetoothDeviceDiscoveryFinished\uFF1Astate:").append(getNowState()).append(";mIsOnConning=").append(mIsOnConning).toString());
            class _cls1
                implements Runnable
            {

                final _cls6 this$1;

                public void run()
                {
                    try
                    {
                        Thread.sleep(1000L);
                    }
                    catch (InterruptedException interruptedexception)
                    {
                        interruptedexception.printStackTrace();
                    }
                    if (mIsOnConning)
                    {
                        return;
                    } else
                    {
                        class _cls1
                            implements Runnable
                        {

                            final _cls1 this$2;

                            public void run()
                            {
                                showToast("\u641C\u7D22\u5B8C\u6210");
                                break MISSING_BLOCK_LABEL_15;
                                if (getNowState() != MyBluetoothManager2.STATE.CONNECTED && getNowState() == MyBluetoothManager2.STATE.CONNECTING)
                                {
                                    setNowState(MyBluetoothManager2.STATE.FAILED);
                                }
                                freshView();
                                return;
                            }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                        }

                        mActivity.runOnUiThread(new _cls1());
                        return;
                    }
                }

                _cls1()
                {
                    this$1 = _cls6.this;
                    super();
                }
            }

            (new Thread(new _cls1())).start();
        }

        public void onBluetoothDeviceDiscoveryFound(BluetoothDevice bluetoothdevice)
        {
            Logger.d("WIFI", (new StringBuilder()).append("Found only one").append(bluetoothdevice.getName()).toString());
            showToast("\u53D1\u73B0\u8BBE\u5907");
            mBluetoothDeviceManager.cancelDiscovery();
            if (isOurDevice(bluetoothdevice) && getNowState() == MyBluetoothManager2.STATE.CONNECTING)
            {
                mIsOnConning = true;
                connDevice(bluetoothdevice);
            }
        }

        public void onBluetoothDeviceDiscoveryStarted()
        {
            Logger.d("WIFI", "DiscoveryStarted");
        }

        _cls6()
        {
            this$0 = CheTingFragment.this;
            super();
        }
    }


    private class _cls7
        implements OnBluetoothDeviceConnectionStateChangedListener
    {

        final CheTingFragment this$0;

        public void onBluetoothDeviceConnectionStateChanged(BluetoothDevice bluetoothdevice, int i)
        {
            switch (i)
            {
            case 9: // '\t'
            default:
                return;

            case 1: // '\001'
                setNowState(MyBluetoothManager2.STATE.CONNECTED);
                for (i = 0; mBluetoothDeviceFmSenderManager == null && i < 400;)
                {
                    i++;
                    Logger.d("WIFI", "\u5F00\u59CB\u521B\u5EFAmanager");
                    if (mMyBluetoothManager2.getBluetoothDeviceFmSenderManager() != null)
                    {
                        mBluetoothDeviceFmSenderManager = mMyBluetoothManager2.getBluetoothDeviceFmSenderManager();
                    } else
                    {
                        mBluetoothDeviceFmSenderManager = BluetoothDeviceFmSenderManager.getInstance();
                        mMyBluetoothManager2.setBluetoothDeviceFmSenderManager(mBluetoothDeviceFmSenderManager);
                    }
                }

                if (i >= 400)
                {
                    setNowState(MyBluetoothManager2.STATE.FAILED);
                }
                freshView();
                Logger.d("WIFI", "\u84DD\u7259\u8FDE\u63A5\u6210\u529F");
                return;

            case 0: // '\0'
                Logger.d("WIFI", "\u84DD\u7259\u65AD\u5F00");
                return;

            case 2: // '\002'
                Logger.d("WIFI", "A2DP\u914D\u5BF9\u4E2D");
                return;

            case 3: // '\003'
                Logger.d("WIFI", "A2DP\u8FDE\u63A5\u4E2D");
                return;

            case 4: // '\004'
                Logger.d("WIFI", "A2DP\u5DF2\u8FDE\u63A5");
                return;

            case 5: // '\005'
                Logger.d("WIFI", "A2DP\u8FDE\u63A5\u5931\u8D25");
                return;

            case 6: // '\006'
                Logger.d("WIFI", "A2DP\u65AD\u5F00\u8FDE\u63A5");
                return;

            case 7: // '\007'
                Logger.d("WIFI", "SPP\u8FDE\u63A5\u4E2D");
                return;

            case 8: // '\b'
                Logger.d("WIFI", "SPP\u5DF2\u8FDE\u63A5");
                return;

            case 10: // '\n'
                Logger.d("WIFI", "SPP\u65AD\u5F00\u8FDE\u63A5");
                return;

            case 11: // '\013'
                Logger.d("WIFI", "\u64CD\u4F5C\u8D85\u65F6");
                if (!isRetryed)
                {
                    isRetryed = true;
                    bluetoothdevice = getA2dpConnDevice();
                    mBluetoothDeviceManager.disconnect(bluetoothdevice);
                    mBluetoothDeviceManager.connect(bluetoothdevice);
                    setReconnectTimeout();
                    return;
                } else
                {
                    setNowState(MyBluetoothManager2.STATE.FAILED);
                    freshView();
                    return;
                }

            case 12: // '\f'
                Logger.d("WIFI", "\u65E0\u6CD5\u5EFA\u7ACB\u8FDE\u63A5");
                break;
            }
            if (!isRetryed)
            {
                isRetryed = true;
                bluetoothdevice = getA2dpConnDevice();
                mBluetoothDeviceManager.disconnect(bluetoothdevice);
                mBluetoothDeviceManager.connect(bluetoothdevice);
                setReconnectTimeout();
                return;
            } else
            {
                setNowState(MyBluetoothManager2.STATE.FAILED);
                freshView();
                return;
            }
        }

        _cls7()
        {
            this$0 = CheTingFragment.this;
            super();
        }
    }


    private class _cls8
        implements OnBluetoothDeviceManagerReadyListener
    {

        final CheTingFragment this$0;

        public void onBluetoothDeviceManagerReady()
        {
            Logger.d("WIFI", "onBluetoothDeviceManagerReady");
            if (mMyBluetoothManager2.getBluetoothDevice() != null)
            {
                setNowState(MyBluetoothManager2.STATE.CONNECTED);
                freshView();
                return;
            } else
            {
                toGetConnedDevice(0);
                return;
            }
        }

        _cls8()
        {
            this$0 = CheTingFragment.this;
            super();
        }
    }


    private class _cls3
        implements Runnable
    {

        final CheTingFragment this$0;

        public void run()
        {
            freshView();
        }

        _cls3()
        {
            this$0 = CheTingFragment.this;
            super();
        }
    }


    private class _cls9 extends TimerTask
    {

        int i;
        final String strings[] = {
            "", ".", "..", "..."
        };
        final CheTingFragment this$0;

        public void run()
        {
            class _cls1
                implements Runnable
            {

                final _cls9 this$1;

                public void run()
                {
                    _cls9 _lcls9 = _cls9.this;
                    _lcls9.i = _lcls9.i + 1;
                    mConnTipsTv.setText(strings[i % 4]);
                }

                _cls1()
                {
                    this$1 = _cls9.this;
                    super();
                }
            }

            mActivity.runOnUiThread(new _cls1());
        }

        _cls9()
        {
            this$0 = CheTingFragment.this;
            super();
            i = 0;
        }
    }


    private class _cls4
        implements Runnable
    {

        final CheTingFragment this$0;

        public void run()
        {
            (new DialogBuilder(MyApplication.a())).setMessage("\u8C03\u9891\u529F\u80FD\u542F\u7528\u5931\u8D25\uFF0C\u8BF7\u91CD\u542F\u559C\u9A6C\u62C9\u96C5APP\u540E\u518D\u8BD5").showWarning();
        }

        _cls4()
        {
            this$0 = CheTingFragment.this;
            super();
        }
    }


    private class _cls5
        implements Runnable
    {

        final CheTingFragment this$0;
        final int val$opType;

        public void run()
        {
            Logger.d("WIFI", (new StringBuilder()).append("toGetConnedDevice opType:").append(opType).toString());
            int i = 0;
            while (getNowState() == MyBluetoothManager2.STATE.CONNECTING && i < 4) 
            {
                judgeConn();
                try
                {
                    Thread.sleep(500L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
                i++;
            }
            mIsOnConning = false;
            if (getNowState() == MyBluetoothManager2.STATE.CONNECTED)
            {
                return;
            } else
            {
                class _cls1
                    implements Runnable
                {

                    final _cls5 this$1;

                    public void run()
                    {
                        opType;
                        JVM INSTR tableswitch 1 2: default 28
                    //                                   1 39
                    //                                   2 52;
                           goto _L1 _L2 _L3
_L1:
                        freshView();
                        return;
_L2:
                        startDiscoverDevice();
                        continue; /* Loop/switch isn't completed */
_L3:
                        setNowState(MyBluetoothManager2.STATE.FAILED);
                        if (true) goto _L1; else goto _L4
_L4:
                    }

                _cls1()
                {
                    this$1 = _cls5.this;
                    super();
                }
                }

                mActivity.runOnUiThread(new _cls1());
                return;
            }
        }

        _cls5()
        {
            this$0 = CheTingFragment.this;
            opType = i;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final CheTingFragment this$0;

        public void run()
        {
            if (BluetoothAdapter.getDefaultAdapter() != null)
            {
                mBtAdapter = BluetoothAdapter.getDefaultAdapter();
            }
        }

        _cls1()
        {
            this$0 = CheTingFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnKeyListener
    {

        final CheTingFragment this$0;

        public boolean onKey(View view, int i, KeyEvent keyevent)
        {
            if (keyevent.getAction() == 1 && i == 4)
            {
                onBack();
                return true;
            } else
            {
                return false;
            }
        }

        _cls2()
        {
            this$0 = CheTingFragment.this;
            super();
        }
    }


    private class _cls10 extends TimerTask
    {

        final CheTingFragment this$0;

        public void run()
        {
            BluetoothDevice bluetoothdevice = getA2dpConnDevice();
            class _cls1
                implements Runnable
            {

                final _cls10 this$1;

                public void run()
                {
                    setNowState(MyBluetoothManager2.STATE.FAILED);
                    freshView();
                }

                _cls1()
                {
                    this$1 = _cls10.this;
                    super();
                }
            }

            if (bluetoothdevice == null || !MyDeviceManager.isSuichetingDevice(bluetoothdevice))
            {
                getActivity().runOnUiThread(new _cls1());
            }
        }

        _cls10()
        {
            this$0 = CheTingFragment.this;
            super();
        }
    }

}
