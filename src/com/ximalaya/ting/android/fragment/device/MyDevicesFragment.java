// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.miniche.QCheTingIntroFragment;
import com.ximalaya.ting.android.fragment.device.che.CheTingIntroFragment;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceListFragment;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoContentFragment;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoIntroFragment;
import com.ximalaya.ting.android.model.AppConfig;
import com.ximalaya.ting.android.modelmanage.DexManager;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            MyDeviceManager, MyDeviceUtil, FunctionStartFragment

public class MyDevicesFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    protected static final String TAG = "device";
    private RelativeLayout mContentView;
    private DexManager mDexManager;
    private RelativeLayout mDoss;
    private String mFunction;
    private RelativeLayout mGoMarket;
    private RelativeLayout mMiniche;
    private RelativeLayout mShukeWifi;
    private RelativeLayout mTingShuBao;
    TextView top_tv;

    public MyDevicesFragment()
    {
    }

    private void initView()
    {
        top_tv = (TextView)(TextView)mContentView.findViewById(0x7f0a00ae);
        top_tv.setText("\u667A\u80FD\u786C\u4EF6\u8BBE\u5907");
        mGoMarket = (RelativeLayout)mContentView.findViewById(0x7f0a0338);
        mGoMarket.setOnClickListener(this);
        Object obj;
        boolean flag;
        if (MyDeviceManager.getInstance(getActivity().getApplicationContext()).getProductModel("\u4F53\u9A8C\u65B0\u58F0\u6D3B") != null)
        {
            mGoMarket.setVisibility(0);
        } else
        {
            mGoMarket.setVisibility(8);
        }
        mTingShuBao = (RelativeLayout)mContentView.findViewById(0x7f0a0334);
        mDoss = (RelativeLayout)mContentView.findViewById(0x7f0a0336);
        mMiniche = (RelativeLayout)mContentView.findViewById(0x7f0a032e);
        mShukeWifi = (RelativeLayout)mContentView.findViewById(0x7f0a0332);
        obj = (android.widget.RelativeLayout.LayoutParams)top_tv.getLayoutParams();
        ((android.widget.RelativeLayout.LayoutParams) (obj)).addRule(13);
        top_tv.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        mContentView.findViewById(0x7f0a032c).setOnClickListener(this);
        mContentView.findViewById(0x7f0a071b).setOnClickListener(this);
        mContentView.findViewById(0x7f0a0330).setOnClickListener(this);
        mContentView.findViewById(0x7f0a0334).setOnClickListener(this);
        mContentView.findViewById(0x7f0a0336).setOnClickListener(this);
        mContentView.findViewById(0x7f0a032e).setOnClickListener(this);
        mContentView.findViewById(0x7f0a0332).setOnClickListener(this);
        obj = AppConfig.getInstance();
        flag = ((AppConfig) (obj)).isHardwareBook;
        flag = ((AppConfig) (obj)).isHardwareDoss;
        if (MyDeviceUtil.isShowEnterance(mCon, "\u542C\u4E66\u5B9D\u9999\u69DF\u8272\u7248\u5F00\u5173"))
        {
            mTingShuBao.setVisibility(0);
        } else
        {
            mTingShuBao.setVisibility(8);
        }
        if (MyDeviceUtil.isShowEnterance(mCon, "\u542C\u4E66\u5B9D\u5F00\u5173"))
        {
            mDoss.setVisibility(0);
        } else
        {
            mDoss.setVisibility(8);
        }
        if (MyDeviceUtil.isShowEnterance(mCon, "\u968F\u8F66\u542CQ\u7248\u5F00\u5173"))
        {
            mMiniche.setVisibility(0);
        } else
        {
            mMiniche.setVisibility(8);
        }
        if (MyDeviceUtil.isShowEnterance(mCon, "wifi\u6545\u4E8B\u673A\u5F00\u5173"))
        {
            mShukeWifi.setVisibility(0);
            return;
        } else
        {
            mShukeWifi.setVisibility(8);
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mDexManager = DexManager.getInstance(mCon);
        initView();
        hidePlayButton();
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 8: default 80
    //                   2131362604: 81
    //                   2131362606: 447
    //                   2131362608: 214
    //                   2131362610: 374
    //                   2131362612: 133
    //                   2131362614: 301
    //                   2131362616: 500
    //                   2131363611: 206;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9
_L1:
        return;
_L2:
        if (!ToolUtil.isFastClick())
        {
            if (mDexManager.isSuichetingOpen())
            {
                startFragment(com/ximalaya/ting/android/fragment/device/che/CheTingIntroFragment, null);
                return;
            } else
            {
                view = new Bundle();
                view.putString("P_Type", MyDeviceManager.DeviceType.suicheting.toString());
                startFragment(com/ximalaya/ting/android/fragment/device/FunctionStartFragment, view);
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L6:
        if (!ToolUtil.isFastClick())
        {
            if (mDexManager.isTingshubaoOpen())
            {
                view = new Bundle();
                view.putInt(DeviceListFragment.deviceType, MyDeviceManager.DeviceType.tingshubao.ordinal());
                startFragment(com/ximalaya/ting/android/fragment/device/dlna/DeviceListFragment, view);
                return;
            } else
            {
                view = new Bundle();
                view.putString("P_Type", MyDeviceManager.DeviceType.tingshubao.toString());
                startFragment(com/ximalaya/ting/android/fragment/device/FunctionStartFragment, view);
                return;
            }
        }
        if (true) goto _L1; else goto _L9
_L9:
        getActivity().onBackPressed();
        return;
_L4:
        if (!ToolUtil.isFastClick())
        {
            if (mDexManager.isXimaoOpen())
            {
                if (XiMaoBTManager.getInstance(mCon).isConnected())
                {
                    Logger.d("device", "\u6545\u4E8B\u673A\u5DF2\u8FDE\u63A5");
                    startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment, null);
                    return;
                } else
                {
                    Logger.d("device", "\u6545\u4E8B\u673A\u672A\u8FDE\u63A5");
                    startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoIntroFragment, null);
                    return;
                }
            } else
            {
                view = new Bundle();
                view.putString("P_Type", MyDeviceManager.DeviceType.ximao.toString());
                startFragment(com/ximalaya/ting/android/fragment/device/FunctionStartFragment, view);
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L7:
        if (!ToolUtil.isFastClick())
        {
            if (mDexManager.isDossOpen())
            {
                view = new Bundle();
                view.putInt(DeviceListFragment.deviceType, MyDeviceManager.DeviceType.doss.ordinal());
                startFragment(com/ximalaya/ting/android/fragment/device/dlna/DeviceListFragment, view);
                return;
            } else
            {
                view = new Bundle();
                view.putString("P_Type", MyDeviceManager.DeviceType.doss.toString());
                startFragment(com/ximalaya/ting/android/fragment/device/FunctionStartFragment, view);
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L5:
        if (!ToolUtil.isFastClick())
        {
            if (mDexManager.isDossOpen())
            {
                view = new Bundle();
                view.putInt(DeviceListFragment.deviceType, MyDeviceManager.DeviceType.shukewifi.ordinal());
                startFragment(com/ximalaya/ting/android/fragment/device/dlna/DeviceListFragment, view);
                return;
            } else
            {
                view = new Bundle();
                view.putString("P_Type", MyDeviceManager.DeviceType.shukewifi.toString());
                startFragment(com/ximalaya/ting/android/fragment/device/FunctionStartFragment, view);
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (!ToolUtil.isFastClick())
        {
            if (mDexManager.isQSuichetingOpen())
            {
                startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheTingIntroFragment, null);
                return;
            } else
            {
                view = new Bundle();
                view.putString("P_Type", MyDeviceManager.DeviceType.Qsuicheting.toString());
                startFragment(com/ximalaya/ting/android/fragment/device/FunctionStartFragment, view);
                return;
            }
        }
        if (true) goto _L1; else goto _L8
_L8:
        if (TextUtils.isEmpty(MyDeviceManager.getInstance(mCon).getMarketURL()))
        {
            showToast("\u656C\u8BF7\u671F\u5F85");
            return;
        } else
        {
            view = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
            view.putExtra("ExtraUrl", MyDeviceManager.getInstance(mCon).getMarketURL());
            getActivity().startActivity(view);
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300ac, viewgroup, false);
        return mContentView;
    }

    public void onResume()
    {
        super.onResume();
        Logger.d("device", "onResume");
        hidePlayButton();
    }

    public void onStop()
    {
        super.onStop();
        showPlayButton();
    }
}
