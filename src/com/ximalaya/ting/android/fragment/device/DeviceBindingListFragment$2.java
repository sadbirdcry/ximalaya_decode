// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.device.dlna.BaseBindableDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            DeviceBindingListFragment

class tAlbum extends MyAsyncTask
{

    AlbumModel tAlbum;
    final DeviceBindingListFragment this$0;
    final long val$albumModelId;
    final int val$channelId;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient String doInBackground(Void avoid[])
    {
        Logger.d(DeviceBindingListFragment.access$100(DeviceBindingListFragment.this), "\u5F00\u59CB\u66F4\u65B0\u6570\u636E");
        avoid = new RequestParams();
        String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
        s = (new StringBuilder()).append(s).append(val$albumModelId).append("/").append(true).append("/").append(1).append("/").append(15).toString();
        avoid = f.a().a(s, avoid, null, null);
        Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
        avoid = JSON.parseObject(avoid);
        if (avoid == null)
        {
            return null;
        }
        if (!"0".equals(avoid.get("ret").toString()))
        {
            break MISSING_BLOCK_LABEL_178;
        }
        tAlbum = (AlbumModel)JSON.parseObject(avoid.getString("album"), com/ximalaya/ting/android/model/album/AlbumModel);
        return null;
        avoid;
        avoid.printStackTrace();
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((String)obj);
    }

    protected void onPostExecute(String s)
    {
        while (!isAdded() || tAlbum == null) 
        {
            return;
        }
        s = DeviceBindingListFragment.access$200(DeviceBindingListFragment.this).getBaseBindableModel();
        s.setAlbums(val$channelId, tAlbum);
        DeviceBindingListFragment.access$200(DeviceBindingListFragment.this).setBaseBindableModel(s);
        DeviceBindingListFragment.access$300(DeviceBindingListFragment.this);
    }

    odel()
    {
        this$0 = final_devicebindinglistfragment;
        val$albumModelId = l;
        val$channelId = I.this;
        super();
        tAlbum = null;
    }
}
