// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.conn;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.ControlConfigure;
import com.ximalaya.ting.android.fragment.device.IConnCallBack;
import com.ximalaya.ting.android.fragment.device.IConnector;
import com.ximalaya.ting.android.library.util.Logger;

public abstract class PwInputFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, IConnCallBack
{

    public static final String NOT_FOUND_WIFI = "\u672A\u627E\u5230WiFi";
    public static final String UNKNOWN_SSID = "<unknown ssid>";
    boolean isCipher;
    private IConnector mConnector;
    private ControlConfigure mControlConfigure;
    protected LinearLayout mDeviceIntro;
    protected TextView mDeviceTypeTv;
    private RelativeLayout mMainLayout;
    public RelativeLayout mOkBtn;
    protected String mPassword;
    private RelativeLayout mProgressLayout;
    EditText mPwEditText;
    ImageView mSwitchBtn;
    TextView mTextRight;
    TextView mTopTv;
    TextView wifiName;

    public PwInputFragment()
    {
        isCipher = true;
    }

    private void changeInputPwShow()
    {
        if (!isCipher)
        {
            mPwEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            return;
        } else
        {
            mPwEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            return;
        }
    }

    public void closeProgress()
    {
        mMainLayout.setVisibility(0);
        mProgressLayout.setVisibility(4);
    }

    protected abstract IConnector getConnector();

    protected void hideSoftInput()
    {
        Context context = mCon;
        Context context1 = mCon;
        ((InputMethodManager)context.getSystemService("input_method")).hideSoftInputFromWindow(mPwEditText.getWindowToken(), 0);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mConnector = getConnector();
        wifiName.setText(mConnector.getDeviceName());
        mPassword = mConnector.getPassword();
        mControlConfigure = mConnector.getControlConfigure();
        if (!TextUtils.isEmpty(mPassword) || mConnector.isNeedPw())
        {
            toConn();
        }
        if (mConnector.getDeviceName().equals("\u672A\u627E\u5230WiFi") || mConnector.getDeviceName().equals("<unknown ssid>"))
        {
            mOkBtn.setPressed(false);
            mOkBtn.setBackgroundColor(mActivity.getApplicationContext().getResources().getColor(0x7f070077));
            return;
        } else
        {
            mOkBtn.setOnClickListener(this);
            return;
        }
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362066: 
            if (isCipher)
            {
                mSwitchBtn.setImageDrawable(getResources().getDrawable(0x7f020356));
                isCipher = false;
            } else
            {
                mSwitchBtn.setImageDrawable(getResources().getDrawable(0x7f020357));
                isCipher = true;
            }
            changeInputPwShow();
            return;

        case 2131362068: 
            mPassword = mPwEditText.getEditableText().toString();
            if (!TextUtils.isEmpty(mPassword))
            {
                toConn();
                return;
            } else
            {
                showToast("\u5BC6\u7801\u4E3A\u7A7A\uFF0C\u8BF7\u91CD\u65B0\u8F93\u5165");
                return;
            }

        case 2131363605: 
            mActivity.onBackPressed();
            return;

        case 2131363606: 
            finish();
            return;
        }
    }

    public void onConnecting(int i)
    {
        Logger.d("wifi", (new StringBuilder()).append("progress:").append(i).toString());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        Log.i("zzw", "onCreateView");
        layoutinflater = (RelativeLayout)layoutinflater.inflate(0x7f03002d, viewgroup, false);
        mMainLayout = (RelativeLayout)layoutinflater.findViewById(0x7f0a0109);
        mProgressLayout = (RelativeLayout)layoutinflater.findViewById(0x7f0a0115);
        mTopTv = (TextView)layoutinflater.findViewById(0x7f0a00ae);
        mTextRight = (TextView)layoutinflater.findViewById(0x7f0a0716);
        mTextRight.setOnClickListener(this);
        mSwitchBtn = (ImageView)layoutinflater.findViewById(0x7f0a0112);
        mPwEditText = (EditText)layoutinflater.findViewById(0x7f0a0113);
        wifiName = (TextView)layoutinflater.findViewById(0x7f0a010e);
        mDeviceTypeTv = (TextView)layoutinflater.findViewById(0x7f0a010b);
        mDeviceIntro = (LinearLayout)layoutinflater.findViewById(0x7f0a010a);
        mTopTv.setText("\u8FDE\u63A5\u7F51\u7EDC");
        mTextRight.setText("\u5207\u6362WiFi");
        mTextRight.setVisibility(0);
        mTextRight.setVisibility(8);
        mSwitchBtn.setOnClickListener(this);
        mOkBtn = (RelativeLayout)layoutinflater.findViewById(0x7f0a0114);
        layoutinflater.findViewById(0x7f0a0715).setOnClickListener(this);
        return layoutinflater;
    }

    public void onFailed()
    {
        mControlConfigure.mFailedType;
        JVM INSTR tableswitch 0 2: default 32
    //                   0 33
    //                   1 41
    //                   2 64;
           goto _L1 _L2 _L3 _L4
_L1:
        return;
_L2:
        showToast("\u5931\u8D25");
        return;
_L3:
        if (mControlConfigure.mConnedFragment != null)
        {
            startFragment(mControlConfigure.mFailedFragment, null);
            return;
        }
          goto _L1
_L4:
        finish();
        return;
    }

    public void onStartConn()
    {
        mPwEditText.setText("******");
        showProgress();
    }

    public void onStop()
    {
        hideSoftInput();
        super.onStop();
    }

    public void onSuccuss()
    {
        switch (mControlConfigure.mConnedType)
        {
        default:
            return;

        case 0: // '\0'
            showToast("\u8FDE\u63A5\u6210\u529F");
            return;

        case 1: // '\001'
            startFragment(mControlConfigure.mConnedFragment, null);
            finish();
            return;

        case 2: // '\002'
            finish();
            break;
        }
    }

    public void showProgress()
    {
        mMainLayout.setVisibility(4);
        mProgressLayout.setVisibility(0);
    }

    protected abstract void toConn();
}
