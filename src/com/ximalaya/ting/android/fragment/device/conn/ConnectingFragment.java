// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.conn;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.IConnCallBack;
import com.ximalaya.ting.android.fragment.device.IConnector;
import com.ximalaya.ting.android.fragment.device.doss.FailConnFragment;
import com.ximalaya.ting.android.library.util.Logger;

public abstract class ConnectingFragment extends BaseActivityLikeFragment
    implements IConnCallBack
{

    private ImageView mBackImg;
    private IConnector mConnector;
    private ImageView mImage1;
    private ImageView mImage2;
    private String mPassword;
    private RelativeLayout mReLayout;
    private TextView topTextView;

    public ConnectingFragment()
    {
    }

    private void toConn()
    {
        Logger.d("WIFI", (new StringBuilder()).append("the password is:").append(mPassword).toString());
        mConnector = getConnector();
        mConnector.connect(mPassword);
    }

    protected abstract IConnector getConnector();

    public void onActivityCreated(final Bundle animationDrawable)
    {
        super.onActivityCreated(animationDrawable);
        topTextView = (TextView)mReLayout.findViewById(0x7f0a00ae);
        topTextView.setText("\u8FDE\u63A5\u4E2D");
        if (getArguments() != null)
        {
            animationDrawable = getArguments();
            if (animationDrawable.containsKey("password"))
            {
                mPassword = animationDrawable.getString("password");
            }
        }
        mImage1 = (ImageView)mReLayout.findViewById(0x7f0a033d);
        mBackImg = (ImageView)mReLayout.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(new _cls1());
        toConn();
        mImage2 = (ImageView)mReLayout.findViewById(0x7f0a033e);
        if (mImage2.getDrawable() instanceof AnimationDrawable)
        {
            animationDrawable = (AnimationDrawable)mImage2.getDrawable();
            mImage2.post(new _cls2());
        }
    }

    public void onConnecting(int i)
    {
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mReLayout = (RelativeLayout)layoutinflater.inflate(0x7f0300ae, null);
        return mReLayout;
    }

    public void onFailed()
    {
        Bundle bundle = new Bundle();
        bundle.putString("device", com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss.toString());
        bundle.putString("password", mPassword);
        removeTopFramentFromManageFragment();
        startFragment(com/ximalaya/ting/android/fragment/device/doss/FailConnFragment, bundle);
    }

    public void onStop()
    {
        super.onStop();
        mConnector.cancelConnect();
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final ConnectingFragment this$0;

        public void onClick(View view)
        {
            onBackPressed.onBackPressed();
        }

        _cls1()
        {
            this$0 = ConnectingFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final ConnectingFragment this$0;
        final AnimationDrawable val$animationDrawable;

        public void run()
        {
            if (animationDrawable != null && !animationDrawable.isRunning())
            {
                animationDrawable.start();
            }
        }

        _cls2()
        {
            this$0 = ConnectingFragment.this;
            animationDrawable = animationdrawable;
            super();
        }
    }

}
