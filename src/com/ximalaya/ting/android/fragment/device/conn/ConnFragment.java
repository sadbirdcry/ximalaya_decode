// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.conn;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.ManageFragment;
import java.lang.ref.SoftReference;
import java.util.List;

public abstract class ConnFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    private ImageView mBackImg;
    public ImageView mDeviceImg;
    private String mPassword;
    private RelativeLayout nextStep;
    private RelativeLayout nextStepView;
    private TextView topTextView;

    public ConnFragment()
    {
    }

    private void initView()
    {
        topTextView = (TextView)nextStepView.findViewById(0x7f0a00ae);
        topTextView.setText("\u8FDB\u5165\u8FDE\u63A5\u6A21\u5F0F");
        nextStep = (RelativeLayout)nextStepView.findViewById(0x7f0a0343);
        nextStep.setOnClickListener(this);
        mBackImg = (ImageView)nextStepView.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(this);
        mDeviceImg = (ImageView)nextStepView.findViewById(0x7f0a0342);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initView();
        if (getArguments() != null)
        {
            bundle = getArguments();
            if (bundle.containsKey("password"))
            {
                mPassword = bundle.getString("password");
            }
        }
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 2: default 32
    //                   2131362627: 33
    //                   2131363611: 138;
           goto _L1 _L2 _L3
_L1:
        return;
_L2:
        view = new Bundle();
        view.putString("password", mPassword);
        toConnectingFragment(view);
        if (getManageFragment() != null)
        {
            int i = getManageFragment().mStacks.size();
            if (i >= 3)
            {
                removeFramentFromManageFragment((Fragment)((SoftReference)getManageFragment().mStacks.get(i - 2)).get());
                removeFramentFromManageFragment((Fragment)((SoftReference)getManageFragment().mStacks.get(i - 3)).get());
                return;
            }
        }
          goto _L1
_L3:
        mActivity.onBackPressed();
        return;
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        nextStepView = (RelativeLayout)layoutinflater.inflate(0x7f0300af, null);
        return nextStepView;
    }

    protected abstract void toConnectingFragment(Bundle bundle);
}
