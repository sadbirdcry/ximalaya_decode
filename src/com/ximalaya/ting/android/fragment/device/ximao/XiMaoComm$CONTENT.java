// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;


// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoComm

public static final class  extends Enum
{

    private static final CONTENT_4 $VALUES[];
    public static final CONTENT_4 CONTENT_1;
    public static final CONTENT_4 CONTENT_2;
    public static final CONTENT_4 CONTENT_3;
    public static final CONTENT_4 CONTENT_4;

    public static String getString( )
    {
        switch (hMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT[.ordinal()])
        {
        default:
            return null;

        case 1: // '\001'
            return "\u6545\u4E8B";

        case 2: // '\002'
            return "\u6027\u683C\u57F9\u517B";

        case 3: // '\003'
            return "\u513F\u6B4C";

        case 4: // '\004'
            return "\u5F55\u97F3";
        }
    }

    public static ordinal valueOf(String s)
    {
        return (ordinal)Enum.valueOf(com/ximalaya/ting/android/fragment/device/ximao/XiMaoComm$CONTENT, s);
    }

    public static ordinal[] values()
    {
        return (ordinal[])$VALUES.clone();
    }

    static 
    {
        CONTENT_1 = new <init>("CONTENT_1", 0);
        CONTENT_2 = new <init>("CONTENT_2", 1);
        CONTENT_3 = new <init>("CONTENT_3", 2);
        CONTENT_4 = new <init>("CONTENT_4", 3);
        $VALUES = (new .VALUES[] {
            CONTENT_1, CONTENT_2, CONTENT_3, CONTENT_4
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
