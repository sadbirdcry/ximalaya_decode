// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.lang.ref.WeakReference;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoContentFragment, XiMaoComm, XiMaoBTManager, XiMaoConnFragment

public class XiMaoIntroFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    static class MyHandler extends Handler
    {

        WeakReference mFragment;

        public void handleMessage(Message message)
        {
            XiMaoIntroFragment ximaointrofragment = (XiMaoIntroFragment)mFragment.get();
            if (ximaointrofragment.isAdded())
            {
                ximaointrofragment.onMessage(message);
            }
        }

        MyHandler(XiMaoIntroFragment ximaointrofragment)
        {
            mFragment = new WeakReference(ximaointrofragment);
        }
    }


    private static final int MSG_BTDEV_CONNECTED = 1;
    private static final int MSG_BTDEV_DISCONNECTED = 2;
    private static final int MSG_BTDEV_FAILED = 6;
    private static final int MSG_BTDEV_PACKET = 5;
    private static final int MSG_BTDEV_RECEIVED = 3;
    private static final int MSG_BTDEV_SENT = 4;
    private static final int MSG_BTDEV_TIMEOUT = 7;
    protected static final String TAG = "ximao";
    private MyHandler MsgHandler;
    private LinearLayout goSetting;
    private boolean isHandled;
    private ImageView mBackImg;
    private TextView mBtnRight;
    private XiMaoBTManager.BTDevConnectionReceiver mConnectionReceiver;
    private RelativeLayout mContentView;
    private LinearLayout mManageXimao;
    private TextView top_tv;

    public XiMaoIntroFragment()
    {
        isHandled = false;
        mConnectionReceiver = new _cls1();
        MsgHandler = new MyHandler(this);
    }

    private void expandHitRect(final View view)
    {
        mContentView.post(new _cls2());
    }

    private void fresh2Connected()
    {
        removeTopFramentFromManageFragment();
        startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment, null);
    }

    private void initView()
    {
        top_tv = (TextView)(TextView)mContentView.findViewById(0x7f0a00ae);
        top_tv.setText("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A");
        android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)top_tv.getLayoutParams();
        layoutparams.addRule(13);
        top_tv.setLayoutParams(layoutparams);
        mBtnRight = (TextView)mContentView.findViewById(0x7f0a071c);
        mBtnRight.setVisibility(0);
        mBtnRight.setOnClickListener(this);
        mBtnRight.setText("\u8BF4\u660E\u4E66");
        goSetting = (LinearLayout)mContentView.findViewById(0x7f0a02f7);
        goSetting.setOnClickListener(this);
        expandHitRect(goSetting);
        mManageXimao = (LinearLayout)mContentView.findViewById(0x7f0a03a2);
        mManageXimao.setOnClickListener(this);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(this);
    }

    private void showWarning()
    {
        mActivity.runOnUiThread(new _cls4());
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initView();
        isHandled = false;
        tryConnect(true);
        isHandled = true;
        hidePlayButton();
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 4: default 48
    //                   2131362551: 77
    //                   2131362722: 49
    //                   2131363611: 61
    //                   2131363612: 69;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        return;
_L3:
        if (!ToolUtil.isFastClick())
        {
            tryConnect(false);
            return;
        }
          goto _L1
_L4:
        getActivity().onBackPressed();
        return;
_L5:
        XiMaoComm.goIntroduction(getActivity());
        return;
_L2:
        getActivity().startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
        return;
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300e5, viewgroup, false);
        return mContentView;
    }

    public void onMessage(Message message)
    {
        switch (message.what)
        {
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        default:
            return;

        case 1: // '\001'
            fresh2Connected();
            break;
        }
    }

    public void onResume()
    {
        super.onResume();
        Logger.d("ximao", "XiMaoIntroFragment onResume");
        XiMaoBTManager.getInstance(mCon).setmBTDevConnectionReceiver(mConnectionReceiver);
        if (!isHandled)
        {
            if (XiMaoBTManager.getInstance(mCon).isConnected())
            {
                removeTopFramentFromManageFragment();
                startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment, null);
            }
        } else
        {
            isHandled = false;
        }
        hidePlayButton();
    }

    public void onStop()
    {
        super.onStop();
        Logger.d("ximao", "XiMaoIntroFragment onStop");
        XiMaoComm.removeBTDevConnectionReceiver(mCon);
    }

    public void tryConnect(final boolean isSlient)
    {
        if (!XiMaoBTManager.getInstance(mCon).isConnected()) goto _L2; else goto _L1
_L1:
        removeTopFramentFromManageFragment();
        startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment, null);
_L4:
        return;
_L2:
        int i;
        i = android.os.Build.VERSION.SDK_INT;
        if (BluetoothAdapter.getDefaultAdapter() != null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!isSlient)
        {
            showWarning();
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (BluetoothAdapter.getDefaultAdapter().isEnabled())
        {
            break; /* Loop/switch isn't completed */
        }
        if (!isSlient)
        {
            showWarning();
            return;
        }
        if (true) goto _L4; else goto _L5
_L5:
        BluetoothDevice bluetoothdevice = MyDeviceManager.getInstance(getActivity()).getConnectedDevice(getActivity());
        if (bluetoothdevice == null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (XiMaoBTManager.isXimao(bluetoothdevice))
        {
            XiMaoBTManager.getInstance(mCon).device = bluetoothdevice;
            removeTopFramentFromManageFragment();
            startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoConnFragment, null);
            return;
        }
        if (!isSlient)
        {
            showWarning();
            return;
        }
        if (true) goto _L4; else goto _L6
_L6:
        if (i >= 11)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!isSlient)
        {
            showWarning();
            return;
        }
        if (true) goto _L4; else goto _L7
_L7:
        if (BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(2) == 2)
        {
            BluetoothAdapter.getDefaultAdapter().getProfileProxy(MyApplication.b(), new _cls3(), 2);
            return;
        }
        if (!isSlient)
        {
            showWarning();
            return;
        }
        if (true) goto _L4; else goto _L8
_L8:
    }




    private class _cls1
        implements XiMaoBTManager.BTDevConnectionReceiver
    {

        final XiMaoIntroFragment this$0;

        public void onConnectFailed()
        {
            MsgHandler.obtainMessage(6).sendToTarget();
        }

        public void onConnected()
        {
            MsgHandler.obtainMessage(1).sendToTarget();
        }

        public void onDisconnected()
        {
            MsgHandler.obtainMessage(2).sendToTarget();
        }

        public void onPacket(byte abyte0[], int i)
        {
            MsgHandler.obtainMessage(5).sendToTarget();
        }

        public void onReceived(byte abyte0[], int i)
        {
            MsgHandler.obtainMessage(3, 0, i, abyte0).sendToTarget();
        }

        public void onSent(byte abyte0[], int i)
        {
            MsgHandler.obtainMessage(4, 0, i, abyte0).sendToTarget();
        }

        public void onTimeout()
        {
            MsgHandler.obtainMessage(7).sendToTarget();
        }

        _cls1()
        {
            this$0 = XiMaoIntroFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final XiMaoIntroFragment this$0;
        final View val$view;

        public void run()
        {
            if (isAdded())
            {
                Object obj = new Rect();
                View view1 = view;
                view1.getHitRect(((Rect) (obj)));
                int i = ToolUtil.dp2px(getActivity(), 10F);
                obj.right = ((Rect) (obj)).right + i;
                obj.left = ((Rect) (obj)).left - i;
                obj = new TouchDelegate(((Rect) (obj)), view1);
                if (android/view/View.isInstance(view1.getParent()))
                {
                    ((View)view1.getParent()).setTouchDelegate(((TouchDelegate) (obj)));
                }
            }
        }

        _cls2()
        {
            this$0 = XiMaoIntroFragment.this;
            view = view1;
            super();
        }
    }



    private class _cls3
        implements android.bluetooth.BluetoothProfile.ServiceListener
    {

        final XiMaoIntroFragment this$0;
        final boolean val$isSlient;

        public void onServiceConnected(int i, BluetoothProfile bluetoothprofile)
        {
            List list;
            list = bluetoothprofile.getConnectedDevices();
            BluetoothAdapter.getDefaultAdapter().closeProfileProxy(i, bluetoothprofile);
            if (list == null || list.size() == 0) goto _L2; else goto _L1
_L1:
            bluetoothprofile = (BluetoothDevice)list.get(0);
            if (!XiMaoBTManager.isXimao(bluetoothprofile)) goto _L4; else goto _L3
_L3:
            XiMaoBTManager.getInstance(mCon).device = bluetoothprofile;
            if (!XiMaoBTManager.getInstance(mCon).isConnected()) goto _L6; else goto _L5
_L5:
            removeTopFramentFromManageFragment();
            startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment, null);
_L8:
            return;
_L6:
            removeTopFramentFromManageFragment();
            startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoConnFragment, null);
            return;
_L4:
            if (!isSlient)
            {
                showWarning();
                return;
            }
            continue; /* Loop/switch isn't completed */
_L2:
            if (!isSlient)
            {
                showWarning();
                return;
            }
            if (true) goto _L8; else goto _L7
_L7:
        }

        public void onServiceDisconnected(int i)
        {
        }

        _cls3()
        {
            this$0 = XiMaoIntroFragment.this;
            isSlient = flag;
            super();
        }
    }

}
