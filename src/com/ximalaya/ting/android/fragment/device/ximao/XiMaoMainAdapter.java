// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.device.bluetooth.ximao.XiMaoDownloadModule;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoBTManager

public class XiMaoMainAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        public TextView name;
        public TextView number;

        private ViewHolder()
        {
        }

    }


    public static List names = new ArrayList();
    private Context mContext;
    private LayoutInflater mInflater;

    public XiMaoMainAdapter(Context context)
    {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        names = new ArrayList();
        names.add("\u6545\u4E8B");
        names.add("\u6027\u683C\u57F9\u517B");
        names.add("\u513F\u6B4C");
        names.add("\u5F55\u97F3");
    }

    public int getCount()
    {
        return names.size();
    }

    public Object getItem(int i)
    {
        return names.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        View view1;
        String s;
        if (view == null)
        {
            view1 = mInflater.inflate(0x7f030148, viewgroup, false);
            viewgroup = new ViewHolder();
            viewgroup.name = (TextView)view1.findViewById(0x7f0a04fb);
            viewgroup.number = (TextView)view1.findViewById(0x7f0a04fc);
            view1.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
            view1 = view;
        }
        s = (String)names.get(i);
        view = "\u6B63\u5728\u67E5\u8BE2\u6587\u4EF6\u6570\u91CF";
        if (i != 0 || XiMaoBTManager.getInstance(mContext).mNumContent1 == -1) goto _L2; else goto _L1
_L1:
        view = (new StringBuilder()).append("").append(XiMaoBTManager.getInstance(mContext).mNumContent1).append("\u9996").toString();
_L4:
        ((ViewHolder) (viewgroup)).number.setText(view);
        ((ViewHolder) (viewgroup)).name.setText(s);
        return view1;
_L2:
        if (i == 1 && XiMaoBTManager.getInstance(mContext).mNumContent2 != -1)
        {
            view = (new StringBuilder()).append("").append(XiMaoBTManager.getInstance(mContext).mNumContent2).append("\u9996").toString();
        } else
        if (i == 2 && XiMaoBTManager.getInstance(mContext).mNumContent3 != -1)
        {
            view = (new StringBuilder()).append("").append(XiMaoBTManager.getInstance(mContext).mNumContent3).append("\u9996").toString();
        } else
        if (i == 3 && XiMaoBTManager.getInstance(mContext).mNumContent4 != -1)
        {
            view = (new StringBuilder()).append("").append(XiMaoBTManager.getInstance(mContext).mNumContent4).append("\u9996").toString();
        } else
        if (i == 4)
        {
            view = (new StringBuilder()).append("\u6B63\u5728\u6DFB\u52A0... 1/").append(XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks().size()).toString();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void toggleDownloadStatus()
    {
        if (XiMaoBTManager.getInstance(mContext).getDownloadModule() != null && XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks() != null && XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks().size() > 0)
        {
            if (!names.contains("\u6DFB\u52A0\u5217\u8868"))
            {
                names.add("\u6DFB\u52A0\u5217\u8868");
            }
        } else
        {
            names.remove("\u6DFB\u52A0\u5217\u8868");
        }
        notifyDataSetChanged();
    }

}
