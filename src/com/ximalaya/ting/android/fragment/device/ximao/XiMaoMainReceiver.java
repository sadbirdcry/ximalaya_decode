// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.os.Message;
import android.util.Log;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoMainHandler

public class XiMaoMainReceiver
    implements XiMaoBTManager.BTDevConnectionReceiver
{

    private XiMaoMainHandler myHandler;

    public XiMaoMainReceiver(XiMaoMainHandler ximaomainhandler)
    {
        myHandler = null;
        myHandler = ximaomainhandler;
    }

    public void onConnectFailed()
    {
    }

    public void onConnected()
    {
        myHandler.obtainMessage(1).sendToTarget();
    }

    public void onDisconnected()
    {
        myHandler.obtainMessage(2).sendToTarget();
    }

    public void onPacket(byte abyte0[], int i)
    {
        Log.d("ximao", "\u6536\u5230Packet");
        myHandler.obtainMessage(5, 0, i, abyte0).sendToTarget();
    }

    public void onReceived(byte abyte0[], int i)
    {
        myHandler.obtainMessage(3, 0, i, abyte0).sendToTarget();
    }

    public void onSent(byte abyte0[], int i)
    {
        myHandler.obtainMessage(4, 0, i, abyte0).sendToTarget();
    }

    public void onTimeout()
    {
    }
}
