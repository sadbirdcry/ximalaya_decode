// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.os.Bundle;
import com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download.BaseDownloadAlbumListFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download.BaseDownloadSoundsListForAlbumFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download.BaseSoundsDownloadForAlbumAdapter;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoSoundsDownloadForAlbumAdapter, XiMaoDownloadSoundsListForAlbumFragment

public class XiMaoDownloadAlbumListFragment extends BaseDownloadAlbumListFragment
{

    public XiMaoDownloadAlbumListFragment()
    {
    }

    public BaseSoundsDownloadForAlbumAdapter getAdapter(List list)
    {
        if (soundsDownloadAdapter == null)
        {
            soundsDownloadAdapter = new XiMaoSoundsDownloadForAlbumAdapter(getActivity(), list);
        }
        return soundsDownloadAdapter;
    }

    public void toSoundListFragment(long l)
    {
        Bundle bundle = new Bundle();
        bundle.putLong(BaseDownloadSoundsListForAlbumFragment.ALBUM_ID, l);
        startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoDownloadSoundsListForAlbumFragment, bundle);
    }
}
