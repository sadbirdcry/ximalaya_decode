// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download.BaseDownloadedSoundListAdapter;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.download.DownloadTask;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoMainAdapter

public class XiMaoDownloadedSoundListAdapter extends BaseDownloadedSoundListAdapter
{

    private MenuDialog mDeviceMenuDialog;

    public XiMaoDownloadedSoundListAdapter(Activity activity, List list)
    {
        super(activity, list);
    }

    private void showDirectionChoose(final DownloadTask task)
    {
        ArrayList arraylist = new ArrayList();
        for (int i = 0; i < XiMaoMainAdapter.names.size(); i++)
        {
            if (!((String)XiMaoMainAdapter.names.get(i)).equals("\u6DFB\u52A0\u5217\u8868"))
            {
                arraylist.add(XiMaoMainAdapter.names.get(i));
            }
        }

        if (mDeviceMenuDialog == null)
        {
            mDeviceMenuDialog = new MenuDialog(MyApplication.a(), arraylist, a.e, new _cls1(), 1);
        } else
        {
            mDeviceMenuDialog.setSelections(arraylist);
        }
        mDeviceMenuDialog.setHeaderTitle("\u6DFB\u52A0\u81F3\u6587\u4EF6\u5939");
        mDeviceMenuDialog.show();
    }

    public void onDownloadClick(DownloadTask downloadtask)
    {
        showDirectionChoose(downloadtask);
    }

    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final XiMaoDownloadedSoundListAdapter this$0;
        final DownloadTask val$task;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            XiMaoBTManager.getInstance(mContext).getDownloadModule().download(task, i + 1);
        }

        _cls1()
        {
            this$0 = XiMaoDownloadedSoundListAdapter.this;
            task = downloadtask;
            super();
        }
    }

}
