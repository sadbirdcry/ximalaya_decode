// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.ximao.XiMaoDownloadModule;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            KeyInfo, XiMaoComm, XiMaoVoiceManager, XiMaoMainHandler, 
//            XiMaoMainReceiver, XiMaoWavManager

public class XiMaoBTManager
{
    public static interface BTDevConnectionReceiver
    {

        public abstract void onConnectFailed();

        public abstract void onConnected();

        public abstract void onDisconnected();

        public abstract void onPacket(byte abyte0[], int i);

        public abstract void onReceived(byte abyte0[], int i);

        public abstract void onSent(byte abyte0[], int i);

        public abstract void onTimeout();
    }

    public static interface BTDevDiscoveryReceiver
    {

        public abstract void onDiscoveryCompleted();

        public abstract void onNewDeviceFound(BluetoothDevice bluetoothdevice);
    }

    public class BTDevSppConnThread extends Thread
    {

        private UUID SPP_UUID;
        byte arrayOfByte2[];
        byte arrayOfByte3[];
        int desPos;
        private InputStream mInStream;
        private OutputStream mOutStream;
        private BluetoothSocket mSocket;
        boolean mThreadFinished;
        int mTryTimes;
        int offset;
        final XiMaoBTManager this$0;
        private BluetoothSocket tmp;

        private boolean checkAudioFinish(byte abyte0[], int i)
        {
            return abyte0[i - 1] == 25 && abyte0[i - 2] == 0 && abyte0[i - 3] == 0 && abyte0[i - 4] == 10 && abyte0[i - 5] == 15;
        }

        private void cleanArray()
        {
            arrayOfByte2 = new byte[1024];
            desPos = 0;
            offset = 0;
            arrayOfByte3 = new byte[1024];
        }

        private void cleanup()
        {
            if (mSocket != null)
            {
                try
                {
                    mNumContent3 = -1;
                    mNumContent1 = -1;
                    mNumContent2 = -1;
                    mNumContent4 = -1;
                    if (mOutStream != null)
                    {
                        mOutStream.close();
                        mOutStream = null;
                    }
                    if (mInStream != null)
                    {
                        mInStream.close();
                        mInStream = null;
                    }
                    mSocket.close();
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
                mSocket = null;
            }
        }

        private boolean createSocket()
        {
            if (device != null)
            {
                break MISSING_BLOCK_LABEL_19;
            }
            Logger.d("ximao", "\u83B7\u53D6\u84DD\u7259\u8BBE\u5907\u5931\u8D25");
            return false;
            if (android.os.Build.VERSION.SDK_INT >= 11) goto _L2; else goto _L1
_L1:
            tmp = device.createRfcommSocketToServiceRecord(SPP_UUID);
_L4:
            mSocket = tmp;
            mSocket.connect();
            mConnStatus = XiMaoComm.STATE.CONNECTING;
            return true;
_L2:
            try
            {
                tmp = device.createInsecureRfcommSocketToServiceRecord(SPP_UUID);
            }
            catch (Exception exception)
            {
                Logger.e("ximao", (new StringBuilder()).append("1st try:").append(exception.toString()).toString());
                exception.printStackTrace();
                mConnStatus = XiMaoComm.STATE.FAILED;
                return false;
            }
            if (true) goto _L4; else goto _L3
_L3:
            Exception exception1;
            exception1;
            showToast((new StringBuilder()).append("pos 1").append(exception1.toString()).toString());
            if (mSocket != null)
            {
                mSocket.close();
                tmp.close();
                Thread.sleep(2000L);
                mSocket = null;
                tmp = null;
            }
            mSocket = null;
            tmp = (BluetoothSocket)device.getClass().getMethod("createRfcommSocket", new Class[] {
                Integer.TYPE
            }).invoke(device, new Object[] {
                Integer.valueOf(1)
            });
            mSocket = tmp;
            mSocket.connect();
            mConnStatus = XiMaoComm.STATE.CONNECTING;
            return true;
            Exception exception2;
            exception2;
            showToast((new StringBuilder()).append("pos 3").append(exception2.toString()).toString());
            if (mSocket != null)
            {
                mSocket.close();
                tmp.close();
                Thread.sleep(2000L);
                mSocket = null;
                tmp = null;
            }
            mSocket = null;
            mConnStatus = XiMaoComm.STATE.FAILED;
            return false;
            exception2;
            Logger.e("ximao", (new StringBuilder()).append("2nd try").append(exception1.toString()).toString());
            showToast((new StringBuilder()).append("2nd try").append(exception1.toString()).toString());
            mConnStatus = XiMaoComm.STATE.FAILED;
            return false;
        }

        private void distributePacket(byte abyte0[], int i)
        {
            if (15 != abyte0[0]) goto _L2; else goto _L1
_L1:
            if (8 != abyte0[1]) goto _L4; else goto _L3
_L3:
            showToast("\u5F00\u59CB\u4E0A\u4F20\u5F55\u97F3");
            Logger.d("ximao", "\u5F00\u59CB\u4F20\u8F93\u667A\u80FD\u8BED\u97F3\u6D41");
            audioTimer = new Timer();
            class _cls2 extends TimerTask
            {

                final BTDevSppConnThread this$1;

                public void run()
                {
                    if (isAudio)
                    {
                        Logger.e("ximao", "\u5F55\u97F3\u6587\u4EF6\u4E0A\u4F20\u8D85\u65F6\uFF0C\u6B63\u5728\u91CD\u7F6E");
                        showToast("\u5F55\u97F3\u6587\u4EF6\u4E0A\u4F20\u8D85\u65F6\uFF0C\u6B63\u5728\u91CD\u7F6E");
                        if (mSppConnThread != null)
                        {
                            mSppConnThread.cleanArray();
                            isAudio = false;
                        }
                    }
                }

                _cls2()
                {
                    this$1 = BTDevSppConnThread.this;
                    super();
                }
            }

            audioTask = new _cls2();
            audioTimer.schedule(audioTask, 25000L);
            while (mXimaoVoiceManager == null) 
            {
                mXimaoVoiceManager = XiMaoVoiceManager.getInstance(mCon);
                mXimaoVoiceManager.initVoice();
            }
            mXimaoVoiceManager.clearResult();
            class _cls3
                implements Runnable
            {

                final BTDevSppConnThread this$1;

                public void run()
                {
                    Logger.d("TAG", "\u6682\u505C");
                    LocalMediaService.getInstance().pause();
                }

                _cls3()
                {
                    this$1 = BTDevSppConnThread.this;
                    super();
                }
            }

            if (MyApplication.a() != null)
            {
                MyApplication.a().runOnUiThread(new _cls3());
            }
            isAudio = true;
            XiMaoWavManager.toalmydatasize = 0;
            XiMaoWavManager.databuff = new ByteArrayOutputStream(0x927c00);
_L6:
            return;
_L4:
            if (10 == abyte0[1])
            {
                isAudio = false;
                return;
            }
            continue; /* Loop/switch isn't completed */
_L2:
            if (18 == abyte0[0])
            {
                if (abyte0[1] == 255 && lastPacket != null)
                {
                    mSppConnThread.write(lastPacket, lastPacket.length);
                    return;
                }
            } else
            {
                if (abyte0[0] == 13 || abyte0[0] == 17)
                {
                    if (mBTDevMainConnectionReceiver != null)
                    {
                        mBTDevMainConnectionReceiver.onPacket(abyte0, i);
                    } else
                    {
                        if (MainTabActivity2.isMainTabActivityAvaliable())
                        {
                            registerXiMaoMainReceiver(MainTabActivity2.mainTabActivity);
                        }
                        Logger.d("ximao", "MainReceiver\u4E3A\u7A7A");
                    }
                    if (mConnectionReceiver != null)
                    {
                        mConnectionReceiver.onPacket(abyte0, i);
                        return;
                    } else
                    {
                        Logger.d("ximao", "ConnectionReceiver\u4E3A\u7A7A1");
                        return;
                    }
                }
                if (abyte0[0] == 14 || abyte0[0] == 9)
                {
                    if (mBTDevMainConnectionReceiver != null)
                    {
                        mBTDevMainConnectionReceiver.onPacket(abyte0, i);
                        return;
                    }
                    if (MainTabActivity2.isMainTabActivityAvaliable())
                    {
                        registerXiMaoMainReceiver(MainTabActivity2.mainTabActivity);
                    }
                    Logger.d("ximao", "MainReceiver\u4E3A\u7A7A");
                    return;
                }
                if (XiMaoComm.isDownloadResponse(abyte0))
                {
                    getDownloadModule().onPacket(abyte0);
                    return;
                }
                if (mConnectionReceiver != null)
                {
                    mConnectionReceiver.onPacket(abyte0, i);
                    return;
                } else
                {
                    Logger.d("ximao", "ConnectionReceiver\u4E3A\u7A7A2");
                    return;
                }
            }
            if (true) goto _L6; else goto _L5
_L5:
        }

        private void write(byte abyte0[], int i)
        {
            XiMaoComm.printPacket("write data", abyte0, i);
            if (mOutStream != null)
            {
                try
                {
                    mOutStream.write(abyte0, 0, i);
                    lastPacket = new byte[i];
                    System.arraycopy(abyte0, 0, lastPacket, 0, i);
                    if (mIsBTDebug && mConnectionReceiver != null)
                    {
                        byte abyte1[] = new byte[i];
                        System.arraycopy(abyte0, 0, abyte1, 0, i);
                        mConnectionReceiver.onSent(abyte1, i);
                    }
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (byte abyte0[])
                {
                    Logger.d("ximao", "write error");
                }
                mThreadFinished = true;
                Logger.d("ximao", "write\u5B58\u5728\u5F02\u5E38\uFF0C\u6B63\u5728\u9000\u51FA");
                abyte0.printStackTrace();
                onDisconnected();
                return;
            } else
            {
                Logger.d("ximao", "\u672A\u8FDE\u63A5");
                return;
            }
        }

        public void cancel()
        {
            cleanup();
            mThreadFinished = true;
        }

        public BluetoothSocket getSocket()
        {
            return mSocket;
        }

        public void run()
        {
            mTryTimes = 0;
            if (mSocket != null) goto _L2; else goto _L1
_L1:
            mConnStatus = XiMaoComm.STATE.CONNECTING;
_L5:
            if (mTryTimes <= 3 && !createSocket()) goto _L3; else goto _L2
_L2:
            if (mConnStatus == XiMaoComm.STATE.FAILED)
            {
                onConnectFailed();
                return;
            }
            break; /* Loop/switch isn't completed */
_L3:
            mTryTimes = mTryTimes + 1;
            if (true) goto _L5; else goto _L4
_L4:
            byte abyte3[];
            Logger.d("ximao", (new StringBuilder()).append("create socket").append(mSocket.hashCode()).toString());
            if (mSocket == null)
            {
                mSocket = null;
                Logger.d("ximao", "mSocket\u4E3A\u7A7A\uFF0C\u521B\u5EFA\u5931\u8D25");
                onConnectFailed();
                return;
            }
            byte abyte0[];
            Exception exception;
            try
            {
                mOutStream = mSocket.getOutputStream();
                mInStream = mSocket.getInputStream();
                Logger.d("ximao", (new StringBuilder()).append("create outstream").append(mOutStream.hashCode()).toString());
                Logger.d("ximao", (new StringBuilder()).append("create instream").append(mInStream.hashCode()).toString());
            }
            catch (Exception exception1)
            {
                mSocket = null;
                Logger.d("ximao", "Stream\u83B7\u53D6\u5931\u8D25\uFF0C\u521B\u5EFA\u5931\u8D25");
                onConnectFailed();
            }
            onConnected();
            abyte0 = new byte[1024];
_L14:
            if (mThreadFinished) goto _L7; else goto _L6
_L6:
            abyte3 = new byte[1024];
            if (!mThreadFinished) goto _L8; else goto _L7
_L7:
            mThreadFinished = true;
            return;
_L8:
            String s;
            int i;
            int j;
            try
            {
                Logger.d("ximao", (new StringBuilder()).append("\u7B49\u5F85\u8BFB\u53D6\u6570\u636E:").append(offset).toString());
                mInStream = mSocket.getInputStream();
                i = mInStream.read(abyte3, offset, 1024 - offset);
            }
            // Misplaced declaration of an exception variable
            catch (Exception exception)
            {
                mThreadFinished = true;
                Logger.d("ximao", "SPP\u8FDE\u63A5\u5B58\u5728\u5F02\u5E38\uFF0C\u6B63\u5728\u9000\u51FA");
                exception.printStackTrace();
                onDisconnected();
                return;
            }
            s = "";
            j = 0;
_L10:
            if (j >= i)
            {
                break; /* Loop/switch isn't completed */
            }
            s = (new StringBuilder()).append(s).append(String.valueOf(Integer.toHexString(abyte3[j] & 0xff | 0xffffff00).substring(6))).append(" ").toString();
            j++;
            if (true) goto _L10; else goto _L9
_L9:
            Logger.d("ximao", (new StringBuilder()).append("\u8BFB\u5230\u6570\u636E").append(s).toString());
            if (mThreadFinished) goto _L7; else goto _L11
_L11:
            int k = i;
            if (isAudio)
            {
                break MISSING_BLOCK_LABEL_589;
            }
_L13:
            j = i;
            if (XiMaoComm.isValidHead(abyte3[0]))
            {
                break; /* Loop/switch isn't completed */
            }
            j = i;
            if (i <= 0)
            {
                break; /* Loop/switch isn't completed */
            }
            Logger.d("ximao", "\u8BE5\u6570\u636E\u4E0D\u662F\u5408\u6CD5\u5934");
            Logger.d("ximao", "\u4E0D\u662F\u5408\u6CD5\u5934\uFF0C\u5904\u7406\u4E2D.");
            byte abyte1[] = new byte[1024];
            System.arraycopy(abyte3, 1, abyte1, 0, i - 1);
            System.arraycopy(abyte1, 0, abyte3, 0, i - 1);
            j = i - 1;
            i = j;
            if (j <= 100)
            {
                continue; /* Loop/switch isn't completed */
            }
            cleanArray();
            break; /* Loop/switch isn't completed */
            if (true) goto _L13; else goto _L12
_L12:
            k = j;
            if (XiMaoComm.isValidHead(abyte3[0]))
            {
                break MISSING_BLOCK_LABEL_589;
            }
            k = j;
            if (j <= 100)
            {
                break MISSING_BLOCK_LABEL_589;
            }
            cleanArray();
              goto _L14
            if (k > 0)
            {
                break MISSING_BLOCK_LABEL_605;
            }
            Logger.d("ximao", "finish");
              goto _L14
            if (mBTDevMainConnectionReceiver == null)
            {
                registerXiMaoMainReceiver(MyApplication.a());
            }
            if (mIsBTDebug)
            {
                byte abyte2[] = new byte[k];
                System.arraycopy(abyte3, offset, abyte2, 0, k);
                mConnectionReceiver.onReceived(abyte2, k);
            }
            if (!isAudio)
            {
                break MISSING_BLOCK_LABEL_832;
            }
            Logger.d("ximao", "audio data");
            XiMaoWavManager.writeData(abyte3, k);
            if (!checkAudioFinish(XiMaoWavManager.databuff.toByteArray(), XiMaoWavManager.toalmydatasize)) goto _L14; else goto _L15
_L15:
            if (audioTimer != null)
            {
                audioTimer.cancel();
            }
            isAudio = false;
            XiMaoWavManager.finishAudio(mCon);
            if (mXimaoVoiceManager == null)
            {
                mXimaoVoiceManager = XiMaoVoiceManager.getInstance(mCon);
                class _cls1
                    implements Runnable
                {

                    final BTDevSppConnThread this$1;

                    public void run()
                    {
                        mXimaoVoiceManager.initVoice();
                    }

                _cls1()
                {
                    this$1 = BTDevSppConnThread.this;
                    super();
                }
                }

                MyApplication.a().runOnUiThread(new _cls1());
            }
            if (XiMaoWavManager.toalmydatasize <= 98)
            {
                break MISSING_BLOCK_LABEL_819;
            }
            mXimaoVoiceManager.writeaudio(XiMaoWavManager.buffers);
            XiMaoWavManager.buffers = null;
_L16:
            XiMaoWavManager.cleanUp();
              goto _L14
            CustomToast.showToast(MyApplication.a(), "\u667A\u80FD\u8BED\u97F3\u6570\u636E\u4E0A\u4F20\u5931\u8D25", 0);
              goto _L16
label0:
            {
                if (desPos + k < 1024)
                {
                    break label0;
                }
                showToast("pos1:\u6570\u636E\u683C\u5F0F\u4E0D\u6B63\u786E");
                cleanArray();
            }
              goto _L14
            System.arraycopy(abyte3, 0, arrayOfByte2, desPos, k);
            desPos = k + desPos;
            i = isFinish(arrayOfByte2, desPos);
            if (i == -1) goto _L14; else goto _L17
_L17:
            if (i >= 0 && i <= 1024)
            {
                break MISSING_BLOCK_LABEL_937;
            }
            showToast("pos2:\u6570\u636E\u683C\u5F0F\u4E0D\u6B63\u786E");
            cleanArray();
              goto _L14
            if (XiMaoComm.check(arrayOfByte2, i))
            {
                distributePacket(arrayOfByte2, i);
            }
            if (i >= desPos)
            {
                break MISSING_BLOCK_LABEL_1226;
            }
            j = desPos - i;
            System.arraycopy(arrayOfByte2, i, arrayOfByte3, 0, j);
            k = isFinish(arrayOfByte3, j);
            if (k != -1) goto _L19; else goto _L18
_L18:
            arrayOfByte2 = new byte[1024];
            System.arraycopy(arrayOfByte3, 0, arrayOfByte2, 0, j);
            desPos = j;
_L24:
            offset = 0;
            arrayOfByte3 = new byte[1024];
              goto _L14
_L19:
            i = k;
            if (k >= -1)
            {
                break MISSING_BLOCK_LABEL_1072;
            }
            cleanArray();
              goto _L14
_L21:
            if (i == -1 || i >= j)
            {
                break; /* Loop/switch isn't completed */
            }
            distributePacket(arrayOfByte3, i);
            j -= i;
            arrayOfByte2 = new byte[1024];
            System.arraycopy(arrayOfByte3, i, arrayOfByte2, 0, j);
            arrayOfByte3 = new byte[1024];
            System.arraycopy(arrayOfByte2, 0, arrayOfByte3, 0, j);
            i = isFinish(arrayOfByte3, j);
            if (true) goto _L21; else goto _L20
_L20:
            if (i != j) goto _L23; else goto _L22
_L22:
            distributePacket(arrayOfByte3, i);
            cleanArray();
              goto _L24
_L23:
            if (i != -1) goto _L24; else goto _L25
_L25:
            i = j - i;
            arrayOfByte2 = new byte[1024];
            System.arraycopy(arrayOfByte3, 0, arrayOfByte2, 0, i);
            desPos = i;
              goto _L24
            arrayOfByte2 = new byte[1024];
            desPos = 0;
              goto _L24
        }






        public BTDevSppConnThread(String s)
        {
            this$0 = XiMaoBTManager.this;
            super();
            SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
            mSocket = null;
            tmp = null;
            mOutStream = null;
            mInStream = null;
            offset = 0;
            desPos = 0;
            arrayOfByte2 = new byte[1024];
            arrayOfByte3 = new byte[1024];
            mThreadFinished = false;
            mTryTimes = 0;
            if (getAdapter() == null)
            {
                return;
            } else
            {
                device = getAdapter().getRemoteDevice(s);
                return;
            }
        }
    }


    public static final String P_XIMAO_BIND_KEY_1 = "P_XIMAO_BIND_KEY_1";
    public static final String P_XIMAO_BIND_KEY_2 = "P_XIMAO_BIND_KEY_2";
    public static final String P_XIMAO_BIND_KEY_3 = "P_XIMAO_BIND_KEY_3";
    public static final int SPP_BUFFER_LENGTH = 1024;
    private static final String TAG = "ximao";
    private static XiMaoBTManager instance;
    public TimerTask audioTask;
    public Timer audioTimer;
    public BluetoothDevice device;
    private boolean isAudio;
    private boolean isSearchContent;
    public ArrayList itemsList;
    public KeyInfo keyInfo1;
    public KeyInfo keyInfo2;
    public KeyInfo keyInfo3;
    public KeyInfo keyInfoTemp;
    private byte lastPacket[];
    private BluetoothAdapter mAdapter;
    private BTDevDiscoveryReceiver mBTDevDiscoveryReceiver;
    private BTDevConnectionReceiver mBTDevMainConnectionReceiver;
    private Context mCon;
    private XiMaoComm.STATE mConnStatus;
    private BTDevConnectionReceiver mConnectionReceiver;
    private XiMaoDownloadModule mDownloadModule;
    private boolean mIsBTDebug;
    public int mNumContent1;
    public int mNumContent2;
    public int mNumContent3;
    public int mNumContent4;
    private BTDevSppConnThread mSppConnThread;
    private double mStorage;
    private XiMaoVoiceManager mXimaoVoiceManager;
    private XiMaoMainHandler mainHandler;
    private XiMaoMainReceiver mainReceiver;
    public TimerTask task;
    public Timer timer;

    private XiMaoBTManager(Context context)
    {
        mBTDevDiscoveryReceiver = null;
        mConnectionReceiver = null;
        mBTDevMainConnectionReceiver = null;
        mSppConnThread = null;
        mConnStatus = XiMaoComm.STATE.START;
        mIsBTDebug = false;
        isAudio = false;
        isSearchContent = false;
        lastPacket = null;
        keyInfo1 = new KeyInfo(1, null, -1L, 0);
        keyInfo2 = new KeyInfo(2, null, -1L, 0);
        keyInfo3 = new KeyInfo(3, null, -1L, 0);
        keyInfoTemp = null;
        mStorage = 0.0D;
        mNumContent3 = -1;
        mNumContent1 = -1;
        mNumContent2 = -1;
        mNumContent4 = -1;
        device = null;
        mCon = null;
        mDownloadModule = new XiMaoDownloadModule(context);
    }

    public static XiMaoBTManager getInstance(Context context)
    {
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/fragment/device/ximao/XiMaoBTManager;
        JVM INSTR monitorenter ;
        if (instance == null)
        {
            instance = new XiMaoBTManager(context);
        }
        com/ximalaya/ting/android/fragment/device/ximao/XiMaoBTManager;
        JVM INSTR monitorexit ;
_L2:
        if (context != null)
        {
            instance.mCon = context;
        }
        return instance;
        context;
        com/ximalaya/ting/android/fragment/device/ximao/XiMaoBTManager;
        JVM INSTR monitorexit ;
        throw context;
    }

    private int isFinish(byte abyte0[], int i)
    {
        XiMaoComm.printPacket("checkFinish", abyte0, i);
        if (i < 5)
        {
            return -1;
        }
        int j = abyte0[2] * 256 + abyte0[3];
        if (i >= j + 5)
        {
            Logger.d("ximao", "finish");
            return j + 5;
        } else
        {
            Logger.d("ximao", "not finish");
            return -1;
        }
    }

    public static boolean isXimao(BluetoothDevice bluetoothdevice)
    {
        bluetoothdevice = bluetoothdevice.getAddress();
        return bluetoothdevice.startsWith("00:58:50") || bluetoothdevice.startsWith("01:58:50");
    }

    private void showWarning()
    {
        if (MyApplication.a() == null)
        {
            return;
        } else
        {
            MyApplication.a().runOnUiThread(new _cls4());
            return;
        }
    }

    public void cancel()
    {
        Logger.d("ximao", "XiMaoBTManger cancel IN");
        if (getInstance(mCon) != null)
        {
            if (getDownloadModule() != null)
            {
                getDownloadModule().cleanup();
            }
            if (mSppConnThread != null)
            {
                mConnStatus = XiMaoComm.STATE.START;
                mSppConnThread.cancel();
                mSppConnThread = null;
            }
            mConnStatus = XiMaoComm.STATE.START;
        }
    }

    public void connectSpp(String s, BTDevConnectionReceiver btdevconnectionreceiver)
    {
        timer = new Timer();
        task = new _cls2();
        if (btdevconnectionreceiver != null)
        {
            setmBTDevConnectionReceiver(btdevconnectionreceiver);
        }
        if (!isConnected())
        {
            mConnStatus = XiMaoComm.STATE.CONNECTING;
            Logger.d("ximao", "\u6B63\u5F0F\u8FDB\u5165\u8FDE\u63A5");
            if (mSppConnThread != null)
            {
                Logger.d("ximao", "\u8FDE\u63A5\u53D1\u73B0Thread\u4E0D\u4E3A\u7A7A");
                mSppConnThread.cancel();
                mSppConnThread = null;
                try
                {
                    Thread.sleep(2000L);
                }
                // Misplaced declaration of an exception variable
                catch (BTDevConnectionReceiver btdevconnectionreceiver)
                {
                    btdevconnectionreceiver.printStackTrace();
                }
            }
            mSppConnThread = new BTDevSppConnThread(s);
            if (mSppConnThread != null)
            {
                mSppConnThread.start();
                return;
            } else
            {
                mConnStatus = XiMaoComm.STATE.FAILED;
                setmBTDevConnectionReceiver(null);
                onConnectFailed();
                return;
            }
        } else
        {
            onConnected();
            return;
        }
    }

    public void enableBlueTooth()
    {
        while (getAdapter() == null || getAdapter().isEnabled()) 
        {
            return;
        }
        getAdapter().enable();
    }

    public BluetoothAdapter getAdapter()
    {
        if (mAdapter != null)
        {
            return mAdapter;
        }
        mAdapter = BluetoothAdapter.getDefaultAdapter();
_L2:
        return mAdapter;
        Exception exception;
        exception;
        if (MyApplication.a() != null)
        {
            MyApplication.a().runOnUiThread(new _cls5());
        }
        if (true) goto _L2; else goto _L1
_L1:
    }

    public XiMaoComm.STATE getConnState()
    {
        return mConnStatus;
    }

    public XiMaoDownloadModule getDownloadModule()
    {
        return mDownloadModule;
    }

    public int getPacketSize(byte abyte0[])
    {
        return abyte0[3] & (abyte0[2] << 8) + 255 & 0xff;
    }

    public double getStorage()
    {
        return mStorage;
    }

    protected BTDevConnectionReceiver getmBTDevMainConnectionReceiver()
    {
        return mBTDevMainConnectionReceiver;
    }

    public void initInUi()
    {
        initVoiceManager();
        if (BluetoothAdapter.getDefaultAdapter() != null)
        {
            mAdapter = BluetoothAdapter.getDefaultAdapter();
        }
    }

    public void initVoiceManager()
    {
        if (mXimaoVoiceManager == null)
        {
            mXimaoVoiceManager = XiMaoVoiceManager.getInstance(MyApplication.b());
            mXimaoVoiceManager.initVoice();
        }
    }

    public boolean isAudio()
    {
        return isAudio;
    }

    public boolean isConnected()
    {
        return getConnState() == XiMaoComm.STATE.CONNECTED && mSppConnThread != null && mSppConnThread.mSocket != null && isSocketConnected() && mSppConnThread.mInStream != null && mSppConnThread.mOutStream != null && !mSppConnThread.mThreadFinished;
    }

    public boolean isModelBinded(long l)
    {
        while (keyInfo1 != null && keyInfo1.keyAlbumId == l || keyInfo2 != null && keyInfo2.keyAlbumId == l || keyInfo3 != null && keyInfo3.keyAlbumId == l) 
        {
            return true;
        }
        return false;
    }

    public boolean isSearchContent()
    {
        return isSearchContent;
    }

    public boolean isSocketConnected()
    {
        return true;
    }

    protected void onConnectFailed()
    {
        mConnStatus = XiMaoComm.STATE.FAILED;
        if (mConnectionReceiver != null)
        {
            mConnectionReceiver.onConnectFailed();
        }
    }

    protected void onConnected()
    {
        if (timer != null)
        {
            timer.cancel();
        } else
        {
            Logger.d("ximao", "MainTabActivity2\u4E3A\u7A7A");
        }
        Logger.d("ximao", "\u5DF2\u8FDE\u63A5");
        mConnStatus = XiMaoComm.STATE.CONNECTED;
        Logger.d("ximao", "spp thread onConnected");
        if (mConnectionReceiver != null)
        {
            mConnectionReceiver.onConnected();
        }
    }

    protected void onDisconnected()
    {
        if (mConnStatus == XiMaoComm.STATE.CONNECTED && mBTDevMainConnectionReceiver != null)
        {
            mConnStatus = XiMaoComm.STATE.DISCONNECTED;
            mBTDevMainConnectionReceiver.onDisconnected();
        } else
        {
            Logger.d("ximao", "mBTDevMainConnectionReceiver\u4E3A\u7A7A");
        }
        cancel();
        mConnStatus = XiMaoComm.STATE.DISCONNECTED;
    }

    public void registerXiMaoMainReceiver(final Activity activity)
    {
        if (activity != null)
        {
            activity.runOnUiThread(new _cls1());
        }
    }

    public void saveKeyInfo()
    {
        switch (keyInfoTemp.keyNum)
        {
        default:
            return;

        case 1: // '\001'
            keyInfo1 = keyInfoTemp;
            return;

        case 2: // '\002'
            keyInfo2 = keyInfoTemp;
            return;

        case 3: // '\003'
            keyInfo3 = keyInfoTemp;
            break;
        }
    }

    protected boolean sendPacket(byte abyte0[], int i)
    {
        boolean flag = false;
        if (mSppConnThread != null)
        {
            mSppConnThread.cleanArray();
            isAudio = false;
            mSppConnThread.write(abyte0, i);
            flag = true;
        }
        return flag;
    }

    public void setConnState(XiMaoComm.STATE state)
    {
        mConnStatus = state;
    }

    public void setDownloadModule(XiMaoDownloadModule ximaodownloadmodule)
    {
        mDownloadModule = ximaodownloadmodule;
    }

    public void setSearchContent(boolean flag)
    {
        isSearchContent = flag;
    }

    public void setStorage(double d)
    {
        mStorage = d;
    }

    protected void setmBTDevConnectionReceiver(BTDevConnectionReceiver btdevconnectionreceiver)
    {
        mConnectionReceiver = btdevconnectionreceiver;
    }

    protected void setmBTDevDiscoveryReceiver(BTDevDiscoveryReceiver btdevdiscoveryreceiver)
    {
        mBTDevDiscoveryReceiver = btdevdiscoveryreceiver;
    }

    public void setmBTDevMainConnectionReceiver(BTDevConnectionReceiver btdevconnectionreceiver)
    {
        mBTDevMainConnectionReceiver = btdevconnectionreceiver;
    }

    public void showToast(String s)
    {
        Logger.d("ximao", s);
    }

    public void stopBlueTooth()
    {
        if (getAdapter() == null)
        {
            return;
        } else
        {
            getAdapter().disable();
            return;
        }
    }

    public void tryConnect(final boolean isSlient)
    {
        int i = android.os.Build.VERSION.SDK_INT;
        if (BluetoothAdapter.getDefaultAdapter() == null)
        {
            if (!isSlient)
            {
                try
                {
                    showWarning();
                    return;
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
            }
            break MISSING_BLOCK_LABEL_42;
        }
        if (BluetoothAdapter.getDefaultAdapter().isEnabled())
        {
            break MISSING_BLOCK_LABEL_43;
        }
        if (isSlient)
        {
            break MISSING_BLOCK_LABEL_154;
        }
        showWarning();
        return;
        return;
        BluetoothDevice bluetoothdevice = MyDeviceManager.getInstance(mCon).getConnectedDevice(MyApplication.a());
        if (bluetoothdevice == null)
        {
            break MISSING_BLOCK_LABEL_92;
        }
        if (isXimao(bluetoothdevice))
        {
            device = bluetoothdevice;
            connectSpp(bluetoothdevice.getAddress(), null);
            return;
        }
        if (isSlient)
        {
            break MISSING_BLOCK_LABEL_154;
        }
        showWarning();
        return;
        if (i >= 11)
        {
            break MISSING_BLOCK_LABEL_107;
        }
        if (isSlient)
        {
            break MISSING_BLOCK_LABEL_154;
        }
        showWarning();
        return;
        if (BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(2) == 2)
        {
            if (mCon != null);
            BluetoothAdapter.getDefaultAdapter().getProfileProxy(MyApplication.b(), new _cls3(), 2);
            return;
        }
        if (isSlient)
        {
            break MISSING_BLOCK_LABEL_154;
        }
        showWarning();
    }



/*
    static XiMaoMainHandler access$002(XiMaoBTManager ximaobtmanager, XiMaoMainHandler ximaomainhandler)
    {
        ximaobtmanager.mainHandler = ximaomainhandler;
        return ximaomainhandler;
    }

*/




/*
    static byte[] access$1002(XiMaoBTManager ximaobtmanager, byte abyte0[])
    {
        ximaobtmanager.lastPacket = abyte0;
        return abyte0;
    }

*/


/*
    static XiMaoMainReceiver access$102(XiMaoBTManager ximaobtmanager, XiMaoMainReceiver ximaomainreceiver)
    {
        ximaobtmanager.mainReceiver = ximaomainreceiver;
        return ximaomainreceiver;
    }

*/




/*
    static BluetoothAdapter access$1802(XiMaoBTManager ximaobtmanager, BluetoothAdapter bluetoothadapter)
    {
        ximaobtmanager.mAdapter = bluetoothadapter;
        return bluetoothadapter;
    }

*/



/*
    static XiMaoComm.STATE access$202(XiMaoBTManager ximaobtmanager, XiMaoComm.STATE state)
    {
        ximaobtmanager.mConnStatus = state;
        return state;
    }

*/



/*
    static boolean access$302(XiMaoBTManager ximaobtmanager, boolean flag)
    {
        ximaobtmanager.isAudio = flag;
        return flag;
    }

*/







/*
    static XiMaoVoiceManager access$802(XiMaoBTManager ximaobtmanager, XiMaoVoiceManager ximaovoicemanager)
    {
        ximaobtmanager.mXimaoVoiceManager = ximaovoicemanager;
        return ximaovoicemanager;
    }

*/


    private class _cls4
        implements Runnable
    {

        final XiMaoBTManager this$0;

        public void run()
        {
            (new DialogBuilder(MyApplication.a())).setMessage("\u5F53\u524D\u6CA1\u6709\u8FDE\u63A5\u6545\u4E8B\u673A\uFF0C\u8BF7\u6309\u7167\u8BF4\u660E\u8FDE\u63A5\u6545\u4E8B\u673A\u540E\u518D\u8BD5").showWarning();
        }

        _cls4()
        {
            this$0 = XiMaoBTManager.this;
            super();
        }
    }


    private class _cls2 extends TimerTask
    {

        final XiMaoBTManager this$0;

        public void run()
        {
            if (mConnStatus != XiMaoComm.STATE.CONNECTED)
            {
                if (mSppConnThread != null)
                {
                    mSppConnThread.cancel();
                }
                mConnStatus = XiMaoComm.STATE.FAILED;
                onConnectFailed();
            }
        }

        _cls2()
        {
            this$0 = XiMaoBTManager.this;
            super();
        }
    }


    private class _cls5
        implements Runnable
    {

        final XiMaoBTManager this$0;

        public void run()
        {
            mAdapter = BluetoothAdapter.getDefaultAdapter();
        }

        _cls5()
        {
            this$0 = XiMaoBTManager.this;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final XiMaoBTManager this$0;
        final Activity val$activity;

        public void run()
        {
            mainHandler = new XiMaoMainHandler(activity);
            mainReceiver = new XiMaoMainReceiver(mainHandler);
            setmBTDevMainConnectionReceiver(mainReceiver);
        }

        _cls1()
        {
            this$0 = XiMaoBTManager.this;
            activity = activity1;
            super();
        }
    }


    private class _cls3
        implements android.bluetooth.BluetoothProfile.ServiceListener
    {

        final XiMaoBTManager this$0;
        final boolean val$isSlient;

        public void onServiceConnected(int i, BluetoothProfile bluetoothprofile)
        {
            List list;
            list = bluetoothprofile.getConnectedDevices();
            BluetoothAdapter.getDefaultAdapter().closeProfileProxy(i, bluetoothprofile);
            if (list == null || list.size() == 0) goto _L2; else goto _L1
_L1:
            bluetoothprofile = (BluetoothDevice)list.get(0);
            if (!XiMaoBTManager.isXimao(bluetoothprofile)) goto _L4; else goto _L3
_L3:
            device = bluetoothprofile;
            connectSpp(bluetoothprofile.getAddress(), null);
_L6:
            return;
_L4:
            if (!isSlient)
            {
                showWarning();
                return;
            }
            continue; /* Loop/switch isn't completed */
_L2:
            if (!isSlient)
            {
                showWarning();
                return;
            }
            if (true) goto _L6; else goto _L5
_L5:
        }

        public void onServiceDisconnected(int i)
        {
        }

        _cls3()
        {
            this$0 = XiMaoBTManager.this;
            isSlient = flag;
            super();
        }
    }

}
