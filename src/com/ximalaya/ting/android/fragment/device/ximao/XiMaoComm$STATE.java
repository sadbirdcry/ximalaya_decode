// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;


// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoComm

public static final class  extends Enum
{

    private static final TIMEOUT $VALUES[];
    public static final TIMEOUT CONNECTED;
    public static final TIMEOUT CONNECTING;
    public static final TIMEOUT DISCONNECTED;
    public static final TIMEOUT FAILED;
    public static final TIMEOUT START;
    public static final TIMEOUT TIMEOUT;

    public static  valueOf(String s)
    {
        return ()Enum.valueOf(com/ximalaya/ting/android/fragment/device/ximao/XiMaoComm$STATE, s);
    }

    public static [] values()
    {
        return ([])$VALUES.clone();
    }

    static 
    {
        START = new <init>("START", 0);
        CONNECTING = new <init>("CONNECTING", 1);
        CONNECTED = new <init>("CONNECTED", 2);
        DISCONNECTED = new <init>("DISCONNECTED", 3);
        FAILED = new <init>("FAILED", 4);
        TIMEOUT = new <init>("TIMEOUT", 5);
        $VALUES = (new .VALUES[] {
            START, CONNECTING, CONNECTED, DISCONNECTED, FAILED, TIMEOUT
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
