// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.device.AlbumBindingListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.search.SearchHistoryHotFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.search.AssociateModel;
import com.ximalaya.ting.android.model.search.SearchAlbum;
import com.ximalaya.ting.android.model.search.SearchAlbumNew;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoComm

public class XiMaoSearchFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    public static class SearchModelDivider
    {

        public int count;
        public String type;

        public SearchModelDivider()
        {
        }
    }


    public static final int HISTORY_COUNT = 6;
    public static final int HOT_COUNT = 9;
    public static final String TAG = "ximao";
    public static final boolean isNew = false;
    JSONArray jsonArray;
    private int key;
    private AlbumBindingListAdapter mAdapter;
    private View mClearSearchText;
    private List mDataList;
    private com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType mDeviceType;
    private TextView mFooterView;
    private SearchHistoryHotFragment mHistoryHotFra;
    private List mHistoryList;
    private boolean mIsLoading;
    private String mKeyWord;
    private ListView mListView;
    private MyDeviceManager mMyDeviceManager;
    private int mPageId;
    private int mPageSize;
    private Button mSearchButton;
    private EditText mSearchText;
    private String mSort;
    private RadioButton mSortBtn1;
    private RadioButton mSortBtn2;
    private RadioButton mSortBtn3;
    private RadioGroup mSortHeader;
    private int mTotal;

    public XiMaoSearchFragment()
    {
        mDataList = new ArrayList();
        mHistoryList = new ArrayList();
        jsonArray = new JSONArray();
        mPageId = 1;
        mPageSize = 15;
        mSort = null;
    }

    private void addHistoryHotFrag()
    {
        FragmentTransaction fragmenttransaction = getFragmentManager().beginTransaction();
        if (mHistoryHotFra == null)
        {
            mHistoryHotFra = SearchHistoryHotFragment.getInstance();
            mHistoryHotFra.setOnItemClickListener(new _cls10());
            fragmenttransaction.add(0x7f0a012a, mHistoryHotFra, "history_hot");
            fragmenttransaction.commitAllowingStateLoss();
            openSoftInput();
        }
    }

    private String getSearchScope()
    {
        return "album";
    }

    private String getSearchWord()
    {
        return mSearchText.getEditableText().toString().trim();
    }

    private String getSortForAlbum(int i)
    {
        switch (i)
        {
        default:
            return "";

        case 2131361920: 
            return "";

        case 2131361921: 
            ToolUtil.onEvent(mCon, "CLICK_SEARCH_ALBUM_NEW");
            return "recent";

        case 2131362123: 
            ToolUtil.onEvent(mCon, "CLICK_SEARCH_ALBUM_PLAY");
            break;
        }
        return "play";
    }

    private String getSortStr(int i)
    {
        return getSortForAlbum(i);
    }

    private void hideSoftInput()
    {
        if (mSearchText != null)
        {
            mSearchText.clearFocus();
        }
        if (mCon != null)
        {
            ((InputMethodManager)mCon.getSystemService("input_method")).hideSoftInputFromWindow(mSearchText.getWindowToken(), 0);
        }
    }

    private void initListener()
    {
        mSearchButton.setOnClickListener(this);
        mClearSearchText.setOnClickListener(this);
        mSearchText.setOnFocusChangeListener(new _cls1());
        mSearchButton.setEnabled(true);
        mSearchText.setOnKeyListener(new _cls2());
        mFooterView.setOnClickListener(new _cls3());
        mListView.setOnTouchListener(new _cls4());
        mListView.setOnScrollListener(new _cls5());
        mSortHeader.setOnCheckedChangeListener(new _cls6());
    }

    private void initUi()
    {
        mListView = (ListView)findViewById(0x7f0a0129);
        mFooterView = (TextView)View.inflate(mCon, 0x7f0301ca, null);
        mFooterView.setGravity(17);
        mListView.addFooterView(mFooterView, null, true);
        mAdapter = new AlbumBindingListAdapter(getActivity(), this, mListView, mDataList);
        mListView.setAdapter(mAdapter);
        mAdapter.setBindingListener(new _cls7());
        mListView.setAdapter(mAdapter);
        mSearchButton = (Button)findViewById(0x7f0a00e0);
        mSearchText = (EditText)findViewById(0x7f0a00df);
        mClearSearchText = findViewById(0x7f0a00e1);
        mSortHeader = (RadioGroup)findViewById(0x7f0a0127);
        mSortBtn1 = (RadioButton)mSortHeader.findViewById(0x7f0a0080);
        mSortBtn2 = (RadioButton)mSortHeader.findViewById(0x7f0a0081);
        mSortBtn3 = (RadioButton)mSortHeader.findViewById(0x7f0a014b);
        mSortHeader.setVisibility(8);
        mSortBtn1.setVisibility(8);
        mSortBtn2.setVisibility(8);
        mSortBtn3.setVisibility(8);
    }

    private void loadLocal(String s)
    {
        s = (new StringBuilder()).append("http://3rd.ximalaya.com/albums/").append(s).append("?i_am=xiyin").toString();
        f.a().a(s, null, null, new _cls8());
    }

    private void loadSearch(AssociateModel associatemodel)
    {
        if (associatemodel == null || TextUtils.isEmpty(associatemodel.title))
        {
            return;
        }
        mPageSize = 20;
        static class _cls11
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType = new int[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.ximao.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls11..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[mDeviceType.ordinal()];
        JVM INSTR tableswitch 1 3: default 60
    //                   1 228
    //                   2 228
    //                   3 104;
           goto _L1 _L2 _L2 _L3
_L1:
        String s;
        s = "";
        associatemodel = null;
_L5:
        if (!TextUtils.isEmpty(mSort))
        {
            associatemodel.add("sort", mSort);
        }
        f.a().a(s, associatemodel, null, new _cls9());
        return;
_L3:
        s = (new StringBuilder()).append("http://3rd.ximalaya.com/search/albums?i_am=xiyin").append("&q=").append(associatemodel.title).toString();
        associatemodel = s;
        if (XiMaoComm.isLockBind(mCon))
        {
            associatemodel = (new StringBuilder()).append(s).append("&category_id=6").toString();
        }
        associatemodel = (new StringBuilder()).append(associatemodel).append("&page=").append(mPageId).toString();
        s = (new StringBuilder()).append(associatemodel).append("&per_page=").append(mPageSize).toString();
        associatemodel = null;
        continue; /* Loop/switch isn't completed */
_L2:
        s = (new StringBuilder()).append(a.P).append("front/v1").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.add("kw", associatemodel.title);
        requestparams.add("page", (new StringBuilder()).append("").append(mPageId).toString());
        requestparams.add("rows", (new StringBuilder()).append("").append(mPageSize).toString());
        requestparams.add("core", "album");
        requestparams.add("condition", "play");
        associatemodel = requestparams;
        if (true) goto _L5; else goto _L4
_L4:
    }

    private void openSoftInput()
    {
        mSearchText.requestFocus();
        if (mCon != null)
        {
            ((InputMethodManager)mCon.getSystemService("input_method")).showSoftInput(mSearchText, 0);
        }
    }

    private void parseSearchAlbumTingshubao(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        s = s.getJSONObject("response");
        if (s == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        mTotal = s.getIntValue("numFound");
        int i = s.getIntValue("start");
        s = s.getString("docs");
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/search/SearchAlbum);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null || s.size() == 0)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        mFooterView.setVisibility(8);
        mListView.setFooterDividersEnabled(false);
        if (mPageId == 1 || i == 0)
        {
            mDataList.clear();
        }
        mDataList.addAll(ModelHelper.toAlbumModelNewList(s));
        mAdapter.notifyDataSetChanged();
        mPageId = mPageId + 1;
    }

    private void parseSearchAlbumXimao(String s)
    {
        boolean flag = false;
        if (TextUtils.isEmpty(s))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        if (s.getIntValue("ret") != 0)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        String s1 = s.getString("albums");
        mTotal = s.getIntValue("total_count");
        int i = s.getIntValue("page");
        if (TextUtils.isEmpty(s1))
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        try
        {
            s = JSON.parseArray(s1, com/ximalaya/ting/android/model/search/SearchAlbumNew);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null || s.size() == 0)
        {
            mFooterView.setVisibility(0);
            mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
            return;
        }
        mFooterView.setVisibility(8);
        mListView.setFooterDividersEnabled(false);
        if (mDataList == null || mDataList.size() == 0)
        {
            flag = true;
        }
        if (mPageId == 1 || i == 1)
        {
            mDataList.clear();
        }
        mDataList.addAll(ModelHelper.toAlbumModelNewList2(s));
        if (flag)
        {
            mListView.setAdapter(mAdapter);
        }
        mAdapter.notifyDataSetChanged();
        mPageId = mPageId + 1;
    }

    private void removeHistoryHotFragment()
    {
        FragmentManager fragmentmanager = getFragmentManager();
        if (fragmentmanager != null && fragmentmanager.findFragmentByTag("history_hot") != null)
        {
            FragmentTransaction fragmenttransaction = getFragmentManager().beginTransaction();
            fragmenttransaction.remove(fragmentmanager.findFragmentByTag("history_hot"));
            fragmenttransaction.commitAllowingStateLoss();
            mHistoryHotFra = null;
        }
    }

    private void saveHistory()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCon);
        if (mHistoryList != null)
        {
            sharedpreferencesutil.saveString("history_search_word", JSON.toJSONString(mHistoryList));
        }
        sharedpreferencesutil.saveString("history_search_scope", getSearchScope());
    }

    void clearHistory()
    {
        if (mHistoryList != null)
        {
            mHistoryList.clear();
            saveHistory();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            key = bundle.getInt("key");
        }
        mDeviceType = MyDeviceManager.getInstance(getActivity().getApplicationContext()).getNowDeviceType();
        mMyDeviceManager = MyDeviceManager.getInstance(mActivity.getApplicationContext());
        initUi();
        initListener();
        addHistoryHotFrag();
    }

    public void onClick(View view)
    {
        if (view.getId() == 0x7f0a00e0)
        {
            search(getSearchWord(), getSearchScope(), true);
        } else
        if (view.getId() == 0x7f0a00e1)
        {
            mSearchText.setText("");
            mSortHeader.setVisibility(8);
            addHistoryHotFrag();
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300ea, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        mAdapter.removeBindingListener();
        super.onDestroyView();
    }

    public void onPause()
    {
        hideSoftInput();
        saveHistory();
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
    }

    void search(String s, String s1, boolean flag)
    {
        if (TextUtils.isEmpty(s))
        {
            showToast("\u8BF7\u8F93\u5165\u641C\u7D22\u5173\u952E\u8BCD\uFF01");
        } else
        {
            hideSoftInput();
            Object obj = mHistoryList.iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                AssociateModel associatemodel = (AssociateModel)((Iterator) (obj)).next();
                if (!s.equals(associatemodel.title))
                {
                    continue;
                }
                mHistoryList.remove(associatemodel);
                break;
            } while (true);
            obj = s1;
            if (TextUtils.isEmpty(s1))
            {
                obj = "album";
            }
            s1 = new AssociateModel();
            s1.type = ((String) (obj));
            s1.title = s;
            mHistoryList.add(0, s1);
            if (mHistoryList.size() > 6)
            {
                mHistoryList = mHistoryList.subList(0, 6);
            }
            mKeyWord = s;
            saveHistory();
            if (flag)
            {
                if (mKeyWord.equals("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A"))
                {
                    jsonArray = new JSONArray();
                    loadLocal("418786");
                    loadLocal("418783");
                    loadLocal("424529");
                    mListView.setDivider(null);
                    mListView.setDividerHeight(ToolUtil.dp2px(mActivity, 10F));
                    removeHistoryHotFragment();
                    return;
                } else
                {
                    mPageId = 1;
                    mDataList.clear();
                    mListView.setDivider(null);
                    mListView.setDividerHeight(ToolUtil.dp2px(mActivity, 10F));
                    loadSearch(s1);
                    removeHistoryHotFragment();
                    return;
                }
            }
        }
    }






/*
    static boolean access$1102(XiMaoSearchFragment ximaosearchfragment, boolean flag)
    {
        ximaosearchfragment.mIsLoading = flag;
        return flag;
    }

*/



/*
    static String access$1302(XiMaoSearchFragment ximaosearchfragment, String s)
    {
        ximaosearchfragment.mSort = s;
        return s;
    }

*/












/*
    static List access$402(XiMaoSearchFragment ximaosearchfragment, List list)
    {
        ximaosearchfragment.mHistoryList = list;
        return list;
    }

*/






/*
    static int access$802(XiMaoSearchFragment ximaosearchfragment, int i)
    {
        ximaosearchfragment.mPageId = i;
        return i;
    }

*/


    private class _cls10
        implements com.ximalaya.ting.android.fragment.search.SearchHistoryHotFragment.OnItemClickListener
    {

        final XiMaoSearchFragment this$0;

        public void onClearHistory(View view)
        {
            clearHistory();
        }

        public void onItemClick(View view, AssociateModel associatemodel)
        {
            mSearchText.setText(associatemodel.title);
            search(associatemodel.title, associatemodel.type, true);
        }

        public void onLoadedHistoryData(List list)
        {
            mHistoryList = list;
        }

        _cls10()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnFocusChangeListener
    {

        final XiMaoSearchFragment this$0;

        public void onFocusChange(View view, boolean flag)
        {
            if (flag && TextUtils.isEmpty(getSearchWord()))
            {
                addHistoryHotFrag();
            }
        }

        _cls1()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnKeyListener
    {

        final XiMaoSearchFragment this$0;

        public boolean onKey(View view, int i, KeyEvent keyevent)
        {
            if (i == 66 && keyevent.getAction() == 0)
            {
                search(getSearchWord(), getSearchScope(), true);
                return true;
            } else
            {
                return false;
            }
        }

        _cls2()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final XiMaoSearchFragment this$0;

        public void onClick(View view)
        {
            mDataList.clear();
            mHistoryList.clear();
            mAdapter.notifyDataSetChanged();
            mFooterView.setText("\u65E0\u5386\u53F2\u8BB0\u5F55");
        }

        _cls3()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnTouchListener
    {

        final XiMaoSearchFragment this$0;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            hideSoftInput();
            return false;
        }

        _cls4()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AbsListView.OnScrollListener
    {

        final XiMaoSearchFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || (mPageId - 1) * mPageSize > mTotal)
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        mFooterView.setVisibility(0);
                        mFooterView.setText("\u52A0\u8F7D\u66F4\u591A\u7ED3\u679C...");
                        abslistview = new AssociateModel();
                        abslistview.title = getSearchWord();
                        abslistview.type = getSearchScope();
                        loadSearch(abslistview);
                    }
                }
                return;
            }
            mFooterView.setVisibility(8);
        }

        _cls5()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final XiMaoSearchFragment this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            mSort = getSortStr(i);
            radiogroup = new AssociateModel();
            radiogroup.title = getSearchWord();
            radiogroup.type = getSearchScope();
            mPageId = 1;
            mDataList.clear();
            mAdapter.notifyDataSetChanged();
            loadSearch(radiogroup);
        }

        _cls6()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }


    private class _cls7
        implements com.ximalaya.ting.android.adapter.device.AlbumBindingListAdapter.OnBindingListener
    {

        final XiMaoSearchFragment this$0;

        public void doCollect(Context context, AlbumModelNew albummodelnew, AlbumItemHolder albumitemholder)
        {
            if (mMyDeviceManager.isConn())
            {
                mMyDeviceManager.bindDevice(key, ModelHelper.toAlbumModel(albummodelnew), getActivity());
            }
            mAdapter.notifyDataSetChanged();
            switch (_cls11..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[mMyDeviceManager.getNowDeviceType().ordinal()])
            {
            default:
                return;

            case 1: // '\001'
                goBackFragment(com/ximalaya/ting/android/fragment/device/doss/DossContentFragment);
                return;

            case 2: // '\002'
                goBackFragment(com/ximalaya/ting/android/fragment/device/DeviceBindingListFragment);
                return;

            case 3: // '\003'
                goBackFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment);
                break;
            }
        }

        _cls7()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }


    private class _cls8 extends com.ximalaya.ting.android.b.a
    {

        final XiMaoSearchFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01");
        }

        public void onStart()
        {
            super.onStart();
            mIsLoading = true;
            mFooterView.setVisibility(0);
            mFooterView.setText("\u6B63\u5728\u641C\u7D22...");
        }

        public void onSuccess(String s)
        {
            if (canGoon())
            {
                if (TextUtils.isEmpty(s))
                {
                    mFooterView.setVisibility(0);
                    mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
                    return;
                }
                Logger.d("ximao", s);
                JSONObject jsonobject = new JSONObject();
                try
                {
                    s = JSONObject.parseObject(s).getJSONObject("album");
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = jsonobject;
                }
                jsonArray.add(s);
                if (jsonArray.size() == 3)
                {
                    s = new JSONObject();
                    s.put("ret", Integer.valueOf(0));
                    s.put("albums", jsonArray);
                    s.put("page", Integer.valueOf(1));
                    s.put("per_page", Integer.valueOf(20));
                    s.put("total_count", Integer.valueOf(3));
                    parseSearchAlbumXimao(s.toJSONString());
                    return;
                }
            }
        }

        _cls8()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }


    private class _cls9 extends com.ximalaya.ting.android.b.a
    {

        final XiMaoSearchFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01");
        }

        public void onStart()
        {
            super.onStart();
            mIsLoading = true;
            mFooterView.setVisibility(0);
            mFooterView.setText("\u6B63\u5728\u641C\u7D22...");
        }

        public void onSuccess(String s)
        {
            if (!canGoon())
            {
                return;
            }
            if (TextUtils.isEmpty(s))
            {
                mFooterView.setVisibility(0);
                mFooterView.setText("\u672A\u641C\u7D22\u5230\u76F8\u5173\u5185\u5BB9");
                return;
            }
            switch (_cls11..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[mDeviceType.ordinal()])
            {
            default:
                return;

            case 1: // '\001'
            case 2: // '\002'
                parseSearchAlbumTingshubao(s);
                return;

            case 3: // '\003'
                Logger.d("ximao", s);
                break;
            }
            parseSearchAlbumXimao(s);
        }

        _cls9()
        {
            this$0 = XiMaoSearchFragment.this;
            super();
        }
    }

}
