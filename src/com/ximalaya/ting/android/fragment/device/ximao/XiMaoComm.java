// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.NetworkOnMainThreadException;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.adapter.device.XiMaoListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.ManageFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.ximao.XiMaoDownloadModule;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.Logger;
import java.io.PrintStream;
import java.lang.ref.SoftReference;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            KeyInfo, XiMaoBTManager, XiMaoListFragment

public class XiMaoComm
{
    public static final class CONTENT extends Enum
    {

        private static final CONTENT $VALUES[];
        public static final CONTENT CONTENT_1;
        public static final CONTENT CONTENT_2;
        public static final CONTENT CONTENT_3;
        public static final CONTENT CONTENT_4;

        public static String getString(CONTENT content)
        {
            static class _cls5
            {

                static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[];
                static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE[];

                static 
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE = new int[PLAYMODE.values().length];
                    try
                    {
                        $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE[PLAYMODE.ORDER.ordinal()] = 1;
                    }
                    catch (NoSuchFieldError nosuchfielderror6) { }
                    try
                    {
                        $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE[PLAYMODE.SHUFFLE.ordinal()] = 2;
                    }
                    catch (NoSuchFieldError nosuchfielderror5) { }
                    try
                    {
                        $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE[PLAYMODE.REPEAT.ordinal()] = 3;
                    }
                    catch (NoSuchFieldError nosuchfielderror4) { }
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT = new int[CONTENT.values().length];
                    try
                    {
                        $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[CONTENT.CONTENT_1.ordinal()] = 1;
                    }
                    catch (NoSuchFieldError nosuchfielderror3) { }
                    try
                    {
                        $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[CONTENT.CONTENT_2.ordinal()] = 2;
                    }
                    catch (NoSuchFieldError nosuchfielderror2) { }
                    try
                    {
                        $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[CONTENT.CONTENT_3.ordinal()] = 3;
                    }
                    catch (NoSuchFieldError nosuchfielderror1) { }
                    try
                    {
                        $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[CONTENT.CONTENT_4.ordinal()] = 4;
                    }
                    catch (NoSuchFieldError nosuchfielderror)
                    {
                        return;
                    }
                }
            }

            switch (_cls5..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT[content.ordinal()])
            {
            default:
                return null;

            case 1: // '\001'
                return "\u6545\u4E8B";

            case 2: // '\002'
                return "\u6027\u683C\u57F9\u517B";

            case 3: // '\003'
                return "\u513F\u6B4C";

            case 4: // '\004'
                return "\u5F55\u97F3";
            }
        }

        public static CONTENT valueOf(String s)
        {
            return (CONTENT)Enum.valueOf(com/ximalaya/ting/android/fragment/device/ximao/XiMaoComm$CONTENT, s);
        }

        public static CONTENT[] values()
        {
            return (CONTENT[])$VALUES.clone();
        }

        static 
        {
            CONTENT_1 = new CONTENT("CONTENT_1", 0);
            CONTENT_2 = new CONTENT("CONTENT_2", 1);
            CONTENT_3 = new CONTENT("CONTENT_3", 2);
            CONTENT_4 = new CONTENT("CONTENT_4", 3);
            $VALUES = (new CONTENT[] {
                CONTENT_1, CONTENT_2, CONTENT_3, CONTENT_4
            });
        }

        private CONTENT(String s, int i)
        {
            super(s, i);
        }
    }

    public class HEAD
    {

        public static final int HEAD_ACK = 18;
        public static final int HEAD_ALBUM = 5;
        public static final int HEAD_ALBUM_RESPONSE = 14;
        public static final int HEAD_BLUETOOTH_STATE = 9;
        public static final int HEAD_CHECK_STORAGE = 7;
        public static final int HEAD_CHECK_STORAGE_RESPONSE = 17;
        public static final int HEAD_CONTENT1 = 2;
        public static final int HEAD_CONTENT1_RESPONSE = 11;
        public static final int HEAD_CONTENT2 = 8;
        public static final int HEAD_CONTENT2_RESPONSE = 19;
        public static final int HEAD_CONTENT3 = 1;
        public static final int HEAD_CONTENT3_RESPONSE = 10;
        public static final int HEAD_CONTENT4 = 3;
        public static final int HEAD_CONTENT4_RESPONSE = 12;
        public static final int HEAD_PLAY_MODE = 4;
        public static final int HEAD_PLAY_MODE_RESPONSE = 13;
        public static final int HEAD_SMART_AUDIO = 15;
        public static final int HEAD_WARNING_PLAY = 9;
        final XiMaoComm this$0;

        public HEAD()
        {
            this$0 = XiMaoComm.this;
            super();
        }
    }

    public static final class PLAYMODE extends Enum
    {

        private static final PLAYMODE $VALUES[];
        public static final PLAYMODE ORDER;
        public static final PLAYMODE REPEAT;
        public static final PLAYMODE SHUFFLE;

        public static PLAYMODE valueOf(String s)
        {
            return (PLAYMODE)Enum.valueOf(com/ximalaya/ting/android/fragment/device/ximao/XiMaoComm$PLAYMODE, s);
        }

        public static PLAYMODE[] values()
        {
            return (PLAYMODE[])$VALUES.clone();
        }

        static 
        {
            ORDER = new PLAYMODE("ORDER", 0);
            SHUFFLE = new PLAYMODE("SHUFFLE", 1);
            REPEAT = new PLAYMODE("REPEAT", 2);
            $VALUES = (new PLAYMODE[] {
                ORDER, SHUFFLE, REPEAT
            });
        }

        private PLAYMODE(String s, int i)
        {
            super(s, i);
        }
    }

    public static final class STATE extends Enum
    {

        private static final STATE $VALUES[];
        public static final STATE CONNECTED;
        public static final STATE CONNECTING;
        public static final STATE DISCONNECTED;
        public static final STATE FAILED;
        public static final STATE START;
        public static final STATE TIMEOUT;

        public static STATE valueOf(String s)
        {
            return (STATE)Enum.valueOf(com/ximalaya/ting/android/fragment/device/ximao/XiMaoComm$STATE, s);
        }

        public static STATE[] values()
        {
            return (STATE[])$VALUES.clone();
        }

        static 
        {
            START = new STATE("START", 0);
            CONNECTING = new STATE("CONNECTING", 1);
            CONNECTED = new STATE("CONNECTED", 2);
            DISCONNECTED = new STATE("DISCONNECTED", 3);
            FAILED = new STATE("FAILED", 4);
            TIMEOUT = new STATE("TIMEOUT", 5);
            $VALUES = (new STATE[] {
                START, CONNECTING, CONNECTED, DISCONNECTED, FAILED, TIMEOUT
            });
        }

        private STATE(String s, int i)
        {
            super(s, i);
        }
    }

    public class TYPE
    {

        public static final int TYPE_A2DP_CONNECTED = 32;
        public static final int TYPE_A2DP_DISCONNECTED = 33;
        public static final int TYPE_BEGIN_UPLOAD = 8;
        public static final int TYPE_CHECK_STORAGE = 17;
        public static final int TYPE_CLICK_NEXT = 14;
        public static final int TYPE_CLICK_PAUSE = 12;
        public static final int TYPE_CLICK_PLAY = 11;
        public static final int TYPE_CLICK_PRE = 13;
        public static final int TYPE_CLICK_VOICE_ADD = 15;
        public static final int TYPE_CLICK_VOICE_SUB = 18;
        public static final int TYPE_CONNECT_A2DP = 34;
        public static final int TYPE_DELETE = 15;
        public static final int TYPE_DOWNLOAD = 50;
        public static final int TYPE_DOWNLOAD_CHECK_FILE = 48;
        public static final int TYPE_DOWNLOAD_FILE_SIZE = 52;
        public static final int TYPE_DOWNLOAD_FINISH = 51;
        public static final int TYPE_DOWNLOAD_PREPARE = 49;
        public static final int TYPE_FAVORITE = 7;
        public static final int TYPE_FINISH_UPLOAD = 10;
        public static final int TYPE_PLAY = 8;
        public static final int TYPE_PLAYMODE_SEARCH = 10;
        public static final int TYPE_PLAY_ORDER = 6;
        public static final int TYPE_PLAY_PAUSE = 3;
        public static final int TYPE_PLAY_PLAY = 2;
        public static final int TYPE_PLAY_REPEAT = 4;
        public static final int TYPE_PLAY_RESPONSE = 2;
        public static final int TYPE_PLAY_SHUFFLE = 5;
        public static final int TYPE_RESPONSE_SEARCH_ALBUM_ID = 21;
        public static final int TYPE_RESPONSE_TO_SEARCH = 255;
        public static final int TYPE_SEARCH = 1;
        public static final int TYPE_SEARCH_ALBUM_ID = 9;
        public static final int TYPE_SEARCH_NUM = 32;
        public static final int TYPE_SEARCH_NUM_RESPONSE = 19;
        public static final int TYPE_SPP_CONNECTED = 34;
        public static final int TYPE_SPP_DISCONNECTED = 35;
        public static final int TYPE_UPLOADING = 9;
        public static final int TYPE_UPLOAD_FILE = 9;
        public static final int TYPE_WARNING_NO_ALBUM = 24;
        public static final int TYPE_WARNING_NO_DATA = 21;
        public static final int TYPE_WARNING_OK = 23;
        public static final int TYPE_WARNING_SAY_AGAIN = 22;
        final XiMaoComm this$0;

        public TYPE()
        {
            this$0 = XiMaoComm.this;
            super();
        }
    }


    public static final int CHECK_SUM_LENGTH = 1;
    public static final String KEY = "key";
    public static final int KEY_1 = 1;
    public static final int KEY_2 = 2;
    public static final int KEY_3 = 3;
    public static final int MAX_PACKET_LENGTH = 1024;
    public static final int MSG_BTDEV_CONNECTED = 1;
    public static final int MSG_BTDEV_DISCONNECTED = 2;
    public static final int MSG_BTDEV_PACKET = 5;
    public static final int MSG_BTDEV_PACKET_COMMAND = 6;
    public static final int MSG_BTDEV_RECEIVED = 3;
    public static final int MSG_BTDEV_SENT = 4;
    public static int NUM_CONTENT1 = 0;
    public static int NUM_CONTENT2 = 0;
    public static int NUM_CONTENT3 = 0;
    public static int NUM_CONTENT4 = 0;
    public static final int PACKET_HEADER_LENGTH = 4;
    public static final boolean PLAY_LOCAL = true;
    public static final String P_LOCK_XIMAO_BIND = "P_LOCK_XIMAO_BIND";
    public static final String P_LOCK_XIMAO_VOICE_SEARCH = "P_LOCK_XIMAO_VOICE_SEARCH";
    public static final String P_XIMAO_DOWNLOAD_INTERRUPT = "P_LOCK_XIMAO_DOWNLOAD_INTERRUPT";
    public static final String P_XIMAO_DOWNLOAD_PAYLOAD = "P_LOCK_XIMAO_DOWNLOAD_PAYLOAD";
    private static final String TAG = "ximao";
    public static boolean isAutoA2DP = false;
    public static PLAYMODE mPlayMode;

    public XiMaoComm()
    {
    }

    public static boolean bindingAlbum(Context context, int i, Activity activity, KeyInfo keyinfo)
    {
        while (keyinfo == null || activity == null) 
        {
            return false;
        }
        activity = long2bytes(keyinfo.keyAlbumId);
        XiMaoBTManager.getInstance(context).keyInfoTemp = keyinfo;
        i;
        JVM INSTR tableswitch 1 3: default 52
    //                   1 54
    //                   2 75
    //                   3 97;
           goto _L1 _L2 _L3 _L4
_L1:
        return true;
_L2:
        sendPacket(context, 2, 7, activity, activity.length);
        XiMaoBTManager.getInstance(context).keyInfo1 = keyinfo;
        continue; /* Loop/switch isn't completed */
_L3:
        sendPacket(context, 8, 7, activity, activity.length);
        XiMaoBTManager.getInstance(context).keyInfo2 = keyinfo;
        continue; /* Loop/switch isn't completed */
_L4:
        sendPacket(context, 1, 7, activity, activity.length);
        XiMaoBTManager.getInstance(context).keyInfo3 = keyinfo;
        if (true) goto _L1; else goto _L5
_L5:
    }

    public static long bytes2long(byte abyte0[])
    {
        return bytes2long(abyte0, 0);
    }

    public static long bytes2long(byte abyte0[], int i)
    {
        long l = 0L;
        for (int j = i; j < i + 8; j++)
        {
            l = l << 8 | (long)(abyte0[j] & 0xff);
        }

        return l;
    }

    public static String bytesToHexString(byte abyte0[])
    {
        StringBuilder stringbuilder = new StringBuilder("");
        if (abyte0 == null || abyte0.length <= 0)
        {
            return null;
        }
        for (int i = 0; i < abyte0.length; i++)
        {
            String s = Integer.toHexString(abyte0[i] & 0xff);
            if (s.length() < 2)
            {
                stringbuilder.append(0);
            }
            stringbuilder.append(s);
        }

        return stringbuilder.toString();
    }

    public static boolean changePlayMode(Context context, PLAYMODE playmode)
    {
        _cls5..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.PLAYMODE[playmode.ordinal()];
        JVM INSTR tableswitch 1 3: default 36
    //                   1 38
    //                   2 48
    //                   3 57;
           goto _L1 _L2 _L3 _L4
_L1:
        return true;
_L2:
        sendCommand(context, 4, 6);
        continue; /* Loop/switch isn't completed */
_L3:
        sendCommand(context, 4, 5);
        continue; /* Loop/switch isn't completed */
_L4:
        sendCommand(context, 4, 4);
        if (true) goto _L1; else goto _L5
_L5:
    }

    public static boolean check(List list)
    {
        if (list != null && list.size() != 0) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        int k = list.size();
        int i = 0;
        int j = 0;
        do
        {
label0:
            {
                if (i < list.size() && i != k - 1)
                {
                    break label0;
                }
                if (j == ((Integer)list.get(k - 1)).intValue())
                {
                    return true;
                }
            }
            if (true)
            {
                continue;
            }
            j += ((Integer)list.get(i)).intValue() & 0xf;
            System.out.println(j);
            i++;
        } while (true);
        if (true) goto _L1; else goto _L3
_L3:
    }

    public static boolean check(byte abyte0[], int i)
    {
        if (abyte0 == null || i == 0)
        {
            return false;
        }
        int j = 0;
        byte byte0 = 0;
        for (; j < i - 1; j++)
        {
            byte0 = (byte)((byte)(byte0 + abyte0[j] & 0xff) & 0xff);
        }

        if (byte0 == abyte0[i - 1])
        {
            Logger.d("ximao", "\u6821\u9A8C\u901A\u8FC7");
            return true;
        } else
        {
            Logger.d("ximao", (new StringBuilder()).append("\u6821\u9A8C\u5931\u8D25").append(byte0).append(":").append(abyte0[i - 1]).toString());
            return false;
        }
    }

    public static void cleanPlayPosition()
    {
        XiMaoListFragment.isPlay = false;
        XiMaoListFragment.mPlayContent = null;
        XiMaoListFragment.mPlayPositionNow = -100;
        Object obj = MyApplication.a();
        if (obj != null && (obj instanceof MainTabActivity2))
        {
            int i;
            if ((MainTabActivity2)obj != null)
            {
                i = ((MainTabActivity2)obj).getManageFragment().mStacks.size();
            } else
            {
                i = 0;
            }
            if (i >= 1)
            {
                obj = (Fragment)((SoftReference)((MainTabActivity2)obj).getManageFragment().mStacks.get(i - 1)).get();
                if ((obj instanceof XiMaoListFragment) && ((XiMaoListFragment)obj).xiMaoListAdapter != null)
                {
                    ((XiMaoListFragment)obj).xiMaoListAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    public static byte getCheckSum(byte abyte0[], int i)
    {
        int j = 0;
        if (abyte0 == null || i == 0)
        {
            return 0;
        }
        byte byte0 = 0;
        for (; j < i; j++)
        {
            byte0 = (byte)((byte)(byte0 + abyte0[j] & 0xff) & 0xff);
        }

        return (byte)(byte0 & 0xff);
    }

    public static int getDownloadInterrupt(Context context)
    {
        return 150;
    }

    public static int getDownloadPayload(Context context)
    {
        return 1000;
    }

    public static void goIntroduction(Activity activity)
    {
        String s = MyDeviceManager.getInstance(activity.getApplicationContext()).getXimaoIntro();
        if (!TextUtils.isEmpty(s))
        {
            Intent intent = new Intent(activity, com/ximalaya/ting/android/activity/web/WebActivityNew);
            intent.putExtra("ExtraUrl", s);
            activity.startActivity(intent);
            return;
        } else
        {
            CustomToast.showToast(activity, "\u656C\u8BF7\u671F\u5F85", 0);
            return;
        }
    }

    public static boolean isDownloadResponse(byte abyte0[])
    {
        if ((abyte0[0] == 11 || abyte0[0] == 19 || abyte0[0] == 10 || abyte0[0] == 12) && (abyte0[1] == 50 || abyte0[1] == 48 || abyte0[1] == 49 || abyte0[1] == 51 || abyte0[1] == 52))
        {
            Logger.d("download", "isDownloadResponse");
            return true;
        } else
        {
            return false;
        }
    }

    public static boolean isLockBind(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean("P_LOCK_XIMAO_BIND", true);
    }

    public static boolean isLockVoiceSearch(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean("P_LOCK_XIMAO_VOICE_SEARCH", true);
    }

    public static boolean isValidHead(byte byte0)
    {
        return byte0 >= 1 && byte0 <= 19;
    }

    public static List loadCategoryInfo(Context context)
    {
        Object obj;
        Object obj1 = null;
        obj = new RequestParams();
        ((RequestParams) (obj)).add("picVersion", "10");
        ((RequestParams) (obj)).add("scale", "2");
        Object obj2;
        try
        {
            obj = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/discovery/v1/categories").toString(), ((RequestParams) (obj)), null, null, true);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj = null;
        }
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a)) goto _L2; else goto _L1
_L1:
        obj2 = JSONObject.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        obj = obj1;
        if (((JSONObject) (obj2)).getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_123;
        }
        obj2 = ((JSONObject) (obj2)).getString("list");
        obj = obj1;
        if (!TextUtils.isEmpty(((CharSequence) (obj2))))
        {
            obj = FindingCategoryModel.getListFromJson(((String) (obj2)));
        }
_L4:
label0:
        {
            if (obj != null)
            {
                obj1 = obj;
                if (((List) (obj)).size() > 0)
                {
                    break label0;
                }
            }
            context = FileUtils.readAssetFileData(context, "finding/category.json");
            obj1 = obj;
            if (!TextUtils.isEmpty(context))
            {
                obj1 = FindingCategoryModel.getListFromJson(context);
            }
        }
        return ((List) (obj1));
        obj;
        ((Exception) (obj)).printStackTrace();
_L2:
        obj = null;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static byte[] long2bytes(long l)
    {
        byte abyte0[] = new byte[8];
        for (int i = 0; i < 8; i++)
        {
            abyte0[i] = (byte)(int)(l >>> 56 - i * 8);
        }

        return abyte0;
    }

    public static boolean needFilterWhenDownloading(Context context, int i)
    {
        boolean flag1 = false;
        boolean flag = flag1;
        if (XiMaoBTManager.getInstance(context).getDownloadModule() != null)
        {
            flag = flag1;
            if (XiMaoBTManager.getInstance(context).getDownloadModule().getDownloadTasks() != null)
            {
                flag = flag1;
                if (XiMaoBTManager.getInstance(context).getDownloadModule().getDownloadTasks().size() != 0)
                {
                    flag = flag1;
                    if (i != 48)
                    {
                        flag = flag1;
                        if (i != 49)
                        {
                            flag = flag1;
                            if (i != 50)
                            {
                                flag = flag1;
                                if (i != 51)
                                {
                                    flag = flag1;
                                    if (i != 52)
                                    {
                                        CustomToast.showToast(context, "\u60A8\u6B63\u5728\u4E0B\u8F7D\u58F0\u97F3\u5230\u6545\u4E8B\u673A\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5", 0);
                                        flag = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return flag;
    }

    public static boolean needFilterWhenDownloading(Context context, byte abyte0[])
    {
        if (XiMaoBTManager.getInstance(context).getDownloadModule() != null && XiMaoBTManager.getInstance(context).getDownloadModule().getDownloadTasks() != null && XiMaoBTManager.getInstance(context).getDownloadModule().getDownloadTasks().size() != 0 && abyte0[1] != 48 && abyte0[1] != 49 && abyte0[1] != 50 && abyte0[1] != 51 && abyte0[1] != 52)
        {
            CustomToast.showToast(context, "\u60A8\u6B63\u5728\u4E0B\u8F7D\u58F0\u97F3\u5230\u6545\u4E8B\u673A\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5", 0);
            return true;
        } else
        {
            return false;
        }
    }

    public static void playNoAlbum(Context context)
    {
        Logger.d("ximao", "\u6CA1\u6709\u627E\u5230\u4E13\u8F91");
        sendCommand(context, 9, 24);
    }

    public static void playNoData(Context context)
    {
        Logger.d("ximao", "\u6CA1\u6709\u7F51\u7EDC\u6570\u636E");
        sendCommand(context, 9, 21);
    }

    public static void playOK(Context context)
    {
        Logger.d("ximao", "\u6B63\u5728\u51C6\u5907\u64AD\u653E");
        sendCommand(context, 9, 23);
    }

    public static void playSayAgain(Context context)
    {
        Logger.d("ximao", "\u6CA1\u6709\u542C\u6E05\u695A");
        sendCommand(context, 9, 22);
    }

    public static void playSound(final int source)
    {
        (new Thread(new _cls4())).start();
    }

    public static void printByte(String s, byte abyte0[], int i)
    {
        String s1 = "";
        for (int j = 0; j < i; j++)
        {
            s1 = (new StringBuilder()).append(s1).append(String.valueOf(Integer.toHexString(abyte0[j] & 0xff | 0xffffff00).substring(6))).append(" ").toString();
        }

        if (s != null && !s.equals(""))
        {
            Logger.d("ximao", (new StringBuilder()).append(s).append(":").append(s1).toString());
            return;
        } else
        {
            Logger.d("ximao", s1);
            return;
        }
    }

    public static void printPacket(String s, byte abyte0[], int i)
    {
        if (i > 100)
        {
            Logger.d("ximao", (new StringBuilder()).append(s).append(" : ").append("Data Lenth:").append(i).toString());
            return;
        }
        String s1 = "";
        for (int j = 0; j < i; j++)
        {
            s1 = (new StringBuilder()).append(s1).append(String.valueOf(Integer.toHexString(abyte0[j] & 0xff | 0xffffff00).substring(6))).append(" ").toString();
        }

        Logger.d("ximao", (new StringBuilder()).append(s).append(" : ").append(s1).toString());
    }

    public static void removeBTDevConnectionReceiver(Context context)
    {
    }

    public static void requestA2DPCon(Context context)
    {
        sendCommand(context, 9, 34);
        cleanPlayPosition();
    }

    public static void sendCommand(final Context mCon, final int HEAD, final int TYPE)
    {
        if (needFilterWhenDownloading(mCon, TYPE))
        {
            return;
        } else
        {
            (new Thread(new _cls1())).start();
            return;
        }
    }

    public static void sendPacket(Context context, int i, int j, byte abyte0[])
    {
        if (needFilterWhenDownloading(context, j))
        {
            return;
        } else
        {
            sendPacket(context, i, j, abyte0, abyte0.length);
            return;
        }
    }

    public static void sendPacket(final Context mCon, final int HEAD, final int TYPE, final byte payload[], final int length)
    {
        if (needFilterWhenDownloading(mCon, TYPE))
        {
            return;
        } else
        {
            (new Thread(new _cls2())).start();
            return;
        }
    }

    public static void sendPacket(final Context mCon, final byte payload[], final int length)
    {
        (new Thread(new _cls3())).start();
    }

    public static void setDownloadInterrupt(int i, Context context)
    {
        SharedPreferencesUtil.getInstance(context).saveInt("P_LOCK_XIMAO_DOWNLOAD_INTERRUPT", i);
    }

    public static void setDownloadPayload(int i, Context context)
    {
        SharedPreferencesUtil.getInstance(context).saveInt("P_LOCK_XIMAO_DOWNLOAD_PAYLOAD", i);
    }

    public static void setLockBind(boolean flag, Context context)
    {
        SharedPreferencesUtil.getInstance(context).saveBoolean("P_LOCK_XIMAO_BIND", flag);
    }

    public static void setLockVoiceSearch(boolean flag, Context context)
    {
        SharedPreferencesUtil.getInstance(context).saveBoolean("P_LOCK_XIMAO_VOICE_SEARCH", flag);
    }

    public static void setmBTDevDiscoveryReceiver(Context context, XiMaoBTManager.BTDevDiscoveryReceiver btdevdiscoveryreceiver)
    {
        XiMaoBTManager.getInstance(context).setmBTDevDiscoveryReceiver(btdevdiscoveryreceiver);
    }

    static 
    {
        mPlayMode = PLAYMODE.REPEAT;
    }

    private class _cls4
        implements Runnable
    {

        final int val$source;

        public void run()
        {
            MediaPlayer mediaplayer = MediaPlayer.create(MyApplication.a(), source);
            try
            {
                Thread.sleep(5000L);
                mediaplayer.prepare();
                mediaplayer.setVolume(1.0F, 1.0F);
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
            mediaplayer.start();
        }

        _cls4()
        {
            source = i;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final int val$HEAD;
        final int val$TYPE;
        final Context val$mCon;

        public void run()
        {
            XiMaoBTManager ximaobtmanager = XiMaoBTManager.getInstance(mCon);
            byte abyte0[] = new byte[1024];
            abyte0[0] = (byte)HEAD;
            abyte0[1] = (byte)TYPE;
            abyte0[2] = 0;
            abyte0[3] = 0;
            abyte0[4] = XiMaoComm.getCheckSum(abyte0, 4);
            ximaobtmanager.sendPacket(abyte0, 5);
        }

        _cls1()
        {
            mCon = context;
            HEAD = i;
            TYPE = j;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final int val$HEAD;
        final int val$TYPE;
        final int val$length;
        final Context val$mCon;
        final byte val$payload[];

        public void run()
        {
            XiMaoBTManager ximaobtmanager = XiMaoBTManager.getInstance(mCon);
            byte abyte0[] = new byte[1024];
            abyte0[0] = (byte)HEAD;
            abyte0[1] = (byte)TYPE;
            abyte0[2] = (byte)(length >> 8 & 0xff);
            abyte0[3] = (byte)(length & 0xff);
            System.arraycopy(payload, 0, abyte0, 4, length);
            abyte0[length + 4] = XiMaoComm.getCheckSum(abyte0, length + 4);
            ximaobtmanager.sendPacket(abyte0, length + 4 + 1);
        }

        _cls2()
        {
            mCon = context;
            HEAD = i;
            TYPE = j;
            length = k;
            payload = abyte0;
            super();
        }
    }


    private class _cls3
        implements Runnable
    {

        final int val$length;
        final Context val$mCon;
        final byte val$payload[];

        public void run()
        {
            XiMaoBTManager.getInstance(mCon).sendPacket(payload, length);
        }

        _cls3()
        {
            mCon = context;
            payload = abyte0;
            length = i;
            super();
        }
    }

}
