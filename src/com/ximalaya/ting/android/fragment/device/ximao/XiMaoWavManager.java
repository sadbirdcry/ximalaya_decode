// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.content.Context;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoComm

public class XiMaoWavManager
{

    static ArrayList buffers = null;
    static ByteArrayOutputStream bytebuff = new ByteArrayOutputStream(0x927c00);
    static ByteArrayOutputStream databuff = new ByteArrayOutputStream(0x927c00);
    static ByteArrayOutputStream headbuff = new ByteArrayOutputStream(1024);
    public static boolean isrecording = false;
    static int toaldatasize = 0;
    static int toalheadsize = 0;
    static int toalmydatasize = 0;

    public XiMaoWavManager()
    {
    }

    public static void cleanUp()
    {
        toalmydatasize = 0;
        databuff = new ByteArrayOutputStream(0x927c00);
    }

    public static void finishAudio(Context context)
    {
        if (toalmydatasize > 98)
        {
            context = new byte[44];
            System.arraycopy(databuff.toByteArray(), toalmydatasize - 5 - 44, context, 0, 44);
            byte abyte0[] = new byte[toalmydatasize - 44 - 5 - 5 - 44];
            System.arraycopy(databuff.toByteArray(), 44, abyte0, 0, toalmydatasize - 44 - 5 - 5 - 44);
            buffers = new ArrayList();
            buffers.add(context);
            buffers.add(abyte0);
            return;
        }
        try
        {
            XiMaoComm.playSayAgain(context);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
        return;
    }

    public static void writeAudio(byte abyte0[], int i)
    {
        toaldatasize += i;
        bytebuff.write(abyte0, 0, i);
    }

    public static void writeAudio(byte abyte0[], int i, int j)
    {
        toaldatasize += j - i;
        bytebuff.write(abyte0, i, j);
    }

    public static void writeData(byte abyte0[], int i)
    {
        toalmydatasize += i;
        databuff.write(abyte0, 0, i);
    }

    public static void writeData(byte abyte0[], int i, int j)
    {
        toalmydatasize += j - i;
        databuff.write(abyte0, i, j);
    }

    public static void writeHead(byte abyte0[], int i)
    {
        toalheadsize += i;
        headbuff.write(abyte0, 0, i);
    }

    public static void writeHead(byte abyte0[], int i, int j)
    {
        toalheadsize += j - i;
        headbuff.write(abyte0, i, j);
    }

    public void writeToWave()
    {
    }

}
