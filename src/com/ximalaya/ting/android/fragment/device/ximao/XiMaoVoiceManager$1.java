// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import com.iflytek.cloud.SpeechRecognizer;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoVoiceManager

class val.buffers
    implements Runnable
{

    final XiMaoVoiceManager this$0;
    final ArrayList val$buffers;

    public void run()
    {
        initVoice();
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("engine_type", "cloud");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("result_type", "json");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("language", "zh_cn");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("accent", "mandarin");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("vad_bos", "10000");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("vad_eos", "10000");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("asr_ptt", "1");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("domain", "iat");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("audio_source", "-1");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).setParameter("sample_rate", "8000");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).startListening(XiMaoVoiceManager.access$100(XiMaoVoiceManager.this));
        Logger.d("MscSpeechLog", "\u5F00\u59CB\u8BC6\u522B\u8BED\u97F3");
        int i = 0;
        while (i < val$buffers.size()) 
        {
            try
            {
                XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).writeAudio((byte[])val$buffers.get(i), 0, ((byte[])val$buffers.get(i)).length);
                Thread.sleep(40L);
            }
            catch (InterruptedException interruptedexception)
            {
                interruptedexception.printStackTrace();
            }
            i++;
        }
        Logger.d("MscSpeechLog", "\u7ED3\u675F\u8BC6\u522B\u8BED\u97F3");
        XiMaoVoiceManager.access$000(XiMaoVoiceManager.this).stopListening();
    }

    I()
    {
        this$0 = final_ximaovoicemanager;
        val$buffers = ArrayList.this;
        super();
    }
}
