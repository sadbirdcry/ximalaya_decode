// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.util.Log;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoMainHandler, XiMaoVoiceManager, XiMaoComm

class val.apiParams extends MyAsyncTask
{

    final XiMaoMainHandler this$0;
    final long val$albumId;
    final HashMap val$apiParams;
    final String val$urlString;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
label0:
        {
            List list = XiMaoVoiceManager.getInstance(mAppContext).loadSoundListData(val$albumId);
            if (list != null)
            {
                avoid = list;
                if (list.size() != 0)
                {
                    break label0;
                }
            }
            Logger.d(XiMaoMainHandler.access$000(XiMaoMainHandler.this), "tsoundDataList=null");
            avoid = null;
        }
        return avoid;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        if (list != null)
        {
            Log.d(XiMaoMainHandler.access$000(XiMaoMainHandler.this), "\u64AD\u653E\u60C5\u51B51");
            list = ModelHelper.albumSoundlistToSoundInfoList(list);
            XiMaoVoiceManager.getInstance(mAppContext).gotoPlay(null, val$urlString, val$apiParams, 2, 3, list, 0, mAppContext, false, false, true);
            return;
        } else
        {
            XiMaoComm.playNoData(mAppContext);
            return;
        }
    }

    ()
    {
        this$0 = final_ximaomainhandler;
        val$albumId = l;
        val$urlString = s;
        val$apiParams = HashMap.this;
        super();
    }
}
