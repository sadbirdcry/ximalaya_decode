// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.Setting;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.SpeechUtility;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.search.SearchAlbumNew;
import com.ximalaya.ting.android.model.sound.AlbumSoundModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.Utilities;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoComm, JsonParser

public class XiMaoVoiceManager
{
    public static final class VoiceResult extends Enum
    {

        private static final VoiceResult $VALUES[];
        public static final VoiceResult No_notify;
        public static final VoiceResult net_error;
        public static final VoiceResult no_data;
        public static final VoiceResult ok;

        public static VoiceResult valueOf(String s)
        {
            return (VoiceResult)Enum.valueOf(com/ximalaya/ting/android/fragment/device/ximao/XiMaoVoiceManager$VoiceResult, s);
        }

        public static VoiceResult[] values()
        {
            return (VoiceResult[])$VALUES.clone();
        }

        static 
        {
            No_notify = new VoiceResult("No_notify", 0);
            no_data = new VoiceResult("no_data", 1);
            net_error = new VoiceResult("net_error", 2);
            ok = new VoiceResult("ok", 3);
            $VALUES = (new VoiceResult[] {
                No_notify, no_data, net_error, ok
            });
        }

        private VoiceResult(String s, int i)
        {
            super(s, i);
        }
    }


    protected static final String TAG = "MscSpeechLog";
    private static XiMaoVoiceManager mXimaoVoiceManager;
    private final int VT_LOCAL = 1;
    private final int VT_PHONE = 0;
    private final int VT_XIMAO = 2;
    public boolean isShowResult;
    private Context mCon;
    private InitListener mInitListener;
    protected boolean mIsLoading;
    private SpeechRecognizer mSpeechRecognizer;
    private int mVoiceType;
    private RecognizerListener recognizerListener;
    private String result;

    private XiMaoVoiceManager(Context context)
    {
        mVoiceType = 0;
        result = "";
        isShowResult = true;
        mInitListener = new _cls2();
        mIsLoading = false;
        recognizerListener = new _cls5();
        mCon = context;
    }

    private static String get2Search(String s)
    {
        if (s != null && !s.equals(""))
        {
            String as[];
            if (s.contains("\u542C"))
            {
                as = Pattern.compile("\u542C").split(s);
            } else
            if (s.contains("\u64AD\u653E"))
            {
                as = Pattern.compile("\u64AD\u653E").split(s);
            } else
            {
                return s;
            }
            if (as != null && as.length > 0)
            {
                return s.substring(as[0].length() + 1, s.length());
            }
        }
        return null;
    }

    public static XiMaoVoiceManager getInstance(Context context)
    {
        if (mXimaoVoiceManager != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/fragment/device/ximao/XiMaoVoiceManager;
        JVM INSTR monitorenter ;
        if (mXimaoVoiceManager == null)
        {
            mXimaoVoiceManager = new XiMaoVoiceManager(context);
        }
        com/ximalaya/ting/android/fragment/device/ximao/XiMaoVoiceManager;
        JVM INSTR monitorexit ;
_L2:
        return mXimaoVoiceManager;
        context;
        com/ximalaya/ting/android/fragment/device/ximao/XiMaoVoiceManager;
        JVM INSTR monitorexit ;
        throw context;
    }

    public static String ignorePunction(String s)
    {
        String s1 = s.replaceAll("\\,|\\.| |\\;|\\:|\\'|\\\"", "");
        s = s1;
        if (s1.matches((new StringBuilder()).append(".*").append("[\u3002\uFF0C\uFF01\uFF1F\uFF08\uFF09\u300A\u300B\u2026\u2026\u3001\uFF1A\u2014\u2014\u3010\u3011\uFF1B\u2019\u201D\u2018\u201C ]").append(".*").toString()))
        {
            s = s1.replaceAll("[\u3002\uFF0C\uFF01\uFF1F\uFF08\uFF09\u300A\u300B\u2026\u2026\u3001\uFF1A\u2014\u2014\u3010\u3011\uFF1B\u2019\u201D\u2018\u201C ]", "");
        }
        return s;
    }

    private void loadSearch2(final String text)
    {
        if (TextUtils.isEmpty(text))
        {
            return;
        } else
        {
            (new _cls3()).myexec(new Void[0]);
            return;
        }
    }

    private void loadSearch3(final String text)
    {
        if (TextUtils.isEmpty(text))
        {
            return;
        } else
        {
            (new _cls4()).myexec(new Void[0]);
            return;
        }
    }

    private void showToast(String s)
    {
        Logger.d("MscSpeechLog", s);
    }

    public void clearResult()
    {
        result = "";
    }

    protected long getAlbumId2Play(List list)
    {
        int j = (int)(Math.random() * 10D);
        int i = j;
        if (j > list.size() - 1)
        {
            i = list.size() - 1;
        }
        return ((SearchAlbumNew)list.get(i)).id;
    }

    public void gotoPlay(int i, SoundInfo soundinfo, Context context, boolean flag, boolean flag1, boolean flag2)
    {
        if (flag1)
        {
            XiMaoComm.playOK(mCon);
        }
        Logger.d("MscSpeechLog", "\u64AD\u653E\u5355\u6761\u58F0\u97F3");
        SoundInfo soundinfo1;
        String s;
        try
        {
            Thread.sleep(2000L);
        }
        catch (Exception exception) { }
        soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
        s = PlayListControl.getAudioSourceForPlayer(soundinfo);
        if (Utilities.isNotBlank(PlayListControl.getPlayListManager().curPlaySrc) && PlayListControl.getPlayListManager().curPlaySrc.equalsIgnoreCase(s))
        {
            Logger.d("MscSpeechLog", "\u64AD\u653E\u7B2C\u4E00\u79CD\u60C5\u51B5");
            LocalMediaService.getInstance().start();
            return;
        }
        if (soundinfo1 == null || !soundinfo1.equals(soundinfo))
        {
            Logger.d("MscSpeechLog", "\u64AD\u653E\u7B2C\u4E8C\u79CD\u60C5\u51B5");
            PlayTools.gotoPlay(i, soundinfo, context, flag, null);
            LocalMediaService.getInstance().start();
            return;
        } else
        {
            Logger.d("MscSpeechLog", "\u64AD\u653E\u7B2C\u4E09\u79CD\u60C5\u51B5");
            LocalMediaService.getInstance().start();
            return;
        }
    }

    public void gotoPlay(int i, List list, int j, Context context, boolean flag, boolean flag1, boolean flag2)
    {
        if (flag1)
        {
            XiMaoComm.playOK(mCon);
        }
        SoundInfo soundinfo;
        String s;
        try
        {
            Thread.sleep(2000L);
        }
        catch (Exception exception) { }
        Logger.d("MscSpeechLog", "\u64AD\u653E\u58F0\u97F3\u5217\u8868");
        soundinfo = PlayListControl.getPlayListManager().getCurSound();
        s = PlayListControl.getAudioSourceForPlayer((SoundInfo)list.get(0));
        if (Utilities.isNotBlank(PlayListControl.getPlayListManager().curPlaySrc) && PlayListControl.getPlayListManager().curPlaySrc.equalsIgnoreCase(s))
        {
            Logger.d("MscSpeechLog", "\u64AD\u653E\u7B2C\u4E00\u79CD\u60C5\u51B5");
            LocalMediaService.getInstance().start();
            return;
        }
        if (soundinfo == null || !soundinfo.equals(list.get(0)))
        {
            Logger.d("MscSpeechLog", "\u64AD\u653E\u7B2C\u4E8C\u79CD\u60C5\u51B5");
            PlayTools.gotoPlay(i, list, j, context, flag, null);
            LocalMediaService.getInstance().start();
            return;
        } else
        {
            Logger.d("MscSpeechLog", "\u64AD\u653E\u7B2C\u4E09\u79CD\u60C5\u51B5");
            LocalMediaService.getInstance().start();
            return;
        }
    }

    public void gotoPlay(String s, String s1, HashMap hashmap, int i, int j, List list, int k, 
            Context context, boolean flag, boolean flag1, boolean flag2)
    {
        if (flag1)
        {
            XiMaoComm.playOK(mCon);
        }
        String s2;
        try
        {
            Thread.sleep(2000L);
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
        Logger.d("MscSpeechLog", "\u64AD\u653E\u58F0\u97F3\u5217\u8868");
        s = PlayListControl.getPlayListManager().getCurSound();
        s2 = PlayListControl.getAudioSourceForPlayer((SoundInfo)list.get(0));
        if (Utilities.isNotBlank(PlayListControl.getPlayListManager().curPlaySrc) && PlayListControl.getPlayListManager().curPlaySrc.equalsIgnoreCase(s2))
        {
            Logger.d("MscSpeechLog", "\u64AD\u653E\u7B2C\u4E00\u79CD\u60C5\u51B5");
            LocalMediaService.getInstance().start();
            return;
        }
        if (s == null || !s.equals(list.get(0)))
        {
            Logger.d("MscSpeechLog", "\u64AD\u653E\u7B2C\u4E8C\u79CD\u60C5\u51B5");
            PlayTools.gotoPlay(j, s1, i, hashmap, list, k, context, true, null);
            LocalMediaService.getInstance().start();
            return;
        } else
        {
            Logger.d("MscSpeechLog", "\u64AD\u653E\u7B2C\u4E09\u79CD\u60C5\u51B5");
            LocalMediaService.getInstance().start();
            return;
        }
    }

    public void handleVoiceResult(RecognizerResult recognizerresult)
    {
        recognizerresult = JsonParser.parseIatResult(recognizerresult.getResultString());
        Logger.d("MscSpeechLog", (new StringBuilder()).append("handleVoiceResult onResult:").append(recognizerresult).toString());
        result = (new StringBuilder()).append(result).append(recognizerresult).toString();
    }

    public void initInUi(Context context)
    {
        Logger.d("MscSpeechLog", "start");
        Setting.showLogcat(true);
        SpeechUtility.createUtility(context, "appid=549bc3c5");
        Logger.d("MscSpeechLog", "end");
    }

    public void initVoice()
    {
        mSpeechRecognizer = SpeechRecognizer.createRecognizer(mCon, mInitListener);
    }

    public List loadSoundListData(long l)
    {
        Object obj;
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
        obj = (new StringBuilder()).append(((String) (obj))).append(l).append("/").append(true).append("/").append(1).append("/").append(20).toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("albumId", (new StringBuilder()).append(l).append("").toString());
        requestparams.put("isAsc", "true");
        requestparams.put("pageSize", "20");
        obj = f.a().a(((String) (obj)), requestparams, null, null);
        Logger.d("MscSpeechLog", (new StringBuilder()).append("result:").append(((String) (obj))).toString());
        if (obj == null || TextUtils.isEmpty(((CharSequence) (obj))))
        {
            XiMaoComm.playNoData(mCon);
            return null;
        }
        obj = JSON.parseObject(((String) (obj)));
        if (obj == null) goto _L2; else goto _L1
_L1:
        if (!((JSONObject) (obj)).isEmpty()) goto _L3; else goto _L2
_L2:
        XiMaoComm.playNoAlbum(mCon);
        return null;
        Exception exception;
        exception;
        obj = null;
_L6:
        Object obj1;
        XiMaoComm.playNoAlbum(mCon);
        Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
        obj1 = obj;
_L5:
        return ((List) (obj1));
_L3:
        if (!"0".equals(((JSONObject) (obj)).get("ret").toString()))
        {
            break MISSING_BLOCK_LABEL_345;
        }
        obj = ((JSONObject) (obj)).getJSONObject("tracks");
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_301;
        }
        if (!((JSONObject) (obj)).isEmpty())
        {
            break MISSING_BLOCK_LABEL_310;
        }
        XiMaoComm.playNoAlbum(mCon);
        return null;
        obj = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/sound/AlbumSoundModel);
        obj1 = obj;
        if (obj != null) goto _L5; else goto _L4
_L4:
        XiMaoComm.playNoAlbum(mCon);
        return null;
        obj1;
          goto _L6
        obj1 = null;
          goto _L5
    }

    public void readExistVoice()
    {
        writeFileAudio();
    }

    public byte[] readFile(String s)
    {
        FileInputStream fileinputstream = new FileInputStream(s);
        Object obj = fileinputstream;
        s = new byte[fileinputstream.available()];
        obj = fileinputstream;
        fileinputstream.read(s);
        obj = s;
        if (fileinputstream == null)
        {
            break MISSING_BLOCK_LABEL_38;
        }
        fileinputstream.close();
        obj = s;
_L2:
        return ((byte []) (obj));
        Exception exception;
        exception;
        fileinputstream = null;
        s = null;
_L5:
        obj = fileinputstream;
        exception.printStackTrace();
        obj = s;
        if (fileinputstream == null) goto _L2; else goto _L1
_L1:
        fileinputstream.close();
        return s;
        obj;
_L3:
        ((IOException) (obj)).printStackTrace();
        return s;
        s;
        obj = null;
_L4:
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        throw s;
        obj;
          goto _L3
        s;
          goto _L4
        exception;
        s = null;
          goto _L5
        exception;
          goto _L5
    }

    protected void searchAndPlay(String s)
    {
        loadSearch3(s);
    }

    public void setParam()
    {
        mSpeechRecognizer.setParameter("params", null);
        mSpeechRecognizer.setParameter("engine_type", "cloud");
        mSpeechRecognizer.setParameter("result_type", "json");
        mSpeechRecognizer.setParameter("language", "zh_cn");
        mSpeechRecognizer.setParameter("accent", "mandarin");
        mSpeechRecognizer.setParameter("vad_bos", "4000");
        mSpeechRecognizer.setParameter("vad_eos", "1000");
        mSpeechRecognizer.setParameter("asr_ptt", "1");
        switch (mVoiceType)
        {
        case 2: // '\002'
        default:
            return;

        case 0: // '\0'
            mSpeechRecognizer.setParameter("asr_audio_path", (new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/ting/ximao/wavaudio.pcm").toString());
            return;

        case 1: // '\001'
            mSpeechRecognizer.setParameter("audio_source", (new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/ting/record/wavaudio.wav").toString());
            break;
        }
    }

    public ArrayList splitBuffer(byte abyte0[], int i, int j)
    {
        ArrayList arraylist = new ArrayList();
        if (j <= 0 || i <= 0 || abyte0 == null || abyte0.length < i)
        {
            return arraylist;
        }
        for (int k = 0; k < i;)
        {
            int l = i - k;
            if (j < l)
            {
                byte abyte1[] = new byte[j];
                System.arraycopy(abyte0, k, abyte1, 0, j);
                arraylist.add(abyte1);
                k += j;
            } else
            {
                byte abyte2[] = new byte[l];
                System.arraycopy(abyte0, k, abyte2, 0, l);
                arraylist.add(abyte2);
                k += l;
            }
        }

        return arraylist;
    }

    public void startVoice()
    {
        startVoice(0);
    }

    public void startVoice(int i)
    {
        i = mSpeechRecognizer.startListening(recognizerListener);
        if (i != 0)
        {
            Logger.d("MscSpeechLog", (new StringBuilder()).append("startVoice \u542C\u5199\u5931\u8D25,\u9519\u8BEF\u7801\uFF1A").append(i).toString());
            return;
        } else
        {
            Logger.d("MscSpeechLog", (new StringBuilder()).append("startVoice \u5F00\u59CB\u542C\u5199\uFF1A").append(i).toString());
            return;
        }
    }

    public void writeFileAudio()
    {
        try
        {
            byte abyte0[] = readFile((new StringBuilder()).append(Environment.getExternalStorageDirectory()).append("/").append("audio.wav").toString());
            writeaudio(splitBuffer(abyte0, abyte0.length, 1280));
            return;
        }
        catch (Exception exception)
        {
            Logger.e("MscSpeechLog", "\u8BFB\u53D6\u6587\u4EF6\u5931\u8D25");
        }
    }

    public void writeaudio(final ArrayList buffers)
    {
        (new Thread(new _cls1())).start();
    }







/*
    static String access$402(XiMaoVoiceManager ximaovoicemanager, String s)
    {
        ximaovoicemanager.result = s;
        return s;
    }

*/


    private class _cls2
        implements InitListener
    {

        final XiMaoVoiceManager this$0;

        public void onInit(int i)
        {
            Logger.d("MscSpeechLog", (new StringBuilder()).append("SpeechRecognizer init() code = ").append(i).toString());
            if (i != 0)
            {
                showToast("\u521D\u59CB\u5316\u5931\u8D25");
            }
        }

        _cls2()
        {
            this$0 = XiMaoVoiceManager.this;
            super();
        }
    }


    private class _cls5
        implements RecognizerListener
    {

        final XiMaoVoiceManager this$0;

        public void onBeginOfSpeech()
        {
            showToast("\u5F00\u59CB\u8BC6\u522B\u5F55\u97F3");
            result = "";
        }

        public void onEndOfSpeech()
        {
            Log.d("MscSpeechLog", "\u8BC6\u522B\u5F55\u97F3\u7ED3\u675F");
        }

        public void onError(SpeechError speecherror)
        {
            if (speecherror.getErrorCode() >= 20001 && speecherror.getErrorCode() <= 20003 || speecherror.getErrorCode() == 10114 || speecherror.getErrorCode() == 10200)
            {
                Logger.d("MscSpeechLog", (new StringBuilder()).append("No Network").append(speecherror).toString());
                XiMaoComm.playNoData(mCon);
                showToast(speecherror.getErrorDescription());
                return;
            } else
            {
                Logger.d("MscSpeechLog", (new StringBuilder()).append("RecognizerListener onError").append(speecherror).toString());
                XiMaoComm.playSayAgain(mCon);
                showToast(speecherror.getErrorDescription());
                return;
            }
        }

        public void onEvent(int i, int j, int k, Bundle bundle)
        {
        }

        public void onResult(RecognizerResult recognizerresult, boolean flag)
        {
label0:
            {
                handleVoiceResult(recognizerresult);
                if (flag)
                {
                    showToast("\u8BC6\u522B\u5F55\u97F3\u7ED3\u675F");
                    showToast(result);
                    recognizerresult = XiMaoVoiceManager.get2Search(XiMaoVoiceManager.ignorePunction(result));
                    if (isShowResult)
                    {
                        CustomToast.showToast(mCon, (new StringBuilder()).append("\u6B63\u5728\u641C\u7D22\u201C").append(recognizerresult).append("\u201D").toString(), 0);
                    }
                    if (TextUtils.isEmpty(recognizerresult))
                    {
                        break label0;
                    }
                    searchAndPlay(recognizerresult);
                }
                return;
            }
            XiMaoComm.playSayAgain(mCon);
        }

        public void onVolumeChanged(int i)
        {
        }

        _cls5()
        {
            this$0 = XiMaoVoiceManager.this;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final XiMaoVoiceManager this$0;
        final String val$text;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = (new StringBuilder()).append("http://3rd.ximalaya.com/search/albums?i_am=xiyin").append("&q=").append(text).toString();
            avoid = (new StringBuilder()).append(avoid).append("&category_id=6").toString();
            avoid = (new StringBuilder()).append(avoid).append("&page=1").toString();
            avoid = (new StringBuilder()).append(avoid).append("&per_page=20").toString();
            avoid = f.a().a(avoid, null, null, null, false);
            Object obj;
            long l;
            if (((com.ximalaya.ting.android.b.n.a) (avoid)).b == 1)
            {
                avoid = ((com.ximalaya.ting.android.b.n.a) (avoid)).a;
            } else
            {
                avoid = null;
            }
            Logger.d("MscSpeechLog", (new StringBuilder()).append("loadSearch onSuccess:").append(avoid).toString());
            if (TextUtils.isEmpty(avoid))
            {
                XiMaoComm.playNoData(mCon);
                return null;
            }
            try
            {
                avoid = JSON.parseObject(avoid);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
                avoid = null;
            }
            if (avoid == null)
            {
                XiMaoComm.playNoAlbum(mCon);
                return null;
            }
            avoid = avoid.getString("albums");
            if (TextUtils.isEmpty(avoid))
            {
                XiMaoComm.playNoAlbum(mCon);
                return null;
            }
            try
            {
                avoid = JSON.parseArray(avoid, com/ximalaya/ting/android/model/search/SearchAlbumNew);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
                avoid = null;
            }
            if (avoid == null || avoid.size() == 0)
            {
                XiMaoComm.playNoAlbum(mCon);
                return null;
            }
            l = getAlbumId2Play(avoid);
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").append(l).append("/").append(true).append("/").append(1).append("/").append(15).toString();
            avoid = f.a().a(avoid, null, null, null, false);
            if (((com.ximalaya.ting.android.b.n.a) (avoid)).b == 1)
            {
                avoid = ((com.ximalaya.ting.android.b.n.a) (avoid)).a;
            } else
            {
                avoid = null;
            }
            Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
            avoid = JSON.parseObject(avoid);
            if (!"0".equals(avoid.get("ret").toString())) goto _L2; else goto _L1
_L1:
            avoid = JSON.parseArray(avoid.getJSONObject("tracks").getString("list"), com/ximalaya/ting/android/model/sound/AlbumSoundModel);
            obj = avoid;
            if (avoid != null) goto _L4; else goto _L3
_L3:
            obj = new ArrayList();
            avoid = ((Void []) (obj));
_L7:
            if (avoid == null || avoid.size() == 0)
            {
                XiMaoComm.playNoAlbum(mCon);
                Logger.d("MscSpeechLog", "tsoundDataList=null");
                return null;
            } else
            {
                return ModelHelper.albumSoundlistToSoundInfoList(avoid);
            }
            obj;
            avoid = null;
_L5:
            XiMaoComm.playNoAlbum(mCon);
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
            obj = avoid;
_L4:
            avoid = ((Void []) (obj));
            continue; /* Loop/switch isn't completed */
            obj;
            if (true) goto _L5; else goto _L2
_L2:
            avoid = null;
            if (true) goto _L7; else goto _L6
_L6:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            mIsLoading = false;
            if (list == null || list.size() == 0)
            {
                return;
            } else
            {
                Logger.d("MscSpeechLog", "\u5F00\u59CB\u64AD\u653E");
                int i = (int)(Math.random() * (double)(list.size() - 1));
                gotoPlay(0, list, i, mCon, false, false, false);
                return;
            }
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            mIsLoading = true;
        }

        _cls3()
        {
            this$0 = XiMaoVoiceManager.this;
            text = s;
            super();
        }
    }


    private class _cls4 extends MyAsyncTask
    {

        final XiMaoVoiceManager this$0;
        final String val$text;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            String s = (new StringBuilder()).append("http://3rd.ximalaya.com/search/tracks?i_am=xiyin").append("&q=").append(text).toString();
            avoid = s;
            if (XiMaoComm.isLockVoiceSearch(mCon))
            {
                avoid = (new StringBuilder()).append(s).append("&category_id=6").toString();
            }
            avoid = (new StringBuilder()).append(avoid).append("&page=1").toString();
            avoid = (new StringBuilder()).append(avoid).append("&per_page=20").toString();
            avoid = f.a().a(avoid, null, null, null, false);
            if (((com.ximalaya.ting.android.b.n.a) (avoid)).b == 1)
            {
                avoid = ((com.ximalaya.ting.android.b.n.a) (avoid)).a;
            } else
            {
                avoid = null;
            }
            Logger.d("MscSpeechLog", (new StringBuilder()).append("loadSearch onSuccess:").append(avoid).toString());
            if (TextUtils.isEmpty(avoid))
            {
                XiMaoComm.playNoData(mCon);
                return null;
            }
            try
            {
                avoid = JSON.parseObject(avoid);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
                avoid = null;
            }
            if (avoid == null)
            {
                XiMaoComm.playNoAlbum(mCon);
                return null;
            }
            avoid = avoid.getString("tracks");
            if (TextUtils.isEmpty(avoid))
            {
                XiMaoComm.playNoAlbum(mCon);
                return null;
            }
            try
            {
                avoid = JSON.parseArray(avoid, com/ximalaya/ting/android/model/search/SearchSoundNew);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
                avoid = null;
            }
            if (avoid == null || avoid.size() == 0)
            {
                XiMaoComm.playNoAlbum(mCon);
                return null;
            } else
            {
                return ModelHelper.searchSoundToSoundInfoList2(avoid);
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            mIsLoading = false;
            if (list == null || list.size() == 0)
            {
                return;
            }
            Logger.d("MscSpeechLog", "\u5F00\u59CB\u64AD\u653E");
            int i;
            if (list.size() >= 5)
            {
                i = (int)(Math.random() * 5D);
            } else
            {
                i = (int)(Math.random() * (double)list.size());
            }
            gotoPlay(0, list, i, mCon, false, true, false);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            mIsLoading = true;
        }

        _cls4()
        {
            this$0 = XiMaoVoiceManager.this;
            text = s;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final XiMaoVoiceManager this$0;
        final ArrayList val$buffers;

        public void run()
        {
            initVoice();
            mSpeechRecognizer.setParameter("engine_type", "cloud");
            mSpeechRecognizer.setParameter("result_type", "json");
            mSpeechRecognizer.setParameter("language", "zh_cn");
            mSpeechRecognizer.setParameter("accent", "mandarin");
            mSpeechRecognizer.setParameter("vad_bos", "10000");
            mSpeechRecognizer.setParameter("vad_eos", "10000");
            mSpeechRecognizer.setParameter("asr_ptt", "1");
            mSpeechRecognizer.setParameter("domain", "iat");
            mSpeechRecognizer.setParameter("audio_source", "-1");
            mSpeechRecognizer.setParameter("sample_rate", "8000");
            mSpeechRecognizer.startListening(recognizerListener);
            Logger.d("MscSpeechLog", "\u5F00\u59CB\u8BC6\u522B\u8BED\u97F3");
            int i = 0;
            while (i < buffers.size()) 
            {
                try
                {
                    mSpeechRecognizer.writeAudio((byte[])buffers.get(i), 0, ((byte[])buffers.get(i)).length);
                    Thread.sleep(40L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
                i++;
            }
            Logger.d("MscSpeechLog", "\u7ED3\u675F\u8BC6\u522B\u8BED\u97F3");
            mSpeechRecognizer.stopListening();
        }

        _cls1()
        {
            this$0 = XiMaoVoiceManager.this;
            buffers = arraylist;
            super();
        }
    }

}
