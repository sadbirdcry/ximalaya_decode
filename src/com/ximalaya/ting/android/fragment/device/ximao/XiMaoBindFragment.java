// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.shu.CategoryFragment;
import com.ximalaya.ting.android.fragment.finding2.category.CategoryTagFragmentNew;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.lang.ref.WeakReference;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            KeyInfo, XiMaoComm, XiMaoBTManager

public class XiMaoBindFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    static class MyHandler extends Handler
    {

        WeakReference mFragment;

        public void handleMessage(Message message)
        {
            XiMaoBindFragment ximaobindfragment = (XiMaoBindFragment)mFragment.get();
            if (ximaobindfragment.isAdded())
            {
                ximaobindfragment.onMessage(message);
            }
        }

        MyHandler(XiMaoBindFragment ximaobindfragment)
        {
            mFragment = new WeakReference(ximaobindfragment);
        }
    }


    private static final int MSG_BTDEV_CONNECTED = 1;
    private static final int MSG_BTDEV_DISCONNECTED = 2;
    private static final int MSG_BTDEV_PACKET = 5;
    private static final int MSG_BTDEV_PACKET_COMMAND = 6;
    private static final int MSG_BTDEV_RECEIVED = 3;
    private static final int MSG_BTDEV_SENT = 4;
    public MyHandler MsgHandler;
    private String TAG;
    LinearLayout mCollect1;
    TextView mCollect1Num;
    TextView mCollect1Text;
    LinearLayout mCollect2;
    TextView mCollect2Num;
    TextView mCollect2Text;
    LinearLayout mCollect3;
    TextView mCollect3Num;
    TextView mCollect3Text;
    RelativeLayout mContentView;
    RelativeLayout mKey1;
    TextView mKey1Name;
    TextView mKey2Name;
    TextView mKey3Name;

    public XiMaoBindFragment()
    {
        TAG = "ximao";
        MsgHandler = new MyHandler(this);
    }

    private void freshBindLayout(KeyInfo keyinfo)
    {
        if (keyinfo == null)
        {
            return;
        }
        switch (keyinfo.keyNum)
        {
        default:
            return;

        case 1: // '\001'
            mCollect1Text.setText(keyinfo.keyAlbumName);
            mCollect1Num.setText((new StringBuilder()).append(keyinfo.trackCount).append("\u9996").toString());
            return;

        case 2: // '\002'
            mCollect2Text.setText(keyinfo.keyAlbumName);
            mCollect2Num.setText((new StringBuilder()).append(keyinfo.trackCount).append("\u9996").toString());
            return;

        case 3: // '\003'
            mCollect3Text.setText(keyinfo.keyAlbumName);
            break;
        }
        mCollect3Num.setText((new StringBuilder()).append(keyinfo.trackCount).append("\u9996").toString());
    }

    private void initOneAlbumInfo(final KeyInfo keyInfo)
    {
        while (keyInfo == null || !ToolUtil.isConnectToNetwork(mCon)) 
        {
            return;
        }
        (new _cls1()).myexec(new Void[0]);
    }

    private void initUi()
    {
        mCollect1 = (LinearLayout)mContentView.findViewById(0x7f0a03a8);
        mCollect1.setOnClickListener(this);
        mCollect2 = (LinearLayout)mContentView.findViewById(0x7f0a03b0);
        mCollect2.setOnClickListener(this);
        mCollect3 = (LinearLayout)mContentView.findViewById(0x7f0a03b8);
        mCollect3.setOnClickListener(this);
        mCollect1Text = (TextView)mContentView.findViewById(0x7f0a03a7);
        mCollect2Text = (TextView)mContentView.findViewById(0x7f0a03af);
        mCollect3Text = (TextView)mContentView.findViewById(0x7f0a03b7);
        mCollect1Num = (TextView)mContentView.findViewById(0x7f0a03a9);
        mCollect2Num = (TextView)mContentView.findViewById(0x7f0a03b1);
        mCollect3Num = (TextView)mContentView.findViewById(0x7f0a03b9);
        mKey1Name = (TextView)mContentView.findViewById(0x7f0a03a6);
        mKey2Name = (TextView)mContentView.findViewById(0x7f0a03ae);
        mKey3Name = (TextView)mContentView.findViewById(0x7f0a03b6);
        mKey1Name.setText("\u300C\u6545\u4E8B\u952E\u300D");
        mKey2Name.setText("\u300C\u6027\u683C\u57F9\u517B\u952E\u300D");
        mKey3Name.setText("\u300C\u513F\u6B4C\u952E\u300D");
    }

    private boolean isEmptyName(String s)
    {
        while (TextUtils.isEmpty(s) || s.equals("\u672A\u83B7\u53D6\u5230\u7ED1\u5B9A\u4E13\u8F91")) 
        {
            return true;
        }
        return false;
    }

    public void goCategoryFragment(int i)
    {
        Bundle bundle;
        bundle = new Bundle();
        bundle.putInt("key", i);
        bundle.putBoolean("showFocus", false);
        bundle.putInt("from", 5);
        if (!XiMaoComm.isLockBind(mCon)) goto _L2; else goto _L1
_L1:
        Object obj;
        obj = XiMaoComm.loadCategoryInfo(mCon);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_209;
        }
        i = 0;
_L5:
        if (i >= ((List) (obj)).size())
        {
            break MISSING_BLOCK_LABEL_209;
        }
        if (!((FindingCategoryModel)((List) (obj)).get(i)).getTitle().equals("\u513F\u7AE5")) goto _L4; else goto _L3
_L3:
        obj = (FindingCategoryModel)((List) (obj)).get(i);
_L6:
        if (obj != null)
        {
            bundle.putString("title", ((FindingCategoryModel) (obj)).getTitle());
            bundle.putString("categoryId", (new StringBuilder()).append(((FindingCategoryModel) (obj)).getId()).append("").toString());
            bundle.putString("categoryName", ((FindingCategoryModel) (obj)).getName());
            bundle.putString("contentType", "album");
            bundle.putBoolean("isSerialized", ((FindingCategoryModel) (obj)).isFinished());
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(mCollect1));
            startFragment(com/ximalaya/ting/android/fragment/finding2/category/CategoryTagFragmentNew, bundle);
        }
        return;
_L4:
        i++;
          goto _L5
_L2:
        startFragment(com/ximalaya/ting/android/fragment/device/shu/CategoryFragment, bundle);
        return;
        obj = null;
          goto _L6
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initUi();
    }

    public void onClick(View view)
    {
        MyDeviceManager.getInstance(mActivity.getApplicationContext()).setNowDeviceType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.ximao);
        switch (view.getId())
        {
        case 2131362724: 
        default:
            return;

        case 2131362728: 
            goCategoryFragment(1);
            return;

        case 2131362736: 
            goCategoryFragment(2);
            return;

        case 2131362744: 
            goCategoryFragment(3);
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300e1, viewgroup, false);
        return mContentView;
    }

    public void onMessage(Message message)
    {
        byte abyte0[] = (byte[])(byte[])message.obj;
        switch (message.what)
        {
        case 6: // '\006'
            byte byte0 = ((byte[])(byte[])message.obj)[0];
            return;

        case 5: // '\005'
            message = (byte[])(byte[])message.obj;
            switch (message[0])
            {
            default:
                return;

            case 10: // '\n'
                switch (message[1])
                {
                default:
                    return;

                case 21: // '\025'
                    break;
                }
                if (XiMaoBTManager.getInstance(mCon).getPacketSize(message) == 8)
                {
                    long l = XiMaoComm.bytes2long(message, 4);
                    XiMaoBTManager.getInstance(mCon).keyInfo3 = new KeyInfo(3, null, l, 0);
                    XiMaoBTManager.getInstance(mCon).keyInfo3.keyAlbumId = l;
                    XiMaoBTManager.getInstance(mCon).keyInfo3.keyAlbumName = "\u672A\u80FD\u83B7\u53D6\u4E13\u8F91\u4FE1\u606F";
                    freshBindLayout(XiMaoBTManager.getInstance(mCon).keyInfo3);
                    initOneAlbumInfo(XiMaoBTManager.getInstance(mCon).keyInfo3);
                    return;
                }
                break;

            case 11: // '\013'
                switch (message[1])
                {
                default:
                    return;

                case 21: // '\025'
                    break;
                }
                if (XiMaoBTManager.getInstance(mCon).getPacketSize(message) == 8)
                {
                    long l1 = XiMaoComm.bytes2long(message, 4);
                    XiMaoBTManager.getInstance(mCon).keyInfo1 = new KeyInfo(1, null, l1, 0);
                    XiMaoBTManager.getInstance(mCon).keyInfo1.keyAlbumId = l1;
                    XiMaoBTManager.getInstance(mCon).keyInfo1.keyAlbumName = "\u672A\u80FD\u83B7\u53D6\u4E13\u8F91\u4FE1\u606F";
                    freshBindLayout(XiMaoBTManager.getInstance(mCon).keyInfo1);
                    initOneAlbumInfo(XiMaoBTManager.getInstance(mCon).keyInfo1);
                    return;
                }
                break;

            case 19: // '\023'
                switch (message[1])
                {
                default:
                    return;

                case 21: // '\025'
                    break;
                }
                if (XiMaoBTManager.getInstance(mCon).getPacketSize(message) == 8)
                {
                    long l2 = XiMaoComm.bytes2long(message, 4);
                    XiMaoBTManager.getInstance(mCon).keyInfo2 = new KeyInfo(2, null, l2, 0);
                    XiMaoBTManager.getInstance(mCon).keyInfo2.keyAlbumId = l2;
                    XiMaoBTManager.getInstance(mCon).keyInfo2.keyAlbumName = "\u672A\u80FD\u83B7\u53D6\u4E13\u8F91\u4FE1\u606F";
                    freshBindLayout(XiMaoBTManager.getInstance(mCon).keyInfo2);
                    initOneAlbumInfo(XiMaoBTManager.getInstance(mCon).keyInfo2);
                    return;
                }
                break;
            }
            break;
        }
        while (true) 
        {
            return;
        }
    }

    public void onResume()
    {
        super.onResume();
        Logger.d(TAG, "onResume");
        if (!isEmptyName(XiMaoBTManager.getInstance(mCon).keyInfo1.keyAlbumName))
        {
            freshBindLayout(XiMaoBTManager.getInstance(mCon).keyInfo1);
        }
        if (!isEmptyName(XiMaoBTManager.getInstance(mCon).keyInfo2.keyAlbumName))
        {
            freshBindLayout(XiMaoBTManager.getInstance(mCon).keyInfo2);
        }
        if (!isEmptyName(XiMaoBTManager.getInstance(mCon).keyInfo3.keyAlbumName))
        {
            freshBindLayout(XiMaoBTManager.getInstance(mCon).keyInfo3);
        }
        if (!ToolUtil.isConnectToNetwork(mCon))
        {
            CustomToast.showToast(mCon, "\u7F51\u7EDC\u4E0D\u7ED9\u529B", 0);
            return;
        } else
        {
            initOneAlbumInfo(XiMaoBTManager.getInstance(mCon).keyInfo1);
            initOneAlbumInfo(XiMaoBTManager.getInstance(mCon).keyInfo2);
            initOneAlbumInfo(XiMaoBTManager.getInstance(mCon).keyInfo3);
            return;
        }
    }



    private class _cls1 extends MyAsyncTask
    {

        final XiMaoBindFragment this$0;
        final KeyInfo val$keyInfo;

        protected transient AlbumModel doInBackground(Void avoid[])
        {
label0:
            {
                avoid = new RequestParams();
                String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
                s = (new StringBuilder()).append(s).append(keyInfo.keyAlbumId).append("/").append(true).append("/").append(1).append("/").append(1).toString();
                avoid = f.a().a(s, avoid, null, null);
                Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
                try
                {
                    avoid = JSON.parseObject(avoid);
                    if (!"0".equals(avoid.get("ret").toString()))
                    {
                        break label0;
                    }
                    avoid = (AlbumModel)JSON.parseObject(avoid.getString("album"), com/ximalaya/ting/android/model/album/AlbumModel);
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    return null;
                }
                return avoid;
            }
            return null;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(AlbumModel albummodel)
        {
            while (!isAdded() ||  == null) 
            {
                return;
            }
            if (albummodel == null)
            {
                albummodel = keyInfo;
            } else
            {
                albummodel = new KeyInfo(keyInfo.keyNum, albummodel.title, albummodel.albumId, albummodel.tracks);
                XiMaoBTManager.getInstance(mCon).keyInfoTemp = albummodel;
                XiMaoBTManager.getInstance(mCon).saveKeyInfo();
            }
            freshBindLayout(albummodel);
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((AlbumModel)obj);
        }

        _cls1()
        {
            this$0 = XiMaoBindFragment.this;
            keyInfo = keyinfo;
            super();
        }
    }

}
