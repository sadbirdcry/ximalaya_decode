// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.ManageFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.transaction.download.LocalSoundCreateDescSorter;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.PlaylistFromDownload;
import com.ximalaya.ting.android.util.ToolUtil;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoBTManager, XiMaoListFragment, XiMaoContentFragment, XiMaoComm, 
//            XiMaoVoiceManager

public class XiMaoMainHandler extends Handler
{

    private final String TAG = getClass().getName();
    private long albumId;
    private boolean isA2DPConnected;
    private boolean isPlayAlbum;
    MainTabActivity2 mActivity;
    Context mAppContext;
    SoundInfo si;

    public XiMaoMainHandler(Activity activity)
    {
        mAppContext = null;
        mActivity = null;
        isPlayAlbum = false;
        isA2DPConnected = true;
        si = null;
        if (activity == null)
        {
            if (MyApplication.a() == null);
        }
        if (activity != null)
        {
            mAppContext = activity.getApplicationContext();
            if (activity instanceof MainTabActivity2)
            {
                mActivity = (MainTabActivity2)activity;
            }
            return;
        } else
        {
            mAppContext = MyApplication.b();
            return;
        }
    }

    private MainTabActivity2 getActivity()
    {
        if (mActivity != null)
        {
            return mActivity;
        }
        if (MyApplication.a() != null && (MyApplication.a() instanceof MainTabActivity2))
        {
            return (MainTabActivity2)MyApplication.a();
        } else
        {
            return null;
        }
    }

    public void handleMessage(Message message)
    {
        message.what;
        JVM INSTR tableswitch 2 5: default 36
    //                   2 833
    //                   3 36
    //                   4 36
    //                   5 37;
           goto _L1 _L2 _L1 _L1 _L3
_L5:
        return;
_L3:
        Log.d("ximao", "\u6536\u5230Pakcet2");
        byte abyte0[] = (byte[])(byte[])message.obj;
        int i = XiMaoBTManager.getInstance(mAppContext).getPacketSize(abyte0);
label0:
        switch (abyte0[0])
        {
        case 10: // '\n'
        case 11: // '\013'
        case 12: // '\f'
        case 15: // '\017'
        case 16: // '\020'
        default:
            return;

        case 14: // '\016'
            break; /* Loop/switch isn't completed */

        case 9: // '\t'
            switch (abyte0[1])
            {
            default:
                return;

            case 34: // '"'
                break label0;

            case 32: // ' '
                isA2DPConnected = true;
                if (isPlayAlbum)
                {
                    Logger.d("ximao", "a2dp\u5DF2\u8FDE\u63A5\uFF0C\u51C6\u5907\u64AD\u653E");
                    isPlayAlbum = false;
                    double d;
                    int j;
                    try
                    {
                        Thread.sleep(2000L);
                    }
                    // Misplaced declaration of an exception variable
                    catch (Message message) { }
                    playAlbum(albumId);
                    return;
                }
                break;

            case 33: // '!'
                isA2DPConnected = false;
                return;

            case 35: // '#'
                XiMaoComm.cleanPlayPosition();
                return;
            }
            break;

        case 17: // '\021'
            if (abyte0[1] == 17)
            {
                d = ((long)abyte0[4] & 255L) << (int)(24L + (((long)abyte0[5] & 255L) << 16) + (((long)abyte0[6] & 255L) << 8) + ((long)abyte0[7] & 255L));
                Logger.d("ximao", (new StringBuilder()).append("storage:").append(d).toString());
                XiMaoBTManager.getInstance(mAppContext).setStorage(d);
                return;
            }
            break;

        case 13: // '\r'
            if (getActivity() == null)
            {
                break;
            }
            i = getActivity().getManageFragment().mStacks.size();
            message = null;
            if (i > 0)
            {
                message = (Fragment)((SoftReference)getActivity().getManageFragment().mStacks.get(i - 1)).get();
            }
            if (message == null || (message instanceof XiMaoListFragment))
            {
                break;
            }
            switch (abyte0[1])
            {
            case 7: // '\007'
            case 8: // '\b'
            case 9: // '\t'
            case 10: // '\n'
            case 11: // '\013'
            case 12: // '\f'
            default:
                return;

            case 4: // '\004'
                XiMaoContentFragment.checkPlayMode = false;
                XiMaoComm.mPlayMode = XiMaoComm.PLAYMODE.REPEAT;
                Log.d("ximao", "\u5F53\u524D\u64AD\u653E\u6A21\u5F0F\u4E3A\u5FAA\u73AF\u64AD\u653E");
                return;

            case 5: // '\005'
                XiMaoContentFragment.checkPlayMode = false;
                XiMaoComm.mPlayMode = XiMaoComm.PLAYMODE.SHUFFLE;
                Log.d("ximao", "\u5F53\u524D\u64AD\u653E\u6A21\u5F0F\u4E3A\u968F\u673A\u64AD\u653E");
                return;

            case 6: // '\006'
                XiMaoContentFragment.checkPlayMode = false;
                XiMaoComm.mPlayMode = XiMaoComm.PLAYMODE.ORDER;
                Log.d("ximao", "\u5F53\u524D\u64AD\u653E\u6A21\u5F0F\u4E3A\u987A\u5E8F\u64AD\u653E");
                return;

            case 13: // '\r'
                if (!XiMaoListFragment.storePlayPosition)
                {
                    XiMaoComm.cleanPlayPosition();
                    return;
                }
                j = XiMaoListFragment.getCurrentNum(mAppContext);
                i = j;
                if (j < 0)
                {
                    i = XiMaoBTManager.getInstance(mAppContext).itemsList.size();
                }
                if (XiMaoComm.mPlayMode == XiMaoComm.PLAYMODE.REPEAT)
                {
                    XiMaoListFragment.mPlayPositionNow = ((XiMaoListFragment.mPlayPositionNow - 1) + i) % i;
                    return;
                }
                if (XiMaoComm.mPlayMode == XiMaoComm.PLAYMODE.SHUFFLE)
                {
                    XiMaoListFragment.mPlayPositionNow = -100;
                    return;
                }
                break;

            case 14: // '\016'
                if (!XiMaoListFragment.storePlayPosition)
                {
                    XiMaoComm.cleanPlayPosition();
                    return;
                }
                j = XiMaoListFragment.getCurrentNum(mAppContext);
                i = j;
                if (j < 0)
                {
                    i = XiMaoBTManager.getInstance(mAppContext).itemsList.size();
                }
                if (XiMaoComm.mPlayMode == XiMaoComm.PLAYMODE.REPEAT)
                {
                    XiMaoListFragment.mPlayPositionNow = (XiMaoListFragment.mPlayPositionNow + 1) % i;
                    return;
                }
                continue; /* Loop/switch isn't completed */
            }
            break;
        }
_L1:
        if (true) goto _L5; else goto _L4
        if (XiMaoComm.mPlayMode != XiMaoComm.PLAYMODE.SHUFFLE) goto _L5; else goto _L6
_L6:
        XiMaoListFragment.mPlayPositionNow = -100;
        return;
_L4:
        switch (abyte0[1])
        {
        default:
            return;

        case 7: // '\007'
            switch (i)
            {
            default:
                Log.d("ximao", "\u5F00\u59CB\u89E3\u6790\u4E13\u8F91ID");
                LocalMediaService.getInstance().pause();
                albumId = XiMaoComm.bytes2long(abyte0, 4);
                if (android.os.Build.VERSION.SDK_INT < 11)
                {
                    playAlbum(albumId);
                    isPlayAlbum = false;
                    return;
                }
                break;

            case 1: // '\001'
                if (abyte0[4] == 1)
                {
                    showToast("\u7ED1\u5B9A\u4E13\u8F91\u6210\u529F");
                    return;
                }
                if (abyte0[4] == 255)
                {
                    showToast("\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
                    return;
                }
                continue; /* Loop/switch isn't completed */
            }
            break;
        }
        if (isA2DPConnected || BluetoothAdapter.getDefaultAdapter() != null && BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(2) == 2)
        {
            playAlbum(albumId);
            isPlayAlbum = false;
            return;
        } else
        {
            Logger.d("ximao", "a2dp\u672A\u8FDE\u63A5");
            isPlayAlbum = true;
            return;
        }
_L2:
        XiMaoComm.cleanPlayPosition();
        i = getActivity().getManageFragment().mStacks.size();
        if (i >= 1)
        {
            ((Fragment)((SoftReference)getActivity().getManageFragment().mStacks.get(i - 1)).get()).onResume();
        }
        if (BluetoothAdapter.getDefaultAdapter().isEnabled() && !MyDeviceManager.isApplicationBroughtToBackground(mAppContext))
        {
            (new DialogBuilder(getActivity())).setMessage("\u5DF2\u7ECF\u4E0E\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A\u65AD\u5F00\u8FDE\u63A5").showWarning();
            return;
        }
        if (true) goto _L5; else goto _L7
_L7:
    }

    public void playAlbum(final long albumId)
    {
_L2:
        int i;
        if (list == null || list.size() == 0 || si == null)
        {
            (new _cls1()).myexec(new Void[0]);
            return;
        }
        if (si.albumId == -1L)
        {
            si.albumId = si.getRealAlubmId();
        }
        if (DownloadHandler.getInstance(MyApplication.b()).isDownloadCompleted(si))
        {
            if (si.albumId == 0L)
            {
                Log.d(TAG, "\u64AD\u653E\u60C5\u51B52");
                XiMaoVoiceManager.getInstance(mAppContext).gotoPlay(10, si, mAppContext, false, false, true);
                return;
            }
            Object obj = DownloadHandler.getInstance(getActivity()).getLocalAlbumRelateSound(si.albumId, new LocalSoundCreateDescSorter());
            if (obj == null || ((ArrayList) (obj)).size() == 0)
            {
                Log.d(TAG, "\u64AD\u653E\u60C5\u51B53");
                XiMaoVoiceManager.getInstance(mAppContext).gotoPlay(10, si, mAppContext, false, false, true);
                return;
            } else
            {
                obj = ModelHelper.downloadlistToPlayList(new DownloadTask(si), ((List) (obj)));
                Log.d(TAG, "\u64AD\u653E\u60C5\u51B54");
                XiMaoVoiceManager.getInstance(mAppContext).gotoPlay(null, urlString, apiParams, 2, 10, ((PlaylistFromDownload) (obj)).soundsList, ((PlaylistFromDownload) (obj)).index, mAppContext, false, false, true);
                return;
            }
        } else
        {
            (new _cls2()).myexec(new Void[0]);
            return;
        }
        final HashMap apiParams = new HashMap();
        final String urlString = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
        apiParams.put("albumId", (new StringBuilder()).append(albumId).append("").toString());
        apiParams.put("isAsc", "true");
        apiParams.put("pageSize", "20");
        if (ToolUtil.isConnectToNetwork(mAppContext))
        {
            List list = HistoryManage.getInstance(mAppContext).getSoundInfoList();
            if (list == null || list.size() == 0)
            {
                Log.d("ximao", "tSoundInfoList is null");
                break MISSING_BLOCK_LABEL_128;
            } else
            {
                i = 0;
                do
                {
                    if (i >= list.size())
                    {
                        continue; /* Loop/switch isn't completed */
                    }
                    if (albumId == ((SoundInfo)list.get(i)).albumId)
                    {
                        si = (SoundInfo)list.get(i);
                        continue; /* Loop/switch isn't completed */
                    }
                    i++;
                } while (true);
            }
        }
        XiMaoComm.playNoData(mAppContext);
        return;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public void showToast(String s)
    {
        CustomToast.showToast(mAppContext, s, 0);
    }


    private class _cls1 extends MyAsyncTask
    {

        final XiMaoMainHandler this$0;
        final long val$albumId;
        final HashMap val$apiParams;
        final String val$urlString;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
label0:
            {
                List list = XiMaoVoiceManager.getInstance(mAppContext).loadSoundListData(albumId);
                if (list != null)
                {
                    avoid = list;
                    if (list.size() != 0)
                    {
                        break label0;
                    }
                }
                Logger.d(TAG, "tsoundDataList=null");
                avoid = null;
            }
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (list != null)
            {
                Log.d(TAG, "\u64AD\u653E\u60C5\u51B51");
                list = ModelHelper.albumSoundlistToSoundInfoList(list);
                XiMaoVoiceManager.getInstance(mAppContext).gotoPlay(null, urlString, apiParams, 2, 3, list, 0, mAppContext, false, false, true);
                return;
            } else
            {
                XiMaoComm.playNoData(mAppContext);
                return;
            }
        }

        _cls1()
        {
            this$0 = XiMaoMainHandler.this;
            albumId = l;
            urlString = s;
            apiParams = hashmap;
            super();
        }
    }


    private class _cls2 extends MyAsyncTask
    {

        final XiMaoMainHandler this$0;
        final long val$albumId;
        final HashMap val$apiParams;
        final String val$urlString;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
label0:
            {
                List list = XiMaoVoiceManager.getInstance(mAppContext).loadSoundListData(albumId);
                if (list != null)
                {
                    avoid = list;
                    if (list.size() != 0)
                    {
                        break label0;
                    }
                }
                Logger.d(TAG, "tsoundDataList=null");
                avoid = null;
            }
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (list != null)
            {
                Log.d(TAG, "\u64AD\u653E\u60C5\u51B55");
                list = ModelHelper.albumSoundlistToSoundInfoList(list);
                for (int i = 0; i < list.size(); i++)
                {
                    if (((SoundInfo)list.get(i)).equals(si))
                    {
                        XiMaoVoiceManager.getInstance(mAppContext).gotoPlay(null, urlString, apiParams, 2, 3, list, i, mAppContext, false, false, true);
                        return;
                    }
                }

                Log.d(TAG, "\u64AD\u653E\u60C5\u51B56");
                XiMaoVoiceManager.getInstance(mAppContext).gotoPlay(null, urlString, apiParams, 2, 3, list, 0, mAppContext, false, false, true);
                return;
            } else
            {
                CustomToast.showToast(mAppContext, "\u4E13\u8F91\u64AD\u653E\u5F02\u5E382\uFF0C\u8BF7\u91CD\u8BD5", 0);
                return;
            }
        }

        _cls2()
        {
            this$0 = XiMaoMainHandler.this;
            albumId = l;
            urlString = s;
            apiParams = hashmap;
            super();
        }
    }

}
