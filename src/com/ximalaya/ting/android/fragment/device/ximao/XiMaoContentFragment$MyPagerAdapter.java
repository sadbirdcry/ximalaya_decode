// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoContentFragment, XiMaoMainFragmentNew, XiMaoBindFragment

public class this._cls0 extends FragmentPagerAdapter
{

    private final String TITLES[] = {
        "\u5185\u7F6E\u7684\u58F0\u97F3", "\u7ED1\u5B9A\u7684\u4E13\u8F91"
    };
    final XiMaoContentFragment this$0;

    public int getCount()
    {
        return TITLES.length;
    }

    public Fragment getItem(int i)
    {
        if (i == 0)
        {
            if (XiMaoContentFragment.access$400(XiMaoContentFragment.this) == null)
            {
                XiMaoContentFragment.access$402(XiMaoContentFragment.this, new XiMaoMainFragmentNew());
            }
            return XiMaoContentFragment.access$400(XiMaoContentFragment.this);
        }
        if (i == 1)
        {
            if (XiMaoContentFragment.access$500(XiMaoContentFragment.this) == null)
            {
                XiMaoContentFragment.access$502(XiMaoContentFragment.this, new XiMaoBindFragment());
            }
            return XiMaoContentFragment.access$500(XiMaoContentFragment.this);
        }
        if (XiMaoContentFragment.access$400(XiMaoContentFragment.this) == null)
        {
            XiMaoContentFragment.access$402(XiMaoContentFragment.this, new XiMaoMainFragmentNew());
        }
        return XiMaoContentFragment.access$400(XiMaoContentFragment.this);
    }

    public CharSequence getPageTitle(int i)
    {
        return TITLES[i];
    }

    public Y(FragmentManager fragmentmanager)
    {
        this$0 = XiMaoContentFragment.this;
        super(fragmentmanager);
    }
}
