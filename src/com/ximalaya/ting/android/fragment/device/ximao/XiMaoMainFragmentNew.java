// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.NetworkOnMainThreadException;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.ximao.XiMaoDownloadModule;
import com.ximalaya.ting.android.fragment.device.shu.CategoryFragment;
import com.ximalaya.ting.android.fragment.finding2.category.CategoryTagFragmentNew;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import java.lang.ref.WeakReference;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoMainAdapter, XiMaoBTManager, XiMaoComm

public class XiMaoMainFragmentNew extends BaseListFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.device.bluetooth.ximao.XiMaoDownloadHandler.OnBluetoothDownloadStatusListener
{
    static class MyHandler extends Handler
    {

        WeakReference mFragment;

        public void handleMessage(Message message)
        {
            XiMaoMainFragmentNew ximaomainfragmentnew = (XiMaoMainFragmentNew)mFragment.get();
            if (ximaomainfragmentnew.isAdded())
            {
                ximaomainfragmentnew.onMessage(message);
            }
        }

        MyHandler(XiMaoMainFragmentNew ximaomainfragmentnew)
        {
            mFragment = new WeakReference(ximaomainfragmentnew);
        }
    }


    private static final int MSG_BTDEV_CONNECTED = 1;
    private static final int MSG_BTDEV_DISCONNECTED = 2;
    private static final int MSG_BTDEV_PACKET = 5;
    private static final int MSG_BTDEV_PACKET_COMMAND = 6;
    private static final int MSG_BTDEV_RECEIVED = 3;
    private static final int MSG_BTDEV_SENT = 4;
    static boolean isFirst = false;
    public MyHandler MsgHandler;
    private String TAG;
    private XiMaoMainAdapter mainAdapter;

    public XiMaoMainFragmentNew()
    {
        TAG = "ximao";
        mainAdapter = null;
        MsgHandler = new MyHandler(this);
    }

    private void initUi()
    {
        fragmentBaseContainerView.findViewById(0x7f0a03e3).setOnClickListener(this);
        mainAdapter = new XiMaoMainAdapter(mCon);
        mListView.setAdapter(mainAdapter);
        mListView.setOnItemClickListener(new _cls1());
    }

    public void freshFileNum()
    {
        mainAdapter.notifyDataSetChanged();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initUi();
        if (isFirst)
        {
            isFirst = false;
        }
        mainAdapter.toggleDownloadStatus();
    }

    public void onClick(View view)
    {
        if (!XiMaoBTManager.getInstance(mCon).isAudio()) goto _L2; else goto _L1
_L1:
        showToast("\u60A8\u6B63\u5728\u8FDB\u884C\u8BED\u97F3\u641C\u7D22\uFF0C\u8BF7\u7A0D\u540E");
_L7:
        return;
_L2:
        switch (view.getId())
        {
        default:
            return;

        case 2131362787: 
            XiMaoComm.sendCommand(mCon, 7, 17);
            break;
        }
        if (!XiMaoComm.isLockBind(mCon)) goto _L4; else goto _L3
_L3:
        int i;
        Bundle bundle = new Bundle();
        try
        {
            view = XiMaoComm.loadCategoryInfo(mCon);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            view = null;
        }
        if (view == null)
        {
            break MISSING_BLOCK_LABEL_285;
        }
        i = 0;
_L8:
        if (i >= view.size())
        {
            break MISSING_BLOCK_LABEL_285;
        }
        if (!((FindingCategoryModel)view.get(i)).getTitle().equals("\u513F\u7AE5")) goto _L6; else goto _L5
_L5:
        view = (FindingCategoryModel)view.get(i);
_L9:
        if (view != null)
        {
            bundle.putString("title", view.getTitle());
            bundle.putString("categoryId", (new StringBuilder()).append(view.getId()).append("").toString());
            bundle.putString("categoryName", view.getName());
            bundle.putString("contentType", "album");
            bundle.putBoolean("isSerialized", view.isFinished());
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(fragmentBaseContainerView.findViewById(0x7f0a03e3)));
            bundle.putBoolean("showFocus", false);
            bundle.putInt("from", 10);
            startFragment(com/ximalaya/ting/android/fragment/finding2/category/CategoryTagFragmentNew, bundle);
            return;
        }
          goto _L7
_L6:
        i++;
          goto _L8
_L4:
        view = new Bundle();
        view.putBoolean("showFocus", false);
        view.putInt("from", 10);
        startFragment(com/ximalaya/ting/android/fragment/device/shu/CategoryFragment, view);
        return;
        view = null;
          goto _L9
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300e8, viewgroup, false);
        mListView = (ListView)fragmentBaseContainerView.findViewById(0x7f0a0129);
        return fragmentBaseContainerView;
    }

    public void onMessage(Message message)
    {
        byte abyte0[] = (byte[])(byte[])message.obj;
        switch (message.what)
        {
        case 3: // '\003'
            showToast((new StringBuilder()).append("RECEIVED").append(new String((byte[])(byte[])message.obj, 0, message.arg2)).toString());
            break;

        case 6: // '\006'
            message = (byte[])(byte[])message.obj;
            switch (message[1])
            {
            case 19: // '\023'
                int i = XiMaoBTManager.getInstance(mCon).getPacketSize(message);
                if (i == 1)
                {
                    i = message[4] & 0xff;
                } else
                if (i == 2)
                {
                    i = (message[5] & 0xff) + ((message[4] & 0xff) << 8);
                } else
                if (i == 4)
                {
                    i = (message[4] & 0xff) + ((message[5] & 0xff) << 8) + ((message[6] & 0xff) << 16) + ((message[7] & 0xff) << 24);
                } else
                {
                    i = 0;
                }
                switch (message[0])
                {
                case 10: // '\n'
                    XiMaoBTManager.getInstance(mCon).mNumContent3 = i;
                    break;

                case 19: // '\023'
                    XiMaoBTManager.getInstance(mCon).mNumContent2 = i;
                    break;

                case 11: // '\013'
                    XiMaoBTManager.getInstance(mCon).mNumContent1 = i;
                    break;

                case 12: // '\f'
                    XiMaoBTManager.getInstance(mCon).mNumContent4 = i;
                    break;
                }
                break;
            }
            break;
        }
        while (true) 
        {
            mainAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void onResume()
    {
        super.onResume();
        freshFileNum();
        if (XiMaoBTManager.getInstance(mCon).getDownloadModule() != null)
        {
            XiMaoBTManager.getInstance(mCon).getDownloadModule().setOnBluetoothDownloadStatusListener(this);
        }
        Logger.d(TAG, "onResume");
    }

    public void onStatusChanged()
    {
        if (mainAdapter != null)
        {
            getActivity().runOnUiThread(new _cls2());
        }
    }

    public void onStop()
    {
        super.onStop();
        XiMaoBTManager.getInstance(mCon).getDownloadModule().removeOnBluetoothDownloadStatusListener(this);
        Logger.d(TAG, "onResume");
    }

    public void showToast(String s)
    {
        CustomToast.showToast(mActivity, s, 0);
    }

    public void toggleDownloadTask()
    {
    }



    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final XiMaoMainFragmentNew this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (i == 0)
            {
                adapterview = new Bundle();
                adapterview.putInt("name", XiMaoComm.CONTENT.CONTENT_1.ordinal());
                startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoListFragment, adapterview);
            } else
            {
                if (i == 1)
                {
                    adapterview = new Bundle();
                    adapterview.putInt("name", XiMaoComm.CONTENT.CONTENT_2.ordinal());
                    startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoListFragment, adapterview);
                    return;
                }
                if (i == 2)
                {
                    adapterview = new Bundle();
                    adapterview.putInt("name", XiMaoComm.CONTENT.CONTENT_3.ordinal());
                    startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoListFragment, adapterview);
                    return;
                }
                if (i == 3)
                {
                    adapterview = new Bundle();
                    adapterview.putInt("name", XiMaoComm.CONTENT.CONTENT_4.ordinal());
                    startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoListFragment, adapterview);
                    return;
                }
                if (i == 4)
                {
                    startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/ximao/XiMaoDownloadTaskFragment, null);
                    return;
                }
            }
        }

        _cls1()
        {
            this$0 = XiMaoMainFragmentNew.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final XiMaoMainFragmentNew this$0;

        public void run()
        {
            mainAdapter.toggleDownloadStatus();
        }

        _cls2()
        {
            this$0 = XiMaoMainFragmentNew.this;
            super();
        }
    }

}
