// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;


public class KeyInfo
{

    public long keyAlbumId;
    public String keyAlbumName;
    public int keyNum;
    public int trackCount;

    public KeyInfo()
    {
    }

    public KeyInfo(int i, String s, long l, int j)
    {
        keyNum = i;
        if (s == null || s.isEmpty())
        {
            keyAlbumName = "\u672A\u83B7\u53D6\u5230\u7ED1\u5B9A\u4E13\u8F91";
        } else
        {
            keyAlbumName = s;
        }
        keyAlbumId = l;
        trackCount = j;
    }

    public String toString()
    {
        return (new StringBuilder()).append("KeyInfo:keyNum:").append(keyNum).append(";").append("keyAlbumName:").append(keyAlbumName).append("keyAlbumId:").append(keyAlbumId).append(",trackCount:").append(trackCount).toString();
    }
}
