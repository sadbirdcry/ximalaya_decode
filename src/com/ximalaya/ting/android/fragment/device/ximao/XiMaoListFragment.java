// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.device.XiMaoListAdapter;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoBTManager, XiMaoComm

public class XiMaoListFragment extends BaseListFragment
    implements android.view.View.OnClickListener, android.widget.AdapterView.OnItemClickListener
{
    private class LoadingData
    {

        public int pageSize;
        final XiMaoListFragment this$0;
        public int totalCount;

        public void reSet()
        {
            totalCount = 0;
            pageSize = 20;
        }

        private LoadingData()
        {
            this$0 = XiMaoListFragment.this;
            super();
            totalCount = 0;
            pageSize = 20;
        }

        LoadingData(_cls1 _pcls1)
        {
            this();
        }
    }

    static class MyHandler extends Handler
    {

        WeakReference mFragment;

        public void handleMessage(Message message)
        {
            XiMaoListFragment ximaolistfragment = (XiMaoListFragment)mFragment.get();
            if (ximaolistfragment != null && ximaolistfragment.isAdded())
            {
                ximaolistfragment.onMessage(message);
            }
        }

        MyHandler(XiMaoListFragment ximaolistfragment)
        {
            mFragment = new WeakReference(ximaolistfragment);
        }
    }


    private static final int MSG_BTDEV_CONNECTED = 1;
    private static final int MSG_BTDEV_DISCONNECTED = 2;
    private static final int MSG_BTDEV_PACKET = 5;
    private static final int MSG_BTDEV_PACKET_COMMAND = 6;
    private static final int MSG_BTDEV_PACKET_FINISH = 7;
    private static final int MSG_BTDEV_PACKET_TIMEOUT = 8;
    private static final int MSG_BTDEV_RECEIVED = 3;
    private static final int MSG_BTDEV_SENT = 4;
    private static final String TAG = "ximao";
    public static boolean isPlay = false;
    public static XiMaoComm.CONTENT mContentName;
    public static XiMaoComm.CONTENT mPlayContent;
    public static int mPlayPositionNow = -100;
    public static boolean storePlayPosition = false;
    private MyHandler MsgHandler;
    private Timer clickTimer;
    private int currentNum;
    HashMap dataMap;
    ImageView device_back_img;
    private boolean isClickable;
    boolean isFinish;
    public boolean isFinished;
    boolean isFirst;
    private boolean isSentCommand;
    private boolean isSentPacket;
    private byte lastPacket[];
    public TextView mBtnRight;
    private XiMaoBTManager.BTDevConnectionReceiver mConnectionReceiver;
    RelativeLayout mContentView;
    private LoadingData soundData;
    public TimerTask task;
    public Timer timer;
    TextView top_tv;
    public XiMaoListAdapter xiMaoListAdapter;

    public XiMaoListFragment()
    {
        isSentCommand = false;
        isSentPacket = false;
        lastPacket = null;
        isClickable = true;
        isFinish = false;
        isFirst = true;
        isFinished = false;
        timer = null;
        task = null;
        clickTimer = null;
        mConnectionReceiver = new _cls1();
        MsgHandler = new MyHandler(this);
    }

    private void addItem(HashMap hashmap)
    {
        if (isAdded() && mCon != null && XiMaoBTManager.getInstance(mCon).itemsList != null)
        {
            XiMaoBTManager.getInstance(mCon).itemsList.add(hashmap);
            xiMaoListAdapter.notifyDataSetChanged();
            Log.d("ximao", "\u6DFB\u52A0Item\u6210\u529F");
            Log.d("ximao", (new StringBuilder()).append("isFinished").append(isFinished).toString());
            Log.d("ximao", (new StringBuilder()).append("soundData.totalCount").append(soundData.totalCount).toString());
            Log.d("ximao", (new StringBuilder()).append("next").append(soundData.totalCount % soundData.pageSize).toString());
            if (!isFinished && soundData.totalCount != 0 && soundData.totalCount % soundData.pageSize == 0)
            {
                Logger.d("ximao", (new StringBuilder()).append("\u67E5\u8BE2").append(soundData.pageSize).append("\u6761\u5B8C\u6210").toString());
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.MORE);
                onStopSearch();
                return;
            }
        }
    }

    public static int getCurrentNum(Context context)
    {
        static class _cls7
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[];
            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE = new int[XiMaoComm.PLAYMODE.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE[XiMaoComm.PLAYMODE.ORDER.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror6) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE[XiMaoComm.PLAYMODE.SHUFFLE.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror5) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$PLAYMODE[XiMaoComm.PLAYMODE.REPEAT.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror4) { }
                $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT = new int[XiMaoComm.CONTENT.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[XiMaoComm.CONTENT.CONTENT_1.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[XiMaoComm.CONTENT.CONTENT_2.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[XiMaoComm.CONTENT.CONTENT_3.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$CONTENT[XiMaoComm.CONTENT.CONTENT_4.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        switch (_cls7..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT[mContentName.ordinal()])
        {
        default:
            return -1;

        case 1: // '\001'
            return XiMaoBTManager.getInstance(context).mNumContent1;

        case 2: // '\002'
            return XiMaoBTManager.getInstance(context).mNumContent2;

        case 3: // '\003'
            return XiMaoBTManager.getInstance(context).mNumContent3;

        case 4: // '\004'
            return XiMaoBTManager.getInstance(context).mNumContent4;
        }
    }

    private void initUi()
    {
        _cls7..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.PLAYMODE[XiMaoComm.mPlayMode.ordinal()];
        JVM INSTR tableswitch 1 3: default 36
    //                   1 259
    //                   2 266
    //                   3 273;
           goto _L1 _L2 _L3 _L4
_L4:
        break MISSING_BLOCK_LABEL_273;
_L1:
        Object obj = "\u987A\u5E8F\u64AD\u653E";
_L5:
        mBtnRight = (TextView)mContentView.findViewById(0x7f0a071c);
        device_back_img = (ImageView)mContentView.findViewById(0x7f0a071b);
        top_tv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mBtnRight.setText(((CharSequence) (obj)));
        mBtnRight.setVisibility(0);
        top_tv.setText(XiMaoComm.CONTENT.getString(mContentName));
        mBtnRight.setOnClickListener(this);
        device_back_img.setOnClickListener(this);
        obj = new View(getActivity());
        ((View) (obj)).setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, getResources().getDimensionPixelSize(0x7f080009)));
        mListView.setOnItemClickListener(this);
        mListView.addFooterView(((View) (obj)), null, false);
        if (mContentName == XiMaoComm.CONTENT.CONTENT_4)
        {
            xiMaoListAdapter = new XiMaoListAdapter(getActivity(), XiMaoBTManager.getInstance(mCon).itemsList, XiMaoComm.CONTENT.CONTENT_4);
        } else
        {
            xiMaoListAdapter = new XiMaoListAdapter(getActivity(), XiMaoBTManager.getInstance(mCon).itemsList, XiMaoComm.CONTENT.CONTENT_1);
        }
        mListView.setAdapter(xiMaoListAdapter);
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        mFooterViewLoading.setOnClickListener(new _cls4());
        return;
_L2:
        obj = "\u987A\u5E8F\u64AD\u653E";
          goto _L5
_L3:
        obj = "\u968F\u673A\u64AD\u653E";
          goto _L5
        obj = "\u5FAA\u73AF\u64AD\u653E";
          goto _L5
    }

    private boolean isCurrentHead(byte byte0)
    {
        while (byte0 == 10 && mContentName == XiMaoComm.CONTENT.CONTENT_3 || byte0 == 11 && mContentName == XiMaoComm.CONTENT.CONTENT_1 || byte0 == 19 && mContentName == XiMaoComm.CONTENT.CONTENT_2 || byte0 == 12 && mContentName == XiMaoComm.CONTENT.CONTENT_4) 
        {
            return true;
        }
        return false;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        if (storePlayPosition);
        soundData = new LoadingData(null);
        XiMaoBTManager.getInstance(mCon).itemsList = new ArrayList();
        initUi();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363612: 
            switch (_cls7..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.PLAYMODE[XiMaoComm.mPlayMode.ordinal()])
            {
            default:
                return;

            case 1: // '\001'
                XiMaoComm.changePlayMode(mCon, XiMaoComm.PLAYMODE.REPEAT);
                return;

            case 2: // '\002'
                XiMaoComm.changePlayMode(mCon, XiMaoComm.PLAYMODE.REPEAT);
                return;

            case 3: // '\003'
                XiMaoComm.changePlayMode(mCon, XiMaoComm.PLAYMODE.SHUFFLE);
                break;
            }
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        int i = getArguments().getInt("name");
        mContentName = XiMaoComm.CONTENT.values()[i];
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300e6, viewgroup, false);
        mListView = (ListView)mContentView.findViewById(0x7f0a03cb);
        isFinished = false;
        return mContentView;
    }

    public void onDestroy()
    {
        isFirst = true;
        super.onDestroy();
    }

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        if (XiMaoBTManager.getInstance(mCon).isSearchContent())
        {
            showToast("\u6B63\u5728\u67E5\u8BE2\u76EE\u5F55\uFF0C\u7A0D\u540E\u4E3A\u60A8\u64AD\u653E");
        }
        if (!isClickable)
        {
            return;
        }
        isClickable = false;
        clickTimer = new Timer();
        adapterview = new _cls5();
        clickTimer.schedule(adapterview, 200L);
        adapterview = (TextView)view.findViewById(0x7f0a0218);
        mActivity.runOnUiThread(new _cls6());
        if (mPlayPositionNow == i && mContentName.equals(mPlayContent) && isPlay)
        {
            if (XiMaoBTManager.getInstance(mCon).isSearchContent())
            {
                isSentCommand = true;
                isSentPacket = false;
                return;
            } else
            {
                XiMaoComm.sendCommand(mCon, 4, 3);
                return;
            }
        }
        adapterview = adapterview.getText();
        if (adapterview == null) goto _L2; else goto _L1
_L1:
        adapterview = adapterview.toString().getBytes("unicode");
_L4:
        view = new byte[adapterview.length - 2];
        for (int j = 0; j < adapterview.length - 2; j++)
        {
            view[j] = adapterview[j + 2];
        }

        break; /* Loop/switch isn't completed */
        adapterview;
        adapterview.printStackTrace();
_L2:
        adapterview = null;
        if (true) goto _L4; else goto _L3
_L3:
        XiMaoComm.printPacket("new flow", view, view.length);
        if (XiMaoBTManager.getInstance(mCon).isSearchContent())
        {
            isSentCommand = false;
            isSentPacket = true;
            lastPacket = new byte[view.length];
            System.arraycopy(view, 0, lastPacket, 0, view.length);
        } else
        {
            playLocal(view);
        }
        onPause(mPlayPositionNow);
        mPlayPositionNow = i;
        mPlayContent = mContentName;
        return;
    }

    public void onMessage(Message message)
    {
        int i;
        i = 4;
        showDebugToast("\u6536\u5230\u6570\u636E\uFF0C\u89E3\u6790\u4E2D1");
        byte abyte0[] = (byte[])(byte[])message.obj;
        int k = message.arg2;
        if (abyte0 != null)
        {
            XiMaoComm.printPacket("data", abyte0, k);
        }
        message.what;
        JVM INSTR tableswitch 1 8: default 88
    //                   1 88
    //                   2 88
    //                   3 89
    //                   4 88
    //                   5 650
    //                   6 135
    //                   7 1104
    //                   8 1127;
           goto _L1 _L1 _L1 _L2 _L1 _L3 _L4 _L5 _L6
_L8:
        return;
_L2:
        showToast((new StringBuilder()).append("RECEIVED").append(new String((byte[])(byte[])message.obj, 0, message.arg2)).toString());
        return;
_L4:
        byte abyte1[] = (byte[])(byte[])message.obj;
        i = message.arg2;
        switch (abyte1[0])
        {
        case 14: // '\016'
        case 15: // '\017'
        case 16: // '\020'
        case 17: // '\021'
        case 18: // '\022'
        default:
            return;

        case 10: // '\n'
        case 11: // '\013'
        case 12: // '\f'
        case 19: // '\023'
            switch (abyte1[1])
            {
            default:
                return;

            case 2: // '\002'
                break;
            }
            if (abyte1[4] == 1)
            {
                onPlay(mPlayPositionNow);
                return;
            }
            break;

        case 13: // '\r'
            switch (abyte1[1])
            {
            case 7: // '\007'
            case 8: // '\b'
            case 9: // '\t'
            case 10: // '\n'
            default:
                return;

            case 15: // '\017'
                break;

            case 2: // '\002'
                onPlay(mPlayPositionNow);
                return;

            case 5: // '\005'
                XiMaoComm.mPlayMode = XiMaoComm.PLAYMODE.SHUFFLE;
                showToast("\u968F\u673A\u64AD\u653E");
                mBtnRight.setText(" \u968F\u673A\u64AD\u653E");
                return;

            case 4: // '\004'
                XiMaoComm.mPlayMode = XiMaoComm.PLAYMODE.REPEAT;
                showToast("\u5FAA\u73AF\u64AD\u653E");
                mBtnRight.setText(" \u5FAA\u73AF\u64AD\u653E");
                return;

            case 6: // '\006'
                XiMaoComm.mPlayMode = XiMaoComm.PLAYMODE.ORDER;
                showToast("\u987A\u5E8F\u64AD\u653E");
                mBtnRight.setText(" \u987A\u5E8F\u64AD\u653E");
                return;

            case 3: // '\003'
                onPause(mPlayPositionNow);
                return;

            case 11: // '\013'
                onPlay(mPlayPositionNow);
                return;

            case 12: // '\f'
                onPause(mPlayPositionNow);
                return;

            case 13: // '\r'
                if (!storePlayPosition)
                {
                    XiMaoComm.cleanPlayPosition();
                    xiMaoListAdapter.notifyDataSetChanged();
                    return;
                }
                currentNum = getCurrentNum(mCon);
                if (currentNum < 0)
                {
                    currentNum = XiMaoBTManager.getInstance(mCon).itemsList.size();
                }
                if (XiMaoComm.mPlayMode == XiMaoComm.PLAYMODE.REPEAT)
                {
                    mPlayPositionNow = ((mPlayPositionNow - 1) + currentNum) % currentNum;
                    xiMaoListAdapter.notifyDataSetChanged();
                    return;
                }
                if (XiMaoComm.mPlayMode == XiMaoComm.PLAYMODE.SHUFFLE)
                {
                    onPause(mPlayPositionNow);
                    mPlayPositionNow = -1;
                    xiMaoListAdapter.notifyDataSetChanged();
                    return;
                }
                break;

            case 14: // '\016'
                if (!storePlayPosition)
                {
                    XiMaoComm.cleanPlayPosition();
                    xiMaoListAdapter.notifyDataSetChanged();
                    return;
                }
                currentNum = getCurrentNum(mCon);
                if (currentNum < 0)
                {
                    currentNum = XiMaoBTManager.getInstance(mCon).itemsList.size();
                }
                if (XiMaoComm.mPlayMode == XiMaoComm.PLAYMODE.REPEAT)
                {
                    mPlayPositionNow = (mPlayPositionNow + 1) % currentNum;
                    xiMaoListAdapter.notifyDataSetChanged();
                    return;
                }
                continue; /* Loop/switch isn't completed */
            }
            break;
        }
_L1:
        if (true) goto _L8; else goto _L7
_L7:
        if (XiMaoComm.mPlayMode != XiMaoComm.PLAYMODE.SHUFFLE) goto _L8; else goto _L9
_L9:
        onPause(mPlayPositionNow);
        mPlayPositionNow = -1;
        xiMaoListAdapter.notifyDataSetChanged();
        return;
_L3:
        byte abyte2[];
        int l;
        showDebugToast("\u6536\u5230\u6570\u636E\uFF0C\u89E3\u6790\u4E2D2");
        abyte2 = (byte[])(byte[])message.obj;
        l = message.arg2;
        abyte2[0];
        JVM INSTR lookupswitch 4: default 720
    //                   10: 721
    //                   11: 721
    //                   12: 721
    //                   19: 721;
           goto _L10 _L11 _L11 _L11 _L11
_L10:
        return;
_L11:
        abyte2[1];
        JVM INSTR lookupswitch 3: default 760
    //                   1: 761
    //                   2: 1033
    //                   19: 835;
           goto _L12 _L13 _L14 _L15
_L14:
        continue; /* Loop/switch isn't completed */
_L12:
        return;
_L13:
        if (!isCurrentHead(abyte2[0]) || abyte2[2] == 0 && abyte2[3] == 1) goto _L8; else goto _L16
_L15:
        int j = XiMaoBTManager.getInstance(mCon).getPacketSize(abyte2);
        if (j == 1)
        {
            j = abyte2[4] & 0xff;
        } else
        if (j == 2)
        {
            j = abyte2[5];
            j = ((abyte2[4] & 0xff) << 8) + (j & 0xff);
        } else
        if (j == 4)
        {
            j = (abyte2[4] & 0xff) + ((abyte2[5] & 0xff) << 8) + ((abyte2[6] & 0xff) << 16) + ((abyte2[7] & 0xff) << 24);
        } else
        {
            j = 0;
        }
        switch (abyte2[0])
        {
        default:
            return;

        case 10: // '\n'
            XiMaoBTManager.getInstance(mCon).mNumContent3 = j;
            return;

        case 19: // '\023'
            XiMaoBTManager.getInstance(mCon).mNumContent2 = j;
            return;

        case 11: // '\013'
            XiMaoBTManager.getInstance(mCon).mNumContent1 = j;
            return;

        case 12: // '\f'
            XiMaoBTManager.getInstance(mCon).mNumContent4 = j;
            return;
        }
        if (abyte2[4] != 1) goto _L8; else goto _L17
_L17:
        onPlay(mPlayPositionNow);
        return;
_L16:
        showDebugToast("\u6536\u5230\u6570\u636E\uFF0C\u89E3\u6790\u4E2D3");
        sendACK();
        message = new byte[l - 5];
        for (; i < l - 1; i += 2)
        {
            message[i - 4] = abyte2[i + 1];
            message[i - 3] = abyte2[i];
        }

        try
        {
            message = new String(message, "unicode");
        }
        // Misplaced declaration of an exception variable
        catch (Message message)
        {
            message.printStackTrace();
            message = "";
        }
        dataMap = new HashMap();
        dataMap.put("title", message);
        addItem(dataMap);
        return;
_L5:
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        XiMaoBTManager.getInstance(mCon).setSearchContent(false);
        onStopSearch();
        return;
_L6:
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.MORE);
        XiMaoBTManager.getInstance(mCon).setSearchContent(false);
        onStopSearch();
        return;
    }

    public void onPause(int i)
    {
        if (i < 0)
        {
            return;
        } else
        {
            isPlay = false;
            mPlayPositionNow = i;
            xiMaoListAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void onPlay(int i)
    {
        if (i < 0)
        {
            return;
        } else
        {
            isPlay = true;
            mPlayPositionNow = i;
            xiMaoListAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void onResume()
    {
        super.onResume();
        if (XiMaoBTManager.getInstance(mCon).getConnState() != XiMaoComm.STATE.CONNECTED)
        {
            if (getActivity() != null)
            {
                getActivity().onBackPressed();
            }
            return;
        }
        if (xiMaoListAdapter != null)
        {
            xiMaoListAdapter.notifyDataSetChanged();
        }
        XiMaoBTManager.getInstance(mCon).setmBTDevConnectionReceiver(mConnectionReceiver);
    }

    public void onStart()
    {
        super.onStart();
        soundData.reSet();
        (new Thread(new _cls2())).start();
    }

    public void onStop()
    {
        super.onStop();
        if (timer != null)
        {
            timer.cancel();
            timer = null;
        }
        if (clickTimer != null)
        {
            clickTimer.cancel();
            clickTimer = null;
        }
        XiMaoComm.removeBTDevConnectionReceiver(mCon);
    }

    public void onStopSearch()
    {
        XiMaoBTManager.getInstance(mCon).setSearchContent(false);
        if (timer != null)
        {
            timer.cancel();
        }
        if (isSentPacket && lastPacket != null)
        {
            try
            {
                Thread.sleep(2000L);
            }
            catch (InterruptedException interruptedexception)
            {
                interruptedexception.printStackTrace();
            }
            playLocal(lastPacket);
        }
        if (isSentCommand)
        {
            try
            {
                Thread.sleep(2000L);
            }
            catch (InterruptedException interruptedexception1)
            {
                interruptedexception1.printStackTrace();
            }
            XiMaoComm.sendCommand(mCon, 4, 3);
        }
        isSentCommand = false;
        isSentPacket = false;
        lastPacket = null;
    }

    public void playLocal(byte abyte0[])
    {
        switch (_cls7..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT[mContentName.ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            XiMaoComm.sendPacket(mCon, 2, 8, abyte0, abyte0.length);
            return;

        case 2: // '\002'
            XiMaoComm.sendPacket(mCon, 8, 8, abyte0, abyte0.length);
            return;

        case 3: // '\003'
            XiMaoComm.sendPacket(mCon, 1, 8, abyte0, abyte0.length);
            return;

        case 4: // '\004'
            XiMaoComm.sendPacket(mCon, 3, 8, abyte0, abyte0.length);
            break;
        }
    }

    public void sendACK()
    {
        if (timer != null)
        {
            timer.cancel();
        }
        LoadingData loadingdata = soundData;
        loadingdata.totalCount = loadingdata.totalCount + 1;
        if (isFinished || soundData.totalCount == 0 || soundData.totalCount % soundData.pageSize == 0) goto _L2; else goto _L1
_L1:
        byte abyte0[];
        Log.d("ximao", "\u7EE7\u7EED\u66F4\u65B0");
        setTimeout();
        abyte0 = new byte[1];
        abyte0[0] = 1;
        _cls7..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT[mContentName.ordinal()];
        JVM INSTR tableswitch 1 4: default 124
    //                   1 125
    //                   2 144
    //                   3 164
    //                   4 183;
           goto _L2 _L3 _L4 _L5 _L6
_L2:
        return;
_L3:
        XiMaoComm.sendPacket(mCon, 2, 255, abyte0, 1);
        isFirst = false;
        return;
_L4:
        XiMaoComm.sendPacket(mCon, 8, 255, abyte0, 1);
        isFirst = false;
        return;
_L5:
        XiMaoComm.sendPacket(mCon, 1, 255, abyte0, 1);
        isFirst = false;
        return;
_L6:
        XiMaoComm.sendPacket(mCon, 3, 255, abyte0, 1);
        isFirst = false;
        return;
    }

    public void setTimeout()
    {
        timer = new Timer();
        task = new _cls3();
        timer.schedule(task, 5000L);
    }

    public void showDebugToast(String s)
    {
        Log.d("ximao", s);
    }

    public void showToast(String s)
    {
        CustomToast.showToast(getActivity(), s, 0);
    }




/*
    static boolean access$202(XiMaoListFragment ximaolistfragment, boolean flag)
    {
        ximaolistfragment.isClickable = flag;
        return flag;
    }

*/

    private class _cls1
        implements XiMaoBTManager.BTDevConnectionReceiver
    {

        final XiMaoListFragment this$0;

        public void onConnectFailed()
        {
        }

        public void onConnected()
        {
            MsgHandler.obtainMessage(1).sendToTarget();
        }

        public void onDisconnected()
        {
            MsgHandler.obtainMessage(2).sendToTarget();
        }

        public void onPacket(byte abyte0[], int i)
        {
            int j = XiMaoBTManager.getInstance(mCon).getPacketSize(abyte0);
            if (j == 0)
            {
                MsgHandler.obtainMessage(6, 0, i, abyte0).sendToTarget();
            } else
            {
                if (j == 1)
                {
                    if ((abyte0[4] & 0xff) == 2)
                    {
                        showDebugToast("\u5DF2\u7ECF\u5B8C\u6210\uFF0C\u8BBE\u7F6EisFinish");
                        isFinished = true;
                        MsgHandler.obtainMessage(7, 0, i, null).sendToTarget();
                        return;
                    }
                    if ((abyte0[4] & 0xff) == 255)
                    {
                        showDebugToast("\u6536\u5230ff\u9519\u8BEF\u6307\u4EE4\uFF0C\u91CD\u53D1");
                        sendACK();
                        return;
                    } else
                    {
                        MsgHandler.obtainMessage(6, 0, i, abyte0).sendToTarget();
                        return;
                    }
                }
                if (j >= 2)
                {
                    MsgHandler.obtainMessage(5, 0, i, abyte0).sendToTarget();
                    return;
                }
            }
        }

        public void onReceived(byte abyte0[], int i)
        {
            MsgHandler.obtainMessage(3, 0, i, abyte0).sendToTarget();
        }

        public void onSent(byte abyte0[], int i)
        {
            MsgHandler.obtainMessage(4, 0, i, abyte0).sendToTarget();
        }

        public void onTimeout()
        {
        }

        _cls1()
        {
            this$0 = XiMaoListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final XiMaoListFragment this$0;

        public void onClick(View view)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
            view = new byte[1];
            view[0] = 1;
            Log.d("ximao", "\u7EE7\u7EED\u66F4\u65B0");
            setTimeout();
            switch (_cls7..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT[XiMaoListFragment.mContentName.ordinal()])
            {
            default:
                return;

            case 1: // '\001'
                XiMaoComm.sendPacket(mCon, 2, 255, view, 1);
                isFirst = false;
                return;

            case 2: // '\002'
                XiMaoComm.sendPacket(mCon, 8, 255, view, 1);
                isFirst = false;
                return;

            case 3: // '\003'
                XiMaoComm.sendPacket(mCon, 1, 255, view, 1);
                isFirst = false;
                return;

            case 4: // '\004'
                XiMaoComm.sendPacket(mCon, 3, 255, view, 1);
                break;
            }
            isFirst = false;
        }

        _cls4()
        {
            this$0 = XiMaoListFragment.this;
            super();
        }
    }


    private class _cls5 extends TimerTask
    {

        final XiMaoListFragment this$0;

        public void run()
        {
            isClickable = true;
        }

        _cls5()
        {
            this$0 = XiMaoListFragment.this;
            super();
        }
    }


    private class _cls6
        implements Runnable
    {

        final XiMaoListFragment this$0;

        public void run()
        {
            if (LocalMediaService.getInstance() != null)
            {
                LocalMediaService.getInstance().pause();
            }
        }

        _cls6()
        {
            this$0 = XiMaoListFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final XiMaoListFragment this$0;

        public void run()
        {
            if (!isFirst) goto _L2; else goto _L1
_L1:
            XiMaoBTManager.getInstance(mCon).setSearchContent(true);
            setTimeout();
            _cls7..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT[XiMaoListFragment.mContentName.ordinal()];
            JVM INSTR tableswitch 1 4: default 72
        //                       1 73
        //                       2 94
        //                       3 116
        //                       4 137;
               goto _L2 _L3 _L4 _L5 _L6
_L2:
            return;
_L3:
            XiMaoComm.sendCommand(mCon, 2, 1);
            isFirst = false;
            return;
_L4:
            XiMaoComm.sendCommand(mCon, 8, 1);
            isFirst = false;
            return;
_L5:
            XiMaoComm.sendCommand(mCon, 1, 1);
            isFirst = false;
            return;
_L6:
            XiMaoComm.sendCommand(mCon, 3, 1);
            isFirst = false;
            return;
        }

        _cls2()
        {
            this$0 = XiMaoListFragment.this;
            super();
        }
    }


    private class _cls3 extends TimerTask
    {

        final XiMaoListFragment this$0;

        public void run()
        {
            if (XiMaoBTManager.getInstance(mCon).isSearchContent())
            {
                Log.e("ximao", "\u76EE\u5F55\u8FD4\u56DE\u64CD\u4F5C\u8D85\u65F6");
                XiMaoBTManager.getInstance(mCon).setSearchContent(false);
                MsgHandler.obtainMessage(8).sendToTarget();
            }
        }

        _cls3()
        {
            this$0 = XiMaoListFragment.this;
            super();
        }
    }

}
