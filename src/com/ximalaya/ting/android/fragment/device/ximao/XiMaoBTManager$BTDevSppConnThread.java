// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.device.bluetooth.ximao.XiMaoDownloadModule;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.UUID;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoBTManager, XiMaoVoiceManager, XiMaoWavManager, XiMaoComm

public class mTryTimes extends Thread
{

    private UUID SPP_UUID;
    byte arrayOfByte2[];
    byte arrayOfByte3[];
    int desPos;
    private InputStream mInStream;
    private OutputStream mOutStream;
    private BluetoothSocket mSocket;
    boolean mThreadFinished;
    int mTryTimes;
    int offset;
    final XiMaoBTManager this$0;
    private BluetoothSocket tmp;

    private boolean checkAudioFinish(byte abyte0[], int i)
    {
        return abyte0[i - 1] == 25 && abyte0[i - 2] == 0 && abyte0[i - 3] == 0 && abyte0[i - 4] == 10 && abyte0[i - 5] == 15;
    }

    private void cleanArray()
    {
        arrayOfByte2 = new byte[1024];
        desPos = 0;
        offset = 0;
        arrayOfByte3 = new byte[1024];
    }

    private void cleanup()
    {
        if (mSocket != null)
        {
            try
            {
                mNumContent3 = -1;
                mNumContent1 = -1;
                mNumContent2 = -1;
                mNumContent4 = -1;
                if (mOutStream != null)
                {
                    mOutStream.close();
                    mOutStream = null;
                }
                if (mInStream != null)
                {
                    mInStream.close();
                    mInStream = null;
                }
                mSocket.close();
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
            mSocket = null;
        }
    }

    private boolean createSocket()
    {
        if (device != null)
        {
            break MISSING_BLOCK_LABEL_19;
        }
        Logger.d("ximao", "\u83B7\u53D6\u84DD\u7259\u8BBE\u5907\u5931\u8D25");
        return false;
        if (android.os.ao.XiMaoBTManager.device >= 11) goto _L2; else goto _L1
_L1:
        tmp = device.createRfcommSocketToServiceRecord(SPP_UUID);
_L4:
        mSocket = tmp;
        mSocket.connect();
        XiMaoBTManager.access$202(XiMaoBTManager.this, this._fld0);
        return true;
_L2:
        try
        {
            tmp = device.createInsecureRfcommSocketToServiceRecord(SPP_UUID);
        }
        catch (Exception exception)
        {
            Logger.e("ximao", (new StringBuilder()).append("1st try:").append(exception.toString()).toString());
            exception.printStackTrace();
            XiMaoBTManager.access$202(XiMaoBTManager.this, this._fld0);
            return false;
        }
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception1;
        exception1;
        showToast((new StringBuilder()).append("pos 1").append(exception1.toString()).toString());
        if (mSocket != null)
        {
            mSocket.close();
            tmp.close();
            Thread.sleep(2000L);
            mSocket = null;
            tmp = null;
        }
        mSocket = null;
        tmp = (BluetoothSocket)device.getClass().getMethod("createRfcommSocket", new Class[] {
            Integer.TYPE
        }).invoke(device, new Object[] {
            Integer.valueOf(1)
        });
        mSocket = tmp;
        mSocket.connect();
        XiMaoBTManager.access$202(XiMaoBTManager.this, this._fld0);
        return true;
        Exception exception2;
        exception2;
        showToast((new StringBuilder()).append("pos 3").append(exception2.toString()).toString());
        if (mSocket != null)
        {
            mSocket.close();
            tmp.close();
            Thread.sleep(2000L);
            mSocket = null;
            tmp = null;
        }
        mSocket = null;
        XiMaoBTManager.access$202(XiMaoBTManager.this, this._fld0);
        return false;
        exception2;
        Logger.e("ximao", (new StringBuilder()).append("2nd try").append(exception1.toString()).toString());
        showToast((new StringBuilder()).append("2nd try").append(exception1.toString()).toString());
        XiMaoBTManager.access$202(XiMaoBTManager.this, this._fld0);
        return false;
    }

    private void distributePacket(byte abyte0[], int i)
    {
        if (15 != abyte0[0]) goto _L2; else goto _L1
_L1:
        if (8 != abyte0[1]) goto _L4; else goto _L3
_L3:
        showToast("\u5F00\u59CB\u4E0A\u4F20\u5F55\u97F3");
        Logger.d("ximao", "\u5F00\u59CB\u4F20\u8F93\u667A\u80FD\u8BED\u97F3\u6D41");
        audioTimer = new Timer();
        class _cls2 extends TimerTask
        {

            final XiMaoBTManager.BTDevSppConnThread this$1;

            public void run()
            {
                if (XiMaoBTManager.access$300(this$0))
                {
                    Logger.e("ximao", "\u5F55\u97F3\u6587\u4EF6\u4E0A\u4F20\u8D85\u65F6\uFF0C\u6B63\u5728\u91CD\u7F6E");
                    showToast("\u5F55\u97F3\u6587\u4EF6\u4E0A\u4F20\u8D85\u65F6\uFF0C\u6B63\u5728\u91CD\u7F6E");
                    if (XiMaoBTManager.access$1100(this$0) != null)
                    {
                        XiMaoBTManager.access$1100(this$0).cleanArray();
                        XiMaoBTManager.access$302(this$0, false);
                    }
                }
            }

            _cls2()
            {
                this$1 = XiMaoBTManager.BTDevSppConnThread.this;
                super();
            }
        }

        audioTask = new _cls2();
        audioTimer.schedule(audioTask, 25000L);
        for (; XiMaoBTManager.access$800(XiMaoBTManager.this) == null; XiMaoBTManager.access$800(XiMaoBTManager.this).initVoice())
        {
            XiMaoBTManager.access$802(XiMaoBTManager.this, XiMaoVoiceManager.getInstance(XiMaoBTManager.access$700(XiMaoBTManager.this)));
        }

        XiMaoBTManager.access$800(XiMaoBTManager.this).clearResult();
        class _cls3
            implements Runnable
        {

            final XiMaoBTManager.BTDevSppConnThread this$1;

            public void run()
            {
                Logger.d("TAG", "\u6682\u505C");
                LocalMediaService.getInstance().pause();
            }

            _cls3()
            {
                this$1 = XiMaoBTManager.BTDevSppConnThread.this;
                super();
            }
        }

        if (MyApplication.a() != null)
        {
            MyApplication.a().runOnUiThread(new _cls3());
        }
        XiMaoBTManager.access$302(XiMaoBTManager.this, true);
        XiMaoWavManager.toalmydatasize = 0;
        XiMaoWavManager.databuff = new ByteArrayOutputStream(0x927c00);
_L6:
        return;
_L4:
        if (10 == abyte0[1])
        {
            XiMaoBTManager.access$302(XiMaoBTManager.this, false);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        if (18 == abyte0[0])
        {
            if (abyte0[1] == 255 && XiMaoBTManager.access$1000(XiMaoBTManager.this) != null)
            {
                XiMaoBTManager.access$1100(XiMaoBTManager.this).write(XiMaoBTManager.access$1000(XiMaoBTManager.this), XiMaoBTManager.access$1000(XiMaoBTManager.this).length);
                return;
            }
        } else
        {
            if (abyte0[0] == 13 || abyte0[0] == 17)
            {
                if (XiMaoBTManager.access$400(XiMaoBTManager.this) != null)
                {
                    XiMaoBTManager.access$400(XiMaoBTManager.this).onPacket(abyte0, i);
                } else
                {
                    if (MainTabActivity2.isMainTabActivityAvaliable())
                    {
                        registerXiMaoMainReceiver(MainTabActivity2.mainTabActivity);
                    }
                    Logger.d("ximao", "MainReceiver\u4E3A\u7A7A");
                }
                if (XiMaoBTManager.access$600(XiMaoBTManager.this) != null)
                {
                    XiMaoBTManager.access$600(XiMaoBTManager.this).onPacket(abyte0, i);
                    return;
                } else
                {
                    Logger.d("ximao", "ConnectionReceiver\u4E3A\u7A7A1");
                    return;
                }
            }
            if (abyte0[0] == 14 || abyte0[0] == 9)
            {
                if (XiMaoBTManager.access$400(XiMaoBTManager.this) != null)
                {
                    XiMaoBTManager.access$400(XiMaoBTManager.this).onPacket(abyte0, i);
                    return;
                }
                if (MainTabActivity2.isMainTabActivityAvaliable())
                {
                    registerXiMaoMainReceiver(MainTabActivity2.mainTabActivity);
                }
                Logger.d("ximao", "MainReceiver\u4E3A\u7A7A");
                return;
            }
            if (XiMaoComm.isDownloadResponse(abyte0))
            {
                getDownloadModule().onPacket(abyte0);
                return;
            }
            if (XiMaoBTManager.access$600(XiMaoBTManager.this) != null)
            {
                XiMaoBTManager.access$600(XiMaoBTManager.this).onPacket(abyte0, i);
                return;
            } else
            {
                Logger.d("ximao", "ConnectionReceiver\u4E3A\u7A7A2");
                return;
            }
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    private void write(byte abyte0[], int i)
    {
        XiMaoComm.printPacket("write data", abyte0, i);
        if (mOutStream != null)
        {
            try
            {
                mOutStream.write(abyte0, 0, i);
                XiMaoBTManager.access$1002(XiMaoBTManager.this, new byte[i]);
                System.arraycopy(abyte0, 0, XiMaoBTManager.access$1000(XiMaoBTManager.this), 0, i);
                if (XiMaoBTManager.access$500(XiMaoBTManager.this) && XiMaoBTManager.access$600(XiMaoBTManager.this) != null)
                {
                    byte abyte1[] = new byte[i];
                    System.arraycopy(abyte0, 0, abyte1, 0, i);
                    XiMaoBTManager.access$600(XiMaoBTManager.this).onSent(abyte1, i);
                }
                return;
            }
            // Misplaced declaration of an exception variable
            catch (byte abyte0[])
            {
                Logger.d("ximao", "write error");
            }
            mThreadFinished = true;
            Logger.d("ximao", "write\u5B58\u5728\u5F02\u5E38\uFF0C\u6B63\u5728\u9000\u51FA");
            abyte0.printStackTrace();
            onDisconnected();
            return;
        } else
        {
            Logger.d("ximao", "\u672A\u8FDE\u63A5");
            return;
        }
    }

    public void cancel()
    {
        cleanup();
        mThreadFinished = true;
    }

    public BluetoothSocket getSocket()
    {
        return mSocket;
    }

    public void run()
    {
        mTryTimes = 0;
        if (mSocket != null) goto _L2; else goto _L1
_L1:
        XiMaoBTManager.access$202(XiMaoBTManager.this, this._fld0);
_L5:
        if (mTryTimes <= 3 && !createSocket()) goto _L3; else goto _L2
_L2:
        if (XiMaoBTManager.access$200(XiMaoBTManager.this) == this._fld0)
        {
            onConnectFailed();
            return;
        }
        break; /* Loop/switch isn't completed */
_L3:
        mTryTimes = mTryTimes + 1;
        if (true) goto _L5; else goto _L4
_L4:
        byte abyte3[];
        Logger.d("ximao", (new StringBuilder()).append("create socket").append(mSocket.hashCode()).toString());
        if (mSocket == null)
        {
            mSocket = null;
            Logger.d("ximao", "mSocket\u4E3A\u7A7A\uFF0C\u521B\u5EFA\u5931\u8D25");
            onConnectFailed();
            return;
        }
        byte abyte0[];
        Exception exception;
        try
        {
            mOutStream = mSocket.getOutputStream();
            mInStream = mSocket.getInputStream();
            Logger.d("ximao", (new StringBuilder()).append("create outstream").append(mOutStream.hashCode()).toString());
            Logger.d("ximao", (new StringBuilder()).append("create instream").append(mInStream.hashCode()).toString());
        }
        catch (Exception exception1)
        {
            mSocket = null;
            Logger.d("ximao", "Stream\u83B7\u53D6\u5931\u8D25\uFF0C\u521B\u5EFA\u5931\u8D25");
            onConnectFailed();
        }
        onConnected();
        abyte0 = new byte[1024];
_L14:
        if (mThreadFinished) goto _L7; else goto _L6
_L6:
        abyte3 = new byte[1024];
        if (!mThreadFinished) goto _L8; else goto _L7
_L7:
        mThreadFinished = true;
        return;
_L8:
        String s;
        int i;
        int j;
        try
        {
            Logger.d("ximao", (new StringBuilder()).append("\u7B49\u5F85\u8BFB\u53D6\u6570\u636E:").append(offset).toString());
            mInStream = mSocket.getInputStream();
            i = mInStream.read(abyte3, offset, 1024 - offset);
        }
        // Misplaced declaration of an exception variable
        catch (Exception exception)
        {
            mThreadFinished = true;
            Logger.d("ximao", "SPP\u8FDE\u63A5\u5B58\u5728\u5F02\u5E38\uFF0C\u6B63\u5728\u9000\u51FA");
            exception.printStackTrace();
            onDisconnected();
            return;
        }
        s = "";
        j = 0;
_L10:
        if (j >= i)
        {
            break; /* Loop/switch isn't completed */
        }
        s = (new StringBuilder()).append(s).append(String.valueOf(Integer.toHexString(abyte3[j] & 0xff | 0xffffff00).substring(6))).append(" ").toString();
        j++;
        if (true) goto _L10; else goto _L9
_L9:
        Logger.d("ximao", (new StringBuilder()).append("\u8BFB\u5230\u6570\u636E").append(s).toString());
        if (mThreadFinished) goto _L7; else goto _L11
_L11:
        int k = i;
        if (XiMaoBTManager.access$300(XiMaoBTManager.this))
        {
            break MISSING_BLOCK_LABEL_589;
        }
_L13:
        j = i;
        if (XiMaoComm.isValidHead(abyte3[0]))
        {
            break; /* Loop/switch isn't completed */
        }
        j = i;
        if (i <= 0)
        {
            break; /* Loop/switch isn't completed */
        }
        Logger.d("ximao", "\u8BE5\u6570\u636E\u4E0D\u662F\u5408\u6CD5\u5934");
        Logger.d("ximao", "\u4E0D\u662F\u5408\u6CD5\u5934\uFF0C\u5904\u7406\u4E2D.");
        byte abyte1[] = new byte[1024];
        System.arraycopy(abyte3, 1, abyte1, 0, i - 1);
        System.arraycopy(abyte1, 0, abyte3, 0, i - 1);
        j = i - 1;
        i = j;
        if (j <= 100)
        {
            continue; /* Loop/switch isn't completed */
        }
        cleanArray();
        break; /* Loop/switch isn't completed */
        if (true) goto _L13; else goto _L12
_L12:
        k = j;
        if (XiMaoComm.isValidHead(abyte3[0]))
        {
            break MISSING_BLOCK_LABEL_589;
        }
        k = j;
        if (j <= 100)
        {
            break MISSING_BLOCK_LABEL_589;
        }
        cleanArray();
          goto _L14
        if (k > 0)
        {
            break MISSING_BLOCK_LABEL_605;
        }
        Logger.d("ximao", "finish");
          goto _L14
        if (XiMaoBTManager.access$400(XiMaoBTManager.this) == null)
        {
            registerXiMaoMainReceiver(MyApplication.a());
        }
        if (XiMaoBTManager.access$500(XiMaoBTManager.this))
        {
            byte abyte2[] = new byte[k];
            System.arraycopy(abyte3, offset, abyte2, 0, k);
            XiMaoBTManager.access$600(XiMaoBTManager.this).onReceived(abyte2, k);
        }
        if (!XiMaoBTManager.access$300(XiMaoBTManager.this))
        {
            break MISSING_BLOCK_LABEL_832;
        }
        Logger.d("ximao", "audio data");
        XiMaoWavManager.writeData(abyte3, k);
        if (!checkAudioFinish(XiMaoWavManager.databuff.toByteArray(), XiMaoWavManager.toalmydatasize)) goto _L14; else goto _L15
_L15:
        if (audioTimer != null)
        {
            audioTimer.cancel();
        }
        XiMaoBTManager.access$302(XiMaoBTManager.this, false);
        XiMaoWavManager.finishAudio(XiMaoBTManager.access$700(XiMaoBTManager.this));
        if (XiMaoBTManager.access$800(XiMaoBTManager.this) == null)
        {
            XiMaoBTManager.access$802(XiMaoBTManager.this, XiMaoVoiceManager.getInstance(XiMaoBTManager.access$700(XiMaoBTManager.this)));
            class _cls1
                implements Runnable
            {

                final XiMaoBTManager.BTDevSppConnThread this$1;

                public void run()
                {
                    XiMaoBTManager.access$800(this$0).initVoice();
                }

            _cls1()
            {
                this$1 = XiMaoBTManager.BTDevSppConnThread.this;
                super();
            }
            }

            MyApplication.a().runOnUiThread(new _cls1());
        }
        if (XiMaoWavManager.toalmydatasize <= 98)
        {
            break MISSING_BLOCK_LABEL_819;
        }
        XiMaoBTManager.access$800(XiMaoBTManager.this).writeaudio(XiMaoWavManager.buffers);
        XiMaoWavManager.buffers = null;
_L16:
        XiMaoWavManager.cleanUp();
          goto _L14
        CustomToast.showToast(MyApplication.a(), "\u667A\u80FD\u8BED\u97F3\u6570\u636E\u4E0A\u4F20\u5931\u8D25", 0);
          goto _L16
label0:
        {
            if (desPos + k < 1024)
            {
                break label0;
            }
            showToast("pos1:\u6570\u636E\u683C\u5F0F\u4E0D\u6B63\u786E");
            cleanArray();
        }
          goto _L14
        System.arraycopy(abyte3, 0, arrayOfByte2, desPos, k);
        desPos = k + desPos;
        i = XiMaoBTManager.access$900(XiMaoBTManager.this, arrayOfByte2, desPos);
        if (i == -1) goto _L14; else goto _L17
_L17:
        if (i >= 0 && i <= 1024)
        {
            break MISSING_BLOCK_LABEL_937;
        }
        showToast("pos2:\u6570\u636E\u683C\u5F0F\u4E0D\u6B63\u786E");
        cleanArray();
          goto _L14
        if (XiMaoComm.check(arrayOfByte2, i))
        {
            distributePacket(arrayOfByte2, i);
        }
        if (i >= desPos)
        {
            break MISSING_BLOCK_LABEL_1226;
        }
        j = desPos - i;
        System.arraycopy(arrayOfByte2, i, arrayOfByte3, 0, j);
        k = XiMaoBTManager.access$900(XiMaoBTManager.this, arrayOfByte3, j);
        if (k != -1) goto _L19; else goto _L18
_L18:
        arrayOfByte2 = new byte[1024];
        System.arraycopy(arrayOfByte3, 0, arrayOfByte2, 0, j);
        desPos = j;
_L24:
        offset = 0;
        arrayOfByte3 = new byte[1024];
          goto _L14
_L19:
        i = k;
        if (k >= -1)
        {
            break MISSING_BLOCK_LABEL_1072;
        }
        cleanArray();
          goto _L14
_L21:
        if (i == -1 || i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        distributePacket(arrayOfByte3, i);
        j -= i;
        arrayOfByte2 = new byte[1024];
        System.arraycopy(arrayOfByte3, i, arrayOfByte2, 0, j);
        arrayOfByte3 = new byte[1024];
        System.arraycopy(arrayOfByte2, 0, arrayOfByte3, 0, j);
        i = XiMaoBTManager.access$900(XiMaoBTManager.this, arrayOfByte3, j);
        if (true) goto _L21; else goto _L20
_L20:
        if (i != j) goto _L23; else goto _L22
_L22:
        distributePacket(arrayOfByte3, i);
        cleanArray();
          goto _L24
_L23:
        if (i != -1) goto _L24; else goto _L25
_L25:
        i = j - i;
        arrayOfByte2 = new byte[1024];
        System.arraycopy(arrayOfByte3, 0, arrayOfByte2, 0, i);
        desPos = i;
          goto _L24
        arrayOfByte2 = new byte[1024];
        desPos = 0;
          goto _L24
    }






    public _cls1(String s)
    {
        this$0 = XiMaoBTManager.this;
        super();
        SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
        mSocket = null;
        tmp = null;
        mOutStream = null;
        mInStream = null;
        offset = 0;
        desPos = 0;
        arrayOfByte2 = new byte[1024];
        arrayOfByte3 = new byte[1024];
        mThreadFinished = false;
        mTryTimes = 0;
        if (getAdapter() == null)
        {
            return;
        } else
        {
            device = getAdapter().getRemoteDevice(s);
            return;
        }
    }
}
