// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.content.Context;
import android.view.View;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download.BaseSoundsDownloadForAlbumAdapter;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class XiMaoSoundsDownloadForAlbumAdapter extends BaseSoundsDownloadForAlbumAdapter
{

    private MenuDialog mDeviceMenuDialog;

    public XiMaoSoundsDownloadForAlbumAdapter(Context context, List list)
    {
        super(context, list);
    }

    private void showDirectionChoose(final List list)
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add("\u6545\u4E8B");
        arraylist.add("\u513F\u6B4C");
        arraylist.add("\u6027\u683C");
        arraylist.add("\u5988\u5988\u8BF4");
        if (mDeviceMenuDialog == null)
        {
            mDeviceMenuDialog = new MenuDialog(MyApplication.a(), arraylist, a.e, new _cls1(), 1);
        } else
        {
            mDeviceMenuDialog.setSelections(arraylist);
        }
        mDeviceMenuDialog.setHeaderTitle("\u6DFB\u52A0\u81F3\u6587\u4EF6\u5939");
        mDeviceMenuDialog.show();
    }

    public void onClick(View view)
    {
        view = (com.ximalaya.ting.android.adapter.SoundsDownloadForAlbumAdapter.ViewHolder)view.getTag();
        long l = ((Long)mapKey.get(((com.ximalaya.ting.android.adapter.SoundsDownloadForAlbumAdapter.ViewHolder) (view)).position)).longValue();
        showDirectionChoose((List)albumMap.get(Long.valueOf(l)));
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final XiMaoSoundsDownloadForAlbumAdapter this$0;
        final List val$list;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            XiMaoBTManager.getInstance(XiMaoSoundsDownloadForAlbumAdapter.this.Object).getDownloadModule().download(list, i + 1);
        }

        _cls1()
        {
            this$0 = XiMaoSoundsDownloadForAlbumAdapter.this;
            list = list1;
            super();
        }
    }

}
