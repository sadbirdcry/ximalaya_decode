// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoComm

public class XiMaoSettingFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    boolean isOnTouch;
    private ImageView mBackImg;
    private TextView mSetting1Info;
    private SwitchButton mSetting1Switch;
    private RelativeLayout mSetting2;
    private TextView mSetting2Info;
    private SwitchButton mSetting2Switch;
    private TextView mSetting3Info;
    private SwitchButton mSetting3Switch;
    private TextView top_tv;

    public XiMaoSettingFragment()
    {
        isOnTouch = false;
    }

    private void initView()
    {
        top_tv = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        top_tv.setText("\u8BBE\u7F6E");
        mBackImg.setOnClickListener(this);
        mSetting1Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a02fe);
        mSetting1Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a02ff);
        mSetting2Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a030d);
        mSetting2 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a030a);
        mSetting2Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a030e);
        mSetting3Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a0308);
        mSetting3Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0309);
        mSetting2.setOnClickListener(this);
        mSetting2Switch.setVisibility(8);
        mSetting2Info.setVisibility(0);
        mSetting1Info.setVisibility(8);
        mSetting1Switch.setVisibility(0);
        mSetting1Switch.setChecked(XiMaoComm.isLockVoiceSearch(mCon));
        mSetting1Switch.setOnCheckedChangeListener(new _cls1());
        mSetting3Info.setVisibility(8);
        mSetting3Switch.setVisibility(0);
        mSetting3Switch.setChecked(XiMaoComm.isLockBind(mCon));
        mSetting3Switch.setOnCheckedChangeListener(new _cls2());
    }

    public void init()
    {
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initView();
        init();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362570: 
            XiMaoComm.goIntroduction(getActivity());
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300eb, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onResume()
    {
        super.onResume();
    }

    public void showDebugToast(String s)
    {
        CustomToast.showToast(mCon, s, 0);
    }

    public void showToast(String s)
    {
        CustomToast.showToast(mCon, s, 0);
    }

    private class _cls1
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final XiMaoSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            if (flag)
            {
                XiMaoComm.setLockVoiceSearch(true, mCon);
                return;
            } else
            {
                XiMaoComm.setLockVoiceSearch(false, mCon);
                return;
            }
        }

        _cls1()
        {
            this$0 = XiMaoSettingFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final XiMaoSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            if (flag)
            {
                XiMaoComm.setLockBind(true, mCon);
                return;
            } else
            {
                XiMaoComm.setLockBind(false, mCon);
                return;
            }
        }

        _cls2()
        {
            this$0 = XiMaoSettingFragment.this;
            super();
        }
    }

}
