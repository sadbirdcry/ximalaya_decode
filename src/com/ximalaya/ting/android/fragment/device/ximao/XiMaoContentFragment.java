// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.util.Logger;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoMainFragmentNew, XiMaoBindFragment, XiMaoBTManager, XiMaoSettingFragment, 
//            XiMaoComm

public class XiMaoContentFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, XiMaoBTManager.BTDevConnectionReceiver
{
    public class MyPagerAdapter extends FragmentPagerAdapter
    {

        private final String TITLES[] = {
            "\u5185\u7F6E\u7684\u58F0\u97F3", "\u7ED1\u5B9A\u7684\u4E13\u8F91"
        };
        final XiMaoContentFragment this$0;

        public int getCount()
        {
            return TITLES.length;
        }

        public Fragment getItem(int i)
        {
            if (i == 0)
            {
                if (mXiMaoMainFragment == null)
                {
                    mXiMaoMainFragment = new XiMaoMainFragmentNew();
                }
                return mXiMaoMainFragment;
            }
            if (i == 1)
            {
                if (mXiMaoBindFragment == null)
                {
                    mXiMaoBindFragment = new XiMaoBindFragment();
                }
                return mXiMaoBindFragment;
            }
            if (mXiMaoMainFragment == null)
            {
                mXiMaoMainFragment = new XiMaoMainFragmentNew();
            }
            return mXiMaoMainFragment;
        }

        public CharSequence getPageTitle(int i)
        {
            return TITLES[i];
        }

        public MyPagerAdapter(FragmentManager fragmentmanager)
        {
            this$0 = XiMaoContentFragment.this;
            super(fragmentmanager);
        }
    }


    private static final String TAG = "ximao";
    public static boolean checkPlayMode = true;
    private static boolean isMainFragment = false;
    private boolean isFirst;
    private boolean isSearchFinish;
    private ImageView mBackImg;
    private TextView mBtDisConn;
    private RelativeLayout mContentView;
    private TextView mTopTv;
    private XiMaoBindFragment mXiMaoBindFragment;
    private XiMaoMainFragmentNew mXiMaoMainFragment;
    private PagerSlidingTabStrip tabs;
    private Timer timer;
    private int tryTimes;

    public XiMaoContentFragment()
    {
        isSearchFinish = true;
        isFirst = true;
        timer = null;
        tryTimes = 0;
    }

    private void initLocalInfos()
    {
        isSearchFinish = true;
        timer = new Timer();
        _cls1 _lcls1 = new _cls1();
        timer.scheduleAtFixedRate(_lcls1, 0L, 5000L);
    }

    private void initView()
    {
        mBtDisConn = (TextView)mContentView.findViewById(0x7f0a071c);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mTopTv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mBtDisConn.setText("\u8BBE\u7F6E");
        mBtDisConn.setVisibility(0);
        mTopTv.setText("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A");
        mBtDisConn.setOnClickListener(this);
        mBackImg.setOnClickListener(this);
        tabs = (PagerSlidingTabStrip)mContentView.findViewById(0x7f0a02dc);
        Object obj = (ViewPager)mContentView.findViewById(0x7f0a017a);
        ((ViewPager) (obj)).setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        ((ViewPager) (obj)).setPageMargin(0);
        tabs.setViewPager(((ViewPager) (obj)));
        obj = new _cls3();
        tabs.setOnPageChangeListener(((android.support.v4.view.ViewPager.OnPageChangeListener) (obj)));
    }

    public void handleMessage(int i, int j, int k, byte abyte0[])
    {
        if (mXiMaoMainFragment != null && mXiMaoMainFragment.MsgHandler != null)
        {
            mXiMaoMainFragment.MsgHandler.obtainMessage(i, 0, k, abyte0).sendToTarget();
        }
        if (mXiMaoBindFragment != null && mXiMaoBindFragment.MsgHandler != null)
        {
            mXiMaoBindFragment.MsgHandler.obtainMessage(i, 0, k, abyte0).sendToTarget();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        XiMaoBTManager.getInstance(mCon).setmBTDevConnectionReceiver(this);
        initView();
        tryTimes = 0;
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;

        case 2131363612: 
            startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoSettingFragment, null);
            break;
        }
    }

    public void onConnectFailed()
    {
    }

    public void onConnected()
    {
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300e2, viewgroup, false);
        return mContentView;
    }

    public void onDestroy()
    {
        tabs.setOnPageChangeListener(null);
        super.onDestroy();
    }

    public void onDisconnected()
    {
    }

    public void onPacket(byte abyte0[], int i)
    {
        int j = XiMaoBTManager.getInstance(mCon).getPacketSize(abyte0);
        if (j == 0)
        {
            handleMessage(6, 0, i, abyte0);
        } else
        {
            if (j == 1 || abyte0[1] == 19)
            {
                handleMessage(6, 0, i, abyte0);
                return;
            }
            if (j >= 2)
            {
                handleMessage(5, 0, i, abyte0);
                return;
            }
        }
    }

    public void onReceived(byte abyte0[], int i)
    {
        handleMessage(3, 0, i, abyte0);
    }

    public void onResume()
    {
        super.onResume();
        Logger.d("ximao", "XiMaoContentFragment onResume");
        showPlayButton();
        isMainFragment = true;
        if (XiMaoBTManager.getInstance(mCon).getConnState() != XiMaoComm.STATE.CONNECTED)
        {
            if (getActivity() != null)
            {
                getActivity().onBackPressed();
            }
        } else
        {
            XiMaoBTManager.getInstance(mCon).setmBTDevConnectionReceiver(this);
            if (XiMaoBTManager.getInstance(mCon).isAudio())
            {
                (new Thread(new _cls2())).start();
            } else
            {
                isFirst = true;
                initLocalInfos();
            }
            if (mXiMaoBindFragment != null)
            {
                mXiMaoBindFragment.onResume();
            }
            if (mXiMaoMainFragment != null)
            {
                mXiMaoMainFragment.onResume();
                return;
            }
        }
    }

    public void onSent(byte abyte0[], int i)
    {
        handleMessage(4, 0, i, abyte0);
    }

    public void onStop()
    {
        super.onStop();
        if (timer != null)
        {
            timer.cancel();
            timer = null;
        }
        isMainFragment = false;
        Logger.d("ximao", "XiMaoContentFragment onStop");
        XiMaoComm.removeBTDevConnectionReceiver(mCon);
    }

    public void onTimeout()
    {
    }




/*
    static int access$008(XiMaoContentFragment ximaocontentfragment)
    {
        int i = ximaocontentfragment.tryTimes;
        ximaocontentfragment.tryTimes = i + 1;
        return i;
    }

*/



/*
    static boolean access$202(XiMaoContentFragment ximaocontentfragment, boolean flag)
    {
        ximaocontentfragment.isSearchFinish = flag;
        return flag;
    }

*/




/*
    static XiMaoMainFragmentNew access$402(XiMaoContentFragment ximaocontentfragment, XiMaoMainFragmentNew ximaomainfragmentnew)
    {
        ximaocontentfragment.mXiMaoMainFragment = ximaomainfragmentnew;
        return ximaomainfragmentnew;
    }

*/



/*
    static XiMaoBindFragment access$502(XiMaoContentFragment ximaocontentfragment, XiMaoBindFragment ximaobindfragment)
    {
        ximaocontentfragment.mXiMaoBindFragment = ximaobindfragment;
        return ximaobindfragment;
    }

*/

    private class _cls1 extends TimerTask
    {

        final XiMaoContentFragment this$0;

        public void run()
        {
            if (tryTimes > 3)
            {
                return;
            }
            try
            {
                int i = 
// JavaClassFileOutputException: get_constant: invalid tag

        _cls1()
        {
            this$0 = XiMaoContentFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final XiMaoContentFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f, int j)
        {
        }

        public void onPageSelected(int i)
        {
        }

        _cls3()
        {
            this$0 = XiMaoContentFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final XiMaoContentFragment this$0;

        public void run()
        {
            while (XiMaoBTManager.getInstance(mCon).isAudio()) 
            {
                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
            }
            int i;
            if (getManageFragment() != null)
            {
                if ((i = getManageFragment().mStacks.size()) > 0 && ((Fragment)((SoftReference)getManageFragment().mStacks.get(i - 1)).get() instanceof XiMaoContentFragment))
                {
                    initLocalInfos();
                    return;
                }
            }
        }

        _cls2()
        {
            this$0 = XiMaoContentFragment.this;
            super();
        }
    }

}
