// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download.BaseDownloadSoundsListForAlbumFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download.BaseDownloadedSoundListAdapter;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoDownloadedSoundListAdapter

public class XiMaoDownloadSoundsListForAlbumFragment extends BaseDownloadSoundsListForAlbumFragment
{

    public XiMaoDownloadSoundsListForAlbumFragment()
    {
    }

    public BaseDownloadedSoundListAdapter getAdapter(List list)
    {
        if (soundsDownloadAdapter == null)
        {
            soundsDownloadAdapter = new XiMaoDownloadedSoundListAdapter(getActivity(), list);
        }
        return soundsDownloadAdapter;
    }
}
