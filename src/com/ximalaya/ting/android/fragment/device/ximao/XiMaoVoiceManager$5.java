// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechError;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoVoiceManager, XiMaoComm

class this._cls0
    implements RecognizerListener
{

    final XiMaoVoiceManager this$0;

    public void onBeginOfSpeech()
    {
        XiMaoVoiceManager.access$200(XiMaoVoiceManager.this, "\u5F00\u59CB\u8BC6\u522B\u5F55\u97F3");
        XiMaoVoiceManager.access$402(XiMaoVoiceManager.this, "");
    }

    public void onEndOfSpeech()
    {
        Log.d("MscSpeechLog", "\u8BC6\u522B\u5F55\u97F3\u7ED3\u675F");
    }

    public void onError(SpeechError speecherror)
    {
        if (speecherror.getErrorCode() >= 20001 && speecherror.getErrorCode() <= 20003 || speecherror.getErrorCode() == 10114 || speecherror.getErrorCode() == 10200)
        {
            Logger.d("MscSpeechLog", (new StringBuilder()).append("No Network").append(speecherror).toString());
            XiMaoComm.playNoData(XiMaoVoiceManager.access$300(XiMaoVoiceManager.this));
            XiMaoVoiceManager.access$200(XiMaoVoiceManager.this, speecherror.getErrorDescription());
            return;
        } else
        {
            Logger.d("MscSpeechLog", (new StringBuilder()).append("RecognizerListener onError").append(speecherror).toString());
            XiMaoComm.playSayAgain(XiMaoVoiceManager.access$300(XiMaoVoiceManager.this));
            XiMaoVoiceManager.access$200(XiMaoVoiceManager.this, speecherror.getErrorDescription());
            return;
        }
    }

    public void onEvent(int i, int j, int k, Bundle bundle)
    {
    }

    public void onResult(RecognizerResult recognizerresult, boolean flag)
    {
label0:
        {
            handleVoiceResult(recognizerresult);
            if (flag)
            {
                XiMaoVoiceManager.access$200(XiMaoVoiceManager.this, "\u8BC6\u522B\u5F55\u97F3\u7ED3\u675F");
                XiMaoVoiceManager.access$200(XiMaoVoiceManager.this, XiMaoVoiceManager.access$400(XiMaoVoiceManager.this));
                recognizerresult = XiMaoVoiceManager.access$500(XiMaoVoiceManager.ignorePunction(XiMaoVoiceManager.access$400(XiMaoVoiceManager.this)));
                if (isShowResult)
                {
                    CustomToast.showToast(XiMaoVoiceManager.access$300(XiMaoVoiceManager.this), (new StringBuilder()).append("\u6B63\u5728\u641C\u7D22\u201C").append(recognizerresult).append("\u201D").toString(), 0);
                }
                if (TextUtils.isEmpty(recognizerresult))
                {
                    break label0;
                }
                searchAndPlay(recognizerresult);
            }
            return;
        }
        XiMaoComm.playSayAgain(XiMaoVoiceManager.access$300(XiMaoVoiceManager.this));
    }

    public void onVolumeChanged(int i)
    {
    }

    I()
    {
        this$0 = XiMaoVoiceManager.this;
        super();
    }
}
