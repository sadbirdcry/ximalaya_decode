// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.ximao;

import android.app.Activity;
import android.app.AlertDialog;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.Logger;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.ximao:
//            XiMaoContentFragment, XiMaoBTManager, XiMaoComm

public class XiMaoConnFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    static class MyHandler extends Handler
    {

        WeakReference mFragment;

        public void handleMessage(Message message)
        {
            XiMaoConnFragment ximaoconnfragment = (XiMaoConnFragment)mFragment.get();
            if (ximaoconnfragment.isAdded())
            {
                ximaoconnfragment.onMessage(message);
            }
        }

        MyHandler(XiMaoConnFragment ximaoconnfragment)
        {
            mFragment = new WeakReference(ximaoconnfragment);
        }
    }


    private static final int MSG_BTDEV_CONNECTED = 1;
    private static final int MSG_BTDEV_DISCONNECTED = 2;
    private static final int MSG_BTDEV_FAILED = 6;
    private static final int MSG_BTDEV_PACKET = 5;
    private static final int MSG_BTDEV_RECEIVED = 3;
    private static final int MSG_BTDEV_SENT = 4;
    private static final int MSG_BTDEV_TIMEOUT = 7;
    public static final int REQUEST_ENABLE_BT = 10;
    protected static final String TAG = "ximao";
    public static final String TARGET_DEVICE = "AD_995";
    static boolean receiver_registered = false;
    private MyHandler MsgHandler;
    private Button btn_send;
    android.app.AlertDialog.Builder builder;
    private String from;
    public boolean isBtEnable;
    private boolean isStartConningTvAnim;
    private ImageView mBackImg;
    private TextView mBtDisConn;
    private TextView mConnBtn;
    private TextView mConnTipsTv;
    private RelativeLayout mConnectedLayout;
    private XiMaoBTManager.BTDevConnectionReceiver mConnectionReceiver;
    private RelativeLayout mConningLayout;
    private RelativeLayout mContentView;
    List mDeviceList;
    private AlertDialog mDiscoveryDialog;
    private RelativeLayout mFailedLayout;
    private RelativeLayout mStartLayout;
    private Timer mTimer;
    private TextView mTopTv;

    public XiMaoConnFragment()
    {
        from = null;
        mDeviceList = new ArrayList();
        mConnectionReceiver = new _cls1();
        MsgHandler = new MyHandler(this);
        isStartConningTvAnim = false;
    }

    private void cancelConningTvAnim()
    {
        if (!isStartConningTvAnim)
        {
            return;
        } else
        {
            isStartConningTvAnim = false;
            mTimer.cancel();
            return;
        }
    }

    private void fresh2Connected()
    {
        if (from == null)
        {
            cancelConningTvAnim();
            removeTopFramentFromManageFragment();
            startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment, null);
            return;
        } else
        {
            cancelConningTvAnim();
            removeTopFramentFromManageFragment();
            return;
        }
    }

    private void fresh2Connecting()
    {
        mStartLayout.setVisibility(8);
        mConningLayout.setVisibility(0);
        mFailedLayout.setVisibility(8);
        mConnectedLayout.setVisibility(8);
        startConningTvAnim();
    }

    private void fresh2Failed()
    {
        mStartLayout.setVisibility(8);
        mConningLayout.setVisibility(8);
        mConnectedLayout.setVisibility(8);
        mFailedLayout.setVisibility(0);
    }

    private void fresh2Start()
    {
        if (!isAdded())
        {
            return;
        } else
        {
            mStartLayout.setVisibility(0);
            mConningLayout.setVisibility(8);
            mFailedLayout.setVisibility(8);
            mConnectedLayout.setVisibility(8);
            cancelConningTvAnim();
            return;
        }
    }

    private void freshView()
    {
        Logger.d("ximao", (new StringBuilder()).append("freshView:state").append(XiMaoBTManager.getInstance(mCon).getConnState()).toString());
        static class _cls5
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$STATE[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$STATE = new int[XiMaoComm.STATE.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$STATE[XiMaoComm.STATE.START.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$STATE[XiMaoComm.STATE.CONNECTING.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$STATE[XiMaoComm.STATE.CONNECTED.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$ximao$XiMaoComm$STATE[XiMaoComm.STATE.FAILED.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        switch (_cls5..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE[XiMaoBTManager.getInstance(mCon).getConnState().ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            fresh2Start();
            return;

        case 2: // '\002'
            fresh2Connecting();
            return;

        case 3: // '\003'
            fresh2Connected();
            return;

        case 4: // '\004'
            fresh2Failed();
            break;
        }
    }

    private void initView()
    {
        mBtDisConn = (TextView)mContentView.findViewById(0x7f0a071c);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mTopTv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mBtDisConn.setOnClickListener(this);
        mBackImg.setOnClickListener(this);
        mTopTv.setOnClickListener(this);
        mConningLayout = (RelativeLayout)mContentView.findViewById(0x7f0a031f);
        mFailedLayout = (RelativeLayout)mContentView.findViewById(0x7f0a0324);
        mConnectedLayout = (RelativeLayout)mContentView.findViewById(0x7f0a0323);
        btn_send = (Button)mContentView.findViewById(0x7f0a0312);
        btn_send.setOnClickListener(this);
        mStartLayout = (RelativeLayout)mContentView.findViewById(0x7f0a0092);
        mConnBtn = (TextView)(TextView)mContentView.findViewById(0x7f0a031c);
        ((TextView)(TextView)mContentView.findViewById(0x7f0a00ae)).setText("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A");
        mConnTipsTv = (TextView)(TextView)mContentView.findViewById(0x7f0a0322);
        hidePlayButton();
        ((TextView)(TextView)mContentView.findViewById(0x7f0a031b)).setOnClickListener(this);
        ((TextView)(TextView)mContentView.findViewById(0x7f0a0329)).setOnClickListener(this);
        mConnBtn.setOnClickListener(this);
    }

    private void onBack()
    {
        _cls5..SwitchMap.com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE[XiMaoBTManager.getInstance(mCon).getConnState().ordinal()];
        JVM INSTR tableswitch 1 4: default 48
    //                   1 53
    //                   2 63
    //                   3 86
    //                   4 96;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        freshView();
        return;
_L2:
        mActivity.onBackPressed();
        continue; /* Loop/switch isn't completed */
_L3:
        XiMaoBTManager.getInstance(mCon).setConnState(XiMaoComm.STATE.START);
        mActivity.onBackPressed();
        continue; /* Loop/switch isn't completed */
_L4:
        mActivity.onBackPressed();
        continue; /* Loop/switch isn't completed */
_L5:
        XiMaoBTManager.getInstance(mCon).setConnState(XiMaoComm.STATE.START);
        mActivity.onBackPressed();
        if (true) goto _L1; else goto _L6
_L6:
    }

    private void startConningTvAnim()
    {
        if (isStartConningTvAnim)
        {
            return;
        } else
        {
            mTimer = new Timer();
            isStartConningTvAnim = true;
            mTimer.schedule(new _cls4(), 500L, 500L);
            return;
        }
    }

    private void toBuy()
    {
        AudioManager audiomanager = (AudioManager)getActivity().getSystemService("audio");
        if (audiomanager.isBluetoothScoOn())
        {
            audiomanager.setMode(2);
            audiomanager.setBluetoothScoOn(false);
            audiomanager.setSpeakerphoneOn(true);
            return;
        } else
        {
            audiomanager.setMode(0);
            audiomanager.setBluetoothScoOn(true);
            audiomanager.setSpeakerphoneOn(false);
            return;
        }
    }

    private void toConn()
    {
        if (XiMaoBTManager.getInstance(mCon).isConnected())
        {
            fresh2Connected();
            return;
        } else
        {
            XiMaoBTManager.getInstance(mCon).setConnState(XiMaoComm.STATE.CONNECTING);
            freshView();
            (new Thread(new _cls3())).start();
            return;
        }
    }

    public void init()
    {
        initView();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        XiMaoBTManager.getInstance(mCon).enableBlueTooth();
        init();
        toConn();
    }

    public void onBtDisable()
    {
        isBtEnable = false;
    }

    public void onBtEnable()
    {
        isBtEnable = true;
        toConn();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362587: 
            toBuy();
            return;

        case 2131362588: 
            toConn();
            return;

        case 2131362601: 
            toConn();
            return;

        case 2131363611: 
            mActivity.onBackPressed();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        if (getArguments() != null && getArguments().containsKey("from"))
        {
            from = getArguments().getString("from");
        }
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f030170, viewgroup, false);
        return mContentView;
    }

    public void onDestroy()
    {
        cancelConningTvAnim();
        super.onDestroy();
    }

    public void onMessage(Message message)
    {
        switch (message.what)
        {
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
        default:
            return;

        case 6: // '\006'
            fresh2Failed();
            return;

        case 7: // '\007'
            fresh2Failed();
            return;

        case 1: // '\001'
            break;
        }
        if (mDiscoveryDialog != null)
        {
            mDiscoveryDialog.dismiss();
        }
        fresh2Connected();
    }

    public void onResume()
    {
        super.onResume();
        XiMaoBTManager.getInstance(mCon).setmBTDevConnectionReceiver(mConnectionReceiver);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new _cls2());
        freshView();
    }

    public void onStop()
    {
        super.onStop();
        XiMaoComm.removeBTDevConnectionReceiver(mCon);
    }

    public void showToast(String s)
    {
        if (isAdded())
        {
            Toast.makeText(getActivity(), s, 0).show();
        }
    }





    private class _cls1
        implements XiMaoBTManager.BTDevConnectionReceiver
    {

        final XiMaoConnFragment this$0;

        public void onConnectFailed()
        {
            MsgHandler.obtainMessage(6).sendToTarget();
        }

        public void onConnected()
        {
            MsgHandler.obtainMessage(1).sendToTarget();
        }

        public void onDisconnected()
        {
            MsgHandler.obtainMessage(2).sendToTarget();
        }

        public void onPacket(byte abyte0[], int i)
        {
            abyte0 = new String(abyte0, 4, XiMaoBTManager.getInstance(mCon).getPacketSize(abyte0));
            MsgHandler.obtainMessage(5, 0, 0, abyte0).sendToTarget();
        }

        public void onReceived(byte abyte0[], int i)
        {
            MsgHandler.obtainMessage(3, 0, i, abyte0).sendToTarget();
        }

        public void onSent(byte abyte0[], int i)
        {
            MsgHandler.obtainMessage(4, 0, i, abyte0).sendToTarget();
        }

        public void onTimeout()
        {
            MsgHandler.obtainMessage(7).sendToTarget();
        }

        _cls1()
        {
            this$0 = XiMaoConnFragment.this;
            super();
        }
    }


    private class _cls4 extends TimerTask
    {

        int i;
        final String strings[] = {
            "", ".", "..", "..."
        };
        final XiMaoConnFragment this$0;

        public void run()
        {
            class _cls1
                implements Runnable
            {

                final _cls4 this$1;

                public void run()
                {
                    _cls4 _lcls4 = _cls4.this;
                    _lcls4.i = _lcls4.i + 1;
                    mConnTipsTv.setText(strings[i % 4]);
                }

                _cls1()
                {
                    this$1 = _cls4.this;
                    super();
                }
            }

            getActivity().runOnUiThread(new _cls1());
        }

        _cls4()
        {
            this$0 = XiMaoConnFragment.this;
            super();
            i = 0;
        }
    }


    private class _cls3
        implements Runnable
    {

        final XiMaoConnFragment this$0;

        public void run()
        {
            XiMaoBTManager.getInstance(mCon).enableBlueTooth();
            XiMaoBTManager.getInstance(mCon).tryConnect(true);
        }

        _cls3()
        {
            this$0 = XiMaoConnFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnKeyListener
    {

        final XiMaoConnFragment this$0;

        public boolean onKey(View view, int i, KeyEvent keyevent)
        {
            if (keyevent.getAction() == 1 && i == 4)
            {
                onBack();
                return true;
            } else
            {
                return false;
            }
        }

        _cls2()
        {
            this$0 = XiMaoConnFragment.this;
            super();
        }
    }

}
