// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.app.Activity;
import android.app.ActivityManager;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.IBinder;
import android.text.TextUtils;
import android.widget.Toast;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceUtil;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.modelmanage.DexManager;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            AlbumBinder, ProductModel

public class MyDeviceManager
{
    public static final class DeviceType extends Enum
    {

        private static final DeviceType $VALUES[];
        public static final DeviceType Qsuicheting;
        public static final DeviceType doss;
        public static final DeviceType other;
        public static final DeviceType shukewifi;
        public static final DeviceType suicheting;
        public static final DeviceType tingshubao;
        public static final DeviceType unReg;
        public static final DeviceType ximao;

        public static DeviceType valueOf(String s)
        {
            return (DeviceType)Enum.valueOf(com/ximalaya/ting/android/fragment/device/MyDeviceManager$DeviceType, s);
        }

        public static DeviceType[] values()
        {
            return (DeviceType[])$VALUES.clone();
        }

        static 
        {
            unReg = new DeviceType("unReg", 0);
            suicheting = new DeviceType("suicheting", 1);
            Qsuicheting = new DeviceType("Qsuicheting", 2);
            ximao = new DeviceType("ximao", 3);
            tingshubao = new DeviceType("tingshubao", 4);
            doss = new DeviceType("doss", 5);
            shukewifi = new DeviceType("shukewifi", 6);
            other = new DeviceType("other", 7);
            $VALUES = (new DeviceType[] {
                unReg, suicheting, Qsuicheting, ximao, tingshubao, doss, shukewifi, other
            });
        }

        private DeviceType(String s, int i)
        {
            super(s, i);
        }
    }


    public static final int MiniAPILEVEL = 11;
    public static final String P_QSUICHETING_FIRST_USED = "P_QSUICHETING_FIRST_USED";
    public static final String P_SUICHETING_FIRST_USED = "P_SUICHETING_FIRST_USED";
    public static final String P_TINGSHUBAO_FIRST_USED = "P_TINGSHUBAO_FIRST_USED";
    public static final String P_TINGSHUBAO_INIT_SET = "P_TINGSHUBAO_INIT_SET";
    public static final String SUICHETING_CANUSE = "suicheting";
    public static final String SUICHETING_FILTER_ADDR = "C9:32:";
    private static final String TAG = com/ximalaya/ting/android/fragment/device/MyDeviceManager.getName();
    public static final String XIMAO_CANUSE = "suicheting";
    private static MyDeviceManager mMyDeviceManager;
    private Context mContext;
    private boolean mIsDlnaCanUse;
    private DeviceType mNowDeviceType;
    private List productModels;
    public String sharePicUrl;

    private MyDeviceManager(Context context)
    {
        productModels = new ArrayList();
        sharePicUrl = "";
        mNowDeviceType = DeviceType.unReg;
        mIsDlnaCanUse = false;
        mContext = context;
    }

    public static MyDeviceManager getInstance(Context context)
    {
        if (mMyDeviceManager != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/fragment/device/MyDeviceManager;
        JVM INSTR monitorenter ;
        if (mMyDeviceManager == null)
        {
            mMyDeviceManager = new MyDeviceManager(context.getApplicationContext());
        }
        com/ximalaya/ting/android/fragment/device/MyDeviceManager;
        JVM INSTR monitorexit ;
_L2:
        if (context != null)
        {
            mMyDeviceManager.mContext = context;
        }
        return mMyDeviceManager;
        context;
        com/ximalaya/ting/android/fragment/device/MyDeviceManager;
        JVM INSTR monitorexit ;
        throw context;
    }

    public static boolean isApplicationBroughtToBackground(Context context)
    {
        List list = ((ActivityManager)context.getSystemService("activity")).getRunningTasks(1);
        return !list.isEmpty() && !((android.app.ActivityManager.RunningTaskInfo)list.get(0)).topActivity.getPackageName().equals(context.getPackageName());
    }

    private boolean isQsuicheting(BluetoothDevice bluetoothdevice)
    {
        return BluetoothManager.getInstance(mContext).checkType(bluetoothdevice) == DeviceType.Qsuicheting;
    }

    public static boolean isSuichetingDevice(BluetoothDevice bluetoothdevice)
    {
        if (bluetoothdevice == null || bluetoothdevice.getAddress() == null)
        {
            return false;
        } else
        {
            return bluetoothdevice.getAddress().startsWith("C9:32:");
        }
    }

    public static void showVersionWarning(Activity activity)
    {
        if (activity != null)
        {
            activity.runOnUiThread(new _cls1());
        }
    }

    public void bindDevice(int i, AlbumModel albummodel, Activity activity)
    {
        AlbumBinder.bindDevice(i, albummodel, activity);
    }

    public void cleanup()
    {
        sharePicUrl = "";
        a.at = null;
        a.ap = null;
        a.aq = null;
        a.ar = null;
        a.at = null;
    }

    public DeviceType getBluetoothDeviceType(BluetoothDevice bluetoothdevice)
    {
        if (isSuichetingDevice(bluetoothdevice))
        {
            return DeviceType.suicheting;
        }
        XiMaoBTManager.getInstance(mContext);
        if (XiMaoBTManager.isXimao(bluetoothdevice))
        {
            return DeviceType.ximao;
        }
        if (isQsuicheting(bluetoothdevice))
        {
            return DeviceType.Qsuicheting;
        } else
        {
            return DeviceType.unReg;
        }
    }

    public BluetoothDevice getConnectedDevice(Activity activity)
    {
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            break MISSING_BLOCK_LABEL_190;
        }
        BluetoothDevice abluetoothdevice[];
        try
        {
            IBinder ibinder = (IBinder)Class.forName("android.os.ServiceManager").getDeclaredMethod("getService", new Class[] {
                java/lang/String
            }).invoke(null, new Object[] {
                "bluetooth_a2dp"
            });
            Method method = Class.forName("android.a.a").getDeclaredClasses()[0].getDeclaredMethod("asInterface", new Class[] {
                android/os/IBinder
            });
            method.setAccessible(true);
            abluetoothdevice = (BluetoothDevice[])(BluetoothDevice[])((android.a.a)method.invoke(null, new Object[] {
                ibinder
            })).getClass().getDeclaredMethod("getConnectedSinks", new Class[0]).invoke((android.a.a)method.invoke(null, new Object[] {
                ibinder
            }), new Object[0]);
        }
        catch (Exception exception)
        {
            showVersionWarning(activity);
            exception.printStackTrace();
            Logger.e(TAG, (new StringBuilder()).append("error:").append(exception.toString()).toString());
            return null;
        }
        if (abluetoothdevice == null)
        {
            break MISSING_BLOCK_LABEL_148;
        }
        if (abluetoothdevice.length != 0)
        {
            activity = abluetoothdevice[0];
            return activity;
        }
        return null;
        if (MyApplication.a() == null)
        {
            return null;
        }
        try
        {
            activity = Class.forName("android.bluetooth.BluetoothA2dp").getDeclaredConstructor(new Class[] {
                android/content/Context, android/bluetooth/BluetoothProfile$ServiceListener
            });
            activity.setAccessible(true);
            activity = ((BluetoothA2dp)activity.newInstance(new Object[] {
                MyApplication.a().getApplicationContext(), new _cls2()
            })).getConnectedDevices();
        }
        // Misplaced declaration of an exception variable
        catch (Activity activity)
        {
            activity.printStackTrace();
            return null;
        }
        if (activity == null)
        {
            break MISSING_BLOCK_LABEL_289;
        }
        if (activity.size() == 0)
        {
            break MISSING_BLOCK_LABEL_289;
        }
        activity = (BluetoothDevice)activity.get(0);
        return activity;
        return null;
    }

    public String getDossName()
    {
        return mContext.getResources().getString(0x7f090213);
    }

    public String getDossSubName()
    {
        return mContext.getResources().getString(0x7f090213);
    }

    public String getMarketURL()
    {
        if (!TextUtils.isEmpty(a.at))
        {
            return a.at;
        }
        List list = getProductModels();
        for (int i = 0; i < list.size(); i++)
        {
            ProductModel productmodel = (ProductModel)list.get(i);
            if (productmodel.title.equals("\u4F53\u9A8C\u65B0\u58F0\u6D3B"))
            {
                a.at = productmodel.getUrl();
                return a.at;
            }
        }

        return a.at;
    }

    public DeviceType getNowDeviceType()
    {
        return mNowDeviceType;
    }

    public ProductModel getProductModel(String s)
    {
        if (productModels != null)
        {
            for (int i = 0; i < productModels.size(); i++)
            {
                ProductModel productmodel = (ProductModel)productModels.get(i);
                if (productmodel.title.endsWith(s))
                {
                    return productmodel;
                }
            }

        }
        return null;
    }

    public List getProductModels()
    {
        return productModels;
    }

    public String getQSuichetingIntro()
    {
        if (!TextUtils.isEmpty(a.as))
        {
            return a.as;
        }
        if (productModels != null)
        {
            for (int i = 0; i < productModels.size(); i++)
            {
                ProductModel productmodel = (ProductModel)productModels.get(i);
                if (productmodel.title.endsWith("\u968F\u8F66\u542CQ\u7248\u8BF4\u660E\u4E66"))
                {
                    a.as = productmodel.url;
                    SharedPreferencesUtil.getInstance(mContext).saveString("\u968F\u8F66\u542CQ\u7248\u8BF4\u660E\u4E66", a.as);
                    return a.as;
                }
            }

        } else
        {
            a.as = SharedPreferencesUtil.getInstance(mContext).getString("\u968F\u8F66\u542CQ\u7248\u8BF4\u660E\u4E66");
            return a.as;
        }
        return null;
    }

    public String getShukeWifiName()
    {
        return mContext.getResources().getString(0x7f090216);
    }

    public String getShukeWifiSubName()
    {
        return mContext.getResources().getString(0x7f090217);
    }

    public String getSuichetingIntro()
    {
        if (!TextUtils.isEmpty(a.ap))
        {
            return a.ap;
        }
        if (productModels != null)
        {
            for (int i = 0; i < productModels.size(); i++)
            {
                ProductModel productmodel = (ProductModel)productModels.get(i);
                if (productmodel.title.endsWith("\u968F\u8F66\u542C\u8BF4\u660E\u4E66"))
                {
                    a.ap = productmodel.url;
                    SharedPreferencesUtil.getInstance(mContext).saveString("\u968F\u8F66\u542C\u8BF4\u660E\u4E66", a.ap);
                    return a.ap;
                }
            }

        } else
        {
            a.ap = SharedPreferencesUtil.getInstance(mContext).getString("\u968F\u8F66\u542C\u8BF4\u660E\u4E66");
            return a.ap;
        }
        return null;
    }

    public String getTingshubaoIntro()
    {
        if (!TextUtils.isEmpty(a.ar))
        {
            return a.ar;
        }
        if (productModels != null)
        {
            for (int i = 0; i < productModels.size(); i++)
            {
                ProductModel productmodel = (ProductModel)productModels.get(i);
                if (productmodel.title.endsWith("\u542C\u4E66\u5B9D\u8BF4\u660E\u4E66"))
                {
                    a.ar = productmodel.url;
                    SharedPreferencesUtil.getInstance(mContext).saveString("\u542C\u4E66\u5B9D\u8BF4\u660E\u4E66", a.ar);
                    return a.ar;
                }
            }

        } else
        {
            a.ar = SharedPreferencesUtil.getInstance(mContext).getString("\u542C\u4E66\u5B9D\u8BF4\u660E\u4E66");
            return a.ar;
        }
        return null;
    }

    public String getTingshubaoName()
    {
        return mContext.getResources().getString(0x7f090214);
    }

    public String getTingshubaoSubName()
    {
        return mContext.getResources().getString(0x7f090214);
    }

    public String getXimaoIntro()
    {
        if (!TextUtils.isEmpty(a.aq))
        {
            return a.aq;
        }
        if (productModels != null)
        {
            for (int i = 0; i < productModels.size(); i++)
            {
                ProductModel productmodel = (ProductModel)productModels.get(i);
                if (productmodel.title.endsWith("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A\u8BF4\u660E\u4E66"))
                {
                    a.aq = productmodel.url;
                    SharedPreferencesUtil.getInstance(mContext).saveString("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A\u8BF4\u660E\u4E66", a.aq);
                    return a.aq;
                }
            }

        } else
        {
            a.aq = SharedPreferencesUtil.getInstance(mContext).getString("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A\u8BF4\u660E\u4E66");
            return a.aq;
        }
        return null;
    }

    public boolean isAlbumBind(long l)
    {
        return mNowDeviceType != DeviceType.ximao ? mNowDeviceType == DeviceType.tingshubao && MainTabActivity2.mainTabActivity != null && DeviceUtil.isAlbumBind(mContext, l) : XiMaoBTManager.getInstance(mContext).isModelBinded(l);
    }

    public boolean isConn()
    {
        boolean flag = true;
        static class _cls5
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType = new int[DeviceType.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[DeviceType.ximao.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[DeviceType.tingshubao.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[DeviceType.doss.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[DeviceType.shukewifi.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        switch (_cls5..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[mNowDeviceType.ordinal()])
        {
        default:
            flag = false;
            // fall through

        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
            return flag;

        case 1: // '\001'
            return XiMaoBTManager.getInstance(mContext).isConnected();
        }
    }

    public boolean isDlnaInit()
    {
        if (mIsDlnaCanUse)
        {
            return true;
        }
        if (DexManager.getInstance(mContext).isDlnaInit())
        {
            mIsDlnaCanUse = true;
            return true;
        } else
        {
            return false;
        }
    }

    public boolean isQSuiCheTingFirstUsed()
    {
        return SharedPreferencesUtil.getInstance(mContext).getBoolean("P_QSUICHETING_FIRST_USED", true);
    }

    public boolean isSuiCheTingFirstUsed()
    {
        return SharedPreferencesUtil.getInstance(mContext).getBoolean("P_SUICHETING_FIRST_USED", true);
    }

    public boolean isTingshubaoFirstUsed()
    {
        return SharedPreferencesUtil.getInstance(mContext).getBoolean("P_TINGSHUBAO_FIRST_USED", true);
    }

    public void onJudgeDeviceConn(BluetoothDevice bluetoothdevice)
    {
    }

    public void parseDeviceInfo()
    {
        String s = (new StringBuilder()).append(a.N).append("mobile/mall/products").toString();
        cleanup();
        f.a().a(s, null, null, new _cls4(), false);
    }

    public void setIsQSuiCheTingFirstUsed(boolean flag)
    {
        SharedPreferencesUtil.getInstance(mContext).saveBoolean("P_QSUICHETING_FIRST_USED", flag);
    }

    public void setIsSuiCheTingFirstUsed(boolean flag)
    {
        SharedPreferencesUtil.getInstance(mContext).saveBoolean("P_SUICHETING_FIRST_USED", flag);
    }

    public void setIsTingshubaoFirstUsed(boolean flag)
    {
        SharedPreferencesUtil.getInstance(mContext).saveBoolean("P_TINGSHUBAO_FIRST_USED", flag);
    }

    public void setNowDeviceType(DeviceType devicetype)
    {
        mNowDeviceType = devicetype;
    }

    public void setProductModels(List list)
    {
        productModels = list;
    }

    public void shareSuiCheTing(final int type)
    {
        Logger.d("sucheting", "shareSuiCheTing IN");
        setIsSuiCheTingFirstUsed(false);
        if (MyApplication.a() != null)
        {
            (new DialogBuilder(MyApplication.a())).setMessage("\u5206\u4EAB\u5230\u670B\u53CB\u5708\uFF0C\u5E2E\u670B\u53CB\u89E3\u8131\u5835\u8F66\u70E6\u607C\uFF01").setTitle("\u968F\u8F66\u542C").setCancelBtn("\u4E0D\u5E2E").setOkBtn("\u7ACB\u5373\u5E2E", new _cls3()).showConfirm();
        }
    }

    public void toEachMarket(String s)
    {
        Object obj = null;
        if (productModels == null)
        {
            Toast.makeText(mContext, "\u656C\u8BF7\u671F\u5F85", 0).show();
        }
        int i = productModels.size();
        String s1;
        if (i > 0)
        {
            s = getProductModel(s);
        } else
        {
            s = null;
        }
        s1 = obj;
        if (i > 0)
        {
            s1 = obj;
            if (s != null)
            {
                s1 = s.getUrl();
            }
        }
        if (i > 0 && s != null && !TextUtils.isEmpty(s1))
        {
            s = new Intent(mContext, com/ximalaya/ting/android/activity/web/WebActivityNew);
            s.putExtra("ExtraUrl", s1);
            mContext.startActivity(s);
            return;
        } else
        {
            Toast.makeText(mContext, "\u656C\u8BF7\u671F\u5F85", 0).show();
            return;
        }
    }

    public void toEachMarket(String s, ProductModel productmodel)
    {
        Object obj = null;
        int i = productModels.size();
        getMarketURL();
        if (i > 0)
        {
            productmodel = getProductModel(s);
        }
        s = obj;
        if (i > 0)
        {
            s = obj;
            if (productmodel != null)
            {
                s = productmodel.getUrl();
            }
        }
        if (i > 0 && productmodel != null && !TextUtils.isEmpty(s))
        {
            productmodel = new Intent(mContext, com/ximalaya/ting/android/activity/web/WebActivityNew);
            productmodel.putExtra("ExtraUrl", s);
            mContext.startActivity(productmodel);
            return;
        } else
        {
            Toast.makeText(mContext, "\u656C\u8BF7\u671F\u5F85", 0).show();
            return;
        }
    }




/*
    static List access$102(MyDeviceManager mydevicemanager, List list)
    {
        mydevicemanager.productModels = list;
        return list;
    }

*/


    private class _cls1
        implements Runnable
    {

        public void run()
        {
            (new DialogBuilder(MyApplication.a())).setMessage("\u5F53\u524D\u7684\u7CFB\u7EDF\u7248\u672C\u8FC7\u4F4E\uFF0C\u4E0D\u652F\u6301\u6B64\u529F\u80FD").showWarning();
        }

        _cls1()
        {
        }
    }


    private class _cls2
        implements android.bluetooth.BluetoothProfile.ServiceListener
    {

        final MyDeviceManager this$0;

        public void onServiceConnected(int i, BluetoothProfile bluetoothprofile)
        {
            BluetoothAdapter.getDefaultAdapter().closeProfileProxy(2, bluetoothprofile);
        }

        public void onServiceDisconnected(int i)
        {
        }

        _cls2()
        {
            this$0 = MyDeviceManager.this;
            super();
        }
    }


    private class _cls4 extends com.ximalaya.ting.android.b.a
    {

        final MyDeviceManager this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        protected void onLoadFailed()
        {
            String s = SharedPreferencesUtil.getInstance(mContext).getString("P_TINGSHUBAO_INIT_SET");
            if (TextUtils.isEmpty(s))
            {
                return;
            }
            try
            {
                productModels = JSON.parseArray(s, com/ximalaya/ting/android/fragment/device/ProductModel);
                return;
            }
            catch (Exception exception)
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
            }
        }

        public void onNetError(int i, String s)
        {
            Logger.d(MyDeviceManager.TAG, "parseDeviceInfo 2 onNetError:errorMessage");
            onLoadFailed();
        }

        public void onSuccess(String s)
        {
            Logger.d(MyDeviceManager.TAG, "parseDeviceInfo 2 onSuccess:errorMessage");
            try
            {
                JSONObject jsonobject = JSON.parseObject(s);
                Logger.d(MyDeviceManager.TAG, (new StringBuilder()).append("url_mall_products onSuccess:").append(s).toString());
                if ("0".equals(jsonobject.get("ret").toString()))
                {
                    s = jsonobject.getString("list");
                    productModels = JSON.parseArray(s, com/ximalaya/ting/android/fragment/device/ProductModel);
                    SharedPreferencesUtil.getInstance(mContext).saveString("P_TINGSHUBAO_INIT_SET", s);
                }
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                onLoadFailed();
            }
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
        }

        _cls4()
        {
            this$0 = MyDeviceManager.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final MyDeviceManager this$0;
        final int val$type;

        public void onExecute()
        {
            (new ShareWxTask(MyApplication.a(), type)).execute(new Object[] {
                Integer.valueOf(1)
            });
        }

        _cls3()
        {
            this$0 = MyDeviceManager.this;
            type = i;
            super();
        }
    }

}
