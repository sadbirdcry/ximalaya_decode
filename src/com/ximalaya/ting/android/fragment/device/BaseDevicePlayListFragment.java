// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.model.mediamanager.Item;
import com.ximalaya.model.mediamanager.Result;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.BaseCurrentPlayingModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.PlayManageController;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BasePlayModule;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.teleal.cling.model.types.UDN;

public abstract class BaseDevicePlayListFragment extends BaseListFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.device.dlna.DlnaManager.OnkeyEventListener
{
    protected class LoadingData
    {

        public boolean loadingNextPage;
        public boolean loadingNextPageAuto;
        public int pageId;
        public int pageSize;
        final BaseDevicePlayListFragment this$0;
        public int totalPages;

        public void reSet()
        {
            pageId = 1;
            pageSize = 100;
            loadingNextPage = false;
            totalPages = 0;
        }

        protected LoadingData()
        {
            this$0 = BaseDevicePlayListFragment.this;
            super();
            pageId = 1;
            pageSize = 100;
            loadingNextPage = false;
            totalPages = 0;
        }
    }

    public class WifiControlHandler extends Handler
    {

        final BaseDevicePlayListFragment this$0;

        public void handleMessage(Message message)
        {
            message.what;
            JVM INSTR lookupswitch 7: default 72
        //                       3: 147
        //                       4: 127
        //                       5: 107
        //                       6: 117
        //                       17: 90
        //                       22: 80
        //                       24: 167;
               goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8
_L1:
            notifyAdapter();
            return;
_L7:
            onMediaChangedReceived();
            continue; /* Loop/switch isn't completed */
_L6:
            onSoundChanged((BaseCurrentPlayingModel)message.obj);
            continue; /* Loop/switch isn't completed */
_L4:
            onSoundPreReceived();
            continue; /* Loop/switch isn't completed */
_L5:
            onSoundNextReceived();
            continue; /* Loop/switch isn't completed */
_L3:
            DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying = true;
            continue; /* Loop/switch isn't completed */
_L2:
            DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying = false;
            continue; /* Loop/switch isn't completed */
_L8:
            onStorageMediaChanged((String)message.obj);
            if (true) goto _L1; else goto _L9
_L9:
        }

        public WifiControlHandler()
        {
            this$0 = BaseDevicePlayListFragment.this;
            super();
        }
    }


    static boolean isFirst = false;
    private String TAG;
    protected int currentQueuePage;
    protected boolean isRefreshMedia;
    protected LoadingData loadingData;
    protected MyProgressDialog loadingDialog;
    protected BaseAdapter mAdapter;
    protected RelativeLayout mAllDelete;
    protected RelativeLayout mAllStart;
    protected ImageView mBackImg;
    protected RelativeLayout mController;
    protected RelativeLayout mDeleteAll;
    protected BaseDeviceItem mDeviceItem;
    protected RelativeLayout mHaveData;
    protected RelativeLayout mNoData;
    protected TextView mNoDataText;
    protected BasePlayModule mPlayModule;
    protected int mPositionInCurrentPage;
    protected int mPositionInListView;
    protected TextView mRightBtn;
    protected RelativeLayout mTaskControl;
    protected RelativeLayout mTopBar;
    protected TextView mTopTv;
    WifiControlHandler mWifiControlHandler;
    protected int soundPage;
    protected TimerTask task;
    protected Timer timer;

    public BaseDevicePlayListFragment()
    {
        TAG = com/ximalaya/ting/android/fragment/device/BaseDevicePlayListFragment.getSimpleName();
        mPositionInCurrentPage = -1;
        mPositionInListView = -1;
        soundPage = 0;
        isRefreshMedia = false;
        currentQueuePage = -1;
        loadingData = new LoadingData();
        mWifiControlHandler = new WifiControlHandler();
    }

    protected void deleteAllMedia()
    {
    }

    protected void dismissLoadingDialog()
    {
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
        if (timer != null)
        {
            timer.cancel();
            timer = null;
        }
    }

    protected abstract BaseAdapter getAdapter();

    protected abstract List getItems();

    protected void initData()
    {
        mListView.setAdapter(getAdapter());
        registerListener();
        loadMoreData();
    }

    protected void initListener()
    {
        mListView.setOnItemClickListener(new _cls1());
        mFooterViewLoading.setOnClickListener(new _cls2());
        mBackImg.setOnClickListener(this);
        mDeleteAll.setOnClickListener(this);
        mRightBtn.setOnClickListener(this);
        mAllDelete.setOnClickListener(this);
        mAllStart.setOnClickListener(this);
    }

    protected void initUi()
    {
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        mTopTv = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mTopBar = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a005f);
        mDeleteAll = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c9);
        mTaskControl = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c3);
        mAllDelete = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c7);
        mAllStart = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c5);
        mNoData = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03be);
        mNoDataText = (TextView)fragmentBaseContainerView.findViewById(0x7f0a03c0);
        mHaveData = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c1);
        mController = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c2);
        mRightBtn = (TextView)fragmentBaseContainerView.findViewById(0x7f0a071c);
    }

    protected void loadMoreData()
    {
        Logger.d(TAG, "\u5F00\u59CB\u8F7D\u5165\u6211\u7684\u4E0B\u8F7D\u4E2D\u7684\u6587\u4EF6\u6570");
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
    }

    protected void notifyAdapter()
    {
        if (getActivity() == null)
        {
            return;
        } else
        {
            getActivity().runOnUiThread(new _cls3());
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        loadingData.reSet();
        currentQueuePage = -1;
        mDeviceItem = DlnaManager.getInstance(getActivity().getApplicationContext()).getOperationStroageModel().getNowDeviceItem();
        mPlayModule = (BasePlayModule)DlnaManager.getInstance(getActivity().getApplicationContext()).getController(mDeviceItem.getDlnaType()).getModule(BasePlayModule.NAME);
        initUi();
        initData();
        initListener();
    }

    protected abstract void onBrowseMediaSuccess(Result result);

    public void onClick(View view)
    {
        switch (view.getId())
        {
        case 2131363612: 
        default:
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;

        case 2131362761: 
            deleteAllMedia();
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300e6, viewgroup, false);
        mListView = (ListView)fragmentBaseContainerView.findViewById(0x7f0a03cb);
        return fragmentBaseContainerView;
    }

    protected void onDeleteAllMediaSuccess()
    {
        Logger.d(TAG, "onDeleteAllMediaSuccess IN");
        dismissLoadingDialog();
        getItems().clear();
        notifyAdapter();
    }

    protected void onItemClicked(int i)
    {
        prePlaySound(i);
    }

    public void onKeyAciton(KeyEvent keyevent)
    {
        while (mWifiControlHandler == null || keyevent == null || !keyevent.getDeviceItem().getUdn().equals(mDeviceItem.getUdn())) 
        {
            return;
        }
        Message message = new Message();
        message.what = keyevent.getEventKey();
        message.obj = keyevent.getT();
        mWifiControlHandler.sendMessage(message);
    }

    protected abstract void onMediaChangedReceived();

    protected void onPlaySuccess()
    {
        dismissLoadingDialog();
        DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying = true;
        if (getItems() != null && mPositionInListView > 0 && mPositionInListView < getItems().size())
        {
            DlnaManager.getInstance(mCon).getOperationStroageModel().setCurrentPlaying((Item)getItems().get(mPositionInListView));
        }
        notifyAdapter();
    }

    public void onResume()
    {
        super.onResume();
        hidePlayButton();
    }

    protected abstract void onSoundChanged(BaseCurrentPlayingModel basecurrentplayingmodel);

    protected abstract void onSoundNextReceived();

    protected abstract void onSoundPreReceived();

    public void onStop()
    {
        super.onStop();
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    protected abstract void onStopReceived();

    protected abstract void onStorageMediaChanged(String s);

    protected abstract void playSound(int i);

    protected void prePlaySound(int i)
    {
        playSound(i);
    }

    protected void refreshMediaList()
    {
        isRefreshMedia = true;
    }

    protected void registerListener()
    {
        DlnaManager.getInstance(getActivity().getApplicationContext()).getPlayManageController().tuisongDevice(mDeviceItem);
    }

    public void showDebugToast(String s)
    {
        CustomToast.showToast(mActivity, s, 0);
    }

    protected void showHaveData()
    {
        mNoData.setVisibility(8);
        mController.setVisibility(0);
        mHaveData.setVisibility(0);
    }

    protected void showNoData()
    {
        mNoData.setVisibility(0);
        mController.setVisibility(8);
        mHaveData.setVisibility(8);
    }

    public void showToast(String s)
    {
        CustomToast.showToast(mActivity, s, 0);
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final BaseDevicePlayListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            DlnaManager.getInstance(getActivity().getApplicationContext()).setOnkeyEventListener(BaseDevicePlayListFragment.this);
            onItemClicked(i);
        }

        _cls1()
        {
            this$0 = BaseDevicePlayListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final BaseDevicePlayListFragment this$0;

        public void onClick(View view)
        {
            loadMoreData();
        }

        _cls2()
        {
            this$0 = BaseDevicePlayListFragment.this;
            super();
        }
    }


    private class _cls3
        implements Runnable
    {

        final BaseDevicePlayListFragment this$0;

        public void run()
        {
            mAdapter.notifyDataSetChanged();
        }

        _cls3()
        {
            this$0 = BaseDevicePlayListFragment.this;
            super();
        }
    }

}
