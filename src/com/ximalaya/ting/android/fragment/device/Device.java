// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.bluetooth.BluetoothDevice;
import android.graphics.drawable.Drawable;

public class Device
{
    public static final class TYPE extends Enum
    {

        private static final TYPE $VALUES[];
        public static final TYPE BLUETOOTH;
        public static final TYPE WIFI;

        public static TYPE valueOf(String s)
        {
            return (TYPE)Enum.valueOf(com/ximalaya/ting/android/fragment/device/Device$TYPE, s);
        }

        public static TYPE[] values()
        {
            return (TYPE[])$VALUES.clone();
        }

        static 
        {
            BLUETOOTH = new TYPE("BLUETOOTH", 0);
            WIFI = new TYPE("WIFI", 1);
            $VALUES = (new TYPE[] {
                BLUETOOTH, WIFI
            });
        }

        private TYPE(String s, int i)
        {
            super(s, i);
        }
    }


    private BluetoothDevice device;
    private Drawable deviceImage;
    private String deviceName;
    private TYPE deviceType;

    public Device(String s, TYPE type)
    {
        deviceName = s;
        deviceType = type;
    }

    public Device(String s, TYPE type, BluetoothDevice bluetoothdevice)
    {
        deviceName = s;
        deviceType = type;
        device = bluetoothdevice;
    }

    public Device(String s, TYPE type, Drawable drawable)
    {
        deviceName = s;
        deviceType = type;
        deviceImage = drawable;
    }

    public Device(String s, TYPE type, Drawable drawable, BluetoothDevice bluetoothdevice)
    {
        deviceName = s;
        deviceType = type;
        deviceImage = drawable;
        device = bluetoothdevice;
    }

    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        return device == null || ((Device)obj).device == null || device.getAddress().equals(((Device)obj).device.getAddress());
    }

    public BluetoothDevice getDevice()
    {
        return device;
    }

    public Drawable getDeviceImage()
    {
        return deviceImage;
    }

    public String getDeviceName()
    {
        return deviceName;
    }

    public TYPE getDeviceType()
    {
        return deviceType;
    }

    public void setDevice(BluetoothDevice bluetoothdevice)
    {
        device = bluetoothdevice;
    }

    public void setDeviceImage(Drawable drawable)
    {
        deviceImage = drawable;
    }

    public void setDeviceName(String s)
    {
        deviceName = s;
    }

    public void setDeviceType(TYPE type)
    {
        deviceType = type;
    }
}
