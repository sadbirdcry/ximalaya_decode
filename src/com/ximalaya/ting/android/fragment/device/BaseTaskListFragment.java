// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.model.mediamanager.Task;
import com.ximalaya.model.mediamanager.TaskList;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.common.adapter.CommonTFDownloadTaskAdapter;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDownloadModule;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.List;
import org.teleal.cling.model.types.UDN;

public abstract class BaseTaskListFragment extends BaseListFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.device.dlna.DlnaManager.OnkeyEventListener
{
    protected class LoadingData
    {

        public int pageId;
        public int pageSize;
        final BaseTaskListFragment this$0;

        public void reSet()
        {
            pageId = 0;
            pageSize = 0;
        }

        protected LoadingData()
        {
            this$0 = BaseTaskListFragment.this;
            super();
            pageId = 1;
            pageSize = 20;
        }
    }

    public class WifiControlHandler extends Handler
    {

        final BaseTaskListFragment this$0;

        public void handleMessage(Message message)
        {
            message.what;
            JVM INSTR tableswitch 20 22: default 32
        //                       20 40
        //                       21 40
        //                       22 40;
               goto _L1 _L2 _L2 _L2
_L1:
            notifyAdapter();
            return;
_L2:
            refreshDownloadTask();
            Logger.d("doss", "\u4E0B\u8F7D\u72B6\u6001\u53D8\u5316");
            if (true) goto _L1; else goto _L3
_L3:
        }

        public WifiControlHandler()
        {
            this$0 = BaseTaskListFragment.this;
            super();
        }
    }


    public static List taskItems;
    private String TAG;
    protected LoadingData loadingData;
    protected MyProgressDialog loadingDialog;
    protected BaseAdapter mAdapter;
    protected RelativeLayout mAllDelete;
    protected RelativeLayout mAllStart;
    protected ImageView mBackImg;
    protected RelativeLayout mController;
    protected RelativeLayout mDeleteAll;
    protected BaseDeviceItem mDeviceItem;
    protected BaseDownloadModule mDownloadModule;
    protected RelativeLayout mHaveData;
    protected RelativeLayout mNoData;
    private TextView mNoDataText;
    protected TextView mRightBtn;
    protected RelativeLayout mTaskControl;
    protected RelativeLayout mTopBar;
    WifiControlHandler mWifiControlHandler;

    public BaseTaskListFragment()
    {
        TAG = "doss";
        loadingData = new LoadingData();
        mWifiControlHandler = new WifiControlHandler();
    }

    private void initData()
    {
        taskItems = new ArrayList();
        mAdapter = new CommonTFDownloadTaskAdapter(mCon, taskItems);
        mListView.setAdapter(mAdapter);
        registerListener();
        loadMoreData();
    }

    private void initListener()
    {
        mListView.setOnItemClickListener(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
        mBackImg.setOnClickListener(this);
        mDeleteAll.setOnClickListener(this);
        mRightBtn.setOnClickListener(this);
        mAllDelete.setOnClickListener(this);
        mAllStart.setOnClickListener(this);
    }

    private void initUi()
    {
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        mTopBar = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a005f);
        mDeleteAll = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c9);
        mTaskControl = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c3);
        mAllDelete = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c7);
        mAllStart = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c5);
        mNoData = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03be);
        mNoDataText = (TextView)fragmentBaseContainerView.findViewById(0x7f0a03c0);
        mHaveData = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c1);
        mController = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c2);
        mRightBtn = (TextView)fragmentBaseContainerView.findViewById(0x7f0a071c);
        mTopBar.setVisibility(8);
        mTaskControl.setVisibility(0);
    }

    private void notifyAdapter()
    {
        mAdapter.notifyDataSetChanged();
        if (taskItems != null && taskItems.size() > 0)
        {
            showHaveData();
            mDeleteAll.setVisibility(8);
            mTaskControl.setVisibility(0);
            return;
        } else
        {
            showNoData();
            return;
        }
    }

    private void registerListener()
    {
        (new Thread(new _cls5())).start();
    }

    private void showHaveData()
    {
        int i;
        if (taskItems == null || taskItems.size() <= 0)
        {
            break MISSING_BLOCK_LABEL_119;
        }
        i = 0;
_L3:
        Object obj;
        if (i >= taskItems.size())
        {
            break MISSING_BLOCK_LABEL_119;
        }
        obj = (Task)taskItems.get(i);
        if (((Task) (obj)).status != 0 && ((Task) (obj)).status != 1 && ((Task) (obj)).status != 4) goto _L2; else goto _L1
_L1:
        obj = "\u5168\u90E8\u6682\u505C";
_L4:
        ((TextView)mAllStart.findViewById(0x7f0a03c6)).setText(((CharSequence) (obj)));
        mNoData.setVisibility(8);
        mController.setVisibility(0);
        mHaveData.setVisibility(0);
        return;
_L2:
        i++;
          goto _L3
        obj = "\u5168\u90E8\u7EE7\u7EED";
          goto _L4
    }

    private void showNoData()
    {
        mNoDataText.setText("\u4EB2\uFF0C\u4F60\u6682\u65F6\u6CA1\u6709\u4E0B\u8F7D\u4EFB\u52A1\u54E6~");
        mNoData.setVisibility(0);
        mController.setVisibility(8);
        mHaveData.setVisibility(8);
    }

    protected void clickTask(int i)
    {
        if (taskItems == null) goto _L2; else goto _L1
_L1:
        ((Task)taskItems.get(i)).status;
        JVM INSTR tableswitch -1 4: default 60
    //                   -1 61
    //                   0 67
    //                   1 67
    //                   2 61
    //                   3 60
    //                   4 61;
           goto _L2 _L3 _L4 _L4 _L3 _L2 _L3
_L2:
        return;
_L3:
        resumeTask(i);
        return;
_L4:
        pauseTask(i);
        return;
    }

    protected void deleteAllTask()
    {
    }

    protected void loadMoreData()
    {
        Logger.d("doss", "\u5F00\u59CB\u8F7D\u5165\u4E0B\u8F7D\u961F\u5217\u6570\u636E");
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        loadingData.reSet();
        mDeviceItem = DlnaManager.getInstance(getActivity().getApplicationContext()).getOperationStroageModel().getNowDeviceItem();
        mDownloadModule = (BaseDownloadModule)DlnaManager.getInstance(getActivity().getApplicationContext()).getController().getModule(BaseDownloadModule.NAME);
        initUi();
        initData();
        initListener();
    }

    protected void onBrowseDownloadTaskSuccess(final TaskList taskList)
    {
        getActivity().runOnUiThread(new _cls2());
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        case 2131363612: 
        default:
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;

        case 2131362759: 
            deleteAllTask();
            return;

        case 2131362757: 
            switchAllTaskStatus();
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300e6, viewgroup, false);
        mListView = (ListView)fragmentBaseContainerView.findViewById(0x7f0a03cb);
        return fragmentBaseContainerView;
    }

    public void onKeyAciton(KeyEvent keyevent)
    {
        while (mWifiControlHandler == null || !keyevent.getDeviceItem().getUdn().equals(mDeviceItem.getUdn())) 
        {
            return;
        }
        Message message = new Message();
        message.what = keyevent.getEventKey();
        message.obj = keyevent.getT();
        mWifiControlHandler.sendMessage(message);
    }

    public void onResume()
    {
        super.onResume();
        hidePlayButton();
        DlnaManager.getInstance(getActivity().getApplicationContext()).setOnkeyEventListener(this);
    }

    public void onStop()
    {
        super.onStop();
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
        DlnaManager.getInstance(getActivity().getApplicationContext()).removeOnkeyEventListener(this);
    }

    protected void pauseAllTask()
    {
    }

    protected abstract void pauseTask(int i);

    public abstract void refreshDownloadTask();

    protected abstract void resumeTask(int i);

    public void showDebugToast(String s)
    {
        CustomToast.showToast(mActivity, s, 0);
    }

    public void showToast(String s)
    {
        CustomToast.showToast(mActivity, s, 0);
    }

    protected void startAllTask()
    {
    }

    protected void switchAllTaskStatus()
    {
        (new Thread(new _cls1())).start();
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final BaseTaskListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            clickTask(i);
        }

        _cls3()
        {
            this$0 = BaseTaskListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final BaseTaskListFragment this$0;

        public void onClick(View view)
        {
            loadMoreData();
        }

        _cls4()
        {
            this$0 = BaseTaskListFragment.this;
            super();
        }
    }


    private class _cls5
        implements Runnable
    {

        final BaseTaskListFragment this$0;

        public void run()
        {
            DlnaManager.getInstance(getActivity().getApplicationContext()).getPlayManageController().tuisongDevice(mDeviceItem);
        }

        _cls5()
        {
            this$0 = BaseTaskListFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final BaseTaskListFragment this$0;
        final TaskList val$taskList;

        public void run()
        {
label0:
            {
                if (taskList != null)
                {
                    if (BaseTaskListFragment.taskItems == null)
                    {
                        break label0;
                    }
                    BaseTaskListFragment.taskItems.clear();
                    BaseTaskListFragment.taskItems.addAll(taskList.tasks);
                }
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                notifyAdapter();
            }
        }

        _cls2()
        {
            this$0 = BaseTaskListFragment.this;
            taskList = tasklist;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final BaseTaskListFragment this$0;

        public void run()
        {
            String s = ((TextView)mAllStart.findViewById(0x7f0a03c6)).getText().toString();
            if (s.equals("\u5168\u90E8\u7EE7\u7EED"))
            {
                startAllTask();
            } else
            if (s.equals("\u5168\u90E8\u6682\u505C"))
            {
                pauseAllTask();
                return;
            }
        }

        _cls1()
        {
            this$0 = BaseTaskListFragment.this;
            super();
        }
    }

}
