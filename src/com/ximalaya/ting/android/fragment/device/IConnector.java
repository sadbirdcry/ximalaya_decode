// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;


// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            ControlConfigure

public interface IConnector
{

    public abstract void cancelConnect();

    public abstract void connect(String s);

    public abstract ControlConfigure getControlConfigure();

    public abstract String getDeviceName();

    public abstract String getPassword();

    public abstract boolean isNeedPw();
}
