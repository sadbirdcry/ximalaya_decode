// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            MyDeviceManager, ProductModel

class this._cls0 extends a
{

    final MyDeviceManager this$0;

    public void onBindXDCS(Header aheader[])
    {
    }

    protected void onLoadFailed()
    {
        String s = SharedPreferencesUtil.getInstance(MyDeviceManager.access$200(MyDeviceManager.this)).getString("P_TINGSHUBAO_INIT_SET");
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        try
        {
            MyDeviceManager.access$102(MyDeviceManager.this, JSON.parseArray(s, com/ximalaya/ting/android/fragment/device/ProductModel));
            return;
        }
        catch (Exception exception)
        {
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
        }
    }

    public void onNetError(int i, String s)
    {
        Logger.d(MyDeviceManager.access$000(), "parseDeviceInfo 2 onNetError:errorMessage");
        onLoadFailed();
    }

    public void onSuccess(String s)
    {
        Logger.d(MyDeviceManager.access$000(), "parseDeviceInfo 2 onSuccess:errorMessage");
        try
        {
            JSONObject jsonobject = JSON.parseObject(s);
            Logger.d(MyDeviceManager.access$000(), (new StringBuilder()).append("url_mall_products onSuccess:").append(s).toString());
            if ("0".equals(jsonobject.get("ret").toString()))
            {
                s = jsonobject.getString("list");
                MyDeviceManager.access$102(MyDeviceManager.this, JSON.parseArray(s, com/ximalaya/ting/android/fragment/device/ProductModel));
                SharedPreferencesUtil.getInstance(MyDeviceManager.access$200(MyDeviceManager.this)).saveString("P_TINGSHUBAO_INIT_SET", s);
            }
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            onLoadFailed();
        }
        Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
    }

    ()
    {
        this$0 = MyDeviceManager.this;
        super();
    }
}
