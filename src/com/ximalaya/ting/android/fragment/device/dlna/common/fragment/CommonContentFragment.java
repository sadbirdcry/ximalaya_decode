// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.DeviceBindingListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceUtil;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.doss.DossSettingFragment;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonTFMainFragment

public abstract class CommonContentFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    public class MyPagerAdapter extends FragmentPagerAdapter
    {

        private final String TITLES[] = {
            "\u7ED1\u5B9A\u7684\u4E13\u8F91", "\u5185\u7F6E\u7684\u58F0\u97F3"
        };
        final CommonContentFragment this$0;

        public int getCount()
        {
            return TITLES.length;
        }

        public Fragment getItem(int i)
        {
            if (i == 1)
            {
                if (mMainFragment == null)
                {
                    mMainFragment = getMainFragment();
                }
                return mMainFragment;
            }
            if (i == 0)
            {
                if (mBindFragment == null)
                {
                    mBindFragment = new DeviceBindingListFragment();
                }
                return mBindFragment;
            } else
            {
                return null;
            }
        }

        public CharSequence getPageTitle(int i)
        {
            return TITLES[i];
        }

        public MyPagerAdapter(FragmentManager fragmentmanager)
        {
            this$0 = CommonContentFragment.this;
            super(fragmentmanager);
        }
    }


    private static final String TAG = com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonContentFragment.getSimpleName();
    private boolean isFirst;
    private boolean isSearchFinish;
    private ImageView mBackImg;
    protected DeviceBindingListFragment mBindFragment;
    private TextView mBtDisConn;
    private CommonTFMainFragment mMainFragment;
    private BaseDeviceItem mMyDeviceItem;
    private com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType mNowDeviceType;
    private TextView mTopTv;
    private PagerSlidingTabStrip tabs;

    public CommonContentFragment()
    {
        isSearchFinish = true;
        isFirst = true;
    }

    private void initView()
    {
        mBtDisConn = (TextView)fragmentBaseContainerView.findViewById(0x7f0a071c);
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        mTopTv = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mBtDisConn.setText("\u8BBE\u7F6E");
        mBtDisConn.setVisibility(0);
        mTopTv.setText(DeviceUtil.getDeviceItemName(mMyDeviceItem));
        mBtDisConn.setOnClickListener(this);
        mBackImg.setOnClickListener(this);
        tabs = (PagerSlidingTabStrip)fragmentBaseContainerView.findViewById(0x7f0a02dc);
        Object obj = (ViewPager)fragmentBaseContainerView.findViewById(0x7f0a017a);
        ((ViewPager) (obj)).setAdapter(new MyPagerAdapter(getChildFragmentManager()));
        ((ViewPager) (obj)).setPageMargin(0);
        tabs.setViewPager(((ViewPager) (obj)));
        obj = new _cls1();
        tabs.setOnPageChangeListener(((android.support.v4.view.ViewPager.OnPageChangeListener) (obj)));
    }

    protected abstract CommonTFMainFragment getMainFragment();

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mMyDeviceItem = DlnaManager.getInstance(getActivity().getApplicationContext()).getOperationStroageModel().getNowDeviceItem();
        mNowDeviceType = mMyDeviceItem.getDlnaType();
        initView();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;

        case 2131363612: 
            startFragment(com/ximalaya/ting/android/fragment/device/doss/DossSettingFragment, null);
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300e2, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onDestroy()
    {
        tabs.setOnPageChangeListener(null);
        super.onDestroy();
    }

    public void onResume()
    {
        super.onResume();
        Logger.d(TAG, "XiMaoContentFragment onResume");
        if (mBindFragment != null)
        {
            mBindFragment.onResume();
        }
        if (mMainFragment != null)
        {
            mMainFragment.onResume();
        }
    }

    public void onStop()
    {
        super.onStop();
    }




/*
    static CommonTFMainFragment access$002(CommonContentFragment commoncontentfragment, CommonTFMainFragment commontfmainfragment)
    {
        commoncontentfragment.mMainFragment = commontfmainfragment;
        return commontfmainfragment;
    }

*/

    private class _cls1
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final CommonContentFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f, int j)
        {
        }

        public void onPageSelected(int i)
        {
        }

        _cls1()
        {
            this$0 = CommonContentFragment.this;
            super();
        }
    }

}
