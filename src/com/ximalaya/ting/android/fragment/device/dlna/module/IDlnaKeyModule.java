// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.module;

import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import org.teleal.cling.model.gena.GENASubscription;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.module:
//            IBaseModule

public interface IDlnaKeyModule
    extends IBaseModule
{

    public abstract KeyEvent parse2Event(GENASubscription genasubscription, BaseDeviceItem basedeviceitem);
}
