// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.model;

import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.UDN;

public class LinkedDeviceModel
{
    public static interface PlayState
    {

        public static final int Pause = 0;
        public static final int Play = 1;
    }


    BaseDeviceItem mNowDeviceItem;
    int mNowPlayState;
    int mNowVolume;

    public LinkedDeviceModel()
    {
    }

    public Service getAvService()
    {
        if (mNowDeviceItem != null)
        {
            return mNowDeviceItem.getAVservice();
        } else
        {
            return null;
        }
    }

    public BaseDeviceItem getNowDeviceItem()
    {
        return mNowDeviceItem;
    }

    public int getNowPlayState()
    {
        return mNowPlayState;
    }

    public int getNowVolume()
    {
        return mNowVolume;
    }

    public Service getRcService()
    {
        if (mNowDeviceItem != null)
        {
            return mNowDeviceItem.getRCservice();
        } else
        {
            return null;
        }
    }

    public String getUDNString()
    {
        return mNowDeviceItem.getUdn().toString();
    }

    public boolean isValid()
    {
        return mNowDeviceItem != null && getAvService() != null;
    }

    public void setNowDeviceItem(BaseDeviceItem basedeviceitem)
    {
        mNowDeviceItem = basedeviceitem;
    }

    public void setNowPlayState(int i)
    {
        mNowPlayState = i;
    }

    public void setNowVolume(int i)
    {
        mNowVolume = i;
    }
}
