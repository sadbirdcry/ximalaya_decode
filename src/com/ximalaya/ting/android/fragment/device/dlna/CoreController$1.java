// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import org.teleal.cling.registry.Registry;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            CoreController

class this._cls0
    implements ServiceConnection
{

    final CoreController this$0;

    public void onServiceConnected(ComponentName componentname, IBinder ibinder)
    {
        CoreController.access$102(CoreController.this, (com.ximalaya.service.npServiceBinder)ibinder);
        CoreController.access$202(CoreController.this, true);
        if (coreUpnpServiceListener != null)
        {
            CoreController.access$100(CoreController.this).setProcessor(coreUpnpServiceListener);
        }
        Log.v("WIFI", "Connected to UPnP Service");
        if (CoreController.access$100(CoreController.this).isSuccessInitialized())
        {
            CoreController.access$100(CoreController.this).getRegistry().addListener(CoreController.access$300(CoreController.this));
            CoreController.access$100(CoreController.this).getRegistry().removeAllRemoteDevices();
            CoreController.access$100(CoreController.this).getRegistry().removeAllLocalDevices();
            startServer();
            searchDevice();
        }
    }

    public void onServiceDisconnected(ComponentName componentname)
    {
        CoreController.access$102(CoreController.this, null);
    }

    ()
    {
        this$0 = CoreController.this;
        super();
    }
}
