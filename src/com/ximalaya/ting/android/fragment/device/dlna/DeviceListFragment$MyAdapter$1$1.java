// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.support.v4.app.FragmentActivity;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            DeviceListFragment, DlnaManager, IDlnaController

class this._cls2
    implements Runnable
{

    final _cls3 this$2;

    public void run()
    {
        class _cls1 extends TimerTask
        {

            final DeviceListFragment.MyAdapter._cls1._cls1 this$3;

            public void run()
            {
                CustomToast.showToast(getActivity(), "\u8BBE\u5907\u5F02\u5E38,\u8BF7\u91CD\u8BD5", 0);
                if (DeviceListFragment.access$400(this$0) != null)
                {
                    DeviceListFragment.access$400(this$0).dismiss();
                    DeviceListFragment.access$402(this$0, null);
                }
            }

            _cls1()
            {
                this$3 = DeviceListFragment.MyAdapter._cls1._cls1.this;
                super();
            }
        }

        DeviceListFragment.access$302(_fld0, new _cls1());
        if (DeviceListFragment.access$500(_fld0) != null)
        {
            DeviceListFragment.access$500(_fld0).cancel();
        }
        DeviceListFragment.access$502(_fld0, new Timer());
        DeviceListFragment.access$500(_fld0).schedule(DeviceListFragment.access$300(_fld0), 15000L);
        class _cls2
            implements Runnable
        {

            final DeviceListFragment.MyAdapter._cls1._cls1 this$3;

            public void run()
            {
                DeviceListFragment.access$402(this$0, new MyProgressDialog(getActivity()));
                DeviceListFragment.access$400(this$0).setTitle("\u63D0\u793A");
                DeviceListFragment.access$400(this$0).setMessage("\u6B63\u5728\u52AA\u529B\u52A0\u8F7D\u8BBE\u5907");
                DeviceListFragment.access$400(this$0).show();
            }

            _cls2()
            {
                this$3 = DeviceListFragment.MyAdapter._cls1._cls1.this;
                super();
            }
        }

        getActivity().runOnUiThread(new _cls2());
        DeviceListFragment.access$602(_fld0, position);
        class _cls3
            implements IActionCallBack
        {

            final DeviceListFragment.MyAdapter._cls1._cls1 this$3;

            public void onFailed()
            {
                CustomToast.showToast(MyApplication.b(), "\u8BBE\u5907\u5F02\u5E38", 0);
                if (DeviceListFragment.access$400(this$0) != null)
                {
                    DeviceListFragment.access$400(this$0).dismiss();
                    DeviceListFragment.access$402(this$0, null);
                }
                if (DeviceListFragment.access$500(this$0) != null)
                {
                    DeviceListFragment.access$500(this$0).cancel();
                }
                class _cls1
                    implements Runnable
                {

                    final _cls3 this$4;

                    public void run()
                    {
                        getCurrentTypeDevices();
                        DeviceListFragment.access$900(this$0).notifyDataSetChanged();
                    }

                        _cls1()
                        {
                            this$4 = _cls3.this;
                            super();
                        }
                }

                if (getActivity() != null)
                {
                    getActivity().runOnUiThread(new _cls1());
                }
            }

            public void onSuccess(ActionModel actionmodel)
            {
                DlnaManager.getInstance(DeviceListFragment.access$700(this$0)).setNowOperationDevice(DeviceListFragment.access$200(this$0));
                DeviceListFragment.access$800(this$0);
            }

            _cls3()
            {
                this$3 = DeviceListFragment.MyAdapter._cls1._cls1.this;
                super();
            }
        }

        DlnaManager.getInstance(getActivity().getApplicationContext()).getController(DeviceListFragment.access$1000(_fld0)).initDeviceInfo(myDeviceItem, new _cls3());
    }

    _cls3()
    {
        this$2 = this._cls2.this;
        super();
    }
}
