// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import com.ximalaya.ting.android.fragment.device.dlna.BaseCurrentPlayingModel;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.common.adapter.CommonSoundsAlbumAdapter;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.model.sound.AlbumSoundModelNew;
import com.ximalaya.ting.android.util.Logger;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonSoundListFragment

public class this._cls0 extends Handler
{

    final CommonSoundListFragment this$0;

    public void handleMessage(Message message)
    {
        Logger.d("doss", (new StringBuilder()).append("WifiControlHandler:handleMessage:msg:").append(message.what).append(",").append(message.arg1).toString());
        message.what;
        JVM INSTR lookupswitch 4: default 84
    //                   3: 146
    //                   4: 103
    //                   17: 95
    //                   24: 189;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        CommonSoundListFragment.access$200(CommonSoundListFragment.this).notifyDataSetChanged();
        return;
_L4:
        onSoundChanged(message);
        continue; /* Loop/switch isn't completed */
_L3:
        if (DlnaManager.getInstance(mCon).getOperationStroageModel() != null)
        {
            Logger.d("doss", "DossSoundListFragment \u6536\u5230Playing\u4E8B\u4EF6");
            DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying = true;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        if (DlnaManager.getInstance(mCon).getOperationStroageModel() != null)
        {
            Logger.d("doss", "DossSoundListFragment \u6536\u5230pause\u4E8B\u4EF6");
            DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying = false;
        }
        continue; /* Loop/switch isn't completed */
_L5:
        message = (String)message.obj;
        Logger.d("tuisong", (new StringBuilder()).append("ARG_STORAGEMEDIA_CHANGE:").append(message).toString());
        if (!message.equals("SONGLIST-NETWORK"))
        {
            DlnaManager.getInstance(getActivity().getApplicationContext()).removeOnkeyEventListener(CommonSoundListFragment.this);
            DlnaManager.getInstance(mCon).getOperationStroageModel().clearCurrentPlaying();
            Logger.d("doss", "\u8DF3\u51FA\u5F53\u524Dqueue");
            CommonSoundListFragment.access$002(CommonSoundListFragment.this, -1);
            CommonSoundListFragment.access$102(CommonSoundListFragment.this, -1);
        }
        if (true) goto _L1; else goto _L6
_L6:
    }

    protected void onSoundChanged(Message message)
    {
        Logger.d("doss", "DossSoundListFragment \u6536\u5230transitioning\u4E8B\u4EF6");
        message = (BaseCurrentPlayingModel)message.obj;
        if (!TextUtils.isEmpty(message.getCurrentPlayList()) && !message.getCurrentPlayList().equals(getPlayListName()))
        {
            DlnaManager.getInstance(getActivity().getApplicationContext()).removeOnkeyEventListener(CommonSoundListFragment.this);
            DlnaManager.getInstance(mCon).getOperationStroageModel().clearCurrentPlaying();
            Logger.d("doss", "\u8DF3\u51FA\u5F53\u524Dqueue");
            CommonSoundListFragment.access$002(CommonSoundListFragment.this, -1);
            CommonSoundListFragment.access$102(CommonSoundListFragment.this, -1);
        } else
        {
            Logger.d("doss", (new StringBuilder()).append("pre mPosition:").append(CommonSoundListFragment.access$100(CommonSoundListFragment.this)).toString());
            Logger.d("doss", (new StringBuilder()).append("currentReceivedTrack:").append(message.getCurrentTrackNum()).toString());
            Logger.d("doss", (new StringBuilder()).append("currentReceivedPage:").append(message.getCurrentPage()).toString());
            Logger.d("doss", (new StringBuilder()).append("pageId:").append(CommonSoundListFragment.access$300(CommonSoundListFragment.this)._fld0 - 1).toString());
            CommonSoundListFragment.access$002(CommonSoundListFragment.this, message.getCurrentPage());
            CommonSoundListFragment.access$102(CommonSoundListFragment.this, message.getCurrentTrackNum() - 1);
            CommonSoundListFragment.access$102(CommonSoundListFragment.this, CommonSoundListFragment.access$300(CommonSoundListFragment.this).e * (CommonSoundListFragment.access$000(CommonSoundListFragment.this) - 1) + CommonSoundListFragment.access$100(CommonSoundListFragment.this));
            if (message.getCurrentPage() > CommonSoundListFragment.access$300(CommonSoundListFragment.this)._fld0 - 1)
            {
                Logger.d("doss", (new StringBuilder()).append("currentPage:").append(CommonSoundListFragment.access$000(CommonSoundListFragment.this)).toString());
                Logger.d("doss", (new StringBuilder()).append("pageId:").append(CommonSoundListFragment.access$300(CommonSoundListFragment.this)._fld0).toString());
                CommonSoundListFragment.access$300(CommonSoundListFragment.this).NextPageAuto = true;
                CommonSoundListFragment.access$400(CommonSoundListFragment.this, mFooterViewLoading);
                DlnaManager.getInstance(mCon).getOperationStroageModel().clearCurrentPlaying();
                return;
            }
            Logger.d("doss", (new StringBuilder()).append("post mPosition:").append(CommonSoundListFragment.access$100(CommonSoundListFragment.this)).toString());
            if (CommonSoundListFragment.access$100(CommonSoundListFragment.this) >= 0 && CommonSoundListFragment.access$100(CommonSoundListFragment.this) < CommonSoundListFragment.access$500(CommonSoundListFragment.this).size())
            {
                DlnaManager.getInstance(mCon).getOperationStroageModel().setCurrentPlaying((AlbumSoundModelNew)CommonSoundListFragment.access$500(CommonSoundListFragment.this).get(CommonSoundListFragment.access$100(CommonSoundListFragment.this)));
                return;
            }
        }
    }

    public ()
    {
        this$0 = CommonSoundListFragment.this;
        super();
    }
}
