// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseManageTFModule;
import com.ximalaya.ting.android.fragment.device.shu.CategoryFragment;
import com.ximalaya.ting.android.util.Logger;
import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonListFinishedDownloadFragment, CommonListUnFinishedTaskFragment

public class CommonTFListSubMainFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    public class MyPagerAdapter extends FragmentPagerAdapter
    {

        private final String TITLES[] = {
            "\u5DF2\u4E0B\u8F7D", "\u6B63\u5728\u4E0B\u8F7D"
        };
        final CommonTFListSubMainFragment this$0;

        public int getCount()
        {
            return TITLES.length;
        }

        public Fragment getItem(int i)
        {
            if (i == 0)
            {
                if (mFinishedDownloadFragment == null)
                {
                    mFinishedDownloadFragment = getFinishedDownloadFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("NUM", deviceNum);
                    bundle.putString("directionName", "\u5DF2\u4E0B\u8F7D");
                    mFinishedDownloadFragment.setArguments(bundle);
                }
                return mFinishedDownloadFragment;
            }
            if (i == 1)
            {
                if (mUnFinishedDownloadFragment == null)
                {
                    mUnFinishedDownloadFragment = new CommonListUnFinishedTaskFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putInt("NUM", deviceNum);
                    bundle1.putString("directionName", "\u6B63\u5728\u4E0B\u8F7D");
                    mUnFinishedDownloadFragment.setArguments(bundle1);
                }
                return mUnFinishedDownloadFragment;
            }
            if (mFinishedDownloadFragment == null)
            {
                mFinishedDownloadFragment = getFinishedDownloadFragment();
                Bundle bundle2 = new Bundle();
                bundle2.putInt("NUM", deviceNum);
                bundle2.putString("directionName", "\u5DF2\u4E0B\u8F7D");
                mFinishedDownloadFragment.setArguments(bundle2);
            }
            return mFinishedDownloadFragment;
        }

        public CharSequence getPageTitle(int i)
        {
            return TITLES[i];
        }

        public MyPagerAdapter(FragmentManager fragmentmanager)
        {
            this$0 = CommonTFListSubMainFragment.this;
            super(fragmentmanager);
        }
    }


    private static final String TAG = "CommonTFListSubMainFragment";
    public static boolean showDownloading = false;
    private String currentDirection;
    private int deviceNum;
    private boolean isFirst;
    private boolean isSearchFinish;
    private ImageView mBackImg;
    private TextView mBtDisConn;
    protected BaseDeviceItem mDeviceItem;
    private CommonListFinishedDownloadFragment mFinishedDownloadFragment;
    protected BaseManageTFModule mManageTFModule;
    private com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType mNowDeviceType;
    private TimerTask mTask;
    private Timer mTimer;
    private TextView mTopTv;
    private CommonListUnFinishedTaskFragment mUnFinishedDownloadFragment;
    private ViewPager pager;
    private PagerSlidingTabStrip tabs;

    public CommonTFListSubMainFragment()
    {
        isSearchFinish = true;
        isFirst = true;
    }

    private void initView()
    {
        mBtDisConn = (TextView)fragmentBaseContainerView.findViewById(0x7f0a071c);
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        mTopTv = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mBtDisConn.setText("\u4E0B\u8F7D\u66F4\u591A");
        mBtDisConn.setVisibility(0);
        mTopTv.setText(currentDirection);
        mBtDisConn.setOnClickListener(this);
        mBackImg.setOnClickListener(this);
        tabs = (PagerSlidingTabStrip)fragmentBaseContainerView.findViewById(0x7f0a02dc);
        pager = (ViewPager)fragmentBaseContainerView.findViewById(0x7f0a017a);
        Object obj = new MyPagerAdapter(getChildFragmentManager());
        pager.setAdapter(((android.support.v4.view.PagerAdapter) (obj)));
        pager.setPageMargin(0);
        tabs.setViewPager(pager);
        obj = new _cls1();
        tabs.setOnPageChangeListener(((android.support.v4.view.ViewPager.OnPageChangeListener) (obj)));
    }

    protected CommonListFinishedDownloadFragment getFinishedDownloadFragment()
    {
        return new CommonListFinishedDownloadFragment();
    }

    protected void initToCategoryFragmentBundle(Bundle bundle)
    {
        bundle.putInt("from", 8);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        if (getArguments() != null && getArguments().containsKey("directionName"))
        {
            currentDirection = getArguments().getString("directionName");
        }
        mDeviceItem = DlnaManager.getInstance(getActivity().getApplicationContext()).getOperationStroageModel().getNowDeviceItem();
        mNowDeviceType = mDeviceItem.getDlnaType();
        mManageTFModule = (BaseManageTFModule)DlnaManager.getInstance(mCon).getController().getModule(BaseManageTFModule.NAME);
        initView();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;

        case 2131363612: 
            toCategoryFragment();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300e2, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onDestroy()
    {
        tabs.setOnPageChangeListener(null);
        super.onDestroy();
    }

    public void onResume()
    {
        super.onResume();
        Logger.d("CommonTFListSubMainFragment", "XiMaoTFListDownloadFragment onResume");
        hidePlayButton();
        if (mFinishedDownloadFragment != null)
        {
            mFinishedDownloadFragment.onResume();
        }
        if (mUnFinishedDownloadFragment != null)
        {
            mUnFinishedDownloadFragment.onResume();
        }
        if (showDownloading)
        {
            pager.setCurrentItem(1);
            showDownloading = false;
        }
    }

    public void onStop()
    {
        if (mTimer != null)
        {
            mTimer.cancel();
            mTimer = null;
        }
        super.onStop();
    }

    protected void toCategoryFragment()
    {
        (new Thread(new _cls2())).start();
        Bundle bundle = new Bundle();
        bundle.putBoolean("showFocus", false);
        initToCategoryFragmentBundle(bundle);
        startFragment(com/ximalaya/ting/android/fragment/device/shu/CategoryFragment, bundle);
    }




/*
    static Timer access$002(CommonTFListSubMainFragment commontflistsubmainfragment, Timer timer)
    {
        commontflistsubmainfragment.mTimer = timer;
        return timer;
    }

*/



/*
    static CommonListFinishedDownloadFragment access$102(CommonTFListSubMainFragment commontflistsubmainfragment, CommonListFinishedDownloadFragment commonlistfinisheddownloadfragment)
    {
        commontflistsubmainfragment.mFinishedDownloadFragment = commonlistfinisheddownloadfragment;
        return commonlistfinisheddownloadfragment;
    }

*/



/*
    static TimerTask access$202(CommonTFListSubMainFragment commontflistsubmainfragment, TimerTask timertask)
    {
        commontflistsubmainfragment.mTask = timertask;
        return timertask;
    }

*/



/*
    static CommonListUnFinishedTaskFragment access$302(CommonTFListSubMainFragment commontflistsubmainfragment, CommonListUnFinishedTaskFragment commonlistunfinishedtaskfragment)
    {
        commontflistsubmainfragment.mUnFinishedDownloadFragment = commonlistunfinishedtaskfragment;
        return commonlistunfinishedtaskfragment;
    }

*/


    private class _cls1
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final CommonTFListSubMainFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f, int j)
        {
        }

        public void onPageSelected(int i)
        {
            if (i == 0)
            {
                if (mTimer != null)
                {
                    mTimer.cancel();
                    mTimer = null;
                }
                mFinishedDownloadFragment.refreshMediaList();
            } else
            if (i == 1)
            {
                class _cls1 extends TimerTask
                {

                    final _cls1 this$1;

                    public void run()
                    {
                        mUnFinishedDownloadFragment.refreshDownloadTask();
                    }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
                }

                mTask = new _cls1();
                mTimer = new Timer();
                mTimer.schedule(mTask, 1000L, 1000L);
                return;
            }
        }

        _cls1()
        {
            this$0 = CommonTFListSubMainFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final CommonTFListSubMainFragment this$0;

        public void run()
        {
            int i = 0;
            while (((CommonDeviceItem)mDeviceItem).getStorage() == null && i < 3) 
            {
                class _cls1 extends GetMediaDiskInfoActionCallback
                {

                    final _cls2 this$1;

                    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
                    {
                        Logger.d("doss", "getMediaDiskInfo failure");
                    }

                    public void received(DiskInfoResult diskinforesult)
                    {
                        Logger.d("doss", "getMediaDiskInfo SUCCESS");
                        ((CommonDeviceItem)mDeviceItem).setStorage(diskinforesult);
                    }

                _cls1(Service service)
                {
                    this$1 = _cls2.this;
                    super(service);
                }
                }

                ActionModel actionmodel;
                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
                actionmodel = ActionModel.createModel(new _cls1(((CommonDeviceItem)mDeviceItem).getMMservice()));
                mManageTFModule.getMediaDiskInfo(actionmodel);
                i++;
            }
        }

        _cls2()
        {
            this$0 = CommonTFListSubMainFragment.this;
            super();
        }
    }

}
