// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.module:
//            BaseDlnaModule

public abstract class BaseDownloadModule extends BaseDlnaModule
{

    public static final String NAME = com/ximalaya/ting/android/fragment/device/dlna/module/BaseDownloadModule.getSimpleName();

    public BaseDownloadModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public abstract void browseDownloadTask(ActionModel actionmodel);

    public abstract void createDownloadTask(ActionModel actionmodel);

    public abstract void deleteDownloadTask(ActionModel actionmodel);

    public String getModuleName()
    {
        return NAME;
    }

    public abstract void pauseDownloadTask(ActionModel actionmodel);

    public abstract void resumeDownloadTask(ActionModel actionmodel);

}
