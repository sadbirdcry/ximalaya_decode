// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseManageTFModule;
import com.ximalaya.ting.android.library.util.Logger;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.controlpoint.ControlPoint;

public class CommonManageTFModule extends BaseManageTFModule
{

    private final String TAG = com/ximalaya/ting/android/fragment/device/dlna/common/module/CommonManageTFModule.getSimpleName();

    public CommonManageTFModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public void browseMedia(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "browseMedia IN");
        getControlPoint().execute(actionmodel);
    }

    public void deleteMedia(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "deleteMedia IN");
        getControlPoint().execute(actionmodel);
    }

    public void getMediaCount(BaseDeviceItem basedeviceitem, IActionCallBack iactioncallback)
    {
        getMediaNum((CommonDeviceItem)basedeviceitem, "DOWNLOAD", iactioncallback);
        getMediaNum((CommonDeviceItem)basedeviceitem, "USBDISK", iactioncallback);
    }

    public void getMediaDiskInfo(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "getMediaDiskInfo IN");
        getControlPoint().execute(actionmodel);
    }

    public void getMediaNum(CommonDeviceItem commondeviceitem, String s, IActionCallBack iactioncallback)
    {
        getControlPoint().execute(new _cls1("BrowseMedia", "TotalMusic", "", 1, 1, s, iactioncallback));
    }

    private class _cls1 extends BrowseMediaActionCallback
    {

        final CommonManageTFModule this$0;
        final IActionCallBack val$callBack;
        final String val$mediaName;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void received(Result result)
        {
            if (!mediaName.equals("DOWNLOAD") && !mediaName.equals("EXTDOWNLOAD")) goto _L2; else goto _L1
_L1:
            DossUtils.NUM_DOWNLOAD = result.totalPages;
_L4:
            (new HashMap()).put(mediaName, Integer.valueOf(result.totalPages));
            result = new ActionModel();
            callBack.onSuccess(result);
            return;
_L2:
            if (mediaName.equals("USBDISK"))
            {
                DossUtils.NUM_OTHERS = result.totalPages;
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        _cls1(String s1, String s2, String s3, int i, 
                int j, String s4, IActionCallBack iactioncallback)
        {
            this$0 = CommonManageTFModule.this;
            mediaName = s4;
            callBack = iactioncallback;
            super(final_service, final_s, s1, s2, s3, i, j);
        }
    }

}
