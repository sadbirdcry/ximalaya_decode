// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.app.Activity;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.conn.PwInputFragment;

public class ShukeFailConnFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    private ImageView mBackImg;
    public String mDeviceType;
    private RelativeLayout mFailConnLayout;
    private ImageView mFailImg;
    public String mPassword;
    private TextView mQuestion1;
    private TextView mQuestion2;
    private TextView mQuestion3;
    private TextView mQuestion4;
    private TextView mQuestion5;
    private Button retry;
    private TextView topTextView;

    public ShukeFailConnFragment()
    {
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            mDeviceType = bundle.getString("device");
        }
        if (bundle != null)
        {
            mPassword = bundle.getString("password");
        }
    }

    private void onRetry()
    {
        if (mDeviceType.equals(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi.toString()))
        {
            Bundle bundle = new Bundle();
            bundle.putString("type", "shukewifi");
            bundle.putString("password", mPassword);
            startFragment(com/ximalaya/ting/android/fragment/device/conn/PwInputFragment, bundle);
        }
        finish();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        topTextView = (TextView)mFailConnLayout.findViewById(0x7f0a00ae);
        topTextView.setText("\u9047\u5230\u4E00\u4E9B\u95EE\u9898");
        retry = (Button)mFailConnLayout.findViewById(0x7f0a034d);
        retry.setOnClickListener(this);
        mBackImg = (ImageView)mFailConnLayout.findViewById(0x7f0a071b);
        mFailImg = (ImageView)mFailConnLayout.findViewById(0x7f0a0344);
        mQuestion1 = (TextView)findViewById(0x7f0a0348);
        mQuestion1.setText(0x7f090218);
        mQuestion2 = (TextView)findViewById(0x7f0a0349);
        mQuestion2.setText(0x7f090219);
        mQuestion3 = (TextView)findViewById(0x7f0a034a);
        mQuestion3.setText(0x7f09021a);
        mBackImg.setOnClickListener(this);
        mFailImg.setImageDrawable(mActivity.getResources().getDrawable(0x7f020552));
        initData();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362637: 
            onRetry();
            return;

        case 2131363611: 
            mActivity.onBackPressed();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mFailConnLayout = (RelativeLayout)layoutinflater.inflate(0x7f0300b0, null);
        return mFailConnLayout;
    }
}
