// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShukeItemBindableModel extends BaseItemBindableModel
{

    private final int DOSS_NUM_ALBUM = 3;

    protected ShukeItemBindableModel()
    {
        mAlbums = new HashMap();
        mKeyNames = new ArrayList();
        mKeyNames.clear();
        mKeyNames.add("\u300C\u6545\u4E8B\u952E\u300D");
        mKeyNames.add("\u300C\u513F\u6B4C\u952E\u300D");
        mKeyNames.add("\u300C\u6027\u683C\u57F9\u517B\u952E\u300D");
    }

    public int getAlbumNum()
    {
        return 3;
    }
}
