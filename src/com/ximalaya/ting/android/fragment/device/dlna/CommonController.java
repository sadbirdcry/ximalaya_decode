// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.content.Context;
import android.text.TextUtils;
import com.ximalaya.model.playqueue.Key;
import com.ximalaya.model.playqueue.ListBase;
import com.ximalaya.model.playqueue.TotalQueue;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.LinkedDeviceModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseTuiSongModule;
import com.ximalaya.ting.android.fragment.device.dlna.subscription.MyPlaySubscriptionCallback;
import com.ximalaya.ting.android.fragment.device.doss.DossUtils;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.types.UDN;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            BaseDlnaController, BaseDeviceItem, BasePlayManageController, CommonDeviceItem, 
//            DeviceUtil

public abstract class CommonController extends BaseDlnaController
{

    private String TAG;

    public CommonController(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
        TAG = com/ximalaya/ting/android/fragment/device/dlna/CommonController.getSimpleName();
    }

    public void addDeivce(BaseDeviceItem basedeviceitem)
    {
        Logger.d(TAG, (new StringBuilder()).append("CommonController addDevice IN").append(basedeviceitem.getUdn().toString()).toString());
        if (mDeviceList == null)
        {
            Logger.d(TAG, "CommonController mDeviceList is null");
            mDeviceList = new ArrayList();
        }
        basedeviceitem = initDevice(basedeviceitem);
        if (!mDeviceList.contains(basedeviceitem))
        {
            mDeviceList.add(basedeviceitem);
        }
        Logger.d(TAG, (new StringBuilder()).append("CommonController Devices Size").append(mDeviceList.size()).toString());
        initDeviceInfo(basedeviceitem, null);
    }

    public void change2Bendi(BasePlayManageController baseplaymanagecontroller)
    {
        if (baseplaymanagecontroller.getPlayQueueSubscriptionCallback() != null)
        {
            baseplaymanagecontroller.getPlayQueueSubscriptionCallback().end();
            baseplaymanagecontroller.setPlayQueueSubscriptionCallback(null);
        }
        if (baseplaymanagecontroller.getMediaManagerSubscriptionCallback() != null)
        {
            baseplaymanagecontroller.getMediaManagerSubscriptionCallback().end();
            baseplaymanagecontroller.setMediaManagerSubscriptionCallback(null);
        }
        DossUtils.mDeviceStartNum = 9999;
        DossUtils.mDeviceEndNum = -9999;
    }

    protected abstract BaseItemBindableModel initBindableModel();

    protected abstract BaseDeviceItem initDevice(BaseDeviceItem basedeviceitem);

    public void initDeviceInfo(BaseDeviceItem basedeviceitem, IActionCallBack iactioncallback)
    {
        CommonDeviceItem commondeviceitem = (CommonDeviceItem)basedeviceitem;
        getControlPoint().execute(new _cls1(basedeviceitem));
    }

    public void onCommandFailed(ActionModel actionmodel)
    {
        Logger.d(TAG, "onCommandFailed IN");
        Object obj = actionmodel;
        if (actionmodel == null)
        {
            obj = new ActionModel();
        }
        actionmodel = (IActionCallBack)((ActionModel) (obj)).result.get("callback");
        obj = (BaseDeviceItem)((ActionModel) (obj)).result.get("device");
        if (obj == null || !(obj instanceof CommonDeviceItem))
        {
            if (actionmodel != null)
            {
                actionmodel.onFailed();
            }
            return;
        } else
        {
            getControlPoint().execute(new _cls2(((BaseDeviceItem) (obj))));
            return;
        }
    }

    protected void parseChannelInfo(CommonDeviceItem commondeviceitem, List list, TotalQueue totalqueue)
    {
        if (totalqueue != null)
        {
            List list1 = totalqueue.QueueList;
label0:
            for (int i = 1; i <= commondeviceitem.getBaseBindableModel().getAlbumNum(); i++)
            {
                String s = ((Key)list.get(i)).name;
                int j;
                if (commondeviceitem.getBaseBindableModel() == null)
                {
                    totalqueue = initBindableModel();
                } else
                {
                    totalqueue = commondeviceitem.getBaseBindableModel();
                }
                if (TextUtils.isEmpty(s))
                {
                    continue;
                }
                j = 0;
                do
                {
                    if (j >= list1.size())
                    {
                        continue label0;
                    }
                    ListBase listbase = (ListBase)list1.get(j);
                    if (listbase.Name.equals(s))
                    {
                        long l = -1L;
                        if (!TextUtils.isEmpty(listbase.SearchUrl))
                        {
                            l = DeviceUtil.getChannelAlbumIdByUrl(listbase.SearchUrl);
                        }
                        if (l < 0L)
                        {
                            AlbumModel albummodel = new AlbumModel();
                            albummodel.albumId = -1L;
                            albummodel.title = s;
                            albummodel.tracks = 0;
                            totalqueue.setAlbums(i, albummodel);
                            commondeviceitem.setBaseBindableModel(totalqueue);
                        } else
                        {
                            AlbumModel albummodel1 = new AlbumModel();
                            albummodel1.albumId = l;
                            albummodel1.title = s;
                            albummodel1.tracks = 0;
                            totalqueue.setAlbums(i, albummodel1);
                            commondeviceitem.setBaseBindableModel(totalqueue);
                            DeviceUtil.initOneAlbumInfo(commondeviceitem, l, i, mContext);
                        }
                        Logger.d(TAG, (new StringBuilder()).append("setChannelInfo:1:").append(l).toString());
                    }
                    j++;
                } while (true);
            }

        }
    }

    public void playSound(BaseDeviceItem basedeviceitem, ActionModel actionmodel)
    {
        BaseTuiSongModule basetuisongmodule = (BaseTuiSongModule)getModule(BaseTuiSongModule.NAME);
        if (basetuisongmodule == null)
        {
            return;
        } else
        {
            basetuisongmodule.tuisong(basedeviceitem, actionmodel);
            return;
        }
    }

    public void tuisongExtra(BasePlayManageController baseplaymanagecontroller)
    {
        Logger.d(TAG, "CommonController tuisongExtra IN");
        CommonDeviceItem commondeviceitem = (CommonDeviceItem)baseplaymanagecontroller.getLinkedDeviceModel().getNowDeviceItem();
        if (commondeviceitem.getPQservice() != null)
        {
            baseplaymanagecontroller.setPlayQueueSubscriptionCallback(new MyPlaySubscriptionCallback(commondeviceitem.getPQservice(), 600, baseplaymanagecontroller));
            getControlPoint().execute(baseplaymanagecontroller.getPlayQueueSubscriptionCallback());
        }
        if (commondeviceitem.getMMservice() != null)
        {
            baseplaymanagecontroller.setMediaManagerSubscriptionCallback(new MyPlaySubscriptionCallback(commondeviceitem.getMMservice(), 600, baseplaymanagecontroller));
            getControlPoint().execute(baseplaymanagecontroller.getMediaManagerSubscriptionCallback());
        }
    }


    private class _cls1 extends GetKeyMappingActionCallback
    {

        final CommonController this$0;
        final IActionCallBack val$callBack;
        final CommonDeviceItem val$commonDeviceItem;
        final BaseDeviceItem val$deviceItem;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            removeDeivce(deviceItem);
            callBack.onFailed();
            Logger.d(TAG, "getKeyMapping Failure");
        }

        public void received(ActionInvocation actioninvocation, List list)
        {
            Logger.d(TAG, "getKeyMapping Success");
            class _cls1 extends BrowseQueueActionCallback
            {

                final _cls1 this$1;
                final List val$keyItems;

                public void failure(ActionInvocation actioninvocation1, UpnpResponse upnpresponse, String s)
                {
                    if (callBack != null)
                    {
                        callBack.onFailed();
                    }
                }

                public void received(String s, TotalQueue totalqueue, PlayList playlist)
                {
                    parseChannelInfo(commonDeviceItem, keyItems, totalqueue);
                    class _cls1
                        implements Runnable
                    {

                        final _cls1 this$2;

                        public void run()
                        {
                            String s1;
                            s1 = (new StringBuilder()).append("http://").append(deviceItem.getIpAddress()).toString();
                            s1 = (new StringBuilder()).append(s1).append("/httpapi.asp?command=talkset:remark:").toString();
                            if (!DossUtils.isSearchXimalayaOnly(mContext))
                            {
                                break MISSING_BLOCK_LABEL_103;
                            }
                            s1 = (new StringBuilder()).append(s1).append("ximalaya").toString();
_L1:
                            f.a().a(s1, null, null, null);
                            return;
                            try
                            {
                                s1 = (new StringBuilder()).append(s1).append("doss").toString();
                            }
                            catch (Exception exception)
                            {
                                exception.printStackTrace();
                                return;
                            }
                              goto _L1
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    (new Thread(new _cls1())).start();
                    if (callBack != null)
                    {
                        callBack.onSuccess(null);
                    }
                }

                _cls1(String s, List list)
                {
                    this$1 = _cls1.this;
                    keyItems = list;
                    super(final_service, s);
                }
            }

            getControlPoint().execute(new _cls1("TotalQueue", list));
        }

        _cls1(BaseDeviceItem basedeviceitem)
        {
            this$0 = CommonController.this;
            commonDeviceItem = commondeviceitem;
            callBack = iactioncallback;
            deviceItem = basedeviceitem;
            super(final_service);
        }
    }


    private class _cls2 extends GetKeyMappingActionCallback
    {

        final CommonController this$0;
        final IActionCallBack val$callBack;
        final BaseDeviceItem val$deviceItem;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            removeDeivce(deviceItem);
            DlnaManager.getInstance(mContext).notifyDeviceChanged();
            if (callBack != null)
            {
                callBack.onFailed();
            }
        }

        public void received(ActionInvocation actioninvocation, List list)
        {
            if (callBack != null)
            {
                callBack.onSuccess(null);
            }
        }

        _cls2(BaseDeviceItem basedeviceitem)
        {
            this$0 = CommonController.this;
            callBack = iactioncallback;
            deviceItem = basedeviceitem;
            super(final_service);
        }
    }

}
