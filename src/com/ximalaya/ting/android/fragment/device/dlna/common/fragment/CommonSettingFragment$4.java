// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.widget.CompoundButton;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonSwitchSearchModeModule;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseSwitchSearchModeModule;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonSettingFragment

class this._cls0
    implements android.widget.ChangeListener
{

    final CommonSettingFragment this$0;

    public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
    {
        compoundbutton = (BaseSwitchSearchModeModule)DlnaManager.getInstance(mCon).getController().getModule(BaseSwitchSearchModeModule.NAME);
        ActionModel actionmodel = new ActionModel();
        actionmodel.result.put(CommonSwitchSearchModeModule.DEVICE, mDeviceItem);
        if (flag)
        {
            class _cls1 extends DossDlnaActionCallBackImpl
            {

                final CommonSettingFragment._cls4 this$1;

                public void onFailed()
                {
                    boolean flag1 = false;
                    CustomToast.showToast(mCon, "\u5207\u6362\u641C\u7D22\u6A21\u5F0F\u5931\u8D25", 0);
                    SwitchButton switchbutton = mSetting1Switch;
                    if (!mSetting1Switch.isChecked())
                    {
                        flag1 = true;
                    }
                    switchbutton.setChecked(flag1);
                }

                public void onSuccess(ActionModel actionmodel1)
                {
                    CustomToast.showToast(mCon, "\u5207\u6362\u641C\u7D22\u6A21\u5F0F\u6210\u529F", 0);
                    setSearchXimalayaOnly(true);
                }

            _cls1()
            {
                this$1 = CommonSettingFragment._cls4.this;
                super();
            }
            }

            actionmodel.result.put(CommonSwitchSearchModeModule.CALL_BACK, new _cls1());
            actionmodel.result.put(CommonSwitchSearchModeModule.SEARCH_MODE, getSwitchModeCommand(true));
            compoundbutton.switchSearchMode(actionmodel);
            return;
        } else
        {
            class _cls2 extends DossDlnaActionCallBackImpl
            {

                final CommonSettingFragment._cls4 this$1;

                public void onFailed()
                {
                    boolean flag1 = false;
                    CustomToast.showToast(mCon, "\u5207\u6362\u641C\u7D22\u6A21\u5F0F\u5931\u8D25", 0);
                    SwitchButton switchbutton = mSetting1Switch;
                    if (!mSetting1Switch.isChecked())
                    {
                        flag1 = true;
                    }
                    switchbutton.setChecked(flag1);
                }

                public void onSuccess(ActionModel actionmodel1)
                {
                    CustomToast.showToast(mCon, "\u5207\u6362\u641C\u7D22\u6A21\u5F0F\u6210\u529F", 0);
                    setSearchXimalayaOnly(false);
                }

            _cls2()
            {
                this$1 = CommonSettingFragment._cls4.this;
                super();
            }
            }

            actionmodel.result.put(CommonSwitchSearchModeModule.CALL_BACK, new _cls2());
            actionmodel.result.put(CommonSwitchSearchModeModule.SEARCH_MODE, getSwitchModeCommand(false));
            compoundbutton.switchSearchMode(actionmodel);
            return;
        }
    }

    _cls2()
    {
        this$0 = CommonSettingFragment.this;
        super();
    }
}
