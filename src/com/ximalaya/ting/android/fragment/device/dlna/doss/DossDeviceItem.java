// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.doss;

import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import org.teleal.cling.model.meta.Device;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.doss:
//            DossItemBindableModel

public class DossDeviceItem extends CommonDeviceItem
{

    public DossDeviceItem(BaseDeviceItem basedeviceitem)
    {
        super(basedeviceitem);
    }

    public DossDeviceItem(Device device, String as[])
    {
        super(device, as);
    }

    public BaseItemBindableModel getBaseBindableModel()
    {
        if (mBaseBindableModel == null)
        {
            mBaseBindableModel = new DossItemBindableModel();
        }
        return mBaseBindableModel;
    }
}
