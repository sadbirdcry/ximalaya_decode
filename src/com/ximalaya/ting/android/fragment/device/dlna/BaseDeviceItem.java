// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import com.ximalaya.activity.DeviceItem;
import java.util.HashMap;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.ServiceType;
import org.teleal.cling.model.types.UDN;

public class BaseDeviceItem
{

    protected Service mAVservice;
    protected DeviceItem mDeviceItem;
    protected com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType mDlnaType;
    protected HashMap mModules;
    protected Service mRCservice;

    public BaseDeviceItem(BaseDeviceItem basedeviceitem)
    {
        mDeviceItem = basedeviceitem.mDeviceItem;
        mDlnaType = basedeviceitem.mDlnaType;
    }

    public BaseDeviceItem(Device device)
    {
        mDeviceItem = new DeviceItem(device);
        initService();
    }

    public transient BaseDeviceItem(Device device, String as[])
    {
        mDeviceItem = new DeviceItem(device, as);
        initService();
    }

    public boolean equals(Object obj)
    {
        boolean flag1 = false;
        boolean flag;
        if (this == obj)
        {
            flag = true;
        } else
        {
            flag = flag1;
            if (obj != null)
            {
                flag = flag1;
                if (getClass() == obj.getClass())
                {
                    obj = (BaseDeviceItem)obj;
                    flag = flag1;
                    if (mDeviceItem != null)
                    {
                        flag = flag1;
                        if (((BaseDeviceItem) (obj)).getDeviceItem() != null)
                        {
                            return mDeviceItem.equals(((BaseDeviceItem) (obj)).getDeviceItem());
                        }
                    }
                }
            }
        }
        return flag;
    }

    public Service getAVservice()
    {
        if (mAVservice == null)
        {
            mAVservice = mDeviceItem.getDevice().findService(new ServiceType("schemas-upnp-org", "AVTransport"));
        }
        return mAVservice;
    }

    public Device getDevice()
    {
        return mDeviceItem.getDevice();
    }

    public DeviceItem getDeviceItem()
    {
        return mDeviceItem;
    }

    public com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getDlnaType()
    {
        return mDlnaType;
    }

    public String getIpAddress()
    {
        return mDeviceItem.getIpAddress();
    }

    public Service getRCservice()
    {
        if (mRCservice == null)
        {
            mRCservice = mDeviceItem.getDevice().findService(new ServiceType("schemas-upnp-org", "RenderingControl"));
        }
        return mRCservice;
    }

    public UDN getUdn()
    {
        return mDeviceItem.getUdn();
    }

    protected void initService()
    {
        mAVservice = mDeviceItem.getDevice().findService(new ServiceType("schemas-upnp-org", "AVTransport"));
        mRCservice = mDeviceItem.getDevice().findService(new ServiceType("schemas-upnp-org", "RenderingControl"));
    }

    public void setAVservice(Service service)
    {
        mAVservice = service;
    }

    public void setDeviceItem(DeviceItem deviceitem)
    {
        mDeviceItem = deviceitem;
    }

    public void setDlnaType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype)
    {
        mDlnaType = devicetype;
    }

    public void setIpAddress(String s)
    {
        mDeviceItem.setIpAddress(s);
    }

    public void setRCservice(Service service)
    {
        mRCservice = service;
    }
}
