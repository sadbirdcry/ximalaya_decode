// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.UpdateStatusModel;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.module:
//            CommonUpdateModule

class val.callBack
    implements Runnable
{

    final CommonUpdateModule this$0;
    final IActionCallBack val$callBack;
    final BaseDeviceItem val$deviceItem;

    public void run()
    {
        Object obj;
        do
        {
            obj = getMvRemoteUpdateStart(val$deviceItem);
            Logger.d("update", (new StringBuilder()).append("ret1=").append(((String) (obj))).toString());
        } while (!((String) (obj)).equals("OK"));
        do
        {
            CommonUpdateModule.isUpdating = true;
            Object obj1;
            String s;
            try
            {
                Thread.sleep(2000L);
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((InterruptedException) (obj)).printStackTrace();
            }
            obj = getMvRemoteUpdateStatus(val$deviceItem);
            Logger.d("update", (new StringBuilder()).append("ret2=").append(((String) (obj))).toString());
        } while (((String) (obj)).equals(Integer.valueOf(10)));
        if (((String) (obj)).equals("32") || ((String) (obj)).equals("25") || ((String) (obj)).equals("40"))
        {
            obj = null;
            do
            {
                try
                {
                    Thread.sleep(2000L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
                s = getMvRomDownloadStatus(val$deviceItem);
                Logger.d("update", (new StringBuilder()).append("ret3=").append(s).toString());
                obj1 = obj;
                if (s != null)
                {
                    obj1 = (UpdateStatusModel)JSON.parseObject(s, com/ximalaya/ting/android/fragment/device/dlna/model/UpdateStatusModel);
                }
                if (((UpdateStatusModel) (obj1)).getStatus() == 2 || ((UpdateStatusModel) (obj1)).getStatus() == 5)
                {
                    break;
                }
                obj = obj1;
            } while (((UpdateStatusModel) (obj1)).getStatus() != 6);
            CommonUpdateModule.isUpdating = false;
            if (((UpdateStatusModel) (obj1)).getStatus() == 6)
            {
                val$callBack.onSuccess(null);
                CommonUpdateModule.access$000(CommonUpdateModule.this);
                return;
            } else
            {
                val$callBack.onFailed();
                return;
            }
        }
        if (((String) (obj)).equals("22") || ((String) (obj)).equals("31"))
        {
            val$callBack.onFailed();
            return;
        }
        if (((String) (obj)).equals("20"))
        {
            val$callBack.onFailed();
            return;
        } else
        {
            val$callBack.onFailed();
            return;
        }
    }

    ()
    {
        this$0 = final_commonupdatemodule;
        val$deviceItem = basedeviceitem;
        val$callBack = IActionCallBack.this;
        super();
    }
}
