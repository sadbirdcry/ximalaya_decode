// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.model;

import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;

public class KeyEvent
{

    private BaseDeviceItem deviceItem;
    private int eventKey;
    private Object t;

    public KeyEvent(BaseDeviceItem basedeviceitem)
    {
        deviceItem = basedeviceitem;
    }

    public BaseDeviceItem getDeviceItem()
    {
        return deviceItem;
    }

    public int getEventKey()
    {
        return eventKey;
    }

    public Object getT()
    {
        return t;
    }

    public void setDeviceItem(BaseDeviceItem basedeviceitem)
    {
        deviceItem = basedeviceitem;
    }

    public void setEventKey(int i)
    {
        eventKey = i;
    }

    public void setT(Object obj)
    {
        t = obj;
    }
}
