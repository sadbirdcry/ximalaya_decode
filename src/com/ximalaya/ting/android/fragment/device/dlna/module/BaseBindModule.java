// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.module:
//            BaseDlnaModule

public abstract class BaseBindModule extends BaseDlnaModule
{

    public static final String BINDCOMMAND = "bindcommand";
    public static final String NAME = com/ximalaya/ting/android/fragment/device/dlna/module/BaseBindModule.getSimpleName();
    protected final String TAG = com/ximalaya/ting/android/fragment/device/dlna/module/BaseBindModule.getSimpleName();

    public BaseBindModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public abstract void bind(BaseDeviceItem basedeviceitem, ActionModel actionmodel);

    public final String getModuleName()
    {
        return NAME;
    }

}
