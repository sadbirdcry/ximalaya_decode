// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.content.Context;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import java.util.ArrayList;

public class ShukeUtil
{

    public static final String NAME_ITEM1 = "\u6545\u4E8B";
    public static final String NAME_ITEM2 = "\u513F\u6B4C";
    public static final String NAME_ITEM3 = "\u6027\u683C\u57F9\u517B";
    public static final String NAME_ITEM4 = "\u5F55\u97F3";
    private static String P_LOCK_SEARCH_CHILDREN = "P_LOCK_SEARCH_CHILDREN";
    public static final String P_LOCK_SHUKE_BIND = "P_LOCK_SHUKE_BIND";
    public static final String P_SHUKE_FIRST_USE = "P_SHUKE_FIRST_USE";

    public ShukeUtil()
    {
    }

    public static boolean isFirstUse(Context context, String s)
    {
        return !SharedPreferencesUtil.getInstance(context).getArrayList("P_SHUKE_FIRST_USE").contains(s);
    }

    public static boolean isLockBind(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean("P_LOCK_SHUKE_BIND", true);
    }

    public static boolean isSearchChildrenOnly(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean(P_LOCK_SEARCH_CHILDREN, true);
    }

    public static void setLockBind(boolean flag, Context context)
    {
        SharedPreferencesUtil.getInstance(context).saveBoolean("P_LOCK_SHUKE_BIND", flag);
    }

    public static void setNotFirstUse(Context context, String s)
    {
        ArrayList arraylist = SharedPreferencesUtil.getInstance(context).getArrayList("P_SHUKE_FIRST_USE");
        if (!arraylist.contains(s))
        {
            arraylist.add(s);
        }
        SharedPreferencesUtil.getInstance(context).saveArrayList("P_SHUKE_FIRST_USE", arraylist);
    }

    public static void setSearchChildrenOnly(boolean flag, Context context)
    {
        SharedPreferencesUtil.getInstance(context).saveBoolean(P_LOCK_SEARCH_CHILDREN, flag);
    }

}
