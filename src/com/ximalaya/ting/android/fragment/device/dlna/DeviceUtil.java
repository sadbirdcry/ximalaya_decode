// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.content.Context;
import com.ximalaya.activity.DeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.DeviceDetails;
import org.teleal.cling.model.meta.DeviceIdentity;
import org.teleal.cling.model.types.UDN;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            BaseDeviceItem, DlnaManager, BaseBindableDeviceItem

public class DeviceUtil
{

    public DeviceUtil()
    {
    }

    public static long getChannelAlbumIdByUrl(String s)
    {
        int i;
        if (!Pattern.compile(".*http://3rd.ximalaya.com/albums/[0-9]+/tracks.*").matcher(s).matches())
        {
            return -1L;
        }
        s = Pattern.compile("[/]+").split(s);
        i = 0;
_L3:
        if (i >= s.length)
        {
            break MISSING_BLOCK_LABEL_94;
        }
        if (!s[i].equals("albums")) goto _L2; else goto _L1
_L1:
        s = s[i + 1];
_L4:
        System.out.println((new StringBuilder()).append("result:").append(s).toString());
        return Long.valueOf(s).longValue();
_L2:
        i++;
          goto _L3
        s = null;
          goto _L4
    }

    public static String getDeviceItemName(BaseDeviceItem basedeviceitem)
    {
        try
        {
            String s = basedeviceitem.getDevice().getDetails().getFriendlyName();
            if (basedeviceitem.getDlnaType() == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao)
            {
                return new String(s.getBytes("iso-8859-1"));
            }
            if (basedeviceitem.getDlnaType() == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss)
            {
                return (new String(s.getBytes("UTF-8"))).replaceAll("DOSS", "TingShuBao");
            }
            basedeviceitem = new String(s.getBytes("UTF-8"));
        }
        // Misplaced declaration of an exception variable
        catch (BaseDeviceItem basedeviceitem)
        {
            return null;
        }
        return basedeviceitem;
    }

    public static String getUdn(BaseDeviceItem basedeviceitem)
    {
        if (basedeviceitem == null)
        {
            return "";
        }
        if (basedeviceitem.getDeviceItem() == null)
        {
            return "";
        }
        if (basedeviceitem.getDeviceItem().getDevice() == null)
        {
            return "";
        }
        if (basedeviceitem.getDeviceItem().getDevice().getIdentity() == null)
        {
            return "";
        }
        if (basedeviceitem.getDeviceItem().getDevice().getIdentity().getUdn() == null)
        {
            return "";
        } else
        {
            return basedeviceitem.getDeviceItem().getDevice().getIdentity().getUdn().toString();
        }
    }

    public static void initOneAlbumInfo(final BaseBindableDeviceItem myDeviceItem, final long albumModelId, final int channelId, Context context)
    {
        while (albumModelId <= 0L || !ToolUtil.isConnectToNetwork(context)) 
        {
            return;
        }
        (new _cls1()).myexec(new Void[0]);
    }

    public static boolean isAlbumBind(Context context, long l)
    {
        BaseDeviceItem basedeviceitem = DlnaManager.getInstance(context).getOperationStroageModel().getNowDeviceItem();
        if (basedeviceitem instanceof BaseBindableDeviceItem)
        {
            return isAlbumBind(context, l, (BaseBindableDeviceItem)basedeviceitem);
        } else
        {
            return false;
        }
    }

    public static boolean isAlbumBind(Context context, long l, BaseBindableDeviceItem basebindabledeviceitem)
    {
        context = basebindabledeviceitem.getBaseBindableModel().getAlbums();
        for (basebindabledeviceitem = context.keySet().iterator(); basebindabledeviceitem.hasNext();)
        {
            AlbumModel albummodel = (AlbumModel)context.get((Integer)basebindabledeviceitem.next());
            if (albummodel == null)
            {
                return false;
            }
            if (albummodel.albumId == l)
            {
                return true;
            }
        }

        return false;
    }

    public static boolean isOurDeviceBySSid(String s)
    {
        return s.contains("QAP") || s.contains("Epitome_") || s.contains("NBCAST-") || s.contains("HiMusic-") || s.contains("tingshubao-");
    }

    private class _cls1 extends MyAsyncTask
    {

        AlbumModel tAlbum;
        final long val$albumModelId;
        final int val$channelId;
        final BaseBindableDeviceItem val$myDeviceItem;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            avoid = new RequestParams();
            String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
            s = (new StringBuilder()).append(s).append(albumModelId).append("/").append(true).append("/").append(1).append("/").append(15).toString();
            avoid = f.a().a(s, avoid, null, null);
            Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
            try
            {
                avoid = JSON.parseObject(avoid);
                if ("0".equals(avoid.get("ret").toString()))
                {
                    tAlbum = (AlbumModel)JSON.parseObject(avoid.getString("album"), com/ximalaya/ting/android/model/album/AlbumModel);
                }
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[]) { }
            if (tAlbum != null)
            {
                myDeviceItem.getBaseBindableModel().setAlbums(channelId, tAlbum);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
        }

        _cls1()
        {
            albumModelId = l;
            myDeviceItem = basebindabledeviceitem;
            channelId = i;
            super();
            tAlbum = null;
        }
    }

}
