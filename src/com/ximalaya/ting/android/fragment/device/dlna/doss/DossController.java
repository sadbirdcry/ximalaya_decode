// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.doss;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonController;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonBindModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonDlnaKeyModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonDownloadModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonManageTFModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonPlayModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonSwitchSearchModeModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonTuiSongModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonUpdateModule;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseVolumeControlModule;
import java.util.ArrayList;
import java.util.List;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.doss:
//            DossItemBindableModel, DossDeviceItem

public class DossController extends CommonController
{

    public static final String DEVICE_DOSS_HEADER = "ximalaya-doss-tb00";
    private String TAG;

    public DossController(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
        TAG = com/ximalaya/ting/android/fragment/device/dlna/doss/DossController.getSimpleName();
    }

    public String[] getCheckedUdn()
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add("69679");
        arraylist.add("ximalaya-doss-tb00");
        arraylist.add("713fa55e");
        return (String[])arraylist.toArray(new String[arraylist.size()]);
    }

    public com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getDeviceType()
    {
        return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss;
    }

    protected BaseItemBindableModel initBindableModel()
    {
        return new DossItemBindableModel();
    }

    protected BaseDeviceItem initDevice(BaseDeviceItem basedeviceitem)
    {
        return new DossDeviceItem(basedeviceitem);
    }

    public void initModules()
    {
        setModule(new CommonManageTFModule(mContext, getControlPoint()));
        setModule(new CommonDownloadModule(mContext, getControlPoint()));
        setModule(new CommonPlayModule(mContext, getControlPoint()));
        setModule(new CommonTuiSongModule(mContext, getControlPoint()));
        setModule(new CommonDlnaKeyModule(mContext, getControlPoint()));
        setModule(new CommonBindModule(mContext, getControlPoint()));
        setModule(new CommonSwitchSearchModeModule(mContext, getControlPoint()));
        setModule(new CommonUpdateModule(mContext, getControlPoint()));
        setModule(new BaseVolumeControlModule(mContext, getControlPoint()));
    }
}
