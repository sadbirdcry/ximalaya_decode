// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.support.v4.app.FragmentActivity;
import com.ximalaya.action.GetPositionInfoActionCallback;
import com.ximalaya.audioserver.HttpServer;
import com.ximalaya.service.CoreUpnpServiceListener;
import com.ximalaya.service.DmcUpnpService;
import com.ximalaya.subscribe.PlaySubscriptionCallback;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import com.ximalaya.ting.android.fragment.device.dlna.model.LinkedDeviceModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDlnaKeyModule;
import com.ximalaya.ting.android.fragment.device.dlna.subscription.MyPlaySubscriptionCallback;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import com.ximalaya.ting.android.util.Logger;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.gena.GENASubscription;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.DeviceDetails;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.meta.RemoteDeviceIdentity;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.DeviceType;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;
import org.teleal.cling.support.model.PositionInfo;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            BasePlayManageController, IDlnaController, BaseDeviceItem, DlnaManager, 
//            DeviceUtil

public class CoreController extends BasePlayManageController
{
    private class DeviceListRegistryListener extends DefaultRegistryListener
    {

        final CoreController this$0;

        public void localDeviceAdded(Registry registry, LocalDevice localdevice)
        {
            registry = new BaseDeviceItem(localdevice, new String[] {
                localdevice.getDetails().getFriendlyName(), localdevice.getDisplayString(), (new StringBuilder()).append("(REMOTE) ").append(localdevice.getType().getDisplayString()).toString()
            });
            if (mOnCoreControllerListener != null)
            {
                mOnCoreControllerListener.onDeviceAdded(registry);
            }
        }

        public void localDeviceRemoved(Registry registry, LocalDevice localdevice)
        {
            registry = new BaseDeviceItem(localdevice, new String[] {
                localdevice.getDisplayString()
            });
            if (mOnCoreControllerListener != null)
            {
                mOnCoreControllerListener.onDeviceRemoved(registry);
            }
        }

        public void remoteDeviceAdded(Registry registry, RemoteDevice remotedevice)
        {
            Logger.d(CoreController.TAG, (new StringBuilder()).append("remoteDeviceAdded:").append(remotedevice).append(",").append(remotedevice.getDetails().getBaseURL()).toString());
            break MISSING_BLOCK_LABEL_40;
            if (remotedevice != null && remotedevice.getType() != null && remotedevice.getType().getNamespace() != null && remotedevice.getType().getType() != null && remotedevice.getType().getNamespace().equals("schemas-upnp-org") && remotedevice.getType().getType().equals("MediaRenderer"))
            {
                registry = new BaseDeviceItem(remotedevice, new String[] {
                    remotedevice.getDetails().getFriendlyName(), remotedevice.getDisplayString(), (new StringBuilder()).append("(REMOTE) ").append(remotedevice.getType().getDisplayString()).toString()
                });
                Logger.d(CoreController.TAG, (new StringBuilder()).append("remoteDeviceAdded:").append(DeviceUtil.getDeviceItemName(registry)).toString());
                registry.setIpAddress(((RemoteDeviceIdentity)remotedevice.getIdentity()).getDescriptorURL().getHost());
                if (mOnCoreControllerListener != null)
                {
                    mOnCoreControllerListener.onDeviceAdded(registry);
                    return;
                }
            }
            return;
        }

        public void remoteDeviceDiscoveryFailed(Registry registry, RemoteDevice remotedevice, Exception exception)
        {
        }

        public void remoteDeviceDiscoveryStarted(Registry registry, RemoteDevice remotedevice)
        {
        }

        public void remoteDeviceRemoved(Registry registry, RemoteDevice remotedevice)
        {
            registry = new BaseDeviceItem(remotedevice, new String[] {
                remotedevice.getDisplayString()
            });
            if (mOnCoreControllerListener != null)
            {
                mOnCoreControllerListener.onDeviceRemoved(registry);
            }
        }

        private DeviceListRegistryListener()
        {
            this$0 = CoreController.this;
            super();
        }

        DeviceListRegistryListener(_cls1 _pcls1)
        {
            this();
        }
    }

    static interface OnCoreControllerListener
    {

        public abstract void onDeviceAdded(BaseDeviceItem basedeviceitem);

        public abstract void onDeviceRemoved(BaseDeviceItem basedeviceitem);

        public abstract void onKeyAciton(KeyEvent keyevent);

        public abstract void onNetworkChanged();
    }

    public static class PositionCallBack extends GetPositionInfoActionCallback
    {

        Context mContext;
        PlayerFragment mFragment;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            if (DlnaManager.getInstance(mContext).getLinkedDeviceModel() != null && DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem() != null)
            {
                class _cls2
                    implements IActionCallBack
                {

                    final PositionCallBack this$0;

                    public void onFailed()
                    {
                    }

                    public void onSuccess(ActionModel actionmodel)
                    {
                    }

                _cls2()
                {
                    this$0 = PositionCallBack.this;
                    super();
                }
                }

                actioninvocation = new _cls2();
                upnpresponse = new ActionModel();
                ((ActionModel) (upnpresponse)).result.put("device", DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem());
                ((ActionModel) (upnpresponse)).result.put("callback", actioninvocation);
                DlnaManager.getInstance(mContext).getController(DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem().getDlnaType()).onCommandFailed(upnpresponse);
            }
            Logger.d(CoreController.TAG, "error:getPosition");
        }

        public void received(ActionInvocation actioninvocation, PositionInfo positioninfo)
        {
            final int duration = (int)positioninfo.getTrackDurationSeconds();
            final int elapsed = (int)positioninfo.getTrackElapsedSeconds();
            Logger.d("position", (new StringBuilder()).append("invocation : ").append(actioninvocation.getAction()).append("   positionInfo:  ").append(positioninfo.getTrackDurationSeconds()).append("   ").append(positioninfo.getTrackElapsedSeconds()).toString());
            class _cls1
                implements Runnable
            {

                final PositionCallBack this$0;
                final int val$duration;
                final int val$elapsed;

                public void run()
                {
                    mFragment.setDlnaSeekbarProgress(duration, elapsed);
                }

                _cls1()
                {
                    this$0 = PositionCallBack.this;
                    duration = i;
                    elapsed = j;
                    super();
                }
            }

            mFragment.getActivity().runOnUiThread(new _cls1());
        }

        public PositionCallBack(Service service, Context context, PlayerFragment playerfragment)
        {
            super(service);
            mFragment = playerfragment;
            mContext = context;
        }
    }

    static class PositionThread extends Thread
    {

        private boolean isGetPosition;
        private Context mContext;
        private PlayerFragment mFragment;
        private com.ximalaya.service.DmcUpnpService.UpnpServiceBinder upnpService;

        public void cancel()
        {
            isGetPosition = false;
        }

        public void run()
        {
            if (DlnaManager.getInstance(mContext).isLinkedDeviceValid())
            {
                while (isGetPosition) 
                {
                    try
                    {
                        Thread.sleep(1000L);
                    }
                    catch (InterruptedException interruptedexception)
                    {
                        interruptedexception.printStackTrace();
                    }
                    if (!DlnaManager.getInstance(mContext).isLinkedDeviceValid())
                    {
                        return;
                    }
                    if (DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem().mAVservice != null && upnpService != null)
                    {
                        upnpService.getControlPoint().execute(new PositionCallBack(DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem().mAVservice, mContext, mFragment));
                    }
                }
            }
_L2:
            return;
            if (true) goto _L2; else goto _L1
_L1:
        }


        public PositionThread(Context context, com.ximalaya.service.DmcUpnpService.UpnpServiceBinder upnpservicebinder, PlayerFragment playerfragment)
        {
            isGetPosition = true;
            mContext = context;
            upnpService = upnpservicebinder;
            mFragment = playerfragment;
        }
    }

    static class ScanThread extends Thread
    {

        CoreController controller;
        boolean mIsSearching;

        public void cancel()
        {
            mIsSearching = false;
        }

        public void run()
        {
            while (mIsSearching) 
            {
                if (mIsSearching)
                {
                    controller.searchDevice();
                }
                try
                {
                    Thread.sleep(3000L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
            }
        }

        public ScanThread(CoreController corecontroller)
        {
            mIsSearching = true;
            controller = null;
            controller = corecontroller;
        }
    }


    protected static final int CHANGE_VOLUME_VAULE = 5;
    protected static final long SEARCH_INTERVAL = 3000L;
    private static final int SERVER_PORT = 8192;
    private static final String TAG = com/ximalaya/ting/android/fragment/device/dlna/CoreController.getSimpleName();
    CoreUpnpServiceListener coreUpnpServiceListener;
    private DeviceListRegistryListener deviceListRegistryListener;
    private Context mContext;
    private DlnaManager mDlnaManager;
    private boolean mIsBind;
    private IDlnaController mNowDlnaController;
    private OnCoreControllerListener mOnCoreControllerListener;
    private PositionThread positionThread;
    private Timer scanEndTimer;
    private ScanThread scanThread;
    private HttpServer server;
    private ServiceConnection serviceConnection;
    private TimerTask task;
    private com.ximalaya.service.DmcUpnpService.UpnpServiceBinder upnpService;

    public CoreController(DlnaManager dlnamanager)
    {
        server = null;
        scanEndTimer = null;
        task = null;
        scanThread = null;
        positionThread = null;
        serviceConnection = new _cls1();
        coreUpnpServiceListener = new _cls3();
        mDlnaManager = dlnamanager;
    }

    private boolean isNowWifiModelValid()
    {
        return getLinkedDeviceModel() != null && getLinkedDeviceModel().isValid();
    }

    private void startScanThread()
    {
        Logger.d("search", "startScanThread IN");
        scanThread = new ScanThread(this);
        scanThread.start();
    }

    private void stopSearch()
    {
        if (scanThread != null)
        {
            scanThread.cancel();
            scanThread = null;
        }
    }

    public void bindService(Context context)
    {
        Logger.d(TAG, "bindService IN");
        mContext = context;
        deviceListRegistryListener = new DeviceListRegistryListener(null);
        try
        {
            mContext.bindService(new Intent(mContext, com/ximalaya/service/DmcUpnpService), serviceConnection, 1);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    public void change2Bendi()
    {
        Logger.d("doss", "CoreController change2Bendi IN");
        mPlaySubscriptionCallback = null;
        if (mNowDlnaController != null)
        {
            mNowDlnaController.change2Bendi(this);
            mNowDlnaController = null;
        }
        if (mPlaySubscriptionCallback != null)
        {
            mPlaySubscriptionCallback.end();
            mPlaySubscriptionCallback = null;
        }
        setNowPlayState(0);
        mLinkedDeviceModel = null;
        try
        {
            if (positionThread != null)
            {
                positionThread.cancel();
            }
        }
        catch (Exception exception) { }
        positionThread = null;
    }

    protected void changeVolume(boolean flag)
    {
        Logger.d("WIFI", "changeVolume IN");
        if (!isUpnpServiceCanUse())
        {
            return;
        }
        int i;
        if (flag)
        {
            if (mLinkedDeviceModel.getNowVolume() > 95)
            {
                i = 100;
            } else
            {
                i = mLinkedDeviceModel.getNowVolume() + 5;
            }
        } else
        if (mLinkedDeviceModel.getNowVolume() < 5)
        {
            i = 0;
        } else
        {
            i = mLinkedDeviceModel.getNowVolume() - 5;
        }
        Logger.d("WIFI", (new StringBuilder()).append("volumePlus:").append(i).toString());
        mLinkedDeviceModel.setNowVolume(i);
        upnpService.getControlPoint().execute(new _cls5(mLinkedDeviceModel.getRcService(), i));
    }

    protected void clear()
    {
        upnpService = null;
        server = null;
        mIsBind = false;
        if (scanThread != null)
        {
            scanThread.cancel();
            scanThread = null;
        }
        if (positionThread != null)
        {
            positionThread.cancel();
            positionThread = null;
        }
    }

    public ControlPoint getControlPoint()
    {
        if (upnpService != null)
        {
            return upnpService.getControlPoint();
        } else
        {
            return null;
        }
    }

    public int getNowPlayState()
    {
        if (mLinkedDeviceModel == null)
        {
            return 0;
        } else
        {
            return mLinkedDeviceModel.getNowPlayState();
        }
    }

    public ServiceConnection getServiceConnection()
    {
        return serviceConnection;
    }

    public boolean isUpnpServiceCanUse()
    {
        return upnpService != null && upnpService.getControlPoint() != null;
    }

    public void next()
    {
        while (!isNowWifiModelValid() || !isUpnpServiceCanUse()) 
        {
            return;
        }
        upnpService.getControlPoint().execute(new _cls7(mLinkedDeviceModel.getAvService()));
    }

    public void onHandleKey(GENASubscription genasubscription)
    {
        Logger.d("upnp", "handlePlayKey2 IN");
        if (mLinkedDeviceModel != null)
        {
            if (mNowDlnaController == null)
            {
                mNowDlnaController = mDlnaManager.getController(mLinkedDeviceModel.getNowDeviceItem().getDlnaType());
            }
            if (mLinkedDeviceModel != null && mLinkedDeviceModel.getNowDeviceItem() != null)
            {
                KeyEvent keyevent = ((BaseDlnaKeyModule)mNowDlnaController.getModule(BaseDlnaKeyModule.NAME)).parse2Event(genasubscription, mLinkedDeviceModel.getNowDeviceItem());
                genasubscription = keyevent;
                if (keyevent == null)
                {
                    genasubscription = new KeyEvent(mLinkedDeviceModel.getNowDeviceItem());
                }
                if (mLinkedDeviceModel != null && mLinkedDeviceModel.getNowDeviceItem() != null)
                {
                    genasubscription.setDeviceItem(mLinkedDeviceModel.getNowDeviceItem());
                }
                if (mOnCoreControllerListener != null)
                {
                    mOnCoreControllerListener.onKeyAciton(genasubscription);
                    return;
                }
            }
        }
    }

    public void pause()
    {
        Logger.d(TAG, "pause");
        while (!isNowWifiModelValid() || !isUpnpServiceCanUse()) 
        {
            return;
        }
        Logger.d(TAG, "pause change mNowPlayState");
        setNowPlayState(0);
        upnpService.getControlPoint().execute(new _cls10(mLinkedDeviceModel.getAvService()));
    }

    public void play()
    {
        Logger.d(TAG, "PALY IN");
        while (!isNowWifiModelValid() || !isUpnpServiceCanUse()) 
        {
            return;
        }
        setNowPlayState(1);
        upnpService.getControlPoint().execute(new _cls11(mLinkedDeviceModel.getAvService()));
    }

    public void playSound(ActionModel actionmodel)
    {
        if (mNowDlnaController != null)
        {
            mNowDlnaController.playSound(mLinkedDeviceModel.getNowDeviceItem(), actionmodel);
        }
    }

    public void pre()
    {
        while (!isNowWifiModelValid() || !isUpnpServiceCanUse()) 
        {
            return;
        }
        upnpService.getControlPoint().execute(new _cls8(mLinkedDeviceModel.getAvService()));
    }

    public void removeDevice(BaseDeviceItem basedeviceitem)
    {
        upnpService.getRegistry().removeDevice(basedeviceitem.getUdn());
    }

    public void searchDevice()
    {
        Logger.d("search device", (new StringBuilder()).append("MyDeviceManager searchDevice: upnpService=").append(upnpService).toString());
        if (!isUpnpServiceCanUse())
        {
            Logger.d(TAG, "searchDevice isUpnpServiceCanUse can't use");
            return;
        } else
        {
            upnpService.getControlPoint().search();
            return;
        }
    }

    public void setNowPlayState(int i)
    {
        if (mLinkedDeviceModel == null)
        {
            return;
        } else
        {
            Logger.d(TAG, (new StringBuilder()).append("handleMessage play state:").append(i).toString());
            mLinkedDeviceModel.setNowPlayState(i);
            return;
        }
    }

    void setOnCoreControllerListener(OnCoreControllerListener oncorecontrollerlistener)
    {
        mOnCoreControllerListener = oncorecontrollerlistener;
    }

    public void setRegister(PlayerFragment playerfragment)
    {
        if (isUpnpServiceCanUse()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (positionThread == null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (positionThread.isGetPosition)
        {
            continue; /* Loop/switch isn't completed */
        }
        positionThread.cancel();
        positionThread = null;
        break; /* Loop/switch isn't completed */
        if (true) goto _L1; else goto _L3
_L3:
        positionThread = new PositionThread(mContext, upnpService, playerfragment);
        positionThread.start();
        return;
    }

    public void setSeekBar(int i, int j, double d)
    {
        Logger.d("doss", "set SeekBar IN");
        if (mLinkedDeviceModel != null && mLinkedDeviceModel.isValid())
        {
            int k = (int)(((double)i * d) / (double)j);
            i = k / 3600;
            j = (k - i * 3600) / 60;
            k = k - i * 3600 - j * 60;
            String s = "";
            if (i < 10)
            {
                s = (new StringBuilder()).append("").append("0").toString();
            }
            String s1 = (new StringBuilder()).append(s).append(i).append(":").toString();
            s = s1;
            if (j < 10)
            {
                s = (new StringBuilder()).append(s1).append("0").toString();
            }
            s1 = (new StringBuilder()).append(s).append(j).append(":").toString();
            s = s1;
            if (k < 10)
            {
                s = (new StringBuilder()).append(s1).append("0").toString();
            }
            s = (new StringBuilder()).append(s).append(k).toString();
            Logger.d("WIFI", (new StringBuilder()).append("seekBar result").append(s).toString());
            if (isUpnpServiceCanUse())
            {
                upnpService.getControlPoint().execute(new _cls12(mLinkedDeviceModel.getNowDeviceItem().mAVservice, s));
                return;
            }
        }
    }

    public void setSeekBar(int i, int j, double d, IActionCallBack iactioncallback)
    {
        Logger.d("doss", "set SeekBar IN");
        while (!mDlnaManager.isDeviceLinked() || !isUpnpServiceCanUse()) 
        {
            return;
        }
        int k = (int)(((double)i * d) / (double)j);
        i = k / 3600;
        j = (k - i * 3600) / 60;
        k = k - i * 3600 - j * 60;
        iactioncallback = "";
        if (i < 10)
        {
            iactioncallback = (new StringBuilder()).append("").append("0").toString();
        }
        String s = (new StringBuilder()).append(iactioncallback).append(i).append(":").toString();
        iactioncallback = s;
        if (j < 10)
        {
            iactioncallback = (new StringBuilder()).append(s).append("0").toString();
        }
        s = (new StringBuilder()).append(iactioncallback).append(j).append(":").toString();
        iactioncallback = s;
        if (k < 10)
        {
            iactioncallback = (new StringBuilder()).append(s).append("0").toString();
        }
        iactioncallback = (new StringBuilder()).append(iactioncallback).append(k).toString();
        Logger.d("WIFI", (new StringBuilder()).append("seekBar result").append(iactioncallback).toString());
        upnpService.getControlPoint().execute(new _cls6(mLinkedDeviceModel.getAvService(), iactioncallback));
    }

    public void setServiceConnection(ServiceConnection serviceconnection)
    {
        serviceConnection = serviceconnection;
    }

    protected void startScanThread(int i)
    {
        if (i != -1)
        {
            if (scanEndTimer != null)
            {
                scanEndTimer.cancel();
                scanEndTimer = null;
            }
            scanEndTimer = new Timer();
            task = new _cls2();
            scanEndTimer.schedule(task, i * 1000);
        }
        stopScanThread();
        startScanThread();
    }

    public boolean startServer()
    {
        if (server != null)
        {
            return true;
        }
        try
        {
            server = new HttpServer(8192);
            Logger.d(TAG, "startServer success");
        }
        catch (IOException ioexception)
        {
            System.err.println((new StringBuilder()).append("Couldn't start server:\n").append(ioexception).toString());
            Logger.d(TAG, (new StringBuilder()).append("startServer failed:").append(ioexception).toString());
            return false;
        }
        return true;
    }

    public void stop()
    {
        Logger.d(TAG, "stop IN");
        while (!isNowWifiModelValid() || !isUpnpServiceCanUse()) 
        {
            return;
        }
        Logger.d("WIFI", "stop");
        setNowPlayState(0);
        upnpService.getControlPoint().execute(new _cls9(mLinkedDeviceModel.getAvService()));
        upnpService.getRegistry().removeListener(deviceListRegistryListener);
    }

    protected void stopScanThread()
    {
        try
        {
            if (scanThread != null)
            {
                Logger.d("search", "stopScanThread IN");
                scanThread.cancel();
                scanThread = null;
            }
            return;
        }
        catch (Exception exception)
        {
            Logger.e("CoreController", "interrupt exception");
        }
    }

    public void tuisongDevice(BaseDeviceItem basedeviceitem)
    {
        if (isUpnpServiceCanUse())
        {
            if (mLinkedDeviceModel == null)
            {
                mLinkedDeviceModel = new LinkedDeviceModel();
            }
            mLinkedDeviceModel.setNowDeviceItem(basedeviceitem);
            mNowDlnaController = mDlnaManager.getController(basedeviceitem.getDlnaType());
            if (mNowDlnaController != null)
            {
                if (mPlaySubscriptionCallback == null)
                {
                    mPlaySubscriptionCallback = new MyPlaySubscriptionCallback(mLinkedDeviceModel.getAvService(), 600, this);
                }
                upnpService.getControlPoint().execute(mPlaySubscriptionCallback);
                mNowDlnaController.tuisongExtra(this);
                return;
            }
        }
    }

    public void unBindService()
    {
        if (!mIsBind)
        {
            return;
        } else
        {
            mContext.unbindService(serviceConnection);
            mIsBind = false;
            return;
        }
    }

    public void volumePlus(boolean flag)
    {
        if (isUpnpServiceCanUse())
        {
            Logger.d("WIFI", (new StringBuilder()).append("upnpService.getControlPoint():").append(upnpService.getControlPoint()).toString());
            if (isUpnpServiceCanUse())
            {
                upnpService.getControlPoint().execute(new _cls4(flag));
                return;
            }
        }
    }




/*
    static com.ximalaya.service.DmcUpnpService.UpnpServiceBinder access$102(CoreController corecontroller, com.ximalaya.service.DmcUpnpService.UpnpServiceBinder upnpservicebinder)
    {
        corecontroller.upnpService = upnpservicebinder;
        return upnpservicebinder;
    }

*/


/*
    static boolean access$202(CoreController corecontroller, boolean flag)
    {
        corecontroller.mIsBind = flag;
        return flag;
    }

*/






    private class _cls1
        implements ServiceConnection
    {

        final CoreController this$0;

        public void onServiceConnected(ComponentName componentname, IBinder ibinder)
        {
            upnpService = (com.ximalaya.service.DmcUpnpService.UpnpServiceBinder)ibinder;
            mIsBind = true;
            if (coreUpnpServiceListener != null)
            {
                upnpService.setProcessor(coreUpnpServiceListener);
            }
            Log.v("WIFI", "Connected to UPnP Service");
            if (upnpService.isSuccessInitialized())
            {
                upnpService.getRegistry().addListener(deviceListRegistryListener);
                upnpService.getRegistry().removeAllRemoteDevices();
                upnpService.getRegistry().removeAllLocalDevices();
                startServer();
                searchDevice();
            }
        }

        public void onServiceDisconnected(ComponentName componentname)
        {
            upnpService = null;
        }

        _cls1()
        {
            this$0 = CoreController.this;
            super();
        }
    }


    private class _cls3
        implements CoreUpnpServiceListener
    {

        final CoreController this$0;

        public void onNetworkChanged(NetworkInterface networkinterface)
        {
            if (mOnCoreControllerListener != null)
            {
                mOnCoreControllerListener.onNetworkChanged();
            }
            stopSearch();
            unBindService();
            bindService(mContext);
        }

        public void onRouterDisabled()
        {
        }

        public void onRouterEnabled()
        {
        }

        public void onRouterError(String s)
        {
        }

        _cls3()
        {
            this$0 = CoreController.this;
            super();
        }
    }


    private class _cls5 extends SetVolumeActionCallback
    {

        final CoreController this$0;

        protected void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse)
        {
            super.failure(actioninvocation, upnpresponse);
        }

        public void success(ActionInvocation actioninvocation)
        {
        }

        _cls5(Service service, long l)
        {
            this$0 = CoreController.this;
            super(service, l);
        }
    }


    private class _cls7 extends NextActionCallback
    {

        final CoreController this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("error", (new StringBuilder()).append("next thread:").append(Thread.currentThread().getName()).toString());
        }

        _cls7(Service service)
        {
            this$0 = CoreController.this;
            super(service);
        }
    }


    private class _cls10 extends PauseActionCallback
    {

        final CoreController this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("error", (new StringBuilder()).append("pause thread:").append(Thread.currentThread().getName()).toString());
        }

        _cls10(Service service)
        {
            this$0 = CoreController.this;
            super(service);
        }
    }


    private class _cls11 extends PlayActionCallback
    {

        final CoreController this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d(CoreController.TAG, "play");
        }

        _cls11(Service service)
        {
            this$0 = CoreController.this;
            super(service);
        }
    }


    private class _cls8 extends PreviousActionCallback
    {

        final CoreController this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("error", (new StringBuilder()).append("pre thread:").append(Thread.currentThread().getName()).toString());
        }

        _cls8(Service service)
        {
            this$0 = CoreController.this;
            super(service);
        }
    }


    private class _cls12 extends SeekActionCallback
    {

        final CoreController this$0;

        protected void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse)
        {
            super.failure(actioninvocation, upnpresponse);
            Logger.d(CoreController.TAG, "error:setSeekBar");
            actioninvocation = new KeyEvent(mLinkedDeviceModel.getNowDeviceItem());
            actioninvocation.setEventKey(1);
            if (mOnCoreControllerListener != null)
            {
                mOnCoreControllerListener.onKeyAciton(actioninvocation);
            }
        }

        public void success(ActionInvocation actioninvocation)
        {
            try
            {
                Thread.sleep(1500L);
            }
            // Misplaced declaration of an exception variable
            catch (ActionInvocation actioninvocation)
            {
                actioninvocation.printStackTrace();
            }
            if (DlnaManager.getInstance(mContext).getLinkedDeviceModel() != null)
            {
                actioninvocation = new KeyEvent(DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem());
                actioninvocation.setEventKey(1);
                if (mOnCoreControllerListener != null)
                {
                    mOnCoreControllerListener.onKeyAciton(actioninvocation);
                }
                Logger.d(CoreController.TAG, "success:setSeekBar");
            }
        }

        _cls12(Service service, String s)
        {
            this$0 = CoreController.this;
            super(service, s);
        }
    }


    private class _cls6 extends SeekActionCallback
    {

        final CoreController this$0;

        protected void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse)
        {
            super.failure(actioninvocation, upnpresponse);
            Logger.d(CoreController.TAG, "error:setSeekBar");
        }

        public void success(ActionInvocation actioninvocation)
        {
            try
            {
                Thread.sleep(1500L);
            }
            // Misplaced declaration of an exception variable
            catch (ActionInvocation actioninvocation)
            {
                actioninvocation.printStackTrace();
            }
            Logger.d(CoreController.TAG, "success:setSeekBar");
        }

        _cls6(Service service, String s)
        {
            this$0 = CoreController.this;
            super(service, s);
        }
    }


    private class _cls2 extends TimerTask
    {

        final CoreController this$0;

        public void run()
        {
            stopScanThread();
        }

        _cls2()
        {
            this$0 = CoreController.this;
            super();
        }
    }


    private class _cls9 extends StopActionCallback
    {

        final CoreController this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("error", (new StringBuilder()).append("stop thread:").append(Thread.currentThread().getName()).toString());
        }

        _cls9(Service service)
        {
            this$0 = CoreController.this;
            super(service);
        }
    }


    private class _cls4 extends GetVolumeActionCallback
    {

        final CoreController this$0;
        final boolean val$isPlus;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void received(ActionInvocation actioninvocation, int i)
        {
            mLinkedDeviceModel.setNowVolume(i);
            Logger.d("WIFI", (new StringBuilder()).append("initVolume:").append(i).toString());
            changeVolume(isPlus);
        }

        _cls4(boolean flag)
        {
            this$0 = CoreController.this;
            isPlus = flag;
            super(final_service);
        }
    }

}
