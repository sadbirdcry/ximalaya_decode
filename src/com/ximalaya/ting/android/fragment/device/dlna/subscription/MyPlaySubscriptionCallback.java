// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.subscription;

import com.ximalaya.subscribe.PlaySubscriptionCallback;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.model.gena.GENASubscription;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;

public class MyPlaySubscriptionCallback extends PlaySubscriptionCallback
{
    public static interface OnPlayKeyHandler
    {

        public abstract void onHandleKey(GENASubscription genasubscription);
    }


    OnPlayKeyHandler mOnPlayKeyHandler;

    public MyPlaySubscriptionCallback(Service service, int i, OnPlayKeyHandler onplaykeyhandler)
    {
        super(service, i);
        mOnPlayKeyHandler = onplaykeyhandler;
    }

    protected void eventReceived(GENASubscription genasubscription)
    {
        if (genasubscription != null)
        {
            Logger.d("upnp", (new StringBuilder()).append("playSound:eventReceived:").append(genasubscription).toString());
            if (mOnPlayKeyHandler != null)
            {
                mOnPlayKeyHandler.onHandleKey(genasubscription);
                return;
            }
        }
    }

    protected void failed(GENASubscription genasubscription, UpnpResponse upnpresponse, Exception exception, String s)
    {
        Logger.d("error", (new StringBuilder()).append("MyPlaySubscriptionCallback thread:").append(Thread.currentThread().getName()).toString());
    }
}
