// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.content.Context;
import com.ximalaya.activity.DeviceItem;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.module.IBaseModule;
import com.ximalaya.ting.android.library.util.Logger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.meta.RemoteDeviceIdentity;
import org.teleal.cling.protocol.RetrieveRemoteDescriptors;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            IDlnaController, DeviceUtil, BaseDeviceItem, DlnaManager, 
//            BasePlayManageController

public abstract class BaseDlnaController
    implements IDlnaController
{

    protected Context mContext;
    protected ControlPoint mControlPoint;
    protected List mDeviceList;
    protected Map mModules;

    protected BaseDlnaController(Context context, ControlPoint controlpoint)
    {
        setControlPoint(controlpoint);
        mDeviceList = new ArrayList();
        mContext = context;
        initModules();
    }

    public abstract void addDeivce(BaseDeviceItem basedeviceitem);

    public void clearDeivce()
    {
        mDeviceList.clear();
    }

    public List getAllDevices()
    {
        if (mDeviceList == null)
        {
            mDeviceList = new ArrayList();
        }
        Logger.d("doss", (new StringBuilder()).append("size()").append(mDeviceList.size()).toString());
        return mDeviceList;
    }

    public abstract String[] getCheckedUdn();

    public ControlPoint getControlPoint()
    {
        return mControlPoint;
    }

    public abstract com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getDeviceType();

    public IBaseModule getModule(String s)
    {
        return (IBaseModule)mModules.get(s);
    }

    public abstract void initDeviceInfo(BaseDeviceItem basedeviceitem, IActionCallBack iactioncallback);

    public boolean isThisDevice(BaseDeviceItem basedeviceitem)
    {
        basedeviceitem = DeviceUtil.getUdn(basedeviceitem);
        String as[] = getCheckedUdn();
        if (as != null)
        {
            int i = 0;
            while (i < as.length) 
            {
                if (basedeviceitem.contains(as[i]))
                {
                    return true;
                }
                i++;
            }
        }
        return false;
    }

    public void removeDeivce(BaseDeviceItem basedeviceitem)
    {
        if (basedeviceitem.getDeviceItem().getDevice() instanceof RemoteDevice)
        {
            RemoteDevice remotedevice = (RemoteDevice)basedeviceitem.getDeviceItem().getDevice();
            RetrieveRemoteDescriptors.activeRetrievals.remove(((RemoteDeviceIdentity)remotedevice.getIdentity()).getDescriptorURL());
            DlnaManager.getInstance(mContext).removeDeviceFromDLNALib(basedeviceitem);
        }
        mDeviceList.remove(basedeviceitem);
    }

    public void setControlPoint(ControlPoint controlpoint)
    {
        mControlPoint = controlpoint;
    }

    public void setModule(IBaseModule ibasemodule)
    {
        if (mModules == null)
        {
            mModules = new HashMap();
        }
        if (!mModules.containsKey(ibasemodule.getModuleName()))
        {
            mModules.put(ibasemodule.getModuleName(), ibasemodule);
        }
    }

    public void tuisongExtra(BasePlayManageController baseplaymanagecontroller)
    {
    }
}
