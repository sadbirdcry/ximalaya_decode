// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.util.Logger;

public class ShukeAccountBindingDialogFragment extends DialogFragment
    implements android.view.View.OnClickListener
{

    private RelativeLayout mBtnBinding;
    private RelativeLayout mBtnLater;

    public ShukeAccountBindingDialogFragment()
    {
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362301: 
            dismissAllowingStateLoss();
            return;

        case 2131362302: 
            break;
        }
        if (getActivity() != null)
        {
            Logger.d("message", "start Activity IN");
            view = new Intent(getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity);
            getActivity().startActivityForResult(view, 0);
        }
        dismissAllowingStateLoss();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setStyle(0x7f0b0027, 0x7f0b0027);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f030068, viewgroup, false);
        mBtnLater = (RelativeLayout)layoutinflater.findViewById(0x7f0a01fd);
        mBtnLater.setOnClickListener(this);
        mBtnLater = (RelativeLayout)layoutinflater.findViewById(0x7f0a01fe);
        mBtnLater.setOnClickListener(this);
        return layoutinflater;
    }
}
