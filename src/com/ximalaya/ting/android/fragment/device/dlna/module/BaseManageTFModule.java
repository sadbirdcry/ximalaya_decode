// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.module:
//            BaseDlnaModule

public abstract class BaseManageTFModule extends BaseDlnaModule
{

    public static final String NAME = com/ximalaya/ting/android/fragment/device/dlna/module/BaseManageTFModule.getSimpleName();

    public BaseManageTFModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public abstract void browseMedia(ActionModel actionmodel);

    public abstract void deleteMedia(ActionModel actionmodel);

    public abstract void getMediaCount(BaseDeviceItem basedeviceitem, IActionCallBack iactioncallback);

    public abstract void getMediaDiskInfo(ActionModel actionmodel);

    public String getModuleName()
    {
        return NAME;
    }

}
