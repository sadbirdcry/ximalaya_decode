// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.fragment.device.conn.ConnFragment;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeConnectingFragment

public class ShukeConnFragment extends ConnFragment
{

    private ImageView mShukeImageView;
    private RelativeLayout nextStepView;

    public ShukeConnFragment()
    {
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mShukeImageView = (ImageView)nextStepView.findViewById(0x7f0a0342);
        mShukeImageView.setImageDrawable(mActivity.getApplicationContext().getResources().getDrawable(0x7f020553));
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        nextStepView = (RelativeLayout)super.onCreateView(layoutinflater, viewgroup, bundle);
        return nextStepView;
    }

    protected void toConnectingFragment(Bundle bundle)
    {
        startFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeConnectingFragment, bundle);
    }
}
