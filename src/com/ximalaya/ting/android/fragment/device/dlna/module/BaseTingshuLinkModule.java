// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.module:
//            BaseDlnaModule

public abstract class BaseTingshuLinkModule extends BaseDlnaModule
{

    public static final String NAME = com/ximalaya/ting/android/fragment/device/dlna/module/BaseTingshuLinkModule.getSimpleName();

    public BaseTingshuLinkModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public abstract String getGetWifiListUrl();

    public String getModuleName()
    {
        return NAME;
    }

    public abstract void getWifiList(IActionCallBack iactioncallback);

}
