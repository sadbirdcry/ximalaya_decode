// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import com.ximalaya.action.GetMediaDiskInfoActionCallback;
import com.ximalaya.model.mediamanager.DiskInfoResult;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonDeviceAlbumSectionDownloadFragment

class this._cls0 extends GetMediaDiskInfoActionCallback
{

    final CommonDeviceAlbumSectionDownloadFragment this$0;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        Logger.d("doss", "getMediaDiskInfo failure");
    }

    public void received(DiskInfoResult diskinforesult)
    {
        Logger.d("doss", "getMediaDiskInfo SUCCESS");
        mDeviceItem.setStorage(diskinforesult);
    }

    (Service service)
    {
        this$0 = CommonDeviceAlbumSectionDownloadFragment.this;
        super(service);
    }
}
