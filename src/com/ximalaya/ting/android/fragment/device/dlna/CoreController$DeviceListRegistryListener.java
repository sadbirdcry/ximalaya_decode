// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import com.ximalaya.ting.android.util.Logger;
import java.net.URL;
import org.teleal.cling.model.meta.DeviceDetails;
import org.teleal.cling.model.meta.LocalDevice;
import org.teleal.cling.model.meta.RemoteDevice;
import org.teleal.cling.model.meta.RemoteDeviceIdentity;
import org.teleal.cling.model.types.DeviceType;
import org.teleal.cling.registry.DefaultRegistryListener;
import org.teleal.cling.registry.Registry;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            CoreController, BaseDeviceItem, DeviceUtil

private class <init> extends DefaultRegistryListener
{

    final CoreController this$0;

    public void localDeviceAdded(Registry registry, LocalDevice localdevice)
    {
        registry = new BaseDeviceItem(localdevice, new String[] {
            localdevice.getDetails().getFriendlyName(), localdevice.getDisplayString(), (new StringBuilder()).append("(REMOTE) ").append(localdevice.getType().getDisplayString()).toString()
        });
        if (CoreController.access$500(CoreController.this) != null)
        {
            CoreController.access$500(CoreController.this).DeviceAdded(registry);
        }
    }

    public void localDeviceRemoved(Registry registry, LocalDevice localdevice)
    {
        registry = new BaseDeviceItem(localdevice, new String[] {
            localdevice.getDisplayString()
        });
        if (CoreController.access$500(CoreController.this) != null)
        {
            CoreController.access$500(CoreController.this).DeviceRemoved(registry);
        }
    }

    public void remoteDeviceAdded(Registry registry, RemoteDevice remotedevice)
    {
        Logger.d(CoreController.access$400(), (new StringBuilder()).append("remoteDeviceAdded:").append(remotedevice).append(",").append(remotedevice.getDetails().getBaseURL()).toString());
        break MISSING_BLOCK_LABEL_40;
        if (remotedevice != null && remotedevice.getType() != null && remotedevice.getType().getNamespace() != null && remotedevice.getType().getType() != null && remotedevice.getType().getNamespace().equals("schemas-upnp-org") && remotedevice.getType().getType().equals("MediaRenderer"))
        {
            registry = new BaseDeviceItem(remotedevice, new String[] {
                remotedevice.getDetails().getFriendlyName(), remotedevice.getDisplayString(), (new StringBuilder()).append("(REMOTE) ").append(remotedevice.getType().getDisplayString()).toString()
            });
            Logger.d(CoreController.access$400(), (new StringBuilder()).append("remoteDeviceAdded:").append(DeviceUtil.getDeviceItemName(registry)).toString());
            registry.setIpAddress(((RemoteDeviceIdentity)remotedevice.getIdentity()).getDescriptorURL().getHost());
            if (CoreController.access$500(CoreController.this) != null)
            {
                CoreController.access$500(CoreController.this).DeviceAdded(registry);
                return;
            }
        }
        return;
    }

    public void remoteDeviceDiscoveryFailed(Registry registry, RemoteDevice remotedevice, Exception exception)
    {
    }

    public void remoteDeviceDiscoveryStarted(Registry registry, RemoteDevice remotedevice)
    {
    }

    public void remoteDeviceRemoved(Registry registry, RemoteDevice remotedevice)
    {
        registry = new BaseDeviceItem(remotedevice, new String[] {
            remotedevice.getDisplayString()
        });
        if (CoreController.access$500(CoreController.this) != null)
        {
            CoreController.access$500(CoreController.this).DeviceRemoved(registry);
        }
    }

    private ()
    {
        this$0 = CoreController.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
