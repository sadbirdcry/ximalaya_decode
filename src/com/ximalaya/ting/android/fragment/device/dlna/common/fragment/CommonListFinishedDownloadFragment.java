// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.support.v4.app.FragmentActivity;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.model.playqueue.ListInfo;
import com.ximalaya.model.playqueue.PlayList;
import com.ximalaya.model.playqueue.SearchUrl;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.common.adapter.CommonTFListAdapter;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonManageTFModule;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseManageTFModule;
import com.ximalaya.ting.android.fragment.device.dlna.module.BasePlayModule;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.List;
import org.teleal.cling.support.playqueue.callback.backupqueue.BackupQueueConstants;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonBaseWifiDevicePlayableListFragment

public class CommonListFinishedDownloadFragment extends CommonBaseWifiDevicePlayableListFragment
{

    public static List downloadList;
    protected CommonManageTFModule mManageTFModule;

    public CommonListFinishedDownloadFragment()
    {
    }

    public PlayList createQueue()
    {
        PlayList playlist = super.createQueue();
        Object obj = new SearchUrl();
        obj.ObjectID = getObjectId();
        playlist.listName = "local";
        obj.BrowseFlag = "BrowseMedia";
        obj.MediaType = "TotalMusic";
        obj.Filter = "";
        obj.PerPage = (new StringBuilder()).append("").append(loadingData.pageSize).toString();
        obj.Page = (new StringBuilder()).append("").append(soundPage).toString();
        obj = JSONObject.toJSONString(obj);
        playlist.listInfo.searchUrl = (new StringBuilder()).append("local://").append(((String) (obj))).toString();
        playlist.listName = (new StringBuilder()).append(playlist.listName).append(getObjectId()).toString();
        playlist.listInfo.SwitchPageMode = 1;
        return playlist;
    }

    protected void deleteAllMedia()
    {
        (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u6E05\u7A7A\u6240\u6709\u4E0B\u8F7D\u7684\u58F0\u97F3\u5417\uFF1F").setOkBtn(new _cls5()).showConfirm();
    }

    protected BaseAdapter getAdapter()
    {
        mAdapter = new CommonTFListAdapter(mCon, getItems(), "\u5DF2\u4E0B\u8F7D", getObjectId());
        return mAdapter;
    }

    protected List getItems()
    {
        if (downloadList == null)
        {
            downloadList = new ArrayList();
        }
        return downloadList;
    }

    protected String getObjectId()
    {
        return "DOWNLOAD";
    }

    protected void initData()
    {
        downloadList = new ArrayList();
        mManageTFModule = (CommonManageTFModule)DlnaManager.getInstance(getActivity().getApplicationContext()).getController(mDeviceItem.getDlnaType()).getModule(BaseManageTFModule.NAME);
        super.initData();
    }

    protected void initUi()
    {
        super.initUi();
        mTopBar.setVisibility(8);
    }

    protected void loadMoreData()
    {
        super.loadMoreData();
        ActionModel actionmodel = ActionModel.createModel(new _cls3(((CommonDeviceItem)mDeviceItem).getMMservice(), getObjectId(), "BrowseMedia", "TotalMusic", "", loadingData.pageSize, loadingData.pageId));
        mManageTFModule.browseMedia(actionmodel);
    }

    protected void onMediaChangedReceived()
    {
        Logger.d("common", "CommonListFinishedDownloadFragment onMediaChangedReceived IN");
        refreshMediaList();
        Object obj = BackupQueueConstants.getBackupQueueContext(createQueue());
        obj = ActionModel.createModel(new _cls4(((CommonDeviceItem)mDeviceItem).getPQservice(), ((String) (obj))));
        mPlayModule.createQueue(((ActionModel) (obj)));
        Logger.d("doss", "\u4E0B\u8F7D\u72B6\u6001\u53D8\u5316,\u53D1\u751Fmedia_add\u4E8B\u4EF6");
    }

    protected void playSound(int i)
    {
        Object obj = (new StringBuilder()).append("local").append(getObjectId()).toString();
        if (soundPage == currentQueuePage)
        {
            obj = ActionModel.createModel(new _cls1(((CommonDeviceItem)mDeviceItem).getPQservice(), ((String) (obj)), i % loadingData.pageSize + 1));
            mPlayModule.playWithIndex(((ActionModel) (obj)));
            return;
        } else
        {
            String s = BackupQueueConstants.getBackupQueueContext(createQueue());
            obj = ActionModel.createModel(new _cls2(((String) (obj)), i));
            mPlayModule.createQueue(((ActionModel) (obj)));
            currentQueuePage = soundPage;
            mPositionInCurrentPage = (i - 1) % loadingData.pageSize + 1;
            return;
        }
    }

    public void refreshMediaList()
    {
        Logger.d("common", "CommonListFinishedDownloadFragment refreshMediaList IN");
        super.refreshMediaList();
        ActionModel actionmodel = ActionModel.createModel(new _cls6(((CommonDeviceItem)mDeviceItem).getMMservice(), getObjectId(), "BrowseMedia", "TotalMusic", "", loadingData.pageSize, loadingData.pageId - 1));
        mManageTFModule.browseMedia(actionmodel);
    }

    protected void showNoData()
    {
        super.showNoData();
        mNoDataText.setText("\u4EB2\uFF0C\u4F60\u6682\u65F6\u6CA1\u6709\u5DF2\u4E0B\u8F7D\u7684\u8282\u76EE\u54E6~");
    }









/*
    static MyProgressDialog access$502(CommonListFinishedDownloadFragment commonlistfinisheddownloadfragment, MyProgressDialog myprogressdialog)
    {
        commonlistfinisheddownloadfragment.loadingDialog = myprogressdialog;
        return myprogressdialog;
    }

*/





    private class _cls5
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final CommonListFinishedDownloadFragment this$0;

        public void onExecute()
        {
            class _cls1
                implements Runnable
            {

                final _cls5 this$1;

                public void run()
                {
                    class _cls1
                        implements Runnable
                    {

                        final _cls1 this$2;

                        public void run()
                        {
                            
// JavaClassFileOutputException: get_constant: invalid tag

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    getActivity().runOnUiThread(new _cls1());
                    class _cls2 extends DeleteMediaActionCallback
                    {

                        final _cls1 this$2;

                        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
                        {
                            Logger.d("doss", "deleteMedia failure");
                            refreshMediaList();
                            dismissLoadingDialog();
                        }

                        public void success(ActionInvocation actioninvocation)
                        {
                            Logger.d("doss", "deleteMedia SUCCESS");
                            getItems().clear();
                            refreshMediaList();
                            dismissLoadingDialog();
                        }

                            _cls2(Service service, String s, String s1, String s2, int i)
                            {
                                this$2 = _cls1.this;
                                super(service, s, s1, s2, i);
                            }
                    }

                    ActionModel actionmodel = ActionModel.createModel(new _cls2(((CommonDeviceItem)_fld0.CommonListFinishedDownloadFragment).getMMservice(), getObjectId(), "TotalMusic", "", -2));
                    mManageTFModule.deleteMedia(actionmodel);
                }

                _cls1()
                {
                    this$1 = _cls5.this;
                    super();
                }
            }

            (new Thread(new _cls1())).start();
        }

        _cls5()
        {
            this$0 = CommonListFinishedDownloadFragment.this;
            super();
        }
    }


    private class _cls3 extends BrowseMediaActionCallback
    {

        final CommonListFinishedDownloadFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void received(Result result)
        {
            Logger.d("doss", "broseMedia received");
            onBrowseMediaSuccess(result);
        }

        _cls3(Service service, String s, String s1, String s2, String s3, int i, 
                int j)
        {
            this$0 = CommonListFinishedDownloadFragment.this;
            super(service, s, s1, s2, s3, i, j);
        }
    }


    private class _cls4 extends CreateQueueActionCallback
    {

        final CommonListFinishedDownloadFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "CreateQueue Failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "CreateQueue Success");
        }

        _cls4(Service service, String s)
        {
            this$0 = CommonListFinishedDownloadFragment.this;
            super(service, s);
        }
    }


    private class _cls1 extends PlayQueueWithIndex
    {

        final CommonListFinishedDownloadFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Log.d("doss", "playwithIndex FAILURE");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Log.d("doss", "playwithIndex SUCCESS");
            onPlaySuccess();
        }

        _cls1(Service service, String s, int i)
        {
            this$0 = CommonListFinishedDownloadFragment.this;
            super(service, s, i);
        }
    }


    private class _cls2 extends CreateQueueActionCallback
    {

        final CommonListFinishedDownloadFragment this$0;
        final String val$listName;
        final int val$position;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "CreateQueue Failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "CreateQueue Success");
            class _cls1 extends PlayQueueWithIndex
            {

                final _cls2 this$1;

                public void failure(ActionInvocation actioninvocation1, UpnpResponse upnpresponse, String s)
                {
                    Log.d("doss", "playwithIndex FAILURE");
                }

                public void success(ActionInvocation actioninvocation1)
                {
                    Log.d("doss", "playwithIndex SUCCESS");
                    onPlaySuccess();
                }

                _cls1(Service service, String s, int i)
                {
                    this$1 = _cls2.this;
                    super(service, s, i);
                }
            }

            actioninvocation = ActionModel.createModel(new _cls1(((CommonDeviceItem)
// JavaClassFileOutputException: get_constant: invalid tag

        _cls2(String s1, int i)
        {
            this$0 = CommonListFinishedDownloadFragment.this;
            listName = s1;
            position = i;
            super(final_service, final_s);
        }
    }


    private class _cls6 extends BrowseMediaActionCallback
    {

        final CommonListFinishedDownloadFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void received(Result result)
        {
            Logger.d("doss", "broseMedia received");
            onBrowseMediaSuccess(result);
        }

        _cls6(Service service, String s, String s1, String s2, String s3, int i, 
                int j)
        {
            this$0 = CommonListFinishedDownloadFragment.this;
            super(service, s, s1, s2, s3, i, j);
        }
    }

}
