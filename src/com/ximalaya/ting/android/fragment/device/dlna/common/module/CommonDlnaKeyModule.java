// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.dlna.BaseCurrentPlayingModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDlnaKeyModule;
import com.ximalaya.ting.android.util.Logger;
import java.util.List;
import java.util.Map;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.gena.GENASubscription;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.state.StateVariableValue;
import org.teleal.cling.model.types.ServiceType;
import org.teleal.cling.model.types.UnsignedIntegerFourBytes;
import org.teleal.cling.support.avtransport.lastchange.AVTransportLastChangeParser;
import org.teleal.cling.support.lastchange.Event;
import org.teleal.cling.support.lastchange.EventedValue;
import org.teleal.cling.support.lastchange.InstanceID;
import org.teleal.cling.support.lastchange.LastChangeParser;
import org.teleal.cling.support.lastchange.ObjectID;
import org.teleal.cling.support.lastchange.QueueID;
import org.teleal.cling.support.mediamanager.lastchange.MMTransportLastChangeParser;
import org.teleal.cling.support.playqueue.lastchange.PQTransportLastChangeParser;

public class CommonDlnaKeyModule extends BaseDlnaKeyModule
{

    LastChangeParser avTransportParser;
    LastChangeParser mmTransportParser;
    LastChangeParser pqTransportParser;

    public CommonDlnaKeyModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
        pqTransportParser = null;
        avTransportParser = null;
        mmTransportParser = null;
    }

    public KeyEvent parse2Event(GENASubscription genasubscription, BaseDeviceItem basedeviceitem)
    {
        int i;
        boolean flag;
        boolean flag1;
        flag1 = false;
        flag = false;
        Logger.d(TAG, "DossDlnaKeyModule parse2Event IN");
        if (genasubscription.getService().getServiceType().getType().equals("MediaManager") && !genasubscription.getCurrentValues().get("LastChange").toString().contains("ObjectID") && genasubscription.getCurrentValues().get("LastChange").toString().contains("<Action val=\"add\"/>"))
        {
            Logger.d("doss", "\u51C6\u5907\u53D1\u9001Add\u4E8B\u4EF6");
            genasubscription = new KeyEvent(basedeviceitem);
            genasubscription.setEventKey(22);
            return genasubscription;
        }
        genasubscription = preParse(genasubscription);
        if (genasubscription == null)
        {
            return null;
        }
        i = 0;
_L2:
        Object obj;
        Object obj1;
        if (i >= genasubscription.size())
        {
            break MISSING_BLOCK_LABEL_848;
        }
        Logger.d("WIFI", (new StringBuilder()).append("i:").append(i).append(",Eventedname  ").append(((EventedValue)genasubscription.get(i)).getName()).append("   value:  ").append(((EventedValue)genasubscription.get(i)).getValue()).toString());
        obj = ((EventedValue)genasubscription.get(i)).getName();
        obj1 = (EventedValue)genasubscription.get(i);
        if (obj1 != null && ((EventedValue) (obj1)).getValue() != null)
        {
            break; /* Loop/switch isn't completed */
        }
_L4:
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        obj1 = ((EventedValue) (obj1)).getValue().toString();
        if (((String) (obj)).equals("Action") && ((String) (obj1)).equals("task_status"))
        {
            genasubscription = new KeyEvent(basedeviceitem);
            genasubscription.setEventKey(20);
            return genasubscription;
        }
        if (((String) (obj)).equals("Action") && ((String) (obj1)).equals("task_renew"))
        {
            genasubscription = new KeyEvent(basedeviceitem);
            genasubscription.setEventKey(21);
            return genasubscription;
        }
        if (((String) (obj)).equals("Action") && ((String) (obj1)).equals("add"))
        {
            genasubscription = new KeyEvent(basedeviceitem);
            genasubscription.setEventKey(22);
            return genasubscription;
        }
        if (((String) (obj)).equals("CurretPlayListName"))
        {
            (new KeyEvent(basedeviceitem)).setEventKey(17);
            obj = new BaseCurrentPlayingModel();
            int j = ((flag) ? 1 : 0);
            while (j < genasubscription.size()) 
            {
                obj1 = ((EventedValue)genasubscription.get(j)).getName();
                Object obj2 = (EventedValue)genasubscription.get(j);
                if (obj2 != null && ((EventedValue) (obj2)).getValue() != null)
                {
                    obj2 = ((EventedValue) (obj2)).getValue().toString();
                    if (((String) (obj1)).equals("CurrentTrack") || ((String) (obj1)).equals("CurrentIndex"))
                    {
                        ((BaseCurrentPlayingModel) (obj)).setCurrentTrackNum(((String) (obj2)));
                    } else
                    if (((String) (obj1)).equals("CurrentPage"))
                    {
                        ((BaseCurrentPlayingModel) (obj)).setCurrentPage(((String) (obj2)));
                    } else
                    if (((String) (obj1)).equals("CurretPlayListName"))
                    {
                        ((BaseCurrentPlayingModel) (obj)).setCurrentPlayList(((String) (obj2)));
                    }
                }
                j++;
            }
            genasubscription = new KeyEvent(basedeviceitem);
            genasubscription.setEventKey(17);
            genasubscription.setT(obj);
            return genasubscription;
        }
        if (((String) (obj)).equals("PlaybackStorageMedium"))
        {
            genasubscription = new KeyEvent(basedeviceitem);
            genasubscription.setT(obj1);
            genasubscription.setEventKey(24);
            return genasubscription;
        }
        if (!((String) (obj)).equals("TransportState")) goto _L4; else goto _L3
_L3:
        if (((String) (obj1)).equals("PAUSED_PLAYBACK"))
        {
            genasubscription = new KeyEvent(basedeviceitem);
            genasubscription.setEventKey(3);
            return genasubscription;
        }
        if (((String) (obj1)).equals("PLAYING"))
        {
            genasubscription = new KeyEvent(basedeviceitem);
            genasubscription.setEventKey(4);
            return genasubscription;
        }
        if (!((String) (obj1)).equals("TRANSITIONING")) goto _L4; else goto _L5
_L5:
        (new KeyEvent(basedeviceitem)).setEventKey(17);
        BaseCurrentPlayingModel basecurrentplayingmodel = new BaseCurrentPlayingModel();
        int k = ((flag1) ? 1 : 0);
        while (k < genasubscription.size()) 
        {
            String s = ((EventedValue)genasubscription.get(k)).getName();
            Object obj3 = (EventedValue)genasubscription.get(k);
            if (obj3 != null && ((EventedValue) (obj3)).getValue() != null)
            {
                obj3 = ((EventedValue) (obj3)).getValue().toString();
                if (s.equals("CurrentTrack") || s.equals("CurrentIndex"))
                {
                    basecurrentplayingmodel.setCurrentTrackNum(((String) (obj3)));
                } else
                if (s.equals("CurrentPage"))
                {
                    basecurrentplayingmodel.setCurrentPage(((String) (obj3)));
                } else
                if (s.equals("CurretPlayListName"))
                {
                    basecurrentplayingmodel.setCurrentTrackNum(((String) (obj3)));
                }
            }
            k++;
        }
        genasubscription = new KeyEvent(basedeviceitem);
        genasubscription.setEventKey(17);
        genasubscription.setT(basecurrentplayingmodel);
        return genasubscription;
        return null;
    }

    protected List preParse(GENASubscription genasubscription)
    {
        Logger.d(TAG, "DossDlnaKeyModule parse2Event IN");
        Object obj;
        if (genasubscription == null)
        {
            obj = null;
        } else
        {
            obj = genasubscription.getCurrentValues();
            Logger.d("upnp", (new StringBuilder()).append("handlePlayKey1").append(((Map) (obj)).get("LastChange")).toString());
            if (obj == null)
            {
                return null;
            }
            obj = (StateVariableValue)((Map) (obj)).get("LastChange");
            if (obj == null)
            {
                return null;
            }
            if (genasubscription.getService().getServiceType().getType().equals("AVTransport"))
            {
                if (avTransportParser == null)
                {
                    avTransportParser = new AVTransportLastChangeParser();
                }
                try
                {
                    Logger.d(TAG, (new StringBuilder()).append("handlePlayKey2:").append(((StateVariableValue) (obj)).toString()).toString());
                    genasubscription = avTransportParser.parse(((StateVariableValue) (obj)).toString());
                }
                // Misplaced declaration of an exception variable
                catch (GENASubscription genasubscription)
                {
                    genasubscription = null;
                }
                if (genasubscription == null)
                {
                    return null;
                }
                try
                {
                    genasubscription = genasubscription.getInstanceID(new UnsignedIntegerFourBytes(0L)).getValues();
                }
                // Misplaced declaration of an exception variable
                catch (GENASubscription genasubscription)
                {
                    genasubscription = null;
                }
            } else
            if (genasubscription.getService().getServiceType().getType().equals("PlayQueue"))
            {
                if (pqTransportParser == null)
                {
                    pqTransportParser = new PQTransportLastChangeParser();
                }
                try
                {
                    genasubscription = pqTransportParser.parse(((StateVariableValue) (obj)).toString());
                }
                // Misplaced declaration of an exception variable
                catch (GENASubscription genasubscription)
                {
                    genasubscription = null;
                }
                if (genasubscription == null)
                {
                    return null;
                }
                try
                {
                    genasubscription = genasubscription.getQueueID(new UnsignedIntegerFourBytes(0L)).getValues();
                }
                // Misplaced declaration of an exception variable
                catch (GENASubscription genasubscription)
                {
                    genasubscription = null;
                }
            } else
            if (genasubscription.getService().getServiceType().getType().equals("MediaManager"))
            {
                if (mmTransportParser == null)
                {
                    mmTransportParser = new MMTransportLastChangeParser();
                }
                try
                {
                    genasubscription = mmTransportParser.parse(((StateVariableValue) (obj)).toString());
                }
                // Misplaced declaration of an exception variable
                catch (GENASubscription genasubscription)
                {
                    genasubscription = null;
                }
                if (genasubscription == null)
                {
                    return null;
                }
                try
                {
                    genasubscription = genasubscription.getObjectID(new UnsignedIntegerFourBytes(0L)).getValues();
                }
                // Misplaced declaration of an exception variable
                catch (GENASubscription genasubscription)
                {
                    genasubscription = null;
                }
            } else
            {
                genasubscription = null;
            }
            obj = genasubscription;
            if (genasubscription == null)
            {
                return null;
            }
        }
        return ((List) (obj));
    }
}
