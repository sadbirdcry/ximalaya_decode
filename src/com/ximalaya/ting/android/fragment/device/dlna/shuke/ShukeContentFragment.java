// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.content.Intent;
import android.view.View;
import com.ximalaya.ting.android.fragment.device.DeviceBindingListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonContentFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonTFMainFragment;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeTFMainFragment, ShukeSettingFragment

public class ShukeContentFragment extends CommonContentFragment
{

    public ShukeContentFragment()
    {
    }

    protected CommonTFMainFragment getMainFragment()
    {
        return new ShukeTFMainFragment();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        Logger.d("message", "ShukeContentFragment onActivityResult");
        super.onActivityResult(i, j, intent);
        if (mBindFragment != null)
        {
            mBindFragment.onActivityResult(i, j, intent);
        }
    }

    public void onClick(View view)
    {
        boolean flag = false;
        view.getId();
        JVM INSTR tableswitch 2131363612 2131363612: default 24
    //                   2131363612 34;
           goto _L1 _L2
_L1:
        if (!flag)
        {
            super.onClick(view);
        }
        return;
_L2:
        flag = true;
        startFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeSettingFragment, null);
        if (true) goto _L1; else goto _L3
_L3:
    }
}
