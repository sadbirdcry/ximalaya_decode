// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.action.PauseActionCallback;
import com.ximalaya.action.PlayActionCallback;
import com.ximalaya.model.playqueue.ListInfo;
import com.ximalaya.model.playqueue.PlayList;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BasePlayModule;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.sound.AlbumSoundModelNew;
import com.ximalaya.ting.android.util.Logger;
import java.util.List;
import java.util.Timer;
import org.teleal.cling.support.playqueue.callback.backupqueue.BackupQueueConstants;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonSoundListFragment

class this._cls0
    implements android.widget.tener
{

    final CommonSoundListFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
label0:
        {
label1:
            {
                if (mListView.getTag() == CommonSoundListFragment.access$200(CommonSoundListFragment.this))
                {
                    if (DlnaManager.getInstance(mCon).getOperationStroageModel().getCurrentPlayingSoundModel() == null || ((AlbumSoundModelNew)CommonSoundListFragment.access$500(CommonSoundListFragment.this).get(i)).id != DlnaManager.getInstance(mCon).getOperationStroageModel().getCurrentPlayingSoundModel().id)
                    {
                        break label0;
                    }
                    if (!DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying)
                    {
                        break label1;
                    }
                    adapterview = ActionModel.createModel(new PauseActionCallback(CommonSoundListFragment.access$700(CommonSoundListFragment.this).getAVservice()));
                    CommonSoundListFragment.access$800(CommonSoundListFragment.this).pause(adapterview);
                }
                return;
            }
            adapterview = ActionModel.createModel(new PlayActionCallback(CommonSoundListFragment.access$700(CommonSoundListFragment.this).getAVservice()));
            CommonSoundListFragment.access$800(CommonSoundListFragment.this).play(adapterview);
            return;
        }
        int j = i / CommonSoundListFragment.access$300(CommonSoundListFragment.this).pageSize + 1;
        CommonSoundListFragment.access$902(CommonSoundListFragment.this, i % CommonSoundListFragment.access$300(CommonSoundListFragment.this).pageSize + 1);
        CommonSoundListFragment.access$102(CommonSoundListFragment.this, i);
        Logger.d("doss", (new StringBuilder()).append("clickPage:").append(j).toString());
        class _cls1 extends TimerTask
        {

            final CommonSoundListFragment._cls2 this$1;

            public void run()
            {
                CustomToast.showToast(getActivity(), "\u64AD\u653E\u5931\u8D25", 0);
                if (loadingDialog != null)
                {
                    loadingDialog.dismiss();
                    loadingDialog = null;
                }
            }

            _cls1()
            {
                this$1 = CommonSoundListFragment._cls2.this;
                super();
            }
        }

        task = new _cls1();
        if (timer != null)
        {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(task, 30000L);
        class _cls2
            implements Runnable
        {

            final CommonSoundListFragment._cls2 this$1;

            public void run()
            {
                loadingDialog = new MyProgressDialog(getActivity());
                loadingDialog.setTitle("\u63D0\u793A");
                loadingDialog.setMessage("\u6B63\u5728\u51C6\u5907\u64AD\u653E\u6B4C\u66F2");
                loadingDialog.show();
            }

            _cls2()
            {
                this$1 = CommonSoundListFragment._cls2.this;
                super();
            }
        }

        getActivity().runOnUiThread(new _cls2());
        if (j != CommonSoundListFragment.access$000(CommonSoundListFragment.this))
        {
            adapterview = new PlayList();
            adapterview.listName = getPlayListName();
            Logger.d("doss", ((PlayList) (adapterview)).listInfo.searchUrl);
            ((PlayList) (adapterview)).listInfo.sourceName = "ximalaya";
            ((PlayList) (adapterview)).listInfo.currentPage = j;
            ((PlayList) (adapterview)).listInfo.totalPages = CommonSoundListFragment.access$300(CommonSoundListFragment.this).totalCount / 20 + 1;
            ((PlayList) (adapterview)).listInfo.lastPlayIndex = CommonSoundListFragment.access$900(CommonSoundListFragment.this);
            ((PlayList) (adapterview)).listInfo.SwitchPageMode = 1;
            view = (new StringBuilder()).append("http://3rd.ximalaya.com/albums/").append(CommonSoundListFragment.access$1000(CommonSoundListFragment.this).albumId).append("/tracks?").toString();
            view = (new StringBuilder()).append(view).append("i_am=doss").toString();
            view = (new StringBuilder()).append(view).append("&per_page=").append(CommonSoundListFragment.access$300(CommonSoundListFragment.this).pageSize).toString();
            view = (new StringBuilder()).append(view).append("&is_asc=").append(CommonSoundListFragment.access$1100(CommonSoundListFragment.this)).toString();
            view = (new StringBuilder()).append(view).append("&page=").append(j).toString();
            ((PlayList) (adapterview)).listInfo.searchUrl = view;
            adapterview = BackupQueueConstants.getBackupQueueContext(adapterview);
            class _cls3 extends BackUpQueueActionCallback
            {

                final CommonSoundListFragment._cls2 this$1;
                final int val$position;

                public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
                {
                    Logger.d("doss", "CreateQueue Failure");
                    dismissLoadingDialog();
                }

                public void success(ActionInvocation actioninvocation)
                {
                    Logger.d("doss", "CreateQueue Success");
                    CommonSoundListFragment.access$1200(this$0, position);
                }

            _cls3(String s, int i)
            {
                this$1 = CommonSoundListFragment._cls2.this;
                position = i;
                super(final_service, s);
            }
            }

            adapterview = ActionModel.createModel(new _cls3(adapterview, i));
            CommonSoundListFragment.access$800(CommonSoundListFragment.this).createQueue(adapterview);
            CommonSoundListFragment.access$002(CommonSoundListFragment.this, j);
            CommonSoundListFragment.access$002(CommonSoundListFragment.this, j);
            try
            {
                Thread.sleep(50L);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (AdapterView adapterview)
            {
                adapterview.printStackTrace();
            }
            return;
        } else
        {
            CommonSoundListFragment.access$1200(CommonSoundListFragment.this, i);
            return;
        }
    }

    _cls3()
    {
        this$0 = CommonSoundListFragment.this;
        super();
    }
}
