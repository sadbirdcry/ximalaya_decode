// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.module;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import com.ximalaya.audioserver.ContentNode;
import com.ximalaya.audioserver.ContentTree;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.Playlist;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.support.contentdirectory.DIDLParser;
import org.teleal.cling.support.model.DIDLContent;
import org.teleal.cling.support.model.ProtocolInfo;
import org.teleal.cling.support.model.Res;
import org.teleal.cling.support.model.item.MusicTrack;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.module:
//            BaseDlnaModule

public abstract class BaseTuiSongModule extends BaseDlnaModule
{

    public static final String CALL_BACK = "callback";
    public static final String NAME = com/ximalaya/ting/android/fragment/device/dlna/module/BaseTuiSongModule.getSimpleName();
    private int audioId;

    public BaseTuiSongModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    private String getLocalIpAddress()
        throws UnknownHostException
    {
        int i = ((WifiManager)mContext.getSystemService("wifi")).getConnectionInfo().getIpAddress();
        return InetAddress.getByName(String.format("%d.%d.%d.%d", new Object[] {
            Integer.valueOf(i & 0xff), Integer.valueOf(i >> 8 & 0xff), Integer.valueOf(i >> 16 & 0xff), Integer.valueOf(i >> 24 & 0xff)
        })).getHostAddress();
    }

    private String getLocalUrl(String s)
    {
        audioId = audioId + 1;
        String s1 = (new StringBuilder()).append("audio-item-").append(audioId).append("").toString();
        ContentTree.addNode(s1, new ContentNode(s1, s));
        try
        {
            s = (new StringBuilder()).append("http://").append(getLocalIpAddress()).append(":8192/").append(s1).toString();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return null;
        }
        return s;
    }

    private String getSoundSrc()
    {
        String s1 = TingMediaPlayer.getTingMediaPlayer(mContext).getmCurrentAudioSrc();
        String s = s1;
        if (s1 != null)
        {
            s = s1;
            if (!s1.contains("http"))
            {
                s = getLocalUrl(s1);
            }
        }
        return s;
    }

    private List getTuisongUrls()
    {
        Object obj = PlayListControl.getPlayListManager().getPlaylist();
        List list = ((Playlist) (obj)).getData();
        ArrayList arraylist = new ArrayList();
        int i = ((Playlist) (obj)).getCurrPosition();
        if (list != null && list.size() > 0)
        {
            for (int j = 0; i < list.size() && j < 20; i++)
            {
                arraylist.add(((SoundInfo)list.get(i)).playUrl64);
                j++;
            }

        }
        obj = getSoundSrc();
        if (arraylist != null && arraylist.size() > 0 && !TextUtils.isEmpty(((CharSequence) (obj))))
        {
            arraylist.set(0, obj);
        }
        return arraylist;
    }

    public String getModuleName()
    {
        return NAME;
    }

    protected void play(BaseDeviceItem basedeviceitem, ActionModel actionmodel)
    {
        String s = getSoundSrc();
        actionmodel = getTuisongUrls();
        actionmodel = new MusicTrack("0", "0", "ximalaya", "", "", "", new Res[] {
            new Res(new ProtocolInfo("http-get:*:audio/aac:DLNA.ORG_OP=01"), Long.valueOf(0L), s, "2", actionmodel, "1")
        });
        if (actionmodel != null)
        {
            DIDLParser didlparser = new DIDLParser();
            DIDLContent didlcontent = new DIDLContent();
            didlcontent.addItem(actionmodel);
            try
            {
                actionmodel = didlparser.generate(didlcontent);
            }
            // Misplaced declaration of an exception variable
            catch (ActionModel actionmodel)
            {
                actionmodel = null;
            }
        } else
        {
            actionmodel = null;
        }
        basedeviceitem = basedeviceitem.getAVservice();
        getControlPoint().execute(new _cls1(basedeviceitem, s, actionmodel));
        try
        {
            Thread.sleep(50L);
        }
        // Misplaced declaration of an exception variable
        catch (BaseDeviceItem basedeviceitem)
        {
            basedeviceitem.printStackTrace();
        }
        DlnaManager.getInstance(mContext).play();
    }

    public abstract void tuisong(BaseDeviceItem basedeviceitem, ActionModel actionmodel);


    private class _cls1 extends SetAVTransportURIActionCallback
    {

        final BaseTuiSongModule this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        _cls1(Service service, String s, String s1)
        {
            this$0 = BaseTuiSongModule.this;
            super(service, s, s1);
        }
    }

}
