// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            CoreController, BaseDeviceItem

static interface 
{

    public abstract void onDeviceAdded(BaseDeviceItem basedeviceitem);

    public abstract void onDeviceRemoved(BaseDeviceItem basedeviceitem);

    public abstract void onKeyAciton(KeyEvent keyevent);

    public abstract void onNetworkChanged();
}
