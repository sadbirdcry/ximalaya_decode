// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseCurrentPlayingModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.PlayManageController;
import com.ximalaya.ting.android.fragment.device.dlna.common.adapter.CommonSoundsAlbumAdapter;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BasePlayModule;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.sound.AlbumSoundModelNew;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.teleal.cling.model.types.UDN;

public class CommonSoundListFragment extends BaseListFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.device.dlna.DlnaManager.OnkeyEventListener
{
    private class LoadingData
    {

        public boolean clear;
        public boolean loadingNextPage;
        public boolean loadingNextPageAuto;
        public int pageId;
        public int pageSize;
        final CommonSoundListFragment this$0;
        public int totalCount;

        public void reSet()
        {
            totalCount = 0;
            pageId = 1;
            pageSize = 20;
            loadingNextPage = false;
            loadingNextPageAuto = false;
        }

        private LoadingData()
        {
            this$0 = CommonSoundListFragment.this;
            super();
            totalCount = 0;
            pageId = 1;
            pageSize = 20;
            loadingNextPage = false;
            clear = false;
            loadingNextPageAuto = false;
        }

        LoadingData(_cls1 _pcls1)
        {
            this();
        }
    }

    public class WifiControlHandler extends Handler
    {

        final CommonSoundListFragment this$0;

        public void handleMessage(Message message)
        {
            Logger.d("doss", (new StringBuilder()).append("WifiControlHandler:handleMessage:msg:").append(message.what).append(",").append(message.arg1).toString());
            message.what;
            JVM INSTR lookupswitch 4: default 84
        //                       3: 146
        //                       4: 103
        //                       17: 95
        //                       24: 189;
               goto _L1 _L2 _L3 _L4 _L5
_L1:
            mSoundsAdapter.notifyDataSetChanged();
            return;
_L4:
            onSoundChanged(message);
            continue; /* Loop/switch isn't completed */
_L3:
            if (DlnaManager.getInstance(mCon).getOperationStroageModel() != null)
            {
                Logger.d("doss", "DossSoundListFragment \u6536\u5230Playing\u4E8B\u4EF6");
                DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying = true;
            }
            continue; /* Loop/switch isn't completed */
_L2:
            if (DlnaManager.getInstance(mCon).getOperationStroageModel() != null)
            {
                Logger.d("doss", "DossSoundListFragment \u6536\u5230pause\u4E8B\u4EF6");
                DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying = false;
            }
            continue; /* Loop/switch isn't completed */
_L5:
            message = (String)message.obj;
            Logger.d("tuisong", (new StringBuilder()).append("ARG_STORAGEMEDIA_CHANGE:").append(message).toString());
            if (!message.equals("SONGLIST-NETWORK"))
            {
                DlnaManager.getInstance(getActivity().getApplicationContext()).removeOnkeyEventListener(CommonSoundListFragment.this);
                DlnaManager.getInstance(mCon).getOperationStroageModel().clearCurrentPlaying();
                Logger.d("doss", "\u8DF3\u51FA\u5F53\u524Dqueue");
                currentPage = -1;
                mPosition = -1;
            }
            if (true) goto _L1; else goto _L6
_L6:
        }

        protected void onSoundChanged(Message message)
        {
            Logger.d("doss", "DossSoundListFragment \u6536\u5230transitioning\u4E8B\u4EF6");
            message = (BaseCurrentPlayingModel)message.obj;
            if (!TextUtils.isEmpty(message.getCurrentPlayList()) && !message.getCurrentPlayList().equals(getPlayListName()))
            {
                DlnaManager.getInstance(getActivity().getApplicationContext()).removeOnkeyEventListener(CommonSoundListFragment.this);
                DlnaManager.getInstance(mCon).getOperationStroageModel().clearCurrentPlaying();
                Logger.d("doss", "\u8DF3\u51FA\u5F53\u524Dqueue");
                currentPage = -1;
                mPosition = -1;
            } else
            {
                Logger.d("doss", (new StringBuilder()).append("pre mPosition:").append(mPosition).toString());
                Logger.d("doss", (new StringBuilder()).append("currentReceivedTrack:").append(message.getCurrentTrackNum()).toString());
                Logger.d("doss", (new StringBuilder()).append("currentReceivedPage:").append(message.getCurrentPage()).toString());
                Logger.d("doss", (new StringBuilder()).append("pageId:").append(soundData.pageId - 1).toString());
                currentPage = message.getCurrentPage();
                mPosition = message.getCurrentTrackNum() - 1;
                mPosition = soundData.pageSize * (currentPage - 1) + mPosition;
                if (message.getCurrentPage() > soundData.pageId - 1)
                {
                    Logger.d("doss", (new StringBuilder()).append("currentPage:").append(currentPage).toString());
                    Logger.d("doss", (new StringBuilder()).append("pageId:").append(soundData.pageId).toString());
                    soundData.loadingNextPageAuto = true;
                    loadMoreData(mFooterViewLoading);
                    DlnaManager.getInstance(mCon).getOperationStroageModel().clearCurrentPlaying();
                    return;
                }
                Logger.d("doss", (new StringBuilder()).append("post mPosition:").append(mPosition).toString());
                if (mPosition >= 0 && mPosition < soundDataList.size())
                {
                    DlnaManager.getInstance(mCon).getOperationStroageModel().setCurrentPlaying((AlbumSoundModelNew)soundDataList.get(mPosition));
                    return;
                }
            }
        }

        public WifiControlHandler()
        {
            this$0 = CommonSoundListFragment.this;
            super();
        }
    }


    public static final String FROM = "from";
    protected static final String TAG = "doss";
    private AlbumModel album;
    private int currentPage;
    private boolean isAsc;
    protected MyProgressDialog loadingDialog;
    private ImageView mBackImg;
    private TextView mBtnRight;
    private BaseDeviceItem mDeviceItem;
    private BasePlayModule mPlayModule;
    private int mPosition;
    private ProgressDialog mProgressDialog;
    private CommonSoundsAlbumAdapter mSoundsAdapter;
    private TextView mToptv;
    WifiControlHandler mWifiControlHandler;
    private int realPosition;
    private LoadingData soundData;
    private List soundDataList;
    protected TimerTask task;
    protected Timer timer;

    public CommonSoundListFragment()
    {
        currentPage = -1;
        isAsc = true;
        album = new AlbumModel();
        mWifiControlHandler = new WifiControlHandler();
    }

    private void initData()
    {
        isAsc = true;
        soundData = new LoadingData(null);
        soundDataList = new ArrayList();
        mSoundsAdapter = new CommonSoundsAlbumAdapter(getActivity(), soundDataList, getPlayListName());
        mListView.setAdapter(mSoundsAdapter);
        mListView.setTag(mSoundsAdapter);
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        loadSoundListData(fragmentBaseContainerView);
    }

    private void initListener()
    {
        mFooterViewLoading.setOnClickListener(new _cls1());
        mListView.setOnItemClickListener(new _cls2());
    }

    private void initUi()
    {
        mToptv = (TextView)(TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mBtnRight = (TextView)fragmentBaseContainerView.findViewById(0x7f0a071c);
        mBtnRight.setVisibility(8);
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(this);
    }

    private void loadMoreData(View view)
    {
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        if (mListView.getTag() == mSoundsAdapter)
        {
            loadSoundListData(view);
        }
    }

    private void loadSoundListData(final View view)
    {
        if (!soundData.loadingNextPage)
        {
            if (ToolUtil.isConnectToNetwork(mCon))
            {
                mDataLoadTask = (new _cls4()).myexec(new Void[0]);
                return;
            }
            if (mListView.getTag() == mSoundsAdapter)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E");
                return;
            }
        }
    }

    private boolean moreSoundDataAvailable(LoadingData loadingdata)
    {
        return loadingdata.pageId * loadingdata.pageSize < loadingdata.totalCount;
    }

    private void playSound(int i)
    {
        DlnaManager.getInstance(getActivity().getApplicationContext()).setOnkeyEventListener(this);
        ActionModel actionmodel = ActionModel.createModel(new _cls3(getPlayListName(), realPosition, i));
        mPlayModule.playWithIndex(actionmodel);
    }

    private void showSelectFooterView()
    {
        if (mListView.getTag() == mSoundsAdapter)
        {
            if (moreSoundDataAvailable(soundData))
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.MORE);
                return;
            }
            if (soundDataList == null || soundDataList.size() == 0)
            {
                setFooterViewText("\u8BE5\u4E13\u8F91\u58F0\u97F3\u6570\u4E3A0");
                return;
            } else
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
        } else
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            return;
        }
    }

    private void unRegisterListener()
    {
        if (LocalMediaService.getInstance() == null);
    }

    protected void dismissLoadingDialog()
    {
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
        if (timer != null)
        {
            timer.cancel();
            timer = null;
        }
    }

    protected String getPlayListName()
    {
        return album.title;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mDeviceItem = DlnaManager.getInstance(getActivity().getApplicationContext()).getOperationStroageModel().getNowDeviceItem();
        mPlayModule = (BasePlayModule)DlnaManager.getInstance(getActivity().getApplicationContext()).getController().getModule(BasePlayModule.NAME);
        initUi();
        bundle = getArguments();
        if (bundle != null)
        {
            bundle = bundle.getString("album");
            if (!Utilities.isBlank(bundle))
            {
                album = (AlbumModel)JSON.parseObject(bundle, com/ximalaya/ting/android/model/album/AlbumModel);
            }
        }
        mToptv.setText(album.title);
        if (mDeviceItem != null)
        {
            (new Thread(new _cls5())).start();
        }
        initData();
        initListener();
    }

    public boolean onBackPressed()
    {
        return super.onBackPressed();
    }

    public void onClick(View view)
    {
        if (!isAdded())
        {
            return;
        }
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300b1, viewgroup, false);
        mListView = (ListView)findViewById(0x7f0a005c);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }
        unRegisterListener();
    }

    public void onKeyAciton(KeyEvent keyevent)
    {
        while (mWifiControlHandler == null || !keyevent.getDeviceItem().getUdn().equals(mDeviceItem.getUdn())) 
        {
            return;
        }
        Message message = new Message();
        message.what = keyevent.getEventKey();
        message.obj = keyevent.getT();
        mWifiControlHandler.sendMessage(message);
    }

    public void onResume()
    {
        super.onResume();
        DlnaManager.getInstance(getActivity().getApplicationContext()).setOnkeyEventListener(this);
    }

    public void onStop()
    {
        super.onStop();
        DlnaManager.getInstance(getActivity().getApplicationContext()).removeOnkeyEventListener(this);
        DlnaManager.getInstance(getActivity().getApplicationContext()).getPlayManageController().change2Bendi();
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }



/*
    static int access$002(CommonSoundListFragment commonsoundlistfragment, int i)
    {
        commonsoundlistfragment.currentPage = i;
        return i;
    }

*/




/*
    static int access$102(CommonSoundListFragment commonsoundlistfragment, int i)
    {
        commonsoundlistfragment.mPosition = i;
        return i;
    }

*/












/*
    static int access$902(CommonSoundListFragment commonsoundlistfragment, int i)
    {
        commonsoundlistfragment.realPosition = i;
        return i;
    }

*/

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final CommonSoundListFragment this$0;

        public void onClick(View view)
        {
            loadMoreData(mFooterViewLoading);
        }

        _cls1()
        {
            this$0 = CommonSoundListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CommonSoundListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
label0:
            {
label1:
                {
                    if (mListView.getTag() == mSoundsAdapter)
                    {
                        if (DlnaManager.getInstance(mCon).getOperationStroageModel().getCurrentPlayingSoundModel() == null || ((AlbumSoundModelNew)soundDataList.get(i)).id != DlnaManager.getInstance(mCon).getOperationStroageModel().getCurrentPlayingSoundModel().id)
                        {
                            break label0;
                        }
                        if (!DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying)
                        {
                            break label1;
                        }
                        adapterview = ActionModel.createModel(new PauseActionCallback(mDeviceItem.getAVservice()));
                        mPlayModule.pause(adapterview);
                    }
                    return;
                }
                adapterview = ActionModel.createModel(new PlayActionCallback(mDeviceItem.getAVservice()));
                mPlayModule.play(adapterview);
                return;
            }
            int j = i / soundData.pageSize + 1;
            realPosition = i % soundData.pageSize + 1;
            mPosition = i;
            Logger.d("doss", (new StringBuilder()).append("clickPage:").append(j).toString());
            class _cls1 extends TimerTask
            {

                final _cls2 this$1;

                public void run()
                {
                    CustomToast.showToast(getActivity(), "\u64AD\u653E\u5931\u8D25", 0);
                    if (loadingDialog != null)
                    {
                        loadingDialog.dismiss();
                        loadingDialog = null;
                    }
                }

                _cls1()
                {
                    this$1 = _cls2.this;
                    super();
                }
            }

            task = new _cls1();
            if (timer != null)
            {
                timer.cancel();
            }
            timer = new Timer();
            timer.schedule(task, 30000L);
            class _cls2
                implements Runnable
            {

                final _cls2 this$1;

                public void run()
                {
                    loadingDialog = new MyProgressDialog(getActivity());
                    loadingDialog.setTitle("\u63D0\u793A");
                    loadingDialog.setMessage("\u6B63\u5728\u51C6\u5907\u64AD\u653E\u6B4C\u66F2");
                    loadingDialog.show();
                }

                _cls2()
                {
                    this$1 = _cls2.this;
                    super();
                }
            }

            getActivity().runOnUiThread(new _cls2());
            if (j != currentPage)
            {
                adapterview = new PlayList();
                adapterview.listName = getPlayListName();
                Logger.d("doss", ((PlayList) (adapterview)).listInfo.searchUrl);
                ((PlayList) (adapterview)).listInfo.sourceName = "ximalaya";
                ((PlayList) (adapterview)).listInfo.currentPage = j;
                ((PlayList) (adapterview)).listInfo.totalPages = soundData.totalCount / 20 + 1;
                ((PlayList) (adapterview)).listInfo.lastPlayIndex = realPosition;
                ((PlayList) (adapterview)).listInfo.SwitchPageMode = 1;
                view = (new StringBuilder()).append("http://3rd.ximalaya.com/albums/").append(album.albumId).append("/tracks?").toString();
                view = (new StringBuilder()).append(view).append("i_am=doss").toString();
                view = (new StringBuilder()).append(view).append("&per_page=").append(soundData.pageSize).toString();
                view = (new StringBuilder()).append(view).append("&is_asc=").append(isAsc).toString();
                view = (new StringBuilder()).append(view).append("&page=").append(j).toString();
                ((PlayList) (adapterview)).listInfo.searchUrl = view;
                adapterview = BackupQueueConstants.getBackupQueueContext(adapterview);
                class _cls3 extends BackUpQueueActionCallback
                {

                    final _cls2 this$1;
                    final int val$position;

                    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
                    {
                        Logger.d("doss", "CreateQueue Failure");
                        dismissLoadingDialog();
                    }

                    public void success(ActionInvocation actioninvocation)
                    {
                        Logger.d("doss", "CreateQueue Success");
                        playSound(position);
                    }

                _cls3(String s, int i)
                {
                    this$1 = _cls2.this;
                    position = i;
                    super(final_service, s);
                }
                }

                adapterview = ActionModel.createModel(new _cls3(adapterview, i));
                mPlayModule.createQueue(adapterview);
                currentPage = j;
                currentPage = j;
                try
                {
                    Thread.sleep(50L);
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (AdapterView adapterview)
                {
                    adapterview.printStackTrace();
                }
                return;
            } else
            {
                playSound(i);
                return;
            }
        }

        _cls2()
        {
            this$0 = CommonSoundListFragment.this;
            super();
        }
    }


    private class _cls4 extends MyAsyncTask
    {

        final CommonSoundListFragment this$0;
        final View val$view;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            Object obj;
            List list;
            Object obj1;
            avoid = new RequestParams();
            obj = (new StringBuilder()).append("http://3rd.ximalaya.com/albums/").append(album.albumId).append("/tracks").toString();
            avoid.put("page", (new StringBuilder()).append(soundData.pageId).append("").toString());
            avoid.put("i_am", "doss");
            avoid.put("is_asc", (new StringBuilder()).append(isAsc).append("").toString());
            avoid.put("per_page", (new StringBuilder()).append(soundData.pageSize).append("").toString());
            obj1 = f.a().a(((String) (obj)), avoid, view, fragmentBaseContainerView);
            Logger.log((new StringBuilder()).append("result:").append(((String) (obj1))).toString());
            avoid = null;
            list = null;
            obj = avoid;
            obj1 = JSON.parseObject(((String) (obj1)));
            obj = avoid;
            if (!"0".equals(((JSONObject) (obj1)).get("ret").toString()))
            {
                break MISSING_BLOCK_LABEL_304;
            }
            obj = avoid;
            avoid = JSON.parseArray(JSON.parseObject(((JSONObject) (obj1)).get("album").toString()).getString("tracks"), com/ximalaya/ting/android/model/sound/AlbumSoundModelNew);
            if (avoid != null)
            {
                break MISSING_BLOCK_LABEL_266;
            }
            obj = new ArrayList();
            avoid = ((Void []) (obj));
            list = avoid;
            obj = avoid;
            if (soundData.totalCount != 0)
            {
                break MISSING_BLOCK_LABEL_304;
            }
            obj = avoid;
            soundData.totalCount = ((JSONObject) (obj1)).getIntValue("total_count");
            list = avoid;
            return list;
            avoid;
_L2:
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
            return ((List) (obj));
            Exception exception;
            exception;
            obj = avoid;
            avoid = exception;
            if (true) goto _L2; else goto _L1
_L1:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (!isAdded())
            {
                return;
            }
            soundData.loadingNextPage = false;
            if (list == null)
            {
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E");
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            if (soundData.pageId != 1) goto _L2; else goto _L1
_L1:
            soundDataList.clear();
_L4:
            soundDataList.addAll(list);
            if (soundData.loadingNextPageAuto)
            {
                Logger.d("doss", (new StringBuilder()).append("\u66F4\u65B0\u6570\u636E\u5B8C\u6210:").append(mPosition).toString());
                soundData.loadingNextPageAuto = false;
                DlnaManager.getInstance(mCon).getOperationStroageModel().setCurrentPlaying((AlbumSoundModelNew)soundDataList.get(mPosition));
            }
            mSoundsAdapter.notifyDataSetChanged();
            showSelectFooterView();
            list = soundData;
            list.pageId = ((LoadingData) (list)).pageId + 1;
            return;
_L2:
            if (soundData.clear)
            {
                soundDataList.clear();
                soundData.clear = false;
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        protected void onPreExecute()
        {
            soundData.loadingNextPage = true;
        }

        _cls4()
        {
            this$0 = CommonSoundListFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls3 extends PlayQueueWithIndex
    {

        final CommonSoundListFragment this$0;
        final int val$position;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Log.d("doss", "playwithIndex FAILURE");
            dismissLoadingDialog();
        }

        public void success(ActionInvocation actioninvocation)
        {
            Log.d("doss", "playwithIndex SUCCESS");
            dismissLoadingDialog();
            DlnaManager.getInstance(mCon).getOperationStroageModel().setCurrentPlaying(mSoundsAdapter.getItem(position));
        }

        _cls3(String s, int i, int j)
        {
            this$0 = CommonSoundListFragment.this;
            position = j;
            super(final_service, s, i);
        }
    }


    private class _cls5
        implements Runnable
    {

        final CommonSoundListFragment this$0;

        public void run()
        {
            DlnaManager.getInstance(getActivity().getApplicationContext()).getPlayManageController().tuisongDevice(mDeviceItem);
        }

        _cls5()
        {
            this$0 = CommonSoundListFragment.this;
            super();
        }
    }

}
