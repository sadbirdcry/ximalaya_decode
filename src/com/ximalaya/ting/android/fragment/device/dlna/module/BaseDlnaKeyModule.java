// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.gena.GENASubscription;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.module:
//            BaseDlnaModule, IDlnaKeyModule

public abstract class BaseDlnaKeyModule extends BaseDlnaModule
    implements IDlnaKeyModule
{

    public static final String NAME = com/ximalaya/ting/android/fragment/device/dlna/module/BaseDlnaKeyModule.getSimpleName();
    protected final String TAG = com/ximalaya/ting/android/fragment/device/dlna/module/BaseDlnaKeyModule.getSimpleName();

    public BaseDlnaKeyModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public final String getModuleName()
    {
        return NAME;
    }

    public abstract KeyEvent parse2Event(GENASubscription genasubscription, BaseDeviceItem basedeviceitem);

}
