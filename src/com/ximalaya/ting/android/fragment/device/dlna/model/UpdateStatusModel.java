// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.model;


public class UpdateStatusModel
{

    private int count;
    private long csize;
    private int dcount;
    private long dsize;
    private String name;
    private String new_ver;
    private int progress;
    private int status;
    private long tsize;
    private String ver;
    private long wsize;

    public UpdateStatusModel()
    {
    }

    public int getCount()
    {
        return count;
    }

    public long getCsize()
    {
        return csize;
    }

    public int getDcount()
    {
        return dcount;
    }

    public long getDsize()
    {
        return dsize;
    }

    public String getName()
    {
        return name;
    }

    public String getNew_ver()
    {
        return new_ver;
    }

    public int getProgress()
    {
        return progress;
    }

    public int getStatus()
    {
        return status;
    }

    public long getTsize()
    {
        return tsize;
    }

    public String getVer()
    {
        return ver;
    }

    public long getWsize()
    {
        return wsize;
    }

    public void setCount(int i)
    {
        count = i;
    }

    public void setCsize(long l)
    {
        csize = l;
    }

    public void setDcount(int i)
    {
        dcount = i;
    }

    public void setDsize(long l)
    {
        dsize = l;
    }

    public void setName(String s)
    {
        name = s;
    }

    public void setNew_ver(String s)
    {
        new_ver = s;
    }

    public void setProgress(int i)
    {
        progress = i;
    }

    public void setStatus(int i)
    {
        status = i;
    }

    public void setTsize(long l)
    {
        tsize = l;
    }

    public void setVer(String s)
    {
        ver = s;
    }

    public void setWsize(long l)
    {
        wsize = l;
    }
}
