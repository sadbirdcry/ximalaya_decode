// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeItemBindableModel

public class ShukeDeviceItem extends CommonDeviceItem
{

    private boolean hasVoiceOfChildren;
    private boolean hasVoiceOfMother;
    private long mumID;
    private long sonID;
    private String token;

    public ShukeDeviceItem(BaseDeviceItem basedeviceitem)
    {
        super(basedeviceitem);
        hasVoiceOfMother = false;
        hasVoiceOfChildren = false;
    }

    public BaseItemBindableModel getBaseBindableModel()
    {
        if (mBaseBindableModel == null)
        {
            mBaseBindableModel = new ShukeItemBindableModel();
        }
        return mBaseBindableModel;
    }

    public long getMumID()
    {
        return mumID;
    }

    public long getSonID()
    {
        return sonID;
    }

    public String getToken()
    {
        return token;
    }

    public boolean hasVoiceOfChildren()
    {
        return hasVoiceOfChildren;
    }

    public boolean hasVoiceOfMother()
    {
        return hasVoiceOfMother;
    }

    public void setHasVoiceOfChildren(boolean flag)
    {
        hasVoiceOfChildren = flag;
    }

    public void setHasVoiceOfMother(boolean flag)
    {
        hasVoiceOfMother = flag;
    }

    public void setMumID(long l)
    {
        mumID = l;
    }

    public void setSonID(long l)
    {
        sonID = l;
    }

    public void setToken(String s)
    {
        token = s;
    }
}
