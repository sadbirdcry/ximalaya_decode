// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.support.v4.app.FragmentActivity;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDownloadModule;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonListUnFinishedTaskFragment

class this._cls1
    implements Runnable
{

    final is._cls0 this$1;

    public void run()
    {
        class _cls1
            implements Runnable
        {

            final CommonListUnFinishedTaskFragment._cls5._cls1 this$2;

            public void run()
            {
                CommonListUnFinishedTaskFragment.access$202(this$0, new MyProgressDialog(getActivity()));
                CommonListUnFinishedTaskFragment.access$300(this$0).setTitle("\u63D0\u793A");
                CommonListUnFinishedTaskFragment.access$400(this$0).setMessage("\u6B63\u5728\u6E05\u7A7A\u5217\u8868");
                CommonListUnFinishedTaskFragment.access$500(this$0).show();
            }

            _cls1()
            {
                this$2 = CommonListUnFinishedTaskFragment._cls5._cls1.this;
                super();
            }
        }

        getActivity().runOnUiThread(new _cls1());
        class _cls2 extends DeleteDownloadTaskActionCallback
        {

            final CommonListUnFinishedTaskFragment._cls5._cls1 this$2;

            public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
            {
                dismissLoadingDialog();
                Logger.d("doss", "deleteDownloadTask failure");
            }

            public void success(ActionInvocation actioninvocation)
            {
                dismissLoadingDialog();
                Logger.d("doss", "deleteDownloadTask SUCCESS");
            }

            _cls2(Service service, String s, boolean flag)
            {
                this$2 = CommonListUnFinishedTaskFragment._cls5._cls1.this;
                super(service, s, flag);
            }
        }

        ActionModel actionmodel = ActionModel.createModel(new _cls2(((CommonDeviceItem)CommonListUnFinishedTaskFragment.access$600(_fld0)).getMMservice(), "UnfinishedTasks", true));
        CommonListUnFinishedTaskFragment.access$700(_fld0).deleteDownloadTask(actionmodel);
    }

    _cls2()
    {
        this$1 = this._cls1.this;
        super();
    }
}
