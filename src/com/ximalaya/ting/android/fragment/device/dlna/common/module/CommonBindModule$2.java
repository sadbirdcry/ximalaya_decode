// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import com.ximalaya.action.BackUpQueueActionCallback;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BindCommandModel;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.module:
//            CommonBindModule

class val.mBindCommand extends BackUpQueueActionCallback
{

    final CommonBindModule this$0;
    final BindCommandModel val$mBindCommand;
    final CommonDeviceItem val$mDeviceItem;
    final AlbumModel val$model;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        CommonBindModule.access$000(CommonBindModule.this, "\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
        Logger.d("doss", "BackUpQueue Failure");
    }

    public void success(ActionInvocation actioninvocation)
    {
        Logger.d("doss", "BackUpQueue Success");
        class _cls1 extends GetKeyMappingActionCallback
        {

            final CommonBindModule._cls2 this$1;

            public void failure(ActionInvocation actioninvocation1, UpnpResponse upnpresponse, String s)
            {
                CommonBindModule.access$000(this$0, "\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
                Logger.d("doss", "GetKeyMappingActionCallback Failure");
            }

            public void received(ActionInvocation actioninvocation1, List list)
            {
                Logger.d("doss", "GetKeyMappingActionCallback Success");
                actioninvocation1 = new Key();
                actioninvocation1.name = model.title;
                list.set(mBindCommand.mChannelId, actioninvocation1);
                actioninvocation1 = KeyMappingQueueConstants.getSetKeyMappingQueueContext(list);
                class _cls1 extends SetKeyMappingActionCallback
                {

                    final _cls1 this$2;

                    public void failure(ActionInvocation actioninvocation2, UpnpResponse upnpresponse, String s)
                    {
                        CommonBindModule.access$000(this$0, "\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
                        Logger.d(CommonBindModule.access$200(this$0), "SetKeyMapping failure");
                    }

                    public void success(ActionInvocation actioninvocation2)
                    {
                        class _cls1 extends TimerTask
                        {

                            final _cls1 this$3;

                            public void run()
                            {
                                CommonBindModule.access$000(this$0, "\u7ED1\u5B9A\u4E13\u8F91\u6210\u529F");
                                Logger.d(CommonBindModule.access$100(this$0), "SetKeyMapping Success");
                                mDeviceItem.getBaseBindableModel().setAlbums(mBindCommand.mChannelId, mBindCommand.mAlbumModel);
                                freshBindFragment();
                            }

                                _cls1()
                                {
                                    this$3 = _cls1.this;
                                    super();
                                }
                        }

                        (new Timer()).schedule(new _cls1(), 500L);
                    }

                        _cls1(Service service, String s)
                        {
                            this$2 = _cls1.this;
                            super(service, s);
                        }
                }

                getControlPoint().execute(new _cls1(mDeviceItem.getPQservice(), actioninvocation1));
            }

            _cls1(Service service)
            {
                this$1 = CommonBindModule._cls2.this;
                super(service);
            }
        }

        getControlPoint().execute(new _cls1(val$mDeviceItem.getPQservice()));
    }

    _cls1(AlbumModel albummodel, BindCommandModel bindcommandmodel)
    {
        this$0 = final_commonbindmodule;
        val$mDeviceItem = CommonDeviceItem.this;
        val$model = albummodel;
        val$mBindCommand = bindcommandmodel;
        super(final_service, final_s);
    }
}
