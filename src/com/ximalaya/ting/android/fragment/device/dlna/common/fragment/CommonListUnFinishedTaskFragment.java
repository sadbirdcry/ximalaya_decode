// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.view.View;
import com.ximalaya.model.mediamanager.Task;
import com.ximalaya.model.mediamanager.TaskList;
import com.ximalaya.ting.android.fragment.device.BaseTaskListFragment;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDownloadModule;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import java.util.List;

public class CommonListUnFinishedTaskFragment extends BaseTaskListFragment
    implements android.view.View.OnClickListener
{

    private final String TAG = com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonListUnFinishedTaskFragment.getSimpleName();

    public CommonListUnFinishedTaskFragment()
    {
    }

    protected void deleteAllTask()
    {
        (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u6E05\u7A7A\u6240\u6709\u6B63\u5728\u4E0B\u8F7D\u7684\u4EFB\u52A1\u5417\uFF1F").setOkBtn(new _cls5()).showConfirm();
    }

    protected void dismissLoadingDialog()
    {
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
    }

    protected void loadMoreData()
    {
        super.loadMoreData();
        ActionModel actionmodel = ActionModel.createModel(new _cls1(((CommonDeviceItem)mDeviceItem).getMMservice(), "UnfinishedTasks", loadingData.pageSize, loadingData.pageSize));
        mDownloadModule.browseDownloadTask(actionmodel);
    }

    public void onClick(View view)
    {
        view.getId();
        super.onClick(view);
    }

    protected void pauseAllTask()
    {
        for (int i = 0; i < taskItems.size(); i++)
        {
            Object obj = (Task)taskItems.get(i);
            obj = ActionModel.createModel(new _cls4(((CommonDeviceItem)mDeviceItem).getMMservice(), ((Task) (obj)).taskID));
            mDownloadModule.pauseDownloadTask(((ActionModel) (obj)));
        }

    }

    protected void pauseTask(int i)
    {
        Object obj = (Task)taskItems.get(i);
        obj = ActionModel.createModel(new _cls7(((CommonDeviceItem)mDeviceItem).getMMservice(), ((Task) (obj)).taskID));
        mDownloadModule.pauseDownloadTask(((ActionModel) (obj)));
    }

    public void refreshDownloadTask()
    {
        ActionModel actionmodel = ActionModel.createModel(new _cls2(((CommonDeviceItem)mDeviceItem).getMMservice(), "UnfinishedTasks", loadingData.pageSize, loadingData.pageSize));
        mDownloadModule.browseDownloadTask(actionmodel);
    }

    protected void resumeTask(int i)
    {
        Object obj = (Task)taskItems.get(i);
        obj = ActionModel.createModel(new _cls6(((CommonDeviceItem)mDeviceItem).getMMservice(), ((Task) (obj)).taskID));
        mDownloadModule.resumeDownloadTask(((ActionModel) (obj)));
    }

    protected void startAllTask()
    {
        for (int i = 0; i < taskItems.size(); i++)
        {
            Object obj = (Task)taskItems.get(i);
            obj = ActionModel.createModel(new _cls3(((CommonDeviceItem)mDeviceItem).getMMservice(), ((Task) (obj)).taskID));
            mDownloadModule.resumeDownloadTask(((ActionModel) (obj)));
        }

    }




/*
    static MyProgressDialog access$202(CommonListUnFinishedTaskFragment commonlistunfinishedtaskfragment, MyProgressDialog myprogressdialog)
    {
        commonlistunfinishedtaskfragment.loadingDialog = myprogressdialog;
        return myprogressdialog;
    }

*/






    private class _cls5
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final CommonListUnFinishedTaskFragment this$0;

        public void onExecute()
        {
            class _cls1
                implements Runnable
            {

                final _cls5 this$1;

                public void run()
                {
                    class _cls1
                        implements Runnable
                    {

                        final _cls1 this$2;

                        public void run()
                        {
                            
// JavaClassFileOutputException: get_constant: invalid tag

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    getActivity().runOnUiThread(new _cls1());
                    class _cls2 extends DeleteDownloadTaskActionCallback
                    {

                        final _cls1 this$2;

                        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
                        {
                            dismissLoadingDialog();
                            Logger.d("doss", "deleteDownloadTask failure");
                        }

                        public void success(ActionInvocation actioninvocation)
                        {
                            dismissLoadingDialog();
                            Logger.d("doss", "deleteDownloadTask SUCCESS");
                        }

                            _cls2(Service service, String s, boolean flag)
                            {
                                this$2 = _cls1.this;
                                super(service, s, flag);
                            }
                    }

                    ActionModel actionmodel = ActionModel.createModel(new _cls2(((CommonDeviceItem)
// JavaClassFileOutputException: get_constant: invalid tag

                _cls1()
                {
                    this$1 = _cls5.this;
                    super();
                }
            }

            (new Thread(new _cls1())).start();
        }

        _cls5()
        {
            this$0 = CommonListUnFinishedTaskFragment.this;
            super();
        }
    }


    private class _cls1 extends BrowseDownloadTaskActionCallback
    {

        final CommonListUnFinishedTaskFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void received(String s, TaskList tasklist)
        {
            Logger.d("doss", "broseMedia received");
            onBrowseDownloadTaskSuccess(tasklist);
        }

        _cls1(Service service, String s, int i, int j)
        {
            this$0 = CommonListUnFinishedTaskFragment.this;
            super(service, s, i, j);
        }
    }


    private class _cls4 extends PauseDownloadTaskActionCallback
    {

        final CommonListUnFinishedTaskFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "resumeDownloadTask failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "resumeDownloadTask SUCCESS");
        }

        _cls4(Service service, String s)
        {
            this$0 = CommonListUnFinishedTaskFragment.this;
            super(service, s);
        }
    }


    private class _cls7 extends PauseDownloadTaskActionCallback
    {

        final CommonListUnFinishedTaskFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "resumeDownloadTask failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "resumeDownloadTask SUCCESS");
        }

        _cls7(Service service, String s)
        {
            this$0 = CommonListUnFinishedTaskFragment.this;
            super(service, s);
        }
    }


    private class _cls2 extends BrowseDownloadTaskActionCallback
    {

        final CommonListUnFinishedTaskFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void received(String s, TaskList tasklist)
        {
            Logger.d("doss", "broseMedia received");
            onBrowseDownloadTaskSuccess(tasklist);
        }

        _cls2(Service service, String s, int i, int j)
        {
            this$0 = CommonListUnFinishedTaskFragment.this;
            super(service, s, i, j);
        }
    }


    private class _cls6 extends ResumeDownloadTaskActionCallback
    {

        final CommonListUnFinishedTaskFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "resumeDownloadTask failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "resumeDownloadTask SUCCESS");
        }

        _cls6(Service service, String s)
        {
            this$0 = CommonListUnFinishedTaskFragment.this;
            super(service, s);
        }
    }


    private class _cls3 extends ResumeDownloadTaskActionCallback
    {

        final CommonListUnFinishedTaskFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "resumeDownloadTask failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "resumeDownloadTask SUCCESS");
        }

        _cls3(Service service, String s)
        {
            this$0 = CommonListUnFinishedTaskFragment.this;
            super(service, s);
        }
    }

}
