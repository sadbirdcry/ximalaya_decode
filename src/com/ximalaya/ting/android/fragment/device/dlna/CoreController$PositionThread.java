// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.dlna.model.LinkedDeviceModel;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            CoreController, DlnaManager, BaseDeviceItem

static class mFragment extends Thread
{

    private boolean isGetPosition;
    private Context mContext;
    private PlayerFragment mFragment;
    private com.ximalaya.service.er upnpService;

    public void cancel()
    {
        isGetPosition = false;
    }

    public void run()
    {
        if (DlnaManager.getInstance(mContext).isLinkedDeviceValid())
        {
            while (isGetPosition) 
            {
                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
                if (!DlnaManager.getInstance(mContext).isLinkedDeviceValid())
                {
                    return;
                }
                if (DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem().mAVservice != null && upnpService != null)
                {
                    upnpService.getControlPoint().execute(new k(DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem().mAVservice, mContext, mFragment));
                }
            }
        }
_L2:
        return;
        if (true) goto _L2; else goto _L1
_L1:
    }


    public k(Context context, com.ximalaya.service.er er, PlayerFragment playerfragment)
    {
        isGetPosition = true;
        mContext = context;
        upnpService = er;
        mFragment = playerfragment;
    }
}
