// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.os.Bundle;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonListOtherDirFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonTFMainFragment;
import com.ximalaya.ting.android.fragment.device.dlna.model.Directory;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeController, ShukeTFListSubMainFragment

public class ShukeTFMainFragment extends CommonTFMainFragment
{

    public ShukeTFMainFragment()
    {
    }

    protected void initData()
    {
        super.initData();
        ShukeController shukecontroller = (ShukeController)DlnaManager.getInstance(mCon).getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi);
        if (shukecontroller != null)
        {
            int i = 0;
            while (i < shukecontroller.getDirectoryNum()) 
            {
                mDirectionName.add(shukecontroller.getDirectory(i).getName());
                i++;
            }
        }
    }

    protected boolean toTFListFragment(String s)
    {
        if (!super.toTFListFragment(s))
        {
            if (s.equals("\u559C\u9A6C\u62C9\u96C5\u7684\u4E0B\u8F7D"))
            {
                Bundle bundle = new Bundle();
                bundle.putString("directionName", s);
                startFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeTFListSubMainFragment, bundle);
                return true;
            } else
            {
                Bundle bundle1 = new Bundle();
                bundle1.putString("directionName", s);
                startFragment(com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonListOtherDirFragment, bundle1);
                return true;
            }
        } else
        {
            return false;
        }
    }
}
