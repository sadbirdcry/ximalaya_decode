// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.tingshubao;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDlnaKeyModule;
import com.ximalaya.ting.android.util.Logger;
import java.util.List;
import java.util.Map;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.gena.GENASubscription;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.state.StateVariableValue;
import org.teleal.cling.model.types.ServiceType;
import org.teleal.cling.model.types.UnsignedIntegerFourBytes;
import org.teleal.cling.support.avtransport.lastchange.AVTransportLastChangeParser;
import org.teleal.cling.support.lastchange.Event;
import org.teleal.cling.support.lastchange.EventedValue;
import org.teleal.cling.support.lastchange.InstanceID;
import org.teleal.cling.support.lastchange.LastChangeParser;

public class TingshuKeyModule extends BaseDlnaKeyModule
{

    private LastChangeParser avTransportParser;

    public TingshuKeyModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public KeyEvent parse2Event(GENASubscription genasubscription, BaseDeviceItem basedeviceitem)
    {
        Logger.d("upnp", "handlePlayKey2 IN");
        basedeviceitem = new KeyEvent(basedeviceitem);
        if (genasubscription == null)
        {
            return basedeviceitem;
        }
        Object obj = genasubscription.getCurrentValues();
        Logger.d("upnp", (new StringBuilder()).append("handlePlayKey1").append(((Map) (obj)).get("LastChange")).toString());
        if (obj == null)
        {
            return basedeviceitem;
        }
        obj = (StateVariableValue)((Map) (obj)).get("LastChange");
        if (obj == null)
        {
            return basedeviceitem;
        }
        int j;
        int k;
        if (genasubscription.getService().getServiceType().getType().equals("AVTransport"))
        {
            if (avTransportParser == null)
            {
                avTransportParser = new AVTransportLastChangeParser();
            }
            try
            {
                Logger.d(TAG, (new StringBuilder()).append("handlePlayKey2:").append(((StateVariableValue) (obj)).toString()).toString());
                genasubscription = avTransportParser.parse(((StateVariableValue) (obj)).toString());
            }
            // Misplaced declaration of an exception variable
            catch (GENASubscription genasubscription)
            {
                genasubscription = null;
            }
            if (genasubscription == null)
            {
                return basedeviceitem;
            }
            try
            {
                genasubscription = genasubscription.getInstanceID(new UnsignedIntegerFourBytes(0L)).getValues();
            }
            // Misplaced declaration of an exception variable
            catch (GENASubscription genasubscription)
            {
                genasubscription = null;
            }
        } else
        {
            genasubscription = null;
        }
        if (genasubscription == null)
        {
            return basedeviceitem;
        }
        j = 0;
        k = 0;
        while (j < genasubscription.size()) 
        {
            Logger.d("WIFI", (new StringBuilder()).append("i:").append(j).append(",Eventedname  ").append(((EventedValue)genasubscription.get(j)).getName()).append("   value:  ").append(((EventedValue)genasubscription.get(j)).getValue()).toString());
            String s = ((EventedValue)genasubscription.get(j)).getName();
            Object obj1 = (EventedValue)genasubscription.get(j);
            int i = k;
            if (obj1 != null)
            {
                if (((EventedValue) (obj1)).getValue() == null)
                {
                    i = k;
                } else
                {
                    obj1 = ((EventedValue) (obj1)).getValue().toString();
                    if (s.equals("TransportState"))
                    {
                        if (((String) (obj1)).equals("PAUSED_PLAYBACK"))
                        {
                            i = k | 0x2000;
                        } else
                        if (((String) (obj1)).equals("PLAYING"))
                        {
                            i = k | 0x4000;
                        } else
                        if (((String) (obj1)).equals("STOPPED"))
                        {
                            i = k | 0x8000;
                        } else
                        {
                            i = k;
                            if (((String) (obj1)).equals("TRANSITIONING"))
                            {
                                i = 4351;
                            }
                        }
                    } else
                    {
                        i = k;
                        if (s.equals("CurrentTrackMetaData"))
                        {
                            if (((String) (obj1)).equals("CHANNEL_PLAY"))
                            {
                                i = k | 0x80;
                            } else
                            if (((String) (obj1)).equals("CHANNEL_PLAY NEXT"))
                            {
                                i = k | 0x20;
                            } else
                            if (((String) (obj1)).equals("CHANNEL_PLAY PREV"))
                            {
                                i = k | 0x40;
                            } else
                            if (((String) (obj1)).equals("CHANNEL_PLAY PREV"))
                            {
                                i = k | 0x40;
                            } else
                            if (((String) (obj1)).equals("NEXT"))
                            {
                                i = k | 8;
                            } else
                            {
                                i = k;
                                if (((String) (obj1)).equals("PREV"))
                                {
                                    i = k | 0x10;
                                }
                            }
                        }
                    }
                }
            }
            j++;
            k = i;
        }
        k;
        JVM INSTR lookupswitch 9: default 672
    //                   8192: 917
    //                   8320: 951
    //                   16384: 883
    //                   16512: 848
    //                   32776: 813
    //                   32784: 779
    //                   32800: 709
    //                   32832: 674
    //                   32896: 744;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10
_L1:
        return basedeviceitem;
_L9:
        Logger.d(TAG, "handlePlayKey2:CHANNEL_PLAY_PRE");
        basedeviceitem.setEventKey(15);
        basedeviceitem.setT(Integer.valueOf(0));
        Logger.d(TAG, "handlePlayKey2 EVENT_META_CHANNEL_PLAY_PRE change mNowPlayState 0");
        continue; /* Loop/switch isn't completed */
_L8:
        Logger.d(TAG, "handlePlayKey2:CHANNEL_PLAY_NEXT");
        basedeviceitem.setEventKey(16);
        basedeviceitem.setT(Integer.valueOf(0));
        Logger.d(TAG, "handlePlayKey2 ARG_SOUND_CHANNEL_NEXT change mNowPlayState 0");
        continue; /* Loop/switch isn't completed */
_L10:
        Logger.d(TAG, "handlePlayKey2:EVENT_META_CHANNEL_PLAY");
        Logger.d(TAG, "handlePlayKey2 ARG_CHANNEL_PLAY change mNowPlayState 0");
        basedeviceitem.setEventKey(8);
        basedeviceitem.setT(Integer.valueOf(0));
        continue; /* Loop/switch isn't completed */
_L7:
        Logger.d(TAG, "handlePlayKey2:PREV");
        basedeviceitem.setEventKey(5);
        basedeviceitem.setT(Integer.valueOf(0));
        Logger.d(TAG, "handlePlayKey2 ARG_SOUND_PRE change mNowPlayState 0");
        continue; /* Loop/switch isn't completed */
_L6:
        Logger.d(TAG, "handlePlayKey2:NEXT");
        basedeviceitem.setEventKey(6);
        basedeviceitem.setT(Integer.valueOf(0));
        Logger.d(TAG, "handlePlayKey2 ARG_SOUND_NEXT change mNowPlayState 0");
        continue; /* Loop/switch isn't completed */
_L5:
        Logger.d(TAG, "handlePlayKey2:CHANNEL_PLAY");
        basedeviceitem.setEventKey(8);
        basedeviceitem.setT(Integer.valueOf(1));
        Logger.d(TAG, "handlePlayKey2 ARG_CHANNEL_PLAY change mNowPlayState:1");
        continue; /* Loop/switch isn't completed */
_L4:
        Logger.d(TAG, "handlePlayKey2:PLAY");
        basedeviceitem.setEventKey(4);
        basedeviceitem.setT(Integer.valueOf(1));
        Logger.d(TAG, "handlePlayKey2 ARG_SOUND_PLAY change mNowPlayState:1");
        continue; /* Loop/switch isn't completed */
_L2:
        Logger.d(TAG, "handlePlayKey2:PAUSE");
        basedeviceitem.setEventKey(3);
        basedeviceitem.setT(Integer.valueOf(0));
        Logger.d(TAG, "handlePlayKey2 EVENT_STATE_PAUSE change mNowPlayState:0");
        continue; /* Loop/switch isn't completed */
_L3:
        Logger.d(TAG, "handlePlayKey2:PAUSE|CHANNEL");
        basedeviceitem.setEventKey(8);
        basedeviceitem.setT(Integer.valueOf(0));
        Logger.d(TAG, "handlePlayKey2 ARG_CHANNEL_PLAY change mNowPlayState:0");
        if (true) goto _L1; else goto _L11
_L11:
    }
}
