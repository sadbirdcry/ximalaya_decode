// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.support.v4.app.FragmentActivity;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.util.CustomToast;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            DeviceListFragment, DlnaManager

class this._cls3
    implements IActionCallBack
{

    final  this$3;

    public void onFailed()
    {
        CustomToast.showToast(MyApplication.b(), "\u8BBE\u5907\u5F02\u5E38", 0);
        if (DeviceListFragment.access$400() != null)
        {
            DeviceListFragment.access$400().dismiss();
            DeviceListFragment.access$402(, null);
        }
        if (DeviceListFragment.access$500() != null)
        {
            DeviceListFragment.access$500().cancel();
        }
        class _cls1
            implements Runnable
        {

            final DeviceListFragment.MyAdapter._cls1._cls1._cls3 this$4;

            public void run()
            {
                getCurrentTypeDevices();
                DeviceListFragment.access$900(this$0).notifyDataSetChanged();
            }

            _cls1()
            {
                this$4 = DeviceListFragment.MyAdapter._cls1._cls1._cls3.this;
                super();
            }
        }

        if (getActivity() != null)
        {
            getActivity().runOnUiThread(new _cls1());
        }
    }

    public void onSuccess(ActionModel actionmodel)
    {
        DlnaManager.getInstance(DeviceListFragment.access$700()).setNowOperationDevice(DeviceListFragment.access$200());
        DeviceListFragment.access$800();
    }

    _cls1()
    {
        this$3 = this._cls3.this;
        super();
    }
}
