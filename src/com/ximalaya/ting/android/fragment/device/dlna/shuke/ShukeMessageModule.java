// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseMessageModule;
import com.ximalaya.ting.android.transaction.d.z;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeDeviceItem

public class ShukeMessageModule extends BaseMessageModule
{
    class CreateAlbumTask extends MyAsyncTask
    {

        final ShukeMessageModule this$0;

        protected transient Integer doInBackground(Void avoid[])
        {
            avoid = (new StringBuilder()).append(a.ac).append("/images/default_album_cover_message.png").toString();
            FileUtils.copyAssetsToFile(.getAssets(), "image/default_album_cover_message.png", avoid);
            final BaseDeviceItem deviceItem = DlnaManager.getInstance().getOperationStroageModel().getNowDeviceItem();
            if ((deviceItem instanceof ShukeDeviceItem) && ((ShukeDeviceItem)deviceItem).hasVoiceOfMother() && ((ShukeDeviceItem)deviceItem).hasVoiceOfChildren())
            {
                return Integer.valueOf(1);
            }
            try
            {
                publishProgress(new Integer[] {
                    Integer.valueOf(0)
                });
                avoid = z.a(com.ximalaya.ting.android.transaction.d.z.e.g, null, new String[] {
                    avoid
                });
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                CustomToast.showToast(, " \u6CA1\u6709\u627E\u5230\u56FE\u7247\u6587\u4EF6", 0);
                avoid.printStackTrace();
                avoid = null;
            }
            if (avoid != null && avoid.b() == 0)
            {
                long l = avoid.a();
                if (!((ShukeDeviceItem)deviceItem).hasVoiceOfMother())
                {
                    CustomToast.showToast(, "\u5F00\u59CB\u521B\u5EFA\u5988\u5988\u7684\u58F0\u97F3\u4E13\u8F91", 0);
                    avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/api1/upload/album_form").toString();
                    RequestParams requestparams = new RequestParams();
                    requestparams.put("title", voiceOfMother);
                    requestparams.put("imageId", (new StringBuilder()).append("").append(l).toString());
                    requestparams.put("isPublic", "false");
                    requestparams.put("categoryId", "1");
                    class _cls1 extends h
                    {

                        final CreateAlbumTask this$1;

                        public void onBindXDCS(Header aheader[])
                        {
                        }

                        public void onFinish()
                        {
                        }

                        public void onNetError(int i, String s)
                        {
                        }

                        public void onStart()
                        {
                        }

                        public void onSuccess(String s)
                        {
                            if (!TextUtils.isEmpty(s))
                            {
                                try
                                {
                                    s = JSONObject.parseObject(s);
                                }
                                // Misplaced declaration of an exception variable
                                catch (String s)
                                {
                                    s.printStackTrace();
                                    return;
                                }
                                if (s.getIntValue("ret") != 0)
                                {
                                    CustomToast.showToast(this$0, (new StringBuilder()).append("\u5931\u8D25").append(s.getString("msg")).toString(), 0);
                                    return;
                                }
                                CustomToast.showToast(this$0, "\u5B8C\u6210\u521B\u5EFA\u5988\u5988\u7684\u58F0\u97F3\u4E13\u8F91", 0);
                                BaseDeviceItem basedeviceitem = DlnaManager.getInstance(this$0).getOperationStroageModel().getNowDeviceItem();
                                if (basedeviceitem != null && (basedeviceitem instanceof ShukeDeviceItem))
                                {
                                    ((ShukeDeviceItem)basedeviceitem).setMumID(s.getLongValue("albumId"));
                                    return;
                                }
                            }
                        }

                _cls1()
                {
                    this$1 = CreateAlbumTask.this;
                    super();
                }
                    }

                    f.a().a(avoid, requestparams, null, new _cls1());
                }
                if (!((ShukeDeviceItem)deviceItem).hasVoiceOfChildren())
                {
                    CustomToast.showToast(, "\u5F00\u59CB\u521B\u5EFA\u5B9D\u5B9D\u7684\u58F0\u97F3\u4E13\u8F91", 0);
                    avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/api1/upload/album_form").toString();
                    RequestParams requestparams1 = new RequestParams();
                    requestparams1.put("title", voiceOfChildren);
                    requestparams1.put("imageId", (new StringBuilder()).append("").append(l).toString());
                    requestparams1.put("isPublic", "false");
                    requestparams1.put("categoryId", "1");
                    class _cls2 extends h
                    {

                        final CreateAlbumTask this$1;
                        final BaseDeviceItem val$deviceItem;

                        public void onBindXDCS(Header aheader[])
                        {
                        }

                        public void onFinish()
                        {
                        }

                        public void onNetError(int i, String s)
                        {
                        }

                        public void onStart()
                        {
                        }

                        public void onSuccess(String s)
                        {
                            if (!TextUtils.isEmpty(s))
                            {
                                try
                                {
                                    s = JSONObject.parseObject(s);
                                }
                                // Misplaced declaration of an exception variable
                                catch (String s)
                                {
                                    s.printStackTrace();
                                    return;
                                }
                                if (s.getIntValue("ret") != 0)
                                {
                                    CustomToast.showToast(
// JavaClassFileOutputException: get_constant: invalid tag

                _cls2()
                {
                    this$1 = CreateAlbumTask.this;
                    deviceItem = basedeviceitem;
                    super();
                }
                    }

                    f.a().a(avoid, requestparams1, null, new _cls2());
                }
                return Integer.valueOf(0);
            }
            if (avoid != null)
            {
                avoid = avoid.c();
                CustomToast.showToast(, (new StringBuilder()).append("\u4E0A\u4F20\u56FE\u7247\u5F02\u5E38").append(avoid).toString(), 0);
            }
            return Integer.valueOf(1);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            super.onPostExecute(integer);
            integer = DlnaManager.getInstance().getOperationStroageModel().getNowDeviceItem();
            if (integer != null && (integer instanceof ShukeDeviceItem))
            {
                sendInitInfo((ShukeDeviceItem)integer);
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        CreateAlbumTask()
        {
            this$0 = ShukeMessageModule.this;
            super();
        }
    }


    private static final String TAG = com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeMessageModule.getSimpleName();
    String voiceOfChildren;
    String voiceOfMother;

    public ShukeMessageModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
        voiceOfMother = "\u5988\u5988\u7684\u58F0\u97F3";
        voiceOfChildren = "\u5B9D\u5B9D\u7684\u58F0\u97F3";
    }

    private void showToast(String s)
    {
    }

    public void createAlbums()
    {
        Logger.d(TAG, "createAlbums IN");
        (new Thread(new _cls2())).start();
    }

    public String getFromAssets(String s)
    {
        BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(mContext.getResources().getAssets().open(s)));
        s = "";
_L1:
        String s2 = bufferedreader.readLine();
        String s1;
        s1 = s;
        if (s2 == null)
        {
            break MISSING_BLOCK_LABEL_75;
        }
        s = (new StringBuilder()).append(s).append(s2).toString();
          goto _L1
        s;
        s.printStackTrace();
        s1 = null;
        return s1;
    }

    public void sendInitInfo(final ShukeDeviceItem deviceItem)
    {
        (new Thread(new _cls1())).start();
    }
















    private class _cls2
        implements Runnable
    {

        final ShukeMessageModule this$0;
        final ShukeDeviceItem val$deviceItem;

        public void run()
        {
            Object obj1 = null;
            Object obj = UserInfoMannage.getInstance().getUser();
            Object obj2 = ;
            long l;
            if (obj == null)
            {
                l = 0L;
            } else
            {
                l = ((LoginInfoModel) (obj)).uid;
            }
            if (obj == null)
            {
                obj = null;
            } else
            {
                obj = ((LoginInfoModel) (obj)).token;
            }
            obj2 = CommonRequest.doGetMyOrOtherAlbums(((Context) (obj2)), l, ((String) (obj)), 0L, 1, 30, null, null);
            obj = obj1;
            if (Utilities.isNotBlank(((String) (obj2))))
            {
                try
                {
                    obj = (AlbumCollection)JSON.parseObject(((String) (obj2)), com/ximalaya/ting/android/model/album/AlbumCollection);
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                    exception = obj1;
                }
            }
            if (obj != null)
            {
                for (int i = 0; i < ((AlbumCollection) (obj)).list.size(); i++)
                {
                    if (!deviceItem.hasVoiceOfMother() && ((AlbumModel)((AlbumCollection) (obj)).list.get(i)).title.equals(voiceOfMother))
                    {
                        deviceItem.setHasVoiceOfMother(true);
                        deviceItem.setMumID(((AlbumModel)((AlbumCollection) (obj)).list.get(i)).albumId);
                    }
                    if (!deviceItem.hasVoiceOfChildren() && ((AlbumModel)((AlbumCollection) (obj)).list.get(i)).title.equals(voiceOfChildren))
                    {
                        deviceItem.setHasVoiceOfChildren(true);
                        deviceItem.setSonID(((AlbumModel)((AlbumCollection) (obj)).list.get(i)).albumId);
                    }
                }

                deviceItem.setHasVoiceOfChildren(true);
                (new CreateAlbumTask()).myexec(new Void[0]);
            }
        }

        _cls2()
        {
            this$0 = ShukeMessageModule.this;
            deviceItem = shukedeviceitem;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final ShukeMessageModule this$0;
        final ShukeDeviceItem val$deviceItem;

        public void run()
        {
            if (deviceItem.getMumID() != 0L);
            if (deviceItem.getSonID() != 0L);
            showToast((new StringBuilder()).append("\u51C6\u5907\u53D1\u9001\u4E13\u8F91ID\u548Ctoken:momID:").append(deviceItem.getMumID()).append("sonID:").append(deviceItem.getSonID()).append("token:").append(UserInfoMannage.getInstance().getUser().token).toString());
            Logger.d(ShukeMessageModule.TAG, "sendInitInfo IN");
            String s = (new StringBuilder()).append("http://").append(deviceItem.getIpAddress()).toString();
            s = (new StringBuilder()).append(s).append("/httpapi.asp?command=ximalayaSetID").toString();
            s = (new StringBuilder()).append(s).append(":").append(deviceItem.getMumID()).toString();
            s = (new StringBuilder()).append(s).append(":").append(deviceItem.getSonID()).toString();
            if (UserInfoMannage.hasLogined() && UserInfoMannage.getInstance().getUser().token != null)
            {
                s = (new StringBuilder()).append(s).append(":").append(UserInfoMannage.getInstance().getUser().uid).append("%2526").append(UserInfoMannage.getInstance().getUser().token).toString();
                Logger.d(ShukeMessageModule.TAG, (new StringBuilder()).append("url:").append(s).toString());
                (new Thread()).start();
                s = f.a().a(s, null, null, null);
                Logger.d(ShukeMessageModule.TAG, (new StringBuilder()).append("result:").append(s).toString());
            }
        }

        _cls1()
        {
            this$0 = ShukeMessageModule.this;
            deviceItem = shukedeviceitem;
            super();
        }
    }

}
