// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.other;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.BasePlayManageController;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseTuiSongModule;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseVolumeControlModule;
import com.ximalaya.ting.android.fragment.device.dlna.tingshubao.TingshuKeyModule;
import com.ximalaya.ting.android.fragment.device.dlna.tingshubao.TingshuTuisongModule;
import java.util.ArrayList;
import java.util.List;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.other:
//            OtherTuisongModule

public class OtherController extends BaseDlnaController
{

    public OtherController(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public void addDeivce(BaseDeviceItem basedeviceitem)
    {
        if (mDeviceList == null)
        {
            mDeviceList = new ArrayList();
        }
        basedeviceitem = new BaseDeviceItem(basedeviceitem);
        if (!mDeviceList.contains(basedeviceitem))
        {
            mDeviceList.add(basedeviceitem);
        }
        initDeviceInfo(basedeviceitem, null);
    }

    public void change2Bendi(BasePlayManageController baseplaymanagecontroller)
    {
    }

    public String[] getCheckedUdn()
    {
        return null;
    }

    public com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getDeviceType()
    {
        return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.other;
    }

    public void initDeviceInfo(BaseDeviceItem basedeviceitem, IActionCallBack iactioncallback)
    {
    }

    public void initModules()
    {
        setModule(new TingshuKeyModule(mContext, getControlPoint()));
        setModule(new OtherTuisongModule(mContext, getControlPoint()));
        setModule(new BaseVolumeControlModule(mContext, getControlPoint()));
    }

    public void onCommandFailed(ActionModel actionmodel)
    {
    }

    public void playSound(BaseDeviceItem basedeviceitem, ActionModel actionmodel)
    {
        ((TingshuTuisongModule)getModule(BaseTuiSongModule.NAME)).tuisong(basedeviceitem, actionmodel);
    }
}
