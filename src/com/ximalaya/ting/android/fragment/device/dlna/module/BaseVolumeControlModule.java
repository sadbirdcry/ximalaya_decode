// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.module:
//            BaseDlnaModule

public class BaseVolumeControlModule extends BaseDlnaModule
{

    public static final int CHANGE_VOLUME_VAULE = 5;
    public static final String NAME = com/ximalaya/ting/android/fragment/device/dlna/module/BaseVolumeControlModule.getSimpleName();

    public BaseVolumeControlModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public void changeVolume(boolean flag)
    {
        Logger.d("WIFI", "changeVolume IN");
        int i;
        if (flag)
        {
            if (DlnaManager.getInstance(mContext).getOperationStroageModel().getNowVolume() > 95)
            {
                i = 100;
            } else
            {
                i = DlnaManager.getInstance(mContext).getOperationStroageModel().getNowVolume() + 5;
            }
        } else
        if (DlnaManager.getInstance(mContext).getOperationStroageModel().getNowVolume() < 5)
        {
            i = 0;
        } else
        {
            i = DlnaManager.getInstance(mContext).getOperationStroageModel().getNowVolume() - 5;
        }
        Logger.d("WIFI", (new StringBuilder()).append("volumePlus:").append(i).toString());
        DlnaManager.getInstance(mContext).getOperationStroageModel().setNowVolume(i);
        getControlPoint().execute(new _cls2(DlnaManager.getInstance(mContext).getOperationStroageModel().getNowDeviceItem().getRCservice(), i));
    }

    public String getModuleName()
    {
        return NAME;
    }

    public void volumePlus(boolean flag)
    {
        getControlPoint().execute(new _cls1(flag));
    }


    private class _cls2 extends SetVolumeActionCallback
    {

        final BaseVolumeControlModule this$0;

        protected void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse)
        {
            super.failure(actioninvocation, upnpresponse);
        }

        public void success(ActionInvocation actioninvocation)
        {
        }

        _cls2(Service service, long l)
        {
            this$0 = BaseVolumeControlModule.this;
            super(service, l);
        }
    }


    private class _cls1 extends GetVolumeActionCallback
    {

        final BaseVolumeControlModule this$0;
        final boolean val$isPlus;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void received(ActionInvocation actioninvocation, int i)
        {
            DlnaManager.getInstance(mContext).getOperationStroageModel().setNowVolume(i);
            Logger.d("WIFI", (new StringBuilder()).append("initVolume:").append(i).toString());
            changeVolume(isPlus);
        }

        _cls1(boolean flag)
        {
            this$0 = BaseVolumeControlModule.this;
            isPlus = flag;
            super(final_service);
        }
    }

}
