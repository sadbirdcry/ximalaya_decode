// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.model.album.AlbumModel;
import org.teleal.cling.model.meta.Device;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            BaseDeviceItem

public class BaseBindableDeviceItem extends BaseDeviceItem
{

    protected BaseItemBindableModel mBaseBindableModel;

    public BaseBindableDeviceItem(BaseDeviceItem basedeviceitem)
    {
        super(basedeviceitem);
    }

    public BaseBindableDeviceItem(Device device, String as[])
    {
        super(device, as);
    }

    public BaseItemBindableModel getBaseBindableModel()
    {
        return mBaseBindableModel;
    }

    public void setBaseBindableModel(BaseItemBindableModel baseitembindablemodel)
    {
        mBaseBindableModel = baseitembindablemodel;
    }

    public void setKeyAlbum(int i, AlbumModel albummodel)
    {
        if (mBaseBindableModel != null)
        {
            mBaseBindableModel.setAlbums(i, albummodel);
        }
    }
}
