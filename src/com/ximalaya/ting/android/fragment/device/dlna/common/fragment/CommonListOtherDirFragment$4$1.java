// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.support.v4.app.FragmentActivity;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonManageTFModule;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonListOtherDirFragment

class this._cls1
    implements Runnable
{

    final is._cls0 this$1;

    public void run()
    {
        class _cls1
            implements Runnable
        {

            final CommonListOtherDirFragment._cls4._cls1 this$2;

            public void run()
            {
                CommonListOtherDirFragment.access$502(this$0, new MyProgressDialog(getActivity()));
                CommonListOtherDirFragment.access$600(this$0).setTitle("\u63D0\u793A");
                CommonListOtherDirFragment.access$700(this$0).setMessage("\u6B63\u5728\u6E05\u7A7A\u5217\u8868");
                CommonListOtherDirFragment.access$800(this$0).show();
            }

            _cls1()
            {
                this$2 = CommonListOtherDirFragment._cls4._cls1.this;
                super();
            }
        }

        getActivity().runOnUiThread(new _cls1());
        class _cls2 extends DeleteMediaActionCallback
        {

            final CommonListOtherDirFragment._cls4._cls1 this$2;

            public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
            {
                Logger.d("doss", "deleteMedia failure");
                refreshMediaList();
                CommonListOtherDirFragment.access$1100(this$0);
            }

            public void success(ActionInvocation actioninvocation)
            {
                Logger.d("doss", "deleteMedia SUCCESS");
                getItems().clear();
                refreshMediaList();
                CommonListOtherDirFragment.access$1200(this$0);
            }

            _cls2(Service service, String s, String s1, String s2, int i)
            {
                this$2 = CommonListOtherDirFragment._cls4._cls1.this;
                super(service, s, s1, s2, i);
            }
        }

        ActionModel actionmodel = ActionModel.createModel(new _cls2(((CommonDeviceItem)CommonListOtherDirFragment.access$900(_fld0)).getMMservice(), "USBDISK", "Directory", CommonListOtherDirFragment.access$1000(_fld0), -2));
        CommonListOtherDirFragment.access$1300(_fld0).deleteMedia(actionmodel);
    }

    _cls2()
    {
        this$1 = this._cls1.this;
        super();
    }
}
