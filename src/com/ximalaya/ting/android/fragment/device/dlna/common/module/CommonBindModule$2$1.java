// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import com.ximalaya.action.GetKeyMappingActionCallback;
import com.ximalaya.model.playqueue.Key;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BindCommandModel;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.Logger;
import java.util.List;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.support.playqueue.callback.keymappingqueue.KeyMappingQueueConstants;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.module:
//            CommonBindModule

class this._cls1 extends GetKeyMappingActionCallback
{

    final l.mDeviceItem this$1;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        CommonBindModule.access$000(_fld0, "\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
        Logger.d("doss", "GetKeyMappingActionCallback Failure");
    }

    public void received(ActionInvocation actioninvocation, List list)
    {
        Logger.d("doss", "GetKeyMappingActionCallback Success");
        actioninvocation = new Key();
        actioninvocation.name = model.title;
        list.set(mBindCommand.mChannelId, actioninvocation);
        actioninvocation = KeyMappingQueueConstants.getSetKeyMappingQueueContext(list);
        class _cls1 extends SetKeyMappingActionCallback
        {

            final CommonBindModule._cls2._cls1 this$2;

            public void failure(ActionInvocation actioninvocation1, UpnpResponse upnpresponse, String s)
            {
                CommonBindModule.access$000(this$0, "\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
                Logger.d(CommonBindModule.access$200(this$0), "SetKeyMapping failure");
            }

            public void success(ActionInvocation actioninvocation1)
            {
                class _cls1 extends TimerTask
                {

                    final _cls1 this$3;

                    public void run()
                    {
                        CommonBindModule.access$000(this$0, "\u7ED1\u5B9A\u4E13\u8F91\u6210\u529F");
                        Logger.d(CommonBindModule.access$100(this$0), "SetKeyMapping Success");
                        mDeviceItem.getBaseBindableModel().setAlbums(mBindCommand.mChannelId, mBindCommand.mAlbumModel);
                        freshBindFragment();
                    }

                        _cls1()
                        {
                            this$3 = _cls1.this;
                            super();
                        }
                }

                (new Timer()).schedule(new _cls1(), 500L);
            }

            _cls1(Service service, String s)
            {
                this$2 = CommonBindModule._cls2._cls1.this;
                super(service, s);
            }
        }

        getControlPoint().execute(new _cls1(mDeviceItem.getPQservice(), actioninvocation));
    }

    _cls1(Service service)
    {
        this$1 = this._cls1.this;
        super(service);
    }
}
