// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import com.ximalaya.subscribe.PlaySubscriptionCallback;
import com.ximalaya.ting.android.fragment.device.dlna.model.LinkedDeviceModel;
import com.ximalaya.ting.android.fragment.device.dlna.subscription.MyPlaySubscriptionCallback;

public abstract class BasePlayManageController
    implements com.ximalaya.ting.android.fragment.device.dlna.subscription.MyPlaySubscriptionCallback.OnPlayKeyHandler
{

    protected LinkedDeviceModel mLinkedDeviceModel;
    protected MyPlaySubscriptionCallback mMediaManagerSubscriptionCallback;
    protected MyPlaySubscriptionCallback mPlayQueueSubscriptionCallback;
    protected PlaySubscriptionCallback mPlaySubscriptionCallback;

    public BasePlayManageController()
    {
    }

    public LinkedDeviceModel getLinkedDeviceModel()
    {
        return mLinkedDeviceModel;
    }

    public MyPlaySubscriptionCallback getMediaManagerSubscriptionCallback()
    {
        return mMediaManagerSubscriptionCallback;
    }

    public MyPlaySubscriptionCallback getPlayQueueSubscriptionCallback()
    {
        return mPlayQueueSubscriptionCallback;
    }

    public PlaySubscriptionCallback getPlaySubscriptionCallback()
    {
        return mPlaySubscriptionCallback;
    }

    public void setLinkedDeviceModel(LinkedDeviceModel linkeddevicemodel)
    {
        mLinkedDeviceModel = linkeddevicemodel;
    }

    public void setMediaManagerSubscriptionCallback(MyPlaySubscriptionCallback myplaysubscriptioncallback)
    {
        mMediaManagerSubscriptionCallback = myplaysubscriptioncallback;
    }

    public void setPlayQueueSubscriptionCallback(MyPlaySubscriptionCallback myplaysubscriptioncallback)
    {
        mPlayQueueSubscriptionCallback = myplaysubscriptioncallback;
    }

    public void setPlaySubscriptionCallback(PlaySubscriptionCallback playsubscriptioncallback)
    {
        mPlaySubscriptionCallback = playsubscriptioncallback;
    }
}
