// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.tingshubao;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseTuiSongModule;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.tingshubao:
//            TingshuController, TingshuDeviceItem

public class TingshuTuisongModule extends BaseTuiSongModule
{

    Context mContext;
    DlnaManager mDlnaManager;
    TingshuController mTingshuController;

    public TingshuTuisongModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
        mDlnaManager = DlnaManager.getInstance(context);
        mTingshuController = (TingshuController)mDlnaManager.getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao);
    }

    public void tuisong(BaseDeviceItem basedeviceitem, ActionModel actionmodel)
    {
        while (!(basedeviceitem instanceof TingshuDeviceItem) || basedeviceitem.getDlnaType() != com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao) 
        {
            return;
        }
        play(basedeviceitem, actionmodel);
    }
}
