// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BasePlayModule;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.controlpoint.ControlPoint;

public class CommonPlayModule extends BasePlayModule
{

    public static String INDEX = "index";
    public static String PLAYLIST = "playlist";

    public CommonPlayModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public void createQueue(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "createQueue IN");
        getControlPoint().execute(actionmodel);
    }

    public void pause(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "pause IN");
        getControlPoint().execute(actionmodel);
    }

    public void play(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "play IN");
        getControlPoint().execute(actionmodel);
    }

    public void playWithIndex(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "playWithIndex IN");
        getControlPoint().execute(actionmodel);
    }

}
