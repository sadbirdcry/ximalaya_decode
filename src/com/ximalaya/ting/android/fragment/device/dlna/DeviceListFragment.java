// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.DeviceBindingListFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukeContentFragment;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukePwInputFragment;
import com.ximalaya.ting.android.fragment.device.doss.DossContentFragment;
import com.ximalaya.ting.android.fragment.device.doss.DossPwInputFragment;
import com.ximalaya.ting.android.fragment.device.shu.WifiIntroFragment;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            DlnaManager, IDlnaController, BaseDeviceItem, DeviceUtil

public class DeviceListFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, DlnaManager.OnDeviceChangedListener
{

    public static String deviceType = "deviceType";
    private MyProgressDialog loadingDialog;
    private MyAdapter mAdapter;
    private ImageView mBackImg;
    private TextView mConnMore;
    private RelativeLayout mContentView;
    private Context mContext;
    private TextView mDeviceDes;
    private TextView mDeviceFirstUse;
    private TextView mDeviceInWifi;
    private TextView mDeviceNoGet;
    private ArrayList mDevices;
    private ListView mListView;
    private MyDeviceManager mMyDeviceManager;
    private RelativeLayout mNoFindLayout;
    private com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType mNowDeviceType;
    private BaseDeviceItem mNowMyDeviceItem;
    private int mPosition;
    private TextView mTopTv;
    private TextView mcheBuy;
    List productModels;
    private TimerTask task;
    private Timer timer;

    public DeviceListFragment()
    {
        mPosition = -1;
        mNowDeviceType = com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao;
        productModels = MyDeviceManager.getInstance(mContext).getProductModels();
    }

    private void distinguishDevice()
    {
        getCurrentTypeDevices();
        static class _cls2
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType = new int[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        switch (_cls2..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[mNowDeviceType.ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            mTopTv.setText(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoName());
            mMyDeviceManager.setNowDeviceType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao);
            return;

        case 2: // '\002'
            mTopTv.setText(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossName());
            mMyDeviceManager.setNowDeviceType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss);
            return;

        case 3: // '\003'
            mTopTv.setText(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiName());
            break;
        }
        mMyDeviceManager.setNowDeviceType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi);
    }

    private void initView()
    {
        mConnMore = (TextView)mContentView.findViewById(0x7f0a071c);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mTopTv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mcheBuy = (TextView)mContentView.findViewById(0x7f0a039c);
        mNoFindLayout = (RelativeLayout)mContentView.findViewById(0x7f0a0393);
        mDeviceDes = (TextView)mContentView.findViewById(0x7f0a0395);
        mDeviceFirstUse = (TextView)mContentView.findViewById(0x7f0a0397);
        mDeviceInWifi = (TextView)mContentView.findViewById(0x7f0a0399);
        mDeviceNoGet = (TextView)mContentView.findViewById(0x7f0a039b);
        mConnMore.setText("\u6DFB\u52A0\u65B0\u8BBE\u5907");
        mConnMore.setVisibility(0);
        mConnMore.setOnClickListener(this);
        mBackImg.setOnClickListener(this);
        mcheBuy.setOnClickListener(this);
        mListView = (ListView)mContentView.findViewById(0x7f0a005c);
        mAdapter = new MyAdapter();
        mListView.setAdapter(mAdapter);
    }

    private void toBindFragment()
    {
        Bundle bundle;
        Logger.d("doss", "toBindFragment IN");
        if (mPosition < 0)
        {
            Logger.d("doss", "mPosition < 0 return");
            return;
        }
        bundle = new Bundle();
        bundle.putInt("NUM", mPosition);
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
        if (timer != null)
        {
            timer.cancel();
        }
        Logger.d("doss", "start Bing Fragment");
        _cls2..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[mNowDeviceType.ordinal()];
        JVM INSTR tableswitch 1 3: default 120
    //                   1 148
    //                   2 137
    //                   3 126;
           goto _L1 _L2 _L3 _L4
_L1:
        mPosition = -1;
        return;
_L4:
        startFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeContentFragment, bundle);
        continue; /* Loop/switch isn't completed */
_L3:
        startFragment(com/ximalaya/ting/android/fragment/device/doss/DossContentFragment, bundle);
        continue; /* Loop/switch isn't completed */
_L2:
        startFragment(com/ximalaya/ting/android/fragment/device/DeviceBindingListFragment, bundle);
        if (true) goto _L1; else goto _L5
_L5:
    }

    public void getCurrentTypeDevices()
    {
        mDevices.clear();
        if (DlnaManager.getInstance(getActivity().getApplicationContext()).getController(mNowDeviceType) != null)
        {
            List list = DlnaManager.getInstance(getActivity().getApplicationContext()).getController(mNowDeviceType).getAllDevices();
            Logger.d("doss", (new StringBuilder()).append(list.size()).append("").toString());
            mDevices.addAll(list);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        onActivityCreated(bundle);
        if (getArguments() != null)
        {
            int i = getArguments().getInt(deviceType);
            mNowDeviceType = com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.values()[i];
        }
        mMyDeviceManager = MyDeviceManager.getInstance(mActivity);
        mDevices = new ArrayList();
        initView();
        distinguishDevice();
        mAdapter.notifyDataSetChanged();
        if (mDevices.size() > 0)
        {
            mNoFindLayout.setVisibility(8);
            mListView.setVisibility(0);
        } else
        {
            mListView.setVisibility(8);
            mNoFindLayout.setVisibility(0);
            distinguishDevice();
            if (mNowDeviceType.equals(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao))
            {
                mDeviceDes.setText((new StringBuilder()).append("\u6682\u672A\u53D1\u73B0").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoSubName()).append("").toString());
                mDeviceFirstUse.setText((new StringBuilder()).append("\u7B2C\u4E00\u6B21\u4F7F\u7528\u8BF7\u70B9\u51FB\u53F3\u4E0A\u89D2\u300C\u6DFB\u52A0\u65B0\u8BBE\u5907\u300D\uFF0C\u5C06").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoSubName()).append("\u8FDE\u63A5\u7F51\u7EDC").toString());
                mDeviceInWifi.setText((new StringBuilder()).append("\u8BF7\u786E\u8BA4").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoSubName()).append("\u5DF2\u63A5\u901A\u7535\u6E90\uFF0C\u4E14\u4E0E\u624B\u673A\u5904\u4E8E\u540C\u4E00WIFI\u7F51\u7EDC\u4E2D").toString());
                mDeviceNoGet.setText((new StringBuilder()).append("\u82E5\u60A8\u5C1A\u672A\u62E5\u6709").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoSubName()).append("\uFF0C\u53EF").toString());
                return;
            }
            if (mNowDeviceType.equals(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss))
            {
                mDeviceDes.setText((new StringBuilder()).append("\u6682\u672A\u53D1\u73B0").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossSubName()).append("").toString());
                mDeviceFirstUse.setText((new StringBuilder()).append("\u7B2C\u4E00\u6B21\u4F7F\u7528\u8BF7\u70B9\u51FB\u53F3\u4E0A\u89D2\u300C\u6DFB\u52A0\u65B0\u8BBE\u5907\u300D\uFF0C\u5C06").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossSubName()).append("\u8FDE\u63A5\u7F51\u7EDC").toString());
                mDeviceInWifi.setText((new StringBuilder()).append("\u8BF7\u786E\u8BA4").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossSubName()).append("\u5DF2\u63A5\u901A\u7535\u6E90\uFF0C\u4E14\u4E0E\u624B\u673A\u5904\u4E8E\u540C\u4E00WIFI\u7F51\u7EDC\u4E2D").toString());
                mDeviceNoGet.setText((new StringBuilder()).append("\u82E5\u60A8\u5C1A\u672A\u62E5\u6709").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossSubName()).append("\uFF0C\u53EF").toString());
                return;
            }
            if (mNowDeviceType.equals(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi))
            {
                mDeviceDes.setText((new StringBuilder()).append("\u6682\u672A\u53D1\u73B0").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiName()).append("").toString());
                mDeviceFirstUse.setText((new StringBuilder()).append("\u7B2C\u4E00\u6B21\u4F7F\u7528\u8BF7\u70B9\u51FB\u53F3\u4E0A\u89D2\u300C\u6DFB\u52A0\u65B0\u8BBE\u5907\u300D\uFF0C\u5C06").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiSubName()).append("\u8FDE\u63A5\u7F51\u7EDC").toString());
                mDeviceInWifi.setText((new StringBuilder()).append("\u8BF7\u786E\u8BA4").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiSubName()).append("\u5DF2\u63A5\u901A\u7535\u6E90\uFF0C\u4E14\u4E0E\u624B\u673A\u5904\u4E8E\u540C\u4E00WIFI\u7F51\u7EDC\u4E2D").toString());
                mDeviceNoGet.setText((new StringBuilder()).append("\u82E5\u60A8\u5C1A\u672A\u62E5\u6709").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiSubName()).append("\uFF0C\u53EF").toString());
                return;
            }
        }
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 3: default 40
    //                   2131362716: 150
    //                   2131363611: 41
    //                   2131363612: 49;
           goto _L1 _L2 _L3 _L4
_L1:
        return;
_L3:
        mActivity.onBackPressed();
        return;
_L4:
        switch (_cls2..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[mNowDeviceType.ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            startFragment(com/ximalaya/ting/android/fragment/device/shu/WifiIntroFragment, null);
            return;

        case 3: // '\003'
            view = new Bundle();
            view.putString("type", "doss");
            startFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukePwInputFragment, view);
            return;

        case 2: // '\002'
            view = new Bundle();
            view.putString("type", "doss");
            startFragment(com/ximalaya/ting/android/fragment/device/doss/DossPwInputFragment, view);
            return;
        }
_L2:
        distinguishDevice();
        if (MyDeviceManager.getInstance(mContext).getProductModels() != null)
        {
            if (mNowDeviceType.equals(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao))
            {
                MyDeviceManager.getInstance(getActivity()).toEachMarket("\u542C\u4E66\u5B9D\u63D2\u4EF6");
                return;
            }
            if (mNowDeviceType.equals(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss))
            {
                MyDeviceManager.getInstance(getActivity()).toEachMarket("\u542C\u5427\u63D2\u4EF6");
                return;
            }
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    public void onCreate(Bundle bundle)
    {
        onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300dd, viewgroup, false);
        return mContentView;
    }

    public void onDestroy()
    {
        onDestroy();
    }

    public void onDeviceChanged()
    {
        mDevices.clear();
        getCurrentTypeDevices();
        Logger.d("activity", (new StringBuilder()).append("name:").append(getClass().getSimpleName()).append(";activtiy:").append(mActivity).toString());
        if (mActivity != null)
        {
            mActivity.runOnUiThread(new _cls1());
        }
    }

    public void onPause()
    {
        onPause();
        if (MyDeviceManager.getInstance(mContext).isDlnaInit())
        {
            DlnaManager.getInstance(mContext).stopScanThread();
        }
    }

    public void onResume()
    {
        onResume();
        DlnaManager.getInstance(mContext).setDeviceChangedListener(this);
        if (MyDeviceManager.getInstance(mContext).isDlnaInit())
        {
            DlnaManager.getInstance(mContext).startScanThread(-1);
        }
    }

    public void onStop()
    {
        if (loadingDialog != null)
        {
            loadingDialog.dismiss();
            loadingDialog = null;
        }
        if (timer != null)
        {
            timer.cancel();
        }
        DlnaManager.getInstance(getActivity().getApplicationContext()).removeDeviceChangedListener(this);
        onStop();
    }









/*
    static BaseDeviceItem access$202(DeviceListFragment devicelistfragment, BaseDeviceItem basedeviceitem)
    {
        devicelistfragment.mNowMyDeviceItem = basedeviceitem;
        return basedeviceitem;
    }

*/



/*
    static TimerTask access$302(DeviceListFragment devicelistfragment, TimerTask timertask)
    {
        devicelistfragment.task = timertask;
        return timertask;
    }

*/



/*
    static MyProgressDialog access$402(DeviceListFragment devicelistfragment, MyProgressDialog myprogressdialog)
    {
        devicelistfragment.loadingDialog = myprogressdialog;
        return myprogressdialog;
    }

*/



/*
    static Timer access$502(DeviceListFragment devicelistfragment, Timer timer1)
    {
        devicelistfragment.timer = timer1;
        return timer1;
    }

*/


/*
    static int access$602(DeviceListFragment devicelistfragment, int i)
    {
        devicelistfragment.mPosition = i;
        return i;
    }

*/




    private class _cls1
        implements Runnable
    {

        final DeviceListFragment this$0;

        public void run()
        {
            mAdapter.notifyDataSetChanged();
            if (mDevices.size() > 0)
            {
                mNoFindLayout.setVisibility(8);
                mListView.setVisibility(0);
                return;
            } else
            {
                mListView.setVisibility(8);
                mNoFindLayout.setVisibility(0);
                return;
            }
        }

        _cls1()
        {
            this$0 = DeviceListFragment.this;
            Object();
        }
    }

}
