// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.ximalaya.ting.android.fragment.device.DeviceBindingListFragment;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonContentFragment

public class this._cls0 extends FragmentPagerAdapter
{

    private final String TITLES[] = {
        "\u7ED1\u5B9A\u7684\u4E13\u8F91", "\u5185\u7F6E\u7684\u58F0\u97F3"
    };
    final CommonContentFragment this$0;

    public int getCount()
    {
        return TITLES.length;
    }

    public Fragment getItem(int i)
    {
        if (i == 1)
        {
            if (CommonContentFragment.access$000(CommonContentFragment.this) == null)
            {
                CommonContentFragment.access$002(CommonContentFragment.this, getMainFragment());
            }
            return CommonContentFragment.access$000(CommonContentFragment.this);
        }
        if (i == 0)
        {
            if (mBindFragment == null)
            {
                mBindFragment = new DeviceBindingListFragment();
            }
            return mBindFragment;
        } else
        {
            return null;
        }
    }

    public CharSequence getPageTitle(int i)
    {
        return TITLES[i];
    }

    public i(FragmentManager fragmentmanager)
    {
        this$0 = CommonContentFragment.this;
        super(fragmentmanager);
    }
}
