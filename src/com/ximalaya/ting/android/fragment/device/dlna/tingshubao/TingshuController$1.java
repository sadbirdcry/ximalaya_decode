// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.tingshubao;

import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.tingshubao:
//            TingshuController, TingshuDeviceItem

class isSuccess extends MyAsyncTask
{

    boolean isSuccess;
    final TingshuController this$0;
    final IActionCallBack val$callBack;
    final TingshuDeviceItem val$tingshuDeviceItem;
    final String val$url;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        Object obj;
        obj = new DefaultHttpClient();
        ((DefaultHttpClient) (obj)).getParams().setParameter("http.connection.timeout", Integer.valueOf(5000));
        avoid = new HttpGet(val$url);
        avoid = ((DefaultHttpClient) (obj)).execute(avoid);
        if (avoid == null) goto _L2; else goto _L1
_L1:
        obj = avoid.getStatusLine();
        if (obj == null) goto _L4; else goto _L3
_L3:
        if (((StatusLine) (obj)).getStatusCode() != 200) goto _L4; else goto _L5
_L5:
        isSuccess = true;
        obj = new BufferedReader(new InputStreamReader(avoid.getEntity().getContent()));
        avoid = ((Void []) (obj));
        StringBuffer stringbuffer = new StringBuffer("");
_L7:
        avoid = ((Void []) (obj));
        Object obj1 = ((BufferedReader) (obj)).readLine();
        if (obj1 == null)
        {
            break; /* Loop/switch isn't completed */
        }
        avoid = ((Void []) (obj));
        stringbuffer.append(((String) (obj1)));
        if (true) goto _L7; else goto _L6
        obj1;
_L33:
        avoid = ((Void []) (obj));
        ((UnsupportedEncodingException) (obj1)).printStackTrace();
        if (obj != null)
        {
            try
            {
                ((BufferedReader) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
        }
        if (val$callBack == null) goto _L9; else goto _L8
_L8:
        if (!isSuccess) goto _L11; else goto _L10
_L10:
        avoid = val$callBack;
_L15:
        avoid.onSuccess(null);
_L9:
        return null;
_L6:
        avoid = ((Void []) (obj));
        obj1 = stringbuffer.toString();
        avoid = ((Void []) (obj));
        Logger.d(TingshuController.TAG, (new StringBuilder()).append("getChanelsAllRequest onSuccess:").append(((String) (obj1))).toString());
        avoid = ((Void []) (obj));
        TingshuController.access$000(TingshuController.this, ((String) (obj1)), val$tingshuDeviceItem);
        avoid = ((Void []) (obj));
        isSuccess = true;
        if (obj != null)
        {
            try
            {
                ((BufferedReader) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
        }
        if (val$callBack == null) goto _L9; else goto _L12
_L12:
        if (!isSuccess) goto _L14; else goto _L13
_L13:
        avoid = val$callBack;
          goto _L15
        obj1;
        obj = null;
_L31:
        avoid = ((Void []) (obj));
        ((ClientProtocolException) (obj1)).printStackTrace();
        if (obj != null)
        {
            try
            {
                ((BufferedReader) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
        }
        if (val$callBack == null) goto _L9; else goto _L16
_L16:
        if (!isSuccess) goto _L18; else goto _L17
_L17:
        avoid = val$callBack;
          goto _L15
        obj1;
        obj = null;
_L30:
        avoid = ((Void []) (obj));
        ((IOException) (obj1)).printStackTrace();
        if (obj != null)
        {
            try
            {
                ((BufferedReader) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
        }
        if (val$callBack == null) goto _L9; else goto _L19
_L19:
        if (!isSuccess) goto _L21; else goto _L20
_L20:
        avoid = val$callBack;
          goto _L15
        obj1;
        obj = null;
_L29:
        avoid = ((Void []) (obj));
        ((IllegalStateException) (obj1)).printStackTrace();
        if (obj != null)
        {
            try
            {
                ((BufferedReader) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
        }
        if (val$callBack == null) goto _L9; else goto _L22
_L22:
        if (!isSuccess) goto _L24; else goto _L23
_L23:
        avoid = val$callBack;
          goto _L15
        obj;
        avoid = null;
_L28:
        if (avoid != null)
        {
            try
            {
                avoid.close();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
        }
        if (val$callBack != null)
        {
            if (isSuccess)
            {
                val$callBack.onSuccess(null);
            } else
            {
                val$callBack.onFailed();
            }
        }
        throw obj;
_L11:
        avoid = val$callBack;
_L25:
        avoid.onFailed();
        return null;
_L18:
        avoid = val$callBack;
          goto _L25
_L21:
        avoid = val$callBack;
          goto _L25
_L24:
        avoid = val$callBack;
          goto _L25
_L14:
        avoid = val$callBack;
          goto _L25
_L4:
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
        }
        if (val$callBack == null) goto _L9; else goto _L26
_L26:
label0:
        {
            if (!isSuccess)
            {
                break label0;
            }
            avoid = val$callBack;
        }
          goto _L15
        avoid = val$callBack;
          goto _L25
_L2:
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
        }
        if (val$callBack == null) goto _L9; else goto _L27
_L27:
label1:
        {
            if (!isSuccess)
            {
                break label1;
            }
            avoid = val$callBack;
        }
          goto _L15
        avoid = val$callBack;
          goto _L25
        obj;
          goto _L28
        obj1;
          goto _L29
        obj1;
          goto _L30
        obj1;
          goto _L31
        obj1;
        obj = null;
        if (true) goto _L33; else goto _L32
_L32:
    }

    ()
    {
        this$0 = final_tingshucontroller;
        val$url = s;
        val$tingshuDeviceItem = tingshudeviceitem;
        val$callBack = IActionCallBack.this;
        super();
        isSuccess = false;
    }
}
