// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import com.ximalaya.model.playqueue.ListInfo;
import com.ximalaya.model.playqueue.PlayList;
import com.ximalaya.model.playqueue.Track;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.doss.DossUtils;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.Playlist;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.List;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.support.contentdirectory.DIDLParser;
import org.teleal.cling.support.model.DIDLContent;
import org.teleal.cling.support.model.ProtocolInfo;
import org.teleal.cling.support.model.Res;
import org.teleal.cling.support.model.item.MusicTrack;
import org.teleal.cling.support.playqueue.callback.backupqueue.BackupQueueConstants;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.module:
//            CommonTuiSongModule

class val.deviceItem
    implements Runnable
{

    final CommonTuiSongModule this$0;
    final CommonDeviceItem val$deviceItem;

    protected PlayList createQueue(Playlist playlist, int i, int j)
    {
        PlayList playlist1 = new PlayList();
        playlist1.listName = DossUtils.getPlayListName(MyApplication.b());
        playlist1.listInfo.sourceName = "ximalaya";
        playlist1.listInfo.trackNumber = j - i;
        playlist1.listInfo.currentPage = 1;
        playlist1.listInfo.totalPages = 1;
        playlist1.listInfo.lastPlayIndex = 1;
        playlist1.listInfo.SwitchPageMode = 1;
        playlist1.tracks = new ArrayList();
        while (i < j) 
        {
            SoundInfo soundinfo = (SoundInfo)playlist.getData().get(i);
            Object obj = new MusicTrack("0", "0", soundinfo.title, soundinfo.nickname, soundinfo.albumName, soundinfo.nickname, new Res[] {
                new Res(new ProtocolInfo("http-get:*:audio/aac:DLNA.ORG_OP=01"), Long.valueOf(0L), soundinfo.playUrl64)
            });
            Track track;
            if (obj != null)
            {
                DIDLParser didlparser = new DIDLParser();
                DIDLContent didlcontent = new DIDLContent();
                didlcontent.addItem(((org.teleal.cling.support.model.item.Item) (obj)));
                try
                {
                    obj = didlparser.generate(didlcontent);
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    obj = null;
                }
            } else
            {
                obj = null;
            }
            if (obj == null)
            {
                return null;
            }
            obj = ((String) (obj)).replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"", "&quot;");
            track = new Track();
            track.URL = soundinfo.playUrl64;
            track.Id = String.valueOf(soundinfo.trackId);
            track.Source = "ximalaya";
            track.Metadata = ((String) (obj));
            playlist1.tracks.add(track);
            i++;
        }
        return playlist1;
    }

    public void run()
    {
        Logger.d("tuisong", "tuisong IN");
        Object obj = PlayListControl.getPlayListManager().getPlaylist();
        int j = ((Playlist) (obj)).getCurrPosition();
        if (j + 20 > ((Playlist) (obj)).size())
        {
            List list = ((Playlist) (obj)).getData();
            List list1 = ((Playlist) (obj)).loadMore();
            if (list1 != null)
            {
                list.addAll(list1);
            }
        }
        long l = ((Playlist) (obj)).getCurrSound().albumId;
        int i;
        if (j + 20 < ((Playlist) (obj)).size())
        {
            i = j + 20;
        } else
        {
            i = ((Playlist) (obj)).size();
        }
        Logger.d("tuisong", (new StringBuilder()).append("current Num:").append(j).toString());
        Logger.d("doss", (new StringBuilder()).append("\u5F53\u524D\u4F4D\u7F6E\uFF1A").append(j).toString());
        Logger.d("doss", (new StringBuilder()).append("\u8BBE\u5907\u5185\u8BB0\u5F55\u7684\u5217\u8868\u5F00\u5934\uFF1A").append(DossUtils.mDeviceStartNum).toString());
        Logger.d("doss", (new StringBuilder()).append("\u8BBE\u5907\u5185\u8BB0\u5F55\u7684\u5217\u8868\u7ED3\u5C3E\uFF1A").append(DossUtils.mDeviceEndNum).toString());
        Logger.d("doss", (new StringBuilder()).append("\u8BBE\u5907\u5185\u8BB0\u5F55\u7684\u6392\u5E8F\u987A\u5E8F\uFF1A\u5B9E\u9645\u7684\u6392\u5E8F\u987A\u5E8F-").append(CommonTuiSongModule.access$000(CommonTuiSongModule.this)).append(":").append(((Playlist) (obj)).isReverse()).toString());
        Logger.d("doss", (new StringBuilder()).append("\u8BBE\u5907\u5185\u8BB0\u5F55\u7684\u4E13\u8F91\u53F7\uFF1A\u5B9E\u9645\u7684\u4E13\u8F91\u53F7-").append(l).append(":").append(currAlbum).toString());
        Logger.d("doss", "\u64AD\u653E\u8282\u76EE\u5728\u5217\u8868\u5185");
        if (DossUtils.mDeviceStartNum <= j && j <= DossUtils.mDeviceEndNum && CommonTuiSongModule.access$000(CommonTuiSongModule.this) == ((Playlist) (obj)).isReverse() && l == currAlbum)
        {
            Logger.d("doss", "\u64AD\u653E\u8282\u76EE\u5728\u5217\u8868\u5185");
            Logger.d("tuisong", (new StringBuilder()).append("real Num:").append((j + 1) - DossUtils.mDeviceStartNum).toString());
            class _cls1 extends PlayQueueWithIndexActionCallback
            {

                final CommonTuiSongModule._cls1 this$1;

                public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
                {
                    if (callBack != null)
                    {
                        callBack.onFailed();
                    }
                    Logger.d("tuisong", "playwithIndex FAILURE");
                }

                public void success(ActionInvocation actioninvocation)
                {
                    Logger.d("tuisong", "playwithIndex SUCCESS");
                    CommonTuiSongModule.access$100(this$0);
                }

            _cls1(Service service, String s, int i)
            {
                this$1 = CommonTuiSongModule._cls1.this;
                super(service, s, i);
            }
            }

            getControlPoint().execute(new _cls1(val$deviceItem.getPQservice(), DossUtils.getPlayListName(MyApplication.b()), (j + 1) - DossUtils.mDeviceStartNum));
        } else
        {
            currAlbum = l;
            CommonTuiSongModule.access$002(CommonTuiSongModule.this, ((Playlist) (obj)).isReverse());
            Logger.d("doss", "\u64AD\u653E\u8282\u76EE\u4E0D\u5728\u5217\u8868\u5185\uFF0C\u91CD\u65B0\u63A8\u9001\u5217\u8868");
            DossUtils.mDeviceStartNum = j;
            DossUtils.mDeviceEndNum = i;
            obj = createQueue(((Playlist) (obj)), j, i);
            if (obj != null)
            {
                obj = BackupQueueConstants.getBackupQueueContext(((PlayList) (obj)));
                class _cls2 extends CreateQueueActionCallback
                {

                    final CommonTuiSongModule._cls1 this$1;

                    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
                    {
                        if (callBack != null)
                        {
                            callBack.onFailed();
                        }
                        Logger.d("tuisong", "CreateQueue Failure");
                    }

                    public void success(ActionInvocation actioninvocation)
                    {
                        Logger.d("tuisong", "CreateQueue Success");
                        class _cls1 extends PlayQueueWithIndexActionCallback
                        {

                            final _cls2 this$2;

                            public void failure(ActionInvocation actioninvocation1, UpnpResponse upnpresponse, String s)
                            {
                                if (callBack != null)
                                {
                                    callBack.onFailed();
                                }
                                Logger.d("tuisong", "playwithIndex FAILURE");
                            }

                            public void success(ActionInvocation actioninvocation1)
                            {
                                Logger.d("tuisong", "playwithIndex SUCCESS");
                                CommonTuiSongModule.access$100(this$0);
                            }

                                _cls1(Service service, String s, int i)
                                {
                                    this$2 = _cls2.this;
                                    super(service, s, i);
                                }
                        }

                        getControlPoint().execute(new _cls1(deviceItem.getPQservice(), DossUtils.getPlayListName(MyApplication.b()), 1));
                    }

            _cls2(Service service, String s)
            {
                this$1 = CommonTuiSongModule._cls1.this;
                super(service, s);
            }
                }

                getControlPoint().execute(new _cls2(val$deviceItem.getPQservice(), ((String) (obj))));
                return;
            }
        }
    }

    _cls2()
    {
        this$0 = final_commontuisongmodule;
        val$deviceItem = CommonDeviceItem.this;
        super();
    }
}
