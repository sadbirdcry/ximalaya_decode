// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import com.ximalaya.action.GetPositionInfoActionCallback;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.LinkedDeviceModel;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import com.ximalaya.ting.android.util.Logger;
import java.util.Map;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.support.model.PositionInfo;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            CoreController, DlnaManager, BaseDeviceItem, IDlnaController

public static class mContext extends GetPositionInfoActionCallback
{

    Context mContext;
    PlayerFragment mFragment;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        if (DlnaManager.getInstance(mContext).getLinkedDeviceModel() != null && DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem() != null)
        {
            class _cls2
                implements IActionCallBack
            {

                final CoreController.PositionCallBack this$0;

                public void onFailed()
                {
                }

                public void onSuccess(ActionModel actionmodel)
                {
                }

            _cls2()
            {
                this$0 = CoreController.PositionCallBack.this;
                super();
            }
            }

            actioninvocation = new _cls2();
            upnpresponse = new ActionModel();
            ((ActionModel) (upnpresponse)).result.put("device", DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem());
            ((ActionModel) (upnpresponse)).result.put("callback", actioninvocation);
            DlnaManager.getInstance(mContext).getController(DlnaManager.getInstance(mContext).getLinkedDeviceModel().getNowDeviceItem().getDlnaType()).onCommandFailed(upnpresponse);
        }
        Logger.d(CoreController.access$400(), "error:getPosition");
    }

    public void received(ActionInvocation actioninvocation, PositionInfo positioninfo)
    {
        final int duration = (int)positioninfo.getTrackDurationSeconds();
        final int elapsed = (int)positioninfo.getTrackElapsedSeconds();
        Logger.d("position", (new StringBuilder()).append("invocation : ").append(actioninvocation.getAction()).append("   positionInfo:  ").append(positioninfo.getTrackDurationSeconds()).append("   ").append(positioninfo.getTrackElapsedSeconds()).toString());
        class _cls1
            implements Runnable
        {

            final CoreController.PositionCallBack this$0;
            final int val$duration;
            final int val$elapsed;

            public void run()
            {
                mFragment.setDlnaSeekbarProgress(duration, elapsed);
            }

            _cls1()
            {
                this$0 = CoreController.PositionCallBack.this;
                duration = i;
                elapsed = j;
                super();
            }
        }

        mFragment.getActivity().runOnUiThread(new _cls1());
    }

    public _cls1(Service service, Context context, PlayerFragment playerfragment)
    {
        super(service);
        mFragment = playerfragment;
        mContext = context;
    }
}
