// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.content.Context;
import android.support.v4.app.Fragment;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.ManageFragment;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.doss.DossController;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import com.ximalaya.ting.android.fragment.device.dlna.model.LinkedDeviceModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.other.OtherController;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukeController;
import com.ximalaya.ting.android.fragment.device.dlna.tingshubao.TingshuController;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import com.ximalaya.ting.android.library.util.Logger;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.DeviceIdentity;
import org.teleal.cling.model.types.UDN;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            IDlnaController, CoreController, BaseDeviceItem, BaseDlnaController, 
//            PlayManageController, DeviceListFragment

public class DlnaManager
{
    public static interface OnDeviceChangedListener
    {

        public abstract void onDeviceChanged();
    }

    public static interface OnPlayServiceUpdateListener
    {

        public abstract void onPlayState(int i);

        public abstract void onSoundChanged(int i);
    }

    public static interface OnkeyEventListener
    {

        public abstract void onKeyAciton(KeyEvent keyevent);
    }


    public static final int SEARCH_TIME = 15;
    public static final String TAG = "WIFI";
    private static DlnaManager mDlnaManager;
    private Context mContext;
    private HashMap mContrllerMap;
    private CoreController mCoreController;
    private boolean mIsDlnaCanUse;
    CoreController.OnCoreControllerListener mOnCoreControllerListener;
    private CopyOnWriteArrayList mOnDeviceChangedListeners;
    private CopyOnWriteArrayList mOnkeyEventListeners;
    private OperationStorageModel mOperationStroageModel;
    private PlayManageController mPlayManageController;

    private DlnaManager(Context context)
    {
        mIsDlnaCanUse = false;
        mOnCoreControllerListener = new _cls1();
        mContext = context.getApplicationContext();
        mOnDeviceChangedListeners = new CopyOnWriteArrayList();
    }

    private void addDevice(BaseDeviceItem basedeviceitem)
    {
        Logger.d("doss", "addDevice IN");
        Object obj = checkDeviceType(basedeviceitem);
        if (obj != null)
        {
            obj = (IDlnaController)mContrllerMap.get(obj);
            if (obj != null)
            {
                ((IDlnaController) (obj)).addDeivce(basedeviceitem);
            }
        }
    }

    private com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType checkDeviceType(BaseDeviceItem basedeviceitem)
    {
        Object obj;
        if (mCoreController.getControlPoint() == null)
        {
            return null;
        }
        obj = basedeviceitem.getDevice().getIdentity().getUdn().toString();
        if (!((String) (obj)).contains("ximalaya-sn00-tsb0") && !((String) (obj)).contains("Hi") && !((String) (obj)).contains("NBCAST"))
        {
            break MISSING_BLOCK_LABEL_138;
        }
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorenter ;
        if (mContrllerMap == null)
        {
            mContrllerMap = new HashMap();
        }
        if (!mContrllerMap.containsKey(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao))
        {
            obj = new TingshuController(mContext, mCoreController.getControlPoint());
            mContrllerMap.put(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao, obj);
        }
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
        basedeviceitem.setDlnaType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao);
        return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao;
        basedeviceitem;
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
        throw basedeviceitem;
        if (!((String) (obj)).contains("ximalaya-doss-tb00") && !((String) (obj)).contains("69679") && !((String) (obj)).contains("713fa55e"))
        {
            break MISSING_BLOCK_LABEL_271;
        }
        Logger.d("doss", "DossController DossDeviceAdded");
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorenter ;
        if (mContrllerMap == null)
        {
            Logger.d("doss", "DossController mContrllerMap is null");
            mContrllerMap = new HashMap();
        }
        if (!mContrllerMap.containsKey(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss))
        {
            Logger.d("doss", "DossController mContrllerMap do not contain dossControl");
            obj = new DossController(mContext, mCoreController.getControlPoint());
            mContrllerMap.put(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss, obj);
        }
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
        basedeviceitem.setDlnaType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss);
        return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss;
        basedeviceitem;
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
        throw basedeviceitem;
        if (!((String) (obj)).contains("ximalaya-shuk"))
        {
            break MISSING_BLOCK_LABEL_386;
        }
        Logger.d("shuke", "ShukeController DossDeviceAdded");
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorenter ;
        if (mContrllerMap == null)
        {
            Logger.d("doss", "DossController mContrllerMap is null");
            mContrllerMap = new HashMap();
        }
        if (!mContrllerMap.containsKey(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi))
        {
            Logger.d("doss", "DossController mContrllerMap do not contain dossControl");
            ShukeController shukecontroller = new ShukeController(mContext, mCoreController.getControlPoint());
            mContrllerMap.put(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi, shukecontroller);
        }
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
        basedeviceitem.setDlnaType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi);
        return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi;
        basedeviceitem;
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
        throw basedeviceitem;
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorenter ;
        if (mContrllerMap == null)
        {
            mContrllerMap = new HashMap();
        }
        if (!mContrllerMap.containsKey(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.other))
        {
            OtherController othercontroller = new OtherController(mContext, mCoreController.getControlPoint());
            mContrllerMap.put(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.other, othercontroller);
        }
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
        basedeviceitem.setDlnaType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.other);
        return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.other;
        basedeviceitem;
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
        throw basedeviceitem;
    }

    private void clearAllDevice()
    {
        if (mContrllerMap != null)
        {
            for (Iterator iterator = mContrllerMap.entrySet().iterator(); iterator.hasNext(); ((IDlnaController)((java.util.Map.Entry)iterator.next()).getValue()).clearDeivce()) { }
        }
    }

    public static DlnaManager getAvaliableInstance()
    {
        return mDlnaManager;
    }

    public static DlnaManager getInstance(Context context)
    {
        if (mDlnaManager != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorenter ;
        if (mDlnaManager == null)
        {
            mDlnaManager = new DlnaManager(context);
        }
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
_L2:
        return mDlnaManager;
        context;
        com/ximalaya/ting/android/fragment/device/dlna/DlnaManager;
        JVM INSTR monitorexit ;
        throw context;
    }

    public static boolean isManagerValid()
    {
        return mDlnaManager != null;
    }

    public void bindService()
    {
        Logger.d("WIFI", "bindService IN");
        if (mCoreController == null)
        {
            mCoreController = new CoreController(mDlnaManager);
        }
        clearAllDevice();
        mCoreController.setOnCoreControllerListener(mOnCoreControllerListener);
        mCoreController.bindService(mContext);
    }

    public void change2Bendi()
    {
        Logger.d("doss", "DlnaManager change2Bendi IN");
        if (mCoreController != null)
        {
            mCoreController.change2Bendi();
        }
        if (mOnkeyEventListeners != null)
        {
            mOnkeyEventListeners.clear();
        }
    }

    public void changeVolume(boolean flag)
    {
        if (mCoreController != null)
        {
            mCoreController.volumePlus(flag);
        }
    }

    public void clear()
    {
        mCoreController.clear();
        mDlnaManager = null;
        Logger.d("WIFI", "DlnaManager clear finish");
    }

    public TingshuController createTingshuController()
    {
        TingshuController tingshucontroller = new TingshuController(mContext, mCoreController.getControlPoint());
        mContrllerMap.put(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao, tingshucontroller);
        return tingshucontroller;
    }

    public List getAllDevices()
    {
        ArrayList arraylist = new ArrayList();
        if (mContrllerMap == null)
        {
            return arraylist;
        }
        Iterator iterator = mContrllerMap.entrySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            if (entry.getValue() instanceof BaseDlnaController)
            {
                arraylist.addAll(((BaseDlnaController)entry.getValue()).mDeviceList);
            }
        } while (true);
        return arraylist;
    }

    public IDlnaController getController()
    {
        if (mContrllerMap != null)
        {
            return (IDlnaController)mContrllerMap.get(mOperationStroageModel.getNowDeviceItem().getDlnaType());
        } else
        {
            return null;
        }
    }

    public IDlnaController getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype)
    {
        if (mContrllerMap != null)
        {
            return (IDlnaController)mContrllerMap.get(devicetype);
        } else
        {
            return null;
        }
    }

    public CoreController getCoreController()
    {
        return mCoreController;
    }

    public List getDeviceListByType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype)
    {
        devicetype = (BaseDlnaController)getController(devicetype);
        if (devicetype == null)
        {
            return null;
        } else
        {
            return devicetype.getAllDevices();
        }
    }

    public LinkedDeviceModel getLinkedDeviceModel()
    {
        if (mCoreController != null)
        {
            return mCoreController.getLinkedDeviceModel();
        } else
        {
            return null;
        }
    }

    public int getNowPlayState()
    {
        if (mCoreController == null)
        {
            return 0;
        } else
        {
            return mCoreController.getNowPlayState();
        }
    }

    public OperationStorageModel getOperationStroageModel()
    {
        return mOperationStroageModel;
    }

    public PlayManageController getPlayManageController()
    {
        if (mPlayManageController == null)
        {
            mPlayManageController = new PlayManageController(mContext, mCoreController.getControlPoint(), this);
            mPlayManageController.setOnCoreControllerListener(mOnCoreControllerListener);
        }
        return mPlayManageController;
    }

    public boolean isDeviceLinked()
    {
        return getLinkedDeviceModel() != null;
    }

    public boolean isHasValidDevices()
    {
        List list = getAllDevices();
        return list != null && list.size() != 0;
    }

    public boolean isInWifiControlPage()
    {
        if (MyApplication.a() != null && (MyApplication.a() instanceof MainTabActivity2))
        {
            Object obj = MyApplication.a();
            int i = ((MainTabActivity2)obj).getManageFragment().mStacks.size();
            if (i >= 1)
            {
                obj = (Fragment)((SoftReference)((MainTabActivity2)obj).getManageFragment().mStacks.get(i - 1)).get();
                String s = obj.getClass().getPackage().getName();
                if (!(obj instanceof DeviceListFragment) && (s.startsWith("com.ximalaya.ting.android.fragment.device.doss") || s.startsWith("com.ximalaya.ting.android.fragment.device.shu") || s.startsWith("com.ximalaya.ting.android.fragment.device.dlna")))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isLinkedDeviceValid()
    {
        LinkedDeviceModel linkeddevicemodel = getLinkedDeviceModel();
        if (linkeddevicemodel == null)
        {
            return false;
        } else
        {
            return linkeddevicemodel.isValid();
        }
    }

    public boolean isOperatedDevicePlaying()
    {
        for (OperationStorageModel operationstoragemodel = getOperationStroageModel(); operationstoragemodel == null || !operationstoragemodel.isPlaying;)
        {
            return false;
        }

        return true;
    }

    public boolean isOperationPlaying()
    {
        if (mOperationStroageModel != null)
        {
            return mOperationStroageModel.isPlaying;
        } else
        {
            return false;
        }
    }

    public void notifyDeviceChanged()
    {
        if (mOnDeviceChangedListeners != null)
        {
            for (Iterator iterator = mOnDeviceChangedListeners.iterator(); iterator.hasNext(); ((OnDeviceChangedListener)iterator.next()).onDeviceChanged()) { }
        }
    }

    public void pause()
    {
        Logger.d("WIFI", "pause");
        if (mCoreController != null)
        {
            mCoreController.pause();
        }
    }

    public void play()
    {
        Logger.d("WIFI", "PALY IN");
        if (mCoreController != null)
        {
            mCoreController.play();
        }
    }

    public void playSound(ActionModel actionmodel)
    {
        if (mCoreController != null)
        {
            mCoreController.playSound(actionmodel);
        }
    }

    public void pre()
    {
        if (mCoreController != null)
        {
            mCoreController.pre();
        }
    }

    public void removeDeviceChangedListener(OnDeviceChangedListener ondevicechangedlistener)
    {
        try
        {
            mOnDeviceChangedListeners.remove(ondevicechangedlistener);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (OnDeviceChangedListener ondevicechangedlistener)
        {
            ondevicechangedlistener.printStackTrace();
        }
    }

    public void removeDeviceFromDLNALib(BaseDeviceItem basedeviceitem)
    {
        if (mCoreController != null)
        {
            mCoreController.removeDevice(basedeviceitem);
        }
    }

    public void removeOnkeyEventListener(OnkeyEventListener onkeyeventlistener)
    {
        if (mOnkeyEventListeners == null)
        {
            return;
        } else
        {
            mOnkeyEventListeners.remove(onkeyeventlistener);
            return;
        }
    }

    public void setController(IDlnaController idlnacontroller)
    {
    }

    public void setDeviceChangedListener(OnDeviceChangedListener ondevicechangedlistener)
    {
        if (!mOnDeviceChangedListeners.contains(ondevicechangedlistener))
        {
            mOnDeviceChangedListeners.add(ondevicechangedlistener);
        }
    }

    public void setNowOperationDevice(BaseDeviceItem basedeviceitem)
    {
        if (mOperationStroageModel == null)
        {
            mOperationStroageModel = new OperationStorageModel();
        }
        mOperationStroageModel.setNowDeviceItem(basedeviceitem);
    }

    public void setNowPlayState(int i)
    {
        if (mCoreController == null)
        {
            return;
        } else
        {
            mCoreController.setNowPlayState(i);
            return;
        }
    }

    public void setOnkeyEventListener(OnkeyEventListener onkeyeventlistener)
    {
        if (mOnkeyEventListeners == null)
        {
            mOnkeyEventListeners = new CopyOnWriteArrayList();
        }
        if (!mOnkeyEventListeners.contains(onkeyeventlistener))
        {
            mOnkeyEventListeners.add(onkeyeventlistener);
        }
    }

    public void setOperationStroageModel(OperationStorageModel operationstoragemodel)
    {
        mOperationStroageModel = operationstoragemodel;
    }

    public void setPlayManageController(PlayManageController playmanagecontroller)
    {
        mPlayManageController = playmanagecontroller;
    }

    public void setRegister(PlayerFragment playerfragment)
    {
        if (mCoreController != null)
        {
            mCoreController.setRegister(playerfragment);
        }
    }

    public void setSeekBar(int i, int j, double d)
    {
        if (mCoreController != null)
        {
            mCoreController.setSeekBar(i, j, d);
        }
    }

    public void setSeekBar(int i, int j, double d, IActionCallBack iactioncallback)
    {
        if (mCoreController != null)
        {
            mCoreController.setSeekBar(i, j, d, iactioncallback);
        }
    }

    public void startScanThread(int i)
    {
        if (mCoreController != null)
        {
            mCoreController.startScanThread(i);
        }
    }

    public void stop()
    {
        Logger.d("WIFI", "stop IN");
        if (mCoreController != null)
        {
            mCoreController.stop();
        }
    }

    public void stopScanThread()
    {
        if (mCoreController != null)
        {
            mCoreController.stopScanThread();
        }
    }

    public void tuisongDevice(BaseDeviceItem basedeviceitem)
    {
        if (mCoreController != null)
        {
            mCoreController.tuisongDevice(basedeviceitem);
        }
        if (mOperationStroageModel != null && mCoreController.getLinkedDeviceModel().getNowDeviceItem().getUdn().equals(mOperationStroageModel.getNowDeviceItem().getUdn()))
        {
            mOperationStroageModel.clearCurrentPlaying();
        }
    }

    public void unBindService()
    {
        mCoreController.unBindService();
    }





    private class _cls1
        implements CoreController.OnCoreControllerListener
    {

        final DlnaManager this$0;

        public void onDeviceAdded(BaseDeviceItem basedeviceitem)
        {
            addDevice(basedeviceitem);
            notifyDeviceChanged();
        }

        public void onDeviceRemoved(BaseDeviceItem basedeviceitem)
        {
            if (mOnDeviceChangedListeners != null)
            {
                for (basedeviceitem = mOnDeviceChangedListeners.iterator(); basedeviceitem.hasNext(); ((OnDeviceChangedListener)basedeviceitem.next()).onDeviceChanged()) { }
            }
        }

        public void onKeyAciton(KeyEvent keyevent)
        {
            if (mOnkeyEventListeners != null)
            {
                for (Iterator iterator = mOnkeyEventListeners.iterator(); iterator.hasNext(); ((OnkeyEventListener)iterator.next()).onKeyAciton(keyevent)) { }
            }
        }

        public void onNetworkChanged()
        {
            clearAllDevice();
        }

        _cls1()
        {
            this$0 = DlnaManager.this;
            super();
        }
    }

}
