// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.content.Context;
import com.ximalaya.subscribe.PlaySubscriptionCallback;
import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import com.ximalaya.ting.android.fragment.device.dlna.model.LinkedDeviceModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDlnaKeyModule;
import com.ximalaya.ting.android.fragment.device.dlna.subscription.MyPlaySubscriptionCallback;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.gena.GENASubscription;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            BasePlayManageController, BaseDeviceItem, DlnaManager, IDlnaController

public class PlayManageController extends BasePlayManageController
{

    private static final String TAG = com/ximalaya/ting/android/fragment/device/dlna/PlayManageController.getSimpleName();
    private Context mContext;
    private ControlPoint mControlPoint;
    private DlnaManager mDlnaManager;
    private IDlnaController mNowDlnaController;
    private CoreController.OnCoreControllerListener mOnCoreControllerListener;

    public PlayManageController(Context context, ControlPoint controlpoint, DlnaManager dlnamanager)
    {
        mContext = context;
        mControlPoint = controlpoint;
        mDlnaManager = dlnamanager;
    }

    public void change2Bendi()
    {
        if (mNowDlnaController == null)
        {
            mNowDlnaController = mDlnaManager.getController(mLinkedDeviceModel.getNowDeviceItem().getDlnaType());
        }
        if (mNowDlnaController != null)
        {
            mPlaySubscriptionCallback = null;
            mNowDlnaController.change2Bendi(this);
            mNowDlnaController = null;
            if (mPlaySubscriptionCallback != null)
            {
                mPlaySubscriptionCallback.end();
                mPlaySubscriptionCallback = null;
                return;
            }
        }
    }

    public LinkedDeviceModel getLinkedDeviceModel()
    {
        return mLinkedDeviceModel;
    }

    public void onHandleKey(GENASubscription genasubscription)
    {
        Logger.d("upnp", "handlePlayKey2 IN");
        if (mNowDlnaController == null)
        {
            mNowDlnaController = mDlnaManager.getController(mLinkedDeviceModel.getNowDeviceItem().getDlnaType());
        }
        KeyEvent keyevent = ((BaseDlnaKeyModule)mNowDlnaController.getModule(BaseDlnaKeyModule.NAME)).parse2Event(genasubscription, mLinkedDeviceModel.getNowDeviceItem());
        genasubscription = keyevent;
        if (keyevent == null)
        {
            genasubscription = new KeyEvent(mLinkedDeviceModel.getNowDeviceItem());
        }
        genasubscription.setDeviceItem(mLinkedDeviceModel.getNowDeviceItem());
        if (mOnCoreControllerListener != null)
        {
            mOnCoreControllerListener.onKeyAciton(genasubscription);
        }
    }

    public void setLinkedDeviceModel(LinkedDeviceModel linkeddevicemodel)
    {
        mLinkedDeviceModel = linkeddevicemodel;
    }

    void setOnCoreControllerListener(CoreController.OnCoreControllerListener oncorecontrollerlistener)
    {
        mOnCoreControllerListener = oncorecontrollerlistener;
    }

    public void tuisongDevice(BaseDeviceItem basedeviceitem)
    {
        if (mLinkedDeviceModel == null)
        {
            mLinkedDeviceModel = new LinkedDeviceModel();
        }
        mLinkedDeviceModel.setNowDeviceItem(basedeviceitem);
        mNowDlnaController = mDlnaManager.getController(basedeviceitem.getDlnaType());
        if (mNowDlnaController == null)
        {
            return;
        }
        if (mPlaySubscriptionCallback == null)
        {
            mPlaySubscriptionCallback = new MyPlaySubscriptionCallback(mLinkedDeviceModel.getAvService(), 600, this);
        }
        mControlPoint.execute(mPlaySubscriptionCallback);
        mNowDlnaController.tuisongExtra(this);
    }

}
