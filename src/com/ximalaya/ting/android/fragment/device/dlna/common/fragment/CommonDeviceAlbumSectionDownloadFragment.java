// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;
import com.ximalaya.model.mediamanager.DiskInfoItem;
import com.ximalaya.model.mediamanager.DiskInfoResult;
import com.ximalaya.ting.android.fragment.album.SectionContentAdapter;
import com.ximalaya.ting.android.fragment.device.album.AbsDeviceAlbumSectionDownloadFragment;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.common.adapter.CommonSectionContentAdapter;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonDownloadModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonManageTFModule;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDownloadModule;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseManageTFModule;
import com.ximalaya.ting.android.fragment.device.doss.DossUtils;
import com.ximalaya.ting.android.model.album.AlbumSectionModel;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonTFListSubMainFragment

public class CommonDeviceAlbumSectionDownloadFragment extends AbsDeviceAlbumSectionDownloadFragment
    implements android.view.View.OnClickListener
{

    private String TAG;
    protected CommonDeviceItem mDeviceItem;
    protected CommonDownloadModule mDownloadModule;
    protected CommonManageTFModule mManageTFModule;
    private TimerTask task;
    private Timer timer;

    public CommonDeviceAlbumSectionDownloadFragment()
    {
        TAG = getClass().getSimpleName();
    }

    protected double getAvailableStorage()
    {
        if (mDeviceItem.getStorage() == null || mDeviceItem.getStorage().items.size() == 0)
        {
            ActionModel actionmodel = ActionModel.createModel(new _cls2(mDeviceItem.getMMservice()));
            mManageTFModule.getMediaDiskInfo(actionmodel);
            return 0.0D;
        } else
        {
            return (double)((DiskInfoItem)mDeviceItem.getStorage().items.get(0)).freeSize;
        }
    }

    protected SectionContentAdapter getContentAdapter(AlbumSectionModel albumsectionmodel)
    {
        Logger.d(TAG, "getContentAdapter IN");
        if (mSectionContentAdapter == null)
        {
            Logger.d(TAG, "getContentAdapter new ");
            mSectionContentAdapter = new CommonSectionContentAdapter(getActivity(), albumsectionmodel);
        }
        mSectionContentAdapter.refreshDownloadStatus();
        return mSectionContentAdapter;
    }

    protected void initDeviceInfo()
    {
        mDeviceItem = (CommonDeviceItem)DlnaManager.getInstance(mCon).getOperationStroageModel().getNowDeviceItem();
        mDownloadModule = (CommonDownloadModule)DlnaManager.getInstance(mCon).getController().getModule(BaseDownloadModule.NAME);
        mManageTFModule = (CommonManageTFModule)DlnaManager.getInstance(mCon).getController().getModule(BaseManageTFModule.NAME);
        ActionModel actionmodel = ActionModel.createModel(new _cls1(mDeviceItem.getMMservice()));
        mManageTFModule.getMediaDiskInfo(actionmodel);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initDeviceInfo();
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 2: default 32
    //                   2131362504: 89
    //                   2131362513: 39;
           goto _L1 _L2 _L3
_L2:
        break MISSING_BLOCK_LABEL_89;
_L1:
        boolean flag = false;
_L4:
        List list;
        if (flag)
        {
            return;
        } else
        {
            super.onClick(view);
            return;
        }
_L3:
        list = mSectionContentAdapter.getCheckedTracks();
        if (list != null)
        {
            DossUtils.goDownload(getActivity().getApplicationContext(), mDeviceItem, mDownloadModule, list, null, com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss);
            CustomToast.showToast(mCon, "\u6B63\u5728\u51C6\u5907\u4E0B\u8F7D", 0);
        }
        flag = true;
          goto _L4
        CommonTFListSubMainFragment.showDownloading = true;
        goBackFragment(com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonTFListSubMainFragment);
        flag = true;
          goto _L4
    }

    public void onDestroy()
    {
        super.onDestroy();
        hidePlayButton();
    }

    protected void updateBottomBarInfo()
    {
        if (mSectionContentAdapter != null)
        {
            if (mSectionContentAdapter.getCount() > 0)
            {
                Iterator iterator = mSectionContentAdapter.getData().getTracks().getList().iterator();
                int i = 0;
                long l = 0L;
                do
                {
                    if (!iterator.hasNext())
                    {
                        break;
                    }
                    com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
                    if (track.getIsChecked())
                    {
                        l += track.getDownloadSize();
                        i++;
                    }
                } while (true);
                if (i > 0)
                {
                    mBottomInfoBar.setVisibility(0);
                    double d = getAvailableStorage();
                    if (d == 0.0D)
                    {
                        mBottomInfoBar.setVisibility(8);
                        return;
                    }
                    if ((double)l > d)
                    {
                        mBottomInfoBar.setText(0x7f0901f2);
                        return;
                    } else
                    {
                        String s = getString(0x7f0901f1, new Object[] {
                            Integer.valueOf(i), ToolUtil.formatFileSize(l), ToolUtil.formatFriendlyFileSize(d)
                        });
                        mBottomInfoBar.setText(s);
                        return;
                    }
                } else
                {
                    mBottomInfoBar.setVisibility(8);
                    return;
                }
            } else
            {
                mBottomInfoBar.setVisibility(8);
                return;
            }
        } else
        {
            mBottomInfoBar.setVisibility(8);
            return;
        }
    }

    protected void updateDownloadingCount(int i)
    {
        if (timer == null)
        {
            timer = new Timer();
            task = new _cls3();
            timer.schedule(task, 0L, 3000L);
        }
    }



    private class _cls2 extends GetMediaDiskInfoActionCallback
    {

        final CommonDeviceAlbumSectionDownloadFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "getMediaDiskInfo failure");
        }

        public void received(DiskInfoResult diskinforesult)
        {
            Logger.d("doss", "getMediaDiskInfo SUCCESS");
            mDeviceItem.setStorage(diskinforesult);
        }

        _cls2(Service service)
        {
            this$0 = CommonDeviceAlbumSectionDownloadFragment.this;
            super(service);
        }
    }


    private class _cls1 extends GetMediaDiskInfoActionCallback
    {

        final CommonDeviceAlbumSectionDownloadFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "getMediaDiskInfo failure");
        }

        public void received(DiskInfoResult diskinforesult)
        {
            Logger.d("doss", "getMediaDiskInfo SUCCESS");
            mDeviceItem.setStorage(diskinforesult);
        }

        _cls1(Service service)
        {
            this$0 = CommonDeviceAlbumSectionDownloadFragment.this;
            super(service);
        }
    }


    private class _cls3 extends TimerTask
    {

        final CommonDeviceAlbumSectionDownloadFragment this$0;

        public void run()
        {
            class _cls1
                implements Runnable
            {

                final _cls3 this$1;
                final int val$count;

                public void run()
                {
                    if (
// JavaClassFileOutputException: get_constant: invalid tag

                _cls1()
                {
                    this$1 = _cls3.this;
                    count = i;
                    super();
                }
            }

            final int count;
            if (CommonListUnFinishedTaskFragment.taskItems != null)
            {
                count = CommonListUnFinishedTaskFragment.taskItems.size();
            } else
            {
                count = 0;
            }
            if (getActivity() != null)
            {
                getActivity().runOnUiThread(new _cls1());
            }
        }

        _cls3()
        {
            this$0 = CommonDeviceAlbumSectionDownloadFragment.this;
            super();
        }
    }

}
