// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import android.content.Context;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.doss.DossDlnaActionCallBackImpl;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseSwitchSearchModeModule;
import java.util.Map;
import org.teleal.cling.controlpoint.ControlPoint;

public class CommonSwitchSearchModeModule extends BaseSwitchSearchModeModule
{

    public static String CALL_BACK = "callback";
    public static String DEVICE = "device";
    public static String SEARCH_MODE = "search_mode";
    BaseDeviceItem deviceItem;

    public CommonSwitchSearchModeModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public void switchSearchMode(ActionModel actionmodel)
    {
        final String mSearchMode = (String)actionmodel.result.get(SEARCH_MODE);
        deviceItem = (BaseDeviceItem)actionmodel.result.get(DEVICE);
        actionmodel = (DossDlnaActionCallBackImpl)actionmodel.result.get(CALL_BACK);
        (new Thread(new _cls1())).start();
    }


    private class _cls1
        implements Runnable
    {

        final CommonSwitchSearchModeModule this$0;
        final RequestParams val$apiParams;
        final DossDlnaActionCallBackImpl val$callBack;
        final String val$mSearchMode;

        public void run()
        {
            String s = (new StringBuilder()).append("http://").append(deviceItem.getIpAddress()).toString();
            s = (new StringBuilder()).append(s).append("/httpapi.asp?command=talkset:remark:").toString();
            s = (new StringBuilder()).append(s).append(mSearchMode).toString();
            if (f.a().a(s, apiParams, null, null).equals("OK"))
            {
                if (callBack != null)
                {
                    callBack.onSuccess(null);
                    return;
                }
                break MISSING_BLOCK_LABEL_128;
            }
            try
            {
                if (callBack != null)
                {
                    callBack.onFailed();
                    return;
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }

        _cls1()
        {
            this$0 = CommonSwitchSearchModeModule.this;
            mSearchMode = s;
            apiParams = requestparams;
            callBack = dossdlnaactioncallbackimpl;
            super();
        }
    }

}
