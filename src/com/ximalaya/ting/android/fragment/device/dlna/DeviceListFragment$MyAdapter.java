// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            DeviceListFragment, BaseDeviceItem, DeviceUtil

public class this._cls0 extends BaseAdapter
{

    final DeviceListFragment this$0;

    public int getCount()
    {
        return DeviceListFragment.access$000(DeviceListFragment.this).size();
    }

    public Object getItem(int i)
    {
        return DeviceListFragment.access$000(DeviceListFragment.this).get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(final int position, final View myDeviceItem, ViewGroup viewgroup)
    {
        viewgroup = myDeviceItem;
        if (myDeviceItem == null)
        {
            viewgroup = View.inflate(DeviceListFragment.access$100(DeviceListFragment.this), 0x7f030142, null);
        }
        myDeviceItem = (BaseDeviceItem)DeviceListFragment.access$000(DeviceListFragment.this).get(position);
        ((TextView)viewgroup.findViewById(0x7f0a02e8)).setText(DeviceUtil.getDeviceItemName(myDeviceItem).replaceAll("DOSS", "TingShuBao"));
        viewgroup.setClickable(true);
        class _cls1
            implements android.view.View.OnClickListener
        {

            final DeviceListFragment.MyAdapter this$1;
            final BaseDeviceItem val$myDeviceItem;
            final int val$position;

            public void onClick(View view)
            {
                if (ToolUtil.isFastClick())
                {
                    return;
                } else
                {
                    DeviceListFragment.access$202(this$0, myDeviceItem);
                    class _cls1
                        implements Runnable
                    {

                        final _cls1 this$2;

                        public void run()
                        {
                            class _cls1 extends TimerTask
                            {

                                final _cls1 this$3;

                                public void run()
                                {
                                    CustomToast.showToast(getActivity(), "\u8BBE\u5907\u5F02\u5E38,\u8BF7\u91CD\u8BD5", 0);
                                    if (DeviceListFragment.access$400(this$0) != null)
                                    {
                                        DeviceListFragment.access$400(this$0).dismiss();
                                        DeviceListFragment.access$402(this$0, null);
                                    }
                                }

                                    _cls1()
                                    {
                                        this$3 = _cls1.this;
                                        super();
                                    }
                            }

                            DeviceListFragment.access$302(this$0, new _cls1());
                            if (DeviceListFragment.access$500(this$0) != null)
                            {
                                DeviceListFragment.access$500(this$0).cancel();
                            }
                            DeviceListFragment.access$502(this$0, new Timer());
                            DeviceListFragment.access$500(this$0).schedule(DeviceListFragment.access$300(this$0), 15000L);
                            class _cls2
                                implements Runnable
                            {

                                final _cls1 this$3;

                                public void run()
                                {
                                    DeviceListFragment.access$402(this$0, new MyProgressDialog(getActivity()));
                                    DeviceListFragment.access$400(this$0).setTitle("\u63D0\u793A");
                                    DeviceListFragment.access$400(this$0).setMessage("\u6B63\u5728\u52AA\u529B\u52A0\u8F7D\u8BBE\u5907");
                                    DeviceListFragment.access$400(this$0).show();
                                }

                                    _cls2()
                                    {
                                        this$3 = _cls1.this;
                                        super();
                                    }
                            }

                            getActivity().runOnUiThread(new _cls2());
                            DeviceListFragment.access$602(this$0, position);
                            class _cls3
                                implements IActionCallBack
                            {

                                final _cls1 this$3;

                                public void onFailed()
                                {
                                    CustomToast.showToast(MyApplication.b(), "\u8BBE\u5907\u5F02\u5E38", 0);
                                    if (DeviceListFragment.access$400(this$0) != null)
                                    {
                                        DeviceListFragment.access$400(this$0).dismiss();
                                        DeviceListFragment.access$402(this$0, null);
                                    }
                                    if (DeviceListFragment.access$500(this$0) != null)
                                    {
                                        DeviceListFragment.access$500(this$0).cancel();
                                    }
                                    class _cls1
                                        implements Runnable
                                    {

                                        final _cls3 this$4;

                                        public void run()
                                        {
                                            getCurrentTypeDevices();
                                            DeviceListFragment.access$900(this$0).notifyDataSetChanged();
                                        }

                                            _cls1()
                                            {
                                                this$4 = _cls3.this;
                                                super();
                                            }
                                    }

                                    if (getActivity() != null)
                                    {
                                        getActivity().runOnUiThread(new _cls1());
                                    }
                                }

                                public void onSuccess(ActionModel actionmodel)
                                {
                                    DlnaManager.getInstance(DeviceListFragment.access$700(this$0)).setNowOperationDevice(DeviceListFragment.access$200(this$0));
                                    DeviceListFragment.access$800(this$0);
                                }

                                    _cls3()
                                    {
                                        this$3 = _cls1.this;
                                        super();
                                    }
                            }

                            DlnaManager.getInstance(getActivity().getApplicationContext()).getController(DeviceListFragment.access$1000(this$0)).initDeviceInfo(myDeviceItem, new _cls3());
                        }

                        _cls1()
                        {
                            this$2 = _cls1.this;
                            super();
                        }
                    }

                    (new Thread(new _cls1())).start();
                    return;
                }
            }

            _cls1()
            {
                this$1 = DeviceListFragment.MyAdapter.this;
                myDeviceItem = basedeviceitem;
                position = i;
                super();
            }
        }

        viewgroup.setOnClickListener(new _cls1());
        return viewgroup;
    }

    public _cls1()
    {
        this$0 = DeviceListFragment.this;
        super();
    }
}
