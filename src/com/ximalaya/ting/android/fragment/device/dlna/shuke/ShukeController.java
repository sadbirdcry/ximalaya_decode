// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonController;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonBindModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonDlnaKeyModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonDownloadModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonPlayModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonTuiSongModule;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonUpdateModule;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.Directory;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseVolumeControlModule;
import java.util.ArrayList;
import java.util.List;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeItemBindableModel, ShukeDeviceItem, ShukeManageTFModule, ShukeSwitchSearchModeModule, 
//            ShukeMessageModule

public class ShukeController extends CommonController
{

    public static final String DEVICE_SHUKE_HEADER = "ximalaya-shuk";
    private String TAG;
    private List directories;

    public ShukeController(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
        directories = null;
        TAG = com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeController.getSimpleName();
        directories = new ArrayList();
        context = new Directory("\u6545\u4E8B", "ximalaya/story", true);
        controlpoint = new Directory("\u513F\u6B4C", "ximalaya/song", true);
        Directory directory = new Directory("\u6027\u683C\u57F9\u517B", "ximalaya/learning", true);
        Directory directory1 = new Directory("\u5F55\u97F3", "ximalaya/record", false);
        directories.add(context);
        directories.add(controlpoint);
        directories.add(directory);
        directories.add(directory1);
    }

    public String[] getCheckedUdn()
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add("ximalaya-doss-shuke");
        return (String[])arraylist.toArray(new String[arraylist.size()]);
    }

    public com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getDeviceType()
    {
        return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi;
    }

    public Directory getDirectory(int i)
    {
        return (Directory)directories.get(i);
    }

    public Directory getDirectory(String s, boolean flag)
    {
        for (int i = 0; i < directories.size(); i++)
        {
            Directory directory = (Directory)directories.get(i);
            if (flag ? directory.getName().equals(s) : directory.getReqName().equals(s))
            {
                return directory;
            }
        }

        return null;
    }

    public int getDirectoryNum()
    {
        return directories.size();
    }

    protected BaseItemBindableModel initBindableModel()
    {
        return new ShukeItemBindableModel();
    }

    protected BaseDeviceItem initDevice(BaseDeviceItem basedeviceitem)
    {
        return new ShukeDeviceItem(basedeviceitem);
    }

    public void initModules()
    {
        setModule(new ShukeManageTFModule(mContext, getControlPoint()));
        setModule(new CommonDownloadModule(mContext, getControlPoint()));
        setModule(new CommonPlayModule(mContext, getControlPoint()));
        setModule(new CommonTuiSongModule(mContext, getControlPoint()));
        setModule(new CommonDlnaKeyModule(mContext, getControlPoint()));
        setModule(new CommonBindModule(mContext, getControlPoint()));
        setModule(new ShukeSwitchSearchModeModule(mContext, getControlPoint()));
        setModule(new CommonUpdateModule(mContext, getControlPoint()));
        setModule(new BaseVolumeControlModule(mContext, getControlPoint()));
        setModule(new ShukeMessageModule(mContext, getControlPoint()));
    }
}
