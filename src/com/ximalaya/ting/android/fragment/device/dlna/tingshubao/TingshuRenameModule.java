// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.tingshubao;

import android.content.Context;
import android.text.TextUtils;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseRenameModule;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.teleal.cling.controlpoint.ControlPoint;

public class TingshuRenameModule extends BaseRenameModule
{

    public TingshuRenameModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    private static String getRenameUrl(BaseDeviceItem basedeviceitem)
    {
        if (basedeviceitem != null)
        {
            if (!TextUtils.isEmpty(basedeviceitem = basedeviceitem.getIpAddress()))
            {
                return (new StringBuilder()).append("http://").append(basedeviceitem).append("/cgi-bin/router-info.cgi").toString();
            }
        }
        return null;
    }

    public void rename(BaseDeviceItem basedeviceitem, String s, IActionCallBack iactioncallback)
    {
        renameDevice(basedeviceitem, s, iactioncallback);
    }

    public void renameDevice(final BaseDeviceItem baseDeviceItem, final String realName, final IActionCallBack callBack)
    {
        (new Thread(new _cls1())).start();
    }

    public boolean renameDevicePost(BaseDeviceItem basedeviceitem, String s, IActionCallBack iactioncallback)
    {
        Object obj;
        Object obj1;
        int i;
        boolean flag;
        boolean flag1;
        boolean flag2;
        boolean flag3;
        int j;
        flag1 = true;
        flag2 = true;
        flag3 = true;
        flag = true;
        i = 0;
        j = 0;
        obj = getRenameUrl(basedeviceitem);
        obj1 = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(((org.apache.http.params.HttpParams) (obj1)), 3000);
        HttpConnectionParams.setSoTimeout(((org.apache.http.params.HttpParams) (obj1)), 3000);
        HttpConnectionParams.setSocketBufferSize(((org.apache.http.params.HttpParams) (obj1)), 8192);
        basedeviceitem = new DefaultHttpClient();
        basedeviceitem.setParams(((org.apache.http.params.HttpParams) (obj1)));
        obj = new HttpPost(((String) (obj)));
        obj1 = new ArrayList();
        ((List) (obj1)).add(new BasicNameValuePair("Devicename", s));
        ((HttpPost) (obj)).setEntity(new UrlEncodedFormEntity(((List) (obj1)), "UTF-8"));
        basedeviceitem = basedeviceitem.execute(((org.apache.http.client.methods.HttpUriRequest) (obj)));
        if (basedeviceitem == null) goto _L2; else goto _L1
_L1:
        s = basedeviceitem.getStatusLine();
        if (s == null) goto _L4; else goto _L3
_L3:
        int k = s.getStatusCode();
        if (k != 200) goto _L4; else goto _L5
_L5:
        byte abyte0[];
        basedeviceitem = basedeviceitem.getEntity().getContent();
        s = new ByteArrayOutputStream();
        abyte0 = new byte[1024];
_L8:
        j = basedeviceitem.read(abyte0);
        if (j == -1) goto _L7; else goto _L6
_L6:
        s.write(abyte0, 0, j);
          goto _L8
        basedeviceitem;
        boolean flag5;
        flag5 = true;
        flag = false;
_L20:
        i = ((flag) ? 1 : 0);
        basedeviceitem.printStackTrace();
        boolean flag4 = flag5;
        if (flag) goto _L10; else goto _L9
_L9:
        flag4 = flag5;
_L12:
        iactioncallback.onFailed();
_L10:
        return flag4;
_L7:
        i = ((flag3) ? 1 : 0);
        iactioncallback.onSuccess(null);
        i = ((flag3) ? 1 : 0);
        basedeviceitem.close();
        i = ((flag3) ? 1 : 0);
        s.close();
        i = 1;
        flag5 = true;
_L13:
        flag4 = flag5;
        if (i != 0) goto _L10; else goto _L11
_L11:
        flag4 = flag5;
          goto _L12
_L4:
        iactioncallback.onFailed();
_L2:
        flag5 = false;
        i = j;
          goto _L13
        basedeviceitem;
        flag = false;
        flag5 = false;
_L19:
        i = ((flag) ? 1 : 0);
        basedeviceitem.printStackTrace();
        flag4 = flag5;
        if (flag) goto _L10; else goto _L14
_L14:
        flag4 = flag5;
          goto _L12
_L18:
        i = ((flag) ? 1 : 0);
        basedeviceitem.printStackTrace();
        flag4 = flag5;
        if (flag) goto _L10; else goto _L15
_L15:
        flag4 = flag5;
          goto _L12
        basedeviceitem;
_L17:
        if (i == 0)
        {
            iactioncallback.onFailed();
        }
        throw basedeviceitem;
        basedeviceitem;
        if (true) goto _L17; else goto _L16
_L16:
        basedeviceitem;
        flag5 = true;
        flag = false;
          goto _L18
        basedeviceitem;
        flag5 = true;
        flag = flag2;
          goto _L18
        basedeviceitem;
        flag5 = true;
        flag = false;
          goto _L19
        basedeviceitem;
        flag5 = true;
        flag = flag1;
          goto _L19
        basedeviceitem;
        flag = false;
        flag5 = false;
          goto _L20
        basedeviceitem;
        flag5 = true;
          goto _L20
        basedeviceitem;
        flag = false;
        flag5 = false;
          goto _L18
    }

    private class _cls1
        implements Runnable
    {

        final TingshuRenameModule this$0;
        final BaseDeviceItem val$baseDeviceItem;
        final IActionCallBack val$callBack;
        final String val$realName;

        public void run()
        {
            renameDevicePost(baseDeviceItem, realName, callBack);
        }

        _cls1()
        {
            this$0 = TingshuRenameModule.this;
            baseDeviceItem = basedeviceitem;
            realName = s;
            callBack = iactioncallback;
            super();
        }
    }

}
