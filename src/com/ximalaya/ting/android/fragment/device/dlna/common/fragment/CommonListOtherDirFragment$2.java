// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import com.ximalaya.action.CreateQueueActionCallback;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.module.BasePlayModule;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonListOtherDirFragment

class val.position extends CreateQueueActionCallback
{

    final CommonListOtherDirFragment this$0;
    final int val$position;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        Logger.d("doss", "CreateQueue Failure");
    }

    public void success(ActionInvocation actioninvocation)
    {
        Logger.d("doss", "CreateQueue Success");
        class _cls1 extends PlayQueueWithIndex
        {

            final CommonListOtherDirFragment._cls2 this$1;

            public void failure(ActionInvocation actioninvocation1, UpnpResponse upnpresponse, String s)
            {
                Log.d("doss", "playwithIndex FAILURE");
            }

            public void success(ActionInvocation actioninvocation1)
            {
                Log.d("doss", "playwithIndex SUCCESS");
                CommonListOtherDirFragment.access$300(this$0);
            }

            _cls1(Service service, String s, int i)
            {
                this$1 = CommonListOtherDirFragment._cls2.this;
                super(service, s, i);
            }
        }

        actioninvocation = ActionModel.createModel(new _cls1(((CommonDeviceItem)CommonListOtherDirFragment.access$100(CommonListOtherDirFragment.this)).getPQservice(), "localUSBDISK", val$position % CommonListOtherDirFragment.access$200(CommonListOtherDirFragment.this).pageSize + 1));
        CommonListOtherDirFragment.access$400(CommonListOtherDirFragment.this).playWithIndex(actioninvocation);
    }

    _cls1(String s, int i)
    {
        this$0 = final_commonlistotherdirfragment;
        val$position = i;
        super(Service.this, s);
    }
}
