// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import com.ximalaya.ting.android.fragment.device.dlna.model.KeyEvent;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            DlnaManager, BaseDeviceItem

class this._cls0
    implements .OnCoreControllerListener
{

    final DlnaManager this$0;

    public void onDeviceAdded(BaseDeviceItem basedeviceitem)
    {
        DlnaManager.access$000(DlnaManager.this, basedeviceitem);
        notifyDeviceChanged();
    }

    public void onDeviceRemoved(BaseDeviceItem basedeviceitem)
    {
        if (DlnaManager.access$100(DlnaManager.this) != null)
        {
            for (basedeviceitem = DlnaManager.access$100(DlnaManager.this).iterator(); basedeviceitem.hasNext(); ((DeviceChangedListener)basedeviceitem.next()).onDeviceChanged()) { }
        }
    }

    public void onKeyAciton(KeyEvent keyevent)
    {
        if (DlnaManager.access$300(DlnaManager.this) != null)
        {
            for (Iterator iterator = DlnaManager.access$300(DlnaManager.this).iterator(); iterator.hasNext(); ((keyEventListener)iterator.next()).onKeyAciton(keyevent)) { }
        }
    }

    public void onNetworkChanged()
    {
        DlnaManager.access$200(DlnaManager.this);
    }

    ()
    {
        this$0 = DlnaManager.this;
        super();
    }
}
