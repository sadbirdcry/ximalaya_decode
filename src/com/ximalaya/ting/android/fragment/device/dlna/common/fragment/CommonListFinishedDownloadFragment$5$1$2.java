// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import com.ximalaya.action.DeleteMediaActionCallback;
import com.ximalaya.ting.android.util.Logger;
import java.util.List;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonListFinishedDownloadFragment

class this._cls2 extends DeleteMediaActionCallback
{

    final ._cls0 this$2;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        Logger.d("doss", "deleteMedia failure");
        refreshMediaList();
        CommonListFinishedDownloadFragment.access$1000(_fld0);
    }

    public void success(ActionInvocation actioninvocation)
    {
        Logger.d("doss", "deleteMedia SUCCESS");
        getItems().clear();
        refreshMediaList();
        CommonListFinishedDownloadFragment.access$1100(_fld0);
    }

    (Service service, String s, String s1, String s2, int i)
    {
        this$2 = this._cls2.this;
        super(service, s, s1, s2, i);
    }
}
