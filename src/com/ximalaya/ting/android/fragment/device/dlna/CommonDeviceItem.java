// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import com.ximalaya.activity.DeviceItem;
import com.ximalaya.model.mediamanager.DiskInfoResult;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.ServiceType;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            BaseBindableDeviceItem, BaseCurrentPlayingModel, BaseDeviceItem

public abstract class CommonDeviceItem extends BaseBindableDeviceItem
{

    protected BaseCurrentPlayingModel mCurrentPlayingModel;
    protected Service mMMservice;
    protected Service mPQservice;
    private DiskInfoResult storage;

    public CommonDeviceItem(BaseDeviceItem basedeviceitem)
    {
        super(basedeviceitem);
        mCurrentPlayingModel = new BaseCurrentPlayingModel();
        storage = null;
    }

    public CommonDeviceItem(Device device, String as[])
    {
        super(device, as);
        mCurrentPlayingModel = new BaseCurrentPlayingModel();
        storage = null;
    }

    public abstract BaseItemBindableModel getBaseBindableModel();

    public BaseCurrentPlayingModel getCurrentPlayingModel()
    {
        return mCurrentPlayingModel;
    }

    public Service getMMservice()
    {
        if (mMMservice == null)
        {
            mMMservice = mDeviceItem.getDevice().findService(new ServiceType("schemas-wiimu-com", "MediaManager"));
        }
        return mMMservice;
    }

    public Service getPQservice()
    {
        if (mPQservice == null)
        {
            mPQservice = mDeviceItem.getDevice().findService(new ServiceType("schemas-wiimu-com", "PlayQueue"));
        }
        return mPQservice;
    }

    public DiskInfoResult getStorage()
    {
        return storage;
    }

    public void setCurrentPlayingModel(BaseCurrentPlayingModel basecurrentplayingmodel)
    {
        mCurrentPlayingModel = basecurrentplayingmodel;
    }

    public void setMMservice(Service service)
    {
        mMMservice = service;
    }

    public void setPQservice(Service service)
    {
        mPQservice = service;
    }

    public void setStorage(DiskInfoResult diskinforesult)
    {
        storage = diskinforesult;
    }
}
