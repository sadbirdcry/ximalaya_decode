// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.model;

import com.ximalaya.ting.android.model.album.AlbumModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class BaseItemBindableModel
{

    protected HashMap mAlbums;
    protected List mKeyNames;

    protected BaseItemBindableModel()
    {
        mAlbums = new HashMap();
        mKeyNames = new ArrayList();
        mKeyNames.clear();
        mKeyNames.add("\u300C\u9891\u9053\u4E00\u300D");
        mKeyNames.add("\u300C\u9891\u9053\u4E8C\u300D");
        mKeyNames.add("\u300C\u9891\u9053\u4E09\u300D");
        mKeyNames.add("\u300C\u9891\u9053\u56DB\u300D");
        mKeyNames.add("\u300C\u9891\u9053\u4E94\u300D");
        mKeyNames.add("\u300C\u9891\u9053\u516D\u300D");
    }

    protected BaseItemBindableModel(List list)
    {
        mAlbums = new HashMap();
        mKeyNames = list;
    }

    public abstract int getAlbumNum();

    public HashMap getAlbums()
    {
        return mAlbums;
    }

    public String getKeyName(int i)
    {
        if (mKeyNames != null && mKeyNames.size() > i)
        {
            return (String)mKeyNames.get(i);
        } else
        {
            return "";
        }
    }

    public void setAlbums(int i, AlbumModel albummodel)
    {
        mAlbums.put(Integer.valueOf(i), albummodel);
    }

    public void setKeyNames(List list)
    {
        mKeyNames = list;
    }
}
