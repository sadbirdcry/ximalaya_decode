// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.model;

import com.ximalaya.model.mediamanager.Item;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.model.sound.AlbumSoundModelNew;

public class OperationStorageModel
{

    private Item currentPlayingItem;
    private AlbumSoundModelNew currentPlayingSoundModel;
    public boolean isPlaying;
    BaseDeviceItem mNowDeviceItem;
    private int mNowVolume;

    public OperationStorageModel()
    {
        isPlaying = false;
    }

    public void clearCurrentPlaying()
    {
        currentPlayingSoundModel = null;
        currentPlayingItem = null;
        isPlaying = false;
    }

    public Item getCurrentPlayingItem()
    {
        return currentPlayingItem;
    }

    public AlbumSoundModelNew getCurrentPlayingSoundModel()
    {
        return currentPlayingSoundModel;
    }

    public BaseDeviceItem getNowDeviceItem()
    {
        return mNowDeviceItem;
    }

    public int getNowVolume()
    {
        return mNowVolume;
    }

    public void setCurrentPlaying(Item item)
    {
        currentPlayingSoundModel = null;
        currentPlayingItem = item;
    }

    public void setCurrentPlaying(AlbumSoundModelNew albumsoundmodelnew)
    {
        currentPlayingSoundModel = albumsoundmodelnew;
        currentPlayingItem = null;
    }

    public void setNowDeviceItem(BaseDeviceItem basedeviceitem)
    {
        mNowDeviceItem = basedeviceitem;
    }

    public void setNowVolume(int i)
    {
        mNowVolume = i;
    }
}
