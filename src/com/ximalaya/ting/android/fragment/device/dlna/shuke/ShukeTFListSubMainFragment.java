// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.os.Bundle;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonListFinishedDownloadFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonTFListSubMainFragment;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseManageTFModule;
import com.ximalaya.ting.android.fragment.device.shu.CategoryFragment;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm;
import com.ximalaya.ting.android.fragment.finding2.category.CategoryTagFragmentNew;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeListFinishedDownloadFragment, ShukeUtil

public class ShukeTFListSubMainFragment extends CommonTFListSubMainFragment
{

    public ShukeTFListSubMainFragment()
    {
    }

    protected CommonListFinishedDownloadFragment getFinishedDownloadFragment()
    {
        return new ShukeListFinishedDownloadFragment();
    }

    protected void initToCategoryFragmentBundle(Bundle bundle)
    {
        bundle.putInt("from", 9);
    }

    protected void toCategoryFragment()
    {
        Bundle bundle;
        (new Thread(new _cls1())).start();
        bundle = new Bundle();
        bundle.putBoolean("showFocus", false);
        initToCategoryFragmentBundle(bundle);
        if (!ShukeUtil.isLockBind(mCon)) goto _L2; else goto _L1
_L1:
        Object obj;
        int i;
        obj = XiMaoComm.loadCategoryInfo(mCon);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_197;
        }
        i = 0;
_L5:
        if (i >= ((List) (obj)).size())
        {
            break MISSING_BLOCK_LABEL_197;
        }
        if (!((FindingCategoryModel)((List) (obj)).get(i)).getTitle().equals("\u513F\u7AE5")) goto _L4; else goto _L3
_L3:
        obj = (FindingCategoryModel)((List) (obj)).get(i);
_L6:
        if (obj != null)
        {
            bundle.putString("title", ((FindingCategoryModel) (obj)).getTitle());
            bundle.putString("categoryId", (new StringBuilder()).append(((FindingCategoryModel) (obj)).getId()).append("").toString());
            bundle.putString("categoryName", ((FindingCategoryModel) (obj)).getName());
            bundle.putString("contentType", "album");
            bundle.putBoolean("isSerialized", ((FindingCategoryModel) (obj)).isFinished());
            startFragment(com/ximalaya/ting/android/fragment/finding2/category/CategoryTagFragmentNew, bundle);
        }
        return;
_L4:
        i++;
          goto _L5
_L2:
        startFragment(com/ximalaya/ting/android/fragment/device/shu/CategoryFragment, bundle);
        return;
        obj = null;
          goto _L6
    }





    private class _cls1
        implements Runnable
    {

        final ShukeTFListSubMainFragment this$0;

        public void run()
        {
            int i = 0;
            while (((CommonDeviceItem)ShukeTFListSubMainFragment.this.Object).getStorage() == null && i < 3) 
            {
                class _cls1 extends GetMediaDiskInfoActionCallback
                {

                    final _cls1 this$1;

                    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
                    {
                        Logger.d("doss", "getMediaDiskInfo failure");
                    }

                    public void received(DiskInfoResult diskinforesult)
                    {
                        Logger.d("doss", "getMediaDiskInfo SUCCESS");
                        ((CommonDeviceItem)
// JavaClassFileOutputException: get_constant: invalid tag

                _cls1(Service service)
                {
                    this$1 = _cls1.this;
                    super(service);
                }
                }

                ActionModel actionmodel;
                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
                actionmodel = ActionModel.createModel(new _cls1(((CommonDeviceItem)ShukeTFListSubMainFragment.this.Object).getMMservice()));
                
// JavaClassFileOutputException: get_constant: invalid tag

        _cls1()
        {
            this$0 = ShukeTFListSubMainFragment.this;
            super();
        }
    }

}
