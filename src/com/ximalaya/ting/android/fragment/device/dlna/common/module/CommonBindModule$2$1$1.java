// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import com.ximalaya.action.SetKeyMappingActionCallback;
import com.ximalaya.ting.android.util.Logger;
import java.util.Timer;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.module:
//            CommonBindModule

class this._cls2 extends SetKeyMappingActionCallback
{

    final _cls1 this$2;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        CommonBindModule.access$000(_fld0, "\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
        Logger.d(CommonBindModule.access$200(_fld0), "SetKeyMapping failure");
    }

    public void success(ActionInvocation actioninvocation)
    {
        class _cls1 extends TimerTask
        {

            final CommonBindModule._cls2._cls1._cls1 this$3;

            public void run()
            {
                CommonBindModule.access$000(this$0, "\u7ED1\u5B9A\u4E13\u8F91\u6210\u529F");
                Logger.d(CommonBindModule.access$100(this$0), "SetKeyMapping Success");
                mDeviceItem.getBaseBindableModel().setAlbums(mBindCommand.mChannelId, mBindCommand.mAlbumModel);
                freshBindFragment();
            }

            _cls1()
            {
                this$3 = CommonBindModule._cls2._cls1._cls1.this;
                super();
            }
        }

        (new Timer()).schedule(new _cls1(), 500L);
    }

    _cls1(Service service, String s)
    {
        this$2 = this._cls2.this;
        super(service, s);
    }
}
