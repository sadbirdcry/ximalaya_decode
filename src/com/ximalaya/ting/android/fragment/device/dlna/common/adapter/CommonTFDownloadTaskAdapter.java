// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.model.mediamanager.MediaInfo;
import com.ximalaya.model.mediamanager.Task;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDownloadModule;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

public class CommonTFDownloadTaskAdapter extends BaseAdapter
{
    class ViewHolder
    {

        TextView mContextLength;
        RelativeLayout mDeleteTask;
        TextView mLength;
        TextView mLine;
        ImageView mStatusImage;
        TextView mStatusName;
        TextView mTaskName;
        final CommonTFDownloadTaskAdapter this$0;

        ViewHolder()
        {
            this$0 = CommonTFDownloadTaskAdapter.this;
            super();
        }
    }


    List listItem;
    private Context mContext;
    private LayoutInflater mInflater;

    public CommonTFDownloadTaskAdapter(Context context, List list)
    {
        mContext = context;
        listItem = list;
        mInflater = LayoutInflater.from(mContext);
    }

    public void deleteItem(int i)
    {
        Task task = (Task)listItem.get(i);
        CommonDeviceItem commondeviceitem = (CommonDeviceItem)DlnaManager.getInstance(mContext).getOperationStroageModel().getNowDeviceItem();
        ((BaseDownloadModule)DlnaManager.getInstance(mContext).getController().getModule(BaseDownloadModule.NAME)).deleteDownloadTask(ActionModel.createModel(new _cls2(commondeviceitem.getMMservice(), task.taskID, true)));
        listItem.remove(i);
        notifyDataSetChanged();
    }

    public int getCount()
    {
        return listItem.size();
    }

    public Object getItem(int i)
    {
        return null;
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
        ViewHolder viewholder;
        int i;
        i = 0x7f0201ea;
        View view1;
        if (view == null)
        {
            viewholder = new ViewHolder();
            view1 = mInflater.inflate(0x7f03011f, viewgroup, false);
            viewholder.mTaskName = (TextView)view1.findViewById(0x7f0a0486);
            viewholder.mStatusName = (TextView)view1.findViewById(0x7f0a0221);
            viewholder.mStatusImage = (ImageView)view1.findViewById(0x7f0a0220);
            viewholder.mDeleteTask = (RelativeLayout)view1.findViewById(0x7f0a0485);
            viewholder.mLine = (TextView)view1.findViewById(0x7f0a0319);
            viewholder.mContextLength = (TextView)view1.findViewById(0x7f0a0489);
            viewholder.mLength = (TextView)view1.findViewById(0x7f0a0488);
            view1.setTag(viewholder);
        } else
        {
            viewholder = (ViewHolder)view.getTag();
            view1 = view;
        }
        if (((Task)listItem.get(position)).mediaInfo.title == null) goto _L2; else goto _L1
_L1:
        ((Task)listItem.get(position)).status;
        JVM INSTR tableswitch -1 4: default 220
    //                   -1 503
    //                   0 513
    //                   1 519
    //                   2 529
    //                   3 541
    //                   4 552;
           goto _L3 _L4 _L5 _L6 _L7 _L8 _L9
_L3:
        i = 0x7f0201e7;
        view = "\u7B49\u5F85\u4E0B\u8F7D";
_L17:
        ((Task)listItem.get(position)).code;
        JVM INSTR tableswitch 700 706: default 284
    //                   700 559
    //                   701 566
    //                   702 573
    //                   703 580
    //                   704 587
    //                   705 284
    //                   706 594;
           goto _L10 _L11 _L12 _L13 _L14 _L15 _L10 _L16
_L10:
        viewgroup = "";
_L18:
        viewholder.mTaskName.setText(((Task)listItem.get(position)).mediaInfo.title.toString());
        viewholder.mDeleteTask.setVisibility(0);
        if (view.equals("\u4E0B\u8F7D\u5931\u8D25"))
        {
            viewholder.mStatusName.setText((new StringBuilder()).append(view).append(":").append(viewgroup).toString());
        } else
        {
            viewholder.mStatusName.setText(view);
        }
        if (view.equals("\u4E0B\u8F7D\u4E2D..."))
        {
            viewholder.mLength.setText(ToolUtil.formatFriendlyFileSize(((Task)listItem.get(position)).length));
            viewholder.mContextLength.setText(ToolUtil.formatFriendlyFileSize(((Task)listItem.get(position)).contextLength));
            viewholder.mLength.setVisibility(0);
            viewholder.mContextLength.setVisibility(0);
            viewholder.mLine.setVisibility(0);
        } else
        {
            viewholder.mLength.setVisibility(8);
            viewholder.mContextLength.setVisibility(8);
            viewholder.mLine.setVisibility(8);
        }
        viewholder.mStatusImage.setImageResource(i);
        viewholder.mDeleteTask.setOnClickListener(new _cls1());
_L2:
        return view1;
_L4:
        view = "\u4E0B\u8F7D\u5931\u8D25";
        i = 0x7f0201e8;
          goto _L17
_L5:
        view = "\u7B49\u5F85\u4E0B\u8F7D";
          goto _L17
_L6:
        i = 0x7f0201e7;
        view = "\u4E0B\u8F7D\u4E2D...";
          goto _L17
_L7:
        view = "\u6682\u505C\u4E2D";
        i = 0x7f0201e9;
          goto _L17
_L8:
        i = 0x7f0201e7;
        view = "\u4E0B\u8F7D\u5B8C\u6210";
          goto _L17
_L9:
        view = "\u51C6\u5907\u4E0B\u8F7D";
          goto _L17
_L11:
        viewgroup = "\u8FDE\u63A5\u51FA\u9519";
          goto _L18
_L12:
        viewgroup = "\u6CA1\u6709\u5B58\u50A8\u8BBE\u5907";
          goto _L18
_L13:
        viewgroup = "\u521B\u5EFA\u6587\u4EF6\u5931\u8D25\uFF0C\u6216\u8005\u7A7A\u95F4\u4E0D\u8DB3";
          goto _L18
_L14:
        viewgroup = "\u65E0\u610F\u4E49";
          goto _L18
_L15:
        viewgroup = "\u4E0B\u8F7D\u8D85\u65F6";
          goto _L18
_L16:
        viewgroup = "\u5269\u4F59\u7A7A\u95F4\u4E0D\u8DB3";
          goto _L18
    }

    private class _cls2 extends DeleteDownloadTaskActionCallback
    {

        final CommonTFDownloadTaskAdapter this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "deleteDownloadTask failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "deleteDownloadTask SUCCESS");
        }

        _cls2(Service service, String s, boolean flag)
        {
            this$0 = CommonTFDownloadTaskAdapter.this;
            super(service, s, flag);
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final CommonTFDownloadTaskAdapter this$0;
        final int val$position;

        public void onClick(View view)
        {
            class _cls1
                implements Runnable
            {

                final _cls1 this$1;

                public void run()
                {
                    class _cls1
                        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                    {

                        final _cls1 this$2;

                        public void onExecute()
                        {
                            deleteItem(position);
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    (new DialogBuilder(MyApplication.a())).setMessage((new StringBuilder()).append("\u786E\u5B9A\u5220\u9664\u58F0\u97F3").append(((Task)listItem.get(position)).mediaInfo.title).append("\uFF1F").toString()).setOkBtn(new _cls1()).showConfirm();
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            MyApplication.a().runOnUiThread(new _cls1());
        }

        _cls1()
        {
            this$0 = CommonTFDownloadTaskAdapter.this;
            position = i;
            super();
        }
    }

}
