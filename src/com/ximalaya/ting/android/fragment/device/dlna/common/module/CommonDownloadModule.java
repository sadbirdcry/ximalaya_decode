// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDownloadModule;
import com.ximalaya.ting.android.library.util.Logger;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.controlpoint.ControlPoint;

public class CommonDownloadModule extends BaseDownloadModule
{

    public CommonDownloadModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public void browseDownloadTask(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "browseDownloadTask IN");
        getControlPoint().execute(actionmodel);
    }

    public void createDownloadTask(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "createDownloadTask IN");
        getControlPoint().execute(actionmodel);
    }

    public void deleteDownloadTask(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "deleteDownloadTask IN");
        getControlPoint().execute(actionmodel);
    }

    public void pauseDownloadTask(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "pauseDownloadTask IN");
        getControlPoint().execute(actionmodel);
    }

    public void resumeDownloadTask(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "resumeDownloadTask IN");
        getControlPoint().execute(actionmodel);
    }
}
