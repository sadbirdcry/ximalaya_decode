// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonSettingFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonUpdateModule;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseMessageModule;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseUpdateModule;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import org.teleal.cling.model.types.UDN;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeUtil, ShukeMessageModule

public class ShukeSettingFragment extends CommonSettingFragment
    implements android.view.View.OnClickListener
{

    public ShukeSettingFragment()
    {
    }

    protected String getSwitchModeCommand(boolean flag)
    {
        if (flag)
        {
            return "0";
        } else
        {
            return "1";
        }
    }

    protected CommonUpdateModule getUpdateModule()
    {
        return (CommonUpdateModule)DlnaManager.getInstance(mCon).getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi).getModule(BaseUpdateModule.NAME);
    }

    protected void initView()
    {
        super.initView();
        mSetting4Name.setText("\u4EC5\u7ED1\u5B9A\u548C\u4E0B\u8F7D\u513F\u7AE5\u7C7B\u8282\u76EE");
        mSetting4.setVisibility(0);
        mSetting4Switch.setChecked(ShukeUtil.isLockBind(mCon));
        mSetting4Switch.setOnCheckedChangeListener(new _cls2());
        mSetting5Name.setText("\u7ED1\u5B9A\u8D26\u53F7");
        mSetting5.setVisibility(0);
        mSetting5Image.setImageDrawable(mCon.getResources().getDrawable(0x7f020311));
        mSetting5Switch.setVisibility(4);
        mSetting5Info.setVisibility(0);
        setLinkedAccount();
        mSetting5.setOnClickListener(new _cls3());
        mSetting1Name.setText("\u4EC5\u8BED\u97F3\u641C\u7D22\u513F\u7AE5\u7C7B\u8282\u76EE");
        mSetting1Image.setImageDrawable(mCon.getResources().getDrawable(0x7f02020d));
    }

    protected boolean isSearchXimalayaOnly()
    {
        return ShukeUtil.isSearchChildrenOnly(mCon);
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        Logger.d("message", "DeviceBindingListFragment onActivityResult");
        super.onActivityResult(i, j, intent);
        if (UserInfoMannage.hasLogined() && getActivity() != null)
        {
            ((ShukeMessageModule)DlnaManager.getInstance(getActivity().getApplicationContext()).getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi).getModule(BaseMessageModule.NAME)).createAlbums();
            ShukeUtil.setNotFirstUse(getActivity().getApplicationContext(), DlnaManager.getInstance(mCon).getOperationStroageModel().getNowDeviceItem().getUdn().getIdentifierString());
        }
        setLinkedAccount();
    }

    protected void setLinkedAccount()
    {
        getActivity().runOnUiThread(new _cls1());
    }

    protected void setSearchXimalayaOnly(boolean flag)
    {
        ShukeUtil.setSearchChildrenOnly(flag, mCon);
    }








    private class _cls2
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final ShukeSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            if (flag)
            {
                ShukeUtil.setLockBind(true, mCon);
                return;
            } else
            {
                ShukeUtil.setLockBind(false, mCon);
                return;
            }
        }

        _cls2()
        {
            this$0 = ShukeSettingFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final ShukeSettingFragment this$0;

        public void onClick(View view)
        {
            if (!UserInfoMannage.hasLogined() && ShukeSettingFragment.this.ShukeSettingFragment != null)
            {
                view = new Intent(ShukeSettingFragment.this.ShukeSettingFragment, com/ximalaya/ting/android/activity/login/LoginActivity);
                ShukeSettingFragment.this.ShukeSettingFragment.startActivityForResult(view, 0);
            }
        }

        _cls3()
        {
            this$0 = ShukeSettingFragment.this;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final ShukeSettingFragment this$0;

        public void run()
        {
            UserInfoMannage userinfomannage = UserInfoMannage.getInstance();
            if (UserInfoMannage.hasLogined() && userinfomannage.getUser() != null)
            {
                
// JavaClassFileOutputException: get_constant: invalid tag

        _cls1()
        {
            this$0 = ShukeSettingFragment.this;
            super();
        }
    }

}
