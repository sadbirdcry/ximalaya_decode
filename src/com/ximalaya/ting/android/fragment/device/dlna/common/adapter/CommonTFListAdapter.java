// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.adapter;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.model.mediamanager.Item;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseManageTFModule;
import com.ximalaya.ting.android.fragment.device.doss.DossUtils;
import java.util.List;

public class CommonTFListAdapter extends BaseAdapter
{
    class ViewHolder
    {

        RelativeLayout deleteSound;
        ImageView playIcon;
        TextView soundName;
        final CommonTFListAdapter this$0;

        ViewHolder()
        {
            this$0 = CommonTFListAdapter.this;
            super();
        }
    }


    private String currentDirection;
    List listItem;
    private Context mContext;
    private LayoutInflater mInflater;
    private String mObjectID;

    public CommonTFListAdapter(Context context, List list, String s)
    {
        mContext = context;
        listItem = list;
        mInflater = LayoutInflater.from(mContext);
        currentDirection = s;
        mObjectID = "DOWNLOAD";
    }

    public CommonTFListAdapter(Context context, List list, String s, String s1)
    {
        mContext = context;
        listItem = list;
        mInflater = LayoutInflater.from(mContext);
        currentDirection = s;
        mObjectID = s1;
    }

    public void deleteItem(int i)
    {
        Item item;
        CommonDeviceItem commondeviceitem;
        BaseManageTFModule basemanagetfmodule;
        item = (Item)listItem.get(i);
        commondeviceitem = (CommonDeviceItem)DlnaManager.getInstance(mContext).getOperationStroageModel().getNowDeviceItem();
        basemanagetfmodule = (BaseManageTFModule)DlnaManager.getInstance(mContext).getController().getModule(BaseManageTFModule.NAME);
        if (!currentDirection.equals("\u559C\u9A6C\u62C9\u96C5\u7684\u4E0B\u8F7D") && !currentDirection.equals("\u5DF2\u4E0B\u8F7D")) goto _L2; else goto _L1
_L1:
        basemanagetfmodule.deleteMedia(ActionModel.createModel(new _cls3(commondeviceitem.getMMservice(), mObjectID, "TotalMusic", "", item.fileID)));
        if (DossUtils.NUM_DOWNLOAD > 0)
        {
            DossUtils.NUM_DOWNLOAD--;
        }
_L4:
        listItem.remove(i);
        notifyDataSetChanged();
        return;
_L2:
        basemanagetfmodule.deleteMedia(ActionModel.createModel(new _cls4(commondeviceitem.getMMservice(), "USBDISK", "TotalMusic", "", item.fileID)));
        if (DossUtils.NUM_OTHERS > 0)
        {
            DossUtils.NUM_OTHERS--;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public int getCount()
    {
        return listItem.size();
    }

    public Object getItem(int i)
    {
        return listItem.get(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
label0:
        {
label1:
            {
                if (view == null)
                {
                    ViewHolder viewholder = new ViewHolder();
                    view = mInflater.inflate(0x7f030146, viewgroup, false);
                    viewholder.soundName = (TextView)view.findViewById(0x7f0a0218);
                    viewholder.playIcon = (ImageView)view.findViewById(0x7f0a0022);
                    viewholder.deleteSound = (RelativeLayout)view.findViewById(0x7f0a04f8);
                    view.setTag(viewholder);
                    viewgroup = viewholder;
                } else
                {
                    viewgroup = (ViewHolder)view.getTag();
                }
                if (((Item)listItem.get(position)).title != null)
                {
                    ((ViewHolder) (viewgroup)).soundName.setText(((Item)listItem.get(position)).title.toString());
                    ((ViewHolder) (viewgroup)).deleteSound.setVisibility(0);
                    ((ViewHolder) (viewgroup)).deleteSound.setOnClickListener(new _cls1());
                    if (DlnaManager.getInstance(mContext).getOperationStroageModel() == null || DlnaManager.getInstance(mContext).getOperationStroageModel().getCurrentPlayingItem() == null || DlnaManager.getInstance(mContext).getOperationStroageModel().getCurrentPlayingItem().fileID != ((Item)listItem.get(position)).fileID || !DlnaManager.getInstance(mContext).getOperationStroageModel().getCurrentPlayingItem().title.equals(((Item)listItem.get(position)).title))
                    {
                        break label0;
                    }
                    ((ViewHolder) (viewgroup)).playIcon.setVisibility(0);
                    if (!DlnaManager.getInstance(mContext).getOperationStroageModel().isPlaying)
                    {
                        break label1;
                    }
                    ((ViewHolder) (viewgroup)).playIcon.setImageResource(0x7f0203e8);
                    if (((ViewHolder) (viewgroup)).playIcon.getDrawable() instanceof AnimationDrawable)
                    {
                        final AnimationDrawable animationDrawable = (AnimationDrawable)((ViewHolder) (viewgroup)).playIcon.getDrawable();
                        ((ViewHolder) (viewgroup)).playIcon.post(new _cls2());
                    }
                }
                return view;
            }
            ((ViewHolder) (viewgroup)).playIcon.setImageResource(0x7f0203e9);
            return view;
        }
        ((ViewHolder) (viewgroup)).playIcon.setVisibility(8);
        return view;
    }

    private class _cls3 extends DeleteMediaActionCallback
    {

        final CommonTFListAdapter this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "deleteMedia failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "deleteMedia SUCCESS");
        }

        _cls3(Service service, String s, String s1, String s2, int i)
        {
            this$0 = CommonTFListAdapter.this;
            super(service, s, s1, s2, i);
        }
    }


    private class _cls4 extends DeleteMediaActionCallback
    {

        final CommonTFListAdapter this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.d("doss", "deleteMedia failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "deleteMedia SUCCESS");
        }

        _cls4(Service service, String s, String s1, String s2, int i)
        {
            this$0 = CommonTFListAdapter.this;
            super(service, s, s1, s2, i);
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final CommonTFListAdapter this$0;
        final int val$position;

        public void onClick(View view)
        {
            class _cls1
                implements Runnable
            {

                final _cls1 this$1;

                public void run()
                {
                    class _cls1
                        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                    {

                        final _cls1 this$2;

                        public void onExecute()
                        {
                            deleteItem(position);
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    (new DialogBuilder(MyApplication.a())).setMessage((new StringBuilder()).append("\u786E\u5B9A\u5220\u9664\u58F0\u97F3").append(((Item)listItem.get(position)).title).append("\uFF1F").toString()).setOkBtn(new _cls1()).showConfirm();
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            MyApplication.a().runOnUiThread(new _cls1());
        }

        _cls1()
        {
            this$0 = CommonTFListAdapter.this;
            position = i;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final CommonTFListAdapter this$0;
        final AnimationDrawable val$animationDrawable;

        public void run()
        {
            if (animationDrawable != null && !animationDrawable.isRunning())
            {
                animationDrawable.start();
            }
        }

        _cls2()
        {
            this$0 = CommonTFListAdapter.this;
            animationDrawable = animationdrawable;
            super();
        }
    }

}
