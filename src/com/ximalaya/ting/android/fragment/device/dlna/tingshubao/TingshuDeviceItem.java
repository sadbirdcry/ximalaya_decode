// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.tingshubao;

import com.ximalaya.ting.android.fragment.device.dlna.BaseBindableDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import org.teleal.cling.model.meta.Device;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.tingshubao:
//            TingshuBindableModel

public class TingshuDeviceItem extends BaseBindableDeviceItem
{

    public TingshuDeviceItem(BaseDeviceItem basedeviceitem)
    {
        super(basedeviceitem);
        mBaseBindableModel = new TingshuBindableModel();
    }

    public TingshuDeviceItem(Device device, String as[])
    {
        super(device, as);
        mBaseBindableModel = new TingshuBindableModel();
    }

    public BaseItemBindableModel getBaseBindableModel()
    {
        if (mBaseBindableModel == null)
        {
            mBaseBindableModel = new TingshuBindableModel();
        }
        return mBaseBindableModel;
    }
}
