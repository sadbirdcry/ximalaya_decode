// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonUpdateModule;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import java.util.Timer;
import java.util.TimerTask;

public abstract class CommonSettingFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    boolean isOnTouch;
    private ImageView mBackImg;
    protected BaseDeviceItem mDeviceItem;
    protected CommonUpdateModule mModule;
    protected ImageView mSetting1Image;
    protected TextView mSetting1Info;
    protected TextView mSetting1Name;
    protected SwitchButton mSetting1Switch;
    protected RelativeLayout mSetting2;
    protected ImageView mSetting2Image;
    protected TextView mSetting2Info;
    protected TextView mSetting2Name;
    protected SwitchButton mSetting2Switch;
    protected RelativeLayout mSetting3;
    protected ImageView mSetting3Image;
    protected TextView mSetting3Info;
    protected TextView mSetting3Name;
    protected SwitchButton mSetting3Switch;
    protected RelativeLayout mSetting4;
    protected ImageView mSetting4Image;
    protected TextView mSetting4Info;
    protected TextView mSetting4Name;
    protected SwitchButton mSetting4Switch;
    protected RelativeLayout mSetting5;
    protected ImageView mSetting5Image;
    protected TextView mSetting5Info;
    protected TextView mSetting5Name;
    protected SwitchButton mSetting5Switch;
    TimerTask task;
    Timer timer;
    private TextView top_tv;

    public CommonSettingFragment()
    {
        timer = null;
        task = null;
        isOnTouch = false;
    }

    private void cancelTimer()
    {
        if (timer != null)
        {
            timer.cancel();
            timer = null;
        }
    }

    protected String getSwitchModeCommand(boolean flag)
    {
        if (flag)
        {
            return "ximalaya";
        } else
        {
            return "doss";
        }
    }

    protected abstract CommonUpdateModule getUpdateModule();

    public void init()
    {
    }

    protected void initView()
    {
        top_tv = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        top_tv.setText("\u8BBE\u7F6E");
        mBackImg.setOnClickListener(this);
        mSetting1Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a02fe);
        mSetting1Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a02ff);
        mSetting1Name = (TextView)fragmentBaseContainerView.findViewById(0x7f0a02fd);
        mSetting1Image = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a02fc);
        mSetting1Name.setText("\u4EC5\u8BED\u97F3\u641C\u7D22\u559C\u9A6C\u62C9\u96C5\u8282\u76EE");
        mSetting1Image.setImageDrawable(getActivity().getApplicationContext().getResources().getDrawable(0x7f0205b0));
        mSetting2Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a030d);
        mSetting2 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a030a);
        mSetting2Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a030e);
        mSetting2Name = (TextView)fragmentBaseContainerView.findViewById(0x7f0a030c);
        mSetting2Image = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a030b);
        mSetting3Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a0308);
        mSetting3 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a0305);
        mSetting3Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0309);
        mSetting3Name = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0307);
        mSetting3Image = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a0306);
        mSetting3Name.setText("\u56FA\u4EF6\u66F4\u65B0");
        mSetting3Info.setText("\u5DF2\u7ECF\u662F\u6700\u65B0\u7684\u56FA\u4EF6\u5566");
        mSetting3Image.setImageDrawable(mCon.getResources().getDrawable(0x7f0205a8));
        mSetting4Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a0303);
        mSetting4 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a0300);
        mSetting4Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0304);
        mSetting4Name = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0302);
        mSetting4Image = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a0301);
        mSetting5Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a03e8);
        mSetting5 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03e5);
        mSetting5Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a03e9);
        mSetting5Name = (TextView)fragmentBaseContainerView.findViewById(0x7f0a03e7);
        mSetting5Image = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a03e6);
        mSetting2.setOnClickListener(this);
        mSetting2Switch.setVisibility(8);
        mSetting2Info.setVisibility(0);
        mSetting1Info.setVisibility(8);
        mSetting1Switch.setVisibility(0);
        mSetting1Switch.setChecked(isSearchXimalayaOnly());
        mSetting1Switch.setOnCheckedChangeListener(new _cls4());
        mSetting3Info.setVisibility(0);
        mSetting3Switch.setVisibility(8);
    }

    protected abstract boolean isSearchXimalayaOnly();

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initView();
        init();
        mDeviceItem = DlnaManager.getInstance(mCon).getOperationStroageModel().getNowDeviceItem();
        mSetting2.setVisibility(8);
        mSetting3Info.setCompoundDrawables(null, null, null, null);
        bundle = ActionModel.createModel(new _cls1(DlnaManager.getInstance(mCon).getOperationStroageModel().getNowDeviceItem().getAVservice()));
        mModule = getUpdateModule();
        mModule.checkUpdate(bundle);
        if (CommonUpdateModule.isUpdating)
        {
            setUpdatingInfo();
        }
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362565: 
            XiMaoComm.goIntroduction(getActivity());
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300eb, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onResume()
    {
        super.onResume();
    }

    public void onStop()
    {
        super.onStop();
        cancelTimer();
    }

    protected abstract void setSearchXimalayaOnly(boolean flag);

    public void setUpdateInfo(final boolean needUpdate)
    {
        cancelTimer();
        if (mActivity == null)
        {
            return;
        } else
        {
            mActivity.runOnUiThread(new _cls2());
            return;
        }
    }

    protected void setUpdatingInfo()
    {
        cancelTimer();
        mSetting3Info.setText("\u66F4\u65B0\u4E2D...");
        timer = new Timer();
        task = new _cls3();
        timer.schedule(task, 1000L, 1000L);
        mSetting3Info.setTextColor(getResources().getColor(0x7f070098));
        mSetting3Info.setClickable(false);
    }

    public void showDebugToast(String s)
    {
        CustomToast.showToast(mCon, s, 0);
    }

    public void showToast(String s)
    {
        CustomToast.showToast(mCon, s, 0);
    }



    private class _cls4
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final CommonSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            compoundbutton = (BaseSwitchSearchModeModule)DlnaManager.getInstance(mCon).getController().getModule(BaseSwitchSearchModeModule.NAME);
            ActionModel actionmodel = new ActionModel();
            actionmodel.result.put(CommonSwitchSearchModeModule.DEVICE, mDeviceItem);
            if (flag)
            {
                class _cls1 extends DossDlnaActionCallBackImpl
                {

                    final _cls4 this$1;

                    public void onFailed()
                    {
                        boolean flag1 = false;
                        CustomToast.showToast(mCon, "\u5207\u6362\u641C\u7D22\u6A21\u5F0F\u5931\u8D25", 0);
                        SwitchButton switchbutton = mSetting1Switch;
                        if (!mSetting1Switch.isChecked())
                        {
                            flag1 = true;
                        }
                        switchbutton.setChecked(flag1);
                    }

                    public void onSuccess(ActionModel actionmodel1)
                    {
                        CustomToast.showToast(mCon, "\u5207\u6362\u641C\u7D22\u6A21\u5F0F\u6210\u529F", 0);
                        setSearchXimalayaOnly(true);
                    }

                _cls1()
                {
                    this$1 = _cls4.this;
                    super();
                }
                }

                actionmodel.result.put(CommonSwitchSearchModeModule.CALL_BACK, new _cls1());
                actionmodel.result.put(CommonSwitchSearchModeModule.SEARCH_MODE, getSwitchModeCommand(true));
                compoundbutton.switchSearchMode(actionmodel);
                return;
            } else
            {
                class _cls2 extends DossDlnaActionCallBackImpl
                {

                    final _cls4 this$1;

                    public void onFailed()
                    {
                        boolean flag1 = false;
                        CustomToast.showToast(mCon, "\u5207\u6362\u641C\u7D22\u6A21\u5F0F\u5931\u8D25", 0);
                        SwitchButton switchbutton = mSetting1Switch;
                        if (!mSetting1Switch.isChecked())
                        {
                            flag1 = true;
                        }
                        switchbutton.setChecked(flag1);
                    }

                    public void onSuccess(ActionModel actionmodel1)
                    {
                        CustomToast.showToast(mCon, "\u5207\u6362\u641C\u7D22\u6A21\u5F0F\u6210\u529F", 0);
                        setSearchXimalayaOnly(false);
                    }

                _cls2()
                {
                    this$1 = _cls4.this;
                    super();
                }
                }

                actionmodel.result.put(CommonSwitchSearchModeModule.CALL_BACK, new _cls2());
                actionmodel.result.put(CommonSwitchSearchModeModule.SEARCH_MODE, getSwitchModeCommand(false));
                compoundbutton.switchSearchMode(actionmodel);
                return;
            }
        }

        _cls4()
        {
            this$0 = CommonSettingFragment.this;
            super();
        }
    }


    private class _cls1 extends GetInfoExActionCallback
    {

        final CommonSettingFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            Logger.e("doss", s);
        }

        public void received(ActionInvocation actioninvocation, InfoEx infoex)
        {
            if (infoex.getVerUpdateFlag() == 1)
            {
                setUpdateInfo(true);
                return;
            } else
            {
                setUpdateInfo(false);
                return;
            }
        }

        _cls1(Service service)
        {
            this$0 = CommonSettingFragment.this;
            super(service);
        }
    }


    private class _cls2
        implements Runnable
    {

        final CommonSettingFragment this$0;
        final boolean val$needUpdate;

        public void run()
        {
            if (needUpdate)
            {
                mSetting3Info.setText("\u7ACB\u5373\u66F4\u65B0");
                mSetting3Info.setTextColor(getResources().getColor(0x7f070004));
                mSetting3Info.setClickable(true);
                class _cls1
                    implements android.view.View.OnClickListener
                {

                    final _cls2 this$1;

                    public void onClick(View view)
                    {
                        setUpdatingInfo();
                        class _cls1 extends DossDlnaActionCallBackImpl
                        {

                            final _cls1 this$2;

                            public void onFailed()
                            {
                            }

                            public void onSuccess(ActionModel actionmodel1)
                            {
                            }

                                _cls1()
                                {
                                    this$2 = _cls1.this;
                                    super();
                                }
                        }

                        view = new _cls1();
                        ActionModel actionmodel = new ActionModel();
                        actionmodel.result.put(CommonUpdateModule.CALL_BACK, view);
                        actionmodel.result.put(CommonUpdateModule.DEVICE, mDeviceItem);
                        mModule.doUpdate(actionmodel);
                    }

                _cls1()
                {
                    this$1 = _cls2.this;
                    super();
                }
                }

                mSetting3Info.setOnClickListener(new _cls1());
                return;
            } else
            {
                mSetting3Info.setText("\u5DF2\u7ECF\u662F\u6700\u65B0\u7684\u56FA\u4EF6\u5566");
                mSetting3Info.setTextColor(getResources().getColor(0x7f070098));
                mSetting3Info.setClickable(false);
                return;
            }
        }

        _cls2()
        {
            this$0 = CommonSettingFragment.this;
            needUpdate = flag;
            super();
        }
    }


}
