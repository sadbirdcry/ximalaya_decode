// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.tingshubao;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.activity.DeviceItem;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.BasePlayManageController;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseTuiSongModule;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseVolumeControlModule;
import com.ximalaya.ting.android.fragment.device.shu.MyWifiConnManager2;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.tingshubao:
//            TingshuDeviceItem, TingshuBindModule, TingshuLinkDeviceModule, TingshuKeyModule, 
//            TingshuTuisongModule, TingshuRenameModule

public class TingshuController extends BaseDlnaController
{

    public static final String DEVICE_TINGSHUBAO_HEADER = "ximalaya-sn00-tsb0";
    protected static final String TAG = com/ximalaya/ting/android/fragment/device/dlna/tingshubao/TingshuController.getSimpleName();
    private MyWifiConnManager2 mMyWifiConnManager2;

    public TingshuController(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
        mMyWifiConnManager2 = new MyWifiConnManager2(MainTabActivity2.mainTabActivity);
    }

    private void getAllChannel(final TingshuDeviceItem tingshuDeviceItem, final IActionCallBack callBack)
    {
        final String url = getChanelsAllRequestUrl(tingshuDeviceItem);
        if (TextUtils.isEmpty(url))
        {
            return;
        } else
        {
            (new _cls1()).myexec(new Void[0]);
            return;
        }
    }

    private String getChanelsAllRequestUrl(TingshuDeviceItem tingshudeviceitem)
    {
        return (new StringBuilder()).append("http://").append(tingshudeviceitem.getDeviceItem().getIpAddress()).append("/cgi-bin/get_channel.cgi").toString();
    }

    private long getChannelAlbumIdByUrl(String s)
    {
        int i;
        if (!Pattern.compile(".*http://3rd.ximalaya.com/albums/[0-9]+/tracks.*").matcher(s).matches())
        {
            return -1L;
        }
        s = Pattern.compile("[/]+").split(s);
        i = 0;
_L3:
        if (i >= s.length)
        {
            break MISSING_BLOCK_LABEL_94;
        }
        if (!s[i].equals("albums")) goto _L2; else goto _L1
_L1:
        s = s[i + 1];
_L4:
        System.out.println((new StringBuilder()).append("result:").append(s).toString());
        return Long.valueOf(s).longValue();
_L2:
        i++;
          goto _L3
        s = null;
          goto _L4
    }

    private void initOneAlbumInfo(final TingshuDeviceItem tingshuDeviceItem, final long albumModelId, final int channelId)
    {
        while (albumModelId <= 0L || !ToolUtil.isConnectToNetwork(mContext)) 
        {
            return;
        }
        (new _cls2()).myexec(new Void[0]);
    }

    private void setChannelInfo(String s, TingshuDeviceItem tingshudeviceitem)
    {
        String s2;
        long l;
        try
        {
            s = JSON.parseObject(s);
            String s1 = s.getString("status");
            Logger.d(TAG, (new StringBuilder()).append("setChannelInfo:status:").append(s1).toString());
            if (!s1.equalsIgnoreCase("ok"))
            {
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return;
        }
        s2 = s.getString("channel1_url");
        if (TextUtils.isEmpty(s2)) goto _L2; else goto _L1
_L1:
        l = getChannelAlbumIdByUrl(s2);
        if (l >= 0L) goto _L4; else goto _L3
_L3:
        tingshudeviceitem.setKeyAlbum(1, null);
_L21:
        Logger.d(TAG, (new StringBuilder()).append("setChannelInfo:1:").append(l).toString());
_L2:
        s2 = s.getString("channel2_url");
        if (TextUtils.isEmpty(s2)) goto _L6; else goto _L5
_L5:
        l = getChannelAlbumIdByUrl(s2);
        if (l >= 0L) goto _L8; else goto _L7
_L7:
        tingshudeviceitem.setKeyAlbum(2, null);
_L17:
        Logger.d(TAG, (new StringBuilder()).append("setChannelInfo:2:").append(l).toString());
_L6:
        s2 = s.getString("channel3_url");
        if (TextUtils.isEmpty(s2)) goto _L10; else goto _L9
_L9:
        l = getChannelAlbumIdByUrl(s2);
        if (l >= 0L) goto _L12; else goto _L11
_L11:
        tingshudeviceitem.setKeyAlbum(3, null);
_L18:
        Logger.d(TAG, (new StringBuilder()).append("setChannelInfo:3:").append(l).toString());
_L10:
        s = s.getString("channel4_url");
        if (TextUtils.isEmpty(s)) goto _L14; else goto _L13
_L13:
        l = getChannelAlbumIdByUrl(s);
        if (l >= 0L) goto _L16; else goto _L15
_L15:
        tingshudeviceitem.setKeyAlbum(4, null);
_L19:
        Logger.d(TAG, (new StringBuilder()).append("setChannelInfo:4:").append(l).toString());
        return;
_L4:
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = l;
        tingshudeviceitem.setKeyAlbum(1, albummodel);
        initOneAlbumInfo(tingshudeviceitem, l, 1);
        continue; /* Loop/switch isn't completed */
_L8:
        AlbumModel albummodel1 = new AlbumModel();
        albummodel1.albumId = l;
        tingshudeviceitem.setKeyAlbum(2, albummodel1);
        initOneAlbumInfo(tingshudeviceitem, l, 2);
          goto _L17
_L12:
        AlbumModel albummodel2 = new AlbumModel();
        albummodel2.albumId = l;
        tingshudeviceitem.setKeyAlbum(3, albummodel2);
        initOneAlbumInfo(tingshudeviceitem, l, 3);
          goto _L18
_L16:
        s = new AlbumModel();
        s.albumId = l;
        tingshudeviceitem.setKeyAlbum(4, s);
        initOneAlbumInfo(tingshudeviceitem, l, 4);
          goto _L19
_L14:
        return;
        if (true) goto _L21; else goto _L20
_L20:
    }

    public void addDeivce(BaseDeviceItem basedeviceitem)
    {
        if (mDeviceList == null)
        {
            mDeviceList = new ArrayList();
        }
        basedeviceitem = new TingshuDeviceItem(basedeviceitem);
        if (!mDeviceList.contains(basedeviceitem))
        {
            mDeviceList.add(basedeviceitem);
        }
        initDeviceInfo(basedeviceitem, null);
    }

    public void change2Bendi(BasePlayManageController baseplaymanagecontroller)
    {
    }

    public String[] getCheckedUdn()
    {
        return (new String[] {
            "ximalaya-sn00-tsb0", "Hi", "NBCAST"
        });
    }

    public com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getDeviceType()
    {
        return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao;
    }

    public MyWifiConnManager2 getMyWifiConnManager2()
    {
        return mMyWifiConnManager2;
    }

    public void initDeviceInfo(BaseDeviceItem basedeviceitem, IActionCallBack iactioncallback)
    {
        if (basedeviceitem.getDlnaType() != com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao)
        {
            return;
        } else
        {
            getAllChannel((TingshuDeviceItem)basedeviceitem, iactioncallback);
            return;
        }
    }

    public void initModules()
    {
        setModule(new TingshuBindModule(mContext, getControlPoint()));
        setModule(new TingshuLinkDeviceModule(mContext, getControlPoint()));
        setModule(new TingshuKeyModule(mContext, getControlPoint()));
        setModule(new TingshuTuisongModule(mContext, getControlPoint()));
        setModule(new TingshuRenameModule(mContext, getControlPoint()));
        setModule(new BaseVolumeControlModule(mContext, getControlPoint()));
    }

    public void onCommandFailed(ActionModel actionmodel)
    {
    }

    public void playSound(BaseDeviceItem basedeviceitem, ActionModel actionmodel)
    {
        ((TingshuTuisongModule)getModule(BaseTuiSongModule.NAME)).tuisong(basedeviceitem, actionmodel);
    }



    private class _cls1 extends MyAsyncTask
    {

        boolean isSuccess;
        final TingshuController this$0;
        final IActionCallBack val$callBack;
        final TingshuDeviceItem val$tingshuDeviceItem;
        final String val$url;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            Object obj;
            obj = new DefaultHttpClient();
            ((DefaultHttpClient) (obj)).getParams().setParameter("http.connection.timeout", Integer.valueOf(5000));
            avoid = new HttpGet(url);
            avoid = ((DefaultHttpClient) (obj)).execute(avoid);
            if (avoid == null) goto _L2; else goto _L1
_L1:
            obj = avoid.getStatusLine();
            if (obj == null) goto _L4; else goto _L3
_L3:
            if (((StatusLine) (obj)).getStatusCode() != 200) goto _L4; else goto _L5
_L5:
            isSuccess = true;
            obj = new BufferedReader(new InputStreamReader(avoid.getEntity().getContent()));
            avoid = ((Void []) (obj));
            StringBuffer stringbuffer = new StringBuffer("");
_L7:
            avoid = ((Void []) (obj));
            Object obj1 = ((BufferedReader) (obj)).readLine();
            if (obj1 == null)
            {
                break; /* Loop/switch isn't completed */
            }
            avoid = ((Void []) (obj));
            stringbuffer.append(((String) (obj1)));
            if (true) goto _L7; else goto _L6
            obj1;
_L33:
            avoid = ((Void []) (obj));
            ((UnsupportedEncodingException) (obj1)).printStackTrace();
            if (obj != null)
            {
                try
                {
                    ((BufferedReader) (obj)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                }
            }
            if (callBack == null) goto _L9; else goto _L8
_L8:
            if (!isSuccess) goto _L11; else goto _L10
_L10:
            avoid = callBack;
_L15:
            avoid.onSuccess(null);
_L9:
            return null;
_L6:
            avoid = ((Void []) (obj));
            obj1 = stringbuffer.toString();
            avoid = ((Void []) (obj));
            Logger.d(TingshuController.TAG, (new StringBuilder()).append("getChanelsAllRequest onSuccess:").append(((String) (obj1))).toString());
            avoid = ((Void []) (obj));
            setChannelInfo(((String) (obj1)), tingshuDeviceItem);
            avoid = ((Void []) (obj));
            isSuccess = true;
            if (obj != null)
            {
                try
                {
                    ((BufferedReader) (obj)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                }
            }
            if (callBack == null) goto _L9; else goto _L12
_L12:
            if (!isSuccess) goto _L14; else goto _L13
_L13:
            avoid = callBack;
              goto _L15
            obj1;
            obj = null;
_L31:
            avoid = ((Void []) (obj));
            ((ClientProtocolException) (obj1)).printStackTrace();
            if (obj != null)
            {
                try
                {
                    ((BufferedReader) (obj)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                }
            }
            if (callBack == null) goto _L9; else goto _L16
_L16:
            if (!isSuccess) goto _L18; else goto _L17
_L17:
            avoid = callBack;
              goto _L15
            obj1;
            obj = null;
_L30:
            avoid = ((Void []) (obj));
            ((IOException) (obj1)).printStackTrace();
            if (obj != null)
            {
                try
                {
                    ((BufferedReader) (obj)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                }
            }
            if (callBack == null) goto _L9; else goto _L19
_L19:
            if (!isSuccess) goto _L21; else goto _L20
_L20:
            avoid = callBack;
              goto _L15
            obj1;
            obj = null;
_L29:
            avoid = ((Void []) (obj));
            ((IllegalStateException) (obj1)).printStackTrace();
            if (obj != null)
            {
                try
                {
                    ((BufferedReader) (obj)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                }
            }
            if (callBack == null) goto _L9; else goto _L22
_L22:
            if (!isSuccess) goto _L24; else goto _L23
_L23:
            avoid = callBack;
              goto _L15
            obj;
            avoid = null;
_L28:
            if (avoid != null)
            {
                try
                {
                    avoid.close();
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                }
            }
            if (callBack != null)
            {
                if (isSuccess)
                {
                    callBack.onSuccess(null);
                } else
                {
                    callBack.onFailed();
                }
            }
            throw obj;
_L11:
            avoid = callBack;
_L25:
            avoid.onFailed();
            return null;
_L18:
            avoid = callBack;
              goto _L25
_L21:
            avoid = callBack;
              goto _L25
_L24:
            avoid = callBack;
              goto _L25
_L14:
            avoid = callBack;
              goto _L25
_L4:
            if (false)
            {
                try
                {
                    throw new NullPointerException();
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                }
            }
            if (callBack == null) goto _L9; else goto _L26
_L26:
label0:
            {
                if (!isSuccess)
                {
                    break label0;
                }
                avoid = callBack;
            }
              goto _L15
            avoid = callBack;
              goto _L25
_L2:
            if (false)
            {
                try
                {
                    throw new NullPointerException();
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                }
            }
            if (callBack == null) goto _L9; else goto _L27
_L27:
label1:
            {
                if (!isSuccess)
                {
                    break label1;
                }
                avoid = callBack;
            }
              goto _L15
            avoid = callBack;
              goto _L25
            obj;
              goto _L28
            obj1;
              goto _L29
            obj1;
              goto _L30
            obj1;
              goto _L31
            obj1;
            obj = null;
            if (true) goto _L33; else goto _L32
_L32:
        }

        _cls1()
        {
            this$0 = TingshuController.this;
            url = s;
            tingshuDeviceItem = tingshudeviceitem;
            callBack = iactioncallback;
            super();
            isSuccess = false;
        }
    }


    private class _cls2 extends MyAsyncTask
    {

        AlbumModel tAlbum;
        final TingshuController this$0;
        final long val$albumModelId;
        final int val$channelId;
        final TingshuDeviceItem val$tingshuDeviceItem;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            avoid = new RequestParams();
            String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
            s = (new StringBuilder()).append(s).append(albumModelId).append("/").append(true).append("/").append(1).append("/").append(15).toString();
            avoid = f.a().a(s, avoid, null, null);
            Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
            try
            {
                avoid = JSON.parseObject(avoid);
                if ("0".equals(avoid.get("ret").toString()))
                {
                    tAlbum = (AlbumModel)JSON.parseObject(avoid.getString("album"), com/ximalaya/ting/android/model/album/AlbumModel);
                }
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[]) { }
            tingshuDeviceItem.setKeyAlbum(channelId + 1, tAlbum);
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
        }

        _cls2()
        {
            this$0 = TingshuController.this;
            albumModelId = l;
            tingshuDeviceItem = tingshudeviceitem;
            channelId = i;
            super();
            tAlbum = null;
        }
    }

}
