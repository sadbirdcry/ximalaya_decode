// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import com.ximalaya.ting.android.fragment.device.shu.MyDeviceItem;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.meta.Service;
import org.teleal.cling.model.types.ServiceType;

public class WifiDeviceModel
{

    public Service mAVservice;
    public Service mMMservice;
    public MyDeviceItem mMyDeviceItem;
    public Service mPQservice;
    public Service mRCservice;

    public WifiDeviceModel(MyDeviceItem mydeviceitem)
    {
        initDeviceModel(mydeviceitem);
    }

    private void initDeviceModel(MyDeviceItem mydeviceitem)
    {
        mMyDeviceItem = mydeviceitem;
        mAVservice = mydeviceitem.getDevice().findService(new ServiceType("schemas-upnp-org", "AVTransport"));
        mRCservice = mydeviceitem.getDevice().findService(new ServiceType("schemas-upnp-org", "RenderingControl"));
        mPQservice = mydeviceitem.getDevice().findService(new ServiceType("schemas-wiimu-com", "PlayQueue"));
        mMMservice = mydeviceitem.getDevice().findService(new ServiceType("schemas-wiimu-com", "MediaManager"));
    }

    public boolean isValid()
    {
        return mMyDeviceItem != null && mAVservice != null && mRCservice != null;
    }
}
