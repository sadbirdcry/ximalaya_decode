// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;


public class BaseCurrentPlayingModel
{

    private int currentPage;
    private String currentPlayList;
    private int currentTrackNum;

    public BaseCurrentPlayingModel()
    {
        currentPage = -1;
        currentPlayList = "";
        currentTrackNum = -1;
    }

    public int getCurrentPage()
    {
        return currentPage;
    }

    public String getCurrentPlayList()
    {
        return currentPlayList;
    }

    public int getCurrentTrackNum()
    {
        return currentTrackNum;
    }

    public void setCurrentPage(String s)
    {
        try
        {
            currentPage = Integer.valueOf(s).intValue();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }

    public void setCurrentPlayList(String s)
    {
        currentPlayList = s;
    }

    public void setCurrentTrackNum(String s)
    {
        try
        {
            currentTrackNum = Integer.valueOf(s).intValue();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
    }
}
