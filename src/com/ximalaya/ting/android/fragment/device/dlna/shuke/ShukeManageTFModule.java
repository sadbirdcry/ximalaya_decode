// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonManageTFModule;
import com.ximalaya.ting.android.fragment.device.dlna.model.Directory;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeController

public class ShukeManageTFModule extends CommonManageTFModule
{

    private final String TAG = com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeManageTFModule.getSimpleName();

    public ShukeManageTFModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public void getMediaCount(BaseDeviceItem basedeviceitem, IActionCallBack iactioncallback)
    {
        getMediaNum((CommonDeviceItem)basedeviceitem, "EXTDOWNLOAD", iactioncallback);
        ShukeController shukecontroller = (ShukeController)DlnaManager.getInstance(mContext).getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi);
        if (shukecontroller != null)
        {
            int i = 0;
            while (i < shukecontroller.getDirectoryNum()) 
            {
                getMediaNumSpecDir((CommonDeviceItem)basedeviceitem, shukecontroller.getDirectory(i).getReqName(), iactioncallback);
                i++;
            }
        }
    }

    public void getMediaNumSpecDir(CommonDeviceItem commondeviceitem, String s, IActionCallBack iactioncallback)
    {
        getControlPoint().execute(new _cls1("BrowseMedia", "Directory", s, 1, 1, s, iactioncallback));
    }


    private class _cls1 extends BrowseMediaActionCallback
    {

        final ShukeManageTFModule this$0;
        final IActionCallBack val$callBack;
        final String val$filter;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void received(Result result)
        {
            ShukeController shukecontroller = (ShukeController)DlnaManager.getInstance(
// JavaClassFileOutputException: get_constant: invalid tag

        _cls1(String s1, String s2, String s3, int i, 
                int j, String s4, IActionCallBack iactioncallback)
        {
            this$0 = ShukeManageTFModule.this;
            filter = s4;
            callBack = iactioncallback;
            super(final_service, final_s, s1, s2, s3, i, j);
        }
    }

}
