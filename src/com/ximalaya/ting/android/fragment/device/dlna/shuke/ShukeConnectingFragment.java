// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.fragment.device.ConnectorFactory;
import com.ximalaya.ting.android.fragment.device.IConnector;
import com.ximalaya.ting.android.fragment.device.conn.ConnectingFragment;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceListFragment;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeFailConnFragment

public class ShukeConnectingFragment extends ConnectingFragment
{

    private ImageView mImageView;
    private RelativeLayout mReLayout;

    public ShukeConnectingFragment()
    {
    }

    protected IConnector getConnector()
    {
        return ConnectorFactory.createConnector(getActivity(), ConnectorFactory.getTypeByName("doss"), this);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mImageView = (ImageView)mReLayout.findViewById(0x7f0a033d);
        mImageView.setImageDrawable(mActivity.getApplicationContext().getResources().getDrawable(0x7f020551));
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mReLayout = (RelativeLayout)super.onCreateView(layoutinflater, viewgroup, bundle);
        return mReLayout;
    }

    public void onFailed()
    {
        Bundle bundle = new Bundle();
        bundle.putString("device", com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi.toString());
        removeTopFramentFromManageFragment();
        startFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeFailConnFragment, bundle);
    }

    public void onSuccuss()
    {
        Bundle bundle = new Bundle();
        bundle.putInt(DeviceListFragment.deviceType, com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi.ordinal());
        removeTopFramentFromManageFragment();
        startFragment(com/ximalaya/ting/android/fragment/device/dlna/DeviceListFragment, bundle);
    }
}
