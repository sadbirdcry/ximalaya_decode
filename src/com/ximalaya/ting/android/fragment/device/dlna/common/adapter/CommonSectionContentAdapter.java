// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.adapter;

import android.content.Context;
import com.ximalaya.model.mediamanager.Item;
import com.ximalaya.model.mediamanager.MediaInfo;
import com.ximalaya.model.mediamanager.Task;
import com.ximalaya.ting.android.fragment.device.album.AbsSectionContentAdapter;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonListFinishedDownloadFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonListUnFinishedTaskFragment;
import com.ximalaya.ting.android.model.album.AlbumSectionModel;
import java.util.Iterator;
import java.util.List;

public class CommonSectionContentAdapter extends AbsSectionContentAdapter
{

    public CommonSectionContentAdapter(Context context, AlbumSectionModel albumsectionmodel)
    {
        super(context, albumsectionmodel);
    }

    protected boolean isInDownloadList(com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track)
    {
        boolean flag1 = false;
        if (CommonListUnFinishedTaskFragment.taskItems == null) goto _L2; else goto _L1
_L1:
        int i = 0;
_L6:
        if (i >= CommonListUnFinishedTaskFragment.taskItems.size()) goto _L2; else goto _L3
_L3:
        Task task = (Task)CommonListUnFinishedTaskFragment.taskItems.get(i);
        if (!track.getTitle().equals(task.mediaInfo.title)) goto _L5; else goto _L4
_L4:
        boolean flag = true;
_L8:
        return flag;
_L5:
        i++;
          goto _L6
_L2:
        flag = flag1;
        if (CommonListFinishedDownloadFragment.downloadList == null) goto _L8; else goto _L7
_L7:
        i = 0;
_L10:
        flag = flag1;
        if (i >= CommonListFinishedDownloadFragment.downloadList.size()) goto _L8; else goto _L9
_L9:
        Item item = (Item)CommonListFinishedDownloadFragment.downloadList.get(i);
        if (track.getTitle().equals(item.title))
        {
            return true;
        }
        i++;
          goto _L10
    }

    public boolean isOneChecked()
    {
label0:
        {
            if (getCount() <= 0)
            {
                break label0;
            }
            Iterator iterator = mData.getTracks().getList().iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
            } while (!((com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next()).getIsChecked());
            return true;
        }
        return false;
    }

    public void refreshDownloadStatus()
    {
        if (getCount() > 0)
        {
            for (Iterator iterator = mData.getTracks().getList().iterator(); iterator.hasNext();)
            {
                com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
                if (isInDownloadList(track))
                {
                    track.setIsDownload(true);
                    track.setIsChecked(false);
                } else
                {
                    track.setIsDownload(false);
                }
            }

        }
        notifyDataSetChanged();
    }
}
