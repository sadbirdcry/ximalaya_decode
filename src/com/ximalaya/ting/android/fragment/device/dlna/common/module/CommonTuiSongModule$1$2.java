// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import com.ximalaya.action.CreateQueueActionCallback;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.doss.DossUtils;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.module:
//            CommonTuiSongModule

class this._cls1 extends CreateQueueActionCallback
{

    final l.deviceItem this$1;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        if (callBack != null)
        {
            callBack.onFailed();
        }
        Logger.d("tuisong", "CreateQueue Failure");
    }

    public void success(ActionInvocation actioninvocation)
    {
        Logger.d("tuisong", "CreateQueue Success");
        class _cls1 extends PlayQueueWithIndexActionCallback
        {

            final CommonTuiSongModule._cls1._cls2 this$2;

            public void failure(ActionInvocation actioninvocation1, UpnpResponse upnpresponse, String s)
            {
                if (callBack != null)
                {
                    callBack.onFailed();
                }
                Logger.d("tuisong", "playwithIndex FAILURE");
            }

            public void success(ActionInvocation actioninvocation1)
            {
                Logger.d("tuisong", "playwithIndex SUCCESS");
                CommonTuiSongModule.access$100(this$0);
            }

            _cls1(Service service, String s, int i)
            {
                this$2 = CommonTuiSongModule._cls1._cls2.this;
                super(service, s, i);
            }
        }

        getControlPoint().execute(new _cls1(deviceItem.getPQservice(), DossUtils.getPlayListName(MyApplication.b()), 1));
    }

    _cls1(Service service, String s)
    {
        this$1 = this._cls1.this;
        super(service, s);
    }
}
