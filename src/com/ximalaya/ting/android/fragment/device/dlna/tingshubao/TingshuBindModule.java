// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.tingshubao;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseBindableDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.BindCommandModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseBindModule;
import com.ximalaya.ting.android.util.Logger;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.tingshubao:
//            TingshuDeviceItem

public class TingshuBindModule extends BaseBindModule
{

    public TingshuBindModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    private boolean Postchannel(String s)
    {
        if (s != null) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        DefaultHttpClient defaulthttpclient = new DefaultHttpClient();
        s = new HttpGet(s);
        int i;
        try
        {
            s = defaulthttpclient.execute(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return false;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return false;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return false;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return false;
        }
        if (s == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        s = s.getStatusLine();
        if (s == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        i = s.getStatusCode();
        if (i == 200)
        {
            return true;
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    private static String getchannelURI(long l, int i)
    {
        String s1 = (new StringBuilder()).append("http://3rd.ximalaya.com/albums/").append(l).append("/tracks?i_am=baori&page=1&per_page=").append(i).append("&is_asc=true").toString();
        String s = s1;
        if (s1.contains(":"))
        {
            s = s1.replaceAll("\\:", "%3A");
        }
        s1 = s;
        if (s.contains("/"))
        {
            s1 = s.replaceAll("\\/", "%2F");
        }
        s = s1;
        if (s1.contains("?"))
        {
            s = s1.replaceAll("\\?", "%3F");
        }
        s1 = s;
        if (s.contains("="))
        {
            s1 = s.replaceAll("\\=", "%3D");
        }
        s = s1;
        if (s1.contains("&"))
        {
            s = s1.replaceAll("\\&", "%26");
        }
        return s;
    }

    private static String setchannelURL(BaseDeviceItem basedeviceitem, long l, int i, int j)
    {
        if (i <= 4) goto _L2; else goto _L1
_L1:
        int k = 4;
_L4:
        String s = (new StringBuilder()).append("channel").append(k).toString();
        return (new StringBuilder()).append("http://").append(basedeviceitem.getIpAddress()).append("/cgi-bin/set_channel.cgi?").append(s).append("=").append(getchannelURI(l, j)).toString();
_L2:
        k = i;
        if (i < 1)
        {
            k = 1;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void bind(BaseDeviceItem basedeviceitem, ActionModel actionmodel)
    {
        basedeviceitem = (TingshuDeviceItem)basedeviceitem;
        basedeviceitem = (BindCommandModel)actionmodel.result.get("bindcommand");
        setChannels(basedeviceitem);
        changeDeviceChannelInfo(basedeviceitem);
    }

    public void changeDeviceChannelInfo(BindCommandModel bindcommandmodel)
    {
        if (bindcommandmodel == null)
        {
            return;
        } else
        {
            bindcommandmodel.mDeviceItem.getBaseBindableModel().setAlbums(bindcommandmodel.mChannelId, bindcommandmodel.mAlbumModel);
            return;
        }
    }

    public void setChannels(final BindCommandModel mBindCommand)
    {
        if (mBindCommand == null)
        {
            Logger.d(TAG, (new StringBuilder()).append("setChannels:").append(null).toString());
        }
        (new Thread(new _cls1())).start();
    }



    private class _cls1
        implements Runnable
    {

        final TingshuBindModule this$0;
        final BindCommandModel val$mBindCommand;

        public void run()
        {
            changeDeviceChannelInfo(mBindCommand);
            Postchannel(TingshuBindModule.setchannelURL(mBindCommand.mDeviceItem, mBindCommand.mAlbumId, mBindCommand.mChannelId, 15));
        }

        _cls1()
        {
            this$0 = TingshuBindModule.this;
            mBindCommand = bindcommandmodel;
            super();
        }
    }

}
