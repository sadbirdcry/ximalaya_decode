// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.tingshubao;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseTingshuLinkModule;
import org.teleal.cling.controlpoint.ControlPoint;

public class TingshuLinkDeviceModule extends BaseTingshuLinkModule
{

    public TingshuLinkDeviceModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    public String getGetWifiListUrl()
    {
        return "http://192.168.103.1/cgi-bin/scan.cgi";
    }

    public void getWifiList(final IActionCallBack callBack)
    {
        (new Thread(new _cls1())).start();
    }

    private class _cls1
        implements Runnable
    {

        List mWifiList;
        final TingshuLinkDeviceModule this$0;
        final IActionCallBack val$callBack;

        public void run()
        {
            boolean flag = false;
            HttpURLConnection httpurlconnection;
            BufferedReader bufferedreader;
            httpurlconnection = (HttpURLConnection)(new URL(getGetWifiListUrl())).openConnection();
            httpurlconnection.connect();
            bufferedreader = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()));
_L4:
            Object obj = bufferedreader.readLine();
            if (obj == null) goto _L2; else goto _L1
_L1:
            System.out.println((new StringBuilder()).append("teshu lines:").append(((String) (obj))).toString());
            if (!((String) (obj)).contains("{")) goto _L4; else goto _L3
_L3:
            bufferedreader.close();
            httpurlconnection.disconnect();
            if (!TextUtils.isEmpty(((CharSequence) (obj)))) goto _L6; else goto _L5
_L5:
            if (mWifiList != null && flag)
            {
                try
                {
                    obj = ActionModel.createModel(mWifiList);
                    callBack.onSuccess(((ActionModel) (obj)));
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    ((Exception) (obj)).printStackTrace();
                }
                callBack.onFailed();
                return;
            }
              goto _L7
_L6:
            obj = JSONObject.parseObject(((String) (obj)));
            if (!((String)((JSONObject) (obj)).get("status")).equalsIgnoreCase("OK"))
            {
                flag = false;
                continue; /* Loop/switch isn't completed */
            }
            mWifiList = JSON.parseArray(((JSONObject) (obj)).getString("wifi_list"), com/ximalaya/ting/android/fragment/device/shu/MyWifiScan);
            flag = true;
            continue; /* Loop/switch isn't completed */
_L7:
            callBack.onFailed();
            return;
            obj;
            if (true) goto _L5; else goto _L2
_L2:
            obj = null;
              goto _L3
        }

        _cls1()
        {
            this$0 = TingshuLinkDeviceModule.this;
            callBack = iactioncallback;
            super();
        }
    }

}
