// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna;

import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.module.IBaseModule;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna:
//            BaseDeviceItem, BasePlayManageController

public interface IDlnaController
{

    public abstract void addDeivce(BaseDeviceItem basedeviceitem);

    public abstract void change2Bendi(BasePlayManageController baseplaymanagecontroller);

    public abstract void clearDeivce();

    public abstract List getAllDevices();

    public abstract String[] getCheckedUdn();

    public abstract com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getDeviceType();

    public abstract IBaseModule getModule(String s);

    public abstract void initDeviceInfo(BaseDeviceItem basedeviceitem, IActionCallBack iactioncallback);

    public abstract void initModules();

    public abstract boolean isThisDevice(BaseDeviceItem basedeviceitem);

    public abstract void onCommandFailed(ActionModel actionmodel);

    public abstract void playSound(BaseDeviceItem basedeviceitem, ActionModel actionmodel);

    public abstract void removeDeivce(BaseDeviceItem basedeviceitem);

    public abstract void setModule(IBaseModule ibasemodule);

    public abstract void tuisongExtra(BasePlayManageController baseplaymanagecontroller);
}
