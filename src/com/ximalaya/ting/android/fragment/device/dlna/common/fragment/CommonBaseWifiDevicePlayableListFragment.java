// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.ximalaya.model.mediamanager.Item;
import com.ximalaya.model.mediamanager.Result;
import com.ximalaya.model.playqueue.ListInfo;
import com.ximalaya.model.playqueue.PlayList;
import com.ximalaya.ting.android.fragment.device.BaseDevicePlayListFragment;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseCurrentPlayingModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BasePlayModule;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.util.Logger;
import java.util.List;
import java.util.Timer;

public abstract class CommonBaseWifiDevicePlayableListFragment extends BaseDevicePlayListFragment
    implements android.view.View.OnClickListener
{

    private String TAG;
    protected int currentQueuePage;
    protected int soundPage;

    public CommonBaseWifiDevicePlayableListFragment()
    {
        TAG = com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonBaseWifiDevicePlayableListFragment.getSimpleName();
        currentQueuePage = -1;
        soundPage = 0;
    }

    protected void clearPlayQueue()
    {
        if (getActivity() == null)
        {
            return;
        } else
        {
            DlnaManager.getInstance(getActivity().getApplicationContext()).removeOnkeyEventListener(this);
            DlnaManager.getInstance(mCon).getOperationStroageModel().clearCurrentPlaying();
            Logger.d("doss", "\u8DF3\u51FA\u5F53\u524Dqueue");
            currentQueuePage = -1;
            mPositionInCurrentPage = -1;
            mPositionInListView = -1;
            return;
        }
    }

    public PlayList createQueue()
    {
        PlayList playlist = new PlayList();
        playlist.listName = "local";
        playlist.listInfo.sourceName = "USBDiskQueue";
        currentQueuePage = soundPage;
        return playlist;
    }

    protected void deleteAllMedia()
    {
    }

    protected void initData()
    {
        mListView.setAdapter(getAdapter());
        registerListener();
        loadMoreData();
    }

    protected void notifyAdapter()
    {
        super.notifyAdapter();
        if (getActivity() == null)
        {
            return;
        } else
        {
            getActivity().runOnUiThread(new _cls6());
            return;
        }
    }

    protected void onBrowseMediaSuccess(final Result result)
    {
        getActivity().runOnUiThread(new _cls1());
    }

    protected void onItemClicked(int i)
    {
        Logger.d("doss", (new StringBuilder()).append("onItemClicked:").append(i).toString());
        if (DlnaManager.getInstance(mCon).getOperationStroageModel().getCurrentPlayingItem() != null && DlnaManager.getInstance(mCon).getOperationStroageModel().getCurrentPlayingItem().fileID == ((Item)getItems().get(i)).fileID && DlnaManager.getInstance(mCon).getOperationStroageModel().getCurrentPlayingItem().title.equals(((Item)getItems().get(i)).title))
        {
            if (!DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying)
            {
                ActionModel actionmodel = ActionModel.createModel(new _cls2(mDeviceItem.getAVservice()));
                mPlayModule.play(actionmodel);
                return;
            } else
            {
                ActionModel actionmodel1 = ActionModel.createModel(new _cls3(mDeviceItem.getAVservice()));
                mPlayModule.pause(actionmodel1);
                return;
            }
        }
        task = new _cls4();
        if (timer != null)
        {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(task, 30000L);
        getActivity().runOnUiThread(new _cls5());
        prePlaySound(i);
    }

    protected void onMediaChanged()
    {
    }

    protected void onMediaChangedReceived()
    {
    }

    protected void onSoundChanged(BaseCurrentPlayingModel basecurrentplayingmodel)
    {
        if (!TextUtils.isEmpty(basecurrentplayingmodel.getCurrentPlayList()) && !basecurrentplayingmodel.getCurrentPlayList().equals("localDOWNLOAD") && !basecurrentplayingmodel.getCurrentPlayList().equals("localUSBDISK") && !basecurrentplayingmodel.getCurrentPlayList().equals("localEXTDOWNLOAD"))
        {
            if (getActivity() != null && this != null)
            {
                clearPlayQueue();
            }
        } else
        {
            mPositionInCurrentPage = basecurrentplayingmodel.getCurrentTrackNum() - 1;
            if (basecurrentplayingmodel.getCurrentPage() != -1)
            {
                currentQueuePage = basecurrentplayingmodel.getCurrentPage();
            }
            mPositionInListView = loadingData.pageSize * (currentQueuePage - 1) + mPositionInCurrentPage;
            if (DlnaManager.getInstance(mCon).getOperationStroageModel() != null && basecurrentplayingmodel.getCurrentPage() != -1 && basecurrentplayingmodel.getCurrentPage() > loadingData.pageId - 1)
            {
                Logger.d(TAG, (new StringBuilder()).append("currentPage:").append(currentQueuePage).toString());
                Logger.d(TAG, (new StringBuilder()).append("pageId:").append(loadingData.pageId).toString());
                loadingData.loadingNextPageAuto = true;
                loadMoreData();
                DlnaManager.getInstance(mCon).getOperationStroageModel().clearCurrentPlaying();
                return;
            }
            Logger.d(TAG, (new StringBuilder()).append("post mPosition:").append(mPositionInListView).toString());
            if (DlnaManager.getInstance(mCon).getOperationStroageModel() != null && mPositionInListView >= 0 && mPositionInListView < getItems().size())
            {
                DlnaManager.getInstance(mCon).getOperationStroageModel().setCurrentPlaying((Item)getItems().get(mPositionInListView));
                return;
            }
        }
    }

    protected void onSoundNextReceived()
    {
        if (mPositionInListView < getItems().size())
        {
            DlnaManager.getInstance(mCon).getOperationStroageModel().setCurrentPlaying((Item)getItems().get(mPositionInListView + 1));
        }
    }

    protected void onSoundPreReceived()
    {
        if (mPositionInListView > 0)
        {
            DlnaManager.getInstance(mCon).getOperationStroageModel().setCurrentPlaying((Item)getItems().get(mPositionInListView - 1));
        }
    }

    protected void onStopReceived()
    {
    }

    protected void onStorageMediaChanged(String s)
    {
        if (!s.equals("SONGLIST-LOCAL"))
        {
            clearPlayQueue();
        }
    }

    protected void prePlaySound(int i)
    {
        soundPage = 0;
        mPositionInListView = i;
        if (i % loadingData.pageSize == 0)
        {
            soundPage = i / loadingData.pageSize;
        } else
        {
            soundPage = i / loadingData.pageSize + 1;
        }
        Logger.d("doss", (new StringBuilder()).append("soundPage-currentQueuePage:").append(soundPage).append("-").append(currentQueuePage).toString());
        playSound(i);
    }

    protected void refreshMediaList()
    {
        isRefreshMedia = true;
    }










/*
    static MyProgressDialog access$1602(CommonBaseWifiDevicePlayableListFragment commonbasewifideviceplayablelistfragment, MyProgressDialog myprogressdialog)
    {
        commonbasewifideviceplayablelistfragment.loadingDialog = myprogressdialog;
        return myprogressdialog;
    }

*/


/*
    static MyProgressDialog access$1702(CommonBaseWifiDevicePlayableListFragment commonbasewifideviceplayablelistfragment, MyProgressDialog myprogressdialog)
    {
        commonbasewifideviceplayablelistfragment.loadingDialog = myprogressdialog;
        return myprogressdialog;
    }

*/














/*
    static boolean access$502(CommonBaseWifiDevicePlayableListFragment commonbasewifideviceplayablelistfragment, boolean flag)
    {
        commonbasewifideviceplayablelistfragment.isRefreshMedia = flag;
        return flag;
    }

*/






    private class _cls1
        implements Runnable
    {

        final CommonBaseWifiDevicePlayableListFragment this$0;
        final Result val$result;

        public void run()
        {
            Logger.d("dosstest", (new StringBuilder()).append("").append(result.items.size()).toString());
            if (getItems() != null)
            {
                getItems().removeAll(result.items);
                getItems().addAll(result.items);
                append.totalPages = result.totalPages;
                if (access$400)
                {
                    false.access$400 = <no variable>;
                } else
                {
                    com.ximalaya.ting.android.fragment.device.BaseDevicePlayListFragment.LoadingData loadingdata = append;
                    loadingdata.pageId = loadingdata.pageId + 1;
                }
                if (result.totalPages >= append.pageId)
                {
                    showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.MORE);
                } else
                {
                    showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                }
                if (append.loadingNextPageAuto)
                {
                    Logger.d(TAG, (new StringBuilder()).append("\u66F4\u65B0\u6570\u636E\u5B8C\u6210:").append().toString());
                    append.loadingNextPageAuto = false;
                    DlnaManager.getInstance(mCon).getOperationStroageModel().setCurrentPlaying((Item)getItems().get());
                }
                notifyAdapter();
            }
        }

        _cls1()
        {
            this$0 = CommonBaseWifiDevicePlayableListFragment.this;
            result = result1;
            super();
        }
    }


    private class _cls2 extends PlayActionCallback
    {

        final CommonBaseWifiDevicePlayableListFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void success(ActionInvocation actioninvocation)
        {
            DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying = true;
        }

        _cls2(Service service)
        {
            this$0 = CommonBaseWifiDevicePlayableListFragment.this;
            super(service);
        }
    }


    private class _cls3 extends PauseActionCallback
    {

        final CommonBaseWifiDevicePlayableListFragment this$0;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
        }

        public void success(ActionInvocation actioninvocation)
        {
            DlnaManager.getInstance(mCon).getOperationStroageModel().isPlaying = false;
        }

        _cls3(Service service)
        {
            this$0 = CommonBaseWifiDevicePlayableListFragment.this;
            super(service);
        }
    }



    private class _cls5
        implements Runnable
    {

        final CommonBaseWifiDevicePlayableListFragment this$0;

        public void run()
        {
            
// JavaClassFileOutputException: get_constant: invalid tag

        _cls5()
        {
            this$0 = CommonBaseWifiDevicePlayableListFragment.this;
            super();
        }
    }

}
