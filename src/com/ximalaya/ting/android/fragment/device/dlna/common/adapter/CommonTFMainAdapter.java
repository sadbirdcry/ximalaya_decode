// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.Directory;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukeController;
import com.ximalaya.ting.android.fragment.device.doss.DossUtils;
import java.util.List;

public class CommonTFMainAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        public TextView name;
        public TextView number;

        private ViewHolder()
        {
        }

    }


    private Context mContext;
    private List mData;
    private LayoutInflater mInflater;

    public CommonTFMainAdapter(Context context, List list)
    {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mData = list;
    }

    public int getCount()
    {
        return mData.size();
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        View view1;
        String s;
        if (view == null)
        {
            view1 = mInflater.inflate(0x7f030120, viewgroup, false);
            viewgroup = new ViewHolder();
            viewgroup.name = (TextView)view1.findViewById(0x7f0a0449);
            viewgroup.number = (TextView)view1.findViewById(0x7f0a048a);
            view1.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
            view1 = view;
        }
        s = (String)mData.get(i);
        if (s.equals("\u559C\u9A6C\u62C9\u96C5\u7684\u4E0B\u8F7D"))
        {
            view = (new StringBuilder()).append("").append(DossUtils.NUM_DOWNLOAD).toString();
            view = (new StringBuilder()).append(view).append("\u96C6").toString();
        } else
        if (s.equals("\u5176\u4ED6"))
        {
            view = (new StringBuilder()).append("").append(DossUtils.NUM_OTHERS).toString();
            view = (new StringBuilder()).append(view).append("\u96C6").toString();
        } else
        {
            view = (ShukeController)DlnaManager.getInstance(mContext).getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi);
            if (view != null)
            {
                view = (new StringBuilder()).append("").append(view.getDirectory(s, true).getNum()).toString();
                view = (new StringBuilder()).append(view).append("\u96C6").toString();
            } else
            {
                view = "";
            }
        }
        ((ViewHolder) (viewgroup)).number.setText(view);
        ((ViewHolder) (viewgroup)).name.setText(s);
        return view1;
    }
}
