// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.adapter.CommonTFMainAdapter;
import com.ximalaya.ting.android.util.CustomToast;
import java.util.ArrayList;
import java.util.List;

public class CommonTFMainFragment extends BaseListFragment
    implements android.view.View.OnClickListener
{

    private String TAG;
    protected CommonTFMainAdapter mAdapter;
    protected List mDirectionName;

    public CommonTFMainFragment()
    {
        TAG = com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonTFMainFragment.getSimpleName();
    }

    private void initUi()
    {
        mListView.setOnItemClickListener(new _cls2());
    }

    protected void initData()
    {
        mDirectionName = new ArrayList();
        mDirectionName.add("\u559C\u9A6C\u62C9\u96C5\u7684\u4E0B\u8F7D");
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initUi();
        initData();
        mAdapter = new CommonTFMainAdapter(mCon, mDirectionName);
        mListView.setAdapter(mAdapter);
    }

    public void onClick(View view)
    {
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300b2, viewgroup, false);
        mListView = (ListView)fragmentBaseContainerView.findViewById(0x7f0a005c);
        return fragmentBaseContainerView;
    }

    public void onResume()
    {
        super.onResume();
        (new Thread(new _cls1())).start();
        if (mAdapter != null)
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onStop()
    {
        super.onStop();
    }

    public void showDebugToast(String s)
    {
        CustomToast.showToast(mActivity, s, 0);
    }

    public void showToast(String s)
    {
        CustomToast.showToast(mActivity, s, 0);
    }

    protected boolean toTFListFragment(String s)
    {
        return false;
    }

    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CommonTFMainFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            adapterview = (String)mDirectionName.get(i);
            toTFListFragment(adapterview);
        }

        _cls2()
        {
            this$0 = CommonTFMainFragment.this;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final CommonTFMainFragment this$0;

        public void run()
        {
            class _cls1
                implements IActionCallBack
            {

                final _cls1 this$1;

                public void onFailed()
                {
                }

                public void onSuccess(ActionModel actionmodel)
                {
                    class _cls1
                        implements Runnable
                    {

                        final _cls1 this$2;

                        public void run()
                        {
                            mAdapter.notifyDataSetChanged();
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    getActivity().runOnUiThread(new _cls1());
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            ((BaseManageTFModule)DlnaManager.getInstance(getActivity().getApplicationContext()).getController().getModule(BaseManageTFModule.NAME)).getMediaCount(DlnaManager.getInstance(getActivity().getApplicationContext()).getOperationStroageModel().getNowDeviceItem(), new _cls1());
        }

        _cls1()
        {
            this$0 = CommonTFMainFragment.this;
            super();
        }
    }

}
