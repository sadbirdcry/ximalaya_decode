// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import com.ximalaya.action.CreateQueueActionCallback;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.module.BasePlayModule;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonListFinishedDownloadFragment

class val.position extends CreateQueueActionCallback
{

    final CommonListFinishedDownloadFragment this$0;
    final String val$listName;
    final int val$position;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        Logger.d("doss", "CreateQueue Failure");
    }

    public void success(ActionInvocation actioninvocation)
    {
        Logger.d("doss", "CreateQueue Success");
        class _cls1 extends PlayQueueWithIndex
        {

            final CommonListFinishedDownloadFragment._cls2 this$1;

            public void failure(ActionInvocation actioninvocation1, UpnpResponse upnpresponse, String s)
            {
                Log.d("doss", "playwithIndex FAILURE");
            }

            public void success(ActionInvocation actioninvocation1)
            {
                Log.d("doss", "playwithIndex SUCCESS");
                CommonListFinishedDownloadFragment.access$300(this$0);
            }

            _cls1(Service service, String s, int i)
            {
                this$1 = CommonListFinishedDownloadFragment._cls2.this;
                super(service, s, i);
            }
        }

        actioninvocation = ActionModel.createModel(new _cls1(((CommonDeviceItem)CommonListFinishedDownloadFragment.access$100(CommonListFinishedDownloadFragment.this)).getPQservice(), val$listName, val$position % CommonListFinishedDownloadFragment.access$200(CommonListFinishedDownloadFragment.this).pageSize + 1));
        CommonListFinishedDownloadFragment.access$400(CommonListFinishedDownloadFragment.this).playWithIndex(actioninvocation);
    }

    _cls1(String s1, int i)
    {
        this$0 = final_commonlistfinisheddownloadfragment;
        val$listName = s1;
        val$position = i;
        super(final_service, String.this);
    }
}
