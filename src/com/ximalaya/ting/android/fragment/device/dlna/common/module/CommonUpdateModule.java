// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import android.content.Context;
import android.support.v4.app.Fragment;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.ManageFragment;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseUpdateModule;
import com.ximalaya.ting.android.fragment.device.doss.DossSettingFragment;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import java.lang.ref.SoftReference;
import java.util.List;
import java.util.Map;
import org.teleal.cling.controlpoint.ActionCallback;
import org.teleal.cling.controlpoint.ControlPoint;

public class CommonUpdateModule extends BaseUpdateModule
{

    public static String CALL_BACK = "callback";
    public static String DEVICE = "device";
    public static boolean isUpdating = false;

    public CommonUpdateModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    private void onUpdateSuccess()
    {
        CustomToast.showToast(mContext, "\u56FA\u4EF6\u5347\u7EA7\u5B8C\u6210\uFF0C\u8BF7\u7A0D\u540E\uFF0C\u7CFB\u7EDF\u6B63\u5728\u91CD\u542F", 0);
        Object obj = MyApplication.a();
        if (obj != null && (obj instanceof MainTabActivity2))
        {
            int i;
            if ((MainTabActivity2)obj != null)
            {
                i = ((MainTabActivity2)obj).getManageFragment().mStacks.size();
            } else
            {
                i = 0;
            }
            if (i >= 1)
            {
                obj = (Fragment)((SoftReference)((MainTabActivity2)obj).getManageFragment().mStacks.get(i - 1)).get();
                if (obj instanceof DossSettingFragment)
                {
                    ((DossSettingFragment)obj).setUpdateInfo(false);
                }
            }
        }
    }

    public void checkUpdate(ActionModel actionmodel)
    {
        actionmodel = (ActionCallback)actionmodel.getDefault();
        Logger.d("doss", "checkUpdate IN");
        getControlPoint().execute(actionmodel);
    }

    public void doUpdate(final ActionModel callBack)
    {
        Logger.d("update", "doUpdate IN");
        final BaseDeviceItem deviceItem = (BaseDeviceItem)callBack.result.get(DEVICE);
        callBack = (IActionCallBack)callBack.result.get(CALL_BACK);
        if (isUpdating)
        {
            return;
        } else
        {
            (new Thread(new _cls1())).start();
            return;
        }
    }

    public String getMvRemoteUpdateStart(BaseDeviceItem basedeviceitem)
    {
        Logger.d("update", "getMvRemoteUpdateStart IN");
        basedeviceitem = (new StringBuilder()).append("http://").append(basedeviceitem.getIpAddress()).toString();
        basedeviceitem = (new StringBuilder()).append(basedeviceitem).append("/httpapi.asp?command=getMvRemoteUpdateStart").toString();
        return f.a().a(basedeviceitem, null, null, null);
    }

    public String getMvRemoteUpdateStatus(BaseDeviceItem basedeviceitem)
    {
        Logger.d("update", "getMvRemoteUpdateStatus IN");
        basedeviceitem = (new StringBuilder()).append("http://").append(basedeviceitem.getIpAddress()).toString();
        basedeviceitem = (new StringBuilder()).append(basedeviceitem).append("/httpapi.asp?command=getMvRemoteUpdateStatus").toString();
        return f.a().a(basedeviceitem, null, null, null);
    }

    public String getMvRomDownloadStatus(BaseDeviceItem basedeviceitem)
    {
        Logger.d("update", "getMvRomDownloadStatus IN");
        basedeviceitem = (new StringBuilder()).append("http://").append(basedeviceitem.getIpAddress()).toString();
        basedeviceitem = (new StringBuilder()).append(basedeviceitem).append("/httpapi.asp?command=getMvRomDownloadStatus").toString();
        return f.a().a(basedeviceitem, null, null, null);
    }



    private class _cls1
        implements Runnable
    {

        final CommonUpdateModule this$0;
        final IActionCallBack val$callBack;
        final BaseDeviceItem val$deviceItem;

        public void run()
        {
            Object obj;
            do
            {
                obj = getMvRemoteUpdateStart(deviceItem);
                Logger.d("update", (new StringBuilder()).append("ret1=").append(((String) (obj))).toString());
            } while (!((String) (obj)).equals("OK"));
            do
            {
                CommonUpdateModule.isUpdating = true;
                Object obj1;
                String s;
                try
                {
                    Thread.sleep(2000L);
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    ((InterruptedException) (obj)).printStackTrace();
                }
                obj = getMvRemoteUpdateStatus(deviceItem);
                Logger.d("update", (new StringBuilder()).append("ret2=").append(((String) (obj))).toString());
            } while (((String) (obj)).equals(Integer.valueOf(10)));
            if (((String) (obj)).equals("32") || ((String) (obj)).equals("25") || ((String) (obj)).equals("40"))
            {
                obj = null;
                do
                {
                    try
                    {
                        Thread.sleep(2000L);
                    }
                    catch (InterruptedException interruptedexception)
                    {
                        interruptedexception.printStackTrace();
                    }
                    s = getMvRomDownloadStatus(deviceItem);
                    Logger.d("update", (new StringBuilder()).append("ret3=").append(s).toString());
                    obj1 = obj;
                    if (s != null)
                    {
                        obj1 = (UpdateStatusModel)JSON.parseObject(s, com/ximalaya/ting/android/fragment/device/dlna/model/UpdateStatusModel);
                    }
                    if (((UpdateStatusModel) (obj1)).getStatus() == 2 || ((UpdateStatusModel) (obj1)).getStatus() == 5)
                    {
                        break;
                    }
                    obj = obj1;
                } while (((UpdateStatusModel) (obj1)).getStatus() != 6);
                CommonUpdateModule.isUpdating = false;
                if (((UpdateStatusModel) (obj1)).getStatus() == 6)
                {
                    callBack.onSuccess(null);
                    onUpdateSuccess();
                    return;
                } else
                {
                    callBack.onFailed();
                    return;
                }
            }
            if (((String) (obj)).equals("22") || ((String) (obj)).equals("31"))
            {
                callBack.onFailed();
                return;
            }
            if (((String) (obj)).equals("20"))
            {
                callBack.onFailed();
                return;
            } else
            {
                callBack.onFailed();
                return;
            }
        }

        _cls1()
        {
            this$0 = CommonUpdateModule.this;
            deviceItem = basedeviceitem;
            callBack = iactioncallback;
            super();
        }
    }

}
