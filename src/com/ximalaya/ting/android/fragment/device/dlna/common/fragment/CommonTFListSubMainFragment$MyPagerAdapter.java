// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.common.fragment:
//            CommonTFListSubMainFragment, CommonListFinishedDownloadFragment, CommonListUnFinishedTaskFragment

public class this._cls0 extends FragmentPagerAdapter
{

    private final String TITLES[] = {
        "\u5DF2\u4E0B\u8F7D", "\u6B63\u5728\u4E0B\u8F7D"
    };
    final CommonTFListSubMainFragment this$0;

    public int getCount()
    {
        return TITLES.length;
    }

    public Fragment getItem(int i)
    {
        if (i == 0)
        {
            if (CommonTFListSubMainFragment.access$100(CommonTFListSubMainFragment.this) == null)
            {
                CommonTFListSubMainFragment.access$102(CommonTFListSubMainFragment.this, getFinishedDownloadFragment());
                Bundle bundle = new Bundle();
                bundle.putInt("NUM", CommonTFListSubMainFragment.access$400(CommonTFListSubMainFragment.this));
                bundle.putString("directionName", "\u5DF2\u4E0B\u8F7D");
                CommonTFListSubMainFragment.access$100(CommonTFListSubMainFragment.this).setArguments(bundle);
            }
            return CommonTFListSubMainFragment.access$100(CommonTFListSubMainFragment.this);
        }
        if (i == 1)
        {
            if (CommonTFListSubMainFragment.access$300(CommonTFListSubMainFragment.this) == null)
            {
                CommonTFListSubMainFragment.access$302(CommonTFListSubMainFragment.this, new CommonListUnFinishedTaskFragment());
                Bundle bundle1 = new Bundle();
                bundle1.putInt("NUM", CommonTFListSubMainFragment.access$400(CommonTFListSubMainFragment.this));
                bundle1.putString("directionName", "\u6B63\u5728\u4E0B\u8F7D");
                CommonTFListSubMainFragment.access$300(CommonTFListSubMainFragment.this).setArguments(bundle1);
            }
            return CommonTFListSubMainFragment.access$300(CommonTFListSubMainFragment.this);
        }
        if (CommonTFListSubMainFragment.access$100(CommonTFListSubMainFragment.this) == null)
        {
            CommonTFListSubMainFragment.access$102(CommonTFListSubMainFragment.this, getFinishedDownloadFragment());
            Bundle bundle2 = new Bundle();
            bundle2.putInt("NUM", CommonTFListSubMainFragment.access$400(CommonTFListSubMainFragment.this));
            bundle2.putString("directionName", "\u5DF2\u4E0B\u8F7D");
            CommonTFListSubMainFragment.access$100(CommonTFListSubMainFragment.this).setArguments(bundle2);
        }
        return CommonTFListSubMainFragment.access$100(CommonTFListSubMainFragment.this);
    }

    public CharSequence getPageTitle(int i)
    {
        return TITLES[i];
    }

    public (FragmentManager fragmentmanager)
    {
        this$0 = CommonTFListSubMainFragment.this;
        super(fragmentmanager);
    }
}
