// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.model;


public class Directory
{

    boolean canAddFiles;
    String name;
    int num;
    String reqName;

    public Directory(String s, String s1, boolean flag)
    {
        canAddFiles = true;
        name = s;
        reqName = s1;
        num = 0;
        canAddFiles = flag;
    }

    public String getName()
    {
        return name;
    }

    public int getNum()
    {
        return num;
    }

    public String getReqName()
    {
        return reqName;
    }

    public boolean isCanAddFiles()
    {
        return canAddFiles;
    }

    public void setCanAddFiles(boolean flag)
    {
        canAddFiles = flag;
    }

    public void setName(String s)
    {
        name = s;
    }

    public void setNum(int i)
    {
        num = i;
    }

    public void setReqName(String s)
    {
        reqName = s;
    }
}
