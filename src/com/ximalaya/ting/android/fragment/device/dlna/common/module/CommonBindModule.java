// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.module;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.ximalaya.model.playqueue.ListInfo;
import com.ximalaya.model.playqueue.PlayList;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.ManageFragment;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BindCommandModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseBindModule;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.CustomToast;
import java.lang.ref.SoftReference;
import java.util.List;
import java.util.Map;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.support.playqueue.callback.backupqueue.BackupQueueConstants;

public class CommonBindModule extends BaseBindModule
{

    public CommonBindModule(Context context, ControlPoint controlpoint)
    {
        super(context, controlpoint);
    }

    private void showToast(String s)
    {
        CustomToast.showToast(mContext, s, 0);
    }

    public void bind(BaseDeviceItem basedeviceitem, ActionModel actionmodel)
    {
        basedeviceitem = (CommonDeviceItem)basedeviceitem;
        actionmodel = (BindCommandModel)actionmodel.result.get("bindcommand");
        AlbumModel albummodel = ((BindCommandModel) (actionmodel)).mAlbumModel;
        Object obj = new PlayList();
        obj.listName = albummodel.title;
        ((PlayList) (obj)).listInfo.searchUrl = (new StringBuilder()).append("http://3rd.ximalaya.com/albums/").append(albummodel.albumId).append("/tracks?i_am=doss&per_page=20&is_asc=true&page=1").toString();
        Log.d("WIFI", ((PlayList) (obj)).listInfo.searchUrl);
        ((PlayList) (obj)).listInfo.sourceName = "Ximalaya";
        ((PlayList) (obj)).listInfo.SwitchPageMode = 1;
        obj = BackupQueueConstants.getBackupQueueContext(((PlayList) (obj)));
        getControlPoint().execute(new _cls2(albummodel, actionmodel));
    }

    public void freshBindFragment()
    {
        Activity activity = MyApplication.a();
        if (activity != null && (activity instanceof MainTabActivity2))
        {
            int i = 0;
            if ((MainTabActivity2)activity != null)
            {
                i = ((MainTabActivity2)activity).getManageFragment().mStacks.size();
            }
            if (i >= 1)
            {
                final Fragment mFra = (Fragment)((SoftReference)((MainTabActivity2)activity).getManageFragment().mStacks.get(i - 1)).get();
                if (mFra.getClass().getSimpleName().contains("ContentFragment"))
                {
                    activity.runOnUiThread(new _cls1());
                }
            }
        }
    }




    private class _cls2 extends BackUpQueueActionCallback
    {

        final CommonBindModule this$0;
        final BindCommandModel val$mBindCommand;
        final CommonDeviceItem val$mDeviceItem;
        final AlbumModel val$model;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            showToast("\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
            Logger.d("doss", "BackUpQueue Failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            Logger.d("doss", "BackUpQueue Success");
            class _cls1 extends GetKeyMappingActionCallback
            {

                final _cls2 this$1;

                public void failure(ActionInvocation actioninvocation1, UpnpResponse upnpresponse, String s)
                {
                    showToast("\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
                    Logger.d("doss", "GetKeyMappingActionCallback Failure");
                }

                public void received(ActionInvocation actioninvocation1, List list)
                {
                    Logger.d("doss", "GetKeyMappingActionCallback Success");
                    actioninvocation1 = new Key();
                    actioninvocation1.name = model.title;
                    list.set(mBindCommand.mChannelId, actioninvocation1);
                    actioninvocation1 = KeyMappingQueueConstants.getSetKeyMappingQueueContext(list);
                    class _cls1 extends SetKeyMappingActionCallback
                    {

                        final _cls1 this$2;

                        public void failure(ActionInvocation actioninvocation2, UpnpResponse upnpresponse, String s)
                        {
                            showToast("\u7ED1\u5B9A\u4E13\u8F91\u5931\u8D25");
                            Logger.d(this$1, "SetKeyMapping failure");
                        }

                        public void success(ActionInvocation actioninvocation2)
                        {
                            class _cls1 extends TimerTask
                            {

                                final _cls1 this$3;

                                public void run()
                                {
                                    showToast("\u7ED1\u5B9A\u4E13\u8F91\u6210\u529F");
                                    Logger.d(_fld2, "SetKeyMapping Success");
                                    mDeviceItem.getBaseBindableModel().setAlbums(mBindCommand.mChannelId, mBindCommand.mAlbumModel);
                                    freshBindFragment();
                                }

                                    _cls1()
                                    {
                                        this$3 = _cls1.this;
                                        super();
                                    }
                            }

                            (new Timer()).schedule(new _cls1(), 500L);
                        }

                            _cls1(Service service, String s)
                            {
                                this$2 = _cls1.this;
                                super(service, s);
                            }
                    }

                    getControlPoint().execute(new _cls1(mDeviceItem.getPQservice(), actioninvocation1));
                }

                _cls1(Service service)
                {
                    this$1 = _cls2.this;
                    super(service);
                }
            }

            getControlPoint().execute(new _cls1(mDeviceItem.getPQservice()));
        }

        _cls2(AlbumModel albummodel, BindCommandModel bindcommandmodel)
        {
            this$0 = CommonBindModule.this;
            mDeviceItem = commondeviceitem;
            model = albummodel;
            mBindCommand = bindcommandmodel;
            super(final_service, final_s);
        }
    }


    private class _cls1
        implements Runnable
    {

        final CommonBindModule this$0;
        final Fragment val$mFra;

        public void run()
        {
            mFra.onResume();
        }

        _cls1()
        {
            this$0 = CommonBindModule.this;
            mFra = fragment;
            super();
        }
    }

}
