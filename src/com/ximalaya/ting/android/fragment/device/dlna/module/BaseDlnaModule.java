// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.module;

import android.content.Context;
import org.teleal.cling.controlpoint.ControlPoint;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.module:
//            IBaseModule

public abstract class BaseDlnaModule
    implements IBaseModule
{

    protected Context mContext;
    protected ControlPoint mControlPoint;

    public BaseDlnaModule(Context context, ControlPoint controlpoint)
    {
        mContext = context;
        setControlPoint(controlpoint);
    }

    public ControlPoint getControlPoint()
    {
        return mControlPoint;
    }

    public abstract String getModuleName();

    public void setControlPoint(ControlPoint controlpoint)
    {
        mControlPoint = controlpoint;
    }
}
