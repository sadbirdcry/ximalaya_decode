// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.common.adapter;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.device.dlna.BaseCurrentPlayingModel;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.model.sound.AlbumSoundModelNew;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class CommonSoundsAlbumAdapter extends BaseAdapter
{
    protected static class ViewHolder
    {

        public ImageView adIndicator;
        public View border;
        public ImageButton btn;
        public TextView commentCount;
        public ImageView cover;
        public TextView createTime;
        public ImageView download;
        public TextView duration;
        public TextView likeCount;
        public ImageView newUpdate;
        public TextView origin;
        public TextView owner;
        public TextView playCount;
        public ImageView playFlag;
        public int position;
        public TextView title;
        public TextView transmitCount;

        protected ViewHolder()
        {
        }
    }


    Activity conActivity;
    List mList;
    String mPlayList;

    public CommonSoundsAlbumAdapter(Activity activity, List list, String s)
    {
        conActivity = activity;
        mList = list;
        mPlayList = s;
    }

    protected void bindData(AlbumSoundModelNew albumsoundmodelnew, ViewHolder viewholder)
    {
        SimpleDateFormat simpledateformat;
        long l;
        viewholder.title.setText(albumsoundmodelnew.title);
        albumsoundmodelnew.created_at = albumsoundmodelnew.created_at.replaceAll("T", " ");
        albumsoundmodelnew.created_at = albumsoundmodelnew.created_at.replaceAll("Z", " ");
        simpledateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        l = 0L;
        long l1 = simpledateformat.parse(albumsoundmodelnew.created_at).getTime();
        l = l1;
_L1:
        viewholder.createTime.setText(ToolUtil.convertTime(l));
        if (isPlaying(albumsoundmodelnew))
        {
            viewholder.playFlag.setVisibility(0);
            ParseException parseexception;
            if (!DlnaManager.getInstance(conActivity).isOperationPlaying())
            {
                viewholder.playFlag.setImageResource(0x7f0203e9);
            } else
            {
                viewholder.playFlag.setImageResource(0x7f0203e8);
                if (viewholder.playFlag.getDrawable() instanceof AnimationDrawable)
                {
                    final AnimationDrawable animationDrawable = (AnimationDrawable)viewholder.playFlag.getDrawable();
                    viewholder.playFlag.post(new _cls1());
                }
            }
        } else
        {
            viewholder.playFlag.setVisibility(8);
            viewholder.playFlag.setImageResource(0x7f020030);
        }
        if (albumsoundmodelnew.plays_count > 0)
        {
            viewholder.playCount.setText(StringUtil.getFriendlyNumStr(albumsoundmodelnew.plays_count));
            viewholder.playCount.setVisibility(0);
        } else
        {
            viewholder.playCount.setText("0");
            viewholder.playCount.setVisibility(8);
        }
        if (albumsoundmodelnew.comments_count > 0)
        {
            viewholder.commentCount.setText(StringUtil.getFriendlyNumStr(albumsoundmodelnew.comments_count));
            viewholder.commentCount.setVisibility(0);
        } else
        {
            viewholder.commentCount.setVisibility(8);
        }
        viewholder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)albumsoundmodelnew.duration)).toString());
        viewholder.duration.setVisibility(0);
        viewholder.download.setVisibility(8);
        return;
        parseexception;
        parseexception.printStackTrace();
          goto _L1
    }

    protected View buildItemView(int i, View view, ViewGroup viewgroup)
    {
        View view1;
        if (view == null)
        {
            view1 = LayoutInflater.from(conActivity).inflate(0x7f03013f, viewgroup, false);
            view = new ViewHolder();
            view.playFlag = (ImageView)view1.findViewById(0x7f0a0022);
            view.title = (TextView)view1.findViewById(0x7f0a00e6);
            view.createTime = (TextView)view1.findViewById(0x7f0a04ed);
            view.duration = (TextView)view1.findViewById(0x7f0a04ef);
            view.commentCount = (TextView)view1.findViewById(0x7f0a04f0);
            view.playCount = (TextView)view1.findViewById(0x7f0a0151);
            view.download = (ImageView)view1.findViewById(0x7f0a02d1);
            view1.setTag(view);
            viewgroup = view;
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
            view1 = view;
        }
        viewgroup.position = i;
        return view1;
    }

    public int getCount()
    {
        return mList.size();
    }

    public AlbumSoundModelNew getItem(int i)
    {
        return (AlbumSoundModelNew)mList.get(i);
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return ((AlbumSoundModelNew)mList.get(i)).id;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        view = buildItemView(i, view, viewgroup);
        bindData((AlbumSoundModelNew)mList.get(i), (ViewHolder)view.getTag());
        return view;
    }

    public boolean isPlaying(AlbumSoundModelNew albumsoundmodelnew)
    {
        String s = ((CommonDeviceItem)DlnaManager.getInstance(conActivity).getOperationStroageModel().getNowDeviceItem()).getCurrentPlayingModel().getCurrentPlayList();
        return (TextUtils.isEmpty(s) || mPlayList.equals(s)) && DlnaManager.getInstance(conActivity).getOperationStroageModel() != null && DlnaManager.getInstance(conActivity).getOperationStroageModel().getCurrentPlayingSoundModel() != null && albumsoundmodelnew.id == DlnaManager.getInstance(conActivity).getOperationStroageModel().getCurrentPlayingSoundModel().id;
    }

    private class _cls1
        implements Runnable
    {

        final CommonSoundsAlbumAdapter this$0;
        final AnimationDrawable val$animationDrawable;

        public void run()
        {
            if (animationDrawable != null && !animationDrawable.isRunning())
            {
                animationDrawable.start();
            }
        }

        _cls1()
        {
            this$0 = CommonSoundsAlbumAdapter.this;
            animationDrawable = animationdrawable;
            super();
        }
    }

}
