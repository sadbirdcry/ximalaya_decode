// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.dlna.shuke;

import android.view.View;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.fragment.album.SectionContentAdapter;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonDeviceAlbumSectionDownloadFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonTFListSubMainFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonDownloadModule;
import com.ximalaya.ting.android.fragment.device.dlna.model.Directory;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.dlna.shuke:
//            ShukeController, ShukeTFListSubMainFragment

public class ShukeDeviceAlbumSectionDownloadFragment extends CommonDeviceAlbumSectionDownloadFragment
{

    MenuDialog mDeviceMenuDialog;

    public ShukeDeviceAlbumSectionDownloadFragment()
    {
    }

    private void showDirectionChoose(final List tasks)
    {
        ArrayList arraylist = new ArrayList();
        final ShukeController controller = (ShukeController)DlnaManager.getInstance(mCon).getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi);
        if (controller == null)
        {
            return;
        }
        for (int i = 0; i < controller.getDirectoryNum(); i++)
        {
            if (controller.getDirectory(i).isCanAddFiles())
            {
                arraylist.add(controller.getDirectory(i).getName());
            }
        }

        mDeviceMenuDialog = new MenuDialog(MyApplication.a(), arraylist, a.e, new _cls1(), 1);
        mDeviceMenuDialog.setHeaderTitle("\u6DFB\u52A0\u81F3\u6587\u4EF6\u5939");
        mDeviceMenuDialog.show();
    }

    public void onClick(View view)
    {
        boolean flag1 = true;
        view.getId();
        JVM INSTR lookupswitch 2: default 32
    //                   2131362504: 65
    //                   2131362513: 39;
           goto _L1 _L2 _L3
_L2:
        break MISSING_BLOCK_LABEL_65;
_L1:
        boolean flag = false;
_L4:
        List list;
        if (flag)
        {
            return;
        } else
        {
            super.onClick(view);
            return;
        }
_L3:
        list = mSectionContentAdapter.getCheckedTracks();
        flag = flag1;
        if (list != null)
        {
            showDirectionChoose(list);
            flag = flag1;
        }
          goto _L4
        CommonTFListSubMainFragment.showDownloading = true;
        goBackFragment(com/ximalaya/ting/android/fragment/device/dlna/shuke/ShukeTFListSubMainFragment);
        flag = flag1;
          goto _L4
    }



    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final ShukeDeviceAlbumSectionDownloadFragment this$0;
        final ShukeController val$controller;
        final List val$tasks;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            DossUtils.goDownload(getActivity().getApplicationContext(), ShukeDeviceAlbumSectionDownloadFragment.this.tasks, ShukeDeviceAlbumSectionDownloadFragment.this.Object, tasks, controller.getDirectory((String)mDeviceMenuDialog.getSelections().get(i), true).getReqName(), com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi);
            CustomToast.showToast(mCon, "\u6B63\u5728\u51C6\u5907\u4E0B\u8F7D", 0);
            mDeviceMenuDialog.dismiss();
        }

        _cls1()
        {
            this$0 = ShukeDeviceAlbumSectionDownloadFragment.this;
            tasks = list;
            controller = shukecontroller;
            super();
        }
    }

}
