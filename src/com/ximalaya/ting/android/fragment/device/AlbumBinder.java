// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.app.Activity;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.BaseBindableDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.model.BindCommandModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseBindModule;
import com.ximalaya.ting.android.fragment.device.ximao.KeyInfo;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm;
import com.ximalaya.ting.android.model.album.AlbumModel;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            MyDeviceManager

public class AlbumBinder
{

    public AlbumBinder()
    {
    }

    public static void bindDevice(int i, AlbumModel albummodel, Activity activity)
    {
        MyDeviceManager.DeviceType devicetype = MyDeviceManager.getInstance(activity).getNowDeviceType();
        static class _cls1
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType = new int[MyDeviceManager.DeviceType.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[MyDeviceManager.DeviceType.ximao.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[MyDeviceManager.DeviceType.tingshubao.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[MyDeviceManager.DeviceType.doss.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[MyDeviceManager.DeviceType.shukewifi.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls1..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[devicetype.ordinal()];
        JVM INSTR tableswitch 1 4: default 48
    //                   1 49
    //                   2 82
    //                   3 188
    //                   4 188;
           goto _L1 _L2 _L3 _L4 _L4
_L1:
        return;
_L2:
        albummodel = new KeyInfo(i, albummodel.title, albummodel.albumId, albummodel.tracks);
        XiMaoComm.bindingAlbum(activity.getApplicationContext(), i, activity, albummodel);
        return;
_L3:
        Object obj = new BindCommandModel();
        obj.mChannelId = i;
        obj.mAlbumId = albummodel.albumId;
        obj.mAlbumModel = albummodel;
        obj.mDeviceItem = (BaseBindableDeviceItem)DlnaManager.getInstance(activity).getOperationStroageModel().getNowDeviceItem();
        albummodel = new ActionModel();
        ((ActionModel) (albummodel)).result.put("bindcommand", obj);
        obj = (BaseBindModule)DlnaManager.getInstance(activity).getController(MyDeviceManager.DeviceType.tingshubao).getModule(BaseBindModule.NAME);
        if (obj != null)
        {
            ((BaseBindModule) (obj)).bind(DlnaManager.getInstance(activity).getOperationStroageModel().getNowDeviceItem(), albummodel);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L4:
        Object obj1 = new BindCommandModel();
        if (obj1 != null)
        {
            obj1.mChannelId = i;
            obj1.mAlbumId = albummodel.albumId;
            obj1.mAlbumModel = albummodel;
            obj1.mDeviceItem = (BaseBindableDeviceItem)DlnaManager.getInstance(activity).getOperationStroageModel().getNowDeviceItem();
            albummodel = new ActionModel();
            ((ActionModel) (albummodel)).result.put("bindcommand", obj1);
            obj1 = (BaseBindModule)DlnaManager.getInstance(activity).getController().getModule(BaseBindModule.NAME);
            if (obj1 != null)
            {
                ((BaseBindModule) (obj1)).bind(DlnaManager.getInstance(activity).getOperationStroageModel().getNowDeviceItem(), albummodel);
                return;
            }
        }
        if (true) goto _L1; else goto _L5
_L5:
    }
}
