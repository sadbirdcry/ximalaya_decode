// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download:
//            BaseSoundsDownloadForAlbumAdapter

public abstract class BaseDownloadAlbumListFragment extends BaseListFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
{

    private List downloadTaskList;
    private ImageView mBackImg;
    private LinearLayout mEmptyView;
    protected BaseSoundsDownloadForAlbumAdapter soundsDownloadAdapter;
    private TextView top_tv;

    public BaseDownloadAlbumListFragment()
    {
        downloadTaskList = new ArrayList();
    }

    private void doGetDownloadList()
    {
        if (getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            (new _cls2()).execute(new Void[0]);
            return;
        }
    }

    private void initData()
    {
        if (!getUserVisibleHint() || getView() == null)
        {
            return;
        } else
        {
            doGetDownloadList();
            return;
        }
    }

    private void registerListener()
    {
        DownloadHandler downloadhandler = DownloadHandler.getInstance(mCon);
        if (downloadhandler != null)
        {
            downloadhandler.addDownloadListeners(this);
        }
    }

    private void showEmptyView()
    {
        if (downloadTaskList.size() == 0)
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(0);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(0);
            mEmptyView.setVisibility(0);
            return;
        } else
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0232).setVisibility(8);
            mEmptyView.setVisibility(8);
            return;
        }
    }

    public abstract BaseSoundsDownloadForAlbumAdapter getAdapter(List list);

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        top_tv = (TextView)(TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        top_tv.setText("\u6DFB\u52A0\u8282\u76EE");
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(this);
        if (PackageUtil.isMeizu() && getActivity() != null)
        {
            mListView.setPadding(0, 0, 0, Utilities.dip2px(getActivity(), 70F));
        }
        mEmptyView = (LinearLayout)LayoutInflater.from(mCon).inflate(0x7f03007c, mListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u4E0B\u8F7D\u8FC7\u4E13\u8F91\u54E6");
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setVisibility(8);
        ((Button)mEmptyView.findViewById(0x7f0a0232)).setVisibility(8);
        mListView.addFooterView(mEmptyView);
        mListView.setAdapter(getAdapter(downloadTaskList));
        showEmptyView();
        registerListener();
        mListView.setOnItemClickListener(new _cls1());
        initData();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300e3, viewgroup, false);
        mListView = (BounceListView)fragmentBaseContainerView.findViewById(0x7f0a0229);
        return fragmentBaseContainerView;
    }

    public void onTaskComplete()
    {
        if (!isAdded() || getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            doGetDownloadList();
            return;
        }
    }

    public void onTaskDelete()
    {
        if (!isAdded() || getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            doGetDownloadList();
            return;
        }
    }

    public abstract void toSoundListFragment(long l);

    public void updateActionInfo()
    {
        doGetDownloadList();
    }

    public void updateDownloadInfo(int i)
    {
        doGetDownloadList();
    }



    private class _cls2 extends AsyncTask
    {

        final BaseDownloadAlbumListFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            return DownloadHandler.getInstance(mCon).getSortedFinishedDownloadList();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (!isAdded())
            {
                return;
            }
            if (list != null)
            {
                downloadTaskList.clear();
                downloadTaskList.addAll(list);
                soundsDownloadAdapter.setList(downloadTaskList);
                soundsDownloadAdapter.notifyDataSetChanged();
            }
            showEmptyView();
        }

        _cls2()
        {
            this$0 = BaseDownloadAlbumListFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final BaseDownloadAlbumListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (downloadTaskList != null && downloadTaskList.size() != 0)
            {
                if ((i -= mListView.getHeaderViewsCount()) >= 0 && i < soundsDownloadAdapter.getCount())
                {
                    l = ((Long)soundsDownloadAdapter.mapKey.get(i)).longValue();
                    toSoundListFragment(l);
                    return;
                }
            }
        }

        _cls1()
        {
            this$0 = BaseDownloadAlbumListFragment.this;
            super();
        }
    }

}
