// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import android.widget.CompoundButton;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.miniche:
//            QCheTingSettingFragment, QCheFmModule, QCheController

class this._cls0
    implements android.widget.angeListener
{

    final QCheTingSettingFragment this$0;

    public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
    {
        if (flag)
        {
            QCheTingSettingFragment.access$000(QCheTingSettingFragment.this).switchControl("Autoplay_on");
            QCheController.setAutoPlaying(mCon, true);
            Logger.d(QCheTingSettingFragment.TAG, "\u81EA\u52A8\u64AD\u653E\u6253\u5F00\u6307\u4EE4\u5DF2\u53D1\u9001");
            return;
        } else
        {
            QCheTingSettingFragment.access$000(QCheTingSettingFragment.this).switchControl("Autoplay_off");
            QCheController.setAutoPlaying(mCon, false);
            Logger.d(QCheTingSettingFragment.TAG, "\u81EA\u52A8\u64AD\u653E\u5173\u95ED\u6307\u4EE4\u5DF2\u53D1\u9001");
            return;
        }
    }

    ()
    {
        this$0 = QCheTingSettingFragment.this;
        super();
    }
}
