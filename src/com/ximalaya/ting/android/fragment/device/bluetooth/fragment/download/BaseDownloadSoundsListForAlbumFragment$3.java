// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download;

import android.widget.TextView;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download:
//            BaseDownloadSoundsListForAlbumFragment, BaseDownloadedSoundListAdapter

class this._cls0
    implements Runnable
{

    final BaseDownloadSoundsListForAlbumFragment this$0;

    public void run()
    {
        if (isAdded())
        {
            Object obj = DownloadHandler.getInstance(mCon);
            Logger.log("0814", (new StringBuilder()).append("====DownloadSoundsListForAlbumFragment========GET downloadTaskList: ").append(BaseDownloadSoundsListForAlbumFragment.access$400(BaseDownloadSoundsListForAlbumFragment.this).size()).toString());
            obj = ((DownloadHandler) (obj)).getSortedFinishedDownloadList();
            if (obj != null && ((List) (obj)).size() != 0)
            {
                int i = SharedPreferencesUtil.getInstance(getActivity()).getInt((new StringBuilder()).append("download_album_soundlist_order").append(BaseDownloadSoundsListForAlbumFragment.access$100(BaseDownloadSoundsListForAlbumFragment.this)).toString(), 1);
                BaseDownloadSoundsListForAlbumFragment.access$000(BaseDownloadSoundsListForAlbumFragment.this).clear();
                obj = ((List) (obj)).iterator();
                do
                {
                    if (!((Iterator) (obj)).hasNext())
                    {
                        break;
                    }
                    DownloadTask downloadtask = (DownloadTask)((Iterator) (obj)).next();
                    if (downloadtask.albumId == BaseDownloadSoundsListForAlbumFragment.access$100(BaseDownloadSoundsListForAlbumFragment.this))
                    {
                        BaseDownloadSoundsListForAlbumFragment.access$000(BaseDownloadSoundsListForAlbumFragment.this).add(downloadtask);
                    }
                } while (true);
                obj = new ArrayList();
                if (i == -1)
                {
                    ((List) (obj)).addAll(BaseDownloadSoundsListForAlbumFragment.access$000(BaseDownloadSoundsListForAlbumFragment.this));
                    Collections.reverse(((List) (obj)));
                } else
                {
                    obj = BaseDownloadSoundsListForAlbumFragment.access$000(BaseDownloadSoundsListForAlbumFragment.this);
                }
                BaseDownloadSoundsListForAlbumFragment.access$400(BaseDownloadSoundsListForAlbumFragment.this).clear();
                BaseDownloadSoundsListForAlbumFragment.access$400(BaseDownloadSoundsListForAlbumFragment.this).addAll(((java.util.Collection) (obj)));
                soundsDownloadAdapter.notifyDataSetChanged();
                BaseDownloadSoundsListForAlbumFragment.access$500(BaseDownloadSoundsListForAlbumFragment.this);
                BaseDownloadSoundsListForAlbumFragment.access$300(BaseDownloadSoundsListForAlbumFragment.this);
                if (BaseDownloadSoundsListForAlbumFragment.access$100(BaseDownloadSoundsListForAlbumFragment.this) == 0L)
                {
                    BaseDownloadSoundsListForAlbumFragment.access$600(BaseDownloadSoundsListForAlbumFragment.this).setText("\u672A\u547D\u540D\u4E13\u8F91");
                    return;
                }
                if (BaseDownloadSoundsListForAlbumFragment.access$400(BaseDownloadSoundsListForAlbumFragment.this) != null && BaseDownloadSoundsListForAlbumFragment.access$400(BaseDownloadSoundsListForAlbumFragment.this).size() > 0)
                {
                    BaseDownloadSoundsListForAlbumFragment.access$600(BaseDownloadSoundsListForAlbumFragment.this).setText(((DownloadTask)BaseDownloadSoundsListForAlbumFragment.access$400(BaseDownloadSoundsListForAlbumFragment.this).get(0)).albumName);
                    return;
                }
            }
        }
    }

    ()
    {
        this$0 = BaseDownloadSoundsListForAlbumFragment.this;
        super();
    }
}
