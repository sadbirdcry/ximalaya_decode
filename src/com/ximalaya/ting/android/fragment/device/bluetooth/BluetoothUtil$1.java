// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            BluetoothManager, BaseBluetoothCallBack

final class val.callback
    implements android.bluetooth..ServiceListener
{

    final BaseBluetoothCallBack val$callback;
    final Context val$context;

    public void onServiceConnected(int i, BluetoothProfile bluetoothprofile)
    {
        Object obj = bluetoothprofile.getConnectedDevices();
        BluetoothManager.getInstance(val$context).setBluetoothProfile(bluetoothprofile);
        if (obj != null && ((List) (obj)).size() != 0)
        {
            bluetoothprofile = (BluetoothDevice)((List) (obj)).get(0);
            obj = new ActionModel();
            ((ActionModel) (obj)).result.put("device", bluetoothprofile);
            val$callback.onSuccess(((ActionModel) (obj)));
        }
    }

    public void onServiceDisconnected(int i)
    {
    }

    lBack()
    {
        val$context = context1;
        val$callback = basebluetoothcallback;
        super();
    }
}
