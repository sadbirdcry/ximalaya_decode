// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;


public class BtPackage
{

    private int length;
    private byte pack[];

    public BtPackage(byte abyte0[], int i)
    {
        pack = abyte0;
        length = i;
    }

    public int getLength()
    {
        return length;
    }

    public byte[] getPack()
    {
        return pack;
    }

    public void setLength(int i)
    {
        length = i;
    }

    public void setPack(byte abyte0[])
    {
        pack = abyte0;
    }
}
