// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.ximao;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.bluetooth.IDownload;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.xdcs.CdnCollectData;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.MemorySpaceUtil;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

public class XiMaoDownloadTask
    implements IDownload, Runnable
{

    public static final int CHECK_EXIST = 1;
    public static final int DOWNLOAD = 3;
    public static final int DOWNLOADACC = 5;
    public static final int DOWNLOAD_AAC = 7;
    public static final int FINISH = 4;
    public static final int PERCENTAGE_DOWNLOAD_AAC = 10;
    public static final int PREPARE = 2;
    public static final int SENT_FILE_SIZE = 6;
    public static final int START = 0;
    private static String TAG = "download";
    public static final String file_name = "shuke.tmp";
    public long finishLength;
    public boolean isAddNumber;
    public boolean isRunning;
    public DownloadTask mDownloadTask;
    public int mNowState;
    public int mPercentage;
    public int mWhichDir;
    long startTime;
    public long totalLength;

    public XiMaoDownloadTask(DownloadTask downloadtask, int i)
    {
        mNowState = 0;
        totalLength = 0L;
        finishLength = 0L;
        isRunning = true;
        mPercentage = 0;
        isAddNumber = false;
        Logger.d(TAG, (new StringBuilder()).append("XiMaoDownloadTask:").append(i).toString());
        mDownloadTask = downloadtask;
        mWhichDir = i;
        mPercentage = 0;
    }

    private Context getContext()
    {
        return MyApplication.b();
    }

    private void handleDownloadAACCompleted()
    {
        CustomToast.showToast(MyApplication.b(), "\u4E0B\u8F7D\u5B8C\u6210", 0);
        mPercentage = 10;
        doTransmission();
    }

    private void handleDownloadFailed()
    {
        CustomToast.showToast(MyApplication.b(), "\u4E0B\u8F7D\u5931\u8D25", 0);
    }

    private void retryDownload()
    {
        CustomToast.showToast(MyApplication.b(), "\u91CD\u8BD5\u4E0B\u8F7D", 0);
        downloadAAC();
    }

    public void checkFileExist()
    {
        Logger.d(TAG, "checkFileExist IN");
        String s = getDownloadTask().title.replaceAll("\\\\", "").replaceAll("/", "").replaceAll(":", "").replaceAll("\\*", "").replaceAll("\\?", "").replaceAll("\"", "").replaceAll("<", "").replaceAll(">", "").replaceAll("|", "");
        String s1 = (new StringBuilder()).append(s).append(".aac").toString();
        byte abyte1[] = null;
        byte abyte0[] = abyte1;
        if (s1 != null)
        {
            try
            {
                abyte0 = s1.getBytes("unicode");
            }
            catch (UnsupportedEncodingException unsupportedencodingexception)
            {
                unsupportedencodingexception.printStackTrace();
                unsupportedencodingexception = abyte1;
            }
        }
        abyte1 = new byte[abyte0.length - 2];
        for (int i = 0; i < abyte0.length - 2; i++)
        {
            abyte1[i] = abyte0[i + 2];
        }

        XiMaoComm.printPacket("new flow", abyte1, abyte1.length);
        XiMaoComm.sendPacket(getContext(), getDirHead(), 48, abyte1);
        CustomToast.showToast(MyApplication.b(), "\u53D1\u9001CHECK_FILE\u547D\u4EE4", 0);
        setNowState(1);
    }

    public void doDownload()
    {
        int i = 0;
        if (isAddNumber) goto _L2; else goto _L1
_L1:
        getWhichDir();
        JVM INSTR tableswitch 1 4: default 44
    //                   1 54
    //                   2 278
    //                   3 289
    //                   4 300;
           goto _L3 _L4 _L5 _L6 _L7
_L3:
        throw new RuntimeException("Error Dir Name");
_L4:
        XiMaoComm.NUM_CONTENT1++;
_L11:
        isAddNumber = true;
_L2:
        Object obj;
        Logger.d(TAG, "doDownload IN");
        CustomToast.showToast(MyApplication.b(), "\u5F00\u59CB\u4F20\u8F93\u6570\u636E", 0);
        setNowState(3);
        obj = new File(getDownloadTask().downloadLocation, "shuke.tmp");
        System.out.println(obj);
        CustomToast.showToast(MyApplication.b(), (new StringBuilder()).append("\u6587\u4EF6\u957F\u5EA6\uFF1A").append(((File) (obj)).length()).append("").toString(), 0);
        totalLength = ((File) (obj)).length();
        byte abyte0[];
        obj = new FileInputStream(((File) (obj)));
        abyte0 = new byte[XiMaoComm.getDownloadPayload(MyApplication.b())];
_L10:
        int j = ((InputStream) (obj)).read(abyte0);
        if (j == -1)
        {
            break MISSING_BLOCK_LABEL_320;
        }
        if (!isRunning)
        {
            break MISSING_BLOCK_LABEL_320;
        }
        i += j;
        finishLength = i;
        XiMaoComm.sendPacket(getContext(), getDirHead(), 50, abyte0, j);
        Thread.sleep(XiMaoComm.getDownloadInterrupt(MyApplication.b()));
        if (mPercentage >= 100) goto _L9; else goto _L8
_L8:
        mPercentage = (int)(10L + (((finishLength * 100L) / totalLength) * 90L) / 100L);
          goto _L10
_L5:
        XiMaoComm.NUM_CONTENT2++;
          goto _L11
_L6:
        XiMaoComm.NUM_CONTENT3++;
          goto _L11
_L7:
        XiMaoComm.NUM_CONTENT4++;
          goto _L11
_L9:
        try
        {
            mPercentage = 100;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((Exception) (obj)).printStackTrace();
            return;
        }
          goto _L10
        Thread.sleep(2000L);
        System.out.println("finish");
        CustomToast.showToast(MyApplication.b(), "\u4F20\u8F93\u5B8C\u6210", 0);
        ((InputStream) (obj)).close();
        finishDownload();
        return;
          goto _L11
    }

    public void doDownload2()
    {
        Object obj;
        int i;
        i = 0;
        Logger.d(TAG, "doDownload IN");
        CustomToast.showToast(MyApplication.b(), "\u5F00\u59CB\u4F20\u8F93\u6570\u636E", 0);
        setNowState(3);
        obj = new File(getDownloadTask().downloadLocation, "shuke.tmp");
        System.out.println(obj);
        CustomToast.showToast(MyApplication.b(), (new StringBuilder()).append("\u6587\u4EF6\u957F\u5EA6\uFF1A").append(((File) (obj)).length()).append("").toString(), 0);
        totalLength = ((File) (obj)).length();
        Object obj1 = new FileInputStream(((File) (obj)));
        obj = obj1;
        byte abyte0[] = new byte[XiMaoComm.getDownloadPayload(MyApplication.b())];
_L6:
        obj = obj1;
        int j = ((InputStream) (obj1)).read(abyte0);
        if (j == -1) goto _L2; else goto _L1
_L1:
        obj = obj1;
        if (!isRunning) goto _L2; else goto _L3
_L3:
        i += j;
        obj = obj1;
        finishLength = i;
        obj = obj1;
        XiMaoComm.sendPacket(getContext(), abyte0, j);
        obj = obj1;
        Thread.sleep(XiMaoComm.getDownloadInterrupt(MyApplication.b()));
        obj = obj1;
        if (mPercentage >= 100) goto _L5; else goto _L4
_L4:
        obj = obj1;
        mPercentage = (int)(10L + (((finishLength * 100L) / totalLength) * 90L) / 100L);
          goto _L6
        Object obj2;
        obj2;
_L9:
        obj = obj1;
        ((Exception) (obj2)).printStackTrace();
        if (obj1 != null)
        {
            try
            {
                ((InputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        finishDownload();
        return;
_L5:
        obj = obj1;
        mPercentage = 100;
          goto _L6
        obj2;
        obj1 = obj;
        obj = obj2;
_L8:
        if (obj1 != null)
        {
            try
            {
                ((InputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((IOException) (obj1)).printStackTrace();
            }
        }
        finishDownload();
        throw obj;
_L2:
        obj = obj1;
        Thread.sleep(2000L);
        obj = obj1;
        System.out.println("finish");
        obj = obj1;
        CustomToast.showToast(MyApplication.b(), "\u4F20\u8F93\u5B8C\u6210", 0);
        if (obj1 != null)
        {
            try
            {
                ((InputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        break MISSING_BLOCK_LABEL_236;
        obj;
        obj1 = null;
        if (true) goto _L8; else goto _L7
_L7:
        obj2;
        obj1 = null;
          goto _L9
    }

    protected void doTransmission()
    {
        String s;
        long l;
        try
        {
            Thread.sleep(1000L);
        }
        catch (InterruptedException interruptedexception)
        {
            interruptedexception.printStackTrace();
        }
        checkFileExist();
        this;
        JVM INSTR monitorenter ;
        wait();
        this;
        JVM INSTR monitorexit ;
_L4:
        preDownload();
        this;
        JVM INSTR monitorenter ;
        wait();
        this;
        JVM INSTR monitorexit ;
_L2:
        doDownload();
        l = (System.currentTimeMillis() - startTime) / 1000L;
        s = (new StringBuilder()).append(l / 60L).append("\u5206").append(l % 60L).append("\u79D2").toString();
        CustomToast.showToast(MyApplication.b(), (new StringBuilder()).append("\u4F20\u8F93\u65F6\u95F4\uFF1A").append(s).toString(), 0);
        this;
        JVM INSTR monitorenter ;
        wait();
        this;
        JVM INSTR monitorexit ;
        return;
        Object obj;
        obj;
        this;
        JVM INSTR monitorexit ;
        try
        {
            throw obj;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((InterruptedException) (obj)).printStackTrace();
        }
        continue; /* Loop/switch isn't completed */
        obj;
        this;
        JVM INSTR monitorexit ;
        try
        {
            throw obj;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((InterruptedException) (obj)).printStackTrace();
        }
        if (true) goto _L2; else goto _L1
_L1:
        obj;
        this;
        JVM INSTR monitorexit ;
        try
        {
            throw obj;
        }
        catch (InterruptedException interruptedexception1)
        {
            interruptedexception1.printStackTrace();
        }
        return;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void downloadAAC()
    {
        Object obj;
        Object obj1;
        Object obj3;
        Context context;
        setNowState(7);
        for (context = MyApplication.b(); context == null || -1 == NetworkUtils.getNetType(context);)
        {
            return;
        }

        ToolUtil.createAppDirectory();
        StorageUtils.checkAndMakeDownloadFolder();
        getDownloadTask().downloadLocation = StorageUtils.getCurrentDownloadLocation();
        if (!StorageUtils.isDirAvaliable(getDownloadTask().downloadLocation))
        {
            MyApplication.a("\u6E29\u99A8\u63D0\u793A", "\u672A\u68C0\u6D4B\u5230SD\u5361\uFF0C\u65E0\u6CD5\u5B8C\u6210\u4E0B\u8F7D");
            return;
        }
        obj1 = null;
        obj3 = null;
        obj = null;
        if (!TextUtils.isEmpty(getDownloadTask().downLoadUrl)) goto _L2; else goto _L1
_L1:
        boolean flag = TextUtils.isEmpty(getDownloadTask().downLoadUrl);
        if (!flag) goto _L2; else goto _L3
_L3:
        if (false)
        {
            throw new NullPointerException();
        }
        obj1 = DataCollectUtil.getInstance(context);
_L4:
        ((DataCollectUtil) (obj1)).statDownloadCDN(((CdnCollectData) (obj)));
        return;
_L2:
        Object obj4;
        long l;
        obj4 = new File(getDownloadTask().downloadLocation, "shuke.tmp");
        if (((File) (obj4)).exists())
        {
            ((File) (obj4)).delete();
        }
        l = MemorySpaceUtil.getAvailableMemorySize(new File(getDownloadTask().downloadLocation));
        if (l <= 0L)
        {
            break MISSING_BLOCK_LABEL_197;
        }
        if (getDownloadTask().filesize < l)
        {
            break MISSING_BLOCK_LABEL_225;
        }
        getDownloadTask().createNotEnoughSpaceDialog();
        if (false)
        {
            throw new NullPointerException();
        }
        obj1 = DataCollectUtil.getInstance(context);
          goto _L4
        HttpGet httpget;
        getDownloadTask().downloadStatus = 1;
        httpget = new HttpGet(getDownloadTask().downLoadUrl);
        httpget.addHeader("RANGE", String.format("bytes=%d-", new Object[] {
            Integer.valueOf(0)
        }));
        httpget.addHeader("Connection", "close");
        obj = new DefaultHttpClient();
        Object obj6;
        int k;
        obj1 = ((HttpClient) (obj)).getParams();
        HttpConnectionParams.setConnectionTimeout(((org.apache.http.params.HttpParams) (obj1)), 5000);
        HttpConnectionParams.setSoTimeout(((org.apache.http.params.HttpParams) (obj1)), 5000);
        FreeFlowUtil.setProxyForHttpClient(((HttpClient) (obj)));
        obj6 = ((HttpClient) (obj)).execute(httpget);
        k = ((HttpResponse) (obj6)).getStatusLine().getStatusCode();
        obj1 = new CdnCollectData();
        Object obj2;
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setAudioUrl(getDownloadTask().downLoadUrl);
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setCdnIP(ToolUtil.getUrlIp(getDownloadTask().downLoadUrl));
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setType("download");
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setStatusCode((new StringBuilder()).append("").append(k).toString());
        obj2 = obj1;
        obj3 = obj;
        Object obj8 = new HashMap();
        obj2 = obj1;
        obj3 = obj;
        Object aobj[] = ((HttpResponse) (obj6)).getAllHeaders();
        obj2 = obj1;
        obj3 = obj;
        int j = aobj.length;
        int i = 0;
_L6:
        Object obj7;
        if (i >= j)
        {
            break; /* Loop/switch isn't completed */
        }
        obj7 = aobj[i];
        obj2 = obj1;
        obj3 = obj;
        ((Map) (obj8)).put(((Header) (obj7)).getName(), ((Header) (obj7)).getValue());
        i++;
        if (true) goto _L6; else goto _L5
_L5:
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setResponseHeader(JSON.toJSONString(obj8));
        if (k != 200) goto _L8; else goto _L7
_L7:
        obj2 = obj1;
        obj3 = obj;
        String s = ((HttpResponse) (obj6)).getFirstHeader("Content-Length").getValue();
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_2082;
        }
        obj2 = obj1;
        obj3 = obj;
        if ("".equalsIgnoreCase(s))
        {
            break MISSING_BLOCK_LABEL_2082;
        }
        obj2 = obj1;
        obj3 = obj;
        l = Long.valueOf(s).longValue();
_L11:
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setAudioBytes(l);
        if (l > 0L) goto _L10; else goto _L9
_L9:
        obj2 = obj1;
        obj3 = obj;
        handleDownloadFailed();
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        obj = DataCollectUtil.getInstance(context);
_L12:
        ((DataCollectUtil) (obj)).statDownloadCDN(((CdnCollectData) (obj1)));
        return;
_L8:
        if (k != 206)
        {
            break MISSING_BLOCK_LABEL_729;
        }
        obj2 = obj1;
        obj3 = obj;
        s = ((HttpResponse) (obj6)).getFirstHeader("Content-Range").getValue();
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_2082;
        }
        obj2 = obj1;
        obj3 = obj;
        if ("".equalsIgnoreCase(s))
        {
            break MISSING_BLOCK_LABEL_2082;
        }
        obj2 = obj1;
        obj3 = obj;
        l = Long.valueOf(s.substring(s.lastIndexOf("/") + 1)).longValue();
          goto _L11
        obj2 = obj1;
        obj3 = obj;
        obj4 = new HttpGet(getDownloadTask().downLoadUrl);
        obj2 = obj1;
        obj3 = obj;
        ((HttpGet) (obj4)).addHeader("Connection", "close");
        obj2 = obj1;
        obj3 = obj;
        obj4 = ((HttpClient) (obj)).execute(((org.apache.http.client.methods.HttpUriRequest) (obj4))).getFirstHeader("Content-Length").getValue();
        if (obj4 == null)
        {
            break MISSING_BLOCK_LABEL_828;
        }
        obj2 = obj1;
        obj3 = obj;
        if ("".equalsIgnoreCase(((String) (obj4))))
        {
            break MISSING_BLOCK_LABEL_828;
        }
        obj2 = obj1;
        obj3 = obj;
        Long.valueOf(((String) (obj4))).longValue();
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        obj = DataCollectUtil.getInstance(context);
          goto _L12
_L10:
        obj2 = obj1;
        obj3 = obj;
        getDownloadTask().filesize = l;
        obj2 = obj1;
        obj3 = obj;
        getDownloadTask().downloaded = 0L;
        obj2 = obj1;
        obj3 = obj;
        obj6 = ((HttpResponse) (obj6)).getEntity();
        if (obj6 == null) goto _L14; else goto _L13
_L13:
        obj2 = obj1;
        obj3 = obj;
        obj7 = new BufferedInputStream(((HttpEntity) (obj6)).getContent(), 2048);
        obj2 = obj1;
        obj3 = obj;
        if (ToolUtil.createDirectory(getDownloadTask().downloadLocation))
        {
            break MISSING_BLOCK_LABEL_973;
        }
        obj2 = obj1;
        obj3 = obj;
        handleDownloadFailed();
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        obj = DataCollectUtil.getInstance(context);
          goto _L12
        obj2 = obj1;
        obj3 = obj;
        obj6 = new File(getDownloadTask().downloadLocation, "shuke.tmp");
        obj2 = obj1;
        obj3 = obj;
        obj8 = new FileOutputStream(((File) (obj6)));
        l = 0L;
        obj2 = obj1;
        obj3 = obj;
        s = new byte[2048];
_L16:
        obj2 = obj1;
        obj3 = obj;
        i = ((BufferedInputStream) (obj7)).read(s, 0, 2048);
        if (i <= 0)
        {
            break; /* Loop/switch isn't completed */
        }
        obj2 = obj1;
        obj3 = obj;
        ((OutputStream) (obj8)).write(s, 0, i);
        l += i;
        obj2 = obj1;
        obj3 = obj;
        DownloadTask downloadtask = getDownloadTask();
        obj2 = obj1;
        obj3 = obj;
        downloadtask.downloaded = downloadtask.downloaded + (long)i;
        obj2 = obj1;
        obj3 = obj;
        if (100L * l >= getDownloadTask().filesize)
        {
            break MISSING_BLOCK_LABEL_1142;
        }
        obj2 = obj1;
        obj3 = obj;
        if (getDownloadTask().getDownloadPercent() < 98)
        {
            continue; /* Loop/switch isn't completed */
        }
        obj2 = obj1;
        obj3 = obj;
        mPercentage = (getDownloadTask().getDownloadPercent() * 10) / 100;
        l = 0L;
        if (true) goto _L16; else goto _L15
_L15:
        obj2 = obj1;
        obj3 = obj;
        if (getDownloadTask().downloaded != getDownloadTask().filesize) goto _L18; else goto _L17
_L17:
        obj2 = obj1;
        obj3 = obj;
        if (getDownloadTask().filesize <= 0L) goto _L18; else goto _L19
_L19:
        obj2 = obj1;
        obj3 = obj;
        handleDownloadAACCompleted();
        if (obj6 == null)
        {
            break MISSING_BLOCK_LABEL_1234;
        }
        obj2 = obj1;
        obj3 = obj;
        ((OutputStream) (obj8)).close();
        obj2 = obj1;
        obj3 = obj;
        ((BufferedInputStream) (obj7)).close();
        obj2 = obj1;
        obj3 = obj;
        httpget.abort();
_L20:
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        obj = DataCollectUtil.getInstance(context);
          goto _L12
_L18:
        if (obj6 == null)
        {
            break MISSING_BLOCK_LABEL_1293;
        }
        obj2 = obj1;
        obj3 = obj;
        ((OutputStream) (obj8)).close();
        if (obj4 == null)
        {
            break MISSING_BLOCK_LABEL_1322;
        }
        obj2 = obj1;
        obj3 = obj;
        if (!((File) (obj4)).exists())
        {
            break MISSING_BLOCK_LABEL_1322;
        }
        obj2 = obj1;
        obj3 = obj;
        ((File) (obj4)).delete();
        obj2 = obj1;
        obj3 = obj;
        Logger.d(TAG, "retryDownload IN #1");
        obj2 = obj1;
        obj3 = obj;
        retryDownload();
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        obj = DataCollectUtil.getInstance(context);
          goto _L12
_L14:
        obj2 = obj1;
        obj3 = obj;
        handleDownloadFailed();
          goto _L20
        obj3;
        obj2 = obj1;
        obj1 = obj;
        obj = obj2;
        obj2 = obj3;
_L33:
        Logger.logToSd((new StringBuilder()).append("DownloadTask ClientProtocolException:").append(((ClientProtocolException) (obj2)).getMessage()).toString());
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_1430;
        }
        ((CdnCollectData) (obj)).setExceptionReason(((ClientProtocolException) (obj2)).getMessage());
        handleDownloadFailed();
        if (obj1 != null)
        {
            ((HttpClient) (obj1)).getConnectionManager().shutdown();
        }
        obj1 = DataCollectUtil.getInstance(context);
          goto _L4
        Object obj5;
        obj5;
        obj = null;
        obj1 = null;
_L32:
        obj2 = obj1;
        obj3 = obj;
        Logger.logToSd((new StringBuilder()).append("DownloadTask ConnectTimeoutException:").append(((ConnectTimeoutException) (obj5)).getMessage()).toString());
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_1524;
        }
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setExceptionReason(((ConnectTimeoutException) (obj5)).getMessage());
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setTimeout(true);
        obj2 = obj1;
        obj3 = obj;
        Logger.d(TAG, "retryDownload IN #2");
        obj2 = obj1;
        obj3 = obj;
        retryDownload();
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        obj = DataCollectUtil.getInstance(context);
          goto _L12
        obj5;
        obj = null;
        obj1 = null;
_L31:
        obj2 = obj1;
        obj3 = obj;
        Logger.logToSd((new StringBuilder()).append("DownloadTask SocketTimeoutException:").append(((SocketTimeoutException) (obj5)).getMessage()).toString());
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_1637;
        }
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setExceptionReason(((SocketTimeoutException) (obj5)).getMessage());
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setTimeout(true);
        obj2 = obj1;
        obj3 = obj;
        Logger.d(TAG, "retryDownload IN #3");
        obj2 = obj1;
        obj3 = obj;
        retryDownload();
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        obj = DataCollectUtil.getInstance(context);
          goto _L12
        obj5;
        obj = null;
        obj1 = null;
_L30:
        obj2 = obj1;
        obj3 = obj;
        Logger.logToSd((new StringBuilder()).append("DownloadTask IOException:").append(((IOException) (obj5)).getMessage()).toString());
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_1740;
        }
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setExceptionReason(((IOException) (obj5)).getMessage());
        obj2 = obj1;
        obj3 = obj;
        obj5 = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (obj5 == null) goto _L22; else goto _L21
_L21:
        obj2 = obj1;
        obj3 = obj;
        flag = ((NetworkInfo) (obj5)).isConnected();
        if (flag) goto _L23; else goto _L22
_L22:
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        obj = DataCollectUtil.getInstance(context);
          goto _L12
_L23:
        obj2 = obj1;
        obj3 = obj;
        handleDownloadFailed();
          goto _L22
        obj1;
        obj = obj3;
_L28:
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        DataCollectUtil.getInstance(context).statDownloadCDN(((CdnCollectData) (obj2)));
        throw obj1;
        obj5;
        obj = null;
        obj1 = null;
_L29:
        obj2 = obj1;
        obj3 = obj;
        Logger.logToSd((new StringBuilder()).append("DownloadTask Throwable:").append(((Throwable) (obj5)).getMessage()).toString());
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_1905;
        }
        obj2 = obj1;
        obj3 = obj;
        ((CdnCollectData) (obj1)).setExceptionReason(((Throwable) (obj5)).getMessage());
        obj2 = obj1;
        obj3 = obj;
        ((Throwable) (obj5)).printStackTrace();
        obj2 = obj1;
        obj3 = obj;
        if (NetworkUtils.getNetType(context) != -1) goto _L25; else goto _L24
_L24:
        obj2 = obj1;
        obj3 = obj;
        handleDownloadFailed();
_L27:
        if (obj != null)
        {
            ((HttpClient) (obj)).getConnectionManager().shutdown();
        }
        obj = DataCollectUtil.getInstance(context);
          goto _L12
_L25:
        obj2 = obj1;
        obj3 = obj;
        if (getDownloadTask().downloadStatus == 4) goto _L27; else goto _L26
_L26:
        obj2 = obj1;
        obj3 = obj;
        handleDownloadFailed();
          goto _L27
        obj1;
        obj = null;
        obj2 = null;
          goto _L28
        obj1;
        obj2 = null;
          goto _L28
        obj2;
        obj3 = obj1;
        obj1 = obj2;
        obj2 = obj;
        obj = obj3;
          goto _L28
        obj5;
        obj1 = null;
          goto _L29
        obj5;
          goto _L29
        obj5;
        obj1 = null;
          goto _L30
        obj5;
          goto _L30
        obj5;
        obj1 = null;
          goto _L31
        obj5;
          goto _L31
        obj5;
        obj1 = null;
          goto _L32
        obj5;
          goto _L32
        obj2;
        obj = obj3;
          goto _L33
        obj2;
        obj1 = obj;
        obj = obj3;
          goto _L33
        l = 0L;
          goto _L11
    }

    public void finishDownload()
    {
        Logger.d(TAG, "finishDownload IN");
        XiMaoComm.sendPacket(getContext(), getDirHead(), 51, new byte[] {
            0
        });
        CustomToast.showToast(MyApplication.b(), "\u53D1\u9001\u5B8C\u6210\u786E\u8BA4\u547D\u4EE4", 0);
        setNowState(4);
    }

    public int getDirHead()
    {
        switch (getWhichDir())
        {
        default:
            throw new RuntimeException("Error Dir Name");

        case 1: // '\001'
            return 2;

        case 2: // '\002'
            return 8;

        case 3: // '\003'
            return 1;

        case 4: // '\004'
            return 3;
        }
    }

    public DownloadTask getDownloadTask()
    {
        return mDownloadTask;
    }

    public long getFinishLength()
    {
        return finishLength;
    }

    public int getNowState()
    {
        return mNowState;
    }

    public int getPercentage()
    {
        return mPercentage;
    }

    public long getTotalLength()
    {
        return totalLength;
    }

    public int getWhichDir()
    {
        return mWhichDir;
    }

    public void preDownload()
    {
        Logger.d(TAG, "preDownload IN");
        XiMaoComm.sendPacket(getContext(), getDirHead(), 49, new byte[] {
            0
        });
        setNowState(2);
        CustomToast.showToast(MyApplication.b(), "\u53D1\u9001PREPARE\u547D\u4EE4", 0);
    }

    public void run()
    {
        Logger.d(TAG, (new StringBuilder()).append(this).append("running").toString());
        mPercentage = 0;
        startTime = System.currentTimeMillis();
        downloadAAC();
        long l = (System.currentTimeMillis() - startTime) / 1000L;
        String s = (new StringBuilder()).append(l / 60L).append("\u5206").append(l % 60L).append("\u79D2").toString();
        CustomToast.showToast(MyApplication.b(), (new StringBuilder()).append("\u4E0B\u8F7D\u65F6\u95F4\uFF1A").append(s).toString(), 0);
    }

    public void sendFileSize()
    {
        File file = new File(getDownloadTask().downloadLocation, "shuke.tmp");
        System.out.println(file);
        CustomToast.showToast(MyApplication.b(), (new StringBuilder()).append("\u6587\u4EF6\u957F\u5EA6\uFF1A").append(file.length()).append("").toString(), 0);
        totalLength = file.length();
        byte abyte0[] = new byte[4];
        for (int i = 0; i < 4; i++)
        {
            abyte0[i] = 0;
        }

        abyte0[0] = (byte)(int)(totalLength >> 24 & 255L);
        abyte0[1] = (byte)(int)(totalLength >> 16 & 255L);
        abyte0[2] = (byte)(int)(totalLength >> 8 & 255L);
        abyte0[3] = (byte)(int)(totalLength & 255L);
        XiMaoComm.sendPacket(getContext(), getDirHead(), 52, abyte0);
        CustomToast.showToast(MyApplication.b(), "\u53D1\u9001FILE_SIZE\u547D\u4EE4", 0);
        setNowState(6);
    }

    public void setDownloadTask(DownloadTask downloadtask)
    {
        mDownloadTask = downloadtask;
    }

    public void setNowState(int i)
    {
        mNowState = i;
    }

    public void setWhichDir(int i)
    {
        mWhichDir = i;
    }

}
