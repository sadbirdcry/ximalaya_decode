// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.fragment.device.ByteUtil;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.BaseBtController;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.BtPackage;
import com.ximalaya.ting.android.fragment.device.bluetooth.ICommand;
import com.ximalaya.ting.android.fragment.device.bluetooth.MyBluetoothDevice;
import com.ximalaya.ting.android.fragment.device.bluetooth.model.RecordModel;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.miniche:
//            BtDeviceType, QCheFmModule, QCheShutDownPlayModule, BaseQCheCommand

public class QCheController extends BaseBtController
{

    public static final int HEAD = 254;
    public static final int MAX_PACKET_LENGTH = 1024;
    public static final String TAG = com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheController.getSimpleName();
    public static final int TAIL = 255;

    public QCheController(Context context)
    {
        super(context);
    }

    private byte getCrc(byte abyte0[])
    {
        byte byte0 = abyte0[0];
        for (int i = 1; i < abyte0.length; i++)
        {
            byte0 ^= abyte0[i];
        }

        return byte0;
    }

    public static byte[] getValues(byte abyte0[])
    {
        int i = abyte0[2] - 5;
        byte abyte1[] = new byte[i];
        System.arraycopy(abyte0, 3, abyte1, 0, i);
        return abyte1;
    }

    public static void goIntroduction(Activity activity)
    {
        String s = MyDeviceManager.getInstance(activity.getApplicationContext()).getQSuichetingIntro();
        if (!TextUtils.isEmpty(s))
        {
            Intent intent = new Intent(activity, com/ximalaya/ting/android/activity/web/WebActivityNew);
            intent.putExtra("ExtraUrl", s);
            activity.startActivity(intent);
            return;
        } else
        {
            CustomToast.showToast(activity, "\u656C\u8BF7\u671F\u5F85", 0);
            return;
        }
    }

    public static boolean isAutoPlaying(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean("autoPlaying", true);
    }

    public static void setAutoPlaying(Context context, boolean flag)
    {
        SharedPreferencesUtil.getInstance(context).saveBoolean("autoPlaying", flag);
    }

    public BtDeviceType getNowBtDeviceType()
    {
        return BtDeviceType.qsuicheting;
    }

    public String getRealName()
    {
        return "\u968F\u8F66\u542C";
    }

    public RecordModel getRecordModel()
    {
        if (BluetoothManager.getInstance(mContext).getMyBluetoothDevice() != null)
        {
            return new RecordModel("1", "6", BluetoothManager.getInstance(mContext).getMyBluetoothDevice().getBluetoothDevice().getAddress());
        } else
        {
            return new RecordModel("1", "6", "QSuiCheTing");
        }
    }

    protected void initModule()
    {
        setModule(new QCheFmModule(mContext));
        setModule(new QCheShutDownPlayModule());
    }

    public boolean isWarning()
    {
        boolean flag = SharedPreferencesUtil.getInstance(mContext).getBoolean("isWarning", true);
        Logger.d(TAG, (new StringBuilder()).append("\u5F53\u524D\u8BB0\u5F55\u7684\u63D0\u793A\u97F3\u72B6\u6001").append(flag).toString());
        return flag;
    }

    public BtPackage parseCommand(ICommand icommand)
    {
        byte abyte0[];
        if (icommand == null)
        {
            return null;
        }
        if (icommand.getDeviceType() != BtDeviceType.qsuicheting)
        {
            return null;
        }
        icommand = (BaseQCheCommand)icommand;
        abyte0 = new byte[1024];
        abyte0[0] = -2;
        icommand.getType();
        JVM INSTR tableswitch 1 5: default 76
    //                   1 144
    //                   2 158
    //                   3 165
    //                   4 151
    //                   5 172;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        break; /* Loop/switch isn't completed */
_L6:
        break MISSING_BLOCK_LABEL_172;
_L7:
        int i;
        if (icommand.getType() == 2)
        {
            icommand = ByteUtil.getBytes(((Integer)icommand.getT()).intValue());
        } else
        {
            icommand = (String)icommand.getT();
            if (!TextUtils.isEmpty(icommand))
            {
                icommand = ByteUtil.getBytes(icommand);
            } else
            {
                icommand = new byte[1024];
            }
        }
        i = icommand.length;
        abyte0[2] = (byte)(i + 5);
        System.arraycopy(icommand, 0, abyte0, 3, i);
        abyte0[i + 3] = getCrc(abyte0);
        abyte0[i + 4] = -1;
        return new BtPackage(abyte0, i + 5);
_L2:
        abyte0[1] = 1;
          goto _L7
_L5:
        abyte0[1] = 4;
          goto _L7
_L3:
        abyte0[1] = 2;
          goto _L7
_L4:
        abyte0[1] = 3;
          goto _L7
        abyte0[1] = 5;
          goto _L7
    }

    public ICommand parseCommand(BtPackage btpackage)
    {
        Object obj;
        obj = btpackage.getPack();
        if (obj == null || obj.length < 5 || obj[0] != -17)
        {
            Logger.d(TAG, "\u5305\u5224\u65AD\u9519\u8BEF");
        }
        btpackage = new BaseQCheCommand();
        obj[1];
        JVM INSTR tableswitch 1 5: default 80
    //                   1 82
    //                   2 137
    //                   3 192
    //                   4 249
    //                   5 393;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        return btpackage;
_L2:
        btpackage.setType(1);
        if (obj[3] != 78 && obj[4] != 65)
        {
            Logger.d(TAG, "FM \u8C03\u9891\u6210\u529F");
            btpackage.setResult(6);
            return btpackage;
        } else
        {
            btpackage.setResult(7);
            Logger.d(TAG, "FM \u8C03\u9891\u5931\u8D25");
            return btpackage;
        }
_L3:
        btpackage.setType(2);
        if (obj[3] != 78 && obj[4] != 65)
        {
            btpackage.setResult(6);
            Logger.d(TAG, "\u6307\u5B9A\u64AD\u653E\u63D0\u793A\u97F3\u6210\u529F");
            return btpackage;
        } else
        {
            btpackage.setResult(7);
            Logger.d(TAG, "\u6307\u5B9A\u64AD\u653E\u63D0\u793A\u97F3\u5931\u8D25");
            return btpackage;
        }
_L4:
        btpackage.setType(3);
        if (obj[3] != 78 && obj[4] != 65)
        {
            btpackage.setResult(6);
            Logger.d(TAG, "\u5F00\u5173\u64CD\u4F5C\u6210\u529F");
            return btpackage;
        } else
        {
            btpackage.setResult(7);
            Logger.d(TAG, "\u5F00\u5173\u64CD\u4F5C\u5931\u8D25");
            return btpackage;
        }
_L5:
        btpackage.setType(4);
        if (obj[3] == 78 || obj[4] == 65) goto _L8; else goto _L7
_L7:
        float f;
        btpackage.setResult(6);
        obj = ByteUtil.getString(getValues(((byte []) (obj))));
        f = 0.0F;
        float f1 = Float.valueOf(((String) (obj))).floatValue();
        f = f1;
_L9:
        obj = Integer.valueOf((int)(f * 10F));
        Logger.d(TAG, (new StringBuilder()).append("\u5F53\u524D\u9891\u7387").append(obj).toString());
        btpackage.setT(obj);
        Logger.d(TAG, "FM \u9891\u7387\u67E5\u8BE2\u6210\u529F");
        return btpackage;
        obj;
        btpackage.setResult(7);
        Logger.d(TAG, "FM1 \u9891\u7387\u67E5\u8BE2\u5931\u8D25");
        if (true) goto _L9; else goto _L8
_L8:
        btpackage.setResult(7);
        Logger.d(TAG, "FM2 \u9891\u7387\u67E5\u8BE2\u5931\u8D25");
        return btpackage;
_L6:
        btpackage.setType(5);
        if (obj[3] != 78 && obj[4] != 65)
        {
            btpackage.setResult(6);
            obj = ByteUtil.getString(getValues(((byte []) (obj))));
            Logger.d(TAG, (new StringBuilder()).append("\u5F00\u5173\u72B6\u6001\u67E5\u8BE2\u8FD4\u56DE\u503C").append(((String) (obj))).toString());
            if (((String) (obj)).equalsIgnoreCase("Voice_on"))
            {
                btpackage.setT(Integer.valueOf(8));
                setWarning(true);
                Logger.d(TAG, "\u63D0\u793A\u97F3\u6253\u5F00");
                return btpackage;
            }
            if (((String) (obj)).equalsIgnoreCase("Voice_off"))
            {
                btpackage.setT(Integer.valueOf(9));
                setWarning(false);
                Logger.d(TAG, "\u63D0\u793A\u97F3\u5173\u95ED");
                return btpackage;
            }
            if (((String) (obj)).equalsIgnoreCase("Autoplay_on"))
            {
                btpackage.setT(Integer.valueOf(10));
                setAutoPlaying(mContext, true);
                Logger.d(TAG, "\u81EA\u52A8\u64AD\u653E\u6253\u5F00");
                return btpackage;
            }
            if (((String) (obj)).equalsIgnoreCase("Autoplay_off"))
            {
                btpackage.setT(Integer.valueOf(11));
                setAutoPlaying(mContext, false);
                Logger.d(TAG, "\u81EA\u52A8\u64AD\u653E\u5173\u95ED");
                return btpackage;
            }
        } else
        {
            btpackage.setResult(7);
            Logger.d(TAG, "\u5F00\u5173\u72B6\u6001\u67E5\u8BE2\u5931\u8D25");
            return btpackage;
        }
        if (true) goto _L1; else goto _L10
_L10:
    }

    public void setWarning(boolean flag)
    {
        SharedPreferencesUtil.getInstance(mContext).saveBoolean("isWarning", flag);
    }

}
