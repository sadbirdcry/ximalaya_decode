// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.bluetooth.model.RecordModel;
import java.util.HashMap;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            IBtDeviceController, IBtModule, ICommand, BtPackage

public abstract class BaseBtController
    implements IBtDeviceController
{

    protected Context mContext;
    protected Map mModules;

    public BaseBtController(Context context)
    {
        mModules = new HashMap();
        mContext = context;
        initModule();
    }

    public IBtModule getModule(String s)
    {
        return (IBtModule)mModules.get(s);
    }

    public abstract String getRealName();

    public abstract RecordModel getRecordModel();

    protected abstract void initModule();

    public abstract BtPackage parseCommand(ICommand icommand);

    public abstract ICommand parseCommand(BtPackage btpackage);

    public void setModule(IBtModule ibtmodule)
    {
        if (mModules == null)
        {
            mModules = new HashMap();
        }
        if (!mModules.containsKey(ibtmodule.getModuleName()))
        {
            mModules.put(ibtmodule.getModuleName(), ibtmodule);
        }
    }
}
