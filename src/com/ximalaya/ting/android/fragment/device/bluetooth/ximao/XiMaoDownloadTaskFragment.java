// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.ximao;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.ximao:
//            XiMaoDownloadListAdapter, XiMaoDownloadModule

public class XiMaoDownloadTaskFragment extends BaseListFragment
    implements android.view.View.OnClickListener, XiMaoDownloadHandler.OnBluetoothDownloadStatusListener
{

    public static List taskItems;
    private String TAG;
    protected MyProgressDialog loadingDialog;
    protected BaseAdapter mAdapter;
    protected RelativeLayout mAllDelete;
    protected RelativeLayout mAllStart;
    protected ImageView mBackImg;
    protected RelativeLayout mController;
    protected RelativeLayout mDeleteAll;
    protected BaseDeviceItem mDeviceItem;
    protected XiMaoDownloadModule mDownloadModule;
    protected RelativeLayout mHaveData;
    protected RelativeLayout mNoData;
    private TextView mNoDataText;
    protected TextView mRightBtn;
    protected RelativeLayout mTaskControl;
    protected RelativeLayout mTopBar;
    protected TextView mTopTv;
    private Timer timer;

    public XiMaoDownloadTaskFragment()
    {
        TAG = com/ximalaya/ting/android/fragment/device/bluetooth/ximao/XiMaoDownloadTaskFragment.getSimpleName();
        timer = null;
    }

    protected void initData()
    {
        taskItems = new ArrayList();
        mAdapter = new XiMaoDownloadListAdapter(mCon);
        mListView.setAdapter(mAdapter);
    }

    protected void initListener()
    {
        mListView.setOnItemClickListener(new _cls2());
        mBackImg.setOnClickListener(this);
        mDeleteAll.setOnClickListener(this);
        mRightBtn.setOnClickListener(this);
        mAllDelete.setOnClickListener(this);
        mAllStart.setOnClickListener(this);
    }

    protected void initUi()
    {
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        mTopBar = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a005f);
        mTopTv = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mTopTv.setText("\u6DFB\u52A0\u5217\u8868");
        mTopTv.setOnClickListener(this);
        mDeleteAll = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c9);
        mTaskControl = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c3);
        mAllDelete = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c7);
        mAllStart = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c5);
        mNoData = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03be);
        mNoDataText = (TextView)fragmentBaseContainerView.findViewById(0x7f0a03c0);
        mHaveData = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c1);
        mController = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a03c2);
        mRightBtn = (TextView)fragmentBaseContainerView.findViewById(0x7f0a071c);
        mController.setVisibility(0);
        mDeleteAll.setVisibility(0);
        mRightBtn.setVisibility(0);
        mRightBtn.setText((new StringBuilder()).append(XiMaoComm.getDownloadInterrupt(mCon)).append("ms").toString());
        mRightBtn.setVisibility(4);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mDownloadModule = XiMaoBTManager.getInstance(mCon).getDownloadModule();
        initUi();
        initData();
        initListener();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;

        case 2131362761: 
            (new DialogBuilder(MyApplication.a())).setMessage("\u786E\u5B9A\u6E05\u7A7A\u4E0B\u8F7D\u4EFB\u52A1\uFF1F").setOkBtn(new _cls3()).showConfirm();
            return;

        case 2131363612: 
            if (XiMaoComm.getDownloadInterrupt(mCon) == 50)
            {
                XiMaoComm.setDownloadInterrupt(100, mCon);
            } else
            if (XiMaoComm.getDownloadInterrupt(mCon) == 100)
            {
                XiMaoComm.setDownloadInterrupt(150, mCon);
            } else
            if (XiMaoComm.getDownloadInterrupt(mCon) == 150)
            {
                XiMaoComm.setDownloadInterrupt(200, mCon);
            } else
            if (XiMaoComm.getDownloadInterrupt(mCon) == 200)
            {
                XiMaoComm.setDownloadInterrupt(50, mCon);
            }
            mRightBtn.setText((new StringBuilder()).append(XiMaoComm.getDownloadInterrupt(mCon)).append("ms").toString());
            return;

        case 2131361966: 
            break;
        }
        if (XiMaoComm.getDownloadPayload(mCon) != 600) goto _L2; else goto _L1
_L1:
        XiMaoComm.setDownloadPayload(750, mCon);
_L4:
        mTopTv.setText((new StringBuilder()).append(XiMaoComm.getDownloadPayload(mCon)).append("\u5B57\u8282").toString());
        return;
_L2:
        if (XiMaoComm.getDownloadPayload(mCon) == 750)
        {
            XiMaoComm.setDownloadPayload(900, mCon);
        } else
        if (XiMaoComm.getDownloadPayload(mCon) == 900)
        {
            XiMaoComm.setDownloadPayload(1000, mCon);
        } else
        if (XiMaoComm.getDownloadPayload(mCon) == 1000)
        {
            XiMaoComm.setDownloadPayload(600, mCon);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300e6, viewgroup, false);
        mListView = (ListView)fragmentBaseContainerView.findViewById(0x7f0a03cb);
        return fragmentBaseContainerView;
    }

    public void onResume()
    {
        super.onResume();
        if (XiMaoBTManager.getInstance(mCon).getDownloadModule() != null)
        {
            XiMaoBTManager.getInstance(mCon).getDownloadModule().setOnBluetoothDownloadStatusListener(this);
        }
        hidePlayButton();
        _cls1 _lcls1 = new _cls1();
        (new Timer()).schedule(_lcls1, 0L, 500L);
    }

    public void onStatusChanged()
    {
        getActivity().runOnUiThread(new _cls4());
    }

    public void onStop()
    {
        super.onStop();
        if (XiMaoBTManager.getInstance(mCon).getDownloadModule() != null)
        {
            XiMaoBTManager.getInstance(mCon).getDownloadModule().removeOnBluetoothDownloadStatusListener(this);
        }
        if (timer != null)
        {
            timer.cancel();
            timer = null;
        }
    }

    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final XiMaoDownloadTaskFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
        }

        _cls2()
        {
            this$0 = XiMaoDownloadTaskFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final XiMaoDownloadTaskFragment this$0;

        public void onExecute()
        {
            XiMaoBTManager.getInstance(mCon).getDownloadModule().clearDownloadTask();
        }

        _cls3()
        {
            this$0 = XiMaoDownloadTaskFragment.this;
            super();
        }
    }


    private class _cls1 extends TimerTask
    {

        final XiMaoDownloadTaskFragment this$0;

        public void run()
        {
            class _cls1
                implements Runnable
            {

                final _cls1 this$1;

                public void run()
                {
                    mAdapter.notifyDataSetChanged();
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            if (mAdapter != null && getActivity() != null)
            {
                getActivity().runOnUiThread(new _cls1());
            }
        }

        _cls1()
        {
            this$0 = XiMaoDownloadTaskFragment.this;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final XiMaoDownloadTaskFragment this$0;

        public void run()
        {
            if (mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        _cls4()
        {
            this$0 = XiMaoDownloadTaskFragment.this;
            super();
        }
    }

}
