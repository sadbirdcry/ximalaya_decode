// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.bluetooth.miniche.BaseQCheCommand;
import com.ximalaya.ting.android.util.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            BluetoothManager, MyBluetoothDevice, ICommand

public class BtSppConnector
{
    public class BTDevSppConnThread extends Thread
    {

        private UUID SPP_UUID;
        byte arrayOfByte2[];
        byte arrayOfByte3[];
        int desPos;
        private InputStream mInStream;
        private OutputStream mOutStream;
        private BluetoothSocket mSocket;
        boolean mThreadFinished;
        int mTryTimes;
        int offset;
        final BtSppConnector this$0;
        private BluetoothSocket tmp;

        private boolean checkAudioFinish(byte abyte0[], int i)
        {
            return abyte0[i - 1] == 25 && abyte0[i - 2] == 0 && abyte0[i - 3] == 0 && abyte0[i - 4] == 10 && abyte0[i - 5] == 15;
        }

        private void cleanArray()
        {
            arrayOfByte2 = new byte[1024];
            desPos = 0;
            offset = 0;
            arrayOfByte3 = new byte[1024];
        }

        private void cleanup()
        {
            Logger.d("spp", "cleanup IN #1");
            if (mSocket != null)
            {
                try
                {
                    Logger.d("spp", "cleanup IN #2");
                    if (mOutStream != null)
                    {
                        Logger.d("spp", "cleanup IN #3");
                        mOutStream.close();
                        mOutStream = null;
                    }
                    if (mInStream != null)
                    {
                        mInStream.close();
                        mInStream = null;
                    }
                    mSocket.close();
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
                Logger.d("spp", "cleanup IN #4");
                mSocket = null;
            }
        }

        private boolean createSocket()
        {
            if (device != null)
            {
                break MISSING_BLOCK_LABEL_20;
            }
            Logger.d(BtSppConnector.TAG, "\u83B7\u53D6\u84DD\u7259\u8BBE\u5907\u5931\u8D25");
            return false;
            if (android.os.Build.VERSION.SDK_INT >= 11) goto _L2; else goto _L1
_L1:
            tmp = device.getBluetoothDevice().createRfcommSocketToServiceRecord(SPP_UUID);
_L4:
            mSocket = tmp;
            mSocket.connect();
            mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTING;
            return true;
_L2:
            try
            {
                tmp = device.getBluetoothDevice().createInsecureRfcommSocketToServiceRecord(SPP_UUID);
            }
            catch (Exception exception)
            {
                Logger.e(BtSppConnector.TAG, (new StringBuilder()).append("1st try:").append(exception.toString()).toString());
                exception.printStackTrace();
                mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.FAILED;
                return false;
            }
            if (true) goto _L4; else goto _L3
_L3:
            Exception exception1;
            exception1;
            if (mSocket != null)
            {
                mSocket.close();
                tmp.close();
                Thread.sleep(2000L);
                mSocket = null;
                tmp = null;
            }
            mSocket = null;
            tmp = (BluetoothSocket)device.getClass().getMethod("createRfcommSocket", new Class[] {
                Integer.TYPE
            }).invoke(device, new Object[] {
                Integer.valueOf(1)
            });
            mSocket = tmp;
            mSocket.connect();
            mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTING;
            return true;
            Exception exception2;
            exception2;
            if (mSocket != null)
            {
                mSocket.close();
                tmp.close();
                Thread.sleep(2000L);
                mSocket = null;
                tmp = null;
            }
            mSocket = null;
            mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.FAILED;
            return false;
            exception2;
            Logger.e(BtSppConnector.TAG, (new StringBuilder()).append("2nd try").append(exception1.toString()).toString());
            mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.FAILED;
            return false;
        }

        private void distributePacket(byte abyte0[], int i)
        {
            if (mBtConnectionReceiver != null)
            {
                mBtConnectionReceiver.onPacket(abyte0, i);
            }
        }

        private void write(byte abyte0[], int i)
        {
            if (mOutStream != null)
            {
                try
                {
                    mOutStream.write(abyte0, 0, i);
                    lastPacket = new byte[i];
                    System.arraycopy(abyte0, 0, lastPacket, 0, i);
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (byte abyte0[])
                {
                    Logger.d(BtSppConnector.TAG, "write error");
                }
                mThreadFinished = true;
                Logger.d(BtSppConnector.TAG, "write\u5B58\u5728\u5F02\u5E38\uFF0C\u6B63\u5728\u9000\u51FA");
                abyte0.printStackTrace();
                onDisconnected();
                return;
            } else
            {
                Logger.d(BtSppConnector.TAG, "\u672A\u8FDE\u63A5");
                return;
            }
        }

        public void cancel()
        {
            cleanup();
            mThreadFinished = true;
        }

        public BluetoothSocket getSocket()
        {
            return mSocket;
        }

        public void run()
        {
            mTryTimes = 0;
            if (mSocket != null) goto _L2; else goto _L1
_L1:
            mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTING;
_L5:
            if (mTryTimes <= 3 && !createSocket()) goto _L3; else goto _L2
_L2:
            if (mConnStatus == com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.FAILED)
            {
                onConnectFailed();
                return;
            }
            break; /* Loop/switch isn't completed */
_L3:
            mTryTimes = mTryTimes + 1;
            if (true) goto _L5; else goto _L4
_L4:
            byte abyte1[];
            Logger.d(BtSppConnector.TAG, (new StringBuilder()).append("create socket").append(mSocket.hashCode()).toString());
            if (mSocket == null)
            {
                mSocket = null;
                Logger.d(BtSppConnector.TAG, "mSocket\u4E3A\u7A7A\uFF0C\u521B\u5EFA\u5931\u8D25");
                onConnectFailed();
                return;
            }
            byte abyte0[];
            Exception exception;
            try
            {
                mOutStream = mSocket.getOutputStream();
                mInStream = mSocket.getInputStream();
                Logger.d(BtSppConnector.TAG, (new StringBuilder()).append("create outstream").append(mOutStream.hashCode()).toString());
                Logger.d(BtSppConnector.TAG, (new StringBuilder()).append("create instream").append(mInStream.hashCode()).toString());
            }
            catch (Exception exception1)
            {
                mSocket = null;
                Logger.d(BtSppConnector.TAG, "Stream\u83B7\u53D6\u5931\u8D25\uFF0C\u521B\u5EFA\u5931\u8D25");
                onConnectFailed();
            }
            onConnected();
            abyte0 = new byte[1024];
_L12:
            if (mThreadFinished) goto _L7; else goto _L6
_L6:
            abyte1 = new byte[1024];
            if (!mThreadFinished) goto _L8; else goto _L7
_L7:
            mThreadFinished = true;
            return;
_L8:
            String s;
            int i;
            int j;
            try
            {
                Logger.d(BtSppConnector.TAG, (new StringBuilder()).append("\u7B49\u5F85\u8BFB\u53D6\u6570\u636E:").append(offset).toString());
                mInStream = mSocket.getInputStream();
                j = mInStream.read(abyte1, offset, 1024 - offset);
            }
            // Misplaced declaration of an exception variable
            catch (Exception exception)
            {
                mThreadFinished = true;
                Logger.d(BtSppConnector.TAG, "SPP\u8FDE\u63A5\u5B58\u5728\u5F02\u5E38\uFF0C\u6B63\u5728\u9000\u51FA");
                exception.printStackTrace();
                onDisconnected();
                return;
            }
            s = "";
            i = 0;
_L10:
            if (i >= j)
            {
                break; /* Loop/switch isn't completed */
            }
            s = (new StringBuilder()).append(s).append(String.valueOf(Integer.toHexString(abyte1[i] & 0xff | 0xffffff00).substring(6))).append(" ").toString();
            i++;
            if (true) goto _L10; else goto _L9
_L9:
            Logger.d(BtSppConnector.TAG, (new StringBuilder()).append("\u8BFB\u5230\u6570\u636E").append(s).toString());
            if (mThreadFinished) goto _L7; else goto _L11
_L11:
            if (mBtConnectionReceiver != null)
            {
                mBtConnectionReceiver.onPacket(abyte1, abyte1.length);
            }
              goto _L12
        }






        public BTDevSppConnThread()
        {
            this$0 = BtSppConnector.this;
            super();
            SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
            mSocket = null;
            tmp = null;
            mOutStream = null;
            mInStream = null;
            offset = 0;
            desPos = 0;
            arrayOfByte2 = new byte[1024];
            arrayOfByte3 = new byte[1024];
            mThreadFinished = false;
            mTryTimes = 0;
            if (mBluetoothAdapter != null);
        }
    }

    public static interface BTSppConnReceiver
    {

        public abstract void onConnectFailed();

        public abstract void onConnected();

        public abstract void onDisconnected();

        public abstract void onPacket(byte abyte0[], int i);

        public abstract void onReceived(byte abyte0[], int i);

        public abstract void onSent(byte abyte0[], int i);

        public abstract void onTimeout();
    }


    public static final int SPP_BUFFER_LENGTH = 1024;
    private static final String TAG = com/ximalaya/ting/android/fragment/device/bluetooth/BtSppConnector.getSimpleName();
    MyBluetoothDevice device;
    private byte lastPacket[];
    BluetoothAdapter mBluetoothAdapter;
    private BTSppConnReceiver mBtConnectionReceiver;
    ICommand mCommand;
    private com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE mConnStatus;
    Context mContext;
    private BTDevSppConnThread mSppConnThread;
    public TimerTask task;
    public Timer timer;

    public BtSppConnector()
    {
        lastPacket = null;
        mCommand = new BaseQCheCommand();
    }

    public BtSppConnector(Context context, MyBluetoothDevice mybluetoothdevice)
    {
        lastPacket = null;
        mCommand = new BaseQCheCommand();
        mContext = context;
        initBtAdapter();
        device = mybluetoothdevice;
    }

    private void disconnectDevice()
    {
        mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.DISCONNECTED;
    }

    private void initBtAdapter()
    {
        if (mBluetoothAdapter == null)
        {
            try
            {
                mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                return;
            }
            catch (Exception exception) { }
            if (MyApplication.a() != null)
            {
                MyApplication.a().runOnUiThread(new _cls2());
                return;
            }
        }
    }

    private int isFinish(byte abyte0[], int i)
    {
        if (i < 5)
        {
            return -1;
        }
        int j = abyte0[2] * 256 + abyte0[3];
        if (i >= j + 5)
        {
            Logger.d("ximao", "finish");
            return j + 5;
        } else
        {
            Logger.d("ximao", "not finish");
            return -1;
        }
    }

    private void showToast(String s)
    {
        Logger.d(TAG, s);
    }

    public void cancel()
    {
        Logger.d("QSuiCheTing", "cancel IN");
        if (mSppConnThread != null)
        {
            mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.START;
            mSppConnThread.cancel();
            mSppConnThread = null;
        }
        mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.START;
    }

    public ICommand getmCommand()
    {
        return mCommand;
    }

    public boolean isSppConnRunning()
    {
        if (mSppConnThread != null)
        {
            Logger.d("spp", (new StringBuilder()).append("").append(mSppConnThread).append(":").append(mSppConnThread.mSocket).append(":").append(mSppConnThread.mInStream).append(":").append(mSppConnThread.mOutStream).append(":").append(mSppConnThread.mThreadFinished).toString());
        } else
        {
            Logger.d("spp", "mSppConnThread is null");
        }
        if (mSppConnThread != null && mSppConnThread.mSocket != null && mSppConnThread.mInStream != null && mSppConnThread.mOutStream != null && !mSppConnThread.mThreadFinished)
        {
            return true;
        } else
        {
            Logger.d("spp", "OUT #4");
            return false;
        }
    }

    protected void onConnectFailed()
    {
        if (timer != null)
        {
            timer.cancel();
        }
        mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.FAILED;
        if (mBtConnectionReceiver != null)
        {
            mBtConnectionReceiver.onConnectFailed();
        }
    }

    protected void onConnected()
    {
        if (timer != null)
        {
            timer.cancel();
        }
        Logger.d(TAG, "\u5DF2\u8FDE\u63A5");
        mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTED;
        Logger.d(TAG, (new StringBuilder()).append("spp thread onConnected").append(BluetoothManager.getInstance(mContext)).append(this).append(mSppConnThread).toString());
        if (mBtConnectionReceiver != null)
        {
            mBtConnectionReceiver.onConnected();
        }
    }

    protected void onDisconnected()
    {
        if (mConnStatus == com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTED && mBtConnectionReceiver != null)
        {
            mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.DISCONNECTED;
            mBtConnectionReceiver.onDisconnected();
        } else
        {
            Logger.d(TAG, "mBTDevMainConnectionReceiver\u4E3A\u7A7A");
        }
        cancel();
        mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.DISCONNECTED;
    }

    public boolean sendPacket(byte abyte0[], int i)
    {
        if (mSppConnThread != null)
        {
            mSppConnThread.cleanArray();
            mSppConnThread.write(abyte0, i);
            return true;
        } else
        {
            return false;
        }
    }

    public void setBtConnReceiaver(BTSppConnReceiver btsppconnreceiver)
    {
        mBtConnectionReceiver = btsppconnreceiver;
    }

    public void setmCommand(ICommand icommand)
    {
        mCommand = icommand;
    }

    void startSppThread()
    {
        while (isSppConnRunning() || device == null) 
        {
            return;
        }
        timer = new Timer();
        task = new _cls1();
        timer.schedule(task, 15000L);
        cancel();
        mSppConnThread = new BTDevSppConnThread();
        mSppConnThread.start();
    }




/*
    static com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE access$002(BtSppConnector btsppconnector, com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE state)
    {
        btsppconnector.mConnStatus = state;
        return state;
    }

*/






/*
    static byte[] access$702(BtSppConnector btsppconnector, byte abyte0[])
    {
        btsppconnector.lastPacket = abyte0;
        return abyte0;
    }

*/

    private class _cls2
        implements Runnable
    {

        final BtSppConnector this$0;

        public void run()
        {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        }

        _cls2()
        {
            this$0 = BtSppConnector.this;
            super();
        }
    }


    private class _cls1 extends TimerTask
    {

        final BtSppConnector this$0;

        public void run()
        {
            if (mConnStatus != com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTED)
            {
                if (mSppConnThread != null)
                {
                    mSppConnThread.cancel();
                }
                mConnStatus = com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.FAILED;
                if (mBtConnectionReceiver != null)
                {
                    mBtConnectionReceiver.onTimeout();
                }
            }
        }

        _cls1()
        {
            this$0 = BtSppConnector.this;
            super();
        }
    }

}
