// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.che;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import com.ximalaya.ting.android.fragment.device.bluetooth.BaseBtController;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.BtPackage;
import com.ximalaya.ting.android.fragment.device.bluetooth.ICommand;
import com.ximalaya.ting.android.fragment.device.bluetooth.MyBluetoothDevice;
import com.ximalaya.ting.android.fragment.device.bluetooth.miniche.BtDeviceType;
import com.ximalaya.ting.android.fragment.device.bluetooth.model.RecordModel;

public class CheController extends BaseBtController
{

    public CheController(Context context)
    {
        super(context);
    }

    public BtDeviceType getNowBtDeviceType()
    {
        return null;
    }

    public String getRealName()
    {
        return null;
    }

    public RecordModel getRecordModel()
    {
        if (BluetoothManager.getInstance(mContext).getMyBluetoothDevice() != null)
        {
            return new RecordModel("1", "1", BluetoothManager.getInstance(mContext).getMyBluetoothDevice().getBluetoothDevice().getAddress());
        } else
        {
            return new RecordModel("1", "1", "suicheting");
        }
    }

    protected void initModule()
    {
    }

    public BtPackage parseCommand(ICommand icommand)
    {
        return null;
    }

    public ICommand parseCommand(BtPackage btpackage)
    {
        return null;
    }
}
