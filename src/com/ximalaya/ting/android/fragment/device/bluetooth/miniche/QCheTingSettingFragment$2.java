// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import android.widget.CompoundButton;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.miniche:
//            QCheTingSettingFragment, QCheFmModule, QCheController

class this._cls0
    implements android.widget.angeListener
{

    final QCheTingSettingFragment this$0;

    public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
    {
        if (flag)
        {
            QCheTingSettingFragment.access$000(QCheTingSettingFragment.this).switchControl("Voice_on");
            QCheTingSettingFragment.access$100(QCheTingSettingFragment.this).setWarning(true);
            Logger.d(QCheTingSettingFragment.TAG, "\u6240\u6709\u8BED\u97F3\u63D0\u793A\u6253\u5F00\u6307\u4EE4\u5DF2\u53D1\u9001");
            return;
        } else
        {
            QCheTingSettingFragment.access$000(QCheTingSettingFragment.this).switchControl("Voice_off");
            QCheTingSettingFragment.access$100(QCheTingSettingFragment.this).setWarning(false);
            Logger.d(QCheTingSettingFragment.TAG, "\u6240\u6709\u8BED\u97F3\u63D0\u793A\u5173\u95ED\u6307\u4EE4\u5DF2\u53D1\u9001");
            return;
        }
    }

    ()
    {
        this$0 = QCheTingSettingFragment.this;
        super();
    }
}
