// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;


public final class BtDeviceType extends Enum
{

    private static final BtDeviceType $VALUES[];
    public static final BtDeviceType qsuicheting;
    public static final BtDeviceType suicheting;
    public static final BtDeviceType unReg;
    public static final BtDeviceType ximao;

    private BtDeviceType(String s, int i)
    {
        super(s, i);
    }

    public static BtDeviceType valueOf(String s)
    {
        return (BtDeviceType)Enum.valueOf(com/ximalaya/ting/android/fragment/device/bluetooth/miniche/BtDeviceType, s);
    }

    public static BtDeviceType[] values()
    {
        return (BtDeviceType[])$VALUES.clone();
    }

    static 
    {
        unReg = new BtDeviceType("unReg", 0);
        suicheting = new BtDeviceType("suicheting", 1);
        ximao = new BtDeviceType("ximao", 2);
        qsuicheting = new BtDeviceType("qsuicheting", 3);
        $VALUES = (new BtDeviceType[] {
            unReg, suicheting, ximao, qsuicheting
        });
    }
}
