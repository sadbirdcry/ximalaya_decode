// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;

import com.ximalaya.ting.android.fragment.device.bluetooth.miniche.BtDeviceType;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            IBtModule

public interface IBtDeviceController
{

    public abstract IBtModule getModule(String s);

    public abstract BtDeviceType getNowBtDeviceType();

    public abstract void setModule(IBtModule ibtmodule);
}
