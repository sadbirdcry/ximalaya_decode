// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;

import android.a.a;
import android.app.Activity;
import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.IBinder;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.util.Logger;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            BluetoothManager, BaseBluetoothCallBack

public class BluetoothUtil
{

    private static final String TAG = com/ximalaya/ting/android/fragment/device/bluetooth/BluetoothUtil.getSimpleName();

    public BluetoothUtil()
    {
    }

    public static void getConnectedDevice(final Context context, final BaseBluetoothCallBack callback)
    {
        if (BluetoothManager.getInstance(context).getBluetoothProfile() == null) goto _L2; else goto _L1
_L1:
        context = BluetoothManager.getInstance(context).getBluetoothProfile().getConnectedDevices();
        if (context == null || context.size() == 0) goto _L4; else goto _L3
_L3:
        context = (BluetoothDevice)context.get(0);
        ActionModel actionmodel = new ActionModel();
        actionmodel.result.put("device", context);
        callback.onSuccess(actionmodel);
_L6:
        return;
_L4:
        callback.onFailed();
        return;
_L2:
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            break MISSING_BLOCK_LABEL_319;
        }
        ActionModel actionmodel1;
        try
        {
            context = (IBinder)Class.forName("android.os.ServiceManager").getDeclaredMethod("getService", new Class[] {
                java/lang/String
            }).invoke(null, new Object[] {
                "bluetooth_a2dp"
            });
            Method method = Class.forName("android.a.a").getDeclaredClasses()[0].getDeclaredMethod("asInterface", new Class[] {
                android/os/IBinder
            });
            method.setAccessible(true);
            context = (BluetoothDevice[])(BluetoothDevice[])((a)method.invoke(null, new Object[] {
                context
            })).getClass().getDeclaredMethod("getConnectedSinks", new Class[0]).invoke((a)method.invoke(null, new Object[] {
                context
            }), new Object[0]);
        }
        // Misplaced declaration of an exception variable
        catch (final Context context)
        {
            context.printStackTrace();
            Logger.e(TAG, (new StringBuilder()).append("error:").append(context.toString()).toString());
            callback.onFailed();
            return;
        }
        if (context == null)
        {
            break MISSING_BLOCK_LABEL_314;
        }
        if (context.length != 0)
        {
            Logger.d(TAG, (new StringBuilder()).append("\u84DD\u7259\u8BBE\u5907").append(context[0]).toString());
            actionmodel1 = new ActionModel();
            actionmodel1.result.put("device", context[0]);
            callback.onSuccess(actionmodel1);
            return;
        }
        callback.onFailed();
        return;
        try
        {
            Constructor constructor = Class.forName("android.bluetooth.BluetoothA2dp").getDeclaredConstructor(new Class[] {
                android/content/Context, android/bluetooth/BluetoothProfile$ServiceListener
            });
            constructor.setAccessible(true);
            context = ((BluetoothA2dp)constructor.newInstance(new Object[] {
                context, new _cls1()
            })).getConnectedDevices();
        }
        // Misplaced declaration of an exception variable
        catch (final Context context)
        {
            context.printStackTrace();
            callback.onFailed();
            return;
        }
        if (context == null) goto _L6; else goto _L5
_L5:
        if (context.size() == 0) goto _L6; else goto _L7
_L7:
        Logger.d(TAG, (new StringBuilder()).append("\u84DD\u7259\u8BBE\u5907").append(context.get(0)).toString());
        ActionModel actionmodel2 = new ActionModel();
        actionmodel2.result.put("device", context.get(0));
        callback.onSuccess(actionmodel2);
        return;
    }

    public static void playContinue(final boolean isContinue)
    {
        if (MyApplication.a() == null)
        {
            return;
        } else
        {
            MyApplication.a().runOnUiThread(new _cls2());
            return;
        }
    }



    private class _cls1
        implements android.bluetooth.BluetoothProfile.ServiceListener
    {

        final BaseBluetoothCallBack val$callback;
        final Context val$context;

        public void onServiceConnected(int i, BluetoothProfile bluetoothprofile)
        {
            Object obj = bluetoothprofile.getConnectedDevices();
            BluetoothManager.getInstance(context).setBluetoothProfile(bluetoothprofile);
            if (obj != null && ((List) (obj)).size() != 0)
            {
                bluetoothprofile = (BluetoothDevice)((List) (obj)).get(0);
                obj = new ActionModel();
                ((ActionModel) (obj)).result.put("device", bluetoothprofile);
                callback.onSuccess(((ActionModel) (obj)));
            }
        }

        public void onServiceDisconnected(int i)
        {
        }

        _cls1()
        {
            context = context1;
            callback = basebluetoothcallback;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final boolean val$isContinue;

        public void run()
        {
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            Logger.d(BluetoothUtil.TAG, (new StringBuilder()).append("isContinue:").append(isContinue).toString());
            if (isContinue)
            {
                if (localmediaservice != null)
                {
                    localmediaservice.start();
                    Logger.d(BluetoothUtil.TAG, "\u64AD\u653E\u5668\u6B63\u5728\u64AD\u653E");
                }
            } else
            if (localmediaservice != null)
            {
                localmediaservice.pause();
                Logger.d(BluetoothUtil.TAG, "\u64AD\u653E\u5668\u5DF2\u6682\u5B9A");
                return;
            }
        }

        _cls2()
        {
            isContinue = flag;
            super();
        }
    }

}
