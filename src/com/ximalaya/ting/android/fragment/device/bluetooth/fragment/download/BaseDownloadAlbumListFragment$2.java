// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download;

import android.os.AsyncTask;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download:
//            BaseDownloadAlbumListFragment, BaseSoundsDownloadForAlbumAdapter

class this._cls0 extends AsyncTask
{

    final BaseDownloadAlbumListFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        return DownloadHandler.getInstance(mCon).getSortedFinishedDownloadList();
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        if (!isAdded())
        {
            return;
        }
        if (list != null)
        {
            BaseDownloadAlbumListFragment.access$000(BaseDownloadAlbumListFragment.this).clear();
            BaseDownloadAlbumListFragment.access$000(BaseDownloadAlbumListFragment.this).addAll(list);
            soundsDownloadAdapter.setList(BaseDownloadAlbumListFragment.access$000(BaseDownloadAlbumListFragment.this));
            soundsDownloadAdapter.notifyDataSetChanged();
        }
        BaseDownloadAlbumListFragment.access$100(BaseDownloadAlbumListFragment.this);
    }

    r()
    {
        this$0 = BaseDownloadAlbumListFragment.this;
        super();
    }
}
