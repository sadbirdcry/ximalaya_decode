// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;

import android.content.Context;
import com.ximalaya.ting.android.model.download.DownloadTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            IBtModule, BaseFmModule

public abstract class BaseDownloadModule
    implements IBtModule
{

    public static final String NAME = com/ximalaya/ting/android/fragment/device/bluetooth/BaseFmModule.getSimpleName();
    protected Context mContext;

    public BaseDownloadModule(Context context)
    {
        mContext = context;
    }

    public abstract void download(DownloadTask downloadtask, int i);

    public abstract void download(List list, int i);

    public String getModuleName()
    {
        return NAME;
    }

}
