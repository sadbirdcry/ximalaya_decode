// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ximalaya.ting.android.adapter.SoundsDownloadForAlbumAdapter;
import java.util.List;

public abstract class BaseSoundsDownloadForAlbumAdapter extends SoundsDownloadForAlbumAdapter
{

    public BaseSoundsDownloadForAlbumAdapter(Context context, List list)
    {
        super(context, list);
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        view = super.getView(i, view, viewgroup);
        viewgroup = (com.ximalaya.ting.android.adapter.SoundsDownloadForAlbumAdapter.ViewHolder)view.getTag();
        ((com.ximalaya.ting.android.adapter.SoundsDownloadForAlbumAdapter.ViewHolder) (viewgroup)).del_btn.setImageDrawable(mContext.getResources().getDrawable(0x7f020016));
        view.setTag(viewgroup);
        return view;
    }

    public abstract void onClick(View view);
}
