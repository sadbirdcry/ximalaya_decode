// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;


// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            BtSppConnector

public static interface 
{

    public abstract void onConnectFailed();

    public abstract void onConnected();

    public abstract void onDisconnected();

    public abstract void onPacket(byte abyte0[], int i);

    public abstract void onReceived(byte abyte0[], int i);

    public abstract void onSent(byte abyte0[], int i);

    public abstract void onTimeout();
}
