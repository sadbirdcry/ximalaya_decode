// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.ICommand;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.view.dial.CircleProgressBar;
import java.math.BigDecimal;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.miniche:
//            BaseQCheCommand, QCheFmModule, QCheTingSettingFragment

public class QCheFmSenderFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.OnBtConnListener, com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.OnGetCommand
{

    private static final String LOADING_STRING = "\u8F7D\u5165\u4E2D..";
    protected static final String TAG = "QCheTing";
    BaseQCheCommand baseQCheCommand;
    Runnable frequenceSet;
    boolean isOnTouch;
    private ImageView mBackImg;
    private TextView mBtnRight;
    private CircleProgressBar mCircleProgressBar;
    private RelativeLayout mContentView;
    private TextView mHzlabel;
    private boolean mInitStartValue;
    private boolean mIsCanSet;
    private float mRateValue;
    private ScrollView mScrollView;
    private Thread mThread;
    private TextView mTop11;
    private TextView mTop12;
    private TextView mTop21;
    private TextView mTop22;
    private ImageView mTurnDownIv;
    private ImageView mTurnUpIv;
    private TextView mValueTv;
    private QCheFmModule qCheFmModule;
    private TextView top_tv;

    public QCheFmSenderFragment()
    {
        mInitStartValue = true;
        baseQCheCommand = new BaseQCheCommand();
        isOnTouch = false;
        frequenceSet = new _cls6();
    }

    public static float add(float f, float f1)
    {
        return (new BigDecimal(Float.toString(f))).add(new BigDecimal(Float.toString(f1))).floatValue();
    }

    private void freshValueShow()
    {
        mValueTv.setText((new StringBuilder()).append(mRateValue).append("").toString());
        mHzlabel.setText("MHz");
        mCircleProgressBar.setValue(mRateValue);
    }

    private void freshValueShowFmOps(final float value)
    {
        Object obj1 = getActivity();
        Object obj = obj1;
        if (obj1 == null)
        {
            obj = mActivity;
        }
        obj1 = obj;
        if (obj == null)
        {
            obj1 = MyApplication.a();
        }
        if (obj1 == null)
        {
            return;
        } else
        {
            ((Activity) (obj1)).runOnUiThread(new _cls7());
            return;
        }
    }

    private void initView()
    {
        mTop11 = (TextView)mContentView.findViewById(0x7f0a009a);
        mTop11.setLayoutParams(new android.widget.LinearLayout.LayoutParams(-2, -2));
        mTop12 = (TextView)mContentView.findViewById(0x7f0a009b);
        mTop12.setLayoutParams(new android.widget.LinearLayout.LayoutParams(-2, -2));
        mTop21 = (TextView)mContentView.findViewById(0x7f0a009c);
        mTop21.setLayoutParams(new android.widget.LinearLayout.LayoutParams(-2, -2));
        mTop22 = (TextView)mContentView.findViewById(0x7f0a009d);
        mTop22.setLayoutParams(new android.widget.LinearLayout.LayoutParams(-2, -2));
        mValueTv = (TextView)findViewById(0x7f0a00a0);
        mHzlabel = (TextView)findViewById(0x7f0a00a1);
        top_tv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        top_tv.setText("\u968F\u8F66\u542CQ\u7248");
        mBackImg.setOnClickListener(this);
        mBtnRight = (TextView)mContentView.findViewById(0x7f0a071c);
        mBtnRight.setOnClickListener(this);
        mBtnRight.setText("\u8BBE\u7F6E");
        mBtnRight.setVisibility(0);
        mScrollView = (ScrollView)findViewById(0x7f0a0098);
        mCircleProgressBar = (CircleProgressBar)findViewById(0x7f0a00a5);
        mCircleProgressBar.setOnTouchListener(new _cls2());
        mValueTv.setText("\u8F7D\u5165\u4E2D..");
        mHzlabel.setText("");
        mTurnUpIv = (ImageView)findViewById(0x7f0a00a2);
        mTurnUpIv.setOnClickListener(this);
        mTurnDownIv = (ImageView)findViewById(0x7f0a00a3);
        mTurnDownIv.setOnClickListener(this);
        mTurnDownIv.setOnTouchListener(new _cls3());
        mTurnUpIv.setOnTouchListener(new _cls4());
        mCircleProgressBar.setDialListener(new _cls5());
    }

    private void removeFrequencyDelayed()
    {
        if (!mIsCanSet)
        {
            mCircleProgressBar.removeCallbacks(frequenceSet);
            mIsCanSet = true;
        }
    }

    public static float sub(float f, float f1)
    {
        return (new BigDecimal(Float.toString(f))).subtract(new BigDecimal(Float.toString(f1))).floatValue();
    }

    private void turnDown()
    {
        if ((double)mRateValue <= 87.5D)
        {
            mRateValue = 108F;
        } else
        {
            mRateValue = sub(mRateValue, 0.1F);
        }
        freshValueShow();
        mCircleProgressBar.setValue(mRateValue);
    }

    private void turnUp()
    {
        if (mRateValue >= 108F)
        {
            mRateValue = 87.5F;
        } else
        {
            mRateValue = add(mRateValue, 0.1F);
        }
        freshValueShow();
        mCircleProgressBar.setValue(mRateValue);
    }

    public void init()
    {
        mContentView.findViewById(0x7f0a0317).setOnClickListener(this);
    }

    public boolean onA2dpConn()
    {
        return false;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        Logger.d("QCheTing", "QCheFmSenderFragment:onActivityCreated");
        initView();
        mThread = new Thread(new _cls1());
        mThread.start();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131361954: 
            turnUp();
            qCheFmModule.setFrequency(mRateValue);
            return;

        case 2131361955: 
            Logger.d("onTouch", "onClick");
            turnDown();
            isOnTouch = false;
            qCheFmModule.setFrequency(mRateValue);
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;

        case 2131363612: 
            startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheTingSettingFragment, null);
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f030016, viewgroup, false);
        return mContentView;
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (mThread == null)
        {
            break MISSING_BLOCK_LABEL_23;
        }
        mThread.interrupt();
        mThread = null;
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
        return;
    }

    public void onGetCommand(ICommand icommand)
    {
        baseQCheCommand = (BaseQCheCommand)icommand;
        baseQCheCommand.getType();
        JVM INSTR tableswitch 4 4: default 32
    //                   4 33;
           goto _L1 _L2
_L1:
        return;
_L2:
        switch (baseQCheCommand.getResult())
        {
        default:
            return;

        case 6: // '\006'
            Logger.d("QCheTing", (new StringBuilder()).append("\u8FD4\u56DE\u9891\u7387 ").append(baseQCheCommand.getT()).toString());
            float f = (float)((Integer)baseQCheCommand.getT()).intValue() / 10F;
            Logger.d("QCheTing", (new StringBuilder()).append("\u67E5\u8BE2\u7ED3\u679C  ").append(f).toString());
            freshValueShowFmOps(f);
            return;

        case 7: // '\007'
            break;
        }
        if (qCheFmModule != null)
        {
            qCheFmModule.frequencyQuery();
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public void onResume()
    {
        Logger.d("spp", "QCheFmSenderFragment onResume");
        super.onResume();
        hidePlayButton();
        if (!BluetoothManager.getInstance(mCon).isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
        {
            getActivity().onBackPressed();
        }
        BluetoothManager.getInstance(mCon).setmOnGetCommands(this);
        BluetoothManager.getInstance(mCon).setBtConnListener(this);
    }

    public boolean onSppConn(com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE btstate)
    {
        boolean flag;
        boolean flag1;
        flag1 = true;
        flag = flag1;
        static class _cls9
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE = new int[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.CONNECTED.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.DISCONNECTED.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.TIMEOUT.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.FAILED.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls9..SwitchMap.com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE[btstate.ordinal()];
        JVM INSTR tableswitch 1 4: default 44
    //                   1 46
    //                   2 48
    //                   3 44
    //                   4 44;
           goto _L1 _L2 _L3 _L1 _L1
_L2:
        break; /* Loop/switch isn't completed */
_L1:
        flag = false;
_L5:
        return flag;
_L3:
        flag = flag1;
        if (getActivity() != null)
        {
            getActivity().runOnUiThread(new _cls8());
            return true;
        }
        if (true) goto _L5; else goto _L4
_L4:
    }

    public void onStop()
    {
        BluetoothManager.getInstance(mCon).removeBtConnListener(this);
        BluetoothManager.getInstance(mCon).removeOnGetCommands(this);
        super.onStop();
    }


/*
    static boolean access$002(QCheFmSenderFragment qchefmsenderfragment, boolean flag)
    {
        qchefmsenderfragment.mInitStartValue = flag;
        return flag;
    }

*/






/*
    static QCheFmModule access$202(QCheFmSenderFragment qchefmsenderfragment, QCheFmModule qchefmmodule)
    {
        qchefmsenderfragment.qCheFmModule = qchefmmodule;
        return qchefmmodule;
    }

*/





/*
    static float access$502(QCheFmSenderFragment qchefmsenderfragment, float f)
    {
        qchefmsenderfragment.mRateValue = f;
        return f;
    }

*/





    private class _cls6
        implements Runnable
    {

        final QCheFmSenderFragment this$0;

        public void run()
        {
            qCheFmModule.setFrequency(mRateValue);
        }

        _cls6()
        {
            this$0 = QCheFmSenderFragment.this;
            super();
        }
    }


    private class _cls7
        implements Runnable
    {

        final QCheFmSenderFragment this$0;
        final float val$value;

        public void run()
        {
            mRateValue = value;
            freshValueShow();
        }

        _cls7()
        {
            this$0 = QCheFmSenderFragment.this;
            value = f;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnTouchListener
    {

        final QCheFmSenderFragment this$0;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            motionevent.getAction();
            JVM INSTR tableswitch 0 1: default 28
        //                       0 40
        //                       1 61;
               goto _L1 _L2 _L3
_L1:
            return mCircleProgressBar.onTouchEvent(motionevent);
_L2:
            mScrollView.requestDisallowInterceptTouchEvent(true);
            removeFrequencyDelayed();
            continue; /* Loop/switch isn't completed */
_L3:
            mScrollView.requestDisallowInterceptTouchEvent(false);
            qCheFmModule.setFrequency(mRateValue);
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls2()
        {
            this$0 = QCheFmSenderFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnTouchListener
    {

        final QCheFmSenderFragment this$0;
        Timer timer;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            switch (motionevent.getAction())
            {
            default:
                return false;

            case 0: // '\0'
                Logger.d("onTouch", "ACTION_DOWN");
                timer = new Timer();
                isOnTouch = true;
                timer.schedule(new _cls1(), 500L, 100L);
                return false;

            case 1: // '\001'
                Logger.d("onTouch", "ACTION_UP");
                break;
            }
            if (timer != null)
            {
                timer.cancel();
                timer = null;
            }
            isOnTouch = false;
            qCheFmModule.setFrequency(mRateValue);
            return false;
        }

        _cls3()
        {
            this$0 = QCheFmSenderFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnTouchListener
    {

        final QCheFmSenderFragment this$0;
        Timer timer;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            switch (motionevent.getAction())
            {
            default:
                return false;

            case 0: // '\0'
                Logger.d("onTouch", "ACTION_DOWN");
                timer = new Timer();
                isOnTouch = true;
                timer.schedule(new _cls1(), 500L, 100L);
                return false;

            case 1: // '\001'
                Logger.d("onTouch", "ACTION_UP");
                break;
            }
            if (timer != null)
            {
                timer.cancel();
                timer = null;
            }
            isOnTouch = false;
            qCheFmModule.setFrequency(mRateValue);
            return false;
        }

        _cls4()
        {
            this$0 = QCheFmSenderFragment.this;
            super();
        }
    }


    private class _cls5
        implements com.ximalaya.ting.android.view.dial.CircleProgressBar.DialListener
    {

        final QCheFmSenderFragment this$0;

        public void onValueAt(float f)
        {
            mRateValue = f;
            freshValueShow();
        }

        _cls5()
        {
            this$0 = QCheFmSenderFragment.this;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final QCheFmSenderFragment this$0;

        public void run()
        {
            mInitStartValue = true;
            while (mValueTv.getText().toString().equals("\u8F7D\u5165\u4E2D..")) 
            {
                qCheFmModule = (QCheFmModule)BluetoothManager.getInstance(mCon).getBtDeviceController().getModule(BaseFmModule.NAME);
                qCheFmModule.frequencyQuery();
                try
                {
                    Thread.sleep(1000L);
                }
                catch (InterruptedException interruptedexception)
                {
                    interruptedexception.printStackTrace();
                }
            }
        }

        _cls1()
        {
            this$0 = QCheFmSenderFragment.this;
            super();
        }
    }


    private class _cls8
        implements Runnable
    {

        final QCheFmSenderFragment this$0;

        public void run()
        {
            goBackFragment(com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheTingIntroFragment);
        }

        _cls8()
        {
            this$0 = QCheFmSenderFragment.this;
            super();
        }
    }

}
