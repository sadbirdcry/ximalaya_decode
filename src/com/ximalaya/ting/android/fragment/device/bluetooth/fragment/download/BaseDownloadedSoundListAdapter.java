// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import com.ximalaya.ting.android.adapter.DownloadedSoundListAdapter;
import com.ximalaya.ting.android.model.download.DownloadTask;
import java.util.List;

public abstract class BaseDownloadedSoundListAdapter extends DownloadedSoundListAdapter
{

    public BaseDownloadedSoundListAdapter(Activity activity, List list)
    {
        super(activity, list);
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
        view = super.getView(position, view, viewgroup);
        viewgroup = (com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder)view.getTag();
        ((com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder) (viewgroup)).cover.setOnClickListener(new _cls1());
        ((com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder) (viewgroup)).btn.setOnClickListener(new _cls2());
        ((com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder) (viewgroup)).btn.setImageResource(0x7f020016);
        view.setTag(viewgroup);
        return view;
    }

    public abstract void onDownloadClick(DownloadTask downloadtask);


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final BaseDownloadedSoundListAdapter this$0;

        public void onClick(View view)
        {
        }

        _cls1()
        {
            this$0 = BaseDownloadedSoundListAdapter.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final BaseDownloadedSoundListAdapter this$0;
        final int val$position;

        public void onClick(View view)
        {
            onDownloadClick((DownloadTask)
// JavaClassFileOutputException: get_constant: invalid tag

        _cls2()
        {
            this$0 = BaseDownloadedSoundListAdapter.this;
            position = i;
            super();
        }
    }

}
