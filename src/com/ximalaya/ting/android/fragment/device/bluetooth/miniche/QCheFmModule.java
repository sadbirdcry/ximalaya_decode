// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.ByteUtil;
import com.ximalaya.ting.android.fragment.device.bluetooth.BaseFmModule;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.ICommand;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.miniche:
//            BaseQCheCommand

public class QCheFmModule extends BaseFmModule
{

    public static final int AUTOPLAY_OFF = 11;
    public static final int AUTOPLAY_ON = 10;
    public static final int FAIL = 7;
    public static final int FM_FREQUENCY = 1;
    public static final int FM_QUERY = 4;
    public static final int PLAY_SOUND = 2;
    public static final int SUCCESS = 6;
    public static final int SWITCH_CONTROL = 3;
    public static final int SWITCH_STATE_QUERY = 5;
    public static final int VOICE_OFF = 9;
    public static final int VOICE_ON = 8;
    BaseQCheCommand baseQcheCommand;
    BluetoothManager mBluetoothManager;
    private Context mContext;

    public QCheFmModule(Context context)
    {
        baseQcheCommand = new BaseQCheCommand();
        mBluetoothManager = BluetoothManager.getInstance(mContext);
        mContext = context;
    }

    public void frequencyQuery()
    {
        while (!mBluetoothManager.isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting) || baseQcheCommand == null) 
        {
            return;
        }
        baseQcheCommand.setT("FM_freq");
        baseQcheCommand.setType(4);
        mBluetoothManager.sendCommand(baseQcheCommand);
        Logger.d(TAG, (new StringBuilder()).append("\u9891\u7387\u67E5\u8BE2\u5DF2\u53D1\u9001 : ").append(ByteUtil.getBytes("FM_freq")).toString());
    }

    public float parseFrequencyCommand(ICommand icommand)
    {
        float f1;
        float f2;
        icommand = (BaseQCheCommand)icommand;
        f1 = 0.0F;
        f2 = 0.0F;
        if (icommand == null) goto _L2; else goto _L1
_L1:
        float f = f2;
        icommand.getType();
        JVM INSTR tableswitch 1 5: default 56
    //                   1 60
    //                   2 175
    //                   3 96
    //                   4 69
    //                   5 107;
           goto _L2 _L3 _L4 _L5 _L6 _L7
_L2:
        f1 = 0.0F;
_L9:
        return f1;
_L3:
        if (icommand.getResult() != 6);
_L6:
        f = f2;
        if (icommand.getResult() == 6)
        {
            f = (float)((Integer)icommand.getT()).intValue() / 10F;
        }
_L5:
        f1 = f;
        if (icommand.getResult() != 6);
_L7:
        f = f1;
        if (icommand.getResult() == 6)
        {
            f = f1;
            switch (((Integer)icommand.getT()).intValue())
            {
            default:
                f = f1;
                break;

            case 8: // '\b'
            case 9: // '\t'
            case 10: // '\n'
            case 11: // '\013'
                break;
            }
        }
_L10:
        f1 = f;
        if (icommand.getResult() != 6) goto _L9; else goto _L8
_L8:
        return f;
_L4:
        f = 0.0F;
          goto _L10
    }

    public void playAlertSound()
    {
        if (!mBluetoothManager.isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
        {
            return;
        } else
        {
            BaseQCheCommand baseqchecommand = new BaseQCheCommand();
            baseqchecommand.setT(Integer.valueOf(1));
            baseqchecommand.setType(2);
            BluetoothManager.getInstance(mContext).sendCommand(baseqchecommand);
            return;
        }
    }

    public void setFrequency(float f)
    {
        if (!mBluetoothManager.isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
        {
            return;
        }
        if (baseQcheCommand != null)
        {
            String s = (new StringBuilder()).append(f).append("").toString();
            baseQcheCommand.setT(s);
            baseQcheCommand.setType(1);
            mBluetoothManager.sendCommand(baseQcheCommand);
        }
        Logger.d(TAG, (new StringBuilder()).append("setFrequency:").append(f).toString());
    }

    public void switchControl(String s)
    {
        if (!mBluetoothManager.isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
        {
            return;
        } else
        {
            baseQcheCommand.setT(s);
            baseQcheCommand.setType(3);
            BluetoothManager.getInstance(mContext).sendCommand(baseQcheCommand);
            return;
        }
    }

    public void switchStateQuery(String s)
    {
        if (!mBluetoothManager.isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
        {
            return;
        } else
        {
            baseQcheCommand.setT(s);
            baseQcheCommand.setType(5);
            BluetoothManager.getInstance(mContext).sendCommand(baseQcheCommand);
            return;
        }
    }
}
