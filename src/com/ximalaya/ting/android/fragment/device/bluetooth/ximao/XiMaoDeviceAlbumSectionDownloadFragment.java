// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.ximao;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.fragment.album.SectionContentAdapter;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonDeviceAlbumSectionDownloadFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonTFListSubMainFragment;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoContentFragment;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoMainAdapter;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoMainFragmentNew;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.album.AlbumSectionModel;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.ximao:
//            XiMaoSectionContentAdapter, XiMaoDownloadModule, XiMaoDownloadTaskFragment

public class XiMaoDeviceAlbumSectionDownloadFragment extends CommonDeviceAlbumSectionDownloadFragment
    implements XiMaoDownloadHandler.OnBluetoothDownloadStatusListener
{

    private MenuDialog mDeviceMenuDialog;

    public XiMaoDeviceAlbumSectionDownloadFragment()
    {
    }

    private void showDirectionChoose(final List tasks)
    {
        int i = 0;
        if (tasks != null && tasks.size() >= 1)
        {
            Logger.d("test", (new StringBuilder()).append("#1").append(((com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)tasks.get(0)).getTitle()).toString());
        }
        ArrayList arraylist = new ArrayList();
        for (; i < XiMaoMainAdapter.names.size(); i++)
        {
            if (!((String)XiMaoMainAdapter.names.get(i)).equals("\u6DFB\u52A0\u5217\u8868"))
            {
                arraylist.add(XiMaoMainAdapter.names.get(i));
            }
        }

        mDeviceMenuDialog = new MenuDialog(MyApplication.a(), arraylist, a.e, new _cls1(), 1);
        mDeviceMenuDialog.setHeaderTitle("\u6DFB\u52A0\u81F3\u6587\u4EF6\u5939");
        mDeviceMenuDialog.show();
    }

    protected double getAvailableStorage()
    {
        return XiMaoBTManager.getInstance(mCon).getStorage();
    }

    protected SectionContentAdapter getContentAdapter(AlbumSectionModel albumsectionmodel)
    {
        if (mSectionContentAdapter == null)
        {
            mSectionContentAdapter = new XiMaoSectionContentAdapter(getActivity(), albumsectionmodel);
        }
        mSectionContentAdapter.refreshDownloadStatus();
        return mSectionContentAdapter;
    }

    protected void initDeviceInfo()
    {
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        XiMaoBTManager.getInstance(mCon).getDownloadModule().setOnBluetoothDownloadStatusListener(this);
    }

    public void onClick(View view)
    {
        boolean flag1 = true;
        view.getId();
        JVM INSTR lookupswitch 2: default 32
    //                   2131362504: 65
    //                   2131362513: 39;
           goto _L1 _L2 _L3
_L2:
        break MISSING_BLOCK_LABEL_65;
_L1:
        boolean flag = false;
_L4:
        List list;
        if (flag)
        {
            return;
        } else
        {
            super.onClick(view);
            return;
        }
_L3:
        list = mSectionContentAdapter.getCheckedTracks();
        flag = flag1;
        if (list != null)
        {
            showDirectionChoose(list);
            flag = flag1;
        }
          goto _L4
        goBackFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment);
        CommonTFListSubMainFragment.showDownloading = true;
        goBackFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoMainFragmentNew);
        startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/ximao/XiMaoDownloadTaskFragment, null);
        flag = flag1;
          goto _L4
    }

    public void onDestroy()
    {
        super.onDestroy();
        XiMaoBTManager.getInstance(mCon).getDownloadModule().removeOnBluetoothDownloadStatusListener(this);
    }

    public void onStatusChanged()
    {
        getActivity().runOnUiThread(new _cls2());
    }

    protected void updateDownloadingCount(int i)
    {
        if (XiMaoBTManager.getInstance(mCon).getDownloadModule().getDownloadTasks() != null)
        {
            i = XiMaoBTManager.getInstance(mCon).getDownloadModule().getDownloadTasks().size();
        } else
        {
            i = 0;
        }
        if (mSectionContentAdapter != null)
        {
            mSectionContentAdapter.refreshDownloadStatus();
        }
        if (i <= 0)
        {
            mDownloadingCount.setVisibility(4);
            return;
        } else
        {
            mDownloadingCount.setText(String.valueOf(i));
            mDownloadingCount.setVisibility(0);
            return;
        }
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final XiMaoDeviceAlbumSectionDownloadFragment this$0;
        final List val$tasks;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            mDeviceMenuDialog.cancel();
            adapterview = XiMaoBTManager.getInstance(mCon).getDownloadModule();
            if (tasks != null && tasks.size() >= 1)
            {
                Logger.d("test", (new StringBuilder()).append("#2").append(((com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)tasks.get(0)).getTitle()).toString());
            }
            adapterview.downloadAAC(tasks, i + 1);
            updateDownloadingCount(0);
        }

        _cls1()
        {
            this$0 = XiMaoDeviceAlbumSectionDownloadFragment.this;
            tasks = list;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final XiMaoDeviceAlbumSectionDownloadFragment this$0;

        public void run()
        {
            updateDownloadingCount(0);
        }

        _cls2()
        {
            this$0 = XiMaoDeviceAlbumSectionDownloadFragment.this;
            super();
        }
    }

}
