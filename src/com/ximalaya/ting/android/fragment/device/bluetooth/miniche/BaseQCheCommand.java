// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import com.ximalaya.ting.android.fragment.device.bluetooth.ICommand;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.miniche:
//            BtDeviceType

public class BaseQCheCommand
    implements ICommand
{

    private int result;
    private Object t;
    private int type;

    public BaseQCheCommand()
    {
    }

    public BtDeviceType getDeviceType()
    {
        return BtDeviceType.qsuicheting;
    }

    public int getResult()
    {
        return result;
    }

    public Object getT()
    {
        return t;
    }

    public int getType()
    {
        return type;
    }

    public void setResult(int i)
    {
        result = i;
    }

    public void setT(Object obj)
    {
        t = obj;
    }

    public void setType(int i)
    {
        type = i;
    }
}
