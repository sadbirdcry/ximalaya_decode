// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;

import android.bluetooth.BluetoothDevice;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            BaseBluetoothCallBack, BluetoothManager

class ck extends BaseBluetoothCallBack
{

    final BluetoothManager this$0;
    final BaseBluetoothCallBack val$callback;
    final boolean val$isNeedPlayStatus;
    final boolean val$isTrySppConn;

    public void onFailed()
    {
        if (val$callback != null)
        {
            val$callback.onFailed();
        }
    }

    public void onSuccess(ActionModel actionmodel)
    {
        actionmodel = (BluetoothDevice)actionmodel.result.get("device");
        doInit(checkType(actionmodel), val$isTrySppConn, val$isNeedPlayStatus, actionmodel);
        if (val$callback != null)
        {
            val$callback.onSuccess(null);
        }
    }

    ck()
    {
        this$0 = final_bluetoothmanager;
        val$isTrySppConn = flag;
        val$isNeedPlayStatus = flag1;
        val$callback = BaseBluetoothCallBack.this;
        super();
    }
}
