// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.ximao;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.bluetooth.BaseDownloadModule;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.ximao:
//            XiMaoDownloadHandler, XiMaoDownloadTask

public class XiMaoDownloadModule extends BaseDownloadModule
{

    private XiMaoDownloadHandler mHandler;

    public XiMaoDownloadModule(Context context)
    {
        super(context);
        mHandler = new XiMaoDownloadHandler(context);
    }

    public void cleanup()
    {
        Logger.d("ximao", "Module cleanup IN");
        if (mHandler != null)
        {
            mHandler.cleanup();
        }
    }

    public void clearDownloadTask()
    {
        if (mHandler != null)
        {
            mHandler.clearDownloadTask();
        }
    }

    public void download(DownloadTask downloadtask, int i)
    {
        downloadtask = new XiMaoDownloadTask(downloadtask, i);
        mHandler.addDownloadTask(downloadtask);
        mHandler.startDownloadTask();
    }

    public void download(List list, int i)
    {
        for (int j = 0; j < list.size(); j++)
        {
            download((DownloadTask)list.get(j), i);
        }

    }

    public void downloadAAC(List list, int i)
    {
        int j = 0;
        if (list != null && list.size() >= 1)
        {
            Logger.d("test", (new StringBuilder()).append("#3").append(((com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)list.get(0)).getTitle()).toString());
        }
        for (list = ModelHelper.toDownloadTaskList(list); j < list.size(); j++)
        {
            download((DownloadTask)list.get(j), i);
        }

    }

    public List getDownloadTasks()
    {
        return mHandler.getUnfinishedDownlaodTasks();
    }

    public void onPacket(byte abyte0[])
    {
        if (mHandler == null)
        {
            throw new RuntimeException("mDownloadHandler is null");
        } else
        {
            mHandler.onPacket(abyte0);
            return;
        }
    }

    public void removeDownloadTask(int i)
    {
        if (mHandler != null)
        {
            mHandler.removeDownloadTask((XiMaoDownloadTask)getDownloadTasks().get(i));
        }
    }

    public void removeDownloadTask(XiMaoDownloadTask ximaodownloadtask)
    {
        if (mHandler != null)
        {
            mHandler.removeDownloadTask(ximaodownloadtask);
        }
    }

    public void removeOnBluetoothDownloadStatusListener(XiMaoDownloadHandler.OnBluetoothDownloadStatusListener onbluetoothdownloadstatuslistener)
    {
        mHandler.removeOnBluetoothDownloadStatusListener(onbluetoothdownloadstatuslistener);
    }

    public void setOnBluetoothDownloadStatusListener(XiMaoDownloadHandler.OnBluetoothDownloadStatusListener onbluetoothdownloadstatuslistener)
    {
        mHandler.setOnBluetoothDownloadStatusListener(onbluetoothdownloadstatuslistener);
    }
}
