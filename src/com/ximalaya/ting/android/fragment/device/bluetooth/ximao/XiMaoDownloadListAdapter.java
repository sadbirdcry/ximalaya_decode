// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.ximao;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.model.download.DownloadTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.ximao:
//            XiMaoDownloadModule, XiMaoDownloadTask

public class XiMaoDownloadListAdapter extends BaseAdapter
{
    class ViewHolder
    {

        TextView mContextLength;
        RelativeLayout mDeleteTask;
        TextView mLength;
        TextView mLine;
        ImageView mStatusImage;
        TextView mStatusName;
        TextView mTaskName;
        final XiMaoDownloadListAdapter this$0;

        ViewHolder()
        {
            this$0 = XiMaoDownloadListAdapter.this;
            super();
        }
    }


    private Context mContext;
    private LayoutInflater mInflater;

    public XiMaoDownloadListAdapter(Context context)
    {
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    public int getCount()
    {
        if (XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks() == null)
        {
            return 0;
        } else
        {
            return XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks().size();
        }
    }

    public Object getItem(int i)
    {
        return XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks().get(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
        View view1;
        if (view == null)
        {
            view = new ViewHolder();
            view1 = mInflater.inflate(0x7f03011f, viewgroup, false);
            view.mTaskName = (TextView)view1.findViewById(0x7f0a0486);
            view.mStatusName = (TextView)view1.findViewById(0x7f0a0221);
            view.mStatusImage = (ImageView)view1.findViewById(0x7f0a0220);
            view.mDeleteTask = (RelativeLayout)view1.findViewById(0x7f0a0485);
            view.mLine = (TextView)view1.findViewById(0x7f0a0319);
            view.mContextLength = (TextView)view1.findViewById(0x7f0a0489);
            view.mLength = (TextView)view1.findViewById(0x7f0a0488);
            view1.setTag(view);
            viewgroup = view;
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
            view1 = view;
        }
        ((ViewHolder) (viewgroup)).mContextLength.setVisibility(4);
        ((ViewHolder) (viewgroup)).mLength.setVisibility(4);
        ((ViewHolder) (viewgroup)).mLine.setVisibility(4);
        ((ViewHolder) (viewgroup)).mTaskName.setText(((XiMaoDownloadTask)XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks().get(position)).getDownloadTask().title);
        ((XiMaoDownloadTask)XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks().get(position)).getNowState();
        JVM INSTR lookupswitch 2: default 244
    //                   0: 363
    //                   4: 369;
           goto _L1 _L2 _L3
_L1:
        ((ViewHolder) (viewgroup)).mStatusName.setVisibility(0);
        ((ViewHolder) (viewgroup)).mLength.setVisibility(0);
        ((ViewHolder) (viewgroup)).mLength.setText((new StringBuilder()).append(((XiMaoDownloadTask)XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks().get(position)).getPercentage()).append("%").toString());
        view = "\u6B63\u5728\u4F20\u8F93\u6570\u636E";
        ((ViewHolder) (viewgroup)).mStatusName.setText("\u6B63\u5728\u4F20\u8F93\u6570\u636E");
_L5:
        ((ViewHolder) (viewgroup)).mStatusName.setText(view);
        ((ViewHolder) (viewgroup)).mDeleteTask.setOnClickListener(new _cls1());
        return view1;
_L2:
        view = "\u7B49\u5F85\u4E0B\u8F7D";
        continue; /* Loop/switch isn't completed */
_L3:
        view = "\u4E0B\u8F7D\u5B8C\u6210";
        if (true) goto _L5; else goto _L4
_L4:
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final XiMaoDownloadListAdapter this$0;
        final int val$position;

        public void onClick(View view)
        {
            class _cls1
                implements Runnable
            {

                final _cls1 this$1;

                public void run()
                {
                    class _cls1
                        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                    {

                        final _cls1 this$2;

                        public void onExecute()
                        {
                            XiMaoBTManager.getInstance(mContext).getDownloadModule().removeDownloadTask(position);
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    (new DialogBuilder(MyApplication.a())).setMessage((new StringBuilder()).append("\u786E\u5B9A\u5220\u9664\u4EFB\u52A1").append(((XiMaoDownloadTask)XiMaoBTManager.getInstance(mContext).getDownloadModule().getDownloadTasks().get(position)).getDownloadTask().title).append("\uFF1F").toString()).setOkBtn(new _cls1()).showConfirm();
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            MyApplication.a().runOnUiThread(new _cls1());
        }

        _cls1()
        {
            this$0 = XiMaoDownloadListAdapter.this;
            position = i;
            super();
        }
    }

}
