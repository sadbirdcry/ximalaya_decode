// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.BaseBtController;
import com.ximalaya.ting.android.fragment.device.bluetooth.BaseShutDownPlayModule;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.ICommand;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.miniche:
//            BaseQCheCommand, QCheController, QCheShutDownPlayModule, QCheFmModule

public class QCheTingSettingFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.OnBtConnListener, com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.OnGetCommand
{

    public static final String TAG = com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheTingSettingFragment.getSimpleName();
    BaseQCheCommand baseQCheCommand;
    boolean isOnTouch;
    private ImageView mBackImg;
    private QCheController mQCheController;
    private QCheFmModule mQCheFmModule;
    private TextView mSetting1Info;
    private SwitchButton mSetting1Switch;
    private RelativeLayout mSetting2;
    private TextView mSetting2Info;
    private SwitchButton mSetting2Switch;
    private RelativeLayout mSetting3;
    private TextView mSetting3Info;
    private SwitchButton mSetting3Switch;
    private RelativeLayout mSetting4;
    private TextView mSetting4Info;
    private SwitchButton mSetting4Switch;
    private QCheShutDownPlayModule qCheShutDownPlayModule;
    private TextView top_tv;

    public QCheTingSettingFragment()
    {
        baseQCheCommand = new BaseQCheCommand();
        isOnTouch = false;
    }

    private void initView()
    {
        top_tv = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        top_tv.setText("\u8BBE\u7F6E");
        mBackImg.setOnClickListener(this);
        mSetting1Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a02fe);
        mSetting1Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a02ff);
        mSetting2Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a030d);
        mSetting2 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a030a);
        mSetting2Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a030e);
        mSetting4Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a0303);
        mSetting4 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a0300);
        mSetting4Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0304);
        mSetting3Switch = (SwitchButton)fragmentBaseContainerView.findViewById(0x7f0a0308);
        mSetting3 = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a0305);
        mSetting3Info = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0309);
        mSetting2.setOnClickListener(this);
        mSetting2Switch.setVisibility(8);
        mSetting2Info.setVisibility(0);
        mSetting4Info.setVisibility(8);
        mSetting4Switch.setVisibility(0);
        if (QCheController.isAutoPlaying(mCon))
        {
            mSetting4Switch.setChecked(true);
        } else
        {
            mSetting4Switch.setChecked(false);
        }
        mSetting4Switch.setOnCheckedChangeListener(new _cls1());
        mSetting1Info.setVisibility(8);
        mSetting1Switch.setVisibility(0);
        if (mQCheController.isWarning())
        {
            mSetting1Switch.setChecked(true);
        } else
        {
            mSetting1Switch.setChecked(false);
        }
        mSetting1Switch.setOnCheckedChangeListener(new _cls2());
        mSetting3Info.setVisibility(8);
        mSetting3Switch.setVisibility(0);
        if (qCheShutDownPlayModule.isContinueing(mCon))
        {
            mSetting3Switch.setChecked(true);
        } else
        {
            mSetting3Switch.setChecked(false);
        }
        mSetting3Switch.setOnCheckedChangeListener(new _cls3());
    }

    public void init()
    {
    }

    public boolean onA2dpConn()
    {
        return false;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mQCheController = new QCheController(getActivity());
        mQCheFmModule = new QCheFmModule(getActivity());
        mQCheFmModule.switchStateQuery("Voice_state");
        mQCheFmModule.switchStateQuery("Autoplay_state");
        qCheShutDownPlayModule = (QCheShutDownPlayModule)BluetoothManager.getInstance(mCon).getBtDeviceController().getModule(BaseShutDownPlayModule.NAME);
        init();
        initView();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362570: 
            QCheController.goIntroduction(getActivity());
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300c4, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onGetCommand(ICommand icommand)
    {
        baseQCheCommand = (BaseQCheCommand)icommand;
        baseQCheCommand.getType();
        JVM INSTR tableswitch 5 5: default 32
    //                   5 33;
           goto _L1 _L2
_L1:
        return;
_L2:
        switch (baseQCheCommand.getResult())
        {
        default:
            return;

        case 6: // '\006'
            continue; /* Loop/switch isn't completed */

        case 7: // '\007'
            mQCheFmModule.switchStateQuery("Voice_state");
            mQCheFmModule.switchStateQuery("Autoplay_state");
            break;
        }
        break; /* Loop/switch isn't completed */
        if (true) goto _L1; else goto _L3
_L3:
    }

    public void onResume()
    {
        super.onResume();
        BluetoothManager.getInstance(mCon).setBtConnListener(this);
        BluetoothManager.getInstance(mCon).setmOnGetCommands(this);
    }

    public boolean onSppConn(com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE btstate)
    {
        boolean flag;
        boolean flag1;
        flag1 = true;
        flag = flag1;
        static class _cls5
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE = new int[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.CONNECTED.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.DISCONNECTED.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.TIMEOUT.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.FAILED.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls5..SwitchMap.com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE[btstate.ordinal()];
        JVM INSTR tableswitch 1 4: default 44
    //                   1 46
    //                   2 48
    //                   3 44
    //                   4 44;
           goto _L1 _L2 _L3 _L1 _L1
_L2:
        break; /* Loop/switch isn't completed */
_L1:
        flag = false;
_L5:
        return flag;
_L3:
        flag = flag1;
        if (getActivity() != null)
        {
            getActivity().runOnUiThread(new _cls4());
            return true;
        }
        if (true) goto _L5; else goto _L4
_L4:
    }

    public void onStop()
    {
        super.onStop();
        BluetoothManager.getInstance(mCon).removeBtConnListener(this);
        BluetoothManager.getInstance(mCon).removeOnGetCommands(this);
    }

    public void showDebugToast(String s)
    {
        CustomToast.showToast(mCon, s, 0);
    }

    public void showToast(String s)
    {
        CustomToast.showToast(mCon, s, 0);
    }






    private class _cls1
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final QCheTingSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            if (flag)
            {
                mQCheFmModule.switchControl("Autoplay_on");
                QCheController.setAutoPlaying(mCon, true);
                Logger.d(QCheTingSettingFragment.TAG, "\u81EA\u52A8\u64AD\u653E\u6253\u5F00\u6307\u4EE4\u5DF2\u53D1\u9001");
                return;
            } else
            {
                mQCheFmModule.switchControl("Autoplay_off");
                QCheController.setAutoPlaying(mCon, false);
                Logger.d(QCheTingSettingFragment.TAG, "\u81EA\u52A8\u64AD\u653E\u5173\u95ED\u6307\u4EE4\u5DF2\u53D1\u9001");
                return;
            }
        }

        _cls1()
        {
            this$0 = QCheTingSettingFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final QCheTingSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            if (flag)
            {
                mQCheFmModule.switchControl("Voice_on");
                mQCheController.setWarning(true);
                Logger.d(QCheTingSettingFragment.TAG, "\u6240\u6709\u8BED\u97F3\u63D0\u793A\u6253\u5F00\u6307\u4EE4\u5DF2\u53D1\u9001");
                return;
            } else
            {
                mQCheFmModule.switchControl("Voice_off");
                mQCheController.setWarning(false);
                Logger.d(QCheTingSettingFragment.TAG, "\u6240\u6709\u8BED\u97F3\u63D0\u793A\u5173\u95ED\u6307\u4EE4\u5DF2\u53D1\u9001");
                return;
            }
        }

        _cls2()
        {
            this$0 = QCheTingSettingFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final QCheTingSettingFragment this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            if (flag)
            {
                qCheShutDownPlayModule.setContinueing(mCon, true);
                mSetting3Switch.setChecked(true);
                Logger.d(QCheTingSettingFragment.TAG, "\u84DD\u7259\u65AD\u5F00\u540E\u7EE7\u7EED\u64AD\u653E\u5F00\u5173\u5F00\u542F");
                return;
            } else
            {
                qCheShutDownPlayModule.setContinueing(mCon, false);
                mSetting3Switch.setChecked(false);
                Logger.d(QCheTingSettingFragment.TAG, "\u84DD\u7259\u65AD\u5F00\u540E\u7EE7\u7EED\u64AD\u653E\u5F00\u5173\u5173\u95ED");
                return;
            }
        }

        _cls3()
        {
            this$0 = QCheTingSettingFragment.this;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final QCheTingSettingFragment this$0;

        public void run()
        {
            goBackFragment(com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheTingIntroFragment);
        }

        _cls4()
        {
            this$0 = QCheTingSettingFragment.this;
            super();
        }
    }

}
