// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import android.content.Context;
import com.ximalaya.ting.android.fragment.device.bluetooth.BaseShutDownPlayModule;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.util.Logger;

public class QCheShutDownPlayModule extends BaseShutDownPlayModule
{

    private Context mContext;

    public QCheShutDownPlayModule()
    {
    }

    public boolean isContinue()
    {
        return isContinueing(mContext);
    }

    public boolean isContinueing(Context context)
    {
        boolean flag = SharedPreferencesUtil.getInstance(context).getBoolean("isContinue", true);
        if (flag)
        {
            Logger.d(TAG, "\u65AD\u5F00\u84DD\u7259\u7EE7\u7EED\u64AD\u653E");
            return flag;
        } else
        {
            Logger.d(TAG, "\u65AD\u5F00\u84DD\u7259\u505C\u6B62\u64AD\u653E");
            return flag;
        }
    }

    public void setContinueing(Context context, boolean flag)
    {
        mContext = context;
        SharedPreferencesUtil.getInstance(context).saveBoolean("isContinue", flag);
    }
}
