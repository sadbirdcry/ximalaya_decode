// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;

import android.bluetooth.BluetoothDevice;

public class MyBluetoothDevice
{

    BluetoothDevice mNowDevice;
    com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType mNowType;

    public MyBluetoothDevice(BluetoothDevice bluetoothdevice)
    {
        mNowDevice = bluetoothdevice;
    }

    public String getAddr()
    {
        return mNowDevice.getAddress();
    }

    public BluetoothDevice getBluetoothDevice()
    {
        return mNowDevice;
    }

    public com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getNowType()
    {
        return mNowType;
    }

    public void setNowType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype)
    {
        mNowType = devicetype;
    }
}
