// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;

import android.a.a;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.IBinder;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.che.CheController;
import com.ximalaya.ting.android.fragment.device.bluetooth.miniche.QCheController;
import com.ximalaya.ting.android.fragment.device.bluetooth.ximao.XiMaoController;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            MyBluetoothDevice, BtSppConnector, OtherController, BaseBluetoothCallBack, 
//            BluetoothUtil, BaseBtController, BtPackage, ICommand

public class BluetoothManager
{
    public static final class BtSTATE extends Enum
    {

        private static final BtSTATE $VALUES[];
        public static final BtSTATE CONNECTED;
        public static final BtSTATE CONNECTING;
        public static final BtSTATE DISCONNECTED;
        public static final BtSTATE FAILED;
        public static final BtSTATE START;
        public static final BtSTATE TIMEOUT;

        public static BtSTATE valueOf(String s)
        {
            return (BtSTATE)Enum.valueOf(com/ximalaya/ting/android/fragment/device/bluetooth/BluetoothManager$BtSTATE, s);
        }

        public static BtSTATE[] values()
        {
            return (BtSTATE[])$VALUES.clone();
        }

        static 
        {
            START = new BtSTATE("START", 0);
            CONNECTING = new BtSTATE("CONNECTING", 1);
            CONNECTED = new BtSTATE("CONNECTED", 2);
            DISCONNECTED = new BtSTATE("DISCONNECTED", 3);
            FAILED = new BtSTATE("FAILED", 4);
            TIMEOUT = new BtSTATE("TIMEOUT", 5);
            $VALUES = (new BtSTATE[] {
                START, CONNECTING, CONNECTED, DISCONNECTED, FAILED, TIMEOUT
            });
        }

        private BtSTATE(String s, int i)
        {
            super(s, i);
        }
    }

    public static interface OnBtConnListener
    {

        public abstract boolean onA2dpConn();

        public abstract boolean onSppConn(BtSTATE btstate);
    }

    public static interface OnGetCommand
    {

        public abstract void onGetCommand(ICommand icommand);
    }


    public static final String QCHETING = "00:58:51";
    public static final String TAG = com/ximalaya/ting/android/fragment/device/bluetooth/BluetoothManager.getSimpleName();
    private static BluetoothManager instance;
    private OnBtConnListener iOnBtConnListener;
    private boolean isNeedContinuePlay;
    private BluetoothAdapter mAdapter;
    private BluetoothProfile mBluetoothProfile;
    private BaseBtController mBtDeviceController;
    private BtSppConnector mBtSppConnector;
    private Context mContext;
    private com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType mDeviceType;
    private MyBluetoothDevice mMyBluetoothDevice;
    private BtSTATE mNowState;
    private List mOnBtConnListeners;
    private List mOnGetCommands;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    BtSppConnector.BTSppConnReceiver receiver;

    public BluetoothManager(Context context)
    {
        mBluetoothProfile = null;
        isNeedContinuePlay = false;
        receiver = new _cls4();
        mContext = context;
    }

    public static BluetoothManager getInstance(Context context)
    {
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/fragment/device/bluetooth/BluetoothManager;
        JVM INSTR monitorenter ;
        if (instance == null)
        {
            instance = new BluetoothManager(context.getApplicationContext());
        }
        com/ximalaya/ting/android/fragment/device/bluetooth/BluetoothManager;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        context;
        com/ximalaya/ting/android/fragment/device/bluetooth/BluetoothManager;
        JVM INSTR monitorexit ;
        throw context;
    }

    public static boolean isSameDevice(MyBluetoothDevice mybluetoothdevice, MyBluetoothDevice mybluetoothdevice1)
    {
        while (mybluetoothdevice == null || mybluetoothdevice1 == null || !mybluetoothdevice.getAddr().equals(mybluetoothdevice1.getAddr())) 
        {
            return false;
        }
        return true;
    }

    private void showWarning(final String msg)
    {
        if (MyApplication.a() == null)
        {
            return;
        } else
        {
            MyApplication.a().runOnUiThread(new _cls5());
            return;
        }
    }

    private void unRegisterPlayListener()
    {
        if (mOnPlayerStatusUpdateListener != null && LocalMediaService.getInstance() != null)
        {
            LocalMediaService.getInstance().removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
            mOnPlayerStatusUpdateListener = null;
        }
    }

    private void write(byte abyte0[], int i)
    {
        if (mBtSppConnector != null && isSppConn(mDeviceType))
        {
            mBtSppConnector.sendPacket(abyte0, i);
        }
    }

    protected void afterGetProxy(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype, boolean flag, boolean flag1, BluetoothProfile bluetoothprofile)
    {
        bluetoothprofile = bluetoothprofile.getConnectedDevices();
        Logger.d("QSuiCheTing", (new StringBuilder()).append("size").append(bluetoothprofile.size()).toString());
        if (bluetoothprofile != null && bluetoothprofile.size() != 0)
        {
            doInit(devicetype, flag, flag1, (BluetoothDevice)bluetoothprofile.get(0));
        }
    }

    public void afterInit(BluetoothDevice bluetoothdevice)
    {
        mNowState = BtSTATE.START;
        if (bluetoothdevice == null)
        {
            mNowState = BtSTATE.DISCONNECTED;
        } else
        {
            mNowState = BtSTATE.CONNECTED;
            if (iOnBtConnListener != null)
            {
                iOnBtConnListener.onA2dpConn();
                return;
            }
        }
    }

    public com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType checkType(BluetoothDevice bluetoothdevice)
    {
        if (bluetoothdevice.getAddress().startsWith("00:58:51"))
        {
            setBtDeviceController(new QCheController(mContext));
            return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting;
        }
        if (XiMaoBTManager.isXimao(bluetoothdevice))
        {
            setBtDeviceController(new XiMaoController(mContext));
            return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.ximao;
        }
        if (MyDeviceManager.isSuichetingDevice(bluetoothdevice))
        {
            setBtDeviceController(new CheController(mContext));
            return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.suicheting;
        } else
        {
            setBtDeviceController(new OtherController(mContext));
            return com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.unReg;
        }
    }

    public void cleanup()
    {
        Logger.d("spp", "BluetoothManager cleanup IN");
        if (mBluetoothProfile != null && mAdapter != null)
        {
            Logger.d("QSuiCheTing", "unbind");
            mAdapter.closeProfileProxy(2, mBluetoothProfile);
            mBluetoothProfile = null;
        }
        if (mBtSppConnector != null)
        {
            mBtSppConnector.cancel();
            mBtSppConnector = null;
        }
        mBluetoothProfile = null;
        mBtDeviceController = null;
        mDeviceType = null;
        mMyBluetoothDevice = null;
        mNowState = BtSTATE.START;
        if (mOnBtConnListeners != null)
        {
            mOnBtConnListeners.clear();
        }
    }

    public void connectSpp()
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag;
        Logger.d("spp", "connectSpp IN");
        if (mBtSppConnector == null)
        {
            mBtSppConnector = new BtSppConnector(mContext, mMyBluetoothDevice);
        }
        flag = mBtSppConnector.isSppConnRunning();
        if (!flag) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        Logger.d("spp", (new StringBuilder()).append(" new mBtSppConnector").append(mBtSppConnector).toString());
        mBtSppConnector.setBtConnReceiaver(receiver);
        Logger.d("QSuiCheTing", "connectSPP");
        mBtSppConnector.startSppThread();
        if (true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    protected void doInit(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype, boolean flag, boolean flag1, BluetoothDevice bluetoothdevice)
    {
        if (checkType(bluetoothdevice) == devicetype)
        {
            mDeviceType = devicetype;
            mMyBluetoothDevice = new MyBluetoothDevice(bluetoothdevice);
            mMyBluetoothDevice.setNowType(devicetype);
            afterInit(bluetoothdevice);
            if (flag1)
            {
                registerPlayListener();
            }
            if (flag)
            {
                connectSpp();
            }
        }
    }

    public BluetoothProfile getBluetoothProfile()
    {
        return mBluetoothProfile;
    }

    public BaseBtController getBtDeviceController()
    {
        return mBtDeviceController;
    }

    public MyBluetoothDevice getMyBluetoothDevice()
    {
        return mMyBluetoothDevice;
    }

    public void init(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype, BluetoothDevice bluetoothdevice, boolean flag, boolean flag1)
    {
        int i = android.os.Build.VERSION.SDK_INT;
        if (MyApplication.a() != null)
        {
            MyApplication.a().runOnUiThread(new _cls1());
        }
        if (i < 11)
        {
            initInLowerAPI(devicetype, bluetoothdevice, flag, flag1);
            return;
        } else
        {
            initInHigherAPI(devicetype, bluetoothdevice, flag, flag1);
            return;
        }
    }

    protected void initInHigherAPI(final com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType deviceType, BluetoothDevice bluetoothdevice, final boolean isTrySppConn, final boolean isNeedPlayStatus)
    {
        if (bluetoothdevice != null)
        {
            doInit(deviceType, isTrySppConn, isNeedPlayStatus, bluetoothdevice);
        } else
        {
            if (mBluetoothProfile != null && mAdapter != null)
            {
                Logger.d("QSuiCheTing", "unbind");
                afterGetProxy(deviceType, isTrySppConn, isNeedPlayStatus, mBluetoothProfile);
            }
            if (mAdapter != null && mAdapter.isEnabled() && mAdapter.getProfileConnectionState(2) == 2)
            {
                bluetoothdevice = MyApplication.b();
                mAdapter.getProfileProxy(bluetoothdevice, new _cls2(), 2);
                return;
            }
        }
    }

    protected void initInLowerAPI(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype, BluetoothDevice bluetoothdevice, boolean flag, boolean flag1)
    {
        try
        {
            bluetoothdevice = (IBinder)Class.forName("android.os.ServiceManager").getDeclaredMethod("getService", new Class[] {
                java/lang/String
            }).invoke(null, new Object[] {
                "bluetooth_a2dp"
            });
            Method method = Class.forName("android.a.a").getDeclaredClasses()[0].getDeclaredMethod("asInterface", new Class[] {
                android/os/IBinder
            });
            method.setAccessible(true);
            bluetoothdevice = (BluetoothDevice[])(BluetoothDevice[])((a)method.invoke(null, new Object[] {
                bluetoothdevice
            })).getClass().getDeclaredMethod("getConnectedSinks", new Class[0]).invoke((a)method.invoke(null, new Object[] {
                bluetoothdevice
            }), new Object[0]);
        }
        // Misplaced declaration of an exception variable
        catch (com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype)
        {
            devicetype.printStackTrace();
            Logger.e(TAG, (new StringBuilder()).append("error:").append(devicetype.toString()).toString());
            return;
        }
        if (bluetoothdevice == null)
        {
            break MISSING_BLOCK_LABEL_155;
        }
        if (bluetoothdevice.length != 0)
        {
            doInit(devicetype, flag, flag1, bluetoothdevice[0]);
        }
    }

    public void initInUi()
    {
        if (BluetoothAdapter.getDefaultAdapter() != null)
        {
            mAdapter = BluetoothAdapter.getDefaultAdapter();
        }
    }

    public void isA2dpConn(final BaseBluetoothCallBack callback, final boolean isTrySppConn, final boolean isNeedPlayStatus)
    {
        if (mMyBluetoothDevice != null)
        {
            if (callback != null)
            {
                callback.onSuccess(null);
            }
            return;
        } else
        {
            BluetoothUtil.getConnectedDevice(mContext, new _cls3());
            return;
        }
    }

    public boolean isA2dpConnSimple()
    {
        return mMyBluetoothDevice != null;
    }

    public boolean isNeedContinuePlay()
    {
        return isNeedContinuePlay;
    }

    public boolean isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype)
    {
        Logger.d(TAG, (new StringBuilder()).append("spp thread onConnected").append(getInstance(mContext)).append(mBtSppConnector).toString());
        if (mDeviceType != devicetype)
        {
            Logger.d("spp", "OUT #1");
            return false;
        }
        if (mBtSppConnector == null)
        {
            Logger.d("spp", "OUT #2");
            return false;
        }
        if (mNowState == BtSTATE.CONNECTED && mBtSppConnector.isSppConnRunning())
        {
            return true;
        } else
        {
            Logger.d("spp", "OUT #3");
            return false;
        }
    }

    public void registerPlayListener()
    {
        if (mOnPlayerStatusUpdateListener != null)
        {
            Logger.d("suicheting", "mOnPlayerStatusUpdateListener return");
        } else
        {
            if (LocalMediaService.getInstance() == null)
            {
                Logger.d("suicheting", "LocalMediaService return");
                return;
            }
            mOnPlayerStatusUpdateListener = new _cls6();
            if (LocalMediaService.getInstance() != null)
            {
                Logger.d("suicheting", "setOnPlayerStatusUpdateListener");
                LocalMediaService.getInstance().setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
                return;
            }
        }
    }

    public void removeBtConnListener(OnBtConnListener onbtconnlistener)
    {
        if (mOnBtConnListeners == null)
        {
            return;
        } else
        {
            mOnBtConnListeners.remove(onbtconnlistener);
            return;
        }
    }

    public void removeOnGetCommands(OnGetCommand ongetcommand)
    {
        if (mOnGetCommands == null)
        {
            return;
        } else
        {
            mOnGetCommands.remove(ongetcommand);
            return;
        }
    }

    public void sendCommand(ICommand icommand)
    {
        if (mBtDeviceController != null)
        {
            icommand = mBtDeviceController.parseCommand(icommand);
            write(icommand.getPack(), icommand.getLength());
        }
    }

    public void setBluetoothProfile(BluetoothProfile bluetoothprofile)
    {
        mBluetoothProfile = bluetoothprofile;
    }

    public void setBtConnListener(OnBtConnListener onbtconnlistener)
    {
        if (mOnBtConnListeners == null)
        {
            mOnBtConnListeners = new ArrayList();
        }
        if (!mOnBtConnListeners.contains(onbtconnlistener))
        {
            mOnBtConnListeners.add(onbtconnlistener);
        }
    }

    public void setBtDeviceController(BaseBtController basebtcontroller)
    {
        mBtDeviceController = basebtcontroller;
    }

    public void setNeedContinuePlay(boolean flag)
    {
        isNeedContinuePlay = flag;
        if (flag)
        {
            (new Timer()).schedule(new _cls7(), 5000L);
        }
    }

    public void setmOnGetCommands(OnGetCommand ongetcommand)
    {
        if (mOnGetCommands == null)
        {
            mOnGetCommands = new ArrayList();
        }
        if (!mOnGetCommands.contains(ongetcommand))
        {
            mOnGetCommands.add(ongetcommand);
        }
    }



/*
    static BluetoothProfile access$002(BluetoothManager bluetoothmanager, BluetoothProfile bluetoothprofile)
    {
        bluetoothmanager.mBluetoothProfile = bluetoothprofile;
        return bluetoothprofile;
    }

*/








/*
    static boolean access$702(BluetoothManager bluetoothmanager, boolean flag)
    {
        bluetoothmanager.isNeedContinuePlay = flag;
        return flag;
    }

*/

    private class _cls4
        implements BtSppConnector.BTSppConnReceiver
    {

        final BluetoothManager this$0;

        public void onConnectFailed()
        {
        }

        public void onConnected()
        {
            if (mOnBtConnListeners != null)
            {
                for (Iterator iterator = mOnBtConnListeners.iterator(); iterator.hasNext(); ((OnBtConnListener)iterator.next()).onSppConn(BtSTATE.CONNECTED)) { }
            }
        }

        public void onDisconnected()
        {
            final String realName = mBtDeviceController.getRealName();
            Logger.d("QSuiCheTing", "onDisconnected IN");
            if (mOnBtConnListeners != null)
            {
                for (Iterator iterator = mOnBtConnListeners.iterator(); iterator.hasNext(); ((OnBtConnListener)iterator.next()).onSppConn(BtSTATE.DISCONNECTED)) { }
            }
            BaseShutDownPlayModule baseshutdownplaymodule = (BaseShutDownPlayModule)mBtDeviceController.getModule(BaseShutDownPlayModule.NAME);
            Logger.d(BluetoothManager.TAG, (new StringBuilder()).append("isContinue\uFF1A").append(baseshutdownplaymodule.isContinue()).toString());
            class _cls1
                implements Runnable
            {

                final _cls4 this$1;
                final String val$realName;

                public void run()
                {
                    String s = (new StringBuilder()).append("\u5DF2\u7ECF\u4E0E").append(realName).append("\u65AD\u5F00\u8FDE\u63A5").toString();
                    (new DialogBuilder(MyApplication.a())).setMessage(s).showWarning();
                }

                _cls1()
                {
                    this$1 = _cls4.this;
                    realName = s;
                    super();
                }
            }

            if (baseshutdownplaymodule != null && baseshutdownplaymodule.isContinue())
            {
                setNeedContinuePlay(true);
                BluetoothUtil.playContinue(baseshutdownplaymodule.isContinue());
            } else
            {
                BluetoothUtil.playContinue(false);
            }
            if (mBtDeviceController != null && mMyBluetoothDevice != null && MyApplication.a() != null)
            {
                MyApplication.a().runOnUiThread(new _cls1());
            }
            cleanup();
        }

        public void onPacket(byte abyte0[], int i)
        {
            abyte0 = mBtDeviceController.parseCommand(new BtPackage(abyte0, i));
            if (mOnGetCommands != null)
            {
                Iterator iterator = mOnGetCommands.iterator();
                while (iterator.hasNext()) 
                {
                    OnGetCommand ongetcommand = (OnGetCommand)iterator.next();
                    if (ongetcommand != null)
                    {
                        ongetcommand.onGetCommand(abyte0);
                    }
                }
            }
        }

        public void onReceived(byte abyte0[], int i)
        {
        }

        public void onSent(byte abyte0[], int i)
        {
        }

        public void onTimeout()
        {
            if (mOnBtConnListeners != null)
            {
                for (Iterator iterator = mOnBtConnListeners.iterator(); iterator.hasNext(); ((OnBtConnListener)iterator.next()).onSppConn(BtSTATE.TIMEOUT)) { }
            }
        }

        _cls4()
        {
            this$0 = BluetoothManager.this;
            super();
        }
    }


    private class _cls5
        implements Runnable
    {

        final BluetoothManager this$0;
        final String val$msg;

        public void run()
        {
            (new DialogBuilder(MyApplication.a())).setMessage(msg).showWarning();
        }

        _cls5()
        {
            this$0 = BluetoothManager.this;
            msg = s;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final BluetoothManager this$0;

        public void run()
        {
            initInUi();
        }

        _cls1()
        {
            this$0 = BluetoothManager.this;
            super();
        }
    }


    private class _cls2
        implements android.bluetooth.BluetoothProfile.ServiceListener
    {

        final BluetoothManager this$0;
        final com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType val$deviceType;
        final boolean val$isNeedPlayStatus;
        final boolean val$isTrySppConn;

        public void onServiceConnected(int i, BluetoothProfile bluetoothprofile)
        {
            Logger.d("QSuiCheTing", "bind Success");
            mBluetoothProfile = bluetoothprofile;
            afterGetProxy(deviceType, isTrySppConn, isNeedPlayStatus, bluetoothprofile);
        }

        public void onServiceDisconnected(int i)
        {
        }

        _cls2()
        {
            this$0 = BluetoothManager.this;
            deviceType = devicetype;
            isTrySppConn = flag;
            isNeedPlayStatus = flag1;
            super();
        }
    }


    private class _cls3 extends BaseBluetoothCallBack
    {

        final BluetoothManager this$0;
        final BaseBluetoothCallBack val$callback;
        final boolean val$isNeedPlayStatus;
        final boolean val$isTrySppConn;

        public void onFailed()
        {
            if (callback != null)
            {
                callback.onFailed();
            }
        }

        public void onSuccess(ActionModel actionmodel)
        {
            actionmodel = (BluetoothDevice)actionmodel.result.get("device");
            doInit(checkType(actionmodel), isTrySppConn, isNeedPlayStatus, actionmodel);
            if (callback != null)
            {
                callback.onSuccess(null);
            }
        }

        _cls3()
        {
            this$0 = BluetoothManager.this;
            isTrySppConn = flag;
            isNeedPlayStatus = flag1;
            callback = basebluetoothcallback;
            super();
        }
    }


    private class _cls6
        implements com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
    {

        final BluetoothManager this$0;

        public void onBufferUpdated(int i)
        {
        }

        public void onLogoPlayFinished()
        {
        }

        public void onPlayCompleted()
        {
        }

        public void onPlayPaused()
        {
        }

        public void onPlayProgressUpdate(int i, int j)
        {
        }

        public void onPlayStarted()
        {
            Logger.d("suicheting", "onPlayStarted IN");
            if (mMyBluetoothDevice == null || mMyBluetoothDevice.getNowType() != com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting || !MyDeviceManager.getInstance(mContext).isQSuiCheTingFirstUsed()) goto _L2; else goto _L1
_L1:
            MyDeviceManager.getInstance(mContext).setIsQSuiCheTingFirstUsed(false);
            MyDeviceManager.getInstance(mContext).shareSuiCheTing(5);
_L4:
            unRegisterPlayListener();
            return;
_L2:
            if (mMyBluetoothDevice != null && mMyBluetoothDevice.getNowType() == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.suicheting && MyDeviceManager.getInstance(mContext).isSuiCheTingFirstUsed())
            {
                MyDeviceManager.getInstance(mContext).setIsSuiCheTingFirstUsed(false);
                MyDeviceManager.getInstance(mContext).shareSuiCheTing(4);
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        public void onPlayerBuffering(boolean flag)
        {
        }

        public void onSoundPrepared(int i)
        {
        }

        public void onStartPlayLogo()
        {
        }

        _cls6()
        {
            this$0 = BluetoothManager.this;
            super();
        }
    }


    private class _cls7 extends TimerTask
    {

        final BluetoothManager this$0;

        public void run()
        {
            isNeedContinuePlay = false;
        }

        _cls7()
        {
            this$0 = BluetoothManager.this;
            super();
        }
    }

}
