// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.fragment.download:
//            BaseDownloadedSoundListAdapter

public abstract class BaseDownloadSoundsListForAlbumFragment extends BaseListFragment
    implements com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
{
    public static interface RemoveSounds
    {

        public abstract void remove();
    }


    public static String ALBUM_ID = "albumId";
    private long albumId;
    private List albumSoundlist;
    private List downloadTaskList;
    View listHeader;
    private TextView listInfoTV;
    private TextView orderTV;
    private ImageView retBtn;
    public BaseDownloadedSoundListAdapter soundsDownloadAdapter;
    private TextView top_view;

    public BaseDownloadSoundsListForAlbumFragment()
    {
        albumSoundlist = new ArrayList();
        downloadTaskList = new ArrayList();
    }

    private void doGetDownloadList()
    {
        if (getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            getActivity().runOnUiThread(new _cls3());
            return;
        }
    }

    private void findViews()
    {
        top_view = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        retBtn = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a007b);
    }

    private void initData()
    {
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========new DoGetDownloadList().myexec(); ");
        doGetDownloadList();
    }

    private void initViews()
    {
        fragmentBaseContainerView.findViewById(0x7f0a0717).setVisibility(4);
        listHeader = LayoutInflater.from(mCon).inflate(0x7f030103, mListView, false);
        android.widget.AbsListView.LayoutParams layoutparams = new android.widget.AbsListView.LayoutParams(-1, Utilities.dip2px(mCon, 44F));
        listHeader.setPadding(Utilities.dip2px(mCon, 20F), Utilities.dip2px(mCon, 13F), Utilities.dip2px(mCon, 15F), 0);
        listHeader.setLayoutParams(layoutparams);
        listInfoTV = (TextView)listHeader.findViewById(0x7f0a043d);
        orderTV = (TextView)listHeader.findViewById(0x7f0a0373);
        mListView.addHeaderView(listHeader);
        mListView.setAdapter(getAdapter(downloadTaskList));
        updateListInfoView();
        updateOrderView();
    }

    private void initViewsListener()
    {
        retBtn.setOnClickListener(new _cls1());
        orderTV.setOnClickListener(new _cls2());
    }

    private void orderAlbumSoundlist(int i)
    {
        if (albumSoundlist != null && albumSoundlist.size() > 0)
        {
            Object obj = new ArrayList();
            if (i == 1)
            {
                obj = albumSoundlist;
            } else
            {
                ((List) (obj)).addAll(albumSoundlist);
                Collections.reverse(((List) (obj)));
            }
            downloadTaskList.clear();
            downloadTaskList.addAll(((java.util.Collection) (obj)));
            soundsDownloadAdapter.notifyDataSetChanged();
            return;
        } else
        {
            doGetDownloadList();
            return;
        }
    }

    private void registerListener()
    {
        DownloadHandler downloadhandler = DownloadHandler.getInstance(mCon);
        if (downloadhandler != null)
        {
            downloadhandler.addDownloadListeners(this);
        }
    }

    private void unRegisterListener()
    {
        DownloadHandler downloadhandler = DownloadHandler.getInstance(mCon);
        if (downloadhandler != null)
        {
            downloadhandler.removeDownloadListeners(this);
        }
    }

    private void updateListInfoView()
    {
label0:
        {
            if (listInfoTV != null)
            {
                if (downloadTaskList != null && downloadTaskList.size() != 0)
                {
                    break label0;
                }
                listInfoTV.setText(getString(0x7f090095, new Object[] {
                    Integer.valueOf(0)
                }));
            }
            return;
        }
        listInfoTV.setText(getString(0x7f090095, new Object[] {
            Integer.valueOf(downloadTaskList.size())
        }));
    }

    private void updateOrderView()
    {
        if (orderTV == null)
        {
            return;
        }
        if (1 == SharedPreferencesUtil.getInstance(getActivity()).getInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), 1))
        {
            orderTV.setCompoundDrawablesWithIntrinsicBounds(0x7f02029c, 0, 0, 0);
            return;
        } else
        {
            orderTV.setCompoundDrawablesWithIntrinsicBounds(0x7f02029e, 0, 0, 0);
            return;
        }
    }

    public void delSound(long l)
    {
    }

    public abstract BaseDownloadedSoundListAdapter getAdapter(List list);

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            albumId = bundle.getLong(ALBUM_ID);
        }
        findViews();
        initViews();
        initViewsListener();
        registerListener();
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onActivityCreated ");
        initData();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onCreateView ");
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030078, viewgroup, false);
        mListView = (BounceListView)fragmentBaseContainerView.findViewById(0x7f0a0228);
        return fragmentBaseContainerView;
    }

    public void onDestroy()
    {
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onDestroy ");
        super.onDestroy();
    }

    public void onDestroyView()
    {
        unRegisterListener();
        super.onDestroyView();
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onDestroyView ");
    }

    public void onResume()
    {
        super.onResume();
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onResume ");
    }

    public void onSdcardStateChanged()
    {
        if (soundsDownloadAdapter != null)
        {
            soundsDownloadAdapter.notifyDataSetChanged();
        }
    }

    public void onTaskComplete()
    {
        doGetDownloadList();
    }

    public void onTaskDelete()
    {
        doGetDownloadList();
    }

    public void updateActionInfo()
    {
        doGetDownloadList();
    }

    public void updateDownloadInfo(int i)
    {
        doGetDownloadList();
    }









    private class _cls3
        implements Runnable
    {

        final BaseDownloadSoundsListForAlbumFragment this$0;

        public void run()
        {
            if (isAdded())
            {
                Object obj = DownloadHandler.getInstance(mCon);
                Logger.log("0814", (new StringBuilder()).append("====DownloadSoundsListForAlbumFragment========GET downloadTaskList: ").append(downloadTaskList.size()).toString());
                obj = ((DownloadHandler) (obj)).getSortedFinishedDownloadList();
                if (obj != null && ((List) (obj)).size() != 0)
                {
                    int i = SharedPreferencesUtil.getInstance(getActivity()).getInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), 1);
                    albumSoundlist.clear();
                    obj = ((List) (obj)).iterator();
                    do
                    {
                        if (!((Iterator) (obj)).hasNext())
                        {
                            break;
                        }
                        DownloadTask downloadtask = (DownloadTask)((Iterator) (obj)).next();
                        if (downloadtask.albumId == albumId)
                        {
                            albumSoundlist.add(downloadtask);
                        }
                    } while (true);
                    obj = new ArrayList();
                    if (i == -1)
                    {
                        ((List) (obj)).addAll(albumSoundlist);
                        Collections.reverse(((List) (obj)));
                    } else
                    {
                        obj = albumSoundlist;
                    }
                    downloadTaskList.clear();
                    downloadTaskList.addAll(((java.util.Collection) (obj)));
                    soundsDownloadAdapter.notifyDataSetChanged();
                    updateListInfoView();
                    updateOrderView();
                    if (albumId == 0L)
                    {
                        top_view.setText("\u672A\u547D\u540D\u4E13\u8F91");
                        return;
                    }
                    if (downloadTaskList != null && downloadTaskList.size() > 0)
                    {
                        top_view.setText(((DownloadTask)downloadTaskList.get(0)).albumName);
                        return;
                    }
                }
            }
        }

        _cls3()
        {
            this$0 = BaseDownloadSoundsListForAlbumFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final BaseDownloadSoundsListForAlbumFragment this$0;

        public void onClick(View view)
        {
            if (getActivity() != null)
            {
                ((MainTabActivity2)getActivity()).onBack();
            }
        }

        _cls1()
        {
            this$0 = BaseDownloadSoundsListForAlbumFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final BaseDownloadSoundsListForAlbumFragment this$0;

        public void onClick(View view)
        {
            int i = -1;
            if (albumSoundlist == null || albumSoundlist.size() <= 1) goto _L2; else goto _L1
_L1:
            int j;
            view = SharedPreferencesUtil.getInstance(getActivity());
            j = view.getInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), 1);
            j;
            JVM INSTR tableswitch -1 1: default 96
        //                       -1 147
        //                       0 96
        //                       1 114;
               goto _L3 _L4 _L3 _L5
_L3:
            i = j;
_L7:
            orderAlbumSoundlist(i);
            updateOrderView();
_L2:
            return;
_L5:
            view.saveInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), -1);
            continue; /* Loop/switch isn't completed */
_L4:
            view.saveInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), 1);
            i = 1;
            if (true) goto _L7; else goto _L6
_L6:
        }

        _cls2()
        {
            this$0 = BaseDownloadSoundsListForAlbumFragment.this;
            super();
        }
    }

}
