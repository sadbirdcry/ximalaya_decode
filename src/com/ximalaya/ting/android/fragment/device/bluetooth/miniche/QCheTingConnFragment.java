// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.library.util.Logger;
import java.util.Timer;

public class QCheTingConnFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.OnBtConnListener
{

    private boolean isStartConningTvAnim;
    private ImageView mBackImg;
    private TextView mBtDisConn;
    private ImageView mConnImg;
    private TextView mConnTipsTv;
    private RelativeLayout mConnecting;
    private LinearLayout mContentView;
    private RelativeLayout mFailedLayout;
    private Timer mTimer;
    private TextView mTopTv;

    public QCheTingConnFragment()
    {
        isStartConningTvAnim = false;
    }

    private void cancelConningTvAnim()
    {
        if (!isStartConningTvAnim)
        {
            return;
        } else
        {
            isStartConningTvAnim = false;
            mTimer.cancel();
            return;
        }
    }

    private void fresh2Connected()
    {
        Logger.d("spp", "fresh2Connected() IN");
        cancelConningTvAnim();
        toFmSenderFragment();
    }

    private void fresh2Connecting()
    {
        if (getActivity() != null)
        {
            getActivity().runOnUiThread(new _cls5());
        }
        startConningTvAnim();
    }

    private void fresh2Failed()
    {
        if (getActivity() != null)
        {
            getActivity().runOnUiThread(new _cls4());
        }
    }

    private void initView()
    {
        mBtDisConn = (TextView)mContentView.findViewById(0x7f0a071c);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mTopTv = (TextView)mContentView.findViewById(0x7f0a00ae);
        ((TextView)(TextView)mContentView.findViewById(0x7f0a00ae)).setText("\u968F\u8F66\u542CQ\u7248");
        mBtDisConn.setOnClickListener(this);
        mBackImg.setOnClickListener(this);
        mConnecting = (RelativeLayout)mContentView.findViewById(0x7f0a0374);
        mConnImg = (ImageView)mContentView.findViewById(0x7f0a0375);
        mConnTipsTv = (TextView)(TextView)mContentView.findViewById(0x7f0a0322);
        mFailedLayout = (RelativeLayout)mContentView.findViewById(0x7f0a0324);
        ((TextView)(TextView)mContentView.findViewById(0x7f0a0329)).setOnClickListener(this);
    }

    private void startConningTvAnim()
    {
        if (isStartConningTvAnim)
        {
            return;
        } else
        {
            mTimer = new Timer();
            isStartConningTvAnim = true;
            mTimer.schedule(new _cls1(), 500L, 500L);
            return;
        }
    }

    private void toConn()
    {
        if (BluetoothManager.getInstance(mCon).isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
        {
            fresh2Connected();
            return;
        } else
        {
            fresh2Connecting();
            return;
        }
    }

    private void toFmSenderFragment()
    {
        Logger.d("spp", "toFmSenderFragment IN");
        if (getActivity() != null)
        {
            getActivity().runOnUiThread(new _cls3());
        }
    }

    private void toGetConnedDevice()
    {
        (new Thread(new _cls2())).start();
    }

    public boolean onA2dpConn()
    {
        return false;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        XiMaoBTManager.getInstance(mCon).cancel();
        mActivity = getActivity();
        if (mActivity == null)
        {
            mActivity = getActivity();
        }
        if (mActivity == null)
        {
            mActivity = MyApplication.a();
        }
        if (mActivity == null)
        {
            return;
        }
        initView();
        if (BluetoothManager.getInstance(mCon).isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
        {
            toFmSenderFragment();
            return;
        } else
        {
            toConn();
            BluetoothManager.getInstance(mCon).connectSpp();
            return;
        }
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        case 2131363612: 
        default:
            return;

        case 2131363611: 
            getActivity().onBackPressed();
            return;

        case 2131362601: 
            toConn();
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (LinearLayout)layoutinflater.inflate(0x7f0300ca, viewgroup, false);
        return mContentView;
    }

    public void onDestroy()
    {
        cancelConningTvAnim();
        super.onDestroy();
    }

    public void onResume()
    {
        super.onResume();
        BluetoothManager.getInstance(mActivity).setBtConnListener(this);
        toGetConnedDevice();
    }

    public boolean onSppConn(com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE btstate)
    {
        boolean flag = true;
        static class _cls6
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE = new int[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.CONNECTED.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.DISCONNECTED.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.TIMEOUT.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$bluetooth$BluetoothManager$BtSTATE[com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE.FAILED.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls6..SwitchMap.com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE[btstate.ordinal()];
        JVM INSTR tableswitch 1 4: default 40
    //                   1 44
    //                   2 42
    //                   3 58
    //                   4 58;
           goto _L1 _L2 _L3 _L4 _L4
_L1:
        flag = false;
_L3:
        return flag;
_L2:
        Logger.d("spp", "onSppConn CONNECTED IN");
        toFmSenderFragment();
        return true;
_L4:
        fresh2Failed();
        if (true) goto _L1; else goto _L5
_L5:
    }





    private class _cls5
        implements Runnable
    {

        final QCheTingConnFragment this$0;

        public void run()
        {
            mConnecting.setVisibility(0);
            mFailedLayout.setVisibility(8);
        }

        _cls5()
        {
            this$0 = QCheTingConnFragment.this;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final QCheTingConnFragment this$0;

        public void run()
        {
            mConnecting.setVisibility(8);
            mFailedLayout.setVisibility(0);
        }

        _cls4()
        {
            this$0 = QCheTingConnFragment.this;
            super();
        }
    }


    private class _cls1 extends TimerTask
    {

        int i;
        final String strings[] = {
            "", ".", "..", "..."
        };
        final QCheTingConnFragment this$0;

        public void run()
        {
            class _cls1
                implements Runnable
            {

                final _cls1 this$1;

                public void run()
                {
                    _cls1 _lcls1 = _cls1.this;
                    _lcls1.i = _lcls1.i + 1;
                    mConnTipsTv.setText(strings[i % 4]);
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            getActivity().runOnUiThread(new _cls1());
        }

        _cls1()
        {
            this$0 = QCheTingConnFragment.this;
            super();
            i = 0;
        }
    }


    private class _cls3
        implements Runnable
    {

        final QCheTingConnFragment this$0;

        public void run()
        {
            finish();
            startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheFmSenderFragment, null);
        }

        _cls3()
        {
            this$0 = QCheTingConnFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final QCheTingConnFragment this$0;

        public void run()
        {
            startConningTvAnim();
        }

        _cls2()
        {
            this$0 = QCheTingConnFragment.this;
            super();
        }
    }

}
