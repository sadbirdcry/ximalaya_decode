// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth;


// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth:
//            IBtModule, ICommand

public abstract class BaseFmModule
    implements IBtModule
{

    public static final String NAME = com/ximalaya/ting/android/fragment/device/bluetooth/BaseFmModule.getSimpleName();
    public static final String TAG = com/ximalaya/ting/android/fragment/device/bluetooth/BaseFmModule.getSimpleName();

    public BaseFmModule()
    {
    }

    public abstract void frequencyQuery();

    public String getModuleName()
    {
        return NAME;
    }

    public abstract float parseFrequencyCommand(ICommand icommand);

    public abstract void setFrequency(float f);

}
