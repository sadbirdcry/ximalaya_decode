// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.ximao;

import android.content.Context;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.CustomToast;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.ximao:
//            XiMaoDownloadTask

public class XiMaoDownloadHandler
{
    public static interface OnBluetoothDownloadStatusListener
    {

        public abstract void onStatusChanged();
    }


    private static final String TAG = "download";
    private XiMaoDownloadTask currentTask;
    private Context mContext;
    private List mOnBluetoothDownloadStatusListener;
    private ExecutorService singleThreadExecutor;
    private List unfinishedDownlaodTasks;

    public XiMaoDownloadHandler(Context context)
    {
        unfinishedDownlaodTasks = null;
        singleThreadExecutor = null;
        currentTask = null;
        mContext = context;
    }

    public void addDownloadTask(XiMaoDownloadTask ximaodownloadtask)
    {
        Logger.d("download", "addDownloadTask IN");
        if (unfinishedDownlaodTasks == null)
        {
            unfinishedDownlaodTasks = new ArrayList();
        }
        if (!unfinishedDownlaodTasks.contains(ximaodownloadtask))
        {
            unfinishedDownlaodTasks.add(ximaodownloadtask);
            for (ximaodownloadtask = mOnBluetoothDownloadStatusListener.iterator(); ximaodownloadtask.hasNext(); ((OnBluetoothDownloadStatusListener)ximaodownloadtask.next()).onStatusChanged()) { }
        }
    }

    protected void cancelCurrentTask()
    {
        if (currentTask != null)
        {
            currentTask.isRunning = false;
            currentTask.finishDownload();
            currentTask = null;
        }
        if (singleThreadExecutor != null && !singleThreadExecutor.isShutdown())
        {
            singleThreadExecutor.shutdown();
        }
        singleThreadExecutor = null;
    }

    public void cleanup()
    {
        Logger.d("ximao", "Handler cleanup IN");
        clearDownloadTask();
    }

    public void clearDownloadTask()
    {
        Logger.d("download", "removeDownloadTask IN");
        if (unfinishedDownlaodTasks != null)
        {
            unfinishedDownlaodTasks.clear();
        }
        cancelCurrentTask();
        if (mOnBluetoothDownloadStatusListener != null)
        {
            for (Iterator iterator = mOnBluetoothDownloadStatusListener.iterator(); iterator.hasNext(); ((OnBluetoothDownloadStatusListener)iterator.next()).onStatusChanged()) { }
        }
    }

    public XiMaoDownloadTask getTopTask()
    {
        if (unfinishedDownlaodTasks != null && unfinishedDownlaodTasks.size() > 0)
        {
            return (XiMaoDownloadTask)unfinishedDownlaodTasks.get(0);
        } else
        {
            return null;
        }
    }

    public List getUnfinishedDownlaodTasks()
    {
        return unfinishedDownlaodTasks;
    }

    public void onPacket(byte abyte0[])
    {
        Logger.d("download", "onPacket IN");
        if (currentTask != null) goto _L2; else goto _L1
_L1:
        Logger.d("download", "onPacket OUT #1 task==null");
_L12:
        return;
_L2:
        Logger.d("download", (new StringBuilder()).append(currentTask).append("").toString());
        abyte0[1];
        JVM INSTR tableswitch 48 52: default 88
    //                   48 89
    //                   49 176
    //                   50 219
    //                   51 231
    //                   52 132;
           goto _L3 _L4 _L5 _L6 _L7 _L8
_L3:
        return;
_L4:
        CustomToast.showToast(mContext, "\u6536\u5230CHECK_FILE\u7684\u56DE\u590D", 0);
        if (currentTask.getNowState() == 1)
        {
            synchronized (currentTask)
            {
                currentTask.notify();
            }
            return;
        }
          goto _L9
        exception;
          goto _L10
_L9:
        if (true) goto _L12; else goto _L11
_L11:
_L10:
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
_L8:
        CustomToast.showToast(mContext, "\u6536\u5230FILE_SIZE\u7684\u56DE\u590D", 0);
        if (currentTask.getNowState() != 6) goto _L12; else goto _L13
_L13:
        synchronized (currentTask)
        {
            currentTask.notify();
        }
        return;
        exception1;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception1;
_L5:
        CustomToast.showToast(mContext, "\u6536\u5230PREPARE\u7684\u56DE\u590D", 0);
        if (currentTask.getNowState() != 2) goto _L12; else goto _L14
_L14:
        synchronized (currentTask)
        {
            currentTask.notify();
        }
        return;
        exception2;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception2;
_L6:
        if (currentTask.getNowState() == 3)
        {
            return;
        }
          goto _L12
_L7:
        CustomToast.showToast(MyApplication.b(), "\u6536\u5230\u5B8C\u6210\u786E\u8BA4\u547D\u4EE4", 0);
        if (currentTask.getNowState() != 4) goto _L12; else goto _L15
_L15:
        synchronized (currentTask)
        {
            currentTask.notify();
        }
        removeDownloadTask(currentTask);
        return;
        exception3;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception3;
    }

    public void removeDownloadTask(XiMaoDownloadTask ximaodownloadtask)
    {
        Logger.d("download", "removeDownloadTask IN");
        Logger.d("download", (new StringBuilder()).append("").append(ximaodownloadtask).toString());
        if (unfinishedDownlaodTasks.contains(ximaodownloadtask))
        {
            unfinishedDownlaodTasks.remove(ximaodownloadtask);
        } else
        {
            Logger.d("download", "not exist");
        }
        if (unfinishedDownlaodTasks == null || unfinishedDownlaodTasks.size() == 0 || currentTask == ximaodownloadtask)
        {
            cancelCurrentTask();
        }
        for (ximaodownloadtask = mOnBluetoothDownloadStatusListener.iterator(); ximaodownloadtask.hasNext(); ((OnBluetoothDownloadStatusListener)ximaodownloadtask.next()).onStatusChanged()) { }
        startDownloadTask();
    }

    public void removeOnBluetoothDownloadStatusListener(OnBluetoothDownloadStatusListener onbluetoothdownloadstatuslistener)
    {
        mOnBluetoothDownloadStatusListener.remove(onbluetoothdownloadstatuslistener);
    }

    public void setOnBluetoothDownloadStatusListener(OnBluetoothDownloadStatusListener onbluetoothdownloadstatuslistener)
    {
        if (mOnBluetoothDownloadStatusListener == null)
        {
            mOnBluetoothDownloadStatusListener = new ArrayList();
        }
        if (!mOnBluetoothDownloadStatusListener.contains(onbluetoothdownloadstatuslistener))
        {
            mOnBluetoothDownloadStatusListener.add(onbluetoothdownloadstatuslistener);
        }
    }

    public void startDownloadTask()
    {
        Logger.d("download", "startDownloadTask IN");
        if (unfinishedDownlaodTasks.size() <= 0 || singleThreadExecutor != null && !singleThreadExecutor.isShutdown() && currentTask != null)
        {
            return;
        } else
        {
            singleThreadExecutor = Executors.newSingleThreadExecutor();
            currentTask = getTopTask();
            Logger.d("download", (new StringBuilder()).append("").append(currentTask).toString());
            singleThreadExecutor.execute(currentTask);
            return;
        }
    }
}
