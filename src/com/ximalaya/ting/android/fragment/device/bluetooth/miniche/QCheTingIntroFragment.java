// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.miniche;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.MyBluetoothDevice;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.bluetooth.miniche:
//            QCheFmSenderFragment, QCheController

public class QCheTingIntroFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.OnBtConnListener
{

    public static final String P_Type = "P_Type";
    public static final String QCHETING = "00:58:51";
    protected static final String TAG = com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheTingIntroFragment.getSimpleName();
    private MyBluetoothDevice device;
    private LinearLayout goBlueTooth;
    private LinearLayout mBackHome;
    private ImageView mBackImg;
    private BluetoothManager mBluetoothManager;
    private TextView mBtnRight;
    private RelativeLayout mContentView;
    private LinearLayout mSetting;
    private TextView top_tv;

    public QCheTingIntroFragment()
    {
    }

    private void expandHitRect(final View view)
    {
        mContentView.post(new _cls5());
    }

    private void initView()
    {
        top_tv = (TextView)(TextView)mContentView.findViewById(0x7f0a00ae);
        top_tv.setText("\u968F\u8F66\u542CQ\u7248");
        android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)top_tv.getLayoutParams();
        layoutparams.addRule(13);
        top_tv.setLayoutParams(layoutparams);
        goBlueTooth = (LinearLayout)mContentView.findViewById(0x7f0a02f7);
        goBlueTooth.setOnClickListener(this);
        expandHitRect(goBlueTooth);
        mBtnRight = (TextView)mContentView.findViewById(0x7f0a071c);
        mBtnRight.setVisibility(0);
        mBtnRight.setText("\u8BF4\u660E\u4E66");
        mBtnRight.setOnClickListener(this);
        mBackHome = (LinearLayout)mContentView.findViewById(0x7f0a02fa);
        mBackHome.setOnClickListener(this);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(this);
        mSetting = (LinearLayout)mContentView.findViewById(0x7f0a02f1);
        mSetting.setOnClickListener(this);
        expandHitRect(mSetting);
    }

    private void showWarning()
    {
        if (MyApplication.a() == null)
        {
            return;
        } else
        {
            MyApplication.a().runOnUiThread(new _cls3());
            return;
        }
    }

    private void showWarning(final String message)
    {
        if (MyApplication.a() == null)
        {
            return;
        } else
        {
            MyApplication.a().runOnUiThread(new _cls4());
            return;
        }
    }

    private void toFmSenderFragment()
    {
        startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheFmSenderFragment, null);
    }

    public boolean onA2dpConn()
    {
        if (mBluetoothManager.getMyBluetoothDevice().getNowType() == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting && mBluetoothManager.isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
        {
            toFmSenderFragment();
        }
        return false;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        Logger.d(TAG, "QCheTingIntroFragment onActivityCreated");
        mBluetoothManager = BluetoothManager.getInstance(getActivity());
        initView();
        hidePlayButton();
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 5: default 56
    //                   2131362545: 76
    //                   2131362551: 142
    //                   2131362554: 57
    //                   2131363611: 68
    //                   2131363612: 167;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        return;
_L4:
        if (!ToolUtil.isFastClick())
        {
            goToFindingPage();
            return;
        }
        continue; /* Loop/switch isn't completed */
_L5:
        getActivity().onBackPressed();
        return;
_L2:
        if (!ToolUtil.isFastClick())
        {
            if (android.os.Build.VERSION.SDK_INT < 11)
            {
                mBluetoothManager.isA2dpConn(new _cls1(), false, true);
                return;
            }
            if (BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(2) == 2)
            {
                mBluetoothManager.isA2dpConn(new _cls2(), false, true);
                return;
            } else
            {
                showWarning();
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (getActivity() != null)
        {
            getActivity().startActivity(new Intent("android.settings.BLUETOOTH_SETTINGS"));
            return;
        }
        if (true) goto _L1; else goto _L6
_L6:
        QCheController.goIntroduction(getActivity());
        return;
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300cb, viewgroup, false);
        return mContentView;
    }

    public void onResume()
    {
        super.onResume();
        Logger.d(TAG, "QCheTingIntroFragment onResume");
        hidePlayButton();
        mBluetoothManager.setBtConnListener(this);
    }

    public boolean onSppConn(com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager.BtSTATE btstate)
    {
        return true;
    }

    public void onStop()
    {
        mBluetoothManager.removeBtConnListener(this);
        super.onStop();
    }






    private class _cls5
        implements Runnable
    {

        final QCheTingIntroFragment this$0;
        final View val$view;

        public void run()
        {
            if (isAdded())
            {
                Object obj = new Rect();
                View view1 = view;
                view1.getHitRect(((Rect) (obj)));
                int i = ToolUtil.dp2px(getActivity(), 100F);
                obj.right = ((Rect) (obj)).right + i;
                obj.left = ((Rect) (obj)).left - i;
                obj.top = ((Rect) (obj)).top + i;
                obj.bottom = ((Rect) (obj)).bottom - i;
                obj = new TouchDelegate(((Rect) (obj)), view1);
                if (android/view/View.isInstance(view1.getParent()))
                {
                    ((View)view1.getParent()).setTouchDelegate(((TouchDelegate) (obj)));
                }
            }
        }

        _cls5()
        {
            this$0 = QCheTingIntroFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls3
        implements Runnable
    {

        final QCheTingIntroFragment this$0;

        public void run()
        {
            (new DialogBuilder(MyApplication.a())).setMessage("\u5F53\u524D\u6CA1\u6709\u8FDE\u63A5\u968F\u8F66\u542C\uFF0C\u8BF7\u6309\u7167\u8BF4\u660E\u8FDE\u63A5\u968F\u8F66\u542C\u540E\u518D\u8BD5").showWarning();
        }

        _cls3()
        {
            this$0 = QCheTingIntroFragment.this;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final QCheTingIntroFragment this$0;
        final String val$message;

        public void run()
        {
            (new DialogBuilder(MyApplication.a())).setMessage(message).showWarning();
        }

        _cls4()
        {
            this$0 = QCheTingIntroFragment.this;
            message = s;
            super();
        }
    }


    private class _cls1 extends BaseBluetoothCallBack
    {

        final QCheTingIntroFragment this$0;

        public void onFailed()
        {
            showWarning();
        }

        public void onSuccess(ActionModel actionmodel)
        {
            if (mBluetoothManager.getMyBluetoothDevice().getNowType() == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting)
            {
                if (mBluetoothManager.isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
                {
                    toFmSenderFragment();
                    return;
                } else
                {
                    showWarning("\u60A8\u7684\u624B\u673A\u7CFB\u7EDF\u7248\u672C\u592A\u4F4E\uFF0C\u8BF7\u66F4\u65B0\u624B\u673A\u7CFB\u7EDF\u7248\u672C\u540E\u518D\u8BD5");
                    return;
                }
            } else
            {
                showWarning();
                return;
            }
        }

        _cls1()
        {
            this$0 = QCheTingIntroFragment.this;
            super();
        }
    }


    private class _cls2 extends BaseBluetoothCallBack
    {

        final QCheTingIntroFragment this$0;

        public void onFailed()
        {
            showWarning();
        }

        public void onSuccess(ActionModel actionmodel)
        {
            if (mBluetoothManager.getMyBluetoothDevice().getNowType() == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting)
            {
                if (mBluetoothManager.isSppConn(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting))
                {
                    toFmSenderFragment();
                    return;
                } else
                {
                    startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheTingConnFragment, null);
                    return;
                }
            } else
            {
                showWarning();
                return;
            }
        }

        _cls2()
        {
            this$0 = QCheTingIntroFragment.this;
            super();
        }
    }

}
