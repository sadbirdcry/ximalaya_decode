// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.bluetooth.model;


public class RecordModel
{

    String device;
    String deviceName;
    String type;

    public RecordModel(String s, String s1, String s2)
    {
        type = s;
        device = s1;
        deviceName = s2;
    }

    public String getDevice()
    {
        return device;
    }

    public String getDeviceName()
    {
        return deviceName;
    }

    public String getType()
    {
        return type;
    }

    public void setDevice(String s)
    {
        device = s;
    }

    public void setDeviceName(String s)
    {
        deviceName = s;
    }

    public void setType(String s)
    {
        type = s;
    }
}
