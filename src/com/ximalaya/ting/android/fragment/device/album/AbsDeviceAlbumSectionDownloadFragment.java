// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.album;

import com.ximalaya.ting.android.fragment.album.AlbumSectionDownloadFragment;
import com.ximalaya.ting.android.fragment.album.SectionContentAdapter;
import com.ximalaya.ting.android.model.album.AlbumSectionModel;

public abstract class AbsDeviceAlbumSectionDownloadFragment extends AlbumSectionDownloadFragment
{

    public AbsDeviceAlbumSectionDownloadFragment()
    {
    }

    protected abstract SectionContentAdapter getContentAdapter(AlbumSectionModel albumsectionmodel);

    protected abstract void updateBottomBarInfo();

    protected abstract void updateDownloadingCount(int i);
}
