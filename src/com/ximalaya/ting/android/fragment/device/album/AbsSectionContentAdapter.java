// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.album;

import android.content.Context;
import com.ximalaya.ting.android.fragment.album.SectionContentAdapter;
import com.ximalaya.ting.android.model.album.AlbumSectionModel;

public abstract class AbsSectionContentAdapter extends SectionContentAdapter
{

    public AbsSectionContentAdapter(Context context, AlbumSectionModel albumsectionmodel)
    {
        super(context, albumsectionmodel);
    }

    protected abstract boolean isInDownloadList(com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track);

    public boolean isItemClickable(int i)
    {
        return super.isItemClickable(i);
    }

    public abstract boolean isOneChecked();
}
