// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceListFragment;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoVoiceManager;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.modelmanage.DexManager;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            MyDeviceManager, ProductModel

public class FunctionStartFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    public static final String P_Type = "P_Type";
    private LinearLayout cheStartFunction;
    MyProgressDialog loadingDialog;
    private Context mContext;
    private RelativeLayout mConvertView;
    private TextView mDeviceDescirbe;
    private TextView mDeviceName;
    private DexManager mDexManager;
    private String mFunction;
    private LinearLayout mNotGetLayout;
    private TextView mSubtitle;
    List productModels;
    private TextView topTextView;

    public FunctionStartFragment()
    {
        productModels = MyDeviceManager.getInstance(mContext).getProductModels();
        loadingDialog = null;
    }

    private void cancelLoadingDialog()
    {
        getActivity().runOnUiThread(new _cls3());
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            mFunction = bundle.getString("P_Type");
        }
    }

    private void initMinicheManager()
    {
        (new Thread(new _cls11())).start();
    }

    private void initXimaoManager()
    {
        final Context contex = getActivity().getApplicationContext();
        XiMaoVoiceManager.getInstance(contex).initInUi(contex);
        XiMaoBTManager.getInstance(contex).initInUi();
        (new Thread(new _cls10())).start();
    }

    private void judgeProductModel(String s, ProductModel productmodel)
    {
        int i = productModels.size();
        if (i > 0)
        {
            productmodel = MyDeviceManager.getInstance(getActivity()).getProductModel(s);
        }
        if (i > 0 && productmodel != null)
        {
            mSubtitle.setText(productmodel.getSubtitle());
        }
        if (i > 0 && productmodel != null)
        {
            mNotGetLayout.setVisibility(0);
            return;
        } else
        {
            mNotGetLayout.setVisibility(8);
            return;
        }
    }

    private void onClickStart()
    {
        if (TextUtils.isEmpty(mFunction))
        {
            showToast("\u7A0B\u5E8F\u9519\u8BEF");
            finish();
        } else
        {
            if (mFunction.equals(MyDeviceManager.DeviceType.suicheting.toString()))
            {
                showLoadingDialog();
                mDexManager.loadSuicheting(new _cls4());
                return;
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.ximao.toString()))
            {
                showLoadingDialog();
                mDexManager.copyLoadDexWithoutLoadSync("Msc_dex.jar", new _cls5());
                return;
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.tingshubao.toString()))
            {
                if (MyDeviceManager.getInstance(mContext).isDlnaInit())
                {
                    mDexManager.setIsTingshubaoOpen(true);
                    toDeviceListFragment(MyDeviceManager.DeviceType.tingshubao);
                    return;
                } else
                {
                    showLoadingDialog();
                    mDexManager.loadDlna(new _cls6(), 1);
                    return;
                }
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.shukewifi.toString()))
            {
                if (MyDeviceManager.getInstance(mContext).isDlnaInit())
                {
                    mDexManager.setSysExit(true);
                    mDexManager.setDossOpen(true);
                    toDeviceListFragment(MyDeviceManager.DeviceType.shukewifi);
                    return;
                } else
                {
                    showLoadingDialog();
                    mDexManager.loadDlna(new _cls7(), 1);
                    return;
                }
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.doss.toString()))
            {
                if (MyDeviceManager.getInstance(mContext).isDlnaInit())
                {
                    mDexManager.setSysExit(true);
                    mDexManager.setDossOpen(true);
                    toDeviceListFragment(MyDeviceManager.DeviceType.doss);
                    return;
                } else
                {
                    showLoadingDialog();
                    mDexManager.loadDlna(new _cls8(), 1);
                    return;
                }
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.Qsuicheting.toString()))
            {
                showLoadingDialog();
                mDexManager.copyLoadDexWithoutLoadSync("Msc_dex.jar", new _cls9());
                return;
            }
        }
    }

    private void showLoadingDialog()
    {
        getActivity().runOnUiThread(new _cls2());
    }

    private void showWarning(final String deviceRestartString)
    {
        MyApplication.a().runOnUiThread(new _cls1());
    }

    public void initView()
    {
        initData();
        topTextView = (TextView)(TextView)mConvertView.findViewById(0x7f0a00ae);
        mSubtitle = (TextView)mConvertView.findViewById(0x7f0a02eb);
        mDeviceDescirbe = (TextView)mConvertView.findViewById(0x7f0a02e9);
        mNotGetLayout = (LinearLayout)mConvertView.findViewById(0x7f0a02ea);
        cheStartFunction = (LinearLayout)mConvertView.findViewById(0x7f0a02e7);
        mDeviceName = (TextView)mConvertView.findViewById(0x7f0a02e8);
        if (productModels == null)
        {
            return;
        }
        if (!mFunction.equals(MyDeviceManager.DeviceType.suicheting.toString())) goto _L2; else goto _L1
_L1:
        topTextView.setText("\u968F\u8F66\u542C");
        mDeviceName.setText("\u5DF2\u8D2D\u4E70\uFF0C\u542F\u7528\u968F\u8F66\u542C\u7BA1\u7406");
        mDeviceDescirbe.setText("\u53EF\u4EE5\u67E5\u770B\u968F\u8F66\u542C\u7684\u8FDE\u63A5\u8BF4\u660E\u3001\u8C03\u6574\u968F\u8F66\u542C\u9891\u7387");
        judgeProductModel("\u968F\u8F66\u542C\u63D2\u4EF6", null);
_L4:
        cheStartFunction.setOnClickListener(this);
        mNotGetLayout.setOnClickListener(this);
        mConvertView.findViewById(0x7f0a071b).setOnClickListener(this);
        return;
_L2:
        if (mFunction.equals(MyDeviceManager.DeviceType.ximao.toString()))
        {
            topTextView.setText("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A");
            mDeviceName.setText("\u5DF2\u8D2D\u4E70\uFF0C\u542F\u7528\u6545\u4E8B\u673A\u7BA1\u7406");
            mDeviceDescirbe.setText("\u53EF\u7BA1\u7406\u5185\u7F6E\u58F0\u97F3\u3001\u66F4\u6362\u7ED1\u5B9A\u7684\u4E13\u8F91\u3001\u542F\u7528\u84DD\u7259\u6A21\u5F0F");
            judgeProductModel("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A\u63D2\u4EF6", null);
        } else
        if (mFunction.equals(MyDeviceManager.DeviceType.tingshubao.toString()))
        {
            topTextView.setText(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoName());
            mDeviceName.setText((new StringBuilder()).append("\u5DF2\u8D2D\u4E70\uFF0C\u542F\u7528").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoName()).append("\u7BA1\u7406").toString());
            mDeviceDescirbe.setText((new StringBuilder()).append("\u53EF\u5C06").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoName()).append("\u8FDE\u7F51\u3001\u66F4\u6362\u7ED1\u5B9A\u7684\u4E13\u8F91\u3001\u901A\u8FC7").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoName()).append("\u64AD\u653E\u559C\u9A6C\u62C9\u96C5\u8282\u76EE\uFF08\u5373XPlay\u529F\u80FD\uFF09").toString());
            judgeProductModel("\u542C\u4E66\u5B9D\u63D2\u4EF6", null);
        } else
        if (mFunction.equals(MyDeviceManager.DeviceType.doss.toString()))
        {
            topTextView.setText(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossName());
            mDeviceName.setText((new StringBuilder()).append("\u5DF2\u8D2D\u4E70\uFF0C\u542F\u7528").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossName()).append("\u7BA1\u7406").toString());
            mDeviceDescirbe.setText((new StringBuilder()).append("\u53EF\u4EE5\u5C06").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossName()).append("\u8FDE\u7F51\u3001\u66F4\u6362\u7ED1\u5B9A\u7684\u4E13\u8F91\u3001\u901A\u8FC7").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossName()).append("\u64AD\u653E\u559C\u9A6C\u62C9\u96C5\u8282\u76EE\uFF08\u5373XPlay\u529F\u80FD\uFF09").toString());
            judgeProductModel("\u542C\u5427\u63D2\u4EF6", null);
        } else
        if (mFunction.equals(MyDeviceManager.DeviceType.shukewifi.toString()))
        {
            topTextView.setText(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiName());
            mDeviceName.setText((new StringBuilder()).append("\u5DF2\u8D2D\u4E70\uFF0C\u542F\u7528").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiName()).append("\u7BA1\u7406").toString());
            mDeviceDescirbe.setText((new StringBuilder()).append("\u53EF\u4EE5\u5C06").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiName()).append("\u8FDE\u7F51\u3001\u66F4\u6362\u7ED1\u5B9A\u7684\u4E13\u8F91\u3001\u901A\u8FC7").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiName()).append("\u64AD\u653E\u559C\u9A6C\u62C9\u96C5\u8282\u76EE\uFF08\u5373XPlay\u529F\u80FD\uFF09").toString());
            judgeProductModel("wifi\u6545\u4E8B\u673A\u63D2\u4EF6", null);
        } else
        if (mFunction.equals(MyDeviceManager.DeviceType.Qsuicheting.toString()))
        {
            topTextView.setText("\u968F\u8F66\u542CQ\u7248");
            mDeviceName.setText("\u5DF2\u8D2D\u4E70\uFF0C\u542F\u7528\u968F\u8F66\u542C\u7BA1\u7406");
            mDeviceDescirbe.setText("\u53EF\u4EE5\u67E5\u770B\u968F\u8F66\u542C\u7684\u8FDE\u63A5\u8BF4\u660E\u3001\u8C03\u6574\u968F\u8F66\u542C\u9891\u7387");
            judgeProductModel("\u968F\u8F66\u542CQ\u7248\u63D2\u4EF6", null);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mDexManager = DexManager.getInstance(getActivity().getApplicationContext());
        initData();
        initView();
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 3: default 40
    //                   2131362535: 41
    //                   2131362538: 46
    //                   2131363611: 233;
           goto _L1 _L2 _L3 _L4
_L1:
        return;
_L2:
        onClickStart();
        return;
_L3:
        if (productModels != null)
        {
            if (mFunction.equals(MyDeviceManager.DeviceType.suicheting.toString()))
            {
                MyDeviceManager.getInstance(getActivity()).toEachMarket("\u968F\u8F66\u542C\u63D2\u4EF6");
                return;
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.ximao.toString()))
            {
                MyDeviceManager.getInstance(getActivity()).toEachMarket("\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A\u63D2\u4EF6");
                return;
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.tingshubao.toString()))
            {
                MyDeviceManager.getInstance(getActivity()).toEachMarket("\u542C\u4E66\u5B9D\u63D2\u4EF6");
                return;
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.doss.toString()))
            {
                MyDeviceManager.getInstance(getActivity()).toEachMarket("\u542C\u5427\u63D2\u4EF6");
                return;
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.shukewifi.toString()))
            {
                MyDeviceManager.getInstance(getActivity()).toEachMarket("wifi\u6545\u4E8B\u673A\u63D2\u4EF6");
                return;
            }
            if (mFunction.equals(MyDeviceManager.DeviceType.Qsuicheting.toString()))
            {
                MyDeviceManager.getInstance(getActivity()).toEachMarket("\u968F\u8F66\u542CQ\u7248\u63D2\u4EF6");
                return;
            }
        }
          goto _L1
_L4:
        finish();
        return;
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mConvertView = (RelativeLayout)layoutinflater.inflate(0x7f0300a3, null);
        return mConvertView;
    }

    protected void toDeviceListFragment(MyDeviceManager.DeviceType devicetype)
    {
        Bundle bundle = new Bundle();
        bundle.putInt(DeviceListFragment.deviceType, devicetype.ordinal());
        startFragment(com/ximalaya/ting/android/fragment/device/dlna/DeviceListFragment, bundle);
        finish();
    }






    private class _cls3
        implements Runnable
    {

        final FunctionStartFragment this$0;

        public void run()
        {
            if (loadingDialog != null)
            {
                loadingDialog.dismiss();
                loadingDialog = null;
            }
        }

        _cls3()
        {
            this$0 = FunctionStartFragment.this;
            super();
        }
    }


    private class _cls11
        implements Runnable
    {

        final FunctionStartFragment this$0;

        public void run()
        {
            BluetoothManager.getInstance(mContext).init(MyDeviceManager.DeviceType.Qsuicheting, null, true, true);
        }

        _cls11()
        {
            this$0 = FunctionStartFragment.this;
            super();
        }
    }


    private class _cls10
        implements Runnable
    {

        final FunctionStartFragment this$0;
        final Context val$contex;

        public void run()
        {
            if (XiMaoBTManager.getInstance(contex).getConnState() != com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.STATE.CONNECTED)
            {
                XiMaoBTManager.getInstance(contex).tryConnect(true);
            }
        }

        _cls10()
        {
            this$0 = FunctionStartFragment.this;
            contex = context;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final FunctionStartFragment this$0;

        public void run()
        {
            mDexManager.setIsSuichetingOpen(true);
            MyBluetoothManager2.getInstance(getActivity().getApplicationContext()).checkDevice();
            cancelLoadingDialog();
            startFragment(com/ximalaya/ting/android/fragment/device/che/CheTingIntroFragment, null);
            finish();
        }

        _cls4()
        {
            this$0 = FunctionStartFragment.this;
            super();
        }
    }


    private class _cls5
        implements Runnable
    {

        final FunctionStartFragment this$0;

        public void run()
        {
            mDexManager.setIsXimaoOpen(true);
            initXimaoManager();
            cancelLoadingDialog();
            if (!XiMaoBTManager.getInstance(mCon).isConnected())
            {
                break MISSING_BLOCK_LABEL_59;
            }
            startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoContentFragment, null);
_L1:
            finish();
            return;
            try
            {
                startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoIntroFragment, null);
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
                return;
            }
              goto _L1
        }

        _cls5()
        {
            this$0 = FunctionStartFragment.this;
            super();
        }
    }


    private class _cls6
        implements Runnable
    {

        final FunctionStartFragment this$0;

        public void run()
        {
            cancelLoadingDialog();
            mDexManager.setIsTingshubaoOpen(true);
            if (getActivity() != null)
            {
                ((MainTabActivity2)getActivity()).tingshubaoBind();
            }
            toDeviceListFragment(MyDeviceManager.DeviceType.tingshubao);
        }

        _cls6()
        {
            this$0 = FunctionStartFragment.this;
            super();
        }
    }


    private class _cls7
        implements Runnable
    {

        final FunctionStartFragment this$0;

        public void run()
        {
            mDexManager.setSysExit(true);
            cancelLoadingDialog();
            mDexManager.setDossOpen(true);
            ((MainTabActivity2)getActivity()).initTingshubao();
            toDeviceListFragment(MyDeviceManager.DeviceType.shukewifi);
        }

        _cls7()
        {
            this$0 = FunctionStartFragment.this;
            super();
        }
    }


    private class _cls8
        implements Runnable
    {

        final FunctionStartFragment this$0;

        public void run()
        {
            mDexManager.setSysExit(true);
            cancelLoadingDialog();
            mDexManager.setDossOpen(true);
            ((MainTabActivity2)getActivity()).initTingshubao();
            toDeviceListFragment(MyDeviceManager.DeviceType.doss);
        }

        _cls8()
        {
            this$0 = FunctionStartFragment.this;
            super();
        }
    }


    private class _cls9
        implements Runnable
    {

        final FunctionStartFragment this$0;

        public void run()
        {
            mDexManager.setIsQSuichetingOpen(true);
            try
            {
                initMinicheManager();
                cancelLoadingDialog();
                startFragment(com/ximalaya/ting/android/fragment/device/bluetooth/miniche/QCheTingIntroFragment, null);
                finish();
                return;
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }

        _cls9()
        {
            this$0 = FunctionStartFragment.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final FunctionStartFragment this$0;

        public void run()
        {
            if (loadingDialog != null)
            {
                loadingDialog.dismiss();
                loadingDialog = null;
            }
            loadingDialog = new MyProgressDialog(getActivity());
            loadingDialog.setTitle("\u63D0\u793A");
            loadingDialog.setMessage("\u6B63\u5728\u542F\u7528");
            loadingDialog.show();
        }

        _cls2()
        {
            this$0 = FunctionStartFragment.this;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final FunctionStartFragment this$0;
        final String val$deviceRestartString;

        public void run()
        {
            (new DialogBuilder(MyApplication.a())).setMessage(deviceRestartString).showWarning();
        }

        _cls1()
        {
            this$0 = FunctionStartFragment.this;
            deviceRestartString = s;
            super();
        }
    }

}
