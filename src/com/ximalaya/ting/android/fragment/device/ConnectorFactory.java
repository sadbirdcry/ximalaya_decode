// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.app.Activity;
import com.ximalaya.ting.android.fragment.device.doss.DossConnImp;

// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            IConnCallBack, BaseConnectorImp

public class ConnectorFactory
{
    public static final class Type extends Enum
    {

        private static final Type $VALUES[];
        public static final Type doss;
        public static final Type tingshubao;

        public static Type valueOf(String s)
        {
            return (Type)Enum.valueOf(com/ximalaya/ting/android/fragment/device/ConnectorFactory$Type, s);
        }

        public static Type[] values()
        {
            return (Type[])$VALUES.clone();
        }

        static 
        {
            tingshubao = new Type("tingshubao", 0);
            doss = new Type("doss", 1);
            $VALUES = (new Type[] {
                tingshubao, doss
            });
        }

        private Type(String s, int i)
        {
            super(s, i);
        }
    }


    public ConnectorFactory()
    {
    }

    public static BaseConnectorImp createConnector(Activity activity, Type type, IConnCallBack iconncallback)
    {
        if (type == Type.doss)
        {
            return new DossConnImp(activity, iconncallback);
        } else
        {
            return null;
        }
    }

    public static Type getTypeByName(String s)
    {
        if (s.equals("doss"))
        {
            return Type.doss;
        }
        if (s.equals("tingshubao"))
        {
            return Type.tingshubao;
        } else
        {
            return null;
        }
    }
}
