// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.callback;

import java.util.HashMap;
import java.util.Map;

public class ActionModel
{

    public static final String CALL_BACK = "callback";
    public static final String DEFAULT = "default";
    public static final String DEVICE = "device";
    public Map result;

    public ActionModel()
    {
        result = new HashMap();
    }

    public static ActionModel createModel(Object obj)
    {
        ActionModel actionmodel = new ActionModel();
        actionmodel.result.put("default", obj);
        return actionmodel;
    }

    public Object getDefault()
    {
        return result.get("default");
    }
}
