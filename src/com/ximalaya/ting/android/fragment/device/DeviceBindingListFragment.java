// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.BaseBindableDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceUtil;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonSoundListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseMessageModule;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukeAccountBindingDialogFragment;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukeMessageModule;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukeUtil;
import com.ximalaya.ting.android.fragment.device.shu.BindingListAdapter;
import com.ximalaya.ting.android.fragment.device.shu.TingshuIntroDialogFragment;
import com.ximalaya.ting.android.fragment.device.shu.WifiDeviceSettingFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.HashMap;
import org.teleal.cling.model.types.UDN;

// Referenced classes of package com.ximalaya.ting.android.fragment.device:
//            MyDeviceManager

public class DeviceBindingListFragment extends BaseListFragment
    implements android.view.View.OnClickListener
{

    private String TAG;
    private BindingListAdapter mAdapter;
    private ImageView mBackImg;
    private TextView mConnMore;
    private BaseBindableDeviceItem mDeviceItem;
    private TextView mIntro;
    private MyDeviceManager.DeviceType mNowDeviceType;
    private RelativeLayout mTopBar;
    private TextView mTopTv;

    public DeviceBindingListFragment()
    {
        TAG = com/ximalaya/ting/android/fragment/device/DeviceBindingListFragment.getSimpleName();
    }

    private void freshAllAlbum()
    {
        Logger.d("doss", "freshAllAlbum IN");
        freshBindLayout();
        for (int i = 0; i < mDeviceItem.getBaseBindableModel().getAlbumNum(); i++)
        {
            AlbumModel albummodel = (AlbumModel)mDeviceItem.getBaseBindableModel().getAlbums().get(Integer.valueOf(i + 1));
            if (albummodel != null)
            {
                initOneAlbumInfo(albummodel.albumId, i + 1);
            }
        }

    }

    private void freshBindLayout()
    {
        mAdapter.notifyDataSetChanged();
    }

    private void initListener()
    {
        if (mDeviceItem.getDlnaType() == MyDeviceManager.DeviceType.doss || mDeviceItem.getDlnaType() == MyDeviceManager.DeviceType.shukewifi)
        {
            mListView.setOnItemClickListener(new _cls1());
            return;
        } else
        {
            mListView.setClickable(false);
            return;
        }
    }

    private void initOneAlbumInfo(final long albumModelId, final int channelId)
    {
        Logger.d("doss", (new StringBuilder()).append("initOneAlbumInfo:albumModelId:").append(albumModelId).append(",channelId:").append(channelId).toString());
        while (albumModelId <= 0L || !ToolUtil.isConnectToNetwork(mCon)) 
        {
            return;
        }
        (new _cls2()).myexec(new Void[0]);
    }

    private void initUi()
    {
        mConnMore = (TextView)fragmentBaseContainerView.findViewById(0x7f0a071c);
        mBackImg = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a071b);
        mTopTv = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        mIntro = (TextView)fragmentBaseContainerView.findViewById(0x7f0a025f);
        mTopBar = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a005f);
        mConnMore.setText("\u8BBE\u7F6E");
        mConnMore.setVisibility(0);
        mTopTv.setText(DeviceUtil.getDeviceItemName(mDeviceItem));
        mConnMore.setOnClickListener(this);
        mBackImg.setOnClickListener(this);
    }

    private void showAccountBindingDialog()
    {
        Logger.d(TAG, "showIntroDialog IN");
        ShukeAccountBindingDialogFragment shukeaccountbindingdialogfragment = new ShukeAccountBindingDialogFragment();
        FragmentTransaction fragmenttransaction = getFragmentManager().beginTransaction();
        fragmenttransaction.add(shukeaccountbindingdialogfragment, "dialog");
        fragmenttransaction.commitAllowingStateLoss();
    }

    private void showIntroDialog()
    {
        Logger.d(TAG, "showIntroDialog IN");
        TingshuIntroDialogFragment tingshuintrodialogfragment = new TingshuIntroDialogFragment();
        FragmentTransaction fragmenttransaction = getFragmentManager().beginTransaction();
        fragmenttransaction.add(tingshuintrodialogfragment, "dialog");
        fragmenttransaction.commitAllowingStateLoss();
    }

    private void toSoundListFragment(int i)
    {
        Bundle bundle = new Bundle();
        bundle.putInt("channelId", i);
        AlbumModel albummodel = (AlbumModel)mDeviceItem.getBaseBindableModel().getAlbums().get(Integer.valueOf(i));
        if (albummodel == null || albummodel.albumId < 0L)
        {
            showToast("\u65E0\u6CD5\u83B7\u53D6\u8BE5\u4E13\u8F91\u4FE1\u606F");
            return;
        }
        if (albummodel.title == null || TextUtils.isEmpty(albummodel.title))
        {
            showToast("\u6B63\u5728\u8F7D\u5165\u4E13\u8F91\u4FE1\u606F\uFF0C\u8BF7\u7A0D\u540E");
            return;
        } else
        {
            bundle.putString("album", ((JSONObject)JSONObject.toJSON(albummodel)).toJSONString());
            startFragment(com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonSoundListFragment, bundle);
            return;
        }
    }

    public void distinguishDevice()
    {
        static class _cls3
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType = new int[MyDeviceManager.DeviceType.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[MyDeviceManager.DeviceType.tingshubao.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[MyDeviceManager.DeviceType.doss.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[MyDeviceManager.DeviceType.shukewifi.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        switch (_cls3..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[mNowDeviceType.ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            if (MyDeviceManager.getInstance(mCon).isTingshubaoFirstUsed())
            {
                MyDeviceManager.getInstance(mCon).setIsTingshubaoFirstUsed(false);
            }
            mIntro.setText((new StringBuilder()).append("\u77ED\u6309").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoSubName()).append("\u4E0A\u7684\u300C\u9891\u9053\u952E\u300D\uFF0C\u5373\u53EF\u64AD\u653E\u7ED1\u5B9A\u7684\u4E13\u8F91").toString());
            return;

        case 2: // '\002'
            mTopBar.setVisibility(8);
            mConnMore.setVisibility(8);
            mIntro.setText((new StringBuilder()).append("\u77ED\u6309").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossSubName()).append("\u4E0A\u7684\u300C\u9891\u9053\u300D\u952E\uFF0C\u5373\u53EF\u64AD\u653E\u7ED1\u5B9A\u7684\u4E13\u8F91").toString());
            return;

        case 3: // '\003'
            mTopBar.setVisibility(8);
            mConnMore.setVisibility(8);
            mIntro.setText((new StringBuilder()).append("\u77ED\u6309").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getShukeWifiSubName()).append("\u4E0A\u7684\u300C\u9891\u9053\u300D\u952E\uFF0C\u5373\u53EF\u64AD\u653E\u7ED1\u5B9A\u7684\u4E13\u8F91").toString());
            showBindingDialog();
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mDeviceItem = (BaseBindableDeviceItem)DlnaManager.getInstance(getActivity().getApplicationContext()).getOperationStroageModel().getNowDeviceItem();
        mNowDeviceType = mDeviceItem.getDlnaType();
        mAdapter = new BindingListAdapter(mCon, this, mDeviceItem);
        mListView.setAdapter(mAdapter);
        initUi();
        initListener();
        distinguishDevice();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        Logger.d("message", "DeviceBindingListFragment onActivityResult");
        super.onActivityResult(i, j, intent);
        if (UserInfoMannage.hasLogined() && getActivity() != null)
        {
            ((ShukeMessageModule)DlnaManager.getInstance(getActivity().getApplicationContext()).getController(MyDeviceManager.DeviceType.shukewifi).getModule(BaseMessageModule.NAME)).createAlbums();
            ShukeUtil.setNotFirstUse(getActivity().getApplicationContext(), mDeviceItem.getUdn().getIdentifierString());
        }
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            mActivity.onBackPressed();
            return;

        case 2131363612: 
            startFragment(com/ximalaya/ting/android/fragment/device/shu/WifiDeviceSettingFragment, null);
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = (RelativeLayout)layoutinflater.inflate(0x7f0300da, viewgroup, false);
        mListView = (ListView)fragmentBaseContainerView.findViewById(0x7f0a038a);
        return fragmentBaseContainerView;
    }

    public void onPause()
    {
        super.onPause();
        Logger.d("doss", "onPause");
    }

    public void onResume()
    {
        Logger.d("doss", "TingshuBindFragment onResume");
        super.onResume();
        freshAllAlbum();
    }

    public void onStop()
    {
        super.onStop();
    }

    protected void showBindingDialog()
    {
        if (ShukeUtil.isFirstUse(mCon, mDeviceItem.getUdn().getIdentifierString()))
        {
            ShukeUtil.setNotFirstUse(mCon, mDeviceItem.getUdn().getIdentifierString());
            if (!UserInfoMannage.hasLogined())
            {
                showAccountBindingDialog();
            }
        }
        if (UserInfoMannage.hasLogined())
        {
            ((ShukeMessageModule)DlnaManager.getInstance(mCon).getController(MyDeviceManager.DeviceType.shukewifi).getModule(BaseMessageModule.NAME)).createAlbums();
        }
    }





    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final DeviceBindingListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            toSoundListFragment(i + 1);
        }

        _cls1()
        {
            this$0 = DeviceBindingListFragment.this;
            super();
        }
    }


    private class _cls2 extends MyAsyncTask
    {

        AlbumModel tAlbum;
        final DeviceBindingListFragment this$0;
        final long val$albumModelId;
        final int val$channelId;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            Logger.d(TAG, "\u5F00\u59CB\u66F4\u65B0\u6570\u636E");
            avoid = new RequestParams();
            String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
            s = (new StringBuilder()).append(s).append(albumModelId).append("/").append(true).append("/").append(1).append("/").append(15).toString();
            avoid = f.a().a(s, avoid, null, null);
            Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
            avoid = JSON.parseObject(avoid);
            if (avoid == null)
            {
                return null;
            }
            if (!"0".equals(avoid.get("ret").toString()))
            {
                break MISSING_BLOCK_LABEL_178;
            }
            tAlbum = (AlbumModel)JSON.parseObject(avoid.getString("album"), com/ximalaya/ting/android/model/album/AlbumModel);
            return null;
            avoid;
            avoid.printStackTrace();
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            while (!isAdded() || tAlbum == null) 
            {
                return;
            }
            s = mDeviceItem.getBaseBindableModel();
            s.setAlbums(channelId, tAlbum);
            mDeviceItem.setBaseBindableModel(s);
            freshBindLayout();
        }

        _cls2()
        {
            this$0 = DeviceBindingListFragment.this;
            albumModelId = l;
            channelId = i;
            super();
            tAlbum = null;
        }
    }

}
