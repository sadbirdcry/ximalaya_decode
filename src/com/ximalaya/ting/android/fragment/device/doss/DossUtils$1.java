// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import android.content.Context;
import com.ximalaya.action.CreateDownloadTaskActionCallback;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.Logger;
import org.teleal.cling.model.action.ActionInvocation;
import org.teleal.cling.model.message.UpnpResponse;
import org.teleal.cling.model.meta.Service;

final class  extends CreateDownloadTaskActionCallback
{

    final Context val$context;
    final com.ximalaya.ting.android.fragment.device.ger.DeviceType val$deviceType;

    public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
    {
        CustomToast.showToast(MyApplication.a(), "\u52A0\u5165\u4E0B\u8F7D\u961F\u5217\u5931\u8D25\uFF0C\u8BF7\u91CD\u8BD5", 0);
        Logger.d("doss", "CreateDownloadTask Failure");
    }

    public void success(ActionInvocation actioninvocation)
    {
        actioninvocation = val$deviceType;
        com.ximalaya.ting.android.fragment.device.ger.DeviceType devicetype = val$deviceType;
        if (actioninvocation == com.ximalaya.ting.android.fragment.device.ger.DeviceType.shukewifi)
        {
            Logger.d("doss", "CreateDownloadTask Success");
            CustomToast.showToast(MyApplication.a(), "\u5DF2\u6210\u529F\u52A0\u5165\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A\u4E0B\u8F7D\u961F\u5217", 0);
            return;
        } else
        {
            Logger.d("doss", "CreateDownloadTask Success");
            CustomToast.showToast(MyApplication.a(), (new StringBuilder()).append("\u5DF2\u6210\u529F\u52A0\u5165").append(MyDeviceManager.getInstance(val$context).getDossName()).append("\u4E0B\u8F7D\u961F\u5217").toString(), 0);
            return;
        }
    }

    eviceType(com.ximalaya.ting.android.fragment.device.ger.DeviceType devicetype, Context context1)
    {
        val$deviceType = devicetype;
        val$context = context1;
        super(final_service, final_s);
    }
}
