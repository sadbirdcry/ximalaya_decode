// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;


// Referenced classes of package com.ximalaya.ting.android.fragment.device.doss:
//            DossConnManager

public static final class  extends Enum
{

    private static final ConnFailed $VALUES[];
    public static final ConnFailed ConnFailed;
    public static final ConnFailed ConnWifi;
    public static final ConnFailed Conned;
    public static final ConnFailed NoConn;
    public static final ConnFailed NoPassword;

    public static  valueOf(String s)
    {
        return ()Enum.valueOf(com/ximalaya/ting/android/fragment/device/doss/DossConnManager$ConnState, s);
    }

    public static [] values()
    {
        return ([])$VALUES.clone();
    }

    static 
    {
        NoConn = new <init>("NoConn", 0);
        ConnWifi = new <init>("ConnWifi", 1);
        NoPassword = new <init>("NoPassword", 2);
        Conned = new <init>("Conned", 3);
        ConnFailed = new <init>("ConnFailed", 4);
        $VALUES = (new .VALUES[] {
            NoConn, ConnWifi, NoPassword, Conned, ConnFailed
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
