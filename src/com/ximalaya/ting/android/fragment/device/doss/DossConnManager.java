// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import android.app.Activity;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;

public class DossConnManager
{
    public static final class ConnState extends Enum
    {

        private static final ConnState $VALUES[];
        public static final ConnState ConnFailed;
        public static final ConnState ConnWifi;
        public static final ConnState Conned;
        public static final ConnState NoConn;
        public static final ConnState NoPassword;

        public static ConnState valueOf(String s)
        {
            return (ConnState)Enum.valueOf(com/ximalaya/ting/android/fragment/device/doss/DossConnManager$ConnState, s);
        }

        public static ConnState[] values()
        {
            return (ConnState[])$VALUES.clone();
        }

        static 
        {
            NoConn = new ConnState("NoConn", 0);
            ConnWifi = new ConnState("ConnWifi", 1);
            NoPassword = new ConnState("NoPassword", 2);
            Conned = new ConnState("Conned", 3);
            ConnFailed = new ConnState("ConnFailed", 4);
            $VALUES = (new ConnState[] {
                NoConn, ConnWifi, NoPassword, Conned, ConnFailed
            });
        }

        private ConnState(String s, int i)
        {
            super(s, i);
        }
    }


    public static final String TAG = com/ximalaya/ting/android/fragment/device/doss/DossConnManager.getSimpleName();
    private static DossConnManager mMyWifiConnManager = null;
    private Activity mActivity;
    private volatile ConnState mNowConnState;
    private SharedPreferencesUtil mSharedPreferencesUtil;
    private WifiManager mWiFiManager;

    private DossConnManager(Activity activity)
    {
        Object obj = activity;
        if (activity == null)
        {
            obj = MainTabActivity2.mainTabActivity;
        }
        mWiFiManager = (WifiManager)(WifiManager)((Activity) (obj)).getSystemService("wifi");
        mSharedPreferencesUtil = SharedPreferencesUtil.getInstance(((Activity) (obj)).getApplicationContext());
        mActivity = ((Activity) (obj));
    }

    public static DossConnManager getInstance(Activity activity)
    {
        if (mMyWifiConnManager != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/fragment/device/doss/DossConnManager;
        JVM INSTR monitorenter ;
        mMyWifiConnManager = new DossConnManager(activity);
        com/ximalaya/ting/android/fragment/device/doss/DossConnManager;
        JVM INSTR monitorexit ;
_L2:
        return mMyWifiConnManager;
        activity;
        com/ximalaya/ting/android/fragment/device/doss/DossConnManager;
        JVM INSTR monitorexit ;
        throw activity;
    }

    public void conn()
    {
    }

    public ConnState getNowConnState()
    {
        return mNowConnState;
    }

    public void setNowConnState(ConnState connstate)
    {
        mNowConnState = connstate;
    }

    public void setScanResult2Conn(ScanResult scanresult)
    {
    }

}
