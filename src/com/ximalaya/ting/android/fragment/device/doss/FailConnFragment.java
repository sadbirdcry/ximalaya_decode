// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.conn.ConnectingFragment;
import com.ximalaya.ting.android.fragment.device.shu.WifiPwInputFragment2;

public class FailConnFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    private ImageView mBackImg;
    private String mDeviceType;
    private RelativeLayout mFailConnLayout;
    private String mPassword;
    private Button retry;
    private TextView topTextView;

    public FailConnFragment()
    {
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            mDeviceType = bundle.getString("device");
        }
        if (bundle != null)
        {
            mPassword = bundle.getString("password");
        }
    }

    private void onRetry()
    {
        if (!mDeviceType.equals(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss.toString())) goto _L2; else goto _L1
_L1:
        Bundle bundle = new Bundle();
        bundle.putString("type", "doss");
        bundle.putString("password", mPassword);
        startFragment(com/ximalaya/ting/android/fragment/device/conn/ConnectingFragment, bundle);
_L4:
        finish();
        return;
_L2:
        if (mDeviceType.equals(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao.toString()))
        {
            startFragment(com/ximalaya/ting/android/fragment/device/shu/WifiPwInputFragment2, null);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        topTextView = (TextView)mFailConnLayout.findViewById(0x7f0a00ae);
        topTextView.setText("\u9047\u5230\u4E00\u4E9B\u95EE\u9898");
        retry = (Button)mFailConnLayout.findViewById(0x7f0a034d);
        retry.setOnClickListener(this);
        mBackImg = (ImageView)mFailConnLayout.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(this);
        initData();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362637: 
            onRetry();
            return;

        case 2131363611: 
            mActivity.onBackPressed();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mFailConnLayout = (RelativeLayout)layoutinflater.inflate(0x7f0300b0, null);
        return mFailConnLayout;
    }
}
