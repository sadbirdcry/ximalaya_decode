// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import android.os.Bundle;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonListOthersFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonTFListSubMainFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonTFMainFragment;
import java.util.List;

public class DossTFMainFragment extends CommonTFMainFragment
{

    public DossTFMainFragment()
    {
    }

    protected void initData()
    {
        super.initData();
        mDirectionName.add("\u5176\u4ED6");
    }

    protected boolean toTFListFragment(String s)
    {
        if (!super.toTFListFragment(s))
        {
            if (s.equals("\u559C\u9A6C\u62C9\u96C5\u7684\u4E0B\u8F7D"))
            {
                Bundle bundle = new Bundle();
                bundle.putString("directionName", s);
                startFragment(com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonTFListSubMainFragment, bundle);
                return true;
            }
            if (s.equals("\u5176\u4ED6"))
            {
                Bundle bundle1 = new Bundle();
                bundle1.putString("directionName", s);
                startFragment(com/ximalaya/ting/android/fragment/device/dlna/common/fragment/CommonListOthersFragment, bundle1);
                return true;
            }
        }
        return false;
    }
}
