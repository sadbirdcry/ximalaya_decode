// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import com.ximalaya.model.mediamanager.MediaInfo;
import com.ximalaya.model.mediamanager.Task;
import com.ximalaya.model.mediamanager.TaskList;
import com.ximalaya.ting.android.fragment.device.callback.ActionModel;
import com.ximalaya.ting.android.fragment.device.dlna.CommonDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseDownloadModule;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;
import org.teleal.cling.support.mediamanager.callback.CreateDownloadTask.CreateDownloadTaskConstants;

public class DossUtils
{

    public static final String ALL_PAUSE = "\u5168\u90E8\u6682\u505C";
    public static final String ALL_STRAT = "\u5168\u90E8\u7EE7\u7EED";
    public static final String DIRECTION_NAME = "directionName";
    public static final String DOWNLOAD = "\u559C\u9A6C\u62C9\u96C5\u7684\u4E0B\u8F7D";
    public static final String FINISHED_DOWNLOAD = "\u5DF2\u4E0B\u8F7D";
    public static int NUM_DOWNLOAD = 0;
    public static int NUM_OTHERS = 0;
    public static final String OTHERS = "\u5176\u4ED6";
    private static String P_LOCK_SEARCH_XIMALAYA = "P_LOCK_SEARCH_XIMALAYA";
    public static final int TASK_QUEUE_DELETE = 6;
    public static final int TASK_QUEUE_DONE = 3;
    public static final int TASK_QUEUE_ERROR = -1;
    public static final int TASK_QUEUE_JUMP = 4;
    public static final int TASK_QUEUE_PAUSE = 2;
    public static final int TASK_QUEUE_READY = 0;
    public static final int TASK_QUEUE_RUN = 1;
    public static final int TASK_QUEUE_START = 5;
    public static final String UNFINISHED_DOWNLOAD = "\u6B63\u5728\u4E0B\u8F7D";
    public static int mDeviceEndNum = -9999;
    public static int mDeviceStartNum = 9999;
    public static String playListName = "";

    public DossUtils()
    {
    }

    public static String getPlayListName(Context context)
    {
        if (TextUtils.isEmpty(playListName))
        {
            try
            {
                playListName = ((TelephonyManager)context.getSystemService("phone")).getDeviceId();
            }
            // Misplaced declaration of an exception variable
            catch (Context context)
            {
                playListName = "PlayListTmp";
            }
        }
        return playListName;
    }

    public static void goDownload(Context context, CommonDeviceItem commondeviceitem, BaseDownloadModule basedownloadmodule, List list, String s, com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype)
    {
        TaskList tasklist = new TaskList();
        int i = 0;
        while (i < list.size()) 
        {
            com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)list.get(i);
            Task task = new Task();
            String s3 = track.getDownloadUrl();
            String as[] = s3.split("\\.");
            String s2 = "";
            String s1;
            if (devicetype == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi)
            {
                s1 = track.getTitle().replaceAll("\\\\", "").replaceAll("/", "").replaceAll(":", "").replaceAll("\\*", "").replaceAll("\\?", "").replaceAll("\"", "").replaceAll("<", "").replaceAll(">", "").replaceAll("|", "");
            } else
            {
                s1 = ToolUtil.md5(track.getTitle()).substring(8, 24);
            }
            if (as.length > 0)
            {
                s2 = as[as.length - 1];
            }
            if (TextUtils.isEmpty(s2))
            {
                task.fileName = s1;
            } else
            {
                task.fileName = (new StringBuilder()).append(s1).append(".").append(s2).toString();
            }
            task.url = s3;
            task.mediaInfo.album = (new StringBuilder()).append(track.getAlbumTitle()).append("").toString();
            task.mediaInfo.title = track.getTitle();
            task.mediaInfo.artist = track.getNickname();
            if (!TextUtils.isEmpty(s))
            {
                task.extSetDir = 1;
                task.directory = s;
            }
            tasklist.tasks.add(task);
            i++;
        }
        if (tasklist.tasks != null && tasklist.tasks.size() > 0)
        {
            tasklist.number = tasklist.tasks.size();
        }
        list = CreateDownloadTaskConstants.getTaskContext(tasklist);
        basedownloadmodule.createDownloadTask(ActionModel.createModel(new _cls1(devicetype, context)));
    }

    public static boolean isSearchXimalayaOnly(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean(P_LOCK_SEARCH_XIMALAYA, true);
    }

    public static void setSearchXimalayaOnly(boolean flag, Context context)
    {
        SharedPreferencesUtil.getInstance(context).saveBoolean(P_LOCK_SEARCH_XIMALAYA, flag);
    }

    static 
    {
        NUM_DOWNLOAD = 0;
        NUM_OTHERS = 0;
    }

    private class _cls1 extends CreateDownloadTaskActionCallback
    {

        final Context val$context;
        final com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType val$deviceType;

        public void failure(ActionInvocation actioninvocation, UpnpResponse upnpresponse, String s)
        {
            CustomToast.showToast(MyApplication.a(), "\u52A0\u5165\u4E0B\u8F7D\u961F\u5217\u5931\u8D25\uFF0C\u8BF7\u91CD\u8BD5", 0);
            Logger.d("doss", "CreateDownloadTask Failure");
        }

        public void success(ActionInvocation actioninvocation)
        {
            actioninvocation = deviceType;
            com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype = deviceType;
            if (actioninvocation == com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi)
            {
                Logger.d("doss", "CreateDownloadTask Success");
                CustomToast.showToast(MyApplication.a(), "\u5DF2\u6210\u529F\u52A0\u5165\u8212\u514B\u667A\u80FD\u7AE5\u8BDD\u6545\u4E8B\u673A\u4E0B\u8F7D\u961F\u5217", 0);
                return;
            } else
            {
                Logger.d("doss", "CreateDownloadTask Success");
                CustomToast.showToast(MyApplication.a(), (new StringBuilder()).append("\u5DF2\u6210\u529F\u52A0\u5165").append(MyDeviceManager.getInstance(context).getDossName()).append("\u4E0B\u8F7D\u961F\u5217").toString(), 0);
                return;
            }
        }

        _cls1(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype, Context context1)
        {
            deviceType = devicetype;
            context = context1;
            super(final_service, final_s);
        }
    }

}
