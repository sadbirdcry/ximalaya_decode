// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonSettingFragment;
import com.ximalaya.ting.android.fragment.device.dlna.common.module.CommonUpdateModule;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseUpdateModule;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.doss:
//            DossUtils

public class DossSettingFragment extends CommonSettingFragment
    implements android.view.View.OnClickListener
{

    public DossSettingFragment()
    {
    }

    protected CommonUpdateModule getUpdateModule()
    {
        return (CommonUpdateModule)DlnaManager.getInstance(mCon).getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss).getModule(BaseUpdateModule.NAME);
    }

    protected boolean isSearchXimalayaOnly()
    {
        return DossUtils.isSearchXimalayaOnly(mCon);
    }

    protected void setSearchXimalayaOnly(boolean flag)
    {
        DossUtils.setSearchXimalayaOnly(flag, mCon);
    }
}
