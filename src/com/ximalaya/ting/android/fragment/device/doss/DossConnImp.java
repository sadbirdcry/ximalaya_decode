// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import android.app.Activity;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import com.androidwiimusdk.library.smartlinkver2.EasylinkBroadcastReceiver;
import com.androidwiimusdk.library.smartlinkver2.IEasylinkSearchExecutor;
import com.androidwiimusdk.library.smartlinkver2.OnLinkingListener;
import com.ximalaya.ting.android.fragment.device.BaseConnectorImp;
import com.ximalaya.ting.android.fragment.device.ControlConfigure;
import com.ximalaya.ting.android.fragment.device.DeviceBindingListFragment;
import com.ximalaya.ting.android.fragment.device.IConnCallBack;
import com.ximalaya.ting.android.fragment.device.dlna.CoreController;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import java.util.Timer;
import java.util.TimerTask;
import org.teleal.cling.controlpoint.ControlPoint;
import org.teleal.cling.model.message.header.EASYLINKHeader;

public class DossConnImp extends BaseConnectorImp
{
    class SampleEaasylinkSearchExecutor
        implements IEasylinkSearchExecutor
    {

        final DossConnImp this$0;

        public void executeEasylinkSearch()
        {
            ControlPoint controlpoint = getControlPoint();
            if (controlpoint != null)
            {
                controlpoint.search(new EASYLINKHeader());
            }
        }

        SampleEaasylinkSearchExecutor()
        {
            this$0 = DossConnImp.this;
            super();
        }
    }


    private static final String TAG = "doss";
    IConnCallBack callBack;
    EasylinkBroadcastReceiver easylink;
    final OnLinkingListener linkingListener = new _cls2();
    Activity mActivity;
    ControlConfigure mControlConfigure;
    private TimerTask task;
    private Timer timer;

    public DossConnImp(Activity activity, IConnCallBack iconncallback)
    {
        super(activity, iconncallback);
        easylink = new EasylinkBroadcastReceiver();
        callBack = iconncallback;
        mActivity = activity;
        mControlConfigure = new ControlConfigure();
        configControl();
    }

    private void cancelTimer()
    {
        if (timer != null)
        {
            timer.cancel();
        }
    }

    private void configControl()
    {
        mControlConfigure = new ControlConfigure();
        mControlConfigure.mConnedType = 1;
        mControlConfigure.mConnedFragment = com/ximalaya/ting/android/fragment/device/DeviceBindingListFragment;
        mControlConfigure.mFailedType = 0;
        mControlConfigure.mFailedFragment = null;
    }

    public void cancelConnect()
    {
        easylink.onStopEasylink();
    }

    public void connect(String s)
    {
        task = new _cls1();
        if (timer != null)
        {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(task, 60000L);
        easylink.onCreate(mActivity.getApplication());
        easylink.setEasylinkSearchExecutor(new SampleEaasylinkSearchExecutor());
        easylink.onStartEasylink(mActivity.getApplicationContext(), s, linkingListener);
    }

    public ControlConfigure getControlConfigure()
    {
        return mControlConfigure;
    }

    public ControlPoint getControlPoint()
    {
        CoreController corecontroller = DlnaManager.getInstance(mActivity).getCoreController();
        if (corecontroller != null)
        {
            return corecontroller.getControlPoint();
        } else
        {
            return null;
        }
    }

    public String getDeviceName()
    {
label0:
        {
            WifiInfo wifiinfo = ((WifiManager)mActivity.getSystemService("wifi")).getConnectionInfo();
            String s = "";
            if (wifiinfo != null)
            {
                s = wifiinfo.getSSID();
                if (s == null)
                {
                    break label0;
                }
                s = s.replaceAll("\"", "");
            }
            return s;
        }
        return "\u672A\u627E\u5230WiFi";
    }

    public String getPassword()
    {
        return null;
    }

    public boolean isNeedPw()
    {
        return false;
    }


    private class _cls2
        implements OnLinkingListener
    {

        final DossConnImp this$0;

        public void onLinkCompleted(final String arg0, final String arg1)
        {
            class _cls4
                implements Runnable
            {

                final _cls2 this$1;
                final String val$arg0;
                final String val$arg1;

                public void run()
                {
                    Logger.d("doss", (new StringBuilder()).append("DossConnImp onLinkCompleted:").append(arg0).append(",").append(arg1).toString());
                    cancelTimer();
                    if (MyDeviceManager.getInstance(mActivity).isDlnaInit())
                    {
                        DlnaManager.getInstance(mActivity).startScanThread(15);
                    }
                    callBack.onSuccuss();
                }

                _cls4()
                {
                    this$1 = _cls2.this;
                    arg0 = s;
                    arg1 = s1;
                    super();
                }
            }

            mActivity.runOnUiThread(new _cls4());
        }

        public void onSendFailure(final Exception e)
        {
            class _cls3
                implements Runnable
            {

                final _cls2 this$1;
                final Exception val$e;

                public void run()
                {
                    Logger.d("doss", (new StringBuilder()).append("DossConnImp onSendFailure e:").append(e.getMessage()).toString());
                    cancelTimer();
                    callBack.onFailed();
                }

                _cls3()
                {
                    this$1 = _cls2.this;
                    e = exception;
                    super();
                }
            }

            if (callBack != null && mActivity != null)
            {
                mActivity.runOnUiThread(new _cls3());
            }
        }

        public void onSending(final int arg0, final int arg1)
        {
            class _cls2
                implements Runnable
            {

                final _cls2 this$1;
                final int val$arg0;
                final int val$arg1;

                public void run()
                {
                    Logger.d("doss", (new StringBuilder()).append("DossConnImp onSending:").append(arg0).append(",").append(arg1).toString());
                    callBack.onConnecting((arg0 / arg1) * 100);
                }

                _cls2()
                {
                    this$1 = _cls2.this;
                    arg0 = i;
                    arg1 = j;
                    super();
                }
            }

            if (callBack != null && mActivity != null)
            {
                mActivity.runOnUiThread(new _cls2());
            }
        }

        public void onStart()
        {
        }

        public void onStop()
        {
        }

        public void onTimeout()
        {
            Logger.d("doss", "timeout #1");
            class _cls1
                implements Runnable
            {

                final _cls2 this$1;

                public void run()
                {
                    cancelTimer();
                    callBack.onFailed();
                }

                _cls1()
                {
                    this$1 = _cls2.this;
                    super();
                }
            }

            if (callBack != null && mActivity != null)
            {
                mActivity.runOnUiThread(new _cls1());
            }
        }

        _cls2()
        {
            this$0 = DossConnImp.this;
            super();
        }
    }


    private class _cls1 extends TimerTask
    {

        final DossConnImp this$0;

        public void run()
        {
            class _cls1
                implements Runnable
            {

                final _cls1 this$1;

                public void run()
                {
                    cancelConnect();
                    callBack.onFailed();
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            if (callBack != null && mActivity != null)
            {
                mActivity.runOnUiThread(new _cls1());
            }
        }

        _cls1()
        {
            this$0 = DossConnImp.this;
            super();
        }
    }

}
