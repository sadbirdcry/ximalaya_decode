// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import android.os.Bundle;
import com.ximalaya.ting.android.fragment.device.ConnectorFactory;
import com.ximalaya.ting.android.fragment.device.IConnector;
import com.ximalaya.ting.android.fragment.device.conn.ConnectingFragment;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceListFragment;

public class DossConnectingFragment extends ConnectingFragment
{

    public DossConnectingFragment()
    {
    }

    protected IConnector getConnector()
    {
        return ConnectorFactory.createConnector(getActivity(), ConnectorFactory.getTypeByName("doss"), this);
    }

    public void onSuccuss()
    {
        Bundle bundle = new Bundle();
        bundle.putInt(DeviceListFragment.deviceType, com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss.ordinal());
        removeTopFramentFromManageFragment();
        startFragment(com/ximalaya/ting/android/fragment/device/dlna/DeviceListFragment, bundle);
    }
}
