// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.device.ConnectorFactory;
import com.ximalaya.ting.android.fragment.device.IConnector;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.conn.PwInputFragment;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.doss:
//            DossConnFragment

public class DossPwInputFragment extends PwInputFragment
{

    public DossPwInputFragment()
    {
    }

    protected IConnector getConnector()
    {
        return ConnectorFactory.createConnector(getActivity(), ConnectorFactory.getTypeByName("doss"), this);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mDeviceIntro.setVisibility(0);
        mDeviceTypeTv.setText(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getDossName());
    }

    protected void toConn()
    {
        Logger.d("WIFI", (new StringBuilder()).append("the password is:").append(mPassword).toString());
        hideSoftInput();
        Bundle bundle = new Bundle();
        bundle.putString("password", mPassword);
        startFragment(com/ximalaya/ting/android/fragment/device/doss/DossConnFragment, bundle);
    }
}
