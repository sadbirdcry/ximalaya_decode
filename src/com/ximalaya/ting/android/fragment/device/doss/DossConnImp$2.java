// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.doss;

import android.app.Activity;
import com.androidwiimusdk.library.smartlinkver2.OnLinkingListener;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.doss:
//            DossConnImp

class this._cls0
    implements OnLinkingListener
{

    final DossConnImp this$0;

    public void onLinkCompleted(final String arg0, final String arg1)
    {
        class _cls4
            implements Runnable
        {

            final DossConnImp._cls2 this$1;
            final String val$arg0;
            final String val$arg1;

            public void run()
            {
                Logger.d("doss", (new StringBuilder()).append("DossConnImp onLinkCompleted:").append(arg0).append(",").append(arg1).toString());
                DossConnImp.access$000(this$0);
                if (MyDeviceManager.getInstance(mActivity).isDlnaInit())
                {
                    DlnaManager.getInstance(mActivity).startScanThread(15);
                }
                callBack.onSuccuss();
            }

            _cls4()
            {
                this$1 = DossConnImp._cls2.this;
                arg0 = s;
                arg1 = s1;
                super();
            }
        }

        mActivity.runOnUiThread(new _cls4());
    }

    public void onSendFailure(final Exception e)
    {
        class _cls3
            implements Runnable
        {

            final DossConnImp._cls2 this$1;
            final Exception val$e;

            public void run()
            {
                Logger.d("doss", (new StringBuilder()).append("DossConnImp onSendFailure e:").append(e.getMessage()).toString());
                DossConnImp.access$000(this$0);
                callBack.onFailed();
            }

            _cls3()
            {
                this$1 = DossConnImp._cls2.this;
                e = exception;
                super();
            }
        }

        if (callBack != null && mActivity != null)
        {
            mActivity.runOnUiThread(new _cls3());
        }
    }

    public void onSending(final int arg0, final int arg1)
    {
        class _cls2
            implements Runnable
        {

            final DossConnImp._cls2 this$1;
            final int val$arg0;
            final int val$arg1;

            public void run()
            {
                Logger.d("doss", (new StringBuilder()).append("DossConnImp onSending:").append(arg0).append(",").append(arg1).toString());
                callBack.onConnecting((arg0 / arg1) * 100);
            }

            _cls2()
            {
                this$1 = DossConnImp._cls2.this;
                arg0 = i;
                arg1 = j;
                super();
            }
        }

        if (callBack != null && mActivity != null)
        {
            mActivity.runOnUiThread(new _cls2());
        }
    }

    public void onStart()
    {
    }

    public void onStop()
    {
    }

    public void onTimeout()
    {
        Logger.d("doss", "timeout #1");
        class _cls1
            implements Runnable
        {

            final DossConnImp._cls2 this$1;

            public void run()
            {
                DossConnImp.access$000(this$0);
                callBack.onFailed();
            }

            _cls1()
            {
                this$1 = DossConnImp._cls2.this;
                super();
            }
        }

        if (callBack != null && mActivity != null)
        {
            mActivity.runOnUiThread(new _cls1());
        }
    }

    _cls1()
    {
        this$0 = DossConnImp.this;
        super();
    }
}
