// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.tingshubao.TingshuController;
import com.ximalaya.ting.android.fragment.device.doss.FailConnFragment;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.shu:
//            MyWifiConnManager2, MyWifiScan, MyWifiSelectFragment2

public class WifiPwInputFragment2 extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, MyWifiConnManager2.OnHandlerFinishListener
{
    class ConningDialog extends Dialog
    {

        TextView mTitle;
        final WifiPwInputFragment2 this$0;

        protected void onCreate(Bundle bundle)
        {
            super.onCreate(bundle);
            requestWindowFeature(1);
            setCancelable(true);
            setContentView(0x7f0300ae);
            getWindow().setLayout(-1, -1);
            mTitle = (TextView)findViewById(0x7f0a01ea);
            ((TextView)findViewById(0x7f0a00ae)).setText("\u8FDE\u63A5\u4E2D");
            class _cls1
                implements android.view.View.OnClickListener
            {

                final ConningDialog this$1;

                public void onClick(View view)
                {
                    mMyWifiConnManager2.setIsContinue(false);
                    cancel();
                }

                _cls1()
                {
                    this$1 = ConningDialog.this;
                    super();
                }
            }

            findViewById(0x7f0a071b).setOnClickListener(new _cls1());
        }

        protected void onStart()
        {
            super.onStart();
            mMyWifiConnManager2.setIsContinue(true);
        }

        protected void onStop()
        {
            super.onStop();
            mMyWifiConnManager2.setIsContinue(false);
        }

        public void setText(String s)
        {
            mTitle.setText(s);
        }

        public ConningDialog(Context context)
        {
            this$0 = WifiPwInputFragment2.this;
            super(context, 0x7f0b0027);
        }
    }

    public static interface OnItemSelectedListener
    {

        public abstract void onItemSelected(View view);
    }


    boolean isCipher;
    private ConningDialog mConningDialog;
    private TextView mDeviceTypeTv;
    private boolean mIsUseProgress;
    MyWifiConnManager2 mMyWifiConnManager2;
    private String mPassword;
    EditText mPwEditText;
    ImageView mSwitchBtn;
    TextView mTextRight;
    TextView mTopTv;
    TextView wifiName;

    public WifiPwInputFragment2()
    {
        isCipher = true;
        mIsUseProgress = true;
    }

    private void changeInputPwShow()
    {
        if (!isCipher)
        {
            mPwEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            return;
        } else
        {
            mPwEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            return;
        }
    }

    private MyWifiConnManager2.ConnState getNowState()
    {
        return mMyWifiConnManager2.getConnState();
    }

    private void toConn(boolean flag)
    {
        Logger.d("WIFI", (new StringBuilder()).append("the password is:").append(mPassword).toString());
        if (!mMyWifiConnManager2.isMyWifiScanOpenNetWork())
        {
            if (TextUtils.isEmpty(mPassword))
            {
                if (flag)
                {
                    showToast("\u5BC6\u7801\u4E3A\u7A7A\uFF0C\u8BF7\u91CD\u65B0\u8F93\u5165");
                }
                return;
            }
        } else
        {
            mPassword = "111111";
        }
        mMyWifiConnManager2.connAp2ClientNew(mPassword, new _cls1());
        showProgress();
    }

    public void closeProgress()
    {
        while (!mIsUseProgress || mConningDialog == null || !mConningDialog.isShowing()) 
        {
            return;
        }
        mConningDialog.dismiss();
        mConningDialog = null;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mMyWifiConnManager2 = ((TingshuController)DlnaManager.getInstance(mActivity.getApplicationContext()).getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao)).getMyWifiConnManager2();
        if (mMyWifiConnManager2.getMyWifiScanNow() != null)
        {
            wifiName.setText(mMyWifiConnManager2.getMyWifiScanNow().ssid);
        }
        if (mPassword == null)
        {
            mPassword = mMyWifiConnManager2.getDefultWifiPw();
        }
        toConn(false);
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362066: 
            if (isCipher)
            {
                mSwitchBtn.setImageDrawable(getResources().getDrawable(0x7f020356));
                isCipher = false;
            } else
            {
                mSwitchBtn.setImageDrawable(getResources().getDrawable(0x7f020357));
                isCipher = true;
            }
            changeInputPwShow();
            return;

        case 2131362068: 
            mPassword = mPwEditText.getEditableText().toString();
            mMyWifiConnManager2.setIsContinue(true);
            toConn(true);
            return;

        case 2131363605: 
            mActivity.onBackPressed();
            return;

        case 2131363606: 
            startFragment(com/ximalaya/ting/android/fragment/device/shu/MyWifiSelectFragment2, null);
            mMyWifiConnManager2.setIsContinue(true);
            finish();
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        Log.i("zzw", "onCreateView");
        layoutinflater = (RelativeLayout)layoutinflater.inflate(0x7f03002d, viewgroup, false);
        mTopTv = (TextView)layoutinflater.findViewById(0x7f0a00ae);
        mTextRight = (TextView)layoutinflater.findViewById(0x7f0a0716);
        mTextRight.setOnClickListener(this);
        mSwitchBtn = (ImageView)layoutinflater.findViewById(0x7f0a0112);
        mPwEditText = (EditText)layoutinflater.findViewById(0x7f0a0113);
        wifiName = (TextView)layoutinflater.findViewById(0x7f0a010e);
        mTopTv.setText("\u8FDE\u63A5\u7F51\u7EDC");
        mTextRight.setText("\u5207\u6362WiFi");
        mTextRight.setVisibility(8);
        mSwitchBtn.setOnClickListener(this);
        layoutinflater.findViewById(0x7f0a0114).setOnClickListener(this);
        layoutinflater.findViewById(0x7f0a0715).setOnClickListener(this);
        mDeviceTypeTv = (TextView)layoutinflater.findViewById(0x7f0a010b);
        mDeviceTypeTv.setText(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoName());
        return layoutinflater;
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    public void onHandlerFinish()
    {
        if (getNowState() == MyWifiConnManager2.ConnState.Conned)
        {
            mMyWifiConnManager2.onConnSuccess();
            goBackFragment(com/ximalaya/ting/android/fragment/device/dlna/DeviceListFragment);
            finish();
        } else
        {
            showToast("\u8FDE\u63A5\u5931\u8D25\uFF0C\u8BF7\u786E\u8BA4\u5BC6\u7801\u6B63\u786E");
            Bundle bundle = new Bundle();
            bundle.putString("device", com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao.toString());
            startFragment(com/ximalaya/ting/android/fragment/device/doss/FailConnFragment, bundle);
        }
        closeProgress();
    }

    public void onResume()
    {
        super.onResume();
    }

    public void onStartConn()
    {
        mPwEditText.setText("******");
        showProgress();
    }

    public void showProgress()
    {
        if (mIsUseProgress)
        {
            if (mConningDialog == null)
            {
                mConningDialog = new ConningDialog(mActivity);
            }
            mConningDialog.show();
            if (!mConningDialog.isShowing() && !mActivity.isFinishing())
            {
                mConningDialog.show();
                return;
            }
        }
    }

    private class _cls1
        implements IActionCallBack
    {

        final WifiPwInputFragment2 this$0;

        public void onFailed()
        {
            onHandlerFinish();
        }

        public void onSuccess(ActionModel actionmodel)
        {
            onHandlerFinish();
        }

        _cls1()
        {
            this$0 = WifiPwInputFragment2.this;
            super();
        }
    }

}
