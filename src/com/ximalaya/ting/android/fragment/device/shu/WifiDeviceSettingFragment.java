// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceUtil;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.util.CustomToast;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.shu:
//            WifiRenameFragment

public class WifiDeviceSettingFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    private ImageView mBackImg;
    private RelativeLayout mContentView;
    private TextView mDeviceName;
    private DlnaManager mDlnaManger;
    private RelativeLayout mIntroLayout;
    private MyDeviceManager mMyDeviceManager;
    private BaseDeviceItem mNowMyDeviceItem;
    private int mPosition;
    private RelativeLayout mRenameLayout;
    private TextView mTopTv;

    public WifiDeviceSettingFragment()
    {
    }

    private void initView()
    {
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mTopTv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mDeviceName = (TextView)mContentView.findViewById(0x7f0a02e8);
        mRenameLayout = (RelativeLayout)mContentView.findViewById(0x7f0a038b);
        mIntroLayout = (RelativeLayout)mContentView.findViewById(0x7f0a010a);
        mTopTv.setText("\u8BBE\u7F6E");
        mDeviceName.setText(DeviceUtil.getDeviceItemName(mNowMyDeviceItem));
        mBackImg.setOnClickListener(this);
        mRenameLayout.setOnClickListener(this);
        mIntroLayout.setOnClickListener(this);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mDlnaManger = DlnaManager.getInstance(mActivity);
        bundle = mDlnaManger.getOperationStroageModel();
        if (bundle == null)
        {
            showToast("\u5F53\u524D\u8BBE\u5907\u5DF2\u65AD\u5F00\uFF0C\u8BF7\u91CD\u8BD5");
            finish();
        }
        mNowMyDeviceItem = bundle.getNowDeviceItem();
        if (mNowMyDeviceItem == null)
        {
            showToast("\u5F53\u524D\u8BBE\u5907\u5DF2\u65AD\u5F00\uFF0C\u8BF7\u91CD\u8BD5");
            finish();
        }
        initView();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            finish();
            return;

        case 2131362699: 
            startFragment(com/ximalaya/ting/android/fragment/device/shu/WifiRenameFragment, null);
            return;

        case 2131362058: 
            view = MyDeviceManager.getInstance(mActivity.getApplicationContext()).getTingshubaoIntro();
            break;
        }
        if (!TextUtils.isEmpty(view))
        {
            Intent intent = new Intent(mActivity, com/ximalaya/ting/android/activity/web/WebActivityNew);
            intent.putExtra("ExtraUrl", view);
            mActivity.startActivity(intent);
            return;
        } else
        {
            CustomToast.showToast(mActivity, "\u656C\u8BF7\u671F\u5F85", 0);
            return;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300dc, null);
        return mContentView;
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    public void onResume()
    {
        super.onResume();
        if (mNowMyDeviceItem != null)
        {
            mDeviceName.setText(DeviceUtil.getDeviceItemName(mNowMyDeviceItem));
            return;
        } else
        {
            showToast("\u8FDE\u63A5\u5DF2\u65AD\u5F00");
            finish();
            return;
        }
    }
}
