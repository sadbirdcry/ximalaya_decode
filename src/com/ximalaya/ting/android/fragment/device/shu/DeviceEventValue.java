// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;


public class DeviceEventValue
{

    public static final int EVENT_META_CHANNEL_PLAY = 128;
    public static final int EVENT_META_CHANNEL_PLAY_NEXT = 32;
    public static final int EVENT_META_CHANNEL_PLAY_PRE = 64;
    public static final int EVENT_META_PLAY_NEXT = 8;
    public static final int EVENT_META_PLAY_PREV = 16;
    public static final int EVENT_STATE_PAUSE = 8192;
    public static final int EVENT_STATE_PLAY = 16384;
    public static final int EVENT_STATE_PLAYLISTNAME = 2304;
    public static final int EVENT_STATE_STOP = 32768;
    public static final int EVENT_STATE_TRACKCHANGE = 12288;
    public static final int EVENT_STATE_TRANSITIONING = 4096;
    public static final String NAME_ACTION = "Action";
    public static final String NAME_CURRENTINDEX = "CurrentIndex";
    public static final String NAME_CURRENTPAGE = "CurrentPage";
    public static final String NAME_CURRENTPLAYLISTNAME = "CurretPlayListName";
    public static final String NAME_CURRENTTRACK = "CurrentTrack";
    public static final String NAME_META = "CurrentTrackMetaData";
    public static final String NAME_STATE = "TransportState";
    public static final String VALUE_ACTION_ADD = "add";
    public static final String VALUE_ACTION_TASKRENEW = "task_renew";
    public static final String VALUE_ACTION_TASKSTATUS = "task_status";
    public static final String VALUE_META_CHANNEL_PLAY = "CHANNEL_PLAY";
    public static final String VALUE_META_CHANNEL_PLAY_NEXT = "CHANNEL_PLAY NEXT";
    public static final String VALUE_META_CHANNEL_PLAY_PRE = "CHANNEL_PLAY PREV";
    public static final String VALUE_META_NEXT = "NEXT";
    public static final String VALUE_META_PREV = "PREV";
    public static final String VALUE_STATE_PAUSE = "PAUSED_PLAYBACK";
    public static final String VALUE_STATE_PLAY = "PLAYING";
    public static final String VALUE_STATE_STOP = "STOPPED";
    public static final String VALUE_STATE_TRANSITIONING = "TRANSITIONING";

    public DeviceEventValue()
    {
    }
}
