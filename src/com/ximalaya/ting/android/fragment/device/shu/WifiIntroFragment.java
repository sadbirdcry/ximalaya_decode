// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.tingshubao.TingshuController;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.shu:
//            MyWifiSelectFragment2, MyWifiConnManager2

public class WifiIntroFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    protected static final String TAG = "wifi";
    private TextView goBlueTooth;
    private LinearLayout goSetting;
    private TextView goSettingTv;
    private ImageView mBackImg;
    private TextView mBtnRight;
    private RelativeLayout mContentView;
    private TextView mDeviceManagerTv;
    private ImageView mIntroIv1;
    private ImageView mIntroIv2;
    private TextView mIntroTv1;
    private TextView mIntroTv2;
    private TextView mIntroTv3;
    private TextView mIntroTvNum3;
    private MyWifiConnManager2 mMyWifiConnManager2;
    private LinearLayout mNext;
    private TextView top_tv;

    public WifiIntroFragment()
    {
    }

    private void expandHitRect(final View view)
    {
        mContentView.post(new _cls1());
    }

    private void initView()
    {
        top_tv = (TextView)(TextView)mContentView.findViewById(0x7f0a00ae);
        top_tv.setText("\u6DFB\u52A0\u65B0\u8BBE\u5907");
        android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)top_tv.getLayoutParams();
        layoutparams.addRule(13);
        top_tv.setLayoutParams(layoutparams);
        mIntroTv1 = (TextView)mContentView.findViewById(0x7f0a02ee);
        mIntroIv1 = (ImageView)mContentView.findViewById(0x7f0a02ef);
        mIntroTv2 = (TextView)mContentView.findViewById(0x7f0a0256);
        mIntroIv2 = (ImageView)mContentView.findViewById(0x7f0a02f3);
        mDeviceManagerTv = (TextView)mContentView.findViewById(0x7f0a03a3);
        mDeviceManagerTv.setText("\u4E0B\u4E00\u6B65");
        mIntroTv1.setText((new StringBuilder()).append("\u5C06").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoName()).append("\u63A5\u901A\u7535\u6E90\uFF0CWIFI\u6307\u793A\u706F\u7531\u84DD\u8272\u95EA\u4EAE\u8F6C\u4E3A\u767D\u8272\u5E38\u4EAE").toString());
        mIntroTv2.setText((new StringBuilder()).append("\u8FDB\u5165\u624B\u673A\u300C\u8BBE\u7F6E\u300D- \u300CWLAN\u300D\u4E2D\u8FDE\u63A5").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoName()).append("\u7684\u7F51\u7EDC\u70ED\u70B9\u300Ctingshubao-xxxx\u300D\uFF0C\u5BC6\u7801\uFF1A12345678\u3002\u8FDE\u63A5\u6210\u529F\u540E\u70B9\u51FB\u300C\u4E0B\u4E00\u6B65\u300D").toString());
        mIntroIv1.setImageResource(0x7f0205d2);
        mIntroIv2.setImageResource(0x7f0205d3);
        mBtnRight = (TextView)mContentView.findViewById(0x7f0a071c);
        mBtnRight.setVisibility(0);
        mBtnRight.setOnClickListener(this);
        mBtnRight.setText("\u8BF4\u660E\u4E66");
        goSetting = (LinearLayout)mContentView.findViewById(0x7f0a03a0);
        goSettingTv = (TextView)mContentView.findViewById(0x7f0a03a1);
        goSetting.setOnClickListener(this);
        goSettingTv.setOnClickListener(this);
        goBlueTooth = (TextView)mContentView.findViewById(0x7f0a0256);
        goBlueTooth.setOnClickListener(this);
        mNext = (LinearLayout)mContentView.findViewById(0x7f0a03a2);
        mNext.setOnClickListener(this);
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(this);
    }

    private void showWarning()
    {
        mActivity.runOnUiThread(new _cls2());
    }

    private void toWifiIntro()
    {
        String s = MyDeviceManager.getInstance(mActivity.getApplicationContext()).getTingshubaoIntro();
        if (!TextUtils.isEmpty(s))
        {
            Intent intent = new Intent(mActivity, com/ximalaya/ting/android/activity/web/WebActivityNew);
            intent.putExtra("ExtraUrl", s);
            mActivity.startActivity(intent);
            return;
        } else
        {
            CustomToast.showToast(mActivity, "\u656C\u8BF7\u671F\u5F85", 0);
            return;
        }
    }

    private void toWifiSelector()
    {
        startFragment(com/ximalaya/ting/android/fragment/device/shu/MyWifiSelectFragment2, null);
    }

    private void toggleWiFi()
    {
        startActivity(new Intent("android.settings.WIFI_SETTINGS"));
    }

    public void next(boolean flag)
    {
        if (mMyWifiConnManager2.getNowConnState() != MyWifiConnManager2.ConnState.Ap)
        {
            showWarning();
            return;
        } else
        {
            toWifiSelector();
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        DlnaManager dlnamanager = DlnaManager.getInstance(mActivity.getApplicationContext());
        TingshuController tingshucontroller = (TingshuController)dlnamanager.getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao);
        bundle = tingshucontroller;
        if (tingshucontroller == null)
        {
            bundle = dlnamanager.createTingshuController();
        }
        mMyWifiConnManager2 = bundle.getMyWifiConnManager2();
        initView();
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 5: default 56
    //                   2131362720: 87
    //                   2131362721: 82
    //                   2131362722: 57
    //                   2131363611: 69
    //                   2131363612: 77;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        return;
_L4:
        if (!ToolUtil.isFastClick())
        {
            next(false);
            return;
        }
          goto _L1
_L5:
        getActivity().onBackPressed();
        return;
_L6:
        toWifiIntro();
        return;
_L3:
        toggleWiFi();
        return;
_L2:
        toggleWiFi();
        return;
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300df, null);
        return mContentView;
    }

    public void onResume()
    {
        super.onResume();
        Logger.d("wifi", "XiMaoIntroFragment onResume");
    }


    private class _cls1
        implements Runnable
    {

        final WifiIntroFragment this$0;
        final View val$view;

        public void run()
        {
            if (isAdded())
            {
                Object obj = new Rect();
                View view1 = view;
                view1.getHitRect(((Rect) (obj)));
                int i = ToolUtil.dp2px(getActivity(), 10F);
                obj.right = ((Rect) (obj)).right + i;
                obj.left = ((Rect) (obj)).left - i;
                obj = new TouchDelegate(((Rect) (obj)), view1);
                if (android/view/View.isInstance(view1.getParent()))
                {
                    ((View)view1.getParent()).setTouchDelegate(((TouchDelegate) (obj)));
                }
            }
        }

        _cls1()
        {
            this$0 = WifiIntroFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final WifiIntroFragment this$0;

        public void run()
        {
            (new DialogBuilder(
// JavaClassFileOutputException: get_constant: invalid tag

        _cls2()
        {
            this$0 = WifiIntroFragment.this;
            super();
        }
    }

}
