// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;

public class WifiConnectFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    public static interface OnItemSelectedListener
    {

        public abstract void onItemSelected(View view);
    }


    boolean isCipher;
    ImageView switch_btn;
    TextView text_right;
    TextView top_tv;

    public WifiConnectFragment()
    {
        isCipher = true;
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362066: 
            break;
        }
        if (isCipher)
        {
            switch_btn.setImageDrawable(getResources().getDrawable(0x7f020356));
            isCipher = false;
            return;
        } else
        {
            switch_btn.setImageDrawable(getResources().getDrawable(0x7f020357));
            isCipher = true;
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        Log.i("zzw", "onCreateView");
        layoutinflater = (RelativeLayout)layoutinflater.inflate(0x7f03002d, viewgroup, false);
        top_tv = (TextView)layoutinflater.findViewById(0x7f0a00ae);
        text_right = (TextView)layoutinflater.findViewById(0x7f0a0716);
        switch_btn = (ImageView)layoutinflater.findViewById(0x7f0a0112);
        top_tv.setText("\u8FDE\u63A5Wifi");
        text_right.setText("\u5207\u6362WiFi");
        text_right.setVisibility(0);
        switch_btn.setOnClickListener(this);
        return layoutinflater;
    }
}
