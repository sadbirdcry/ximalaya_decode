// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ximalaya.ting.android.fragment.BaseFragment;

public class ReloadFragment extends BaseFragment
{
    public static interface Callback
    {

        public abstract void reload(View view);
    }


    private FragmentManager mFragmentManager;
    private View mRootView;

    public ReloadFragment()
    {
    }

    public static void hide(FragmentManager fragmentmanager, ReloadFragment reloadfragment)
    {
        if (fragmentmanager != null && reloadfragment != null)
        {
            fragmentmanager.beginTransaction().remove(reloadfragment).commitAllowingStateLoss();
        }
    }

    public static ReloadFragment show(FragmentManager fragmentmanager, int i)
    {
        ReloadFragment reloadfragment = new ReloadFragment();
        reloadfragment.setFragmentManager(fragmentmanager);
        fragmentmanager.beginTransaction().add(i, reloadfragment).commitAllowingStateLoss();
        return reloadfragment;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mRootView.setOnClickListener(new _cls1());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mRootView = layoutinflater.inflate(0x7f03017b, viewgroup, false);
        return mRootView;
    }

    public void setFragmentManager(FragmentManager fragmentmanager)
    {
        mFragmentManager = fragmentmanager;
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final ReloadFragment this$0;

        public void onClick(View view)
        {
            view = getParentFragment();
            if (view != null)
            {
                if (view instanceof Callback)
                {
                    mFragmentManager.beginTransaction().remove(ReloadFragment.this).commitAllowingStateLoss();
                    ((Callback)view).reload(mRootView);
                }
            } else
            if (getActivity() != null && (getActivity() instanceof Callback))
            {
                mFragmentManager.beginTransaction().remove(ReloadFragment.this).commitAllowingStateLoss();
                ((Callback)getActivity()).reload(mRootView);
                return;
            }
        }

        _cls1()
        {
            this$0 = ReloadFragment.this;
            super();
        }
    }

}
