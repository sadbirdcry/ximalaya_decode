// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.dlna.BaseBindableDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.BindCommandModel;
import com.ximalaya.ting.android.fragment.device.dlna.shuke.ShukeUtil;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm;
import com.ximalaya.ting.android.fragment.finding2.category.CategoryTagFragmentNew;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.shu:
//            CategoryFragment

public class BindingListAdapter extends BaseAdapter
{
    class ViewHolder
    {

        TextView mAlbumKeyName;
        LinearLayout mCollect;
        TextView mCollectName;
        TextView mCollectNum;
        ImageView mKeyNameImage;
        final BindingListAdapter this$0;

        ViewHolder()
        {
            this$0 = BindingListAdapter.this;
            super();
        }
    }


    int images[] = {
        0x7f0201c7, 0x7f0201c8, 0x7f0201c9, 0x7f0201ca, 0x7f0201cb, 0x7f0201cc
    };
    Context mContext;
    BaseBindableDeviceItem mDeviceItem;
    private BaseActivityLikeFragment mFragment;
    private LayoutInflater mInflater;

    public BindingListAdapter(Context context, BaseActivityLikeFragment baseactivitylikefragment, BaseBindableDeviceItem basebindabledeviceitem)
    {
        mContext = context;
        mDeviceItem = basebindabledeviceitem;
        mFragment = baseactivitylikefragment;
        mInflater = LayoutInflater.from(mContext);
    }

    private void toCategoryFragment(int i)
    {
        BindCommandModel bindcommandmodel;
        Bundle bundle;
        bindcommandmodel = new BindCommandModel();
        bundle = new Bundle();
        bundle.putInt("key", i);
        bundle.putBoolean("showFocus", false);
        static class _cls2
        {

            static final int $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[];

            static 
            {
                $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType = new int[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.values().length];
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.shukewifi.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    $SwitchMap$com$ximalaya$ting$android$fragment$device$MyDeviceManager$DeviceType[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.doss.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls2..SwitchMap.com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType[mDeviceItem.getDlnaType().ordinal()];
        JVM INSTR tableswitch 1 3: default 72
    //                   1 89
    //                   2 281
    //                   3 293;
           goto _L1 _L2 _L3 _L4
_L1:
        bindcommandmodel.mChannelId = i;
        mFragment.startFragment(com/ximalaya/ting/android/fragment/device/shu/CategoryFragment, bundle);
        return;
_L2:
        if (!ShukeUtil.isLockBind(mContext)) goto _L6; else goto _L5
_L5:
        Object obj;
        int j;
        obj = XiMaoComm.loadCategoryInfo(mContext);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_305;
        }
        j = 0;
_L9:
        if (j >= ((List) (obj)).size())
        {
            break MISSING_BLOCK_LABEL_305;
        }
        if (!((FindingCategoryModel)((List) (obj)).get(j)).getTitle().equals("\u513F\u7AE5")) goto _L8; else goto _L7
_L7:
        obj = (FindingCategoryModel)((List) (obj)).get(j);
_L10:
        if (obj != null)
        {
            bundle.putString("title", ((FindingCategoryModel) (obj)).getTitle());
            bundle.putString("categoryId", (new StringBuilder()).append(((FindingCategoryModel) (obj)).getId()).append("").toString());
            bundle.putString("categoryName", ((FindingCategoryModel) (obj)).getName());
            bundle.putString("contentType", "album");
            bundle.putBoolean("isSerialized", ((FindingCategoryModel) (obj)).isFinished());
            bundle.putInt("from", 7);
            bindcommandmodel.mChannelId = i;
            mFragment.startFragment(com/ximalaya/ting/android/fragment/finding2/category/CategoryTagFragmentNew, bundle);
            return;
        }
          goto _L1
_L8:
        j++;
          goto _L9
_L6:
        bundle.putInt("from", 7);
          goto _L1
_L3:
        bundle.putInt("from", 6);
          goto _L1
_L4:
        bundle.putInt("from", 7);
          goto _L1
        obj = null;
          goto _L10
    }

    public int getCount()
    {
        return mDeviceItem.getBaseBindableModel().getAlbumNum();
    }

    public Object getItem(int i)
    {
        return mDeviceItem.getBaseBindableModel().getAlbums().get(Integer.valueOf(i + 1));
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
        AlbumModel albummodel;
        if (view == null)
        {
            ViewHolder viewholder = new ViewHolder();
            view = mInflater.inflate(0x7f03011e, viewgroup, false);
            viewholder.mKeyNameImage = (ImageView)view.findViewById(0x7f0a0481);
            viewholder.mAlbumKeyName = (TextView)view.findViewById(0x7f0a0482);
            viewholder.mCollectName = (TextView)view.findViewById(0x7f0a0483);
            viewholder.mCollectNum = (TextView)view.findViewById(0x7f0a0484);
            viewholder.mCollect = (LinearLayout)view.findViewById(0x7f0a0154);
            view.setTag(viewholder);
            viewgroup = viewholder;
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        ((ViewHolder) (viewgroup)).mKeyNameImage.setImageResource(images[position]);
        ((ViewHolder) (viewgroup)).mAlbumKeyName.setText(mDeviceItem.getBaseBindableModel().getKeyName(position));
        ((ViewHolder) (viewgroup)).mCollect.setOnClickListener(new _cls1());
        albummodel = (AlbumModel)getItem(position);
        if (albummodel == null || TextUtils.isEmpty(albummodel.title))
        {
            ((ViewHolder) (viewgroup)).mCollectName.setText("\u672A\u83B7\u53D6\u6210\u529F");
            ((ViewHolder) (viewgroup)).mCollectNum.setText("0\u96C6");
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).mCollectName.setText(albummodel.title);
            ((ViewHolder) (viewgroup)).mCollectNum.setText((new StringBuilder()).append(albummodel.tracks).append("\u96C6").toString());
            return view;
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final BindingListAdapter this$0;
        final int val$position;

        public void onClick(View view)
        {
            toCategoryFragment(position + 1);
        }

        _cls1()
        {
            this$0 = BindingListAdapter.this;
            position = i;
            super();
        }
    }

}
