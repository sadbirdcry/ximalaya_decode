// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Utilities;
import java.util.HashMap;
import java.util.List;

public class CategoryAdapter extends BaseAdapter
{
    public static interface OnCellClickListener
    {

        public abstract void onCellClick(int i, FindingCategoryModel findingcategorymodel, View view);
    }

    private static class ViewHolder
    {

        public View leftCell;
        public ImageView leftCellIv;
        public TextView leftCellTv;
        public View rightCell;
        public ImageView rightCellIv;
        public TextView rightCellTv;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private static final int COLUMNS = 2;
    private static final int DIVIDER = 1;
    private static final int GROUP_ITEM_COUNT = 6;
    private static final int GROUP_ROWS = 3;
    private static final int ITEM = 0;
    private static HashMap sDefaultDrawables;
    private OnCellClickListener mCellClickListener;
    private Context mContext;
    private List mData;
    private LayoutInflater mInflater;

    public CategoryAdapter(Context context, List list)
    {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mData = list;
        buildDefaultDrawable();
    }

    private void buildDefaultDrawable()
    {
        if (sDefaultDrawables == null)
        {
            sDefaultDrawables = new HashMap();
            sDefaultDrawables.put("\u6709\u58F0\u5C0F\u8BF4", Integer.valueOf(0x7f020141));
            sDefaultDrawables.put("\u97F3\u4E50", Integer.valueOf(0x7f02013f));
            sDefaultDrawables.put("\u7EFC\u827A\u5A31\u4E50", Integer.valueOf(0x7f02012f));
            sDefaultDrawables.put("\u76F8\u58F0\u8BC4\u4E66", Integer.valueOf(0x7f02011a));
            sDefaultDrawables.put("\u6700\u65B0\u8D44\u8BAF", Integer.valueOf(0x7f020140));
            sDefaultDrawables.put("\u60C5\u611F\u751F\u6D3B", Integer.valueOf(0x7f02013a));
            sDefaultDrawables.put("\u5386\u53F2\u4EBA\u6587", Integer.valueOf(0x7f020137));
            sDefaultDrawables.put("\u5916\u8BED", Integer.valueOf(0x7f020139));
            sDefaultDrawables.put("\u57F9\u8BAD\u8BB2\u5EA7", Integer.valueOf(0x7f020148));
            sDefaultDrawables.put("\u767E\u5BB6\u8BB2\u575B", Integer.valueOf(0x7f020147));
            sDefaultDrawables.put("\u5E7F\u64AD\u5267", Integer.valueOf(0x7f020145));
            sDefaultDrawables.put("\u620F\u5267", Integer.valueOf(0x7f020119));
            sDefaultDrawables.put("\u513F\u7AE5", Integer.valueOf(0x7f020117));
            sDefaultDrawables.put("\u7535\u53F0", Integer.valueOf(0x7f020144));
            sDefaultDrawables.put("\u8D22\u7ECF", Integer.valueOf(0x7f020131));
            sDefaultDrawables.put("\u79D1\u6280", Integer.valueOf(0x7f020138));
            sDefaultDrawables.put("\u5065\u5EB7\u517B\u751F", Integer.valueOf(0x7f020135));
            sDefaultDrawables.put("\u6821\u56ED", Integer.valueOf(0x7f020146));
            sDefaultDrawables.put("\u6C7D\u8F66", Integer.valueOf(0x7f020116));
            sDefaultDrawables.put("\u65C5\u884C", Integer.valueOf(0x7f020149));
            sDefaultDrawables.put("\u7535\u5F71", Integer.valueOf(0x7f02013d));
            sDefaultDrawables.put("\u6E38\u620F", Integer.valueOf(0x7f020132));
            sDefaultDrawables.put("\u5176\u4ED6", Integer.valueOf(0x7f020142));
        }
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        }
        int i;
        if (mData.size() % 2 == 0)
        {
            i = mData.size() / 2;
        } else
        {
            i = mData.size() / 2 + 1;
        }
        if (mData.size() % 6 == 0)
        {
            return i + mData.size() / 6;
        } else
        {
            return i + (mData.size() / 6 + 1);
        }
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public int getItemViewType(int i)
    {
        while ((i - i / 4) * 2 >= mData.size() || (i + 1) % 4 == 0) 
        {
            return 1;
        }
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        int j = 0x7f020142;
        if (getItemViewType(i) == 1)
        {
            viewgroup = view;
            if (view == null)
            {
                viewgroup = new View(mContext);
                viewgroup.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, Utilities.dip2px(mContext, 10F)));
            }
            return viewgroup;
        }
        final int row;
        if (view == null)
        {
            view = mInflater.inflate(0x7f030123, viewgroup, false);
            viewgroup = new ViewHolder(null);
            viewgroup.leftCell = view.findViewById(0x7f0a048d);
            viewgroup.leftCellIv = (ImageView)view.findViewById(0x7f0a048f);
            viewgroup.leftCellTv = (TextView)view.findViewById(0x7f0a0490);
            viewgroup.rightCell = view.findViewById(0x7f0a0492);
            viewgroup.rightCellIv = (ImageView)view.findViewById(0x7f0a0494);
            viewgroup.rightCellTv = (TextView)view.findViewById(0x7f0a0495);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        if (i == 0)
        {
            view.setPadding(0, Utilities.dip2px(mContext, 10F), 0, 0);
        } else
        {
            view.setPadding(0, 0, 0, 0);
        }
        row = i - i / 4;
        if (row * 2 < mData.size())
        {
            final FindingCategoryModel leftModel = (FindingCategoryModel)mData.get(row * 2);
            ((ViewHolder) (viewgroup)).leftCellTv.setText(leftModel.getTitle());
            final FindingCategoryModel rightModel;
            if (sDefaultDrawables.get(leftModel.getTitle()) != null)
            {
                i = ((Integer)sDefaultDrawables.get(leftModel.getTitle())).intValue();
            } else
            {
                i = 0x7f020142;
            }
            ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).leftCellIv, leftModel.getCoverPath(), i);
            ((ViewHolder) (viewgroup)).leftCell.setOnClickListener(new _cls1());
        }
        if (row * 2 + 1 < mData.size())
        {
            ((ViewHolder) (viewgroup)).rightCell.setVisibility(0);
            rightModel = (FindingCategoryModel)mData.get(row * 2 + 1);
            ((ViewHolder) (viewgroup)).rightCellTv.setText(rightModel.getTitle());
            i = j;
            if (sDefaultDrawables.get(rightModel.getTitle()) != null)
            {
                i = ((Integer)sDefaultDrawables.get(rightModel.getTitle())).intValue();
            }
            ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).rightCellIv, rightModel.getCoverPath(), i);
            ((ViewHolder) (viewgroup)).rightCell.setOnClickListener(new _cls2());
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).rightCell.setVisibility(4);
            ((ViewHolder) (viewgroup)).rightCell.setClickable(false);
            return view;
        }
    }

    public int getViewTypeCount()
    {
        return 2;
    }

    public void setOnCellClickListener(OnCellClickListener oncellclicklistener)
    {
        mCellClickListener = oncellclicklistener;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final CategoryAdapter this$0;
        final FindingCategoryModel val$leftModel;
        final int val$row;

        public void onClick(View view)
        {
            if (mCellClickListener != null)
            {
                mCellClickListener.onCellClick(row * 2, leftModel, view);
            }
        }

        _cls1()
        {
            this$0 = CategoryAdapter.this;
            row = i;
            leftModel = findingcategorymodel;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final CategoryAdapter this$0;
        final FindingCategoryModel val$rightModel;
        final int val$row;

        public void onClick(View view)
        {
            if (mCellClickListener != null)
            {
                mCellClickListener.onCellClick(row * 2 + 1, rightModel, view);
            }
        }

        _cls2()
        {
            this$0 = CategoryAdapter.this;
            row = i;
            rightModel = findingcategorymodel;
            super();
        }
    }

}
