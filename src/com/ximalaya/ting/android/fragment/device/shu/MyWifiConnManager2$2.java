// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.setting.SettingControl;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.shu:
//            MyWifiConnManager2, MyWifiScan

class val.callBack extends MyAsyncTask
{

    final MyWifiConnManager2 this$0;
    final IActionCallBack val$callBack;
    final String val$pword;

    private boolean isConnRightNew(WifiInfo wifiinfo)
    {
        return wifiinfo.getBSSID().equalsIgnoreCase(MyWifiConnManager2.access$300(MyWifiConnManager2.this).bssid);
    }

    private boolean judgeDeviceConnState()
    {
        Logger.d("TingshuConn", "judgeDeviceConnState");
        HttpURLConnection httpurlconnection;
        BufferedReader bufferedreader;
        httpurlconnection = (HttpURLConnection)(new URL("http://192.168.103.1/cgi-bin/state.cgi")).openConnection();
        httpurlconnection.connect();
        bufferedreader = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()));
_L2:
        Object obj = bufferedreader.readLine();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_226;
        }
        Logger.d(MyWifiConnManager2.TAG, (new StringBuilder()).append("state lines:").append(((String) (obj))).toString());
        if (!((String) (obj)).contains("{")) goto _L2; else goto _L1
_L1:
        boolean flag;
        Logger.d("teshude", (new StringBuilder()).append("state lines:").append(((String) (obj))).toString());
        bufferedreader.close();
        httpurlconnection.disconnect();
        flag = TextUtils.isEmpty(((CharSequence) (obj)));
        if (flag)
        {
            return false;
        }
        obj = JSONObject.parseObject(((String) (obj)));
        if (!((String)((JSONObject) (obj)).get("status")).equalsIgnoreCase("OK"))
        {
            flag = false;
        } else
        {
            try
            {
                flag = ((String)((JSONObject) (obj)).get("wificonnecting")).equals("0");
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                return false;
            }
            if (!flag);
            flag = true;
        }
        break MISSING_BLOCK_LABEL_231;
        obj;
        ((Exception) (obj)).printStackTrace();
        Logger.d("excep", (new StringBuilder()).append("e:").append(((Exception) (obj)).getMessage()).toString());
        return false;
        obj = null;
          goto _L1
        return flag;
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        if (MyWifiConnManager2.access$300(MyWifiConnManager2.this) != null) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        int i;
        Logger.d("TingshuConn", "1");
        callDevice2Conn(MyWifiConnManager2.access$300(MyWifiConnManager2.this).ssid, val$pword);
        i = 0;
_L7:
        if (i >= 10) goto _L4; else goto _L3
_L3:
        if (judgeDeviceConnState()) goto _L6; else goto _L5
_L5:
        MyWifiConnManager2.access$400(MyWifiConnManager2.this).connectNewAP(val$pword, MyWifiConnManager2.access$300(MyWifiConnManager2.this).ssid, getTypeByCapabilites(MyWifiConnManager2.access$300(MyWifiConnManager2.this)));
_L4:
        long l;
        try
        {
            Thread.sleep(1000L);
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            avoid.printStackTrace();
        }
        l = System.currentTimeMillis();
        if (System.currentTimeMillis() - l < 60000L && isIsContinue())
        {
            avoid = MyWifiConnManager2.access$500(MyWifiConnManager2.this).getConnectionInfo();
            if (avoid == null)
            {
                break MISSING_BLOCK_LABEL_339;
            }
            android.net.tate tate = WifiInfo.getDetailedStateOf(avoid.getSupplicantState());
            if (tate == android.net.tate.CONNECTED || tate == android.net.tate.OBTAINING_IPADDR && avoid.getIpAddress() != 0)
            {
                if (!isConnRightNew(avoid))
                {
                    break MISSING_BLOCK_LABEL_339;
                }
                Logger.d("TingshuConn", "isConnRightNew");
                MyWifiConnManager2.access$602(MyWifiConnManager2.this, nnState.Conned);
                MyWifiConnManager2.access$700(MyWifiConnManager2.this, MyWifiConnManager2.access$300(MyWifiConnManager2.this).bssid, val$pword);
                i = 1;
            } else
            {
                if (tate != android.net.tate.FAILED)
                {
                    break MISSING_BLOCK_LABEL_339;
                }
                Logger.d("TingshuConn", "detailedState == NetworkInfo.DetailedState.FAILED");
                i = 2;
            }
            break MISSING_BLOCK_LABEL_220;
        }
        continue; /* Loop/switch isn't completed */
_L6:
        i++;
          goto _L7
_L9:
        if (i == 1)
        {
            Logger.d("TingshuConn", "connType ConnState.Conned");
            setConnState(nnState.Conned);
            return null;
        }
        if (i == 2)
        {
            Logger.d("TingshuConn", "connType ConnState.ConnFailed");
            setConnState(nnState.ConnFailed);
            MyWifiConnManager2.access$800(MyWifiConnManager2.this, MyWifiConnManager2.access$300(MyWifiConnManager2.this).bssid);
            return null;
        }
        try
        {
            Thread.sleep(4000L);
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            avoid.printStackTrace();
        }
        break MISSING_BLOCK_LABEL_104;
        i = 0;
        if (true) goto _L9; else goto _L8
_L8:
        if (true) goto _L1; else goto _L10
_L10:
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        super.onPostExecute(void1);
        Logger.d("wifi", "connAp2Client onPostExecute");
        Logger.d("TingshuConn", (new StringBuilder()).append("onPostExecute:").append(nnState.Conned).toString());
        if (MyWifiConnManager2.access$100(MyWifiConnManager2.this) != null)
        {
            MyWifiConnManager2.access$100(MyWifiConnManager2.this).onHandlerFinish();
        }
        if (MyWifiConnManager2.access$600(MyWifiConnManager2.this) == nnState.Conned)
        {
            CustomToast.showToast(MyWifiConnManager2.access$200(MyWifiConnManager2.this), "\u97F3\u7BB1\u8FDE\u63A5\u6210\u529F", 1000);
            if (val$callBack != null)
            {
                val$callBack.onSuccess(null);
            }
        } else
        if (val$callBack != null)
        {
            val$callBack.onSuccess(null);
            return;
        }
    }

    HandlerFinishListener()
    {
        this$0 = final_mywificonnmanager2;
        val$pword = s;
        val$callBack = IActionCallBack.this;
        super();
    }
}
