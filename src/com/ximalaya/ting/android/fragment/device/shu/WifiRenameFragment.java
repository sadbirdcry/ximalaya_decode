// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceUtil;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.model.OperationStorageModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseRenameModule;

public class WifiRenameFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{

    private final String RENAME_SUCCESS = (new StringBuilder()).append("\u91CD\u547D\u540D\u6210\u529F\uFF0C\u91CD\u542F").append(MyDeviceManager.getInstance(getActivity().getApplicationContext()).getTingshubaoName()).append("\u5373\u53EF\u751F\u6548").toString();
    private ImageView mBackImg;
    private RelativeLayout mContentView;
    private DlnaManager mDlnaManger;
    private EditText mEditTextName;
    private BaseDeviceItem mNowMyDeviceItem;
    private TextView mRightBtn;
    private TextView mTopTv;
    private BaseRenameModule renameModule;

    public WifiRenameFragment()
    {
    }

    private void hideSoftInput()
    {
        if (mEditTextName != null)
        {
            mEditTextName.clearFocus();
        }
        if (mCon != null)
        {
            ((InputMethodManager)mCon.getSystemService("input_method")).hideSoftInputFromWindow(mEditTextName.getWindowToken(), 0);
        }
    }

    private void initView()
    {
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mTopTv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mRightBtn = (TextView)mContentView.findViewById(0x7f0a071c);
        mRightBtn.setText("\u4FDD\u5B58");
        mRightBtn.setVisibility(0);
        mRightBtn.setOnClickListener(this);
        mEditTextName = (EditText)mContentView.findViewById(0x7f0a038c);
        mEditTextName.setHint(DeviceUtil.getDeviceItemName(mNowMyDeviceItem));
        mTopTv.setText("\u91CD\u547D\u540D");
        mBackImg.setOnClickListener(this);
    }

    private void openSoftInput()
    {
        mEditTextName.requestFocus();
        if (mCon != null)
        {
            ((InputMethodManager)mCon.getSystemService("input_method")).showSoftInput(mEditTextName, 0);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mDlnaManger = DlnaManager.getInstance(mActivity.getApplicationContext());
        bundle = mDlnaManger.getOperationStroageModel();
        renameModule = (BaseRenameModule)((BaseDlnaController)mDlnaManger.getDeviceListByType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao)).getModule(BaseRenameModule.NAME);
        if (bundle == null)
        {
            showToast("\u5F53\u524D\u8BBE\u5907\u5DF2\u65AD\u5F00\uFF0C\u8BF7\u91CD\u8BD5");
            finish();
        }
        mNowMyDeviceItem = bundle.getNowDeviceItem();
        if (mNowMyDeviceItem == null)
        {
            showToast("\u5F53\u524D\u8BBE\u5907\u5DF2\u65AD\u5F00\uFF0C\u8BF7\u91CD\u8BD5");
            finish();
        }
        initView();
        openSoftInput();
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            mActivity.onBackPressed();
            return;

        case 2131363612: 
            view = mEditTextName.getEditableText().toString();
            break;
        }
        renameModule.renameDevice(mNowMyDeviceItem, view, new _cls1());
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300db, null);
        return mContentView;
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    public void onPause()
    {
        super.onPause();
        hideSoftInput();
    }

    public void onResume()
    {
        super.onResume();
    }




}
