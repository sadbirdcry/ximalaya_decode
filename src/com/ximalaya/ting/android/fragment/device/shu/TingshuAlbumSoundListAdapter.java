// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.BaseListSoundsAdapter;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

public class TingshuAlbumSoundListAdapter extends BaseListSoundsAdapter
{

    public TingshuAlbumSoundListAdapter(Activity activity, List list)
    {
        super(activity, list);
    }

    protected void bindData(final SoundInfo info, final com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder holder)
    {
        holder.title.setText(info.title);
        b.a().a(info);
        holder.createTime.setText(ToolUtil.convertTime(info.create_at));
        holder.owner.setText((new StringBuilder()).append("by ").append(info.nickname).toString());
        TextView textview = holder.origin;
        String s;
        if (info.user_source == 1)
        {
            s = "\u539F\u521B";
        } else
        {
            s = "\u91C7\u96C6";
        }
        textview.setText(s);
        if (isPlaying(info.trackId))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        if (info.plays_counts > 0)
        {
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.plays_counts));
        } else
        {
            holder.playCount.setVisibility(8);
        }
        if (info.favorites_counts > 0)
        {
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.favorites_counts));
            holder.likeCount.setVisibility(0);
            if (info.is_favorited)
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f02008f), null, null, null);
            } else
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020091), null, null, null);
            }
        } else
        {
            holder.likeCount.setVisibility(8);
        }
        if (info.comments_counts > 0)
        {
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.comments_counts));
            holder.commentCount.setVisibility(0);
        } else
        {
            holder.commentCount.setVisibility(8);
        }
        if (info.duration > 0.0D)
        {
            holder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)info.duration)).toString());
            holder.duration.setVisibility(0);
        } else
        {
            holder.duration.setVisibility(8);
        }
        holder.cover.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(holder.cover, info.coverSmall, 0x7f0202de);
        holder.cover.setOnClickListener(new _cls1());
        holder.btn.setVisibility(8);
        holder.origin.setVisibility(8);
        holder.createTime.setVisibility(8);
    }

    protected volatile void bindData(Object obj, com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((SoundInfo)obj, viewholder);
    }

    public void setMargins(View view, int i, int j, int k, int l)
    {
        i = ToolUtil.dp2px(mContext, i);
        j = ToolUtil.dp2px(mContext, j);
        k = ToolUtil.dp2px(mContext, k);
        l = ToolUtil.dp2px(mContext, l);
        if (view.getLayoutParams() instanceof android.view.ViewGroup.MarginLayoutParams)
        {
            ((android.view.ViewGroup.MarginLayoutParams)view.getLayoutParams()).setMargins(i, j, k, l);
            view.requestLayout();
        }
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final TingshuAlbumSoundListAdapter this$0;
        final com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder val$holder;
        final SoundInfo val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, TingshuAlbumSoundListAdapter.this.holder.indexOf(info), info, TingshuAlbumSoundListAdapter.this.holder);
        }

        _cls1()
        {
            this$0 = TingshuAlbumSoundListAdapter.this;
            holder = viewholder;
            info = soundinfo;
            super();
        }
    }

}
