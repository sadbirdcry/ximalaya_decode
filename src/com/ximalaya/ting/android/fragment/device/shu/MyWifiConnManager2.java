// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.app.Activity;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.TextUtils;
import com.eshare.himusic.setupwizard.setwifi.ConfigurationSecurities;
import com.eshare.himusic.setupwizard.setwifi.Wifi;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.device.callback.IActionCallBack;
import com.ximalaya.ting.android.fragment.device.setting.SettingControl;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.util.CustomToast;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.shu:
//            MyWifiScan

public class MyWifiConnManager2
{
    public static final class ConnState extends Enum
    {

        private static final ConnState $VALUES[];
        public static final ConnState Ap;
        public static final ConnState ApFailed;
        public static final ConnState ConnFailed;
        public static final ConnState ConnWifi;
        public static final ConnState Conned;
        public static final ConnState NoConn;
        public static final ConnState NoDefaultNet;
        public static final ConnState NoPassword;

        public static ConnState valueOf(String s)
        {
            return (ConnState)Enum.valueOf(com/ximalaya/ting/android/fragment/device/shu/MyWifiConnManager2$ConnState, s);
        }

        public static ConnState[] values()
        {
            return (ConnState[])$VALUES.clone();
        }

        static 
        {
            NoConn = new ConnState("NoConn", 0);
            ConnWifi = new ConnState("ConnWifi", 1);
            Ap = new ConnState("Ap", 2);
            NoPassword = new ConnState("NoPassword", 3);
            NoDefaultNet = new ConnState("NoDefaultNet", 4);
            ApFailed = new ConnState("ApFailed", 5);
            Conned = new ConnState("Conned", 6);
            ConnFailed = new ConnState("ConnFailed", 7);
            $VALUES = (new ConnState[] {
                NoConn, ConnWifi, Ap, NoPassword, NoDefaultNet, ApFailed, Conned, ConnFailed
            });
        }

        private ConnState(String s, int i)
        {
            super(s, i);
        }
    }

    public static interface OnHandlerFinishListener
    {

        public abstract void onHandlerFinish();
    }


    private static final String DEVICE_DEFULT_IP = "192.168.103.1";
    public static final String TAG = com/ximalaya/ting/android/fragment/device/shu/MyWifiConnManager2.getSimpleName();
    private static MyWifiConnManager2 mMyWifiConnManager = null;
    private Activity mActivity;
    private ScanResult mDefaultDiviceScanResult;
    private ScanResult mDefaultWIFIScanResult;
    private boolean mIsContinue;
    private MyWifiScan mMyWifiScanNow;
    private volatile ConnState mNowConnState;
    private OnHandlerFinishListener mOnHandlerFinishListener;
    private SettingControl mSettingControl;
    private SharedPreferencesUtil mSharedPreferencesUtil;
    private WifiManager mWiFiManager;

    public MyWifiConnManager2(Activity activity)
    {
        mIsContinue = true;
        mWiFiManager = (WifiManager)(WifiManager)activity.getSystemService("wifi");
        mSharedPreferencesUtil = SharedPreferencesUtil.getInstance(activity.getApplicationContext());
        mActivity = activity;
    }

    private void deletePassword(String s)
    {
        mSharedPreferencesUtil.removeByKey(s);
    }

    public static boolean isOurDeviceBySSid(String s)
    {
        return s.contains("QAP") || s.contains("Epitome_") || s.contains("NBCAST-") || s.contains("HiMusic-") || s.contains("tingshubao-") || s.contains("ximalaya-sn00-tsb0");
    }

    private boolean isWifiValid(WifiInfo wifiinfo)
    {
        while (TextUtils.isEmpty(wifiinfo.getBSSID()) || !SupplicantState.isValidState(wifiinfo.getSupplicantState())) 
        {
            return false;
        }
        return true;
    }

    private void savePassword(String s, String s1)
    {
        mSharedPreferencesUtil.saveString(s, s1);
    }

    public void callDevice2Conn(String s, String s1)
    {
        Logger.d("TingshuConn", "callDevice2Conn");
        mWiFiManager.getConnectionInfo().getSSID();
        wifiControl("http://192.168.103.1/cgi-bin/connect.cgi", s, s1);
    }

    public void connAp2Client(ScanResult scanresult, String s)
    {
_L2:
        return;
        if (!isIsContinue() || !isConnValid()) goto _L2; else goto _L1
_L1:
        int i;
        Logger.d("thethread", (new StringBuilder()).append("mDefaultWIFIScanResult:").append(mDefaultWIFIScanResult).toString());
        Logger.d("thethread", (new StringBuilder()).append("mDefaultWIFIScanResult.SSID:").append(mDefaultWIFIScanResult.SSID).toString());
        callDevice2Conn(scanresult.SSID, s);
        WifiInfo wifiinfo;
        long l;
        try
        {
            Thread.sleep(1000L);
        }
        catch (InterruptedException interruptedexception)
        {
            interruptedexception.printStackTrace();
        }
        i = android.provider.Settings.Secure.getInt(mActivity.getContentResolver(), "wifi_num_open_networks_kept", 10);
        if (isOpenNetWork(scanresult))
        {
            Wifi.connectToNewNetwork(mActivity, mWiFiManager, scanresult, null, i);
        } else
        {
            Wifi.connectToNewNetwork(mActivity, mWiFiManager, scanresult, s, i);
        }
        l = System.currentTimeMillis();
_L9:
        if (System.currentTimeMillis() - l >= 20000L) goto _L4; else goto _L3
_L3:
        if (!isIsContinue()) goto _L2; else goto _L5
_L5:
        wifiinfo = mWiFiManager.getConnectionInfo();
        if (wifiinfo == null)
        {
            break MISSING_BLOCK_LABEL_345;
        }
        android.net.NetworkInfo.DetailedState detailedstate = WifiInfo.getDetailedStateOf(wifiinfo.getSupplicantState());
        if (detailedstate == android.net.NetworkInfo.DetailedState.CONNECTED || detailedstate == android.net.NetworkInfo.DetailedState.OBTAINING_IPADDR && wifiinfo.getIpAddress() != 0)
        {
            if (!isConnRight(wifiinfo))
            {
                break MISSING_BLOCK_LABEL_345;
            }
            mNowConnState = ConnState.Conned;
            savePassword(scanresult.BSSID, s);
            i = 1;
        } else
        {
            if (detailedstate != android.net.NetworkInfo.DetailedState.FAILED)
            {
                break MISSING_BLOCK_LABEL_345;
            }
            i = 2;
        }
_L10:
        if (i != 1) goto _L7; else goto _L6
_L6:
        setConnState(ConnState.Conned);
_L4:
        Logger.d(TAG, (new StringBuilder()).append("connectToNewNetwork is:").append(false).toString());
        return;
_L7:
        if (i != 2)
        {
            break; /* Loop/switch isn't completed */
        }
        setConnState(ConnState.ConnFailed);
        deletePassword(scanresult.BSSID);
        if (true) goto _L4; else goto _L8
_L8:
        try
        {
            Thread.sleep(300L);
        }
        catch (InterruptedException interruptedexception1)
        {
            interruptedexception1.printStackTrace();
        }
          goto _L9
        i = 0;
          goto _L10
    }

    public void connAp2Client(final String password)
    {
        if (mDefaultWIFIScanResult == null)
        {
            CustomToast.showToast(mActivity, "\u6CA1\u6709\u5B9A\u5236\u53EF\u7528\u7F51\u7EDC", 200);
            return;
        } else
        {
            (new _cls1()).myexec(new Void[0]);
            return;
        }
    }

    public void connAp2ClientNew(final String pword, final IActionCallBack callBack)
    {
        if (mSettingControl == null)
        {
            mSettingControl = new SettingControl(mActivity.getApplicationContext());
        }
        mWiFiManager.getConfiguredNetworks();
        (new _cls2()).myexec(new Void[0]);
    }

    public ConnState getConnState()
    {
        return mNowConnState;
    }

    public ScanResult getDefaultDeviceScanResult()
    {
        return mDefaultDiviceScanResult;
    }

    public String getDefultWifiPw()
    {
        if (mDefaultWIFIScanResult == null)
        {
            return null;
        } else
        {
            return getPassword(mDefaultWIFIScanResult.BSSID);
        }
    }

    public MyWifiScan getMyWifiScanNow()
    {
        return mMyWifiScanNow;
    }

    public ConnState getNowConnState()
    {
        WifiInfo wifiinfo = mWiFiManager.getConnectionInfo();
        Logger.d(TAG, (new StringBuilder()).append("judgeConnState:wifiInfo").append(wifiinfo).toString());
        if (isOurDeviceBySSid(wifiinfo.getSSID()))
        {
            Logger.d(TAG, "getNowConnState ConnState.Ap");
            return ConnState.Ap;
        }
        if (isWifiValid(wifiinfo))
        {
            Logger.d(TAG, "getNowConnState ConnState.ConnWifi");
            return ConnState.ConnWifi;
        } else
        {
            Logger.d(TAG, "getNowConnState ConnState.NoConn");
            return ConnState.NoConn;
        }
    }

    public WifiInfo getNowWifiInfo()
    {
        return mWiFiManager.getConnectionInfo();
    }

    public String getPassword(String s)
    {
        return mSharedPreferencesUtil.getString(s);
    }

    public ScanResult getScanResult2Conn()
    {
        return mDefaultWIFIScanResult;
    }

    public int getTypeByCapabilites(MyWifiScan mywifiscan)
    {
        mywifiscan = mywifiscan.capabilites;
        if (!mywifiscan.contains("NONE"))
        {
            if (mywifiscan.contains("WEP-PSK"))
            {
                return 1;
            }
            if (mywifiscan.contains("WPA_RSN"))
            {
                return 2;
            }
        }
        return 0;
    }

    public boolean isConnRight(WifiInfo wifiinfo)
    {
        String s = wifiinfo.getSSID();
        Logger.d(TAG, (new StringBuilder()).append("isConnRight:curSsid=").append(s).append(";mDefaultWIFIScanResult:").append(mDefaultWIFIScanResult.SSID).toString());
        return (new StringBuilder()).append("\"").append(mDefaultWIFIScanResult.SSID).append("\"").toString().equals(s) || mDefaultWIFIScanResult.SSID.equals(s) || mDefaultWIFIScanResult.BSSID.equals(wifiinfo.getBSSID());
    }

    public boolean isConnValid()
    {
        return mWiFiManager.getConnectionInfo().getSSID() != null;
    }

    public boolean isIsContinue()
    {
        return mIsContinue;
    }

    public boolean isMyWifiScanOpenNetWork()
    {
        while (mMyWifiScanNow == null || getTypeByCapabilites(mMyWifiScanNow) != 0) 
        {
            return false;
        }
        return true;
    }

    public boolean isOpenNetWork()
    {
        String s = Wifi.ConfigSec.getScanResultSecurity(mDefaultWIFIScanResult);
        return Wifi.ConfigSec.isOpenNetwork(s);
    }

    public boolean isOpenNetWork(ScanResult scanresult)
    {
        scanresult = Wifi.ConfigSec.getScanResultSecurity(scanresult);
        return Wifi.ConfigSec.isOpenNetwork(scanresult);
    }

    public boolean isWifiEnable()
    {
        switch (mWiFiManager.getWifiState())
        {
        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
        case 4: // '\004'
        default:
            return false;

        case 3: // '\003'
            return true;
        }
    }

    public void onConnSuccess()
    {
        if (mActivity != null && (mActivity instanceof MainTabActivity2))
        {
            mNowConnState = ConnState.NoConn;
            return;
        } else
        {
            CustomToast.showToast(mActivity, "\u7A0B\u5E8F\u9519\u8BEF\uFF0C\u8BF7\u624B\u52A8\u9000\u51FA", 200);
            return;
        }
    }

    public void removeOnHandlerFinishListener()
    {
        mOnHandlerFinishListener = null;
    }

    public void setConnState(ConnState connstate)
    {
        mNowConnState = connstate;
    }

    public void setDefaultDeviceScanResult(ScanResult scanresult)
    {
        mDefaultDiviceScanResult = scanresult;
    }

    public void setIsContinue(boolean flag)
    {
        mIsContinue = flag;
    }

    public void setMyWifiScanNow(MyWifiScan mywifiscan)
    {
        mMyWifiScanNow = mywifiscan;
    }

    public void setScanResult2Conn(ScanResult scanresult)
    {
        mDefaultWIFIScanResult = scanresult;
    }

    public void setmOnHandlerFinishListener(OnHandlerFinishListener onhandlerfinishlistener)
    {
        mOnHandlerFinishListener = onhandlerfinishlistener;
    }

    public void wifiControl(String s, String s1, String s2)
    {
        Logger.d(TAG, (new StringBuilder()).append("wifiControl:url=").append(s).append(";username=").append(s1).append(";password=").append(s2).toString());
        if (s != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        s1 = String.format((new StringBuilder()).append(s).append("?SSID=%s&passwd=%s").toString(), new Object[] {
            s1, s2
        });
        s = new DefaultHttpClient();
        s1 = new HttpGet(s1);
        s = s.execute(s1);
        if (s == null) goto _L1; else goto _L3
_L3:
        s1 = s.getStatusLine();
        Logger.d(TAG, (new StringBuilder()).append("wifiControl statusLine=").append(s1.getStatusCode()).toString());
        if (s1 == null) goto _L1; else goto _L4
_L4:
        if (s1.getStatusCode() != 200) goto _L1; else goto _L5
_L5:
        s = s.getEntity().getContent();
        s1 = new ByteArrayOutputStream();
        s2 = new byte[1024];
_L6:
        int i = s.read(s2);
        if (i == -1)
        {
            break MISSING_BLOCK_LABEL_224;
        }
        s1.write(s2, 0, i);
          goto _L6
        try
        {
            s2 = s1.toString();
            Logger.d("WIFI", (new StringBuilder()).append("wifiControl:result:").append(s2).toString());
            s.close();
            s1.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
        return;
    }










/*
    static ConnState access$602(MyWifiConnManager2 mywificonnmanager2, ConnState connstate)
    {
        mywificonnmanager2.mNowConnState = connstate;
        return connstate;
    }

*/



    private class _cls1 extends MyAsyncTask
    {

        final MyWifiConnManager2 this$0;
        final String val$password;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            connAp2Client(mDefaultWIFIScanResult, password);
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            super.onPostExecute(void1);
            Logger.d("wifi", "connAp2Client onPostExecute");
            if (mOnHandlerFinishListener != null)
            {
                mOnHandlerFinishListener.onHandlerFinish();
                CustomToast.showToast(mActivity, "\u97F3\u7BB1\u8FDE\u63A5\u6210\u529F", 1000);
            }
        }

        _cls1()
        {
            this$0 = MyWifiConnManager2.this;
            password = s;
            super();
        }
    }


    private class _cls2 extends MyAsyncTask
    {

        final MyWifiConnManager2 this$0;
        final IActionCallBack val$callBack;
        final String val$pword;

        private boolean isConnRightNew(WifiInfo wifiinfo)
        {
            return wifiinfo.getBSSID().equalsIgnoreCase(mMyWifiScanNow.bssid);
        }

        private boolean judgeDeviceConnState()
        {
            Logger.d("TingshuConn", "judgeDeviceConnState");
            HttpURLConnection httpurlconnection;
            BufferedReader bufferedreader;
            httpurlconnection = (HttpURLConnection)(new URL("http://192.168.103.1/cgi-bin/state.cgi")).openConnection();
            httpurlconnection.connect();
            bufferedreader = new BufferedReader(new InputStreamReader(httpurlconnection.getInputStream()));
_L2:
            Object obj = bufferedreader.readLine();
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_226;
            }
            Logger.d(MyWifiConnManager2.TAG, (new StringBuilder()).append("state lines:").append(((String) (obj))).toString());
            if (!((String) (obj)).contains("{")) goto _L2; else goto _L1
_L1:
            boolean flag;
            Logger.d("teshude", (new StringBuilder()).append("state lines:").append(((String) (obj))).toString());
            bufferedreader.close();
            httpurlconnection.disconnect();
            flag = TextUtils.isEmpty(((CharSequence) (obj)));
            if (flag)
            {
                return false;
            }
            obj = JSONObject.parseObject(((String) (obj)));
            if (!((String)((JSONObject) (obj)).get("status")).equalsIgnoreCase("OK"))
            {
                flag = false;
            } else
            {
                try
                {
                    flag = ((String)((JSONObject) (obj)).get("wificonnecting")).equals("0");
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    return false;
                }
                if (!flag);
                flag = true;
            }
            break MISSING_BLOCK_LABEL_231;
            obj;
            ((Exception) (obj)).printStackTrace();
            Logger.d("excep", (new StringBuilder()).append("e:").append(((Exception) (obj)).getMessage()).toString());
            return false;
            obj = null;
              goto _L1
            return flag;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            if (mMyWifiScanNow != null) goto _L2; else goto _L1
_L1:
            return null;
_L2:
            int i;
            Logger.d("TingshuConn", "1");
            callDevice2Conn(mMyWifiScanNow.ssid, pword);
            i = 0;
_L7:
            if (i >= 10) goto _L4; else goto _L3
_L3:
            if (judgeDeviceConnState()) goto _L6; else goto _L5
_L5:
            mSettingControl.connectNewAP(pword, mMyWifiScanNow.ssid, getTypeByCapabilites(mMyWifiScanNow));
_L4:
            long l;
            try
            {
                Thread.sleep(1000L);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
            l = System.currentTimeMillis();
            if (System.currentTimeMillis() - l < 60000L && isIsContinue())
            {
                avoid = mWiFiManager.getConnectionInfo();
                if (avoid == null)
                {
                    break MISSING_BLOCK_LABEL_339;
                }
                android.net.NetworkInfo.DetailedState detailedstate = WifiInfo.getDetailedStateOf(avoid.getSupplicantState());
                if (detailedstate == android.net.NetworkInfo.DetailedState.CONNECTED || detailedstate == android.net.NetworkInfo.DetailedState.OBTAINING_IPADDR && avoid.getIpAddress() != 0)
                {
                    if (!isConnRightNew(avoid))
                    {
                        break MISSING_BLOCK_LABEL_339;
                    }
                    Logger.d("TingshuConn", "isConnRightNew");
                    mNowConnState = ConnState.Conned;
                    savePassword(mMyWifiScanNow.bssid, pword);
                    i = 1;
                } else
                {
                    if (detailedstate != android.net.NetworkInfo.DetailedState.FAILED)
                    {
                        break MISSING_BLOCK_LABEL_339;
                    }
                    Logger.d("TingshuConn", "detailedState == NetworkInfo.DetailedState.FAILED");
                    i = 2;
                }
                break MISSING_BLOCK_LABEL_220;
            }
            continue; /* Loop/switch isn't completed */
_L6:
            i++;
              goto _L7
_L9:
            if (i == 1)
            {
                Logger.d("TingshuConn", "connType ConnState.Conned");
                setConnState(ConnState.Conned);
                return null;
            }
            if (i == 2)
            {
                Logger.d("TingshuConn", "connType ConnState.ConnFailed");
                setConnState(ConnState.ConnFailed);
                deletePassword(mMyWifiScanNow.bssid);
                return null;
            }
            try
            {
                Thread.sleep(4000L);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
            break MISSING_BLOCK_LABEL_104;
            i = 0;
            if (true) goto _L9; else goto _L8
_L8:
            if (true) goto _L1; else goto _L10
_L10:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            super.onPostExecute(void1);
            Logger.d("wifi", "connAp2Client onPostExecute");
            Logger.d("TingshuConn", (new StringBuilder()).append("onPostExecute:").append(ConnState.Conned).toString());
            if (mOnHandlerFinishListener != null)
            {
                mOnHandlerFinishListener.onHandlerFinish();
            }
            if (mNowConnState == ConnState.Conned)
            {
                CustomToast.showToast(mActivity, "\u97F3\u7BB1\u8FDE\u63A5\u6210\u529F", 1000);
                if (callBack != null)
                {
                    callBack.onSuccess(null);
                }
            } else
            if (callBack != null)
            {
                callBack.onSuccess(null);
                return;
            }
        }

        _cls2()
        {
            this$0 = MyWifiConnManager2.this;
            pword = s;
            callBack = iactioncallback;
            super();
        }
    }

}
