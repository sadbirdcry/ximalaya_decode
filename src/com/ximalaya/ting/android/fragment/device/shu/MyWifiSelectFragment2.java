// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.app.Activity;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseTingshuLinkModule;
import com.ximalaya.ting.android.fragment.device.dlna.tingshubao.TingshuController;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.shu:
//            MyWifiConnManager2, WifiPwInputFragment2, MyWifiScan

public class MyWifiSelectFragment2 extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    public class MyAdapter extends BaseAdapter
    {

        final MyWifiSelectFragment2 this$0;

        public int getCount()
        {
            return mScanResults.size();
        }

        public Object getItem(int i)
        {
            return mScanResults.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, final View myWifiScan, ViewGroup viewgroup)
        {
            viewgroup = myWifiScan;
            if (myWifiScan == null)
            {
                viewgroup = View.inflate(
// JavaClassFileOutputException: get_constant: invalid tag

        public MyAdapter()
        {
            this$0 = MyWifiSelectFragment2.this;
            super();
        }

        class _cls1
            implements android.view.View.OnClickListener
        {

            final MyAdapter this$1;
            final MyWifiScan val$myWifiScan;

            public void onClick(View view)
            {
                mMyWifiConnManager2.setIsContinue(true);
                toConn(myWifiScan);
            }

                _cls1()
                {
                    this$1 = MyAdapter.this;
                    myWifiScan = mywifiscan;
                    super();
                }
        }

    }


    private static String ENCODING = "GBK";
    public static final String PROTECT_WEB = "\u901A\u8FC7 WEB \u8FDB\u884C\u4FDD\u62A4";
    public static final String PROTECT_WPA = "\u901A\u8FC7 WEB WPA/WPA2 \u8FDB\u884C\u4FDD\u62A4";
    private MyAdapter mAdapter;
    private ImageView mBackImg;
    private RelativeLayout mContentView;
    private ListView mListView;
    private MyDeviceManager mMyDeviceManager;
    private MyWifiConnManager2 mMyWifiConnManager2;
    private WifiInfo mNowWifiInfo;
    private ProgressBar mProgress;
    private List mScanResults;
    private TextView mTopTv;
    private WifiManager mWiFiManager;

    public MyWifiSelectFragment2()
    {
    }

    private List handlerScanResults(List list)
    {
        if (mNowWifiInfo == null)
        {
            return mWiFiManager.getScanResults();
        }
        list = mWiFiManager.getScanResults();
        for (Iterator iterator = list.iterator(); iterator.hasNext();)
        {
            ScanResult scanresult = (ScanResult)iterator.next();
            if (scanresult.BSSID.equals(mNowWifiInfo.getBSSID()))
            {
                list.remove(scanresult);
                return list;
            }
        }

        return list;
    }

    private void initView()
    {
        mBackImg = (ImageView)mContentView.findViewById(0x7f0a071b);
        mBackImg.setOnClickListener(this);
        mTopTv = (TextView)mContentView.findViewById(0x7f0a00ae);
        mProgress = (ProgressBar)mContentView.findViewById(0x7f0a0392);
        mTopTv.setText("\u9009\u62E9\u7F51\u7EDC");
        mListView = (ListView)mContentView.findViewById(0x7f0a005c);
        LinearLayout linearlayout = (LinearLayout)LayoutInflater.from(getActivity()).inflate(0x7f030066, mListView, false);
        mListView.addHeaderView(linearlayout);
        mAdapter = new MyAdapter();
        mListView.setAdapter(mAdapter);
        mProgress.setVisibility(0);
        mListView.setVisibility(8);
    }

    private void toConn(MyWifiScan mywifiscan)
    {
        mMyWifiConnManager2.setMyWifiScanNow(mywifiscan);
        startFragment(com/ximalaya/ting/android/fragment/device/shu/WifiPwInputFragment2, null);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = (TingshuController)DlnaManager.getInstance(mActivity.getApplicationContext()).getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao);
        mMyWifiConnManager2 = bundle.getMyWifiConnManager2();
        mMyDeviceManager = MyDeviceManager.getInstance(mActivity);
        mNowWifiInfo = mMyWifiConnManager2.getNowWifiInfo();
        mWiFiManager = (WifiManager)mActivity.getSystemService("wifi");
        mScanResults = new ArrayList();
        initView();
        ((BaseTingshuLinkModule)bundle.getModule(BaseTingshuLinkModule.NAME)).getWifiList(new _cls1());
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131363611: 
            mActivity.onBackPressed();
            break;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = (RelativeLayout)layoutinflater.inflate(0x7f0300dd, null);
        return mContentView;
    }

    public void onResume()
    {
        super.onResume();
    }

    public void onStop()
    {
        super.onStop();
    }











}
