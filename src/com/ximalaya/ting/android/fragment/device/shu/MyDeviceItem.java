// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import com.ximalaya.activity.DeviceItem;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.sound.SoundInfoList;
import org.teleal.cling.model.meta.Device;
import org.teleal.cling.model.types.UDN;

public class MyDeviceItem
{
    public class ChannelInfo
    {

        public int albumId;
        public int channelId;
        final MyDeviceItem this$0;
        public long trackId;

        public ChannelInfo()
        {
            this$0 = MyDeviceItem.this;
            super();
        }
    }


    private ChannelInfo channelInfo;
    DeviceItem deviceItem;
    private AlbumModel mAlbumModel1;
    private AlbumModel mAlbumModel2;
    private AlbumModel mAlbumModel3;
    private AlbumModel mAlbumModel4;
    private AlbumModel mAlbumModel5;
    private AlbumModel mAlbumModel6;
    private com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType mDeviceType;
    private com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType mDlnaType;
    private SoundInfoList mSoundInfoList1;
    private SoundInfoList mSoundInfoList2;
    private SoundInfoList mSoundInfoList3;
    private SoundInfoList mSoundInfoList4;
    private SoundInfoList mSoundInfoList5;
    private SoundInfoList mSoundInfoList6;

    public MyDeviceItem(Device device)
    {
        mAlbumModel1 = null;
        mAlbumModel2 = null;
        mAlbumModel3 = null;
        mAlbumModel4 = null;
        mAlbumModel5 = null;
        mAlbumModel6 = null;
        deviceItem = new DeviceItem(device);
    }

    public transient MyDeviceItem(Device device, String as[])
    {
        mAlbumModel1 = null;
        mAlbumModel2 = null;
        mAlbumModel3 = null;
        mAlbumModel4 = null;
        mAlbumModel5 = null;
        mAlbumModel6 = null;
        deviceItem = new DeviceItem(device, as);
    }

    public AlbumModel getAlbumModel(int i)
    {
        switch (i)
        {
        default:
            return null;

        case 1: // '\001'
            return mAlbumModel1;

        case 2: // '\002'
            return mAlbumModel2;

        case 3: // '\003'
            return mAlbumModel3;

        case 4: // '\004'
            return mAlbumModel4;

        case 5: // '\005'
            return mAlbumModel5;

        case 6: // '\006'
            return mAlbumModel6;
        }
    }

    public ChannelInfo getChannelInfo()
    {
        return channelInfo;
    }

    public Device getDevice()
    {
        return deviceItem.getDevice();
    }

    public com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getDlnaType()
    {
        return mDlnaType;
    }

    public String getIpAddress()
    {
        return deviceItem.getIpAddress();
    }

    public SoundInfoList getSoundInfoList(int i)
    {
        switch (i)
        {
        default:
            return null;

        case 1: // '\001'
            return mSoundInfoList1;

        case 2: // '\002'
            return mSoundInfoList2;

        case 3: // '\003'
            return mSoundInfoList3;

        case 4: // '\004'
            return mSoundInfoList4;
        }
    }

    public UDN getUdn()
    {
        return deviceItem.getUdn();
    }

    public AlbumModel getmAlbumModel1()
    {
        return mAlbumModel1;
    }

    public AlbumModel getmAlbumModel2()
    {
        return mAlbumModel2;
    }

    public AlbumModel getmAlbumModel3()
    {
        return mAlbumModel3;
    }

    public AlbumModel getmAlbumModel4()
    {
        return mAlbumModel4;
    }

    public AlbumModel getmAlbumModel5()
    {
        return mAlbumModel5;
    }

    public AlbumModel getmAlbumModel6()
    {
        return mAlbumModel6;
    }

    public com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType getmDeviceType()
    {
        return mDeviceType;
    }

    public SoundInfoList getmSoundInfoList5()
    {
        return mSoundInfoList5;
    }

    public SoundInfoList getmSoundInfoList6()
    {
        return mSoundInfoList6;
    }

    public void setAlbumModel(AlbumModel albummodel, int i)
    {
        switch (i)
        {
        default:
            return;

        case 1: // '\001'
            mAlbumModel1 = albummodel;
            return;

        case 2: // '\002'
            mAlbumModel2 = albummodel;
            return;

        case 3: // '\003'
            mAlbumModel3 = albummodel;
            return;

        case 4: // '\004'
            mAlbumModel4 = albummodel;
            return;

        case 5: // '\005'
            mAlbumModel5 = albummodel;
            return;

        case 6: // '\006'
            mAlbumModel6 = albummodel;
            break;
        }
    }

    public void setChannelInfo(ChannelInfo channelinfo)
    {
        channelInfo = channelinfo;
    }

    public void setDlnaType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype)
    {
        mDlnaType = devicetype;
    }

    public void setIpAddress(String s)
    {
        deviceItem.setIpAddress(s);
    }

    public void setSoundInfoList(SoundInfoList soundinfolist, int i)
    {
        switch (i)
        {
        default:
            return;

        case 1: // '\001'
            mSoundInfoList1 = soundinfolist;
            return;

        case 2: // '\002'
            mSoundInfoList2 = soundinfolist;
            return;

        case 3: // '\003'
            mSoundInfoList3 = soundinfolist;
            return;

        case 4: // '\004'
            mSoundInfoList4 = soundinfolist;
            break;
        }
    }

    public void setmAlbumModel1(AlbumModel albummodel)
    {
        mAlbumModel1 = albummodel;
    }

    public void setmAlbumModel2(AlbumModel albummodel)
    {
        mAlbumModel2 = albummodel;
    }

    public void setmAlbumModel3(AlbumModel albummodel)
    {
        mAlbumModel3 = albummodel;
    }

    public void setmAlbumModel4(AlbumModel albummodel)
    {
        mAlbumModel4 = albummodel;
    }

    public void setmAlbumModel5(AlbumModel albummodel)
    {
        mAlbumModel5 = albummodel;
    }

    public void setmAlbumModel6(AlbumModel albummodel)
    {
        mAlbumModel6 = albummodel;
    }

    public void setmDeviceType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype)
    {
        mDeviceType = devicetype;
    }

    public void setmSoundInfoList5(SoundInfoList soundinfolist)
    {
        mSoundInfoList5 = soundinfolist;
    }

    public void setmSoundInfoList6(SoundInfoList soundinfolist)
    {
        mSoundInfoList6 = soundinfolist;
    }
}
