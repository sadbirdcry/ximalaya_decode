// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.List;

public class CategoryLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "mobile/discovery/v1/categories";
    private List mData;

    public CategoryLoader(Context context)
    {
        super(context);
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((List)obj);
    }

    public void deliverResult(List list)
    {
        super.deliverResult(list);
        mData = list;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public List loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).add("picVersion", "10");
        ((RequestParams) (obj)).add("scale", "2");
        obj = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/discovery/v1/categories").toString(), ((RequestParams) (obj)), true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            try
            {
                obj = JSONObject.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
                if (((JSONObject) (obj)).getIntValue("ret") == 0)
                {
                    obj = ((JSONObject) (obj)).getString("list");
                    if (!TextUtils.isEmpty(((CharSequence) (obj))))
                    {
                        mData = FindingCategoryModel.getListFromJson(((String) (obj)));
                    }
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        if (mData == null || mData.size() <= 0)
        {
            String s = FileUtils.readAssetFileData(getContext(), "finding/category.json");
            if (!TextUtils.isEmpty(s))
            {
                mData = FindingCategoryModel.getListFromJson(s);
            }
        }
        return mData;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
