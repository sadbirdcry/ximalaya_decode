// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.shu;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceUtil;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoSearchFragment;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.shu:
//            CategoryLoader, CategoryAdapter, ReloadFragment

public class CategoryFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener, ReloadFragment.Callback
{

    private int key;
    private CategoryAdapter mAdapter;
    private int mFrom;
    private ListView mListView;
    private Loader mLoader;
    private boolean showFocus;

    public CategoryFragment()
    {
        mFrom = -1;
        key = -1;
        showFocus = true;
    }

    private void clearRef()
    {
        mListView.setAdapter(null);
        mListView = null;
    }

    private void initView()
    {
        setTitleText("\u9009\u62E9\u4E13\u8F91");
        fragmentBaseContainerView.findViewById(0x7f0a071b).setOnClickListener(this);
        fragmentBaseContainerView.findViewById(0x7f0a0062).setOnClickListener(this);
    }

    public static CategoryFragment newInstance()
    {
        return new CategoryFragment();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initView();
        mLoader = getLoaderManager().initLoader(0, null, this);
        if (getArguments().containsKey("from"))
        {
            mFrom = getArguments().getInt("from");
        } else
        {
            mFrom = -1;
        }
        if (getArguments().containsKey("key"))
        {
            key = getArguments().getInt("key");
        } else
        {
            key = -1;
        }
        if (MyDeviceUtil.isFromDeviceDownload(mFrom))
        {
            fragmentBaseContainerView.findViewById(0x7f0a0062).setVisibility(8);
        }
    }

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 2: default 32
    //                   2131361890: 48
    //                   2131363611: 33;
           goto _L1 _L2 _L3
_L1:
        return;
_L3:
        if (mActivity != null)
        {
            mActivity.onBackPressed();
            return;
        }
          goto _L1
_L2:
        view = new Bundle();
        view.putInt("key", key);
        view.putInt("from", mFrom);
        startFragment(com/ximalaya/ting/android/fragment/device/ximao/XiMaoSearchFragment, view);
        return;
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        return new CategoryLoader(getActivity());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
label0:
        {
            layoutinflater = layoutinflater.inflate(0x7f030004, viewgroup, false);
            mListView = (ListView)layoutinflater.findViewById(0x7f0a0064);
            fragmentBaseContainerView = layoutinflater;
            if (getArguments() != null)
            {
                if (getArguments().containsKey("from"))
                {
                    mFrom = getArguments().getInt("from");
                } else
                {
                    mFrom = -1;
                }
                if (getArguments().containsKey("key"))
                {
                    key = getArguments().getInt("key");
                } else
                {
                    key = -1;
                }
                if (!getArguments().containsKey("showFocus"))
                {
                    break label0;
                }
                showFocus = getArguments().getBoolean("showFocus");
            }
            return layoutinflater;
        }
        showFocus = true;
        return layoutinflater;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        if (list != null && list.size() > 0)
        {
            mAdapter = new CategoryAdapter(getActivity(), list);
            mAdapter.setOnCellClickListener(new _cls1());
            mListView.setAdapter(mAdapter);
            return;
        } else
        {
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(fragmentBaseContainerView, fragmentBaseContainerView);
            return;
        }
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
    }

    public void onStart()
    {
        super.onStart();
    }

    public void onStop()
    {
        super.onStop();
    }

    public void onViewCreated(View view, Bundle bundle)
    {
        super.onViewCreated(view, bundle);
    }

    public void onViewStateRestored(Bundle bundle)
    {
        super.onViewStateRestored(bundle);
    }

    public void reload(View view)
    {
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(0, null, this);
            return;
        } else
        {
            mLoader = getLoaderManager().restartLoader(0, null, this);
            return;
        }
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
    }




    private class _cls1
        implements CategoryAdapter.OnCellClickListener
    {

        final CategoryFragment this$0;

        public void onCellClick(int i, FindingCategoryModel findingcategorymodel, View view)
        {
            view = new Bundle();
            view.putString("title", findingcategorymodel.getTitle());
            view.putString("categoryId", (new StringBuilder()).append(findingcategorymodel.getId()).append("").toString());
            view.putString("categoryName", findingcategorymodel.getName());
            view.putString("contentType", findingcategorymodel.getContentType());
            view.putBoolean("isSerialized", findingcategorymodel.isFinished());
            view.putInt("from", mFrom);
            view.putInt("key", key);
            view.putBoolean("showFocus", showFocus);
            startFragment(com/ximalaya/ting/android/fragment/finding2/category/CategoryTagFragmentNew, view);
        }

        _cls1()
        {
            this$0 = CategoryFragment.this;
            super();
        }
    }

}
