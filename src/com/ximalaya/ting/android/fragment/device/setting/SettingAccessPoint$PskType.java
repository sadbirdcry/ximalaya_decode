// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.setting;


// Referenced classes of package com.ximalaya.ting.android.fragment.device.setting:
//            SettingAccessPoint

static final class  extends Enum
{

    private static final WPA_WPA2 $VALUES[];
    public static final WPA_WPA2 UNKNOWN;
    public static final WPA_WPA2 WPA;
    public static final WPA_WPA2 WPA2;
    public static final WPA_WPA2 WPA_WPA2;

    public static  valueOf(String s)
    {
        return ()Enum.valueOf(com/ximalaya/ting/android/fragment/device/setting/SettingAccessPoint$PskType, s);
    }

    public static [] values()
    {
        return ([])$VALUES.clone();
    }

    static 
    {
        UNKNOWN = new <init>("UNKNOWN", 0);
        WPA = new <init>("WPA", 1);
        WPA2 = new <init>("WPA2", 2);
        WPA_WPA2 = new <init>("WPA_WPA2", 3);
        $VALUES = (new .VALUES[] {
            UNKNOWN, WPA, WPA2, WPA_WPA2
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
