// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.setting;

import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.setting:
//            SettingControl

private class <init> extends Handler
{

    private int mRetry;
    final SettingControl this$0;

    void forceScan()
    {
        removeMessages(0);
        sendEmptyMessage(0);
    }

    public void handleMessage(Message message)
    {
        if (SettingControl.access$200(SettingControl.this).startScan())
        {
            mRetry = 0;
        } else
        {
            int i = mRetry + 1;
            mRetry = i;
            if (i >= 3)
            {
                mRetry = 0;
                return;
            }
        }
        sendEmptyMessageDelayed(0, 10000L);
    }

    void pause()
    {
        mRetry = 0;
        removeMessages(0);
    }

    void resume()
    {
        if (!hasMessages(0))
        {
            sendEmptyMessage(0);
        }
    }

    private ()
    {
        this$0 = SettingControl.this;
        super();
        mRetry = 0;
    }

    mRetry(mRetry mretry)
    {
        this();
    }
}
