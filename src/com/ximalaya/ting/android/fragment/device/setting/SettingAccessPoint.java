// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.setting;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import java.util.BitSet;

public class SettingAccessPoint
    implements Comparable
{
    static final class PskType extends Enum
    {

        private static final PskType $VALUES[];
        public static final PskType UNKNOWN;
        public static final PskType WPA;
        public static final PskType WPA2;
        public static final PskType WPA_WPA2;

        public static PskType valueOf(String s)
        {
            return (PskType)Enum.valueOf(com/ximalaya/ting/android/fragment/device/setting/SettingAccessPoint$PskType, s);
        }

        public static PskType[] values()
        {
            return (PskType[])$VALUES.clone();
        }

        static 
        {
            UNKNOWN = new PskType("UNKNOWN", 0);
            WPA = new PskType("WPA", 1);
            WPA2 = new PskType("WPA2", 2);
            WPA_WPA2 = new PskType("WPA_WPA2", 3);
            $VALUES = (new PskType[] {
                UNKNOWN, WPA, WPA2, WPA_WPA2
            });
        }

        private PskType(String s, int i)
        {
            super(s, i);
        }
    }


    public static final int SECURITY_EAP = 3;
    public static final int SECURITY_NONE = 0;
    public static final int SECURITY_PSK = 2;
    public static final int SECURITY_WEP = 1;
    static final String TAG = "SettingsAccessPoint";
    String bssid;
    private WifiConfiguration mConfig;
    private WifiInfo mInfo;
    private int mRssi;
    private ScanResult mScanResult;
    private android.net.NetworkInfo.DetailedState mState;
    int networkId;
    PskType pskType;
    int security;
    String ssid;
    boolean wpsAvailable;

    SettingAccessPoint(ScanResult scanresult)
    {
        wpsAvailable = false;
        pskType = PskType.UNKNOWN;
        loadResult(scanresult);
    }

    SettingAccessPoint(WifiConfiguration wificonfiguration)
    {
        wpsAvailable = false;
        pskType = PskType.UNKNOWN;
        loadConfig(wificonfiguration);
    }

    static String convertToQuotedString(String s)
    {
        return (new StringBuilder()).append("\"").append(s).append("\"").toString();
    }

    private static PskType getPskType(ScanResult scanresult)
    {
        boolean flag = scanresult.capabilities.contains("WPA-PSK");
        boolean flag1 = scanresult.capabilities.contains("WPA2-PSK");
        if (flag1 && flag)
        {
            return PskType.WPA_WPA2;
        }
        if (flag1)
        {
            return PskType.WPA2;
        }
        if (flag)
        {
            return PskType.WPA;
        } else
        {
            Log.w("SettingsAccessPoint", (new StringBuilder()).append("Received abnormal flag string: ").append(scanresult.capabilities).toString());
            return PskType.UNKNOWN;
        }
    }

    private static int getSecurity(ScanResult scanresult)
    {
        if (scanresult.capabilities.contains("WEP"))
        {
            return 1;
        }
        if (scanresult.capabilities.contains("PSK"))
        {
            return 2;
        }
        return !scanresult.capabilities.contains("EAP") ? 0 : 3;
    }

    static int getSecurity(WifiConfiguration wificonfiguration)
    {
        byte byte0 = 1;
        if (wificonfiguration.allowedKeyManagement.get(1))
        {
            byte0 = 2;
        } else
        {
            if (wificonfiguration.allowedKeyManagement.get(2) || wificonfiguration.allowedKeyManagement.get(3))
            {
                return 3;
            }
            if (wificonfiguration.wepKeys[0] == null)
            {
                return 0;
            }
        }
        return byte0;
    }

    private void loadConfig(WifiConfiguration wificonfiguration)
    {
        String s;
        if (wificonfiguration.SSID == null)
        {
            s = "";
        } else
        {
            s = removeDoubleQuotes(wificonfiguration.SSID);
        }
        ssid = s;
        bssid = wificonfiguration.BSSID;
        security = getSecurity(wificonfiguration);
        networkId = wificonfiguration.networkId;
        mRssi = 0x7fffffff;
        mConfig = wificonfiguration;
    }

    private void loadResult(ScanResult scanresult)
    {
        ssid = scanresult.SSID;
        bssid = scanresult.BSSID;
        security = getSecurity(scanresult);
        boolean flag;
        if (security != 3 && scanresult.capabilities.contains("WPS"))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        wpsAvailable = flag;
        if (security == 2)
        {
            pskType = getPskType(scanresult);
        }
        networkId = -1;
        mRssi = scanresult.level;
        mScanResult = scanresult;
    }

    static String removeDoubleQuotes(String s)
    {
        int i = s.length();
        String s1 = s;
        if (i > 1)
        {
            s1 = s;
            if (s.charAt(0) == '"')
            {
                s1 = s;
                if (s.charAt(i - 1) == '"')
                {
                    s1 = s.substring(1, i - 1);
                }
            }
        }
        return s1;
    }

    public int compareTo(SettingAccessPoint settingaccesspoint)
    {
        int i = -1;
        if (mInfo == settingaccesspoint.mInfo) goto _L2; else goto _L1
_L1:
        if (mInfo == null) goto _L4; else goto _L3
_L3:
        return i;
_L4:
        return 1;
_L2:
        if ((mRssi ^ settingaccesspoint.mRssi) >= 0)
        {
            break; /* Loop/switch isn't completed */
        }
        if (mRssi == 0x7fffffff)
        {
            return 1;
        }
        if (true) goto _L3; else goto _L5
_L5:
        if ((networkId ^ settingaccesspoint.networkId) >= 0)
        {
            break; /* Loop/switch isn't completed */
        }
        if (networkId == -1)
        {
            return 1;
        }
        if (true) goto _L3; else goto _L6
_L6:
        int j = WifiManager.compareSignalLevel(settingaccesspoint.mRssi, mRssi);
        i = j;
        if (j == 0)
        {
            return ssid.compareToIgnoreCase(settingaccesspoint.ssid);
        }
        if (true) goto _L3; else goto _L7
_L7:
    }

    public volatile int compareTo(Object obj)
    {
        return compareTo((SettingAccessPoint)obj);
    }

    WifiConfiguration getConfig()
    {
        return mConfig;
    }

    WifiInfo getInfo()
    {
        return mInfo;
    }

    int getLevel()
    {
        if (mRssi == 0x7fffffff)
        {
            return -1;
        } else
        {
            return WifiManager.calculateSignalLevel(mRssi, 4);
        }
    }

    public int getSecurityType()
    {
        return security;
    }

    android.net.NetworkInfo.DetailedState getState()
    {
        return mState;
    }

    void update(WifiInfo wifiinfo, android.net.NetworkInfo.DetailedState detailedstate)
    {
        boolean flag1 = true;
        boolean flag = true;
        if (wifiinfo != null && networkId != -1 && networkId == wifiinfo.getNetworkId())
        {
            if (mInfo != null)
            {
                flag = false;
            }
            mRssi = wifiinfo.getRssi();
            mInfo = wifiinfo;
            mState = detailedstate;
        } else
        if (mInfo != null)
        {
            mInfo = null;
            mState = null;
            flag = flag1;
        } else
        {
            flag = false;
        }
        if (!flag);
    }

    boolean update(ScanResult scanresult)
    {
        if (ssid.equals(scanresult.SSID) && security == getSecurity(scanresult))
        {
            if (WifiManager.compareSignalLevel(scanresult.level, mRssi) > 0)
            {
                mRssi = scanresult.level;
            }
            if (security == 2)
            {
                pskType = getPskType(scanresult);
            }
            return true;
        } else
        {
            return false;
        }
    }
}
