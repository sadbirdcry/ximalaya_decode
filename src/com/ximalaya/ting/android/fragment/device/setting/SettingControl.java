// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.setting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

// Referenced classes of package com.ximalaya.ting.android.fragment.device.setting:
//            SettingAccessPoint, SettingInputMethodNotify, settingNotify

public class SettingControl
{
    private class Multimap
    {

        private HashMap store;
        final SettingControl this$0;

        List getAll(Object obj)
        {
            obj = (List)store.get(obj);
            if (obj != null)
            {
                return ((List) (obj));
            } else
            {
                return Collections.emptyList();
            }
        }

        void put(Object obj, Object obj1)
        {
            List list = (List)store.get(obj);
            Object obj2 = list;
            if (list == null)
            {
                obj2 = new ArrayList(3);
                store.put(obj, obj2);
            }
            ((List) (obj2)).add(obj1);
        }

        private Multimap()
        {
            this$0 = SettingControl.this;
            super();
            store = new HashMap();
        }

        Multimap(_cls1 _pcls1)
        {
            this();
        }
    }

    private class SettingScanner extends Handler
    {

        private int mRetry;
        final SettingControl this$0;

        void forceScan()
        {
            removeMessages(0);
            sendEmptyMessage(0);
        }

        public void handleMessage(Message message)
        {
            if (mWifiManager.startScan())
            {
                mRetry = 0;
            } else
            {
                int i = mRetry + 1;
                mRetry = i;
                if (i >= 3)
                {
                    mRetry = 0;
                    return;
                }
            }
            sendEmptyMessageDelayed(0, 10000L);
        }

        void pause()
        {
            mRetry = 0;
            removeMessages(0);
        }

        void resume()
        {
            if (!hasMessages(0))
            {
                sendEmptyMessage(0);
            }
        }

        private SettingScanner()
        {
            this$0 = SettingControl.this;
            super();
            mRetry = 0;
        }

        SettingScanner(_cls1 _pcls1)
        {
            this();
        }
    }


    public static final int INVALID_NETWORK_ID_CH = -1;
    private static final int WIFI_RESCAN_INTERVAL_MS = 10000;
    private String Dhcpinfo;
    private SettingInputMethodNotify SettingInputMethodNotifylistener;
    private final String WIFI_ENTER_PASSWORD = "com.changhong.launcher.CONNECT";
    private final String WIFI_INPUTMETHOD_QUIT = "com.changhong.launcher.COMPLETE";
    private AtomicBoolean mConnected;
    private final IntentFilter mFilter = new IntentFilter();
    private WifiInfo mLastInfo;
    private android.net.NetworkInfo.DetailedState mLastState;
    private final BroadcastReceiver mReceiver = new _cls1();
    private final SettingScanner mScanner = new SettingScanner(null);
    private WifiManager mWifiManager;
    private settingNotify settinglistener;

    public SettingControl(Context context)
    {
        mConnected = new AtomicBoolean(false);
        mFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        mFilter.addAction("android.net.wifi.SCAN_RESULTS");
        mFilter.addAction("android.net.wifi.NETWORK_IDS_CHANGED");
        mFilter.addAction("android.net.wifi.supplicant.STATE_CHANGE");
        mFilter.addAction("android.net.wifi.STATE_CHANGE");
        mFilter.addAction("android.net.wifi.RSSI_CHANGED");
        mFilter.addAction("com.changhong.launcher.CONNECT");
        mFilter.addAction("com.changhong.launcher.COMPLETE");
        mWifiManager = (WifiManager)context.getSystemService("wifi");
    }

    private List constructAccessPoints()
    {
        ArrayList arraylist = new ArrayList();
        Multimap multimap = new Multimap(null);
        Object obj = mWifiManager.getConfiguredNetworks();
        if (obj != null)
        {
            SettingAccessPoint settingaccesspoint;
            for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); multimap.put(settingaccesspoint.ssid, settingaccesspoint))
            {
                settingaccesspoint = new SettingAccessPoint((WifiConfiguration)((Iterator) (obj)).next());
                settingaccesspoint.update(mLastInfo, mLastState);
                arraylist.add(settingaccesspoint);
            }

        }
        obj = mWifiManager.getScanResults();
        if (obj != null)
        {
            obj = ((List) (obj)).iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                Object obj1 = (ScanResult)((Iterator) (obj)).next();
                if (((ScanResult) (obj1)).SSID != null && ((ScanResult) (obj1)).SSID.length() != 0 && !((ScanResult) (obj1)).capabilities.contains("[IBSS]"))
                {
                    Iterator iterator = multimap.getAll(((ScanResult) (obj1)).SSID).iterator();
                    boolean flag = false;
                    do
                    {
                        if (!iterator.hasNext())
                        {
                            break;
                        }
                        if (((SettingAccessPoint)iterator.next()).update(((ScanResult) (obj1))))
                        {
                            flag = true;
                        }
                    } while (true);
                    if (!flag)
                    {
                        obj1 = new SettingAccessPoint(((ScanResult) (obj1)));
                        arraylist.add(obj1);
                        multimap.put(((SettingAccessPoint) (obj1)).ssid, obj1);
                    }
                }
            } while (true);
        }
        Collections.sort(arraylist);
        return arraylist;
    }

    public static Object getDeclaredField(Object obj, String s)
        throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
    {
        s = obj.getClass().getDeclaredField(s);
        s.setAccessible(true);
        return s.get(obj);
    }

    public static Object getField(Object obj, String s)
        throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
    {
        return obj.getClass().getField(s).get(obj);
    }

    private void handleEvent(Context context, Intent intent)
    {
        context = intent.getAction();
        if (!"android.net.wifi.WIFI_STATE_CHANGED".equals(context)) goto _L2; else goto _L1
_L1:
        updateWifiState(intent.getIntExtra("wifi_state", 4));
_L4:
        return;
_L2:
        if ("android.net.wifi.SCAN_RESULTS".equals(context))
        {
            updateAccessPoints();
            return;
        }
        if (!"android.net.wifi.supplicant.STATE_CHANGE".equals(context))
        {
            break; /* Loop/switch isn't completed */
        }
        if (!mConnected.get())
        {
            updateConnectionState(WifiInfo.getDetailedStateOf((SupplicantState)intent.getParcelableExtra("newState")));
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if ("android.net.wifi.STATE_CHANGE".equals(context))
        {
            context = (NetworkInfo)intent.getParcelableExtra("networkInfo");
            mConnected.set(context.isConnected());
            updateAccessPoints();
            updateConnectionState(context.getDetailedState());
            return;
        }
        if ("android.net.wifi.RSSI_CHANGED".equals(context))
        {
            updateConnectionState(null);
            return;
        }
        if ("com.changhong.launcher.CONNECT".equals(context))
        {
            notifyConnect();
            return;
        }
        if ("com.changhong.launcher.COMPLETE".equals(context))
        {
            notifyQuitInputMethod();
            return;
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    private static boolean isHex(String s)
    {
        for (int i = s.length() - 1; i >= 0; i--)
        {
            char c = s.charAt(i);
            if ((c < '0' || c > '9') && (c < 'A' || c > 'F') && (c < 'a' || c > 'f'))
            {
                return false;
            }
        }

        return true;
    }

    private static boolean isHexWepKey(String s)
    {
        int i = s.length();
        if (i != 10 && i != 26 && i != 58)
        {
            return false;
        } else
        {
            return isHex(s);
        }
    }

    private void notifyConnect()
    {
        SettingInputMethodNotifylistener.passwordCompletedAndConnect();
    }

    private void notifyQuitInputMethod()
    {
        SettingInputMethodNotifylistener.quitInputMethod();
    }

    public static void setDNS(InetAddress inetaddress, WifiConfiguration wificonfiguration)
        throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException
    {
        wificonfiguration = ((WifiConfiguration) (getField(wificonfiguration, "linkProperties")));
        if (wificonfiguration == null)
        {
            return;
        } else
        {
            wificonfiguration = (ArrayList)getDeclaredField(wificonfiguration, "mDnses");
            wificonfiguration.clear();
            wificonfiguration.add(inetaddress);
            return;
        }
    }

    public static void setEnumField(Object obj, String s, String s1)
        throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException
    {
        s1 = obj.getClass().getField(s1);
        s1.set(obj, Enum.valueOf(s1.getType(), s));
    }

    public static void setGateway(InetAddress inetaddress, WifiConfiguration wificonfiguration)
        throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, InstantiationException, InvocationTargetException
    {
        wificonfiguration = ((WifiConfiguration) (getField(wificonfiguration, "linkProperties")));
        if (wificonfiguration == null)
        {
            return;
        } else
        {
            inetaddress = ((InetAddress) (Class.forName("android.net.RouteInfo").getConstructor(new Class[] {
                java/net/InetAddress
            }).newInstance(new Object[] {
                inetaddress
            })));
            wificonfiguration = (ArrayList)getDeclaredField(wificonfiguration, "mRoutes");
            wificonfiguration.clear();
            wificonfiguration.add(inetaddress);
            return;
        }
    }

    public static void setIpAddress(InetAddress inetaddress, int i, WifiConfiguration wificonfiguration)
        throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException, NoSuchMethodException, ClassNotFoundException, InstantiationException, InvocationTargetException
    {
        wificonfiguration = ((WifiConfiguration) (getField(wificonfiguration, "linkProperties")));
        if (wificonfiguration == null)
        {
            return;
        } else
        {
            inetaddress = ((InetAddress) (Class.forName("android.net.LinkAddress").getConstructor(new Class[] {
                java/net/InetAddress, Integer.TYPE
            }).newInstance(new Object[] {
                inetaddress, Integer.valueOf(i)
            })));
            wificonfiguration = (ArrayList)getDeclaredField(wificonfiguration, "mLinkAddresses");
            wificonfiguration.clear();
            wificonfiguration.add(inetaddress);
            return;
        }
    }

    public static void setIpAssignment(String s, WifiConfiguration wificonfiguration)
        throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException
    {
        setEnumField(wificonfiguration, s, "ipAssignment");
    }

    private void updateAccessPoints()
    {
        mWifiManager.getWifiState();
        JVM INSTR tableswitch 0 3: default 36
    //                   0 36
    //                   1 36
    //                   2 36
    //                   3 37;
           goto _L1 _L1 _L1 _L1 _L2
_L1:
        return;
_L2:
        List list = constructAccessPoints();
        if (settinglistener != null)
        {
            settinglistener.deleteAllPoints();
            settinglistener.showAllPoints(list);
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    private void updateConnectionState(android.net.NetworkInfo.DetailedState detailedstate)
    {
        if (!mWifiManager.isWifiEnabled())
        {
            mScanner.pause();
        } else
        {
            if (detailedstate == android.net.NetworkInfo.DetailedState.OBTAINING_IPADDR)
            {
                mScanner.pause();
            } else
            {
                mScanner.resume();
            }
            mLastInfo = mWifiManager.getConnectionInfo();
            if (detailedstate != null)
            {
                mLastState = detailedstate;
            }
            if (detailedstate == android.net.NetworkInfo.DetailedState.CONNECTED)
            {
                Dhcpinfo = mWifiManager.getDhcpInfo().toString();
            }
            if (detailedstate != null && settinglistener != null)
            {
                settinglistener.updateState(detailedstate);
                return;
            }
        }
    }

    private void updateWifiState(int i)
    {
        switch (i)
        {
        case 2: // '\002'
        default:
            mLastInfo = null;
            mLastState = null;
            mScanner.pause();
            return;

        case 3: // '\003'
            mScanner.resume();
            break;
        }
    }

    public int addNetworkStaticConfig(String s, String s1, String s2, String s3, String s4, int i)
        throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException, UnknownHostException, NoSuchMethodException, ClassNotFoundException, InstantiationException, InvocationTargetException
    {
        WifiConfiguration wificonfiguration;
        wificonfiguration = new WifiConfiguration();
        wificonfiguration.SSID = (new StringBuilder()).append("\"").append(s4).append("\"").toString();
        if (i != 0) goto _L2; else goto _L1
_L1:
        wificonfiguration.allowedKeyManagement.set(0);
_L4:
        setIpAssignment("STATIC", wificonfiguration);
        setIpAddress(InetAddress.getByName(s1), 24, wificonfiguration);
        setGateway(InetAddress.getByName(s2), wificonfiguration);
        setDNS(InetAddress.getByName(s3), wificonfiguration);
        return saveNetwork(wificonfiguration);
_L2:
        if (i == 1)
        {
            wificonfiguration.hiddenSSID = true;
            wificonfiguration.wepKeys[0] = (new StringBuilder()).append("\"").append(s).append("\"").toString();
            wificonfiguration.allowedAuthAlgorithms.set(1);
            wificonfiguration.allowedGroupCiphers.set(3);
            wificonfiguration.allowedGroupCiphers.set(2);
            wificonfiguration.allowedGroupCiphers.set(0);
            wificonfiguration.allowedGroupCiphers.set(1);
            wificonfiguration.allowedKeyManagement.set(0);
            wificonfiguration.wepTxKeyIndex = 0;
        } else
        if (i == 2)
        {
            wificonfiguration.preSharedKey = (new StringBuilder()).append("\"").append(s).append("\"").toString();
            wificonfiguration.hiddenSSID = true;
            wificonfiguration.allowedAuthAlgorithms.set(0);
            wificonfiguration.allowedGroupCiphers.set(2);
            wificonfiguration.allowedKeyManagement.set(1);
            wificonfiguration.allowedPairwiseCiphers.set(1);
            wificonfiguration.allowedGroupCiphers.set(3);
            wificonfiguration.allowedPairwiseCiphers.set(2);
            wificonfiguration.status = 2;
        } else
        if (i == 3)
        {
            wificonfiguration.hiddenSSID = false;
            wificonfiguration.status = 2;
            wificonfiguration.allowedAuthAlgorithms.set(0);
            wificonfiguration.allowedAuthAlgorithms.set(1);
            wificonfiguration.allowedPairwiseCiphers.set(1);
            wificonfiguration.allowedPairwiseCiphers.set(2);
            wificonfiguration.allowedProtocols.set(1);
            wificonfiguration.allowedProtocols.set(0);
            wificonfiguration.allowedKeyManagement.set(2);
            wificonfiguration.allowedGroupCiphers.set(2);
            wificonfiguration.allowedGroupCiphers.set(3);
            wificonfiguration.preSharedKey = (new StringBuilder()).append("\"").append(s).append("\"").toString();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void connectNetwork(int i)
    {
        if (i < 0)
        {
            return;
        } else
        {
            mWifiManager.enableNetwork(i, true);
            mWifiManager.saveConfiguration();
            mWifiManager.reconnect();
            return;
        }
    }

    public void connectNetwork(WifiConfiguration wificonfiguration)
    {
        int i = mWifiManager.addNetwork(wificonfiguration);
        if (i != -1)
        {
            mWifiManager.enableNetwork(i, true);
            mWifiManager.saveConfiguration();
            mWifiManager.reconnect();
        }
    }

    public void connectNewAP(String s, String s1, int i)
    {
        WifiConfiguration wificonfiguration;
        wificonfiguration = new WifiConfiguration();
        wificonfiguration.SSID = (new StringBuilder()).append("\"").append(s1).append("\"").toString();
        if (i != 0) goto _L2; else goto _L1
_L1:
        wificonfiguration.allowedKeyManagement.set(0);
_L4:
        connectNetwork(wificonfiguration);
        return;
_L2:
        if (i == 1)
        {
            if (!TextUtils.isEmpty(s))
            {
                if (isHexWepKey(s))
                {
                    wificonfiguration.wepKeys[0] = s;
                } else
                {
                    wificonfiguration.wepKeys[0] = (new StringBuilder()).append("\"").append(s).append("\"").toString();
                }
            }
            wificonfiguration.allowedAuthAlgorithms.set(0);
            wificonfiguration.allowedAuthAlgorithms.set(1);
            wificonfiguration.allowedKeyManagement.set(0);
            wificonfiguration.wepTxKeyIndex = 0;
        } else
        if (i == 2)
        {
            wificonfiguration.preSharedKey = (new StringBuilder()).append("\"").append(s).append("\"").toString();
            wificonfiguration.hiddenSSID = true;
            wificonfiguration.allowedAuthAlgorithms.set(0);
            wificonfiguration.allowedGroupCiphers.set(2);
            wificonfiguration.allowedKeyManagement.set(1);
            wificonfiguration.allowedPairwiseCiphers.set(1);
            wificonfiguration.allowedGroupCiphers.set(3);
            wificonfiguration.allowedPairwiseCiphers.set(2);
            wificonfiguration.status = 2;
        } else
        if (i == 3)
        {
            wificonfiguration.hiddenSSID = false;
            wificonfiguration.status = 2;
            wificonfiguration.allowedAuthAlgorithms.set(0);
            wificonfiguration.allowedAuthAlgorithms.set(1);
            wificonfiguration.allowedPairwiseCiphers.set(1);
            wificonfiguration.allowedPairwiseCiphers.set(2);
            wificonfiguration.allowedProtocols.set(1);
            wificonfiguration.allowedProtocols.set(0);
            wificonfiguration.allowedKeyManagement.set(2);
            wificonfiguration.allowedGroupCiphers.set(2);
            wificonfiguration.allowedGroupCiphers.set(3);
            wificonfiguration.preSharedKey = (new StringBuilder()).append("\"").append(s).append("\"").toString();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public boolean enableWifi(boolean flag)
    {
        return mWifiManager.setWifiEnabled(flag);
    }

    public void forgetNetwork(int i)
    {
        if (i < 0)
        {
            return;
        } else
        {
            mWifiManager.removeNetwork(i);
            mWifiManager.saveConfiguration();
            return;
        }
    }

    public String getDhcpinfo()
    {
        return Dhcpinfo;
    }

    public WifiInfo getWifiInfo()
    {
        return mWifiManager.getConnectionInfo();
    }

    public void pauseWifiScan()
    {
        if (mWifiManager.isWifiEnabled())
        {
            mScanner.pause();
        }
    }

    public void registerReceiver(Context context)
    {
        context.registerReceiver(mReceiver, mFilter);
    }

    public void resumeWifiScan()
    {
        if (mWifiManager.isWifiEnabled())
        {
            mScanner.resume();
        }
    }

    public int saveExitNetwork(WifiConfiguration wificonfiguration)
    {
        int i = mWifiManager.updateNetwork(wificonfiguration);
        mWifiManager.enableNetwork(wificonfiguration.networkId, true);
        mWifiManager.saveConfiguration();
        mWifiManager.reconnect();
        return i;
    }

    public int saveNetwork(WifiConfiguration wificonfiguration)
    {
        int i = mWifiManager.addNetwork(wificonfiguration);
        if (i != -1)
        {
            mWifiManager.enableNetwork(i, true);
            mWifiManager.saveConfiguration();
            mWifiManager.reconnect();
        }
        return i;
    }

    public void setSettingInputMethodNotifyListener(SettingInputMethodNotify settinginputmethodnotify)
    {
        SettingInputMethodNotifylistener = settinginputmethodnotify;
    }

    public void setSettingNotifyListener(settingNotify settingnotify)
    {
        settinglistener = settingnotify;
    }

    public void unregisterReceiver(Context context)
    {
        context.unregisterReceiver(mReceiver);
    }

    public int updateNetworkStaticConfig(WifiConfiguration wificonfiguration, String s, String s1, String s2)
        throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException, UnknownHostException, NoSuchMethodException, ClassNotFoundException, InstantiationException, InvocationTargetException
    {
        setIpAssignment("STATIC", wificonfiguration);
        setIpAddress(InetAddress.getByName(s), 24, wificonfiguration);
        setGateway(InetAddress.getByName(s1), wificonfiguration);
        setDNS(InetAddress.getByName(s2), wificonfiguration);
        return saveExitNetwork(wificonfiguration);
    }


    private class _cls1 extends BroadcastReceiver
    {

        final SettingControl this$0;

        public void onReceive(Context context, Intent intent)
        {
        }

        _cls1()
        {
            this$0 = SettingControl.this;
            super();
        }
    }

}
