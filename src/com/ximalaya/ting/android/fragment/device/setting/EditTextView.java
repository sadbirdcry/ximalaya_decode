// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.setting;

import android.content.Context;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

public class EditTextView extends TextView
    implements Runnable
{

    private String lastText;
    private boolean skip;

    public EditTextView(Context context)
    {
        super(context);
        skip = false;
        lastText = "";
    }

    public EditTextView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        skip = false;
        lastText = "";
    }

    public EditTextView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        skip = false;
        lastText = "";
    }

    private String getPasswordString(int i, char c)
    {
        return (new StringBuilder()).append(getStarString(i)).append(c).toString();
    }

    private String getStarString(int i)
    {
        StringBuilder stringbuilder = new StringBuilder();
        for (int j = 0; j < i; j++)
        {
            stringbuilder.append("*");
        }

        return stringbuilder.toString();
    }

    public String getEditText()
    {
        return lastText;
    }

    public void run()
    {
        skip = true;
        setText(getStarString(getText().length()));
    }

    public void setText(CharSequence charsequence, android.widget.TextView.BufferType buffertype)
    {
        if (skip)
        {
            skip = false;
            super.setText(charsequence, buffertype);
            return;
        }
        TextPaint textpaint;
        if (charsequence.length() != 0)
        {
            if (lastText.length() < charsequence.length())
            {
                lastText = (new StringBuilder()).append(lastText).append(charsequence.toString().substring(charsequence.length() - 1)).toString();
            } else
            {
                lastText = lastText.substring(0, lastText.length() - 1);
            }
        } else
        {
            lastText = "";
        }
        textpaint = getPaint();
        if (charsequence.length() != 0)
        {
            removeCallbacks(this);
            if (getText().length() > charsequence.length())
            {
                charsequence = getStarString(charsequence.length());
                if (textpaint.measureText(charsequence) >= (float)getWidth())
                {
                    setGravity(5);
                } else
                {
                    setGravity(3);
                }
            } else
            {
                int i = charsequence.length() - 1;
                charsequence = getPasswordString(i, charsequence.charAt(i));
                if (textpaint.measureText(charsequence) >= (float)getWidth())
                {
                    setGravity(5);
                } else
                {
                    setGravity(3);
                }
                postDelayed(this, 1000L);
            }
            super.setText(charsequence, buffertype);
            return;
        } else
        {
            super.setText(charsequence, buffertype);
            return;
        }
    }
}
