// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.setting;

import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;

public class SettingScanResult
{

    private String SettingSSID;
    private int Signallevel;
    private String capabilities;
    private WifiConfiguration configuration;
    private String dhcpinfo;
    private WifiInfo info;
    private android.net.NetworkInfo.DetailedState mState;
    private int networkId;
    private int securityType;

    public SettingScanResult(String s, String s1, int i)
    {
        securityType = 0;
        SettingSSID = s;
        capabilities = s1;
        Signallevel = i;
    }

    public String getCapabilities()
    {
        return capabilities;
    }

    public String getDhcpinfo()
    {
        return dhcpinfo;
    }

    public int getNetworkId()
    {
        return networkId;
    }

    public String getSSID()
    {
        return SettingSSID;
    }

    public int getSecurityType()
    {
        return securityType;
    }

    public int getSignallevel()
    {
        return Signallevel;
    }

    public android.net.NetworkInfo.DetailedState getState()
    {
        return mState;
    }

    public WifiConfiguration getWifiConfig()
    {
        return configuration;
    }

    public WifiInfo getWifiInfo()
    {
        return info;
    }

    public void setCapabilities(String s)
    {
        capabilities = s;
    }

    public void setDhcpinfo(String s)
    {
        dhcpinfo = s;
    }

    public void setSSID(String s)
    {
        SettingSSID = s;
    }

    public void setSecurityType(int i)
    {
        securityType = i;
    }

    public void setSignallevel(int i)
    {
        Signallevel = i;
    }

    public void setState(android.net.NetworkInfo.DetailedState detailedstate)
    {
        mState = detailedstate;
    }

    public void setWifiConfig(WifiConfiguration wificonfiguration)
    {
        configuration = wificonfiguration;
    }

    public void setWifiInfo(WifiInfo wifiinfo)
    {
        info = wifiinfo;
    }

    public void setnetworkId(int i)
    {
        networkId = i;
    }
}
