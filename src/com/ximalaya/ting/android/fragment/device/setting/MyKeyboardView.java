// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.device.setting;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.util.AttributeSet;
import java.util.ArrayList;
import java.util.List;

public class MyKeyboardView extends KeyboardView
{

    private Keyboard currentKeyboard;
    private android.inputmethodservice.Keyboard.Key focusedKey;
    private List keys;
    private int lastKeyIndex;
    private Paint linePaint;
    private Paint p;
    private Rect rect;
    private Rect viewRect;

    public MyKeyboardView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        keys = new ArrayList();
        lastKeyIndex = 0;
        rect = new Rect();
        p = new Paint();
        viewRect = new Rect();
        linePaint = new Paint();
    }

    public MyKeyboardView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        keys = new ArrayList();
        lastKeyIndex = 0;
        rect = new Rect();
        p = new Paint();
        viewRect = new Rect();
        linePaint = new Paint();
    }

    public int getLastKeyIndex()
    {
        return lastKeyIndex;
    }

    public void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        currentKeyboard = getKeyboard();
        keys = currentKeyboard.getKeys();
        int l = ((android.inputmethodservice.Keyboard.Key)keys.get(0)).x;
        int i1 = ((android.inputmethodservice.Keyboard.Key)keys.get(0)).y;
        linePaint.setColor(Color.argb(255, 84, 84, 84));
        linePaint.setStyle(android.graphics.Paint.Style.STROKE);
        linePaint.setStrokeWidth(1.0F);
        int k = 0;
        int j = 0;
        int i = 0;
        for (; k < keys.size(); k++)
        {
            android.inputmethodservice.Keyboard.Key key = (android.inputmethodservice.Keyboard.Key)keys.get(k);
            if (key == null)
            {
                continue;
            }
            canvas.drawLine(key.x, key.y + key.height, key.x + key.width, key.y + key.height, linePaint);
            canvas.drawLine(key.x + key.width, key.y, key.x + key.width, key.y + key.height, linePaint);
            CharSequence charsequence = key.label;
            if (charsequence != null && (charsequence.equals("\u5B8C\u6210") || charsequence.equals("\u8FDE\u63A5")))
            {
                i = key.x;
                int j1 = key.width;
                j = key.y + key.height;
                i += j1;
            }
        }

        canvas.drawLine(i - 1, i1, i - 1, j, linePaint);
        canvas.drawLine(l, j - 1, i, j - 1, linePaint);
        if (getLocalVisibleRect(viewRect))
        {
            canvas.drawRect(viewRect, linePaint);
        }
        p.setColor(Color.argb(255, 107, 189, 252));
        p.setStyle(android.graphics.Paint.Style.STROKE);
        p.setStrokeWidth(3.75F);
        focusedKey = (android.inputmethodservice.Keyboard.Key)keys.get(lastKeyIndex);
        rect.set(focusedKey.x, focusedKey.y, focusedKey.x + focusedKey.width, focusedKey.y + focusedKey.height);
        canvas.drawRect(rect, p);
    }

    public void sendKey(int i)
    {
        int ai[] = new int[1];
        getOnKeyboardActionListener().onKey(i, ai);
    }

    public void setLastKeyIndex(int i)
    {
        lastKeyIndex = i;
    }
}
