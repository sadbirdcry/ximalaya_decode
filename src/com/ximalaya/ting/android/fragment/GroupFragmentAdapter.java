// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import java.lang.ref.SoftReference;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseActionInterface

public class GroupFragmentAdapter extends FragmentStatePagerAdapter
{

    public HashMap fragmentMap;
    private List mFragmentList;

    public GroupFragmentAdapter(FragmentManager fragmentmanager, List list)
    {
        super(fragmentmanager);
        fragmentMap = new HashMap();
        mFragmentList = list;
    }

    public int getCount()
    {
        if (mFragmentList != null)
        {
            return mFragmentList.size();
        } else
        {
            return 0;
        }
    }

    public Fragment getFragment(int i)
    {
        SoftReference softreference = (SoftReference)fragmentMap.get(Integer.valueOf(i));
        if (softreference != null)
        {
            return (Fragment)softreference.get();
        } else
        {
            return null;
        }
    }

    public Fragment getItem(int i)
    {
        Fragment fragment;
        try
        {
            fragment = (Fragment)((BaseViewPagerFragmentGroup.ClassInfo)mFragmentList.get(i)).className.newInstance();
        }
        catch (Exception exception)
        {
            return new Fragment();
        }
        if (fragment == null)
        {
            break MISSING_BLOCK_LABEL_117;
        }
        if (((BaseViewPagerFragmentGroup.ClassInfo)mFragmentList.get(i)).bundle != null)
        {
            fragment.setArguments(((BaseViewPagerFragmentGroup.ClassInfo)mFragmentList.get(i)).bundle);
        }
        if (((BaseViewPagerFragmentGroup.ClassInfo)mFragmentList.get(i)).fragmentManager != null && (fragment instanceof BaseActionInterface))
        {
            ((BaseActionInterface)fragment).setFragmentManager(((BaseViewPagerFragmentGroup.ClassInfo)mFragmentList.get(i)).fragmentManager);
        }
        fragmentMap.put(Integer.valueOf(i), new SoftReference(fragment));
        return fragment;
    }
}
