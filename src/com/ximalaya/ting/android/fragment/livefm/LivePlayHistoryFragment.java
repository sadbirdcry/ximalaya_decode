// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.livefm.LiveHistoryListAdapter;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.modelmanage.LiveHistoryManage;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.SlideView;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.List;

public class LivePlayHistoryFragment extends BaseActivityLikeFragment
{

    private LiveHistoryListAdapter adapter;
    private TextView clearBtn;
    private LinearLayout mEmptyView;
    private boolean mExitWhenPlay;
    private BounceListView mListView;
    private com.ximalaya.ting.android.view.SlideView.OnFinishListener mOnFinishCallback;
    private SlideView mSlideView;
    private List soundInfoList;

    public LivePlayHistoryFragment()
    {
        soundInfoList = new ArrayList();
    }

    private void delAllItems()
    {
        if (soundInfoList.size() == 0)
        {
            return;
        } else
        {
            (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u6E05\u7A7A\u64AD\u653E\u5386\u53F2\uFF1F").setOkBtn(new _cls5()).showConfirm();
            return;
        }
    }

    private void delItem(final int position)
    {
        (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u5220\u9664\u8BE5\u6761\u64AD\u653E\u8BB0\u5F55\uFF1F").setOkBtn(new _cls6()).showConfirm();
    }

    private void initListener()
    {
        mSlideView.setOnFinishListener(new _cls1());
        clearBtn.setOnClickListener(new _cls2());
        mListView.setOnItemClickListener(new _cls3());
        mListView.setOnItemLongClickListener(new _cls4());
    }

    private void initViews()
    {
        setTitleText("\u64AD\u653E\u5386\u53F2");
        clearBtn = (TextView)findViewById(0x7f0a0717);
        mListView = (BounceListView)findViewById(0x7f0a005c);
        soundInfoList.clear();
        soundInfoList.addAll(LiveHistoryManage.getInstance(getActivity().getApplicationContext()).getSoundInfoList());
        mEmptyView = (LinearLayout)LayoutInflater.from(mActivity).inflate(0x7f03007c, mListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u64AD\u653E\u8FC7\u58F0\u97F3\u54E6");
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setVisibility(8);
        ((Button)mEmptyView.findViewById(0x7f0a0232)).setVisibility(8);
        mListView.addFooterView(mEmptyView);
        View view;
        if (soundInfoList.size() == 0)
        {
            mEmptyView.setVisibility(0);
            clearBtn.setVisibility(8);
        } else
        {
            clearBtn.setVisibility(0);
            mEmptyView.setVisibility(8);
        }
        view = new View(mActivity);
        view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ToolUtil.dp2px(mActivity, 75F)));
        mListView.addFooterView(view);
        adapter = new LiveHistoryListAdapter(soundInfoList, getActivity());
        mListView.setAdapter(adapter);
    }

    private void updateView()
    {
        soundInfoList.clear();
        soundInfoList.addAll(LiveHistoryManage.getInstance(getActivity().getApplicationContext()).getSoundInfoList());
        if (soundInfoList.size() == 0)
        {
            clearBtn.setVisibility(8);
            mEmptyView.setVisibility(0);
        } else
        {
            clearBtn.setVisibility(0);
            mEmptyView.setVisibility(8);
        }
        adapter.notifyDataSetChanged();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initViews();
        initListener();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mExitWhenPlay = bundle.getBoolean("exit_when_play", false);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300f5, viewgroup, false);
        mSlideView = (SlideView)fragmentBaseContainerView;
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        setOnFinishCallback(null);
        super.onDestroyView();
    }

    public void setOnFinishCallback(com.ximalaya.ting.android.view.SlideView.OnFinishListener onfinishlistener)
    {
        mOnFinishCallback = onfinishlistener;
    }









    private class _cls5
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final LivePlayHistoryFragment this$0;

        public void onExecute()
        {
            LiveHistoryManage.getInstance(mCon).deleteAllSound();
            updateView();
        }

        _cls5()
        {
            this$0 = LivePlayHistoryFragment.this;
            super();
        }
    }


    private class _cls6
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final LivePlayHistoryFragment this$0;
        final int val$position;

        public void onExecute()
        {
            LiveHistoryManage.getInstance(getActivity().getApplicationContext()).deleteSound(position);
            updateView();
        }

        _cls6()
        {
            this$0 = LivePlayHistoryFragment.this;
            position = i;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.view.SlideView.OnFinishListener
    {

        final LivePlayHistoryFragment this$0;

        public boolean onFinish()
        {
            if (mOnFinishCallback != null)
            {
                return mOnFinishCallback.onFinish();
            } else
            {
                return false;
            }
        }

        _cls1()
        {
            this$0 = LivePlayHistoryFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final LivePlayHistoryFragment this$0;

        public void onClick(View view)
        {
            delAllItems();
        }

        _cls2()
        {
            this$0 = LivePlayHistoryFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final LivePlayHistoryFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            int j = i - mListView.getHeaderViewsCount();
            if (j >= 0 && j < adapter.getCount())
            {
                PlayTools.gotoPlay(32, (SoundInfo)soundInfoList.get(i), getActivity(), DataCollectUtil.getDataFromView(view));
                if (mExitWhenPlay && mOnFinishCallback != null)
                {
                    mOnFinishCallback.onFinish();
                    return;
                }
            }
        }

        _cls3()
        {
            this$0 = LivePlayHistoryFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final LivePlayHistoryFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= adapter.getCount())
            {
                return false;
            } else
            {
                delItem(i);
                return true;
            }
        }

        _cls4()
        {
            this$0 = LivePlayHistoryFragment.this;
            super();
        }
    }

}
