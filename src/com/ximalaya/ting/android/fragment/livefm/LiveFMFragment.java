// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.tab.FindingFragmentV3;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.model.livefm.RecommendRadioListModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.LiveHistoryManage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.livefm:
//            LiveFMCategoryTagFragment, LiveProvinceRadioTabFragment, RadioRankListFragment, LivePlayHistoryFragment

public class LiveFMFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.modelmanage.LiveHistoryManage.HistoryUpdateListener, com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{

    public static final String CATEGORY_TYPE = "type";
    private static final int HISTORY_SHOW_SIZE = 5;
    public static final int LOCAL_RADIO = 0;
    public static final int NATIONAL_RADIO = 1;
    public static final int NET_RADIO = 3;
    public static final int PROVINCE_RADIO = 2;
    private List historyPlayIcons;
    private List historySounds;
    private View historyTitle;
    private LayoutInflater inflater;
    private boolean mIsDataLoaded;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private ViewGroup playHistorySection;
    private List rankPlayIcons;
    private ImageView recommendRadio1Cover;
    private TextView recommendRadio1Name;
    private ImageView recommendRadio2Cover;
    private TextView recommendRadio2Name;
    private ImageView recommendRadio3Cover;
    private TextView recommendRadio3Name;
    private List recommendRadioListModels;
    private ImageView topRadio1Cover;
    private TextView topRadio1Name;
    private TextView topRadio1PlayCount;
    private TextView topRadio1ProgramName;
    private View topRadio1View;
    private ImageView topRadio2Cover;
    private TextView topRadio2Name;
    private TextView topRadio2PlayCount;
    private TextView topRadio2ProgramName;
    private View topRadio2View;
    private ImageView topRadio3Cover;
    private TextView topRadio3Name;
    private TextView topRadio3PlayCount;
    private TextView topRadio3ProgramName;
    private View topRadio3View;
    private List topRadioListModels;

    public LiveFMFragment()
    {
        recommendRadioListModels = new ArrayList();
        topRadioListModels = new ArrayList();
        rankPlayIcons = null;
        historyPlayIcons = new ArrayList();
        historySounds = new ArrayList();
        mIsDataLoaded = false;
    }

    private void initRadioCategory()
    {
        findViewById(0x7f0a0413).setOnClickListener(this);
        findViewById(0x7f0a0414).setOnClickListener(this);
        findViewById(0x7f0a0415).setOnClickListener(this);
        findViewById(0x7f0a0416).setOnClickListener(this);
    }

    private void initRanklistViews()
    {
        rankPlayIcons = new ArrayList();
        topRadio1View = findViewById(0x7f0a0428);
        topRadio1Cover = (ImageView)topRadio1View.findViewById(0x7f0a0519);
        topRadio1Name = (TextView)topRadio1View.findViewById(0x7f0a051a);
        topRadio1ProgramName = (TextView)topRadio1View.findViewById(0x7f0a051b);
        topRadio1PlayCount = (TextView)topRadio1View.findViewById(0x7f0a0151);
        ImageView imageview = (ImageView)topRadio1View.findViewById(0x7f0a0022);
        rankPlayIcons.add(imageview);
        imageview.setOnClickListener(this);
        topRadio2View = findViewById(0x7f0a042a);
        topRadio2Cover = (ImageView)topRadio2View.findViewById(0x7f0a0519);
        topRadio2Name = (TextView)topRadio2View.findViewById(0x7f0a051a);
        topRadio2ProgramName = (TextView)topRadio2View.findViewById(0x7f0a051b);
        topRadio2PlayCount = (TextView)topRadio2View.findViewById(0x7f0a0151);
        imageview = (ImageView)topRadio2View.findViewById(0x7f0a0022);
        rankPlayIcons.add(imageview);
        imageview.setOnClickListener(this);
        topRadio3View = findViewById(0x7f0a042c);
        topRadio3Cover = (ImageView)topRadio3View.findViewById(0x7f0a0519);
        topRadio3Name = (TextView)topRadio3View.findViewById(0x7f0a051a);
        topRadio3ProgramName = (TextView)topRadio3View.findViewById(0x7f0a051b);
        topRadio3PlayCount = (TextView)topRadio3View.findViewById(0x7f0a0151);
        imageview = (ImageView)topRadio3View.findViewById(0x7f0a0022);
        topRadio3View.findViewById(0x7f0a051c).setVisibility(8);
        rankPlayIcons.add(imageview);
        imageview.setOnClickListener(this);
        findViewById(0x7f0a0426).setOnClickListener(this);
        findViewById(0x7f0a0427).setOnClickListener(this);
        findViewById(0x7f0a0429).setOnClickListener(this);
        findViewById(0x7f0a042b).setOnClickListener(this);
    }

    private void initRecommendRadios()
    {
        recommendRadio1Cover = (ImageView)findViewById(0x7f0a041b);
        recommendRadio1Name = (TextView)findViewById(0x7f0a041c);
        recommendRadio2Cover = (ImageView)findViewById(0x7f0a041f);
        recommendRadio2Name = (TextView)findViewById(0x7f0a0420);
        recommendRadio3Cover = (ImageView)findViewById(0x7f0a0423);
        recommendRadio3Name = (TextView)findViewById(0x7f0a0424);
        recommendRadio1Cover.setOnClickListener(this);
        recommendRadio2Cover.setOnClickListener(this);
        recommendRadio3Cover.setOnClickListener(this);
        Object obj1 = (RelativeLayout)findViewById(0x7f0a041a);
        Object obj = (RelativeLayout)findViewById(0x7f0a041e);
        RelativeLayout relativelayout = (RelativeLayout)findViewById(0x7f0a0422);
        int i = (ToolUtil.getScreenWidth(mActivity) - ToolUtil.dp2px(mActivity, 10F) * 4) / 3;
        android.view.ViewGroup.LayoutParams layoutparams = ((RelativeLayout) (obj1)).getLayoutParams();
        layoutparams.height = i;
        ((RelativeLayout) (obj1)).setLayoutParams(layoutparams);
        obj1 = ((RelativeLayout) (obj)).getLayoutParams();
        obj1.height = i;
        ((RelativeLayout) (obj)).setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj1)));
        obj = relativelayout.getLayoutParams();
        obj.height = i;
        relativelayout.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
    }

    private void initViews()
    {
        setTitleText("\u76F4\u64ADFM");
        if (PackageUtil.isMeizu() && getActivity() != null)
        {
            findViewById(0x7f0a0070).setPadding(0, 0, 0, Utilities.dip2px(getActivity(), 70F));
        }
        initRadioCategory();
        initRecommendRadios();
        initRanklistViews();
        playHistorySection = (ViewGroup)findViewById(0x7f0a0430);
        historyTitle = findViewById(0x7f0a042d);
    }

    private void loadData()
    {
        if (!getUserVisibleHint() || getView() == null)
        {
            return;
        }
        if (!mIsDataLoaded)
        {
            String s = (new StringBuilder()).append(a.O).append("getHomePageRadiosList").toString();
            String s1 = DataCollectUtil.getDataFromView(fragmentBaseContainerView, FindingFragmentV3.recommendLiveEntryText, null);
            f.a().a(s, null, s1, new _cls2());
            return;
        }
        if (recommendRadioListModels != null && !recommendRadioListModels.isEmpty())
        {
            updateRecommendRadios();
        }
        if (topRadioListModels != null && !topRadioListModels.isEmpty())
        {
            updateRanklistViews();
        }
        if (rankPlayIcons != null)
        {
            updatePlayIcons();
        }
        updatePlayHistoryViews();
    }

    private void parseJson(String s)
    {
        s = JSON.parseObject(s);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_202;
        }
        if (!"0000".equals(s.getString("ret")))
        {
            break MISSING_BLOCK_LABEL_202;
        }
        String s1 = s.getString("result");
        s = JSON.parseObject(s1);
        if (TextUtils.isEmpty(s1))
        {
            break MISSING_BLOCK_LABEL_202;
        }
        if (s == null)
        {
            return;
        }
        Object obj;
        obj = s.getString("recommandRadioList");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            break MISSING_BLOCK_LABEL_110;
        }
        obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/livefm/RecommendRadioListModel);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_110;
        }
        if (((List) (obj)).size() != 0)
        {
            recommendRadioListModels.clear();
            recommendRadioListModels.addAll(((java.util.Collection) (obj)));
            updateRecommendRadios();
        }
        s = s.getString("topRadioList");
        if (TextUtils.isEmpty(s))
        {
            break MISSING_BLOCK_LABEL_170;
        }
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/livefm/RadioSound);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_170;
        }
        if (s.size() != 0)
        {
            topRadioListModels.clear();
            topRadioListModels.addAll(s);
            updateRanklistViews();
        }
        updatePlayIcons();
        return;
        s;
        Logger.log((new StringBuilder()).append("live fm parseJson exception: ").append(s.getMessage()).toString());
    }

    private void playSound(ImageView imageview, SoundInfo soundinfo, String s)
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        SoundInfo soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
        if (localmediaservice == null || soundinfo == null)
        {
            return;
        }
        if (soundinfo1 == null)
        {
            PlayTools.gotoPlay(32, soundinfo, getActivity(), false, s);
            imageview.setImageResource(0x7f020254);
            return;
        }
        switch (localmediaservice.getPlayServiceState())
        {
        default:
            return;

        case 0: // '\0'
            PlayTools.gotoPlay(32, soundinfo, getActivity(), false, s);
            imageview.setImageResource(0x7f020254);
            return;

        case 1: // '\001'
        case 3: // '\003'
            if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.radioId == soundinfo1.radioId)
            {
                localmediaservice.pause();
                imageview.setImageResource(0x7f020253);
                return;
            } else
            {
                PlayTools.gotoPlay(32, soundinfo, getActivity(), false, s);
                imageview.setImageResource(0x7f020254);
                return;
            }

        case 2: // '\002'
            break;
        }
        if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.radioId == soundinfo1.radioId)
        {
            localmediaservice.start();
            imageview.setImageResource(0x7f020254);
            return;
        } else
        {
            PlayTools.gotoPlay(32, soundinfo, getActivity(), false, s);
            imageview.setImageResource(0x7f020254);
            return;
        }
    }

    private void registerListener()
    {
        LiveHistoryManage.getInstance(mCon).setOnHistoryUpdateListener(this);
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls1();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(this);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
        LiveHistoryManage.getInstance(mCon).removeHistoryUpdateListener(this);
    }

    private void updatePlayHistoryViews()
    {
        MyLogger.getLogger().print();
        historyPlayIcons.clear();
        historySounds.clear();
        Object obj = LiveHistoryManage.getInstance(mCon).getSoundInfoList();
        int i = ((List) (obj)).size();
        if (i == 0)
        {
            playHistorySection.setVisibility(8);
            historyTitle.setVisibility(8);
        } else
        {
            historyTitle.setVisibility(0);
            playHistorySection.setVisibility(0);
            if (i > 5)
            {
                i = 5;
            }
            historySounds.addAll(((List) (obj)).subList(0, i));
            playHistorySection.removeAllViews();
            findViewById(0x7f0a042f).setOnClickListener(this);
            obj = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            int j = 0;
            while (j < i) 
            {
                SoundInfo soundinfo = (SoundInfo)historySounds.get(j);
                ViewGroup viewgroup = (ViewGroup)inflater.inflate(0x7f030156, playHistorySection, false);
                ImageManager2.from(mActivity).displayImage((ImageView)viewgroup.findViewById(0x7f0a0519), soundinfo.coverSmall, -1);
                ((TextView)viewgroup.findViewById(0x7f0a051a)).setText(soundinfo.radioName);
                ((TextView)viewgroup.findViewById(0x7f0a051b)).setText((new StringBuilder()).append("\u4E0A\u6B21\u6536\u542C\u8282\u76EE\uFF1A").append(soundinfo.programName).toString());
                if (soundinfo.startPlayTime != null)
                {
                    TextView textview = (TextView)viewgroup.findViewById(0x7f0a0151);
                    textview.setText((new StringBuilder()).append("\u4E0A\u6B21\u6536\u542C\u65F6\u95F4\uFF1A").append(((SimpleDateFormat) (obj)).format(soundinfo.startPlayTime)).toString());
                    textview.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                }
                ImageView imageview = (ImageView)viewgroup.findViewById(0x7f0a0022);
                historyPlayIcons.add(imageview);
                imageview.setOnClickListener(this);
                imageview.setTag(soundinfo);
                if (ToolUtil.isLivePlaying(soundinfo.radioId))
                {
                    imageview.setImageResource(0x7f020254);
                }
                viewgroup.setOnClickListener(this);
                viewgroup.setTag(soundinfo);
                playHistorySection.addView(viewgroup);
                if (j == i - 1)
                {
                    viewgroup.findViewById(0x7f0a051c).setVisibility(8);
                }
                j++;
            }
        }
    }

    private void updatePlayIcons()
    {
        boolean flag = false;
        MyLogger.getLogger().print();
        int i = 0;
label0:
        do
        {
label1:
            {
                int j = ((flag) ? 1 : 0);
                if (i < rankPlayIcons.size())
                {
                    j = ((flag) ? 1 : 0);
                    if (topRadioListModels.size() != 0)
                    {
                        if (i < topRadioListModels.size())
                        {
                            break label1;
                        }
                        j = ((flag) ? 1 : 0);
                    }
                }
                while (j < historyPlayIcons.size()) 
                {
                    Object obj = (SoundInfo)LiveHistoryManage.getInstance(mCon.getApplicationContext()).getSoundInfoList().get(j);
                    ImageView imageview = (ImageView)historyPlayIcons.get(j);
                    if (ToolUtil.isLivePlaying(((SoundInfo) (obj)).radioId))
                    {
                        if (LocalMediaService.getInstance().isPaused())
                        {
                            imageview.setImageResource(0x7f020253);
                        } else
                        {
                            imageview.setImageResource(0x7f020254);
                        }
                    } else
                    {
                        imageview.setImageResource(0x7f020253);
                    }
                    j++;
                }
                break label0;
            }
            obj = (RadioSound)topRadioListModels.get(i);
            imageview = (ImageView)rankPlayIcons.get(i);
            if (ToolUtil.isLivePlaying(((RadioSound) (obj)).getRadioId()))
            {
                if (LocalMediaService.getInstance().isPaused())
                {
                    imageview.setImageResource(0x7f020253);
                } else
                {
                    imageview.setImageResource(0x7f020254);
                }
            } else
            {
                imageview.setImageResource(0x7f020253);
            }
            i++;
        } while (true);
    }

    private void updateRanklistViews()
    {
        ImageManager2.from(getActivity()).displayImage(topRadio1Cover, ((RadioSound)topRadioListModels.get(0)).getRadioCoverSmall(), 0x7f0202e0);
        topRadio1Name.setText(((RadioSound)topRadioListModels.get(0)).getRname());
        if (TextUtils.isEmpty(((RadioSound)topRadioListModels.get(0)).getProgramName()))
        {
            topRadio1ProgramName.setText("\u6682\u65E0\u8282\u76EE\u5355");
        } else
        {
            topRadio1ProgramName.setText((new StringBuilder()).append("\u6B63\u5728\u76F4\u64AD\uFF1A ").append(((RadioSound)topRadioListModels.get(0)).getProgramName()).toString());
        }
        topRadio1PlayCount.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(((RadioSound)topRadioListModels.get(0)).getRadioPlayCount())).append("\u4EBA").toString());
        ImageManager2.from(getActivity()).displayImage(topRadio2Cover, ((RadioSound)topRadioListModels.get(1)).getRadioCoverSmall(), 0x7f0202e0);
        topRadio2Name.setText(((RadioSound)topRadioListModels.get(1)).getRname());
        if (TextUtils.isEmpty(((RadioSound)topRadioListModels.get(1)).getProgramName()))
        {
            topRadio2ProgramName.setText("\u6682\u65E0\u8282\u76EE\u5355");
        } else
        {
            topRadio2ProgramName.setText((new StringBuilder()).append("\u6B63\u5728\u76F4\u64AD\uFF1A ").append(((RadioSound)topRadioListModels.get(1)).getProgramName()).toString());
        }
        topRadio2PlayCount.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(((RadioSound)topRadioListModels.get(1)).getRadioPlayCount())).append("\u4EBA").toString());
        ImageManager2.from(getActivity()).displayImage(topRadio3Cover, ((RadioSound)topRadioListModels.get(2)).getRadioCoverSmall(), 0x7f0202e0);
        topRadio3Name.setText(((RadioSound)topRadioListModels.get(2)).getRname());
        if (TextUtils.isEmpty(((RadioSound)topRadioListModels.get(2)).getProgramName()))
        {
            topRadio3ProgramName.setText("\u6682\u65E0\u8282\u76EE\u5355");
        } else
        {
            topRadio3ProgramName.setText((new StringBuilder()).append("\u6B63\u5728\u76F4\u64AD\uFF1A ").append(((RadioSound)topRadioListModels.get(2)).getProgramName()).toString());
        }
        topRadio3PlayCount.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(((RadioSound)topRadioListModels.get(2)).getRadioPlayCount())).append("\u4EBA").toString());
    }

    private void updateRecommendRadios()
    {
        ImageManager2.from(getActivity()).displayImage(recommendRadio1Cover, ((RecommendRadioListModel)recommendRadioListModels.get(0)).getPicPath(), 0x7f0202e0);
        recommendRadio1Name.setText(((RecommendRadioListModel)recommendRadioListModels.get(0)).getRname());
        ImageManager2.from(getActivity()).displayImage(recommendRadio2Cover, ((RecommendRadioListModel)recommendRadioListModels.get(1)).getPicPath(), 0x7f0202e0);
        recommendRadio2Name.setText(((RecommendRadioListModel)recommendRadioListModels.get(1)).getRname());
        ImageManager2.from(getActivity()).displayImage(recommendRadio3Cover, ((RecommendRadioListModel)recommendRadioListModels.get(2)).getPicPath(), 0x7f0202e0);
        recommendRadio3Name.setText(((RecommendRadioListModel)recommendRadioListModels.get(2)).getRname());
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        inflater = LayoutInflater.from(getActivity());
        initViews();
        registerListener();
        loadData();
    }

    public void onClick(View view)
    {
        String s;
        int i;
        boolean flag;
        flag = false;
        i = view.getId();
        s = DataCollectUtil.getDataFromView(view);
        i;
        JVM INSTR lookupswitch 14: default 136
    //                   2131361826: 575
    //                   2131362835: 137
    //                   2131362836: 169
    //                   2131362837: 201
    //                   2131362838: 226
    //                   2131362843: 258
    //                   2131362847: 299
    //                   2131362851: 340
    //                   2131362854: 525
    //                   2131362855: 381
    //                   2131362857: 429
    //                   2131362859: 477
    //                   2131362863: 550
    //                   2131363096: 762;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11 _L12 _L13 _L14 _L15
_L1:
        return;
_L3:
        view = new Bundle();
        view.putInt("type", 0);
        view.putString("xdcs_data_bundle", s);
        startFragment(com/ximalaya/ting/android/fragment/livefm/LiveFMCategoryTagFragment, view);
        return;
_L4:
        view = new Bundle();
        view.putInt("type", 1);
        view.putString("xdcs_data_bundle", s);
        startFragment(com/ximalaya/ting/android/fragment/livefm/LiveFMCategoryTagFragment, view);
        return;
_L5:
        view = new Bundle();
        view.putString("xdcs_data_bundle", s);
        startFragment(com/ximalaya/ting/android/fragment/livefm/LiveProvinceRadioTabFragment, view);
        return;
_L6:
        view = new Bundle();
        view.putInt("type", 3);
        view.putString("xdcs_data_bundle", s);
        startFragment(com/ximalaya/ting/android/fragment/livefm/LiveFMCategoryTagFragment, view);
        return;
_L7:
        if (recommendRadioListModels.size() >= 1)
        {
            PlayTools.gotoPlay(32, ModelHelper.toSoundInfo((RecommendRadioListModel)recommendRadioListModels.get(0)), getActivity(), true, s);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L8:
        if (recommendRadioListModels.size() >= 2)
        {
            PlayTools.gotoPlay(32, ModelHelper.toSoundInfo((RecommendRadioListModel)recommendRadioListModels.get(1)), getActivity(), true, s);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L9:
        if (recommendRadioListModels.size() >= 3)
        {
            PlayTools.gotoPlay(32, ModelHelper.toSoundInfo((RecommendRadioListModel)recommendRadioListModels.get(2)), getActivity(), true, s);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L11:
        if (topRadioListModels.size() >= 1)
        {
            view = (RadioSound)topRadioListModels.get(0);
            view.setCategory(1);
            PlayTools.gotoPlay(32, ModelHelper.toSoundInfo(view), getActivity(), true, s);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L12:
        if (topRadioListModels.size() >= 2)
        {
            view = (RadioSound)topRadioListModels.get(1);
            view.setCategory(1);
            PlayTools.gotoPlay(32, ModelHelper.toSoundInfo(view), getActivity(), true, s);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L13:
        if (topRadioListModels.size() >= 3)
        {
            view = (RadioSound)topRadioListModels.get(2);
            view.setCategory(1);
            PlayTools.gotoPlay(32, ModelHelper.toSoundInfo(view), getActivity(), true, s);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L10:
        view = new Bundle();
        view.putString("xdcs_data_bundle", s);
        startFragment(com/ximalaya/ting/android/fragment/livefm/RadioRankListFragment, view);
        return;
_L14:
        view = new Bundle();
        view.putString("xdcs_data_bundle", s);
        startFragment(com/ximalaya/ting/android/fragment/livefm/LivePlayHistoryFragment, view);
        return;
_L2:
        int j = 0;
        do
        {
label0:
            {
                int k = ((flag) ? 1 : 0);
                if (j < rankPlayIcons.size())
                {
                    if (topRadioListModels.size() != 0)
                    {
                        break label0;
                    }
                    k = ((flag) ? 1 : 0);
                }
                while (k < historyPlayIcons.size()) 
                {
                    Object obj = (ImageView)historyPlayIcons.get(k);
                    if (obj == view)
                    {
                        obj = (SoundInfo)historySounds.get(k);
                        playSound((ImageView)view, ((SoundInfo) (obj)), DataCollectUtil.getDataFromView(view));
                    } else
                    {
                        ((ImageView) (obj)).setImageResource(0x7f020253);
                    }
                    k++;
                }
                continue; /* Loop/switch isn't completed */
            }
            obj = (ImageView)rankPlayIcons.get(j);
            if (obj == view)
            {
                obj = (RadioSound)topRadioListModels.get(j);
                ((RadioSound) (obj)).setCategory(1);
                playSound((ImageView)view, ModelHelper.toSoundInfo(((RadioSound) (obj))), DataCollectUtil.getDataFromView(view));
            } else
            {
                ((ImageView) (obj)).setImageResource(0x7f020253);
            }
            j++;
        } while (true);
_L15:
        SoundInfo soundinfo = (SoundInfo)view.getTag();
        if (soundinfo != null)
        {
            PlayTools.gotoPlay(32, soundinfo, getActivity(), true, DataCollectUtil.getDataFromView(view));
            return;
        }
        if (true) goto _L1; else goto _L16
_L16:
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300f3, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        unRegisterListener();
    }

    public void onPlayCanceled()
    {
    }

    public void onResume()
    {
        super.onResume();
        if (getUserVisibleHint())
        {
            updatePlayHistoryViews();
            updatePlayIcons();
        }
    }

    public void onSoundChanged(int i)
    {
        if (isAdded())
        {
            updatePlayIcons();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        loadData();
    }

    public void update()
    {
        if (isAdded())
        {
            updatePlayHistoryViews();
        }
    }



/*
    static boolean access$102(LiveFMFragment livefmfragment, boolean flag)
    {
        livefmfragment.mIsDataLoaded = flag;
        return flag;
    }

*/


    private class _cls2 extends com.ximalaya.ting.android.b.a
    {

        final LiveFMFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            mIsDataLoaded = true;
            if (!TextUtils.isEmpty(s))
            {
                parseJson(s);
            }
        }

        _cls2()
        {
            this$0 = LiveFMFragment.this;
            super();
        }
    }


    private class _cls1 extends OnPlayerStatusUpdateListenerProxy
    {

        final LiveFMFragment this$0;

        public void onPlayStateChange()
        {
            if (isAdded())
            {
                updatePlayIcons();
            }
        }

        _cls1()
        {
            this$0 = LiveFMFragment.this;
            super();
        }
    }

}
