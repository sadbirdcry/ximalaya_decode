// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RadioGroup;
import com.ximalaya.ting.android.adapter.livefm.LivePlaylistAdapter;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.view.SlideView;
import java.util.ArrayList;
import java.util.List;

public class LivePlaylistFragment extends BaseFragment
{

    private static final int TYPE_TODAY = 2;
    private static final int TYPE_TOMORROW = 3;
    private static final int TYPE_YESTERDAY = 1;
    private LivePlaylistAdapter mAdapter;
    private View mEmptyView;
    private ListView mListView;
    private com.ximalaya.ting.android.view.SlideView.OnFinishListener mOnFinishListener;
    private RadioGroup mRadioGroup;
    private SlideView mRootView;
    private List sounds;
    private int type;

    public LivePlaylistFragment()
    {
        sounds = new ArrayList();
        type = 2;
    }

    private void updateEmptyView()
    {
        if (sounds.size() == 0)
        {
            mEmptyView.setVisibility(0);
            return;
        } else
        {
            mEmptyView.setVisibility(8);
            return;
        }
    }

    private void updatePlaylist()
    {
        if (mRadioGroup == null) goto _L2; else goto _L1
_L1:
        if (type != 1) goto _L4; else goto _L3
_L3:
        mRadioGroup.check(0x7f0a0080);
        if (PlayListControl.getPlayListManager().yesterdayRadioSounds != null)
        {
            sounds.clear();
            sounds.addAll(PlayListControl.getPlayListManager().yesterdayRadioSounds);
        }
_L6:
        updateEmptyView();
        mAdapter.notifyDataSetChanged();
_L2:
        return;
_L4:
        if (type == 3)
        {
            mRadioGroup.check(0x7f0a014b);
            if (PlayListControl.getPlayListManager().tomorrowSounds != null)
            {
                sounds.clear();
                sounds.addAll(PlayListControl.getPlayListManager().tomorrowSounds);
            }
        } else
        {
            mRadioGroup.check(0x7f0a0081);
            if (PlayListControl.getPlayListManager().todayRadioSounds != null)
            {
                sounds.clear();
                sounds.addAll(PlayListControl.getPlayListManager().todayRadioSounds);
            }
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            type = bundle.getInt("type");
        }
        mEmptyView = LayoutInflater.from(getActivity()).inflate(0x7f03015d, mListView, false);
        mListView.addFooterView(mEmptyView, null, false);
        mEmptyView.setVisibility(8);
        mRootView.findViewById(0x7f0a007b).setOnClickListener(new _cls1());
        mRootView.setOnFinishListener(new _cls2());
        mRadioGroup.setOnCheckedChangeListener(new _cls3());
        mListView.setOnItemClickListener(new _cls4());
        mAdapter = new LivePlaylistAdapter(getActivity(), sounds);
        mListView.setAdapter(mAdapter);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mRootView = (SlideView)layoutinflater.inflate(0x7f0300c2, viewgroup, false);
        mListView = (ListView)mRootView.findViewById(0x7f0a005c);
        mRadioGroup = (RadioGroup)mRootView.findViewById(0x7f0a035c);
        return mRootView;
    }

    public void onDestroyView()
    {
        setOnFinishListener(null);
        super.onDestroyView();
    }

    public void onResume()
    {
        super.onResume();
        updatePlaylist();
    }

    public void setOnFinishListener(com.ximalaya.ting.android.view.SlideView.OnFinishListener onfinishlistener)
    {
        mOnFinishListener = onfinishlistener;
    }





/*
    static int access$202(LivePlaylistFragment liveplaylistfragment, int i)
    {
        liveplaylistfragment.type = i;
        return i;
    }

*/



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final LivePlaylistFragment this$0;

        public void onClick(View view)
        {
            if (mOnFinishListener != null)
            {
                mOnFinishListener.onFinish();
            }
        }

        _cls1()
        {
            this$0 = LivePlaylistFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.SlideView.OnFinishListener
    {

        final LivePlaylistFragment this$0;

        public boolean onFinish()
        {
            if (mOnFinishListener != null)
            {
                return mOnFinishListener.onFinish();
            } else
            {
                return false;
            }
        }

        _cls2()
        {
            this$0 = LivePlaylistFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final LivePlaylistFragment this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            i;
            JVM INSTR lookupswitch 3: default 36
        //                       2131361920: 64
        //                       2131361921: 116
        //                       2131362123: 168;
               goto _L1 _L2 _L3 _L4
_L1:
            LivePlayerFragment.playlistType = type;
            updateEmptyView();
            mAdapter.notifyDataSetChanged();
            return;
_L2:
            if (PlayListControl.getPlayListManager().yesterdayRadioSounds != null)
            {
                sounds.clear();
                sounds.addAll(PlayListControl.getPlayListManager().yesterdayRadioSounds);
            }
            type = 1;
            continue; /* Loop/switch isn't completed */
_L3:
            if (PlayListControl.getPlayListManager().todayRadioSounds != null)
            {
                sounds.clear();
                sounds.addAll(PlayListControl.getPlayListManager().todayRadioSounds);
            }
            type = 2;
            continue; /* Loop/switch isn't completed */
_L4:
            if (PlayListControl.getPlayListManager().tomorrowSounds != null)
            {
                sounds.clear();
                sounds.addAll(PlayListControl.getPlayListManager().tomorrowSounds);
            }
            type = 3;
            if (true) goto _L1; else goto _L5
_L5:
        }

        _cls3()
        {
            this$0 = LivePlaylistFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemClickListener
    {

        final LivePlaylistFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (mOnFinishListener != null)
            {
                mOnFinishListener.onFinish();
            }
            adapterview = LocalMediaService.getInstance();
            break MISSING_BLOCK_LABEL_27;
_L4:
            do
            {
                return;
            } while (adapterview == null || type == 3);
            if (type != 1) goto _L2; else goto _L1
_L1:
            if (PlayListControl.getPlayListManager().yesterdayRadioSounds == null || PlayListControl.getPlayListManager().yesterdayRadioSounds.size() == 0 || i >= PlayListControl.getPlayListManager().yesterdayRadioSounds.size()) goto _L4; else goto _L3
_L3:
            view = (RadioSound)PlayListControl.getPlayListManager().yesterdayRadioSounds.get(i);
            if (view == null || view.getProgramId() == 0) goto _L4; else goto _L5
_L5:
            adapterview.doPlay(i);
_L7:
            updateEmptyView();
            mAdapter.notifyDataSetChanged();
            return;
_L2:
            if (type != 2) goto _L7; else goto _L6
_L6:
            if (PlayListControl.getPlayListManager().todayRadioSounds == null || PlayListControl.getPlayListManager().todayRadioSounds.size() == 0 || i >= PlayListControl.getPlayListManager().todayRadioSounds.size()) goto _L4; else goto _L8
_L8:
            view = (RadioSound)PlayListControl.getPlayListManager().todayRadioSounds.get(i);
            if (view == null || view.getProgramId() == 0 || view.getCategory() == 3) goto _L4; else goto _L9
_L9:
            adapterview.doPlay(PlayListControl.getPlayListManager().yesterdayRadioSounds.size() + i);
              goto _L7
        }

        _cls4()
        {
            this$0 = LivePlaylistFragment.this;
            super();
        }
    }

}
