// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.astuetz.PagerSlidingTabStrip;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.livefm.ProvinceCategoryAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.livefm.ProvinceModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.SlideView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.livefm:
//            LiveProvinceRadioFragment

public class LiveProvinceRadioTabFragment extends BaseActivityLikeFragment
{
    class ProvinceFragmentAdapter extends FragmentStatePagerAdapter
    {

        final LiveProvinceRadioTabFragment this$0;

        public int getCount()
        {
            return mProvinces.size();
        }

        public Fragment getItem(int i)
        {
            if (mProvinces.size() == 0)
            {
                return new Fragment();
            } else
            {
                LiveProvinceRadioFragment liveprovinceradiofragment = new LiveProvinceRadioFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("province_code", ((ProvinceModel)mProvinces.get(i)).getProvinceCode());
                liveprovinceradiofragment.setArguments(bundle);
                return liveprovinceradiofragment;
            }
        }

        public CharSequence getPageTitle(int i)
        {
            return ((ProvinceModel)mProvinces.get(i)).getProvinceName();
        }

        public ProvinceFragmentAdapter(FragmentManager fragmentmanager)
        {
            this$0 = LiveProvinceRadioTabFragment.this;
            super(fragmentmanager);
        }
    }


    private static final String PROVINCE_DATA = "province_data";
    private ProvinceFragmentAdapter adapter;
    private View holderView;
    private ProvinceCategoryAdapter mCategoryAdapter;
    private GridView mCategoryView;
    private boolean mCollapse;
    private int mCurIndexInList;
    private List mProvinces;
    private ViewPager pager;
    private View popupBg;
    private PopupWindow popupWindow;
    private SlideView slideView;
    private TextView switchHintTxt;
    private ImageView switchImg;
    private View switchLayout;
    private PagerSlidingTabStrip tabs;

    public LiveProvinceRadioTabFragment()
    {
        mProvinces = new ArrayList();
        mCurIndexInList = 0;
        mCollapse = false;
    }

    private void initData()
    {
        String s = readSavedProvinceData();
        if (!TextUtils.isEmpty(s))
        {
            parseProvinceJson(s);
        }
        loadProvinceData();
    }

    private void initListener()
    {
        _cls1 _lcls1 = new _cls1();
        tabs.setOnPageChangeListener(_lcls1);
    }

    private void initProviceCategoryView()
    {
        switchLayout = findViewById(0x7f0a0431);
        switchLayout.setVisibility(0);
        switchImg = (ImageView)findViewById(0x7f0a0432);
        switchHintTxt = (TextView)findViewById(0x7f0a0433);
        popupBg = findViewById(0x7f0a0076);
        switchImg.setOnClickListener(new _cls2());
        holderView = LayoutInflater.from(mCon).inflate(0x7f03015e, null);
        mCategoryView = (GridView)holderView.findViewById(0x7f0a0075);
        mCategoryView.setNumColumns(5);
        mCategoryAdapter = new ProvinceCategoryAdapter(mCon, mProvinces);
        mCategoryView.setAdapter(mCategoryAdapter);
        mCategoryView.setOnItemClickListener(new _cls3());
    }

    private void initTabs()
    {
        tabs.setDividerColor(0);
        tabs.setIndicatorColor(getResources().getColor(0x7f070004));
        tabs.setIndicatorHeight(Utilities.dip2px(getActivity(), 3F));
        tabs.setDividerPadding(0);
        tabs.setTabSwitch(true);
        tabs.setActivateTextColor(getResources().getColor(0x7f070004));
        tabs.setDeactivateTextColor(getResources().getColor(0x7f0700c3));
        tabs.setUnderlineHeight(0);
        tabs.setTextSize(14);
        tabs.setTabPaddingLeftRight(Utilities.dip2px(getActivity(), 20F));
        adapter = new ProvinceFragmentAdapter(getChildFragmentManager());
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);
    }

    private void initViews()
    {
        setTitleText("\u7701\u5E02\u53F0");
        slideView = (SlideView)findViewById(0x7f0a0055);
        slideView.setSlideRight(true);
        tabs = (PagerSlidingTabStrip)findViewById(0x7f0a02dc);
        tabs.setDisallowInterceptTouchEventView((ViewGroup)tabs.getParent());
        pager = (ViewPager)findViewById(0x7f0a017a);
    }

    private void loadProvinceData()
    {
        String s = (new StringBuilder()).append(a.O).append("getProvinceList").toString();
        RequestParams requestparams = new RequestParams();
        f.a().a(s, requestparams, DataCollectUtil.getDataFromView(fragmentBaseContainerView), new _cls4());
    }

    private void parseProvinceJson(String s)
    {
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            MyLogger.getLogger().d((new StringBuilder()).append("loadProvinceData exception: ").append(s.getMessage()).toString());
            return;
        }
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_120;
        }
        if (!"0000".equals(s.getString("ret")))
        {
            break MISSING_BLOCK_LABEL_120;
        }
        s = s.getString("result");
        if (TextUtils.isEmpty(s))
        {
            break MISSING_BLOCK_LABEL_120;
        }
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/livefm/ProvinceModel);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_120;
        }
        if (s.size() > 0)
        {
            ((ProvinceModel)s.get(0)).setShow(true);
            mProvinces.clear();
            mProvinces.addAll(s);
            initTabs();
            adapter.notifyDataSetChanged();
            mCategoryAdapter.notifyDataSetChanged();
            tabs.a();
        }
    }

    private String readSavedProvinceData()
    {
        return SharedPreferencesUtil.getInstance(getActivity().getApplicationContext()).getString("province_data");
    }

    private void saveProvinceData(String s)
    {
        SharedPreferencesUtil.getInstance(getActivity().getApplicationContext()).saveString("province_data", s);
    }

    private void switchProvinceView(int i)
    {
        ProvinceModel provincemodel = (ProvinceModel)mProvinces.get(i);
        ((ProvinceModel)mProvinces.get(mCurIndexInList)).setShow(false);
        provincemodel.setShow(true);
        mCurIndexInList = i;
        mCategoryAdapter.notifyDataSetChanged();
        pager.setCurrentItem(i);
        if (i == 0)
        {
            slideView.setForbidSlide(false);
            return;
        } else
        {
            slideView.setForbidSlide(true);
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initViews();
        initListener();
        initProviceCategoryView();
        initData();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300f7, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        tabs.b();
        tabs.setOnPageChangeListener(null);
        super.onDestroyView();
    }





/*
    static boolean access$102(LiveProvinceRadioTabFragment liveprovinceradiotabfragment, boolean flag)
    {
        liveprovinceradiotabfragment.mCollapse = flag;
        return flag;
    }

*/








/*
    static PopupWindow access$502(LiveProvinceRadioTabFragment liveprovinceradiotabfragment, PopupWindow popupwindow)
    {
        liveprovinceradiotabfragment.popupWindow = popupwindow;
        return popupwindow;
    }

*/





    private class _cls1
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final LiveProvinceRadioTabFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f1, int j)
        {
        }

        public void onPageSelected(int i)
        {
            switchProvinceView(i);
        }

        _cls1()
        {
            this$0 = LiveProvinceRadioTabFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final LiveProvinceRadioTabFragment this$0;

        public void onClick(View view)
        {
            if (mCollapse)
            {
                tabs.setVisibility(0);
                switchHintTxt.setVisibility(8);
                switchImg.setImageResource(0x7f020314);
                if (popupWindow != null)
                {
                    popupWindow.dismiss();
                }
                mCollapse = false;
                return;
            }
            tabs.setVisibility(8);
            switchHintTxt.setVisibility(0);
            switchImg.setImageResource(0x7f020315);
            if (popupWindow == null)
            {
                popupWindow = new PopupWindow(holderView, -1, -2);
                popupWindow.setAnimationStyle(0x7f0b000f);
                popupWindow.setFocusable(true);
                popupWindow.setOutsideTouchable(false);
                popupWindow.setBackgroundDrawable(getResources().getDrawable(0x7f070003));
            }
            popupWindow.showAsDropDown(switchLayout);
            class _cls1
                implements Runnable
            {

                final _cls2 this$1;

                public void run()
                {
                    if (canGoon())
                    {
                        popupBg.setVisibility(0);
                    }
                }

                _cls1()
                {
                    this$1 = _cls2.this;
                    super();
                }
            }

            popupBg.postDelayed(new _cls1(), 200L);
            class _cls2
                implements android.widget.PopupWindow.OnDismissListener
            {

                final _cls2 this$1;

                public void onDismiss()
                {
                    class _cls1
                        implements Runnable
                    {

                        final _cls2 this$2;

                        public void run()
                        {
                            if (canGoon())
                            {
                                tabs.setVisibility(0);
                                switchHintTxt.setVisibility(8);
                                switchImg.setImageResource(0x7f020314);
                                popupBg.setVisibility(8);
                                mCollapse = false;
                            }
                        }

                            _cls1()
                            {
                                this$2 = _cls2.this;
                                super();
                            }
                    }

                    popupBg.postDelayed(new _cls1(), 200L);
                }

                _cls2()
                {
                    this$1 = _cls2.this;
                    super();
                }
            }

            popupWindow.setOnDismissListener(new _cls2());
            mCollapse = true;
        }

        _cls2()
        {
            this$0 = LiveProvinceRadioTabFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final LiveProvinceRadioTabFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            switchProvinceView(i);
            if (popupWindow != null)
            {
                popupWindow.dismiss();
            }
        }

        _cls3()
        {
            this$0 = LiveProvinceRadioTabFragment.this;
            super();
        }
    }


    private class _cls4 extends com.ximalaya.ting.android.b.a
    {

        final LiveProvinceRadioTabFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(final String responseContent)
        {
            while (!canGoon() || TextUtils.isEmpty(responseContent)) 
            {
                return;
            }
            class _cls1
                implements MyCallback
            {

                final _cls4 this$1;
                final String val$responseContent;

                public void execute()
                {
                    parseProvinceJson(responseContent);
                    saveProvinceData(responseContent);
                }

                _cls1()
                {
                    this$1 = _cls4.this;
                    responseContent = s;
                    super();
                }
            }

            doAfterAnimation(new _cls1());
        }

        _cls4()
        {
            this$0 = LiveProvinceRadioTabFragment.this;
            super();
        }
    }

}
