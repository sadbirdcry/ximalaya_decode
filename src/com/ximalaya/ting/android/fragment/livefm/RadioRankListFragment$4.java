// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.text.TextUtils;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.livefm:
//            RadioRankListFragment

class this._cls0 extends a
{

    final RadioRankListFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
    }

    public void onFinish()
    {
        RadioRankListFragment.access$000(RadioRankListFragment.this).onRefreshComplete();
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
        RadioRankListFragment.access$500(RadioRankListFragment.this, oterViewFlag.NO_CONNECTION);
    }

    public void onStart()
    {
        RadioRankListFragment.access$500(RadioRankListFragment.this, oterViewFlag.LOADING);
    }

    public void onSuccess(final String responseContent)
    {
        if (!canGoon())
        {
            return;
        }
        if (TextUtils.isEmpty(responseContent))
        {
            showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
            RadioRankListFragment.access$500(RadioRankListFragment.this, oterViewFlag.NO_DATA);
            return;
        } else
        {
            class _cls1
                implements MyCallback
            {

                final RadioRankListFragment._cls4 this$1;
                final String val$responseContent;

                public void execute()
                {
                    RadioRankListFragment.access$600(this$0, responseContent);
                }

            _cls1()
            {
                this$1 = RadioRankListFragment._cls4.this;
                responseContent = s;
                super();
            }
            }

            RadioRankListFragment.access$700(RadioRankListFragment.this, new _cls1());
            return;
        }
    }

    _cls1()
    {
        this$0 = RadioRankListFragment.this;
        super();
    }
}
