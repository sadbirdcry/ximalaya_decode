// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.livefm.AnnouncerData;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.livefm.RadioPlayUrl;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.BlurableImageView;
import com.ximalaya.ting.android.view.seekbar.MySeekBar;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.livefm:
//            LivePlaylistFragment, LivePlayHistoryFragment

public class LivePlayerFragment extends BaseActivityLikeFragment
    implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener, com.ximalaya.ting.android.service.play.TingMediaPlayer.LiveSoundUpdateCallback, com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
{

    private static final int OPERAION_ALARM = 1;
    private static final int OPERATION_CANCEL = 2;
    private static final int OPERATION_TIMER_SHUTDOWN = 0;
    public static int playlistType = 0;
    private TextView announcersTxt;
    private View controlBar;
    private TextView endTimeTxt;
    private boolean isUpdateProcessStart;
    private ImageView mBackBtn;
    private BlurableImageView mBlurCoverBg;
    private LocalMediaService mBoundService;
    private ServiceConnection mConnection;
    private View mContentView;
    private Context mContext;
    private ImageView mExitTimerBtn;
    private boolean mForbidProgressUpdate;
    private boolean mIsBound;
    private LivePlayHistoryFragment mLivePlayHistoryFragment;
    private LivePlaylistFragment mLivePlaylistFragment;
    private LinearLayout mMovingTimeBar;
    private ImageView mNextSoundBtn;
    private Button mPlayOrPauseBtn;
    private ImageView mPreSoundBtn;
    private TextView mProgressLabel;
    private View mRightSpace;
    private MySeekBar mSeekBar;
    private SoundInfo mSoundInfo;
    private final Handler mUiHandler = new Handler(Looper.getMainLooper());
    private Runnable mUpdateProgressRunnable;
    private ProgressBar mWaittingProgressBar;
    private TextView playCountTxt;
    private TextView playHistoryTxt;
    private TextView playListTxt;
    private TextView programNameTxt;
    private TextView programNameTxtTop;
    private ImageView radioIconImg;
    private TextView radioNameTxt;
    private TextView radioNameTxtTop;
    private TextView startTimeTxt;
    private List todaySchedules;
    private List tomorrowSchedules;
    private List yesterdaySchedules;

    public LivePlayerFragment()
    {
        mBoundService = null;
        mIsBound = false;
        mForbidProgressUpdate = false;
        isUpdateProcessStart = false;
        yesterdaySchedules = new ArrayList();
        todaySchedules = new ArrayList();
        tomorrowSchedules = new ArrayList();
        mConnection = new _cls16();
        mUpdateProgressRunnable = new _cls17();
    }

    private void clearSchedulesList()
    {
        yesterdaySchedules.clear();
        todaySchedules.clear();
        tomorrowSchedules.clear();
    }

    private void doBindService()
    {
        mContext.bindService(new Intent(mContext, com/ximalaya/ting/android/service/play/LocalMediaService), mConnection, 1);
    }

    private void doUnbindService()
    {
        if (mIsBound)
        {
            mContext.unbindService(mConnection);
            mIsBound = false;
        }
    }

    private void forbidSeek()
    {
        if (mSeekBar != null)
        {
            mSeekBar.setCanSeek(false);
        }
        if (mWaittingProgressBar != null)
        {
            mWaittingProgressBar.setVisibility(0);
        }
    }

    private int getLivePlayProgress(String s)
    {
        if (TextUtils.isEmpty(s) || !s.contains(":"))
        {
            return 0;
        } else
        {
            s = s.split(":");
            int i = Integer.parseInt(s[0]);
            int j = Integer.parseInt(s[1]);
            s = Calendar.getInstance();
            s.set(11, i);
            s.set(12, j);
            return (int)(System.currentTimeMillis() - s.getTimeInMillis());
        }
    }

    private void initListeners()
    {
        mContentView.setOnTouchListener(new _cls3());
        mBackBtn.setOnClickListener(new _cls4());
        mExitTimerBtn.setOnClickListener(new _cls5());
        playListTxt.setOnClickListener(new _cls6());
        playHistoryTxt.setOnClickListener(new _cls7());
        findViewById(0x7f0a0519).setOnClickListener(new _cls8());
        findViewById(0x7f0a052b).setOnClickListener(new _cls9());
        if (mSeekBar != null)
        {
            mSeekBar.setProgressNumberFormat(new _cls10());
            mSeekBar.setOnSeekBarChangeListener(new _cls11());
        }
        if (mPreSoundBtn != null)
        {
            mPreSoundBtn.setOnClickListener(new _cls12());
        }
        if (mPlayOrPauseBtn != null)
        {
            mPlayOrPauseBtn.setOnClickListener(new _cls13());
        }
        if (mNextSoundBtn != null)
        {
            mNextSoundBtn.setOnClickListener(new _cls14());
        }
    }

    private void initViews()
    {
        mBackBtn = (ImageView)findViewById(0x7f0a0536);
        mExitTimerBtn = (ImageView)findViewById(0x7f0a0537);
        startTimeTxt = (TextView)findViewById(0x7f0a0531);
        endTimeTxt = (TextView)findViewById(0x7f0a0532);
        radioNameTxtTop = (TextView)findViewById(0x7f0a00e6);
        programNameTxtTop = (TextView)findViewById(0x7f0a0530);
        announcersTxt = (TextView)findViewById(0x7f0a051f);
        radioNameTxt = (TextView)findViewById(0x7f0a051a);
        programNameTxt = (TextView)findViewById(0x7f0a051b);
        playCountTxt = (TextView)findViewById(0x7f0a0151);
        radioIconImg = (ImageView)findViewById(0x7f0a0519);
        mBlurCoverBg = (BlurableImageView)findViewById(0x7f0a052d);
        mBlurCoverBg.setTag(0x7f0a0024, Integer.valueOf(0));
        mBlurCoverBg.setBackgroundColor(Color.parseColor("#b3202332"));
        mBlurCoverBg.setTag(0x7f0a003b, Boolean.valueOf(true));
        markImageView(mBlurCoverBg);
        controlBar = findViewById(0x7f0a052f);
        playListTxt = (TextView)controlBar.findViewById(0x7f0a00cc);
        playHistoryTxt = (TextView)controlBar.findViewById(0x7f0a0525);
        mPreSoundBtn = (ImageView)controlBar.findViewById(0x7f0a0522);
        mPlayOrPauseBtn = (Button)controlBar.findViewById(0x7f0a0523);
        mNextSoundBtn = (ImageView)controlBar.findViewById(0x7f0a0524);
        mSeekBar = (MySeekBar)controlBar.findViewById(0x7f0a0528);
        mWaittingProgressBar = (ProgressBar)controlBar.findViewById(0x7f0a0529);
        mRightSpace = controlBar.findViewById(0x7f0a0527);
        if (android.os.Build.VERSION.SDK_INT < 11)
        {
            controlBar.findViewById(0x7f0a0526).setVisibility(8);
            controlBar.findViewById(0x7f0a0527).setVisibility(8);
            mSeekBar.setThumbOffset(0);
        }
        mMovingTimeBar = (LinearLayout)findViewById(0x7f0a0533);
        mProgressLabel = (TextView)findViewById(0x7f0a0534);
    }

    private boolean isLivingProgramFinished(String s)
    {
        if (TextUtils.isEmpty(s) || !s.contains(":"))
        {
            return false;
        }
        s = s.split(":");
        int j = Integer.parseInt(s[0]);
        int i = j;
        if (j == 0)
        {
            i = 24;
        }
        j = Integer.parseInt(s[1]);
        s = Calendar.getInstance();
        int k = s.get(11);
        int l = s.get(12);
        return k > i || k == i && l >= j;
    }

    private void liveProgramFinishedAction()
    {
        if (PlayListControl.getPlayListManager().curIndex == PlayListControl.getPlayListManager().getSize() - 1)
        {
            return;
        }
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        soundinfo.playUrl32 = soundinfo.listenBackUrl;
        soundinfo.playUrl64 = soundinfo.listenBackUrl;
        soundinfo.category = 2;
        MyLogger.getLogger().d((new StringBuilder()).append("before sound: ").append(soundinfo).toString());
        int i = mBoundService.playNext(false);
        if (i > yesterdaySchedules.size() + 1)
        {
            int j = i - yesterdaySchedules.size() - 1;
            ((RadioSound)todaySchedules.get(j)).setCategory(2);
            ((RadioSound)todaySchedules.get(j + 1)).setCategory(1);
        }
        MyLogger.getLogger().d((new StringBuilder()).append("switch index=").append(i).toString());
        mSoundInfo = PlayListControl.getPlayListManager().get(i);
        mSoundInfo.category = 1;
        MyLogger.getLogger().d((new StringBuilder()).append("after sound: ").append(mSoundInfo).toString());
        updateUI();
    }

    private void loadProgramSchedules()
    {
        if (mSoundInfo == null)
        {
            return;
        } else
        {
            int i = mSoundInfo.radioId;
            String s = (new StringBuilder()).append(a.O).append("getProgramSchedules").toString();
            RequestParams requestparams = new RequestParams();
            requestparams.put("radioId", i);
            f.a().a(s, requestparams, DataCollectUtil.getDataFromView(mContentView), new _cls2());
            return;
        }
    }

    private void loadSoundDetail()
    {
        if (mSoundInfo == null)
        {
            return;
        } else
        {
            int i = mSoundInfo.radioId;
            MyLogger.getLogger().d((new StringBuilder()).append("radioId=").append(i).toString());
            String s = (new StringBuilder()).append(a.O).append("getProgramDetail").toString();
            RequestParams requestparams = new RequestParams();
            requestparams.put("radioId", i);
            f.a().a(s, requestparams, DataCollectUtil.getDataFromView(fragmentBaseContainerView), new _cls1());
            return;
        }
    }

    private void restoreUiToDefault()
    {
        mRightSpace.setBackgroundResource(0x7f070049);
    }

    private void showOperationChooser()
    {
        final MenuDialog dialog = new MenuDialog(getActivity(), 0x7f0c0029);
        dialog.setOnItemClickListener(new _cls15());
        dialog.show();
    }

    private void startUpdateProgress()
    {
        isUpdateProcessStart = true;
        mUiHandler.postDelayed(mUpdateProgressRunnable, 1000L);
    }

    private void stopUpdateProgress()
    {
        isUpdateProcessStart = false;
        mUiHandler.removeCallbacks(mUpdateProgressRunnable);
    }

    private void unForbidSeek()
    {
        if (mWaittingProgressBar != null)
        {
            mWaittingProgressBar.setVisibility(8);
        }
        if (mSeekBar != null)
        {
            mSeekBar.setCanSeek(true);
        }
    }

    private void updateEmptySoundInfo()
    {
        mSoundInfo.programName = "\u6682\u65E0\u8282\u76EE\u5355";
        mSoundInfo.programScheduleId = 0L;
        mSoundInfo.programId = 0L;
        mSoundInfo.announcerList = null;
        mSoundInfo.startTime = "00:00";
        mSoundInfo.endTime = "24:00";
        mSoundInfo.validDate = new Date();
    }

    private void updateMoveTime(int i, int j)
    {
        if (i >= 0 && j >= 0 && i <= j)
        {
            int k = ToolUtil.getScreenWidth(mContext);
            if (j > 0 && mMovingTimeBar != null && mProgressLabel != null && mSoundInfo != null)
            {
                mProgressLabel.setVisibility(0);
                String as[] = mSoundInfo.startTime.split(":");
                int l = Integer.parseInt(as[0]);
                String s = ToolUtil.toTime(Integer.parseInt(as[1]) * 60 + l * 3600 + i / 1000);
                mProgressLabel.setText((new StringBuilder()).append(s).append("/").append(mSoundInfo.endTime).append(":00").toString());
                if (mProgressLabel.getWidth() > 0)
                {
                    float f1 = (float)i / (float)j;
                    mMovingTimeBar.setPadding((int)((float)(k - mProgressLabel.getWidth()) * f1), 0, 0, 0);
                    return;
                }
            }
        }
    }

    private void updatePlayControlBar()
    {
        SoundInfo soundinfo;
        int i;
        i = PlayListControl.getPlayListManager().curIndex;
        i = PlayListControl.getPlayListManager().getSize();
        soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo == null)
        {
            return;
        }
        if (soundinfo.category != 1) goto _L2; else goto _L1
_L1:
        if (i > 1)
        {
            mNextSoundBtn.setEnabled(false);
            mPreSoundBtn.setEnabled(true);
        } else
        {
            mNextSoundBtn.setEnabled(false);
            mPreSoundBtn.setEnabled(false);
        }
_L4:
        updatePlayPauseSwitchButton();
        return;
_L2:
        if (soundinfo.category == 2)
        {
            mPreSoundBtn.setEnabled(true);
            mNextSoundBtn.setEnabled(true);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void updatePlayPauseSwitchButton()
    {
        while (this == null || !isAdded() || mBoundService == null || mBoundService.getMediaPlayerState() != 4) 
        {
            return;
        }
        mPlayOrPauseBtn.setBackgroundResource(0x7f0203de);
        mPlayOrPauseBtn.setContentDescription("\u6682\u505C");
    }

    private void updateProgress(int i, int j)
    {
        if (mSoundInfo != null && mSeekBar != null && j > 0)
        {
            if (mSeekBar.getMax() != LocalMediaService.getInstance().getDuration())
            {
                mSeekBar.setMax(LocalMediaService.getInstance().getDuration());
            }
            mSeekBar.setProgress(i);
            mSeekBar.invalidate();
        }
    }

    private void updateSoundInfoView()
    {
        if (mSoundInfo == null)
        {
            return;
        }
        startTimeTxt.setText(mSoundInfo.startTime);
        endTimeTxt.setText(mSoundInfo.endTime);
        radioNameTxtTop.setText(mSoundInfo.radioName);
        programNameTxtTop.setText(mSoundInfo.programName);
        Object obj = mSoundInfo.announcerList;
        if (obj != null && ((List) (obj)).size() > 0)
        {
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.append("\u4E3B\u64AD:");
            for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); stringbuilder.append(" "))
            {
                stringbuilder.append(((AnnouncerData)((Iterator) (obj)).next()).getAnnouncerName());
            }

            announcersTxt.setVisibility(0);
            announcersTxt.setText(stringbuilder.toString());
        } else
        {
            announcersTxt.setVisibility(8);
        }
        radioNameTxt.setText(mSoundInfo.radioName);
        programNameTxt.setText((new StringBuilder()).append("\u6B63\u5728\u76F4\u64AD\uFF1A").append(mSoundInfo.programName).toString());
        playCountTxt.setText(StringUtil.getFriendlyNumStr(mSoundInfo.plays_counts));
        ImageManager2.from(mContext).displayImage(radioIconImg, mSoundInfo.coverSmall, 0x7f0202de);
        ImageManager2.from(mContext).displayImage(mBlurCoverBg, mSoundInfo.coverLarge, 0x7f0202de);
    }

    private void updateTodaySchedulesStatus()
    {
        Iterator iterator = todaySchedules.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            RadioSound radiosound = (RadioSound)iterator.next();
            radiosound.setRadioCoverSmall(mSoundInfo.coverSmall);
            radiosound.setRadioCoverLarge(mSoundInfo.coverLarge);
            radiosound.setRadioId(mSoundInfo.radioId);
            radiosound.setRname(mSoundInfo.radioName);
            radiosound.setRadioPlayCount(mSoundInfo.plays_counts);
            radiosound.setLiveUrl(mSoundInfo.liveUrl);
            radiosound.setFmuid(mSoundInfo.uid);
            Object obj;
            String s;
            if (TingMediaPlayer.getTingMediaPlayer(getActivity().getApplicationContext()).isUseSystemPlayer())
            {
                obj = (new StringBuilder()).append(radiosound.getListenBackUrl()).append("&transcode=ts").toString();
            } else
            {
                obj = radiosound.getListenBackUrl();
            }
            radiosound.setListenBackUrl(((String) (obj)));
            s = radiosound.getStartTime();
            obj = radiosound.getEndTime();
            if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(((CharSequence) (obj))))
            {
                String as[] = s.split(":");
                int k = Integer.parseInt(as[0]);
                int l = Integer.parseInt(as[1]);
                obj = ((String) (obj)).split(":");
                int j = Integer.parseInt(obj[0]);
                int i = j;
                if (k > j)
                {
                    i = j;
                    if (j == 0)
                    {
                        i = 24;
                    }
                }
                j = Integer.parseInt(obj[1]);
                obj = Calendar.getInstance();
                int i1 = ((Calendar) (obj)).get(11);
                int j1 = ((Calendar) (obj)).get(12);
                if (i1 < k || i1 == k && j1 < l)
                {
                    radiosound.setCategory(3);
                } else
                if (i1 > i || i1 == i && j1 > j)
                {
                    radiosound.setCategory(2);
                } else
                {
                    radiosound.setCategory(1);
                    RadioPlayUrl radioplayurl = new RadioPlayUrl();
                    radioplayurl.setRadio_24_aac(mSoundInfo.playUrl32);
                    radioplayurl.setRadio_24_ts(mSoundInfo.playUrl32);
                    radiosound.setRadioPlayUrl(radioplayurl);
                }
            }
        } while (true);
    }

    private void updateTomorrowSchedulesStatus()
    {
        Iterator iterator = tomorrowSchedules.iterator();
        while (iterator.hasNext()) 
        {
            RadioSound radiosound = (RadioSound)iterator.next();
            radiosound.setRadioCoverSmall(mSoundInfo.coverSmall);
            radiosound.setRadioCoverLarge(mSoundInfo.coverLarge);
            radiosound.setRadioId(mSoundInfo.radioId);
            radiosound.setRname(mSoundInfo.radioName);
            radiosound.setRadioPlayCount(mSoundInfo.plays_counts);
            radiosound.setFmuid(mSoundInfo.uid);
            String s;
            if (TingMediaPlayer.getTingMediaPlayer(getActivity().getApplicationContext()).isUseSystemPlayer())
            {
                s = (new StringBuilder()).append(radiosound.getListenBackUrl()).append("&transcode=ts").toString();
            } else
            {
                s = radiosound.getListenBackUrl();
            }
            radiosound.setListenBackUrl(s);
            radiosound.setCategory(3);
            radiosound.setLiveUrl(mSoundInfo.liveUrl);
        }
    }

    private void updateUI()
    {
        updateSoundInfoView();
        updatePlayControlBar();
    }

    private void updateYesterdaySchedulesStatus()
    {
        Iterator iterator = yesterdaySchedules.iterator();
        while (iterator.hasNext()) 
        {
            RadioSound radiosound = (RadioSound)iterator.next();
            radiosound.setRadioCoverSmall(mSoundInfo.coverSmall);
            radiosound.setRadioCoverLarge(mSoundInfo.coverLarge);
            radiosound.setRadioId(mSoundInfo.radioId);
            radiosound.setRname(mSoundInfo.radioName);
            radiosound.setRadioPlayCount(mSoundInfo.plays_counts);
            radiosound.setCategory(2);
            radiosound.setFmuid(mSoundInfo.uid);
            String s;
            if (TingMediaPlayer.getTingMediaPlayer(getActivity().getApplicationContext()).isUseSystemPlayer())
            {
                s = (new StringBuilder()).append(radiosound.getListenBackUrl()).append("&transcode=ts").toString();
            } else
            {
                s = radiosound.getListenBackUrl();
            }
            radiosound.setListenBackUrl(s);
            radiosound.setLiveUrl(mSoundInfo.liveUrl);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        MyLogger.getLogger().print();
        mContext = getActivity();
        mSoundInfo = PlayListControl.getPlayListManager().getCurSound();
        initViews();
        initListeners();
        loadSoundDetail();
        doBindService();
        restoreUiToDefault();
    }

    public boolean onBackPressed()
    {
        if (mLivePlaylistFragment != null && mLivePlaylistFragment.isVisible())
        {
            getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).remove(mLivePlaylistFragment).commitAllowingStateLoss();
            mLivePlaylistFragment = null;
            return true;
        }
        if (mLivePlayHistoryFragment != null && mLivePlayHistoryFragment.isVisible())
        {
            getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).remove(mLivePlayHistoryFragment).commitAllowingStateLoss();
            mLivePlayHistoryFragment = null;
            return true;
        } else
        {
            return false;
        }
    }

    public void onBind(String s)
    {
        DataCollectUtil.bindDataToView(s, fragmentBaseContainerView);
    }

    public void onBufferUpdated(int i)
    {
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        if (ToolUtil.isUseSmartbarAsTab())
        {
            setHasOptionsMenu(true);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mContentView = layoutinflater.inflate(0x7f03015c, null);
        fragmentBaseContainerView = mContentView;
        return mContentView;
    }

    public void onDestroyView()
    {
        if (mBoundService != null)
        {
            mBoundService.removeOnPlayerUpdateListener(this);
            mBoundService.removeOnPlayServiceUpdateListener(this);
            mBoundService.removeLiveSoundUpdateCallback();
        }
        stopUpdateProgress();
        doUnbindService();
        super.onDestroyView();
    }

    public void onLogoPlayFinished()
    {
    }

    public void onPause()
    {
        super.onPause();
    }

    public void onPlayCanceled()
    {
        MyLogger.getLogger().print();
    }

    public void onPlayCompleted()
    {
        MyLogger.getLogger().print();
        if (this != null && isAdded())
        {
            mPlayOrPauseBtn.setBackgroundResource(0x7f020406);
            mPlayOrPauseBtn.setContentDescription("\u5F00\u59CB\u64AD\u653E");
            mUiHandler.removeCallbacks(mUpdateProgressRunnable);
        }
    }

    public void onPlayPaused()
    {
        MyLogger.getLogger().print();
        if (this != null && isAdded())
        {
            mPlayOrPauseBtn.setBackgroundResource(0x7f020406);
            mPlayOrPauseBtn.setContentDescription("\u5F00\u59CB\u64AD\u653E");
        }
    }

    public void onPlayProgressUpdate(int i, int j)
    {
        while (this == null || !isAdded() || mSoundInfo == null || mSoundInfo.category != 2) 
        {
            return;
        }
        updatePlayProgress(i, j);
    }

    public void onPlayStarted()
    {
label0:
        {
            MyLogger.getLogger().print();
            if (this != null && isAdded())
            {
                mPlayOrPauseBtn.setBackgroundResource(0x7f0203de);
                mPlayOrPauseBtn.setContentDescription("\u6682\u505C");
                if (mSoundInfo == null || mSoundInfo.category != 1)
                {
                    break label0;
                }
                if (mWaittingProgressBar != null)
                {
                    mWaittingProgressBar.setVisibility(8);
                }
                if (mSeekBar != null)
                {
                    mSeekBar.setCanSeek(false);
                }
                if (isVisible())
                {
                    startUpdateProgress();
                }
            }
            return;
        }
        stopUpdateProgress();
        unForbidSeek();
    }

    public void onPlayerBuffering(boolean flag)
    {
label0:
        {
            Logger.log((new StringBuilder()).append("onPlayerBuffering updateOnPlayerBuffering 111").append(flag).toString());
            if (this != null && isAdded())
            {
                if (!flag)
                {
                    break label0;
                }
                forbidSeek();
            }
            return;
        }
        unForbidSeek();
    }

    public void onPrepareOptionsMenu(Menu menu)
    {
        if (ToolUtil.isUseSmartbarAsTab())
        {
            getActivity().getActionBar().removeAllTabs();
        }
        super.onPrepareOptionsMenu(menu);
    }

    public void onResume()
    {
        super.onResume();
        if (mSoundInfo != null && mBoundService != null && mBoundService.isPlaying() && mSoundInfo.category == 1)
        {
            startUpdateProgress();
        }
    }

    public void onSoundChanged(int i)
    {
        MyLogger.getLogger().print();
        if (this != null && isAdded() && mWaittingProgressBar != null)
        {
            mWaittingProgressBar.setVisibility(0);
        }
        stopUpdateProgress();
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo == null || soundinfo.category == 0)
        {
            mSoundInfo = PlayListControl.getPlayListManager().getCurSound();
            return;
        }
        if (this != null && isAdded())
        {
            if (soundinfo.radioId != mSoundInfo.radioId)
            {
                playlistType = 0;
                mSoundInfo = PlayListControl.getPlayListManager().getCurSound();
                clearSchedulesList();
                loadSoundDetail();
            } else
            {
                mSoundInfo = PlayListControl.getPlayListManager().getCurSound();
                updateTodaySchedulesStatus();
            }
        }
        updateUI();
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        MyLogger.getLogger().print();
    }

    public void onSoundPrepared(int i)
    {
        MyLogger.getLogger().print();
        if (this != null && isAdded() && mWaittingProgressBar != null)
        {
            mWaittingProgressBar.setVisibility(8);
        }
    }

    public void onStartPlayLogo()
    {
    }

    public void updateLiveSound()
    {
        MyLogger.getLogger().print();
        mSoundInfo.programName = "\u6682\u65E0\u8282\u76EE\u5355";
        mSoundInfo.category = 1;
        mSoundInfo.programScheduleId = 0L;
        mSoundInfo.programId = 0L;
        mSoundInfo.announcerList = null;
        mSoundInfo.startTime = "00:00";
        mSoundInfo.endTime = "24:00";
        mSoundInfo.playUrl32 = mSoundInfo.liveUrl;
        mSoundInfo.playUrl64 = mSoundInfo.liveUrl;
        mSoundInfo.validDate = new Date();
        clearSchedulesList();
        forbidSeek();
        loadSoundDetail();
        PlayListControl.getPlayListManager().curPlaySrc = null;
        if (mBoundService != null)
        {
            mBoundService.doPlay();
        }
    }

    public void updatePlayProgress(int i, int j)
    {
        if (i > j || mForbidProgressUpdate)
        {
            return;
        } else
        {
            updateProgress(i, j);
            return;
        }
    }













/*
    static LivePlaylistFragment access$1702(LivePlayerFragment liveplayerfragment, LivePlaylistFragment liveplaylistfragment)
    {
        liveplayerfragment.mLivePlaylistFragment = liveplaylistfragment;
        return liveplaylistfragment;
    }

*/



/*
    static LivePlayHistoryFragment access$1802(LivePlayerFragment liveplayerfragment, LivePlayHistoryFragment liveplayhistoryfragment)
    {
        liveplayerfragment.mLivePlayHistoryFragment = liveplayhistoryfragment;
        return liveplayhistoryfragment;
    }

*/





/*
    static boolean access$2102(LivePlayerFragment liveplayerfragment, boolean flag)
    {
        liveplayerfragment.mForbidProgressUpdate = flag;
        return flag;
    }

*/





/*
    static LocalMediaService access$2402(LivePlayerFragment liveplayerfragment, LocalMediaService localmediaservice)
    {
        liveplayerfragment.mBoundService = localmediaservice;
        return localmediaservice;
    }

*/




/*
    static boolean access$2702(LivePlayerFragment liveplayerfragment, boolean flag)
    {
        liveplayerfragment.mIsBound = flag;
        return flag;
    }

*/













    private class _cls16
        implements ServiceConnection
    {

        final LivePlayerFragment this$0;

        public void onServiceConnected(ComponentName componentname, IBinder ibinder)
        {
            mBoundService = ((com.ximalaya.ting.android.service.play.LocalMediaService.LocalBinder)ibinder).getService();
            mBoundService.setOnPlayerStatusUpdateListener(LivePlayerFragment.this);
            mBoundService.setOnPlayServiceUpdateListener(LivePlayerFragment.this);
            mBoundService.setLiveSoundUpdateCallback(LivePlayerFragment.this);
            mIsBound = true;
            updatePlayPauseSwitchButton();
        }

        public void onServiceDisconnected(ComponentName componentname)
        {
            mBoundService = null;
        }

        _cls16()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls17
        implements Runnable
    {

        final LivePlayerFragment this$0;

        public void run()
        {
            if (mSeekBar != null && mSoundInfo != null)
            {
                if (mSeekBar.getMax() != LocalMediaService.getInstance().getDuration())
                {
                    mSeekBar.setMax(LocalMediaService.getInstance().getDuration());
                }
                mSeekBar.setProgress(getLivePlayProgress(mSoundInfo.startTime));
                mSeekBar.invalidate();
                if (isLivingProgramFinished(mSoundInfo.endTime))
                {
                    liveProgramFinishedAction();
                }
                mUiHandler.postDelayed(mUpdateProgressRunnable, 1000L);
            }
        }

        _cls17()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnTouchListener
    {

        final LivePlayerFragment this$0;

        public boolean onTouch(View view, MotionEvent motionevent)
        {
            return true;
        }

        _cls3()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final LivePlayerFragment this$0;

        public void onClick(View view)
        {
            view = getActivity();
            if (view instanceof MainTabActivity2)
            {
                ((MainTabActivity2)view).onbackLivePlayFragment();
            }
        }

        _cls4()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final LivePlayerFragment this$0;

        public void onClick(View view)
        {
            showOperationChooser();
        }

        _cls5()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final LivePlayerFragment this$0;

        public void onClick(View view)
        {
            if (mSoundInfo == null)
            {
                return;
            } else
            {
                mLivePlaylistFragment = new LivePlaylistFragment();
                view = new Bundle();
                view.putInt("type", LivePlayerFragment.playlistType);
                mLivePlaylistFragment.setArguments(view);
                class _cls1
                    implements com.ximalaya.ting.android.view.SlideView.OnFinishListener
                {

                    final _cls6 this$1;

                    public boolean onFinish()
                    {
                        getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).remove(mLivePlaylistFragment).commitAllowingStateLoss();
                        mLivePlaylistFragment = null;
                        return true;
                    }

                _cls1()
                {
                    this$1 = _cls6.this;
                    super();
                }
                }

                mLivePlaylistFragment.setOnFinishListener(new _cls1());
                view = getChildFragmentManager().beginTransaction();
                view.setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020);
                view.add(0x7f0a0372, mLivePlaylistFragment);
                view.commitAllowingStateLoss();
                return;
            }
        }

        _cls6()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.view.View.OnClickListener
    {

        final LivePlayerFragment this$0;

        public void onClick(View view)
        {
            mLivePlayHistoryFragment = new LivePlayHistoryFragment();
            view = new Bundle();
            view.putBoolean("exit_when_play", true);
            mLivePlayHistoryFragment.setArguments(view);
            class _cls1
                implements com.ximalaya.ting.android.view.SlideView.OnFinishListener
            {

                final _cls7 this$1;

                public boolean onFinish()
                {
                    getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).remove(mLivePlayHistoryFragment).commitAllowingStateLoss();
                    mLivePlayHistoryFragment = null;
                    return true;
                }

                _cls1()
                {
                    this$1 = _cls7.this;
                    super();
                }
            }

            mLivePlayHistoryFragment.setOnFinishCallback(new _cls1());
            getChildFragmentManager().beginTransaction().setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020).add(0x7f0a0372, mLivePlayHistoryFragment).commitAllowingStateLoss();
        }

        _cls7()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls8
        implements android.view.View.OnClickListener
    {

        final LivePlayerFragment this$0;

        public void onClick(View view)
        {
label0:
            {
                if (mSoundInfo != null)
                {
                    if (mSoundInfo.uid != 0L)
                    {
                        break label0;
                    }
                    loadSoundDetail();
                }
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putLong("toUid", mSoundInfo.uid);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
        }

        _cls8()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls9
        implements android.view.View.OnClickListener
    {

        final LivePlayerFragment this$0;

        public void onClick(View view)
        {
label0:
            {
                if (mSoundInfo != null)
                {
                    if (mSoundInfo.uid != 0L)
                    {
                        break label0;
                    }
                    loadSoundDetail();
                }
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putLong("toUid", mSoundInfo.uid);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
        }

        _cls9()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls10
        implements com.ximalaya.ting.android.view.seekbar.MySeekBar.ProgressNubmerFormat
    {

        final LivePlayerFragment this$0;

        public String format(int i, int j)
        {
            if (mSoundInfo.category == 1)
            {
                return (new SimpleDateFormat("HH:mm:ss")).format(new Date());
            }
            if (TextUtils.isEmpty(mSoundInfo.startTime))
            {
                return "";
            } else
            {
                String as[] = mSoundInfo.startTime.split(":");
                j = Integer.parseInt(as[0]);
                return ToolUtil.toTime(Integer.parseInt(as[1]) * 60 + j * 3600 + i / 1000);
            }
        }

        public String getFormatString(int i, int j)
        {
            return "00:00:00";
        }

        _cls10()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls11
        implements android.widget.SeekBar.OnSeekBarChangeListener
    {

        final LivePlayerFragment this$0;

        public void onProgressChanged(SeekBar seekbar, int i, boolean flag)
        {
            if (flag)
            {
                seekbar = LocalMediaService.getInstance();
                if (seekbar != null)
                {
                    int j = seekbar.getDuration();
                    updateMoveTime(i, j);
                }
            }
        }

        public void onStartTrackingTouch(SeekBar seekbar)
        {
            mForbidProgressUpdate = true;
        }

        public void onStopTrackingTouch(SeekBar seekbar)
        {
            forbidSeek();
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            if (localmediaservice != null)
            {
                localmediaservice.seekToProgress(seekbar.getProgress(), seekbar.getMax());
            }
            mForbidProgressUpdate = false;
            mProgressLabel.setVisibility(4);
        }

        _cls11()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls12
        implements android.view.View.OnClickListener
    {

        final LivePlayerFragment this$0;

        public void onClick(View view)
        {
            if (mBoundService != null)
            {
                ToolUtil.onEvent(mContext, "Nowplaying_BackOne");
                int i = mBoundService.playPrev(true);
                MyLogger.getLogger().d((new StringBuilder()).append("preBtn index = ").append(i).toString());
                mUiHandler.removeCallbacks(mUpdateProgressRunnable);
            }
        }

        _cls12()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls13
        implements android.view.View.OnClickListener
    {

        final LivePlayerFragment this$0;

        public void onClick(View view)
        {
            if (mBoundService == null) goto _L2; else goto _L1
_L1:
            ToolUtil.onEvent(mContext, "Nowplaying_Paly");
            mBoundService.getMediaPlayerState();
            JVM INSTR tableswitch 0 8: default 84
        //                       0 85
        //                       1 84
        //                       2 171
        //                       3 97
        //                       4 160
        //                       5 108
        //                       6 85
        //                       7 97
        //                       8 171;
               goto _L2 _L3 _L2 _L4 _L5 _L6 _L7 _L3 _L5 _L4
_L2:
            return;
_L3:
            mBoundService.doPlay();
            return;
_L5:
            mBoundService.start();
            return;
_L7:
            if (mSoundInfo.category == 1 && ToolUtil.isLiveSoundInValidDate(mSoundInfo.validDate))
            {
                mBoundService.replay();
                return;
            } else
            {
                mBoundService.start();
                return;
            }
_L6:
            mBoundService.pause();
            return;
_L4:
            forbidSeek();
            mBoundService.restart();
            return;
        }

        _cls13()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls14
        implements android.view.View.OnClickListener
    {

        final LivePlayerFragment this$0;

        public void onClick(View view)
        {
            if (mBoundService != null)
            {
                ToolUtil.onEvent(mContext, "Nowplaying_NextOne");
                mBoundService.playNext(true);
            }
        }

        _cls14()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls2 extends com.ximalaya.ting.android.b.a
    {

        final LivePlayerFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mContentView);
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            s = JSON.parseObject(s);
            if (!"0000".equals(s.getString("ret"))) goto _L4; else goto _L3
_L3:
            s = s.getString("result");
            if (TextUtils.isEmpty(s)) goto _L1; else goto _L5
_L5:
            Object obj;
            s = JSON.parseObject(s);
            obj = s.getString("yesterdaySchedules");
            if (TextUtils.isEmpty(((CharSequence) (obj)))) goto _L7; else goto _L6
_L6:
            yesterdaySchedules.clear();
            obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/livefm/RadioSound);
            if (obj == null) goto _L7; else goto _L8
_L8:
            if (((List) (obj)).size() > 0)
            {
                yesterdaySchedules.addAll(((java.util.Collection) (obj)));
            }
_L7:
            obj = s.getString("todaySchedules");
            if (TextUtils.isEmpty(((CharSequence) (obj)))) goto _L10; else goto _L9
_L9:
            todaySchedules.clear();
            obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/livefm/RadioSound);
            if (obj == null) goto _L10; else goto _L11
_L11:
            if (((List) (obj)).size() > 0)
            {
                todaySchedules.addAll(((java.util.Collection) (obj)));
            }
_L10:
            s = s.getString("tomorrowSchedules");
            if (TextUtils.isEmpty(s)) goto _L13; else goto _L12
_L12:
            tomorrowSchedules.clear();
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/livefm/RadioSound);
            if (s == null) goto _L13; else goto _L14
_L14:
            if (s.size() > 0)
            {
                tomorrowSchedules.addAll(s);
            }
_L13:
            updateYesterdaySchedulesStatus();
            updateTodaySchedulesStatus();
            updateTomorrowSchedulesStatus();
            s = new ArrayList();
            s.addAll(yesterdaySchedules);
            s.addAll(todaySchedules);
            s.addAll(tomorrowSchedules);
            PlayListControl.getPlayListManager().updateLivePlaylist(ModelHelper.toSoundInfolist(s));
            updateUI();
_L16:
            PlayListControl.getPlayListManager().yesterdayRadioSounds = yesterdaySchedules;
            PlayListControl.getPlayListManager().todayRadioSounds = todaySchedules;
            PlayListControl.getPlayListManager().tomorrowSounds = tomorrowSchedules;
            return;
            s;
            MyLogger.getLogger().d((new StringBuilder()).append("exception:").append(s.getMessage()).toString());
            return;
_L4:
            if (!"2002".equals(s.getString("ret"))) goto _L16; else goto _L15
_L15:
            clearSchedulesList();
              goto _L16
        }

        _cls2()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls1 extends com.ximalaya.ting.android.b.a
    {

        final LivePlayerFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mContentView);
        }

        public void onNetError(int i, String s)
        {
            MyLogger.getLogger().d((new StringBuilder()).append("statusCode =").append(i).append(", errorMessage=").append(s).toString());
            updateEmptySoundInfo();
            updateUI();
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                return;
            }
            s = JSON.parseObject(s);
            if (!"0000".equals(s.getString("ret")))
            {
                break MISSING_BLOCK_LABEL_329;
            }
            s = s.getString("result");
            if (TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            s = (RadioSound)JSON.parseObject(s, com/ximalaya/ting/android/model/livefm/RadioSound);
            MyLogger.getLogger().d((new StringBuilder()).append("radioSound=").append(s).toString());
            if (s == null) goto _L4; else goto _L3
_L3:
            if (TextUtils.isEmpty(s.getProgramName())) goto _L4; else goto _L5
_L5:
            mSoundInfo.programName = s.getProgramName();
            mSoundInfo.programScheduleId = s.getProgramScheduleId();
            mSoundInfo.programId = s.getProgramId();
            mSoundInfo.startTime = s.getStartTime();
            mSoundInfo.endTime = s.getEndTime();
            mSoundInfo.coverLarge = s.getPlayBackgroundPic();
            mSoundInfo.announcerList = s.getAnnouncerList();
            mSoundInfo.uid = s.getFmuid();
            mSoundInfo.validDate = new Date();
_L6:
            updateUI();
            TingMediaPlayer.getTingMediaPlayer(mContext).updateLiveDuration();
            updateProgress(0, LocalMediaService.getInstance().getDuration());
            if (!isUpdateProcessStart)
            {
                startUpdateProgress();
            }
            loadProgramSchedules();
            return;
_L4:
            try
            {
                updateEmptySoundInfo();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                MyLogger.getLogger().d((new StringBuilder()).append("loadSoundDetail exception=").append(s.getMessage()).toString());
                return;
            }
              goto _L6
_L2:
            updateEmptySoundInfo();
              goto _L6
            updateEmptySoundInfo();
              goto _L6
        }

        _cls1()
        {
            this$0 = LivePlayerFragment.this;
            super();
        }
    }


    private class _cls15
        implements android.widget.AdapterView.OnItemClickListener
    {

        final LivePlayerFragment this$0;
        final MenuDialog val$dialog;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (dialog != null)
            {
                dialog.dismiss();
            }
            switch (i)
            {
            default:
                return;

            case 0: // '\0'
                ToolUtil.onEvent(mContext, "Nowplaying_DingShi");
                startActivity(new Intent(mContext, com/ximalaya/ting/android/activity/setting/PlanTerminateActivity));
                return;

            case 1: // '\001'
                adapterview = new Intent(mContext, com/ximalaya/ting/android/activity/setting/WakeUpSettingActivity);
                break;
            }
            adapterview.putExtra("from", 2);
            startActivity(adapterview);
        }

        _cls15()
        {
            this$0 = LivePlayerFragment.this;
            dialog = menudialog;
            super();
        }
    }

}
