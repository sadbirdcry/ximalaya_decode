// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.widget.SeekBar;
import android.widget.TextView;
import com.ximalaya.ting.android.service.play.LocalMediaService;

// Referenced classes of package com.ximalaya.ting.android.fragment.livefm:
//            LivePlayerFragment

class this._cls0
    implements android.widget.eListener
{

    final LivePlayerFragment this$0;

    public void onProgressChanged(SeekBar seekbar, int i, boolean flag)
    {
        if (flag)
        {
            seekbar = LocalMediaService.getInstance();
            if (seekbar != null)
            {
                int j = seekbar.getDuration();
                LivePlayerFragment.access$2300(LivePlayerFragment.this, i, j);
            }
        }
    }

    public void onStartTrackingTouch(SeekBar seekbar)
    {
        LivePlayerFragment.access$2102(LivePlayerFragment.this, true);
    }

    public void onStopTrackingTouch(SeekBar seekbar)
    {
        LivePlayerFragment.access$2000(LivePlayerFragment.this);
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.seekToProgress(seekbar.getProgress(), seekbar.getMax());
        }
        LivePlayerFragment.access$2102(LivePlayerFragment.this, false);
        LivePlayerFragment.access$2200(LivePlayerFragment.this).setVisibility(4);
    }

    ()
    {
        this$0 = LivePlayerFragment.this;
        super();
    }
}
