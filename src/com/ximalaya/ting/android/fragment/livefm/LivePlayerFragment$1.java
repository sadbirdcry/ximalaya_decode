// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.util.Date;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.livefm:
//            LivePlayerFragment

class this._cls0 extends a
{

    final LivePlayerFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, LivePlayerFragment.access$800(LivePlayerFragment.this));
    }

    public void onNetError(int i, String s)
    {
        MyLogger.getLogger().d((new StringBuilder()).append("statusCode =").append(i).append(", errorMessage=").append(s).toString());
        LivePlayerFragment.access$100(LivePlayerFragment.this);
        LivePlayerFragment.access$200(LivePlayerFragment.this);
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        s = JSON.parseObject(s);
        if (!"0000".equals(s.getString("ret")))
        {
            break MISSING_BLOCK_LABEL_329;
        }
        s = s.getString("result");
        if (TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        s = (RadioSound)JSON.parseObject(s, com/ximalaya/ting/android/model/livefm/RadioSound);
        MyLogger.getLogger().d((new StringBuilder()).append("radioSound=").append(s).toString());
        if (s == null) goto _L4; else goto _L3
_L3:
        if (TextUtils.isEmpty(s.getProgramName())) goto _L4; else goto _L5
_L5:
        LivePlayerFragment.access$000(LivePlayerFragment.this).programName = s.getProgramName();
        LivePlayerFragment.access$000(LivePlayerFragment.this).programScheduleId = s.getProgramScheduleId();
        LivePlayerFragment.access$000(LivePlayerFragment.this).programId = s.getProgramId();
        LivePlayerFragment.access$000(LivePlayerFragment.this).startTime = s.getStartTime();
        LivePlayerFragment.access$000(LivePlayerFragment.this).endTime = s.getEndTime();
        LivePlayerFragment.access$000(LivePlayerFragment.this).coverLarge = s.getPlayBackgroundPic();
        LivePlayerFragment.access$000(LivePlayerFragment.this).announcerList = s.getAnnouncerList();
        LivePlayerFragment.access$000(LivePlayerFragment.this).uid = s.getFmuid();
        LivePlayerFragment.access$000(LivePlayerFragment.this).validDate = new Date();
_L6:
        LivePlayerFragment.access$200(LivePlayerFragment.this);
        TingMediaPlayer.getTingMediaPlayer(LivePlayerFragment.access$300(LivePlayerFragment.this)).updateLiveDuration();
        LivePlayerFragment.access$400(LivePlayerFragment.this, 0, LocalMediaService.getInstance().getDuration());
        if (!LivePlayerFragment.access$500(LivePlayerFragment.this))
        {
            LivePlayerFragment.access$600(LivePlayerFragment.this);
        }
        LivePlayerFragment.access$700(LivePlayerFragment.this);
        return;
_L4:
        try
        {
            LivePlayerFragment.access$100(LivePlayerFragment.this);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            MyLogger.getLogger().d((new StringBuilder()).append("loadSoundDetail exception=").append(s.getMessage()).toString());
            return;
        }
          goto _L6
_L2:
        LivePlayerFragment.access$100(LivePlayerFragment.this);
          goto _L6
        LivePlayerFragment.access$100(LivePlayerFragment.this);
          goto _L6
    }

    ()
    {
        this$0 = LivePlayerFragment.this;
        super();
    }
}
