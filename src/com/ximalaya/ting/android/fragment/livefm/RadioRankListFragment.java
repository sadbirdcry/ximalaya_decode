// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.livefm.RankListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public class RadioRankListFragment extends BaseActivityLikeFragment
{
    private static final class FooterViewFlag extends Enum
    {

        private static final FooterViewFlag $VALUES[];
        public static final FooterViewFlag FAIL_GET_DATA;
        public static final FooterViewFlag HIDE_ALL;
        public static final FooterViewFlag LOADING;
        public static final FooterViewFlag MORE;
        public static final FooterViewFlag NO_CONNECTION;
        public static final FooterViewFlag NO_DATA;

        public static FooterViewFlag valueOf(String s)
        {
            return (FooterViewFlag)Enum.valueOf(com/ximalaya/ting/android/fragment/livefm/RadioRankListFragment$FooterViewFlag, s);
        }

        public static FooterViewFlag[] values()
        {
            return (FooterViewFlag[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterViewFlag("MORE", 0);
            LOADING = new FooterViewFlag("LOADING", 1);
            NO_CONNECTION = new FooterViewFlag("NO_CONNECTION", 2);
            HIDE_ALL = new FooterViewFlag("HIDE_ALL", 3);
            FAIL_GET_DATA = new FooterViewFlag("FAIL_GET_DATA", 4);
            NO_DATA = new FooterViewFlag("NO_DATA", 5);
            $VALUES = (new FooterViewFlag[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, FAIL_GET_DATA, NO_DATA
            });
        }

        private FooterViewFlag(String s, int i)
        {
            super(s, i);
        }
    }


    private RelativeLayout mFooterViewLoading;
    private PullToRefreshListView mListView;
    private List radios;
    private RankListAdapter rankListAdapter;

    public RadioRankListFragment()
    {
        radios = new ArrayList();
    }

    private void initFootView()
    {
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(mCon).inflate(0x7f0301fb, null);
        mListView.addFooterView(mFooterViewLoading);
        showFooterView(FooterViewFlag.HIDE_ALL);
        mFooterViewLoading.setOnClickListener(new _cls3());
        View view = new View(mActivity);
        view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ToolUtil.dp2px(mActivity, 75F)));
        mListView.addFooterView(view);
    }

    private void initViews()
    {
        setTitleText("\u7535\u53F0\u6392\u884C\u699C");
        mListView = (PullToRefreshListView)findViewById(0x7f0a0095);
        mListView.setHeaderDividersEnabled(false);
        initFootView();
        rankListAdapter = new RankListAdapter(getActivity(), radios);
        mListView.setAdapter(rankListAdapter);
        mListView.setOnRefreshListener(new _cls1());
        mListView.setOnItemClickListener(new _cls2());
        mListView.toRefreshing();
    }

    private void loadData(View view)
    {
        String s = (new StringBuilder()).append(a.O).append("getTopRadiosList").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("radioNum", 20);
        f.a().a(s, requestparams, DataCollectUtil.getDataFromView(view), new _cls4());
    }

    private void parseRadioJson(String s)
    {
        s = JSON.parseObject(s);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_135;
        }
        if (!"0000".equals(s.getString("ret")))
        {
            break MISSING_BLOCK_LABEL_135;
        }
        s = s.getString("result");
        if (TextUtils.isEmpty(s))
        {
            break MISSING_BLOCK_LABEL_149;
        }
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/livefm/RadioSound);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_92;
        }
        if (s.size() > 0)
        {
            radios.clear();
            radios.addAll(s);
            rankListAdapter.notifyDataSetChanged();
            showFooterView(FooterViewFlag.HIDE_ALL);
            return;
        }
        try
        {
            showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
            showFooterView(FooterViewFlag.NO_DATA);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.log((new StringBuilder()).append("parseJson exception=").append(s.getMessage()).toString());
        }
        return;
        showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
        showFooterView(FooterViewFlag.NO_DATA);
    }

    private void showFooterView(FooterViewFlag footerviewflag)
    {
        if (isAdded() && mListView != null && mFooterViewLoading != null)
        {
            mListView.setFooterDividersEnabled(false);
            mFooterViewLoading.setVisibility(0);
            if (footerviewflag == FooterViewFlag.MORE)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                return;
            }
            if (footerviewflag == FooterViewFlag.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerviewflag == FooterViewFlag.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerviewflag == FooterViewFlag.FAIL_GET_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerviewflag == FooterViewFlag.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u5F53\u524D\u6CA1\u6709\u6570\u636E");
                return;
            }
            if (footerviewflag == FooterViewFlag.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initViews();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300f8, null);
        return fragmentBaseContainerView;
    }









    private class _cls3
        implements android.view.View.OnClickListener
    {

        final RadioRankListFragment this$0;

        public void onClick(View view)
        {
            loadData(mFooterViewLoading);
        }

        _cls3()
        {
            this$0 = RadioRankListFragment.this;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final RadioRankListFragment this$0;

        public void onRefresh()
        {
            loadData(mListView);
        }

        _cls1()
        {
            this$0 = RadioRankListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final RadioRankListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= radios.size())
            {
                return;
            } else
            {
                adapterview = (RadioSound)radios.get(i);
                adapterview.setCategory(1);
                PlayTools.gotoPlay(32, ModelHelper.toSoundInfo(adapterview), getActivity(), true, DataCollectUtil.getDataFromView(view));
                rankListAdapter.notifyDataSetChanged();
                return;
            }
        }

        _cls2()
        {
            this$0 = RadioRankListFragment.this;
            super();
        }
    }


    private class _cls4 extends com.ximalaya.ting.android.b.a
    {

        final RadioRankListFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            mListView.onRefreshComplete();
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
            showFooterView(FooterViewFlag.NO_CONNECTION);
        }

        public void onStart()
        {
            showFooterView(FooterViewFlag.LOADING);
        }

        public void onSuccess(final String responseContent)
        {
            if (!canGoon())
            {
                return;
            }
            if (TextUtils.isEmpty(responseContent))
            {
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
                showFooterView(FooterViewFlag.NO_DATA);
                return;
            } else
            {
                class _cls1
                    implements MyCallback
                {

                    final _cls4 this$1;
                    final String val$responseContent;

                    public void execute()
                    {
                        parseRadioJson(responseContent);
                    }

                _cls1()
                {
                    this$1 = _cls4.this;
                    responseContent = s;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                return;
            }
        }

        _cls4()
        {
            this$0 = RadioRankListFragment.this;
            super();
        }
    }

}
