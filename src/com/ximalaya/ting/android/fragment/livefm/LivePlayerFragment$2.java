// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.livefm:
//            LivePlayerFragment

class this._cls0 extends a
{

    final LivePlayerFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, LivePlayerFragment.access$800(LivePlayerFragment.this));
    }

    public void onNetError(int i, String s)
    {
    }

    public void onSuccess(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        s = JSON.parseObject(s);
        if (!"0000".equals(s.getString("ret"))) goto _L4; else goto _L3
_L3:
        s = s.getString("result");
        if (TextUtils.isEmpty(s)) goto _L1; else goto _L5
_L5:
        Object obj;
        s = JSON.parseObject(s);
        obj = s.getString("yesterdaySchedules");
        if (TextUtils.isEmpty(((CharSequence) (obj)))) goto _L7; else goto _L6
_L6:
        LivePlayerFragment.access$900(LivePlayerFragment.this).clear();
        obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/livefm/RadioSound);
        if (obj == null) goto _L7; else goto _L8
_L8:
        if (((List) (obj)).size() > 0)
        {
            LivePlayerFragment.access$900(LivePlayerFragment.this).addAll(((java.util.Collection) (obj)));
        }
_L7:
        obj = s.getString("todaySchedules");
        if (TextUtils.isEmpty(((CharSequence) (obj)))) goto _L10; else goto _L9
_L9:
        LivePlayerFragment.access$1000(LivePlayerFragment.this).clear();
        obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/livefm/RadioSound);
        if (obj == null) goto _L10; else goto _L11
_L11:
        if (((List) (obj)).size() > 0)
        {
            LivePlayerFragment.access$1000(LivePlayerFragment.this).addAll(((java.util.Collection) (obj)));
        }
_L10:
        s = s.getString("tomorrowSchedules");
        if (TextUtils.isEmpty(s)) goto _L13; else goto _L12
_L12:
        LivePlayerFragment.access$1100(LivePlayerFragment.this).clear();
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/livefm/RadioSound);
        if (s == null) goto _L13; else goto _L14
_L14:
        if (s.size() > 0)
        {
            LivePlayerFragment.access$1100(LivePlayerFragment.this).addAll(s);
        }
_L13:
        LivePlayerFragment.access$1200(LivePlayerFragment.this);
        LivePlayerFragment.access$1300(LivePlayerFragment.this);
        LivePlayerFragment.access$1400(LivePlayerFragment.this);
        s = new ArrayList();
        s.addAll(LivePlayerFragment.access$900(LivePlayerFragment.this));
        s.addAll(LivePlayerFragment.access$1000(LivePlayerFragment.this));
        s.addAll(LivePlayerFragment.access$1100(LivePlayerFragment.this));
        PlayListControl.getPlayListManager().updateLivePlaylist(ModelHelper.toSoundInfolist(s));
        LivePlayerFragment.access$200(LivePlayerFragment.this);
_L16:
        PlayListControl.getPlayListManager().yesterdayRadioSounds = LivePlayerFragment.access$900(LivePlayerFragment.this);
        PlayListControl.getPlayListManager().todayRadioSounds = LivePlayerFragment.access$1000(LivePlayerFragment.this);
        PlayListControl.getPlayListManager().tomorrowSounds = LivePlayerFragment.access$1100(LivePlayerFragment.this);
        return;
        s;
        MyLogger.getLogger().d((new StringBuilder()).append("exception:").append(s.getMessage()).toString());
        return;
_L4:
        if (!"2002".equals(s.getString("ret"))) goto _L16; else goto _L15
_L15:
        LivePlayerFragment.access$1500(LivePlayerFragment.this);
          goto _L16
    }

    ()
    {
        this$0 = LivePlayerFragment.this;
        super();
    }
}
