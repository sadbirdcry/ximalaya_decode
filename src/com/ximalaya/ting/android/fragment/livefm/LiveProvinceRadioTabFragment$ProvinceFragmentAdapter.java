// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.ximalaya.ting.android.model.livefm.ProvinceModel;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.livefm:
//            LiveProvinceRadioTabFragment, LiveProvinceRadioFragment

class this._cls0 extends FragmentStatePagerAdapter
{

    final LiveProvinceRadioTabFragment this$0;

    public int getCount()
    {
        return LiveProvinceRadioTabFragment.access$1200(LiveProvinceRadioTabFragment.this).size();
    }

    public Fragment getItem(int i)
    {
        if (LiveProvinceRadioTabFragment.access$1200(LiveProvinceRadioTabFragment.this).size() == 0)
        {
            return new Fragment();
        } else
        {
            LiveProvinceRadioFragment liveprovinceradiofragment = new LiveProvinceRadioFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("province_code", ((ProvinceModel)LiveProvinceRadioTabFragment.access$1200(LiveProvinceRadioTabFragment.this).get(i)).getProvinceCode());
            liveprovinceradiofragment.setArguments(bundle);
            return liveprovinceradiofragment;
        }
    }

    public CharSequence getPageTitle(int i)
    {
        return ((ProvinceModel)LiveProvinceRadioTabFragment.access$1200(LiveProvinceRadioTabFragment.this).get(i)).getProvinceName();
    }

    public (FragmentManager fragmentmanager)
    {
        this$0 = LiveProvinceRadioTabFragment.this;
        super(fragmentmanager);
    }
}
