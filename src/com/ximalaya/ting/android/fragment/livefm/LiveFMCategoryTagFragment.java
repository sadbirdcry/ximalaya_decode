// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.livefm.RadioListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.modelmanage.BaiduLocationManager;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public class LiveFMCategoryTagFragment extends BaseActivityLikeFragment
{
    private static final class FooterViewFlag extends Enum
    {

        private static final FooterViewFlag $VALUES[];
        public static final FooterViewFlag FAIL_GET_DATA;
        public static final FooterViewFlag HIDE_ALL;
        public static final FooterViewFlag LOADING;
        public static final FooterViewFlag MORE;
        public static final FooterViewFlag NO_CONNECTION;
        public static final FooterViewFlag NO_DATA;

        public static FooterViewFlag valueOf(String s)
        {
            return (FooterViewFlag)Enum.valueOf(com/ximalaya/ting/android/fragment/livefm/LiveFMCategoryTagFragment$FooterViewFlag, s);
        }

        public static FooterViewFlag[] values()
        {
            return (FooterViewFlag[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterViewFlag("MORE", 0);
            LOADING = new FooterViewFlag("LOADING", 1);
            NO_CONNECTION = new FooterViewFlag("NO_CONNECTION", 2);
            HIDE_ALL = new FooterViewFlag("HIDE_ALL", 3);
            FAIL_GET_DATA = new FooterViewFlag("FAIL_GET_DATA", 4);
            NO_DATA = new FooterViewFlag("NO_DATA", 5);
            $VALUES = (new FooterViewFlag[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, FAIL_GET_DATA, NO_DATA
            });
        }

        private FooterViewFlag(String s, int i)
        {
            super(s, i);
        }
    }


    private static final int LOCAL_RADIO = 0;
    private static final int NATIONAL_RADIO = 1;
    private static final int NET_RADIO = 3;
    private static final int PROVINCE_RADIO = 2;
    private int categoryType;
    private int mCurrentProvinceCode;
    private RelativeLayout mFooterViewLoading;
    private boolean mIsLoading;
    private PullToRefreshListView mListView;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private int mPageId;
    private int mPageSize;
    private RadioListAdapter radioListAdapter;
    private String radioType[] = {
        "\u672C\u5730\u53F0", "\u56FD\u5BB6\u53F0", "\u7701\u5E02\u53F0", "\u7F51\u7EDC\u53F0"
    };
    private List radios;
    private int totalSize;

    public LiveFMCategoryTagFragment()
    {
        radios = new ArrayList();
        mPageId = 1;
        mPageSize = 15;
        totalSize = 0;
        mIsLoading = false;
        mCurrentProvinceCode = 0;
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            categoryType = bundle.getInt("type");
        }
        if (categoryType == 0)
        {
            int i = BaiduLocationManager.getInstance().getSavedProvinceCode();
            if (i > 0)
            {
                mCurrentProvinceCode = i;
            }
        }
    }

    private void initFootView()
    {
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(mCon).inflate(0x7f0301fb, null);
        mListView.addFooterView(mFooterViewLoading);
        showFooterView(FooterViewFlag.HIDE_ALL);
        mFooterViewLoading.setOnClickListener(new _cls5());
        View view = new View(mActivity);
        view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ToolUtil.dp2px(mActivity, 75F)));
        mListView.addFooterView(view);
    }

    private void initViews()
    {
        setTitleText(radioType[categoryType]);
        mListView = (PullToRefreshListView)findViewById(0x7f0a0095);
        mListView.setHeaderDividersEnabled(false);
        initFootView();
        radioListAdapter = new RadioListAdapter(getActivity(), radios);
        mListView.setAdapter(radioListAdapter);
        mListView.setOnRefreshListener(new _cls1());
        mListView.setOnItemClickListener(new _cls2());
        mListView.setOnScrollListener(new _cls3());
        mListView.toRefreshing();
    }

    private void loadData(View view)
    {
        if (categoryType == 0 && mCurrentProvinceCode == 0)
        {
            mListView.onRefreshComplete();
            showFooterView(FooterViewFlag.NO_DATA);
            showToast("\u65E0\u6CD5\u83B7\u53D6\u5230\u4F4D\u7F6E\u4FE1\u606F\uFF0C\u8BF7\u5728\u624B\u673A\u8BBE\u7F6E\u4E2D\u6253\u5F00\u559C\u9A6C\u62C9\u96C5FM\u5E94\u7528\u4F4D\u7F6E\u4FE1\u606F\u7684\u8BFB\u53D6\u6743\u9650");
        } else
        if (!mIsLoading)
        {
            mIsLoading = true;
            String s = (new StringBuilder()).append(a.O).append("getRadiosListByType").toString();
            RequestParams requestparams = new RequestParams();
            if (categoryType == 0)
            {
                requestparams.put("radioType", 2);
            } else
            {
                requestparams.put("radioType", categoryType);
            }
            if (categoryType == 2 || categoryType == 0)
            {
                requestparams.put("provinceCode", mCurrentProvinceCode);
            }
            requestparams.put("pageSize", mPageSize);
            requestparams.put("pageNum", mPageId);
            f.a().a(s, requestparams, DataCollectUtil.getDataFromView(view), new _cls6());
            return;
        }
    }

    private void parseRadioJson(String s)
    {
        String s1;
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.log((new StringBuilder()).append("parseJson exception=").append(s.getMessage()).toString());
            return;
        }
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_176;
        }
        if (!"0000".equals(s.getString("ret")))
        {
            break MISSING_BLOCK_LABEL_176;
        }
        s1 = s.getString("result");
        totalSize = s.getIntValue("total");
        if (TextUtils.isEmpty(s1))
        {
            break MISSING_BLOCK_LABEL_190;
        }
        s = JSON.parseArray(s1, com/ximalaya/ting/android/model/livefm/RadioSound);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_125;
        }
        if (s.size() > 0)
        {
            if (mPageId == 1)
            {
                radios.clear();
            }
            radios.addAll(s);
            mPageId = mPageId + 1;
            radioListAdapter.notifyDataSetChanged();
            showFooterView(FooterViewFlag.HIDE_ALL);
            return;
        }
        if (mPageId == 1)
        {
            showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
            showFooterView(FooterViewFlag.NO_DATA);
            return;
        }
        break MISSING_BLOCK_LABEL_190;
        showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
        showFooterView(FooterViewFlag.NO_DATA);
    }

    private void registerListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            mOnPlayerStatusUpdateListener = new _cls4();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void showFooterView(FooterViewFlag footerviewflag)
    {
        if (isAdded() && mListView != null && mFooterViewLoading != null)
        {
            mListView.setFooterDividersEnabled(false);
            mFooterViewLoading.setVisibility(0);
            if (footerviewflag == FooterViewFlag.MORE)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                return;
            }
            if (footerviewflag == FooterViewFlag.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerviewflag == FooterViewFlag.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerviewflag == FooterViewFlag.FAIL_GET_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerviewflag == FooterViewFlag.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u5F53\u524D\u6CA1\u6709\u6570\u636E");
                return;
            }
            if (footerviewflag == FooterViewFlag.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initData();
        initViews();
        registerListener();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300f4, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        unRegisterListener();
    }


/*
    static int access$002(LiveFMCategoryTagFragment livefmcategorytagfragment, int i)
    {
        livefmcategorytagfragment.mPageId = i;
        return i;
    }

*/









/*
    static boolean access$602(LiveFMCategoryTagFragment livefmcategorytagfragment, boolean flag)
    {
        livefmcategorytagfragment.mIsLoading = flag;
        return flag;
    }

*/




    private class _cls5
        implements android.view.View.OnClickListener
    {

        final LiveFMCategoryTagFragment this$0;

        public void onClick(View view)
        {
            loadData(mFooterViewLoading);
        }

        _cls5()
        {
            this$0 = LiveFMCategoryTagFragment.this;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final LiveFMCategoryTagFragment this$0;

        public void onRefresh()
        {
            mPageId = 1;
            loadData(mListView);
        }

        _cls1()
        {
            this$0 = LiveFMCategoryTagFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final LiveFMCategoryTagFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= radios.size())
            {
                return;
            } else
            {
                adapterview = (RadioSound)radios.get(i);
                adapterview.setCategory(1);
                PlayTools.gotoPlay(32, ModelHelper.toSoundInfo(adapterview), getActivity(), true, DataCollectUtil.getDataFromView(view));
                radioListAdapter.notifyDataSetChanged();
                return;
            }
        }

        _cls2()
        {
            this$0 = LiveFMCategoryTagFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AbsListView.OnScrollListener
    {

        final LiveFMCategoryTagFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            mListView.onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (radios == null || abslistview.getLastVisiblePosition() <= i || radios.size() >= totalSize)
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        showFooterView(FooterViewFlag.LOADING);
                        loadData(mListView);
                    }
                }
                return;
            }
            showFooterView(FooterViewFlag.HIDE_ALL);
        }

        _cls3()
        {
            this$0 = LiveFMCategoryTagFragment.this;
            super();
        }
    }


    private class _cls6 extends com.ximalaya.ting.android.b.a
    {

        final LiveFMCategoryTagFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            mIsLoading = false;
            mListView.onRefreshComplete();
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
            showFooterView(FooterViewFlag.NO_CONNECTION);
        }

        public void onStart()
        {
            mIsLoading = true;
            showFooterView(FooterViewFlag.LOADING);
        }

        public void onSuccess(final String responseContent)
        {
            if (!canGoon())
            {
                return;
            }
            if (TextUtils.isEmpty(responseContent))
            {
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
                showFooterView(FooterViewFlag.NO_DATA);
                return;
            } else
            {
                class _cls1
                    implements MyCallback
                {

                    final _cls6 this$1;
                    final String val$responseContent;

                    public void execute()
                    {
                        parseRadioJson(responseContent);
                    }

                _cls1()
                {
                    this$1 = _cls6.this;
                    responseContent = s;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                return;
            }
        }

        _cls6()
        {
            this$0 = LiveFMCategoryTagFragment.this;
            super();
        }
    }


    private class _cls4 extends OnPlayerStatusUpdateListenerProxy
    {

        final LiveFMCategoryTagFragment this$0;

        public void onPlayStateChange()
        {
            if (isAdded())
            {
                radioListAdapter.notifyDataSetChanged();
            }
        }

        _cls4()
        {
            this$0 = LiveFMCategoryTagFragment.this;
            super();
        }
    }

}
