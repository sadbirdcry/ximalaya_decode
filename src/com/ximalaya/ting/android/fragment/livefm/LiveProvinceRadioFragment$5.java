// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.livefm;

import android.text.TextUtils;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.livefm:
//            LiveProvinceRadioFragment

class this._cls0 extends a
{

    final LiveProvinceRadioFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
    }

    public void onFinish()
    {
        LiveProvinceRadioFragment.access$602(LiveProvinceRadioFragment.this, false);
        LiveProvinceRadioFragment.access$100(LiveProvinceRadioFragment.this).onRefreshComplete();
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
        LiveProvinceRadioFragment.access$700(LiveProvinceRadioFragment.this, oterViewFlag.NO_CONNECTION);
    }

    public void onStart()
    {
        LiveProvinceRadioFragment.access$602(LiveProvinceRadioFragment.this, true);
        LiveProvinceRadioFragment.access$700(LiveProvinceRadioFragment.this, oterViewFlag.LOADING);
    }

    public void onSuccess(String s)
    {
        if (!canGoon())
        {
            return;
        }
        if (TextUtils.isEmpty(s))
        {
            showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
            LiveProvinceRadioFragment.access$700(LiveProvinceRadioFragment.this, oterViewFlag.NO_DATA);
            return;
        } else
        {
            LiveProvinceRadioFragment.access$900(LiveProvinceRadioFragment.this, s);
            return;
        }
    }

    oterViewFlag()
    {
        this$0 = LiveProvinceRadioFragment.this;
        super();
    }
}
