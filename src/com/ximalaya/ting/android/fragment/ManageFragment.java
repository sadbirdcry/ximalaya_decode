// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseFragment, BaseActivityLikeFragment

public class ManageFragment extends BaseFragment
{
    public static interface OnFragmentStackChangeListener
    {

        public abstract void onFragmentFinish(List list);

        public abstract void onNewFragmentStart(List list);
    }


    private OnFragmentStackChangeListener mStackChangeListener;
    public List mStacks;

    public ManageFragment()
    {
        mStacks = new ArrayList();
    }

    private Fragment getBelowFragment()
    {
        if (mStacks.size() > 1)
        {
            return (Fragment)((SoftReference)mStacks.get(mStacks.size() - 2)).get();
        } else
        {
            return null;
        }
    }

    private void pushFragment(Fragment fragment, int i, int j)
    {
        Fragment fragment1;
        FragmentTransaction fragmenttransaction;
        removeFragmentFromStacks(fragment, true);
        fragment1 = null;
        if (mStacks.size() > 0)
        {
            Fragment fragment2 = (Fragment)((SoftReference)mStacks.get(mStacks.size() - 1)).get();
            fragment1 = fragment2;
            if (fragment2 != null)
            {
                fragment2.onPause();
                fragment1 = fragment2;
            }
        }
        mStacks.add(new SoftReference(fragment));
        fragmenttransaction = getChildFragmentManager().beginTransaction();
        if (i != 0 && j != 0) goto _L2; else goto _L1
_L1:
        fragmenttransaction.setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020);
_L4:
        fragmenttransaction.add(0x7f0a0434, fragment);
        fragmenttransaction.commitAllowingStateLoss();
        if (fragment1 != null && (fragment1 instanceof BaseFragment))
        {
            ((BaseFragment)fragment1).releaseViewTreeImageView();
        }
        return;
_L2:
        if (i != -1 && j != -1)
        {
            fragmenttransaction.setCustomAnimations(i, j, i, j);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void pushFragments(Fragment fragment)
    {
        pushFragment(fragment, 0, 0);
    }

    private void removeFragment(Fragment fragment, boolean flag)
    {
        int i = 0;
        if (fragment != null)
        {
            FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
            if (flag)
            {
                Bundle bundle = fragment.getArguments();
                int j;
                if (bundle != null)
                {
                    j = bundle.getInt("in_anim");
                    i = bundle.getInt("out_anim");
                } else
                {
                    j = 0;
                }
                if (j != 0 && i != 0)
                {
                    fragmenttransaction.setCustomAnimations(j, i, j, i);
                } else
                {
                    fragmenttransaction.setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020);
                }
            }
            fragmenttransaction.remove(fragment);
            fragmenttransaction.commitAllowingStateLoss();
        }
    }

    public void clearAllFragmentFromStacks()
    {
        if (mStacks.size() > 0)
        {
            FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
            Iterator iterator = mStacks.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                Fragment fragment = (Fragment)((SoftReference)iterator.next()).get();
                if (fragment != null)
                {
                    fragmenttransaction.remove(fragment);
                }
            } while (true);
            fragmenttransaction.commitAllowingStateLoss();
            mStacks.clear();
            if (ToolUtil.isUseSmartbarAsTab())
            {
                getActivity().invalidateOptionsMenu();
            }
        }
    }

    public Fragment getCurrentFragment()
    {
        if (mStacks.size() > 0)
        {
            return (Fragment)((SoftReference)mStacks.get(mStacks.size() - 1)).get();
        } else
        {
            return null;
        }
    }

    public int getFragmentCount()
    {
        if (mStacks == null)
        {
            return 0;
        } else
        {
            return mStacks.size();
        }
    }

    public void init()
    {
        if (mStacks == null)
        {
            mStacks = new ArrayList();
        }
    }

    public boolean onBackPressed()
    {
        if (mStacks.size() > 0)
        {
            if (!((BaseActivityLikeFragment)getCurrentFragment()).onBackPressed())
            {
                removeTopFragment();
                if (a.f && mStackChangeListener != null)
                {
                    mStackChangeListener.onFragmentFinish(mStacks);
                }
            }
            return true;
        } else
        {
            return false;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        init();
        mCon = getActivity().getApplicationContext();
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300f9, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        mStackChangeListener = null;
        super.onDestroyView();
    }

    public void removeFragmentFromStacks(Fragment fragment, boolean flag)
    {
        if (mStacks.size() <= 0) goto _L2; else goto _L1
_L1:
        if (fragment != getCurrentFragment()) goto _L4; else goto _L3
_L3:
        onBackPressed();
_L2:
        return;
_L4:
        Iterator iterator = mStacks.iterator();
_L7:
        Fragment fragment1;
        while (iterator.hasNext()) 
        {
label0:
            {
                fragment1 = (Fragment)((SoftReference)iterator.next()).get();
                if (fragment1 != null)
                {
                    break label0;
                }
                iterator.remove();
            }
        }
        if (true) goto _L2; else goto _L5
_L5:
        if (!flag)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!fragment1.getClass().equals(fragment.getClass())) goto _L7; else goto _L6
_L6:
        iterator.remove();
        removeFragment(fragment1, false);
        return;
        if (fragment1 != fragment) goto _L7; else goto _L8
_L8:
        iterator.remove();
        removeFragment(fragment1, true);
        return;
    }

    public void removeTopFragment()
    {
        Fragment fragment = getBelowFragment();
        if (fragment != null && (fragment instanceof BaseFragment))
        {
            ((BaseFragment)fragment).restoreViewTreeImageView();
        }
        fragment = getCurrentFragment();
        if (fragment != null)
        {
            mStacks.remove(mStacks.size() - 1);
            removeFragment(fragment, true);
            fragment = getCurrentFragment();
            if (fragment != null)
            {
                fragment.onResume();
            } else
            if (getActivity() instanceof MainTabActivity2)
            {
                ((MainTabActivity2)getActivity()).onResume();
                return;
            }
        }
    }

    public void setOnFragmentStackChangeListener(OnFragmentStackChangeListener onfragmentstackchangelistener)
    {
        mStackChangeListener = onfragmentstackchangelistener;
    }

    public void startFragment(Fragment fragment)
    {
        pushFragments(fragment);
        if (a.f && mStackChangeListener != null)
        {
            mStackChangeListener.onNewFragmentStart(mStacks);
        }
    }

    public void startFragment(Fragment fragment, int i, int j)
    {
        pushFragment(fragment, i, j);
        if (a.f && mStackChangeListener != null)
        {
            mStackChangeListener.onNewFragmentStart(mStacks);
        }
    }

    public void startFragment(Class class1, Bundle bundle)
    {
        try
        {
            class1 = ((Class) (class1.newInstance()));
        }
        // Misplaced declaration of an exception variable
        catch (Class class1)
        {
            return;
        }
        if (class1 == null) goto _L2; else goto _L1
_L1:
        if (!(class1 instanceof Fragment)) goto _L2; else goto _L3
_L3:
        class1 = (Fragment)class1;
_L5:
        if (class1 == null || bundle == null)
        {
            break MISSING_BLOCK_LABEL_34;
        }
        class1.setArguments(bundle);
        if (class1 == null)
        {
            break MISSING_BLOCK_LABEL_44;
        }
        startFragment(((Fragment) (class1)));
        return;
        Logger.log("fragment\u751F\u6210\u5931\u8D25\uFF01\uFF01\uFF01");
        return;
_L2:
        class1 = null;
        if (true) goto _L5; else goto _L4
_L4:
    }
}
