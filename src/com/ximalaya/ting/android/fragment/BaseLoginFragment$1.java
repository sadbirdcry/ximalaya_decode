// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import com.ximalaya.ting.android.model.auth.AuthInfo;
import com.ximalaya.ting.android.util.Logger;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseLoginFragment

class this._cls0
    implements IUiListener
{

    final BaseLoginFragment this$0;

    public void onCancel()
    {
    }

    public void onComplete(Object obj)
    {
        obj = (JSONObject)obj;
        if (obj == null)
        {
            showToast("\u83B7\u53D6QQ\u4FE1\u606F\u5931\u8D25\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01");
            return;
        }
        AuthInfo authinfo;
        authinfo = new AuthInfo();
        if (!((JSONObject) (obj)).has("ret"))
        {
            break MISSING_BLOCK_LABEL_141;
        }
        if (((JSONObject) (obj)).getInt("ret") == 0)
        {
            authinfo.access_token = ((JSONObject) (obj)).getString("access_token");
            authinfo.expires_in = ((JSONObject) (obj)).getString("expires_in");
            authinfo.openid = ((JSONObject) (obj)).getString("openid");
            (new ginTask(mActivity, getArguments())).myexec(new Object[] {
                Integer.valueOf(2), authinfo
            });
            return;
        }
        try
        {
            showToast(((JSONObject) (obj)).getString("msg"));
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            showToast("\u89E3\u6790QQ\u4FE1\u606F\u5931\u8D25\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01");
            return;
        }
        authinfo.access_token = ((JSONObject) (obj)).getString("access_token");
        authinfo.expires_in = ((JSONObject) (obj)).getString("expires_in");
        authinfo.openid = ((JSONObject) (obj)).getString("openid");
        (new ginTask(mActivity, getArguments())).myexec(new Object[] {
            Integer.valueOf(2), authinfo
        });
        return;
    }

    public void onError(UiError uierror)
    {
        Logger.e(BaseLoginFragment.access$000(), uierror.errorMessage);
    }

    ginTask()
    {
        this$0 = BaseLoginFragment.this;
        super();
    }
}
