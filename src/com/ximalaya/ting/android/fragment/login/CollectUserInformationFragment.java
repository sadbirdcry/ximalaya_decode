// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.login;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.Scroller;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.login.ActivityCallbackDelegate;
import com.ximalaya.ting.android.activity.login.CallbackDelegateable;
import com.ximalaya.ting.android.animation.FixedSpeedScroller;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.FragmentCallback;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.util.ViewUtil;
import com.ximalaya.ting.android.view.EntireRowCenterLayout;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CollectUserInformationFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, ActivityCallbackDelegate
{
    private static final class AboutMeInfo
    {

        public String mGender;
        public ArrayList mInterestedTags;
        public String mType;

        private AboutMeInfo()
        {
            mInterestedTags = new ArrayList();
        }

        AboutMeInfo(_cls1 _pcls1)
        {
            this();
        }
    }

    static interface ICallback
    {

        public abstract void onGenderSelected(String s);

        public abstract void onInterestedTagSelected(String s);

        public abstract void onInterestedTagUnselected(String s);

        public abstract void onSubmitInfo();

        public abstract void onTypeSelected(String s);
    }

    static class Tag
    {

        private String title;

        public String getTitle()
        {
            return title;
        }

        public void setTitle(String s)
        {
            title = s;
        }

        Tag()
        {
        }
    }

    private static class TagLoader extends AsyncTaskLoader
    {

        private List mData;
        private AboutMeInfo mInfo;

        public volatile void deliverResult(Object obj)
        {
            deliverResult((List)obj);
        }

        public void deliverResult(List list)
        {
            super.deliverResult(list);
            mData = list;
        }

        public volatile Object loadInBackground()
        {
            return loadInBackground();
        }

        public List loadInBackground()
        {
            Object obj = new RequestParams();
            ((RequestParams) (obj)).put("category", mInfo.mGender);
            ((RequestParams) (obj)).put("subcategory", mInfo.mType);
            obj = f.a().a((new StringBuilder()).append(a.u).append("csrv/coldboot/v1/tags").toString(), ((RequestParams) (obj)), false);
            if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
            {
                try
                {
                    obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
                    if (((JSONObject) (obj)).getIntValue("ret") == 0)
                    {
                        obj = ((JSONObject) (obj)).getString("list");
                        if (!TextUtils.isEmpty(((CharSequence) (obj))))
                        {
                            mData = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/fragment/login/CollectUserInformationFragment$Tag);
                        }
                    }
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
            }
            if (mData == null || mData.size() <= 0)
            {
                String s = FileUtils.readAssetFileData(getContext(), "coldboot/coldboot_tags.json");
                if (!TextUtils.isEmpty(s))
                {
                    try
                    {
                        mData = JSONObject.parseArray(s, com/ximalaya/ting/android/fragment/login/CollectUserInformationFragment$Tag);
                    }
                    catch (Exception exception1) { }
                }
            }
            return mData;
        }

        protected void onStartLoading()
        {
            super.onStartLoading();
            if (mData == null)
            {
                forceLoad();
                return;
            } else
            {
                deliverResult(mData);
                return;
            }
        }

        public TagLoader(Context context, AboutMeInfo aboutmeinfo)
        {
            super(context);
            mInfo = aboutmeinfo;
        }
    }

    private static class TheViewPagerAdpater extends PagerAdapter
    {

        private static final int COUNT_PAGE = 3;
        private ICallback mCallback;
        private EntireRowCenterLayout mCategoryContainer;
        private Context mContext;
        private boolean mEnableSubmit;
        private View mGenderPage;
        private LayoutInflater mInflater;
        private List mInterestedTags;
        private View mPrimaryItem;
        private Button mSubmitBtn;
        private View mTagPage;
        private View mTypePage;
        private ArrayList mViews;

        public void addGenderPage()
        {
            if (mGenderPage == null)
            {
                mGenderPage = View.inflate(mContext, 0x7f03005c, null);
                ((TextView)mGenderPage.findViewById(0x7f0a01c7)).setText(Html.fromHtml("\u8BA9\u5C0F\u559C<font color=\"#fc5832\">\u66F4\u61C2\u4F60</font>"));
                final CheckBox male = (CheckBox)mGenderPage.findViewById(0x7f0a01ca);
                final CheckBox female = (CheckBox)mGenderPage.findViewById(0x7f0a01cb);
                class _cls1
                    implements android.view.View.OnClickListener
                {

                    final TheViewPagerAdpater this$0;
                    final CheckBox val$female;
                    final CheckBox val$male;

                    public void onClick(View view)
                    {
                        male.setChecked(true);
                        female.setChecked(false);
                        if (mCallback != null)
                        {
                            mCallback.onGenderSelected("\u5E05\u54E5");
                        }
                    }

                _cls1()
                {
                    this$0 = TheViewPagerAdpater.this;
                    male = checkbox;
                    female = checkbox1;
                    super();
                }
                }

                male.setOnClickListener(new _cls1());
                class _cls2
                    implements android.view.View.OnClickListener
                {

                    final TheViewPagerAdpater this$0;
                    final CheckBox val$female;
                    final CheckBox val$male;

                    public void onClick(View view)
                    {
                        male.setChecked(false);
                        female.setChecked(true);
                        if (mCallback != null)
                        {
                            mCallback.onGenderSelected("\u7F8E\u5973");
                        }
                    }

                _cls2()
                {
                    this$0 = TheViewPagerAdpater.this;
                    male = checkbox;
                    female = checkbox1;
                    super();
                }
                }

                female.setOnClickListener(new _cls2());
                mViews.add(mGenderPage);
                notifyDataSetChanged();
            }
        }

        public void addTagPage()
        {
            if (mTagPage == null)
            {
                mTagPage = View.inflate(mContext, 0x7f03005e, null);
                ((TextView)mTagPage.findViewById(0x7f0a01c6)).setText(Html.fromHtml("\u5C0F\u559C<font color=\"#fc5832\">\u6B63\u5BF9\u4F60\u7684\u53E3\u5473</font>"));
                mCategoryContainer = (EntireRowCenterLayout)mTagPage.findViewById(0x7f0a01d5);
                mCategoryContainer.setHorizontalSpace(Utilities.dip2px(mContext, 20F));
                mCategoryContainer.setVerticalSpace(Utilities.dip2px(mContext, 10F));
                mCategoryContainer.setColumns(2);
                setInterestedTags(mInterestedTags);
                mSubmitBtn = (Button)mTagPage.findViewById(0x7f0a01d4);
                class _cls7
                    implements android.view.View.OnClickListener
                {

                    final TheViewPagerAdpater this$0;

                    public void onClick(View view)
                    {
                        if (mEnableSubmit)
                        {
                            if (mCallback != null)
                            {
                                mCallback.onSubmitInfo();
                            }
                            return;
                        } else
                        {
                            CustomToast.showToast(mContext, "\u8BF7\u81F3\u5C11\u9009\u62E9\u4E00\u4E2A\u5206\u7C7B\u54E6~", 1);
                            return;
                        }
                    }

                _cls7()
                {
                    this$0 = TheViewPagerAdpater.this;
                    super();
                }
                }

                mSubmitBtn.setOnClickListener(new _cls7());
                mViews.add(mTagPage);
                notifyDataSetChanged();
            }
        }

        public void addTypePage()
        {
            if (mTypePage == null)
            {
                mTypePage = View.inflate(mContext, 0x7f03005d, null);
                ((TextView)mTypePage.findViewById(0x7f0a01c6)).setText(Html.fromHtml("\u8BA9\u5C0F\u559C<font color=\"#fc5832\">\u66F4\u8D34\u8FD1\u4F60</font>"));
                final CheckBox officer = (CheckBox)mTypePage.findViewById(0x7f0a01cd);
                final CheckBox retired = (CheckBox)mTypePage.findViewById(0x7f0a01cf);
                final CheckBox student = (CheckBox)mTypePage.findViewById(0x7f0a01d1);
                final CheckBox children = (CheckBox)mTypePage.findViewById(0x7f0a01d3);
                class _cls3
                    implements android.view.View.OnClickListener
                {

                    final TheViewPagerAdpater this$0;
                    final CheckBox val$children;
                    final CheckBox val$officer;
                    final CheckBox val$retired;
                    final CheckBox val$student;

                    public void onClick(View view)
                    {
                        officer.setChecked(true);
                        retired.setChecked(false);
                        student.setChecked(false);
                        children.setChecked(false);
                        if (mCallback != null)
                        {
                            mCallback.onTypeSelected("\u4E0A\u73ED\u65CF");
                        }
                    }

                _cls3()
                {
                    this$0 = TheViewPagerAdpater.this;
                    officer = checkbox;
                    retired = checkbox1;
                    student = checkbox2;
                    children = checkbox3;
                    super();
                }
                }

                officer.setOnClickListener(new _cls3());
                class _cls4
                    implements android.view.View.OnClickListener
                {

                    final TheViewPagerAdpater this$0;
                    final CheckBox val$children;
                    final CheckBox val$officer;
                    final CheckBox val$retired;
                    final CheckBox val$student;

                    public void onClick(View view)
                    {
                        officer.setChecked(false);
                        retired.setChecked(true);
                        student.setChecked(false);
                        children.setChecked(false);
                        if (mCallback != null)
                        {
                            mCallback.onTypeSelected("\u9000\u4F11\u5566");
                        }
                    }

                _cls4()
                {
                    this$0 = TheViewPagerAdpater.this;
                    officer = checkbox;
                    retired = checkbox1;
                    student = checkbox2;
                    children = checkbox3;
                    super();
                }
                }

                retired.setOnClickListener(new _cls4());
                class _cls5
                    implements android.view.View.OnClickListener
                {

                    final TheViewPagerAdpater this$0;
                    final CheckBox val$children;
                    final CheckBox val$officer;
                    final CheckBox val$retired;
                    final CheckBox val$student;

                    public void onClick(View view)
                    {
                        officer.setChecked(false);
                        retired.setChecked(false);
                        student.setChecked(true);
                        children.setChecked(false);
                        if (mCallback != null)
                        {
                            mCallback.onTypeSelected("\u5B66\u751F\u515A");
                        }
                    }

                _cls5()
                {
                    this$0 = TheViewPagerAdpater.this;
                    officer = checkbox;
                    retired = checkbox1;
                    student = checkbox2;
                    children = checkbox3;
                    super();
                }
                }

                student.setOnClickListener(new _cls5());
                class _cls6
                    implements android.view.View.OnClickListener
                {

                    final TheViewPagerAdpater this$0;
                    final CheckBox val$children;
                    final CheckBox val$officer;
                    final CheckBox val$retired;
                    final CheckBox val$student;

                    public void onClick(View view)
                    {
                        officer.setChecked(false);
                        retired.setChecked(false);
                        student.setChecked(false);
                        children.setChecked(true);
                        if (mCallback != null)
                        {
                            mCallback.onTypeSelected("\u5C0F\u670B\u53CB");
                        }
                    }

                _cls6()
                {
                    this$0 = TheViewPagerAdpater.this;
                    officer = checkbox;
                    retired = checkbox1;
                    student = checkbox2;
                    children = checkbox3;
                    super();
                }
                }

                children.setOnClickListener(new _cls6());
                mViews.add(mTypePage);
                notifyDataSetChanged();
            }
        }

        public void destroyItem(ViewGroup viewgroup, int i, Object obj)
        {
            viewgroup.removeView((View)obj);
        }

        public int getCount()
        {
            return mViews.size();
        }

        public int getItemPosition(Object obj)
        {
            return obj != mPrimaryItem ? -2 : -1;
        }

        public Object instantiateItem(ViewGroup viewgroup, int i)
        {
            View view2;
            switch (i)
            {
            default:
                return null;

            case 0: // '\0'
                View view = (View)mViews.get(i);
                viewgroup.addView(view);
                return view;

            case 1: // '\001'
                View view1 = (View)mViews.get(i);
                viewgroup.addView(view1);
                return view1;

            case 2: // '\002'
                view2 = (View)mViews.get(i);
                break;
            }
            viewgroup.addView(view2);
            return view2;
        }

        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }

        public void setEnableSubmit(boolean flag)
        {
label0:
            {
                mEnableSubmit = flag;
                if (mSubmitBtn != null)
                {
                    if (!flag)
                    {
                        break label0;
                    }
                    mSubmitBtn.setBackgroundResource(0x7f020002);
                }
                return;
            }
            mSubmitBtn.setBackgroundResource(0x7f020003);
        }

        public void setICallback(ICallback icallback)
        {
            mCallback = icallback;
        }

        public void setInterestedTags(List list)
        {
            mInterestedTags = list;
            if (mCategoryContainer != null)
            {
                mCategoryContainer.removeAllViews();
                if (mInterestedTags != null)
                {
                    final CheckedTextView tv;
                    for (list = mInterestedTags.iterator(); list.hasNext(); mCategoryContainer.addView(tv))
                    {
                        final Tag tag = (Tag)list.next();
                        tv = (CheckedTextView)mInflater.inflate(0x7f03010a, mCategoryContainer, false);
                        class _cls8
                            implements android.view.View.OnClickListener
                        {

                            final TheViewPagerAdpater this$0;
                            final Tag val$tag;
                            final CheckedTextView val$tv;

                            public void onClick(View view)
                            {
                                tv.toggle();
                                if (tv.isChecked())
                                {
                                    if (mCallback != null)
                                    {
                                        mCallback.onInterestedTagSelected(tag.getTitle());
                                    }
                                } else
                                if (mCallback != null)
                                {
                                    mCallback.onInterestedTagUnselected(tag.getTitle());
                                    return;
                                }
                            }

                _cls8()
                {
                    this$0 = TheViewPagerAdpater.this;
                    tv = checkedtextview;
                    tag = tag1;
                    super();
                }
                        }

                        tv.setOnClickListener(new _cls8());
                        tv.setText(tag.getTitle());
                    }

                }
            }
        }

        public void setPrimaryItem(ViewGroup viewgroup, int i, Object obj)
        {
            super.setPrimaryItem(viewgroup, i, obj);
            mPrimaryItem = (View)obj;
        }




        public TheViewPagerAdpater(Context context)
        {
            mContext = context;
            mInflater = LayoutInflater.from(mContext);
            mViews = new ArrayList(3);
            addGenderPage();
        }
    }


    public static final int SUBMIT_ALREADY = 1;
    public static final int SUBMIT_GET_FAIL = 2;
    public static final int SUBMIT_NO = 3;
    private static final Interpolator sInterpolator = new _cls1();
    private AboutMeInfo mAboutMeInfo;
    private TheViewPagerAdpater mAdapter;
    private FixedSpeedScroller mAutomaicScroller;
    private boolean mClickExit;
    private Scroller mCurrScroller;
    private Scroller mDefaultScroller;
    private boolean mInfoDataChanged;
    private Loader mLoader;
    private ViewPager mViewPager;

    public CollectUserInformationFragment()
    {
    }

    public static int askIsSubmitedFromRemote(Context context)
    {
        String s = (new StringBuilder()).append(a.u).append("csrv/coldboot/v1/posted").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("deviceId", ((MyApplication)context.getApplicationContext()).i());
        context = f.a().a(s, requestparams, null);
        if (((com.ximalaya.ting.android.b.n.a) (context)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (context)).a))
        {
            break MISSING_BLOCK_LABEL_111;
        }
        boolean flag;
        context = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (context)).a);
        if (context.getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_111;
        }
        flag = context.getBooleanValue("posted");
        return !flag ? 3 : 1;
        context;
        context.printStackTrace();
        return 2;
    }

    private boolean canGetTags()
    {
        return !TextUtils.isEmpty(mAboutMeInfo.mGender) && !TextUtils.isEmpty(mAboutMeInfo.mType);
    }

    private boolean canSubmit()
    {
        return !TextUtils.isEmpty(mAboutMeInfo.mGender) && !TextUtils.isEmpty(mAboutMeInfo.mType) && mAboutMeInfo.mInterestedTags.size() > 0;
    }

    private void gotoNextPage()
    {
        ViewUtil.setViewPagerScroller(mViewPager, mAutomaicScroller);
        mCurrScroller = mAutomaicScroller;
        int i = mViewPager.getCurrentItem();
        if (i + 1 < mViewPager.getAdapter().getCount())
        {
            mViewPager.setCurrentItem(i + 1, true);
        }
    }

    private boolean gotoPrevious()
    {
        int i = mViewPager.getCurrentItem();
        if (i - 1 >= 0)
        {
            mViewPager.setCurrentItem(i - 1, true);
            return true;
        } else
        {
            return false;
        }
    }

    private static boolean hasLocalData(Context context)
    {
        return !TextUtils.isEmpty(SharedPreferencesUtil.getInstance(context).getString("coldboot_user_data"));
    }

    public static void ignore(Context context)
    {
        int i = SharedPreferencesUtil.getInstance(context).getInt("coldboot_ignore_times", 0);
        SharedPreferencesUtil.getInstance(context).saveInt("coldboot_ignore_times", i + 1);
        SharedPreferencesUtil.getInstance(context).saveLong("coldboot_ignore_when", System.currentTimeMillis());
    }

    public static boolean isSubmited(Context context)
    {
        return SharedPreferencesUtil.getInstance(context).getBoolean("submited_about_me_info", false);
    }

    private void loadTags()
    {
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(0, null, this);
            return;
        } else
        {
            mLoader = getLoaderManager().restartLoader(0, null, this);
            return;
        }
    }

    public static CollectUserInformationFragment newInstance()
    {
        return new CollectUserInformationFragment();
    }

    private static void postData(final Context context, final AboutMeInfo info)
    {
        String s = (new StringBuilder()).append(a.u).append("csrv/coldboot/v1/lovedTags").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("category", info.mGender);
        requestparams.put("subcategory", info.mType);
        requestparams.put("deviceId", ((MyApplication)(MyApplication)context.getApplicationContext()).i());
        StringBuffer stringbuffer = new StringBuffer();
        for (Iterator iterator = info.mInterestedTags.iterator(); iterator.hasNext(); stringbuffer.append(","))
        {
            stringbuffer.append((String)iterator.next());
        }

        stringbuffer.deleteCharAt(stringbuffer.length() - 1);
        requestparams.put("tags", stringbuffer.toString());
        context = new _cls4();
        f.a().b(s, requestparams, null, context);
    }

    public static void postDataBackground(Context context)
    {
        String s = SharedPreferencesUtil.getInstance(context).getString("coldboot_user_data");
        if (!TextUtils.isEmpty(s))
        {
            postData(context, (AboutMeInfo)JSON.parseObject(s, com/ximalaya/ting/android/fragment/login/CollectUserInformationFragment$AboutMeInfo));
        }
    }

    public static void removeFromLocal(Context context)
    {
        SharedPreferencesUtil.getInstance(context).removeByKey("coldboot_user_data");
    }

    private static void saveToLocal(Context context, AboutMeInfo aboutmeinfo)
    {
        aboutmeinfo = JSON.toJSONString(aboutmeinfo);
        SharedPreferencesUtil.getInstance(context).saveString("coldboot_user_data", aboutmeinfo);
    }

    public static void setIsSubmited(Context context)
    {
        SharedPreferencesUtil.getInstance(context).saveBoolean("submited_about_me_info", true);
    }

    public static boolean shouldShow(Context context)
    {
        int i;
        boolean flag;
        boolean flag1;
        flag = true;
        flag1 = false;
        if (isSubmited(context.getApplicationContext()))
        {
            break MISSING_BLOCK_LABEL_129;
        }
        i = SharedPreferencesUtil.getInstance(context).getInt("coldboot_ignore_times", 0);
        if (i <= 0 || i > 2) goto _L2; else goto _L1
_L1:
        long l = SharedPreferencesUtil.getInstance(context).getLong("coldboot_ignore_when");
        if (System.currentTimeMillis() - l <= (new long[] {
    0x240c8400L, 0xffffffff9a7ec800L
})[i - 1])
        {
            flag = false;
        }
_L4:
        return flag;
_L2:
        if (i > 2)
        {
            setIsSubmited(context);
            return false;
        }
        i = askIsSubmitedFromRemote(context.getApplicationContext());
        if (i == 1)
        {
            setIsSubmited(context.getApplicationContext());
            return false;
        }
        flag = flag1;
        if (i != 3) goto _L4; else goto _L3
_L3:
        return true;
        return hasLocalData(context);
    }

    private void submit()
    {
        if (canSubmit())
        {
            if (!NetworkUtils.isNetworkAvaliable(getActivity().getApplicationContext()))
            {
                saveToLocal(getActivity().getApplicationContext(), mAboutMeInfo);
            } else
            {
                postData(getActivity().getApplicationContext(), mAboutMeInfo);
            }
            if (mCallback != null)
            {
                mCallback.onFinish();
            }
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mAdapter = new TheViewPagerAdpater(getActivity());
        mViewPager.setAdapter(mAdapter);
        mAboutMeInfo = new AboutMeInfo(null);
        mAdapter.setICallback(new _cls2());
        mViewPager.setOnPageChangeListener(new _cls3());
    }

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (activity instanceof CallbackDelegateable)
        {
            ((CallbackDelegateable)activity).setCallbackDelegate(this);
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mDefaultScroller = new Scroller(getActivity(), sInterpolator);
        mAutomaicScroller = new FixedSpeedScroller(getActivity(), new AccelerateInterpolator());
        mAutomaicScroller.setmDuration(500);
        mCurrScroller = mDefaultScroller;
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        return new TagLoader(getActivity(), mAboutMeInfo);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300a7, viewgroup, false);
        mViewPager = (ViewPager)layoutinflater.findViewById(0x7f0a017a);
        mViewPager.setOffscreenPageLimit(3);
        return layoutinflater;
    }

    public void onDetach()
    {
        super.onDetach();
        if (getActivity() instanceof CallbackDelegateable)
        {
            ((CallbackDelegateable)getActivity()).setCallbackDelegate(null);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            if (!gotoPrevious());
            return true;
        } else
        {
            return false;
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        mAboutMeInfo.mInterestedTags.clear();
        mAdapter.setEnableSubmit(canSubmit());
        mAdapter.setInterestedTags(list);
        mInfoDataChanged = false;
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onStop()
    {
        super.onStop();
        if (!mClickExit)
        {
            ignore(getActivity().getApplicationContext());
        }
    }





/*
    static Scroller access$1002(CollectUserInformationFragment collectuserinformationfragment, Scroller scroller)
    {
        collectuserinformationfragment.mCurrScroller = scroller;
        return scroller;
    }

*/






/*
    static boolean access$202(CollectUserInformationFragment collectuserinformationfragment, boolean flag)
    {
        collectuserinformationfragment.mInfoDataChanged = flag;
        return flag;
    }

*/





/*
    static boolean access$602(CollectUserInformationFragment collectuserinformationfragment, boolean flag)
    {
        collectuserinformationfragment.mClickExit = flag;
        return flag;
    }

*/




    private class _cls4 extends com.ximalaya.ting.android.b.a
    {

        final Context val$context;
        final AboutMeInfo val$info;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
            CollectUserInformationFragment.saveToLocal(context, info);
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                break MISSING_BLOCK_LABEL_44;
            }
            if (JSON.parseObject(s).getIntValue("ret") == 0)
            {
                CollectUserInformationFragment.removeFromLocal(context);
                return;
            }
            try
            {
                CollectUserInformationFragment.saveToLocal(context, info);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }

        _cls4()
        {
            context = context1;
            info = aboutmeinfo;
            super();
        }
    }


    private class _cls2
        implements ICallback
    {

        final CollectUserInformationFragment this$0;

        public void onGenderSelected(String s)
        {
            mAboutMeInfo.mGender = s;
            mInfoDataChanged = true;
            mAdapter.setEnableSubmit(canSubmit());
            mAdapter.addTypePage();
            gotoNextPage();
        }

        public void onInterestedTagSelected(String s)
        {
            mAboutMeInfo.mInterestedTags.add(s);
            mAdapter.setEnableSubmit(canSubmit());
        }

        public void onInterestedTagUnselected(String s)
        {
            mAboutMeInfo.mInterestedTags.remove(s);
            mAdapter.setEnableSubmit(canSubmit());
        }

        public void onSubmitInfo()
        {
            mClickExit = true;
            submit();
            finish();
        }

        public void onTypeSelected(String s)
        {
            mAboutMeInfo.mType = s;
            mInfoDataChanged = true;
            mAdapter.setEnableSubmit(canSubmit());
            mAdapter.addTagPage();
            gotoNextPage();
        }

        _cls2()
        {
            this$0 = CollectUserInformationFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final CollectUserInformationFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
            if (i == 0 && mCurrScroller != mDefaultScroller)
            {
                ViewUtil.setViewPagerScroller(mViewPager, mDefaultScroller);
                mCurrScroller = mDefaultScroller;
            }
        }

        public void onPageScrolled(int i, float f1, int j)
        {
        }

        public void onPageSelected(int i)
        {
            if (i == 2)
            {
                mAdapter.setEnableSubmit(canSubmit());
                if (mInfoDataChanged && canGetTags())
                {
                    loadTags();
                }
            }
        }

        _cls3()
        {
            this$0 = CollectUserInformationFragment.this;
            super();
        }
    }


    private class _cls1
        implements Interpolator
    {

        public float getInterpolation(float f1)
        {
            f1--;
            return f1 * (f1 * f1 * f1 * f1) + 1.0F;
        }

        _cls1()
        {
        }
    }

}
