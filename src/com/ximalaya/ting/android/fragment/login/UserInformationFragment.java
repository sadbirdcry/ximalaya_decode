// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.login;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ximalaya.ting.android.activity.login.ActivityCallbackDelegate;
import com.ximalaya.ting.android.activity.login.CallbackDelegateable;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.FragmentCallback;

// Referenced classes of package com.ximalaya.ting.android.fragment.login:
//            CollectUserInformationFragment

public class UserInformationFragment extends BaseFragment
    implements ActivityCallbackDelegate
{

    private boolean mClickExit;

    public UserInformationFragment()
    {
    }

    private void enter()
    {
        mClickExit = true;
        CollectUserInformationFragment collectuserinformationfragment = CollectUserInformationFragment.newInstance();
        collectuserinformationfragment.setFragmentCallback(new _cls3());
        getFragmentManager().beginTransaction().replace(0x7f0a00f1, collectuserinformationfragment).commitAllowingStateLoss();
    }

    private void exitAndIgnore()
    {
        mClickExit = true;
        CollectUserInformationFragment.ignore(getActivity().getApplicationContext());
        if (mCallback != null)
        {
            mCallback.onFinish();
        }
    }

    public static UserInformationFragment newInstance()
    {
        return new UserInformationFragment();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        getView().findViewById(0x7f0a0376).setOnClickListener(new _cls1());
        getView().findViewById(0x7f0a039d).setOnClickListener(new _cls2());
    }

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (activity instanceof CallbackDelegateable)
        {
            ((CallbackDelegateable)activity).setCallbackDelegate(this);
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        return layoutinflater.inflate(0x7f0300de, viewgroup, false);
    }

    public void onDetach()
    {
        super.onDetach();
        if (getActivity() instanceof CallbackDelegateable)
        {
            ((CallbackDelegateable)getActivity()).setCallbackDelegate(null);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            exitAndIgnore();
            return true;
        } else
        {
            return false;
        }
    }

    public void onStop()
    {
        super.onStop();
        if (!mClickExit)
        {
            CollectUserInformationFragment.ignore(getActivity().getApplicationContext());
        }
    }





    private class _cls3
        implements FragmentCallback
    {

        final UserInformationFragment this$0;

        public void onFinish()
        {
            if (UserInformationFragment.this.onFinish != null)
            {
                UserInformationFragment.this.onFinish.onFinish();
            }
        }

        _cls3()
        {
            this$0 = UserInformationFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final UserInformationFragment this$0;

        public void onClick(View view)
        {
            enter();
        }

        _cls1()
        {
            this$0 = UserInformationFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final UserInformationFragment this$0;

        public void onClick(View view)
        {
            exitAndIgnore();
        }

        _cls2()
        {
            this$0 = UserInformationFragment.this;
            super();
        }
    }

}
