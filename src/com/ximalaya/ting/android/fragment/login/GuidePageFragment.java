// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.login;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.view.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import java.util.Iterator;

public class GuidePageFragment extends BaseFragment
{
    private static class ViewPagerAdapter extends PagerAdapter
    {

        private ArrayList mChildViews;
        private Context mContext;

        public void destory()
        {
            for (Iterator iterator = mChildViews.iterator(); iterator.hasNext(); ((View)iterator.next()).setOnClickListener(null)) { }
            mContext = null;
        }

        public void destroyItem(ViewGroup viewgroup, int i, Object obj)
        {
            viewgroup.removeView((View)mChildViews.get(i));
        }

        public int getCount()
        {
            return mChildViews.size();
        }

        public Object instantiateItem(ViewGroup viewgroup, int i)
        {
            viewgroup.addView((View)mChildViews.get(i));
            return mChildViews.get(i);
        }

        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }

        public ViewPagerAdapter(final Activity context)
        {
            mContext = context;
            mChildViews = new ArrayList(5);
            ImageView imageview = new ImageView(mContext);
            imageview.setImageResource(0x7f020303);
            imageview.setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
            mChildViews.add(imageview);
            imageview = new ImageView(mContext);
            imageview.setImageResource(0x7f020304);
            imageview.setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
            mChildViews.add(imageview);
            imageview = new ImageView(mContext);
            imageview.setImageResource(0x7f020305);
            imageview.setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
            mChildViews.add(imageview);
            imageview = new ImageView(mContext);
            imageview.setImageResource(0x7f020306);
            imageview.setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
            class _cls1
                implements android.view.View.OnClickListener
            {

                final ViewPagerAdapter this$0;
                final Activity val$context;

                public void onClick(View view)
                {
                    context.finish();
                }

                _cls1()
                {
                    this$0 = ViewPagerAdapter.this;
                    context = activity;
                    super();
                }
            }

            imageview.setOnClickListener(new _cls1());
            mChildViews.add(imageview);
        }
    }


    private ViewPagerAdapter mAdapter;
    private CirclePageIndicator mIndicator;
    private ViewPager mViewPager;

    public GuidePageFragment()
    {
    }

    public static GuidePageFragment getInstance()
    {
        return new GuidePageFragment();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mIndicator.setOnPageChangeListener(new _cls1());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f03009a, viewgroup, false);
        mViewPager = (ViewPager)layoutinflater.findViewById(0x7f0a02c4);
        mAdapter = new ViewPagerAdapter(getActivity());
        mViewPager.setAdapter(mAdapter);
        mIndicator = (CirclePageIndicator)layoutinflater.findViewById(0x7f0a02c5);
        mIndicator.setViewPager(mViewPager);
        mIndicator.setCentered(true);
        return layoutinflater;
    }

    public void onDestroyView()
    {
        mAdapter.destory();
        super.onDestroyView();
    }

    private class _cls1
        implements android.support.v4.view.ViewPager.OnPageChangeListener
    {

        final GuidePageFragment this$0;

        public void onPageScrollStateChanged(int i)
        {
        }

        public void onPageScrolled(int i, float f, int j)
        {
        }

        public void onPageSelected(int i)
        {
        }

        _cls1()
        {
            this$0 = GuidePageFragment.this;
            super();
        }
    }

}
