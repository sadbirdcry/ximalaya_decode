// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.login;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseLoginFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.Utilities;

public class LoginFragment extends BaseLoginFragment
{

    private TextView fpwdTextView;
    public ImageView homeButton;
    private Button lgButton;
    private TextView lgRegisterTextView;
    private EditText lg_nameEditText;
    private EditText lg_pwdEditText;
    private ImageButton lg_qqButton;
    private ImageButton lg_webButton;
    private ImageButton lg_weixinButton;
    public ImageView nextButton;
    public ImageView retButton;
    public TextView topTextView;
    public View top_bar;

    public LoginFragment()
    {
    }

    private void initUI()
    {
        if (!PackageUtil.isMIUI() || android.os.Build.VERSION.SDK_INT < 9)
        {
            findViewById(0x7f0a0546).setVisibility(8);
            View view = findViewById(0x7f0a0543);
            android.widget.LinearLayout.LayoutParams layoutparams = (android.widget.LinearLayout.LayoutParams)view.getLayoutParams();
            layoutparams.leftMargin = Utilities.dip2px(getActivity(), 16F);
            layoutparams.rightMargin = 0;
            view.setLayoutParams(layoutparams);
        }
        lg_webButton = (ImageButton)findViewById(0x7f0a0544);
        lg_webButton.setOnClickListener(new _cls1());
        lg_qqButton = (ImageButton)findViewById(0x7f0a0542);
        lg_qqButton.setOnClickListener(new _cls2());
        lg_weixinButton = (ImageButton)findViewById(0x7f0a0541);
        lg_weixinButton.setOnClickListener(new _cls3());
        findViewById(0x7f0a0547).setOnClickListener(new _cls4());
        lg_nameEditText = (EditText)findViewById(0x7f0a0219);
        lg_pwdEditText = (EditText)findViewById(0x7f0a0113);
        lgButton = (Button)findViewById(0x7f0a00bc);
        lgButton.setOnClickListener(new _cls5());
        fpwdTextView = (TextView)findViewById(0x7f0a054a);
        fpwdTextView.setOnClickListener(new _cls6());
        lgRegisterTextView = (TextView)findViewById(0x7f0a0549);
        lgRegisterTextView.setOnClickListener(new _cls7());
    }

    public void dialog(String s)
    {
        (new DialogBuilder(mActivity)).setMessage(s).showWarning();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        setTitleText(getString(0x7f0900a6));
        initUI();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030164, null);
        return fragmentBaseContainerView;
    }













    private class _cls1
        implements android.view.View.OnClickListener
    {

        final LoginFragment this$0;

        public void onClick(View view)
        {
            if (ToolUtil.isConnectToNetwork(mCon))
            {
                sinaLogin();
                return;
            } else
            {
                Toast.makeText(
// JavaClassFileOutputException: get_constant: invalid tag

        _cls1()
        {
            this$0 = LoginFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final LoginFragment this$0;

        public void onClick(View view)
        {
            if (ToolUtil.isConnectToNetwork(mCon))
            {
                QQLogin();
                return;
            } else
            {
                Toast.makeText(
// JavaClassFileOutputException: get_constant: invalid tag

        _cls2()
        {
            this$0 = LoginFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final LoginFragment this$0;

        public void onClick(View view)
        {
            if (ToolUtil.isConnectToNetwork(mCon))
            {
                loginWithWX();
                return;
            } else
            {
                Toast.makeText(
// JavaClassFileOutputException: get_constant: invalid tag

        _cls3()
        {
            this$0 = LoginFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final LoginFragment this$0;

        public void onClick(View view)
        {
            if (ToolUtil.isConnectToNetwork(mCon))
            {
                loginWithXiaomi();
                return;
            } else
            {
                Toast.makeText(
// JavaClassFileOutputException: get_constant: invalid tag

        _cls4()
        {
            this$0 = LoginFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final LoginFragment this$0;

        public void onClick(View view)
        {
            view = lg_nameEditText.getText().toString().trim();
            String s = lg_pwdEditText.getText().toString().trim();
            if (ToolUtil.isConnectToNetwork(mCon))
            {
                if (TextUtils.isEmpty(view) || TextUtils.isEmpty(s))
                {
                    dialog((new StringBuilder()).append(getString(0x7f0900b8)).append("\uFF01").toString());
                    return;
                }
                if (!ToolUtil.verifiEmail(view) && !ToolUtil.verifiPhone(view))
                {
                    dialog("\u7528\u6237\u540D\u683C\u5F0F\u8F93\u5165\u6709\u8BEF\uFF01");
                    return;
                } else
                {
                    ximalayaLogin(view, s);
                    return;
                }
            } else
            {
                Toast.makeText(trim, getString(0x7f0900a0), 1).show();
                return;
            }
        }

        _cls5()
        {
            this$0 = LoginFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final LoginFragment this$0;

        public void onClick(View view)
        {
            view = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
            view.putExtra("ExtraUrl", e.am);
            getActivity().startActivity(view);
        }

        _cls6()
        {
            this$0 = LoginFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.view.View.OnClickListener
    {

        final LoginFragment this$0;

        public void onClick(View view)
        {
            view = new Intent(mCon, com/ximalaya/ting/android/activity/login/RegisterActivity);
            view.setFlags(0x20000000);
            startActivity(view);
        }

        _cls7()
        {
            this$0 = LoginFragment.this;
            super();
        }
    }

}
