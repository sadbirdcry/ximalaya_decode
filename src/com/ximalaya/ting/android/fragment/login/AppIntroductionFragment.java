// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.util.ToolUtil;

public class AppIntroductionFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener
{
    public static interface OnAppIntroDismissCallback
    {

        public abstract void onDismiss();
    }


    private OnAppIntroDismissCallback mCallback;

    public AppIntroductionFragment()
    {
    }

    private void close()
    {
        android.support.v4.app.FragmentActivity fragmentactivity = getActivity();
        if (fragmentactivity instanceof MainTabActivity2)
        {
            ((MainTabActivity2)fragmentactivity).onBack();
        }
    }

    public static void goIntroPage(Context context)
    {
        Intent intent = new Intent(context, com/ximalaya/ting/android/activity/web/WebActivityNew);
        intent.putExtra("ExtraUrl", (new StringBuilder()).append(a.v).append("third/android/newFunction.html?version=").append(ToolUtil.getAppVersion(context)).toString());
        context.startActivity(intent);
    }

    public static AppIntroductionFragment newInstance()
    {
        AppIntroductionFragment appintroductionfragment = new AppIntroductionFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("in_anim", 0x7f040004);
        bundle.putInt("out_anim", 0x7f040005);
        appintroductionfragment.setArguments(bundle);
        return appintroductionfragment;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getActivity();
        if (bundle instanceof MainTabActivity2)
        {
            ((MainTabActivity2)bundle).hidePlayButton();
        }
        getView().findViewById(0x7f0a013d).setOnClickListener(this);
        getView().findViewById(0x7f0a02d8).setOnClickListener(this);
    }

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
        if (activity instanceof OnAppIntroDismissCallback)
        {
            mCallback = (OnAppIntroDismissCallback)activity;
        }
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 2: default 32
    //                   2131362109: 33
    //                   2131362520: 54;
           goto _L1 _L2 _L3
_L1:
        return;
_L2:
        close();
        if (mCallback != null)
        {
            mCallback.onDismiss();
            return;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        goIntroPage(getActivity());
        close();
        if (mCallback != null)
        {
            mCallback.onDismiss();
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        return layoutinflater.inflate(0x7f03009d, viewgroup, false);
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        android.support.v4.app.FragmentActivity fragmentactivity = getActivity();
        if (fragmentactivity instanceof MainTabActivity2)
        {
            ((MainTabActivity2)fragmentactivity).showPlayButton();
        }
    }
}
