// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.content.Context;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.ArrayList;
import java.util.Collections;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            SortDownloadSoundFragment

private static class  extends MyAsyncTaskLoader
{

    private ArrayList mData;

    public volatile void deliverResult(Object obj)
    {
        deliverResult((ArrayList)obj);
    }

    public void deliverResult(ArrayList arraylist)
    {
        super.deliverResult(arraylist);
        mData = arraylist;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public ArrayList loadInBackground()
    {
        ArrayList arraylist = DownloadHandler.getInstance(getContext()).getFinishedTasks();
        Collections.sort(arraylist, SortDownloadSoundFragment.access$200());
        return arraylist;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData != null)
        {
            deliverResult(mData);
            return;
        } else
        {
            forceLoad();
            return;
        }
    }

    public (Context context)
    {
        super(context);
    }
}
