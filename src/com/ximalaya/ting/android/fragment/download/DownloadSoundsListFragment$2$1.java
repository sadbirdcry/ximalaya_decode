// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadSoundsListFragment

class this._cls1
    implements com.ximalaya.ting.android.library.view.dialog.is._cls1
{

    final _cls1.myexec this$1;

    public void onExecute()
    {
        ToolUtil.onEvent(getActivity(), "Down_sound_Deleteall");
        class _cls1 extends MyAsyncTask
        {

            ProgressDialog pd;
            final DownloadSoundsListFragment._cls2._cls1 this$2;

            protected transient Boolean doInBackground(Void avoid[])
            {
                DownloadHandler.getInstance(mAppContext).delAllCompleteTasks();
                return Boolean.valueOf(true);
            }

            protected volatile Object doInBackground(Object aobj[])
            {
                return doInBackground((Void[])aobj);
            }

            protected void onPostExecute(Boolean boolean1)
            {
                super.onPostExecute(boolean1);
                if (pd != null)
                {
                    pd.cancel();
                    pd = null;
                }
                if (boolean1.booleanValue())
                {
                    if (soundsDownloadAdapter != null)
                    {
                        soundsDownloadAdapter.getData().clear();
                        soundsDownloadAdapter.notifyDataSetChanged();
                    }
                    DownloadSoundsListFragment.access$000(this$0).clear();
                    DownloadSoundsListFragment.access$200(this$0).setVisibility(4);
                }
                DownloadSoundsListFragment.access$300(this$0);
            }

            protected volatile void onPostExecute(Object obj)
            {
                onPostExecute((Boolean)obj);
            }

            protected void onPreExecute()
            {
                super.onPreExecute();
                pd = new MyProgressDialog(mActivity);
                pd.show();
                class _cls1
                    implements android.content.DialogInterface.OnKeyListener
                {

                    final _cls1 this$3;

                    public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
                    {
                        return true;
                    }

                        _cls1()
                        {
                            this$3 = _cls1.this;
                            super();
                        }
                }

                pd.setOnKeyListener(new _cls1());
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("\u6B63\u5728\u6E05\u9664\u5DF2\u4E0B\u8F7D\u58F0\u97F3\uFF0C\u8BF7\u7B49\u5F85...");
                PlayListControl.getPlayListManager().doBeforeDelAllDownload();
            }

            _cls1()
            {
                this$2 = DownloadSoundsListFragment._cls2._cls1.this;
                super();
                pd = null;
            }
        }

        (new _cls1()).myexec(new Void[0]);
    }

    _cls1()
    {
        this$1 = this._cls1.this;
        super();
    }
}
