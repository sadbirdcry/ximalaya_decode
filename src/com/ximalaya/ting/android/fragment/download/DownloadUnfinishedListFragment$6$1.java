// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.app.ProgressDialog;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadUnfinishedListFragment

class pd extends MyAsyncTask
{

    ProgressDialog pd;
    final pd this$1;

    protected transient Boolean doInBackground(Void avoid[])
    {
        DownloadHandler.getInstance(mAppContext).pauseAllDownload();
        return Boolean.valueOf(true);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(Boolean boolean1)
    {
        super.onPostExecute(boolean1);
        if (pd != null)
        {
            pd.cancel();
            pd = null;
        }
        updateListViewHeader();
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Boolean)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        pd = new MyProgressDialog(mActivity);
        class _cls1
            implements android.content.DialogInterface.OnKeyListener
        {

            final DownloadUnfinishedListFragment._cls6._cls1 this$2;

            public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
            {
                return true;
            }

            _cls1()
            {
                this$2 = DownloadUnfinishedListFragment._cls6._cls1.this;
                super();
            }
        }

        pd.setOnKeyListener(new _cls1());
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("\u4E00\u952E\u6682\u505C\u6240\u6709\u672A\u5B8C\u6210\u4EFB\u52A1\uFF0C\u8BF7\u7B49\u5F85...");
        pd.show();
    }

    _cls1()
    {
        this$1 = this._cls1.this;
        super();
        pd = null;
    }
}
