// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.PlaylistFromDownload;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadUnfinishedListFragment

class this._cls0
    implements android.widget.loadUnfinishedListFragment._cls3
{

    final DownloadUnfinishedListFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        if (DownloadUnfinishedListFragment.access$000(DownloadUnfinishedListFragment.this) != null && DownloadUnfinishedListFragment.access$000(DownloadUnfinishedListFragment.this).size() != 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if ((i -= DownloadUnfinishedListFragment.access$300(DownloadUnfinishedListFragment.this).getHeaderViewsCount()) < 0 || i + 1 > DownloadUnfinishedListFragment.access$000(DownloadUnfinishedListFragment.this).size() || (adapterview = (DownloadTask)DownloadUnfinishedListFragment.access$000(DownloadUnfinishedListFragment.this).get(i)) == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (((DownloadTask) (adapterview)).downloadStatus != 4)
        {
            break; /* Loop/switch isn't completed */
        }
        adapterview = ModelHelper.downloadlistToPlayList(adapterview, DownloadUnfinishedListFragment.access$000(DownloadUnfinishedListFragment.this));
        if (((PlaylistFromDownload) (adapterview)).index >= 0)
        {
            PlayTools.gotoPlayLocals(2, ((PlaylistFromDownload) (adapterview)).soundsList, ((PlaylistFromDownload) (adapterview)).index, getActivity(), DataCollectUtil.getDataFromView(view));
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        if (((DownloadTask) (adapterview)).downloadStatus == 1)
        {
            Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:pause", true);
            DownloadHandler.getInstance(mAppContext).pauseDownload(adapterview);
            return;
        }
        if (((DownloadTask) (adapterview)).downloadStatus == 2)
        {
            Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:resume from pause", true);
            if (NetworkUtils.getNetType(getActivity()) == -1)
            {
                Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                return;
            } else
            {
                view = DownLoadTools.getInstance();
                view.resume(adapterview);
                view.release();
                return;
            }
        }
        if (((DownloadTask) (adapterview)).downloadStatus == 3)
        {
            Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:resume from failed", true);
            if (NetworkUtils.getNetType(getActivity()) == -1)
            {
                Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                return;
            } else
            {
                view = DownLoadTools.getInstance();
                view.resume(adapterview);
                view.release();
                return;
            }
        }
        if (((DownloadTask) (adapterview)).downloadStatus == 0)
        {
            Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:start NOW from waiting", true);
            if (NetworkUtils.getNetType(getActivity()) == -1)
            {
                Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                return;
            } else
            {
                DownloadHandler.getInstance(mAppContext).startNow(adapterview);
                return;
            }
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    ()
    {
        this$0 = DownloadUnfinishedListFragment.this;
        super();
    }
}
