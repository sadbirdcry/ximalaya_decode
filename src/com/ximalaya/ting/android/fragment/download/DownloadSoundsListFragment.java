// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.adapter.DownloadedSoundListAdapter;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.List;

public class DownloadSoundsListFragment extends BaseFragment
    implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener, com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
{

    private TextView clearAllTV;
    private BounceListView downloadListView;
    private List downloadTaskList;
    public Activity mActivity;
    public Context mAppContext;
    private LinearLayout mEmptyView;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private TextView mSortBtn;
    public DownloadedSoundListAdapter soundsDownloadAdapter;

    public DownloadSoundsListFragment()
    {
        downloadTaskList = new ArrayList();
        clearAllTV = null;
    }

    private void doGetDownloadList()
    {
        if (getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            (new _cls5()).execute(new Void[0]);
            return;
        }
    }

    private void registerListener()
    {
        Object obj = LocalMediaService.getInstance();
        if (obj != null)
        {
            ((LocalMediaService) (obj)).setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls6();
            ((LocalMediaService) (obj)).setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
        obj = DownloadHandler.getInstance(mAppContext);
        if (obj != null)
        {
            ((DownloadHandler) (obj)).addDownloadListeners(this);
        }
    }

    private void showDialogOnDownloadFileMissAll()
    {
        if (getActivity() == null)
        {
            return;
        } else
        {
            (new DialogBuilder(getActivity())).setMessage(0x7f090182).setOkBtn(0x7f090185).showWarning();
            return;
        }
    }

    private void showDialogOnDownloadFileMissPartly()
    {
        if (getActivity() == null)
        {
            return;
        } else
        {
            (new DialogBuilder(getActivity())).setMessage(0x7f090183).setOkBtn(0x7f090185).showWarning();
            return;
        }
    }

    private void showDownloadFileCheckResult()
    {
        MyApplication.g;
        JVM INSTR tableswitch 1 2: default 24
    //                   1 29
    //                   2 36;
           goto _L1 _L2 _L3
_L1:
        MyApplication.g = -1;
        return;
_L2:
        showDialogOnDownloadFileMissPartly();
        continue; /* Loop/switch isn't completed */
_L3:
        showDialogOnDownloadFileMissAll();
        if (true) goto _L1; else goto _L4
_L4:
    }

    private void showEmptyView()
    {
        if (downloadTaskList.size() == 0)
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(0);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(0);
            mEmptyView.setVisibility(0);
            return;
        } else
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0232).setVisibility(8);
            mEmptyView.setVisibility(8);
            return;
        }
    }

    private void unRegisterListener()
    {
        Object obj = LocalMediaService.getInstance();
        if (obj != null)
        {
            ((LocalMediaService) (obj)).removeOnPlayServiceUpdateListener(this);
            ((LocalMediaService) (obj)).removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
        obj = DownloadHandler.getInstance(mAppContext);
        if (obj != null)
        {
            ((DownloadHandler) (obj)).removeDownloadListeners(this);
        }
    }

    private void updateClearAllButton()
    {
label0:
        {
            if (clearAllTV != null)
            {
                if (downloadTaskList == null || downloadTaskList.size() <= 0)
                {
                    break label0;
                }
                clearAllTV.setVisibility(0);
                mSortBtn.setVisibility(0);
            }
            return;
        }
        clearAllTV.setVisibility(4);
        mSortBtn.setVisibility(4);
    }

    public void delSound(long l)
    {
    }

    public void notifyDataSetChanged()
    {
        List list = soundsDownloadAdapter.getData();
        DownloadHandler.getInstance(getActivity()).sortData(list);
        soundsDownloadAdapter.notifyDataSetChanged();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        Logger.log("0814", "====DownloadSoundsListFragment========onActivityCreated ");
        downloadListView.setOnItemClickListener(new _cls1());
        clearAllTV.setOnClickListener(new _cls2());
        downloadListView.setOnItemLongClickListener(new _cls3());
        mSortBtn.setOnClickListener(new _cls4());
        doGetDownloadList();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (isAdded() && i == 1 && j == 1)
        {
            i = intent.getIntExtra("position", -1);
            long l = intent.getLongExtra("trackId", 0L);
            if (downloadTaskList != null && i < downloadTaskList.size() && i >= 0 && ((DownloadTask)downloadTaskList.get(i)).trackId == l)
            {
                ((DownloadTask)downloadTaskList.get(i)).isRelay = true;
                soundsDownloadAdapter.notifyDataSetChanged();
                return;
            }
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        Logger.log("0814", "====DownloadSoundsListFragment========onCreateView ");
        mActivity = getActivity();
        mAppContext = mActivity.getApplicationContext();
        viewgroup = layoutinflater.inflate(0x7f030079, viewgroup, false);
        downloadListView = (BounceListView)viewgroup.findViewById(0x7f0a0229);
        if (PackageUtil.isMeizu() && getActivity() != null)
        {
            downloadListView.setPadding(0, 0, 0, Utilities.dip2px(getActivity(), 70F));
        }
        downloadListView.setFooterDividersEnabled(false);
        soundsDownloadAdapter = new DownloadedSoundListAdapter(getActivity(), downloadTaskList);
        bundle = LayoutInflater.from(mAppContext).inflate(0x7f030075, downloadListView, false);
        clearAllTV = (TextView)bundle.findViewById(0x7f0a0212);
        mSortBtn = (TextView)bundle.findViewById(0x7f0a020f);
        mSortBtn.setCompoundDrawablesWithIntrinsicBounds(0x7f0202c4, 0, 0, 0);
        mSortBtn.setVisibility(0);
        mSortBtn.setText(0x7f090180);
        downloadListView.addHeaderView(bundle);
        mEmptyView = (LinearLayout)layoutinflater.inflate(0x7f03007c, downloadListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u4E0B\u8F7D\u8FC7\u58F0\u97F3\u54E6");
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setVisibility(8);
        ((Button)mEmptyView.findViewById(0x7f0a0232)).setVisibility(8);
        downloadListView.addFooterView(mEmptyView);
        downloadListView.setAdapter(soundsDownloadAdapter);
        updateClearAllButton();
        showEmptyView();
        registerListener();
        return viewgroup;
    }

    public void onDestroyView()
    {
        Logger.log("0814", "====DownloadSoundsListFragment========onDestroyView ");
        unRegisterListener();
        super.onDestroyView();
    }

    public void onPlayCanceled()
    {
    }

    public void onResume()
    {
        Logger.log("0814", "====DownloadSoundsListFragment========onResume ");
        super.onResume();
        showDownloadFileCheckResult();
    }

    public void onSdcardStateChanged()
    {
        if (soundsDownloadAdapter != null)
        {
            soundsDownloadAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundChanged(int i)
    {
        if (soundsDownloadAdapter != null)
        {
            soundsDownloadAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (i < 0 || soundinfo == null || i >= downloadTaskList.size() || ((DownloadTask)downloadTaskList.get(i)).trackId != soundinfo.trackId || soundsDownloadAdapter == null)
        {
            return;
        } else
        {
            DownloadTask downloadtask = (DownloadTask)downloadTaskList.get(i);
            downloadtask.is_favorited = soundinfo.is_favorited;
            downloadtask.favorites_counts = soundinfo.favorites_counts;
            soundsDownloadAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void onTaskComplete()
    {
        doGetDownloadList();
    }

    public void onTaskDelete()
    {
        doGetDownloadList();
    }

    public void updateActionInfo()
    {
        doGetDownloadList();
    }

    public void updateDownloadInfo(int i)
    {
        doGetDownloadList();
    }






    private class _cls5 extends AsyncTask
    {

        final DownloadSoundsListFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            return DownloadHandler.getInstance(mAppContext).getSortedFinishedDownloadList();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (!isAdded())
            {
                return;
            }
            downloadTaskList.clear();
            if (list != null && list.size() > 0)
            {
                downloadTaskList.addAll(list);
            }
            soundsDownloadAdapter.notifyDataSetChanged();
            updateClearAllButton();
            showEmptyView();
        }

        _cls5()
        {
            this$0 = DownloadSoundsListFragment.this;
            super();
        }
    }


    private class _cls6 extends OnPlayerStatusUpdateListenerProxy
    {

        final DownloadSoundsListFragment this$0;

        public void onPlayStateChange()
        {
            soundsDownloadAdapter.notifyDataSetChanged();
        }

        _cls6()
        {
            this$0 = DownloadSoundsListFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final DownloadSoundsListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (downloadTaskList != null && downloadTaskList.size() != 0) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if ((i -= downloadListView.getHeaderViewsCount()) < 0 || i >= downloadTaskList.size() || (adapterview = (DownloadTask)downloadTaskList.get(i)) == null)
            {
                continue; /* Loop/switch isn't completed */
            }
            if (((DownloadTask) (adapterview)).downloadStatus != 4)
            {
                break; /* Loop/switch isn't completed */
            }
            adapterview = ModelHelper.downloadlistToPlayList(adapterview, downloadTaskList);
            if (((PlaylistFromDownload) (adapterview)).index >= 0)
            {
                PlayTools.gotoPlayLocals(17, ((PlaylistFromDownload) (adapterview)).soundsList, ((PlaylistFromDownload) (adapterview)).index, getActivity(), DataCollectUtil.getDataFromView(view));
                return;
            }
            if (true) goto _L1; else goto _L3
_L3:
            if (((DownloadTask) (adapterview)).downloadStatus == 1)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:pause", true);
                DownloadHandler.getInstance(mAppContext).pauseDownload(adapterview);
                return;
            }
            if (((DownloadTask) (adapterview)).downloadStatus == 2)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:resume from pause", true);
                if (NetworkUtils.getNetType(getActivity()) == -1)
                {
                    Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                    return;
                } else
                {
                    view = DownLoadTools.getInstance();
                    view.resume(adapterview);
                    view.release();
                    return;
                }
            }
            if (((DownloadTask) (adapterview)).downloadStatus == 3)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:resume from failed", true);
                if (NetworkUtils.getNetType(getActivity()) == -1)
                {
                    Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                    return;
                } else
                {
                    view = DownLoadTools.getInstance();
                    view.resume(adapterview);
                    view.release();
                    return;
                }
            }
            if (((DownloadTask) (adapterview)).downloadStatus == 0)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:start NOW from waiting", true);
                if (NetworkUtils.getNetType(getActivity()) == -1)
                {
                    Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                    return;
                } else
                {
                    DownloadHandler.getInstance(mAppContext).startNow(adapterview);
                    return;
                }
            }
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls1()
        {
            this$0 = DownloadSoundsListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final DownloadSoundsListFragment this$0;

        public void onClick(View view)
        {
            class _cls1
                implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
            {

                final _cls2 this$1;

                public void onExecute()
                {
                    ToolUtil.onEvent(getActivity(), "Down_sound_Deleteall");
                    class _cls1 extends MyAsyncTask
                    {

                        ProgressDialog pd;
                        final _cls1 this$2;

                        protected transient Boolean doInBackground(Void avoid[])
                        {
                            DownloadHandler.getInstance(mAppContext).delAllCompleteTasks();
                            return Boolean.valueOf(true);
                        }

                        protected volatile Object doInBackground(Object aobj[])
                        {
                            return doInBackground((Void[])aobj);
                        }

                        protected void onPostExecute(Boolean boolean1)
                        {
                            super.onPostExecute(boolean1);
                            if (pd != null)
                            {
                                pd.cancel();
                                pd = null;
                            }
                            if (boolean1.booleanValue())
                            {
                                if (soundsDownloadAdapter != null)
                                {
                                    soundsDownloadAdapter.getData().clear();
                                    soundsDownloadAdapter.notifyDataSetChanged();
                                }
                                downloadTaskList.clear();
                                clearAllTV.setVisibility(4);
                            }
                            showEmptyView();
                        }

                        protected volatile void onPostExecute(Object obj)
                        {
                            onPostExecute((Boolean)obj);
                        }

                        protected void onPreExecute()
                        {
                            super.onPreExecute();
                            pd = new MyProgressDialog(mActivity);
                            pd.show();
                            class _cls1
                                implements android.content.DialogInterface.OnKeyListener
                            {

                                final _cls1 this$3;

                                public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
                                {
                                    return true;
                                }

                                    _cls1()
                                    {
                                        this$3 = _cls1.this;
                                        super();
                                    }
                            }

                            pd.setOnKeyListener(new _cls1());
                            pd.setCanceledOnTouchOutside(false);
                            pd.setMessage("\u6B63\u5728\u6E05\u9664\u5DF2\u4E0B\u8F7D\u58F0\u97F3\uFF0C\u8BF7\u7B49\u5F85...");
                            PlayListControl.getPlayListManager().doBeforeDelAllDownload();
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                                pd = null;
                            }
                    }

                    (new _cls1()).myexec(new Void[0]);
                }

                _cls1()
                {
                    this$1 = _cls2.this;
                    super();
                }
            }

            (new DialogBuilder(mActivity)).setMessage("\u786E\u5B9A\u6E05\u7A7A\u6240\u6709\u4E0B\u8F7D\u7684\u58F0\u97F3\uFF1F").setOkBtn(new _cls1()).showConfirm();
        }

        _cls2()
        {
            this$0 = DownloadSoundsListFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final DownloadSoundsListFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= downloadListView.getHeaderViewsCount();
            if (i < 0 || i >= soundsDownloadAdapter.getCount())
            {
                return false;
            } else
            {
                soundsDownloadAdapter.handleItemLongClick((Likeable)soundsDownloadAdapter.getData().get(i), view);
                return true;
            }
        }

        _cls3()
        {
            this$0 = DownloadSoundsListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final DownloadSoundsListFragment this$0;

        public void onClick(View view)
        {
            view = SortDownloadSoundFragment.getInstance();
            view.setTargetFragment(DownloadSoundsListFragment.this, 1);
            FragmentTransaction fragmenttransaction = getActivity().getSupportFragmentManager().beginTransaction();
            fragmenttransaction.setCustomAnimations(0x7f04001c, 0x7f040020, 0x7f04001c, 0x7f040020);
            fragmenttransaction.add(0x7f0a00c4, view).addToBackStack(null).commitAllowingStateLoss();
        }

        _cls4()
        {
            this$0 = DownloadSoundsListFragment.this;
            super();
        }
    }

}
