// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.widget.TextView;
import com.ximalaya.ting.android.adapter.DownloadedSoundListAdapter;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadSoundsListForAlbumFragment

class this._cls0
    implements Runnable
{

    final DownloadSoundsListForAlbumFragment this$0;

    public void run()
    {
        if (isAdded()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        obj = DownloadHandler.getInstance(mAppContext);
        Logger.log("0814", (new StringBuilder()).append("====DownloadSoundsListForAlbumFragment========GET downloadTaskList: ").append(DownloadSoundsListForAlbumFragment.access$100(DownloadSoundsListForAlbumFragment.this).size()).toString());
        obj = ((DownloadHandler) (obj)).getSortedFinishedDownloadList();
        if (obj == null || ((List) (obj)).size() == 0) goto _L1; else goto _L3
_L3:
        int i = SharedPreferencesUtil.getInstance(getActivity()).getInt((new StringBuilder()).append("download_album_soundlist_order").append(DownloadSoundsListForAlbumFragment.access$300(DownloadSoundsListForAlbumFragment.this)).toString(), 1);
        DownloadSoundsListForAlbumFragment.access$200(DownloadSoundsListForAlbumFragment.this).clear();
        obj = ((List) (obj)).iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break;
            }
            DownloadTask downloadtask = (DownloadTask)((Iterator) (obj)).next();
            if ("albumId".equals(DownloadSoundsListForAlbumFragment.access$900(DownloadSoundsListForAlbumFragment.this)) && downloadtask.albumId == DownloadSoundsListForAlbumFragment.access$300(DownloadSoundsListForAlbumFragment.this))
            {
                DownloadSoundsListForAlbumFragment.access$200(DownloadSoundsListForAlbumFragment.this).add(downloadtask);
            }
        } while (true);
        obj = new ArrayList();
        if (i == -1)
        {
            ((List) (obj)).addAll(DownloadSoundsListForAlbumFragment.access$200(DownloadSoundsListForAlbumFragment.this));
            Collections.reverse(((List) (obj)));
        } else
        {
            obj = DownloadSoundsListForAlbumFragment.access$200(DownloadSoundsListForAlbumFragment.this);
        }
        DownloadSoundsListForAlbumFragment.access$100(DownloadSoundsListForAlbumFragment.this).clear();
        DownloadSoundsListForAlbumFragment.access$100(DownloadSoundsListForAlbumFragment.this).addAll(((java.util.Collection) (obj)));
        soundsDownloadAdapter.notifyDataSetChanged();
        DownloadSoundsListForAlbumFragment.access$1000(DownloadSoundsListForAlbumFragment.this);
        DownloadSoundsListForAlbumFragment.access$500(DownloadSoundsListForAlbumFragment.this);
        if (DownloadSoundsListForAlbumFragment.access$200(DownloadSoundsListForAlbumFragment.this) != null && DownloadSoundsListForAlbumFragment.access$200(DownloadSoundsListForAlbumFragment.this).size() > 0)
        {
            DownloadSoundsListForAlbumFragment.access$602(DownloadSoundsListForAlbumFragment.this, ModelHelper.toAlbumModel((DownloadTask)DownloadSoundsListForAlbumFragment.access$200(DownloadSoundsListForAlbumFragment.this).get(0)));
        }
        if (!"albumId".equals(DownloadSoundsListForAlbumFragment.access$900(DownloadSoundsListForAlbumFragment.this)))
        {
            continue; /* Loop/switch isn't completed */
        }
        if (DownloadSoundsListForAlbumFragment.access$300(DownloadSoundsListForAlbumFragment.this) == 0L)
        {
            DownloadSoundsListForAlbumFragment.access$1100(DownloadSoundsListForAlbumFragment.this).setText("\u672A\u547D\u540D\u4E13\u8F91");
            return;
        }
        if (DownloadSoundsListForAlbumFragment.access$100(DownloadSoundsListForAlbumFragment.this) == null || DownloadSoundsListForAlbumFragment.access$100(DownloadSoundsListForAlbumFragment.this).size() <= 0) goto _L1; else goto _L4
_L4:
        DownloadSoundsListForAlbumFragment.access$1100(DownloadSoundsListForAlbumFragment.this).setText(((DownloadTask)DownloadSoundsListForAlbumFragment.access$100(DownloadSoundsListForAlbumFragment.this).get(0)).albumName);
        return;
        if (DownloadSoundsListForAlbumFragment.access$100(DownloadSoundsListForAlbumFragment.this) == null || DownloadSoundsListForAlbumFragment.access$100(DownloadSoundsListForAlbumFragment.this).size() <= 0) goto _L1; else goto _L5
_L5:
        DownloadSoundsListForAlbumFragment.access$1100(DownloadSoundsListForAlbumFragment.this).setText(((DownloadTask)DownloadSoundsListForAlbumFragment.access$100(DownloadSoundsListForAlbumFragment.this).get(0)).nickname);
        return;
    }

    ()
    {
        this$0 = DownloadSoundsListForAlbumFragment.this;
        super();
    }
}
