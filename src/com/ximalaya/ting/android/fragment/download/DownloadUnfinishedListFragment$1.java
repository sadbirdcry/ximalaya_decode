// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.os.AsyncTask;
import com.ximalaya.ting.android.adapter.SoundsDownloadAdapter;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadUnfinishedListFragment

class this._cls0 extends AsyncTask
{

    final DownloadUnfinishedListFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        return DownloadHandler.getInstance(mAppContext).getUnfinishedTasks();
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        if (!isAdded())
        {
            return;
        }
        DownloadUnfinishedListFragment.access$000(DownloadUnfinishedListFragment.this).clear();
        if (list != null && list.size() > 0)
        {
            DownloadUnfinishedListFragment.access$000(DownloadUnfinishedListFragment.this).addAll(list);
        }
        DownloadUnfinishedListFragment.access$100(DownloadUnfinishedListFragment.this).notifyDataSetChanged();
        updateListViewHeader();
        DownloadUnfinishedListFragment.access$200(DownloadUnfinishedListFragment.this);
    }

    ()
    {
        this$0 = DownloadUnfinishedListFragment.this;
        super();
    }
}
