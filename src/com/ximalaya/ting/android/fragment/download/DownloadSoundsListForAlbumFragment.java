// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.DownloadedSoundListAdapter;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DownloadSoundsListForAlbumFragment extends BaseFragment
    implements com.ximalaya.ting.android.adapter.DownloadedSoundListAdapter.DelDownloadListener, com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener, com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
{
    public static interface RemoveSounds
    {

        public abstract void remove();
    }


    private TextView addDownloadSoundTxt;
    private long albumId;
    private List albumSoundlist;
    private com.ximalaya.ting.android.transaction.download.AlbumDownload.FinishCallback callback;
    private BounceListView downloadListView;
    private List downloadTaskList;
    private String flag;
    View listHeader;
    private TextView listInfoTV;
    public Activity mActivity;
    private AlbumModel mAlbumModel;
    public Context mAppContext;
    private TextView orderTV;
    private RemoveSounds removeSounds;
    private ImageView retBtn;
    public DownloadedSoundListAdapter soundsDownloadAdapter;
    private TextView top_view;

    public DownloadSoundsListForAlbumFragment()
    {
        albumSoundlist = new ArrayList();
        downloadTaskList = new ArrayList();
        callback = new _cls6();
    }

    public DownloadSoundsListForAlbumFragment(long l, String s, RemoveSounds removesounds)
    {
        albumSoundlist = new ArrayList();
        downloadTaskList = new ArrayList();
        callback = new _cls6();
        albumId = l;
        flag = s;
        removeSounds = removesounds;
    }

    private void doGetDownloadList()
    {
        if (getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            getActivity().runOnUiThread(new _cls8());
            return;
        }
    }

    private void findViews()
    {
        downloadListView = (BounceListView)fragmentBaseContainerView.findViewById(0x7f0a0228);
        top_view = (TextView)fragmentBaseContainerView.findViewById(0x7f0a00ae);
        retBtn = (ImageView)fragmentBaseContainerView.findViewById(0x7f0a007b);
        addDownloadSoundTxt = (TextView)fragmentBaseContainerView.findViewById(0x7f0a0717);
        addDownloadSoundTxt.setText("\u6DFB\u52A0");
    }

    private void initData()
    {
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========new DoGetDownloadList().myexec(); ");
        doGetDownloadList();
    }

    private void initViews()
    {
        listHeader = LayoutInflater.from(mAppContext).inflate(0x7f030103, downloadListView, false);
        android.widget.AbsListView.LayoutParams layoutparams = new android.widget.AbsListView.LayoutParams(-1, Utilities.dip2px(mAppContext, 44F));
        listHeader.setPadding(Utilities.dip2px(mAppContext, 20F), Utilities.dip2px(mAppContext, 13F), Utilities.dip2px(mAppContext, 15F), 0);
        listHeader.setLayoutParams(layoutparams);
        listInfoTV = (TextView)listHeader.findViewById(0x7f0a043d);
        orderTV = (TextView)listHeader.findViewById(0x7f0a0373);
        downloadListView.addHeaderView(listHeader);
        soundsDownloadAdapter = new DownloadedSoundListAdapter(getActivity(), downloadTaskList);
        soundsDownloadAdapter.setDelDownloadListener(this);
        downloadListView.setAdapter(soundsDownloadAdapter);
        updateListInfoView();
        updateOrderView();
    }

    private void initViewsListener()
    {
        downloadListView.setOnItemClickListener(new _cls1());
        retBtn.setOnClickListener(new _cls2());
        downloadListView.setOnItemLongClickListener(new _cls3());
        orderTV.setOnClickListener(new _cls4());
        addDownloadSoundTxt.setOnClickListener(new _cls5());
    }

    private void orderAlbumSoundlist(int i)
    {
        if (albumSoundlist != null && albumSoundlist.size() > 0)
        {
            Object obj = new ArrayList();
            if (i == 1)
            {
                obj = albumSoundlist;
            } else
            {
                ((List) (obj)).addAll(albumSoundlist);
                Collections.reverse(((List) (obj)));
            }
            downloadTaskList.clear();
            downloadTaskList.addAll(((java.util.Collection) (obj)));
            soundsDownloadAdapter.notifyDataSetChanged();
            return;
        } else
        {
            doGetDownloadList();
            return;
        }
    }

    private void registerListener()
    {
        Object obj = LocalMediaService.getInstance();
        if (obj != null)
        {
            ((LocalMediaService) (obj)).setOnPlayServiceUpdateListener(this);
        }
        obj = DownloadHandler.getInstance(mAppContext);
        if (obj != null)
        {
            ((DownloadHandler) (obj)).addDownloadListeners(this);
        }
    }

    private void unRegisterListener()
    {
        Object obj = LocalMediaService.getInstance();
        if (obj != null)
        {
            ((LocalMediaService) (obj)).removeOnPlayServiceUpdateListener(this);
        }
        obj = DownloadHandler.getInstance(mAppContext);
        if (obj != null)
        {
            ((DownloadHandler) (obj)).removeDownloadListeners(this);
        }
    }

    private void updateListInfoView()
    {
label0:
        {
            if (listInfoTV != null)
            {
                if (downloadTaskList != null && downloadTaskList.size() != 0)
                {
                    break label0;
                }
                listInfoTV.setText(getString(0x7f090095, new Object[] {
                    Integer.valueOf(0)
                }));
            }
            return;
        }
        listInfoTV.setText(getString(0x7f090095, new Object[] {
            Integer.valueOf(downloadTaskList.size())
        }));
    }

    private void updateOrderView()
    {
        if (orderTV == null)
        {
            return;
        }
        if (1 == SharedPreferencesUtil.getInstance(getActivity()).getInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), 1))
        {
            orderTV.setCompoundDrawablesWithIntrinsicBounds(0x7f02029c, 0, 0, 0);
            return;
        } else
        {
            orderTV.setCompoundDrawablesWithIntrinsicBounds(0x7f02029e, 0, 0, 0);
            return;
        }
    }

    public void delSound(long l)
    {
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onActivityCreated ");
        initData();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onCreateView ");
        mActivity = getActivity();
        mAppContext = mActivity.getApplicationContext();
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030078, viewgroup, false);
        findViews();
        initViews();
        initViewsListener();
        registerListener();
        return fragmentBaseContainerView;
    }

    public void onDestroy()
    {
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onDestroy ");
        super.onDestroy();
    }

    public void onDestroyView()
    {
        removeSounds.remove();
        unRegisterListener();
        super.onDestroyView();
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onDestroyView ");
    }

    public void onPause()
    {
        super.onPause();
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onPause ");
    }

    public void onPlayCanceled()
    {
    }

    public void onResume()
    {
        super.onResume();
        Logger.log("0814", "====DownloadSoundsListForAlbumFragment========onResume ");
    }

    public void onSdcardStateChanged()
    {
        if (soundsDownloadAdapter != null)
        {
            soundsDownloadAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundChanged(int i)
    {
        if (soundsDownloadAdapter != null)
        {
            soundsDownloadAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (i < 0 || soundinfo == null || i >= downloadTaskList.size() || ((DownloadTask)downloadTaskList.get(i)).trackId != soundinfo.trackId || soundsDownloadAdapter == null)
        {
            return;
        } else
        {
            DownloadTask downloadtask = (DownloadTask)downloadTaskList.get(i);
            downloadtask.is_favorited = soundinfo.is_favorited;
            downloadtask.favorites_counts = soundinfo.favorites_counts;
            soundsDownloadAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void onTaskComplete()
    {
        doGetDownloadList();
    }

    public void onTaskDelete()
    {
        doGetDownloadList();
    }

    public void remove()
    {
        updateListInfoView();
    }

    public final List sortDownloadList(List list, final int orderType)
    {
        if (list == null || list.size() <= 1)
        {
            return list;
        }
        try
        {
            Collections.sort(list, new _cls7());
        }
        catch (Exception exception)
        {
            Logger.throwRuntimeException((new StringBuilder()).append("\u4E0B\u8F7D\u4E13\u8F91\u5217\u8868\u6392\u5E8F\u7B97\u6CD5\u5F02\u5E38\uFF1A ").append(Logger.getLineInfo()).toString());
            return list;
        }
        return list;
    }

    public void updateActionInfo()
    {
        doGetDownloadList();
    }

    public void updateDownloadInfo(int i)
    {
        doGetDownloadList();
    }











/*
    static AlbumModel access$602(DownloadSoundsListForAlbumFragment downloadsoundslistforalbumfragment, AlbumModel albummodel)
    {
        downloadsoundslistforalbumfragment.mAlbumModel = albummodel;
        return albummodel;
    }

*/




    private class _cls6
        implements com.ximalaya.ting.android.transaction.download.AlbumDownload.FinishCallback
    {

        final DownloadSoundsListForAlbumFragment this$0;

        public void onFinish()
        {
            while (!isAdded() || getFragmentManager() == null || mAlbumModel == null) 
            {
                return;
            }
            ShareToWeixinDialogFragment sharetoweixindialogfragment = ShareToWeixinDialogFragment.getInstance(mAlbumModel);
            FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
            fragmenttransaction.add(sharetoweixindialogfragment, "dialog");
            fragmenttransaction.commitAllowingStateLoss();
        }

        _cls6()
        {
            this$0 = DownloadSoundsListForAlbumFragment.this;
            super();
        }
    }


    private class _cls8
        implements Runnable
    {

        final DownloadSoundsListForAlbumFragment this$0;

        public void run()
        {
            if (isAdded()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            Object obj;
            obj = DownloadHandler.getInstance(mAppContext);
            Logger.log("0814", (new StringBuilder()).append("====DownloadSoundsListForAlbumFragment========GET downloadTaskList: ").append(downloadTaskList.size()).toString());
            obj = ((DownloadHandler) (obj)).getSortedFinishedDownloadList();
            if (obj == null || ((List) (obj)).size() == 0) goto _L1; else goto _L3
_L3:
            int i = SharedPreferencesUtil.getInstance(getActivity()).getInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), 1);
            albumSoundlist.clear();
            obj = ((List) (obj)).iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                DownloadTask downloadtask = (DownloadTask)((Iterator) (obj)).next();
                if ("albumId".equals(flag) && downloadtask.albumId == albumId)
                {
                    albumSoundlist.add(downloadtask);
                }
            } while (true);
            obj = new ArrayList();
            if (i == -1)
            {
                ((List) (obj)).addAll(albumSoundlist);
                Collections.reverse(((List) (obj)));
            } else
            {
                obj = albumSoundlist;
            }
            downloadTaskList.clear();
            downloadTaskList.addAll(((java.util.Collection) (obj)));
            soundsDownloadAdapter.notifyDataSetChanged();
            updateListInfoView();
            updateOrderView();
            if (albumSoundlist != null && albumSoundlist.size() > 0)
            {
                mAlbumModel = ModelHelper.toAlbumModel((DownloadTask)albumSoundlist.get(0));
            }
            if (!"albumId".equals(flag))
            {
                continue; /* Loop/switch isn't completed */
            }
            if (albumId == 0L)
            {
                top_view.setText("\u672A\u547D\u540D\u4E13\u8F91");
                return;
            }
            if (downloadTaskList == null || downloadTaskList.size() <= 0) goto _L1; else goto _L4
_L4:
            top_view.setText(((DownloadTask)downloadTaskList.get(0)).albumName);
            return;
            if (downloadTaskList == null || downloadTaskList.size() <= 0) goto _L1; else goto _L5
_L5:
            top_view.setText(((DownloadTask)downloadTaskList.get(0)).nickname);
            return;
        }

        _cls8()
        {
            this$0 = DownloadSoundsListForAlbumFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final DownloadSoundsListForAlbumFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (downloadListView != null && downloadListView.getCount() > i && i != 0) goto _L2; else goto _L1
_L1:
            return;
_L2:
            adapterview = (DownloadTask)downloadListView.getItemAtPosition(i);
            if (((DownloadTask) (adapterview)).downloadStatus != 4)
            {
                break; /* Loop/switch isn't completed */
            }
            adapterview = ModelHelper.downloadlistToPlayList(adapterview, downloadTaskList);
            if (((PlaylistFromDownload) (adapterview)).index >= 0)
            {
                PlayTools.gotoPlayLocals(2, ((PlaylistFromDownload) (adapterview)).soundsList, ((PlaylistFromDownload) (adapterview)).index, getActivity(), DataCollectUtil.getDataFromView(view));
                return;
            }
            if (true) goto _L1; else goto _L3
_L3:
            if (((DownloadTask) (adapterview)).downloadStatus == 1)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:pause", true);
                DownloadHandler.getInstance(mAppContext).pauseDownload(adapterview);
                return;
            }
            if (((DownloadTask) (adapterview)).downloadStatus == 2)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:resume from pause", true);
                if (NetworkUtils.getNetType(getActivity()) == -1)
                {
                    Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                    return;
                } else
                {
                    view = DownLoadTools.getInstance();
                    view.resume(adapterview);
                    view.release();
                    return;
                }
            }
            if (((DownloadTask) (adapterview)).downloadStatus == 3)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:resume from failed", true);
                if (NetworkUtils.getNetType(getActivity()) == -1)
                {
                    Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                    return;
                } else
                {
                    view = DownLoadTools.getInstance();
                    view.resume(adapterview);
                    view.release();
                    return;
                }
            }
            if (((DownloadTask) (adapterview)).downloadStatus == 0)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:start NOW from waiting", true);
                if (NetworkUtils.getNetType(getActivity()) == -1)
                {
                    Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                    return;
                } else
                {
                    DownloadHandler.getInstance(mAppContext).startNow(adapterview);
                    return;
                }
            }
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls1()
        {
            this$0 = DownloadSoundsListForAlbumFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final DownloadSoundsListForAlbumFragment this$0;

        public void onClick(View view)
        {
            if (getActivity() != null)
            {
                ((MainTabActivity2)getActivity()).onBack();
            }
        }

        _cls2()
        {
            this$0 = DownloadSoundsListForAlbumFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final DownloadSoundsListForAlbumFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= downloadListView.getHeaderViewsCount();
            if (i < 0 || i >= soundsDownloadAdapter.getCount())
            {
                return false;
            } else
            {
                soundsDownloadAdapter.handleItemLongClick((Likeable)soundsDownloadAdapter.getData().get(i), view);
                return true;
            }
        }

        _cls3()
        {
            this$0 = DownloadSoundsListForAlbumFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final DownloadSoundsListForAlbumFragment this$0;

        public void onClick(View view)
        {
            int i = -1;
            if (albumSoundlist == null || albumSoundlist.size() <= 1) goto _L2; else goto _L1
_L1:
            int j;
            view = SharedPreferencesUtil.getInstance(getActivity());
            j = view.getInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), 1);
            j;
            JVM INSTR tableswitch -1 1: default 96
        //                       -1 147
        //                       0 96
        //                       1 114;
               goto _L3 _L4 _L3 _L5
_L3:
            i = j;
_L7:
            orderAlbumSoundlist(i);
            updateOrderView();
_L2:
            return;
_L5:
            view.saveInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), -1);
            continue; /* Loop/switch isn't completed */
_L4:
            view.saveInt((new StringBuilder()).append("download_album_soundlist_order").append(albumId).toString(), 1);
            i = 1;
            if (true) goto _L7; else goto _L6
_L6:
        }

        _cls4()
        {
            this$0 = DownloadSoundsListForAlbumFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final DownloadSoundsListForAlbumFragment this$0;

        public void onClick(View view)
        {
            (new AlbumDownload(getActivity(), DownloadSoundsListForAlbumFragment.this, mAlbumModel, callback, addDownloadSoundTxt)).batchDownload();
        }

        _cls5()
        {
            this$0 = DownloadSoundsListForAlbumFragment.this;
            super();
        }
    }


    private class _cls7
        implements Comparator
    {

        final DownloadSoundsListForAlbumFragment this$0;
        final int val$orderType;

        public int compare(DownloadTask downloadtask, DownloadTask downloadtask1)
        {
            if (downloadtask == null || downloadtask1 == null)
            {
                break MISSING_BLOCK_LABEL_110;
            }
            if (downloadtask.orderNum <= downloadtask1.orderNum) goto _L2; else goto _L1
_L1:
            if (1 != orderType) goto _L4; else goto _L3
_L3:
            return 1;
_L4:
            return -1;
_L2:
            if (downloadtask.orderNum >= downloadtask1.orderNum)
            {
                break; /* Loop/switch isn't completed */
            }
            if (1 == orderType)
            {
                return -1;
            }
            if (true) goto _L3; else goto _L5
_L5:
            if (downloadtask.orderNum != downloadtask1.orderNum)
            {
                break MISSING_BLOCK_LABEL_110;
            }
            if (downloadtask.create_at <= downloadtask1.create_at)
            {
                break; /* Loop/switch isn't completed */
            }
            if (1 != orderType)
            {
                return -1;
            }
            if (true) goto _L3; else goto _L6
_L6:
            if (downloadtask.create_at >= downloadtask1.create_at)
            {
                break MISSING_BLOCK_LABEL_110;
            }
            if (1 != orderType) goto _L3; else goto _L7
_L7:
            return -1;
            return 0;
        }

        public volatile int compare(Object obj, Object obj1)
        {
            return compare((DownloadTask)obj, (DownloadTask)obj1);
        }

        _cls7()
        {
            this$0 = DownloadSoundsListForAlbumFragment.this;
            orderType = i;
            super();
        }
    }

}
