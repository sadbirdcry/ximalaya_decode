// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadFragmentGroup, DownloadSoundsListFragment, DownloadAlbumListFragment, DownloadSoundsListForAlbumFragment

class this._cls0 extends BroadcastReceiver
{

    final DownloadFragmentGroup this$0;

    public void onReceive(Context context, Intent intent)
    {
        context = intent.getAction();
        Logger.log("test_sd", (new StringBuilder()).append("[BroadcastReceiver]====").append(context).toString());
        onSdcardStateChanged();
        context = DownloadFragmentGroup.access$400(DownloadFragmentGroup.this);
        if (context != null)
        {
            if (context instanceof DownloadSoundsListFragment)
            {
                context = (DownloadSoundsListFragment)context;
                if (context != null)
                {
                    context.onSdcardStateChanged();
                }
            } else
            if (context instanceof DownloadAlbumListFragment)
            {
                context = manager.findFragmentByTag("tag_download_album_sounds");
                if (context != null && (context instanceof DownloadSoundsListForAlbumFragment))
                {
                    context = (DownloadSoundsListForAlbumFragment)context;
                    if (context != null)
                    {
                        context.onSdcardStateChanged();
                        return;
                    }
                }
            }
        }
    }

    umFragment()
    {
        this$0 = DownloadFragmentGroup.this;
        super();
    }
}
