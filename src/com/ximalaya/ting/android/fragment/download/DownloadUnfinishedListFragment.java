// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.adapter.SoundsDownloadAdapter;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.List;

public class DownloadUnfinishedListFragment extends BaseFragment
    implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener, com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
{

    private TextView batchPauseTV;
    private TextView batchResumeTV;
    private TextView clearAllTV;
    private BounceListView downloadListView;
    private List downloadTaskList;
    public Activity mActivity;
    public Context mAppContext;
    private LinearLayout mEmptyView;
    private Handler mHandler;
    private SoundsDownloadAdapter soundsDownloadAdapter;

    public DownloadUnfinishedListFragment()
    {
        downloadTaskList = new ArrayList();
        batchPauseTV = null;
        batchResumeTV = null;
        clearAllTV = null;
        mHandler = new _cls2();
    }

    private void clearRef()
    {
        downloadListView.setAdapter(null);
        soundsDownloadAdapter = null;
        downloadListView = null;
    }

    private void doGetDownloadList()
    {
        if (getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            (new _cls1()).execute(new Void[0]);
            return;
        }
    }

    private View getItemView(long l)
    {
        int i = getItemViewPostion(l);
        if (i >= 0)
        {
            if ((i -= downloadListView.getFirstVisiblePosition() - downloadListView.getHeaderViewsCount()) >= 0 && i < downloadListView.getChildCount())
            {
                return downloadListView.getChildAt(i);
            }
        }
        return null;
    }

    private int getItemViewPostion(long l)
    {
        if (downloadListView == null || downloadTaskList == null || downloadTaskList.size() <= 0) goto _L2; else goto _L1
_L1:
        int i = downloadListView.getFirstVisiblePosition() - 1;
_L4:
        if (i >= downloadTaskList.size())
        {
            break; /* Loop/switch isn't completed */
        }
          goto _L3
_L6:
        i++;
        if (true) goto _L4; else goto _L2
_L3:
        if (i < 0 || l != ((DownloadTask)downloadTaskList.get(i)).trackId) goto _L6; else goto _L5
_L5:
        return i;
_L2:
        return -1;
    }

    private void initData()
    {
        if (!getUserVisibleHint() || getView() == null)
        {
            return;
        } else
        {
            doGetDownloadList();
            return;
        }
    }

    private void registerListener()
    {
        Object obj = LocalMediaService.getInstance();
        if (obj != null)
        {
            ((LocalMediaService) (obj)).setOnPlayServiceUpdateListener(this);
        }
        obj = DownloadHandler.getInstance(mAppContext);
        if (obj != null)
        {
            ((DownloadHandler) (obj)).setHandler(mHandler);
            ((DownloadHandler) (obj)).addDownloadListeners(this);
        }
    }

    private void showDialogOnDownloadFileMissAll()
    {
        if (getActivity() == null)
        {
            return;
        } else
        {
            (new DialogBuilder(getActivity())).setMessage(0x7f090182).setOkBtn(0x7f090185).showWarning();
            return;
        }
    }

    private void showDialogOnDownloadFileMissPartly()
    {
        if (getActivity() == null)
        {
            return;
        } else
        {
            (new DialogBuilder(getActivity())).setMessage(0x7f090183).setOkBtn(0x7f090185).showWarning();
            return;
        }
    }

    private void showDownloadFileCheckResult()
    {
        MyApplication.g;
        JVM INSTR tableswitch 1 2: default 24
    //                   1 29
    //                   2 36;
           goto _L1 _L2 _L3
_L1:
        MyApplication.g = -1;
        return;
_L2:
        showDialogOnDownloadFileMissPartly();
        continue; /* Loop/switch isn't completed */
_L3:
        showDialogOnDownloadFileMissAll();
        if (true) goto _L1; else goto _L4
_L4:
    }

    private void showEmptyView()
    {
        if (downloadTaskList.size() == 0)
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(0);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(0);
            mEmptyView.setVisibility(0);
            return;
        } else
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0232).setVisibility(8);
            mEmptyView.setVisibility(8);
            return;
        }
    }

    private void unRegisterListener()
    {
        Object obj = LocalMediaService.getInstance();
        if (obj != null)
        {
            ((LocalMediaService) (obj)).removeOnPlayServiceUpdateListener(this);
        }
        obj = DownloadHandler.getInstance(mAppContext);
        if (obj != null)
        {
            ((DownloadHandler) (obj)).setHandler(null);
            ((DownloadHandler) (obj)).removeDownloadListeners(this);
        }
    }

    private void updateBatchPauseButton()
    {
        while (MyApplication.b() == null || batchPauseTV == null) 
        {
            return;
        }
        DownloadHandler downloadhandler = DownloadHandler.getInstance(MyApplication.b());
        if (downloadhandler != null && downloadhandler.isDownloading())
        {
            batchPauseTV.setVisibility(0);
            return;
        } else
        {
            batchPauseTV.setVisibility(8);
            return;
        }
    }

    private void updateBatchResumeButton()
    {
        while (MyApplication.b() == null || batchResumeTV == null) 
        {
            return;
        }
        DownloadHandler downloadhandler = DownloadHandler.getInstance(MyApplication.b());
        if (downloadhandler != null && downloadhandler.hasUnfinishedTask() && !downloadhandler.isDownloading())
        {
            batchResumeTV.setVisibility(0);
            return;
        } else
        {
            batchResumeTV.setVisibility(8);
            return;
        }
    }

    private void updateClearAllButton()
    {
label0:
        {
            if (clearAllTV != null)
            {
                if (downloadTaskList == null || downloadTaskList.size() <= 0)
                {
                    break label0;
                }
                clearAllTV.setVisibility(0);
            }
            return;
        }
        clearAllTV.setVisibility(4);
    }

    private void updateDownloadingView(long l, int i, long l1, long l2)
    {
        this;
        JVM INSTR monitorenter ;
        Object obj = getItemView(l);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_187;
        }
        ProgressBar progressbar;
        TextView textview;
        ImageView imageview;
        progressbar = (ProgressBar)((View) (obj)).findViewById(0x7f0a0223);
        textview = (TextView)((View) (obj)).findViewById(0x7f0a0224);
        imageview = (ImageView)((View) (obj)).findViewById(0x7f0a0220);
        obj = (TextView)((View) (obj)).findViewById(0x7f0a0221);
        if (imageview == null)
        {
            break MISSING_BLOCK_LABEL_74;
        }
        imageview.setImageResource(0x7f0201f7);
        if (progressbar == null)
        {
            break MISSING_BLOCK_LABEL_85;
        }
        progressbar.setVisibility(0);
        if (textview == null)
        {
            break MISSING_BLOCK_LABEL_96;
        }
        textview.setVisibility(0);
        if (imageview == null)
        {
            break MISSING_BLOCK_LABEL_107;
        }
        imageview.setVisibility(0);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_126;
        }
        ((TextView) (obj)).setVisibility(0);
        ((TextView) (obj)).setText("\u6B63\u5728\u4E0B\u8F7D");
        if (progressbar == null)
        {
            break MISSING_BLOCK_LABEL_137;
        }
        progressbar.setProgress(i);
        if (textview == null)
        {
            break MISSING_BLOCK_LABEL_187;
        }
        textview.setText((new StringBuilder()).append(ToolUtil.toMBFormatString(l1)).append("M/").append(ToolUtil.toMBFormatString(l2)).append("M").toString());
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void delSound(long l)
    {
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        Logger.log("0814", "====DownloadSoundsListFragment========onActivityCreated ");
        registerListener();
        downloadListView.setOnItemClickListener(new _cls3());
        clearAllTV.setOnClickListener(new _cls4());
        batchResumeTV.setOnClickListener(new _cls5());
        batchPauseTV.setOnClickListener(new _cls6());
        downloadListView.setOnItemLongClickListener(new _cls7());
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (isAdded() && i == 1 && j == 1)
        {
            i = intent.getIntExtra("position", -1);
            long l = intent.getLongExtra("trackId", 0L);
            if (downloadTaskList != null && i < downloadTaskList.size() && i >= 0 && ((DownloadTask)downloadTaskList.get(i)).trackId == l)
            {
                ((DownloadTask)downloadTaskList.get(i)).isRelay = true;
                soundsDownloadAdapter.notifyDataSetChanged();
                return;
            }
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        Logger.log("0814", "====DownloadSoundsListFragment========onCreateView ");
        mActivity = getActivity();
        mAppContext = mActivity.getApplicationContext();
        viewgroup = layoutinflater.inflate(0x7f030079, viewgroup, false);
        downloadListView = (BounceListView)viewgroup.findViewById(0x7f0a0229);
        if (PackageUtil.isMeizu() && getActivity() != null)
        {
            downloadListView.setPadding(0, 0, 0, Utilities.dip2px(getActivity(), 70F));
        }
        downloadListView.setFooterDividersEnabled(false);
        soundsDownloadAdapter = new SoundsDownloadAdapter(downloadListView, getActivity(), downloadTaskList);
        soundsDownloadAdapter.setIsUnfinishDownload();
        bundle = LayoutInflater.from(mAppContext).inflate(0x7f030075, downloadListView, false);
        batchPauseTV = (TextView)bundle.findViewById(0x7f0a020f);
        batchResumeTV = (TextView)bundle.findViewById(0x7f0a0210);
        clearAllTV = (TextView)bundle.findViewById(0x7f0a0212);
        clearAllTV.setVisibility(4);
        downloadListView.addHeaderView(bundle);
        mEmptyView = (LinearLayout)layoutinflater.inflate(0x7f03007c, downloadListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u4F60\u6682\u65F6\u6CA1\u6709\u4E0B\u8F7D\u4EFB\u52A1\u54E6");
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setVisibility(8);
        ((Button)mEmptyView.findViewById(0x7f0a0232)).setVisibility(8);
        downloadListView.addFooterView(mEmptyView);
        downloadListView.setAdapter(soundsDownloadAdapter);
        if (downloadTaskList != null && !downloadTaskList.isEmpty())
        {
            updateListViewHeader();
        }
        showEmptyView();
        return viewgroup;
    }

    public void onDestroyView()
    {
        Logger.log("0814", "====DownloadSoundsListFragment========onDestroyView ");
        unRegisterListener();
        super.onDestroyView();
    }

    public void onPlayCanceled()
    {
    }

    public void onResume()
    {
        Logger.log("0814", "====DownloadSoundsListFragment========onResume ");
        super.onResume();
        showDownloadFileCheckResult();
    }

    public void onSoundChanged(int i)
    {
        if (soundsDownloadAdapter != null)
        {
            soundsDownloadAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (i < 0 || soundinfo == null || i >= downloadTaskList.size() || ((DownloadTask)downloadTaskList.get(i)).trackId != soundinfo.trackId || soundsDownloadAdapter == null)
        {
            return;
        } else
        {
            DownloadTask downloadtask = (DownloadTask)downloadTaskList.get(i);
            downloadtask.is_favorited = soundinfo.is_favorited;
            downloadtask.favorites_counts = soundinfo.favorites_counts;
            soundsDownloadAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void onStart()
    {
        super.onStart();
        initData();
    }

    public void onTaskComplete()
    {
        doGetDownloadList();
    }

    public void onTaskDelete()
    {
        doGetDownloadList();
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        initData();
    }

    public void updateActionInfo()
    {
        doGetDownloadList();
    }

    public void updateDownloadInfo(int i)
    {
        doGetDownloadList();
    }

    public void updateListView(int i, long l, int j, String s, long l1, 
            long l2)
    {
        Logger.log("dl_download", (new StringBuilder()).append("updateListView[").append(i).append(" | ").append(l).append(" | ").append(j).append(" | ").append(s).append("] ").toString(), true);
        j;
        JVM INSTR tableswitch 1 1: default 84
    //                   1 85;
           goto _L1 _L2
_L1:
        return;
_L2:
        if (i >= 0 && i <= 100)
        {
            updateDownloadingView(l, i, l1, l2);
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public void updateListViewHeader()
    {
        updateBatchPauseButton();
        updateBatchResumeButton();
        updateClearAllButton();
    }





    private class _cls2 extends Handler
    {

        final DownloadUnfinishedListFragment this$0;

        public void handleMessage(Message message)
        {
            showEmptyView();
            message.what;
            JVM INSTR tableswitch 2147422211 2147422214: default 40
        //                       2147422211 41
        //                       2147422212 40
        //                       2147422213 40
        //                       2147422214 81;
               goto _L1 _L2 _L1 _L1 _L3
_L1:
            return;
_L2:
            message = (PercentUpdatePacket)message.obj;
            updateListView(((PercentUpdatePacket) (message)).percentage, ((PercentUpdatePacket) (message)).trackId, ((PercentUpdatePacket) (message)).action, ((PercentUpdatePacket) (message)).name, ((PercentUpdatePacket) (message)).downloaded, ((PercentUpdatePacket) (message)).filesize);
            return;
_L3:
            if (message.obj instanceof PercentUpdatePacket)
            {
                message = (PercentUpdatePacket)message.obj;
                if (soundsDownloadAdapter != null && message != null)
                {
                    downloadTaskList.clear();
                    downloadTaskList.addAll(((PercentUpdatePacket) (message)).downloadList);
                    soundsDownloadAdapter.notifyDataSetChanged();
                }
                updateListViewHeader();
                return;
            }
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls2()
        {
            this$0 = DownloadUnfinishedListFragment.this;
            super();
        }
    }


    private class _cls1 extends AsyncTask
    {

        final DownloadUnfinishedListFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            return DownloadHandler.getInstance(mAppContext).getUnfinishedTasks();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (!isAdded())
            {
                return;
            }
            downloadTaskList.clear();
            if (list != null && list.size() > 0)
            {
                downloadTaskList.addAll(list);
            }
            soundsDownloadAdapter.notifyDataSetChanged();
            updateListViewHeader();
            showEmptyView();
        }

        _cls1()
        {
            this$0 = DownloadUnfinishedListFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final DownloadUnfinishedListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (downloadTaskList != null && downloadTaskList.size() != 0) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if ((i -= downloadListView.getHeaderViewsCount()) < 0 || i + 1 > downloadTaskList.size() || (adapterview = (DownloadTask)downloadTaskList.get(i)) == null)
            {
                continue; /* Loop/switch isn't completed */
            }
            if (((DownloadTask) (adapterview)).downloadStatus != 4)
            {
                break; /* Loop/switch isn't completed */
            }
            adapterview = ModelHelper.downloadlistToPlayList(adapterview, downloadTaskList);
            if (((PlaylistFromDownload) (adapterview)).index >= 0)
            {
                PlayTools.gotoPlayLocals(2, ((PlaylistFromDownload) (adapterview)).soundsList, ((PlaylistFromDownload) (adapterview)).index, getActivity(), DataCollectUtil.getDataFromView(view));
                return;
            }
            if (true) goto _L1; else goto _L3
_L3:
            if (((DownloadTask) (adapterview)).downloadStatus == 1)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:pause", true);
                DownloadHandler.getInstance(mAppContext).pauseDownload(adapterview);
                return;
            }
            if (((DownloadTask) (adapterview)).downloadStatus == 2)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:resume from pause", true);
                if (NetworkUtils.getNetType(getActivity()) == -1)
                {
                    Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                    return;
                } else
                {
                    view = DownLoadTools.getInstance();
                    view.resume(adapterview);
                    view.release();
                    return;
                }
            }
            if (((DownloadTask) (adapterview)).downloadStatus == 3)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:resume from failed", true);
                if (NetworkUtils.getNetType(getActivity()) == -1)
                {
                    Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                    return;
                } else
                {
                    view = DownLoadTools.getInstance();
                    view.resume(adapterview);
                    view.release();
                    return;
                }
            }
            if (((DownloadTask) (adapterview)).downloadStatus == 0)
            {
                Logger.log("dl_download", "[DOWNLOADING]--ItemClick--Action:start NOW from waiting", true);
                if (NetworkUtils.getNetType(getActivity()) == -1)
                {
                    Toast.makeText(getActivity(), "\u6CA1\u6709\u68C0\u6D4B\u5230\u53EF\u7528\u7F51\u7EDC\uFF0C\u8BF7\u8FDE\u63A5\u7F51\u7EDC\u518D\u8BD5", 0).show();
                    return;
                } else
                {
                    DownloadHandler.getInstance(mAppContext).startNow(adapterview);
                    return;
                }
            }
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls3()
        {
            this$0 = DownloadUnfinishedListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final DownloadUnfinishedListFragment this$0;

        public void onClick(View view)
        {
            class _cls1
                implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
            {

                final _cls4 this$1;

                public void onExecute()
                {
                    class _cls1 extends MyAsyncTask
                    {

                        ProgressDialog pd;
                        final _cls1 this$2;

                        protected transient Boolean doInBackground(Void avoid[])
                        {
                            DownloadHandler.getInstance(mAppContext).delAllIncompleteTasks();
                            return Boolean.valueOf(true);
                        }

                        protected volatile Object doInBackground(Object aobj[])
                        {
                            return doInBackground((Void[])aobj);
                        }

                        protected void onPostExecute(Boolean boolean1)
                        {
                            super.onPostExecute(boolean1);
                            if (pd != null)
                            {
                                pd.cancel();
                                pd = null;
                            }
                            if (boolean1.booleanValue())
                            {
                                if (soundsDownloadAdapter != null)
                                {
                                    soundsDownloadAdapter.getData().clear();
                                    soundsDownloadAdapter.notifyDataSetChanged();
                                }
                                downloadTaskList.clear();
                                updateListViewHeader();
                            }
                            showEmptyView();
                        }

                        protected volatile void onPostExecute(Object obj)
                        {
                            onPostExecute((Boolean)obj);
                        }

                        protected void onPreExecute()
                        {
                            super.onPreExecute();
                            pd = new MyProgressDialog(mActivity);
                            pd.show();
                            class _cls1
                                implements android.content.DialogInterface.OnKeyListener
                            {

                                final _cls1 this$3;

                                public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
                                {
                                    return true;
                                }

                                    _cls1()
                                    {
                                        this$3 = _cls1.this;
                                        super();
                                    }
                            }

                            pd.setOnKeyListener(new _cls1());
                            pd.setCanceledOnTouchOutside(false);
                            pd.setMessage("\u6B63\u5728\u6E05\u9664\u6240\u6709\u672A\u5B8C\u6210\u7684\u4E0B\u8F7D\u4EFB\u52A1\uFF0C\u8BF7\u7B49\u5F85...");
                            PlayListControl.getPlayListManager().doBeforeDelAllDownload();
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                                pd = null;
                            }
                    }

                    (new _cls1()).myexec(new Void[0]);
                }

                _cls1()
                {
                    this$1 = _cls4.this;
                    super();
                }
            }

            (new DialogBuilder(mActivity)).setMessage("\u786E\u5B9A\u6E05\u7A7A\u6240\u6709\u6B63\u5728\u4E0B\u8F7D\u7684\u4EFB\u52A1\uFF1F").setOkBtn(new _cls1()).showConfirm();
        }

        _cls4()
        {
            this$0 = DownloadUnfinishedListFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final DownloadUnfinishedListFragment this$0;

        public void onClick(View view)
        {
            view = DownLoadTools.getInstance();
            view.resumeAll();
            view.release();
            updateListViewHeader();
        }

        _cls5()
        {
            this$0 = DownloadUnfinishedListFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final DownloadUnfinishedListFragment this$0;

        public void onClick(View view)
        {
            class _cls1 extends MyAsyncTask
            {

                ProgressDialog pd;
                final _cls6 this$1;

                protected transient Boolean doInBackground(Void avoid[])
                {
                    DownloadHandler.getInstance(mAppContext).pauseAllDownload();
                    return Boolean.valueOf(true);
                }

                protected volatile Object doInBackground(Object aobj[])
                {
                    return doInBackground((Void[])aobj);
                }

                protected void onPostExecute(Boolean boolean1)
                {
                    super.onPostExecute(boolean1);
                    if (pd != null)
                    {
                        pd.cancel();
                        pd = null;
                    }
                    updateListViewHeader();
                }

                protected volatile void onPostExecute(Object obj)
                {
                    onPostExecute((Boolean)obj);
                }

                protected void onPreExecute()
                {
                    super.onPreExecute();
                    pd = new MyProgressDialog(mActivity);
                    class _cls1
                        implements android.content.DialogInterface.OnKeyListener
                    {

                        final _cls1 this$2;

                        public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
                        {
                            return true;
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    pd.setOnKeyListener(new _cls1());
                    pd.setCanceledOnTouchOutside(false);
                    pd.setMessage("\u4E00\u952E\u6682\u505C\u6240\u6709\u672A\u5B8C\u6210\u4EFB\u52A1\uFF0C\u8BF7\u7B49\u5F85...");
                    pd.show();
                }

                _cls1()
                {
                    this$1 = _cls6.this;
                    super();
                    pd = null;
                }
            }

            (new _cls1()).myexec(new Void[0]);
        }

        _cls6()
        {
            this$0 = DownloadUnfinishedListFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final DownloadUnfinishedListFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, final int posistion, long l)
        {
            while (posistion > downloadTaskList.size() || posistion == 0 || ((DownloadTask)downloadTaskList.get(posistion - 1)).downloadStatus >= 4) 
            {
                return false;
            }
            class _cls1
                implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
            {

                final _cls7 this$1;
                final int val$posistion;

                public void onExecute()
                {
                    if (posistion - 1 >= downloadTaskList.size())
                    {
                        return;
                    } else
                    {
                        class _cls1 extends MyAsyncTask
                        {

                            ProgressDialog pd;
                            final _cls1 this$2;

                            protected transient Boolean doInBackground(Void avoid[])
                            {
                                boolean flag;
                                if (DownloadHandler.getInstance(mAppContext).delDownloadTask((DownloadTask)downloadTaskList.get(posistion - 1)) == 0)
                                {
                                    flag = true;
                                } else
                                {
                                    flag = false;
                                }
                                return Boolean.valueOf(flag);
                            }

                            protected volatile Object doInBackground(Object aobj[])
                            {
                                return doInBackground((Void[])aobj);
                            }

                            protected void onPostExecute(Boolean boolean1)
                            {
                                if (pd != null)
                                {
                                    pd.cancel();
                                    pd = null;
                                }
                                if (boolean1.booleanValue())
                                {
                                    updateListViewHeader();
                                }
                                showEmptyView();
                            }

                            protected volatile void onPostExecute(Object obj)
                            {
                                onPostExecute((Boolean)obj);
                            }

                            protected void onPreExecute()
                            {
                                super.onPreExecute();
                                pd = new MyProgressDialog(mActivity);
                                pd.setMessage("\u6B63\u5728\u6E05\u9664\u4E0B\u8F7D\u5217\u8868\uFF0C\u8BF7\u7B49\u5F85...");
                                pd.show();
                                PlayListControl.getPlayListManager().doBeforeDelete((SoundInfo)downloadTaskList.get(posistion - 1));
                            }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                                pd = null;
                            }
                        }

                        (new _cls1()).myexec(new Void[0]);
                        return;
                    }
                }

                _cls1()
                {
                    this$1 = _cls7.this;
                    posistion = i;
                    super();
                }
            }

            (new DialogBuilder(mActivity)).setMessage("\u662F\u5426\u786E\u5B9A\u5220\u9664\u8BE5\u58F0\u97F3?").setOkBtn(new _cls1()).showConfirm();
            return false;
        }

        _cls7()
        {
            this$0 = DownloadUnfinishedListFragment.this;
            super();
        }
    }

}
