// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.view.listview.DragSortController;
import com.ximalaya.ting.android.library.view.listview.DragSortListView;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.SlideRightOutView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadSoundsListFragment

public class SortDownloadSoundFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener, com.ximalaya.ting.android.library.view.listview.DragSortListView.DropListener
{
    private static class DataLoader extends MyAsyncTaskLoader
    {

        private ArrayList mData;

        public volatile void deliverResult(Object obj)
        {
            deliverResult((ArrayList)obj);
        }

        public void deliverResult(ArrayList arraylist)
        {
            super.deliverResult(arraylist);
            mData = arraylist;
        }

        public volatile Object loadInBackground()
        {
            return loadInBackground();
        }

        public ArrayList loadInBackground()
        {
            ArrayList arraylist = DownloadHandler.getInstance(getContext()).getFinishedTasks();
            Collections.sort(arraylist, SortDownloadSoundFragment.DATA_COMPARATOR);
            return arraylist;
        }

        protected void onStartLoading()
        {
            super.onStartLoading();
            if (mData != null)
            {
                deliverResult(mData);
                return;
            } else
            {
                forceLoad();
                return;
            }
        }

        public DataLoader(Context context)
        {
            super(context);
        }
    }

    private static class ListAdapter extends BaseAdapter
    {

        private List mData;
        private LayoutInflater mInflater;

        public void drop(int i, int j)
        {
            DownloadTask downloadtask = (DownloadTask)mData.remove(i);
            mData.add(j, downloadtask);
            notifyDataSetChanged();
        }

        public int getCount()
        {
            if (mData == null)
            {
                return 0;
            } else
            {
                return mData.size();
            }
        }

        public List getData()
        {
            return mData;
        }

        public Object getItem(int i)
        {
            return mData.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            DownloadTask downloadtask;
            if (view == null)
            {
                viewgroup = new ViewHolder();
                view = mInflater.inflate(0x7f03013a, null);
                viewgroup.mTitle = (TextView)view.findViewById(0x1020014);
                viewgroup.mOwner = (TextView)view.findViewById(0x1020015);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (ViewHolder)view.getTag();
            }
            downloadtask = (DownloadTask)mData.get(i);
            ((ViewHolder) (viewgroup)).mTitle.setText(downloadtask.title);
            ((ViewHolder) (viewgroup)).mOwner.setText(downloadtask.nickname);
            return view;
        }

        public ListAdapter(Context context, List list)
        {
            mData = list;
            mInflater = LayoutInflater.from(context);
        }
    }

    class ListAdapter.ViewHolder
    {

        TextView mOwner;
        TextView mTitle;
        final ListAdapter this$0;

        ListAdapter.ViewHolder()
        {
            this$0 = ListAdapter.this;
            super();
        }
    }


    private static final Comparator DATA_COMPARATOR = new _cls1();
    private ListAdapter mAdapter;
    private boolean mApplyChange;
    private ImageView mBackBtn;
    private DragSortListView mListView;
    private SlideRightOutView mSlideView;
    private TextView mTitleTextView;

    public SortDownloadSoundFragment()
    {
        mApplyChange = false;
    }

    private void applyChange()
    {
        List list = mAdapter.getData();
        for (int i = 0; i < list.size(); i++)
        {
            ((DownloadTask)list.get(i)).orderPositon = i;
        }

    }

    private void exit(boolean flag)
    {
        mApplyChange = flag;
        getActivity().getSupportFragmentManager().popBackStack();
    }

    public static SortDownloadSoundFragment getInstance()
    {
        return new SortDownloadSoundFragment();
    }

    public void drop(int i, int j)
    {
        if (mAdapter != null && i != j)
        {
            mAdapter.drop(i, j);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mListView.setDropListener(this);
        getLoaderManager().initLoader(1, null, this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new _cls2());
        mBackBtn.setOnClickListener(this);
        findViewById(0x7f0a020c).setOnClickListener(this);
        findViewById(0x7f0a02e1).setOnClickListener(this);
        mSlideView.setOnFinishListener(new _cls3());
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            return;

        case 2131362529: 
            exit(true);
            return;

        case 2131361915: 
        case 2131362316: 
            exit(false);
            return;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        if (ToolUtil.isUseSmartbarAsTab())
        {
            setHasOptionsMenu(true);
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        return new DataLoader(getActivity());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300d7, viewgroup, false);
        mSlideView = (SlideRightOutView)layoutinflater.findViewById(0x7f0a0055);
        mListView = (DragSortListView)layoutinflater.findViewById(0x102000a);
        mListView.setDragEnabled(true);
        viewgroup = new DragSortController(mListView);
        viewgroup.setDragInitMode(2);
        viewgroup.setDragHandleId(0x7f0a00f1);
        viewgroup.setSortEnabled(true);
        mListView.setFloatViewManager(viewgroup);
        mListView.setOnTouchListener(viewgroup);
        mBackBtn = (ImageView)layoutinflater.findViewById(0x7f0a007b);
        mTitleTextView = (TextView)layoutinflater.findViewById(0x7f0a00ae);
        mTitleTextView.setText(0x7f0901e2);
        return layoutinflater;
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (mApplyChange)
        {
            applyChange();
            android.support.v4.app.Fragment fragment = getTargetFragment();
            if (fragment instanceof DownloadSoundsListFragment)
            {
                ((DownloadSoundsListFragment)fragment).notifyDataSetChanged();
            }
            if (mAdapter != null)
            {
                (new _cls4()).execute(new Void[0]);
            }
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (ArrayList)obj);
    }

    public void onLoadFinished(Loader loader, ArrayList arraylist)
    {
        mAdapter = new ListAdapter(getActivity(), arraylist);
        mListView.setAdapter(mAdapter);
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onPrepareOptionsMenu(Menu menu)
    {
        super.onPrepareOptionsMenu(menu);
        if (ToolUtil.isUseSmartbarAsTab())
        {
            getActivity().getActionBar().removeAllTabs();
        }
    }





    private class _cls2
        implements android.view.View.OnKeyListener
    {

        final SortDownloadSoundFragment this$0;

        public boolean onKey(View view, int i, KeyEvent keyevent)
        {
            if (i == 4)
            {
                getActivity().getSupportFragmentManager().popBackStack();
                return true;
            } else
            {
                return false;
            }
        }

        _cls2()
        {
            this$0 = SortDownloadSoundFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.SlideRightOutView.OnFinishListener
    {

        final SortDownloadSoundFragment this$0;

        public boolean onFinish()
        {
            exit(false);
            return true;
        }

        _cls3()
        {
            this$0 = SortDownloadSoundFragment.this;
            super();
        }
    }


    private class _cls4 extends AsyncTask
    {

        final SortDownloadSoundFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            (new DownloadTableHandler(MyApplication.b())).updateOrderPosition(mAdapter.getData());
            return null;
        }

        _cls4()
        {
            this$0 = SortDownloadSoundFragment.this;
            super();
        }
    }


    private class _cls1
        implements Comparator
    {

        public int compare(DownloadTask downloadtask, DownloadTask downloadtask1)
        {
            return downloadtask.orderPositon - downloadtask1.orderPositon;
        }

        public volatile int compare(Object obj, Object obj1)
        {
            return compare((DownloadTask)obj, (DownloadTask)obj1);
        }

        _cls1()
        {
        }
    }

}
