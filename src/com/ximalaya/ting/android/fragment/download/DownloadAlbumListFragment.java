// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.SoundsDownloadForAlbumAdapter;
import com.ximalaya.ting.android.fragment.BaseActionInterface;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.List;

public class DownloadAlbumListFragment extends BaseFragment
    implements BaseActionInterface, DownloadSoundsListForAlbumFragment.RemoveSounds, com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
{

    public static final int MAX_ALBUM_COUNT_SHOULD_SHOW_RECOMMEND = 4;
    private TextView clearAllTV;
    public BounceListView downloadListView;
    private List downloadTaskList;
    public Activity mActivity;
    public Context mAppContext;
    private LinearLayout mEmptyView;
    private FrameLayout mRecommendEntry;
    public FragmentManager manager;
    private SoundsDownloadForAlbumAdapter soundsDownloadAdapter;

    public DownloadAlbumListFragment()
    {
        downloadTaskList = new ArrayList();
        clearAllTV = null;
    }

    private void addRecommendFooterView(LayoutInflater layoutinflater)
    {
        layoutinflater = layoutinflater.inflate(0x7f030111, null);
        mRecommendEntry = new FrameLayout(getActivity());
        Object obj = new android.widget.FrameLayout.LayoutParams(-1, -2);
        mRecommendEntry.addView(layoutinflater, ((android.view.ViewGroup.LayoutParams) (obj)));
        layoutinflater.findViewById(0x7f0a00a8).setVisibility(8);
        int i = Utilities.dip2px(getActivity(), 10F);
        mRecommendEntry.setPadding(0, 0, 0, i);
        obj = (TextView)layoutinflater.findViewById(0x7f0a00ea);
        ((TextView) (obj)).setText("\u70ED\u95E8\u4E0B\u8F7D\u63A8\u8350");
        ((TextView) (obj)).setCompoundDrawablesWithIntrinsicBounds(0x7f0202ab, 0, 0, 0);
        ((TextView)layoutinflater.findViewById(0x7f0a02aa)).setText("\u6BCF\u65E5\u66F4\u65B0");
        ((ImageView)layoutinflater.findViewById(0x7f0a0023)).setImageResource(0x7f0201fb);
        ((ImageView)layoutinflater.findViewById(0x7f0a045f)).setImageResource(0x7f020041);
        layoutinflater.findViewById(0x7f0a0460).setVisibility(8);
        layoutinflater = layoutinflater.findViewById(0x7f0a00a8);
        layoutinflater.setVisibility(4);
        obj = (android.widget.RelativeLayout.LayoutParams)layoutinflater.getLayoutParams();
        obj.topMargin = Utilities.dip2px(mActivity, 10F);
        layoutinflater.setLayoutParams(((android.view.ViewGroup.LayoutParams) (obj)));
        downloadListView.addFooterView(mRecommendEntry);
        if (downloadTaskList != null && downloadTaskList.size() > 4)
        {
            mRecommendEntry.setVisibility(8);
        }
    }

    private void clearRef()
    {
        downloadListView.setAdapter(null);
        soundsDownloadAdapter = null;
        downloadListView = null;
    }

    private void doGetDownloadList()
    {
        if (getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            (new _cls1()).execute(new Void[0]);
            return;
        }
    }

    private void initData()
    {
        if (!getUserVisibleHint() || getView() == null)
        {
            return;
        } else
        {
            doGetDownloadList();
            return;
        }
    }

    public static DownloadAlbumListFragment newInstance(FragmentManager fragmentmanager)
    {
        DownloadAlbumListFragment downloadalbumlistfragment = new DownloadAlbumListFragment();
        downloadalbumlistfragment.setFragmentManager(fragmentmanager);
        return downloadalbumlistfragment;
    }

    private void registerListener()
    {
        DownloadHandler downloadhandler = DownloadHandler.getInstance(mAppContext);
        if (downloadhandler != null)
        {
            downloadhandler.addDownloadListeners(this);
        }
    }

    private void showEmptyView()
    {
        if (downloadTaskList.size() == 0)
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(0);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(0);
            mEmptyView.setVisibility(0);
            return;
        } else
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0232).setVisibility(8);
            mEmptyView.setVisibility(8);
            return;
        }
    }

    private void unRegisterListener()
    {
        DownloadHandler downloadhandler = DownloadHandler.getInstance(mAppContext);
        if (downloadhandler != null)
        {
            downloadhandler.removeDownloadListeners(this);
        }
    }

    private void updateClearAllButton()
    {
label0:
        {
            if (clearAllTV != null)
            {
                if (downloadTaskList == null || downloadTaskList.size() <= 0)
                {
                    break label0;
                }
                clearAllTV.setVisibility(0);
            }
            return;
        }
        clearAllTV.setVisibility(4);
    }

    public void delSound(long l)
    {
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        registerListener();
        downloadListView.setOnItemClickListener(new _cls2());
        clearAllTV.setOnClickListener(new _cls3());
        mRecommendEntry.setOnClickListener(new _cls4());
        initData();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mActivity = getActivity();
        mAppContext = mActivity.getApplicationContext();
        viewgroup = layoutinflater.inflate(0x7f030079, viewgroup, false);
        downloadListView = (BounceListView)viewgroup.findViewById(0x7f0a0229);
        if (PackageUtil.isMeizu() && getActivity() != null)
        {
            downloadListView.setPadding(0, 0, 0, Utilities.dip2px(getActivity(), 70F));
        }
        soundsDownloadAdapter = new SoundsDownloadForAlbumAdapter(getActivity(), downloadTaskList);
        clearAllTV = (TextView)LayoutInflater.from(mAppContext).inflate(0x7f030076, downloadListView, false).findViewById(0x7f0a0212);
        addRecommendFooterView(layoutinflater);
        mEmptyView = (LinearLayout)layoutinflater.inflate(0x7f03007c, downloadListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u4E0B\u8F7D\u8FC7\u4E13\u8F91\u54E6");
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setVisibility(8);
        ((Button)mEmptyView.findViewById(0x7f0a0232)).setVisibility(8);
        downloadListView.addFooterView(mEmptyView);
        downloadListView.setAdapter(soundsDownloadAdapter);
        updateClearAllButton();
        showEmptyView();
        return viewgroup;
    }

    public void onDestroyView()
    {
        unRegisterListener();
        super.onDestroyView();
    }

    public void onResume()
    {
        super.onResume();
    }

    public void onTaskComplete()
    {
        if (!isAdded() || getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            doGetDownloadList();
            return;
        }
    }

    public void onTaskDelete()
    {
        if (!isAdded() || getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            doGetDownloadList();
            return;
        }
    }

    public void pushFragments(Fragment fragment)
    {
        pushFragments(fragment, null);
    }

    public void pushFragments(Fragment fragment, String s)
    {
        FragmentTransaction fragmenttransaction = manager.beginTransaction();
        fragmenttransaction.add(0x7f0a016d, fragment, s);
        fragmenttransaction.addToBackStack(null);
        fragmenttransaction.commitAllowingStateLoss();
    }

    public void remove()
    {
        doGetDownloadList();
    }

    public void setFragmentManager(FragmentManager fragmentmanager)
    {
        manager = fragmentmanager;
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        initData();
    }

    public void updateActionInfo()
    {
        doGetDownloadList();
    }

    public void updateDownloadInfo(int i)
    {
        doGetDownloadList();
    }







    private class _cls1 extends AsyncTask
    {

        final DownloadAlbumListFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            return DownloadHandler.getInstance(mAppContext).getSortedFinishedDownloadList();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (!isAdded())
            {
                return;
            }
            if (list != null)
            {
                downloadTaskList.clear();
                downloadTaskList.addAll(list);
                soundsDownloadAdapter.setList(downloadTaskList);
                if (soundsDownloadAdapter.getCount() <= 4)
                {
                    mRecommendEntry.setVisibility(0);
                } else
                {
                    mRecommendEntry.setVisibility(8);
                }
                soundsDownloadAdapter.notifyDataSetChanged();
            }
            updateClearAllButton();
            showEmptyView();
        }

        _cls1()
        {
            this$0 = DownloadAlbumListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final DownloadAlbumListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (downloadTaskList != null && downloadTaskList.size() != 0)
            {
                if ((i -= downloadListView.getHeaderViewsCount()) >= 0 && i < soundsDownloadAdapter.getCount())
                {
                    l = ((Long)soundsDownloadAdapter.mapKey.get(i)).longValue();
                    pushFragments(new DownloadSoundsListForAlbumFragment(l, "albumId", DownloadAlbumListFragment.this), "tag_download_album_sounds");
                    return;
                }
            }
        }

        _cls2()
        {
            this$0 = DownloadAlbumListFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final DownloadAlbumListFragment this$0;

        public void onClick(View view)
        {
            ToolUtil.onEvent(getActivity(), "Down_sound_Deleteall");
            class _cls1
                implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
            {

                final _cls3 this$1;

                public void onExecute()
                {
                    class _cls1 extends MyAsyncTask
                    {

                        ProgressDialog pd;
                        final _cls1 this$2;

                        protected transient Boolean doInBackground(Void avoid[])
                        {
                            DownloadHandler.getInstance(mAppContext).delAllCompleteTasks();
                            return Boolean.valueOf(true);
                        }

                        protected volatile Object doInBackground(Object aobj[])
                        {
                            return doInBackground((Void[])aobj);
                        }

                        protected void onPostExecute(Boolean boolean1)
                        {
                            super.onPostExecute(boolean1);
                            if (pd != null)
                            {
                                pd.cancel();
                                pd = null;
                            }
                            if (boolean1.booleanValue())
                            {
                                if (soundsDownloadAdapter != null)
                                {
                                    downloadTaskList.clear();
                                    soundsDownloadAdapter.notifyDataSetChanged();
                                }
                                downloadTaskList.clear();
                                clearAllTV.setVisibility(4);
                            }
                            showEmptyView();
                        }

                        protected volatile void onPostExecute(Object obj)
                        {
                            onPostExecute((Boolean)obj);
                        }

                        protected void onPreExecute()
                        {
                            super.onPreExecute();
                            pd = new MyProgressDialog(mActivity);
                            pd.show();
                            class _cls1
                                implements android.content.DialogInterface.OnKeyListener
                            {

                                final _cls1 this$3;

                                public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
                                {
                                    return true;
                                }

                                    _cls1()
                                    {
                                        this$3 = _cls1.this;
                                        super();
                                    }
                            }

                            pd.setOnKeyListener(new _cls1());
                            pd.setCanceledOnTouchOutside(false);
                            pd.setMessage("\u6B63\u5728\u6E05\u9664\u5DF2\u4E0B\u8F7D\u4E13\u8F91\uFF0C\u8BF7\u7B49\u5F85...");
                            PlayListControl.getPlayListManager().doBeforeDelAllDownload();
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                                pd = null;
                            }
                    }

                    (new _cls1()).myexec(new Void[0]);
                }

                _cls1()
                {
                    this$1 = _cls3.this;
                    super();
                }
            }

            (new DialogBuilder(mActivity)).setMessage("\u786E\u5B9A\u6E05\u7A7A\u6240\u6709\u4E0B\u8F7D\u7684\u4E13\u8F91\uFF1F").setOkBtn(new _cls1()).showConfirm();
        }

        _cls3()
        {
            this$0 = DownloadAlbumListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final DownloadAlbumListFragment this$0;

        public void onClick(View view)
        {
            Bundle bundle = new Bundle();
            bundle.putString("categoryId", "0");
            bundle.putString("sort_by", "classic");
            bundle.putInt("from", 4);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/finding2/category/CategoryDetailFragment, bundle);
        }

        _cls4()
        {
            this$0 = DownloadAlbumListFragment.this;
            super();
        }
    }

}
