// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.os.AsyncTask;
import android.widget.FrameLayout;
import com.ximalaya.ting.android.adapter.SoundsDownloadForAlbumAdapter;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadAlbumListFragment

class this._cls0 extends AsyncTask
{

    final DownloadAlbumListFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        return DownloadHandler.getInstance(mAppContext).getSortedFinishedDownloadList();
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        if (!isAdded())
        {
            return;
        }
        if (list != null)
        {
            DownloadAlbumListFragment.access$000(DownloadAlbumListFragment.this).clear();
            DownloadAlbumListFragment.access$000(DownloadAlbumListFragment.this).addAll(list);
            DownloadAlbumListFragment.access$100(DownloadAlbumListFragment.this).setList(DownloadAlbumListFragment.access$000(DownloadAlbumListFragment.this));
            if (DownloadAlbumListFragment.access$100(DownloadAlbumListFragment.this).getCount() <= 4)
            {
                DownloadAlbumListFragment.access$200(DownloadAlbumListFragment.this).setVisibility(0);
            } else
            {
                DownloadAlbumListFragment.access$200(DownloadAlbumListFragment.this).setVisibility(8);
            }
            DownloadAlbumListFragment.access$100(DownloadAlbumListFragment.this).notifyDataSetChanged();
        }
        DownloadAlbumListFragment.access$300(DownloadAlbumListFragment.this);
        DownloadAlbumListFragment.access$400(DownloadAlbumListFragment.this);
    }

    ()
    {
        this$0 = DownloadAlbumListFragment.this;
        super();
    }
}
