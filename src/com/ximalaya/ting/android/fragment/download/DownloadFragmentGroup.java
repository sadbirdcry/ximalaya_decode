// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadAlbumListFragment, DownloadSoundsListFragment, DownloadUnfinishedListFragment

public class DownloadFragmentGroup extends BaseFragment
    implements android.support.v4.view.ViewPager.OnPageChangeListener, com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
{
    class TabAdapter extends FragmentPagerAdapter
        implements com.astuetz.PagerSlidingTabStrip.c
    {

        final DownloadFragmentGroup this$0;
        private List tips;

        public int getCount()
        {
            return mTabTitles.length;
        }

        public Fragment getCurrentFragment(ViewPager viewpager)
        {
            Method method = getClass().getSuperclass().getDeclaredMethod("makeFragmentName", new Class[] {
                Integer.TYPE, Long.TYPE
            });
            Object obj = getClass().getSuperclass().getDeclaredField("mFragmentManager");
            ((Field) (obj)).setAccessible(true);
            obj = (FragmentManager)((Field) (obj)).get(this);
            method.setAccessible(true);
            viewpager = ((FragmentManager) (obj)).findFragmentByTag((String)method.invoke(null, new Object[] {
                Integer.valueOf(viewpager.getId()), Long.valueOf(viewpager.getCurrentItem())
            }));
            return viewpager;
            viewpager;
            viewpager.printStackTrace();
_L2:
            return null;
            viewpager;
            viewpager.printStackTrace();
            continue; /* Loop/switch isn't completed */
            viewpager;
            viewpager.printStackTrace();
            continue; /* Loop/switch isn't completed */
            viewpager;
            viewpager.printStackTrace();
            continue; /* Loop/switch isn't completed */
            viewpager;
            viewpager.printStackTrace();
            if (true) goto _L2; else goto _L1
_L1:
        }

        public Fragment getItem(int i)
        {
            switch (i)
            {
            default:
                return null;

            case 0: // '\0'
                return DownloadAlbumListFragment.newInstance(manager);

            case 1: // '\001'
                return new DownloadSoundsListFragment();

            case 2: // '\002'
                return new DownloadUnfinishedListFragment();
            }
        }

        public CharSequence getPageTitle(int i)
        {
            return mTabTitles[i];
        }

        public View getTabWidget(int i)
        {
            View view = LayoutInflater.from(mCon).inflate(0x7f0301da, null);
            view.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(ToolUtil.getScreenWidth(mCon) / 3, ToolUtil.dp2px(mCon, 45F)));
            ((TextView)view.findViewById(0x7f0a06f1)).setText(getPageTitle(i));
            TextView textview = (TextView)view.findViewById(0x7f0a06f2);
            tips.add(textview);
            return view;
        }

        public void setTip(int i, int j)
        {
label0:
            {
                if (tips.size() > i)
                {
                    if (j <= 0)
                    {
                        break label0;
                    }
                    ((TextView)tips.get(i)).setVisibility(0);
                    TextView textview = (TextView)tips.get(i);
                    String s;
                    if (j >= 99)
                    {
                        s = "N";
                    } else
                    {
                        s = (new StringBuilder()).append(j).append("").toString();
                    }
                    textview.setText(s);
                }
                return;
            }
            ((TextView)tips.get(i)).setVisibility(8);
        }

        public TabAdapter(FragmentManager fragmentmanager)
        {
            this$0 = DownloadFragmentGroup.this;
            super(fragmentmanager);
            tips = new ArrayList();
        }
    }


    public boolean isSDcardListenerRegisted;
    private TabAdapter mAdapter;
    private BroadcastReceiver mBroadcastReceiver;
    private ViewPager mPager;
    private String mTabTitles[] = {
        "\u4E13\u8F91", "\u58F0\u97F3", "\u4E0B\u8F7D\u4E2D"
    };
    private PagerSlidingTabStrip mTabs;
    public FragmentManager manager;
    private TextView spaceOccupationTV;
    private ProgressBar spcaceOcupationPB;

    public DownloadFragmentGroup()
    {
        mBroadcastReceiver = new _cls5();
    }

    public DownloadFragmentGroup(FragmentManager fragmentmanager)
    {
        mBroadcastReceiver = new _cls5();
        manager = fragmentmanager;
    }

    private Fragment getCurrentFragment()
    {
        if (mAdapter == null)
        {
            return null;
        } else
        {
            return mAdapter.getCurrentFragment(mPager);
        }
    }

    private void initIncompleteTaskNum()
    {
        DownloadHandler downloadhandler = DownloadHandler.getInstance(getActivity());
        if (downloadhandler != null)
        {
            updateIncompleteTaskNum(downloadhandler.getIncompleteTaskCount());
        }
    }

    private void initTabs()
    {
        mTabs.setDividerColor(0);
        mTabs.setIndicatorColor(Color.parseColor("#f86442"));
        mTabs.setUnderlineColor(Color.parseColor("#f6f6f7"));
        mTabs.setUnderlineHeight(0);
        mTabs.setDividerPadding(0);
        mTabs.setDeactivateTextColor(Color.parseColor("#666666"));
        mTabs.setActivateTextColor(Color.parseColor("#f86442"));
        mTabs.setTabSwitch(true);
        mTabs.setTextSize(15);
        mTabs.setTextColorResource(0x7f0700ce);
        mTabs.setIndicatorHeight(Utilities.dip2px(getActivity(), 3F));
        mTabs.setTabPaddingLeftRight(Utilities.dip2px(getActivity(), 0.0F));
        mAdapter = new TabAdapter(manager);
        mPager.setAdapter(mAdapter);
        mTabs.setViewPager(mPager);
        initIncompleteTaskNum();
        updateSpaceOccupationView();
        registerListener();
        mTabs.a(0);
        mTabs.setOnPageChangeListener(this);
    }

    private void registerListener()
    {
        DownloadHandler downloadhandler = DownloadHandler.getInstance(getActivity());
        if (downloadhandler != null)
        {
            downloadhandler.addDownloadListeners(this);
        }
        registerSDcardListener();
    }

    private void unRegisterListener()
    {
        DownloadHandler downloadhandler = DownloadHandler.getInstance(getActivity());
        if (downloadhandler != null)
        {
            downloadhandler.removeDownloadListeners(this);
        }
        unregisterSDcardListener(mBroadcastReceiver);
    }

    private void updateIncompleteTaskNum(int i)
    {
        if (mAdapter != null)
        {
            mAdapter.setTip(2, i);
        }
    }

    private void updateSpaceOccupationView()
    {
        (new Thread(new _cls4())).start();
    }

    private void updateSpaceOccupyProgress(int i, int j)
    {
        if (spaceOccupationTV != null && isAdded())
        {
            int k = 0;
            if (i + j > 0)
            {
                k = (i * 1000) / (i + j);
            }
            spcaceOcupationPB.setMax(1000);
            spcaceOcupationPB.setProgress(k);
        }
    }

    private void updateSpaceOccupyText(int i, int j)
    {
        if (spaceOccupationTV != null && isAdded())
        {
            String s;
            String s1;
            if (i >= 1024)
            {
                s = ToolUtil.formatNumber(1, (float)i / 1024F) + "G";
            } else
            {
                s = i + "M";
            }
            if (j >= 1024)
            {
                s1 = ToolUtil.formatNumber(1, (float)j / 1024F) + "G";
            } else
            {
                s1 = j + "M";
            }
            s = "\u5DF2\u5360\u7528" + s + ", \u53EF\u7528\u7A7A\u95F4" + s1;
            spaceOccupationTV.setText(s);
            spaceOccupationTV.setContentDescription(s);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initTabs();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (getCurrentFragment() == null)
        {
            return;
        } else
        {
            getCurrentFragment().onActivityResult(i, j, intent);
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0301de, viewgroup, false);
        mTabs = (PagerSlidingTabStrip)layoutinflater.findViewById(0x7f0a02dc);
        mPager = (ViewPager)layoutinflater.findViewById(0x7f0a017a);
        spaceOccupationTV = (TextView)layoutinflater.findViewById(0x7f0a06f9);
        spcaceOcupationPB = (ProgressBar)layoutinflater.findViewById(0x7f0a06f8);
        return layoutinflater;
    }

    public void onDestroyView()
    {
        unRegisterListener();
        super.onDestroyView();
    }

    public void onPageScrollStateChanged(int i)
    {
    }

    public void onPageScrolled(int i, float f, int j)
    {
    }

    public void onPageSelected(int i)
    {
        Logger.log((new StringBuilder()).append("onPageSelected position:").append(i).toString());
        if (i == 0)
        {
            Fragment fragment = getCurrentFragment();
            if (fragment != null && (fragment instanceof DownloadAlbumListFragment))
            {
                ((DownloadAlbumListFragment)fragment).manager = manager;
            }
        }
    }

    public void onSdcardStateChanged()
    {
        updateSpaceOccupationView();
    }

    public void onTaskComplete()
    {
        if (!isAdded() || getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            getActivity().runOnUiThread(new _cls1());
            return;
        }
    }

    public void onTaskDelete()
    {
        if (!isAdded() || getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            getActivity().runOnUiThread(new _cls2());
            return;
        }
    }

    public final void registerSDcardListener()
    {
        if (isSDcardListenerRegisted || getActivity() == null)
        {
            break MISSING_BLOCK_LABEL_162;
        }
        this;
        JVM INSTR monitorenter ;
        if (!isSDcardListenerRegisted && getActivity() != null)
        {
            IntentFilter intentfilter = new IntentFilter();
            intentfilter.addAction("android.intent.action.MEDIA_BAD_REMOVAL");
            intentfilter.addAction("android.intent.action.MEDIA_BUTTON");
            intentfilter.addAction("android.intent.action.MEDIA_CHECKING");
            intentfilter.addAction("android.intent.action.MEDIA_EJECT");
            intentfilter.addAction("android.intent.action.MEDIA_MOUNTED");
            intentfilter.addAction("android.intent.action.MEDIA_NOFS");
            intentfilter.addAction("android.intent.action.MEDIA_REMOVED");
            intentfilter.addAction("android.intent.action.MEDIA_SCANNER_FINISHED");
            intentfilter.addAction("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
            intentfilter.addAction("android.intent.action.MEDIA_SCANNER_STARTED");
            intentfilter.addAction("android.intent.action.MEDIA_SHARED");
            intentfilter.addAction("android.intent.action.MEDIA_UNMOUNTABLE");
            intentfilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
            intentfilter.addDataScheme("file");
            getActivity().registerReceiver(mBroadcastReceiver, intentfilter);
            isSDcardListenerRegisted = true;
        }
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void setCurrentFragment(int i)
    {
        if (mPager != null)
        {
            mPager.setCurrentItem(i, true);
        }
    }

    public void setCurrentFragment(int i, boolean flag)
    {
        if (manager != null)
        {
            Fragment fragment = manager.findFragmentByTag("tag_download_album_sounds");
            if (fragment != null)
            {
                FragmentTransaction fragmenttransaction = manager.beginTransaction();
                fragmenttransaction.remove(fragment);
                fragmenttransaction.commitAllowingStateLoss();
            }
        }
        if (mPager != null)
        {
            mPager.setCurrentItem(i, flag);
        }
    }

    public final void unregisterSDcardListener(BroadcastReceiver broadcastreceiver)
    {
        if (!isSDcardListenerRegisted || getActivity() == null)
        {
            break MISSING_BLOCK_LABEL_54;
        }
        this;
        JVM INSTR monitorenter ;
        if (isSDcardListenerRegisted && getActivity() != null)
        {
            getActivity().unregisterReceiver(mBroadcastReceiver);
            isSDcardListenerRegisted = false;
        }
        this;
        JVM INSTR monitorexit ;
        return;
        broadcastreceiver;
        this;
        JVM INSTR monitorexit ;
        throw broadcastreceiver;
    }

    public void updateActionInfo()
    {
    }

    public void updateDownloadInfo(final int contents)
    {
        if (!isAdded())
        {
            return;
        } else
        {
            getActivity().runOnUiThread(new _cls3());
            return;
        }
    }







    private class _cls5 extends BroadcastReceiver
    {

        final DownloadFragmentGroup this$0;

        public void onReceive(Context context, Intent intent)
        {
            context = intent.getAction();
            Logger.log("test_sd", (new StringBuilder()).append("[BroadcastReceiver]====").append(context).toString());
            onSdcardStateChanged();
            context = getCurrentFragment();
            if (context != null)
            {
                if (context instanceof DownloadSoundsListFragment)
                {
                    context = (DownloadSoundsListFragment)context;
                    if (context != null)
                    {
                        context.onSdcardStateChanged();
                    }
                } else
                if (context instanceof DownloadAlbumListFragment)
                {
                    context = manager.findFragmentByTag("tag_download_album_sounds");
                    if (context != null && (context instanceof DownloadSoundsListForAlbumFragment))
                    {
                        context = (DownloadSoundsListForAlbumFragment)context;
                        if (context != null)
                        {
                            context.onSdcardStateChanged();
                            return;
                        }
                    }
                }
            }
        }

        _cls5()
        {
            this$0 = DownloadFragmentGroup.this;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final DownloadFragmentGroup this$0;

        public void run()
        {
            final int downloadSize = (int)StorageUtils.getTotalDownloadSize();
            final int freeSize = (int)(StorageUtils.getFreeStorageSize() / 1024D / 1024D);
            if (getActivity() == null || getActivity().isFinishing())
            {
                return;
            } else
            {
                class _cls1
                    implements Runnable
                {

                    final _cls4 this$1;
                    final int val$downloadSize;
                    final int val$freeSize;

                    public void run()
                    {
                        if (!isAdded())
                        {
                            return;
                        } else
                        {
                            updateSpaceOccupyText(downloadSize, freeSize);
                            updateSpaceOccupyProgress(downloadSize, freeSize);
                            return;
                        }
                    }

                _cls1()
                {
                    this$1 = _cls4.this;
                    downloadSize = i;
                    freeSize = j;
                    super();
                }
                }

                getActivity().runOnUiThread(new _cls1());
                return;
            }
        }

        _cls4()
        {
            this$0 = DownloadFragmentGroup.this;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final DownloadFragmentGroup this$0;

        public void run()
        {
            updateSpaceOccupationView();
        }

        _cls1()
        {
            this$0 = DownloadFragmentGroup.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final DownloadFragmentGroup this$0;

        public void run()
        {
            updateSpaceOccupationView();
        }

        _cls2()
        {
            this$0 = DownloadFragmentGroup.this;
            super();
        }
    }


    private class _cls3
        implements Runnable
    {

        final DownloadFragmentGroup this$0;
        final int val$contents;

        public void run()
        {
            updateIncompleteTaskNum(contents);
        }

        _cls3()
        {
            this$0 = DownloadFragmentGroup.this;
            contents = i;
            super();
        }
    }

}
