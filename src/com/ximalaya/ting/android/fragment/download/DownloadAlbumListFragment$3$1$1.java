// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.app.ProgressDialog;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.SoundsDownloadForAlbumAdapter;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadAlbumListFragment

class pd extends MyAsyncTask
{

    ProgressDialog pd;
    final d this$2;

    protected transient Boolean doInBackground(Void avoid[])
    {
        DownloadHandler.getInstance(mAppContext).delAllCompleteTasks();
        return Boolean.valueOf(true);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(Boolean boolean1)
    {
        super.onPostExecute(boolean1);
        if (pd != null)
        {
            pd.cancel();
            pd = null;
        }
        if (boolean1.booleanValue())
        {
            if (DownloadAlbumListFragment.access$100(_fld0) != null)
            {
                DownloadAlbumListFragment.access$000(_fld0).clear();
                DownloadAlbumListFragment.access$100(_fld0).notifyDataSetChanged();
            }
            DownloadAlbumListFragment.access$000(_fld0).clear();
            DownloadAlbumListFragment.access$500(_fld0).setVisibility(4);
        }
        DownloadAlbumListFragment.access$400(_fld0);
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Boolean)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        pd = new MyProgressDialog(mActivity);
        pd.show();
        class _cls1
            implements android.content.DialogInterface.OnKeyListener
        {

            final DownloadAlbumListFragment._cls3._cls1._cls1 this$3;

            public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
            {
                return true;
            }

            _cls1()
            {
                this$3 = DownloadAlbumListFragment._cls3._cls1._cls1.this;
                super();
            }
        }

        pd.setOnKeyListener(new _cls1());
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("\u6B63\u5728\u6E05\u9664\u5DF2\u4E0B\u8F7D\u4E13\u8F91\uFF0C\u8BF7\u7B49\u5F85...");
        PlayListControl.getPlayListManager().doBeforeDelAllDownload();
    }

    _cls1()
    {
        this$2 = this._cls2.this;
        super();
        pd = null;
    }
}
