// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.os.AsyncTask;
import com.ximalaya.ting.android.adapter.DownloadedSoundListAdapter;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadSoundsListFragment

class this._cls0 extends AsyncTask
{

    final DownloadSoundsListFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        return DownloadHandler.getInstance(mAppContext).getSortedFinishedDownloadList();
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        if (!isAdded())
        {
            return;
        }
        DownloadSoundsListFragment.access$000(DownloadSoundsListFragment.this).clear();
        if (list != null && list.size() > 0)
        {
            DownloadSoundsListFragment.access$000(DownloadSoundsListFragment.this).addAll(list);
        }
        soundsDownloadAdapter.notifyDataSetChanged();
        DownloadSoundsListFragment.access$400(DownloadSoundsListFragment.this);
        DownloadSoundsListFragment.access$300(DownloadSoundsListFragment.this);
    }

    ()
    {
        this$0 = DownloadSoundsListFragment.this;
        super();
    }
}
