// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.download.DownloadTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadUnfinishedListFragment

class this._cls0
    implements android.widget.er
{

    final DownloadUnfinishedListFragment this$0;

    public boolean onItemLongClick(AdapterView adapterview, View view, final int posistion, long l)
    {
        while (posistion > DownloadUnfinishedListFragment.access$000(DownloadUnfinishedListFragment.this).size() || posistion == 0 || ((DownloadTask)DownloadUnfinishedListFragment.access$000(DownloadUnfinishedListFragment.this).get(posistion - 1)).downloadStatus >= 4) 
        {
            return false;
        }
        class _cls1
            implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
        {

            final DownloadUnfinishedListFragment._cls7 this$1;
            final int val$posistion;

            public void onExecute()
            {
                if (posistion - 1 >= DownloadUnfinishedListFragment.access$000(this$0).size())
                {
                    return;
                } else
                {
                    class _cls1 extends MyAsyncTask
                    {

                        ProgressDialog pd;
                        final _cls1 this$2;

                        protected transient Boolean doInBackground(Void avoid[])
                        {
                            boolean flag;
                            if (DownloadHandler.getInstance(mAppContext).delDownloadTask((DownloadTask)DownloadUnfinishedListFragment.access$000(this$0).get(posistion - 1)) == 0)
                            {
                                flag = true;
                            } else
                            {
                                flag = false;
                            }
                            return Boolean.valueOf(flag);
                        }

                        protected volatile Object doInBackground(Object aobj[])
                        {
                            return doInBackground((Void[])aobj);
                        }

                        protected void onPostExecute(Boolean boolean1)
                        {
                            if (pd != null)
                            {
                                pd.cancel();
                                pd = null;
                            }
                            if (boolean1.booleanValue())
                            {
                                updateListViewHeader();
                            }
                            DownloadUnfinishedListFragment.access$200(this$0);
                        }

                        protected volatile void onPostExecute(Object obj)
                        {
                            onPostExecute((Boolean)obj);
                        }

                        protected void onPreExecute()
                        {
                            super.onPreExecute();
                            pd = new MyProgressDialog(mActivity);
                            pd.setMessage("\u6B63\u5728\u6E05\u9664\u4E0B\u8F7D\u5217\u8868\uFF0C\u8BF7\u7B49\u5F85...");
                            pd.show();
                            PlayListControl.getPlayListManager().doBeforeDelete((SoundInfo)DownloadUnfinishedListFragment.access$000(this$0).get(posistion - 1));
                        }

                        _cls1()
                        {
                            this$2 = _cls1.this;
                            super();
                            pd = null;
                        }
                    }

                    (new _cls1()).myexec(new Void[0]);
                    return;
                }
            }

            _cls1()
            {
                this$1 = DownloadUnfinishedListFragment._cls7.this;
                posistion = i;
                super();
            }
        }

        (new DialogBuilder(mActivity)).setMessage("\u662F\u5426\u786E\u5B9A\u5220\u9664\u8BE5\u58F0\u97F3?").setOkBtn(new _cls1()).showConfirm();
        return false;
    }

    _cls1()
    {
        this$0 = DownloadUnfinishedListFragment.this;
        super();
    }
}
