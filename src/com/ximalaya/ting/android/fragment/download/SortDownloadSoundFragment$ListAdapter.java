// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.model.download.DownloadTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            SortDownloadSoundFragment

private static class mInflater extends BaseAdapter
{
    class ViewHolder
    {

        TextView mOwner;
        TextView mTitle;
        final SortDownloadSoundFragment.ListAdapter this$0;

        ViewHolder()
        {
            this$0 = SortDownloadSoundFragment.ListAdapter.this;
            super();
        }
    }


    private List mData;
    private LayoutInflater mInflater;

    public void drop(int i, int j)
    {
        DownloadTask downloadtask = (DownloadTask)mData.remove(i);
        mData.add(j, downloadtask);
        notifyDataSetChanged();
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public List getData()
    {
        return mData;
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        DownloadTask downloadtask;
        if (view == null)
        {
            viewgroup = new ViewHolder();
            view = mInflater.inflate(0x7f03013a, null);
            viewgroup.mTitle = (TextView)view.findViewById(0x1020014);
            viewgroup.mOwner = (TextView)view.findViewById(0x1020015);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        downloadtask = (DownloadTask)mData.get(i);
        ((ViewHolder) (viewgroup)).mTitle.setText(downloadtask.title);
        ((ViewHolder) (viewgroup)).mOwner.setText(downloadtask.nickname);
        return view;
    }

    public ViewHolder.this._cls0(Context context, List list)
    {
        mData = list;
        mInflater = LayoutInflater.from(context);
    }
}
