// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.download;

import android.app.ProgressDialog;
import com.ximalaya.ting.android.adapter.SoundsDownloadAdapter;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.download:
//            DownloadUnfinishedListFragment

class pd extends MyAsyncTask
{

    ProgressDialog pd;
    final pd this$2;

    protected transient Boolean doInBackground(Void avoid[])
    {
        DownloadHandler.getInstance(mAppContext).delAllIncompleteTasks();
        return Boolean.valueOf(true);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(Boolean boolean1)
    {
        super.onPostExecute(boolean1);
        if (pd != null)
        {
            pd.cancel();
            pd = null;
        }
        if (boolean1.booleanValue())
        {
            if (DownloadUnfinishedListFragment.access$100(_fld0) != null)
            {
                DownloadUnfinishedListFragment.access$100(_fld0).getData().clear();
                DownloadUnfinishedListFragment.access$100(_fld0).notifyDataSetChanged();
            }
            DownloadUnfinishedListFragment.access$000(_fld0).clear();
            updateListViewHeader();
        }
        DownloadUnfinishedListFragment.access$200(_fld0);
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Boolean)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        pd = new MyProgressDialog(mActivity);
        pd.show();
        class _cls1
            implements android.content.DialogInterface.OnKeyListener
        {

            final DownloadUnfinishedListFragment._cls4._cls1._cls1 this$3;

            public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
            {
                return true;
            }

            _cls1()
            {
                this$3 = DownloadUnfinishedListFragment._cls4._cls1._cls1.this;
                super();
            }
        }

        pd.setOnKeyListener(new _cls1());
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("\u6B63\u5728\u6E05\u9664\u6240\u6709\u672A\u5B8C\u6210\u7684\u4E0B\u8F7D\u4EFB\u52A1\uFF0C\u8BF7\u7B49\u5F85...");
        PlayListControl.getPlayListManager().doBeforeDelAllDownload();
    }

    _cls1()
    {
        this$2 = this._cls2.this;
        super();
        pd = null;
    }
}
