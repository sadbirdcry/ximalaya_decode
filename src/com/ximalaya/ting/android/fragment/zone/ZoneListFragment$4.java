// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.ting.android.model.zone.ZoneModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneListFragment, ZoneFragment

class this._cls0
    implements android.widget.lickListener
{

    final ZoneListFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        if (!OneClickHelper.getInstance().onClick(view) || i < 0 || (i + 1) - mListView.getHeaderViewsCount() > ZoneListFragment.access$500(ZoneListFragment.this).size())
        {
            return;
        } else
        {
            ToolUtil.onEvent(mCon, "zone_main");
            adapterview = new Bundle();
            adapterview.putLong("zoneId", ((ZoneModel)ZoneListFragment.access$500(ZoneListFragment.this).get(i - mListView.getHeaderViewsCount())).getId());
            adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment, adapterview);
            return;
        }
    }

    ()
    {
        this$0 = ZoneListFragment.this;
        super();
    }
}
