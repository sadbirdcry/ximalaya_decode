// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.view.BlurableImageView;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneFragment

class this._cls0
    implements com.ximalaya.ting.android.util.isplayCallback
{

    final ZoneFragment this$0;

    public void onCompleteDisplay(String s, Bitmap bitmap)
    {
        if (mCon == null)
        {
            return;
        }
        Bitmap bitmap1 = ImageManager2.from(mCon).getFromMemCache((new StringBuilder()).append(s).append("/blur").toString());
        if (bitmap1 != null)
        {
            ZoneFragment.access$1300(ZoneFragment.this).setImageBitmap(bitmap1);
        } else
        if (bitmap != null)
        {
            ZoneFragment.access$1300(ZoneFragment.this).blur(new BitmapDrawable(mCon.getResources(), bitmap), (new StringBuilder()).append(s).append("/blur").toString(), true);
        } else
        {
            ZoneFragment.access$1300(ZoneFragment.this).setImageDrawable(new ColorDrawable(Color.parseColor("#b3202332")));
            ZoneFragment.access$1300(ZoneFragment.this).setResourceUrl(null);
        }
        ZoneFragment.access$1402(ZoneFragment.this, null);
    }

    lback()
    {
        this$0 = ZoneFragment.this;
        super();
    }
}
