// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.Button;
import android.widget.PopupWindow;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.view.multiimgpicker.MultiImgPickerActivity;
import java.io.File;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            PostFragment

public class _cls4 extends PopupWindow
{

    final PostFragment this$0;

    private void takePhoto()
    {
        Intent intent;
        File file;
        intent = new Intent("android.media.action.IMAGE_CAPTURE");
        file = new File((new StringBuilder()).append(a.af).append(File.separator).append(System.currentTimeMillis()).append(".jpg").toString());
        if (file.exists()) goto _L2; else goto _L1
_L1:
        file.getParentFile().mkdirs();
_L4:
        PostFragment.access$4502(PostFragment.this, file.getAbsolutePath());
        intent.putExtra("output", Uri.fromFile(file));
        startActivityForResult(intent, 0);
        return;
_L2:
        if (file.exists())
        {
            file.delete();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }


    public _cls4.val.this._cls0(final Context mContext, View view)
    {
        this.this$0 = PostFragment.this;
        super(mContext, null, 0x7f0b0012);
        Object obj = View.inflate(mContext, 0x7f03012a, null);
        setOutsideTouchable(true);
        setWidth(-1);
        setHeight(-1);
        setContentView(((View) (obj)));
        setAnimationStyle(0x7f0b0012);
        update();
        view = (Button)((View) (obj)).findViewById(0x7f0a04ac);
        Button button = (Button)((View) (obj)).findViewById(0x7f0a04ad);
        obj = (Button)((View) (obj)).findViewById(0x7f0a04ae);
        class _cls1
            implements android.widget.PopupWindow.OnDismissListener
        {

            final PostFragment.PopupWindows this$1;
            final PostFragment val$this$0;

            public void onDismiss()
            {
                if (PostFragment.access$1300(PostFragment.PopupWindows.this.this$0) != null)
                {
                    PostFragment.access$1300(PostFragment.PopupWindows.this.this$0).dismiss();
                }
            }

            _cls1()
            {
                this$1 = PostFragment.PopupWindows.this;
                this$0 = postfragment;
                super();
            }
        }

        setOnDismissListener(new _cls1());
        class _cls2
            implements android.view.View.OnClickListener
        {

            final PostFragment.PopupWindows this$1;
            final PostFragment val$this$0;

            public void onClick(View view1)
            {
                dismiss();
                takePhoto();
            }

            _cls2()
            {
                this$1 = PostFragment.PopupWindows.this;
                this$0 = postfragment;
                super();
            }
        }

        view.setOnClickListener(new _cls2());
        class _cls3
            implements android.view.View.OnClickListener
        {

            final PostFragment.PopupWindows this$1;
            final Context val$mContext;
            final PostFragment val$this$0;

            public void onClick(View view1)
            {
                dismiss();
                view1 = new Intent(mContext, com/ximalaya/ting/android/view/multiimgpicker/MultiImgPickerActivity);
                int j = 5 - PostFragment.access$1100(PostFragment.PopupWindows.this.this$0).size();
                int i = j;
                if (PostFragment.access$1100(PostFragment.PopupWindows.this.this$0).contains("add_default"))
                {
                    i = j + 1;
                }
                view1.putExtra("can_add_size", i);
                view1.putExtra("max_size", 5);
                startActivityForResult(view1, 1);
            }

            _cls3()
            {
                this$1 = PostFragment.PopupWindows.this;
                this$0 = postfragment;
                mContext = context;
                super();
            }
        }

        button.setOnClickListener(new _cls3());
        class _cls4
            implements android.view.View.OnClickListener
        {

            final PostFragment.PopupWindows this$1;
            final PostFragment val$this$0;

            public void onClick(View view1)
            {
                dismiss();
            }

            _cls4()
            {
                this$1 = PostFragment.PopupWindows.this;
                this$0 = postfragment;
                super();
            }
        }

        ((Button) (obj)).setOnClickListener(new _cls4());
    }
}
