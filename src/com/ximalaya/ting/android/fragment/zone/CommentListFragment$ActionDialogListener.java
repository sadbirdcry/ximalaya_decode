// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.zone.CommentModel;
import com.ximalaya.ting.android.model.zone.PostReportInfo;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            CommentListFragment

private class xdcsData
    implements android.widget.ent.ActionDialogListener
{

    private CommentModel model;
    final CommentListFragment this$0;
    private String xdcsData;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        DataCollectUtil.bindDataToView(xdcsData, view);
        if (CommentListFragment.access$700(CommentListFragment.this) != null)
        {
            CommentListFragment.access$700(CommentListFragment.this).dismiss();
        }
        if (!OneClickHelper.getInstance().onClick(view))
        {
            return;
        }
        switch (i)
        {
        default:
            return;

        case 0: // '\0'
            adapterview = new Bundle();
            adapterview.putLong("toUid", model.getPoster().getUid());
            adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, adapterview);
            return;

        case 1: // '\001'
            if (model.isMainPost())
            {
                comment();
                return;
            } else
            {
                reply(model.getPoster().getNickname(), model.getId(), model.getGroupId());
                return;
            }

        case 2: // '\002'
            adapterview = new PostReportInfo();
            adapterview.setReporterId(UserInfoMannage.getInstance().getUser().getUid().longValue());
            adapterview.setReportedId(model.getPoster().getUid());
            adapterview.setPostId(CommentListFragment.access$2200(CommentListFragment.this));
            if (model.isMainPost())
            {
                adapterview.setType(1);
            } else
            {
                adapterview.setType(2);
                adapterview.setCommentId(model.getId());
            }
            report(adapterview, view);
            return;

        case 3: // '\003'
            break;
        }
        if (!UserInfoMannage.hasLogined())
        {
            adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
            startActivity(adapterview);
            return;
        } else
        {
            CommentListFragment.access$2300(CommentListFragment.this, view, model);
            return;
        }
    }

    public (CommentModel commentmodel, String s)
    {
        this$0 = CommentListFragment.this;
        super();
        model = commentmodel;
        xdcsData = s;
    }
}
