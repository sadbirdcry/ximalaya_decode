// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.activity.report.ReportActivity;
import com.ximalaya.ting.android.adapter.zone.CommentAdapter;
import com.ximalaya.ting.android.adapter.zone.PicAddAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.play.ImageViewer;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.model.zone.CommentModel;
import com.ximalaya.ting.android.model.zone.PostEditorData;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.model.zone.PostReportInfo;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.BitmapUtils;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.MyGridView;
import com.ximalaya.ting.android.view.emotion.EmotionSelectorPanel;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import com.ximalaya.ting.android.view.multiimgpicker.MultiImgPickerActivity;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgItem;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneConstants

public class PostFragment extends BaseListFragment
    implements android.view.View.OnClickListener, android.view.ViewTreeObserver.OnGlobalLayoutListener, com.ximalaya.ting.android.adapter.zone.CommentAdapter.PostActionListner
{
    public class PopupWindows extends PopupWindow
    {

        final PostFragment this$0;

        private void takePhoto()
        {
            Intent intent;
            File file;
            intent = new Intent("android.media.action.IMAGE_CAPTURE");
            file = new File((new StringBuilder()).append(a.af).append(File.separator).append(System.currentTimeMillis()).append(".jpg").toString());
            if (file.exists()) goto _L2; else goto _L1
_L1:
            file.getParentFile().mkdirs();
_L4:
            mPhotoPath = file.getAbsolutePath();
            intent.putExtra("output", Uri.fromFile(file));
            startActivityForResult(intent, 0);
            return;
_L2:
            if (file.exists())
            {
                file.delete();
            }
            if (true) goto _L4; else goto _L3
_L3:
        }


        public PopupWindows(final Context mContext, View view)
        {
            this$0 = PostFragment.this;
            super(mContext, null, 0x7f0b0012);
            Object obj = View.inflate(mContext, 0x7f03012a, null);
            setOutsideTouchable(true);
            setWidth(-1);
            setHeight(-1);
            setContentView(((View) (obj)));
            setAnimationStyle(0x7f0b0012);
            update();
            view = (Button)((View) (obj)).findViewById(0x7f0a04ac);
            Button button = (Button)((View) (obj)).findViewById(0x7f0a04ad);
            obj = (Button)((View) (obj)).findViewById(0x7f0a04ae);
            class _cls1
                implements android.widget.PopupWindow.OnDismissListener
            {

                final PopupWindows this$1;
                final PostFragment val$this$0;

                public void onDismiss()
                {
                    if (mDimBgDialog != null)
                    {
                        mDimBgDialog.dismiss();
                    }
                }


// JavaClassFileOutputException: Invalid index accessing method local variables table of <init>
            }

            setOnDismissListener(new _cls1());
            class _cls2
                implements android.view.View.OnClickListener
            {

                final PopupWindows this$1;
                final PostFragment val$this$0;

                public void onClick(View view1)
                {
                    dismiss();
                    takePhoto();
                }


// JavaClassFileOutputException: Invalid index accessing method local variables table of <init>
            }

            view.setOnClickListener(new _cls2());
            class _cls3
                implements android.view.View.OnClickListener
            {

                final PopupWindows this$1;
                final Context val$mContext;
                final PostFragment val$this$0;

                public void onClick(View view1)
                {
                    dismiss();
                    view1 = new Intent(mContext, com/ximalaya/ting/android/view/multiimgpicker/MultiImgPickerActivity);
                    int j = 5 - mPicPathList.size();
                    int i = j;
                    if (mPicPathList.contains("add_default"))
                    {
                        i = j + 1;
                    }
                    view1.putExtra("can_add_size", i);
                    view1.putExtra("max_size", 5);
                    startActivityForResult(view1, 1);
                }


// JavaClassFileOutputException: Invalid index accessing method local variables table of <init>
            }

            button.setOnClickListener(new _cls3());
            class _cls4
                implements android.view.View.OnClickListener
            {

                final PopupWindows this$1;
                final PostFragment val$this$0;

                public void onClick(View view1)
                {
                    dismiss();
                }


// JavaClassFileOutputException: Invalid index accessing method local variables table of <init>
            }

            ((Button) (obj)).setOnClickListener(new _cls4());
        }
    }

    class UploadImgTask extends MyAsyncTask
    {

        BaseModel bm;
        Map pathImgIdMap;
        final PostFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected transient Void doInBackground(Object aobj[])
        {
            Object obj = (List)aobj[0];
            if (obj != null && !((List) (obj)).isEmpty()) goto _L2; else goto _L1
_L1:
            return null;
_L2:
            pathImgIdMap = new HashMap();
            obj = ((List) (obj)).iterator();
_L6:
            if (!((Iterator) (obj)).hasNext()) goto _L1; else goto _L3
_L3:
            String s;
            Object obj1;
            s = (String)((Iterator) (obj)).next();
            obj1 = uploadFile(s, (View)aobj[1]);
            if (obj1 == null) goto _L1; else goto _L4
_L4:
            obj1 = JSON.parseObject(((String) (obj1)));
            if (obj1 == null) goto _L6; else goto _L5
_L5:
            bm = new BaseModel();
            bm.ret = ((JSONObject) (obj1)).getInteger("ret").intValue();
            if (bm.ret != 0) goto _L1; else goto _L7
_L7:
            bm.msg = ((JSONObject) (obj1)).getString("msg");
            if (bm.msg == null) goto _L6; else goto _L8
_L8:
            obj1 = JSONObject.parseArray(bm.msg);
            if (((JSONArray) (obj1)).size() <= 0) goto _L6; else goto _L9
_L9:
            obj1 = (JSONObject)((JSONArray) (obj1)).get(0);
            if (!((JSONObject) (obj1)).containsKey("uploadTrack")) goto _L6; else goto _L10
_L10:
            obj1 = ((JSONObject) (obj1)).getJSONObject("uploadTrack");
            if (obj1 != null)
            {
                try
                {
                    obj1 = ((JSONObject) (obj1)).getString("id");
                    if (!TextUtils.isEmpty(((CharSequence) (obj1))))
                    {
                        pathImgIdMap.put(s, obj1);
                    }
                }
                catch (Exception exception)
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
                }
            }
              goto _L6
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            if (!canGoon())
            {
                return;
            }
            if (pathImgIdMap != null)
            {
                java.util.Map.Entry entry;
                for (void1 = pathImgIdMap.entrySet().iterator(); void1.hasNext(); mUploadedImgs.put(entry.getKey(), entry.getValue()))
                {
                    entry = (java.util.Map.Entry)void1.next();
                }

            }
            verifyImgAllUploaded(fragmentBaseContainerView, false);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        UploadImgTask()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private static final int PAGE_SIZE = 30;
    private static final int SELECT_PICTURE = 1;
    private static final int TAKE_PICTURE = 0;
    private static final int ZOOM_PICTURE = 2;
    private RelativeLayout listviewContainer;
    private MenuDialog mActionDlg;
    private CommentAdapter mAdapter;
    private ImageView mBackToZoneBtn;
    private ImageView mBackToZoneFloatBtn;
    private EditText mContentEt;
    private String mContentStr;
    private ArrayList mDataList;
    private Dialog mDimBgDialog;
    private ImageView mEmotionBtnIv;
    private EmotionSelectorPanel mEmotionSelector;
    private FrameLayout mExtraLayout;
    private boolean mFilterByHost;
    private RadioGroup mFilterHeader;
    private RadioGroup mFilterHeaderFloat;
    private RelativeLayout mFilterHeaderFloatLayout;
    private RelativeLayout mFilterHeaderLayout;
    private boolean mHasMore;
    private long mHostUid;
    private ImageViewer mImageViewer;
    private boolean mIsEditComment;
    private boolean mIsLoadingComment;
    private boolean mIsLoadingMain;
    private PostModel mMainPost;
    private InputMethodManager mMethodManager;
    private String mPhotoPath;
    private PicAddAdapter mPicAdapter;
    private ImageView mPicBtnIv;
    private MyGridView mPicGridView;
    private List mPicPathList;
    private PopupWindows mPopupDialog;
    private long mPostId;
    private long mReplyCommentGroupId;
    private long mReplyCommentId;
    private String mReplyPosterName;
    private View mRoot;
    private ImageView mSendBtnIv;
    private ImageView mSoftInputBtnIv;
    private List mToDelPics;
    private UploadImgTask mUploadImgTask;
    private Map mUploadedImgs;
    private long mZoneId;
    ProgressDialog pd;

    public PostFragment()
    {
        mDataList = new ArrayList();
        mFilterByHost = false;
        mPicPathList = new ArrayList();
        mToDelPics = new ArrayList();
        mUploadedImgs = new HashMap();
        mIsEditComment = true;
        pd = null;
    }

    private String buildImgIdParam()
    {
        StringBuilder stringbuilder = new StringBuilder();
        Iterator iterator = mPicPathList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            String s = (String)iterator.next();
            if (!s.equals("add_defautl"))
            {
                s = (String)mUploadedImgs.get(s);
                if (s != null)
                {
                    stringbuilder.append(s).append(",");
                }
            }
        } while (true);
        if (stringbuilder.length() > 0)
        {
            return stringbuilder.substring(0, stringbuilder.length() - 1);
        } else
        {
            return stringbuilder.toString();
        }
    }

    private void compressImages(List list, final boolean isTakePhoto)
    {
        if (pd == null)
        {
            pd = createProgressDialog("\u6DFB\u52A0\u56FE\u7247", "\u52A0\u8F7D\u4E2D...");
        } else
        {
            pd.setTitle("\u6DFB\u52A0\u56FE\u7247");
            pd.setMessage("\u52A0\u8F7D\u4E2D...");
        }
        pd.show();
        BitmapUtils.compressImages(list, false, new _cls29());
    }

    private void confirmAbaddonEditing(final boolean isEditComment, final String replyPosterName, final long replyCommentId, final long replyCommentGroupId)
    {
        if (mContentEt.getText() != null && mContentEt.getText().toString().trim().length() > 0)
        {
            (new DialogBuilder(getActivity())).setMessage("\u60A8\u8FD8\u6709\u672A\u53D1\u8868\u7684\u5185\u5BB9\uFF0C\u786E\u8BA4\u653E\u5F03\u5417\uFF1F").setOkBtn(new _cls23()).showConfirm();
            return;
        } else
        {
            resetData();
            mIsEditComment = isEditComment;
            mReplyCommentId = replyCommentId;
            mReplyPosterName = replyPosterName;
            mReplyCommentGroupId = replyCommentGroupId;
            findViewById(0x7f0a0310).setVisibility(0);
            setContentEditor(true);
            return;
        }
    }

    private void createPopupDialog()
    {
        mDimBgDialog = new Dialog(getActivity(), 0x1030010);
        final View emptyDialog = LayoutInflater.from(mCon).inflate(0x7f03006a, null);
        android.view.WindowManager.LayoutParams layoutparams = mDimBgDialog.getWindow().getAttributes();
        layoutparams.dimAmount = 0.5F;
        mDimBgDialog.getWindow().setAttributes(layoutparams);
        mDimBgDialog.getWindow().addFlags(2);
        mDimBgDialog.setContentView(emptyDialog);
        mDimBgDialog.setCanceledOnTouchOutside(true);
        if (mPopupDialog == null)
        {
            mPopupDialog = new PopupWindows(mCon, mPicGridView);
        }
        emptyDialog.setOnClickListener(new _cls25());
        mDimBgDialog.setOnCancelListener(new _cls26());
        mDimBgDialog.setOnShowListener(new _cls27());
    }

    private ProgressDialog createProgressDialog(String s, String s1)
    {
        MyProgressDialog myprogressdialog = new MyProgressDialog(getActivity());
        myprogressdialog.setOnKeyListener(new _cls28());
        myprogressdialog.setCanceledOnTouchOutside(false);
        myprogressdialog.setTitle(s);
        myprogressdialog.setMessage(s1);
        return myprogressdialog;
    }

    private void delete(final View view, final CommentModel model)
    {
        if (model == null)
        {
            return;
        }
        String s;
        if (model.isMainPost())
        {
            s = "\u5E16\u5B50";
        } else
        if (model.getParentComment() != null)
        {
            s = "\u56DE\u590D";
        } else
        {
            s = "\u8BC4\u8BBA";
        }
        (new DialogBuilder(getActivity())).setMessage((new StringBuilder()).append("\u786E\u8BA4\u8981\u5220\u9664\u8BE5").append(s).append("\u5417\uFF1F").toString()).setOkBtn(new _cls19()).showConfirm();
    }

    private void doAfterDelete(CommentModel commentmodel)
    {
        Iterator iterator = mDataList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            if (((CommentModel)iterator.next()).getId() != commentmodel.getId())
            {
                continue;
            }
            iterator.remove();
            break;
        } while (true);
        mAdapter.notifyDataSetChanged();
    }

    private void doDeleteComment(View view, final CommentModel model)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", mZoneId);
        requestparams.put("postId", mPostId);
        String s = (new StringBuilder()).append(ZoneConstants.URL_COMMENTS).append("/").append(model.getId()).toString();
        f.a().c(s, requestparams, DataCollectUtil.getDataFromView(view), new _cls21());
    }

    private void doDeletePost(View view, CommentModel commentmodel)
    {
        commentmodel = (new StringBuilder()).append(ZoneConstants.URL_POSTS).append("/").append(mPostId).toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", mZoneId);
        f.a().c(commentmodel, requestparams, DataCollectUtil.getDataFromView(view), new _cls20());
    }

    private void doDeleteReply(View view, final CommentModel model)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", mZoneId);
        requestparams.put("postId", mPostId);
        String s = (new StringBuilder()).append(ZoneConstants.URL_REPLIES).append("/").append(model.getId()).toString();
        f.a().c(s, requestparams, DataCollectUtil.getDataFromView(view), new _cls22());
    }

    private void doSubmit(View view)
    {
        if (!hasAddPic() && (mContentEt.getText() == null || mContentEt.getText().toString().length() == 0))
        {
            showToast("\u5185\u5BB9\u4E0D\u80FD\u4E3A\u7A7A");
            return;
        }
        if (mContentEt.getText() != null && mContentEt.getText().length() > 10000)
        {
            showToast("\u5185\u5BB9\u4E0D\u80FD\u8D85\u8FC710000\u5B57");
            return;
        }
        if (pd == null)
        {
            pd = createProgressDialog("\u63D0\u793A", "\u6B63\u5728\u53D1\u8868\u4E2D...");
        } else
        {
            pd.setTitle("\u63D0\u793A");
            pd.setMessage("\u6B63\u5728\u53D1\u8868\u4E2D...");
        }
        pd.show();
        if (!hasAddPic())
        {
            if (mIsEditComment)
            {
                doSubmitComment(view);
                return;
            } else
            {
                doSubmitReply(view);
                return;
            }
        } else
        {
            verifyImgAllUploaded(view, true);
            return;
        }
    }

    private void doSubmitComment(final View view)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", mZoneId);
        requestparams.put("postId", mPostId);
        requestparams.put("content", mContentEt.getText().toString());
        if (!mUploadedImgs.isEmpty())
        {
            requestparams.put("imageIds", buildImgIdParam());
        }
        f.a().b(ZoneConstants.URL_COMMENTS, requestparams, DataCollectUtil.getDataFromView(view), new _cls30());
    }

    private void doSubmitReply(final View view)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", mZoneId);
        requestparams.put("postId", mPostId);
        requestparams.put("commentId", mReplyCommentId);
        requestparams.put("groupId", mReplyCommentGroupId);
        requestparams.put("content", mContentEt.getText().toString());
        if (!mUploadedImgs.isEmpty())
        {
            requestparams.put("imageIds", buildImgIdParam());
        }
        f.a().b(ZoneConstants.URL_REPLIES, requestparams, DataCollectUtil.getDataFromView(view), new _cls31());
    }

    private void finishAddPic()
    {
        if (pd != null)
        {
            pd.cancel();
            pd = null;
        }
        genAddBtnToList();
        mPicAdapter.notifyDataSetChanged();
    }

    private void genAddBtnToList()
    {
        mPicPathList.remove("add_default");
        if (mPicPathList.size() < 5)
        {
            mPicPathList.add("add_default");
        }
    }

    private void getTempFromPref()
    {
        LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
        if (logininfomodel != null)
        {
            SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCon);
            Object obj = sharedpreferencesutil.getString("temp_zone_comment_reply");
            if (!TextUtils.isEmpty(((CharSequence) (obj))))
            {
                try
                {
                    obj = (PostEditorData)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/zone/PostEditorData);
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
                    obj = null;
                }
                if (obj != null)
                {
                    if (((PostEditorData) (obj)).getUid() != ((UserInfoModel) (logininfomodel)).uid)
                    {
                        sharedpreferencesutil.removeByKey("temp_zone_comment_reply");
                        return;
                    }
                    if (((PostEditorData) (obj)).getZoneId() == mZoneId && ((PostEditorData) (obj)).getPostId() == mPostId)
                    {
                        mReplyPosterName = ((PostEditorData) (obj)).getReplyPosterName();
                        mReplyCommentId = ((PostEditorData) (obj)).getReplyCommentId();
                        mReplyCommentGroupId = ((PostEditorData) (obj)).getReplyCommentGroupId();
                        mContentStr = ((PostEditorData) (obj)).getContent();
                        mIsEditComment = ((PostEditorData) (obj)).isEditComment();
                        mToDelPics.clear();
                        mPicPathList.clear();
                        mPicPathList.addAll(((PostEditorData) (obj)).getImagePaths());
                        mUploadedImgs = ((PostEditorData) (obj)).getUploadedImgs();
                        mPicAdapter.notifyDataSetChanged();
                        return;
                    }
                }
            }
        }
    }

    private boolean hasAddPic()
    {
        if (!mPicPathList.contains("add_default")) goto _L2; else goto _L1
_L1:
        if (mPicPathList.size() <= 1) goto _L4; else goto _L3
_L3:
        return true;
_L4:
        return false;
_L2:
        if (mPicPathList.size() <= 0)
        {
            return false;
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    private void hideExtraViews()
    {
        mEmotionSelector.hideEmotionPanel();
        mEmotionSelector.setVisibility(8);
        mPicGridView.setVisibility(8);
        mExtraLayout.setVisibility(8);
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            mZoneId = bundle.getLong("zoneId");
            mPostId = bundle.getLong("postId");
            mHostUid = bundle.getLong("hostUid");
        }
        getTempFromPref();
        setContentEditor(false);
        genAddBtnToList();
        mImageViewer = new ImageViewer(mCon);
        mAdapter = new CommentAdapter(this, mImageViewer, mCon, mDataList, mHostUid);
        ((PullToRefreshListView)mListView).setAdapter(mAdapter);
        long l = getAnimationLeftTime();
        if (l > 0L)
        {
            fragmentBaseContainerView.postDelayed(new _cls15(), l);
            return;
        } else
        {
            ((PullToRefreshListView)mListView).toRefreshing();
            return;
        }
    }

    private void initHeaderListener(RadioGroup radiogroup)
    {
        radiogroup.setOnCheckedChangeListener(new _cls14());
    }

    private void initListener()
    {
        mBackToZoneBtn.setOnClickListener(new _cls1());
        mBackToZoneFloatBtn.setOnClickListener(new _cls2());
        if (mFilterHeaderLayout.findViewById(0x7f0a007b) != null)
        {
            mFilterHeaderLayout.findViewById(0x7f0a007b).setOnClickListener(new _cls3());
        }
        if (mFilterHeaderFloatLayout.findViewById(0x7f0a007b) != null)
        {
            mFilterHeaderFloatLayout.findViewById(0x7f0a007b).setOnClickListener(new _cls4());
        }
        ((PullToRefreshListView)mListView).setOnScrollListener(new _cls5());
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls6());
        mFooterViewLoading.setOnClickListener(new _cls7());
        initHeaderListener(mFilterHeader);
        initHeaderListener(mFilterHeaderFloat);
        mListView.setOnItemClickListener(new _cls8());
        mSendBtnIv.setOnClickListener(this);
        mEmotionBtnIv.setOnClickListener(this);
        mPicBtnIv.setOnClickListener(this);
        mSoftInputBtnIv.setOnClickListener(this);
        mRoot.getViewTreeObserver().addOnGlobalLayoutListener(this);
        mPicGridView.setOnItemClickListener(new _cls9());
        mEmotionSelector.setOnEditContentListener(new _cls10());
        mContentEt.setOnClickListener(new _cls11());
        mContentEt.setOnFocusChangeListener(new _cls12());
        findViewById(0x7f0a0310).setOnClickListener(new _cls13());
    }

    private void initViews()
    {
        mRoot = findViewById(0x7f0a030f);
        mFilterHeaderLayout = (RelativeLayout)LayoutInflater.from(mActivity).inflate(0x7f030197, mListView, false);
        mFilterHeader = (RadioGroup)mFilterHeaderLayout.findViewById(0x7f0a0448);
        mBackToZoneBtn = (ImageView)mFilterHeaderLayout.findViewById(0x7f0a0641);
        ((PullToRefreshListView)mListView).addHeaderView(mFilterHeaderLayout);
        mFilterHeaderFloatLayout = (RelativeLayout)LayoutInflater.from(getActivity()).inflate(0x7f030197, null);
        mFilterHeaderFloat = (RadioGroup)mFilterHeaderFloatLayout.findViewById(0x7f0a0448);
        mBackToZoneFloatBtn = (ImageView)mFilterHeaderFloatLayout.findViewById(0x7f0a0641);
        listviewContainer = (RelativeLayout)findViewById(0x7f0a005e);
        ((PullToRefreshListView)mListView).setFloatHeadView(mFilterHeaderFloatLayout);
        listviewContainer.addView(mFilterHeaderFloatLayout);
        mMethodManager = (InputMethodManager)mCon.getSystemService("input_method");
        mContentEt = (EditText)findViewById(0x7f0a00d2);
        mExtraLayout = (FrameLayout)findViewById(0x7f0a00d3);
        mEmotionBtnIv = (ImageView)findViewById(0x7f0a00d4);
        mSendBtnIv = (ImageView)findViewById(0x7f0a0312);
        mSoftInputBtnIv = (ImageView)findViewById(0x7f0a00d6);
        mPicBtnIv = (ImageView)findViewById(0x7f0a00d5);
        mEmotionSelector = (EmotionSelectorPanel)findViewById(0x7f0a00d9);
        mPicGridView = (MyGridView)findViewById(0x7f0a00d8);
        mPicGridView.setSelector(new ColorDrawable(0));
        mPicAdapter = new PicAddAdapter(mCon, mPicPathList, mUploadedImgs);
        mPicGridView.setAdapter(mPicAdapter);
    }

    private boolean isAllUploaded()
    {
        for (Iterator iterator = mPicPathList.iterator(); iterator.hasNext();)
        {
            String s = (String)iterator.next();
            if (!s.equals("add_default") && mUploadedImgs.get(s) == null)
            {
                return false;
            }
        }

        return true;
    }

    private void loadComments(View view, boolean flag)
    {
        if (mIsLoadingComment)
        {
            return;
        }
        if (flag)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        }
        mIsLoadingComment = true;
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", mZoneId);
        requestparams.put("direction", "0");
        requestparams.put("postId", mPostId);
        if (!mDataList.isEmpty() && flag)
        {
            requestparams.put("timeline", ((CommentModel)mDataList.get(mDataList.size() - 1)).getTimeline());
        } else
        {
            requestparams.put("timeline", 0);
        }
        requestparams.put("timelineType", "0");
        requestparams.put("maxSizeOfComments", 30);
        requestparams.put("order", 0);
        if (mFilterByHost && mMainPost != null)
        {
            requestparams.put("posterUid", mMainPost.getPoster().getUid());
        }
        f.a().a(ZoneConstants.URL_COMMENTS, requestparams, DataCollectUtil.getDataFromView(view), new _cls17());
    }

    private void loadPost()
    {
        if (mIsLoadingMain || mIsLoadingComment)
        {
            return;
        } else
        {
            mIsLoadingMain = true;
            String s = (new StringBuilder()).append(ZoneConstants.URL_POSTS).append("/").append(mPostId).toString();
            RequestParams requestparams = new RequestParams();
            requestparams.put("zoneId", mZoneId);
            f.a().a(s, requestparams, DataCollectUtil.getDataFromView(mListView), new _cls16());
            return;
        }
    }

    private JSONObject parseJSON(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            return null;
        }
        return s;
    }

    private void removePicDefault()
    {
        mPicPathList.remove("add_default");
    }

    private void resetData()
    {
        mIsEditComment = true;
        mReplyCommentId = -1L;
        mReplyCommentGroupId = -1L;
        mReplyPosterName = null;
        mContentStr = null;
        mUploadedImgs.clear();
        mPicPathList.clear();
        mPicPathList.add("add_default");
        mPicAdapter.notifyDataSetChanged();
        SharedPreferencesUtil.getInstance(mCon).removeByKey("temp_zone_comment_reply");
    }

    private void saveTempToPref()
    {
        LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
        if (logininfomodel != null)
        {
            if (mContentEt != null && mContentEt.getText() != null && mContentEt.getText().toString().trim().length() > 0)
            {
                PostEditorData posteditordata = new PostEditorData();
                posteditordata.setUid(((UserInfoModel) (logininfomodel)).uid);
                posteditordata.setZoneId(mZoneId);
                posteditordata.setPostId(mPostId);
                posteditordata.setReplyCommentGroupId(mReplyCommentGroupId);
                posteditordata.setReplyPosterName(mReplyPosterName);
                posteditordata.setReplyCommentId(mReplyCommentId);
                posteditordata.setEditComment(mIsEditComment);
                posteditordata.setImagePaths(mPicPathList);
                posteditordata.setUploadedImgs(mUploadedImgs);
                posteditordata.setContent(mContentEt.getText().toString());
                SharedPreferencesUtil.getInstance(mCon).saveString("temp_zone_comment_reply", JSON.toJSONString(posteditordata));
            }
            if (!mToDelPics.isEmpty())
            {
                Object obj = SharedPreferencesUtil.getInstance(mCon).getArrayList("temp_zone_img");
                ArrayList arraylist = ((ArrayList) (obj));
                if (obj == null)
                {
                    arraylist = new ArrayList();
                }
                obj = mToDelPics.iterator();
                do
                {
                    if (!((Iterator) (obj)).hasNext())
                    {
                        break;
                    }
                    String s = (String)((Iterator) (obj)).next();
                    if (!arraylist.contains(s))
                    {
                        arraylist.add(s);
                    }
                } while (true);
                SharedPreferencesUtil.getInstance(mCon).saveArrayList("temp_zone_img", (ArrayList)arraylist);
                return;
            }
        }
    }

    private void setCommentParent(List list, List list1)
    {
        int k = list.size();
        int l = list1.size();
        for (int i = 0; i != l; i++)
        {
            for (int j = 0; j != k; j++)
            {
                if (((CommentModel)list1.get(i)).getId() == ((CommentModel)list.get(j)).getParentCommentId())
                {
                    ((CommentModel)list.get(j)).setParentComment((CommentModel)list1.get(i));
                }
            }

        }

    }

    private void setContentEditor(boolean flag)
    {
        EditText edittext = mContentEt;
        CharSequence charsequence;
        if (mContentStr == null)
        {
            charsequence = null;
        } else
        {
            charsequence = EmotionUtil.getInstance().convertEmotionTextWithChangeLine(mContentStr);
        }
        edittext.setText(charsequence);
        if (mIsEditComment)
        {
            mContentEt.setHint("\u56DE\u590D\uFF1A");
        } else
        {
            EditText edittext1 = mContentEt;
            StringBuilder stringbuilder = (new StringBuilder()).append("\u56DE\u590D");
            String s;
            if (TextUtils.isEmpty(mReplyPosterName))
            {
                s = "";
            } else
            {
                s = (new StringBuilder()).append("@").append(mReplyPosterName).toString();
            }
            edittext1.setHint(stringbuilder.append(s).append(":").toString());
        }
        if (flag)
        {
            fragmentBaseContainerView.post(new _cls24());
            showEmotionOrPic(true);
            mEmotionSelector.showEmotionGrid();
            return;
        } else
        {
            mMethodManager.hideSoftInputFromWindow(mContentEt.getWindowToken(), 0);
            mEmotionSelector.hideEmotionPanel();
            mExtraLayout.setVisibility(8);
            mPicGridView.setVisibility(8);
            mEmotionSelector.setVisibility(8);
            return;
        }
    }

    private void showEmotionOrPic(boolean flag)
    {
        mExtraLayout.setVisibility(0);
        if (flag)
        {
            mPicGridView.setVisibility(8);
            mEmotionSelector.setVisibility(0);
            return;
        } else
        {
            mPicGridView.setVisibility(0);
            mEmotionSelector.setVisibility(8);
            return;
        }
    }

    private void switchEmotionInput()
    {
        if (!mContentEt.isFocused())
        {
            mContentEt.requestFocus();
        }
        if (mEmotionBtnIv.getVisibility() == 0)
        {
            mMethodManager.hideSoftInputFromWindow(mContentEt.getWindowToken(), 0);
            fragmentBaseContainerView.postDelayed(new _cls18(), 100L);
            return;
        } else
        {
            mMethodManager.toggleSoftInputFromWindow(mContentEt.getWindowToken(), 0, 0);
            return;
        }
    }

    private void switchEmotionInputBtn(boolean flag)
    {
        if (flag)
        {
            mEmotionBtnIv.setVisibility(0);
            mSoftInputBtnIv.setVisibility(8);
            return;
        } else
        {
            mEmotionBtnIv.setVisibility(8);
            mSoftInputBtnIv.setVisibility(0);
            return;
        }
    }

    private String uploadFile(String s, View view)
    {
        s = new File(s);
        if (!s.exists())
        {
            return null;
        }
        String s1 = ZoneConstants.URL_UPLOAD_COMMENT_IMG;
        if (loginInfoModel == null)
        {
            loginInfoModel = UserInfoMannage.getInstance().getUser();
        }
        try
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("fileSize", (new StringBuilder()).append(s.length()).append("").toString());
            requestparams.put("uid", (new StringBuilder()).append(loginInfoModel.uid).append("").toString());
            requestparams.put("token", loginInfoModel.token);
            requestparams.put("myfile", s);
            s = f.a().b(s1, requestparams, view, null);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return s;
    }

    private void verifyImgAllUploaded(View view, boolean flag)
    {
        if (!isAllUploaded()) goto _L2; else goto _L1
_L1:
        if (!mIsEditComment) goto _L4; else goto _L3
_L3:
        doSubmitComment(view);
_L6:
        return;
_L4:
        doSubmitReply(view);
        return;
_L2:
        if (flag)
        {
            if (mUploadImgTask == null || mUploadImgTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
            {
                mUploadImgTask = new UploadImgTask();
                mUploadImgTask.myexec(new Object[] {
                    mPicPathList, view
                });
                return;
            }
        } else
        {
            if (pd != null)
            {
                pd.cancel();
                pd = null;
            }
            showToast("\u56FE\u7247\u672A\u5168\u90E8\u4E0A\u4F20\u6210\u529F\uFF0C\u8BF7\u91CD\u8BD5");
            mPicAdapter.notifyDataSetChanged();
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void comment()
    {
        mContentEt.requestFocus();
        confirmAbaddonEditing(true, null, -1L, -1L);
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initViews();
        initData();
        initListener();
        ToolUtil.onEvent(mCon, "zone_post_main");
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        i;
        JVM INSTR tableswitch 0 2: default 28
    //                   0 29
    //                   1 91
    //                   2 194;
           goto _L1 _L2 _L3 _L4
_L1:
        return;
_L2:
        if (j == -1 && !TextUtils.isEmpty(mPhotoPath))
        {
            mPicPathList.add(mPhotoPath);
            mPicPathList.remove("add_default");
            compressImages(Arrays.asList(new String[] {
                mPhotoPath
            }), true);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (j == -1)
        {
            Object obj = (List)intent.getSerializableExtra("image_list");
            if (obj != null)
            {
                intent = new ArrayList();
                for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); intent.add(((ImgItem)((Iterator) (obj)).next()).getPath())) { }
                mPicPathList.addAll(intent);
                mPicPathList.remove("add_default");
                compressImages(intent, false);
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L4:
        if (j == -1)
        {
            intent = (List)intent.getSerializableExtra("image_list");
            if (intent != null)
            {
                mPicPathList.clear();
                mPicPathList.addAll(intent);
                finishAddPic();
                return;
            }
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    public boolean onBackPressed()
    {
        if (mPopupDialog != null && mPopupDialog.isShowing())
        {
            mPopupDialog.dismiss();
            return true;
        }
        if (mImageViewer != null && mImageViewer.isShowing())
        {
            mImageViewer.dismiss();
            return true;
        }
        if (mExtraLayout.getVisibility() == 0)
        {
            hideExtraViews();
            return true;
        }
        if (mUploadImgTask != null && mUploadImgTask.getStatus() != android.os.AsyncTask.Status.FINISHED)
        {
            mMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getApplicationWindowToken(), 0);
            return false;
        }
        if (mContentEt.getText() != null && mContentEt.getText().toString().trim().length() > 0)
        {
            (new DialogBuilder(getActivity())).setMessage("\u60A8\u8FD8\u6709\u672A\u53D1\u8868\u7684\u5185\u5BB9\uFF0C\u786E\u8BA4\u653E\u5F03\u5417\uFF1F").setOkBtn(new _cls35()).showConfirm();
            return true;
        } else
        {
            return false;
        }
    }

    public void onClick(View view)
    {
        if (!OneClickHelper.getInstance().onClick(view)) goto _L2; else goto _L1
_L1:
        view.getId();
        JVM INSTR lookupswitch 4: default 56
    //                   2131362004: 57
    //                   2131362005: 89
    //                   2131362006: 73
    //                   2131362578: 269;
           goto _L2 _L3 _L4 _L5 _L6
_L2:
        return;
_L3:
        findViewById(0x7f0a0310).setVisibility(0);
        switchEmotionInput();
        return;
_L5:
        findViewById(0x7f0a0310).setVisibility(0);
        switchEmotionInput();
        return;
_L4:
        if (!mPicPathList.contains("add_default") && mPicPathList.size() == 5)
        {
            mMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getApplicationWindowToken(), 0);
            if (mExtraLayout.getVisibility() == 0 && mPicGridView.getVisibility() == 0)
            {
                showToast("\u6700\u591A\u9009\u62E95\u5F20\u56FE\u7247");
                return;
            } else
            {
                mContentEt.postDelayed(new _cls32(), 250L);
                return;
            }
        }
        if (!hasAddPic() || mExtraLayout.getVisibility() == 0 && mPicGridView.getVisibility() == 0)
        {
            mContentEt.postDelayed(new _cls33(), 50L);
        }
        mMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getApplicationWindowToken(), 0);
        mContentEt.postDelayed(new _cls34(), 250L);
        return;
_L6:
        if (!UserInfoMannage.hasLogined())
        {
            startActivity(new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity));
            return;
        } else
        {
            doSubmit(view);
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300c9, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onDestroy()
    {
        if (android.os.Build.VERSION.SDK_INT >= 16)
        {
            mRoot.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        } else
        {
            mRoot.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
        super.onDestroy();
    }

    public void onGlobalLayout()
    {
        Rect rect = new Rect();
        mRoot.getWindowVisibleDisplayFrame(rect);
        if (Math.abs(mRoot.getRootView().getHeight() - (rect.bottom - rect.top)) > 100 || mExtraLayout.getVisibility() != 0 || mPicGridView.getVisibility() == 0)
        {
            switchEmotionInputBtn(true);
            return;
        } else
        {
            switchEmotionInputBtn(false);
            return;
        }
    }

    public void onPause()
    {
        super.onPause();
        showPlayButton();
        if (!canGoon())
        {
            if (pd != null)
            {
                pd.dismiss();
            }
            if (mPopupDialog != null)
            {
                mPopupDialog.dismiss();
            }
        }
        saveTempToPref();
    }

    public void onResume()
    {
        super.onResume();
        hidePlayButton();
    }

    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        saveTempToPref();
    }

    public void reply(String s, long l, long l1)
    {
        mContentEt.requestFocus();
        confirmAbaddonEditing(false, s, l, l1);
    }

    public void report(PostReportInfo postreportinfo, View view)
    {
        Intent intent = new Intent(mCon, com/ximalaya/ting/android/activity/report/ReportActivity);
        intent.putExtra("report_type", 3);
        postreportinfo.setZoneId(mZoneId);
        intent.putExtra("post_data", postreportinfo);
        intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startActivity(intent);
    }



















/*
    static boolean access$2402(PostFragment postfragment, boolean flag)
    {
        postfragment.mFilterByHost = flag;
        return flag;
    }

*/





/*
    static PostModel access$2702(PostFragment postfragment, PostModel postmodel)
    {
        postfragment.mMainPost = postmodel;
        return postmodel;
    }

*/


/*
    static boolean access$2802(PostFragment postfragment, boolean flag)
    {
        postfragment.mIsLoadingMain = flag;
        return flag;
    }

*/





/*
    static boolean access$302(PostFragment postfragment, boolean flag)
    {
        postfragment.mHasMore = flag;
        return flag;
    }

*/








/*
    static boolean access$3702(PostFragment postfragment, boolean flag)
    {
        postfragment.mIsEditComment = flag;
        return flag;
    }

*/


/*
    static long access$3802(PostFragment postfragment, long l)
    {
        postfragment.mReplyCommentId = l;
        return l;
    }

*/


/*
    static String access$3902(PostFragment postfragment, String s)
    {
        postfragment.mReplyPosterName = s;
        return s;
    }

*/



/*
    static long access$4002(PostFragment postfragment, long l)
    {
        postfragment.mReplyCommentGroupId = l;
        return l;
    }

*/


/*
    static boolean access$402(PostFragment postfragment, boolean flag)
    {
        postfragment.mIsLoadingComment = flag;
        return flag;
    }

*/




/*
    static PopupWindows access$4202(PostFragment postfragment, PopupWindows popupwindows)
    {
        postfragment.mPopupDialog = popupwindows;
        return popupwindows;
    }

*/




/*
    static String access$4502(PostFragment postfragment, String s)
    {
        postfragment.mPhotoPath = s;
        return s;
    }

*/











/*
    static MenuDialog access$802(PostFragment postfragment, MenuDialog menudialog)
    {
        postfragment.mActionDlg = menudialog;
        return menudialog;
    }

*/


    private class _cls29
        implements com.ximalaya.ting.android.util.BitmapUtils.CompressCallback2
    {

        final PostFragment this$0;
        final boolean val$isTakePhoto;

        public void onFinished(Map map, boolean flag)
        {
            map = map.entrySet().iterator();
            do
            {
                if (!map.hasNext())
                {
                    break;
                }
                java.util.Map.Entry entry = (java.util.Map.Entry)map.next();
                if (!mPicPathList.contains(((Uri)entry.getValue()).getPath()))
                {
                    mPicPathList.remove(entry.getKey());
                    mPicPathList.add(((Uri)entry.getValue()).getPath());
                    if (flag)
                    {
                        mToDelPics.add(((Uri)entry.getValue()).getPath());
                    }
                }
            } while (true);
            if (!isTakePhoto) goto _L2; else goto _L1
_L1:
            int i = ViewUtil.getBitmapDegree(mPhotoPath);
            if (i == 0) goto _L2; else goto _L3
_L3:
            class _cls1
                implements MyCallback
            {

                final _cls29 this$1;

                public void execute()
                {
                    if (!canGoon())
                    {
                        return;
                    } else
                    {
                        class _cls1
                            implements Runnable
                        {

                            final _cls1 this$2;

                            public void run()
                            {
                                finishAddPic();
                            }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                        }

                        getActivity().runOnUiThread(new _cls1());
                        return;
                    }
                }

                _cls1()
                {
                    this$1 = _cls29.this;
                    super();
                }
            }

            ViewUtil.rotateBitmapByDegree(mPhotoPath, i, new _cls1());
_L5:
            return;
_L2:
            if (canGoon())
            {
                class _cls2
                    implements Runnable
                {

                    final _cls29 this$1;

                    public void run()
                    {
                        finishAddPic();
                    }

                _cls2()
                {
                    this$1 = _cls29.this;
                    super();
                }
                }

                getActivity().runOnUiThread(new _cls2());
                return;
            }
            if (true) goto _L5; else goto _L4
_L4:
        }

        _cls29()
        {
            this$0 = PostFragment.this;
            isTakePhoto = flag;
            super();
        }
    }


    private class _cls23
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final PostFragment this$0;
        final boolean val$isEditComment;
        final long val$replyCommentGroupId;
        final long val$replyCommentId;
        final String val$replyPosterName;

        public void onExecute()
        {
            resetData();
            mIsEditComment = isEditComment;
            mReplyCommentId = replyCommentId;
            mReplyPosterName = replyPosterName;
            mReplyCommentGroupId = replyCommentGroupId;
            findViewById(0x7f0a0310).setVisibility(0);
            setContentEditor(true);
        }

        _cls23()
        {
            this$0 = PostFragment.this;
            isEditComment = flag;
            replyCommentId = l;
            replyPosterName = s;
            replyCommentGroupId = l1;
            super();
        }
    }


    private class _cls25
        implements android.view.View.OnClickListener
    {

        final PostFragment this$0;

        public void onClick(View view)
        {
            if (mPopupDialog != null && mPopupDialog.isShowing())
            {
                mPopupDialog.dismiss();
            }
        }

        _cls25()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls26
        implements android.content.DialogInterface.OnCancelListener
    {

        final PostFragment this$0;

        public void onCancel(DialogInterface dialoginterface)
        {
            if (mPopupDialog != null && mPopupDialog.isShowing())
            {
                mPopupDialog.dismiss();
            }
        }

        _cls26()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls27
        implements android.content.DialogInterface.OnShowListener
    {

        final PostFragment this$0;
        final View val$emptyDialog;

        public void onShow(DialogInterface dialoginterface)
        {
            if (mPopupDialog == null)
            {
                mPopupDialog = new PopupWindows(mCon, mPicGridView);
            }
            mPopupDialog.showAtLocation(emptyDialog, 80, 0, 0);
        }

        _cls27()
        {
            this$0 = PostFragment.this;
            emptyDialog = view;
            super();
        }
    }


    private class _cls28
        implements android.content.DialogInterface.OnKeyListener
    {

        final PostFragment this$0;

        public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
        {
            if (i == 4)
            {
                onBackPressed();
                return true;
            } else
            {
                return false;
            }
        }

        _cls28()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls19
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final PostFragment this$0;
        final CommentModel val$model;
        final View val$view;

        public void onExecute()
        {
            if (pd == null)
            {
                pd = createProgressDialog("\u63D0\u793A", "\u6B63\u5728\u5220\u9664\u4E2D...");
            } else
            {
                pd.setTitle("\u63D0\u793A");
                pd.setMessage("\u6B63\u5728\u5220\u9664\u4E2D...");
            }
            pd.show();
            if (model.isMainPost())
            {
                doDeletePost(view, model);
                return;
            }
            if (model.getParentComment() != null)
            {
                doDeleteReply(view, model);
                return;
            } else
            {
                doDeleteComment(view, model);
                return;
            }
        }

        _cls19()
        {
            this$0 = PostFragment.this;
            model = commentmodel;
            view = view1;
            super();
        }
    }


    private class _cls21 extends com.ximalaya.ting.android.b.a
    {

        final PostFragment this$0;
        final CommentModel val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
            if (pd != null && pd.isShowing())
            {
                pd.cancel();
                pd = null;
            }
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            Object obj;
            for (obj = null; !canGoon() || TextUtils.isEmpty(s);)
            {
                return;
            }

            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = obj;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u5220\u9664\u8BC4\u8BBA\u5931\u8D25";
                } else
                {
                    s = (new StringBuilder()).append("\u5220\u9664\u8BC4\u8BBA\u5931\u8D25:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                doAfterDelete(model);
                showToast("\u5220\u9664\u8BC4\u8BBA\u6210\u529F");
                return;
            }
        }

        _cls21()
        {
            this$0 = PostFragment.this;
            model = commentmodel;
            super();
        }
    }


    private class _cls20 extends com.ximalaya.ting.android.b.a
    {

        final PostFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
            if (pd != null && pd.isShowing())
            {
                pd.cancel();
                pd = null;
            }
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            Object obj;
            for (obj = null; !canGoon() || TextUtils.isEmpty(s);)
            {
                return;
            }

            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = obj;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u5220\u9664\u5E16\u5B50\u5931\u8D25";
                } else
                {
                    s = (new StringBuilder()).append("\u5220\u9664\u5E16\u5B50\u5931\u8D25:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                showToast("\u5220\u9664\u5E16\u5B50\u6210\u529F");
                finish();
                return;
            }
        }

        _cls20()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls22 extends com.ximalaya.ting.android.b.a
    {

        final PostFragment this$0;
        final CommentModel val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
            if (pd != null && pd.isShowing())
            {
                pd.cancel();
                pd = null;
            }
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            Object obj;
            for (obj = null; !canGoon() || TextUtils.isEmpty(s);)
            {
                return;
            }

            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = obj;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u5220\u9664\u56DE\u590D\u5931\u8D25";
                } else
                {
                    s = (new StringBuilder()).append("\u5220\u9664\u56DE\u590D\u5931\u8D25:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                doAfterDelete(model);
                showToast("\u5220\u9664\u56DE\u590D\u6210\u529F");
                return;
            }
        }

        _cls22()
        {
            this$0 = PostFragment.this;
            model = commentmodel;
            super();
        }
    }


    private class _cls30 extends com.ximalaya.ting.android.b.a
    {

        final PostFragment this$0;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            super.onFinish();
            if (pd != null)
            {
                pd.cancel();
                pd = null;
            }
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            if (canGoon() && !TextUtils.isEmpty(s))
            {
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                    s = null;
                }
                if (s == null || s.getIntValue("ret") != 0)
                {
                    if (s == null)
                    {
                        s = null;
                    } else
                    {
                        s = s.getString("msg");
                    }
                    if (TextUtils.isEmpty(s))
                    {
                        s = "\u53D1\u8868\u8BC4\u8BBA\u5931\u8D25";
                    } else
                    {
                        s = (new StringBuilder()).append("\u53D1\u8868\u8BC4\u8BBA\u5931\u8D25:").append(s).toString();
                    }
                    showToast(s);
                    return;
                }
                showToast("\u53D1\u8868\u8BC4\u8BBA\u6210\u529F");
                s = s.getString("result");
                if (TextUtils.isEmpty(s))
                {
                    finish();
                    return;
                }
                try
                {
                    s = (CommentModel)JSON.parseObject(s, com/ximalaya/ting/android/model/zone/CommentModel);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                    s = null;
                }
                if (s == null)
                {
                    finish();
                    return;
                }
                resetData();
                setContentEditor(false);
                if (!mHasMore)
                {
                    loadComments(view, true);
                    return;
                }
            }
        }

        _cls30()
        {
            this$0 = PostFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls31 extends com.ximalaya.ting.android.b.a
    {

        final PostFragment this$0;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            super.onFinish();
            if (pd != null && pd.isShowing())
            {
                pd.cancel();
                pd = null;
            }
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            if (canGoon() && !TextUtils.isEmpty(s))
            {
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                    s = null;
                }
                if (s == null || s.getIntValue("ret") != 0)
                {
                    if (s == null)
                    {
                        s = null;
                    } else
                    {
                        s = s.getString("msg");
                    }
                    if (TextUtils.isEmpty(s))
                    {
                        s = "\u53D1\u8868\u56DE\u590D\u5931\u8D25";
                    } else
                    {
                        s = (new StringBuilder()).append("\u53D1\u8868\u56DE\u590D\u5931\u8D25:").append(s).toString();
                    }
                    showToast(s);
                    return;
                }
                showToast("\u53D1\u8868\u56DE\u590D\u6210\u529F");
                s = s.getString("result");
                if (TextUtils.isEmpty(s))
                {
                    finish();
                    return;
                }
                try
                {
                    s = (CommentModel)JSON.parseObject(s, com/ximalaya/ting/android/model/zone/CommentModel);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                    s = null;
                }
                if (s == null)
                {
                    finish();
                    return;
                }
                resetData();
                setContentEditor(false);
                if (!mHasMore)
                {
                    loadComments(view, true);
                    return;
                }
            }
        }

        _cls31()
        {
            this$0 = PostFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls15
        implements Runnable
    {

        final PostFragment this$0;

        public void run()
        {
            if (canGoon())
            {
                ((PullToRefreshListView)mListView).toRefreshing();
            }
        }

        _cls15()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls14
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final PostFragment this$0;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            if (radiogroup == mFilterHeaderFloat)
            {
                if (mFilterHeader.getCheckedRadioButtonId() != i)
                {
                    ((RadioButton)mFilterHeader.findViewById(i)).setChecked(true);
                }
                return;
            }
            if (mFilterHeaderFloat.getCheckedRadioButtonId() != i)
            {
                ((RadioButton)mFilterHeaderFloat.findViewById(i)).setChecked(true);
            }
            if (i != 0x7f0a0642) goto _L2; else goto _L1
_L1:
            mFilterByHost = false;
_L4:
            mDataList.clear();
            ((PullToRefreshListView)mListView).toRefreshing();
            return;
_L2:
            if (i == 0x7f0a0643)
            {
                mFilterByHost = true;
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        _cls14()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final PostFragment this$0;

        public void onClick(View view)
        {
            if (OneClickHelper.getInstance().onClick(view))
            {
                Bundle bundle = new Bundle();
                bundle.putLong("zoneId", mZoneId);
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment, bundle);
            }
        }

        _cls1()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final PostFragment this$0;

        public void onClick(View view)
        {
            if (OneClickHelper.getInstance().onClick(view))
            {
                Bundle bundle = new Bundle();
                bundle.putLong("zoneId", mZoneId);
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment, bundle);
            }
        }

        _cls2()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final PostFragment this$0;

        public void onClick(View view)
        {
            finishFragment();
        }

        _cls3()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final PostFragment this$0;

        public void onClick(View view)
        {
            finishFragment();
        }

        _cls4()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AbsListView.OnScrollListener
    {

        final PostFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || !mHasMore)
                    {
                        break label0;
                    }
                    if (!mIsLoadingComment)
                    {
                        loadComments(mListView, true);
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls5()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls6
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final PostFragment this$0;

        public void onRefresh()
        {
            loadPost();
        }

        _cls6()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.view.View.OnClickListener
    {

        final PostFragment this$0;

        public void onClick(View view)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            ((PullToRefreshListView)mListView).toRefreshing();
        }

        _cls7()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls8
        implements android.widget.AdapterView.OnItemClickListener
    {

        final PostFragment this$0;

        public void onItemClick(final AdapterView model, final View listViewItem, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i >= 0 && mAdapter.getCount() >= i + 1)
            {
                if ((model = (CommentModel)mAdapter.getItem(i)) != null)
                {
                    ArrayList arraylist = new ArrayList();
                    arraylist.add("\u67E5\u770B\u8D44\u6599");
                    arraylist.add("\u56DE\u590D");
                    arraylist.add("\u4E3E\u62A5");
                    if (model.isCanBeDeleted())
                    {
                        arraylist.add("\u5220\u9664");
                    }
                    class _cls1
                        implements android.widget.AdapterView.OnItemClickListener
                    {

                        final _cls8 this$1;
                        final View val$listViewItem;
                        final CommentModel val$model;

                        public void onItemClick(AdapterView adapterview, View view, int j, long l1)
                        {
                            if (mActionDlg != null)
                            {
                                mActionDlg.dismiss();
                            }
                            if (!OneClickHelper.getInstance().onClick(view))
                            {
                                return;
                            }
                            switch (j)
                            {
                            default:
                                return;

                            case 0: // '\0'
                                adapterview = new Bundle();
                                adapterview.putLong("toUid", model.getPoster().getUid());
                                adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(listViewItem));
                                startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, adapterview);
                                return;

                            case 1: // '\001'
                                if (!UserInfoMannage.hasLogined())
                                {
                                    adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                                    adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(listViewItem));
                                    startActivity(adapterview);
                                    return;
                                }
                                if (model.isMainPost())
                                {
                                    comment();
                                    return;
                                } else
                                {
                                    reply(model.getPoster().getNickname(), model.getId(), model.getGroupId());
                                    return;
                                }

                            case 2: // '\002'
                                if (!UserInfoMannage.hasLogined())
                                {
                                    adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                                    adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(listViewItem));
                                    startActivity(adapterview);
                                    return;
                                }
                                adapterview = new PostReportInfo();
                                adapterview.setReporterId(UserInfoMannage.getInstance().getUser().getUid().longValue());
                                adapterview.setReportedId(model.getPoster().getUid());
                                adapterview.setPostId(mPostId);
                                if (model.isMainPost())
                                {
                                    adapterview.setType(1);
                                } else
                                {
                                    adapterview.setType(2);
                                    adapterview.setCommentId(model.getId());
                                }
                                report(adapterview, listViewItem);
                                return;

                            case 3: // '\003'
                                break;
                            }
                            if (!UserInfoMannage.hasLogined())
                            {
                                adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(listViewItem));
                                startActivity(adapterview);
                                return;
                            } else
                            {
                                delete(listViewItem, model);
                                return;
                            }
                        }

                _cls1()
                {
                    this$1 = _cls8.this;
                    model = commentmodel;
                    listViewItem = view;
                    super();
                }
                    }

                    if (mActionDlg == null)
                    {
                        mActionDlg = new MenuDialog(getActivity(), arraylist);
                    } else
                    {
                        mActionDlg.setSelections(arraylist);
                    }
                    mActionDlg.setOnItemClickListener(new _cls1());
                    mActionDlg.show();
                    return;
                }
            }
        }

        _cls8()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls9
        implements android.widget.AdapterView.OnItemClickListener
    {

        final PostFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (mPicPathList.contains("add_default") && i == mPicPathList.size() - 1)
            {
                mMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getApplicationWindowToken(), 0);
                if (mDimBgDialog == null)
                {
                    createPopupDialog();
                }
                mDimBgDialog.show();
                return;
            } else
            {
                removePicDefault();
                adapterview = new Intent(mCon, com/ximalaya/ting/android/view/multiimgpicker/ImageZoomActivity);
                adapterview.putExtra("image_list", (Serializable)mPicPathList);
                adapterview.putExtra("current_img_position", i);
                startActivityForResult(adapterview, 2);
                return;
            }
        }

        _cls9()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls10
        implements com.ximalaya.ting.android.view.emotion.EmotionSelectorPanel.OnEditContentListener
    {

        final PostFragment this$0;

        public void insert(String s, Drawable drawable)
        {
            if (mContentEt.isFocused())
            {
                EmotionUtil.getInstance().insertEmotion(mContentEt, s, drawable);
            }
        }

        public void remove()
        {
            if (mContentEt.isFocused())
            {
                EmotionUtil.getInstance().deleteEmotion(mContentEt);
            }
        }

        _cls10()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls11
        implements android.view.View.OnClickListener
    {

        final PostFragment this$0;

        public void onClick(View view)
        {
            hideExtraViews();
            findViewById(0x7f0a0310).setVisibility(0);
        }

        _cls11()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls12
        implements android.view.View.OnFocusChangeListener
    {

        final PostFragment this$0;

        public void onFocusChange(View view, boolean flag)
        {
            if (flag)
            {
                mEmotionBtnIv.setEnabled(true);
                mPicBtnIv.setEnabled(true);
                hideExtraViews();
                findViewById(0x7f0a0310).setVisibility(0);
            }
        }

        _cls12()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls13
        implements android.view.View.OnClickListener
    {

        final PostFragment this$0;

        public void onClick(View view)
        {
            mMethodManager.hideSoftInputFromWindow(mContentEt.getWindowToken(), 0);
            findViewById(0x7f0a0310).setVisibility(8);
            class _cls1
                implements Runnable
            {

                final _cls13 this$1;

                public void run()
                {
                    if (canGoon())
                    {
                        mEmotionSelector.hideEmotionPanel();
                        mExtraLayout.setVisibility(8);
                    }
                }

                _cls1()
                {
                    this$1 = _cls13.this;
                    super();
                }
            }

            mContentEt.postDelayed(new _cls1(), 250L);
        }

        _cls13()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls17 extends com.ximalaya.ting.android.b.a
    {

        final PostFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mListView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoadingComment = false;
            ((PullToRefreshListView)mListView).onRefreshComplete();
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
        }

        public void onStart()
        {
            super.onStart();
            mHasMore = true;
        }

        public void onSuccess(String s)
        {
            Object obj1;
            obj1 = null;
            if (!canGoon())
            {
                return;
            }
            s = parseJSON(s);
            if (s == null)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
            if (s.getIntValue("ret") != 0)
            {
                s = s.getString("msg");
                if (!TextUtils.isEmpty(s))
                {
                    showToast(s);
                }
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
            Object obj = s.getJSONObject("result");
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_268;
            }
            String s1;
            s = ((JSONObject) (obj)).getString("comments");
            s1 = ((JSONObject) (obj)).getString("parentComments");
            if (TextUtils.isEmpty(s))
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
              goto _L1
            Exception exception;
            exception;
            s = null;
_L4:
            List list;
            exception.printStackTrace();
            exception = s;
            list = obj1;
              goto _L2
_L1:
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/zone/CommentModel);
            list = obj1;
            exception = s;
            if (TextUtils.isEmpty(s1)) goto _L2; else goto _L3
_L3:
            list = JSON.parseArray(s1, com/ximalaya/ting/android/model/zone/CommentModel);
            exception = s;
_L2:
            if (exception == null || exception.size() == 0)
            {
                mHasMore = false;
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
            if (exception.size() < 30)
            {
                mHasMore = false;
            }
            setCommentParent(exception, list);
            mDataList.addAll(exception);
            mAdapter.notifyDataSetChanged();
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            return;
            exception;
              goto _L4
            exception = null;
            list = obj1;
              goto _L2
        }

        _cls17()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls16 extends com.ximalaya.ting.android.b.a
    {

        final PostFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mListView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoadingMain = false;
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
            ((PullToRefreshListView)mListView).onRefreshComplete();
        }

        public void onSuccess(String s)
        {
            int i;
            while (!canGoon() || s == null) 
            {
                return;
            }
            s = parseJSON(s);
            if (s == null)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                ((PullToRefreshListView)mListView).onRefreshComplete();
                return;
            }
            i = s.getIntValue("ret");
            if (i == 0) goto _L2; else goto _L1
_L1:
            s = s.getString("msg");
            if (i != 311) goto _L4; else goto _L3
_L3:
            showToast("\u5E16\u5B50\u4E0D\u5B58\u5728\u6216\u8005\u5DF2\u7ECF\u88AB\u5220\u9664");
_L5:
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            ((PullToRefreshListView)mListView).onRefreshComplete();
            return;
_L4:
            if (!TextUtils.isEmpty(s))
            {
                showToast(s);
            }
            if (true) goto _L5; else goto _L2
_L2:
            s = s.getString("result");
            if (TextUtils.isEmpty(s))
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                ((PullToRefreshListView)mListView).onRefreshComplete();
                return;
            }
            try
            {
                mMainPost = (PostModel)JSON.parseObject(s, com/ximalaya/ting/android/model/zone/PostModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            }
            if (mMainPost != null)
            {
                mDataList.clear();
                mDataList.add(CommentModel.convert(mMainPost));
                mAdapter.notifyDataSetChanged();
                loadComments(mListView, false);
                return;
            } else
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                ((PullToRefreshListView)mListView).onRefreshComplete();
                return;
            }
        }

        _cls16()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls24
        implements Runnable
    {

        final PostFragment this$0;

        public void run()
        {
            if (canGoon())
            {
                mMethodManager.showSoftInput(mContentEt, 0);
            }
        }

        _cls24()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls18
        implements Runnable
    {

        final PostFragment this$0;

        public void run()
        {
            if (canGoon())
            {
                showEmotionOrPic(true);
                mEmotionSelector.showEmotionGrid();
            }
        }

        _cls18()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls35
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final PostFragment this$0;

        public void onExecute()
        {
            resetData();
            mContentEt.setText(null);
            getActivity().onBackPressed();
        }

        _cls35()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls32
        implements Runnable
    {

        final PostFragment this$0;

        public void run()
        {
            if (canGoon())
            {
                showEmotionOrPic(false);
            }
        }

        _cls32()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls33
        implements Runnable
    {

        final PostFragment this$0;

        public void run()
        {
            if (!canGoon())
            {
                return;
            }
            if (mDimBgDialog == null)
            {
                createPopupDialog();
            }
            mDimBgDialog.show();
        }

        _cls33()
        {
            this$0 = PostFragment.this;
            super();
        }
    }


    private class _cls34
        implements Runnable
    {

        final PostFragment this$0;

        public void run()
        {
            if (canGoon())
            {
                showEmotionOrPic(false);
            }
        }

        _cls34()
        {
            this$0 = PostFragment.this;
            super();
        }
    }

}
