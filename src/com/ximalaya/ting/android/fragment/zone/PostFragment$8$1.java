// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.zone.CommentModel;
import com.ximalaya.ting.android.model.zone.PostReportInfo;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            PostFragment

class val.listViewItem
    implements android.widget.mClickListener
{

    final val.model this$1;
    final View val$listViewItem;
    final CommentModel val$model;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        if (PostFragment.access$800(_fld0) != null)
        {
            PostFragment.access$800(_fld0).dismiss();
        }
        if (!OneClickHelper.getInstance().onClick(view))
        {
            return;
        }
        switch (i)
        {
        default:
            return;

        case 0: // '\0'
            adapterview = new Bundle();
            adapterview.putLong("toUid", val$model.getPoster().getUid());
            adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(val$listViewItem));
            startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, adapterview);
            return;

        case 1: // '\001'
            if (!UserInfoMannage.hasLogined())
            {
                adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(val$listViewItem));
                startActivity(adapterview);
                return;
            }
            if (val$model.isMainPost())
            {
                comment();
                return;
            } else
            {
                reply(val$model.getPoster().getNickname(), val$model.getId(), val$model.getGroupId());
                return;
            }

        case 2: // '\002'
            if (!UserInfoMannage.hasLogined())
            {
                adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(val$listViewItem));
                startActivity(adapterview);
                return;
            }
            adapterview = new PostReportInfo();
            adapterview.setReporterId(UserInfoMannage.getInstance().getUser().getUid().longValue());
            adapterview.setReportedId(val$model.getPoster().getUid());
            adapterview.setPostId(PostFragment.access$900(_fld0));
            if (val$model.isMainPost())
            {
                adapterview.setType(1);
            } else
            {
                adapterview.setType(2);
                adapterview.setCommentId(val$model.getId());
            }
            report(adapterview, val$listViewItem);
            return;

        case 3: // '\003'
            break;
        }
        if (!UserInfoMannage.hasLogined())
        {
            adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
            adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(val$listViewItem));
            startActivity(adapterview);
            return;
        } else
        {
            PostFragment.access$1000(_fld0, val$listViewItem, val$model);
            return;
        }
    }

    del()
    {
        this$1 = final_del;
        val$model = commentmodel;
        val$listViewItem = View.this;
        super();
    }
}
