// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.zone.ReplyMessageAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.zone.ReplyMessageModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneConstants

public class ZoneMessageFragment extends BaseListFragment
{

    private static final int PAGE_SIZE = 30;
    private ReplyMessageAdapter mAdapter;
    private ArrayList mDataList;
    private boolean mHasMore;
    private boolean mIsLoading;
    private int mPageId;

    public ZoneMessageFragment()
    {
        mDataList = new ArrayList();
        mPageId = 1;
    }

    private void initData()
    {
        mPageId = 1;
        ((PullToRefreshListView)mListView).toRefreshing();
    }

    private void initListener()
    {
        ((PullToRefreshListView)mListView).setOnScrollListener(new _cls1());
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls2());
        mFooterViewLoading.setOnClickListener(new _cls3());
        mListView.setOnItemClickListener(new _cls4());
    }

    private void initViews()
    {
        ((TextView)findViewById(0x7f0a00ae)).setText("\u5708\u5B50\u6D88\u606F");
        mAdapter = new ReplyMessageAdapter(mCon, mDataList);
        ((PullToRefreshListView)mListView).setAdapter(mAdapter);
    }

    private void loadData(final boolean pullFromTop)
    {
        if (mIsLoading)
        {
            return;
        }
        RequestParams requestparams;
        if (!pullFromTop)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        } else
        {
            mPageId = 1;
        }
        mIsLoading = true;
        requestparams = new RequestParams();
        requestparams.put("pageId", mPageId);
        requestparams.put("pageSize", 30);
        f.a().a(ZoneConstants.URL_REPLY_MSG, requestparams, DataCollectUtil.getDataFromView(mListView), new _cls5());
    }

    private void parseData(boolean flag, String s)
    {
        s = parseJSON(s);
        if (s == null)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            return;
        }
        if (s.getIntValue("ret") != 0)
        {
            s = s.getString("msg");
            if (!TextUtils.isEmpty(s))
            {
                showToast(s);
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
            return;
        }
        String s1 = s.getString("list");
        if (TextUtils.isEmpty(s1))
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            return;
        }
        int i = s.getIntValue("maxPageId");
        try
        {
            s = JSON.parseArray(s1, com/ximalaya/ting/android/model/zone/ReplyMessageModel);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null || s.size() == 0)
        {
            mHasMore = false;
            if (flag)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            } else
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
        }
        if (flag)
        {
            mDataList.clear();
        }
        mDataList.addAll(s);
        if (i > mPageId)
        {
            mPageId = mPageId + 1;
        } else
        {
            mHasMore = false;
        }
        mAdapter.notifyDataSetChanged();
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
    }

    private JSONObject parseJSON(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            return null;
        }
        return s;
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initViews();
        initListener();
        initData();
        ToolUtil.onEvent(mCon, "zone_post_message");
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300f1, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onResume()
    {
        super.onResume();
    }



/*
    static boolean access$002(ZoneMessageFragment zonemessagefragment, boolean flag)
    {
        zonemessagefragment.mHasMore = flag;
        return flag;
    }

*/



/*
    static boolean access$102(ZoneMessageFragment zonemessagefragment, boolean flag)
    {
        zonemessagefragment.mIsLoading = flag;
        return flag;
    }

*/





    private class _cls1
        implements android.widget.AbsListView.OnScrollListener
    {

        final ZoneMessageFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || !mHasMore)
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        loadData(false);
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls1()
        {
            this$0 = ZoneMessageFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final ZoneMessageFragment this$0;

        public void onRefresh()
        {
            loadData(true);
        }

        _cls2()
        {
            this$0 = ZoneMessageFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final ZoneMessageFragment this$0;

        public void onClick(View view)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            ((PullToRefreshListView)mListView).toRefreshing();
        }

        _cls3()
        {
            this$0 = ZoneMessageFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemClickListener
    {

        final ZoneMessageFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            int j = i - mListView.getHeaderViewsCount();
            if (i < 0 || j < 0 || mDataList.size() < j + 1 || !OneClickHelper.getInstance().onClick(view))
            {
                return;
            } else
            {
                adapterview = new Bundle();
                adapterview.putLong("zoneId", ((ReplyMessageModel)mDataList.get(j)).getZoneId());
                adapterview.putLong("postId", ((ReplyMessageModel)mDataList.get(j)).getPostId());
                adapterview.putLong("timeline", ((ReplyMessageModel)mDataList.get(j)).getTimeline());
                adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/zone/CommentListFragment, adapterview);
                return;
            }
        }

        _cls4()
        {
            this$0 = ZoneMessageFragment.this;
            super();
        }
    }


    private class _cls5 extends a
    {

        final ZoneMessageFragment this$0;
        final boolean val$pullFromTop;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
            ((PullToRefreshListView)mListView).onRefreshComplete();
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
        }

        public void onStart()
        {
            super.onStart();
            mHasMore = true;
        }

        public void onSuccess(final String responseContent)
        {
            if (!canGoon())
            {
                return;
            } else
            {
                class _cls1
                    implements MyCallback
                {

                    final _cls5 this$1;
                    final String val$responseContent;

                    public void execute()
                    {
                        parseData(pullFromTop, responseContent);
                    }

                _cls1()
                {
                    this$1 = _cls5.this;
                    responseContent = s;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                return;
            }
        }

        _cls5()
        {
            this$0 = ZoneMessageFragment.this;
            pullFromTop = flag;
            super();
        }
    }

}
