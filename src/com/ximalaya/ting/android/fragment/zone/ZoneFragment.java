// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.activity.zone.PostCreateActivity;
import com.ximalaya.ting.android.adapter.zone.PostAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.model.zone.RoleInfo;
import com.ximalaya.ting.android.model.zone.ZoneModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.BlurableImageView;
import com.ximalaya.ting.android.view.RoundedHexagonImageView;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneConstants, ZoneDetailFragment

public class ZoneFragment extends BaseListFragment
    implements android.view.View.OnClickListener
{
    private class TopPostAdapter extends BaseAdapter
    {

        private ArrayList mDataList;
        final ZoneFragment this$0;

        public int getCount()
        {
            if (mDataList == null)
            {
                return 0;
            } else
            {
                return mDataList.size();
            }
        }

        public Object getItem(int i)
        {
            if (mDataList == null)
            {
                return null;
            } else
            {
                return (PostModel)mDataList.get(i);
            }
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            Object obj = (PostModel)mDataList.get(i);
            TextView textview;
            if (view == null)
            {
                view = View.inflate(mCon, 0x7f030132, null);
                viewgroup = new ViewHolder(null);
                view.setTag(viewgroup);
                viewgroup.postNameTv = (TextView)view.findViewById(0x7f0a04dd);
                viewgroup.border = view.findViewById(0x7f0a00a8);
            } else
            {
                viewgroup = (ViewHolder)view.getTag();
            }
            textview = ((ViewHolder) (viewgroup)).postNameTv;
            if (((PostModel) (obj)).getTitle() == null)
            {
                obj = "";
            } else
            {
                obj = ((PostModel) (obj)).getTitle();
            }
            textview.setText(((CharSequence) (obj)));
            if (i + 1 == mDataList.size())
            {
                ((ViewHolder) (viewgroup)).border.setVisibility(8);
                return view;
            } else
            {
                ((ViewHolder) (viewgroup)).border.setVisibility(0);
                return view;
            }
        }

        public TopPostAdapter(ArrayList arraylist)
        {
            this$0 = ZoneFragment.this;
            super();
            mDataList = new ArrayList();
            mDataList = arraylist;
        }
    }

    private class ViewHolder
    {

        View border;
        TextView postNameTv;
        final ZoneFragment this$0;

        private ViewHolder()
        {
            this$0 = ZoneFragment.this;
            super();
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private static final int INTENT_NEW_POST = 1;
    private static final int PAGE_SIZE = 30;
    private Bitmap floatViewBg;
    private PostAdapter mAdapter;
    private BlurableImageView mCoverBg;
    private TextView mFollowTv;
    private boolean mHasMore;
    private View mHeaderLayout;
    private TextView mIntroTv;
    private boolean mIsLoading;
    private TextView mMemberNumTv;
    private TextView mNameTv;
    private ArrayList mPostList;
    private TextView mPostNumTv;
    private ProgressBar mProgressBar;
    private LinearLayout mProgressLayout;
    private RelativeLayout mTitleBarLayout;
    private RelativeLayout mTitleBarLayoutFloat;
    private TextView mTitleTv;
    private TextView mTitleTvFloat;
    private ArrayList mTopList;
    private TopPostAdapter mTopPostAdapter;
    private ListView mTopPostListView;
    private ZoneModel mZone;
    private RoundedHexagonImageView mZoneCoverImg;
    private View mZoneDetailBorder;
    private RelativeLayout mZoneDetailLayout;
    private RelativeLayout mZoneDetailMainLayout;
    private long mZoneId;

    public ZoneFragment()
    {
        mTopList = new ArrayList();
        mPostList = new ArrayList();
    }

    private void buildFloatImg()
    {
        if (floatViewBg == null && canGoon()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if ((mCoverBg.getDrawable() instanceof BitmapDrawable) && (((BitmapDrawable)mCoverBg.getDrawable()).getBitmap() == null || ((BitmapDrawable)mCoverBg.getDrawable()).getBitmap().isRecycled())) goto _L1; else goto _L3
_L3:
        mCoverBg.buildDrawingCache();
        Bitmap bitmap = mCoverBg.getDrawingCache();
        int ai[] = new int[2];
        mCoverBg.getLocationInWindow(ai);
        int ai1[] = new int[2];
        mCoverBg.getLocationInWindow(ai1);
        floatViewBg = Bitmap.createBitmap(bitmap, 0, ai[1] - ai1[1], ToolUtil.getScreenWidth(mCon), (int)getResources().getDimension(0x7f080025));
        ((ImageView)mTitleBarLayoutFloat.findViewById(0x7f0a074c)).setImageBitmap(floatViewBg);
        return;
        Exception exception;
        exception;
        Logger.e("zone", "blurHeaderImage", exception);
        return;
    }

    private void buildGroupByIsTop(List list)
    {
        mPostList.clear();
        mTopList.clear();
        int j = list.size();
        int i = 0;
        while (i != j) 
        {
            if (((PostModel)list.get(i)).isTop())
            {
                mTopList.add(list.get(i));
            } else
            {
                mPostList.add(list.get(i));
            }
            i++;
        }
    }

    private void doFollow(final View view)
    {
        mProgressBar.setVisibility(0);
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", (new StringBuilder()).append("").append(mZone.getId()).toString());
        f.a().b(ZoneConstants.URL_MY_ZONES, requestparams, DataCollectUtil.getDataFromView(view), new _cls11());
    }

    private void doUnfollow(final View view)
    {
        mProgressBar.setVisibility(0);
        String s = (new StringBuilder()).append(ZoneConstants.URL_MY_ZONES).append("/").append(mZone.getId()).toString();
        f.a().a(s, DataCollectUtil.getDataFromView(view), new _cls12());
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            mZoneId = bundle.getLong("zoneId");
        }
        long l = getAnimationLeftTime();
        if (mTopPostListView.getVisibility() == 0)
        {
            mTopPostListView.setVisibility(8);
        }
        if (l > 0L)
        {
            fragmentBaseContainerView.postDelayed(new _cls7(), l);
            return;
        } else
        {
            ((PullToRefreshListView)mListView).onRefresh();
            return;
        }
    }

    private void initHeaderViews()
    {
        mHeaderLayout = LayoutInflater.from(mCon).inflate(0x7f0301ff, mListView, false);
        mTitleBarLayout = (RelativeLayout)mHeaderLayout.findViewById(0x7f0a0202);
        mTitleTv = (TextView)mHeaderLayout.findViewById(0x7f0a00ae);
        mTitleTv.setText("\u5708\u5B50");
        mCoverBg = (BlurableImageView)mHeaderLayout.findViewById(0x7f0a03ec);
        mZoneDetailMainLayout = (RelativeLayout)mHeaderLayout.findViewById(0x7f0a0747);
        mZoneDetailLayout = (RelativeLayout)mHeaderLayout.findViewById(0x7f0a0749);
        mZoneDetailBorder = mHeaderLayout.findViewById(0x7f0a074b);
        mNameTv = (TextView)mHeaderLayout.findViewById(0x7f0a03f1);
        mZoneCoverImg = (RoundedHexagonImageView)mHeaderLayout.findViewById(0x7f0a03ef);
        markImageView(mCoverBg);
        markImageView(mZoneCoverImg);
        mIntroTv = (TextView)mHeaderLayout.findViewById(0x7f0a03f2);
        mMemberNumTv = (TextView)mHeaderLayout.findViewById(0x7f0a03f3);
        mPostNumTv = (TextView)mHeaderLayout.findViewById(0x7f0a03f4);
        mFollowTv = (TextView)mHeaderLayout.findViewById(0x7f0a03f5);
        mHeaderLayout.findViewById(0x7f0a00a8).setVisibility(8);
        mTopPostListView = (ListView)mHeaderLayout.findViewById(0x7f0a074a);
        mTopPostAdapter = new TopPostAdapter(mTopList);
        mTopPostListView.setAdapter(mTopPostAdapter);
        ((PullToRefreshListView)mListView).addHeaderView(mHeaderLayout);
    }

    private void initListener()
    {
        ((PullToRefreshListView)mListView).setOnScrollListener(new _cls1());
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls2());
        ((PullToRefreshListView)mListView).setOnFloatViewVisibilityChangedCallback(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
        mListView.setOnItemClickListener(new _cls5());
        mFollowTv.setOnClickListener(this);
        mZoneDetailLayout.setOnClickListener(this);
        mZoneCoverImg.setOnClickListener(this);
        mTitleBarLayout.findViewById(0x7f0a0748).setOnClickListener(this);
        mTitleBarLayoutFloat.findViewById(0x7f0a0748).setOnClickListener(this);
        mTitleBarLayout.findViewById(0x7f0a007b).setOnClickListener(this);
        mTitleBarLayoutFloat.findViewById(0x7f0a007b).setOnClickListener(this);
        mTopPostListView.setOnItemClickListener(new _cls6());
    }

    private void initViews()
    {
        mProgressBar = (ProgressBar)findViewById(0x7f0a0091);
        initHeaderViews();
        mTitleBarLayoutFloat = (RelativeLayout)LayoutInflater.from(getActivity()).inflate(0x7f030200, null);
        mTitleTvFloat = (TextView)mTitleBarLayoutFloat.findViewById(0x7f0a00ae);
        mProgressLayout = (LinearLayout)findViewById(0x7f0a012c);
        mProgressLayout.setVisibility(0);
        mTitleTvFloat.setText("\u5708\u5B50");
        ((PullToRefreshListView)mListView).setFloatHeadView(mTitleBarLayoutFloat);
        ((RelativeLayout)findViewById(0x7f0a005e)).addView(mTitleBarLayoutFloat);
        mAdapter = new PostAdapter(this, mCon, mPostList, false);
        ((PullToRefreshListView)mListView).setAdapter(mAdapter);
    }

    private void loadData(View view, final boolean pullFromEnd)
    {
        if (mIsLoading)
        {
            return;
        }
        if (pullFromEnd)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        }
        mIsLoading = true;
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", mZoneId);
        if (!mPostList.isEmpty() && pullFromEnd)
        {
            requestparams.put("timeline", ((PostModel)mPostList.get(mPostList.size() - 1)).getTimeline());
        } else
        {
            requestparams.put("timeline", 0);
        }
        requestparams.put("timelineType", "0");
        requestparams.put("maxSizeOfPosts", 30);
        f.a().a(ZoneConstants.URL_POSTS, requestparams, DataCollectUtil.getDataFromView(view), new _cls10());
    }

    private void loadZoneDetail()
    {
        String s = (new StringBuilder()).append(ZoneConstants.URL_ZONES).append("/").append(mZoneId).toString();
        f.a().a(s, new RequestParams(), DataCollectUtil.getDataFromView(mHeaderLayout), new _cls8());
    }

    private JSONObject parseJSON(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            return null;
        }
        return s;
    }

    private void refreshDetail()
    {
        if (mZone != null)
        {
            if (!TextUtils.isEmpty(mZone.getDisplayName()))
            {
                mTitleTv.setText(mZone.getDisplayName());
                mTitleTvFloat.setText(mZone.getDisplayName());
            }
            if (mZoneDetailMainLayout.getVisibility() != 0)
            {
                mZoneDetailMainLayout.setVisibility(0);
            }
            TextView textview = mNameTv;
            Object obj;
            if (mZone.getDisplayName() == null)
            {
                obj = "";
            } else
            {
                obj = mZone.getDisplayName();
            }
            textview.setText(((CharSequence) (obj)));
            ImageManager2.from(mCon).displayImage(mZoneCoverImg, mZone.getImageUrl(), 0x7f0200db, new _cls9());
            textview = mIntroTv;
            if (mZone.getDescription() == null)
            {
                obj = "";
            } else
            {
                obj = mZone.getDescription();
            }
            textview.setText(((CharSequence) (obj)));
            mMemberNumTv.setText((new StringBuilder()).append("\u6210\u5458").append(StringUtil.getFriendlyNumStr(mZone.getNumOfMembers())).toString());
            mPostNumTv.setText((new StringBuilder()).append("\u5E16\u5B50").append(StringUtil.getFriendlyNumStr(mZone.getNumOfPosts())).toString());
            obj = UserInfoMannage.getInstance().getUser();
            if (obj != null && mZone.getOwner() != null && ((LoginInfoModel) (obj)).uid == mZone.getOwner().getUid())
            {
                mFollowTv.setVisibility(8);
                return;
            }
            mFollowTv.setVisibility(0);
            if (mZone.getMyZone() != null && mZone.getMyZone().isJoint())
            {
                refreshForFollow(false);
                return;
            } else
            {
                refreshForFollow(true);
                return;
            }
        } else
        {
            mZoneDetailMainLayout.setVisibility(8);
            return;
        }
    }

    private void refreshForFollow(boolean flag)
    {
        if (flag)
        {
            mFollowTv.setCompoundDrawablesWithIntrinsicBounds(0x7f0205dc, 0, 0, 0);
            mFollowTv.setPadding(ToolUtil.dp2px(mCon, 10F), 0, 0, 0);
            if (mCon != null)
            {
                mFollowTv.setBackgroundResource(0x7f020103);
            }
            mFollowTv.setText("\u52A0\u5165");
            return;
        }
        mFollowTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        mFollowTv.setPadding(0, 0, 0, 0);
        if (mCon != null)
        {
            mFollowTv.setBackgroundResource(0x7f020102);
        }
        mFollowTv.setText("\u5DF2\u52A0\u5165");
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initViews();
        initData();
        initListener();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        i;
        JVM INSTR tableswitch 1 1: default 20
    //                   1 21;
           goto _L1 _L2
_L1:
        return;
_L2:
        if (j == -1)
        {
            ((PullToRefreshListView)mListView).onRefresh();
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public void onClick(final View v)
    {
        if (!OneClickHelper.getInstance().onClick(v)) goto _L2; else goto _L1
_L1:
        v.getId();
        JVM INSTR lookupswitch 5: default 64
    //                   2131361915: 150
    //                   2131362799: 186
    //                   2131362805: 225
    //                   2131363656: 65
    //                   2131363657: 186;
           goto _L2 _L3 _L4 _L5 _L6 _L4
_L2:
        return;
_L6:
        if (!UserInfoMannage.hasLogined())
        {
            Intent intent = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
            intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(v));
            startActivity(intent);
            return;
        } else
        {
            Intent intent1 = new Intent(mCon, com/ximalaya/ting/android/activity/zone/PostCreateActivity);
            intent1.putExtra("zoneId", mZoneId);
            intent1.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(v));
            startActivityForResult(intent1, 1);
            return;
        }
_L3:
        if (isAdded())
        {
            if (getActivity() instanceof MainTabActivity2)
            {
                ((MainTabActivity2)getActivity()).onBack();
                return;
            } else
            {
                getActivity().finish();
                return;
            }
        }
        if (true)
        {
            continue; /* Loop/switch isn't completed */
        }
_L4:
        Bundle bundle = new Bundle();
        bundle.putSerializable("zone_data", mZone);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(v));
        startFragment(com/ximalaya/ting/android/fragment/zone/ZoneDetailFragment, bundle);
        return;
_L5:
        if (!UserInfoMannage.hasLogined())
        {
            Intent intent2 = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
            intent2.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(v));
            startActivity(intent2);
            return;
        }
        if (mZone != null && mZone.getMyZone() != null)
        {
            if (mZone.getMyZone().isJoint())
            {
                (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u8981\u9000\u51FA\u6B64\u5708\u5B50\uFF1F").setOkBtn(new _cls13()).showConfirm();
                return;
            } else
            {
                doFollow(v);
                return;
            }
        }
        if (true) goto _L2; else goto _L7
_L7:
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300ec, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (mCoverBg != null)
        {
            mCoverBg.destroyDrawingCache();
        }
        if (floatViewBg != null && !floatViewBg.isRecycled())
        {
            floatViewBg.recycle();
            floatViewBg = null;
        }
    }

    public void onResume()
    {
        super.onResume();
    }



/*
    static boolean access$002(ZoneFragment zonefragment, boolean flag)
    {
        zonefragment.mHasMore = flag;
        return flag;
    }

*/




/*
    static boolean access$102(ZoneFragment zonefragment, boolean flag)
    {
        zonefragment.mIsLoading = flag;
        return flag;
    }

*/





/*
    static Bitmap access$1402(ZoneFragment zonefragment, Bitmap bitmap)
    {
        zonefragment.floatViewBg = bitmap;
        return bitmap;
    }

*/















/*
    static ZoneModel access$502(ZoneFragment zonefragment, ZoneModel zonemodel)
    {
        zonefragment.mZone = zonemodel;
        return zonemodel;
    }

*/





    private class _cls11 extends a
    {

        final ZoneFragment this$0;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            mProgressBar.setVisibility(8);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            Object obj = null;
            if (TextUtils.isEmpty(s))
            {
                return;
            }
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = obj;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF";
                } else
                {
                    s = (new StringBuilder()).append("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                mZone.getMyZone().setIsJoint(true);
                refreshForFollow(false);
                showToast("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u6210\u529F");
                return;
            }
        }

        _cls11()
        {
            this$0 = ZoneFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls12 extends a
    {

        final ZoneFragment this$0;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            mProgressBar.setVisibility(8);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            Object obj = null;
            if (TextUtils.isEmpty(s))
            {
                return;
            }
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = obj;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u9000\u51FA\u5708\u5B50\u53D1\u751F\u9519\u8BEF";
                } else
                {
                    s = (new StringBuilder()).append("\u9000\u51FA\u5708\u5B50\u53D1\u751F\u9519\u8BEF:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                mZone.getMyZone().setIsJoint(false);
                refreshForFollow(true);
                showToast("\u9000\u51FA\u5708\u5B50\u6210\u529F");
                return;
            }
        }

        _cls12()
        {
            this$0 = ZoneFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls7
        implements Runnable
    {

        final ZoneFragment this$0;

        public void run()
        {
            if (canGoon())
            {
                ((PullToRefreshListView)mListView).onRefresh();
            }
        }

        _cls7()
        {
            this$0 = ZoneFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.widget.AbsListView.OnScrollListener
    {

        final ZoneFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || !mHasMore)
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        loadData(mListView, true);
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls1()
        {
            this$0 = ZoneFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final ZoneFragment this$0;

        public void onRefresh()
        {
            loadZoneDetail();
            loadData(mListView, false);
        }

        _cls2()
        {
            this$0 = ZoneFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnFloatVisibilityChangedCallback
    {

        final ZoneFragment this$0;

        public void OnFloatVisibilityChanged(View view, int i)
        {
            if (i == 0)
            {
                buildFloatImg();
            }
        }

        _cls3()
        {
            this$0 = ZoneFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final ZoneFragment this$0;

        public void onClick(View view)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            if (mZone == null)
            {
                ((PullToRefreshListView)mListView).toRefreshing();
                return;
            } else
            {
                loadData(view, false);
                return;
            }
        }

        _cls4()
        {
            this$0 = ZoneFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AdapterView.OnItemClickListener
    {

        final ZoneFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            int j = i - mListView.getHeaderViewsCount();
            if (i < 0 || j < 0 || mPostList.size() < j + 1 || !OneClickHelper.getInstance().onClick(view))
            {
                return;
            } else
            {
                adapterview = new Bundle();
                adapterview.putLong("zoneId", mZoneId);
                adapterview.putLong("postId", ((PostModel)mPostList.get(j)).getId());
                adapterview.putLong("hostUid", ((PostModel)mPostList.get(j)).getPoster().getUid());
                adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/zone/PostFragment, adapterview);
                return;
            }
        }

        _cls5()
        {
            this$0 = ZoneFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.widget.AdapterView.OnItemClickListener
    {

        final ZoneFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            while (!OneClickHelper.getInstance().onClick(view) || i < 0 || mTopList.size() < i + 1) 
            {
                return;
            }
            adapterview = new Bundle();
            adapterview.putLong("zoneId", ((PostModel)mTopList.get(i)).getZoneId());
            adapterview.putLong("postId", ((PostModel)mTopList.get(i)).getId());
            adapterview.putLong("hostUid", ((PostModel)mTopList.get(i)).getPoster().getUid());
            adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/PostFragment, adapterview);
        }

        _cls6()
        {
            this$0 = ZoneFragment.this;
            super();
        }
    }


    private class _cls10 extends a
    {

        final ZoneFragment this$0;
        final boolean val$pullFromEnd;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mListView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
            ((PullToRefreshListView)mListView).onRefreshComplete();
            if (mProgressLayout.getVisibility() == 0)
            {
                mProgressLayout.setVisibility(8);
            }
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
            mTopPostListView.setVisibility(8);
        }

        public void onStart()
        {
            super.onStart();
            mHasMore = true;
        }

        public void onSuccess(String s)
        {
            if (!canGoon())
            {
                return;
            }
            s = parseJSON(s);
            if (s == null)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
            if (s.getIntValue("ret") != 0)
            {
                s = s.getString("msg");
                if (!TextUtils.isEmpty(s))
                {
                    showToast(s);
                }
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
            s = s.getString("result");
            if (TextUtils.isEmpty(s))
            {
                mHasMore = false;
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
            try
            {
                s = JSON.parseArray(s, com/ximalaya/ting/android/model/zone/PostModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = null;
            }
            if (s == null || s.size() == 0)
            {
                mHasMore = false;
                if (pullFromEnd)
                {
                    showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                    return;
                } else
                {
                    showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                    return;
                }
            }
            if (s.size() < 30)
            {
                mHasMore = false;
            }
            if (pullFromEnd)
            {
                mPostList.addAll(s);
            } else
            {
                buildGroupByIsTop(s);
            }
            if (mTopList.isEmpty())
            {
                mTopPostListView.setVisibility(8);
                mZoneDetailBorder.setVisibility(8);
            } else
            {
                mZoneDetailBorder.setVisibility(0);
                mTopPostListView.setVisibility(0);
            }
            ViewUtil.setListViewHeight(mTopPostListView);
            mTopPostAdapter.notifyDataSetChanged();
            mAdapter.notifyDataSetChanged();
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls10()
        {
            this$0 = ZoneFragment.this;
            pullFromEnd = flag;
            super();
        }
    }


    private class _cls8 extends a
    {

        final ZoneFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mHeaderLayout);
        }

        public void onFinish()
        {
            super.onFinish();
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            mZoneDetailMainLayout.setVisibility(8);
            ((PullToRefreshListView)mListView).onRefreshComplete();
        }

        public void onSuccess(String s)
        {
_L2:
            return;
            if (!canGoon() || s == null) goto _L2; else goto _L1
_L1:
            s = parseJSON(s);
            if (s == null) goto _L2; else goto _L3
_L3:
            int i = s.getIntValue("ret");
            if (i == 0) goto _L5; else goto _L4
_L4:
            s = s.getString("msg");
            if (i != 310)
            {
                continue; /* Loop/switch isn't completed */
            }
            try
            {
                showToast("\u5708\u5B50\u4E0D\u5B58\u5728\u6216\u8005\u5DF2\u7ECF\u88AB\u5220\u9664");
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            }
_L8:
            refreshDetail();
            return;
            if (TextUtils.isEmpty(s)) goto _L2; else goto _L6
_L6:
            showToast(s);
            return;
_L5:
            s = s.getString("result");
            if (TextUtils.isEmpty(s)) goto _L2; else goto _L7
_L7:
            mZone = (ZoneModel)JSON.parseObject(s, com/ximalaya/ting/android/model/zone/ZoneModel);
              goto _L8
        }

        _cls8()
        {
            this$0 = ZoneFragment.this;
            super();
        }
    }


    private class _cls9
        implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
    {

        final ZoneFragment this$0;

        public void onCompleteDisplay(String s, Bitmap bitmap)
        {
            if (mCon == null)
            {
                return;
            }
            Bitmap bitmap1 = ImageManager2.from(mCon).getFromMemCache((new StringBuilder()).append(s).append("/blur").toString());
            if (bitmap1 != null)
            {
                mCoverBg.setImageBitmap(bitmap1);
            } else
            if (bitmap != null)
            {
                mCoverBg.blur(new BitmapDrawable(mCon.getResources(), bitmap), (new StringBuilder()).append(s).append("/blur").toString(), true);
            } else
            {
                mCoverBg.setImageDrawable(new ColorDrawable(Color.parseColor("#b3202332")));
                mCoverBg.setResourceUrl(null);
            }
            floatViewBg = null;
        }

        _cls9()
        {
            this$0 = ZoneFragment.this;
            super();
        }
    }


    private class _cls13
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final ZoneFragment this$0;
        final View val$v;

        public void onExecute()
        {
            doUnfollow(v);
        }

        _cls13()
        {
            this$0 = ZoneFragment.this;
            v = view;
            super();
        }
    }

}
