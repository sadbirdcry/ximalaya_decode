// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.text.TextUtils;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.adapter.zone.CommentAdapter;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.zone.CommentModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            CommentListFragment

class val.firstLoad extends a
{

    final CommentListFragment this$0;
    final boolean val$firstLoad;
    final boolean val$pullFromEnd;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, mListView);
    }

    public void onFinish()
    {
        super.onFinish();
        CommentListFragment.access$3402(CommentListFragment.this, false);
        ((PullToRefreshListView)mListView).onRefreshComplete();
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        showFooterView(com.ximalaya.ting.android.fragment.View.NO_CONNECTION);
    }

    public void onStart()
    {
        super.onStart();
    }

    public void onSuccess(String s)
    {
        Object obj1 = null;
        if (canGoon()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        s = CommentListFragment.access$2700(CommentListFragment.this, s);
        if (s == null)
        {
            CommentListFragment commentlistfragment = CommentListFragment.this;
            if (val$pullFromEnd)
            {
                s = com.ximalaya.ting.android.fragment.View.NO_DATA;
            } else
            {
                s = com.ximalaya.ting.android.fragment.View.HIDE_ALL;
            }
            commentlistfragment.showFooterView(s);
            return;
        }
        if (s.getIntValue("ret") == 0)
        {
            break MISSING_BLOCK_LABEL_173;
        }
        obj = s.getString("msg");
        if (s.getIntValue("ret") == 320)
        {
            if (CommentListFragment.access$900(CommentListFragment.this).isEmpty())
            {
                CommentListFragment.access$900(CommentListFragment.this).add(CommentModel.convert(CommentListFragment.access$800(CommentListFragment.this)));
                CommentListFragment.access$502(CommentListFragment.this, false);
                CommentListFragment.access$600(CommentListFragment.this).notifyDataSetChanged();
            }
            showToast("\u8BC4\u8BBA\u4E0D\u5B58\u5728\u6216\u8005\u5DF2\u7ECF\u88AB\u5220\u9664");
            return;
        }
        if (TextUtils.isEmpty(((CharSequence) (obj)))) goto _L1; else goto _L3
_L3:
        showToast(((String) (obj)));
        showFooterView(com.ximalaya.ting.android.fragment.View.NO_CONNECTION);
        return;
        obj = s.getJSONObject("result");
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_651;
        }
        String s1;
        s = ((JSONObject) (obj)).getString("comments");
        s1 = ((JSONObject) (obj)).getString("parentComments");
        if (!TextUtils.isEmpty(s)) goto _L5; else goto _L4
_L4:
        obj = CommentListFragment.this;
        if (!val$pullFromEnd) goto _L7; else goto _L6
_L6:
        s = com.ximalaya.ting.android.fragment.View.NO_DATA;
_L9:
        ((CommentListFragment) (obj)).showFooterView(s);
        return;
        Exception exception;
        exception;
        s = null;
_L11:
        List list;
        exception.printStackTrace();
        exception = s;
        list = obj1;
          goto _L8
_L7:
        s = com.ximalaya.ting.android.fragment.View.HIDE_ALL;
          goto _L9
_L5:
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/zone/CommentModel);
        list = obj1;
        exception = s;
        if (TextUtils.isEmpty(s1)) goto _L8; else goto _L10
_L10:
        list = JSON.parseArray(s1, com/ximalaya/ting/android/model/zone/CommentModel);
        exception = s;
_L8:
        if (exception == null || exception.size() == 0)
        {
            if (val$pullFromEnd)
            {
                CommentListFragment.access$302(CommentListFragment.this, false);
            } else
            {
                CommentListFragment.access$502(CommentListFragment.this, false);
            }
            if (CommentListFragment.access$2600(CommentListFragment.this))
            {
                CommentListFragment.access$3000(CommentListFragment.this);
            }
            showFooterView(com.ximalaya.ting.android.fragment.View.HIDE_ALL);
            return;
        }
        int i;
        if (exception.size() < 30)
        {
            if (val$pullFromEnd)
            {
                CommentListFragment.access$302(CommentListFragment.this, false);
            } else
            {
                CommentListFragment.access$502(CommentListFragment.this, false);
            }
        }
        CommentListFragment.access$3100(CommentListFragment.this, exception, list);
        if (val$firstLoad || CommentListFragment.access$3200(CommentListFragment.this))
        {
            CommentListFragment.access$900(CommentListFragment.this).clear();
        }
        if (val$pullFromEnd)
        {
            CommentListFragment.access$900(CommentListFragment.this).addAll(exception);
        } else
        {
            CommentListFragment.access$900(CommentListFragment.this).addAll(0, exception);
        }
        i = exception.size();
        i = mListView.getHeaderViewsCount() + (i - 1);
        if (((CommentModel)CommentListFragment.access$900(CommentListFragment.this).get(0)).isMainPost())
        {
            CommentListFragment.access$502(CommentListFragment.this, false);
        } else
        if (((CommentModel)CommentListFragment.access$900(CommentListFragment.this).get(0)).getNumOfFloor() == 1 || !CommentListFragment.access$500(CommentListFragment.this))
        {
            CommentListFragment.access$900(CommentListFragment.this).add(0, CommentModel.convert(CommentListFragment.access$800(CommentListFragment.this)));
            CommentListFragment.access$502(CommentListFragment.this, false);
            i++;
        }
        CommentListFragment.access$600(CommentListFragment.this).notifyDataSetChanged();
        if (CommentListFragment.access$3300(CommentListFragment.this))
        {
            mListView.setSelection(i);
        }
        if (val$firstLoad)
        {
            showFooterView(com.ximalaya.ting.android.fragment.View.MORE);
            return;
        } else
        {
            showFooterView(com.ximalaya.ting.android.fragment.View.HIDE_ALL);
            return;
        }
        exception;
          goto _L11
        exception = null;
        list = obj1;
          goto _L8
    }

    ()
    {
        this$0 = final_commentlistfragment;
        val$pullFromEnd = flag;
        val$firstLoad = Z.this;
        super();
    }
}
