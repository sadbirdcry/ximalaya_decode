// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.ting.android.model.zone.ReplyMessageModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneMessageFragment, CommentListFragment

class this._cls0
    implements android.widget.kListener
{

    final ZoneMessageFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        int j = i - mListView.getHeaderViewsCount();
        if (i < 0 || j < 0 || ZoneMessageFragment.access$300(ZoneMessageFragment.this).size() < j + 1 || !OneClickHelper.getInstance().onClick(view))
        {
            return;
        } else
        {
            adapterview = new Bundle();
            adapterview.putLong("zoneId", ((ReplyMessageModel)ZoneMessageFragment.access$300(ZoneMessageFragment.this).get(j)).getZoneId());
            adapterview.putLong("postId", ((ReplyMessageModel)ZoneMessageFragment.access$300(ZoneMessageFragment.this).get(j)).getPostId());
            adapterview.putLong("timeline", ((ReplyMessageModel)ZoneMessageFragment.access$300(ZoneMessageFragment.this).get(j)).getTimeline());
            adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/CommentListFragment, adapterview);
            return;
        }
    }

    ()
    {
        this$0 = ZoneMessageFragment.this;
        super();
    }
}
