// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.text.TextUtils;
import android.widget.ProgressBar;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.zone.RoleInfo;
import com.ximalaya.ting.android.model.zone.ZoneModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneDetailFragment

class this._cls0 extends a
{

    final ZoneDetailFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
    }

    public void onFinish()
    {
        ZoneDetailFragment.access$500(ZoneDetailFragment.this).setVisibility(8);
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
    }

    public void onSuccess(String s)
    {
        Object obj;
        for (obj = null; !canGoon() || TextUtils.isEmpty(s);)
        {
            return;
        }

        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            s = null;
        }
        if (s == null || s.getIntValue("ret") != 0)
        {
            if (s == null)
            {
                s = obj;
            } else
            {
                s = s.getString("msg");
            }
            if (TextUtils.isEmpty(s))
            {
                s = "\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF";
            } else
            {
                s = (new StringBuilder()).append("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF:").append(s).toString();
            }
            showToast(s);
            return;
        } else
        {
            ZoneDetailFragment.access$100(ZoneDetailFragment.this).getMyZone().setIsJoint(true);
            ZoneDetailFragment.access$400(ZoneDetailFragment.this, false);
            showToast("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u6210\u529F");
            return;
        }
    }

    ()
    {
        this$0 = ZoneDetailFragment.this;
        super();
    }
}
