// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.ting.android.adapter.zone.CommentAdapter;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.zone.CommentModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            CommentListFragment

class this._cls0
    implements android.widget.kListener
{

    final CommentListFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        i -= mListView.getHeaderViewsCount();
        if (i >= 0 && CommentListFragment.access$600(CommentListFragment.this).getCount() >= i + 1)
        {
            if ((adapterview = (CommentModel)CommentListFragment.access$600(CommentListFragment.this).getItem(i)) != null)
            {
                ArrayList arraylist = new ArrayList();
                arraylist.add("\u67E5\u770B\u8D44\u6599");
                arraylist.add("\u56DE\u590D");
                arraylist.add("\u4E3E\u62A5");
                if (adapterview.isCanBeDeleted())
                {
                    arraylist.add("\u5220\u9664");
                }
                if (CommentListFragment.access$700(CommentListFragment.this) == null)
                {
                    CommentListFragment.access$702(CommentListFragment.this, new MenuDialog(getActivity(), arraylist));
                } else
                {
                    CommentListFragment.access$700(CommentListFragment.this).setSelections(arraylist);
                }
                CommentListFragment.access$700(CommentListFragment.this).setOnItemClickListener(new tionDialogListener(CommentListFragment.this, adapterview, DataCollectUtil.getDataFromView(view)));
                CommentListFragment.access$700(CommentListFragment.this).show();
                return;
            }
        }
    }

    tionDialogListener()
    {
        this$0 = CommentListFragment.this;
        super();
    }
}
