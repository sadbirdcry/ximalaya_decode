// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.adapter.zone.PostAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.model.zone.RoleInfo;
import com.ximalaya.ting.android.model.zone.ZoneModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import com.ximalaya.ting.android.view.RoundedHexagonImageView;
import com.ximalaya.ting.android.view.SlideView;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneConstants

public class ZoneEntryFragmentNew extends BaseActivityLikeFragment
    implements com.ximalaya.ting.android.fragment.ReloadFragment.Callback
{

    private LayoutInflater inflater;
    private boolean isHideHeaderAndSlide;
    private ArrayList mAllMyZones;
    private ArrayList mAllRecomendZones;
    private boolean mHasNetError;
    private View mHeader;
    private boolean mIsDataLoaded;
    private boolean mIsMyZoneLoading;
    private boolean mIsRecommendPostLoading;
    private boolean mIsRecommendZoneLoading;
    private LinearLayout mLoadingView;
    private View mMyZoneBorder;
    private LinearLayout mMyZoneLayout;
    private ArrayList mMyZoneList;
    private TextView mMyZoneMoreTv;
    private TextView mMyZoneTitleTv;
    private RelativeLayout mNetErrorLoadingLayout;
    private TextView mNetErrorTv;
    private PostAdapter mPostAdapter;
    private ArrayList mPostList;
    private PullToRefreshListView mPostListView;
    private TextView mPostTitleTv;
    private ProgressBar mProgressBar;
    private View mRecomendZoneBorder;
    private LinearLayout mRecommendZoneLayout;
    private ArrayList mRecommendZoneList;
    private TextView mRecommendZoneMoreTv;
    private TextView mRecommendZoneTitleTv;
    private TextView mRegZoneTv;

    public ZoneEntryFragmentNew()
    {
        mMyZoneList = new ArrayList();
        mRecommendZoneList = new ArrayList();
        mPostList = new ArrayList();
        mAllMyZones = new ArrayList();
        mAllRecomendZones = new ArrayList();
        mHasNetError = false;
        mIsMyZoneLoading = false;
        mIsRecommendZoneLoading = false;
        mIsRecommendPostLoading = false;
        isHideHeaderAndSlide = false;
    }

    private void clearRef()
    {
        mMyZoneLayout.removeAllViews();
        mMyZoneLayout = null;
        mRecommendZoneLayout.removeAllViews();
        mRecommendZoneLayout = null;
        mHeader = null;
        mPostListView.setAdapter(null);
        mPostAdapter = null;
        mPostListView = null;
        fragmentBaseContainerView = null;
    }

    private void doFollow(View view, final ZoneModel m)
    {
        mProgressBar.setVisibility(0);
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", (new StringBuilder()).append("").append(m.getId()).toString());
        f.a().b(ZoneConstants.URL_MY_ZONES, requestparams, DataCollectUtil.getDataFromView(view), new _cls10());
    }

    private void doLoad()
    {
        if (!NetworkUtils.isNetworkAvaliable(getActivity()))
        {
            mNetErrorLoadingLayout.setVisibility(8);
            mLoadingView.setVisibility(8);
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
            return;
        } else
        {
            mHasNetError = false;
            mNetErrorLoadingLayout.setVisibility(8);
            loadMyZone();
            loadRecommendZone();
            loadRecommendPost();
            return;
        }
    }

    private void doUnfollow(View view, final ZoneModel m)
    {
        mProgressBar.setVisibility(0);
        String s = (new StringBuilder()).append(ZoneConstants.URL_MY_ZONES).append("/").append(m.getId()).toString();
        f.a().a(s, DataCollectUtil.getDataFromView(view), new _cls11());
    }

    private void hideContentLoading()
    {
        if (mLoadingView.getVisibility() != 8)
        {
            mLoadingView.setVisibility(8);
        }
    }

    private void initData()
    {
        if (!getUserVisibleHint() || getView() == null)
        {
            return;
        }
        if (!mIsDataLoaded)
        {
            mIsDataLoaded = true;
            doAfterAnimation(new _cls7());
            return;
        } else
        {
            hideContentLoading();
            parseOldData();
            return;
        }
    }

    private void initListener()
    {
        mPostListView.setOnRefreshListener(new _cls1());
        mMyZoneMoreTv.setOnClickListener(new _cls2());
        mRecommendZoneMoreTv.setOnClickListener(new _cls3());
        mPostListView.setOnItemClickListener(new _cls4());
        mNetErrorLoadingLayout.setOnClickListener(new _cls5());
        mRegZoneTv.setOnClickListener(new _cls6());
    }

    private void initTitleBarAndSlide()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            isHideHeaderAndSlide = bundle.getBoolean("isHideHeaderAndSlide", false);
            if (isHideHeaderAndSlide)
            {
                if (findViewById(0x7f0a0202) != null)
                {
                    findViewById(0x7f0a0202).setVisibility(8);
                }
                View view = findViewById(0x7f0a030f);
                if (view != null && (view instanceof SlideView))
                {
                    ((SlideView)view).setSlide(false);
                    return;
                }
            }
        }
    }

    private void initViews(LayoutInflater layoutinflater)
    {
        ((TextView)findViewById(0x7f0a00ae)).setText("\u5708\u5B50");
        mLoadingView = (LinearLayout)findViewById(0x7f0a012c);
        if (!mIsDataLoaded)
        {
            mLoadingView.setVisibility(0);
        }
        mProgressBar = (ProgressBar)findViewById(0x7f0a0091);
        mPostListView = (PullToRefreshListView)findViewById(0x7f0a040a);
        mHeader = (ViewGroup)layoutinflater.inflate(0x7f0300ee, mPostListView, false);
        mNetErrorLoadingLayout = (RelativeLayout)mHeader.findViewById(0x7f0a0408);
        mHeader.findViewById(0x7f0a073c).setVisibility(8);
        mNetErrorTv = (TextView)mHeader.findViewById(0x7f0a073b);
        mNetErrorTv.setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73\uFF0C\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
        mPostListView.addHeaderView(mHeader);
        mPostAdapter = new PostAdapter(this, getActivity(), mPostList, true);
        mPostListView.setAdapter(mPostAdapter);
        mMyZoneLayout = (LinearLayout)mHeader.findViewById(0x7f0a03ff);
        mRecommendZoneLayout = (LinearLayout)mHeader.findViewById(0x7f0a0403);
        mMyZoneBorder = mHeader.findViewById(0x7f0a0400);
        mMyZoneMoreTv = (TextView)mHeader.findViewById(0x7f0a0401);
        mMyZoneTitleTv = (TextView)mHeader.findViewById(0x7f0a03fe);
        mRecomendZoneBorder = mHeader.findViewById(0x7f0a0404);
        mRecommendZoneMoreTv = (TextView)mHeader.findViewById(0x7f0a0406);
        mRecommendZoneTitleTv = (TextView)mHeader.findViewById(0x7f0a0402);
        mPostTitleTv = (TextView)mHeader.findViewById(0x7f0a0407);
        mRegZoneTv = (TextView)findViewById(0x7f0a074f);
        mRegZoneTv.setText("\u521B\u5EFA");
        if (isShowCreateZoneBtn())
        {
            mRegZoneTv.setVisibility(0);
        } else
        {
            mRegZoneTv.setVisibility(8);
        }
        if (mIsDataLoaded)
        {
            if (mMyZoneList != null && !mMyZoneList.isEmpty())
            {
                refreshMyZones();
            }
            if (mRecommendZoneList != null && !mRecommendZoneList.isEmpty())
            {
                refreshRecommendZones();
            }
            if (mPostList != null && !mPostList.isEmpty())
            {
                mPostTitleTv.setVisibility(0);
            }
        }
    }

    private boolean isShowCreateZoneBtn()
    {
        LoginInfoModel logininfomodel;
        if (UserInfoMannage.hasLogined())
        {
            if (!UserInfoMannage.hasLogined() || UserInfoMannage.getInstance().getUser() == null || ((logininfomodel = UserInfoMannage.getInstance().getUser()) == null || ((UserInfoModel) (logininfomodel)).zoneId > 0L))
            {
                return false;
            }
        }
        return true;
    }

    private void loadMyZone()
    {
        if (mIsMyZoneLoading)
        {
            return;
        }
        if (!UserInfoMannage.hasLogined())
        {
            mMyZoneLayout.setVisibility(8);
            return;
        } else
        {
            mIsMyZoneLoading = true;
            RequestParams requestparams = new RequestParams();
            requestparams.put("timeline", 0);
            requestparams.put("maxSizeOfZones", 20);
            f.a().a(ZoneConstants.URL_MY_ZONES, requestparams, DataCollectUtil.getDataFromView(mMyZoneLayout), new _cls12());
            return;
        }
    }

    private void loadRecommendPost()
    {
        if (mIsRecommendPostLoading)
        {
            return;
        } else
        {
            mIsRecommendPostLoading = true;
            f.a().a(ZoneConstants.URL_RECOMMEND_POSTS, new RequestParams(), DataCollectUtil.getDataFromView(mPostListView), new _cls14());
            return;
        }
    }

    private void loadRecommendZone()
    {
        if (mIsRecommendZoneLoading)
        {
            return;
        } else
        {
            mIsRecommendZoneLoading = true;
            f.a().a(ZoneConstants.URL_RECOMMEND_ZONES, new RequestParams(), DataCollectUtil.getDataFromView(mRecommendZoneLayout), new _cls13());
            return;
        }
    }

    public static ZoneEntryFragmentNew newInstance(boolean flag)
    {
        ZoneEntryFragmentNew zoneentryfragmentnew = new ZoneEntryFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isHideHeaderAndSlide", flag);
        zoneentryfragmentnew.setArguments(bundle);
        return zoneentryfragmentnew;
    }

    private JSONObject parseJSON(String s)
    {
        Object obj = null;
        Object obj1 = null;
        if (TextUtils.isEmpty(s))
        {
            obj = obj1;
        } else
        {
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = ((String) (obj));
            }
            obj = s;
            if (s != null)
            {
                obj = s;
                if (s.getIntValue("ret") != 0)
                {
                    String s1 = s.getString("msg");
                    obj = s;
                    if (!TextUtils.isEmpty(s1))
                    {
                        showToast(s1);
                        return s;
                    }
                }
            }
        }
        return ((JSONObject) (obj));
    }

    private void parseOldData()
    {
        if (mPostList != null && !mPostList.isEmpty())
        {
            mPostAdapter.notifyDataSetChanged();
        }
    }

    private void refreshAfterFollowRecommend(ZoneModel zonemodel)
    {
        Iterator iterator = mRecommendZoneList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            ZoneModel zonemodel1 = (ZoneModel)iterator.next();
            if (zonemodel.getId() != zonemodel1.getId())
            {
                continue;
            }
            iterator.remove();
            break;
        } while (true);
        iterator = mAllRecomendZones.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            ZoneModel zonemodel2 = (ZoneModel)iterator.next();
            if (zonemodel.getId() != zonemodel2.getId())
            {
                continue;
            }
            iterator.remove();
            break;
        } while (true);
        if (mAllRecomendZones.size() >= 4)
        {
            mRecommendZoneList.add(mAllRecomendZones.get(3));
        }
        mAllMyZones.add(0, zonemodel);
        mMyZoneList.add(0, zonemodel);
        if (mMyZoneList.size() > 4)
        {
            mMyZoneList.remove(mMyZoneList.get(4));
        }
        refreshMyZones();
        refreshRecommendZones();
    }

    private void refreshMyZones()
    {
        if (mMyZoneList == null || mMyZoneList.isEmpty())
        {
            mMyZoneLayout.setVisibility(8);
            mMyZoneTitleTv.setVisibility(8);
        } else
        {
            mMyZoneLayout.setVisibility(0);
            mMyZoneTitleTv.setVisibility(0);
        }
        mMyZoneLayout.removeAllViews();
        for (int i = 0; i != mMyZoneList.size(); i++)
        {
            ViewGroup viewgroup = (ViewGroup)inflater.inflate(0x7f030149, mMyZoneLayout, false);
            setZoneData(viewgroup, mMyZoneList, i, false);
            mMyZoneLayout.addView(viewgroup);
        }

        showMyZoneMore();
    }

    private void refreshRecommendZones()
    {
        if (mRecommendZoneList == null || mRecommendZoneList.isEmpty())
        {
            mRecommendZoneLayout.setVisibility(8);
            mRecommendZoneTitleTv.setVisibility(8);
        } else
        {
            mRecommendZoneLayout.setVisibility(0);
            mRecommendZoneTitleTv.setVisibility(0);
        }
        mRecommendZoneLayout.removeAllViews();
        for (int i = 0; i != mRecommendZoneList.size(); i++)
        {
            ViewGroup viewgroup = (ViewGroup)inflater.inflate(0x7f030149, mRecommendZoneLayout, false);
            setZoneData(viewgroup, mRecommendZoneList, i, true);
            mRecommendZoneLayout.addView(viewgroup);
        }

        showRecmdZoneMore();
    }

    private void setZoneData(ViewGroup viewgroup, ArrayList arraylist, int i, boolean flag)
    {
        final ZoneModel m = (ZoneModel)arraylist.get(i);
        viewgroup.setTag(m);
        TextView textview1 = (TextView)viewgroup.findViewById(0x7f0a03f1);
        RoundedHexagonImageView roundedhexagonimageview = (RoundedHexagonImageView)viewgroup.findViewById(0x7f0a03ef);
        TextView textview2 = (TextView)viewgroup.findViewById(0x7f0a03f2);
        TextView textview3 = (TextView)viewgroup.findViewById(0x7f0a03f3);
        TextView textview4 = (TextView)viewgroup.findViewById(0x7f0a03f4);
        TextView textview = (TextView)viewgroup.findViewById(0x7f0a03f5);
        ImageView imageview = (ImageView)viewgroup.findViewById(0x7f0a0153);
        View view = viewgroup.findViewById(0x7f0a00a8);
        Object obj;
        if (m.getDisplayName() == null)
        {
            obj = "";
        } else
        {
            obj = m.getDisplayName();
        }
        textview1.setText(((CharSequence) (obj)));
        textview3.setText((new StringBuilder()).append("\u6210\u5458").append(StringUtil.getFriendlyNumStr(m.getNumOfMembers())).toString());
        textview4.setText((new StringBuilder()).append("\u5E16\u5B50").append(StringUtil.getFriendlyNumStr(m.getNumOfPosts())).toString());
        textview2.setText(m.getDescription());
        ImageManager2.from(mCon).displayImage(roundedhexagonimageview, m.getImageUrl(), 0x7f0200db);
        obj = UserInfoMannage.getInstance().getUser();
        if (flag)
        {
            if (obj != null && m.getOwner() != null && ((LoginInfoModel) (obj)).uid == m.getOwner().getUid())
            {
                textview.setVisibility(8);
            } else
            {
                textview.setVisibility(0);
                ViewUtil.expandClickArea(getActivity(), textview, 10, 25, 10, 25);
                if (m.getMyZone() != null && m.getMyZone().isJoint())
                {
                    textview.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    textview.setPadding(0, 0, 0, 0);
                    if (getActivity() != null)
                    {
                        textview.setBackgroundResource(0x7f020102);
                    }
                    textview.setText("\u5DF2\u52A0\u5165");
                } else
                {
                    textview.setPadding(ToolUtil.dp2px(getActivity(), 10F), 0, 0, 0);
                    textview.setCompoundDrawablesWithIntrinsicBounds(0x7f0205dc, 0, 0, 0);
                    if (getActivity() != null)
                    {
                        textview.setBackgroundResource(0x7f020103);
                    }
                    textview.setText("\u52A0\u5165");
                }
            }
            textview.setOnClickListener(new _cls8());
            imageview.setVisibility(8);
        } else
        {
            imageview.setVisibility(0);
            textview.setVisibility(8);
        }
        if (i + 1 == arraylist.size())
        {
            view.setVisibility(8);
        } else
        {
            view.setVisibility(0);
        }
        viewgroup.setOnClickListener(new _cls9());
    }

    private void showErrorOnce(String s)
    {
        if (!mHasNetError)
        {
            mHasNetError = true;
            showToast(s);
        }
    }

    private void showMyZoneMore()
    {
        mMyZoneLayout.setVisibility(0);
        if (mAllMyZones.size() <= 4)
        {
            mMyZoneBorder.setVisibility(8);
            mMyZoneMoreTv.setVisibility(8);
            return;
        } else
        {
            mMyZoneBorder.setVisibility(0);
            mMyZoneMoreTv.setVisibility(0);
            return;
        }
    }

    private void showRecmdZoneMore()
    {
        mRecommendZoneLayout.setVisibility(0);
        if (mAllRecomendZones.size() <= 4)
        {
            mRecomendZoneBorder.setVisibility(8);
            mRecommendZoneMoreTv.setVisibility(8);
            return;
        } else
        {
            mRecomendZoneBorder.setVisibility(0);
            mRecommendZoneMoreTv.setVisibility(0);
            return;
        }
    }

    private void showReload(boolean flag, boolean flag1, boolean flag2, boolean flag3)
    {
        if (!flag && !flag2 && flag1 && flag3)
        {
            mNetErrorLoadingLayout.setVisibility(8);
            mLoadingView.setVisibility(8);
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        inflater = LayoutInflater.from(getActivity());
        initData();
        initListener();
        ToolUtil.onEvent(mCon, "zone_homepage");
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300ef, viewgroup, false);
        initTitleBarAndSlide();
        initViews(layoutinflater);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void onResume()
    {
        super.onResume();
        if (isAdded() && mActivity != null && ((MyApplication)mActivity.getApplication()).e == 1)
        {
            mPostListView.toRefreshing();
            ((MyApplication)mActivity.getApplication()).e = 0;
        }
        if (isAdded() && !UserInfoMannage.hasLogined() && mMyZoneLayout.getVisibility() != 8)
        {
            mAllMyZones.clear();
            mMyZoneList.clear();
            mPostListView.toRefreshing();
        }
    }

    public void reload(View view)
    {
        doLoad();
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        initData();
    }











/*
    static boolean access$1602(ZoneEntryFragmentNew zoneentryfragmentnew, boolean flag)
    {
        zoneentryfragmentnew.mIsRecommendPostLoading = flag;
        return flag;
    }

*/



/*
    static boolean access$1702(ZoneEntryFragmentNew zoneentryfragmentnew, boolean flag)
    {
        zoneentryfragmentnew.mIsRecommendZoneLoading = flag;
        return flag;
    }

*/










/*
    static boolean access$2402(ZoneEntryFragmentNew zoneentryfragmentnew, boolean flag)
    {
        zoneentryfragmentnew.mIsMyZoneLoading = flag;
        return flag;
    }

*/












    private class _cls10 extends a
    {

        final ZoneEntryFragmentNew this$0;
        final ZoneModel val$m;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mRecommendZoneLayout);
        }

        public void onFinish()
        {
            mProgressBar.setVisibility(8);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            Object obj;
            for (obj = null; !canGoon() || TextUtils.isEmpty(s);)
            {
                return;
            }

            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = obj;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF";
                } else
                {
                    s = (new StringBuilder()).append("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                m.getMyZone().setIsJoint(true);
                refreshAfterFollowRecommend(m);
                showToast("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u6210\u529F");
                return;
            }
        }

        _cls10()
        {
            this$0 = ZoneEntryFragmentNew.this;
            m = zonemodel;
            super();
        }
    }


    private class _cls11 extends a
    {

        final ZoneEntryFragmentNew this$0;
        final ZoneModel val$m;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mRecommendZoneLayout);
        }

        public void onFinish()
        {
            mProgressBar.setVisibility(8);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            Object obj;
            for (obj = null; !canGoon() || TextUtils.isEmpty(s);)
            {
                return;
            }

            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = obj;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u9000\u51FA\u5708\u5B50\u53D1\u751F\u9519\u8BEF";
                } else
                {
                    s = (new StringBuilder()).append("\u9000\u51FA\u5708\u5B50\u53D1\u751F\u9519\u8BEF:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                m.getMyZone().setIsJoint(false);
                showToast("\u9000\u51FA\u5708\u5B50\u6210\u529F");
                refreshRecommendZones();
                return;
            }
        }

        _cls11()
        {
            this$0 = ZoneEntryFragmentNew.this;
            m = zonemodel;
            super();
        }
    }


    private class _cls7
        implements MyCallback
    {

        final ZoneEntryFragmentNew this$0;

        public void execute()
        {
            doLoad();
        }

        _cls7()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final ZoneEntryFragmentNew this$0;

        public void onRefresh()
        {
            doLoad();
        }

        _cls1()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final ZoneEntryFragmentNew this$0;

        public void onClick(View view)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("zone_type", 0);
            bundle.putSerializable("zone_list", mAllMyZones);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/ZoneListFragment, bundle);
        }

        _cls2()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final ZoneEntryFragmentNew this$0;

        public void onClick(View view)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("zone_type", 1);
            bundle.putSerializable("zone_list", mAllRecomendZones);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/ZoneListFragment, bundle);
        }

        _cls3()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemClickListener
    {

        final ZoneEntryFragmentNew this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mPostListView.getHeaderViewsCount();
            if (i < 0 || mPostList.size() < i + 1 || !OneClickHelper.getInstance().onClick(view))
            {
                return;
            } else
            {
                adapterview = new Bundle();
                adapterview.putLong("zoneId", ((PostModel)mPostList.get(i)).getZoneId());
                adapterview.putLong("postId", ((PostModel)mPostList.get(i)).getId());
                adapterview.putLong("hostUid", ((PostModel)mPostList.get(i)).getPoster().getUid());
                adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/zone/PostFragment, adapterview);
                return;
            }
        }

        _cls4()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final ZoneEntryFragmentNew this$0;

        public void onClick(View view)
        {
            mNetErrorLoadingLayout.setVisibility(8);
            mPostListView.toRefreshing();
        }

        _cls5()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final ZoneEntryFragmentNew this$0;

        public void onClick(View view)
        {
            if (!UserInfoMannage.hasLogined())
            {
                view = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                startActivity(view);
                return;
            } else
            {
                view = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
                view.putExtra("ExtraUrl", com.ximalaya.ting.android.a.C);
                startActivity(view);
                return;
            }
        }

        _cls6()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls12 extends a
    {

        final ZoneEntryFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mMyZoneLayout);
        }

        public void onFinish()
        {
label0:
            {
                if (mPostListView != null)
                {
                    mPostListView.onRefreshComplete();
                }
                mIsMyZoneLoading = false;
                if (mMyZoneLayout != null)
                {
                    if (mMyZoneList != null && !mMyZoneList.isEmpty())
                    {
                        break label0;
                    }
                    mMyZoneLayout.setVisibility(8);
                }
                return;
            }
            mMyZoneLayout.setVisibility(0);
        }

        public void onNetError(int i, String s)
        {
            boolean flag1 = false;
            showErrorOnce("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            if (canGoon())
            {
                hideContentLoading();
                if (mMyZoneList == null || mMyZoneList.isEmpty())
                {
                    mMyZoneLayout.setVisibility(8);
                }
                mNetErrorLoadingLayout.setVisibility(0);
                s = ZoneEntryFragmentNew.this;
                boolean flag2 = mIsRecommendPostLoading;
                boolean flag;
                boolean flag3;
                if (mPostList == null || mPostList.isEmpty())
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                flag3 = mIsRecommendZoneLoading;
                if (mRecommendZoneList == null || mRecommendZoneList.isEmpty())
                {
                    flag1 = true;
                }
                s.showReload(flag2, flag, flag3, flag1);
            }
        }

        public void onSuccess(String s)
        {
            if (canGoon()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            hideContentLoading();
            if (!UserInfoMannage.hasLogined())
            {
                mMyZoneLayout.setVisibility(8);
                return;
            }
            s = parseJSON(s);
            if (s == null) goto _L1; else goto _L3
_L3:
            Object obj;
            mAllMyZones.clear();
            mMyZoneList.clear();
            obj = s.getString("result");
            if (TextUtils.isEmpty(((CharSequence) (obj))))
            {
                refreshMyZones();
                return;
            }
            s = null;
            obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/zone/ZoneModel);
            s = ((String) (obj));
_L4:
            if (s != null && s.size() != 0)
            {
                mAllMyZones.addAll((ArrayList)s);
                Exception exception;
                if (s.size() <= 4)
                {
                    mMyZoneList.addAll((ArrayList)s);
                    mMyZoneBorder.setVisibility(8);
                    mMyZoneMoreTv.setVisibility(8);
                } else
                {
                    mMyZoneList.addAll(s.subList(0, 4));
                    mMyZoneBorder.setVisibility(0);
                    mMyZoneMoreTv.setVisibility(0);
                }
            }
            refreshMyZones();
            return;
            exception;
            exception.printStackTrace();
              goto _L4
        }

        _cls12()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls14 extends a
    {

        final ZoneEntryFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mPostListView);
        }

        public void onFinish()
        {
            if (mPostListView != null)
            {
                mPostListView.onRefreshComplete();
            }
            mIsRecommendPostLoading = false;
        }

        public void onNetError(int i, String s)
        {
            boolean flag1 = false;
            showErrorOnce("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            if (canGoon())
            {
                hideContentLoading();
                mNetErrorLoadingLayout.setVisibility(0);
                s = ZoneEntryFragmentNew.this;
                boolean flag2 = mIsRecommendZoneLoading;
                boolean flag;
                boolean flag3;
                if (mRecommendZoneList == null || mRecommendZoneList.isEmpty())
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                flag3 = mIsMyZoneLoading;
                if (mMyZoneList == null || mMyZoneList.isEmpty())
                {
                    flag1 = true;
                }
                s.showReload(flag2, flag, flag3, flag1);
            }
        }

        public void onSuccess(String s)
        {
            if (canGoon()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            hideContentLoading();
            s = parseJSON(s);
            if (s == null) goto _L1; else goto _L3
_L3:
            Object obj = s.getString("result");
            if (TextUtils.isEmpty(((CharSequence) (obj)))) goto _L1; else goto _L4
_L4:
            s = null;
            obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/zone/PostModel);
            s = ((String) (obj));
_L6:
            mPostList.clear();
            if (s != null && s.size() != 0)
            {
                mPostTitleTv.setVisibility(0);
                mPostList.addAll((ArrayList)s);
            }
            mPostAdapter.notifyDataSetChanged();
            return;
            Exception exception;
            exception;
            exception.printStackTrace();
            if (true) goto _L6; else goto _L5
_L5:
        }

        _cls14()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls13 extends a
    {

        final ZoneEntryFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mRecommendZoneLayout);
        }

        public void onFinish()
        {
label0:
            {
                if (mPostListView != null)
                {
                    mPostListView.onRefreshComplete();
                }
                mIsRecommendZoneLoading = false;
                if (mRecommendZoneLayout != null)
                {
                    if (mRecommendZoneList != null && !mRecommendZoneList.isEmpty())
                    {
                        break label0;
                    }
                    mRecommendZoneLayout.setVisibility(8);
                }
                return;
            }
            mRecommendZoneLayout.setVisibility(0);
        }

        public void onNetError(int i, String s)
        {
            boolean flag1 = false;
            showErrorOnce("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            if (canGoon())
            {
                hideContentLoading();
                mNetErrorLoadingLayout.setVisibility(0);
                s = ZoneEntryFragmentNew.this;
                boolean flag2 = mIsRecommendPostLoading;
                boolean flag;
                boolean flag3;
                if (mPostList == null || mPostList.isEmpty())
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                flag3 = mIsMyZoneLoading;
                if (mMyZoneList == null || mMyZoneList.isEmpty())
                {
                    flag1 = true;
                }
                s.showReload(flag2, flag, flag3, flag1);
            }
        }

        public void onSuccess(String s)
        {
            if (canGoon()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            hideContentLoading();
            s = parseJSON(s);
            if (s == null) goto _L1; else goto _L3
_L3:
            Object obj;
            mRecommendZoneList.clear();
            mAllRecomendZones.clear();
            obj = s.getString("result");
            if (TextUtils.isEmpty(((CharSequence) (obj))))
            {
                refreshRecommendZones();
                return;
            }
            s = null;
            obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/zone/ZoneModel);
            s = ((String) (obj));
_L4:
            if (s != null && s.size() != 0)
            {
                mAllRecomendZones.addAll((ArrayList)s);
                Exception exception;
                if (s.size() <= 4)
                {
                    mRecommendZoneList.addAll((ArrayList)s);
                    mRecomendZoneBorder.setVisibility(8);
                    mRecommendZoneMoreTv.setVisibility(8);
                } else
                {
                    mRecommendZoneList.addAll(s.subList(0, 4));
                    mRecomendZoneBorder.setVisibility(0);
                    mRecommendZoneMoreTv.setVisibility(0);
                }
            }
            refreshRecommendZones();
            return;
            exception;
            exception.printStackTrace();
              goto _L4
        }

        _cls13()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }


    private class _cls8
        implements android.view.View.OnClickListener
    {

        final ZoneEntryFragmentNew this$0;
        final ZoneModel val$m;

        public void onClick(final View v)
        {
            if (!UserInfoMannage.hasLogined())
            {
                if (canGoon())
                {
                    v = new Intent(getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity);
                    getActivity().startActivity(v);
                }
            } else
            if (m != null && m.getMyZone() != null)
            {
                if (m.getMyZone().isJoint() && getActivity() != null)
                {
                    class _cls1
                        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                    {

                        final _cls8 this$1;
                        final View val$v;

                        public void onExecute()
                        {
                            doUnfollow(v, m);
                        }

                _cls1()
                {
                    this$1 = _cls8.this;
                    v = view;
                    super();
                }
                    }

                    (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u8981\u9000\u51FA\u6B64\u5708\u5B50\uFF1F").setOkBtn(new _cls1()).showConfirm();
                    return;
                } else
                {
                    doFollow(v, m);
                    return;
                }
            }
        }

        _cls8()
        {
            this$0 = ZoneEntryFragmentNew.this;
            m = zonemodel;
            super();
        }
    }


    private class _cls9
        implements android.view.View.OnClickListener
    {

        final ZoneEntryFragmentNew this$0;

        public void onClick(View view)
        {
            while (!OneClickHelper.getInstance().onClick(view) || view.getTag() == null || !(view.getTag() instanceof ZoneModel)) 
            {
                return;
            }
            ToolUtil.onEvent(mCon, "zone_main");
            Bundle bundle = new Bundle();
            bundle.putLong("zoneId", ((ZoneModel)view.getTag()).getId());
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment, bundle);
        }

        _cls9()
        {
            this$0 = ZoneEntryFragmentNew.this;
            super();
        }
    }

}
