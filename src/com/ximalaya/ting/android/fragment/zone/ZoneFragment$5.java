// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneFragment, PostFragment

class this._cls0
    implements android.widget.temClickListener
{

    final ZoneFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        int j = i - mListView.getHeaderViewsCount();
        if (i < 0 || j < 0 || ZoneFragment.access$600(ZoneFragment.this).size() < j + 1 || !OneClickHelper.getInstance().onClick(view))
        {
            return;
        } else
        {
            adapterview = new Bundle();
            adapterview.putLong("zoneId", ZoneFragment.access$700(ZoneFragment.this));
            adapterview.putLong("postId", ((PostModel)ZoneFragment.access$600(ZoneFragment.this).get(j)).getId());
            adapterview.putLong("hostUid", ((PostModel)ZoneFragment.access$600(ZoneFragment.this).get(j)).getPoster().getUid());
            adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/PostFragment, adapterview);
            return;
        }
    }

    ()
    {
        this$0 = ZoneFragment.this;
        super();
    }
}
