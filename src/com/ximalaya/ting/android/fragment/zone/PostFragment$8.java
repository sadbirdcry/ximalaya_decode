// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.ting.android.adapter.zone.CommentAdapter;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.zone.CommentModel;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            PostFragment

class this._cls0
    implements android.widget.temClickListener
{

    final PostFragment this$0;

    public void onItemClick(final AdapterView model, final View listViewItem, int i, long l)
    {
        i -= mListView.getHeaderViewsCount();
        if (i >= 0 && PostFragment.access$700(PostFragment.this).getCount() >= i + 1)
        {
            if ((model = (CommentModel)PostFragment.access$700(PostFragment.this).getItem(i)) != null)
            {
                ArrayList arraylist = new ArrayList();
                arraylist.add("\u67E5\u770B\u8D44\u6599");
                arraylist.add("\u56DE\u590D");
                arraylist.add("\u4E3E\u62A5");
                if (model.isCanBeDeleted())
                {
                    arraylist.add("\u5220\u9664");
                }
                class _cls1
                    implements android.widget.AdapterView.OnItemClickListener
                {

                    final PostFragment._cls8 this$1;
                    final View val$listViewItem;
                    final CommentModel val$model;

                    public void onItemClick(AdapterView adapterview, View view, int j, long l1)
                    {
                        if (PostFragment.access$800(this$0) != null)
                        {
                            PostFragment.access$800(this$0).dismiss();
                        }
                        if (!OneClickHelper.getInstance().onClick(view))
                        {
                            return;
                        }
                        switch (j)
                        {
                        default:
                            return;

                        case 0: // '\0'
                            adapterview = new Bundle();
                            adapterview.putLong("toUid", model.getPoster().getUid());
                            adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(listViewItem));
                            startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, adapterview);
                            return;

                        case 1: // '\001'
                            if (!UserInfoMannage.hasLogined())
                            {
                                adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(listViewItem));
                                startActivity(adapterview);
                                return;
                            }
                            if (model.isMainPost())
                            {
                                comment();
                                return;
                            } else
                            {
                                reply(model.getPoster().getNickname(), model.getId(), model.getGroupId());
                                return;
                            }

                        case 2: // '\002'
                            if (!UserInfoMannage.hasLogined())
                            {
                                adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(listViewItem));
                                startActivity(adapterview);
                                return;
                            }
                            adapterview = new PostReportInfo();
                            adapterview.setReporterId(UserInfoMannage.getInstance().getUser().getUid().longValue());
                            adapterview.setReportedId(model.getPoster().getUid());
                            adapterview.setPostId(PostFragment.access$900(this$0));
                            if (model.isMainPost())
                            {
                                adapterview.setType(1);
                            } else
                            {
                                adapterview.setType(2);
                                adapterview.setCommentId(model.getId());
                            }
                            report(adapterview, listViewItem);
                            return;

                        case 3: // '\003'
                            break;
                        }
                        if (!UserInfoMannage.hasLogined())
                        {
                            adapterview = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                            adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(listViewItem));
                            startActivity(adapterview);
                            return;
                        } else
                        {
                            PostFragment.access$1000(this$0, listViewItem, model);
                            return;
                        }
                    }

            _cls1()
            {
                this$1 = PostFragment._cls8.this;
                model = commentmodel;
                listViewItem = view;
                super();
            }
                }

                if (PostFragment.access$800(PostFragment.this) == null)
                {
                    PostFragment.access$802(PostFragment.this, new MenuDialog(getActivity(), arraylist));
                } else
                {
                    PostFragment.access$800(PostFragment.this).setSelections(arraylist);
                }
                PostFragment.access$800(PostFragment.this).setOnItemClickListener(new _cls1());
                PostFragment.access$800(PostFragment.this).show();
                return;
            }
        }
    }

    _cls1()
    {
        this$0 = PostFragment.this;
        super();
    }
}
