// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.zone.ZoneModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneEntryFragmentNew

class this._cls0 extends a
{

    final ZoneEntryFragmentNew this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, ZoneEntryFragmentNew.access$1500(ZoneEntryFragmentNew.this));
    }

    public void onFinish()
    {
label0:
        {
            if (ZoneEntryFragmentNew.access$300(ZoneEntryFragmentNew.this) != null)
            {
                ZoneEntryFragmentNew.access$300(ZoneEntryFragmentNew.this).onRefreshComplete();
            }
            ZoneEntryFragmentNew.access$2402(ZoneEntryFragmentNew.this, false);
            if (ZoneEntryFragmentNew.access$1500(ZoneEntryFragmentNew.this) != null)
            {
                if (ZoneEntryFragmentNew.access$1400(ZoneEntryFragmentNew.this) != null && !ZoneEntryFragmentNew.access$1400(ZoneEntryFragmentNew.this).isEmpty())
                {
                    break label0;
                }
                ZoneEntryFragmentNew.access$1500(ZoneEntryFragmentNew.this).setVisibility(8);
            }
            return;
        }
        ZoneEntryFragmentNew.access$1500(ZoneEntryFragmentNew.this).setVisibility(0);
    }

    public void onNetError(int i, String s)
    {
        boolean flag1 = false;
        ZoneEntryFragmentNew.access$1200(ZoneEntryFragmentNew.this, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        if (canGoon())
        {
            ZoneEntryFragmentNew.access$1300(ZoneEntryFragmentNew.this);
            if (ZoneEntryFragmentNew.access$1400(ZoneEntryFragmentNew.this) == null || ZoneEntryFragmentNew.access$1400(ZoneEntryFragmentNew.this).isEmpty())
            {
                ZoneEntryFragmentNew.access$1500(ZoneEntryFragmentNew.this).setVisibility(8);
            }
            ZoneEntryFragmentNew.access$500(ZoneEntryFragmentNew.this).setVisibility(0);
            s = ZoneEntryFragmentNew.this;
            boolean flag2 = ZoneEntryFragmentNew.access$1600(ZoneEntryFragmentNew.this);
            boolean flag;
            boolean flag3;
            if (ZoneEntryFragmentNew.access$400(ZoneEntryFragmentNew.this) == null || ZoneEntryFragmentNew.access$400(ZoneEntryFragmentNew.this).isEmpty())
            {
                flag = true;
            } else
            {
                flag = false;
            }
            flag3 = ZoneEntryFragmentNew.access$1700(ZoneEntryFragmentNew.this);
            if (ZoneEntryFragmentNew.access$1800(ZoneEntryFragmentNew.this) == null || ZoneEntryFragmentNew.access$1800(ZoneEntryFragmentNew.this).isEmpty())
            {
                flag1 = true;
            }
            ZoneEntryFragmentNew.access$1900(s, flag2, flag, flag3, flag1);
        }
    }

    public void onSuccess(String s)
    {
        if (canGoon()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        ZoneEntryFragmentNew.access$1300(ZoneEntryFragmentNew.this);
        if (!UserInfoMannage.hasLogined())
        {
            ZoneEntryFragmentNew.access$1500(ZoneEntryFragmentNew.this).setVisibility(8);
            return;
        }
        s = ZoneEntryFragmentNew.access$2000(ZoneEntryFragmentNew.this, s);
        if (s == null) goto _L1; else goto _L3
_L3:
        Object obj;
        ZoneEntryFragmentNew.access$100(ZoneEntryFragmentNew.this).clear();
        ZoneEntryFragmentNew.access$1400(ZoneEntryFragmentNew.this).clear();
        obj = s.getString("result");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            ZoneEntryFragmentNew.access$2100(ZoneEntryFragmentNew.this);
            return;
        }
        s = null;
        obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/zone/ZoneModel);
        s = ((String) (obj));
_L4:
        if (s != null && s.size() != 0)
        {
            ZoneEntryFragmentNew.access$100(ZoneEntryFragmentNew.this).addAll((ArrayList)s);
            Exception exception;
            if (s.size() <= 4)
            {
                ZoneEntryFragmentNew.access$1400(ZoneEntryFragmentNew.this).addAll((ArrayList)s);
                ZoneEntryFragmentNew.access$2200(ZoneEntryFragmentNew.this).setVisibility(8);
                ZoneEntryFragmentNew.access$2300(ZoneEntryFragmentNew.this).setVisibility(8);
            } else
            {
                ZoneEntryFragmentNew.access$1400(ZoneEntryFragmentNew.this).addAll(s.subList(0, 4));
                ZoneEntryFragmentNew.access$2200(ZoneEntryFragmentNew.this).setVisibility(0);
                ZoneEntryFragmentNew.access$2300(ZoneEntryFragmentNew.this).setVisibility(0);
            }
        }
        ZoneEntryFragmentNew.access$2100(ZoneEntryFragmentNew.this);
        return;
        exception;
        exception.printStackTrace();
          goto _L4
    }

    ()
    {
        this$0 = ZoneEntryFragmentNew.this;
        super();
    }
}
