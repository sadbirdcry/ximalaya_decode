// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.model.zone.RoleInfo;
import com.ximalaya.ting.android.model.zone.ZoneModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.BlurableImageView;
import com.ximalaya.ting.android.view.RoundedHexagonImageView;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneConstants

public class ZoneDetailFragment extends BaseActivityLikeFragment
{

    private BlurableImageView coverBg;
    private TextView descTv;
    private TextView followTv;
    private ProgressBar mProgressBar;
    private ZoneModel mZone;
    private TextView memberNumTv;
    private TextView nameTv;
    private TextView ownerNameTv;
    private TextView postNumTv;
    private RoundedHexagonImageView zoneCoverImg;

    public ZoneDetailFragment()
    {
    }

    private void initListener()
    {
        followTv.setOnClickListener(new _cls2());
    }

    private void initView()
    {
        ((TextView)findViewById(0x7f0a00ae)).setText("\u5708\u5B50\u8BE6\u60C5");
        mProgressBar = (ProgressBar)findViewById(0x7f0a0091);
        ((RelativeLayout)findViewById(0x7f0a03eb)).setBackgroundResource(0x7f070006);
        nameTv = (TextView)findViewById(0x7f0a03f1);
        coverBg = (BlurableImageView)findViewById(0x7f0a03ec);
        zoneCoverImg = (RoundedHexagonImageView)findViewById(0x7f0a03ef);
        zoneCoverImg.setTag(0x7f0a0037, Boolean.valueOf(true));
        markImageView(zoneCoverImg);
        TextView textview = (TextView)findViewById(0x7f0a03f2);
        memberNumTv = (TextView)findViewById(0x7f0a03f3);
        postNumTv = (TextView)findViewById(0x7f0a03f4);
        followTv = (TextView)findViewById(0x7f0a03f5);
        ownerNameTv = (TextView)findViewById(0x7f0a03fa);
        descTv = (TextView)findViewById(0x7f0a03fd);
        if (mZone != null)
        {
            TextView textview1 = nameTv;
            Object obj;
            if (mZone.getDisplayName() == null)
            {
                obj = "";
            } else
            {
                obj = mZone.getDisplayName();
            }
            textview1.setText(((CharSequence) (obj)));
            ImageManager2.from(mCon).displayImage(zoneCoverImg, mZone.getImageUrl(), 0x7f0200db, new _cls1());
            textview.setVisibility(8);
            memberNumTv.setText((new StringBuilder()).append("\u6210\u5458").append(StringUtil.getFriendlyNumStr(mZone.getNumOfMembers())).toString());
            postNumTv.setText((new StringBuilder()).append("\u5E16\u5B50").append(StringUtil.getFriendlyNumStr(mZone.getNumOfPosts())).toString());
            obj = UserInfoMannage.getInstance().getUser();
            if (obj != null && mZone.getOwner() != null && ((LoginInfoModel) (obj)).uid == mZone.getOwner().getUid())
            {
                followTv.setVisibility(8);
            } else
            {
                followTv.setVisibility(0);
                if (mZone.getMyZone() != null && mZone.getMyZone().isJoint())
                {
                    refreshForFollow(false);
                } else
                {
                    refreshForFollow(true);
                }
            }
            textview = ownerNameTv;
            if (mZone.getOwner() == null || mZone.getOwner().getNickname() == null)
            {
                obj = "";
            } else
            {
                obj = mZone.getOwner().getNickname();
            }
            textview.setText(((CharSequence) (obj)));
            textview = descTv;
            if (mZone.getDescription() == null)
            {
                obj = "";
            } else
            {
                obj = mZone.getDescription();
            }
            textview.setText(((CharSequence) (obj)));
        }
    }

    private void joinZone(View view)
    {
        mProgressBar.setVisibility(0);
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", (new StringBuilder()).append("").append(mZone.getId()).toString());
        f.a().b(ZoneConstants.URL_MY_ZONES, requestparams, DataCollectUtil.getDataFromView(view), new _cls3());
    }

    private void quitZone(View view)
    {
        mProgressBar.setVisibility(0);
        String s = (new StringBuilder()).append(ZoneConstants.URL_MY_ZONES).append("/").append(mZone.getId()).toString();
        f.a().a(s, DataCollectUtil.getDataFromView(view), new _cls4());
    }

    private void refreshForFollow(boolean flag)
    {
        if (flag)
        {
            followTv.setCompoundDrawablesWithIntrinsicBounds(0x7f0205dc, 0, 0, 0);
            followTv.setPadding(ToolUtil.dp2px(mCon, 10F), 0, 0, 0);
            if (mCon != null)
            {
                followTv.setBackgroundResource(0x7f020103);
            }
            followTv.setText("\u52A0\u5165");
            return;
        }
        followTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        followTv.setPadding(0, 0, 0, 0);
        if (mCon != null)
        {
            followTv.setBackgroundResource(0x7f020102);
        }
        followTv.setText("\u5DF2\u52A0\u5165");
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mZone = (ZoneModel)bundle.getSerializable("zone_data");
        }
        initView();
        initListener();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300ed, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onPause()
    {
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
    }







    private class _cls2
        implements android.view.View.OnClickListener
    {

        final ZoneDetailFragment this$0;

        public void onClick(final View v)
        {
            if (!UserInfoMannage.hasLogined())
            {
                v = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                startActivity(v);
            } else
            if (mZone != null && mZone.getMyZone() != null)
            {
                if (mZone.getMyZone().isJoint())
                {
                    class _cls1
                        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                    {

                        final _cls2 this$1;
                        final View val$v;

                        public void onExecute()
                        {
                            quitZone(v);
                        }

                _cls1()
                {
                    this$1 = _cls2.this;
                    v = view;
                    super();
                }
                    }

                    (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u8981\u9000\u51FA\u6B64\u5708\u5B50\uFF1F").setOkBtn(new _cls1()).showConfirm();
                    return;
                } else
                {
                    joinZone(v);
                    return;
                }
            }
        }

        _cls2()
        {
            this$0 = ZoneDetailFragment.this;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
    {

        final ZoneDetailFragment this$0;

        public void onCompleteDisplay(String s, Bitmap bitmap)
        {
            if (mCon == null)
            {
                return;
            }
            Bitmap bitmap1 = ImageManager2.from(mCon).getFromMemCache((new StringBuilder()).append(s).append("/blur").toString());
            if (bitmap1 != null)
            {
                coverBg.setImageBitmap(bitmap1);
                return;
            }
            if (bitmap != null)
            {
                coverBg.blur(new BitmapDrawable(mCon.getResources(), bitmap), (new StringBuilder()).append(s).append("/blur").toString(), true);
                return;
            } else
            {
                coverBg.setImageDrawable(new ColorDrawable(Color.parseColor("#b3202332")));
                coverBg.setResourceUrl(null);
                return;
            }
        }

        _cls1()
        {
            this$0 = ZoneDetailFragment.this;
            super();
        }
    }


    private class _cls3 extends a
    {

        final ZoneDetailFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            mProgressBar.setVisibility(8);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            Object obj;
            for (obj = null; !canGoon() || TextUtils.isEmpty(s);)
            {
                return;
            }

            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = obj;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF";
                } else
                {
                    s = (new StringBuilder()).append("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                mZone.getMyZone().setIsJoint(true);
                refreshForFollow(false);
                showToast("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u6210\u529F");
                return;
            }
        }

        _cls3()
        {
            this$0 = ZoneDetailFragment.this;
            super();
        }
    }


    private class _cls4 extends a
    {

        final ZoneDetailFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            mProgressBar.setVisibility(8);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            while (!canGoon() || TextUtils.isEmpty(s)) 
            {
                return;
            }
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                return;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = null;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u9000\u51FA\u5708\u5B50\u53D1\u751F\u9519\u8BEF";
                } else
                {
                    s = (new StringBuilder()).append("\u9000\u51FA\u5708\u5B50\u53D1\u751F\u9519\u8BEF:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                mZone.getMyZone().setIsJoint(false);
                refreshForFollow(true);
                showToast("\u9000\u51FA\u5708\u5B50\u6210\u529F");
                return;
            }
        }

        _cls4()
        {
            this$0 = ZoneDetailFragment.this;
            super();
        }
    }

}
