// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import com.ximalaya.ting.android.a;

public class ZoneConstants
{

    public static final String URL_COMMENTS;
    public static final String URL_HOTPOST;
    public static final String URL_MY_ZONES;
    public static final String URL_POSTS;
    public static final String URL_RECOMMEND_POSTS;
    public static final String URL_RECOMMEND_ZONES;
    public static final String URL_REPLIES;
    public static final String URL_REPLY_MSG;
    public static final String URL_UPLOAD_COMMENT_IMG;
    public static final String URL_UPLOAD_IMG;
    public static final String URL_ZONES;
    public static final String URL_ZONE_REG;
    public static final String URL_ZONE_REPORT;
    public static final String URL_ZONE_REQ_STATUS;

    public ZoneConstants()
    {
    }

    static 
    {
        URL_RECOMMEND_ZONES = (new StringBuilder()).append(a.y).append("v1").append("/recommendedZones").toString();
        URL_RECOMMEND_POSTS = (new StringBuilder()).append(a.y).append("v1").append("/recommendedPosts").toString();
        URL_ZONES = (new StringBuilder()).append(a.y).append("v1").append("/zones").toString();
        URL_MY_ZONES = (new StringBuilder()).append(a.y).append("v1").append("/me/zones").toString();
        URL_POSTS = (new StringBuilder()).append(a.y).append("v1").append("/posts").toString();
        URL_COMMENTS = (new StringBuilder()).append(a.y).append("v1").append("/comments").toString();
        URL_REPLIES = (new StringBuilder()).append(a.y).append("v1").append("/replies").toString();
        URL_HOTPOST = (new StringBuilder()).append(a.y).append("v1").append("/hottestPosts/latest").toString();
        URL_UPLOAD_IMG = (new StringBuilder()).append(a.F).append("dtres/postPicture/upload").toString();
        URL_UPLOAD_COMMENT_IMG = (new StringBuilder()).append(a.F).append("dtres/postReply/upload").toString();
        URL_ZONE_REG = (new StringBuilder()).append(a.z).append("v1").append("/zoneRequests").toString();
        URL_ZONE_REPORT = (new StringBuilder()).append(a.z).append("v1").append("/reports").toString();
        URL_REPLY_MSG = (new StringBuilder()).append(a.u).append("mobile/").append("api1").append("/zone/message/in").toString();
        URL_ZONE_REQ_STATUS = (new StringBuilder()).append(a.z).append("v1").append("/zoneRequests/status").toString();
    }
}
