// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.adapter.zone.CommentAdapter;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.zone.CommentModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            PostFragment

class this._cls0 extends a
{

    final PostFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, mListView);
    }

    public void onFinish()
    {
        super.onFinish();
        PostFragment.access$402(PostFragment.this, false);
        ((PullToRefreshListView)mListView).onRefreshComplete();
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        showFooterView(com.ximalaya.ting.android.fragment..FooterView.NO_CONNECTION);
    }

    public void onStart()
    {
        super.onStart();
        PostFragment.access$302(PostFragment.this, true);
    }

    public void onSuccess(String s)
    {
        Object obj1;
        obj1 = null;
        if (!canGoon())
        {
            return;
        }
        s = PostFragment.access$2600(PostFragment.this, s);
        if (s == null)
        {
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
            return;
        }
        if (s.getIntValue("ret") != 0)
        {
            s = s.getString("msg");
            if (!TextUtils.isEmpty(s))
            {
                showToast(s);
            }
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
            return;
        }
        Object obj = s.getJSONObject("result");
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_268;
        }
        String s1;
        s = ((JSONObject) (obj)).getString("comments");
        s1 = ((JSONObject) (obj)).getString("parentComments");
        if (TextUtils.isEmpty(s))
        {
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
            return;
        }
          goto _L1
        Exception exception;
        exception;
        s = null;
_L4:
        List list;
        exception.printStackTrace();
        exception = s;
        list = obj1;
          goto _L2
_L1:
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/zone/CommentModel);
        list = obj1;
        exception = s;
        if (TextUtils.isEmpty(s1)) goto _L2; else goto _L3
_L3:
        list = JSON.parseArray(s1, com/ximalaya/ting/android/model/zone/CommentModel);
        exception = s;
_L2:
        if (exception == null || exception.size() == 0)
        {
            PostFragment.access$302(PostFragment.this, false);
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
            return;
        }
        if (exception.size() < 30)
        {
            PostFragment.access$302(PostFragment.this, false);
        }
        PostFragment.access$2900(PostFragment.this, exception, list);
        PostFragment.access$2500(PostFragment.this).addAll(exception);
        PostFragment.access$700(PostFragment.this).notifyDataSetChanged();
        showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
        return;
        exception;
          goto _L4
        exception = null;
        list = obj1;
          goto _L2
    }

    erView()
    {
        this$0 = PostFragment.this;
        super();
    }
}
