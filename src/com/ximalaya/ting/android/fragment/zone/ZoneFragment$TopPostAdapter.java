// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.model.zone.PostModel;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneFragment

private class mDataList extends BaseAdapter
{

    private ArrayList mDataList;
    final ZoneFragment this$0;

    public int getCount()
    {
        if (mDataList == null)
        {
            return 0;
        } else
        {
            return mDataList.size();
        }
    }

    public Object getItem(int i)
    {
        if (mDataList == null)
        {
            return null;
        } else
        {
            return (PostModel)mDataList.get(i);
        }
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        Object obj = (PostModel)mDataList.get(i);
        TextView textview;
        if (view == null)
        {
            view = View.inflate(mCon, 0x7f030132, null);
            viewgroup = new t>(ZoneFragment.this, null);
            view.setTag(viewgroup);
            viewgroup.NameTv = (TextView)view.findViewById(0x7f0a04dd);
            viewgroup.er = view.findViewById(0x7f0a00a8);
        } else
        {
            viewgroup = (er)view.getTag();
        }
        textview = ((er) (viewgroup)).NameTv;
        if (((PostModel) (obj)).getTitle() == null)
        {
            obj = "";
        } else
        {
            obj = ((PostModel) (obj)).getTitle();
        }
        textview.setText(((CharSequence) (obj)));
        if (i + 1 == mDataList.size())
        {
            ((mDataList) (viewgroup)).er.setVisibility(8);
            return view;
        } else
        {
            ((er) (viewgroup)).er.setVisibility(0);
            return view;
        }
    }

    public I(ArrayList arraylist)
    {
        this$0 = ZoneFragment.this;
        super();
        mDataList = new ArrayList();
        mDataList = arraylist;
    }
}
