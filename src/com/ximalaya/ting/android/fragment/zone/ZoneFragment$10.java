// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.adapter.zone.PostAdapter;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneFragment

class val.pullFromEnd extends a
{

    final ZoneFragment this$0;
    final boolean val$pullFromEnd;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, mListView);
    }

    public void onFinish()
    {
        super.onFinish();
        ZoneFragment.access$102(ZoneFragment.this, false);
        ((PullToRefreshListView)mListView).onRefreshComplete();
        if (ZoneFragment.access$2000(ZoneFragment.this).getVisibility() == 0)
        {
            ZoneFragment.access$2000(ZoneFragment.this).setVisibility(8);
        }
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        showFooterView(com.ximalaya.ting.android.fragment..FooterView.NO_CONNECTION);
        ZoneFragment.access$1500(ZoneFragment.this).setVisibility(8);
    }

    public void onStart()
    {
        super.onStart();
        ZoneFragment.access$002(ZoneFragment.this, true);
    }

    public void onSuccess(String s)
    {
        if (!canGoon())
        {
            return;
        }
        s = ZoneFragment.access$1000(ZoneFragment.this, s);
        if (s == null)
        {
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
            return;
        }
        if (s.getIntValue("ret") != 0)
        {
            s = s.getString("msg");
            if (!TextUtils.isEmpty(s))
            {
                showToast(s);
            }
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
            return;
        }
        s = s.getString("result");
        if (TextUtils.isEmpty(s))
        {
            ZoneFragment.access$002(ZoneFragment.this, false);
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
            return;
        }
        try
        {
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/zone/PostModel);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null || s.size() == 0)
        {
            ZoneFragment.access$002(ZoneFragment.this, false);
            if (val$pullFromEnd)
            {
                showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
                return;
            } else
            {
                showFooterView(com.ximalaya.ting.android.fragment..FooterView.NO_DATA);
                return;
            }
        }
        if (s.size() < 30)
        {
            ZoneFragment.access$002(ZoneFragment.this, false);
        }
        if (val$pullFromEnd)
        {
            ZoneFragment.access$600(ZoneFragment.this).addAll(s);
        } else
        {
            ZoneFragment.access$1600(ZoneFragment.this, s);
        }
        if (ZoneFragment.access$800(ZoneFragment.this).isEmpty())
        {
            ZoneFragment.access$1500(ZoneFragment.this).setVisibility(8);
            ZoneFragment.access$1700(ZoneFragment.this).setVisibility(8);
        } else
        {
            ZoneFragment.access$1700(ZoneFragment.this).setVisibility(0);
            ZoneFragment.access$1500(ZoneFragment.this).setVisibility(0);
        }
        ViewUtil.setListViewHeight(ZoneFragment.access$1500(ZoneFragment.this));
        ZoneFragment.access$1800(ZoneFragment.this).notifyDataSetChanged();
        ZoneFragment.access$1900(ZoneFragment.this).notifyDataSetChanged();
        showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
    }

    PostAdapter()
    {
        this$0 = final_zonefragment;
        val$pullFromEnd = Z.this;
        super();
    }
}
