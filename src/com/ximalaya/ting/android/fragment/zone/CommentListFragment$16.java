// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.adapter.zone.CommentAdapter;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            CommentListFragment

class this._cls0 extends a
{

    final CommentListFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, mListView);
    }

    public void onFinish()
    {
        super.onFinish();
        CommentListFragment.access$2902(CommentListFragment.this, false);
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        showFooterView(com.ximalaya.ting.android.fragment.View.NO_CONNECTION);
        ((PullToRefreshListView)mListView).onRefreshComplete();
    }

    public void onStart()
    {
        CommentListFragment.access$502(CommentListFragment.this, true);
        CommentListFragment.access$302(CommentListFragment.this, true);
    }

    public void onSuccess(String s)
    {
        while (!canGoon() || s == null) 
        {
            return;
        }
        s = CommentListFragment.access$2700(CommentListFragment.this, s);
        if (s == null)
        {
            try
            {
                showFooterView(com.ximalaya.ting.android.fragment.View.NO_DATA);
                ((PullToRefreshListView)mListView).onRefreshComplete();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
            return;
        }
        int i = s.getIntValue("ret");
        if (i == 0)
        {
            break MISSING_BLOCK_LABEL_134;
        }
        s = s.getString("msg");
        if (i != 311) goto _L2; else goto _L1
_L1:
        showToast("\u5E16\u5B50\u4E0D\u5B58\u5728\u6216\u8005\u5DF2\u7ECF\u88AB\u5220\u9664");
_L4:
        showFooterView(com.ximalaya.ting.android.fragment.View.NO_DATA);
        ((PullToRefreshListView)mListView).onRefreshComplete();
        return;
_L2:
        if (!TextUtils.isEmpty(s))
        {
            showToast(s);
        }
        if (true) goto _L4; else goto _L3
_L3:
        s = s.getString("result");
        if (TextUtils.isEmpty(s))
        {
            showFooterView(com.ximalaya.ting.android.fragment.View.NO_DATA);
            ((PullToRefreshListView)mListView).onRefreshComplete();
            return;
        }
        CommentListFragment.access$802(CommentListFragment.this, (PostModel)JSON.parseObject(s, com/ximalaya/ting/android/model/zone/PostModel));
_L5:
        if (CommentListFragment.access$800(CommentListFragment.this) != null)
        {
            CommentListFragment.access$600(CommentListFragment.this).setHostUid(CommentListFragment.access$800(CommentListFragment.this).getPoster().getUid());
            CommentListFragment.access$2800(CommentListFragment.this, mListView, true, false);
            return;
        }
        break MISSING_BLOCK_LABEL_276;
        s;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
          goto _L5
        showFooterView(com.ximalaya.ting.android.fragment.View.NO_DATA);
        showToast("\u5E16\u5B50\u4E0D\u5B58\u5728\u6216\u8005\u5DF2\u7ECF\u88AB\u5220\u9664");
        ((PullToRefreshListView)mListView).onRefreshComplete();
        return;
    }

    ()
    {
        this$0 = CommentListFragment.this;
        super();
    }
}
