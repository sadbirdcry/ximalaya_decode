// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneEntryFragmentNew, PostFragment

class this._cls0
    implements android.widget.Listener
{

    final ZoneEntryFragmentNew this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        i -= ZoneEntryFragmentNew.access$300(ZoneEntryFragmentNew.this).getHeaderViewsCount();
        if (i < 0 || ZoneEntryFragmentNew.access$400(ZoneEntryFragmentNew.this).size() < i + 1 || !OneClickHelper.getInstance().onClick(view))
        {
            return;
        } else
        {
            adapterview = new Bundle();
            adapterview.putLong("zoneId", ((PostModel)ZoneEntryFragmentNew.access$400(ZoneEntryFragmentNew.this).get(i)).getZoneId());
            adapterview.putLong("postId", ((PostModel)ZoneEntryFragmentNew.access$400(ZoneEntryFragmentNew.this).get(i)).getId());
            adapterview.putLong("hostUid", ((PostModel)ZoneEntryFragmentNew.access$400(ZoneEntryFragmentNew.this).get(i)).getPoster().getUid());
            adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/zone/PostFragment, adapterview);
            return;
        }
    }

    ()
    {
        this$0 = ZoneEntryFragmentNew.this;
        super();
    }
}
