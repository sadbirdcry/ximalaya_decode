// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.zone.ZoneAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.zone.RoleInfo;
import com.ximalaya.ting.android.model.zone.ZoneModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneConstants

public class ZoneListFragment extends BaseListFragment
    implements com.ximalaya.ting.android.adapter.zone.ZoneAdapter.ZoneActionListener
{

    public static final String EXTRA_CATEGROY_ID = "category_id";
    public static final String EXTRA_ZONE_LIST = "zone_list";
    public static final String EXTRA_ZONE_TYPE = "zone_type";
    private static final int MAX_SIZE = 30;
    public static final int TYPE_CATEGORY = 2;
    public static final int TYPE_MINE = 0;
    public static final int TYPE_RECOMMEND = 1;
    private ZoneAdapter mAdapter;
    private long mCategoryId;
    private ArrayList mDataList;
    private boolean mHasMore;
    private boolean mIsLoading;
    private ProgressBar mProgressBar;
    private int mType;

    public ZoneListFragment()
    {
        mDataList = new ArrayList();
        mHasMore = true;
    }

    private void initData()
    {
        Object obj;
        obj = getArguments();
        mType = ((Bundle) (obj)).getInt("zone_type");
        if (mType != 2) goto _L2; else goto _L1
_L1:
        mCategoryId = ((Bundle) (obj)).getLong("category_id");
_L4:
        long l = getAnimationLeftTime();
        if (l > 0L)
        {
            fragmentBaseContainerView.postDelayed(new _cls1(), l);
            return;
        }
        break; /* Loop/switch isn't completed */
_L2:
        mDataList.clear();
        obj = (List)((Bundle) (obj)).getSerializable("zone_list");
        if (obj != null)
        {
            mDataList.addAll(((java.util.Collection) (obj)));
        }
        if (mType != 1)
        {
            break; /* Loop/switch isn't completed */
        }
        mHasMore = false;
        if (true) goto _L4; else goto _L3
_L3:
        if (30 > mDataList.size())
        {
            mHasMore = false;
        }
        Iterator iterator = mDataList.iterator();
        while (iterator.hasNext()) 
        {
            if (!((ZoneModel)iterator.next()).getMyZone().isJoint())
            {
                iterator.remove();
            }
        }
        if (true) goto _L4; else goto _L5
_L5:
        if (mType == 2)
        {
            ((PullToRefreshListView)mListView).toRefreshing();
            return;
        } else
        {
            mAdapter.notifyDataSetChanged();
            return;
        }
    }

    private void initListener()
    {
        mListView.setOnScrollListener(new _cls2());
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls3());
        ((PullToRefreshListView)mListView).setOnItemClickListener(new _cls4());
        mFooterViewLoading.setOnClickListener(new _cls5());
    }

    private void initView()
    {
        TextView textview = (TextView)findViewById(0x7f0a00ae);
        if (mType == 0)
        {
            textview.setText("\u6211\u7684\u5708\u5B50");
        } else
        {
            textview.setText("\u63A8\u8350\u5708\u5B50");
        }
        mProgressBar = (ProgressBar)findViewById(0x7f0a0091);
        mAdapter = new ZoneAdapter(this, mCon, mDataList);
        mListView.setAdapter(mAdapter);
    }

    private void loadData(final boolean pullFromTop)
    {
        if (mIsLoading)
        {
            return;
        }
        if (!pullFromTop)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        }
        mHasMore = true;
        mIsLoading = true;
        RequestParams requestparams = new RequestParams();
        String s;
        if (mType != 1)
        {
            if (pullFromTop || mDataList == null || mDataList.isEmpty())
            {
                requestparams.put("timeline", 0);
            } else
            {
                requestparams.put("timeline", ((ZoneModel)mDataList.get(mDataList.size() - 1)).getTimeline());
            }
            if (mType == 2)
            {
                s = (new StringBuilder()).append(a.y).append("v1").append("/soundCategory/").append(mCategoryId).append("/recommendedZones").toString();
            } else
            {
                s = ZoneConstants.URL_MY_ZONES;
            }
        } else
        {
            s = ZoneConstants.URL_RECOMMEND_ZONES;
        }
        requestparams.put("maxSizeOfZones", 30);
        f.a().a(s, requestparams, DataCollectUtil.getDataFromView(mListView), new _cls6());
    }

    private JSONObject parseJSON(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            return null;
        }
        return s;
    }

    public void follow(View view, int i)
    {
        if (i < 0 || i + 1 > mDataList.size())
        {
            return;
        } else
        {
            mProgressBar.setVisibility(0);
            final ZoneModel data = (ZoneModel)mDataList.get(i);
            RequestParams requestparams = new RequestParams();
            requestparams.put("zoneId", (new StringBuilder()).append("").append(data.getId()).toString());
            f.a().b(ZoneConstants.URL_MY_ZONES, requestparams, DataCollectUtil.getDataFromView(view), new _cls7());
            return;
        }
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initData();
        initView();
        initListener();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300f0, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onPause()
    {
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
    }

    public void unFollow(View view, int i)
    {
        if (i < 0 || i + 1 > mDataList.size())
        {
            return;
        } else
        {
            mProgressBar.setVisibility(0);
            final ZoneModel data = (ZoneModel)mDataList.get(i);
            String s = (new StringBuilder()).append(ZoneConstants.URL_MY_ZONES).append("/").append(data.getId()).toString();
            f.a().a(s, DataCollectUtil.getDataFromView(view), new _cls8());
            return;
        }
    }





/*
    static boolean access$202(ZoneListFragment zonelistfragment, boolean flag)
    {
        zonelistfragment.mHasMore = flag;
        return flag;
    }

*/



/*
    static boolean access$302(ZoneListFragment zonelistfragment, boolean flag)
    {
        zonelistfragment.mIsLoading = flag;
        return flag;
    }

*/





    private class _cls1
        implements Runnable
    {

        final ZoneListFragment this$0;

        public void run()
        {
label0:
            {
                if (canGoon())
                {
                    if (mType != 2)
                    {
                        break label0;
                    }
                    ((PullToRefreshListView)mListView).toRefreshing();
                }
                return;
            }
            mAdapter.notifyDataSetChanged();
        }

        _cls1()
        {
            this$0 = ZoneListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AbsListView.OnScrollListener
    {

        final ZoneListFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || !mHasMore)
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        loadData(false);
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls2()
        {
            this$0 = ZoneListFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final ZoneListFragment this$0;

        public void onRefresh()
        {
            loadData(true);
        }

        _cls3()
        {
            this$0 = ZoneListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemClickListener
    {

        final ZoneListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (!OneClickHelper.getInstance().onClick(view) || i < 0 || (i + 1) - mListView.getHeaderViewsCount() > mDataList.size())
            {
                return;
            } else
            {
                ToolUtil.onEvent(mCon, "zone_main");
                adapterview = new Bundle();
                adapterview.putLong("zoneId", ((ZoneModel)mDataList.get(i - mListView.getHeaderViewsCount())).getId());
                adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment, adapterview);
                return;
            }
        }

        _cls4()
        {
            this$0 = ZoneListFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final ZoneListFragment this$0;

        public void onClick(View view)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            ((PullToRefreshListView)mListView).toRefreshing();
        }

        _cls5()
        {
            this$0 = ZoneListFragment.this;
            super();
        }
    }


    private class _cls6 extends com.ximalaya.ting.android.b.a
    {

        final ZoneListFragment this$0;
        final boolean val$pullFromTop;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            mIsLoading = false;
            ((PullToRefreshListView)mListView).onRefreshComplete();
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
        }

        public void onSuccess(String s)
        {
            if (!canGoon())
            {
                return;
            }
            s = parseJSON(s);
            if (s == null)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
            if (s.getIntValue("ret") != 0)
            {
                s = s.getString("msg");
                if (!TextUtils.isEmpty(s))
                {
                    showToast(s);
                }
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            if (pullFromTop)
            {
                mDataList.clear();
            }
            s = s.getString("result");
            if (TextUtils.isEmpty(s))
            {
                mHasMore = false;
                if (pullFromTop)
                {
                    showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                } else
                {
                    showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                }
                mAdapter.notifyDataSetChanged();
                return;
            }
            try
            {
                s = JSON.parseArray(s, com/ximalaya/ting/android/model/zone/ZoneModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = null;
            }
            if (s == null || s.size() == 0)
            {
                if (pullFromTop)
                {
                    showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                } else
                {
                    showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                }
                mHasMore = false;
                mAdapter.notifyDataSetChanged();
                return;
            }
            if (s.size() < 30 || mType == 1)
            {
                mHasMore = false;
            }
            mDataList.addAll((ArrayList)s);
            mAdapter.notifyDataSetChanged();
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls6()
        {
            this$0 = ZoneListFragment.this;
            pullFromTop = flag;
            super();
        }
    }


    private class _cls7 extends com.ximalaya.ting.android.b.a
    {

        final ZoneListFragment this$0;
        final ZoneModel val$data;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            mProgressBar.setVisibility(8);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            while (!canGoon() || TextUtils.isEmpty(s)) 
            {
                return;
            }
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                return;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = null;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF";
                } else
                {
                    s = (new StringBuilder()).append("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u53D1\u751F\u9519\u8BEF:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                data.getMyZone().setIsJoint(true);
                mAdapter.notifyDataSetChanged();
                showToast("\u7533\u8BF7\u52A0\u5165\u5708\u5B50\u6210\u529F");
                return;
            }
        }

        _cls7()
        {
            this$0 = ZoneListFragment.this;
            data = zonemodel;
            super();
        }
    }


    private class _cls8 extends com.ximalaya.ting.android.b.a
    {

        final ZoneListFragment this$0;
        final ZoneModel val$data;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            mProgressBar.setVisibility(8);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onSuccess(String s)
        {
            Object obj;
            for (obj = null; !canGoon() || TextUtils.isEmpty(s);)
            {
                return;
            }

            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = obj;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u9000\u51FA\u5708\u5B50\u53D1\u751F\u9519\u8BEF";
                } else
                {
                    s = (new StringBuilder()).append("\u9000\u51FA\u5708\u5B50\u53D1\u751F\u9519\u8BEF:").append(s).toString();
                }
                showToast(s);
                return;
            } else
            {
                data.getMyZone().setIsJoint(false);
                mAdapter.notifyDataSetChanged();
                showToast("\u9000\u51FA\u5708\u5B50\u6210\u529F");
                return;
            }
        }

        _cls8()
        {
            this$0 = ZoneListFragment.this;
            data = zonemodel;
            super();
        }
    }

}
