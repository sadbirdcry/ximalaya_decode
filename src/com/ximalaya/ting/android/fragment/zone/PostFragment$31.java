// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.app.ProgressDialog;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.zone.CommentModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            PostFragment

class val.view extends a
{

    final PostFragment this$0;
    final View val$view;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$view);
    }

    public void onFinish()
    {
        super.onFinish();
        if (pd != null && pd.isShowing())
        {
            pd.cancel();
            pd = null;
        }
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
    }

    public void onSuccess(String s)
    {
        if (canGoon() && !TextUtils.isEmpty(s))
        {
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (s == null)
                {
                    s = null;
                } else
                {
                    s = s.getString("msg");
                }
                if (TextUtils.isEmpty(s))
                {
                    s = "\u53D1\u8868\u56DE\u590D\u5931\u8D25";
                } else
                {
                    s = (new StringBuilder()).append("\u53D1\u8868\u56DE\u590D\u5931\u8D25:").append(s).toString();
                }
                showToast(s);
                return;
            }
            showToast("\u53D1\u8868\u56DE\u590D\u6210\u529F");
            s = s.getString("result");
            if (TextUtils.isEmpty(s))
            {
                finish();
                return;
            }
            try
            {
                s = (CommentModel)JSON.parseObject(s, com/ximalaya/ting/android/model/zone/CommentModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = null;
            }
            if (s == null)
            {
                finish();
                return;
            }
            PostFragment.access$3600(PostFragment.this);
            PostFragment.access$4100(PostFragment.this, false);
            if (!PostFragment.access$300(PostFragment.this))
            {
                PostFragment.access$500(PostFragment.this, val$view, true);
                return;
            }
        }
    }

    ()
    {
        this$0 = final_postfragment;
        val$view = View.this;
        super();
    }
}
