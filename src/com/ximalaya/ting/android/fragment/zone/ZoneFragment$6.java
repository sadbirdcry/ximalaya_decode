// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneFragment, PostFragment

class this._cls0
    implements android.widget.temClickListener
{

    final ZoneFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        while (!OneClickHelper.getInstance().onClick(view) || i < 0 || ZoneFragment.access$800(ZoneFragment.this).size() < i + 1) 
        {
            return;
        }
        adapterview = new Bundle();
        adapterview.putLong("zoneId", ((PostModel)ZoneFragment.access$800(ZoneFragment.this).get(i)).getZoneId());
        adapterview.putLong("postId", ((PostModel)ZoneFragment.access$800(ZoneFragment.this).get(i)).getId());
        adapterview.putLong("hostUid", ((PostModel)ZoneFragment.access$800(ZoneFragment.this).get(i)).getPoster().getUid());
        adapterview.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startFragment(com/ximalaya/ting/android/fragment/zone/PostFragment, adapterview);
    }

    ()
    {
        this$0 = ZoneFragment.this;
        super();
    }
}
