// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.view.BlurableImageView;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            ZoneDetailFragment

class this._cls0
    implements com.ximalaya.ting.android.util.Callback
{

    final ZoneDetailFragment this$0;

    public void onCompleteDisplay(String s, Bitmap bitmap)
    {
        if (mCon == null)
        {
            return;
        }
        Bitmap bitmap1 = ImageManager2.from(mCon).getFromMemCache((new StringBuilder()).append(s).append("/blur").toString());
        if (bitmap1 != null)
        {
            ZoneDetailFragment.access$000(ZoneDetailFragment.this).setImageBitmap(bitmap1);
            return;
        }
        if (bitmap != null)
        {
            ZoneDetailFragment.access$000(ZoneDetailFragment.this).blur(new BitmapDrawable(mCon.getResources(), bitmap), (new StringBuilder()).append(s).append("/blur").toString(), true);
            return;
        } else
        {
            ZoneDetailFragment.access$000(ZoneDetailFragment.this).setImageDrawable(new ColorDrawable(Color.parseColor("#b3202332")));
            ZoneDetailFragment.access$000(ZoneDetailFragment.this).setResourceUrl(null);
            return;
        }
    }

    ()
    {
        this$0 = ZoneDetailFragment.this;
        super();
    }
}
