// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.zone;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.adapter.zone.CommentAdapter;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.zone.CommentModel;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.zone:
//            PostFragment

class this._cls0 extends a
{

    final PostFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, mListView);
    }

    public void onFinish()
    {
        super.onFinish();
        PostFragment.access$2802(PostFragment.this, false);
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        showFooterView(com.ximalaya.ting.android.fragment..FooterView.NO_CONNECTION);
        ((PullToRefreshListView)mListView).onRefreshComplete();
    }

    public void onSuccess(String s)
    {
        int i;
        while (!canGoon() || s == null) 
        {
            return;
        }
        s = PostFragment.access$2600(PostFragment.this, s);
        if (s == null)
        {
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.NO_DATA);
            ((PullToRefreshListView)mListView).onRefreshComplete();
            return;
        }
        i = s.getIntValue("ret");
        if (i == 0) goto _L2; else goto _L1
_L1:
        s = s.getString("msg");
        if (i != 311) goto _L4; else goto _L3
_L3:
        showToast("\u5E16\u5B50\u4E0D\u5B58\u5728\u6216\u8005\u5DF2\u7ECF\u88AB\u5220\u9664");
_L5:
        showFooterView(com.ximalaya.ting.android.fragment..FooterView.HIDE_ALL);
        ((PullToRefreshListView)mListView).onRefreshComplete();
        return;
_L4:
        if (!TextUtils.isEmpty(s))
        {
            showToast(s);
        }
        if (true) goto _L5; else goto _L2
_L2:
        s = s.getString("result");
        if (TextUtils.isEmpty(s))
        {
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.NO_DATA);
            ((PullToRefreshListView)mListView).onRefreshComplete();
            return;
        }
        try
        {
            PostFragment.access$2702(PostFragment.this, (PostModel)JSON.parseObject(s, com/ximalaya/ting/android/model/zone/PostModel));
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
        }
        if (PostFragment.access$2700(PostFragment.this) != null)
        {
            PostFragment.access$2500(PostFragment.this).clear();
            PostFragment.access$2500(PostFragment.this).add(CommentModel.convert(PostFragment.access$2700(PostFragment.this)));
            PostFragment.access$700(PostFragment.this).notifyDataSetChanged();
            PostFragment.access$500(PostFragment.this, mListView, false);
            return;
        } else
        {
            showFooterView(com.ximalaya.ting.android.fragment..FooterView.NO_DATA);
            ((PullToRefreshListView)mListView).onRefreshComplete();
            return;
        }
    }

    tView()
    {
        this$0 = PostFragment.this;
        super();
    }
}
