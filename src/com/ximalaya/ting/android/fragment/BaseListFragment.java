// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseActivityLikeFragment

public class BaseListFragment extends BaseActivityLikeFragment
{
    public static final class FooterView extends Enum
    {

        private static final FooterView $VALUES[];
        public static final FooterView FAIL_GET_DATA;
        public static final FooterView HIDE_ALL;
        public static final FooterView LOADING;
        public static final FooterView MORE;
        public static final FooterView NO_CONNECTION;
        public static final FooterView NO_DATA;

        public static FooterView valueOf(String s)
        {
            return (FooterView)Enum.valueOf(com/ximalaya/ting/android/fragment/BaseListFragment$FooterView, s);
        }

        public static FooterView[] values()
        {
            return (FooterView[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterView("MORE", 0);
            LOADING = new FooterView("LOADING", 1);
            NO_CONNECTION = new FooterView("NO_CONNECTION", 2);
            HIDE_ALL = new FooterView("HIDE_ALL", 3);
            NO_DATA = new FooterView("NO_DATA", 4);
            FAIL_GET_DATA = new FooterView("FAIL_GET_DATA", 5);
            $VALUES = (new FooterView[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA, FAIL_GET_DATA
            });
        }

        private FooterView(String s, int i)
        {
            super(s, i);
        }
    }


    public MyAsyncTask mDataLoadTask;
    public RelativeLayout mFooterViewLoading;
    public ListView mListView;

    public BaseListFragment()
    {
    }

    public void initCommon()
    {
        super.initCommon();
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(mCon).inflate(0x7f0301fb, null);
        mListView.addFooterView(mFooterViewLoading);
        showFooterView(FooterView.HIDE_ALL);
    }

    public void setFooterViewText(String s)
    {
        mFooterViewLoading.setVisibility(0);
        mFooterViewLoading.setClickable(false);
        mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
        mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
        ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText((new StringBuilder()).append(s).append("").toString());
    }

    public void showFooterView(FooterView footerview)
    {
        if (canGoon())
        {
            mListView.setFooterDividersEnabled(false);
            mFooterViewLoading.setVisibility(0);
            if (footerview == FooterView.MORE)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                return;
            }
            if (footerview == FooterView.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerview == FooterView.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == FooterView.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u6CA1\u6709\u6570\u636E");
                return;
            }
            if (footerview == FooterView.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
            if (footerview == FooterView.FAIL_GET_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
        }
    }
}
