// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseActivityLikeFragment

public class BaseFragmentGroup extends BaseActivityLikeFragment
{
    private class ClassInfo
    {

        Bundle bundle;
        Class className;
        final BaseFragmentGroup this$0;

        private ClassInfo()
        {
            this$0 = BaseFragmentGroup.this;
            super();
            bundle = null;
        }

    }


    public String mCurrentFragmentTag;
    public HashMap mStacks;
    private HashMap nameStacks;
    private RelativeLayout titleBarView;

    public BaseFragmentGroup()
    {
        mStacks = new HashMap();
        nameStacks = new HashMap();
    }

    private void pushFragments(String s, Fragment fragment, boolean flag)
    {
        if (flag)
        {
            mStacks.put(s, fragment);
            FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
            fragmenttransaction.add(0x7f0a0070, fragment);
            fragmenttransaction.commitAllowingStateLoss();
            if (getCurrentFragment() != null)
            {
                getCurrentFragment().onPause();
            }
        } else
        {
            showFragmentByTag(s);
        }
        mCurrentFragmentTag = s;
    }

    private void setCurrentFragment(String s)
    {
        if (mStacks.get(s) != null) goto _L2; else goto _L1
_L1:
        Object obj1 = null;
        Object obj2 = ((ClassInfo)nameStacks.get(s)).className.newInstance();
        Object obj;
        obj = obj1;
        if (obj2 == null)
        {
            break MISSING_BLOCK_LABEL_55;
        }
        obj = obj1;
        if (obj2 instanceof Fragment)
        {
            obj = (Fragment)obj2;
        }
        obj1 = obj;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_100;
        }
        obj1 = obj;
        if (((ClassInfo)nameStacks.get(s)).bundle == null)
        {
            break MISSING_BLOCK_LABEL_100;
        }
        ((Fragment) (obj)).setArguments(((ClassInfo)nameStacks.get(s)).bundle);
        obj1 = obj;
_L3:
        if (obj1 != null)
        {
            pushFragments(s, ((Fragment) (obj1)), true);
            return;
        } else
        {
            Logger.log("fragment\u751F\u6210\u5931\u8D25\uFF01\uFF01\uFF01");
            return;
        }
        obj;
        obj = null;
_L4:
        obj1 = obj;
          goto _L3
_L2:
        pushFragments(s, (Fragment)mStacks.get(s), false);
        return;
        Exception exception;
        exception;
          goto _L4
    }

    private void showFragmentByTag(String s)
    {
        if (getCurrentFragment() != null)
        {
            getCurrentFragment().onPause();
        }
        FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
        Iterator iterator = mStacks.entrySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            java.util.Map.Entry entry = (java.util.Map.Entry)iterator.next();
            if (entry.getKey() != null && entry.getValue() != null)
            {
                if (((String)entry.getKey()).equals(s))
                {
                    fragmenttransaction.show((Fragment)entry.getValue());
                    ((Fragment)entry.getValue()).onResume();
                } else
                {
                    fragmenttransaction.hide((Fragment)entry.getValue());
                }
            }
        } while (true);
        fragmenttransaction.commitAllowingStateLoss();
    }

    public Fragment getCurrentFragment()
    {
        if (mStacks.get(mCurrentFragmentTag) != null)
        {
            return (Fragment)mStacks.get(mCurrentFragmentTag);
        } else
        {
            Logger.log("\u6CA1\u627E\u5230CurrentFragment");
            return null;
        }
    }

    public String getCurrentFragmentTag()
    {
        return mCurrentFragmentTag;
    }

    public Fragment getFragmentByTag(String s)
    {
        if (mStacks.get(s) != null)
        {
            return (Fragment)mStacks.get(s);
        } else
        {
            Logger.log("stack\u4E2D\u6CA1\u627E\u5230\u8BE5\u7C7B");
            return null;
        }
    }

    public void init()
    {
        if (mStacks == null)
        {
            mStacks = new HashMap();
        }
        if (nameStacks == null)
        {
            nameStacks = new HashMap();
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        mCon = getActivity().getApplicationContext();
        if (getArguments() != null && getArguments().containsKey("sideslip"))
        {
            fragmentBaseContainerView = layoutinflater.inflate(0x7f030048, viewgroup, false);
        } else
        {
            fragmentBaseContainerView = layoutinflater.inflate(0x7f030047, viewgroup, false);
        }
        titleBarView = (RelativeLayout)fragmentBaseContainerView.findViewById(0x7f0a00de);
        init();
        return fragmentBaseContainerView;
    }

    public void onPause()
    {
        super.onPause();
        if (getCurrentFragment() != null)
        {
            getCurrentFragment().onPause();
        }
    }

    public void onResume()
    {
        super.onResume();
        if (getCurrentFragment() != null)
        {
            getCurrentFragment().onResume();
        }
    }

    public void putFragmentInStacks(String s, Class class1)
    {
        ClassInfo classinfo = new ClassInfo();
        classinfo.className = class1;
        nameStacks.put(s, classinfo);
    }

    public void putFragmentInStacks(String s, Class class1, Bundle bundle)
    {
        ClassInfo classinfo = new ClassInfo();
        classinfo.className = class1;
        classinfo.bundle = bundle;
        nameStacks.put(s, classinfo);
    }

    public void setCurrentFragmentByTag(String s)
    {
        if (s == null || s.equals(mCurrentFragmentTag))
        {
            return;
        }
        if (nameStacks.get(s) != null)
        {
            setCurrentFragment(s);
            return;
        } else
        {
            Logger.throwRuntimeException("\u5FC5\u987B\u5148\u8C03\u7528putFragmentInStacks\u65B9\u6CD5\uFF01\uFF01\uFF01\uFF01\uFF01");
            return;
        }
    }

    public void setTitleBarView(View view)
    {
        android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, (int)getResources().getDimension(0x7f080025));
        titleBarView.addView(view, layoutparams);
    }

    public void setTitleBarView(View view, int i)
    {
        android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, ToolUtil.dp2px(getActivity(), i));
        titleBarView.addView(view, layoutparams);
    }
}
