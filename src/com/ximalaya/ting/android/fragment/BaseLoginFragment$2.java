// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.os.Bundle;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WeiboAuthListener;
import com.sina.weibo.sdk.exception.WeiboException;
import com.ximalaya.ting.android.model.auth.AuthInfo;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseLoginFragment

class this._cls0
    implements WeiboAuthListener
{

    final BaseLoginFragment this$0;

    public void onCancel()
    {
    }

    public void onComplete(Bundle bundle)
    {
        if (Oauth2AccessToken.parseAccessToken(bundle).isSessionValid())
        {
            AuthInfo authinfo = new AuthInfo();
            authinfo.access_token = bundle.getString("access_token");
            authinfo.expires_in = bundle.getString("expires_in");
            authinfo.openid = bundle.getString("uid");
            (new ginTask(mActivity, getArguments())).myexec(new Object[] {
                Integer.valueOf(1), authinfo
            });
            return;
        } else
        {
            bundle = bundle.getString("code", "");
            showToast((new StringBuilder()).append("Weibo error code:").append(bundle).toString());
            return;
        }
    }

    public void onWeiboException(WeiboException weiboexception)
    {
        showToast((new StringBuilder()).append("weibo\u5F02\u5E38:").append(weiboexception.getMessage()).toString());
    }

    ginTask()
    {
        this$0 = BaseLoginFragment.this;
        super();
    }
}
