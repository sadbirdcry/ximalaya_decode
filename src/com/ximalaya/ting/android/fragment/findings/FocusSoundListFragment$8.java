// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.SoundsHotAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.sound.HotSoundModel;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.findings:
//            FocusSoundListFragment

class val.view extends MyAsyncTask
{

    final FocusSoundListFragment this$0;
    final View val$view;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        avoid = (new StringBuilder()).append(a.u).append("m/focus_list").toString();
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("page", (new StringBuilder()).append(FocusSoundListFragment.access$100(FocusSoundListFragment.this)).append("").toString());
        ((RequestParams) (obj)).put("per_page", (new StringBuilder()).append(FocusSoundListFragment.access$600(FocusSoundListFragment.this)).append("").toString());
        ((RequestParams) (obj)).put("type", FocusSoundListFragment.access$1200(FocusSoundListFragment.this));
        ((RequestParams) (obj)).put("id", FocusSoundListFragment.access$1300(FocusSoundListFragment.this));
        obj = f.a().a(avoid, ((RequestParams) (obj)), val$view, FocusSoundListFragment.access$200(FocusSoundListFragment.this));
        Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
        avoid = null;
        try
        {
            obj = JSON.parseObject(((String) (obj)));
            String s = ((JSONObject) (obj)).get("ret").toString();
            if (FocusSoundListFragment.access$700(FocusSoundListFragment.this) == 0)
            {
                FocusSoundListFragment.access$702(FocusSoundListFragment.this, ((JSONObject) (obj)).getIntValue("count"));
            }
            if ("0".equals(s))
            {
                avoid = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/sound/HotSoundModel);
            }
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
            return null;
        }
        return avoid;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        if (mCon != null && isAdded())
        {
            FocusSoundListFragment.access$002(FocusSoundListFragment.this, false);
            FocusSoundListFragment.access$200(FocusSoundListFragment.this).onRefreshComplete();
            if (list == null)
            {
                FocusSoundListFragment.access$1400(FocusSoundListFragment.this, true);
                if (FocusSoundListFragment.access$900(FocusSoundListFragment.this).size() > 0)
                {
                    FocusSoundListFragment.access$400(FocusSoundListFragment.this, oterView.NO_DATA);
                    return;
                }
            } else
            {
                if (FocusSoundListFragment.access$100(FocusSoundListFragment.this) == 1)
                {
                    FocusSoundListFragment.access$900(FocusSoundListFragment.this).clear();
                    FocusSoundListFragment.access$900(FocusSoundListFragment.this).addAll(list);
                    FocusSoundListFragment.access$1100(FocusSoundListFragment.this).notifyDataSetChanged();
                } else
                {
                    FocusSoundListFragment.access$900(FocusSoundListFragment.this).addAll(list);
                    FocusSoundListFragment.access$1100(FocusSoundListFragment.this).notifyDataSetChanged();
                }
                if (FocusSoundListFragment.access$1500(FocusSoundListFragment.this))
                {
                    FocusSoundListFragment.access$400(FocusSoundListFragment.this, oterView.MORE);
                } else
                {
                    FocusSoundListFragment.access$400(FocusSoundListFragment.this, oterView.HIDE_ALL);
                }
                int _tmp = FocusSoundListFragment.access$108(FocusSoundListFragment.this);
                FocusSoundListFragment.access$1400(FocusSoundListFragment.this, false);
                return;
            }
        }
    }

    protected void onPreExecute()
    {
        FocusSoundListFragment.access$002(FocusSoundListFragment.this, true);
        FocusSoundListFragment.access$1400(FocusSoundListFragment.this, false);
    }

    oterView()
    {
        this$0 = final_focussoundlistfragment;
        val$view = View.this;
        super();
    }
}
