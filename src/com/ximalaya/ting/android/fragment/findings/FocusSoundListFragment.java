// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.SoundsHotAdapter;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.model.sound.HotSoundModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public class FocusSoundListFragment extends BaseActivityLikeFragment
    implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{
    private static final class FooterView extends Enum
    {

        private static final FooterView $VALUES[];
        public static final FooterView HIDE_ALL;
        public static final FooterView LOADING;
        public static final FooterView MORE;
        public static final FooterView NO_CONNECTION;
        public static final FooterView NO_DATA;

        public static FooterView valueOf(String s)
        {
            return (FooterView)Enum.valueOf(com/ximalaya/ting/android/fragment/findings/FocusSoundListFragment$FooterView, s);
        }

        public static FooterView[] values()
        {
            return (FooterView[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterView("MORE", 0);
            LOADING = new FooterView("LOADING", 1);
            NO_CONNECTION = new FooterView("NO_CONNECTION", 2);
            HIDE_ALL = new FooterView("HIDE_ALL", 3);
            NO_DATA = new FooterView("NO_DATA", 4);
            $VALUES = (new FooterView[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA
            });
        }

        private FooterView(String s, int i)
        {
            super(s, i);
        }
    }


    private List dataList;
    private String id;
    private boolean loadingNextPage;
    private MyAsyncTask mDataLoadTask;
    private RelativeLayout mFooterViewLoading;
    private Handler mHandler;
    private PullToRefreshListView mListView;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private SoundsHotAdapter mSoundsHotAdapter;
    private ViewGroup noNetLayout;
    private int pageId;
    private int pageSize;
    private int soundType;
    private String title;
    private int totalCount;
    private String type;
    private String url;

    public FocusSoundListFragment()
    {
        totalCount = 0;
        pageId = 1;
        pageSize = 15;
        url = "m/category_track_list";
        loadingNextPage = false;
        soundType = 0;
        mHandler = new _cls1();
    }

    private void initData()
    {
        mListView.toRefreshing();
    }

    private void initListener()
    {
        mListView.setOnRefreshListener(new _cls2());
        noNetLayout.setOnClickListener(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
        mListView.setOnScrollListener(new _cls5());
        mListView.setOnItemClickListener(new _cls6());
        mListView.setOnItemLongClickListener(new _cls7());
    }

    private void initUi(LayoutInflater layoutinflater)
    {
        mListView = (PullToRefreshListView)findViewById(0x7f0a0095);
        dataList = new ArrayList();
        noNetLayout = (ViewGroup)findViewById(0x7f0a035b);
        mFooterViewLoading = (RelativeLayout)layoutinflater.inflate(0x7f0301fb, null);
        mListView.addFooterView(mFooterViewLoading);
        mSoundsHotAdapter = new SoundsHotAdapter(getActivity(), dataList, mHandler);
        mListView.setAdapter(mSoundsHotAdapter);
        showFooterView(FooterView.HIDE_ALL);
    }

    private void loadDataListData(final View view)
    {
        if (ToolUtil.isConnectToNetwork(mCon))
        {
            mDataLoadTask = (new _cls8()).myexec(new Void[0]);
            return;
        } else
        {
            mListView.onRefreshComplete();
            showFooterView(FooterView.NO_CONNECTION);
            showReloadLayout(true);
            return;
        }
    }

    private boolean moreDataAvailable()
    {
        return pageId * pageSize < totalCount;
    }

    private void registerListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls9();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void showFooterView(FooterView footerview)
    {
        mListView.setFooterDividersEnabled(false);
        mFooterViewLoading.setVisibility(0);
        if (footerview == FooterView.MORE)
        {
            mFooterViewLoading.setClickable(true);
            mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
            ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
            mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
        } else
        {
            if (footerview == FooterView.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerview == FooterView.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == FooterView.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerview == FooterView.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }

    private void showReloadLayout(boolean flag)
    {
        if (flag && (dataList == null || dataList.size() == 0))
        {
            noNetLayout.setVisibility(0);
            showFooterView(FooterView.HIDE_ALL);
            return;
        } else
        {
            noNetLayout.setVisibility(8);
            return;
        }
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(this);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        if (getArguments() != null)
        {
            type = getArguments().getString("type");
            id = getArguments().getString("id");
            title = getArguments().getString("title");
            if (!Utilities.isBlank(title))
            {
                setTitleText(title);
            }
        }
        initListener();
        initData();
        registerListener();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (mCon != null && isAdded() && i == 1 && j == 1)
        {
            i = intent.getIntExtra("position", -1);
            long l = intent.getLongExtra("trackId", 0L);
            if (dataList != null && i < dataList.size() && i >= 0 && ((HotSoundModel)dataList.get(i)).id == l)
            {
                ((HotSoundModel)dataList.get(i)).isRelay = true;
                if (mSoundsHotAdapter != null)
                {
                    mSoundsHotAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030014, null);
        initUi(layoutinflater);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        unRegisterListener();
    }

    public void onPlayCanceled()
    {
    }

    public void onSoundChanged(int i)
    {
        if (mSoundsHotAdapter != null)
        {
            mSoundsHotAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (i < 0 || soundinfo == null || i >= dataList.size() || ((HotSoundModel)dataList.get(i)).id != soundinfo.trackId || mSoundsHotAdapter == null)
        {
            return;
        } else
        {
            HotSoundModel hotsoundmodel = (HotSoundModel)dataList.get(i);
            hotsoundmodel.is_favorited = soundinfo.is_favorited;
            hotsoundmodel.favorites_counts = soundinfo.favorites_counts;
            mSoundsHotAdapter.notifyDataSetChanged();
            return;
        }
    }



/*
    static boolean access$002(FocusSoundListFragment focussoundlistfragment, boolean flag)
    {
        focussoundlistfragment.loadingNextPage = flag;
        return flag;
    }

*/




/*
    static int access$102(FocusSoundListFragment focussoundlistfragment, int i)
    {
        focussoundlistfragment.pageId = i;
        return i;
    }

*/


/*
    static int access$108(FocusSoundListFragment focussoundlistfragment)
    {
        int i = focussoundlistfragment.pageId;
        focussoundlistfragment.pageId = i + 1;
        return i;
    }

*/













/*
    static int access$702(FocusSoundListFragment focussoundlistfragment, int i)
    {
        focussoundlistfragment.totalCount = i;
        return i;
    }

*/



    private class _cls1 extends Handler
    {

        final FocusSoundListFragment this$0;

        public void handleMessage(Message message)
        {
            switch (message.what)
            {
            default:
                return;

            case 2147418114: 
                message = new Intent(mCon, com/ximalaya/ting/android/activity/login/LoginActivity);
                break;
            }
            startActivity(message);
        }

        _cls1()
        {
            this$0 = FocusSoundListFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final FocusSoundListFragment this$0;

        public void onRefresh()
        {
            if (!loadingNextPage)
            {
                pageId = 1;
                loadDataListData(mListView);
            }
        }

        _cls2()
        {
            this$0 = FocusSoundListFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final FocusSoundListFragment this$0;

        public void onClick(View view)
        {
            mListView.toRefreshing();
        }

        _cls3()
        {
            this$0 = FocusSoundListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final FocusSoundListFragment this$0;

        public void onClick(View view)
        {
            if (!loadingNextPage)
            {
                showFooterView(FooterView.LOADING);
                loadDataListData(mFooterViewLoading);
            }
        }

        _cls4()
        {
            this$0 = FocusSoundListFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AbsListView.OnScrollListener
    {

        final FocusSoundListFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            mListView.onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0)
            {
                i = abslistview.getCount();
                if (i > 5)
                {
                    i -= 5;
                } else
                {
                    i--;
                }
                if (abslistview.getLastVisiblePosition() > i && (pageId - 1) * pageSize < totalCount && (mDataLoadTask == null || mDataLoadTask.getStatus() != android.os.AsyncTask.Status.RUNNING) && !loadingNextPage)
                {
                    showFooterView(FooterView.LOADING);
                    loadDataListData(mListView);
                }
            }
        }

        _cls5()
        {
            this$0 = FocusSoundListFragment.this;
            super();
        }
    }



    private class _cls7
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final FocusSoundListFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= mSoundsHotAdapter.getCount())
            {
                return false;
            } else
            {
                mSoundsHotAdapter.handleItemLongClick((Likeable)mSoundsHotAdapter.getData().get(i), view);
                return false;
            }
        }

        _cls7()
        {
            this$0 = FocusSoundListFragment.this;
            super();
        }
    }


    private class _cls8 extends MyAsyncTask
    {

        final FocusSoundListFragment this$0;
        final View val$view;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = (new StringBuilder()).append(a.u).append("m/focus_list").toString();
            Object obj = new RequestParams();
            ((RequestParams) (obj)).put("page", (new StringBuilder()).append(pageId).append("").toString());
            ((RequestParams) (obj)).put("per_page", (new StringBuilder()).append(pageSize).append("").toString());
            ((RequestParams) (obj)).put("type", type);
            ((RequestParams) (obj)).put("id", id);
            obj = f.a().a(avoid, ((RequestParams) (obj)), view, mListView);
            Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
            avoid = null;
            try
            {
                obj = JSON.parseObject(((String) (obj)));
                String s = ((JSONObject) (obj)).get("ret").toString();
                if (totalCount == 0)
                {
                    totalCount = ((JSONObject) (obj)).getIntValue("count");
                }
                if ("0".equals(s))
                {
                    avoid = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/sound/HotSoundModel);
                }
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
                return null;
            }
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (mCon != null && isAdded())
            {
                loadingNextPage = false;
                mListView.onRefreshComplete();
                if (list == null)
                {
                    showReloadLayout(true);
                    if (dataList.size() > 0)
                    {
                        showFooterView(FooterView.NO_DATA);
                        return;
                    }
                } else
                {
                    if (pageId == 1)
                    {
                        dataList.clear();
                        dataList.addAll(list);
                        mSoundsHotAdapter.notifyDataSetChanged();
                    } else
                    {
                        dataList.addAll(list);
                        mSoundsHotAdapter.notifyDataSetChanged();
                    }
                    if (moreDataAvailable())
                    {
                        showFooterView(FooterView.MORE);
                    } else
                    {
                        showFooterView(FooterView.HIDE_ALL);
                    }
                    int i = ((toString) (this)).toString;
                    showReloadLayout(false);
                    return;
                }
            }
        }

        protected void onPreExecute()
        {
            loadingNextPage = true;
            showReloadLayout(false);
        }

        _cls8()
        {
            this$0 = FocusSoundListFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls9 extends OnPlayerStatusUpdateListenerProxy
    {

        final FocusSoundListFragment this$0;

        public void onPlayStateChange()
        {
            mSoundsHotAdapter.notifyDataSetChanged();
        }

        _cls9()
        {
            this$0 = FocusSoundListFragment.this;
            super();
        }
    }

}
