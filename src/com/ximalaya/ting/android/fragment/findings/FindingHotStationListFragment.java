// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.HotStationListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.filter.ViewGridMenu;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;

public class FindingHotStationListFragment extends BaseListFragment
    implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener, com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
{

    private HotStationListAdapter mAdapter;
    private String mCategory;
    private ImageView mEmptyView;
    private ViewGridMenu mFilterGridView;
    private PopupWindow mFilterWindow;
    private RadioGroup mHeaderRadioGroupFloat;
    private boolean mIsLoading;
    private int mMaxPage;
    private int mPageId;
    private int mPageSize;
    private String mSortBy;
    private RadioGroup mSortHeader;
    private ArrayList mStations;
    private String mTitle;

    public FindingHotStationListFragment()
    {
        mPageId = 1;
        mPageSize = 20;
        mTitle = null;
        mStations = new ArrayList();
        mIsLoading = false;
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle == null)
        {
            return;
        } else
        {
            mSortBy = bundle.getString("condition");
            mCategory = bundle.getString("category");
            mTitle = bundle.getString("title");
            return;
        }
    }

    private void initHeaderGroup(RadioGroup radiogroup)
    {
        RadioButton radiobutton = (RadioButton)radiogroup.findViewById(0x7f0a0080);
        RadioButton radiobutton1 = (RadioButton)radiogroup.findViewById(0x7f0a0081);
        radiogroup = (RadioButton)radiogroup.findViewById(0x7f0a014b);
        radiobutton.setText("\u6700\u70ED");
        radiobutton1.setVisibility(8);
        radiogroup.setText("\u6700\u65B0");
    }

    private void initHeaderListener(final RadioGroup headerRadioGroup)
    {
        headerRadioGroup.setOnCheckedChangeListener(new _cls6());
    }

    private void initView()
    {
        top_bar = findViewById(0x7f0a005f);
        mEmptyView = (ImageView)findViewById(0x7f0a0060);
        mSortHeader = (RadioGroup)LayoutInflater.from(mActivity).inflate(0x7f030109, mListView, false);
        initHeaderGroup(mSortHeader);
        if (!isNewBozhuPage())
        {
            ((PullToRefreshListView)mListView).addHeaderView(mSortHeader);
        }
        Object obj = (RelativeLayout)findViewById(0x7f0a005e);
        mHeaderRadioGroupFloat = (RadioGroup)LayoutInflater.from(getActivity()).inflate(0x7f030109, null);
        mHeaderRadioGroupFloat.setLayoutParams(mSortHeader.getLayoutParams());
        mHeaderRadioGroupFloat.setPadding(ToolUtil.dp2px(getActivity(), 10F), ToolUtil.dp2px(getActivity(), 10F), ToolUtil.dp2px(getActivity(), 10F), ToolUtil.dp2px(getActivity(), 10F));
        initHeaderGroup(mHeaderRadioGroupFloat);
        if (!isNewBozhuPage())
        {
            ((PullToRefreshListView)mListView).setFloatHeadView(mHeaderRadioGroupFloat);
            ((RelativeLayout) (obj)).addView(mHeaderRadioGroupFloat);
        }
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        mAdapter = new HotStationListAdapter(this, mListView, mStations);
        ((PullToRefreshListView)mListView).setAdapter(mAdapter);
        if (TextUtils.isEmpty(mTitle))
        {
            obj = "\u4E2A\u4EBA\u7535\u53F0";
        } else
        {
            obj = mTitle;
        }
        setTitleText(((String) (obj)));
        if (!isNewBozhuPage())
        {
            topTextView.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f0201eb, 0);
        }
        mFilterGridView = new ViewGridMenu(mActivity, 0);
        initHeaderListener(mSortHeader);
        initHeaderListener(mHeaderRadioGroupFloat);
        topTextView.setOnClickListener(new _cls1());
        mFilterGridView.setOnSelectListener(new _cls2());
        ((PullToRefreshListView)mListView).setOnItemClickListener(new _cls3());
        ((PullToRefreshListView)mListView).setOnScrollListener(new _cls4());
        mFooterViewLoading.setOnClickListener(new _cls5());
    }

    private boolean isNewBozhuPage()
    {
        return mTitle != null && mTitle.equals("\u65B0\u664B\u4E3B\u64AD");
    }

    private void loadData(View view)
    {
        if (mIsLoading)
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        if (TextUtils.isEmpty(mSortBy))
        {
            mSortBy = "hot";
        }
        if (isNewBozhuPage())
        {
            mSortBy = "new";
        }
        requestparams.put("condition", mSortBy);
        if (TextUtils.isEmpty(mCategory))
        {
            mCategory = "all";
        }
        requestparams.put("category_name", mCategory);
        requestparams.put("page", (new StringBuilder()).append("").append(mPageId).toString());
        requestparams.put("per_page", (new StringBuilder()).append("").append(mPageSize).toString());
        f.a().a("m/explore_user_list", requestparams, DataCollectUtil.getDataFromView(view), new _cls7(), true);
    }

    private void loadFollowStatus()
    {
        Object obj = UserInfoMannage.getInstance().getUser();
        if (obj == null)
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("uid", (new StringBuilder()).append("").append(((LoginInfoModel) (obj)).uid).toString());
        obj = new StringBuilder();
        Iterator iterator = mStations.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            StationModel stationmodel = (StationModel)iterator.next();
            if (stationmodel != null)
            {
                ((StringBuilder) (obj)).append(stationmodel.uid).append(",");
            }
        } while (true);
        requestparams.put("uids", ((StringBuilder) (obj)).toString());
        f.a().a("m/follow_status", requestparams, null, new _cls8());
    }

    private boolean moreDataAvailable()
    {
        return mPageId <= mMaxPage;
    }

    private void registePlayerCallback()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayerStatusUpdateListener(this);
            localmediaservice.setOnPlayServiceUpdateListener(this);
        }
    }

    private void unregistePlayerCallback()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayerUpdateListener(this);
            localmediaservice.removeOnPlayServiceUpdateListener(this);
        }
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a0278);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initData();
        initView();
        registePlayerCallback();
    }

    public void onBufferUpdated(int i)
    {
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300fd, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        unregistePlayerCallback();
    }

    public void onLogoPlayFinished()
    {
    }

    public void onPause()
    {
        super.onPause();
    }

    public void onPlayCanceled()
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onPlayCompleted()
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onPlayPaused()
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onPlayProgressUpdate(int i, int j)
    {
    }

    public void onPlayStarted()
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onPlayerBuffering(boolean flag)
    {
    }

    public void onResume()
    {
        super.onResume();
        mPageId = 1;
        loadData(fragmentBaseContainerView);
    }

    public void onSoundChanged(int i)
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
    }

    public void onSoundPrepared(int i)
    {
    }

    public void onStartPlayLogo()
    {
    }





/*
    static PopupWindow access$102(FindingHotStationListFragment findinghotstationlistfragment, PopupWindow popupwindow)
    {
        findinghotstationlistfragment.mFilterWindow = popupwindow;
        return popupwindow;
    }

*/




/*
    static String access$1302(FindingHotStationListFragment findinghotstationlistfragment, String s)
    {
        findinghotstationlistfragment.mSortBy = s;
        return s;
    }

*/


/*
    static int access$1402(FindingHotStationListFragment findinghotstationlistfragment, int i)
    {
        findinghotstationlistfragment.mMaxPage = i;
        return i;
    }

*/




/*
    static String access$302(FindingHotStationListFragment findinghotstationlistfragment, String s)
    {
        findinghotstationlistfragment.mCategory = s;
        return s;
    }

*/



/*
    static String access$402(FindingHotStationListFragment findinghotstationlistfragment, String s)
    {
        findinghotstationlistfragment.mTitle = s;
        return s;
    }

*/



/*
    static boolean access$502(FindingHotStationListFragment findinghotstationlistfragment, boolean flag)
    {
        findinghotstationlistfragment.mIsLoading = flag;
        return flag;
    }

*/



/*
    static int access$602(FindingHotStationListFragment findinghotstationlistfragment, int i)
    {
        findinghotstationlistfragment.mPageId = i;
        return i;
    }

*/


/*
    static int access$608(FindingHotStationListFragment findinghotstationlistfragment)
    {
        int i = findinghotstationlistfragment.mPageId;
        findinghotstationlistfragment.mPageId = i + 1;
        return i;
    }

*/




    private class _cls6
        implements android.widget.RadioGroup.OnCheckedChangeListener
    {

        final FindingHotStationListFragment this$0;
        final RadioGroup val$headerRadioGroup;

        public void onCheckedChanged(RadioGroup radiogroup, int i)
        {
            if (radiogroup == mHeaderRadioGroupFloat)
            {
                if (mSortHeader.getCheckedRadioButtonId() != i)
                {
                    ((RadioButton)mSortHeader.findViewById(i)).setChecked(true);
                }
                return;
            }
            if (mHeaderRadioGroupFloat.getCheckedRadioButtonId() != i)
            {
                ((RadioButton)mHeaderRadioGroupFloat.findViewById(i)).setChecked(true);
            }
            if (i != 0x7f0a0080) goto _L2; else goto _L1
_L1:
            mSortBy = "hot";
_L4:
            mPageId = 1;
            mStations.clear();
            mAdapter.notifyDataSetChanged();
            loadData(headerRadioGroup);
            return;
_L2:
            if (i == 0x7f0a014b)
            {
                ToolUtil.onEvent(mCon, "CLICK_PERSON_STATION_LIST_TAB_NEW");
                mSortBy = "new";
            }
            if (true) goto _L4; else goto _L3
_L3:
        }

        _cls6()
        {
            this$0 = FindingHotStationListFragment.this;
            headerRadioGroup = radiogroup;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FindingHotStationListFragment this$0;

        public void onClick(View view)
        {
            if (isNewBozhuPage())
            {
                return;
            }
            if (mFilterWindow == null)
            {
                mFilterWindow = new PopupWindow(mFilterGridView, -1, -2);
                mFilterWindow.setAnimationStyle(0x7f0b000f);
                mFilterWindow.setFocusable(true);
                mFilterWindow.setOutsideTouchable(false);
                mFilterWindow.setBackgroundDrawable(getResources().getDrawable(0x7f070003));
            }
            mFilterWindow.showAsDropDown(top_bar);
            class _cls1
                implements android.widget.PopupWindow.OnDismissListener
            {

                final _cls1 this$1;

                public void onDismiss()
                {
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            mFilterWindow.setOnDismissListener(new _cls1());
        }

        _cls1()
        {
            this$0 = FindingHotStationListFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.filter.ViewGridMenu.OnSelectListener
    {

        final FindingHotStationListFragment this$0;

        public void getValue(String s, String s1)
        {
            mCategory = s;
            if (s1 != null && !s1.equals(""))
            {
                if (s1.equals("\u5168\u90E8"))
                {
                    mTitle = "\u4E2A\u4EBA\u7535\u53F0";
                } else
                {
                    mTitle = s1;
                }
                setTitleText(mTitle);
            }
            mFilterWindow.dismiss();
            if (!mIsLoading)
            {
                mPageId = 1;
                mStations.clear();
                mAdapter.notifyDataSetChanged();
                loadData(mFilterGridView);
            }
        }

        _cls2()
        {
            this$0 = FindingHotStationListFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final FindingHotStationListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            adapterview = (StationModel)mStations.get(i - ((PullToRefreshListView)mListView).getHeaderViewsCount());
            if (adapterview == null)
            {
                return;
            } else
            {
                Bundle bundle = new Bundle();
                bundle.putLong("toUid", ((StationModel) (adapterview)).uid);
                bundle.putInt("from", 1);
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
                return;
            }
        }

        _cls3()
        {
            this$0 = FindingHotStationListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AbsListView.OnScrollListener
    {

        final FindingHotStationListFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || !moreDataAvailable())
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
                        loadData(mListView);
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls4()
        {
            this$0 = FindingHotStationListFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final FindingHotStationListFragment this$0;

        public void onClick(View view)
        {
            loadData(mFooterViewLoading);
        }

        _cls5()
        {
            this$0 = FindingHotStationListFragment.this;
            super();
        }
    }


    private class _cls7 extends a
    {

        final FindingHotStationListFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
        }

        public void onStart()
        {
            super.onStart();
            mIsLoading = true;
            if (mPageId != 1);
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        }

        public void onSuccess(final String list)
        {
            if (TextUtils.isEmpty(list))
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            Object obj = null;
            try
            {
                list = JSON.parseObject(list);
            }
            // Misplaced declaration of an exception variable
            catch (final String list)
            {
                list.printStackTrace();
                list = obj;
            }
            if (list == null || list.getIntValue("ret") != 0)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            mMaxPage = list.getIntValue("maxPageId");
            list = list.getString("list");
            if (TextUtils.isEmpty(list))
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            list = JSON.parseArray(list, com/ximalaya/ting/android/model/broadcast/StationModel);
            if (list == null || list.size() == 0)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            } else
            {
                class _cls1
                    implements Runnable
                {

                    final _cls7 this$1;
                    final List val$list;

                    public void run()
                    {
                        if (mPageId == 1)
                        {
                            mStations.clear();
                        }
                        mStations.addAll(list);
                        mAdapter.notifyDataSetChanged();
                        if (UserInfoMannage.hasLogined())
                        {
                            loadFollowStatus();
                        }
                        int i = 
// JavaClassFileOutputException: get_constant: invalid tag

                _cls1()
                {
                    this$1 = _cls7.this;
                    list = list1;
                    super();
                }
                }

                mListView.postDelayed(new _cls1(), getAnimationLeftTime());
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
        }

        _cls7()
        {
            this$0 = FindingHotStationListFragment.this;
            super();
        }
    }


    private class _cls8 extends a
    {

        final FindingHotStationListFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if ((s = JSON.parseObject(s)) != null && s.getIntValue("ret") == 0 && (s = s.getJSONObject("status")) != null)
            {
                int i = 0;
                while (i < mStations.size()) 
                {
                    StationModel stationmodel = (StationModel)mStations.get(i);
                    if (stationmodel != null)
                    {
                        boolean flag;
                        if (s.getIntValue((new StringBuilder()).append("").append(stationmodel.uid).toString()) == 1)
                        {
                            flag = true;
                        } else
                        {
                            flag = false;
                        }
                        stationmodel.isFollowed = flag;
                    }
                    mAdapter.updateItem(i);
                    i++;
                }
            }
            if (true) goto _L1; else goto _L3
_L3:
        }

        _cls8()
        {
            this$0 = FindingHotStationListFragment.this;
            super();
        }
    }

}
