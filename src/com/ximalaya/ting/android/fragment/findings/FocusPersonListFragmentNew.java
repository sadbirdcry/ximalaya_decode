// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.broadcast.Broadcaster;
import com.ximalaya.ting.android.model.holder.PersionStationBigHolder;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FocusPersonListFragmentNew extends BaseListFragment
    implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener, com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
{
    class FocusItemAdapter extends BaseAdapter
    {

        final FocusPersonListFragmentNew this$0;

        public int getCount()
        {
            return mStations.size();
        }

        public Object getItem(int i)
        {
            return mStations.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, final View holder, ViewGroup viewgroup)
        {
            Broadcaster broadcaster = (Broadcaster)mStations.get(i);
            viewgroup = holder;
            if (holder == null)
            {
                viewgroup = PersionStationBigHolder.getView(
// JavaClassFileOutputException: get_constant: invalid tag

        FocusItemAdapter()
        {
            this$0 = FocusPersonListFragmentNew.this;
            super();
        }

        class _cls1
            implements android.view.View.OnClickListener
        {

            final FocusItemAdapter this$1;

            public void onClick(View view)
            {
                Broadcaster broadcaster1 = (Broadcaster)view.getTag(0x7f090000);
                if (broadcaster1 == null)
                {
                    return;
                }
                if (broadcaster1.tracks == null)
                {
                    loadAlbumSound(broadcaster1, true, false, view);
                    return;
                }
                SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
                LocalMediaService localmediaservice1 = LocalMediaService.getInstance();
                if (soundinfo != null && broadcaster1.tracks.contains(toSoundInfoNew(soundinfo)) && !localmediaservice1.isPaused())
                {
                    localmediaservice1.pause();
                    return;
                }
                if (broadcaster1.tracks.contains(toSoundInfoNew(soundinfo)) && localmediaservice1.isPaused())
                {
                    localmediaservice1.start();
                    return;
                } else
                {
                    PlayTools.gotoPlay(0, null, 0, null, ModelHelper.toSoundInfo(broadcaster1.tracks), 0, 
// JavaClassFileOutputException: get_constant: invalid tag

                _cls1()
                {
                    this$1 = FocusItemAdapter.this;
                    super();
                }
        }


        class _cls2
            implements android.view.View.OnClickListener
        {

            final FocusItemAdapter this$1;
            final PersionStationBigHolder val$holder;

            public void onClick(View view)
            {
                Object obj1 = (Broadcaster)view.getTag(0x7f090000);
                if (obj1 == null)
                {
                    return;
                }
                if (!UserInfoMannage.hasLogined())
                {
                    setFollowStatus(holder, ((Broadcaster) (obj1)).is_follow);
                    obj1 = new Intent(
// JavaClassFileOutputException: get_constant: invalid tag

                _cls2()
                {
                    this$1 = FocusItemAdapter.this;
                    holder = persionstationbigholder;
                    super();
                }
        }

    }


    public static final String FROM = "from";
    public static final int FROM_FOCUS = 1;
    private FocusItemAdapter mAdapter;
    private long mFocusId;
    private int mFrom;
    private boolean mIsLoading;
    private ProgressBar mLoading;
    private int mMaxPage;
    private int mPageId;
    private int mPageSize;
    private ArrayList mStations;
    private String mTitle;
    private int mType;

    public FocusPersonListFragmentNew()
    {
        mStations = new ArrayList();
        mPageId = 1;
        mPageSize = 20;
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle == null)
        {
            return;
        } else
        {
            mType = bundle.getInt("type");
            mFocusId = bundle.getLong("id");
            mTitle = bundle.getString("title");
            mFrom = bundle.getInt("from");
            return;
        }
    }

    private void initView()
    {
        mLoading = (ProgressBar)findViewById(0x7f0a0061);
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls2());
        mListView.setOnItemClickListener(new _cls3());
        mListView.setOnScrollListener(new _cls4());
        mAdapter = new FocusItemAdapter();
        mListView.setAdapter(mAdapter);
        setTitleText(mTitle);
    }

    private void loadAlbumSound(final Broadcaster model, final boolean play, final boolean showPlay, final View view)
    {
        if (model == null)
        {
            return;
        } else
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("uid", (new StringBuilder()).append("").append(model.uid).toString());
            f.a().a("m/prelisten", requestparams, DataCollectUtil.getDataFromView(view), new _cls5());
            return;
        }
    }

    private void loadData()
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("page", (new StringBuilder()).append("").append(mPageId).toString());
        requestparams.put("per_page", (new StringBuilder()).append("").append(mPageSize).toString());
        requestparams.put("type", mType);
        requestparams.put("id", mFocusId);
        f.a().a("m/focus_list", requestparams, DataCollectUtil.getDataFromView(fragmentBaseContainerView), new _cls6(), true);
    }

    private void loadFollowStatus()
    {
        Object obj = UserInfoMannage.getInstance().getUser();
        if (obj == null)
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("uid", (new StringBuilder()).append("").append(((LoginInfoModel) (obj)).uid).toString());
        obj = new StringBuilder();
        Iterator iterator = mStations.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Broadcaster broadcaster = (Broadcaster)iterator.next();
            if (broadcaster != null)
            {
                ((StringBuilder) (obj)).append(broadcaster.uid).append(",");
            }
        } while (true);
        requestparams.put("uids", ((StringBuilder) (obj)).toString());
        f.a().a("m/follow_status", requestparams, DataCollectUtil.getDataFromView(mListView), new _cls7());
    }

    private void registePlayerCallback()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayerStatusUpdateListener(this);
            localmediaservice.setOnPlayServiceUpdateListener(this);
        }
    }

    private void setFollowStatus(PersionStationBigHolder persionstationbigholder, boolean flag)
    {
        if (flag)
        {
            persionstationbigholder.follow.setBackgroundResource(0x7f020266);
            persionstationbigholder.followImg.setVisibility(8);
            persionstationbigholder.followTxt.setText("\u5DF2\u5173\u6CE8");
            persionstationbigholder.followTxt.setTextColor(Color.parseColor("#ffffff"));
            return;
        } else
        {
            persionstationbigholder.follow.setBackgroundResource(0x7f020263);
            persionstationbigholder.followImg.setVisibility(0);
            persionstationbigholder.followTxt.setText("\u5173\u6CE8");
            persionstationbigholder.followTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    private SoundInfoNew toSoundInfoNew(SoundInfo soundinfo)
    {
        if (soundinfo == null)
        {
            return null;
        } else
        {
            SoundInfoNew soundinfonew = new SoundInfoNew();
            soundinfonew.id = soundinfo.trackId;
            return soundinfonew;
        }
    }

    private void unregistePlayerCallback()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayerUpdateListener(this);
            localmediaservice.removeOnPlayServiceUpdateListener(this);
        }
    }

    public void doFollow(final Context context, final Broadcaster model, final PersionStationBigHolder holder, final View view)
    {
        if (UserInfoMannage.getInstance().getUser() == null)
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("toUid", (new StringBuilder()).append("").append(model.uid).toString());
        StringBuilder stringbuilder = (new StringBuilder()).append("");
        boolean flag;
        if (!model.is_follow)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        requestparams.put("isFollow", stringbuilder.append(flag).toString());
        f.a().b("mobile/follow", requestparams, DataCollectUtil.getDataFromView(view), new _cls1());
    }

    public void onActivityCreated(Bundle bundle)
    {
        mListView = (ListView)findViewById(0x7f0a02c7);
        onActivityCreated(bundle);
        initData();
        initView();
        registePlayerCallback();
    }

    public void onBufferUpdated(int i)
    {
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03009b, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        unregistePlayerCallback();
        onDestroyView();
    }

    public void onLogoPlayFinished()
    {
    }

    public void onPlayCanceled()
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onPlayCompleted()
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onPlayPaused()
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onPlayProgressUpdate(int i, int j)
    {
    }

    public void onPlayStarted()
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onPlayerBuffering(boolean flag)
    {
    }

    public void onResume()
    {
        onResume();
        mListView.postDelayed(new _cls8(), getAnimationLeftTime());
    }

    public void onSoundChanged(int i)
    {
        mAdapter.notifyDataSetChanged();
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
    }

    public void onSoundPrepared(int i)
    {
    }

    public void onStartPlayLogo()
    {
    }






/*
    static boolean access$1102(FocusPersonListFragmentNew focuspersonlistfragmentnew, boolean flag)
    {
        focuspersonlistfragmentnew.mIsLoading = flag;
        return flag;
    }

*/



/*
    static int access$1202(FocusPersonListFragmentNew focuspersonlistfragmentnew, int i)
    {
        focuspersonlistfragmentnew.mPageId = i;
        return i;
    }

*/


/*
    static int access$1208(FocusPersonListFragmentNew focuspersonlistfragmentnew)
    {
        int i = focuspersonlistfragmentnew.mPageId;
        focuspersonlistfragmentnew.mPageId = i + 1;
        return i;
    }

*/





/*
    static int access$1502(FocusPersonListFragmentNew focuspersonlistfragmentnew, int i)
    {
        focuspersonlistfragmentnew.mMaxPage = i;
        return i;
    }

*/


















    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final FocusPersonListFragmentNew this$0;

        public void onRefresh()
        {
            if (!mIsLoading)
            {
                mPageId = 1;
                loadData();
            }
        }

        _cls2()
        {
            this$0 = FocusPersonListFragmentNew.this;
            Object();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final FocusPersonListFragmentNew this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            Bundle bundle;
            adapterview = (Broadcaster)mStations.get(i - mListView.getHeaderViewsCount());
            if (adapterview == null)
            {
                return;
            }
            bundle = new Bundle();
            bundle.putLong("toUid", ((Broadcaster) (adapterview)).uid);
            mFrom;
            JVM INSTR tableswitch 1 1: default 76
        //                       1 99;
               goto _L1 _L2
_L1:
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
            return;
_L2:
            bundle.putInt("from", 3);
            if (true) goto _L1; else goto _L3
_L3:
        }

        _cls3()
        {
            this$0 = FocusPersonListFragmentNew.this;
            Object();
        }
    }


    private class _cls4
        implements android.widget.AbsListView.OnScrollListener
    {

        final FocusPersonListFragmentNew this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || mPageId > mMaxPage)
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
                        loadData();
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls4()
        {
            this$0 = FocusPersonListFragmentNew.this;
            Object();
        }
    }


    private class _cls5 extends a
    {

        ProgressDialog pd;
        final FocusPersonListFragmentNew this$0;
        final Broadcaster val$model;
        final boolean val$play;
        final boolean val$showPlay;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            super.onFinish();
            pd.cancel();
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(access$1600, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
        }

        public void onStart()
        {
            super.onStart();
            pd = new MyProgressDialog(access$1600);
            pd.setMessage("\u6B63\u5728\u52A0\u8F7D\u58F0\u97F3\u5217\u8868...");
            pd.show();
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                Toast.makeText(access$1600, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            } else
            {
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = null;
                }
                if (s == null || s.getIntValue("ret") != 0)
                {
                    Toast.makeText(access$1600, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                    return;
                }
                s = s.getString("list");
                if (TextUtils.isEmpty(s))
                {
                    Toast.makeText(access$1600, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                    return;
                }
                s = JSON.parseArray(s, com/ximalaya/ting/android/model/sound/SoundInfoNew);
                if (s == null || s.size() == 0)
                {
                    Toast.makeText(access$1600, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                    return;
                }
                model.tracks = s;
                if (play && MyApplication.a() != null)
                {
                    PlayTools.gotoPlay(0, null, 0, null, ModelHelper.toSoundInfo(s), 0, MyApplication.a(), showPlay, DataCollectUtil.getDataFromView(view));
                    return;
                }
            }
        }

        _cls5()
        {
            this$0 = FocusPersonListFragmentNew.this;
            model = broadcaster;
            play = flag;
            showPlay = flag1;
            view = view1;
            a();
        }
    }


    private class _cls6 extends a
    {

        final FocusPersonListFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
            mLoading.setVisibility(8);
            mIsLoading = false;
            ((PullToRefreshListView)mListView).onRefreshComplete();
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onStart()
        {
            super.onStart();
            if (mPageId == 1)
            {
                mLoading.setVisibility(0);
            } else
            {
                mLoading.setVisibility(8);
            }
            mIsLoading = true;
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        }

        public void onSuccess(String s)
        {
            if (!isAdded())
            {
                return;
            }
            if (TextUtils.isEmpty(s))
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            Object obj = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = obj;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            mMaxPage = s.getIntValue("maxPageId");
            s = s.getString("list");
            if (TextUtils.isEmpty(s))
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/broadcast/Broadcaster);
            if (s == null || s.size() == 0)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            if (mPageId == 1)
            {
                mStations.clear();
            }
            mStations.addAll(s);
            mAdapter.notifyDataSetChanged();
            int i = 
// JavaClassFileOutputException: get_constant: invalid tag

        _cls6()
        {
            this$0 = FocusPersonListFragmentNew.this;
            a();
        }
    }


    private class _cls7 extends a
    {

        final FocusPersonListFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mListView);
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s))
            {
                Broadcaster broadcaster = null;
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = broadcaster;
                }
                if (s != null && s.getIntValue("ret") == 0)
                {
                    s = s.getJSONObject("status");
                    if (s != null)
                    {
                        int i = 0;
                        while (i < mStations.size()) 
                        {
                            broadcaster = (Broadcaster)mStations.get(i);
                            if (broadcaster != null)
                            {
                                boolean flag;
                                if (s.getIntValue((new StringBuilder()).append("").append(broadcaster.uid).toString()) == 1)
                                {
                                    flag = true;
                                } else
                                {
                                    flag = false;
                                }
                                broadcaster.is_follow = flag;
                            }
                            i++;
                        }
                        mAdapter.notifyDataSetChanged();
                        return;
                    }
                }
            }
        }

        _cls7()
        {
            this$0 = FocusPersonListFragmentNew.this;
            a();
        }
    }


    private class _cls1 extends a
    {

        final FocusPersonListFragmentNew this$0;
        final Context val$context;
        final PersionStationBigHolder val$holder;
        final Broadcaster val$model;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            onFinish();
            setFollowStatus(holder, model.is_follow);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
                return;
            }
            Object obj = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = obj;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
                return;
            }
            s = model;
            boolean flag;
            if (!model.is_follow)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            s.is_follow = flag;
            if (model.is_follow)
            {
                s = "\u5173\u6CE8\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u5173\u6CE8\u6210\u529F";
            }
            Toast.makeText(context, s, 0).show();
        }

        _cls1()
        {
            this$0 = FocusPersonListFragmentNew.this;
            holder = persionstationbigholder;
            model = broadcaster;
            context = context1;
            view = view1;
            a();
        }
    }


    private class _cls8
        implements Runnable
    {

        final FocusPersonListFragmentNew this$0;

        public void run()
        {
            ((PullToRefreshListView)mListView).toRefreshing();
        }

        _cls8()
        {
            this$0 = FocusPersonListFragmentNew.this;
            Object();
        }
    }

}
