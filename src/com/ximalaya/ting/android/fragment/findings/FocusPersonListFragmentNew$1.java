// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.broadcast.Broadcaster;
import com.ximalaya.ting.android.model.holder.PersionStationBigHolder;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.findings:
//            FocusPersonListFragmentNew

class val.view extends a
{

    final FocusPersonListFragmentNew this$0;
    final Context val$context;
    final PersionStationBigHolder val$holder;
    final Broadcaster val$model;
    final View val$view;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$view);
    }

    public void onFinish()
    {
        super.onFinish();
        FocusPersonListFragmentNew.access$600(FocusPersonListFragmentNew.this, val$holder, val$model.is_follow);
    }

    public void onNetError(int i, String s)
    {
        Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
            return;
        }
        Object obj = null;
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = obj;
        }
        if (s == null || s.getIntValue("ret") != 0)
        {
            Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
            return;
        }
        s = val$model;
        boolean flag;
        if (!val$model.is_follow)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        s.is_follow = flag;
        if (val$model.is_follow)
        {
            s = "\u5173\u6CE8\u6210\u529F\uFF01";
        } else
        {
            s = "\u53D6\u6D88\u5173\u6CE8\u6210\u529F";
        }
        Toast.makeText(val$context, s, 0).show();
    }

    ()
    {
        this$0 = final_focuspersonlistfragmentnew;
        val$holder = persionstationbigholder;
        val$model = broadcaster;
        val$context = context1;
        val$view = View.this;
        super();
    }
}
