// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.text.TextUtils;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.findings:
//            FindingHotStationListFragment

class this._cls0 extends a
{

    final FindingHotStationListFragment this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
    }

    public void onFinish()
    {
        super.onFinish();
        FindingHotStationListFragment.access$502(FindingHotStationListFragment.this, false);
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
        showFooterView(com.ximalaya.ting.android.fragment.ONNECTION);
    }

    public void onStart()
    {
        super.onStart();
        FindingHotStationListFragment.access$502(FindingHotStationListFragment.this, true);
        if (FindingHotStationListFragment.access$600(FindingHotStationListFragment.this) != 1);
        showFooterView(com.ximalaya.ting.android.fragment.ING);
    }

    public void onSuccess(final String list)
    {
        if (TextUtils.isEmpty(list))
        {
            showFooterView(com.ximalaya.ting.android.fragment.ATA);
            return;
        }
        Object obj = null;
        try
        {
            list = JSON.parseObject(list);
        }
        // Misplaced declaration of an exception variable
        catch (final String list)
        {
            list.printStackTrace();
            list = obj;
        }
        if (list == null || list.getIntValue("ret") != 0)
        {
            showFooterView(com.ximalaya.ting.android.fragment.ATA);
            return;
        }
        FindingHotStationListFragment.access$1402(FindingHotStationListFragment.this, list.getIntValue("maxPageId"));
        list = list.getString("list");
        if (TextUtils.isEmpty(list))
        {
            showFooterView(com.ximalaya.ting.android.fragment.ATA);
            return;
        }
        list = JSON.parseArray(list, com/ximalaya/ting/android/model/broadcast/StationModel);
        if (list == null || list.size() == 0)
        {
            showFooterView(com.ximalaya.ting.android.fragment.ATA);
            return;
        } else
        {
            class _cls1
                implements Runnable
            {

                final FindingHotStationListFragment._cls7 this$1;
                final List val$list;

                public void run()
                {
                    if (FindingHotStationListFragment.access$600(this$0) == 1)
                    {
                        FindingHotStationListFragment.access$700(this$0).clear();
                    }
                    FindingHotStationListFragment.access$700(this$0).addAll(list);
                    FindingHotStationListFragment.access$800(this$0).notifyDataSetChanged();
                    if (UserInfoMannage.hasLogined())
                    {
                        FindingHotStationListFragment.access$1500(this$0);
                    }
                    int _tmp = FindingHotStationListFragment.access$608(this$0);
                }

            _cls1()
            {
                this$1 = FindingHotStationListFragment._cls7.this;
                list = list1;
                super();
            }
            }

            mListView.postDelayed(new _cls1(), getAnimationLeftTime());
            showFooterView(com.ximalaya.ting.android.fragment._ALL);
            return;
        }
    }

    _cls1()
    {
        this$0 = FindingHotStationListFragment.this;
        super();
    }
}
