// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.album.HotAlbum;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FocusAlbumListFragmentNew extends BaseListFragment
{
    class FocusItemAdapter extends BaseAdapter
    {

        final FocusAlbumListFragmentNew this$0;

        public int getCount()
        {
            return mAlbums.size();
        }

        public Object getItem(int i)
        {
            return mAlbums.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, final View holder, ViewGroup viewgroup)
        {
            boolean flag1 = true;
            HotAlbum hotalbum = (HotAlbum)mAlbums.get(i);
            viewgroup = holder;
            if (holder == null)
            {
                viewgroup = AlbumItemHolder.getView(
// JavaClassFileOutputException: get_constant: invalid tag

        FocusItemAdapter()
        {
            this$0 = FocusAlbumListFragmentNew.this;
            super();
        }

        class _cls1
            implements android.view.View.OnClickListener
        {

            final FocusItemAdapter this$1;
            final AlbumItemHolder val$holder;

            public void onClick(View view)
            {
                HotAlbum hotalbum1 = (HotAlbum)view.getTag(0x7f090000);
                if (hotalbum1 == null)
                {
                    return;
                } else
                {
                    mCurrentTaskId = hotalbum1.id;
                    doCollect(hotalbum1, holder, view);
                    return;
                }
            }

                _cls1()
                {
                    this$1 = FocusItemAdapter.this;
                    holder = albumitemholder;
                    super();
                }
        }

    }


    private FocusItemAdapter mAdapter;
    private ArrayList mAlbums;
    private long mCurrentTaskId;
    private long mFocusId;
    private boolean mIsLoading;
    private int mMaxPage;
    private int mPageId;
    private int mPageSize;
    private String mTitle;
    private int mType;

    public FocusAlbumListFragmentNew()
    {
        mAlbums = new ArrayList();
        mPageId = 1;
        mPageSize = 20;
    }

    private void doCollect(final HotAlbum model, final AlbumItemHolder holder, final View view)
    {
        if (UserInfoMannage.hasLogined())
        {
            String s;
            RequestParams requestparams;
            if (model.is_favorite)
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.album_id).toString());
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(view), new _cls1());
        } else
        {
            final AlbumModel am = ModelHelper.toAlbumModel(model);
            if (AlbumModelManage.getInstance().ensureLocalCollectAllow(getActivity(), am, view))
            {
                (new _cls2()).myexec(new Void[0]);
                return;
            }
        }
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle == null)
        {
            return;
        } else
        {
            mType = bundle.getInt("type");
            mFocusId = bundle.getLong("id");
            mTitle = bundle.getString("title");
            return;
        }
    }

    private void initView()
    {
        findViewById(0x7f0a0061).setVisibility(8);
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls3());
        mListView.setOnItemClickListener(new _cls4());
        mListView.setOnScrollListener(new _cls5());
        mAdapter = new FocusItemAdapter();
        mListView.setAdapter(mAdapter);
        setTitleText(mTitle);
    }

    private void loadData()
    {
        if (mIsLoading)
        {
            return;
        } else
        {
            mIsLoading = true;
            RequestParams requestparams = new RequestParams();
            requestparams.put("page", (new StringBuilder()).append("").append(mPageId).toString());
            requestparams.put("per_page", (new StringBuilder()).append("").append(mPageSize).toString());
            requestparams.put("type", mType);
            requestparams.put("id", mFocusId);
            f.a().a("m/focus_list", requestparams, DataCollectUtil.getDataFromView(fragmentBaseContainerView), new _cls6(), true);
            return;
        }
    }

    private void loadRSSStatus()
    {
        if (!UserInfoMannage.hasLogined())
        {
            loadRSSStatusFromLocal();
            return;
        }
        LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
        RequestParams requestparams = new RequestParams();
        StringBuilder stringbuilder = new StringBuilder();
        for (Iterator iterator = mAlbums.iterator(); iterator.hasNext(); stringbuilder.append(((HotAlbum)iterator.next()).album_id).append(",")) { }
        requestparams.add("uid", (new StringBuilder()).append("").append(logininfomodel.uid).toString());
        requestparams.add("album_ids", stringbuilder.toString());
        f.a().a("m/album_subscribe_status", requestparams, null, new _cls7());
    }

    private void loadRSSStatusFromLocal()
    {
        (new _cls8()).myexec(new Void[0]);
    }

    private void parseData(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
            return;
        }
        Object obj = null;
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = obj;
        }
        if (s == null || s.getIntValue("ret") != 0)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
            return;
        }
        mMaxPage = s.getIntValue("maxPageId");
        s = s.getString("list");
        if (TextUtils.isEmpty(s))
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
            return;
        }
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/album/HotAlbum);
        if (s == null || s.size() == 0)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
            return;
        }
        if (mPageId == 1)
        {
            mAlbums.clear();
        }
        mAlbums.addAll(s);
        mAdapter.notifyDataSetChanged();
        mPageId = mPageId + 1;
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        loadRSSStatus();
    }

    private void setCollectStatus(AlbumItemHolder albumitemholder, boolean flag)
    {
        if (flag)
        {
            albumitemholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ee, 0, 0);
            albumitemholder.collectTxt.setText("\u5DF2\u6536\u85CF");
            albumitemholder.collectTxt.setTextColor(Color.parseColor("#999999"));
            return;
        } else
        {
            albumitemholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ed, 0, 0);
            albumitemholder.collectTxt.setText("\u6536\u85CF");
            albumitemholder.collectTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        mListView = (ListView)findViewById(0x7f0a02c7);
        super.onActivityCreated(bundle);
        initData();
        initView();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03009b, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void onResume()
    {
        super.onResume();
        ((PullToRefreshListView)mListView).toRefreshing();
    }









/*
    static long access$202(FocusAlbumListFragmentNew focusalbumlistfragmentnew, long l)
    {
        focusalbumlistfragmentnew.mCurrentTaskId = l;
        return l;
    }

*/







/*
    static boolean access$702(FocusAlbumListFragmentNew focusalbumlistfragmentnew, boolean flag)
    {
        focusalbumlistfragmentnew.mIsLoading = flag;
        return flag;
    }

*/



/*
    static int access$802(FocusAlbumListFragmentNew focusalbumlistfragmentnew, int i)
    {
        focusalbumlistfragmentnew.mPageId = i;
        return i;
    }

*/


    private class _cls1 extends a
    {

        final FocusAlbumListFragmentNew this$0;
        final AlbumItemHolder val$holder;
        final HotAlbum val$model;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            if (mCurrentTaskId == model.id)
            {
                setCollectStatus(holder, model.is_favorite);
            }
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
                return;
            }
            Object obj = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = obj;
            }
            if (s != null && s.getIntValue("ret") == 0)
            {
                s = model;
                boolean flag;
                if (!model.is_favorite)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                s.is_favorite = flag;
                if (mCurrentTaskId == model.id)
                {
                    setCollectStatus(holder, model.is_favorite);
                }
                if (model.is_favorite)
                {
                    s = "\u6536\u85CF\u6210\u529F\uFF01";
                } else
                {
                    s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
                }
                showToast(s);
                return;
            }
            if (mCurrentTaskId == model.id)
            {
                setCollectStatus(holder, model.is_favorite);
            }
            showToast("\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        _cls1()
        {
            this$0 = FocusAlbumListFragmentNew.this;
            model = hotalbum;
            holder = albumitemholder;
            view = view1;
            super();
        }
    }


    private class _cls2 extends MyAsyncTask
    {

        final FocusAlbumListFragmentNew this$0;
        final AlbumModel val$am;
        final AlbumItemHolder val$holder;
        final HotAlbum val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            HotAlbum hotalbum = model;
            boolean flag;
            if (!model.is_favorite)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            hotalbum.is_favorite = flag;
            if (!model.is_favorite)
            {
                avoid.deleteAlbumInLocalAlbumList(am);
            } else
            {
                avoid.saveAlbumModel(am);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            if (((HotAlbum)holder.collect.getTag(0x7f090000)).id == model.id)
            {
                AlbumItemHolder.setCollectStatus(holder, model.is_favorite);
            }
        }

        _cls2()
        {
            this$0 = FocusAlbumListFragmentNew.this;
            model = hotalbum;
            am = albummodel;
            holder = albumitemholder;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final FocusAlbumListFragmentNew this$0;

        public void onRefresh()
        {
            if (!mIsLoading)
            {
                mPageId = 1;
                loadData();
            }
        }

        _cls3()
        {
            this$0 = FocusAlbumListFragmentNew.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemClickListener
    {

        final FocusAlbumListFragmentNew this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (i - mListView.getHeaderViewsCount() >= 0 && (i - mListView.getHeaderViewsCount()) + 1 <= mAlbums.size())
            {
                Object obj = (HotAlbum)mAlbums.get(i - mListView.getHeaderViewsCount());
                adapterview = new AlbumModel();
                adapterview.albumId = ((HotAlbum) (obj)).album_id;
                obj = new Bundle();
                ((Bundle) (obj)).putString("album", JSON.toJSONString(adapterview));
                ((Bundle) (obj)).putInt("from", 7);
                ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj)));
            }
        }

        _cls4()
        {
            this$0 = FocusAlbumListFragmentNew.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AbsListView.OnScrollListener
    {

        final FocusAlbumListFragmentNew this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || mPageId > mMaxPage)
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
                        loadData();
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls5()
        {
            this$0 = FocusAlbumListFragmentNew.this;
            super();
        }
    }


    private class _cls6 extends a
    {

        final FocusAlbumListFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
            ((PullToRefreshListView)mListView).onRefreshComplete();
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onStart()
        {
            super.onStart();
            if (mPageId > 1)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
            }
        }

        public void onSuccess(final String responseContent)
        {
            if (!canGoon())
            {
                return;
            } else
            {
                class _cls1
                    implements MyCallback
                {

                    final _cls6 this$1;
                    final String val$responseContent;

                    public void execute()
                    {
                        parseData(responseContent);
                    }

                _cls1()
                {
                    this$1 = _cls6.this;
                    responseContent = s;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                return;
            }
        }

        _cls6()
        {
            this$0 = FocusAlbumListFragmentNew.this;
            super();
        }
    }


    private class _cls7 extends a
    {

        final FocusAlbumListFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s))
            {
                HotAlbum hotalbum = null;
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = hotalbum;
                }
                if (s != null && s.getIntValue("ret") == 0)
                {
                    s = s.getJSONObject("status");
                    if (s != null)
                    {
                        int i = 0;
                        while (i < mAlbums.size()) 
                        {
                            hotalbum = (HotAlbum)mAlbums.get(i);
                            boolean flag;
                            if (s.getIntValue((new StringBuilder()).append("").append(hotalbum.album_id).toString()) == 1)
                            {
                                flag = true;
                            } else
                            {
                                flag = false;
                            }
                            hotalbum.is_favorite = flag;
                            i++;
                        }
                        mAdapter.notifyDataSetChanged();
                        return;
                    }
                }
            }
        }

        _cls7()
        {
            this$0 = FocusAlbumListFragmentNew.this;
            super();
        }
    }


    private class _cls8 extends MyAsyncTask
    {

        final FocusAlbumListFragmentNew this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            int i = 0;
            while (i < mAlbums.size()) 
            {
                avoid = (HotAlbum)mAlbums.get(i);
                boolean flag;
                if (AlbumModelManage.getInstance().isHadCollected(((HotAlbum) (avoid)).album_id) != null)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                avoid.is_favorite = flag;
                i++;
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            mAdapter.notifyDataSetChanged();
        }

        _cls8()
        {
            this$0 = FocusAlbumListFragmentNew.this;
            super();
        }
    }

}
