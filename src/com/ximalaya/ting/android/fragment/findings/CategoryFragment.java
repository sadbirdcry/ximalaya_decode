// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.finding.CategoryAdapter;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.fragment.finding2.category.CategoryContentFragment;
import com.ximalaya.ting.android.model.ad.BannerAdController;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.List;

public class CategoryFragment extends BaseFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, com.ximalaya.ting.android.fragment.ReloadFragment.Callback
{

    private static final int HIGHLIGHT_COUNT = 5;
    private CategoryAdapter mAdapter;
    private BannerAdController mBannerAdController;
    private ListView mListView;
    private Loader mLoader;
    private View mLoadingView;

    public CategoryFragment()
    {
    }

    private void clickItem(FindingCategoryModel findingcategorymodel, View view)
    {
        startFragment(CategoryContentFragment.newInstance((int)findingcategorymodel.getId(), findingcategorymodel.getContentType(), findingcategorymodel.getTitle(), view, false));
    }

    public static CategoryFragment newInstance()
    {
        return new CategoryFragment();
    }

    private void setupHeader(List list, ImageView aimageview[])
    {
        int i = 0;
        while (i < 5) 
        {
            if (i < list.size())
            {
                final FindingCategoryModel model = (FindingCategoryModel)list.get(i);
                final ImageView view = aimageview[i];
                ImageManager2.from(getActivity()).displayImage(aimageview[i], model.getCoverPath(), 0x7f020136);
                aimageview[i].setOnClickListener(new _cls2());
            } else
            {
                aimageview[i].setVisibility(4);
            }
            i++;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        if (a.l)
        {
            mBannerAdController = new BannerAdController(getActivity(), mListView, "cata_index_banner");
            mBannerAdController.load(this);
            bundle = mBannerAdController.getView();
            bundle.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ((ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 10F) * 2) * 170) / 720));
            mListView.addFooterView(bundle);
        }
        mLoader = getLoaderManager().initLoader(0x7f0a002d, null, this);
        ((MyAsyncTaskLoader)mLoader).setXDCSBindView(fragmentBaseContainerView, fragmentBaseContainerView);
    }

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        return new com.ximalaya.ting.android.c.a(getActivity());
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f03009e, viewgroup, false);
        mListView = (ListView)layoutinflater.findViewById(0x7f0a02d9);
        fragmentBaseContainerView = layoutinflater;
        if (PackageUtil.isMeizu() && getActivity() != null)
        {
            mListView.setPadding(0, 0, 0, Utilities.dip2px(getActivity(), 70F));
        }
        mLoadingView = fragmentBaseContainerView.findViewById(0x7f0a012c);
        return layoutinflater;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        if (mLoadingView != null)
        {
            mLoadingView.setVisibility(8);
        }
        if (list != null && list.size() > 0)
        {
            Object obj = LayoutInflater.from(getActivity()).inflate(0x7f030104, mListView, false);
            mListView.addHeaderView(((View) (obj)));
            loader = new ImageView[5];
            loader[0] = (ImageView)((View) (obj)).findViewById(0x7f0a043f);
            loader[1] = (ImageView)((View) (obj)).findViewById(0x7f0a0440);
            loader[2] = (ImageView)((View) (obj)).findViewById(0x7f0a0441);
            loader[3] = (ImageView)((View) (obj)).findViewById(0x7f0a0442);
            loader[4] = (ImageView)((View) (obj)).findViewById(0x7f0a0443);
            if (list.size() <= 5)
            {
                setupHeader(list, loader);
                return;
            }
            obj = new ArrayList(5);
            ArrayList arraylist = new ArrayList(list.size() - 5);
            int i = 0;
            while (i < list.size()) 
            {
                if (i < 5)
                {
                    ((ArrayList) (obj)).add(list.get(i));
                } else
                {
                    arraylist.add(list.get(i));
                }
                i++;
            }
            setupHeader(((List) (obj)), loader);
            mAdapter = new CategoryAdapter(getActivity(), arraylist);
            mAdapter.setOnCellClickListener(new _cls1());
            mListView.setAdapter(mAdapter);
            return;
        } else
        {
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
            return;
        }
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onPause()
    {
        super.onPause();
        if (mBannerAdController != null)
        {
            mBannerAdController.stopSwapAd();
        }
    }

    public void onResume()
    {
        super.onResume();
        if (mBannerAdController != null)
        {
            mBannerAdController.swapAd();
        }
    }

    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
    }

    public void onStart()
    {
        super.onStart();
    }

    public void onStop()
    {
        super.onStop();
    }

    public void onViewCreated(View view, Bundle bundle)
    {
        super.onViewCreated(view, bundle);
    }

    public void onViewStateRestored(Bundle bundle)
    {
        super.onViewStateRestored(bundle);
    }

    public void reload(View view)
    {
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(0x7f0a002d, null, this);
        } else
        {
            mLoader = getLoaderManager().restartLoader(0x7f0a002d, null, this);
        }
        ((MyAsyncTaskLoader)mLoader).setXDCSBindView(null, fragmentBaseContainerView);
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final CategoryFragment this$0;
        final FindingCategoryModel val$model;
        final ImageView val$view;

        public void onClick(View view1)
        {
            clickItem(model, view);
        }

        _cls2()
        {
            this$0 = CategoryFragment.this;
            model = findingcategorymodel;
            view = imageview;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.adapter.finding.CategoryAdapter.OnCellClickListener
    {

        final CategoryFragment this$0;

        public void onCellClick(int i, FindingCategoryModel findingcategorymodel, View view)
        {
            clickItem(findingcategorymodel, view);
        }

        _cls1()
        {
            this$0 = CategoryFragment.this;
            super();
        }
    }

}
