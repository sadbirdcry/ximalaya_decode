// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.widget.LinearLayout;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.album.HotAlbum;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.findings:
//            FocusAlbumListFragmentNew

class val.holder extends MyAsyncTask
{

    final FocusAlbumListFragmentNew this$0;
    final AlbumModel val$am;
    final AlbumItemHolder val$holder;
    final HotAlbum val$model;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        avoid = AlbumModelManage.getInstance();
        HotAlbum hotalbum = val$model;
        boolean flag;
        if (!val$model.is_favorite)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        hotalbum.is_favorite = flag;
        if (!val$model.is_favorite)
        {
            avoid.deleteAlbumInLocalAlbumList(val$am);
        } else
        {
            avoid.saveAlbumModel(val$am);
        }
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        if (((HotAlbum)val$holder.collect.getTag(0x7f090000)).id == val$model.id)
        {
            AlbumItemHolder.setCollectStatus(val$holder, val$model.is_favorite);
        }
    }

    ()
    {
        this$0 = final_focusalbumlistfragmentnew;
        val$model = hotalbum;
        val$am = albummodel;
        val$holder = AlbumItemHolder.this;
        super();
    }
}
