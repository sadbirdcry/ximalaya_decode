// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.HotStationAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.model.broadcast.StationCategory;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.SlideView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FindingHotStationCategoryFragment extends BaseActivityLikeFragment
    implements com.ximalaya.ting.android.fragment.ReloadFragment.Callback
{
    public static final class FooterView extends Enum
    {

        private static final FooterView $VALUES[];
        public static final FooterView HIDE_ALL;
        public static final FooterView LOADING;
        public static final FooterView MORE;
        public static final FooterView NO_CONNECTION;
        public static final FooterView NO_DATA;

        public static FooterView valueOf(String s)
        {
            return (FooterView)Enum.valueOf(com/ximalaya/ting/android/fragment/findings/FindingHotStationCategoryFragment$FooterView, s);
        }

        public static FooterView[] values()
        {
            return (FooterView[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterView("MORE", 0);
            LOADING = new FooterView("LOADING", 1);
            NO_CONNECTION = new FooterView("NO_CONNECTION", 2);
            HIDE_ALL = new FooterView("HIDE_ALL", 3);
            NO_DATA = new FooterView("NO_DATA", 4);
            $VALUES = (new FooterView[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA
            });
        }

        private FooterView(String s, int i)
        {
            super(s, i);
        }
    }


    private boolean isHideHeaderAndSlide;
    private HotStationAdapter mAdapter;
    private ArrayList mCategorys;
    private RelativeLayout mFooterViewLoading;
    private boolean mIsDataLoaded;
    private boolean mIsLoading;
    private ExpandableListView mListView;
    private View mLoadingView;
    private int mMaxPageId;
    private int mPageId;
    private int mSelectedPosition;

    public FindingHotStationCategoryFragment()
    {
        mCategorys = new ArrayList();
        mPageId = 1;
        mIsLoading = false;
        isHideHeaderAndSlide = false;
    }

    private void clearRef()
    {
        mListView = null;
        mAdapter = null;
        mFooterViewLoading = null;
        fragmentBaseContainerView = null;
    }

    private void initTitleBarAndSlide()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            isHideHeaderAndSlide = bundle.getBoolean("isHideHeaderAndSlide", false);
            if (isHideHeaderAndSlide)
            {
                if (findViewById(0x7f0a005f) != null)
                {
                    findViewById(0x7f0a005f).setVisibility(8);
                }
                if (findViewById(0x7f0a0055) != null)
                {
                    ((SlideView)findViewById(0x7f0a0055)).setSlide(false);
                }
            }
        }
    }

    private void initView()
    {
        mListView = (ExpandableListView)findViewById(0x7f0a0278);
        mLoadingView = findViewById(0x7f0a012c);
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(mCon).inflate(0x7f0301fb, null);
        mListView.addFooterView(mFooterViewLoading);
        showFooterView(FooterView.HIDE_ALL);
        View view = new View(mActivity);
        view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ToolUtil.dp2px(mActivity, 70F)));
        mListView.addFooterView(view);
        mAdapter = new HotStationAdapter(this, mListView, mCategorys);
        mListView.setAdapter(mAdapter);
        if (mSelectedPosition != 0)
        {
            mListView.setSelection(mSelectedPosition);
        }
        mListView.setOnGroupClickListener(new _cls1());
        mListView.setOnItemClickListener(new _cls2());
        mListView.setOnScrollListener(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
    }

    private void loadData(final boolean loadMore)
    {
        if (getUserVisibleHint() && getView() != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (mIsDataLoaded && !loadMore)
        {
            continue; /* Loop/switch isn't completed */
        }
        mIsDataLoaded = true;
        if (mIsLoading) goto _L1; else goto _L3
_L3:
        if (!loadMore)
        {
            mLoadingView.setVisibility(0);
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("page", (new StringBuilder()).append("").append(mPageId).toString());
        f.a().a("m/explore_user_index", requestparams, null, new _cls5(), true);
        return;
        if (mCategorys == null || mCategorys.isEmpty()) goto _L1; else goto _L4
_L4:
        if (mAdapter != null)
        {
            mAdapter.notifyDataSetChanged();
            return;
        } else
        {
            mAdapter = new HotStationAdapter(this, mListView, mCategorys);
            mListView.setAdapter(mAdapter);
            return;
        }
    }

    private void loadFollowStatus()
    {
        Object obj = UserInfoMannage.getInstance().getUser();
        if (obj == null)
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("uid", (new StringBuilder()).append("").append(((LoginInfoModel) (obj)).uid).toString());
        obj = new StringBuilder();
        Iterator iterator = mCategorys.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj1 = (StationCategory)iterator.next();
            if (obj1 != null && ((StationCategory) (obj1)).list != null)
            {
                obj1 = ((StationCategory) (obj1)).list.iterator();
                while (((Iterator) (obj1)).hasNext()) 
                {
                    ((StringBuilder) (obj)).append(((StationModel)((Iterator) (obj1)).next()).uid).append(",");
                }
            }
        } while (true);
        requestparams.put("uids", ((StringBuilder) (obj)).toString());
        f.a().a("m/follow_status", requestparams, null, new _cls6());
    }

    private boolean moreDataAvailable()
    {
        return mPageId <= mMaxPageId;
    }

    public static FindingHotStationCategoryFragment newInstance(boolean flag)
    {
        FindingHotStationCategoryFragment findinghotstationcategoryfragment = new FindingHotStationCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isHideHeaderAndSlide", flag);
        findinghotstationcategoryfragment.setArguments(bundle);
        return findinghotstationcategoryfragment;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initTitleBarAndSlide();
        initView();
        setTitleText("\u6700\u706B\u4E3B\u64AD");
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300fc, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        if (mListView != null)
        {
            mSelectedPosition = mListView.getFirstVisiblePosition();
        }
    }

    public void onPause()
    {
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        loadData(false);
    }

    public void reload(View view)
    {
        mIsDataLoaded = false;
        loadData(false);
    }

    public void setFooterViewText(String s)
    {
        mFooterViewLoading.setVisibility(0);
        mFooterViewLoading.setClickable(false);
        mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
        mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
        ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText((new StringBuilder()).append(s).append("").toString());
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        loadData(false);
    }

    public void showFooterView(FooterView footerview)
    {
        if (canGoon())
        {
            mListView.setFooterDividersEnabled(false);
            mFooterViewLoading.setVisibility(0);
            if (footerview == FooterView.MORE)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                return;
            }
            if (footerview == FooterView.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerview == FooterView.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == FooterView.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerview == FooterView.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }




/*
    static boolean access$102(FindingHotStationCategoryFragment findinghotstationcategoryfragment, boolean flag)
    {
        findinghotstationcategoryfragment.mIsLoading = flag;
        return flag;
    }

*/





/*
    static int access$408(FindingHotStationCategoryFragment findinghotstationcategoryfragment)
    {
        int i = findinghotstationcategoryfragment.mPageId;
        findinghotstationcategoryfragment.mPageId = i + 1;
        return i;
    }

*/


/*
    static int access$502(FindingHotStationCategoryFragment findinghotstationcategoryfragment, int i)
    {
        findinghotstationcategoryfragment.mMaxPageId = i;
        return i;
    }

*/





    private class _cls1
        implements android.widget.ExpandableListView.OnGroupClickListener
    {

        final FindingHotStationCategoryFragment this$0;

        public boolean onGroupClick(ExpandableListView expandablelistview, View view, int i, long l)
        {
            return true;
        }

        _cls1()
        {
            this$0 = FindingHotStationCategoryFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final FindingHotStationCategoryFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
        }

        _cls2()
        {
            this$0 = FindingHotStationCategoryFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AbsListView.OnScrollListener
    {

        final FindingHotStationCategoryFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || !moreDataAvailable())
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        showFooterView(FooterView.LOADING);
                        loadData(true);
                    }
                }
                return;
            }
            showFooterView(FooterView.HIDE_ALL);
        }

        _cls3()
        {
            this$0 = FindingHotStationCategoryFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final FindingHotStationCategoryFragment this$0;

        public void onClick(View view)
        {
            loadData(true);
        }

        _cls4()
        {
            this$0 = FindingHotStationCategoryFragment.this;
            super();
        }
    }


    private class _cls5 extends a
    {

        final FindingHotStationCategoryFragment this$0;
        final boolean val$loadMore;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
        }

        public void onNetError(int i, String s)
        {
            if (!canGoon())
            {
                return;
            }
            mLoadingView.setVisibility(8);
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF01");
            if (loadMore)
            {
                showFooterView(FooterView.NO_DATA);
                return;
            } else
            {
                ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
                return;
            }
        }

        public void onStart()
        {
            super.onStart();
            mIsLoading = true;
            if (mPageId != 1);
            if (loadMore)
            {
                showFooterView(FooterView.LOADING);
            }
        }

        public void onSuccess(String s)
        {
            Object obj = null;
            if (!canGoon())
            {
                return;
            }
            mLoadingView.setVisibility(8);
            if (TextUtils.isEmpty(s))
            {
                if (loadMore)
                {
                    showFooterView(FooterView.NO_DATA);
                    return;
                } else
                {
                    ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
                    return;
                }
            }
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                if (loadMore)
                {
                    showFooterView(FooterView.NO_DATA);
                    return;
                } else
                {
                    ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
                    return;
                }
            }
            mMaxPageId = s.getIntValue("maxPageId");
            final List list = s.getString("list");
            if (TextUtils.isEmpty(list))
            {
                if (loadMore)
                {
                    showFooterView(FooterView.NO_DATA);
                    return;
                } else
                {
                    ReloadFragment.show(getChildFragmentManager(), 0x7f0a005a);
                    return;
                }
            }
            list = JSON.parseArray(list, com/ximalaya/ting/android/model/broadcast/StationCategory);
            JSONObject jsonobject = s.getJSONObject("recommendBozhu");
            s = obj;
            if (jsonobject != null)
            {
                s = obj;
                if (jsonobject.getIntValue("ret") == 0)
                {
                    List list1 = JSON.parseArray(jsonobject.getString("list"), com/ximalaya/ting/android/model/broadcast/StationModel);
                    s = new StationCategory();
                    s.id = -1L;
                    s.title = "\u65B0\u664B\u4E3B\u64AD";
                    s.name = "all";
                    s.list = list1;
                }
            }
            if (list != null && list.size() > 0)
            {
                if (s != null)
                {
                    list.add(1, s);
                }
                class _cls1
                    implements Runnable
                {

                    final _cls5 this$1;
                    final List val$list;

                    public void run()
                    {
                        if (!canGoon())
                        {
                            return;
                        }
                        if (mPageId == 1)
                        {
                            mCategorys.clear();
                        }
                        mCategorys.addAll(list);
                        mAdapter.notifyDataSetChanged();
                        for (int i = 0; i < mAdapter.getGroupCount(); i++)
                        {
                            mListView.expandGroup(i);
                        }

                        if (UserInfoMannage.hasLogined())
                        {
                            loadFollowStatus();
                        }
                        int i = 
// JavaClassFileOutputException: get_constant: invalid tag

                _cls1()
                {
                    this$1 = _cls5.this;
                    list = list1;
                    super();
                }
                }

                mListView.postDelayed(new _cls1(), getAnimationLeftTime());
            }
            showFooterView(FooterView.HIDE_ALL);
        }

        _cls5()
        {
            this$0 = FindingHotStationCategoryFragment.this;
            loadMore = flag;
            super();
        }
    }


    private class _cls6 extends a
    {

        final FindingHotStationCategoryFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if ((s = JSON.parseObject(s)) == null)
            {
                continue; /* Loop/switch isn't completed */
            }
            if (s.getIntValue("ret") != 0)
            {
                continue; /* Loop/switch isn't completed */
            }
            s = s.getJSONObject("status");
            int i;
            if (s == null)
            {
                continue; /* Loop/switch isn't completed */
            }
            i = 0;
_L5:
            Object obj;
            if (i >= mCategorys.size())
            {
                continue; /* Loop/switch isn't completed */
            }
            obj = (StationCategory)mCategorys.get(i);
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_186;
            }
            if (((StationCategory) (obj)).list == null)
            {
                break MISSING_BLOCK_LABEL_186;
            }
            obj = ((StationCategory) (obj)).list.iterator();
_L3:
            StationModel stationmodel;
            if (!((Iterator) (obj)).hasNext())
            {
                break MISSING_BLOCK_LABEL_186;
            }
            stationmodel = (StationModel)((Iterator) (obj)).next();
            boolean flag;
            if (s.getIntValue((new StringBuilder()).append("").append(stationmodel.uid).toString()) == 1)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            try
            {
                stationmodel.isFollowed = flag;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.log((new StringBuilder()).append("excepion: ").append(s.getMessage()).toString());
                return;
            }
              goto _L3
            mAdapter.updateItem(i);
            i++;
            if (true) goto _L5; else goto _L4
_L4:
            if (true) goto _L1; else goto _L6
_L6:
        }

        _cls6()
        {
            this$0 = FindingHotStationCategoryFragment.this;
            super();
        }
    }

}
