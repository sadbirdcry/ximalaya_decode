// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;


// Referenced classes of package com.ximalaya.ting.android.fragment.findings:
//            FindingHotStationCategoryFragment

public static final class  extends Enum
{

    private static final NO_DATA $VALUES[];
    public static final NO_DATA HIDE_ALL;
    public static final NO_DATA LOADING;
    public static final NO_DATA MORE;
    public static final NO_DATA NO_CONNECTION;
    public static final NO_DATA NO_DATA;

    public static  valueOf(String s)
    {
        return ()Enum.valueOf(com/ximalaya/ting/android/fragment/findings/FindingHotStationCategoryFragment$FooterView, s);
    }

    public static [] values()
    {
        return ([])$VALUES.clone();
    }

    static 
    {
        MORE = new <init>("MORE", 0);
        LOADING = new <init>("LOADING", 1);
        NO_CONNECTION = new <init>("NO_CONNECTION", 2);
        HIDE_ALL = new <init>("HIDE_ALL", 3);
        NO_DATA = new <init>("NO_DATA", 4);
        $VALUES = (new .VALUES[] {
            MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
