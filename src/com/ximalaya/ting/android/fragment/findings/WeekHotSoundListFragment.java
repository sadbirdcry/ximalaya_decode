// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.SoundsHotNewAdapter;
import com.ximalaya.ting.android.c.d;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

public class WeekHotSoundListFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks
{
    private static final class FooterViewFlag extends Enum
    {

        private static final FooterViewFlag $VALUES[];
        public static final FooterViewFlag FAIL_GET_DATA;
        public static final FooterViewFlag HIDE_ALL;
        public static final FooterViewFlag LOADING;
        public static final FooterViewFlag MORE;
        public static final FooterViewFlag NO_CONNECTION;
        public static final FooterViewFlag NO_DATA;

        public static FooterViewFlag valueOf(String s)
        {
            return (FooterViewFlag)Enum.valueOf(com/ximalaya/ting/android/fragment/findings/WeekHotSoundListFragment$FooterViewFlag, s);
        }

        public static FooterViewFlag[] values()
        {
            return (FooterViewFlag[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterViewFlag("MORE", 0);
            LOADING = new FooterViewFlag("LOADING", 1);
            NO_CONNECTION = new FooterViewFlag("NO_CONNECTION", 2);
            HIDE_ALL = new FooterViewFlag("HIDE_ALL", 3);
            FAIL_GET_DATA = new FooterViewFlag("FAIL_GET_DATA", 4);
            NO_DATA = new FooterViewFlag("NO_DATA", 5);
            $VALUES = (new FooterViewFlag[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, FAIL_GET_DATA, NO_DATA
            });
        }

        private FooterViewFlag(String s, int i)
        {
            super(s, i);
        }
    }


    private static final int ID_LOAD_DATA = 0;
    private static Handler sHandler = new Handler(Looper.getMainLooper());
    private SoundsHotNewAdapter mAdapter;
    private RelativeLayout mFooterViewLoading;
    private boolean mIsLoading;
    private PullToRefreshListView mListView;
    private Loader mLoader;
    private View mNoNetworkLayout;
    private int mPageId;
    private int mTotalCount;

    public WeekHotSoundListFragment()
    {
        mPageId = 0;
    }

    public static WeekHotSoundListFragment getInstance()
    {
        Bundle bundle = new Bundle();
        bundle.putInt("in_anim", 0x7f040009);
        bundle.putInt("out_anim", 0x7f040008);
        WeekHotSoundListFragment weekhotsoundlistfragment = new WeekHotSoundListFragment();
        weekhotsoundlistfragment.setArguments(bundle);
        return weekhotsoundlistfragment;
    }

    private void loadData(View view)
    {
label0:
        {
            mIsLoading = true;
            showFooterView(FooterViewFlag.LOADING);
            mPageId = mPageId + 1;
            if (getActivity() != null)
            {
                if (mLoader != null)
                {
                    break label0;
                }
                mLoader = getLoaderManager().initLoader(0, null, this);
                ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
            }
            return;
        }
        mLoader = getLoaderManager().restartLoader(0, null, this);
        ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
    }

    private void showFooterView(FooterViewFlag footerviewflag)
    {
        if (isAdded() && mListView != null && mFooterViewLoading != null)
        {
            mListView.setFooterDividersEnabled(false);
            mFooterViewLoading.setVisibility(0);
            if (footerviewflag == FooterViewFlag.MORE)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                return;
            }
            if (footerviewflag == FooterViewFlag.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerviewflag == FooterViewFlag.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerviewflag == FooterViewFlag.FAIL_GET_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerviewflag == FooterViewFlag.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u5F53\u524D\u6CA1\u6709\u6570\u636E");
                return;
            }
            if (footerviewflag == FooterViewFlag.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }

    private void showNoNotworkLayout(boolean flag)
    {
        View view = mNoNetworkLayout;
        int i;
        if (flag)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        view.setVisibility(i);
    }

    protected void loadMoreData(View view)
    {
label0:
        {
            if (mPageId * 15 < mTotalCount)
            {
                mPageId = mPageId + 1;
                showFooterView(FooterViewFlag.LOADING);
                if (mLoader != null)
                {
                    break label0;
                }
                mLoader = getLoaderManager().initLoader(0, null, this);
                ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
            }
            return;
        }
        mLoader = getLoaderManager().restartLoader(0, null, this);
        ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, mListView);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mListView.setOnRefreshListener(new _cls1());
        mListView.setMyScrollListener2(new _cls2());
        mListView.setOnItemClickListener(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
        mNoNetworkLayout.setOnClickListener(new _cls5());
        initCommon();
        setTitleText(getString(0x7f0901f6));
        sHandler.postDelayed(new _cls6(), 400L);
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        switch (i)
        {
        default:
            return null;

        case 0: // '\0'
            showNoNotworkLayout(false);
            break;
        }
        return new d(getActivity(), mPageId);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300d1, viewgroup, false);
        fragmentBaseContainerView = layoutinflater;
        layoutinflater.setClickable(true);
        mListView = (PullToRefreshListView)layoutinflater.findViewById(0x7f0a0025);
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(getActivity()).inflate(0x7f0301fb, null);
        mListView.setHeaderDividersEnabled(false);
        mListView.addFooterView(mFooterViewLoading);
        mNoNetworkLayout = layoutinflater.findViewById(0x7f0a02d6);
        return layoutinflater;
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        if (loader == null) goto _L2; else goto _L1
_L1:
        loader.getId();
        JVM INSTR tableswitch 0 0: default 28
    //                   0 29;
           goto _L2 _L3
_L2:
        return;
_L3:
        mTotalCount = ((d)loader).a();
        if (list != null && list.size() > 0)
        {
            if (mAdapter == null)
            {
                mAdapter = new SoundsHotNewAdapter(getActivity(), list);
                mListView.setAdapter(mAdapter);
            } else
            {
                mAdapter.addData(list);
            }
            showFooterView(FooterViewFlag.HIDE_ALL);
        } else
        {
            if (mPageId > 0)
            {
                mPageId = mPageId - 1;
            }
            if (mPageId <= 0)
            {
                showNoNotworkLayout(true);
            }
            showFooterView(FooterViewFlag.FAIL_GET_DATA);
        }
        mListView.onRefreshComplete();
        mIsLoading = false;
        return;
    }

    public void onLoaderReset(Loader loader)
    {
    }




/*
    static boolean access$002(WeekHotSoundListFragment weekhotsoundlistfragment, boolean flag)
    {
        weekhotsoundlistfragment.mIsLoading = flag;
        return flag;
    }

*/


/*
    static int access$102(WeekHotSoundListFragment weekhotsoundlistfragment, int i)
    {
        weekhotsoundlistfragment.mPageId = i;
        return i;
    }

*/




/*
    static Loader access$302(WeekHotSoundListFragment weekhotsoundlistfragment, Loader loader)
    {
        weekhotsoundlistfragment.mLoader = loader;
        return loader;
    }

*/






    private class _cls1
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final WeekHotSoundListFragment this$0;

        public void onRefresh()
        {
            mIsLoading = true;
            mPageId = 1;
            if (mAdapter != null && mAdapter.getData() != null)
            {
                mAdapter.getData().clear();
            }
            if (mLoader == null)
            {
                mLoader = getLoaderManager().initLoader(0, null, WeekHotSoundListFragment.this);
                ((MyAsyncTaskLoader)mLoader).setXDCSBindView(mListView, mListView);
                return;
            } else
            {
                mLoader = getLoaderManager().restartLoader(0, null, WeekHotSoundListFragment.this);
                ((MyAsyncTaskLoader)mLoader).setXDCSBindView(mListView, mListView);
                return;
            }
        }

        _cls1()
        {
            this$0 = WeekHotSoundListFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnScrollListner
    {

        final WeekHotSoundListFragment this$0;

        public void onMyScrollStateChanged(AbsListView abslistview, int i)
        {
            if (mListView.getLastVisiblePosition() >= mListView.getCount() - 1 && i == 0 && !mIsLoading)
            {
                mIsLoading = true;
                loadMoreData(mListView);
            }
        }

        _cls2()
        {
            this$0 = WeekHotSoundListFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final WeekHotSoundListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            PlayTools.gotoPlay(5, ModelHelper.toSoundInfo(mAdapter.getData()), i - mListView.getHeaderViewsCount(), getActivity(), DataCollectUtil.getDataFromView(view));
        }

        _cls3()
        {
            this$0 = WeekHotSoundListFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final WeekHotSoundListFragment this$0;

        public void onClick(View view)
        {
            if (!mIsLoading)
            {
                showFooterView(FooterViewFlag.LOADING);
                loadMoreData(mFooterViewLoading);
            }
        }

        _cls4()
        {
            this$0 = WeekHotSoundListFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final WeekHotSoundListFragment this$0;

        public void onClick(View view)
        {
            loadData(mNoNetworkLayout);
        }

        _cls5()
        {
            this$0 = WeekHotSoundListFragment.this;
            super();
        }
    }


    private class _cls6
        implements Runnable
    {

        final WeekHotSoundListFragment this$0;

        public void run()
        {
            loadData(mListView);
        }

        _cls6()
        {
            this$0 = WeekHotSoundListFragment.this;
            super();
        }
    }

}
