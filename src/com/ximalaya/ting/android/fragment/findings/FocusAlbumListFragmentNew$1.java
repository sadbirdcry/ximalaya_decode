// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.album.HotAlbum;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.findings:
//            FocusAlbumListFragmentNew

class val.view extends a
{

    final FocusAlbumListFragmentNew this$0;
    final AlbumItemHolder val$holder;
    final HotAlbum val$model;
    final View val$view;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$view);
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        if (FocusAlbumListFragmentNew.access$200(FocusAlbumListFragmentNew.this) == val$model.id)
        {
            FocusAlbumListFragmentNew.access$500(FocusAlbumListFragmentNew.this, val$holder, val$model.is_favorite);
        }
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            return;
        }
        Object obj = null;
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = obj;
        }
        if (s != null && s.getIntValue("ret") == 0)
        {
            s = val$model;
            boolean flag;
            if (!val$model.is_favorite)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            s.is_favorite = flag;
            if (FocusAlbumListFragmentNew.access$200(FocusAlbumListFragmentNew.this) == val$model.id)
            {
                FocusAlbumListFragmentNew.access$500(FocusAlbumListFragmentNew.this, val$holder, val$model.is_favorite);
            }
            if (val$model.is_favorite)
            {
                s = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            showToast(s);
            return;
        }
        if (FocusAlbumListFragmentNew.access$200(FocusAlbumListFragmentNew.this) == val$model.id)
        {
            FocusAlbumListFragmentNew.access$500(FocusAlbumListFragmentNew.this, val$holder, val$model.is_favorite);
        }
        showToast("\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
    }

    ()
    {
        this$0 = final_focusalbumlistfragmentnew;
        val$model = hotalbum;
        val$holder = albumitemholder;
        val$view = View.this;
        super();
    }
}
