// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseFragment;

public class ComingSoonFragment extends BaseFragment
{

    public ComingSoonFragment()
    {
    }

    public static ComingSoonFragment newInstance()
    {
        return new ComingSoonFragment();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = new TextView(getActivity());
        layoutinflater.setText("\u656C\u8BF7\u671F\u5F85\uFF01");
        layoutinflater.setGravity(17);
        layoutinflater.setTextSize(30F);
        layoutinflater.setTextColor(0xff000000);
        layoutinflater.setBackgroundColor(-1);
        return layoutinflater;
    }
}
