// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.findings;

import android.text.TextUtils;
import android.widget.ProgressBar;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.broadcast.Broadcaster;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.findings:
//            FocusPersonListFragmentNew

class this._cls0 extends a
{

    final FocusPersonListFragmentNew this$0;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
    }

    public void onFinish()
    {
        super.onFinish();
        FocusPersonListFragmentNew.access$2200(FocusPersonListFragmentNew.this).setVisibility(8);
        FocusPersonListFragmentNew.access$1102(FocusPersonListFragmentNew.this, false);
        ((PullToRefreshListView)mListView).onRefreshComplete();
    }

    public void onNetError(int i, String s)
    {
        showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
    }

    public void onStart()
    {
        super.onStart();
        if (FocusPersonListFragmentNew.access$1200(FocusPersonListFragmentNew.this) == 1)
        {
            FocusPersonListFragmentNew.access$2200(FocusPersonListFragmentNew.this).setVisibility(0);
        } else
        {
            FocusPersonListFragmentNew.access$2200(FocusPersonListFragmentNew.this).setVisibility(8);
        }
        FocusPersonListFragmentNew.access$1102(FocusPersonListFragmentNew.this, true);
        showFooterView(com.ximalaya.ting.android.fragment.OADING);
    }

    public void onSuccess(String s)
    {
        if (!isAdded())
        {
            return;
        }
        if (TextUtils.isEmpty(s))
        {
            showFooterView(com.ximalaya.ting.android.fragment.O_DATA);
            return;
        }
        Object obj = null;
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = obj;
        }
        if (s == null || s.getIntValue("ret") != 0)
        {
            showFooterView(com.ximalaya.ting.android.fragment.O_DATA);
            return;
        }
        FocusPersonListFragmentNew.access$1502(FocusPersonListFragmentNew.this, s.getIntValue("maxPageId"));
        s = s.getString("list");
        if (TextUtils.isEmpty(s))
        {
            showFooterView(com.ximalaya.ting.android.fragment.O_DATA);
            return;
        }
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/broadcast/Broadcaster);
        if (s == null || s.size() == 0)
        {
            showFooterView(com.ximalaya.ting.android.fragment.O_DATA);
            return;
        }
        if (FocusPersonListFragmentNew.access$1200(FocusPersonListFragmentNew.this) == 1)
        {
            FocusPersonListFragmentNew.access$000(FocusPersonListFragmentNew.this).clear();
        }
        FocusPersonListFragmentNew.access$000(FocusPersonListFragmentNew.this).addAll(s);
        FocusPersonListFragmentNew.access$2300(FocusPersonListFragmentNew.this).notifyDataSetChanged();
        int _tmp = FocusPersonListFragmentNew.access$1208(FocusPersonListFragmentNew.this);
        showFooterView(com.ximalaya.ting.android.fragment.IDE_ALL);
        FocusPersonListFragmentNew.access$2400(FocusPersonListFragmentNew.this);
    }

    cusItemAdapter()
    {
        this$0 = FocusPersonListFragmentNew.this;
        super();
    }
}
