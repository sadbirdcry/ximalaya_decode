// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.ActivityListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.hot_activity.ActivityListModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.SlideView;
import java.util.ArrayList;
import java.util.List;

public class ActivityListFragment extends BaseActivityLikeFragment
    implements com.ximalaya.ting.android.fragment.ReloadFragment.Callback
{

    private ActivityListAdapter adapter;
    private boolean isHideHeaderAndSlide;
    private boolean isLoadingNetData;
    private ListView mListView;
    private View mLoadingView;
    private int maxPageId;
    private List models;
    private int pageId;

    public ActivityListFragment()
    {
        isHideHeaderAndSlide = false;
        pageId = 1;
        isLoadingNetData = false;
    }

    private void clearRef()
    {
        mListView.setAdapter(null);
        mListView = null;
        adapter = null;
        fragmentBaseContainerView = null;
    }

    private void initTitleBarAndSlide()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            isHideHeaderAndSlide = bundle.getBoolean("isHideHeaderAndSlide", false);
            if (isHideHeaderAndSlide)
            {
                if (findViewById(0x7f0a005f) != null)
                {
                    findViewById(0x7f0a005f).setVisibility(8);
                }
                if (findViewById(0x7f0a0055) != null)
                {
                    ((SlideView)findViewById(0x7f0a0055)).setSlide(false);
                }
            }
        }
    }

    private void initViews()
    {
        setTitleText("\u559C\u9A6C\u62C9\u96C5\u7CBE\u5F69\u6D3B\u52A8");
        topTextView.setMaxWidth(ToolUtil.dp2px(getActivity(), 200F));
        mLoadingView = findViewById(0x7f0a012c);
        mListView = (ListView)findViewById(0x7f0a012b);
        adapter = new ActivityListAdapter(getActivity(), models);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new _cls1());
        mListView.setOnScrollListener(new _cls2());
    }

    private void loadActivityData(boolean flag)
    {
        if (!flag)
        {
            mLoadingView.setVisibility(0);
        }
        String s = (new StringBuilder()).append(a.S).append("activity-web/activity/activityList").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("pageId", pageId);
        f.a().a(s, requestparams, DataCollectUtil.getDataFromView(fragmentBaseContainerView), new _cls3());
    }

    public static ActivityListFragment newInstance(boolean flag)
    {
        ActivityListFragment activitylistfragment = new ActivityListFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isHideHeaderAndSlide", flag);
        activitylistfragment.setArguments(bundle);
        return activitylistfragment;
    }

    private void parseActivityData(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        if (pageId == 1)
        {
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a012d);
        }
_L4:
        return;
_L2:
        s = JSON.parseObject(s);
        if (s == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (s.getIntValue("ret") == 0)
        {
            break MISSING_BLOCK_LABEL_108;
        }
        if (pageId != 1) goto _L4; else goto _L3
_L3:
        ReloadFragment.show(getChildFragmentManager(), 0x7f0a012d);
        return;
        s;
        Logger.log((new StringBuilder()).append("parseActivityData exception : ").append(s.getMessage()).toString());
        if (pageId != 1) goto _L4; else goto _L5
_L5:
        ReloadFragment.show(getChildFragmentManager(), 0x7f0a012d);
        return;
        String s1 = s.getString("msg");
        if (!TextUtils.isEmpty(s1))
        {
            if (pageId == 1)
            {
                ReloadFragment.show(getChildFragmentManager(), 0x7f0a012d);
            }
            showToast(s1);
        }
        maxPageId = s.getIntValue("maxPageId");
        s = s.getJSONObject("result");
        if (s == null) goto _L4; else goto _L6
_L6:
        s = s.getString("activityData");
        if (TextUtils.isEmpty(s))
        {
            continue; /* Loop/switch isn't completed */
        }
        s = JSON.parseArray(s, com/ximalaya/ting/android/model/hot_activity/ActivityListModel);
        if (s == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (s.size() > 0)
        {
            if (models == null)
            {
                models = new ArrayList();
                adapter.setData(models);
            }
            if (pageId == 1)
            {
                models.clear();
            }
            models.addAll(s);
            adapter.notifyDataSetChanged();
            return;
        }
        if (pageId != 1) goto _L4; else goto _L7
_L7:
        ReloadFragment.show(getChildFragmentManager(), 0x7f0a012d);
        return;
        if (pageId != 1) goto _L4; else goto _L8
_L8:
        ReloadFragment.show(getChildFragmentManager(), 0x7f0a012d);
        return;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initTitleBarAndSlide();
        initViews();
        loadActivityData(false);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030035, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void reload(View view)
    {
        loadActivityData(false);
    }





/*
    static boolean access$202(ActivityListFragment activitylistfragment, boolean flag)
    {
        activitylistfragment.isLoadingNetData = flag;
        return flag;
    }

*/




/*
    static int access$408(ActivityListFragment activitylistfragment)
    {
        int i = activitylistfragment.pageId;
        activitylistfragment.pageId = i + 1;
        return i;
    }

*/





    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final ActivityListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            adapterview = (ActivityListModel)adapter.getItem(i);
            if (adapterview != null)
            {
                Bundle bundle = new Bundle();
                bundle.putString("ExtraUrl", (new StringBuilder()).append(a.S).append("activity-web/activity/").append(((ActivityListModel) (adapterview)).activityId).toString());
                bundle.putInt("web_view_type", 8);
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/web/WebFragment, bundle);
            }
        }

        _cls1()
        {
            this$0 = ActivityListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AbsListView.OnScrollListener
    {

        final ActivityListFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0 && models != null && !isLoadingNetData && adapter != null && abslistview.getLastVisiblePosition() - 1 == adapter.getCount() && maxPageId > pageId)
            {
                int i = ((access._cls200) (this)).access$200;
                loadActivityData(true);
            }
        }

        _cls2()
        {
            this$0 = ActivityListFragment.this;
            super();
        }
    }


    private class _cls3 extends com.ximalaya.ting.android.b.a
    {

        final ActivityListFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onNetError(int i, String s)
        {
            if (canGoon())
            {
                mLoadingView.setVisibility(8);
                isLoadingNetData = false;
                showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
                if (pageId == 1)
                {
                    ReloadFragment.show(getChildFragmentManager(), 0x7f0a012d);
                    return;
                }
            }
        }

        public void onStart()
        {
            isLoadingNetData = true;
        }

        public void onSuccess(final String responseContent)
        {
            while (isDetached() || !canGoon()) 
            {
                return;
            }
            mLoadingView.setVisibility(8);
            isLoadingNetData = false;
            class _cls1
                implements MyCallback
            {

                final _cls3 this$1;
                final String val$responseContent;

                public void execute()
                {
                    parseActivityData(responseContent);
                }

                _cls1()
                {
                    this$1 = _cls3.this;
                    responseContent = s;
                    super();
                }
            }

            doAfterAnimation(new _cls1());
        }

        _cls3()
        {
            this$0 = ActivityListFragment.this;
            super();
        }
    }

}
