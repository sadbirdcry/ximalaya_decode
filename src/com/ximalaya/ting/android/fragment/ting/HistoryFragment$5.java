// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.transaction.download.LocalSoundCreateDescSorter;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.PlaylistFromDownload;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting:
//            HistoryFragment

class this._cls0
    implements android.widget.ClickListener
{

    final HistoryFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        boolean flag = true;
        boolean flag3 = false;
        boolean flag2 = false;
        if (HistoryFragment.access$400(HistoryFragment.this) != null && HistoryFragment.access$400(HistoryFragment.this).size() != 0)
        {
            if ((i -= HistoryFragment.access$200(HistoryFragment.this).getHeaderViewsCount()) >= 0 && HistoryFragment.access$400(HistoryFragment.this).size() > i)
            {
                adapterview = (SoundInfo)HistoryFragment.access$400(HistoryFragment.this).get(i);
                if (((SoundInfo) (adapterview)).albumId == -1L)
                {
                    adapterview.albumId = adapterview.getRealAlubmId();
                }
                if (DownloadHandler.getInstance(HistoryFragment.access$500(HistoryFragment.this)).isDownloadCompleted(adapterview))
                {
                    if (((SoundInfo) (adapterview)).albumId == 0L)
                    {
                        android.support.v4.app.FragmentActivity fragmentactivity = getActivity();
                        if (HistoryFragment.access$600(HistoryFragment.this))
                        {
                            flag = flag2;
                        } else
                        {
                            flag = true;
                        }
                        PlayTools.gotoPlay(10, adapterview, fragmentactivity, flag, DataCollectUtil.getDataFromView(view));
                    } else
                    {
                        Object obj = DownloadHandler.getInstance(HistoryFragment.access$700(HistoryFragment.this)).getLocalAlbumRelateSound(((SoundInfo) (adapterview)).albumId, new LocalSoundCreateDescSorter());
                        if (obj == null || ((ArrayList) (obj)).size() == 0)
                        {
                            PlayTools.gotoPlay(10, adapterview, getActivity(), DataCollectUtil.getDataFromView(view));
                        } else
                        {
                            obj = ModelHelper.downloadlistToPlayList(new DownloadTask(adapterview), ((List) (obj)));
                            adapterview = ((PlaylistFromDownload) (obj)).soundsList;
                            i = ((PlaylistFromDownload) (obj)).index;
                            obj = getActivity();
                            if (HistoryFragment.access$600(HistoryFragment.this))
                            {
                                flag = false;
                            }
                            PlayTools.gotoPlay(10, null, 0, null, adapterview, i, ((android.content.Context) (obj)), flag, DataCollectUtil.getDataFromView(view));
                        }
                    }
                } else
                {
                    android.support.v4.app.FragmentActivity fragmentactivity1 = getActivity();
                    boolean flag1;
                    if (HistoryFragment.access$600(HistoryFragment.this))
                    {
                        flag1 = flag3;
                    } else
                    {
                        flag1 = true;
                    }
                    PlayTools.gotoPlay(10, adapterview, fragmentactivity1, flag1, DataCollectUtil.getDataFromView(view));
                }
                if (HistoryFragment.access$600(HistoryFragment.this) && HistoryFragment.access$800(HistoryFragment.this) != null)
                {
                    HistoryFragment.access$800(HistoryFragment.this).onFinish();
                    return;
                }
            }
        }
    }

    reateDescSorter()
    {
        this$0 = HistoryFragment.this;
        super();
    }
}
