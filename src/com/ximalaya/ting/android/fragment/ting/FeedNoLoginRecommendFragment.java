// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.adapter.feed.FeedRecommendAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.model.ad.BannerAdController;
import com.ximalaya.ting.android.model.ad.FeedAd2;
import com.ximalaya.ting.android.model.ad.IThirdAd;
import com.ximalaya.ting.android.model.feed2.FeedRecommendModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoList;
import com.ximalaya.ting.android.modelmanage.AdManager;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FeedNoLoginRecommendFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, com.ximalaya.ting.android.adapter.feed.FeedRecommendAdapter.FeedRecommendActionHandler
{
    public static class FeedRecommendList extends BaseModel
    {

        public List data;

        public FeedRecommendList()
        {
        }
    }

    private static final class FooterView extends Enum
    {

        private static final FooterView $VALUES[];
        public static final FooterView HIDE_ALL;
        public static final FooterView LOADING;
        public static final FooterView NO_CONNECTION;

        public static FooterView valueOf(String s)
        {
            return (FooterView)Enum.valueOf(com/ximalaya/ting/android/fragment/ting/FeedNoLoginRecommendFragment$FooterView, s);
        }

        public static FooterView[] values()
        {
            return (FooterView[])$VALUES.clone();
        }

        static 
        {
            LOADING = new FooterView("LOADING", 0);
            NO_CONNECTION = new FooterView("NO_CONNECTION", 1);
            HIDE_ALL = new FooterView("HIDE_ALL", 2);
            $VALUES = (new FooterView[] {
                LOADING, NO_CONNECTION, HIDE_ALL
            });
        }

        private FooterView(String s, int i)
        {
            super(s, i);
        }
    }

    class GetAlbumListTask extends MyAsyncTask
    {

        FeedRecommendModel frm;
        View fromBindView;
        final FeedNoLoginRecommendFragment this$0;

        protected transient SoundInfoList doInBackground(Void avoid[])
        {
            return CommonRequest.getAlbumList(mCon, frm.trackId, frm.albumId, frm.uid, frm.albumTitle, frm.albumCover, frm.recSrc, frm.recTrack, fromBindView, null);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(SoundInfoList soundinfolist)
        {
            if (loadingPopupWindow != null)
            {
                loadingPopupWindow.dismiss();
            }
            if (!canGoon())
            {
                return;
            }
            if (soundinfolist != null)
            {
                if (soundinfolist.ret == 0)
                {
                    PlayTools.gotoPlay(30, soundinfolist.data, getSoundIndex(soundinfolist, frm.trackId), getActivity(), DataCollectUtil.getDataFromView(fromBindView));
                    return;
                } else
                {
                    Toast.makeText(mCon, soundinfolist.msg, 0).show();
                    return;
                }
            } else
            {
                Toast.makeText(mCon, "\u7F51\u7EDC\u8BF7\u6C42\u9519\u8BEF\uFF0C\u8BF7\u91CD\u8BD5", 0).show();
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((SoundInfoList)obj);
        }

        protected void onPreExecute()
        {
            if (loadingDialog == null)
            {
                createDimLoadingDialog();
            }
            loadingDialog.show();
        }

        public GetAlbumListTask setModel(FeedRecommendModel feedrecommendmodel, View view)
        {
            frm = feedrecommendmodel;
            fromBindView = view;
            return this;
        }

        GetAlbumListTask()
        {
            this$0 = FeedNoLoginRecommendFragment.this;
            super();
            frm = null;
        }
    }


    private FooterView footerViewNow;
    Dialog loadingDialog;
    PopupWindow loadingPopupWindow;
    private FeedRecommendAdapter mAdapter;
    private List mAds;
    private BannerAdController mBannerAdController;
    private List mDataList;
    private int mFirstVisiblePosition;
    private RelativeLayout mFooterViewLoading;
    private LinearLayout mHeaderView;
    private boolean mIsAdLoading;
    private boolean mIsFistResumed;
    private boolean mIsLoadedData;
    private ListView mListView;
    private RelativeLayout mRecommendHeader;
    private View mReloadDataView;
    private IThirdAd mThirdBannerAd;
    boolean y;

    public FeedNoLoginRecommendFragment()
    {
        mDataList = new ArrayList();
        mIsLoadedData = false;
        y = false;
        mIsAdLoading = false;
        mIsFistResumed = false;
    }

    private void addAd(List list)
    {
        while (list == null || list.size() <= 0 || mAds == null || mAds.size() <= 0) 
        {
            return;
        }
        ArrayList arraylist = new ArrayList();
        Iterator iterator = mAds.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj = (FeedAd2)iterator.next();
            if (((FeedAd2) (obj)).position >= 0 && ((FeedAd2) (obj)).position < list.size())
            {
                list.add(((FeedAd2) (obj)).position, FeedRecommendModel.from(((FeedAd2) (obj))));
                arraylist.add(obj);
                ThirdAdStatUtil.getInstance().thirdAdStatRequest(((FeedAd2) (obj)).thirdStatUrl);
                obj = AdManager.getInstance().getAdIdFromUrl(((FeedAd2) (obj)).link);
                if (!TextUtils.isEmpty(((CharSequence) (obj))))
                {
                    AdCollectData adcollectdata = new AdCollectData();
                    adcollectdata.setAdItemId(((String) (obj)));
                    adcollectdata.setAdSource("0");
                    adcollectdata.setAndroidId(ToolUtil.getAndroidId(mCon.getApplicationContext()));
                    adcollectdata.setLogType("tingShow");
                    adcollectdata.setPositionName("feed_follow");
                    adcollectdata.setResponseId(((String) (obj)));
                    adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                    adcollectdata.setTrackId("-1");
                    DataCollectUtil.getInstance(mCon.getApplicationContext()).statOnlineAd(adcollectdata);
                }
            }
        } while (true);
        mAds.removeAll(arraylist);
    }

    private void addBannerAd()
    {
        if (a.k)
        {
            mBannerAdController = new BannerAdController(getActivity(), mListView, "banner");
            mBannerAdController.load(this);
            View view = mBannerAdController.getView();
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ((ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 10F) * 2) * 170) / 720));
            mHeaderView.addView(view);
        }
    }

    private void createDimLoadingDialog()
    {
        loadingDialog = new Dialog(getActivity(), 0x1030010);
        final View emptyDialog = LayoutInflater.from(getActivity()).inflate(0x7f03006a, null);
        android.view.WindowManager.LayoutParams layoutparams = loadingDialog.getWindow().getAttributes();
        layoutparams.dimAmount = 0.5F;
        loadingDialog.getWindow().setAttributes(layoutparams);
        loadingDialog.getWindow().addFlags(2);
        loadingDialog.setContentView(emptyDialog);
        loadingDialog.setCanceledOnTouchOutside(true);
        loadingDialog.setOnShowListener(new _cls4());
    }

    private void getFeedFromPref()
    {
        Object obj = SharedPreferencesUtil.getInstance(mCon).getString("temp_feed_nologin");
        try
        {
            obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/feed2/FeedRecommendModel);
        }
        catch (Exception exception)
        {
            Logger.e("FeedNoLoginRecommendFragment", "\u89E3\u6790\u672A\u767B\u5F55Feed\u7F13\u5B58JSON\u5931\u8D25", exception);
            exception.printStackTrace();
            exception = null;
        }
        if (obj != null)
        {
            mDataList.addAll(((java.util.Collection) (obj)));
            mAdapter.notifyDataSetChanged();
        }
    }

    private int getSoundIndex(SoundInfoList soundinfolist, long l)
    {
        if (soundinfolist == null || soundinfolist.data == null || soundinfolist.data.isEmpty())
        {
            return 0;
        }
        int j = soundinfolist.data.size();
        for (int i = 0; i != j; i++)
        {
            if (l == ((SoundInfo)soundinfolist.data.get(i)).trackId)
            {
                return i;
            }
        }

        return soundinfolist.data.size() - 1;
    }

    private void initData()
    {
        if (getUserVisibleHint() && getView() != null)
        {
            if (!mIsLoadedData)
            {
                mIsLoadedData = true;
                if (!NetworkUtils.isNetworkAvaliable(getActivity()))
                {
                    getFeedFromPref();
                }
                loadDataList(fragmentBaseContainerView);
                return;
            }
            if (mDataList != null && !mDataList.isEmpty())
            {
                if (mAdapter == null)
                {
                    mAdapter = new FeedRecommendAdapter(getActivity(), mDataList, this);
                    mListView.setAdapter(mAdapter);
                    if (mFirstVisiblePosition > 0)
                    {
                        mListView.setSelection(mFirstVisiblePosition);
                    }
                }
                mRecommendHeader.setVisibility(0);
                mAdapter.notifyDataSetChanged();
            } else
            {
                showNoNetworkLayout(true);
            }
            if (mBannerAdController != null)
            {
                mBannerAdController.swapAd();
                return;
            }
        }
    }

    private void initHeaderView()
    {
        mRecommendHeader = (RelativeLayout)View.inflate(mCon, 0x7f030081, null);
        mHeaderView.addView(mRecommendHeader);
        if (mAdapter == null || mAdapter.getCount() == 0)
        {
            mRecommendHeader.setVisibility(8);
        }
    }

    private void initUI()
    {
        mListView = (ListView)fragmentBaseContainerView.findViewById(0x7f0a005c);
        mHeaderView = new LinearLayout(mCon);
        mHeaderView.setOrientation(1);
        mListView.addHeaderView(mHeaderView);
        addBannerAd();
        initHeaderView();
        mAdapter = new FeedRecommendAdapter(mActivity, mDataList, this);
        mListView.setAdapter(mAdapter);
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(getActivity()).inflate(0x7f0301fb, null);
        mListView.addFooterView(mFooterViewLoading);
        showFooterView(FooterView.HIDE_ALL);
        mFooterViewLoading.setOnClickListener(new _cls1());
        mReloadDataView.setOnClickListener(new _cls2());
    }

    private void loadDataList(View view)
    {
        if (NetworkUtils.isNetworkAvaliable(getActivity()))
        {
            showNoNetworkLayout(false);
            showFooterView(FooterView.LOADING);
            RequestParams requestparams = new RequestParams();
            if (loginInfoModel == null)
            {
                loginInfoModel = UserInfoMannage.getInstance().getUser();
            }
            if (loginInfoModel != null)
            {
                requestparams.put("uid", (new StringBuilder()).append("").append(loginInfoModel.uid).toString());
                requestparams.put("token", loginInfoModel.token);
            }
            f.a().a("mobile/api/1/feed/unlogin/dynamic", requestparams, DataCollectUtil.getDataFromView(view), new _cls3());
        } else
        if (mAdapter == null || mAdapter.getCount() <= 0)
        {
            showNoNetworkLayout(true);
            return;
        }
    }

    public static FeedNoLoginRecommendFragment newInstance()
    {
        return new FeedNoLoginRecommendFragment();
    }

    private void parseData(String s)
    {
        if (s == null)
        {
            if (mAdapter == null || mAdapter.getCount() <= 0)
            {
                showNoNetworkLayout(true);
            }
            showFooterView(FooterView.HIDE_ALL);
        } else
        {
            try
            {
                s = (FeedRecommendList)JSON.parseObject(s, com/ximalaya/ting/android/fragment/ting/FeedNoLoginRecommendFragment$FeedRecommendList);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.e("FeedNologinRecommendFragment", "\u89E3\u6790JSON\u5931\u8D25", s);
                s = null;
            }
            if (s == null || ((FeedRecommendList) (s)).ret != 0 || ((FeedRecommendList) (s)).data == null || ((FeedRecommendList) (s)).data.isEmpty())
            {
                showFooterView(FooterView.HIDE_ALL);
                if (mAdapter == null || mAdapter.getCount() <= 0)
                {
                    showNoNetworkLayout(true);
                    return;
                }
            } else
            {
                mDataList.clear();
                if (((FeedRecommendList) (s)).data == null || ((FeedRecommendList) (s)).data.isEmpty())
                {
                    showFooterView(FooterView.HIDE_ALL);
                    return;
                } else
                {
                    mRecommendHeader.setVisibility(0);
                    addAd(((FeedRecommendList) (s)).data);
                    mDataList.addAll(((FeedRecommendList) (s)).data);
                    mAdapter.notifyDataSetChanged();
                    showFooterView(FooterView.HIDE_ALL);
                    return;
                }
            }
        }
    }

    private void saveFeedToPref()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCon);
        if (mDataList != null)
        {
            sharedpreferencesutil.saveString("temp_feed_nologin", JSON.toJSONString(mDataList));
        }
    }

    private void showFooterView(FooterView footerview)
    {
        footerViewNow = footerview;
        mListView.setFooterDividersEnabled(false);
        mFooterViewLoading.setVisibility(0);
        if (footerview == FooterView.LOADING)
        {
            mFooterViewLoading.setClickable(false);
            mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
            mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
            ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
        } else
        {
            if (footerview == FooterView.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == FooterView.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }

    private void showNoNetworkLayout(boolean flag)
    {
        byte byte0 = 8;
        Object obj = mReloadDataView;
        int i;
        if (flag)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        ((View) (obj)).setVisibility(i);
        obj = mListView;
        if (flag)
        {
            i = byte0;
        } else
        {
            i = 0;
        }
        ((ListView) (obj)).setVisibility(i);
    }

    private void updateClickEvent(long l)
    {
        RequestParams requestparams = new RequestParams();
        String s = (new StringBuilder()).append("m/feed_recomm_albums/").append(l).append("/clicked").toString();
        f.a().b(s, requestparams, null, new _cls5());
    }

    public void doRegAndFollow(long l, long l1, View view)
    {
        updateClickEvent(l1);
        ((MyApplication)(MyApplication)mActivity.getApplicationContext()).a(l);
        view = new Intent(getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity);
        getActivity().startActivity(view);
    }

    public void goHostHomePage(long l, long l1, View view)
    {
        updateClickEvent(l1);
        Bundle bundle = new Bundle();
        bundle.putLong("toUid", l);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        if (MyApplication.a() instanceof MainTabActivity2)
        {
            ((MainTabActivity2)MyApplication.a()).startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
        }
    }

    public void goToPlay(int i, View view)
    {
        FeedRecommendModel feedrecommendmodel;
label0:
        {
            feedrecommendmodel = (FeedRecommendModel)mAdapter.getItem(i);
            if (feedrecommendmodel != null)
            {
                if (feedrecommendmodel.type != 1)
                {
                    break label0;
                }
                FeedAd2.handleClick(this, (FeedAd2)feedrecommendmodel.tag, "feed_follow");
            }
            return;
        }
        ToolUtil.onEvent(getActivity(), "Feed_Recommend");
        updateClickEvent(feedrecommendmodel.albumId);
        (new GetAlbumListTask()).setModel(feedrecommendmodel, view).myexec(new Void[0]);
    }

    public void loadAd()
    {
        if (mIsAdLoading)
        {
            return;
        } else
        {
            getLoaderManager().restartLoader(0x7f0a0030, null, this);
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initUI();
        getLoaderManager().initLoader(0x7f0a0030, null, this);
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        mIsAdLoading = true;
        return new com.ximalaya.ting.android.model.ad.FeedAd2.Loader(getActivity(), "feed_follow");
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300b8, viewgroup, false);
        mReloadDataView = fragmentBaseContainerView.findViewById(0x7f0a0353);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        if (mListView != null)
        {
            mFirstVisiblePosition = mListView.getFirstVisiblePosition();
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        mIsAdLoading = false;
        mAds = list;
        initData();
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onPause()
    {
        if (mBannerAdController != null)
        {
            mBannerAdController.stopSwapAd();
        }
        saveFeedToPref();
        super.onPause();
        if (mThirdBannerAd != null)
        {
            mThirdBannerAd.pause();
        }
    }

    public void onResume()
    {
        super.onResume();
        if (mBannerAdController != null)
        {
            mBannerAdController.swapAd();
        }
        if (mThirdBannerAd != null)
        {
            mThirdBannerAd.resume();
        }
        if (mIsFistResumed)
        {
            if (getUserVisibleHint())
            {
                loadAd();
            }
            return;
        } else
        {
            mIsFistResumed = true;
            return;
        }
    }

    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        saveFeedToPref();
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        initData();
    }












    private class _cls4
        implements android.content.DialogInterface.OnShowListener
    {

        final FeedNoLoginRecommendFragment this$0;
        final View val$emptyDialog;

        public void onShow(final DialogInterface dialog)
        {
            if (loadingPopupWindow == null)
            {
                loadingPopupWindow = new PopupWindow((RelativeLayout)LayoutInflater.from(getActivity()).inflate(0x7f030196, null), -2, -2);
                loadingPopupWindow.setFocusable(false);
                loadingPopupWindow.setOutsideTouchable(false);
                class _cls1
                    implements android.widget.PopupWindow.OnDismissListener
                {

                    final _cls4 this$1;
                    final DialogInterface val$dialog;

                    public void onDismiss()
                    {
                        if (dialog != null)
                        {
                            dialog.dismiss();
                        }
                    }

                _cls1()
                {
                    this$1 = _cls4.this;
                    dialog = dialoginterface;
                    super();
                }
                }

                loadingPopupWindow.setOnDismissListener(new _cls1());
            }
            loadingPopupWindow.showAtLocation(emptyDialog, 17, 0, 0);
        }

        _cls4()
        {
            this$0 = FeedNoLoginRecommendFragment.this;
            emptyDialog = view;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FeedNoLoginRecommendFragment this$0;

        public void onClick(View view)
        {
            if (footerViewNow != FooterView.LOADING)
            {
                loadDataList(mFooterViewLoading);
            }
        }

        _cls1()
        {
            this$0 = FeedNoLoginRecommendFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final FeedNoLoginRecommendFragment this$0;

        public void onClick(View view)
        {
            loadDataList(mReloadDataView);
        }

        _cls2()
        {
            this$0 = FeedNoLoginRecommendFragment.this;
            super();
        }
    }


    private class _cls3 extends com.ximalaya.ting.android.b.a
    {

        final FeedNoLoginRecommendFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
        }

        public void onNetError(int i, String s)
        {
            if (canGoon())
            {
                showFooterView(FooterView.NO_CONNECTION);
                if (mAdapter == null || mAdapter.getCount() <= 0)
                {
                    showNoNetworkLayout(true);
                }
            }
        }

        public void onSuccess(final String responseContent)
        {
            class _cls1
                implements MyCallback
            {

                final _cls3 this$1;
                final String val$responseContent;

                public void execute()
                {
                    parseData(responseContent);
                }

                _cls1()
                {
                    this$1 = _cls3.this;
                    responseContent = s;
                    super();
                }
            }

            if (canGoon())
            {
                doAfterAnimation(new _cls1());
            }
        }

        _cls3()
        {
            this$0 = FeedNoLoginRecommendFragment.this;
            super();
        }
    }


    private class _cls5 extends com.ximalaya.ting.android.b.a
    {

        final FeedNoLoginRecommendFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
            Logger.e("\u8C03\u7528feed\u63A8\u8350\u4E3B\u64AD\u4E13\u8F91\u70B9\u51FB\u7EDF\u8BA1\u63A5\u53E3\u5931\u8D25", s);
        }

        public void onSuccess(String s)
        {
            Object obj = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = obj;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                return;
            } else
            {
                Logger.v("feed", " \u8C03\u7528feed\u63A8\u8350\u4E3B\u64AD\u4E13\u8F91\u70B9\u51FB\u7EDF\u8BA1\u63A5\u53E3\u6210\u529F");
                return;
            }
        }

        _cls5()
        {
            this$0 = FeedNoLoginRecommendFragment.this;
            super();
        }
    }

}
