// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting:
//            CollectFragment

class this._cls0
    implements android.widget.ClickListener
{

    final CollectFragment this$0;

    public void onItemClick(AdapterView adapterview, final View arg1, int i, long l)
    {
        if (CollectFragment.access$1000(CollectFragment.this) == null || CollectFragment.access$1000(CollectFragment.this).size() == 0)
        {
            return;
        }
        Object obj = arg1.findViewById(0x7f0a0156);
        adapterview = (AlbumModel)CollectFragment.access$1000(CollectFragment.this).get(i - CollectFragment.access$400(CollectFragment.this).getHeaderViewsCount());
        if (((View) (obj)).getVisibility() == 0)
        {
            ((View) (obj)).setVisibility(8);
            adapterview.hasNew = false;
            class _cls1 extends MyAsyncTask
            {

                final CollectFragment._cls5 this$1;
                final View val$arg1;

                protected volatile Object doInBackground(Object aobj[])
                {
                    return doInBackground((Long[])aobj);
                }

                protected transient String doInBackground(Long along[])
                {
                    long l1 = along[0].longValue();
                    along = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/subscribe/check").toString();
                    RequestParams requestparams = new RequestParams();
                    requestparams.put("albumId", (new StringBuilder()).append(l1).append("").toString());
                    return f.a().b(along, requestparams, arg1, null);
                }

                protected volatile void onPostExecute(Object obj1)
                {
                    onPostExecute((String)obj1);
                }

                protected void onPostExecute(String s)
                {
                    if (!Utilities.isBlank(s)) goto _L2; else goto _L1
_L1:
                    return;
_L2:
                    if (JSON.parseObject(s).getIntValue("ret") != 0) goto _L1; else goto _L3
_L3:
                    CollectFragment.access$1100(this$0).notifyDataSetChanged();
                    return;
                    s;
                    s.printStackTrace();
                    return;
                }

            _cls1()
            {
                this$1 = CollectFragment._cls5.this;
                arg1 = view;
                super();
            }
            }

            (new _cls1()).myexec(new Long[] {
                Long.valueOf(((AlbumModel) (adapterview)).albumId)
            });
        }
        obj = new Bundle();
        ((Bundle) (obj)).putString("album", JSON.toJSONString(adapterview));
        if (CollectFragment.access$1200(CollectFragment.this) == 1)
        {
            i = 9;
        } else
        {
            i = 1;
        }
        ((Bundle) (obj)).putInt("from", i);
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(arg1));
        startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj)));
    }

    _cls1()
    {
        this$0 = CollectFragment.this;
        super();
    }
}
