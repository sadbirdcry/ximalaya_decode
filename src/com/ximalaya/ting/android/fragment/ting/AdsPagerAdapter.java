// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.ximalaya.ting.android.library.model.BaseAdModel;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting:
//            AdImageFragment

public class AdsPagerAdapter extends FragmentStatePagerAdapter
{

    private List mAdds;
    private String positionName;

    public AdsPagerAdapter(FragmentManager fragmentmanager, List list, String s)
    {
        super(fragmentmanager);
        mAdds = list;
        if (mAdds == null)
        {
            mAdds = new ArrayList();
        }
        positionName = s;
    }

    private int getSameFragmentPosition(String s)
    {
        if (mAdds != null) goto _L2; else goto _L1
_L1:
        int j = -2;
_L4:
        return j;
_L2:
        int i = 0;
label0:
        do
        {
label1:
            {
                if (i >= mAdds.size())
                {
                    break label1;
                }
                j = i;
                if (((BaseAdModel)mAdds.get(i)).getICover().equals(s))
                {
                    break label0;
                }
                i++;
            }
        } while (true);
        if (true) goto _L4; else goto _L3
_L3:
        return -2;
    }

    public int getCount()
    {
        return 0x7fffffff;
    }

    public Fragment getItem(int i)
    {
        int j = i;
        if (mAdds.size() > 0)
        {
            j = i % mAdds.size();
        }
        return new AdImageFragment((BaseAdModel)mAdds.get(j), positionName);
    }

    public int getItemPosition(Object obj)
    {
        int i = getSameFragmentPosition(((AdImageFragment)obj).getCover());
        if (i >= 0)
        {
            return i;
        } else
        {
            return -2;
        }
    }

    public void setData(List list)
    {
        if (mAdds == null)
        {
            mAdds = new ArrayList();
        }
        mAdds.clear();
        mAdds.addAll(list);
    }
}
