// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.SoundInforHistoryNewAdapter;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.List;

public class HistoryFragment extends BaseFragment
    implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{

    private View headView;
    private com.ximalaya.ting.android.modelmanage.HistoryManage.HistoryUpdateListener historyUpdateListener;
    private SoundInforHistoryNewAdapter mAdapter;
    private LinearLayout mEmptyView;
    private boolean mExitWhenPlay;
    private int mFirstVisiblePosition;
    private boolean mIsLoadedData;
    private BounceListView mListView;
    private com.ximalaya.ting.android.view.SlideRightOutView.OnFinishListener mOnFinishCallback;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private List tSoundInfoList;
    private LoginInfoModel user;

    public HistoryFragment()
    {
        tSoundInfoList = new ArrayList();
        mIsLoadedData = false;
    }

    private void clearRef()
    {
        mListView.setAdapter(null);
        mListView = null;
        mAdapter = null;
        fragmentBaseContainerView = null;
    }

    private void initData()
    {
        user = UserInfoMannage.getInstance().getUser();
        mListView = (BounceListView)fragmentBaseContainerView.findViewById(0x7f0a005c);
        headView = LayoutInflater.from(getActivity()).inflate(0x7f030108, mListView, false);
        mListView.addHeaderView(headView);
        if (tSoundInfoList.isEmpty())
        {
            headView.setVisibility(8);
        }
        mEmptyView = (LinearLayout)LayoutInflater.from(mActivity).inflate(0x7f03007c, mListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u64AD\u653E\u8FC7\u58F0\u97F3\u54E6");
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setVisibility(8);
        ((Button)mEmptyView.findViewById(0x7f0a0232)).setVisibility(8);
        mListView.addFooterView(mEmptyView);
        mEmptyView.setVisibility(8);
        mAdapter = new SoundInforHistoryNewAdapter(getActivity(), tSoundInfoList);
        mListView.setAdapter(mAdapter);
        headView.findViewById(0x7f0a0447).setOnClickListener(new _cls1());
        setData();
    }

    private void initListener()
    {
        historyUpdateListener = new _cls2();
        HistoryManage.getInstance(getActivity()).setOnHistoryUpdateListener(historyUpdateListener);
        headView.findViewById(0x7f0a0447).setOnClickListener(new _cls3());
        mListView.setOnItemLongClickListener(new _cls4());
        mListView.setOnItemClickListener(new _cls5());
    }

    public static HistoryFragment newInstance()
    {
        return new HistoryFragment();
    }

    private void registerListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls6();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void setData()
    {
        if (getUserVisibleHint() && getView() != null)
        {
            if (!mIsLoadedData)
            {
                mIsLoadedData = true;
                tSoundInfoList = HistoryManage.getInstance(getActivity()).getSoundInfoList();
                mAdapter = new SoundInforHistoryNewAdapter(getActivity(), tSoundInfoList);
                mListView.setAdapter(mAdapter);
                if (mFirstVisiblePosition > 0)
                {
                    mListView.setSelection(mFirstVisiblePosition);
                }
                showEmptyView();
                return;
            }
            showEmptyView();
            if (tSoundInfoList != null)
            {
                if (mAdapter == null)
                {
                    mAdapter = new SoundInforHistoryNewAdapter(getActivity(), tSoundInfoList);
                    mListView.setAdapter(mAdapter);
                    return;
                } else
                {
                    mAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    private void showEmptyView()
    {
        if (tSoundInfoList.size() == 0)
        {
            mEmptyView.setVisibility(0);
            headView.setVisibility(8);
            return;
        } else
        {
            mEmptyView.setVisibility(8);
            headView.setVisibility(0);
            return;
        }
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(this);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initData();
        initListener();
        registerListener();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mExitWhenPlay = bundle.getBoolean("exit_when_play");
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300be, viewgroup, false);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        HistoryManage.getInstance(getActivity()).removeHistoryUpdateListener(historyUpdateListener);
        unRegisterListener();
        super.onDestroyView();
        if (mListView != null)
        {
            mFirstVisiblePosition = mListView.getFirstVisiblePosition();
        }
    }

    public void onPlayCanceled()
    {
    }

    public void onResume()
    {
        super.onResume();
        if (UserInfoMannage.getInstance().getUser() != null && !UserInfoMannage.getInstance().getUser().equals(user))
        {
            HistoryManage.getInstance(getActivity()).updateHistoryList();
            user = UserInfoMannage.getInstance().getUser();
        }
    }

    public void onSoundChanged(int i)
    {
        if (mAdapter != null)
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (i < 0 || soundinfo == null || i >= tSoundInfoList.size() || ((SoundInfo)tSoundInfoList.get(i)).trackId != soundinfo.trackId || mAdapter == null)
        {
            return;
        } else
        {
            SoundInfo soundinfo1 = (SoundInfo)tSoundInfoList.get(i);
            soundinfo1.is_favorited = soundinfo.is_favorited;
            soundinfo1.favorites_counts = soundinfo.favorites_counts;
            mAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void setOnFinishCallback(com.ximalaya.ting.android.view.SlideRightOutView.OnFinishListener onfinishlistener)
    {
        mOnFinishCallback = onfinishlistener;
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        setData();
    }










    private class _cls1
        implements android.view.View.OnClickListener
    {

        final HistoryFragment this$0;

        public void onClick(View view)
        {
            headView.performClick();
        }

        _cls1()
        {
            this$0 = HistoryFragment.this;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.modelmanage.HistoryManage.HistoryUpdateListener
    {

        final HistoryFragment this$0;

        public void update()
        {
            if (mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
                mListView.setSelection(0);
                showEmptyView();
            }
        }

        _cls2()
        {
            this$0 = HistoryFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final HistoryFragment this$0;

        public void onClick(View view)
        {
            if (tSoundInfoList.size() == 0)
            {
                return;
            } else
            {
                class _cls1
                    implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                {

                    final _cls3 this$1;

                    public void onExecute()
                    {
                        HistoryManage.getInstance(getActivity()).deleteAllSound();
                    }

                _cls1()
                {
                    this$1 = _cls3.this;
                    super();
                }
                }

                (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u6E05\u7A7A\u64AD\u653E\u5386\u53F2\uFF1F").setOkBtn(new _cls1()).showConfirm();
                return;
            }
        }

        _cls3()
        {
            this$0 = HistoryFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final HistoryFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= mAdapter.getCount())
            {
                return false;
            }
            if (mAdapter.isSound(i))
            {
                mAdapter.handleItemLongClick((Likeable)mAdapter.getData().get(i), view);
            } else
            {
                mAdapter.delItem(i);
            }
            return true;
        }

        _cls4()
        {
            this$0 = HistoryFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AdapterView.OnItemClickListener
    {

        final HistoryFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            boolean flag = true;
            boolean flag3 = false;
            boolean flag2 = false;
            if (tSoundInfoList != null && tSoundInfoList.size() != 0)
            {
                if ((i -= mListView.getHeaderViewsCount()) >= 0 && tSoundInfoList.size() > i)
                {
                    adapterview = (SoundInfo)tSoundInfoList.get(i);
                    if (((SoundInfo) (adapterview)).albumId == -1L)
                    {
                        adapterview.albumId = adapterview.getRealAlubmId();
                    }
                    if (DownloadHandler.getInstance(
// JavaClassFileOutputException: get_constant: invalid tag

        _cls5()
        {
            this$0 = HistoryFragment.this;
            super();
        }
    }


    private class _cls6 extends OnPlayerStatusUpdateListenerProxy
    {

        final HistoryFragment this$0;

        public void onPlayStateChange()
        {
            if (mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        _cls6()
        {
            this$0 = HistoryFragment.this;
            super();
        }
    }

}
