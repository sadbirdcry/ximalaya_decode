// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.widget.LinearLayout;
import com.ximalaya.ting.android.adapter.feed.FeedCollectAlbumListAdapter;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting:
//            CollectFragment

class this._cls0 extends MyAsyncTask
{

    final CollectFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        return AlbumModelManage.getInstance().getLocalCollectAlbumList();
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(List list)
    {
        if (!isAdded())
        {
            return;
        }
        if (list != null && list.size() > 0)
        {
            if (CollectFragment.access$1000(CollectFragment.this) == null)
            {
                CollectFragment.access$1002(CollectFragment.this, new ArrayList(list));
            } else
            {
                CollectFragment.access$1000(CollectFragment.this).clear();
                CollectFragment.access$1000(CollectFragment.this).addAll(list);
            }
        }
        CollectFragment.access$400(CollectFragment.this).onRefreshComplete();
        if (CollectFragment.access$1000(CollectFragment.this) != null && CollectFragment.access$1000(CollectFragment.this).size() > 0)
        {
            CollectFragment.access$1100(CollectFragment.this).notifyDataSetChanged();
            CollectFragment.access$1400(CollectFragment.this).setVisibility(8);
            return;
        } else
        {
            CollectFragment.access$1400(CollectFragment.this).setVisibility(0);
            return;
        }
    }

    protected void onPreExecute()
    {
        CollectFragment.access$1300(CollectFragment.this, false);
    }

    tAdapter()
    {
        this$0 = CollectFragment.this;
        super();
    }
}
