// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.app.Dialog;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Toast;
import com.ximalaya.ting.android.model.feed2.FeedRecommendModel;
import com.ximalaya.ting.android.model.sound.SoundInfoList;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting:
//            FeedNoLoginRecommendFragment

class frm extends MyAsyncTask
{

    FeedRecommendModel frm;
    View fromBindView;
    final FeedNoLoginRecommendFragment this$0;

    protected transient SoundInfoList doInBackground(Void avoid[])
    {
        return CommonRequest.getAlbumList(mCon, frm.trackId, frm.albumId, frm.uid, frm.albumTitle, frm.albumCover, frm.recSrc, frm.recTrack, fromBindView, null);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(SoundInfoList soundinfolist)
    {
        if (loadingPopupWindow != null)
        {
            loadingPopupWindow.dismiss();
        }
        if (!canGoon())
        {
            return;
        }
        if (soundinfolist != null)
        {
            if (soundinfolist.ret == 0)
            {
                PlayTools.gotoPlay(30, soundinfolist.data, FeedNoLoginRecommendFragment.access$500(FeedNoLoginRecommendFragment.this, soundinfolist, frm.trackId), getActivity(), DataCollectUtil.getDataFromView(fromBindView));
                return;
            } else
            {
                Toast.makeText(mCon, soundinfolist.msg, 0).show();
                return;
            }
        } else
        {
            Toast.makeText(mCon, "\u7F51\u7EDC\u8BF7\u6C42\u9519\u8BEF\uFF0C\u8BF7\u91CD\u8BD5", 0).show();
            return;
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((SoundInfoList)obj);
    }

    protected void onPreExecute()
    {
        if (loadingDialog == null)
        {
            FeedNoLoginRecommendFragment.access$400(FeedNoLoginRecommendFragment.this);
        }
        loadingDialog.show();
    }

    public this._cls0 setModel(FeedRecommendModel feedrecommendmodel, View view)
    {
        frm = feedrecommendmodel;
        fromBindView = view;
        return this;
    }

    ()
    {
        this$0 = FeedNoLoginRecommendFragment.this;
        super();
        frm = null;
    }
}
