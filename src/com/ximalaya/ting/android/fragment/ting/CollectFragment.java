// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.feed.FeedCollectAlbumListAdapter;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.List;

public class CollectFragment extends BaseActivityLikeFragment
{
    private static final class FooterView extends Enum
    {

        private static final FooterView $VALUES[];
        public static final FooterView HIDE_ALL;
        public static final FooterView LOADING;
        public static final FooterView MORE;
        public static final FooterView NO_CONNECTION;
        public static final FooterView NO_DATA;

        public static FooterView valueOf(String s)
        {
            return (FooterView)Enum.valueOf(com/ximalaya/ting/android/fragment/ting/CollectFragment$FooterView, s);
        }

        public static FooterView[] values()
        {
            return (FooterView[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterView("MORE", 0);
            LOADING = new FooterView("LOADING", 1);
            NO_CONNECTION = new FooterView("NO_CONNECTION", 2);
            HIDE_ALL = new FooterView("HIDE_ALL", 3);
            NO_DATA = new FooterView("NO_DATA", 4);
            $VALUES = (new FooterView[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA
            });
        }

        private FooterView(String s, int i)
        {
            super(s, i);
        }
    }


    public static final String FROM = "from";
    public static final int FROM_FEED = 1;
    public static boolean isNeedRefresh = false;
    private boolean loadingNextPage;
    private MyAsyncTask mDataLoadTask;
    private LinearLayout mEmptyView;
    private RelativeLayout mFooterViewLoading;
    private int mFrom;
    private PullToRefreshListView mListView;
    private View mNoNetworkLayout;
    private FeedCollectAlbumListAdapter mOtherAlbumAdapter;
    private List otherAlbumdataList;
    private int pageId;
    private int pageSize;
    private int totalCount;

    public CollectFragment()
    {
        loadingNextPage = false;
        pageId = 1;
        pageSize = 30;
        totalCount = 0;
    }

    private void initData()
    {
        mEmptyView = (LinearLayout)LayoutInflater.from(mActivity).inflate(0x7f03007c, mListView, false);
        mEmptyView.setVisibility(8);
        ((Button)mEmptyView.findViewById(0x7f0a0232)).setOnClickListener(new _cls1());
        android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, -1);
        layoutparams.addRule(3, 0x7f0a0066);
        ((ViewGroup)fragmentBaseContainerView).addView(mEmptyView, layoutparams);
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(getActivity()).inflate(0x7f0301fb, null);
        mListView.addFooterView(mFooterViewLoading);
        otherAlbumdataList = new ArrayList();
        mOtherAlbumAdapter = new FeedCollectAlbumListAdapter(getActivity(), otherAlbumdataList, this);
        Log.d("CollectAlbum", "Init album data");
        mListView.setAdapter(mOtherAlbumAdapter);
        showFooterView(FooterView.HIDE_ALL);
    }

    private void initListener()
    {
        mListView.setOnRefreshListener(new _cls2());
        mFooterViewLoading.setOnClickListener(new _cls3());
        mListView.setOnScrollListener(new _cls4());
        mListView.setOnItemClickListener(new _cls5());
        mNoNetworkLayout.setOnClickListener(new _cls6());
    }

    private void loadDataListData(final View view)
    {
        if (!UserInfoMannage.hasLogined())
        {
            break MISSING_BLOCK_LABEL_62;
        }
        if (!ToolUtil.isConnectToNetwork(getActivity())) goto _L2; else goto _L1
_L1:
        mDataLoadTask = (new _cls7()).myexec(new Void[0]);
_L4:
        return;
_L2:
        mListView.onRefreshComplete();
        if (otherAlbumdataList.size() != 0) goto _L4; else goto _L3
_L3:
        showNoNetworkLayout(true);
        return;
        (new _cls8()).myexec(new Void[0]);
        return;
    }

    private boolean moreSoundDataAvailable()
    {
        return pageId * pageSize < totalCount;
    }

    private void showNoNetworkLayout(boolean flag)
    {
        View view = mNoNetworkLayout;
        int i;
        if (flag)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        view.setVisibility(i);
    }

    public void loadData()
    {
        if (!loadingNextPage && otherAlbumdataList.size() == 0 || isNeedRefresh)
        {
            Log.d("CollectAlbum", "Load data");
            isNeedRefresh = false;
            pageId = 1;
            if (otherAlbumdataList.size() > 0)
            {
                mListView.setSelection(0);
            }
            mListView.toRefreshing();
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        setTitleText("\u6536\u85CF\u7684\u4E13\u8F91");
        mListView = (PullToRefreshListView)fragmentBaseContainerView.findViewById(0x7f0a005c);
        initData();
        initListener();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mFrom = bundle.getInt("from");
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300a6, viewgroup, false);
        mNoNetworkLayout = fragmentBaseContainerView.findViewById(0x7f0a02d6);
        return fragmentBaseContainerView;
    }

    public void onRefresh()
    {
        if (!loadingNextPage)
        {
            mListView.setSelection(0);
            mListView.toRefreshing();
        }
    }

    public void onResume()
    {
        super.onResume();
        setPlayPath("play_Subscribe");
        loadData();
    }

    public void showEmptyView(boolean flag)
    {
        if (mEmptyView != null)
        {
            LinearLayout linearlayout = mEmptyView;
            int i;
            if (flag)
            {
                i = 0;
            } else
            {
                i = 8;
            }
            linearlayout.setVisibility(i);
        }
    }

    public void showFooterView(FooterView footerview)
    {
        mListView.setFooterDividersEnabled(false);
        mFooterViewLoading.setVisibility(0);
        if (footerview == FooterView.MORE)
        {
            mFooterViewLoading.setClickable(true);
            mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
            ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
            mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
        } else
        {
            if (footerview == FooterView.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerview == FooterView.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == FooterView.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerview == FooterView.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }

    public void showLoading(boolean flag)
    {
label0:
        {
            if (canGoon())
            {
                if (!flag)
                {
                    break label0;
                }
                findViewById(0x7f0a0091).setVisibility(0);
            }
            return;
        }
        findViewById(0x7f0a0091).setVisibility(8);
    }






/*
    static List access$1002(CollectFragment collectfragment, List list)
    {
        collectfragment.otherAlbumdataList = list;
        return list;
    }

*/









/*
    static boolean access$202(CollectFragment collectfragment, boolean flag)
    {
        collectfragment.loadingNextPage = flag;
        return flag;
    }

*/



/*
    static int access$302(CollectFragment collectfragment, int i)
    {
        collectfragment.pageId = i;
        return i;
    }

*/


/*
    static int access$308(CollectFragment collectfragment)
    {
        int i = collectfragment.pageId;
        collectfragment.pageId = i + 1;
        return i;
    }

*/







/*
    static int access$802(CollectFragment collectfragment, int i)
    {
        collectfragment.totalCount = i;
        return i;
    }

*/



    private class _cls2
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final CollectFragment this$0;

        public void onRefresh()
        {
            if (!loadingNextPage)
            {
                pageId = 1;
                loadDataListData(mListView);
            }
        }

        _cls2()
        {
            this$0 = CollectFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final CollectFragment this$0;

        public void onClick(View view)
        {
            if (!loadingNextPage)
            {
                showFooterView(FooterView.LOADING);
                loadDataListData(mFooterViewLoading);
            }
        }

        _cls3()
        {
            this$0 = CollectFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AbsListView.OnScrollListener
    {

        final CollectFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            mListView.onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0)
            {
                i = abslistview.getCount();
                if (i > 5)
                {
                    i -= 5;
                } else
                {
                    i--;
                }
                if (abslistview.getLastVisiblePosition() > i && (pageId - 1) * pageSize < totalCount && (mDataLoadTask == null || mDataLoadTask.getStatus() != android.os.AsyncTask.Status.RUNNING) && !loadingNextPage && UserInfoMannage.hasLogined())
                {
                    showFooterView(FooterView.LOADING);
                    loadDataListData(mListView);
                }
            }
        }

        _cls4()
        {
            this$0 = CollectFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CollectFragment this$0;

        public void onItemClick(AdapterView adapterview, final View arg1, int i, long l)
        {
            if (otherAlbumdataList == null || otherAlbumdataList.size() == 0)
            {
                return;
            }
            Object obj = arg1.findViewById(0x7f0a0156);
            adapterview = (AlbumModel)otherAlbumdataList.get(i - mListView.getHeaderViewsCount());
            if (((View) (obj)).getVisibility() == 0)
            {
                ((View) (obj)).setVisibility(8);
                adapterview.hasNew = false;
                class _cls1 extends MyAsyncTask
                {

                    final _cls5 this$1;
                    final View val$arg1;

                    protected volatile Object doInBackground(Object aobj[])
                    {
                        return doInBackground((Long[])aobj);
                    }

                    protected transient String doInBackground(Long along[])
                    {
                        long l1 = along[0].longValue();
                        along = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/subscribe/check").toString();
                        RequestParams requestparams = new RequestParams();
                        requestparams.put("albumId", (new StringBuilder()).append(l1).append("").toString());
                        return f.a().b(along, requestparams, arg1, null);
                    }

                    protected volatile void onPostExecute(Object obj1)
                    {
                        onPostExecute((String)obj1);
                    }

                    protected void onPostExecute(String s)
                    {
                        if (!Utilities.isBlank(s)) goto _L2; else goto _L1
_L1:
                        return;
_L2:
                        if (JSON.parseObject(s).getIntValue("ret") != 0) goto _L1; else goto _L3
_L3:
                        mOtherAlbumAdapter.notifyDataSetChanged();
                        return;
                        s;
                        s.printStackTrace();
                        return;
                    }

                _cls1()
                {
                    this$1 = _cls5.this;
                    arg1 = view;
                    super();
                }
                }

                (new _cls1()).myexec(new Long[] {
                    Long.valueOf(((AlbumModel) (adapterview)).albumId)
                });
            }
            obj = new Bundle();
            ((Bundle) (obj)).putString("album", JSON.toJSONString(adapterview));
            if (mFrom == 1)
            {
                i = 9;
            } else
            {
                i = 1;
            }
            ((Bundle) (obj)).putInt("from", i);
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(arg1));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj)));
        }

        _cls5()
        {
            this$0 = CollectFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final CollectFragment this$0;

        public void onClick(View view)
        {
            loadData();
        }

        _cls6()
        {
            this$0 = CollectFragment.this;
            super();
        }
    }


    private class _cls7 extends MyAsyncTask
    {

        final CollectFragment this$0;
        final View val$view;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/subscribe/list").toString();
            Object obj = new RequestParams();
            ((RequestParams) (obj)).put("pageId", (new StringBuilder()).append(pageId).append("").toString());
            ((RequestParams) (obj)).put("pageSize", (new StringBuilder()).append(pageSize).append("").toString());
            obj = f.a().a(avoid, ((RequestParams) (obj)), view, mListView);
            Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
            avoid = null;
            try
            {
                obj = JSON.parseObject(((String) (obj)));
                String s = ((JSONObject) (obj)).get("ret").toString();
                totalCount = ((JSONObject) (obj)).getIntValue("totalCount");
                if ("0".equals(s))
                {
                    avoid = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/album/AlbumModel);
                }
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
                return null;
            }
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(final List result)
        {
            if (!canGoon())
            {
                return;
            }
            mListView.onRefreshComplete();
            loadingNextPage = false;
            if (result == null)
            {
                Log.d("CollectAlbum", "loadDataListData");
                if (otherAlbumdataList.size() > 0)
                {
                    showFooterView(FooterView.NO_DATA);
                }
                if (otherAlbumdataList.size() == 0)
                {
                    showNoNetworkLayout(true);
                    return;
                } else
                {
                    mEmptyView.findViewById(0x7f0a011a).setVisibility(8);
                    mEmptyView.findViewById(0x7f0a0231).setVisibility(8);
                    mEmptyView.findViewById(0x7f0a0232).setVisibility(8);
                    mEmptyView.setVisibility(8);
                    return;
                }
            } else
            {
                class _cls1
                    implements MyCallback
                {

                    final _cls7 this$1;
                    final List val$result;

                    public void execute()
                    {
                        if (pageId == 1)
                        {
                            otherAlbumdataList.clear();
                            otherAlbumdataList.addAll(result);
                            mOtherAlbumAdapter.notifyDataSetChanged();
                        } else
                        {
                            otherAlbumdataList.addAll(result);
                            mOtherAlbumAdapter.notifyDataSetChanged();
                        }
                        if (moreSoundDataAvailable())
                        {
                            showFooterView(FooterView.MORE);
                        } else
                        {
                            showFooterView(FooterView.HIDE_ALL);
                        }
                        if (otherAlbumdataList.size() == 0)
                        {
                            mEmptyView.setVisibility(0);
                        } else
                        {
                            mEmptyView.setVisibility(8);
                        }
                        int i = 
// JavaClassFileOutputException: get_constant: invalid tag

                _cls1()
                {
                    this$1 = _cls7.this;
                    result = list;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                return;
            }
        }

        protected void onPreExecute()
        {
            loadingNextPage = true;
            showNoNetworkLayout(false);
        }

        _cls7()
        {
            this$0 = CollectFragment.this;
            view = view1;
            super();
        }
    }


    private class _cls8 extends MyAsyncTask
    {

        final CollectFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            return AlbumModelManage.getInstance().getLocalCollectAlbumList();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            if (!isAdded())
            {
                return;
            }
            if (list != null && list.size() > 0)
            {
                if (otherAlbumdataList == null)
                {
                    otherAlbumdataList = new ArrayList(list);
                } else
                {
                    otherAlbumdataList.clear();
                    otherAlbumdataList.addAll(list);
                }
            }
            mListView.onRefreshComplete();
            if (otherAlbumdataList != null && otherAlbumdataList.size() > 0)
            {
                mOtherAlbumAdapter.notifyDataSetChanged();
                mEmptyView.setVisibility(8);
                return;
            } else
            {
                mEmptyView.setVisibility(0);
                return;
            }
        }

        protected void onPreExecute()
        {
            showNoNetworkLayout(false);
        }

        _cls8()
        {
            this$0 = CollectFragment.this;
            super();
        }
    }

}
