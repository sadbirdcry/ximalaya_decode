// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.model.ad.FeedAd2;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting.feed:
//            FeedCollectFragment

class this._cls0
    implements android.widget.kListener
{

    final FeedCollectFragment this$0;

    public void onItemClick(AdapterView adapterview, final View arg1, int i, long l)
    {
        if (OneClickHelper.getInstance().onClick(arg1) && FeedCollectFragment.access$800(FeedCollectFragment.this) != null && FeedCollectFragment.access$800(FeedCollectFragment.this).size() != 0 && FeedCollectFragment.access$800(FeedCollectFragment.this).size() > i - mListView.getHeaderViewsCount())
        {
            Object obj = arg1.findViewById(0x7f0a0156);
            adapterview = (AlbumModel)FeedCollectFragment.access$800(FeedCollectFragment.this).get(i - mListView.getHeaderViewsCount());
            if (((AlbumModel) (adapterview)).type == 1)
            {
                FeedAd2.handleClick(FeedCollectFragment.this, (FeedAd2)((AlbumModel) (adapterview)).tag, "feed_collect_list");
                return;
            }
            if (!((AlbumModel) (adapterview)).isRecommendTitle)
            {
                byte byte0;
                if (((AlbumModel) (adapterview)).isRecommend)
                {
                    ToolUtil.onEvent(getActivity(), "Collection_Recommend");
                } else
                {
                    ToolUtil.onEvent(getActivity(), "Collection_Attention");
                }
                if (((View) (obj)).getVisibility() == 0)
                {
                    ((View) (obj)).setVisibility(8);
                    adapterview.hasNew = false;
                    class _cls1 extends MyAsyncTask
                    {

                        final FeedCollectFragment._cls5 this$1;
                        final View val$arg1;

                        protected volatile Object doInBackground(Object aobj[])
                        {
                            return doInBackground((Long[])aobj);
                        }

                        protected transient String doInBackground(Long along[])
                        {
                            long l1 = along[0].longValue();
                            along = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/subscribe/check").toString();
                            RequestParams requestparams = new RequestParams();
                            requestparams.put("albumId", (new StringBuilder()).append(l1).append("").toString());
                            return f.a().b(along, requestparams, arg1, null);
                        }

                        protected volatile void onPostExecute(Object obj1)
                        {
                            onPostExecute((String)obj1);
                        }

                        protected void onPostExecute(String s)
                        {
                            if (!Utilities.isBlank(s)) goto _L2; else goto _L1
_L1:
                            return;
_L2:
                            if (JSON.parseObject(s).getIntValue("ret") != 0) goto _L1; else goto _L3
_L3:
                            FeedCollectFragment.access$900(this$0).notifyDataSetChanged();
                            return;
                            s;
                            s.printStackTrace();
                            return;
                        }

            _cls1()
            {
                this$1 = FeedCollectFragment._cls5.this;
                arg1 = view;
                super();
            }
                    }

                    (new _cls1()).myexec(new Long[] {
                        Long.valueOf(((AlbumModel) (adapterview)).albumId)
                    });
                }
                obj = new Bundle();
                ((Bundle) (obj)).putString("album", JSON.toJSONString(adapterview));
                ((Bundle) (obj)).putString("rec_src", ((AlbumModel) (adapterview)).recSrc);
                ((Bundle) (obj)).putString("rec_track", ((AlbumModel) (adapterview)).recTrack);
                if (((AlbumModel) (adapterview)).isRecommend)
                {
                    byte0 = 11;
                } else
                {
                    byte0 = 1;
                }
                ((Bundle) (obj)).putInt("from", byte0);
                ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(arg1, "\u63A8\u8350\u6536\u85CF", (new StringBuilder()).append("").append(i - mListView.getHeaderViewsCount()).toString()));
                startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj)));
                return;
            }
        }
    }

    _cls1()
    {
        this$0 = FeedCollectFragment.this;
        super();
    }
}
