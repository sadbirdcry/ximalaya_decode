// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import android.app.Dialog;
import android.view.View;
import android.widget.PopupWindow;
import com.ximalaya.ting.android.model.feed2.FeedModel2;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoList;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting.feed:
//            FeedFragmentNew

class data extends MyAsyncTask
{

    FeedModel2 data;
    View fromBindView;
    final FeedFragmentNew this$0;

    protected transient SoundInfoList doInBackground(Void avoid[])
    {
        avoid = CommonRequest.getAlbumList(mCon, data.trackId, data.albumId, data.uid, data.albumTitle, data.albumCover, data.recSrc, data.recTrack, fromBindView, null);
        if (avoid != null && ((SoundInfoList) (avoid)).data != null && data != null)
        {
            int j = ((SoundInfoList) (avoid)).data.size();
            for (int i = 0; i != j; i++)
            {
                ((SoundInfo)((SoundInfoList) (avoid)).data.get(i)).recSrc = data.recSrc;
                ((SoundInfo)((SoundInfoList) (avoid)).data.get(i)).recTrack = data.recTrack;
            }

        }
        return avoid;
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(SoundInfoList soundinfolist)
    {
        if (loadingPopupWindow != null)
        {
            loadingPopupWindow.dismiss();
        }
        if (!canGoon())
        {
            return;
        }
        if (soundinfolist != null)
        {
            if (soundinfolist.ret == 0 && soundinfolist.data != null)
            {
                PlayTools.gotoPlay(30, soundinfolist.data, FeedFragmentNew.access$2100(FeedFragmentNew.this, soundinfolist, data.trackId), getActivity(), DataCollectUtil.getDataFromView(fromBindView));
                return;
            } else
            {
                showToast(soundinfolist.msg);
                return;
            }
        } else
        {
            showToast("\u7F51\u7EDC\u8BF7\u6C42\u9519\u8BEF\uFF0C\u8BF7\u91CD\u8BD5");
            return;
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((SoundInfoList)obj);
    }

    protected void onPreExecute()
    {
        if (loadingDialog == null)
        {
            FeedFragmentNew.access$1600(FeedFragmentNew.this);
        }
        loadingDialog.show();
    }

    public this._cls0 setModel(FeedModel2 feedmodel2, View view)
    {
        data = feedmodel2;
        fromBindView = view;
        return this;
    }

    ()
    {
        this$0 = FeedFragmentNew.this;
        super();
        data = null;
    }
}
