// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import android.app.Dialog;
import android.view.View;
import android.widget.PopupWindow;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.feed.FeedAdapter2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.feed2.FeedModel2;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting.feed:
//            FeedFragmentNew

class val.position extends MyAsyncTask
{

    final FeedFragmentNew this$0;
    final long val$albumId;
    final boolean val$currentIsTop;
    final View val$fromBindView;
    final int val$position;
    final View val$toBindView;
    final long val$uId;

    protected transient Boolean doInBackground(Void avoid[])
    {
        RequestParams requestparams;
        if (val$currentIsTop)
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/api/1/feed/top/delete").toString();
        } else
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/api/1/feed/top/create").toString();
        }
        requestparams = new RequestParams();
        requestparams.put("albumUid", String.valueOf(val$uId));
        requestparams.put("albumId", String.valueOf(val$albumId));
        avoid = (BaseModel)JSON.parseObject(f.a().b(avoid, requestparams, val$fromBindView, val$toBindView), com/ximalaya/ting/android/model/BaseModel);
        if (avoid == null)
        {
            break MISSING_BLOCK_LABEL_226;
        }
        if (((BaseModel) (avoid)).ret != 0)
        {
            break MISSING_BLOCK_LABEL_226;
        }
        avoid = (FeedModel2)FeedFragmentNew.access$1700(FeedFragmentNew.this).get(val$position);
        if (!val$currentIsTop) goto _L2; else goto _L1
_L1:
        avoid.isTop = false;
_L4:
        return Boolean.valueOf(true);
_L2:
        avoid.isTop = true;
        FeedFragmentNew.access$1700(FeedFragmentNew.this).remove(avoid);
        FeedFragmentNew.access$1700(FeedFragmentNew.this).add(0, avoid);
        if (true) goto _L4; else goto _L3
_L3:
        avoid;
        if (loadingPopupWindow != null)
        {
            loadingPopupWindow.dismiss();
        }
        Logger.e("FeedFragement", "\u7F6E\u9876/\u53D6\u6D88\u7F6E\u9876\u52A8\u6001\u51FA\u9519", avoid);
        return Boolean.valueOf(false);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(Boolean boolean1)
    {
        if (loadingPopupWindow != null)
        {
            loadingPopupWindow.dismiss();
        }
        if (boolean1 != null && boolean1.booleanValue())
        {
            FeedFragmentNew feedfragmentnew = FeedFragmentNew.this;
            StringBuilder stringbuilder = new StringBuilder();
            if (val$currentIsTop)
            {
                boolean1 = "\u53D6\u6D88";
            } else
            {
                boolean1 = "";
            }
            feedfragmentnew.showToast(stringbuilder.append(boolean1).append("\u7F6E\u9876\u6210\u529F").toString());
            FeedFragmentNew.access$200(FeedFragmentNew.this).notifyDataSetChanged();
            return;
        }
        FeedFragmentNew feedfragmentnew1 = FeedFragmentNew.this;
        StringBuilder stringbuilder1 = new StringBuilder();
        if (val$currentIsTop)
        {
            boolean1 = "\u53D6\u6D88";
        } else
        {
            boolean1 = "";
        }
        feedfragmentnew1.showToast(stringbuilder1.append(boolean1).append("\u7F6E\u9876\u5931\u8D25").toString());
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Boolean)obj);
    }

    protected void onPreExecute()
    {
        if (loadingDialog == null)
        {
            FeedFragmentNew.access$1600(FeedFragmentNew.this);
        }
        loadingDialog.show();
    }

    ()
    {
        this$0 = final_feedfragmentnew;
        val$currentIsTop = flag;
        val$uId = l;
        val$albumId = l1;
        val$fromBindView = view;
        val$toBindView = view1;
        val$position = I.this;
        super();
    }
}
