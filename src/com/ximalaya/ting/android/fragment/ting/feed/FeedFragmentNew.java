// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.adapter.feed.FeedAdapter2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.userspace.NewThingFragment;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.NoReadModel;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.model.ad.BannerAdController;
import com.ximalaya.ting.android.model.ad.FeedAd2;
import com.ximalaya.ting.android.model.ad.IThirdAd;
import com.ximalaya.ting.android.model.feed2.FeedModel2;
import com.ximalaya.ting.android.model.feed2.FeedRecommendModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoList;
import com.ximalaya.ting.android.modelmanage.AdManager;
import com.ximalaya.ting.android.modelmanage.NoReadManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FeedFragmentNew extends BaseListFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, com.ximalaya.ting.android.adapter.feed.FeedAdapter2.ActionListener
{
    class GetAlbumListTask extends MyAsyncTask
    {

        FeedModel2 data;
        View fromBindView;
        final FeedFragmentNew this$0;

        protected transient SoundInfoList doInBackground(Void avoid[])
        {
            avoid = CommonRequest.getAlbumList(mCon, data.trackId, data.albumId, data.uid, data.albumTitle, data.albumCover, data.recSrc, data.recTrack, fromBindView, null);
            if (avoid != null && ((SoundInfoList) (avoid)).data != null && data != null)
            {
                int j = ((SoundInfoList) (avoid)).data.size();
                for (int i = 0; i != j; i++)
                {
                    ((SoundInfo)((SoundInfoList) (avoid)).data.get(i)).recSrc = data.recSrc;
                    ((SoundInfo)((SoundInfoList) (avoid)).data.get(i)).recTrack = data.recTrack;
                }

            }
            return avoid;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(SoundInfoList soundinfolist)
        {
            if (loadingPopupWindow != null)
            {
                loadingPopupWindow.dismiss();
            }
            if (!canGoon())
            {
                return;
            }
            if (soundinfolist != null)
            {
                if (soundinfolist.ret == 0 && soundinfolist.data != null)
                {
                    PlayTools.gotoPlay(30, soundinfolist.data, getSoundIndex(soundinfolist, data.trackId), getActivity(), DataCollectUtil.getDataFromView(fromBindView));
                    return;
                } else
                {
                    showToast(soundinfolist.msg);
                    return;
                }
            } else
            {
                showToast("\u7F51\u7EDC\u8BF7\u6C42\u9519\u8BEF\uFF0C\u8BF7\u91CD\u8BD5");
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((SoundInfoList)obj);
        }

        protected void onPreExecute()
        {
            if (loadingDialog == null)
            {
                createDimLoadingDialog();
            }
            loadingDialog.show();
        }

        public GetAlbumListTask setModel(FeedModel2 feedmodel2, View view)
        {
            data = feedmodel2;
            fromBindView = view;
            return this;
        }

        GetAlbumListTask()
        {
            this$0 = FeedFragmentNew.this;
            super();
            data = null;
        }
    }


    private int SHOW_RECOMMEND_COUNT;
    private int feedPageId;
    private int feedPageSize;
    private int feedTotalCount;
    private boolean ignoreNewThingItem;
    Dialog loadingDialog;
    PopupWindow loadingPopupWindow;
    private BannerAdController mBannerAdController;
    private List mDataList;
    private MenuDialog mFeedActionDlg;
    private FeedAdapter2 mFeedAdapter;
    private List mFeedAds;
    private int mFirstVisiblePosition;
    private Handler mHandler;
    private boolean mHasMore;
    private View mHeaderAdsView;
    private boolean mIsFistResumed;
    private boolean mIsLoadedData;
    private boolean mIsLoading;
    private boolean mIsloadingAd;
    private boolean mPulldownRefresh;
    private View mReloadDataView;
    private IThirdAd mThirdBannerAd;
    private LoginInfoModel user;

    public FeedFragmentNew()
    {
        mIsLoading = false;
        mHasMore = true;
        mFeedAds = new ArrayList();
        ignoreNewThingItem = false;
        SHOW_RECOMMEND_COUNT = 10;
        mIsLoadedData = false;
        mIsFistResumed = false;
        mDataList = new ArrayList();
        feedTotalCount = 0;
        feedPageId = 1;
        feedPageSize = 30;
        mPulldownRefresh = false;
        mIsloadingAd = false;
    }

    private void addAd()
    {
        if (a.k)
        {
            mBannerAdController = new BannerAdController(getActivity(), mListView, "banner");
            mBannerAdController.load(this);
            View view = mBannerAdController.getView();
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ((ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 10F) * 2) * 170) / 720));
            mListView.addHeaderView(view);
        }
    }

    private void appendRecommends(List list)
    {
        if (list != null && !list.isEmpty())
        {
            if (!(list = FeedModel2.convert(list)).isEmpty())
            {
                FeedModel2 feedmodel2 = new FeedModel2();
                feedmodel2.isRecommendTitle = true;
                mDataList.add(feedmodel2);
                mDataList.addAll(list);
                mFeedAdapter.notifyDataSetChanged();
                return;
            }
        }
    }

    private void clearFeedDot()
    {
        if (NoReadManage.getInstance() != null)
        {
            if (NoReadManage.getInstance().getNoReadModel() != null)
            {
                NoReadManage.getInstance().getNoReadModel().newEventCountChanged = false;
                NoReadManage.getInstance().getNoReadModel().newFeedCount = 0;
            }
            NoReadManage.getInstance().updateNoReadManageManual();
        }
    }

    private void clearRef()
    {
        mListView.setAdapter(null);
        mListView = null;
        mFeedAdapter = null;
        fragmentBaseContainerView = null;
    }

    private void clickAd(FeedModel2 feedmodel2)
    {
        if (feedmodel2.type == 1 && feedmodel2.clickType == 1)
        {
            Object obj = new AdCollectData();
            String s = AdManager.getInstance().getAdIdFromUrl(feedmodel2.link);
            ((AdCollectData) (obj)).setAdItemId(s);
            ((AdCollectData) (obj)).setAdSource("0");
            ((AdCollectData) (obj)).setAndroidId(ToolUtil.getAndroidId(getActivity().getApplicationContext()));
            ((AdCollectData) (obj)).setLogType("tingClick");
            ((AdCollectData) (obj)).setPositionName("feed_follow");
            ((AdCollectData) (obj)).setResponseId(s);
            ((AdCollectData) (obj)).setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
            ((AdCollectData) (obj)).setTrackId("-1");
            obj = AdManager.getInstance().getAdRealJTUrl(feedmodel2.link, ((AdCollectData) (obj)));
            if (feedmodel2.openlinkType == 0)
            {
                ThirdAdStatUtil.getInstance().execAfterDecorateUrl(((String) (obj)), new _cls3());
            } else
            if (feedmodel2.openlinkType == 1)
            {
                ThirdAdStatUtil.getInstance().execAfterDecorateUrl(((String) (obj)), new _cls4());
                return;
            }
        }
    }

    private FeedModel2 convertAd2FeedModel(FeedAd2 feedad2)
    {
        if (feedad2 == null)
        {
            return null;
        } else
        {
            FeedModel2 feedmodel2 = new FeedModel2();
            feedmodel2.isAd = true;
            feedmodel2.link = feedad2.link;
            feedmodel2.cover = feedad2.cover;
            feedmodel2.type = feedad2.linkType;
            feedmodel2.albumTitle = feedad2.name;
            feedmodel2.trackTitle = feedad2.description;
            feedmodel2.clickType = feedad2.clickType;
            feedmodel2.openlinkType = feedad2.openlinkType;
            return feedmodel2;
        }
    }

    private void createDimLoadingDialog()
    {
        loadingDialog = new Dialog(getActivity(), 0x1030010);
        final View emptyDialog = LayoutInflater.from(getActivity()).inflate(0x7f03006a, null);
        android.view.WindowManager.LayoutParams layoutparams = loadingDialog.getWindow().getAttributes();
        layoutparams.dimAmount = 0.5F;
        loadingDialog.getWindow().setAttributes(layoutparams);
        loadingDialog.getWindow().addFlags(2);
        loadingDialog.setContentView(emptyDialog);
        loadingDialog.setCanceledOnTouchOutside(true);
        loadingDialog.setOnShowListener(new _cls11());
    }

    private void doBlock(final long uId, final long albumId, final int position, final View fromBindView, final View toBindView)
    {
        ToolUtil.onEvent(mCon, "Feed_Set_Blocked");
        (new _cls15()).execute(new Void[0]);
    }

    private void doFollow(final FeedModel2 model, final View view)
    {
        if (!UserInfoMannage.hasLogined())
        {
            return;
        } else
        {
            showLoading(false);
            RequestParams requestparams = new RequestParams();
            requestparams.put("toUid", (new StringBuilder()).append("").append(model.uid).toString());
            requestparams.put("isFollow", "true");
            f.a().b("mobile/follow", requestparams, DataCollectUtil.getDataFromView(view), new _cls18());
            return;
        }
    }

    private void doTop(final boolean currentIsTop, final long uId, final long albumId, final int position, final View fromBindView, 
            final View toBindView)
    {
        if (!currentIsTop)
        {
            ToolUtil.onEvent(mCon, "Feed_Set_Top");
        }
        (new _cls12()).execute(new Void[0]);
    }

    private void doUnFollow(final FeedModel2 model, final View view)
    {
        if (!UserInfoMannage.hasLogined())
        {
            return;
        } else
        {
            showLoading(true);
            RequestParams requestparams = new RequestParams();
            requestparams.put("toUid", (new StringBuilder()).append("").append(model.uid).toString());
            requestparams.put("isFollow", "false");
            f.a().b("mobile/follow", requestparams, DataCollectUtil.getDataFromView(view), new _cls17());
            return;
        }
    }

    private int getSoundIndex(SoundInfoList soundinfolist, long l)
    {
        if (soundinfolist == null)
        {
            return 0;
        }
        int j = soundinfolist.data.size();
        for (int i = 0; i != j; i++)
        {
            if (l == ((SoundInfo)soundinfolist.data.get(i)).trackId)
            {
                return i;
            }
        }

        return soundinfolist.data.size() - 1;
    }

    private List getTempToSave(List list)
    {
        ArrayList arraylist = new ArrayList();
        if (list == null)
        {
            return arraylist;
        }
        list = list.iterator();
        do
        {
            if (!list.hasNext())
            {
                break;
            }
            FeedModel2 feedmodel2 = (FeedModel2)list.next();
            if (!feedmodel2.isAd)
            {
                arraylist.add(feedmodel2);
            }
        } while (true);
        return arraylist;
    }

    private void goToNewThings(FeedModel2 feedmodel2, View view)
    {
        feedmodel2.unreadNum = 0;
        mFeedAdapter.notifyDataSetChanged();
        feedmodel2 = new RequestParams();
        feedmodel2.put("sign", "2");
        feedmodel2.put("size", "20");
        f.a().a("mobile/api/1/feed/event", feedmodel2, DataCollectUtil.getDataFromView(view), new _cls6());
        feedmodel2 = new Bundle();
        feedmodel2.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        if (MyApplication.a() != null)
        {
            ((MainTabActivity2)MyApplication.a()).startFragment(com/ximalaya/ting/android/fragment/userspace/NewThingFragment, feedmodel2);
        }
    }

    private boolean hasMore()
    {
        if (feedPageId != 1) goto _L2; else goto _L1
_L1:
        if (feedPageSize >= feedTotalCount) goto _L4; else goto _L3
_L3:
        return true;
_L4:
        return false;
_L2:
        if ((feedPageId - 1) * feedPageSize >= feedTotalCount)
        {
            return false;
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    private void initData()
    {
        if (getUserVisibleHint() && getView() != null)
        {
            if (!mIsLoadedData)
            {
                mIsLoadedData = true;
                ((PullToRefreshListView)mListView).toRefreshing();
                return;
            }
            if (mDataList != null && !mDataList.isEmpty())
            {
                if (mFeedAdapter == null)
                {
                    mFeedAdapter = new FeedAdapter2(getActivity(), mDataList, this);
                    mListView.setAdapter(mFeedAdapter);
                    if (mFirstVisiblePosition > 0)
                    {
                        mListView.setSelection(mFirstVisiblePosition);
                    }
                } else
                {
                    insertOrReplaceFeedAd(mDataList);
                }
                mFeedAdapter.notifyDataSetChanged();
            } else
            {
                showNoNetworkLayout(true);
            }
            if (mBannerAdController != null)
            {
                mBannerAdController.swapAd();
                return;
            }
        }
    }

    private void initFeedItemClickListener()
    {
        mListView.setOnItemClickListener(new _cls5());
    }

    private void initFeedItemLongClickListener()
    {
        mListView.setOnItemLongClickListener(new _cls7());
    }

    private void initListViewScrollListener()
    {
        mListView.setOnScrollListener(new _cls8());
    }

    private void initListeners()
    {
        initListViewScrollListener();
        initFeedItemClickListener();
        initFeedItemLongClickListener();
        initRefreshListener();
        mReloadDataView.setOnClickListener(new _cls1());
        mFooterViewLoading.setOnClickListener(new _cls2());
    }

    private void initRefreshListener()
    {
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls9());
    }

    private void initUI()
    {
        addAd();
        mFeedAdapter = new FeedAdapter2(getActivity(), mDataList, this);
        mListView.setAdapter(mFeedAdapter);
        mListView.setHeaderDividersEnabled(false);
    }

    private void insertOrReplaceFeedAd(List list)
    {
        while (list == null || list.size() <= 0 || mFeedAds == null || mFeedAds.size() <= 0) 
        {
            return;
        }
        ArrayList arraylist = new ArrayList();
        Iterator iterator = mFeedAds.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj = (FeedAd2)iterator.next();
            if (((FeedAd2) (obj)).position >= 0 && ((FeedAd2) (obj)).position < list.size())
            {
                if (((FeedModel2)list.get(((FeedAd2) (obj)).position)).isAd)
                {
                    list.set(((FeedAd2) (obj)).position, convertAd2FeedModel(((FeedAd2) (obj))));
                } else
                {
                    list.add(((FeedAd2) (obj)).position, convertAd2FeedModel(((FeedAd2) (obj))));
                }
                arraylist.add(obj);
                ThirdAdStatUtil.getInstance().thirdAdStatRequest(((FeedAd2) (obj)).thirdStatUrl);
                obj = AdManager.getInstance().getAdIdFromUrl(((FeedAd2) (obj)).link);
                if (!TextUtils.isEmpty(((CharSequence) (obj))))
                {
                    AdCollectData adcollectdata = new AdCollectData();
                    adcollectdata.setAdItemId(((String) (obj)));
                    adcollectdata.setAdSource("0");
                    adcollectdata.setAndroidId(ToolUtil.getAndroidId(mCon.getApplicationContext()));
                    adcollectdata.setLogType("tingShow");
                    adcollectdata.setPositionName("feed_follow");
                    adcollectdata.setResponseId(((String) (obj)));
                    adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                    adcollectdata.setTrackId("-1");
                    DataCollectUtil.getInstance(mCon.getApplicationContext()).statOnlineAd(adcollectdata);
                }
            }
        } while (true);
        mFeedAds.removeAll(arraylist);
    }

    private void loadFeedData(final boolean isPullUp)
    {
        if (NetworkUtils.isNetworkAvaliable(getActivity())) goto _L2; else goto _L1
_L1:
        ((PullToRefreshListView)mListView).onRefreshComplete();
        showNoNetworkLayout(true);
_L4:
        return;
_L2:
        showNoNetworkLayout(false);
        ignoreNewThingItem = false;
        if (mIsLoading) goto _L4; else goto _L3
_L3:
        RequestParams requestparams;
        int i;
        if (isPullUp)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        }
        mHasMore = true;
        mIsLoading = true;
        requestparams = new RequestParams();
        if (loginInfoModel != null)
        {
            requestparams.put("uid", (new StringBuilder()).append("").append(loginInfoModel.uid).toString());
            requestparams.put("token", loginInfoModel.token);
        }
        requestparams.put("size", feedPageSize);
        i = mDataList.size();
        if (!isPullUp || i <= 1)
        {
            break MISSING_BLOCK_LABEL_224;
        }
        i--;
_L6:
        FeedModel2 feedmodel2;
        if (i < 0)
        {
            break MISSING_BLOCK_LABEL_224;
        }
        feedmodel2 = (FeedModel2)mDataList.get(i);
        if (feedmodel2.dynamicType != 2)
        {
            break; /* Loop/switch isn't completed */
        }
        ignoreNewThingItem = true;
_L8:
        i--;
        if (true) goto _L6; else goto _L5
_L5:
        if (feedmodel2.isAd) goto _L8; else goto _L7
_L7:
        requestparams.put("timeline", feedmodel2.timeline);
        int j;
        if (isPullUp)
        {
            j = 1;
        } else
        {
            j = 2;
        }
        requestparams.put("sign", j);
        f.a().a("mobile/api/1/feed/recommend/dynamic", requestparams, DataCollectUtil.getDataFromView(mListView), new _cls10(), true);
        return;
    }

    public static FeedFragmentNew newInstance()
    {
        return new FeedFragmentNew();
    }

    private void parseFeedData(boolean flag, String s, boolean flag1)
    {
        if (canGoon()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (!TextUtils.isEmpty(s))
        {
            break; /* Loop/switch isn't completed */
        }
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        mHasMore = false;
        if (!flag)
        {
            showNoNetworkLayout(true);
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        Object obj;
        try
        {
            obj = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            obj = null;
        }
        if (obj != null && ((JSONObject) (obj)).getIntValue("ret") == 0)
        {
            break; /* Loop/switch isn't completed */
        }
        mHasMore = false;
        if (!flag)
        {
            showNoNetworkLayout(true);
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
        s = ((JSONObject) (obj)).getString("frDatas");
        try
        {
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/feed2/FeedRecommendModel);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        feedTotalCount = ((JSONObject) (obj)).getIntValue("totalSize");
        obj = ((JSONObject) (obj)).getString("datas");
        if (!flag)
        {
            mDataList.clear();
        }
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            if (!flag)
            {
                appendRecommends(s);
            }
            mHasMore = false;
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            return;
        }
        try
        {
            obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/feed2/FeedModel2);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((Exception) (obj)).printStackTrace();
            obj = null;
        }
        if (obj == null || ((List) (obj)).size() == 0)
        {
            if (!flag)
            {
                appendRecommends(s);
            }
            mHasMore = false;
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            return;
        }
        mHasMore = hasMore();
        if (!flag)
        {
            feedPageId = 1;
            mDataList.clear();
        }
        if (!flag1) goto _L6; else goto _L5
_L5:
        int i;
        int j;
        j = ((List) (obj)).size();
        i = 0;
_L11:
        if (i == j) goto _L6; else goto _L7
_L7:
        if (((FeedModel2)((List) (obj)).get(i)).dynamicType != 2) goto _L9; else goto _L8
_L8:
        ((List) (obj)).remove(i);
_L6:
        insertOrReplaceFeedAd(((List) (obj)));
        mDataList.addAll(((java.util.Collection) (obj)));
        mFeedAdapter.notifyDataSetChanged();
        feedPageId = feedPageId + 1;
        if (!flag && ((List) (obj)).size() <= SHOW_RECOMMEND_COUNT)
        {
            appendRecommends(s);
        }
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        return;
_L9:
        i++;
        if (true) goto _L11; else goto _L10
_L10:
    }

    private void refreshAfterFollow(FeedModel2 feedmodel2, boolean flag)
    {
        Iterator iterator = mDataList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            FeedModel2 feedmodel2_1 = (FeedModel2)iterator.next();
            if (!feedmodel2_1.isRecommend || feedmodel2_1.albumId != feedmodel2.albumId || feedmodel2_1.uid != feedmodel2.uid)
            {
                continue;
            }
            feedmodel2_1.isFollowed = flag;
            mFeedAdapter.notifyDataSetChanged();
            break;
        } while (true);
    }

    private void saveFeedToPref()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCon);
        List list = getTempToSave(mDataList);
        if (!list.isEmpty())
        {
            sharedpreferencesutil.saveString("temp_feed", JSON.toJSONString(list));
        }
    }

    private void showBlockTipsDlgIfNeeded(final long uId, final long albumId, final int position, final View fromBindView, final View toBindView)
    {
        if (SharedPreferencesUtil.getInstance(mCon).getBoolean("dont_show_feed_block_tips"))
        {
            doBlock(uId, albumId, position, fromBindView, toBindView);
            return;
        } else
        {
            (new DialogBuilder(mActivity)).setMessage("\u9690\u85CF\u7684\u5185\u5BB9\uFF0C\u53EF\u5728\u201C\u8BBE\u7F6E->\u52A8\u6001\u8BBE\u7F6E\u201D\u4E2D\u6062\u590D").setOkBtn("\u786E\u5B9A", new _cls14()).setCancelBtn("\u4E0D\u518D\u63D0\u9192", new _cls13()).showConfirm();
            return;
        }
    }

    private void showNoNetworkLayout(boolean flag)
    {
        byte byte0;
        byte0 = 8;
        break MISSING_BLOCK_LABEL_4;
        if (canGoon() && (mDataList == null || mDataList.isEmpty()))
        {
            int i;
            if (mReloadDataView != null)
            {
                Object obj = mReloadDataView;
                if (flag)
                {
                    i = 0;
                } else
                {
                    i = 8;
                }
                ((View) (obj)).setVisibility(i);
            }
            if (mListView != null)
            {
                obj = mListView;
                if (flag)
                {
                    i = byte0;
                } else
                {
                    i = 0;
                }
                ((ListView) (obj)).setVisibility(i);
                return;
            }
        }
        return;
    }

    private void updateClickEvent(long l)
    {
        RequestParams requestparams = new RequestParams();
        String s = (new StringBuilder()).append("m/feed_recomm_albums/").append(l).append("/clicked").toString();
        f.a().b(s, requestparams, null, new _cls19());
    }

    public void doRegAndFollow(final FeedModel2 model, final View view)
    {
        ToolUtil.onEvent(getActivity(), "Feed_Recommend_Follow");
        updateClickEvent(model.albumId);
        if (model.uid == -1L)
        {
            return;
        }
        if (model.isFollowed)
        {
            (new DialogBuilder(getActivity())).setMessage("\u786E\u5B9A\u8981\u53D6\u6D88\u5173\u6CE8\uFF1F").setOkBtn(new _cls16()).showConfirm();
            return;
        } else
        {
            doFollow(model, view);
            return;
        }
    }

    public void goHostHomePage(long l, long l1, View view)
    {
        updateClickEvent(l1);
        Bundle bundle = new Bundle();
        bundle.putLong("toUid", l);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        ((MainTabActivity2)MyApplication.a()).startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
    }

    public void goToPlay(FeedModel2 feedmodel2, View view)
    {
        if (feedmodel2 != null)
        {
            ToolUtil.onEvent(getActivity(), "Feed_Recommend");
            updateClickEvent(feedmodel2.albumId);
            (new GetAlbumListTask()).setModel(feedmodel2, view).myexec(new Void[0]);
        }
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    public void loadAd()
    {
        if (mIsloadingAd)
        {
            return;
        } else
        {
            getLoaderManager().restartLoader(0x7f0a0030, null, this);
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        user = UserInfoMannage.getInstance().getUser();
        mHandler = new Handler();
        initUI();
        initListeners();
        initData();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        mIsloadingAd = true;
        return new com.ximalaya.ting.android.model.ad.FeedAd2.Loader(getActivity(), "feed_follow");
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300b7, viewgroup, false);
        mReloadDataView = fragmentBaseContainerView.findViewById(0x7f0a0351);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        if (mListView != null)
        {
            mFirstVisiblePosition = mListView.getFirstVisiblePosition();
        }
        clearRef();
        mIsFistResumed = false;
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        mIsloadingAd = false;
        mFeedAds = list;
        if (mPulldownRefresh)
        {
            mPulldownRefresh = false;
            loadFeedData(false);
            return;
        } else
        {
            initData();
            return;
        }
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onPause()
    {
        if (mBannerAdController != null)
        {
            mBannerAdController.stopSwapAd();
        }
        saveFeedToPref();
        super.onPause();
        if (mThirdBannerAd != null)
        {
            mThirdBannerAd.pause();
        }
    }

    public void onRefresh()
    {
        ((PullToRefreshListView)mListView).toRefreshing();
        mListView.setSelection(0);
    }

    public void onResume()
    {
        super.onResume();
        setPlayPath("play_feed");
        if (UserInfoMannage.getInstance().getUser() != null)
        {
            if (user == null || !user.equals(UserInfoMannage.getInstance().getUser()))
            {
                if (mDataList.size() > 0)
                {
                    mDataList.clear();
                    mFeedAdapter.notifyDataSetChanged();
                }
                mIsLoading = false;
                user = UserInfoMannage.getInstance().getUser();
            }
            if (mBannerAdController != null)
            {
                mBannerAdController.swapAd();
            }
            if (mThirdBannerAd != null)
            {
                mThirdBannerAd.resume();
            }
            if (mIsFistResumed)
            {
                if (getUserVisibleHint())
                {
                    loadAd();
                    return;
                }
            } else
            {
                mIsFistResumed = true;
                return;
            }
        }
    }

    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        saveFeedToPref();
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        initData();
    }

    public void showLoading(boolean flag)
    {
label0:
        {
            if (canGoon())
            {
                if (!flag)
                {
                    break label0;
                }
                findViewById(0x7f0a0091).setVisibility(0);
            }
            return;
        }
        findViewById(0x7f0a0091).setVisibility(8);
    }




/*
    static boolean access$1002(FeedFragmentNew feedfragmentnew, boolean flag)
    {
        feedfragmentnew.mPulldownRefresh = flag;
        return flag;
    }

*/



/*
    static int access$1202(FeedFragmentNew feedfragmentnew, int i)
    {
        feedfragmentnew.feedPageId = i;
        return i;
    }

*/















/*
    static MenuDialog access$502(FeedFragmentNew feedfragmentnew, MenuDialog menudialog)
    {
        feedfragmentnew.mFeedActionDlg = menudialog;
        return menudialog;
    }

*/






/*
    static boolean access$902(FeedFragmentNew feedfragmentnew, boolean flag)
    {
        feedfragmentnew.mIsLoading = flag;
        return flag;
    }

*/

    private class _cls3
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final FeedFragmentNew this$0;

        public void execute(String s)
        {
            s = WebFragment.newInstance(s);
            startFragment(s);
        }

        _cls3()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final FeedFragmentNew this$0;

        public void execute(String s)
        {
            try
            {
                if (!TextUtils.isEmpty(s))
                {
                    s = new Intent("android.intent.action.VIEW", Uri.parse(s));
                    startActivity(s);
                }
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }

        _cls4()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls11
        implements android.content.DialogInterface.OnShowListener
    {

        final FeedFragmentNew this$0;
        final View val$emptyDialog;

        public void onShow(final DialogInterface dialog)
        {
            if (loadingPopupWindow == null)
            {
                loadingPopupWindow = new PopupWindow((RelativeLayout)LayoutInflater.from(getActivity()).inflate(0x7f030196, null), -2, -2);
                loadingPopupWindow.setFocusable(false);
                loadingPopupWindow.setOutsideTouchable(false);
                class _cls1
                    implements android.widget.PopupWindow.OnDismissListener
                {

                    final _cls11 this$1;
                    final DialogInterface val$dialog;

                    public void onDismiss()
                    {
                        if (dialog != null)
                        {
                            dialog.dismiss();
                        }
                    }

                _cls1()
                {
                    this$1 = _cls11.this;
                    dialog = dialoginterface;
                    super();
                }
                }

                loadingPopupWindow.setOnDismissListener(new _cls1());
            }
            loadingPopupWindow.showAtLocation(emptyDialog, 17, 0, 0);
        }

        _cls11()
        {
            this$0 = FeedFragmentNew.this;
            emptyDialog = view;
            super();
        }
    }


    private class _cls15 extends MyAsyncTask
    {

        final FeedFragmentNew this$0;
        final long val$albumId;
        final View val$fromBindView;
        final int val$position;
        final View val$toBindView;
        final long val$uId;

        protected transient Boolean doInBackground(Void avoid[])
        {
            RequestParams requestparams;
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/api/1/feed/block/create").toString();
            requestparams = new RequestParams();
            requestparams.put("albumUid", String.valueOf(uId));
            requestparams.put("albumId", String.valueOf(albumId));
            avoid = (BaseModel)JSON.parseObject(f.a().b(avoid, requestparams, fromBindView, toBindView), com/ximalaya/ting/android/model/BaseModel);
            if (avoid == null)
            {
                break MISSING_BLOCK_LABEL_153;
            }
            if (((BaseModel) (avoid)).ret != 0)
            {
                break MISSING_BLOCK_LABEL_153;
            }
            Logger.log("FeedAdaper", (new StringBuilder()).append("==\u5C4F\u853D\u52A8\u6001\u6210\u529F==").append(albumId).toString());
            return Boolean.valueOf(true);
            avoid;
            if (loadingPopupWindow != null)
            {
                loadingPopupWindow.dismiss();
            }
            Logger.e("FeedFragement", "\u5C4F\u853D\u52A8\u6001\u51FA\u9519", avoid);
            return Boolean.valueOf(false);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(Boolean boolean1)
        {
            if (boolean1 != null && boolean1.booleanValue())
            {
                if (position + 1 <= mDataList.size())
                {
                    mDataList.remove(position);
                }
                mFeedAdapter.notifyDataSetChanged();
            } else
            {
                showToast("\u5C4F\u853D\u5931\u8D25");
            }
            if (loadingPopupWindow != null)
            {
                loadingPopupWindow.dismiss();
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Boolean)obj);
        }

        protected void onPreExecute()
        {
            if (loadingDialog == null)
            {
                createDimLoadingDialog();
            }
            loadingDialog.show();
        }

        _cls15()
        {
            this$0 = FeedFragmentNew.this;
            uId = l;
            albumId = l1;
            fromBindView = view;
            toBindView = view1;
            position = i;
            super();
        }
    }


    private class _cls18 extends com.ximalaya.ting.android.b.a
    {

        final FeedFragmentNew this$0;
        final FeedModel2 val$model;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            super.onFinish();
            showLoading(false);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u4E0D\u7ED9\u529B\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onStart()
        {
            super.onStart();
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s))
            {
                Object obj = null;
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = obj;
                }
                if (s != null && s.getIntValue("ret") == 0)
                {
                    refreshAfterFollow(model, true);
                    showToast("\u5173\u6CE8\u6210\u529F\uFF01");
                    return;
                }
            }
        }

        _cls18()
        {
            this$0 = FeedFragmentNew.this;
            model = feedmodel2;
            view = view1;
            super();
        }
    }


    private class _cls12 extends MyAsyncTask
    {

        final FeedFragmentNew this$0;
        final long val$albumId;
        final boolean val$currentIsTop;
        final View val$fromBindView;
        final int val$position;
        final View val$toBindView;
        final long val$uId;

        protected transient Boolean doInBackground(Void avoid[])
        {
            RequestParams requestparams;
            if (currentIsTop)
            {
                avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/api/1/feed/top/delete").toString();
            } else
            {
                avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/api/1/feed/top/create").toString();
            }
            requestparams = new RequestParams();
            requestparams.put("albumUid", String.valueOf(uId));
            requestparams.put("albumId", String.valueOf(albumId));
            avoid = (BaseModel)JSON.parseObject(f.a().b(avoid, requestparams, fromBindView, toBindView), com/ximalaya/ting/android/model/BaseModel);
            if (avoid == null)
            {
                break MISSING_BLOCK_LABEL_226;
            }
            if (((BaseModel) (avoid)).ret != 0)
            {
                break MISSING_BLOCK_LABEL_226;
            }
            avoid = (FeedModel2)mDataList.get(position);
            if (!currentIsTop) goto _L2; else goto _L1
_L1:
            avoid.isTop = false;
_L4:
            return Boolean.valueOf(true);
_L2:
            avoid.isTop = true;
            mDataList.remove(avoid);
            mDataList.add(0, avoid);
            if (true) goto _L4; else goto _L3
_L3:
            avoid;
            if (loadingPopupWindow != null)
            {
                loadingPopupWindow.dismiss();
            }
            Logger.e("FeedFragement", "\u7F6E\u9876/\u53D6\u6D88\u7F6E\u9876\u52A8\u6001\u51FA\u9519", avoid);
            return Boolean.valueOf(false);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(Boolean boolean1)
        {
            if (loadingPopupWindow != null)
            {
                loadingPopupWindow.dismiss();
            }
            if (boolean1 != null && boolean1.booleanValue())
            {
                FeedFragmentNew feedfragmentnew = FeedFragmentNew.this;
                StringBuilder stringbuilder = new StringBuilder();
                if (currentIsTop)
                {
                    boolean1 = "\u53D6\u6D88";
                } else
                {
                    boolean1 = "";
                }
                feedfragmentnew.showToast(stringbuilder.append(boolean1).append("\u7F6E\u9876\u6210\u529F").toString());
                mFeedAdapter.notifyDataSetChanged();
                return;
            }
            FeedFragmentNew feedfragmentnew1 = FeedFragmentNew.this;
            StringBuilder stringbuilder1 = new StringBuilder();
            if (currentIsTop)
            {
                boolean1 = "\u53D6\u6D88";
            } else
            {
                boolean1 = "";
            }
            feedfragmentnew1.showToast(stringbuilder1.append(boolean1).append("\u7F6E\u9876\u5931\u8D25").toString());
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Boolean)obj);
        }

        protected void onPreExecute()
        {
            if (loadingDialog == null)
            {
                createDimLoadingDialog();
            }
            loadingDialog.show();
        }

        _cls12()
        {
            this$0 = FeedFragmentNew.this;
            currentIsTop = flag;
            uId = l;
            albumId = l1;
            fromBindView = view;
            toBindView = view1;
            position = i;
            super();
        }
    }


    private class _cls17 extends com.ximalaya.ting.android.b.a
    {

        final FeedFragmentNew this$0;
        final FeedModel2 val$model;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            super.onFinish();
            showLoading(false);
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u4E0D\u7ED9\u529B\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        }

        public void onStart()
        {
            super.onStart();
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s))
            {
                Object obj = null;
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = obj;
                }
                if (s != null && s.getIntValue("ret") == 0)
                {
                    refreshAfterFollow(model, false);
                    showToast("\u5DF2\u53D6\u6D88\u5173\u6CE8");
                    return;
                }
            }
        }

        _cls17()
        {
            this$0 = FeedFragmentNew.this;
            model = feedmodel2;
            view = view1;
            super();
        }
    }


    private class _cls6 extends com.ximalaya.ting.android.b.a
    {

        final FeedFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
            Logger.e("FeedFragment", (new StringBuilder()).append("\u65B0feed\u65B0\u9C9C\u4E8B\u63A5\u53E3\u8C03\u7528\u5931\u8D25:").append(s).toString());
        }

        public void onSuccess(String s)
        {
        }

        _cls6()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AdapterView.OnItemClickListener
    {

        final FeedFragmentNew this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (OneClickHelper.getInstance().onClick(view))
            {
                if ((i -= mListView.getHeaderViewsCount()) >= 0 && mFeedAdapter.getCount() > i)
                {
                    adapterview = (FeedModel2)mFeedAdapter.getItem(i);
                    if (((FeedModel2) (adapterview)).dynamicType == 2)
                    {
                        goToNewThings(adapterview, view);
                        return;
                    }
                    if (((FeedModel2) (adapterview)).isAd)
                    {
                        clickAd(adapterview);
                        return;
                    }
                    if (!((FeedModel2) (adapterview)).isRecommendTitle)
                    {
                        ToolUtil.onEvent(getActivity(), "Feed_Attention");
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("feed", adapterview);
                        bundle.putInt("feed_new_count", ((FeedModel2) (adapterview)).unreadNum);
                        adapterview.unreadNum = 0;
                        mFeedAdapter.notifyDataSetChanged();
                        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                        startFragment(com/ximalaya/ting/android/fragment/ting/feed/FeedAlbumDetailFragment, bundle);
                        return;
                    }
                }
            }
        }

        _cls5()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls7
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final FeedFragmentNew this$0;

        public boolean onItemLongClick(AdapterView adapterview, final View viewInListView, final int itemRealPosition, long l)
        {
            itemRealPosition -= mListView.getHeaderViewsCount();
            if (itemRealPosition < 0 || mFeedAdapter.getCount() < itemRealPosition + 1)
            {
                return false;
            }
            final FeedModel2 model = (FeedModel2)mFeedAdapter.getItem(itemRealPosition);
            if (model == null || model.dynamicType == 2 || model.isAd || model.isRecommend || model.isRecommendTitle)
            {
                return false;
            }
            ArrayList arraylist = new ArrayList();
            class _cls1
                implements android.widget.AdapterView.OnItemClickListener
            {

                final _cls7 this$1;
                final int val$itemRealPosition;
                final FeedModel2 val$model;
                final View val$viewInListView;

                public void onItemClick(AdapterView adapterview1, View view, int i, long l1)
                {
                    i;
                    JVM INSTR tableswitch 0 1: default 24
                //                               0 38
                //                               1 90;
                       goto _L1 _L2 _L3
_L1:
                    mFeedActionDlg.dismiss();
                    return;
_L2:
                    doTop(model.isTop, model.uid, model.albumId, itemRealPosition, viewInListView, mListView);
                    continue; /* Loop/switch isn't completed */
_L3:
                    showBlockTipsDlgIfNeeded(model.uid, model.albumId, itemRealPosition, viewInListView, mListView);
                    if (true) goto _L1; else goto _L4
_L4:
                }

                _cls1()
                {
                    this$1 = _cls7.this;
                    model = feedmodel2;
                    itemRealPosition = i;
                    viewInListView = view;
                    super();
                }
            }

            if (model.isTop)
            {
                adapterview = "\u53D6\u6D88\u7F6E\u9876";
            } else
            {
                adapterview = "\u7F6E\u9876";
            }
            arraylist.add(adapterview);
            arraylist.add("\u9690\u85CF");
            if (mFeedActionDlg == null)
            {
                mFeedActionDlg = new MenuDialog(getActivity(), arraylist);
            } else
            {
                mFeedActionDlg.setSelections(arraylist);
            }
            mFeedActionDlg.setOnItemClickListener(new _cls1());
            mFeedActionDlg.show();
            return true;
        }

        _cls7()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls8
        implements android.widget.AbsListView.OnScrollListener
    {

        final FeedFragmentNew this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0 && mHasMore)
            {
                i = abslistview.getCount();
                if (i > 5)
                {
                    i -= 5;
                } else
                {
                    i--;
                }
                if (abslistview.getLastVisiblePosition() > i && mHasMore && !mIsLoading)
                {
                    loadFeedData(true);
                }
            }
        }

        _cls8()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FeedFragmentNew this$0;

        public void onClick(View view)
        {
            showNoNetworkLayout(false);
            ((PullToRefreshListView)mListView).onRefreshComplete();
            ((PullToRefreshListView)mListView).toRefreshing();
        }

        _cls1()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final FeedFragmentNew this$0;

        public void onClick(View view)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            loadFeedData(true);
        }

        _cls2()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls9
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final FeedFragmentNew this$0;

        public void onRefresh()
        {
            if (!mIsLoading)
            {
                mPulldownRefresh = true;
                clearFeedDot();
                feedPageId = 1;
                loadAd();
            }
        }

        _cls9()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls10 extends com.ximalaya.ting.android.b.a
    {

        final FeedFragmentNew this$0;
        final boolean val$isPullUp;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mListView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
            if (mListView != null)
            {
                ((PullToRefreshListView)mListView).onRefreshComplete();
            }
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            showNoNetworkLayout(true);
        }

        public void onStart()
        {
            super.onStart();
            if (loginInfoModel == null)
            {
                loginInfoModel = UserInfoMannage.getInstance().getUser();
            }
        }

        public void onSuccess(final String responseContent)
        {
            class _cls1
                implements MyCallback
            {

                final _cls10 this$1;
                final String val$responseContent;

                public void execute()
                {
                    parseFeedData(isPullUp, responseContent, ignoreNewThingItem);
                }

                _cls1()
                {
                    this$1 = _cls10.this;
                    responseContent = s;
                    super();
                }
            }

            doAfterAnimation(new _cls1());
        }

        _cls10()
        {
            this$0 = FeedFragmentNew.this;
            isPullUp = flag;
            super();
        }
    }


    private class _cls14
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final FeedFragmentNew this$0;
        final long val$albumId;
        final View val$fromBindView;
        final int val$position;
        final View val$toBindView;
        final long val$uId;

        public void onExecute()
        {
            doBlock(uId, albumId, position, fromBindView, toBindView);
        }

        _cls14()
        {
            this$0 = FeedFragmentNew.this;
            uId = l;
            albumId = l1;
            position = i;
            fromBindView = view;
            toBindView = view1;
            super();
        }
    }


    private class _cls13
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final FeedFragmentNew this$0;
        final long val$albumId;
        final View val$fromBindView;
        final int val$position;
        final View val$toBindView;
        final long val$uId;

        public void onExecute()
        {
            SharedPreferencesUtil.getInstance(mCon).saveBoolean("dont_show_feed_block_tips", true);
            doBlock(uId, albumId, position, fromBindView, toBindView);
        }

        _cls13()
        {
            this$0 = FeedFragmentNew.this;
            uId = l;
            albumId = l1;
            position = i;
            fromBindView = view;
            toBindView = view1;
            super();
        }
    }


    private class _cls19 extends com.ximalaya.ting.android.b.a
    {

        final FeedFragmentNew this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
            Logger.e("\u8C03\u7528feed\u63A8\u8350\u4E3B\u64AD\u4E13\u8F91\u70B9\u51FB\u7EDF\u8BA1\u63A5\u53E3\u5931\u8D25", s);
        }

        public void onSuccess(String s)
        {
            Object obj = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = obj;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                return;
            } else
            {
                Logger.v("feed", " \u8C03\u7528feed\u63A8\u8350\u4E3B\u64AD\u4E13\u8F91\u70B9\u51FB\u7EDF\u8BA1\u63A5\u53E3\u6210\u529F");
                return;
            }
        }

        _cls19()
        {
            this$0 = FeedFragmentNew.this;
            super();
        }
    }


    private class _cls16
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final FeedFragmentNew this$0;
        final FeedModel2 val$model;
        final View val$view;

        public void onExecute()
        {
            doUnFollow(model, view);
        }

        _cls16()
        {
            this$0 = FeedFragmentNew.this;
            model = feedmodel2;
            view = view1;
            super();
        }
    }

}
