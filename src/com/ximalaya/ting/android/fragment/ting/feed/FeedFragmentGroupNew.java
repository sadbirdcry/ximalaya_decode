// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.astuetz.PagerSlidingTabStrip;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.ting.FeedNoLoginRecommendFragment;
import com.ximalaya.ting.android.fragment.ting.HistoryFragment;
import com.ximalaya.ting.android.model.NoReadModel;
import com.ximalaya.ting.android.modelmanage.NoReadManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting.feed:
//            FeedFragmentNew, FeedCollectFragment

public class FeedFragmentGroupNew extends BaseFragment
    implements android.support.v4.view.ViewPager.OnPageChangeListener, com.ximalaya.ting.android.modelmanage.NoReadManage.NoReadUpdateListener
{
    class TabAdapter extends FragmentPagerAdapter
        implements com.astuetz.PagerSlidingTabStrip.c
    {

        final FeedFragmentGroupNew this$0;
        private List tipDots;

        public int getCount()
        {
            return mTabTitles.length;
        }

        public Fragment getCurrentFragment(ViewPager viewpager)
        {
            Method method = getClass().getSuperclass().getDeclaredMethod("makeFragmentName", new Class[] {
                Integer.TYPE, Long.TYPE
            });
            Object obj = getClass().getSuperclass().getDeclaredField("mFragmentManager");
            ((Field) (obj)).setAccessible(true);
            obj = (FragmentManager)((Field) (obj)).get(this);
            method.setAccessible(true);
            viewpager = ((FragmentManager) (obj)).findFragmentByTag((String)method.invoke(null, new Object[] {
                Integer.valueOf(viewpager.getId()), Long.valueOf(viewpager.getCurrentItem())
            }));
            return viewpager;
            viewpager;
            viewpager.printStackTrace();
_L2:
            return null;
            viewpager;
            viewpager.printStackTrace();
            continue; /* Loop/switch isn't completed */
            viewpager;
            viewpager.printStackTrace();
            continue; /* Loop/switch isn't completed */
            viewpager;
            viewpager.printStackTrace();
            continue; /* Loop/switch isn't completed */
            viewpager;
            viewpager.printStackTrace();
            if (true) goto _L2; else goto _L1
_L1:
        }

        public Fragment getItem(int i)
        {
            switch (i)
            {
            default:
                return null;

            case 0: // '\0'
                if (UserInfoMannage.hasLogined())
                {
                    return FeedFragmentNew.newInstance();
                } else
                {
                    return FeedNoLoginRecommendFragment.newInstance();
                }

            case 1: // '\001'
                return FeedCollectFragment.newInstance();

            case 2: // '\002'
                return HistoryFragment.newInstance();
            }
        }

        public CharSequence getPageTitle(int i)
        {
            return mTabTitles[i];
        }

        public View getTabWidget(int i)
        {
            View view = LayoutInflater.from(mCon).inflate(0x7f0301db, null);
            view.setLayoutParams(new android.widget.RelativeLayout.LayoutParams(ToolUtil.getScreenWidth(mCon) / 3, ToolUtil.dp2px(mCon, 45F)));
            ((TextView)view.findViewById(0x7f0a06f1)).setText(getPageTitle(i));
            View view1 = view.findViewById(0x7f0a04e6);
            tipDots.add(view1);
            return view;
        }

        public void setTipDotVisible(int i, boolean flag)
        {
            if (tipDots.size() > i)
            {
                View view = (View)tipDots.get(i);
                if (flag)
                {
                    i = 0;
                } else
                {
                    i = 8;
                }
                view.setVisibility(i);
            }
        }

        public TabAdapter(FragmentManager fragmentmanager)
        {
            this$0 = FeedFragmentGroupNew.this;
            super(fragmentmanager);
            tipDots = new ArrayList();
        }
    }


    private TabAdapter mAdapter;
    private ViewPager mPager;
    private String mTabTitles[] = {
        "\u5173\u6CE8", "\u6536\u85CF", "\u5386\u53F2"
    };
    private PagerSlidingTabStrip mTabs;

    public FeedFragmentGroupNew()
    {
    }

    private void clearAlbumDot()
    {
        if (NoReadManage.getInstance() != null)
        {
            if (NoReadManage.getInstance().getNoReadModel() != null)
            {
                NoReadManage.getInstance().getNoReadModel().favoriteAlbumIsUpdate = false;
            }
            NoReadManage.getInstance().updateNoReadManageManual();
        }
    }

    private void clearFeedDot()
    {
        if (NoReadManage.getInstance() != null)
        {
            if (NoReadManage.getInstance().getNoReadModel() != null)
            {
                NoReadManage.getInstance().getNoReadModel().newEventCountChanged = false;
                NoReadManage.getInstance().getNoReadModel().newFeedCount = 0;
            }
            NoReadManage.getInstance().updateNoReadManageManual();
        }
    }

    private boolean collectHasNew(NoReadModel noreadmodel)
    {
        if (noreadmodel == null)
        {
            return false;
        } else
        {
            return noreadmodel.favoriteAlbumIsUpdate;
        }
    }

    private boolean feedHasNew(NoReadModel noreadmodel)
    {
        while (noreadmodel == null || noreadmodel.newFeedCount <= 0 && !noreadmodel.newEventCountChanged) 
        {
            return false;
        }
        return true;
    }

    private Fragment getCurrentFragment()
    {
        if (mAdapter == null)
        {
            return null;
        } else
        {
            return mAdapter.getCurrentFragment(mPager);
        }
    }

    private void initTabs()
    {
        mTabs.setDividerColor(0);
        mTabs.setIndicatorColor(Color.parseColor("#f86442"));
        mTabs.setUnderlineColor(Color.parseColor("#f6f6f7"));
        mTabs.setUnderlineHeight(0);
        mTabs.setDividerPadding(0);
        mTabs.setDeactivateTextColor(Color.parseColor("#666666"));
        mTabs.setActivateTextColor(Color.parseColor("#f86442"));
        mTabs.setTabSwitch(true);
        mTabs.setTextSize(15);
        mTabs.setTextColorResource(0x7f0700ce);
        mTabs.setIndicatorHeight(Utilities.dip2px(getActivity(), 3F));
        mTabs.setTabPaddingLeftRight(Utilities.dip2px(getActivity(), 17F));
        mAdapter = new TabAdapter(getChildFragmentManager());
        mPager.setAdapter(mAdapter);
        mTabs.setViewPager(mPager);
        mTabs.a(0);
        mTabs.setOnPageChangeListener(this);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initTabs();
        if (NoReadManage.getInstance() != null)
        {
            NoReadManage.getInstance().setOnNoReadUpdateListenerListener(this);
        }
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (getCurrentFragment() == null)
        {
            return;
        } else
        {
            getCurrentFragment().onActivityResult(i, j, intent);
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0301e0, viewgroup, false);
        mTabs = (PagerSlidingTabStrip)layoutinflater.findViewById(0x7f0a02dc);
        mPager = (ViewPager)layoutinflater.findViewById(0x7f0a017a);
        return layoutinflater;
    }

    public void onDestroyView()
    {
        mTabs.b();
        mTabs.setOnPageChangeListener(null);
        if (NoReadManage.getInstance() != null)
        {
            NoReadManage.getInstance().removeNoReadUpdateListenerListener(this);
        }
        super.onDestroyView();
    }

    public void onPageScrollStateChanged(int i)
    {
    }

    public void onPageScrolled(int i, float f, int j)
    {
    }

    public void onPageSelected(int i)
    {
        if (i != 0) goto _L2; else goto _L1
_L1:
        clearFeedDot();
_L11:
        Fragment fragment = getCurrentFragment();
        if (i != 0) goto _L4; else goto _L3
_L3:
        if (!(fragment instanceof FeedFragmentNew)) goto _L6; else goto _L5
_L5:
        ((FeedFragmentNew)fragment).loadAd();
_L8:
        return;
_L2:
        if (i == 1)
        {
            clearAlbumDot();
        }
        continue; /* Loop/switch isn't completed */
_L6:
        if (!(fragment instanceof FeedNoLoginRecommendFragment)) goto _L8; else goto _L7
_L7:
        ((FeedNoLoginRecommendFragment)fragment).loadAd();
        return;
_L4:
        if (i != 1 || !(fragment instanceof FeedCollectFragment)) goto _L8; else goto _L9
_L9:
        ((FeedCollectFragment)fragment).loadAd();
        return;
        if (true) goto _L11; else goto _L10
_L10:
    }

    public void onPause()
    {
        super.onPause();
        if (getCurrentFragment() != null)
        {
            getCurrentFragment().onPause();
        }
    }

    public void onRefresh()
    {
        if (getCurrentFragment() != null)
        {
            ((BaseFragment)getCurrentFragment()).onRefresh();
        }
    }

    public void onResume()
    {
        super.onResume();
        if (getCurrentFragment() != null && getCurrentFragment().isResumed())
        {
            getCurrentFragment().onResume();
        }
        if (getCurrentFragment() != null)
        {
            if (getCurrentFragment() instanceof FeedFragmentNew)
            {
                clearFeedDot();
            } else
            if (getCurrentFragment() instanceof FeedCollectFragment)
            {
                clearAlbumDot();
                return;
            }
        }
    }

    public void refreshLocalFav()
    {
        Fragment fragment = getCurrentFragment();
        if (fragment != null && (fragment instanceof FeedCollectFragment) && fragment.isAdded())
        {
            ((FeedCollectFragment)fragment).refreshLocalIfNeed();
        }
    }

    public void update(NoReadModel noreadmodel)
    {
        if (noreadmodel == null)
        {
            return;
        } else
        {
            mAdapter.setTipDotVisible(0, feedHasNew(noreadmodel));
            mAdapter.setTipDotVisible(1, collectHasNew(noreadmodel));
            return;
        }
    }

}
