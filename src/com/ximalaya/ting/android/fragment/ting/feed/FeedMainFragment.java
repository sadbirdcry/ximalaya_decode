// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.util.DataCollectUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting.feed:
//            FeedFragmentGroupNew

public class FeedMainFragment extends BaseFragment
{

    private RelativeLayout container_layout;
    public FeedFragmentGroupNew fragmentGroup;
    private FragmentManager manager;

    public FeedMainFragment()
    {
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (fragmentGroup == null)
        {
            return;
        } else
        {
            fragmentGroup.onActivityResult(i, j, intent);
            return;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        container_layout = (RelativeLayout)layoutinflater.inflate(0x7f0300b6, viewgroup, false);
        manager = getChildFragmentManager();
        fragmentGroup = new FeedFragmentGroupNew();
        pushFragments(fragmentGroup);
        DataCollectUtil.bindViewIdDataToView(container_layout);
        return container_layout;
    }

    public void onPause()
    {
        super.onPause();
        if (fragmentGroup != null)
        {
            fragmentGroup.onPause();
        }
    }

    public void onRefresh()
    {
        if (fragmentGroup != null && fragmentGroup.canGoon())
        {
            fragmentGroup.onRefresh();
        }
    }

    public void onResume()
    {
        super.onResume();
        setPlayPath("play_feed");
        if (fragmentGroup != null)
        {
            fragmentGroup.onResume();
        }
    }

    public void pushFragments(Fragment fragment)
    {
        while (getActivity() == null || getActivity().isFinishing() || android.os.Build.VERSION.SDK_INT >= 17 && getActivity() != null && getActivity().isDestroyed()) 
        {
            return;
        }
        try
        {
            FragmentTransaction fragmenttransaction = manager.beginTransaction();
            fragmenttransaction.add(0x7f0a0089, fragment);
            fragmenttransaction.addToBackStack(null);
            fragmenttransaction.commitAllowingStateLoss();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Fragment fragment)
        {
            fragment.printStackTrace();
        }
    }

    public void refreshLocalFav()
    {
        if (fragmentGroup != null && fragmentGroup.canGoon())
        {
            fragmentGroup.refreshLocalFav();
        }
    }
}
