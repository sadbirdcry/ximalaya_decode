// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.ting.android.adapter.feed.FeedAdapter2;
import com.ximalaya.ting.android.model.feed2.FeedModel2;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting.feed:
//            FeedFragmentNew, FeedAlbumDetailFragment

class this._cls0
    implements android.widget.ClickListener
{

    final FeedFragmentNew this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        if (OneClickHelper.getInstance().onClick(view))
        {
            if ((i -= mListView.getHeaderViewsCount()) >= 0 && FeedFragmentNew.access$200(FeedFragmentNew.this).getCount() > i)
            {
                adapterview = (FeedModel2)FeedFragmentNew.access$200(FeedFragmentNew.this).getItem(i);
                if (((FeedModel2) (adapterview)).dynamicType == 2)
                {
                    FeedFragmentNew.access$300(FeedFragmentNew.this, adapterview, view);
                    return;
                }
                if (((FeedModel2) (adapterview)).isAd)
                {
                    FeedFragmentNew.access$400(FeedFragmentNew.this, adapterview);
                    return;
                }
                if (!((FeedModel2) (adapterview)).isRecommendTitle)
                {
                    ToolUtil.onEvent(getActivity(), "Feed_Attention");
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("feed", adapterview);
                    bundle.putInt("feed_new_count", ((FeedModel2) (adapterview)).unreadNum);
                    adapterview.unreadNum = 0;
                    FeedFragmentNew.access$200(FeedFragmentNew.this).notifyDataSetChanged();
                    bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                    startFragment(com/ximalaya/ting/android/fragment/ting/feed/FeedAlbumDetailFragment, bundle);
                    return;
                }
            }
        }
    }

    gment()
    {
        this$0 = FeedFragmentNew.this;
        super();
    }
}
