// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.feed.FeedSoundAdapter2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.feed2.FeedModel2;
import com.ximalaya.ting.android.model.feed2.FeedSoundInfo;
import com.ximalaya.ting.android.model.feed2.FeedSoundInfoList;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.NetworkUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class FeedAlbumDetailFragment extends BaseListFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{

    private static final int PAGE_SIZE = 30;
    private FeedSoundAdapter2 mAdapter;
    private ArrayList mDataList;
    private FeedModel2 mFeedModel;
    private boolean mHasMore;
    private boolean mIsLoading;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private View mReloadDataView;
    private int newFeedCount;
    private int pageId;

    public FeedAlbumDetailFragment()
    {
        newFeedCount = 0;
        mDataList = new ArrayList();
        pageId = 1;
    }

    private void initData()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            mFeedModel = (FeedModel2)bundle.getSerializable("feed");
            newFeedCount = bundle.getInt("feed_new_count");
        }
        loadData(true);
    }

    private void initView()
    {
        String s;
        if (mFeedModel == null || mFeedModel.albumTitle == null)
        {
            s = "\u4E13\u8F91\u8BE6\u60C5";
        } else
        {
            s = mFeedModel.albumTitle;
        }
        setTitleText(s);
        mAdapter = new FeedSoundAdapter2(getActivity(), mDataList, newFeedCount);
        mListView.setAdapter(mAdapter);
    }

    private void loadData(final boolean firstLoad)
    {
        if (!NetworkUtils.isNetworkAvaliable(getActivity()))
        {
            showNoNetworkLayout(true);
        } else
        if (!mIsLoading)
        {
            mIsLoading = true;
            showNoNetworkLayout(false);
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
            RequestParams requestparams = new RequestParams();
            if (loginInfoModel != null)
            {
                requestparams.put("uid", loginInfoModel.uid);
                requestparams.put("token", loginInfoModel.token);
            }
            requestparams.put("albumId", mFeedModel.albumId);
            requestparams.put("albumUid", mFeedModel.uid);
            requestparams.put("pageId", pageId);
            requestparams.put("pageSize", 30);
            f.a().a("mobile/api/1/feed/album/list", requestparams, DataCollectUtil.getDataFromView(fragmentBaseContainerView), new _cls5());
            return;
        }
    }

    private void parseData(boolean flag, String s)
    {
        try
        {
            s = (FeedSoundInfoList)JSON.parseObject(s, com/ximalaya/ting/android/model/feed2/FeedSoundInfoList);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            s = null;
        }
        if (s == null || ((FeedSoundInfoList) (s)).ret != 0)
        {
            showToast("\u83B7\u53D6\u64AD\u653E\u5217\u8868\u5931\u8D25");
            showNoNetworkLayout(true);
            return;
        }
        if (((FeedSoundInfoList) (s)).data == null || ((FeedSoundInfoList) (s)).data.isEmpty())
        {
            mHasMore = false;
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
            return;
        }
        if (((FeedSoundInfoList) (s)).data.size() < 30)
        {
            mHasMore = false;
        }
        pageId = pageId + 1;
        if (flag)
        {
            mDataList.clear();
        }
        mDataList.addAll(setAlbumInfo(((FeedSoundInfoList) (s)).data));
        mAdapter.notifyDataSetChanged();
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        s = new HashMap();
        if (loginInfoModel != null)
        {
            s.put("uid", (new StringBuilder()).append(loginInfoModel.uid).append("").toString());
            s.put("token", (new StringBuilder()).append(loginInfoModel.token).append("").toString());
        }
        s.put("albumId", (new StringBuilder()).append(mFeedModel.albumId).append("").toString());
        s.put("albumUid", (new StringBuilder()).append(mFeedModel.uid).append("").toString());
        s.put("pageId", (new StringBuilder()).append(pageId).append("").toString());
        s.put("pageSize", "30");
        mAdapter.setDataSource("mobile/api/1/feed/album/list");
        mAdapter.setPageId(pageId);
        mAdapter.setParams(s);
    }

    private void regListener()
    {
        mReloadDataView.setOnClickListener(this);
        mFooterViewLoading.setOnClickListener(new _cls1());
        mListView.setOnScrollListener(new _cls2());
        mListView.setOnItemClickListener(new _cls3());
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls4();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private List setAlbumInfo(List list)
    {
        for (Iterator iterator = list.iterator(); iterator.hasNext();)
        {
            FeedSoundInfo feedsoundinfo = (FeedSoundInfo)iterator.next();
            feedsoundinfo.albumId = mFeedModel.albumId;
            feedsoundinfo.albumTitle = mFeedModel.albumTitle;
        }

        return list;
    }

    private void showNoNetworkLayout(boolean flag)
    {
        byte byte0 = 8;
        if (mDataList == null || mDataList.isEmpty())
        {
            Object obj = mReloadDataView;
            int i;
            if (flag)
            {
                i = 0;
            } else
            {
                i = 8;
            }
            ((View) (obj)).setVisibility(i);
            obj = mListView;
            if (flag)
            {
                i = byte0;
            } else
            {
                i = 0;
            }
            ((ListView) (obj)).setVisibility(i);
        }
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(this);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initData();
        initView();
        regListener();
    }

    public void onClick(View view)
    {
        if (!isAdded())
        {
            return;
        }
        switch (view.getId())
        {
        default:
            return;

        case 2131362641: 
            onRefresh();
            break;
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300b4, viewgroup, false);
        mReloadDataView = fragmentBaseContainerView.findViewById(0x7f0a0351);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        unRegisterListener();
    }

    public void onPlayCanceled()
    {
    }

    public void onRefresh()
    {
        loadData(true);
        mListView.setSelection(0);
    }

    public void onResume()
    {
        super.onResume();
        setPlayPath("play_feed");
    }

    public void onSoundChanged(int i)
    {
        if (mAdapter != null)
        {
            mAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (i < 0 || soundinfo == null || i >= mDataList.size() || ((FeedSoundInfo)mDataList.get(i)).trackId != soundinfo.trackId || mDataList == null)
        {
            return;
        } else
        {
            ((FeedSoundInfo)mDataList.get(i)).favoritesCounts = soundinfo.favorites_counts;
            mAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void showLoading(boolean flag)
    {
label0:
        {
            if (canGoon())
            {
                if (!flag)
                {
                    break label0;
                }
                findViewById(0x7f0a0091).setVisibility(0);
            }
            return;
        }
        findViewById(0x7f0a0091).setVisibility(8);
    }



/*
    static boolean access$002(FeedAlbumDetailFragment feedalbumdetailfragment, boolean flag)
    {
        feedalbumdetailfragment.mIsLoading = flag;
        return flag;
    }

*/




/*
    static boolean access$102(FeedAlbumDetailFragment feedalbumdetailfragment, boolean flag)
    {
        feedalbumdetailfragment.mHasMore = flag;
        return flag;
    }

*/









    private class _cls5 extends a
    {

        final FeedAlbumDetailFragment this$0;
        final boolean val$firstLoad;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            showNoNetworkLayout(true);
        }

        public void onStart()
        {
            super.onStart();
            mHasMore = true;
        }

        public void onSuccess(final String responseContent)
        {
            if (!canGoon())
            {
                return;
            } else
            {
                class _cls1
                    implements MyCallback
                {

                    final _cls5 this$1;
                    final String val$responseContent;

                    public void execute()
                    {
                        parseData(firstLoad, responseContent);
                    }

                _cls1()
                {
                    this$1 = _cls5.this;
                    responseContent = s;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                return;
            }
        }

        _cls5()
        {
            this$0 = FeedAlbumDetailFragment.this;
            firstLoad = flag;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FeedAlbumDetailFragment this$0;

        public void onClick(View view)
        {
            if (!mIsLoading)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                onRefresh();
            }
        }

        _cls1()
        {
            this$0 = FeedAlbumDetailFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AbsListView.OnScrollListener
    {

        final FeedAlbumDetailFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || !mHasMore)
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        loadData(false);
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls2()
        {
            this$0 = FeedAlbumDetailFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final FeedAlbumDetailFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (OneClickHelper.getInstance().onClick(view))
            {
                i -= mListView.getHeaderViewsCount();
                if (mDataList != null && i >= 0 && i < mDataList.size())
                {
                    adapterview = ModelHelper.toSoundInfoList(mDataList);
                    HashMap hashmap = new HashMap();
                    if (loginInfoModel != null)
                    {
                        hashmap.put("uid", (new StringBuilder()).append(loginInfoModel.uid).append("").toString());
                        hashmap.put("token", (new StringBuilder()).append(loginInfoModel.token).append("").toString());
                    }
                    hashmap.put("albumId", (new StringBuilder()).append(mFeedModel.albumId).append("").toString());
                    hashmap.put("albumUid", (new StringBuilder()).append(mFeedModel.uid).append("").toString());
                    hashmap.put("pageId", (new StringBuilder()).append(pageId).append("").toString());
                    hashmap.put("pageSize", "30");
                    PlayTools.gotoPlay(1, "mobile/api/1/feed/album/list", pageId, hashmap, adapterview, i, 
// JavaClassFileOutputException: get_constant: invalid tag

        _cls3()
        {
            this$0 = FeedAlbumDetailFragment.this;
            super();
        }
    }


    private class _cls4 extends OnPlayerStatusUpdateListenerProxy
    {

        final FeedAlbumDetailFragment this$0;

        public void onPlayStateChange()
        {
            mAdapter.notifyDataSetChanged();
        }

        _cls4()
        {
            this$0 = FeedAlbumDetailFragment.this;
            super();
        }
    }

}
