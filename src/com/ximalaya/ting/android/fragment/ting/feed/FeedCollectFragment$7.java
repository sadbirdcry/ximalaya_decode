// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting.feed:
//            FeedCollectFragment

class this._cls0 extends MyAsyncTask
{

    final FeedCollectFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/subscribe/recommend/list").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("pageId", (new StringBuilder()).append(FeedCollectFragment.access$300(FeedCollectFragment.this)).append("").toString());
        requestparams.put("pageSize", (new StringBuilder()).append(FeedCollectFragment.access$400(FeedCollectFragment.this)).append("").toString());
        avoid = f.a().a(avoid, requestparams, mListView, mListView);
        Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
        Object obj = JSON.parseObject(avoid);
        if (obj == null)
        {
            return null;
        }
        avoid = ((JSONObject) (obj)).get("ret").toString();
        FeedCollectFragment.access$502(FeedCollectFragment.this, ((JSONObject) (obj)).getIntValue("totalCount"));
        if (!"0".equals(avoid))
        {
            break MISSING_BLOCK_LABEL_338;
        }
        avoid = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/album/AlbumModel);
        try
        {
            obj = JSON.parseArray(((JSONObject) (obj)).getString("frDatas"), com/ximalaya/ting/android/model/album/AlbumModel);
            break MISSING_BLOCK_LABEL_205;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L1
        obj;
        avoid = null;
_L1:
        Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
        obj = null;
        Object obj1 = avoid;
        if (avoid == null)
        {
            obj1 = new ArrayList();
        }
        if (obj != null && !((List) (obj)).isEmpty())
        {
            avoid = new AlbumModel();
            avoid.isRecommendTitle = true;
            ((List) (obj1)).add(avoid);
            for (avoid = ((List) (obj)).iterator(); avoid.hasNext();)
            {
                ((AlbumModel)avoid.next()).isRecommend = true;
            }

            ((List) (obj1)).addAll(((java.util.Collection) (obj)));
        }
        return ((List) (obj1));
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(final List result)
    {
        FeedCollectFragment.access$002(FeedCollectFragment.this, false);
        if (isAdded())
        {
            ((PullToRefreshListView)mListView).onRefreshComplete();
            if (result == null)
            {
                FeedCollectFragment.access$1200(FeedCollectFragment.this, true);
                return;
            }
            if (result.isEmpty())
            {
                if (FeedCollectFragment.access$800(FeedCollectFragment.this).isEmpty())
                {
                    showFooterView(com.ximalaya.ting.android.fragment.rView.NO_DATA);
                    return;
                }
            } else
            {
                class _cls1
                    implements MyCallback
                {

                    final FeedCollectFragment._cls7 this$1;
                    final List val$result;

                    public void execute()
                    {
                        if (FeedCollectFragment.access$300(this$0) == 1)
                        {
                            FeedCollectFragment.access$800(this$0).clear();
                            FeedCollectFragment.access$800(this$0).addAll(result);
                            FeedCollectFragment.access$1300(this$0, FeedCollectFragment.access$800(this$0));
                            FeedCollectFragment.access$900(this$0).notifyDataSetChanged();
                        } else
                        {
                            FeedCollectFragment.access$800(this$0).addAll(result);
                            FeedCollectFragment.access$1300(this$0, FeedCollectFragment.access$800(this$0));
                            FeedCollectFragment.access$900(this$0).notifyDataSetChanged();
                        }
                        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                        int _tmp = FeedCollectFragment.access$308(this$0);
                    }

            _cls1()
            {
                this$1 = FeedCollectFragment._cls7.this;
                result = list;
                super();
            }
                }

                FeedCollectFragment.access$1400(FeedCollectFragment.this, new _cls1());
                return;
            }
        }
    }

    protected void onPreExecute()
    {
        FeedCollectFragment.access$1200(FeedCollectFragment.this, false);
        if (FeedCollectFragment.access$300(FeedCollectFragment.this) > 1)
        {
            showFooterView(com.ximalaya.ting.android.fragment.rView.LOADING);
        }
        FeedCollectFragment.access$002(FeedCollectFragment.this, true);
    }

    _cls1()
    {
        this$0 = FeedCollectFragment.this;
        super();
    }
}
