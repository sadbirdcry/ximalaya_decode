// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting.feed;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.adapter.feed.FeedCollectAlbumListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.NoReadModel;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.model.ad.BannerAdController;
import com.ximalaya.ting.android.model.ad.FeedAd2;
import com.ximalaya.ting.android.model.ad.IThirdAd;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.modelmanage.AdManager;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.NoReadManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.SlideView;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FeedCollectFragment extends BaseListFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks
{

    public static boolean isNeedRefresh = false;
    private boolean loadingNextPage;
    private FeedCollectAlbumListAdapter mAdapter;
    private List mAds;
    private com.ximalaya.ting.android.modelmanage.AlbumModelManage.OnDataChangedCallback mAlbumCollectDataChangeCallback;
    private BannerAdController mBannerAdController;
    private List mDataList;
    private MyAsyncTask mDataLoadTask;
    private Handler mHandler;
    private boolean mIsCollectAlbumDataChanged;
    private boolean mIsFistResumed;
    private boolean mIsLoadedData;
    private boolean mIsloadingAd;
    private boolean mPullToRefresh;
    private View mReloadDataView;
    private IThirdAd mThirdBannerAd;
    private int pageId;
    private int pageSize;
    private int totalCount;

    public FeedCollectFragment()
    {
        mDataList = new ArrayList();
        loadingNextPage = false;
        pageId = 1;
        pageSize = 30;
        totalCount = 0;
        mIsLoadedData = false;
        mIsloadingAd = false;
        mIsFistResumed = false;
        mPullToRefresh = false;
    }

    private void addAd()
    {
        if (a.k)
        {
            mBannerAdController = new BannerAdController(getActivity(), mListView, "feed_collect");
            mBannerAdController.load(this);
            View view = mBannerAdController.getView();
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ((ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 10F) * 2) * 170) / 720));
            mListView.addHeaderView(view);
        }
    }

    private void clearAlbumDot()
    {
        if (NoReadManage.getInstance() != null)
        {
            if (NoReadManage.getInstance().getNoReadModel() != null)
            {
                NoReadManage.getInstance().getNoReadModel().favoriteAlbumIsUpdate = false;
            }
            NoReadManage.getInstance().updateNoReadManageManual();
        }
    }

    private void doLoad()
    {
        if (!loadingNextPage || isNeedRefresh)
        {
            isNeedRefresh = false;
            pageId = 1;
            if (mDataList.size() > 0)
            {
                mListView.setSelection(0);
            }
            loadDataListData();
        }
    }

    private List getUnFollowList(List list, List list1)
    {
        ArrayList arraylist;
        arraylist = new ArrayList();
        if (list == null)
        {
            return arraylist;
        }
        list = list.iterator();
_L2:
        AlbumModel albummodel;
        boolean flag;
        if (!list.hasNext())
        {
            break; /* Loop/switch isn't completed */
        }
        albummodel = (AlbumModel)list.next();
        Iterator iterator = list1.iterator();
        AlbumModel albummodel1;
        do
        {
            if (!iterator.hasNext())
            {
                break MISSING_BLOCK_LABEL_127;
            }
            albummodel1 = (AlbumModel)iterator.next();
        } while (albummodel1.uid != albummodel.uid || albummodel1.albumId != albummodel.albumId);
        flag = true;
_L3:
        if (!flag)
        {
            albummodel.isRecommend = true;
            arraylist.add(albummodel);
        }
        if (true) goto _L2; else goto _L1
_L1:
        return arraylist;
        flag = false;
          goto _L3
    }

    private void initListener()
    {
        ((PullToRefreshListView)mListView).setOnRefreshListener(new _cls1());
        mFooterViewLoading.setOnClickListener(new _cls2());
        mReloadDataView.setOnClickListener(new _cls3());
        mListView.setOnScrollListener(new _cls4());
        mListView.setOnItemClickListener(new _cls5());
    }

    private void initView()
    {
        mHandler = new Handler();
        addAd();
        mDataList = new ArrayList();
        mAdapter = new FeedCollectAlbumListAdapter(getActivity(), mDataList, this);
        mListView.setAdapter(mAdapter);
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
    }

    private void insertOrReplaceAd(List list)
    {
        while (list == null || list.size() <= 0 || mAds == null || mAds.size() <= 0) 
        {
            return;
        }
        ArrayList arraylist = new ArrayList();
        Iterator iterator = mAds.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj = (FeedAd2)iterator.next();
            if (((FeedAd2) (obj)).position >= 0 && ((FeedAd2) (obj)).position < list.size())
            {
                if (((AlbumModel)list.get(((FeedAd2) (obj)).position)).type == 1)
                {
                    list.set(((FeedAd2) (obj)).position, AlbumModel.from(((FeedAd2) (obj))));
                } else
                {
                    list.add(((FeedAd2) (obj)).position, AlbumModel.from(((FeedAd2) (obj))));
                }
                arraylist.add(obj);
                ThirdAdStatUtil.getInstance().thirdAdStatRequest(((FeedAd2) (obj)).thirdStatUrl);
                obj = AdManager.getInstance().getAdIdFromUrl(((FeedAd2) (obj)).link);
                if (!TextUtils.isEmpty(((CharSequence) (obj))))
                {
                    AdCollectData adcollectdata = new AdCollectData();
                    adcollectdata.setAdItemId(((String) (obj)));
                    adcollectdata.setAdSource("0");
                    adcollectdata.setAndroidId(ToolUtil.getAndroidId(mCon.getApplicationContext()));
                    adcollectdata.setLogType("tingShow");
                    adcollectdata.setPositionName("feed_collect_list");
                    adcollectdata.setResponseId(((String) (obj)));
                    adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                    adcollectdata.setTrackId("-1");
                    DataCollectUtil.getInstance(mCon.getApplicationContext()).statOnlineAd(adcollectdata);
                }
            }
        } while (true);
        mAds.removeAll(arraylist);
    }

    private void loadDataListData()
    {
        if (!UserInfoMannage.hasLogined())
        {
            break MISSING_BLOCK_LABEL_74;
        }
        if (!ToolUtil.isConnectToNetwork(getActivity())) goto _L2; else goto _L1
_L1:
        showNoNetworkLayout(false);
        mDataLoadTask = (new _cls7()).myexec(new Void[0]);
_L4:
        return;
_L2:
        ((PullToRefreshListView)mListView).onRefreshComplete();
        if (mAdapter != null && mAdapter.getCount() > 0) goto _L4; else goto _L3
_L3:
        showNoNetworkLayout(true);
        return;
        loadLocal();
        return;
    }

    private void loadLocal()
    {
        (new _cls8()).myexec(new Void[0]);
    }

    private void loadRecommendNoLogin()
    {
        f.a().a("mobile/album/recommend/list/unlogin", new RequestParams(), DataCollectUtil.getDataFromView(fragmentBaseContainerView), new _cls6());
    }

    public static FeedCollectFragment newInstance()
    {
        return new FeedCollectFragment();
    }

    private void parseLocal(List list)
    {
        if (list != null && list.size() > 0)
        {
            if (mDataList == null)
            {
                mDataList = new ArrayList(list);
            } else
            {
                mDataList.clear();
                mDataList.addAll(list);
            }
        } else
        {
            mDataList.clear();
        }
        if (mDataList.size() <= 10)
        {
            mAdapter.notifyDataSetChanged();
            if (NetworkUtils.isNetworkAvaliable(getActivity()))
            {
                loadRecommendNoLogin();
                return;
            }
            ((PullToRefreshListView)mListView).onRefreshComplete();
            if (mDataList.size() == 0)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            } else
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
        } else
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            ((PullToRefreshListView)mListView).onRefreshComplete();
            mAdapter.notifyDataSetChanged();
            return;
        }
    }

    private void showNoNetworkLayout(boolean flag)
    {
        byte byte0;
        for (byte0 = 8; !canGoon() || mDataList != null && !mDataList.isEmpty();)
        {
            return;
        }

        Object obj = mReloadDataView;
        int i;
        if (flag)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        ((View) (obj)).setVisibility(i);
        obj = mListView;
        if (flag)
        {
            i = byte0;
        } else
        {
            i = 0;
        }
        ((ListView) (obj)).setVisibility(i);
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a005c);
        super.initCommon();
    }

    public void initData()
    {
        if (getUserVisibleHint() && getView() != null)
        {
            if (isNeedRefresh)
            {
                doLoad();
                return;
            }
            if (!mIsLoadedData)
            {
                mIsLoadedData = true;
                ((PullToRefreshListView)mListView).toRefreshing();
                return;
            }
            if (mAdapter == null)
            {
                mAdapter = new FeedCollectAlbumListAdapter(getActivity(), mDataList, this);
                mListView.setAdapter(mAdapter);
            } else
            {
                insertOrReplaceAd(mDataList);
            }
            mAdapter.notifyDataSetChanged();
            ((PullToRefreshListView)mListView).onRefreshComplete();
            if (mBannerAdController != null)
            {
                mBannerAdController.swapAd();
                return;
            }
        }
    }

    public void loadAd()
    {
        if (mIsloadingAd)
        {
            return;
        } else
        {
            getLoaderManager().restartLoader(0x7f0a0030, null, this);
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        ((SlideView)findViewById(0x7f0a030f)).setSlide(false);
        mListView = (PullToRefreshListView)fragmentBaseContainerView.findViewById(0x7f0a005c);
        initView();
        initListener();
        initData();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mAlbumCollectDataChangeCallback = new _cls9();
        AlbumModelManage.getInstance().addDataChangedCallback(mAlbumCollectDataChangeCallback);
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        mIsloadingAd = true;
        return new com.ximalaya.ting.android.model.ad.FeedAd2.Loader(getActivity(), "feed_collect_list");
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300b5, viewgroup, false);
        mReloadDataView = fragmentBaseContainerView.findViewById(0x7f0a0351);
        return fragmentBaseContainerView;
    }

    public void onDestroy()
    {
        super.onDestroy();
        if (mAlbumCollectDataChangeCallback != null)
        {
            AlbumModelManage.getInstance().removeDataChangedCallback(mAlbumCollectDataChangeCallback);
            mAlbumCollectDataChangeCallback = null;
        }
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        mIsFistResumed = false;
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        mIsloadingAd = false;
        mAds = list;
        if (mPullToRefresh)
        {
            mPullToRefresh = false;
            doLoad();
            return;
        } else
        {
            initData();
            return;
        }
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onPause()
    {
        if (mBannerAdController != null)
        {
            mBannerAdController.stopSwapAd();
        }
        super.onPause();
        if (mThirdBannerAd != null)
        {
            mThirdBannerAd.pause();
        }
    }

    public void onRefresh()
    {
        if (!loadingNextPage)
        {
            showNoNetworkLayout(false);
            ((PullToRefreshListView)mListView).onRefreshComplete();
            ((PullToRefreshListView)mListView).toRefreshing();
            mListView.setSelection(0);
        }
    }

    public void onResume()
    {
        super.onResume();
        setPlayPath("play_Subscribe");
        if (mBannerAdController != null)
        {
            mBannerAdController.swapAd();
        }
        if (isNeedRefresh)
        {
            isNeedRefresh = false;
            onRefresh();
        }
        if (mThirdBannerAd != null)
        {
            mThirdBannerAd.resume();
        }
        if (mIsFistResumed)
        {
            if (getUserVisibleHint())
            {
                loadAd();
            }
            return;
        } else
        {
            mIsFistResumed = true;
            return;
        }
    }

    public void refreshLocalIfNeed()
    {
        if (!UserInfoMannage.hasLogined() && mIsCollectAlbumDataChanged)
        {
            loadLocal();
            mIsCollectAlbumDataChanged = false;
        }
    }

    public void setUserVisibleHint(boolean flag)
    {
        super.setUserVisibleHint(flag);
        initData();
    }

    public void showLoading(boolean flag)
    {
label0:
        {
            if (canGoon())
            {
                if (!flag)
                {
                    break label0;
                }
                findViewById(0x7f0a0091).setVisibility(0);
            }
            return;
        }
        findViewById(0x7f0a0091).setVisibility(8);
    }




/*
    static boolean access$002(FeedCollectFragment feedcollectfragment, boolean flag)
    {
        feedcollectfragment.loadingNextPage = flag;
        return flag;
    }

*/



/*
    static boolean access$102(FeedCollectFragment feedcollectfragment, boolean flag)
    {
        feedcollectfragment.mPullToRefresh = flag;
        return flag;
    }

*/








/*
    static boolean access$1702(FeedCollectFragment feedcollectfragment, boolean flag)
    {
        feedcollectfragment.mIsCollectAlbumDataChanged = flag;
        return flag;
    }

*/




/*
    static int access$302(FeedCollectFragment feedcollectfragment, int i)
    {
        feedcollectfragment.pageId = i;
        return i;
    }

*/


/*
    static int access$308(FeedCollectFragment feedcollectfragment)
    {
        int i = feedcollectfragment.pageId;
        feedcollectfragment.pageId = i + 1;
        return i;
    }

*/




/*
    static int access$502(FeedCollectFragment feedcollectfragment, int i)
    {
        feedcollectfragment.totalCount = i;
        return i;
    }

*/





    private class _cls1
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final FeedCollectFragment this$0;

        public void onRefresh()
        {
            if (!loadingNextPage)
            {
                mPullToRefresh = true;
                clearAlbumDot();
                pageId = 1;
                loadAd();
            }
        }

        _cls1()
        {
            this$0 = FeedCollectFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final FeedCollectFragment this$0;

        public void onClick(View view)
        {
            if (!loadingNextPage)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                onRefresh();
            }
        }

        _cls2()
        {
            this$0 = FeedCollectFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final FeedCollectFragment this$0;

        public void onClick(View view)
        {
            onRefresh();
        }

        _cls3()
        {
            this$0 = FeedCollectFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AbsListView.OnScrollListener
    {

        final FeedCollectFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            ((PullToRefreshListView)mListView).onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0)
            {
                i = abslistview.getCount();
                if (i > 5)
                {
                    i -= 5;
                } else
                {
                    i--;
                }
                if (abslistview.getLastVisiblePosition() > i && (pageId - 1) * pageSize < totalCount && (mDataLoadTask == null || mDataLoadTask.getStatus() != android.os.AsyncTask.Status.RUNNING) && !loadingNextPage)
                {
                    loadDataListData();
                }
            }
        }

        _cls4()
        {
            this$0 = FeedCollectFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.widget.AdapterView.OnItemClickListener
    {

        final FeedCollectFragment this$0;

        public void onItemClick(AdapterView adapterview, final View arg1, int i, long l)
        {
            if (OneClickHelper.getInstance().onClick(arg1) && mDataList != null && mDataList.size() != 0 && mDataList.size() > i - mListView.getHeaderViewsCount())
            {
                Object obj = arg1.findViewById(0x7f0a0156);
                adapterview = (AlbumModel)mDataList.get(i - mListView.getHeaderViewsCount());
                if (((AlbumModel) (adapterview)).type == 1)
                {
                    FeedAd2.handleClick(FeedCollectFragment.this, (FeedAd2)((AlbumModel) (adapterview)).tag, "feed_collect_list");
                    return;
                }
                if (!((AlbumModel) (adapterview)).isRecommendTitle)
                {
                    byte byte0;
                    if (((AlbumModel) (adapterview)).isRecommend)
                    {
                        ToolUtil.onEvent(getActivity(), "Collection_Recommend");
                    } else
                    {
                        ToolUtil.onEvent(getActivity(), "Collection_Attention");
                    }
                    if (((View) (obj)).getVisibility() == 0)
                    {
                        ((View) (obj)).setVisibility(8);
                        adapterview.hasNew = false;
                        class _cls1 extends MyAsyncTask
                        {

                            final _cls5 this$1;
                            final View val$arg1;

                            protected volatile Object doInBackground(Object aobj[])
                            {
                                return doInBackground((Long[])aobj);
                            }

                            protected transient String doInBackground(Long along[])
                            {
                                long l1 = along[0].longValue();
                                along = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/subscribe/check").toString();
                                RequestParams requestparams = new RequestParams();
                                requestparams.put("albumId", (new StringBuilder()).append(l1).append("").toString());
                                return f.a().b(along, requestparams, arg1, null);
                            }

                            protected volatile void onPostExecute(Object obj1)
                            {
                                onPostExecute((String)obj1);
                            }

                            protected void onPostExecute(String s)
                            {
                                if (!Utilities.isBlank(s)) goto _L2; else goto _L1
_L1:
                                return;
_L2:
                                if (JSON.parseObject(s).getIntValue("ret") != 0) goto _L1; else goto _L3
_L3:
                                mAdapter.notifyDataSetChanged();
                                return;
                                s;
                                s.printStackTrace();
                                return;
                            }

                _cls1()
                {
                    this$1 = _cls5.this;
                    arg1 = view;
                    super();
                }
                        }

                        (new _cls1()).myexec(new Long[] {
                            Long.valueOf(((AlbumModel) (adapterview)).albumId)
                        });
                    }
                    obj = new Bundle();
                    ((Bundle) (obj)).putString("album", JSON.toJSONString(adapterview));
                    ((Bundle) (obj)).putString("rec_src", ((AlbumModel) (adapterview)).recSrc);
                    ((Bundle) (obj)).putString("rec_track", ((AlbumModel) (adapterview)).recTrack);
                    if (((AlbumModel) (adapterview)).isRecommend)
                    {
                        byte0 = 11;
                    } else
                    {
                        byte0 = 1;
                    }
                    ((Bundle) (obj)).putInt("from", byte0);
                    ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(arg1, "\u63A8\u8350\u6536\u85CF", (new StringBuilder()).append("").append(i - mListView.getHeaderViewsCount()).toString()));
                    startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj)));
                    return;
                }
            }
        }

        _cls5()
        {
            this$0 = FeedCollectFragment.this;
            super();
        }
    }


    private class _cls7 extends MyAsyncTask
    {

        final FeedCollectFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/subscribe/recommend/list").toString();
            RequestParams requestparams = new RequestParams();
            requestparams.put("pageId", (new StringBuilder()).append(pageId).append("").toString());
            requestparams.put("pageSize", (new StringBuilder()).append(pageSize).append("").toString());
            avoid = f.a().a(avoid, requestparams, mListView, mListView);
            Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
            Object obj = JSON.parseObject(avoid);
            if (obj == null)
            {
                return null;
            }
            avoid = ((JSONObject) (obj)).get("ret").toString();
            totalCount = ((JSONObject) (obj)).getIntValue("totalCount");
            if (!"0".equals(avoid))
            {
                break MISSING_BLOCK_LABEL_338;
            }
            avoid = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/album/AlbumModel);
            try
            {
                obj = JSON.parseArray(((JSONObject) (obj)).getString("frDatas"), com/ximalaya/ting/android/model/album/AlbumModel);
                break MISSING_BLOCK_LABEL_205;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
              goto _L1
            obj;
            avoid = null;
_L1:
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
            obj = null;
            Object obj1 = avoid;
            if (avoid == null)
            {
                obj1 = new ArrayList();
            }
            if (obj != null && !((List) (obj)).isEmpty())
            {
                avoid = new AlbumModel();
                avoid.isRecommendTitle = true;
                ((List) (obj1)).add(avoid);
                for (avoid = ((List) (obj)).iterator(); avoid.hasNext();)
                {
                    ((AlbumModel)avoid.next()).isRecommend = true;
                }

                ((List) (obj1)).addAll(((java.util.Collection) (obj)));
            }
            return ((List) (obj1));
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(final List result)
        {
            loadingNextPage = false;
            if (isAdded())
            {
                ((PullToRefreshListView)mListView).onRefreshComplete();
                if (result == null)
                {
                    showNoNetworkLayout(true);
                    return;
                }
                if (result.isEmpty())
                {
                    if (mDataList.isEmpty())
                    {
                        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                        return;
                    }
                } else
                {
                    class _cls1
                        implements MyCallback
                    {

                        final _cls7 this$1;
                        final List val$result;

                        public void execute()
                        {
                            if (pageId == 1)
                            {
                                mDataList.clear();
                                mDataList.addAll(result);
                                insertOrReplaceAd(mDataList);
                                mAdapter.notifyDataSetChanged();
                            } else
                            {
                                mDataList.addAll(result);
                                insertOrReplaceAd(mDataList);
                                mAdapter.notifyDataSetChanged();
                            }
                            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                            int i = 
// JavaClassFileOutputException: get_constant: invalid tag

                _cls1()
                {
                    this$1 = _cls7.this;
                    result = list;
                    super();
                }
                    }

                    doAfterAnimation(new _cls1());
                    return;
                }
            }
        }

        protected void onPreExecute()
        {
            showNoNetworkLayout(false);
            if (pageId > 1)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
            }
            loadingNextPage = true;
        }

        _cls7()
        {
            this$0 = FeedCollectFragment.this;
            super();
        }
    }


    private class _cls8 extends MyAsyncTask
    {

        final FeedCollectFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            return AlbumModelManage.getInstance().getLocalCollectAlbumList();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(final List data)
        {
            if (!canGoon())
            {
                return;
            } else
            {
                insertOrReplaceAd(data);
                class _cls1
                    implements MyCallback
                {

                    final _cls8 this$1;
                    final List val$data;

                    public void execute()
                    {
                        parseLocal(data);
                    }

                _cls1()
                {
                    this$1 = _cls8.this;
                    data = list;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                return;
            }
        }

        protected void onPreExecute()
        {
            showNoNetworkLayout(false);
        }

        _cls8()
        {
            this$0 = FeedCollectFragment.this;
            super();
        }
    }


    private class _cls6 extends com.ximalaya.ting.android.b.a
    {

        final FeedCollectFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            ((PullToRefreshListView)mListView).onRefreshComplete();
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(final String tList)
        {
            Object obj = null;
            if (!TextUtils.isEmpty(tList))
            {
                List list;
                try
                {
                    tList = JSON.parseObject(tList);
                }
                // Misplaced declaration of an exception variable
                catch (final String tList)
                {
                    tList.printStackTrace();
                    tList = null;
                }
                list = obj;
                if (tList != null)
                {
                    list = obj;
                    if (tList.getIntValue("ret") == 0)
                    {
                        tList = tList.getString("frDatas");
                        class _cls1
                            implements MyCallback
                        {

                            final _cls6 this$1;
                            final List val$tList;

                            public void execute()
                            {
                                AlbumModel albummodel = new AlbumModel();
                                albummodel.isRecommendTitle = true;
                                mDataList.add(albummodel);
                                mDataList.addAll(tList);
                                mAdapter.notifyDataSetChanged();
                            }

                _cls1()
                {
                    this$1 = _cls6.this;
                    tList = list;
                    super();
                }
                        }

                        try
                        {
                            list = JSON.parseArray(tList, com/ximalaya/ting/android/model/album/AlbumModel);
                        }
                        // Misplaced declaration of an exception variable
                        catch (final String tList)
                        {
                            tList.printStackTrace();
                            list = obj;
                        }
                    }
                }
                tList = getUnFollowList(list, mDataList);
                if (!tList.isEmpty())
                {
                    doAfterAnimation(new _cls1());
                    return;
                }
            }
        }

        _cls6()
        {
            this$0 = FeedCollectFragment.this;
            super();
        }
    }


    private class _cls9
        implements com.ximalaya.ting.android.modelmanage.AlbumModelManage.OnDataChangedCallback
    {

        final FeedCollectFragment this$0;

        public void onDataChanged()
        {
            mIsCollectAlbumDataChanged = true;
        }

        _cls9()
        {
            this$0 = FeedCollectFragment.this;
            super();
        }
    }

}
