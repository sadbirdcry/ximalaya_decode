// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.ting:
//            CollectFragment

class val.view extends MyAsyncTask
{

    final CollectFragment this$0;
    final View val$view;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/subscribe/list").toString();
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("pageId", (new StringBuilder()).append(CollectFragment.access$300(CollectFragment.this)).append("").toString());
        ((RequestParams) (obj)).put("pageSize", (new StringBuilder()).append(CollectFragment.access$700(CollectFragment.this)).append("").toString());
        obj = f.a().a(avoid, ((RequestParams) (obj)), val$view, CollectFragment.access$400(CollectFragment.this));
        Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
        avoid = null;
        try
        {
            obj = JSON.parseObject(((String) (obj)));
            String s = ((JSONObject) (obj)).get("ret").toString();
            CollectFragment.access$802(CollectFragment.this, ((JSONObject) (obj)).getIntValue("totalCount"));
            if ("0".equals(s))
            {
                avoid = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/album/AlbumModel);
            }
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
            return null;
        }
        return avoid;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(final List result)
    {
        if (!canGoon())
        {
            return;
        }
        CollectFragment.access$400(CollectFragment.this).onRefreshComplete();
        CollectFragment.access$202(CollectFragment.this, false);
        if (result == null)
        {
            Log.d("CollectAlbum", "loadDataListData");
            if (CollectFragment.access$1000(CollectFragment.this).size() > 0)
            {
                showFooterView(oterView.NO_DATA);
            }
            if (CollectFragment.access$1000(CollectFragment.this).size() == 0)
            {
                CollectFragment.access$1300(CollectFragment.this, true);
                return;
            } else
            {
                CollectFragment.access$1400(CollectFragment.this).findViewById(0x7f0a011a).setVisibility(8);
                CollectFragment.access$1400(CollectFragment.this).findViewById(0x7f0a0231).setVisibility(8);
                CollectFragment.access$1400(CollectFragment.this).findViewById(0x7f0a0232).setVisibility(8);
                CollectFragment.access$1400(CollectFragment.this).setVisibility(8);
                return;
            }
        } else
        {
            class _cls1
                implements MyCallback
            {

                final CollectFragment._cls7 this$1;
                final List val$result;

                public void execute()
                {
                    if (CollectFragment.access$300(this$0) == 1)
                    {
                        CollectFragment.access$1000(this$0).clear();
                        CollectFragment.access$1000(this$0).addAll(result);
                        CollectFragment.access$1100(this$0).notifyDataSetChanged();
                    } else
                    {
                        CollectFragment.access$1000(this$0).addAll(result);
                        CollectFragment.access$1100(this$0).notifyDataSetChanged();
                    }
                    if (CollectFragment.access$1500(this$0))
                    {
                        showFooterView(CollectFragment.FooterView.MORE);
                    } else
                    {
                        showFooterView(CollectFragment.FooterView.HIDE_ALL);
                    }
                    if (CollectFragment.access$1000(this$0).size() == 0)
                    {
                        CollectFragment.access$1400(this$0).setVisibility(0);
                    } else
                    {
                        CollectFragment.access$1400(this$0).setVisibility(8);
                    }
                    int _tmp = CollectFragment.access$308(this$0);
                }

            _cls1()
            {
                this$1 = CollectFragment._cls7.this;
                result = list;
                super();
            }
            }

            CollectFragment.access$1600(CollectFragment.this, new _cls1());
            return;
        }
    }

    protected void onPreExecute()
    {
        CollectFragment.access$202(CollectFragment.this, true);
        CollectFragment.access$1300(CollectFragment.this, false);
    }

    _cls1()
    {
        this$0 = final_collectfragment;
        val$view = View.this;
        super();
    }
}
