// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.ting;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.library.model.BaseAdModel;
import com.ximalaya.ting.android.model.ad.NoPageAd;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.RoundedImageView;

public class AdImageFragment extends Fragment
{

    private BaseAdModel mAppAd;
    private String positionName;

    public AdImageFragment()
    {
    }

    public AdImageFragment(BaseAdModel baseadmodel, String s)
    {
        mAppAd = baseadmodel;
        positionName = s;
    }

    private void downloadFile(String s)
    {
        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(s, new _cls1());
    }

    private View getFullImg()
    {
        RoundedImageView roundedimageview = new RoundedImageView(getActivity());
        roundedimageview.setCornerRadius(ToolUtil.dp2px(getActivity(), 0.0F));
        roundedimageview.setRoundBackground(true);
        roundedimageview.setScaleType(android.widget.ImageView.ScaleType.FIT_XY);
        roundedimageview.setHasPressDownShade(false);
        roundedimageview.setTag(0x7f0a0037, Boolean.valueOf(true));
        if (mAppAd != null)
        {
            ImageManager2.from(getActivity()).displayImage(roundedimageview, mAppAd.getICover(), 0x7f020011);
        }
        roundedimageview.setOnClickListener(new _cls4());
        return roundedimageview;
    }

    private View getTxtWithLargeImg()
    {
        View view = LayoutInflater.from(getActivity()).inflate(0x7f03010b, null);
        if (mAppAd != null)
        {
            NoPageAd nopagead = (NoPageAd)mAppAd;
            TextView textview = (TextView)view.findViewById(0x7f0a0449);
            Object obj;
            if (nopagead.getName() == null)
            {
                obj = "";
            } else
            {
                obj = nopagead.getName();
            }
            textview.setText(((CharSequence) (obj)));
            textview = (TextView)view.findViewById(0x7f0a044a);
            if (nopagead.getDescription() == null)
            {
                obj = "";
            } else
            {
                obj = nopagead.getDescription();
            }
            textview.setText(((CharSequence) (obj)));
            obj = (ImageView)view.findViewById(0x7f0a0177);
            ImageManager2.from(getActivity()).displayImage(((ImageView) (obj)), mAppAd.getICover(), 0x7f020011);
            view.setOnClickListener(new _cls2());
        }
        return view;
    }

    private View getTxtWithSmallImg()
    {
        View view = LayoutInflater.from(getActivity()).inflate(0x7f03010c, null);
        if (mAppAd != null)
        {
            NoPageAd nopagead = (NoPageAd)mAppAd;
            TextView textview = (TextView)view.findViewById(0x7f0a0449);
            Object obj;
            if (nopagead.getName() == null)
            {
                obj = "";
            } else
            {
                obj = nopagead.getName();
            }
            textview.setText(((CharSequence) (obj)));
            textview = (TextView)view.findViewById(0x7f0a044a);
            if (nopagead.getDescription() == null)
            {
                obj = "";
            } else
            {
                obj = nopagead.getDescription();
            }
            textview.setText(((CharSequence) (obj)));
            obj = (ImageView)view.findViewById(0x7f0a0177);
            ImageManager2.from(getActivity()).displayImage(((ImageView) (obj)), mAppAd.getICover(), 0x7f020011);
            view.setOnClickListener(new _cls3());
        }
        return view;
    }

    private void goToWeb(String s)
    {
        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(s, new _cls5());
    }

    private void handleClick()
    {
        if (mAppAd.getILinkType() == 1 || mAppAd.getILinkType() == 0)
        {
            goToWeb(mAppAd.getILink());
        } else
        if (mAppAd.getILinkType() == 2)
        {
            downloadFile(mAppAd.getILink());
            return;
        }
    }

    public String getCover()
    {
        if (mAppAd == null)
        {
            return "";
        } else
        {
            return mAppAd.getICover();
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        if (!(mAppAd instanceof NoPageAd)) goto _L2; else goto _L1
_L1:
        ((NoPageAd)mAppAd).getShowstyle();
        JVM INSTR tableswitch 1 3: default 48
    //                   1 53
    //                   2 58
    //                   3 63;
           goto _L2 _L3 _L4 _L5
_L2:
        return getFullImg();
_L3:
        return getFullImg();
_L4:
        return getTxtWithSmallImg();
_L5:
        return getTxtWithLargeImg();
    }



    private class _cls1
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final AdImageFragment this$0;

        public void execute(final String url)
        {
            if (getActivity().isFinishing())
            {
                return;
            } else
            {
                class _cls1
                    implements Runnable
                {

                    final _cls1 this$1;
                    final String val$url;

                    public void run()
                    {
                        Object obj = new AdCollectData();
                        Object obj1 = AdManager.getInstance().getAdIdFromUrl(url);
                        ((AdCollectData) (obj)).setAdItemId(((String) (obj1)));
                        ((AdCollectData) (obj)).setAdSource("0");
                        ((AdCollectData) (obj)).setAndroidId(ToolUtil.getAndroidId(getActivity().getApplicationContext()));
                        ((AdCollectData) (obj)).setLogType("tingClick");
                        ((AdCollectData) (obj)).setPositionName(positionName);
                        ((AdCollectData) (obj)).setResponseId(((String) (obj1)));
                        ((AdCollectData) (obj)).setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                        ((AdCollectData) (obj)).setTrackId("-1");
                        obj = AdManager.getInstance().getAdRealJTUrl(url, ((AdCollectData) (obj)));
                        obj1 = new Intent(getActivity(), com/ximalaya/ting/android/library/service/DownloadService);
                        ((Intent) (obj1)).putExtra("download_url", ((String) (obj)));
                        getActivity().startService(((Intent) (obj1)));
                    }

                _cls1()
                {
                    this$1 = _cls1.this;
                    url = s;
                    super();
                }
                }

                getActivity().runOnUiThread(new _cls1());
                return;
            }
        }

        _cls1()
        {
            this$0 = AdImageFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final AdImageFragment this$0;

        public void onClick(View view)
        {
            handleClick();
        }

        _cls4()
        {
            this$0 = AdImageFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final AdImageFragment this$0;

        public void onClick(View view)
        {
            handleClick();
        }

        _cls2()
        {
            this$0 = AdImageFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final AdImageFragment this$0;

        public void onClick(View view)
        {
            handleClick();
        }

        _cls3()
        {
            this$0 = AdImageFragment.this;
            super();
        }
    }


    private class _cls5
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final AdImageFragment this$0;

        public void execute(final String url)
        {
            if (getActivity().isFinishing())
            {
                return;
            } else
            {
                class _cls1
                    implements Runnable
                {

                    final _cls5 this$1;
                    final String val$url;

                    public void run()
                    {
                        Object obj = new AdCollectData();
                        Object obj1 = AdManager.getInstance().getAdIdFromUrl(url);
                        ((AdCollectData) (obj)).setAdItemId(((String) (obj1)));
                        ((AdCollectData) (obj)).setAdSource("0");
                        ((AdCollectData) (obj)).setAndroidId(ToolUtil.getAndroidId(getActivity().getApplicationContext()));
                        ((AdCollectData) (obj)).setLogType("tingClick");
                        ((AdCollectData) (obj)).setPositionName(positionName);
                        ((AdCollectData) (obj)).setResponseId(((String) (obj1)));
                        ((AdCollectData) (obj)).setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                        ((AdCollectData) (obj)).setTrackId("-1");
                        obj = AdManager.getInstance().getAdRealJTUrl(url, ((AdCollectData) (obj)));
                        obj1 = new Intent(getActivity(), com/ximalaya/ting/android/activity/web/WebActivityNew);
                        ((Intent) (obj1)).putExtra("ExtraUrl", ((String) (obj)));
                        getActivity().startActivity(((Intent) (obj1)));
                    }

                _cls1()
                {
                    this$1 = _cls5.this;
                    url = s;
                    super();
                }
                }

                getActivity().runOnUiThread(new _cls1());
                return;
            }
        }

        _cls5()
        {
            this$0 = AdImageFragment.this;
            super();
        }
    }

}
