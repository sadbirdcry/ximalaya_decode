// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumFragment

private class computePagerIndex extends BaseAdapter
{

    private int mLastSize;
    private List mPagerIndexs;
    final AlbumFragment this$0;

    private void computePagerIndex()
    {
        int j = (int)Math.ceil((float)AlbumFragment.access$1600(AlbumFragment.this).ount / (float)AlbumFragment.access$1600(AlbumFragment.this).ze);
        for (int i = 0; i < j; i++)
        {
            computePagerIndex computepagerindex = new ze(AlbumFragment.this, null);
            computepagerindex.x = i + 1;
            computepagerindex.ex = AlbumFragment.access$1600(AlbumFragment.this).ze * i + 1;
            computepagerindex. = Math.min(AlbumFragment.access$1600(AlbumFragment.this).ze * (i + 1), AlbumFragment.access$1600(AlbumFragment.this).ount);
            computepagerindex.ng = (new StringBuilder()).append(computepagerindex.ex).append("~").append(computepagerindex.).toString();
            mPagerIndexs.add(computepagerindex);
        }

        mLastSize = AlbumFragment.access$1600(AlbumFragment.this).ount;
    }

    public int getCount()
    {
        return mPagerIndexs.size();
    }

    public Object getItem(int i)
    {
        return mPagerIndexs.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (view == null)
        {
            view = LayoutInflater.from(mCon).inflate(0x7f030006, AlbumFragment.access$3100(AlbumFragment.this), false);
        }
        viewgroup = (TextView)view;
        mPagerIndexs mpagerindexs = (this._cls0)mPagerIndexs.get(i);
        viewgroup.setText(mpagerindexs.ng);
        if (mpagerindexs.x == AlbumFragment.access$1600(AlbumFragment.this). - 1)
        {
            viewgroup.setBackgroundResource(0x7f02002c);
            viewgroup.setTextColor(Color.parseColor("#ffffff"));
            return view;
        } else
        {
            viewgroup.setBackgroundResource(0x7f02002b);
            viewgroup.setTextColor(Color.parseColor("#333333"));
            return view;
        }
    }

    public void notifyDataSetChanged()
    {
        if (mLastSize != AlbumFragment.access$1600(AlbumFragment.this).ount)
        {
            computePagerIndex();
        }
    }

    public ()
    {
        this$0 = AlbumFragment.this;
        super();
        mLastSize = 0;
        mPagerIndexs = new ArrayList();
        computePagerIndex();
    }
}
