// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.Context;
import android.view.View;
import android.widget.Toast;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.album.IAlbum;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            GeneralAlbumListApdater

final class val.context extends MyAsyncTask
{

    final Context val$context;
    final ewHolder val$holder;
    final AlbumModel val$m;
    final IAlbum val$model;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        avoid = AlbumModelManage.getInstance();
        IAlbum ialbum = val$model;
        boolean flag;
        if (!val$model.isIAlbumCollect())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        ialbum.setIAlbumCollect(flag);
        val$m.lastUptrackAt = val$model.getIAlbumLastUpdateAt();
        val$m.tracks = (int)val$model.getIAlbumTrackCount();
        val$m.coverSmall = val$model.getIAlbumCoverUrl();
        val$m.title = val$model.getIAlbumTitle();
        if (!val$model.isIAlbumCollect())
        {
            avoid.deleteAlbumInLocalAlbumList(val$m);
        } else
        {
            avoid.saveAlbumModel(val$m);
        }
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        if (((IAlbum)val$holder.collectLayout.getTag(0x7f090000)).getIAlbumId() == val$model.getIAlbumId())
        {
            GeneralAlbumListApdater.access$100(val$holder, val$model.isIAlbumCollect());
        }
        if (val$model.isIAlbumCollect())
        {
            void1 = "\u6536\u85CF\u6210\u529F\uFF01";
        } else
        {
            void1 = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
        }
        Toast.makeText(val$context, void1, 0).show();
    }

    ewHolder()
    {
        val$model = ialbum;
        val$m = albummodel;
        val$holder = ewholder;
        val$context = context1;
        super();
    }
}
