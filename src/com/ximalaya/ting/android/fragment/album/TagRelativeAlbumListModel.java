// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.model.Collectable;
import com.ximalaya.ting.android.model.album.IAlbum;
import java.util.List;

public class TagRelativeAlbumListModel
{
    public static class AlbumItem
        implements Collectable, IAlbum
    {

        private String albumCoverUrl290;
        private long id;
        private String intro;
        private boolean isCollected;
        private long lastUptrackAt;
        private long playsCounts;
        private int serialState;
        private String tags;
        private String title;
        private long trackCounts;

        public String getAlbumCoverUrl290()
        {
            return albumCoverUrl290;
        }

        public long getCId()
        {
            return id;
        }

        public String getIAlbumCoverUrl()
        {
            return albumCoverUrl290;
        }

        public long getIAlbumId()
        {
            return id;
        }

        public long getIAlbumLastUpdateAt()
        {
            return lastUptrackAt;
        }

        public long getIAlbumPlayCount()
        {
            return playsCounts;
        }

        public String getIAlbumTag()
        {
            return tags;
        }

        public String getIAlbumTitle()
        {
            return title;
        }

        public long getIAlbumTrackCount()
        {
            return trackCounts;
        }

        public String getIIntro()
        {
            return intro;
        }

        public long getId()
        {
            return id;
        }

        public String getIntro()
        {
            return intro;
        }

        public long getLastUptrackAt()
        {
            return lastUptrackAt;
        }

        public long getPlaysCounts()
        {
            return playsCounts;
        }

        public int getSerialState()
        {
            return serialState;
        }

        public String getTags()
        {
            return tags;
        }

        public String getTitle()
        {
            return title;
        }

        public long getTrackCounts()
        {
            return trackCounts;
        }

        public boolean isCCollected()
        {
            return isCollected;
        }

        public boolean isCollected()
        {
            return isCollected;
        }

        public boolean isIAlbumCollect()
        {
            return isCollected;
        }

        public void setAlbumCoverUrl290(String s)
        {
            albumCoverUrl290 = s;
        }

        public void setCCollected(boolean flag)
        {
            isCollected = flag;
        }

        public void setCollected(boolean flag)
        {
            isCollected = flag;
        }

        public void setIAlbumCollect(boolean flag)
        {
            isCollected = flag;
        }

        public void setId(long l)
        {
            id = l;
        }

        public void setIntro(String s)
        {
            intro = s;
        }

        public void setLastUptrackAt(long l)
        {
            lastUptrackAt = l;
        }

        public void setPlaysCounts(long l)
        {
            playsCounts = l;
        }

        public void setSerialState(int i)
        {
            serialState = i;
        }

        public void setTags(String s)
        {
            tags = s;
        }

        public void setTitle(String s)
        {
            title = s;
        }

        public void setTrackCounts(long l)
        {
            trackCounts = l;
        }

        public AlbumItem()
        {
        }
    }


    private int count;
    private List list;
    private int maxPageId;

    public TagRelativeAlbumListModel()
    {
    }

    public static TagRelativeAlbumListModel getFromJson(String s)
    {
        return (TagRelativeAlbumListModel)JSONObject.parseObject(s, com/ximalaya/ting/android/fragment/album/TagRelativeAlbumListModel);
    }

    public int getCount()
    {
        return count;
    }

    public List getList()
    {
        return list;
    }

    public int getMaxPageId()
    {
        return maxPageId;
    }

    public void setCount(int i)
    {
        count = i;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setMaxPageId(int i)
    {
        maxPageId = i;
    }
}
