// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            GeneralAlbumListApdater, TagRelativeAlbumListLoader, TagRelativeAlbumListModel

public class TagRelativeAlbumListFragment extends BaseListFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener
{

    private static final int LOAD_COLLECT_STATUS = 1;
    private static final int LOAD_DATA = 0;
    private GeneralAlbumListApdater mAdapter;
    private android.support.v4.app.LoaderManager.LoaderCallbacks mAlbumStatusCallback;
    private boolean mIsLoading;
    private Loader mLoader;
    private View mNoNetLayout;
    private int mPageId;
    private Loader mStatusLoader;
    private String mTagName;
    private int mTotalCount;

    public TagRelativeAlbumListFragment()
    {
        mAlbumStatusCallback = new _cls5();
    }

    public static TagRelativeAlbumListFragment getInstance(String s)
    {
        Bundle bundle = new Bundle();
        bundle.putString("tag_name", s);
        s = new TagRelativeAlbumListFragment();
        s.setArguments(bundle);
        return s;
    }

    private void loadData()
    {
        mIsLoading = true;
        Bundle bundle = new Bundle();
        bundle.putString("tag_name", mTagName);
        int i = mPageId + 1;
        mPageId = i;
        bundle.putInt("page_id", i);
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(0, bundle, this);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(mListView, mListView);
            return;
        } else
        {
            mLoader = getLoaderManager().restartLoader(0, bundle, this);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(mListView, mListView);
            return;
        }
    }

    private void loadMoreData()
    {
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        loadData();
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a0025);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        if (!TextUtils.isEmpty(mTagName))
        {
            setTitleText(mTagName);
        }
        mAdapter = new GeneralAlbumListApdater(getActivity(), new ArrayList());
        mListView.setAdapter(mAdapter);
        mListView.setOnScrollListener(new _cls1());
        mListView.setOnItemClickListener(new _cls2());
        mNoNetLayout.setOnClickListener(new _cls3());
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        loadData();
    }

    public void onClick(View view)
    {
        view.getId();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mTagName = getArguments().getString("tag_name");
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        String s = bundle.getString("tag_name");
        i = bundle.getInt("page_id");
        return new TagRelativeAlbumListLoader(getActivity(), s, i);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300c0, viewgroup, false);
        mNoNetLayout = findViewById(0x7f0a035b);
        return fragmentBaseContainerView;
    }

    public void onLoadFinished(Loader loader, final TagRelativeAlbumListModel data)
    {
        doAfterAnimation(new _cls4());
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (TagRelativeAlbumListModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }



/*
    static GeneralAlbumListApdater access$002(TagRelativeAlbumListFragment tagrelativealbumlistfragment, GeneralAlbumListApdater generalalbumlistapdater)
    {
        tagrelativealbumlistfragment.mAdapter = generalalbumlistapdater;
        return generalalbumlistapdater;
    }

*/



/*
    static int access$102(TagRelativeAlbumListFragment tagrelativealbumlistfragment, int i)
    {
        tagrelativealbumlistfragment.mTotalCount = i;
        return i;
    }

*/



/*
    static boolean access$202(TagRelativeAlbumListFragment tagrelativealbumlistfragment, boolean flag)
    {
        tagrelativealbumlistfragment.mIsLoading = flag;
        return flag;
    }

*/






/*
    static Loader access$602(TagRelativeAlbumListFragment tagrelativealbumlistfragment, Loader loader)
    {
        tagrelativealbumlistfragment.mStatusLoader = loader;
        return loader;
    }

*/




/*
    static int access$806(TagRelativeAlbumListFragment tagrelativealbumlistfragment)
    {
        int i = tagrelativealbumlistfragment.mPageId - 1;
        tagrelativealbumlistfragment.mPageId = i;
        return i;
    }

*/

    private class _cls5
        implements android.support.v4.app.LoaderManager.LoaderCallbacks
    {

        final TagRelativeAlbumListFragment this$0;

        public Loader onCreateLoader(int i, Bundle bundle)
        {
            return new b(getActivity(), mAdapter.getData());
        }

        public volatile void onLoadFinished(Loader loader, Object obj)
        {
            onLoadFinished(loader, (List)obj);
        }

        public void onLoadFinished(Loader loader, List list)
        {
            mAdapter.notifyDataSetChanged();
        }

        public void onLoaderReset(Loader loader)
        {
        }

        _cls5()
        {
            this$0 = TagRelativeAlbumListFragment.this;
            super();
        }
    }


    private class _cls1
        implements android.widget.AbsListView.OnScrollListener
    {

        final TagRelativeAlbumListFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (mListView.getLastVisiblePosition() >= mListView.getCount() - 1 && i == 0 && mAdapter.getCount() < mTotalCount && !mIsLoading)
            {
                mIsLoading = true;
                loadMoreData();
            }
        }

        _cls1()
        {
            this$0 = TagRelativeAlbumListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final TagRelativeAlbumListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= mAdapter.getCount())
            {
                return;
            } else
            {
                adapterview = ModelHelper.toAlbumModel((TagRelativeAlbumListModel.AlbumItem)mAdapter.getData().get(i));
                Bundle bundle = new Bundle();
                bundle.putString("album", JSON.toJSONString(adapterview));
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
                return;
            }
        }

        _cls2()
        {
            this$0 = TagRelativeAlbumListFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final TagRelativeAlbumListFragment this$0;

        public void onClick(View view)
        {
            loadData();
        }

        _cls3()
        {
            this$0 = TagRelativeAlbumListFragment.this;
            super();
        }
    }


    private class _cls4
        implements MyCallback
    {

        final TagRelativeAlbumListFragment this$0;
        final TagRelativeAlbumListModel val$data;

        public void execute()
        {
            if (data != null && data.getList() != null && data.getList().size() > 0)
            {
                mNoNetLayout.setVisibility(8);
                mTotalCount = data.getCount();
                if (mAdapter == null)
                {
                    mAdapter = new GeneralAlbumListApdater(getActivity(), data.getList());
                    mListView.setAdapter(mAdapter);
                } else
                {
                    mAdapter.addData(data.getList());
                }
                if (mAdapter.getCount() > 0)
                {
                    if (mStatusLoader == null)
                    {
                        mStatusLoader = getLoaderManager().initLoader(1, null, mAlbumStatusCallback);
                    } else
                    {
                        mStatusLoader = getLoaderManager().restartLoader(1, null, mAlbumStatusCallback);
                    }
                }
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            } else
            {
                int i = 
// JavaClassFileOutputException: get_constant: invalid tag

        _cls4()
        {
            this$0 = TagRelativeAlbumListFragment.this;
            data = tagrelativealbumlistmodel;
            super();
        }
    }

}
