// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.album.AlbumSectionModel;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.MultiDirectionSlidingDrawer;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumSectionDownloadFragment, SectionAdapter, SectionContentAdapter

class val.data
    implements MyCallback
{

    final AlbumSectionDownloadFragment this$0;
    final AlbumSectionModel val$data;

    public void execute()
    {
        if (val$data != null && val$data.getTracks() != null && val$data.getTracks().List() != null && val$data.getTracks().List().size() > 0)
        {
            AlbumSectionDownloadFragment.access$1000(AlbumSectionDownloadFragment.this, true);
            if (AlbumSectionDownloadFragment.access$000(AlbumSectionDownloadFragment.this) == null)
            {
                AlbumSectionDownloadFragment.access$002(AlbumSectionDownloadFragment.this, new SectionAdapter(getActivity(), val$data.getTracks().TotalCount(), val$data.getTracks().PageSize()));
                AlbumSectionDownloadFragment.access$1100(AlbumSectionDownloadFragment.this).setAdapter(AlbumSectionDownloadFragment.access$000(AlbumSectionDownloadFragment.this));
                int i = AlbumSectionDownloadFragment.access$000(AlbumSectionDownloadFragment.this).getCount();
                int j = (getResources().getDisplayMetrics().widthPixels - Utilities.dip2px(getActivity(), 20F)) / Utilities.dip2px(getActivity(), 80F);
                TextView textview;
                AlbumSectionDownloadFragment albumsectiondownloadfragment;
                StringBuilder stringbuilder;
                if (i % j == 0)
                {
                    i /= j;
                } else
                {
                    i = i / j + 1;
                }
                i *= Utilities.dip2px(getActivity(), 48F);
                if (AlbumSectionDownloadFragment.access$1200(AlbumSectionDownloadFragment.this).getHeight() - Utilities.dip2px(getActivity(), 200F) > i)
                {
                    AlbumSectionDownloadFragment.access$1200(AlbumSectionDownloadFragment.this).setPullDownViewMarginBottom(AlbumSectionDownloadFragment.access$1200(AlbumSectionDownloadFragment.this).getHeight() - i - Utilities.dip2px(getActivity(), 20F));
                } else
                {
                    AlbumSectionDownloadFragment.access$1200(AlbumSectionDownloadFragment.this).setPullDownViewMarginBottom(Utilities.dip2px(getActivity(), 180F));
                }
            } else
            {
                AlbumSectionDownloadFragment.access$000(AlbumSectionDownloadFragment.this).reset(val$data.getTracks().TotalCount(), val$data.getTracks().PageSize());
            }
            if (mSectionContentAdapter == null)
            {
                AlbumSectionDownloadFragment.access$1300(AlbumSectionDownloadFragment.this).setAdapter(getContentAdapter(val$data));
            } else
            {
                mSectionContentAdapter.reset(val$data);
            }
            AlbumSectionDownloadFragment.access$1400(AlbumSectionDownloadFragment.this).setText(getString(0x7f0901f3, new Object[] {
                Integer.valueOf(val$data.getTracks().TotalCount())
            }));
            j = val$data.getTracks().PageSize() * AlbumSectionDownloadFragment.access$400(AlbumSectionDownloadFragment.this) + val$data.getTracks().PageSize();
            textview = AlbumSectionDownloadFragment.access$700(AlbumSectionDownloadFragment.this);
            albumsectiondownloadfragment = AlbumSectionDownloadFragment.this;
            stringbuilder = (new StringBuilder()).append(val$data.getTracks().PageSize() * AlbumSectionDownloadFragment.access$400(AlbumSectionDownloadFragment.this) + 1).append("~");
            i = j;
            if (j > val$data.getTracks().TotalCount())
            {
                i = val$data.getTracks().TotalCount();
            }
            textview.setText(albumsectiondownloadfragment.getString(0x7f0901f4, new Object[] {
                stringbuilder.append(i).toString()
            }));
        } else
        {
            AlbumSectionDownloadFragment.access$1000(AlbumSectionDownloadFragment.this, false);
            AlbumSectionDownloadFragment.access$1500(AlbumSectionDownloadFragment.this, true);
        }
        AlbumSectionDownloadFragment.access$302(AlbumSectionDownloadFragment.this, false);
        AlbumSectionDownloadFragment.access$500(AlbumSectionDownloadFragment.this).setChecked(false);
        AlbumSectionDownloadFragment.access$800(AlbumSectionDownloadFragment.this).setEnabled(false);
        updateBottomBarInfo();
        AlbumSectionDownloadFragment.access$600(AlbumSectionDownloadFragment.this);
        AlbumSectionDownloadFragment.access$1600(AlbumSectionDownloadFragment.this);
        if (AlbumSectionDownloadFragment.access$1200(AlbumSectionDownloadFragment.this) != null && AlbumSectionDownloadFragment.access$1200(AlbumSectionDownloadFragment.this).isShowing())
        {
            AlbumSectionDownloadFragment.access$1200(AlbumSectionDownloadFragment.this).closePullDownPanel();
        }
    }

    ()
    {
        this$0 = final_albumsectiondownloadfragment;
        val$data = AlbumSectionModel.this;
        super();
    }
}
