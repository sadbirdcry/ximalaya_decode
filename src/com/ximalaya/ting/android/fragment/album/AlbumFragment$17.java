// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.view.BlurableImageView;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumFragment

class this._cls0
    implements com.ximalaya.ting.android.util.playCallback
{

    final AlbumFragment this$0;

    public void onCompleteDisplay(String s, Bitmap bitmap)
    {
        if (mCon == null)
        {
            return;
        }
        Bitmap bitmap1 = ImageManager2.from(mCon).getFromMemCache((new StringBuilder()).append(s).append("/blur").toString());
        if (bitmap1 != null)
        {
            AlbumFragment.access$2500(AlbumFragment.this).setImageBitmap(bitmap1);
        } else
        if (bitmap != null)
        {
            AlbumFragment.access$2500(AlbumFragment.this).blur(new BitmapDrawable(mCon.getResources(), bitmap), (new StringBuilder()).append(s).append("/blur").toString(), true);
        } else
        {
            AlbumFragment.access$2500(AlbumFragment.this).setImageDrawable(new ColorDrawable(Color.parseColor("#b3202332")));
            AlbumFragment.access$2500(AlbumFragment.this).setResourceUrl(null);
        }
        AlbumFragment.access$2602(AlbumFragment.this, null);
    }

    ck()
    {
        this$0 = AlbumFragment.this;
        super();
    }
}
