// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.view.flowlayout.FlowLayout;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.Utilities;

public class AlbumIntroFragment extends BaseActivityLikeFragment
{

    private AlbumModel mData;

    public AlbumIntroFragment()
    {
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        if (mData != null)
        {
            setTitleText(mData.title);
            ((TextView)findViewById(0x7f0a0175)).setText(mData.title);
            ((TextView)findViewById(0x7f0a0378)).setText(mData.nickname);
            if (TextUtils.isEmpty(mData.introRich))
            {
                ((TextView)findViewById(0x7f0a046a)).setText(mData.intro);
            } else
            {
                ((TextView)findViewById(0x7f0a046a)).setText(Html.fromHtml(mData.introRich));
            }
            if (!TextUtils.isEmpty(mData.tags))
            {
                bundle = (FlowLayout)findViewById(0x7f0a0468);
                String as[] = mData.tags.split(",");
                int j = as.length;
                for (int i = 0; i < j; i++)
                {
                    final String tag = as[i];
                    TextView textview = new TextView(getActivity());
                    textview.setBackgroundResource(0x7f020580);
                    textview.setTextSize(13F);
                    textview.setTextColor(Color.parseColor("#394257"));
                    textview.setText(tag);
                    com.ximalaya.ting.android.library.view.flowlayout.FlowLayout.LayoutParams layoutparams = new com.ximalaya.ting.android.library.view.flowlayout.FlowLayout.LayoutParams(-2, -2);
                    layoutparams.leftMargin = Utilities.dip2px(getActivity(), 10F);
                    layoutparams.bottomMargin = Utilities.dip2px(getActivity(), 10F);
                    bundle.addView(textview, layoutparams);
                    textview.setOnClickListener(new _cls1());
                }

            }
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mData = (AlbumModel)getArguments().getSerializable("data");
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f03003c, viewgroup, false);
        return fragmentBaseContainerView;
    }

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final AlbumIntroFragment this$0;
        final String val$tag;

        public void onClick(View view)
        {
            TagRelativeAlbumListFragment tagrelativealbumlistfragment = TagRelativeAlbumListFragment.getInstance(tag);
            Bundle bundle = tagrelativealbumlistfragment.getArguments();
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            tagrelativealbumlistfragment.setArguments(bundle);
            startFragment(tagrelativealbumlistfragment);
        }

        _cls1()
        {
            this$0 = AlbumIntroFragment.this;
            tag = s;
            super();
        }
    }

}
