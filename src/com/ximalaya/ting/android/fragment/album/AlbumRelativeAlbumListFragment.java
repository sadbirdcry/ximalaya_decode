// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.ximalaya.ting.android.c.b;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumRelativeModelLoader, GeneralAlbumListApdater

public class AlbumRelativeAlbumListFragment extends BaseListFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks
{
    private class InternalAdapter extends GeneralAlbumListApdater
    {

        final AlbumRelativeAlbumListFragment this$0;

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            return super.getView(i, view, viewgroup);
        }

        public InternalAdapter(Context context, List list)
        {
            this$0 = AlbumRelativeAlbumListFragment.this;
            super(context, list);
        }
    }


    private static final int LOAD_COLLECT_STATUS = 1;
    private static final int LOAD_DATA = 0;
    private GeneralAlbumListApdater mAdapter;
    private long mAlbumId;
    private Loader mLoader;
    private View mNoNetLayout;

    public AlbumRelativeAlbumListFragment()
    {
    }

    public static AlbumRelativeAlbumListFragment getInstance(long l)
    {
        AlbumRelativeAlbumListFragment albumrelativealbumlistfragment = new AlbumRelativeAlbumListFragment();
        Bundle bundle = new Bundle();
        bundle.putLong("album_id", l);
        albumrelativealbumlistfragment.setArguments(bundle);
        return albumrelativealbumlistfragment;
    }

    private void loadData()
    {
        if (mLoader == null)
        {
            mLoader = getLoaderManager().initLoader(0, null, this);
        } else
        {
            mLoader = getLoaderManager().restartLoader(0, null, this);
        }
        ((MyAsyncTaskLoader)mLoader).setXDCSBindView(fragmentBaseContainerView, fragmentBaseContainerView);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        setTitleText(getString(0x7f0901fe));
        mAdapter = new InternalAdapter(getActivity(), new ArrayList());
        mListView.setAdapter(mAdapter);
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        loadData();
        mNoNetLayout.setOnClickListener(new _cls1());
        mListView.setOnItemClickListener(new _cls2());
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mAlbumId = bundle.getLong("album_id");
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        switch (i)
        {
        default:
            return null;

        case 0: // '\0'
            return new AlbumRelativeModelLoader(getActivity(), mAlbumId);

        case 1: // '\001'
            return new b(getActivity(), mAdapter.getData());
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f0300c0, viewgroup, false);
        fragmentBaseContainerView = layoutinflater;
        mListView = (ListView)layoutinflater.findViewById(0x7f0a0025);
        mNoNetLayout = layoutinflater.findViewById(0x7f0a035b);
        return layoutinflater;
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, final List data)
    {
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        if (loader == null) goto _L2; else goto _L1
_L1:
        loader.getId();
        JVM INSTR tableswitch 0 1: default 36
    //                   0 37
    //                   1 51;
           goto _L2 _L3 _L4
_L2:
        return;
_L3:
        doAfterAnimation(new _cls3());
        return;
_L4:
        mAdapter.notifyDataSetChanged();
        return;
    }

    public void onLoaderReset(Loader loader)
    {
    }




/*
    static GeneralAlbumListApdater access$102(AlbumRelativeAlbumListFragment albumrelativealbumlistfragment, GeneralAlbumListApdater generalalbumlistapdater)
    {
        albumrelativealbumlistfragment.mAdapter = generalalbumlistapdater;
        return generalalbumlistapdater;
    }

*/




/*
    static Loader access$302(AlbumRelativeAlbumListFragment albumrelativealbumlistfragment, Loader loader)
    {
        albumrelativealbumlistfragment.mLoader = loader;
        return loader;
    }

*/

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final AlbumRelativeAlbumListFragment this$0;

        public void onClick(View view)
        {
            loadData();
        }

        _cls1()
        {
            this$0 = AlbumRelativeAlbumListFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final AlbumRelativeAlbumListFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i >= 0 && i < mAdapter.getCount())
            {
                adapterview = (AlbumRelativeAlbumModel)mAdapter.getData().get(i);
                com.ximalaya.ting.android.model.album.AlbumModel albummodel = ModelHelper.toAlbumModel(adapterview);
                Bundle bundle = new Bundle();
                bundle.putString("album", JSON.toJSONString(albummodel));
                bundle.putString("rec_src", adapterview.getRecSrc());
                bundle.putString("rec_track", adapterview.getRecTrack());
                bundle.putInt("from", 10);
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
            }
        }

        _cls2()
        {
            this$0 = AlbumRelativeAlbumListFragment.this;
            super();
        }
    }


    private class _cls3
        implements MyCallback
    {

        final AlbumRelativeAlbumListFragment this$0;
        final List val$data;

        public void execute()
        {
label0:
            {
label1:
                {
                    if (data == null || data.size() <= 0)
                    {
                        break label0;
                    }
                    mNoNetLayout.setVisibility(8);
                    mAdapter = new InternalAdapter(getActivity(), data);
                    mListView.setAdapter(mAdapter);
                    if (mAdapter.getCount() > 0)
                    {
                        if (mLoader != null)
                        {
                            break label1;
                        }
                        mLoader = getLoaderManager().initLoader(1, null, AlbumRelativeAlbumListFragment.this);
                    }
                    return;
                }
                mLoader = getLoaderManager().restartLoader(1, null, AlbumRelativeAlbumListFragment.this);
                return;
            }
            mNoNetLayout.setVisibility(0);
        }

        _cls3()
        {
            this$0 = AlbumRelativeAlbumListFragment.this;
            data = list;
            super();
        }
    }

}
