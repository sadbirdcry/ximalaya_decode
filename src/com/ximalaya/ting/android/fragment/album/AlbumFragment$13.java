// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.ximalaya.ting.android.adapter.SoundsAlbumAdapter;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumFragment

class this._cls0
    implements android.widget.mClickListener
{

    final AlbumFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
label0:
        {
            if (mListView.getTag() == AlbumFragment.access$100(AlbumFragment.this))
            {
                i -= mListView.getHeaderViewsCount();
                if (AlbumFragment.access$1100(AlbumFragment.this) != null && i >= 0 && i < AlbumFragment.access$100(AlbumFragment.this).getData().size())
                {
                    break label0;
                }
            }
            return;
        }
        List list = ModelHelper.albumSoundlistToSoundInfoList(AlbumFragment.access$1100(AlbumFragment.this), AlbumFragment.access$1200(AlbumFragment.this), AlbumFragment.access$1300(AlbumFragment.this));
        int j = AlbumFragment.access$1400(AlbumFragment.this);
        HashMap hashmap = new HashMap();
        if (AlbumFragment.access$1500(AlbumFragment.this))
        {
            adapterview = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/my/album/track/").toString();
            hashmap.put("albumId", (new StringBuilder()).append(AlbumFragment.access$000(AlbumFragment.this).albumId).append("").toString());
            hashmap.put("albumUid", (new StringBuilder()).append(AlbumFragment.access$000(AlbumFragment.this).uid).append("").toString());
            hashmap.put("pageId", (new StringBuilder()).append(AlbumFragment.access$1600(AlbumFragment.this).pageId).append("").toString());
            hashmap.put("pageSize", (new StringBuilder()).append(AlbumFragment.access$1600(AlbumFragment.this).pageSize).append("").toString());
            hashmap.put("isAsc", (new StringBuilder()).append(AlbumFragment.access$1700(AlbumFragment.this)).append("").toString());
        } else
        {
            adapterview = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
            hashmap.put("albumId", (new StringBuilder()).append(AlbumFragment.access$000(AlbumFragment.this).albumId).append("").toString());
            hashmap.put("isAsc", (new StringBuilder()).append(AlbumFragment.access$1700(AlbumFragment.this)).append("").toString());
            hashmap.put("pageSize", (new StringBuilder()).append(AlbumFragment.access$1600(AlbumFragment.this).pageSize).append("").toString());
        }
        PlayTools.gotoPlay(j, adapterview, AlbumFragment.access$1600(AlbumFragment.this).pageId, hashmap, list, i, AlbumFragment.access$1800(AlbumFragment.this), true, DataCollectUtil.getDataFromView(view));
    }

    dingData()
    {
        this$0 = AlbumFragment.this;
        super();
    }
}
