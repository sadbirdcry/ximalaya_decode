// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.album.IAlbum;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

public class GeneralAlbumListApdater extends BaseAdapter
{
    protected static class ViewHolder
    {

        View collectLayout;
        TextView collectTxt;
        ImageView cover;
        TextView playCount;
        TextView title;
        TextView trackCount;
        TextView updateAt;

        protected ViewHolder()
        {
        }
    }


    private Context mContext;
    private List mData;
    private LayoutInflater mInflater;

    public GeneralAlbumListApdater(Context context, List list)
    {
        mInflater = LayoutInflater.from(context);
        mData = list;
        mContext = context;
    }

    public static void doCollect(final Context context, final IAlbum model, final ViewHolder holder)
    {
        if (UserInfoMannage.hasLogined())
        {
            String s;
            RequestParams requestparams;
            if (model.isIAlbumCollect())
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.getIAlbumId()).toString());
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(holder.collectLayout), new _cls2());
        } else
        {
            final AlbumModel m = new AlbumModel();
            m.albumId = model.getIAlbumId();
            m.isFavorite = model.isIAlbumCollect();
            if (AlbumModelManage.getInstance().ensureLocalCollectAllow(context, m, holder.collectLayout))
            {
                (new _cls3()).myexec(new Void[0]);
                return;
            }
        }
    }

    private static void setCollectStatus(ViewHolder viewholder, boolean flag)
    {
        if (flag)
        {
            viewholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ee, 0, 0);
            viewholder.collectTxt.setText("\u5DF2\u6536\u85CF");
            viewholder.collectTxt.setTextColor(Color.parseColor("#999999"));
            return;
        } else
        {
            viewholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ed, 0, 0);
            viewholder.collectTxt.setText("\u6536\u85CF");
            viewholder.collectTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    public void addData(List list)
    {
        if (mData == null)
        {
            mData = list;
        } else
        {
            mData.addAll(list);
        }
        notifyDataSetChanged();
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public List getData()
    {
        return mData;
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, final ViewGroup h)
    {
        boolean flag1 = true;
        String s;
        final IAlbum album;
        TextView textview;
        boolean flag;
        if (view == null)
        {
            view = mInflater.inflate(0x7f03003d, h, false);
            h = new ViewHolder();
            h.title = (TextView)view.findViewById(0x7f0a00ea);
            h.cover = (ImageView)view.findViewById(0x7f0a0023);
            h.trackCount = (TextView)view.findViewById(0x7f0a0152);
            h.playCount = (TextView)view.findViewById(0x7f0a0151);
            h.updateAt = (TextView)view.findViewById(0x7f0a014f);
            h.collectLayout = view.findViewById(0x7f0a0154);
            h.collectTxt = (TextView)view.findViewById(0x7f0a0155);
            h.updateAt = (TextView)view.findViewById(0x7f0a014f);
            view.setTag(h);
        } else
        {
            h = (ViewHolder)view.getTag();
        }
        album = (IAlbum)mData.get(i);
        ((ViewHolder) (h)).title.setText(album.getIAlbumTitle());
        if (album.getIAlbumPlayCount() > 0L)
        {
            ((ViewHolder) (h)).playCount.setText(StringUtil.getFriendlyNumStr(album.getIAlbumPlayCount()));
            ((ViewHolder) (h)).playCount.setVisibility(0);
        } else
        {
            ((ViewHolder) (h)).playCount.setVisibility(8);
        }
        ((ViewHolder) (h)).trackCount.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(album.getIAlbumTrackCount())).append("\u96C6").toString());
        textview = ((ViewHolder) (h)).updateAt;
        if (album.getIIntro() == null)
        {
            s = "";
        } else
        {
            s = album.getIIntro();
        }
        textview.setText(s);
        ImageManager2.from(mContext).displayImage(((ViewHolder) (h)).cover, album.getIAlbumCoverUrl(), 0x7f0202e0);
        setCollectStatus(h, album.isIAlbumCollect());
        ((ViewHolder) (h)).collectLayout.setTag(0x7f090000, album);
        ((ViewHolder) (h)).collectLayout.setOnClickListener(new _cls1());
        h = mContext;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (i + 1 != mData.size())
        {
            flag1 = false;
        }
        ViewUtil.buildAlbumItemSpace(h, view, flag, flag1, 81);
        return view;
    }

    public void setData(List list)
    {
        mData = list;
        notifyDataSetChanged();
    }



    private class _cls2 extends a
    {

        final Context val$context;
        final ViewHolder val$holder;
        final IAlbum val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, holder.collectLayout);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            if (((IAlbum)holder.collectLayout.getTag(0x7f090000)).getIAlbumId() == model.getIAlbumId())
            {
                GeneralAlbumListApdater.setCollectStatus(holder, model.isIAlbumCollect());
            }
        }

        public void onSuccess(String s)
        {
            boolean flag = true;
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
_L4:
            return;
_L2:
            s = JSON.parseObject(s);
            if (s == null) goto _L4; else goto _L3
_L3:
            int i = s.getIntValue("ret");
            if (i != 0)
            {
                break MISSING_BLOCK_LABEL_161;
            }
            s = model;
            if (model.isIAlbumCollect())
            {
                flag = false;
            }
            s.setIAlbumCollect(flag);
            if (((IAlbum)holder.collectLayout.getTag(0x7f090000)).getIAlbumId() == model.getIAlbumId())
            {
                GeneralAlbumListApdater.setCollectStatus(holder, model.isIAlbumCollect());
            }
            if (model.isIAlbumCollect())
            {
                s = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            try
            {
                Toast.makeText(context, s, 0).show();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
            return;
            if (i != 791)
            {
                break MISSING_BLOCK_LABEL_178;
            }
            model.setIAlbumCollect(true);
            if (((IAlbum)holder.collectLayout.getTag(0x7f090000)).getIAlbumId() == model.getIAlbumId())
            {
                GeneralAlbumListApdater.setCollectStatus(holder, model.isIAlbumCollect());
            }
            if (s.getString("msg") != null)
            {
                break MISSING_BLOCK_LABEL_252;
            }
            s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
_L6:
            Toast.makeText(context, s, 0).show();
            return;
            s = s.getString("msg");
            if (true) goto _L6; else goto _L5
_L5:
        }

        _cls2()
        {
            context = context1;
            holder = viewholder;
            model = ialbum;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final Context val$context;
        final ViewHolder val$holder;
        final AlbumModel val$m;
        final IAlbum val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            IAlbum ialbum = model;
            boolean flag;
            if (!model.isIAlbumCollect())
            {
                flag = true;
            } else
            {
                flag = false;
            }
            ialbum.setIAlbumCollect(flag);
            m.lastUptrackAt = model.getIAlbumLastUpdateAt();
            m.tracks = (int)model.getIAlbumTrackCount();
            m.coverSmall = model.getIAlbumCoverUrl();
            m.title = model.getIAlbumTitle();
            if (!model.isIAlbumCollect())
            {
                avoid.deleteAlbumInLocalAlbumList(m);
            } else
            {
                avoid.saveAlbumModel(m);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            if (((IAlbum)holder.collectLayout.getTag(0x7f090000)).getIAlbumId() == model.getIAlbumId())
            {
                GeneralAlbumListApdater.setCollectStatus(holder, model.isIAlbumCollect());
            }
            if (model.isIAlbumCollect())
            {
                void1 = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                void1 = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            Toast.makeText(context, void1, 0).show();
        }

        _cls3()
        {
            model = ialbum;
            m = albummodel;
            holder = viewholder;
            context = context1;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final GeneralAlbumListApdater this$0;
        final IAlbum val$album;
        final ViewHolder val$h;

        public void onClick(View view)
        {
            GeneralAlbumListApdater.doCollect(mContext, album, h);
        }

        _cls1()
        {
            this$0 = GeneralAlbumListApdater.this;
            album = ialbum;
            h = viewholder;
            super();
        }
    }

}
