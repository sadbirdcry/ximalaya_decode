// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumRelativeAlbumModel

public class AlbumRelativeModelLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "rec-association/recommend/album/by_album";
    private long mAlbumId;
    private List mData;

    public AlbumRelativeModelLoader(Context context, long l)
    {
        super(context);
        mAlbumId = l;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((List)obj);
    }

    public void deliverResult(List list)
    {
        super.deliverResult(list);
        mData = list;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    public List loadInBackground()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).put("albumId", mAlbumId);
        obj = f.a().a((new StringBuilder()).append(a.G).append("rec-association/recommend/album/by_album").toString(), ((RequestParams) (obj)), fromBindView, toBindView, false);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            try
            {
                obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
                if (((JSONObject) (obj)).getIntValue("ret") == 0)
                {
                    obj = ((JSONObject) (obj)).getString("albums");
                    if (!TextUtils.isEmpty(((CharSequence) (obj))))
                    {
                        mData = AlbumRelativeAlbumModel.getListFromJson(((String) (obj)));
                    }
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        return mData;
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData == null)
        {
            forceLoad();
            return;
        } else
        {
            deliverResult(mData);
            return;
        }
    }
}
