// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            TagRelativeAlbumListFragment, GeneralAlbumListApdater, AlbumFragment

class this._cls0
    implements android.widget.
{

    final TagRelativeAlbumListFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        i -= mListView.getHeaderViewsCount();
        if (i < 0 || i >= TagRelativeAlbumListFragment.access$000(TagRelativeAlbumListFragment.this).getCount())
        {
            return;
        } else
        {
            adapterview = ModelHelper.toAlbumModel((Item)TagRelativeAlbumListFragment.access$000(TagRelativeAlbumListFragment.this).getData().get(i));
            Bundle bundle = new Bundle();
            bundle.putString("album", JSON.toJSONString(adapterview));
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
            return;
        }
    }

    Item()
    {
        this$0 = TagRelativeAlbumListFragment.this;
        super();
    }
}
