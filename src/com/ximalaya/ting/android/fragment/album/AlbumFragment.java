// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.activity.report.ReportActivity;
import com.ximalaya.ting.android.activity.share.BaseShareDialog;
import com.ximalaya.ting.android.adapter.SoundsAlbumAdapter;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.device.dlna.BaseBindableDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.BaseDeviceItem;
import com.ximalaya.ting.android.fragment.device.dlna.DeviceUtil;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.common.fragment.CommonDeviceAlbumSectionDownloadFragment;
import com.ximalaya.ting.android.fragment.device.dlna.model.BaseItemBindableModel;
import com.ximalaya.ting.android.fragment.device.dlna.model.BindCommandModel;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseBindModule;
import com.ximalaya.ting.android.fragment.device.dlna.tingshubao.TingshuBindModule;
import com.ximalaya.ting.android.fragment.ting.CollectFragment;
import com.ximalaya.ting.android.fragment.ting.feed.FeedCollectFragment;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.fragment.zone.ZoneFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.album.HotAlbum;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.AlbumSoundModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.DexManager;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.transaction.download.AlbumDownload;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.BlurableImageView;
import com.ximalaya.ting.android.view.bounceview.BounceHeadListView;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumSectionDownloadFragment, AlbumIntroFragment, AlbumRelativeAlbumListFragment

public class AlbumFragment extends BaseListFragment
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{
    private class AlbumPagerAdapter extends BaseAdapter
    {

        private int mLastSize;
        private List mPagerIndexs;
        final AlbumFragment this$0;

        private void computePagerIndex()
        {
            int j = (int)Math.ceil((float)soundData.totalCount / (float)soundData.pageSize);
            for (int i = 0; i < j; i++)
            {
                PageIndex pageindex = new PageIndex(null);
                pageindex.pageIndex = i + 1;
                pageindex.startIndex = soundData.pageSize * i + 1;
                pageindex.endIndex = Math.min(soundData.pageSize * (i + 1), soundData.totalCount);
                pageindex.pageString = (new StringBuilder()).append(pageindex.startIndex).append("~").append(pageindex.endIndex).toString();
                mPagerIndexs.add(pageindex);
            }

            mLastSize = soundData.totalCount;
        }

        public int getCount()
        {
            return mPagerIndexs.size();
        }

        public Object getItem(int i)
        {
            return mPagerIndexs.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            if (view == null)
            {
                view = LayoutInflater.from(mCon).inflate(0x7f030006, mAlbumPager, false);
            }
            viewgroup = (TextView)view;
            PageIndex pageindex = (PageIndex)mPagerIndexs.get(i);
            viewgroup.setText(pageindex.pageString);
            if (pageindex.pageIndex == soundData.pageId - 1)
            {
                viewgroup.setBackgroundResource(0x7f02002c);
                viewgroup.setTextColor(Color.parseColor("#ffffff"));
                return view;
            } else
            {
                viewgroup.setBackgroundResource(0x7f02002b);
                viewgroup.setTextColor(Color.parseColor("#333333"));
                return view;
            }
        }

        public void notifyDataSetChanged()
        {
            if (mLastSize != soundData.totalCount)
            {
                computePagerIndex();
            }
        }

        public AlbumPagerAdapter()
        {
            this$0 = AlbumFragment.this;
            super();
            mLastSize = 0;
            mPagerIndexs = new ArrayList();
            computePagerIndex();
        }
    }

    private class ChannelShowDialog extends Dialog
        implements android.view.View.OnClickListener
    {

        private TextView channel1Name;
        private TextView channel2Name;
        private TextView channel3Name;
        private TextView channel4Name;
        private BaseDeviceItem deviceItemTemp;
        private DlnaManager mDlnaManager;
        private String names[];
        final AlbumFragment this$0;
        private String title;
        private TextView titleName;

        private void setNameChannel(TextView textview, String s)
        {
            if (TextUtils.isEmpty(s))
            {
                textview.setText("\u672A\u83B7\u53D6\u6210\u529F");
                return;
            } else
            {
                textview.setText((new StringBuilder()).append("\u5F53\u524D\u4E13\u8F91:").append(s).toString());
                return;
            }
        }

        public void onClick(View view)
        {
            TingshuBindModule tingshubindmodule;
            BindCommandModel bindcommandmodel;
            tingshubindmodule = (TingshuBindModule)mDlnaManager.getController(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao).getModule(BaseBindModule.NAME);
            bindcommandmodel = new BindCommandModel();
            bindcommandmodel.mAlbumId = album.albumId;
            bindcommandmodel.mDeviceItem = (BaseBindableDeviceItem)deviceItemTemp;
            bindcommandmodel.mAlbumModel = album;
            view.getId();
            JVM INSTR lookupswitch 4: default 112
        //                       2131362287: 131
        //                       2131362290: 139
        //                       2131362293: 147
        //                       2131362296: 155;
               goto _L1 _L2 _L3 _L4 _L5
_L1:
            tingshubindmodule.setChannels(bindcommandmodel);
            menuDialog = null;
            dismiss();
            return;
_L2:
            bindcommandmodel.mChannelId = 0;
            continue; /* Loop/switch isn't completed */
_L3:
            bindcommandmodel.mChannelId = 1;
            continue; /* Loop/switch isn't completed */
_L4:
            bindcommandmodel.mChannelId = 2;
            continue; /* Loop/switch isn't completed */
_L5:
            bindcommandmodel.mChannelId = 3;
            if (true) goto _L1; else goto _L6
_L6:
        }

        protected void onCreate(Bundle bundle)
        {
            super.onCreate(bundle);
            requestWindowFeature(1);
            setContentView(0x7f030067);
            channel1Name = (TextView)findViewById(0x7f0a01f1);
            channel2Name = (TextView)findViewById(0x7f0a01f4);
            channel3Name = (TextView)findViewById(0x7f0a01f7);
            channel4Name = (TextView)findViewById(0x7f0a01fa);
            titleName = (TextView)findViewById(0x7f0a0158);
            if (names != null)
            {
                setNameChannel(channel1Name, names[0]);
                setNameChannel(channel2Name, names[1]);
                setNameChannel(channel3Name, names[2]);
                setNameChannel(channel4Name, names[3]);
            }
            findViewById(0x7f0a01ef).setOnClickListener(this);
            findViewById(0x7f0a01f2).setOnClickListener(this);
            findViewById(0x7f0a01f5).setOnClickListener(this);
            findViewById(0x7f0a01f8).setOnClickListener(this);
            titleName.setText(title);
        }

        public void setDevice(BaseDeviceItem basedeviceitem)
        {
            deviceItemTemp = basedeviceitem;
        }

        public ChannelShowDialog(Context context, String as[], String s)
        {
            this$0 = AlbumFragment.this;
            super(context, 0x7f0b0002);
            names = as;
            title = s;
            mDlnaManager = DlnaManager.getInstance(context);
        }
    }

    private class LoadingData
    {

        public boolean clear;
        public boolean loadingNextPage;
        public int pageId;
        public int pageSize;
        final AlbumFragment this$0;
        public int totalCount;

        public void reSet()
        {
            totalCount = 0;
            pageId = 1;
            pageSize = 20;
            loadingNextPage = false;
        }

        private LoadingData()
        {
            this$0 = AlbumFragment.this;
            super();
            totalCount = 0;
            pageId = 1;
            pageSize = 20;
            loadingNextPage = false;
            clear = false;
        }

        LoadingData(_cls1 _pcls1)
        {
            this();
        }
    }

    private class PageIndex
    {

        public int endIndex;
        public int pageIndex;
        public String pageString;
        public int startIndex;
        final AlbumFragment this$0;

        private PageIndex()
        {
            this$0 = AlbumFragment.this;
            super();
        }

        PageIndex(_cls1 _pcls1)
        {
            this();
        }
    }


    public static final String FROM = "from";
    public static final int FROM_ALBUM_RELATIVE = 10;
    public static final int FROM_COLLECTION = 1;
    public static final int FROM_COLLECTION_RECOMMEND = 11;
    public static final int FROM_DISCOVERY_CATEGORY = 2;
    public static final int FROM_DISCOVERY_FOCUS = 3;
    public static final int FROM_FEED = 9;
    public static final int FROM_FOCUS = 7;
    public static final int FROM_GUESS_LIKE = 12;
    public static final int FROM_HOT_ANCHOR = 4;
    public static final int FROM_RECOMMEND_ALBUM = 6;
    public static final int FROM_SEARCH = 8;
    public static final int FROM_SUBJECT = 5;
    protected static final String TAG = com/ximalaya/ting/android/fragment/album/AlbumFragment.getSimpleName();
    private AlbumModel album;
    private TextView albumIntro;
    private TextView collectAlbumTxt;
    private BlurableImageView coverBg;
    private ImageView coverImg;
    private View downloadAlbum;
    private View floatView;
    private Bitmap floatViewBg;
    private View headerFloatView;
    private ViewGroup headerView;
    private boolean isAsc;
    private boolean isMyAlbum;
    private GridView mAlbumPager;
    private BaseAdapter mAlbumPagerAdapter;
    private com.ximalaya.ting.android.transaction.download.AlbumDownload.FinishCallback mCallback;
    private View mCollectBtn;
    private View mFloatCollectBtn;
    private int mFrom;
    private Handler mHandler;
    private PopupWindow mMoreMenuPanel;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private PopupWindow mPagerSelectorContainer;
    private View mPagerSelectorContent;
    private ProgressDialog mProgressDialog;
    private String mRecSrc;
    private String mRecTrack;
    private ScoreManage mScoreManage;
    private SoundsAlbumAdapter mSoundsAdapter;
    private long mToPlayTrackId;
    private MenuDialog menuDialog;
    private View moreView;
    private TextView nickNameTextView;
    private TextView pageSelector;
    private TextView pageSelectorFloat;
    private View pagerSelectorBorder;
    private View pagerSelectorBorderFloat;
    private TextView playCount;
    private View relatedAlbum;
    private int screenWidth;
    private TextView sortBtn;
    private TextView sortBtnFloat;
    private TextView soundCount;
    private TextView soundCountFloat;
    private LoadingData soundData;
    private List soundDataList;
    private TextView tag1;
    private TextView tag2;
    private TextView tag3;

    public AlbumFragment()
    {
        mCallback = new _cls1();
        isAsc = true;
        album = new AlbumModel();
        isMyAlbum = false;
        mHandler = new _cls2();
        mToPlayTrackId = -1L;
    }

    private void buildFloatViewBackground()
    {
        if (floatViewBg != null)
        {
            break MISSING_BLOCK_LABEL_128;
        }
        Bitmap bitmap;
        headerView.buildDrawingCache();
        bitmap = headerView.getDrawingCache();
        if (bitmap == null)
        {
            int ai[];
            int ai1[];
            try
            {
                ((BounceHeadListView)mListView).setCloneFloatViewBackgroundDefualt();
                return;
            }
            catch (Exception exception)
            {
                ((BounceHeadListView)mListView).setCloneFloatViewBackgroundDefualt();
            }
            break MISSING_BLOCK_LABEL_128;
        }
        ai = new int[2];
        floatView.getLocationInWindow(ai);
        ai1 = new int[2];
        headerView.getLocationInWindow(ai1);
        floatViewBg = Bitmap.createBitmap(bitmap, 0, ai[1] - ai1[1], floatView.getWidth(), floatView.getHeight());
        ((BounceHeadListView)mListView).setCloneFloatViewBackground(new BitmapDrawable(getResources(), floatViewBg));
        return;
    }

    private void dissmisSeletor()
    {
        mPagerSelectorContainer.dismiss();
    }

    private void downloadAlbum(final boolean isDownloadStart)
    {
        if (isDownloadStart)
        {
            mProgressDialog = ToolUtil.createProgressDialog(mCon, 0, true, true);
            mProgressDialog.setMessage(getResources().getString(0x7f090188));
            mProgressDialog.show();
        }
        mDataLoadTask = (new _cls3()).myexec(new Void[0]);
    }

    private void favoriteAlbum(View view)
    {
        if (ToolUtil.isConnectToNetwork(mCon))
        {
            mCollectBtn.setEnabled(false);
            mFloatCollectBtn.setEnabled(false);
            if (UserInfoMannage.hasLogined())
            {
                boolean flag;
                if (!album.isFavorite)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                resetCollectImgStatus(flag);
                mDataLoadTask = (new _cls4()).myexec(new Void[0]);
            } else
            if (AlbumModelManage.getInstance().ensureLocalCollectAllow(mActivity, album, view))
            {
                (new _cls5()).myexec(new Void[0]);
                return;
            }
        }
    }

    private int getPlaySource()
    {
        switch (mFrom)
        {
        case 3: // '\003'
        default:
            return 3;

        case 1: // '\001'
            return 19;

        case 2: // '\002'
            return 20;

        case 4: // '\004'
            return 21;

        case 5: // '\005'
            return 22;

        case 6: // '\006'
            return 23;

        case 7: // '\007'
            return 12;

        case 8: // '\b'
            return 11;

        case 9: // '\t'
            return 1;

        case 11: // '\013'
            return 29;

        case 12: // '\f'
            return 28;

        case 10: // '\n'
            return 27;
        }
    }

    private void goDownloadAlbum()
    {
        if (soundDataList == null || album == null)
        {
            return;
        }
        int i = NetworkUtils.getNetType(mCon);
        if (-1 == i)
        {
            showToast(getResources().getString(0x7f09017c));
            return;
        }
        if (!StorageUtils.isInternalSDcardAvaliable() && !StorageUtils.isExternalSDcardAvaliable())
        {
            showToast(com.ximalaya.ting.android.a.a.a.f.a());
            return;
        }
        boolean flag = SharedPreferencesUtil.getInstance(mCon).getBoolean("is_download_enabled_in_3g", false);
        if (1 == i)
        {
            showToast(getResources().getString(0x7f09017f));
            downloadAlbum(true);
            return;
        }
        if (!flag)
        {
            NetworkUtils.showChangeNetWorkSetConfirm(new _cls6(), new _cls7());
            return;
        } else
        {
            (new DialogBuilder(MyApplication.a())).setTitle("\u6E29\u99A8\u63D0\u793A").setMessage("\u4EB2\uFF0C\u4F60\u5C06\u4F7F\u75282G/3G/4G\u7F51\u7EDC\u4E0B\u8F7D\u4EFB\u52A1\uFF0C\u5982\u6B64\u4F1A\u6D88\u8017\u6D41\u91CF\u54E6").setCancelBtn("\u6682\u65F6\u505C\u6B62", new _cls9()).setOkBtn("\u7EE7\u7EED\u4E0B\u8F7D", new _cls8()).showConfirm();
            return;
        }
    }

    private void initData(long l)
    {
        topTextView = (TextView)headerView.findViewById(0x7f0a0158);
        isAsc = true;
        soundData = new LoadingData(null);
        updateHeadView();
        screenWidth = ToolUtil.getScreenWidth(mCon);
        View view = new View(getActivity());
        view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, getResources().getDimensionPixelSize(0x7f080009)));
        mListView.addFooterView(view, null, false);
        soundDataList = new ArrayList();
        mSoundsAdapter = new SoundsAlbumAdapter(getActivity(), soundDataList, getPlaySource());
        mListView.setAdapter(mSoundsAdapter);
        mListView.setTag(mSoundsAdapter);
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        loadSoundListData(fragmentBaseContainerView, l);
    }

    private void initListener()
    {
        coverImg.setOnClickListener(this);
        findViewById(0x7f0a03bc).setOnClickListener(this);
        findViewById(0x7f0a044b).setOnClickListener(this);
        findViewById(0x7f0a044d).setOnClickListener(this);
        headerFloatView.findViewById(0x7f0a03bc).setOnClickListener(this);
        headerFloatView.findViewById(0x7f0a044b).setOnClickListener(this);
        headerFloatView.findViewById(0x7f0a044d).setOnClickListener(this);
        findViewById(0x7f0a0453).setOnClickListener(this);
        sortBtn.setOnClickListener(this);
        sortBtnFloat.setOnClickListener(this);
        pageSelector.setOnClickListener(this);
        pageSelectorFloat.setOnClickListener(this);
        findViewById(0x7f0a0277).setOnClickListener(this);
        findViewById(0x7f0a0452).setOnClickListener(this);
        findViewById(0x7f0a0451).setOnClickListener(this);
        mFooterViewLoading.setOnClickListener(new _cls10());
        ((BounceHeadListView)mListView).setOnPullFinishListener(new _cls11());
        ((BounceHeadListView)mListView).setOnCloneFloatViewVisibilityChangedCallback(new _cls12());
        mListView.setOnItemClickListener(new _cls13());
        mListView.setOnItemLongClickListener(new _cls14());
        registerListener();
    }

    private void initPagerSelector()
    {
        if (mPagerSelectorContainer == null)
        {
            mPagerSelectorContent = LayoutInflater.from(mCon).inflate(0x7f030007, mListView, false);
            mAlbumPager = (GridView)mPagerSelectorContent.findViewById(0x7f0a0067);
            mAlbumPagerAdapter = new AlbumPagerAdapter();
            mAlbumPager.setAdapter(mAlbumPagerAdapter);
            mPagerSelectorContent.findViewById(0x7f0a0068).setOnClickListener(new _cls19());
            mAlbumPager.setOnItemClickListener(new _cls20());
            mPagerSelectorContainer = new PopupWindow(mActivity);
            mPagerSelectorContainer.setContentView(mPagerSelectorContent);
            mPagerSelectorContainer.setAnimationStyle(0x7f0b000f);
            mPagerSelectorContainer.setWidth(-1);
            mPagerSelectorContainer.setHeight(-1);
            mPagerSelectorContainer.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#b0000000")));
            mPagerSelectorContainer.setOutsideTouchable(true);
            mPagerSelectorContainer.setOnDismissListener(new _cls21());
            mPagerSelectorContainer.setFocusable(true);
            mPagerSelectorContainer.update();
        }
    }

    private void initUi(LayoutInflater layoutinflater)
    {
        mListView = (BounceHeadListView)findViewById(0x7f0a005c);
        headerView = (ViewGroup)layoutinflater.inflate(0x7f03010e, mListView, false);
        moreView = headerView.findViewById(0x7f0a0277);
        headerFloatView = ((BounceHeadListView)mListView).addHeaderView(headerView, 0x7f03010d, (RelativeLayout)findViewById(0x7f0a0056));
        downloadAlbum = findViewById(0x7f0a03bc);
        relatedAlbum = findViewById(0x7f0a044d);
        mCollectBtn = findViewById(0x7f0a044b);
        mFloatCollectBtn = headerFloatView.findViewById(0x7f0a044b);
        collectAlbumTxt = (TextView)findViewById(0x7f0a044c);
        coverImg = (ImageView)headerView.findViewById(0x7f0a044f);
        coverBg = (BlurableImageView)headerView.findViewById(0x7f0a03ec);
        floatView = headerView.findViewById(0x7f0a0448);
        coverBg.setTag(0x7f0a003b, Boolean.valueOf(true));
        markImageView(coverImg);
        markImageView(coverBg);
        nickNameTextView = (TextView)findViewById(0x7f0a0452);
        albumIntro = (TextView)headerView.findViewById(0x7f0a0453);
        playCount = (TextView)headerView.findViewById(0x7f0a0151);
        tag1 = (TextView)headerView.findViewById(0x7f0a0454);
        tag2 = (TextView)headerView.findViewById(0x7f0a0455);
        tag3 = (TextView)headerView.findViewById(0x7f0a0456);
        sortBtn = (TextView)findViewById(0x7f0a0373);
        sortBtnFloat = (TextView)headerFloatView.findViewById(0x7f0a0373);
        soundCount = (TextView)findViewById(0x7f0a043d);
        soundCountFloat = (TextView)headerFloatView.findViewById(0x7f0a043d);
        pagerSelectorBorder = headerView.findViewById(0x7f0a00a8);
        pagerSelectorBorderFloat = headerFloatView.findViewById(0x7f0a00a8);
        pageSelector = (TextView)findViewById(0x7f0a043e);
        pageSelector.setVisibility(0);
        pageSelectorFloat = (TextView)headerFloatView.findViewById(0x7f0a043e);
        pageSelectorFloat.setVisibility(0);
    }

    private boolean isHasDevice2Bind()
    {
        if (!DexManager.getInstance(mActivity).isDlnaInit())
        {
            return false;
        }
        ArrayList arraylist = (ArrayList)DlnaManager.getInstance(getActivity()).getDeviceListByType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao);
        boolean flag;
        if (arraylist != null && arraylist.size() > 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        return flag;
    }

    private void loadMoreData(View view)
    {
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        if (mListView.getTag() == mSoundsAdapter)
        {
            loadSoundListData(view);
        }
    }

    private void loadSoundListData(View view)
    {
        loadSoundListData(view, -1L);
    }

    private void loadSoundListData(final View view, final long trackId)
    {
        if (!soundData.loadingNextPage)
        {
            if (ToolUtil.isConnectToNetwork(mCon))
            {
                mDataLoadTask = (new _cls15()).myexec(new Void[0]);
                return;
            }
            if (mListView.getTag() == mSoundsAdapter)
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E");
                return;
            }
        }
    }

    private boolean moreSoundDataAvailable(LoadingData loadingdata)
    {
        return loadingdata.pageId * loadingdata.pageSize < loadingdata.totalCount;
    }

    private void onClickDeviceItem(List list, int i, int j)
    {
        if (menuDialog != null)
        {
            menuDialog.dismiss();
        }
        j;
        JVM INSTR tableswitch 0 1: default 36
    //                   0 37
    //                   1 258;
           goto _L1 _L2 _L3
_L1:
        return;
_L2:
        list = (BaseBindableDeviceItem)list.get(i);
        String as[] = new String[4];
        AlbumModel albummodel = (AlbumModel)list.getBaseBindableModel().getAlbums().get(Integer.valueOf(1));
        if (albummodel != null && albummodel.albumId > 0L)
        {
            as[0] = albummodel.title;
        }
        albummodel = (AlbumModel)list.getBaseBindableModel().getAlbums().get(Integer.valueOf(2));
        if (albummodel != null && albummodel.albumId > 0L)
        {
            as[1] = albummodel.title;
        }
        albummodel = (AlbumModel)list.getBaseBindableModel().getAlbums().get(Integer.valueOf(3));
        if (albummodel != null && albummodel.albumId > 0L)
        {
            as[2] = albummodel.title;
        }
        albummodel = (AlbumModel)list.getBaseBindableModel().getAlbums().get(Integer.valueOf(4));
        if (albummodel != null && albummodel.albumId > 0L)
        {
            as[3] = albummodel.title;
        }
        ChannelShowDialog channelshowdialog = new ChannelShowDialog(mActivity, as, DeviceUtil.getDeviceItemName(list));
        channelshowdialog.setDevice(list);
        channelshowdialog.show();
        return;
_L3:
        if (album != null)
        {
            list = CommonDeviceAlbumSectionDownloadFragment.newInstance(album);
            Bundle bundle = list.getArguments();
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(downloadAlbum));
            list.setArguments(bundle);
            startFragment(list);
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    private void onSetChannel()
    {
        ArrayList arraylist = (ArrayList)DlnaManager.getInstance(mActivity).getDeviceListByType(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.tingshubao);
        if (arraylist == null || arraylist.size() == 0)
        {
            showToast("\u60A8\u5F53\u524D\u6CA1\u6709\u8FDE\u63A5\u7684\u8BBE\u5907");
        }
        if (arraylist.size() == 1)
        {
            onClickDeviceItem(arraylist, 0, 0);
            return;
        }
        ArrayList arraylist1 = new ArrayList(arraylist.size());
        for (int i = 0; i < arraylist.size(); i++)
        {
            arraylist1.add(DeviceUtil.getDeviceItemName((BaseDeviceItem)arraylist.get(i)));
        }

        if (menuDialog == null)
        {
            menuDialog = new MenuDialog(mActivity, arraylist1, new _cls22());
        }
        menuDialog.setHeaderTitle("\u9009\u62E9\u7ED1\u5B9A\u8BBE\u5907");
        menuDialog.show();
    }

    private void registerListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.setOnPlayServiceUpdateListener(this);
            mOnPlayerStatusUpdateListener = new _cls16();
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void resetCollectImgStatus(boolean flag)
    {
        TextView textview = (TextView)headerFloatView.findViewById(0x7f0a044c);
        if (flag)
        {
            collectAlbumTxt.setCompoundDrawablesWithIntrinsicBounds(0x7f02028f, 0, 0, 0);
            collectAlbumTxt.setText("\u5DF2\u6536\u85CF");
            textview.setCompoundDrawablesWithIntrinsicBounds(0x7f02028f, 0, 0, 0);
            textview.setText("\u5DF2\u6536\u85CF");
            return;
        } else
        {
            collectAlbumTxt.setCompoundDrawablesWithIntrinsicBounds(0x7f02028e, 0, 0, 0);
            collectAlbumTxt.setText("\u6536\u85CF");
            textview.setCompoundDrawablesWithIntrinsicBounds(0x7f02028e, 0, 0, 0);
            textview.setText("\u6536\u85CF");
            return;
        }
    }

    private void setOrderNumber(List list)
    {
        list = list.iterator();
        for (int i = 0; list.hasNext(); i++)
        {
            ((DownloadTask)list.next()).orderNum = i;
        }

    }

    private void showSelectFooterView()
    {
        if (mListView.getTag() == mSoundsAdapter)
        {
            if (moreSoundDataAvailable(soundData))
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.MORE);
                return;
            }
            if (soundDataList == null || soundDataList.size() == 0)
            {
                setFooterViewText("\u8BE5\u4E13\u8F91\u58F0\u97F3\u6570\u4E3A0");
                return;
            } else
            {
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
        } else
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            return;
        }
    }

    public static void start(BaseFragment basefragment, View view, AlbumModel albummodel, String s, String s1)
    {
        Bundle bundle = new Bundle();
        bundle.putString("album", JSON.toJSONString(albummodel));
        if (!TextUtils.isEmpty(s))
        {
            bundle.putString("rec_src", s);
        }
        if (!TextUtils.isEmpty(s1))
        {
            bundle.putString("rec_track", s1);
        }
        bundle.putInt("from", 10);
        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        basefragment.startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(this);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void updateAlbumData()
    {
        if (album != null)
        {
            ImageManager2.from(mCon).displayImage((ImageView)findViewById(0x7f0a0451), album.avatarPath, -1);
        }
    }

    private void updateHeadView()
    {
        if (album != null)
        {
            ImageManager2.from(mCon).displayImage(coverImg, album.coverLarge, -1, new _cls17());
            ImageManager2.from(mCon).displayImage((ImageView)findViewById(0x7f0a0451), album.avatarPath, -1);
            if (album.playTimes > 0)
            {
                playCount.setVisibility(0);
                playCount.setText(StringUtil.getFriendlyNumStr(album.playTimes));
            } else
            {
                playCount.setVisibility(8);
            }
            if (!TextUtils.isEmpty(album.introRich))
            {
                try
                {
                    albumIntro.setText(Html.fromHtml(album.introRich));
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
            } else
            if (TextUtils.isEmpty(album.intro))
            {
                albumIntro.setText(0x7f090170);
            } else
            {
                albumIntro.setText(album.intro);
            }
            if (!TextUtils.isEmpty(album.tags))
            {
                String as[] = album.tags.split(",");
                TextView atextview[] = new TextView[3];
                atextview[0] = tag1;
                atextview[1] = tag2;
                atextview[2] = tag3;
                int i = 0;
                for (int j = 0; i < as.length && i < atextview.length && as[i].length() + j < 8; i++)
                {
                    atextview[i].setVisibility(0);
                    atextview[i].setText(as[i]);
                    j += as[i].length();
                    final String tag = as[i];
                    atextview[i].setOnClickListener(new _cls18());
                }

            } else
            {
                tag1.setVisibility(8);
                tag2.setVisibility(8);
                tag3.setVisibility(8);
            }
            if (!Utilities.isBlank(album.nickname))
            {
                nickNameTextView.setText(album.nickname);
            } else
            {
                nickNameTextView.setText("");
            }
            if (!Utilities.isBlank(album.title))
            {
                setTitleText(album.title);
            }
            soundCount.setText(mCon.getString(0x7f090095, new Object[] {
                Integer.valueOf(album.tracks)
            }));
            soundCountFloat.setText(mCon.getString(0x7f090095, new Object[] {
                Integer.valueOf(album.tracks)
            }));
        }
    }

    protected void doPlay(long l)
    {
        if (l >= 0L && mSoundsAdapter != null && mSoundsAdapter.getData() != null && mSoundsAdapter.getCount() > 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int i;
        int j;
        j = mSoundsAdapter.getCount();
        i = 0;
_L5:
        if (i == j)
        {
            break MISSING_BLOCK_LABEL_196;
        }
        if (((AlbumSoundModel)mSoundsAdapter.getData().get(i)).trackId != l) goto _L4; else goto _L3
_L3:
        AlbumSoundModel albumsoundmodel = (AlbumSoundModel)mSoundsAdapter.getData().get(i);
_L6:
        AlbumSoundModel albumsoundmodel1 = albumsoundmodel;
        if (albumsoundmodel == null)
        {
            albumsoundmodel1 = (AlbumSoundModel)mSoundsAdapter.getItem(0);
        }
        if (albumsoundmodel1 != null && LocalMediaService.getInstance() != null && !LocalMediaService.getInstance().isPlaying() && !LocalMediaService.getInstance().isPlayingAds())
        {
            mSoundsAdapter.playSound(null, i, ModelHelper.toSoundInfo(albumsoundmodel1, mRecSrc, mRecTrack), ModelHelper.albumSoundlistToSoundInfoList(mSoundsAdapter.getData(), mRecSrc, mRecTrack));
            return;
        }
          goto _L1
_L4:
        i++;
          goto _L5
        albumsoundmodel = null;
        i = 0;
          goto _L6
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        bundle = getArguments();
        mScoreManage = ScoreManage.getInstance(mActivity.getApplicationContext());
        if (bundle != null)
        {
            Object obj = bundle.getString("hot_album");
            if (!Utilities.isBlank(((String) (obj))))
            {
                obj = (HotAlbum)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/album/HotAlbum);
                album.albumId = ((HotAlbum) (obj)).album_id;
                album.uid = ((HotAlbum) (obj)).album_uid;
                album.nickname = ((HotAlbum) (obj)).nickname;
                album.title = ((HotAlbum) (obj)).title;
                album.tags = ((HotAlbum) (obj)).tags;
                album.intro = ((HotAlbum) (obj)).short_intro;
                album.coverLarge = ((HotAlbum) (obj)).album_cover_path_290;
            }
            obj = bundle.getString("album");
            if (!Utilities.isBlank(((String) (obj))))
            {
                album = (AlbumModel)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/album/AlbumModel);
            }
            mRecSrc = bundle.getString("rec_src");
            mRecTrack = bundle.getString("rec_track");
            mToPlayTrackId = bundle.getLong("track_id_to_play", -1L);
        }
        if (UserInfoMannage.hasLogined() && album != null && UserInfoMannage.getInstance().getUser().uid == album.uid)
        {
            isMyAlbum = true;
        }
        initData(mToPlayTrackId);
        initListener();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (mCon != null && isAdded() && i == 1 && j == 1)
        {
            i = intent.getIntExtra("position", -1);
            long l = intent.getLongExtra("trackId", 0L);
            if (soundDataList != null && i < soundDataList.size() && i >= 0 && ((AlbumSoundModel)soundDataList.get(i)).trackId == l)
            {
                ((AlbumSoundModel)soundDataList.get(i)).isRelay = true;
                if (mSoundsAdapter != null)
                {
                    mSoundsAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    public boolean onBackPressed()
    {
        if (mPagerSelectorContainer != null && mPagerSelectorContainer.isShowing())
        {
            dissmisSeletor();
            return true;
        } else
        {
            return super.onBackPressed();
        }
    }

    public void onClick(View view)
    {
        if (isAdded()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        switch (view.getId())
        {
        default:
            return;

        case 2131362116: 
            if (mMoreMenuPanel != null)
            {
                mMoreMenuPanel.dismiss();
            }
            if (album != null && album.zoneId > 0)
            {
                view = new Bundle();
                view.putLong("zoneId", album.zoneId);
                startFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment, view);
                return;
            }
            break;

        case 2131362899: 
            Bundle bundle = new Bundle();
            bundle.putSerializable("data", album);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumIntroFragment, bundle);
            return;

        case 2131362878: 
            initPagerSelector();
            if (mPagerSelectorContainer != null)
            {
                if (mPagerSelectorContainer.isShowing())
                {
                    dissmisSeletor();
                    return;
                }
                mPagerSelectorContainer.showAsDropDown(view, 0, 0);
                sortBtn.setTextColor(Color.parseColor("#999999"));
                sortBtnFloat.setTextColor(Color.parseColor("#999999"));
                if (isAsc)
                {
                    sortBtn.setCompoundDrawablesWithIntrinsicBounds(0x7f02029d, 0, 0, 0);
                    sortBtnFloat.setCompoundDrawablesWithIntrinsicBounds(0x7f02029d, 0, 0, 0);
                    return;
                } else
                {
                    sortBtn.setCompoundDrawablesWithIntrinsicBounds(0x7f02029f, 0, 0, 0);
                    sortBtnFloat.setCompoundDrawablesWithIntrinsicBounds(0x7f02029f, 0, 0, 0);
                    return;
                }
            }
            break;

        case 2131362675: 
            if (isAsc)
            {
                isAsc = false;
                sortBtn.setCompoundDrawablesWithIntrinsicBounds(0x7f02029e, 0, 0, 0);
                sortBtnFloat.setCompoundDrawablesWithIntrinsicBounds(0x7f02029e, 0, 0, 0);
                soundData.reSet();
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
                loadSoundListData(view);
                return;
            } else
            {
                isAsc = true;
                sortBtn.setCompoundDrawablesWithIntrinsicBounds(0x7f02029c, 0, 0, 0);
                sortBtnFloat.setCompoundDrawablesWithIntrinsicBounds(0x7f02029c, 0, 0, 0);
                soundData.reSet();
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
                loadSoundListData(view);
                return;
            }

        case 2131362893: 
            ToolUtil.onEvent(mActivity, "Album_Related");
            if (album != null)
            {
                AlbumRelativeAlbumListFragment albumrelativealbumlistfragment = AlbumRelativeAlbumListFragment.getInstance(album.albumId);
                Bundle bundle2 = albumrelativealbumlistfragment.getArguments();
                bundle2.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                albumrelativealbumlistfragment.setArguments(bundle2);
                startFragment(albumrelativealbumlistfragment);
            } else
            {
                showToast(mCon.getString(0x7f09009d));
            }
            ToolUtil.onEvent(mCon, "Album_OtherAlbum");
            return;

        case 2131362897: 
        case 2131362898: 
            Bundle bundle1 = new Bundle();
            bundle1.putLong("toUid", album.uid);
            bundle1.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle1);
            return;

        case 2131362117: 
            ToolUtil.onEvent(mCon, "Album_Share");
            if (mMoreMenuPanel != null)
            {
                mMoreMenuPanel.dismiss();
            }
            if (album != null)
            {
                (new BaseShareDialog(mActivity, album, moreView)).show();
                return;
            } else
            {
                showToast("\u4EB2\uFF0C\u6CA1\u6709\u4E13\u8F91\u4FE1\u606F\u54E6~");
                return;
            }

        case 2131362891: 
            ToolUtil.onEvent(mCon, "Album_Collection");
            CollectFragment.isNeedRefresh = true;
            FeedCollectFragment.isNeedRefresh = true;
            favoriteAlbum(view);
            return;

        case 2131362748: 
            (new AlbumDownload(getActivity(), this, album, mCallback, downloadAlbum)).batchDownload();
            return;

        case 2131362895: 
            PlayTools.gotoPlay(3, ModelHelper.albumSoundlistToSoundInfoList(soundDataList), 0, mActivity, DataCollectUtil.getDataFromView(view));
            return;

        case 2131362423: 
            View view1 = LayoutInflater.from(getActivity()).inflate(0x7f03003a, null);
            view1.findViewById(0x7f0a0145).setOnClickListener(this);
            view1.findViewById(0x7f0a0148).setOnClickListener(this);
            view1.findViewById(0x7f0a0146).setOnClickListener(this);
            if (isHasDevice2Bind())
            {
                view1.findViewById(0x7f0a0146).setVisibility(0);
            } else
            {
                view1.findViewById(0x7f0a0146).setVisibility(8);
            }
            if (a.o && album != null && album.zoneId > 0)
            {
                view1.findViewById(0x7f0a0144).setVisibility(0);
                view1.findViewById(0x7f0a0144).setOnClickListener(this);
            } else
            {
                view1.findViewById(0x7f0a0144).setVisibility(8);
            }
            mMoreMenuPanel = new PopupWindow(view1, -2, -2);
            mMoreMenuPanel.setBackgroundDrawable(new BitmapDrawable());
            mMoreMenuPanel.setOutsideTouchable(true);
            mMoreMenuPanel.setFocusable(true);
            mMoreMenuPanel.showAsDropDown(view, Utilities.dip2px(getActivity(), -70F), 0);
            return;

        case 2131362120: 
            if (UserInfoMannage.hasLogined())
            {
                if (mMoreMenuPanel != null)
                {
                    mMoreMenuPanel.dismiss();
                }
                view = new Intent(mActivity, com/ximalaya/ting/android/activity/report/ReportActivity);
                view.putExtra("report_type", 2);
                view.putExtra("album_id", album.albumId);
                view.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(moreView));
                startActivity(view);
                return;
            } else
            {
                view = new Intent(mActivity, com/ximalaya/ting/android/activity/login/LoginActivity);
                view.setFlags(0x20000000);
                startActivity(view);
                return;
            }

        case 2131362118: 
            if (mMoreMenuPanel != null)
            {
                mMoreMenuPanel.dismiss();
            }
            continue; /* Loop/switch isn't completed */
        }
        if (true) goto _L1; else goto _L3
_L3:
        if (!DexManager.getInstance(mActivity).isDlnaInit()) goto _L1; else goto _L4
_L4:
        if (!isHasDevice2Bind())
        {
            showToast("\u6CA1\u6709\u53EF\u7528\u8BBE\u5907\u6765\u8BBE\u7F6E\u9891\u9053");
            return;
        } else
        {
            onSetChannel();
            return;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mFrom = bundle.getInt("from");
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030002, null);
        initUi(layoutinflater);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }
        unRegisterListener();
    }

    public void onPlayCanceled()
    {
    }

    public void onSoundChanged(int i)
    {
        if (mSoundsAdapter != null)
        {
            mSoundsAdapter.notifyDataSetChanged();
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
        if (i < 0 || soundinfo == null || i >= soundDataList.size() || ((AlbumSoundModel)soundDataList.get(i)).trackId != soundinfo.trackId || mSoundsAdapter == null)
        {
            return;
        } else
        {
            AlbumSoundModel albumsoundmodel = (AlbumSoundModel)soundDataList.get(i);
            albumsoundmodel.isLike = soundinfo.is_favorited;
            albumsoundmodel.likes = soundinfo.favorites_counts;
            mSoundsAdapter.notifyDataSetChanged();
            return;
        }
    }




/*
    static AlbumModel access$002(AlbumFragment albumfragment, AlbumModel albummodel)
    {
        albumfragment.album = albummodel;
        return albummodel;
    }

*/




















/*
    static Bitmap access$2602(AlbumFragment albumfragment, Bitmap bitmap)
    {
        albumfragment.floatViewBg = bitmap;
        return bitmap;
    }

*/







/*
    static MenuDialog access$3202(AlbumFragment albumfragment, MenuDialog menudialog)
    {
        albumfragment.menuDialog = menudialog;
        return menudialog;
    }

*/








    private class _cls1
        implements com.ximalaya.ting.android.transaction.download.AlbumDownload.FinishCallback
    {

        final AlbumFragment this$0;

        public void onFinish()
        {
            if (isAdded() && getFragmentManager() != null)
            {
                if (album != null)
                {
                    ShareToWeixinDialogFragment sharetoweixindialogfragment = ShareToWeixinDialogFragment.getInstance(album);
                    FragmentTransaction fragmenttransaction = getChildFragmentManager().beginTransaction();
                    fragmenttransaction.add(sharetoweixindialogfragment, "dialog");
                    fragmenttransaction.commitAllowingStateLoss();
                }
                if (mSoundsAdapter != null)
                {
                    mSoundsAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }

        _cls1()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls2 extends Handler
    {

        final AlbumFragment this$0;

        public void handleMessage(Message message)
        {
            message.what;
            JVM INSTR tableswitch 0 0: default 24
        //                       0 25;
               goto _L1 _L2
_L1:
            return;
_L2:
            if (mProgressDialog != null && mProgressDialog.isShowing())
            {
                message = (new StringBuilder()).append("\u6B63\u5728\u52A0\u5165\u4E0B\u8F7D\u961F\u5217:").append(message.arg1).append("/").append(message.arg2).toString();
                mProgressDialog.setMessage(message);
                return;
            }
            if (true) goto _L1; else goto _L3
_L3:
        }

        _cls2()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final AlbumFragment this$0;
        final boolean val$isDownloadStart;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            Object obj;
            avoid = "";
            obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/track").toString();
            RequestParams requestparams = new RequestParams();
            requestparams.put("albumId", (new StringBuilder()).append(album.albumId).append("").toString());
            requestparams.put("albumUid", (new StringBuilder()).append(album.uid).append("").toString());
            requestparams.put("pageId", "1");
            requestparams.put("pageSize", (new StringBuilder()).append(album.tracks).append("").toString());
            obj = f.a().a(((String) (obj)), requestparams, null, null);
            Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_351;
            }
            obj = JSON.parseObject(((String) (obj)));
            if (!"0".equals(((JSONObject) (obj)).get("ret").toString()))
            {
                break MISSING_BLOCK_LABEL_354;
            }
            avoid = (AlbumModel)JSON.parseObject(((JSONObject) (obj)).getString("album"), com/ximalaya/ting/android/model/album/AlbumModel);
            if (avoid == null)
            {
                break MISSING_BLOCK_LABEL_230;
            }
            album = avoid;
            avoid = JSON.parseArray(((JSONObject) (obj)).getJSONObject("tracks").getString("list"), com/ximalaya/ting/android/model/sound/AlbumSoundModel);
            class _cls1
                implements Runnable
            {

                final _cls3 this$1;

                public void run()
                {
                    if (mProgressDialog != null && mProgressDialog.isShowing())
                    {
                        mProgressDialog.setMessage(getResources().getString(0x7f090189));
                    }
                }

                _cls1()
                {
                    this$1 = _cls3.this;
                    super();
                }
            }

            if (isDownloadStart)
            {
                getActivity().runOnUiThread(new _cls1());
            }
            avoid = ModelHelper.toDownloadListForAlbum(avoid);
            setOrderNumber(avoid);
            avoid = DownloadHandler.getInstance(mCon).insertAll(avoid, isDownloadStart);
            if (avoid != null)
            {
                try
                {
                    return avoid.a();
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
                }
            } else
            {
                return "\u52A0\u5165\u4E0B\u8F7D\u5217\u8868\u5931\u8D25";
            }
            return "\u89E3\u6790json\u5F02\u5E38";
            avoid = "\u7F51\u7EDC\u8BF7\u6C42\u8D85\u65F6\uFF0C\u8BF7\u91CD\u8BD5";
            return avoid;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            while (!isAdded() || mProgressDialog == null || !mProgressDialog.isShowing() || !isDownloadStart) 
            {
                return;
            }
            mProgressDialog.setMessage(s);
            mProgressDialog.dismiss();
        }

        _cls3()
        {
            this$0 = AlbumFragment.this;
            isDownloadStart = flag;
            super();
        }
    }


    private class _cls4 extends MyAsyncTask
    {

        final AlbumFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            avoid = ApiUtil.getApiHost();
            if (!album.isFavorite) goto _L2; else goto _L1
_L1:
            Object obj = (new StringBuilder()).append(avoid).append("mobile/album/subscribe/delete").toString();
            avoid = ((Void []) (obj));
_L4:
            obj = new RequestParams();
            ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append("").append(album.albumId).toString());
            avoid = f.a().b(avoid, ((RequestParams) (obj)), mCollectBtn, mCollectBtn);
            if (TextUtils.isEmpty(avoid))
            {
                break MISSING_BLOCK_LABEL_182;
            }
            avoid = JSON.parseObject(avoid);
            if (avoid.getInteger("ret").intValue() == 0)
            {
                return "0";
            }
            break; /* Loop/switch isn't completed */
_L2:
            obj = (new StringBuilder()).append(avoid).append("mobile/album/subscribe/create").toString();
            avoid = ((Void []) (obj));
            continue; /* Loop/switch isn't completed */
            Exception exception;
            exception;
            exception.printStackTrace();
            if (true) goto _L4; else goto _L3
_L3:
            avoid = avoid.getString("msg");
            return avoid;
            avoid;
            Logger.e("AlbumFragment_favorite", "\u89E3\u6790JSON\u5F02\u5E38", avoid);
            return "\u7F51\u7EDC\u8BBF\u95EE\u5F02\u5E38";
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            boolean flag = true;
            if (!isAdded())
            {
                return;
            }
            mCollectBtn.setEnabled(true);
            mFloatCollectBtn.setEnabled(true);
            if (!"0".equals(s))
            {
                showToast(s);
                resetCollectImgStatus(album.isFavorite);
                return;
            }
            s = album;
            if (album.isFavorite)
            {
                flag = false;
            }
            s.isFavorite = flag;
        }

        _cls4()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls5 extends MyAsyncTask
    {

        final AlbumFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            AlbumModel albummodel = album;
            boolean flag;
            if (!album.isFavorite)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            albummodel.isFavorite = flag;
            if (!album.isFavorite)
            {
                avoid.deleteAlbumInLocalAlbumList(album);
            } else
            {
                avoid.saveAlbumModel(album);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            resetCollectImgStatus(album.isFavorite);
            mCollectBtn.setEnabled(true);
            mFloatCollectBtn.setEnabled(true);
        }

        _cls5()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls6
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final AlbumFragment this$0;

        public void onExecute()
        {
            downloadAlbum(true);
        }

        _cls6()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls7
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final AlbumFragment this$0;

        public void onExecute()
        {
            downloadAlbum(false);
        }

        _cls7()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls9
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final AlbumFragment this$0;

        public void onExecute()
        {
            downloadAlbum(false);
        }

        _cls9()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls8
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final AlbumFragment this$0;

        public void onExecute()
        {
            downloadAlbum(true);
        }

        _cls8()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls10
        implements android.view.View.OnClickListener
    {

        final AlbumFragment this$0;

        public void onClick(View view)
        {
            loadMoreData(mFooterViewLoading);
        }

        _cls10()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls11
        implements com.ximalaya.ting.android.view.bounceview.BounceViewHelper.PullFinishListener
    {

        final AlbumFragment this$0;

        public void pullDown()
        {
            Logger.log("pullDown");
        }

        public void pullUp()
        {
            Logger.log("pullUp");
            if (mFooterViewLoading.getVisibility() == 0)
            {
                loadMoreData(mListView);
            }
        }

        _cls11()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls12
        implements com.ximalaya.ting.android.view.bounceview.BounceHeadListView.OnCloneFloatVisibilityChangedCallback
    {

        final AlbumFragment this$0;

        public void OnCloneFloatVisibilityChanged(View view, int i)
        {
            if (i == 0)
            {
                buildFloatViewBackground();
            }
        }

        _cls12()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }



    private class _cls14
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final AlbumFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i < 0 || i >= mSoundsAdapter.getCount())
            {
                return false;
            } else
            {
                mSoundsAdapter.handleItemLongClick((Likeable)mSoundsAdapter.getData().get(i), view);
                return true;
            }
        }

        _cls14()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls19
        implements android.view.View.OnClickListener
    {

        final AlbumFragment this$0;

        public void onClick(View view)
        {
            dissmisSeletor();
        }

        _cls19()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls20
        implements android.widget.AdapterView.OnItemClickListener
    {

        final AlbumFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            adapterview = (PageIndex)mAlbumPagerAdapter.getItem(i);
            if (!soundData.loadingNextPage)
            {
                soundData.pageId = ((PageIndex) (adapterview)).pageIndex;
                soundData.clear = true;
                loadSoundListData(pageSelector);
            }
            dissmisSeletor();
        }

        _cls20()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls21
        implements android.widget.PopupWindow.OnDismissListener
    {

        final AlbumFragment this$0;

        public void onDismiss()
        {
            if (isAsc)
            {
                sortBtn.setCompoundDrawablesWithIntrinsicBounds(0x7f02029c, 0, 0, 0);
                sortBtnFloat.setCompoundDrawablesWithIntrinsicBounds(0x7f02029c, 0, 0, 0);
            } else
            {
                sortBtn.setCompoundDrawablesWithIntrinsicBounds(0x7f02029e, 0, 0, 0);
                sortBtnFloat.setCompoundDrawablesWithIntrinsicBounds(0x7f02029e, 0, 0, 0);
            }
            sortBtn.setTextColor(Color.parseColor("#333333"));
            sortBtnFloat.setTextColor(Color.parseColor("#333333"));
        }

        _cls21()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls15 extends MyAsyncTask
    {

        final AlbumFragment this$0;
        final long val$trackId;
        final View val$view;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient List doInBackground(Void avoid[])
        {
            Object obj;
            obj = new RequestParams();
            JSONObject jsonobject;
            if (isMyAlbum)
            {
                avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/my/album/track").toString();
                ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append(album.albumId).append("").toString());
                ((RequestParams) (obj)).put("albumUid", (new StringBuilder()).append(album.uid).append("").toString());
                ((RequestParams) (obj)).put("pageId", (new StringBuilder()).append(soundData.pageId).append("").toString());
                ((RequestParams) (obj)).put("pageSize", (new StringBuilder()).append(soundData.pageSize).append("").toString());
                ((RequestParams) (obj)).put("isAsc", (new StringBuilder()).append(isAsc).append("").toString());
            } else
            {
                avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
                avoid = (new StringBuilder()).append(avoid).append(album.albumId).append("/").append(isAsc).append("/").append(soundData.pageId).append("/").append(soundData.pageSize).toString();
                ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append(album.albumId).append("").toString());
                ((RequestParams) (obj)).put("isAsc", (new StringBuilder()).append(isAsc).append("").toString());
                ((RequestParams) (obj)).put("pageSize", (new StringBuilder()).append(soundData.pageSize).append("").toString());
            }
            avoid = f.a().a(avoid, ((RequestParams) (obj)), view, fragmentBaseContainerView);
            Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
            avoid = JSON.parseObject(avoid);
            if (!"0".equals(avoid.get("ret").toString())) goto _L2; else goto _L1
_L1:
            obj = (AlbumModel)JSON.parseObject(avoid.getString("album"), com/ximalaya/ting/android/model/album/AlbumModel);
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_299;
            }
            album = ((AlbumModel) (obj));
            jsonobject = avoid.getJSONObject("tracks");
            avoid = JSON.parseArray(jsonobject.getString("list"), com/ximalaya/ting/android/model/sound/AlbumSoundModel);
            if (avoid != null)
            {
                break MISSING_BLOCK_LABEL_332;
            }
            obj = new ArrayList();
            avoid = ((Void []) (obj));
            obj = avoid;
            if (soundData.totalCount != 0) goto _L4; else goto _L3
_L3:
            soundData.totalCount = jsonobject.getIntValue("totalCount");
            obj = avoid;
              goto _L4
            obj;
            avoid = null;
_L5:
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
            obj = avoid;
_L4:
            if (isMyAlbum || obj == null || ((List) (obj)).size() == 0 || UserInfoMannage.hasLogined() || album == null)
            {
                return ((List) (obj));
            }
            avoid = album;
            boolean flag;
            if (AlbumModelManage.getInstance().isHadCollected(album.albumId) != null)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            avoid.isFavorite = flag;
            return ((List) (obj));
            obj;
              goto _L5
            obj;
              goto _L5
_L2:
            obj = null;
            if (true) goto _L4; else goto _L6
_L6:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(final List result)
        {
            if (!isAdded())
            {
                return;
            }
            sortBtn.setClickable(true);
            sortBtnFloat.setClickable(true);
            soundData.loadingNextPage = false;
            if (result == null)
            {
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E");
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            if (mAlbumPagerAdapter != null)
            {
                mAlbumPagerAdapter.notifyDataSetChanged();
            }
            if (soundData.pageId == 1)
            {
                class _cls1
                    implements Runnable
                {

                    final _cls15 this$1;
                    final List val$result;

                    public void run()
                    {
                        if (isAdded())
                        {
                            soundDataList.clear();
                            soundDataList.addAll(result);
                            mSoundsAdapter.notifyDataSetChanged();
                            updateHeadView();
                            showSelectFooterView();
                            Object obj = soundData;
                            obj.pageId = ((LoadingData) (obj)).pageId + 1;
                            HashMap hashmap = new HashMap();
                            if (isMyAlbum)
                            {
                                obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/my/album/track/").toString();
                                hashmap.put("albumId", (new StringBuilder()).append(album.albumId).append("").toString());
                                hashmap.put("albumUid", (new StringBuilder()).append(album.uid).append("").toString());
                                hashmap.put("pageId", (new StringBuilder()).append(soundData.pageId).append("").toString());
                                hashmap.put("pageSize", (new StringBuilder()).append(soundData.pageSize).append("").toString());
                                hashmap.put("isAsc", (new StringBuilder()).append(isAsc).append("").toString());
                            } else
                            {
                                obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
                                hashmap.put("albumId", (new StringBuilder()).append(album.albumId).append("").toString());
                                hashmap.put("isAsc", (new StringBuilder()).append(isAsc).append("").toString());
                                hashmap.put("pageSize", (new StringBuilder()).append(soundData.pageSize).append("").toString());
                            }
                            mSoundsAdapter.setDataSource(((String) (obj)));
                            mSoundsAdapter.setPageId(soundData.pageId);
                            mSoundsAdapter.setParams(hashmap);
                            doPlay(trackId);
                        }
                    }

                _cls1()
                {
                    this$1 = _cls15.this;
                    result = list;
                    super();
                }
                }

                result = new _cls1();
                long l = getAnimationLeftTime();
                if (l > 0L)
                {
                    mListView.postDelayed(result, l);
                } else
                {
                    result.run();
                }
                updateAlbumData();
            } else
            {
                if (soundData.clear)
                {
                    soundDataList.clear();
                    soundData.clear = false;
                }
                soundDataList.addAll(result);
                mSoundsAdapter.notifyDataSetChanged();
                showSelectFooterView();
                result = soundData;
                result.pageId = ((LoadingData) (result)).pageId + 1;
            }
            resetCollectImgStatus(album.isFavorite);
        }

        protected void onPreExecute()
        {
            soundData.loadingNextPage = true;
            sortBtn.setClickable(false);
            sortBtnFloat.setClickable(false);
        }

        _cls15()
        {
            this$0 = AlbumFragment.this;
            view = view1;
            trackId = l;
            super();
        }
    }



    private class _cls16 extends OnPlayerStatusUpdateListenerProxy
    {

        final AlbumFragment this$0;

        public void onPlayStateChange()
        {
            mSoundsAdapter.notifyDataSetChanged();
        }

        _cls16()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls17
        implements com.ximalaya.ting.android.util.ImageManager2.DisplayCallback
    {

        final AlbumFragment this$0;

        public void onCompleteDisplay(String s, Bitmap bitmap)
        {
            if (mCon == null)
            {
                return;
            }
            Bitmap bitmap1 = ImageManager2.from(mCon).getFromMemCache((new StringBuilder()).append(s).append("/blur").toString());
            if (bitmap1 != null)
            {
                coverBg.setImageBitmap(bitmap1);
            } else
            if (bitmap != null)
            {
                coverBg.blur(new BitmapDrawable(mCon.getResources(), bitmap), (new StringBuilder()).append(s).append("/blur").toString(), true);
            } else
            {
                coverBg.setImageDrawable(new ColorDrawable(Color.parseColor("#b3202332")));
                coverBg.setResourceUrl(null);
            }
            floatViewBg = null;
        }

        _cls17()
        {
            this$0 = AlbumFragment.this;
            super();
        }
    }


    private class _cls18
        implements android.view.View.OnClickListener
    {

        final AlbumFragment this$0;
        final String val$tag;

        public void onClick(View view)
        {
            TagRelativeAlbumListFragment tagrelativealbumlistfragment = TagRelativeAlbumListFragment.getInstance(tag);
            Bundle bundle = tagrelativealbumlistfragment.getArguments();
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            tagrelativealbumlistfragment.setArguments(bundle);
            startFragment(tagrelativealbumlistfragment);
        }

        _cls18()
        {
            this$0 = AlbumFragment.this;
            tag = s;
            super();
        }
    }

}
