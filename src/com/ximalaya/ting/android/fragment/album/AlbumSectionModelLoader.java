// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.Context;
import com.ximalaya.ting.android.model.album.AlbumSectionModel;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.Iterator;
import java.util.List;

class AlbumSectionModelLoader extends MyAsyncTaskLoader
{

    private long mAlbumId;
    private AlbumSectionModel mData;
    private int mPageId;

    public AlbumSectionModelLoader(Context context, long l, int i)
    {
        super(context);
        mAlbumId = l;
        mPageId = i;
    }

    public void deliverResult(AlbumSectionModel albumsectionmodel)
    {
        super.deliverResult(albumsectionmodel);
        mData = albumsectionmodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((AlbumSectionModel)obj);
    }

    public AlbumSectionModel loadInBackground()
    {
        mData = AlbumSectionModel.get(mAlbumId, mPageId, fromBindView, toBindView);
        if (mData != null && mData.getTracks() != null && mData.getTracks().getList() != null)
        {
            for (Iterator iterator = mData.getTracks().getList().iterator(); iterator.hasNext();)
            {
                com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
                if (DownloadHandler.getInstance(getContext()).isInDownloadList(track.getTrackId()))
                {
                    track.setIsDownload(true);
                } else
                {
                    track.setIsDownload(false);
                }
            }

        }
        return mData;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData != null)
        {
            deliverResult(mData);
            return;
        } else
        {
            forceLoad();
            return;
        }
    }
}
