// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumRelativeAlbumListFragment, GeneralAlbumListApdater, AlbumRelativeAlbumModel, AlbumFragment

class this._cls0
    implements android.widget.lativeAlbumListFragment._cls2
{

    final AlbumRelativeAlbumListFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        i -= mListView.getHeaderViewsCount();
        if (i >= 0 && i < AlbumRelativeAlbumListFragment.access$100(AlbumRelativeAlbumListFragment.this).getCount())
        {
            adapterview = (AlbumRelativeAlbumModel)AlbumRelativeAlbumListFragment.access$100(AlbumRelativeAlbumListFragment.this).getData().get(i);
            com.ximalaya.ting.android.model.album.AlbumModel albummodel = ModelHelper.toAlbumModel(adapterview);
            Bundle bundle = new Bundle();
            bundle.putString("album", JSON.toJSONString(albummodel));
            bundle.putString("rec_src", adapterview.getRecSrc());
            bundle.putString("rec_track", adapterview.getRecTrack());
            bundle.putInt("from", 10);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
        }
    }

    ()
    {
        this$0 = AlbumRelativeAlbumListFragment.this;
        super();
    }
}
