// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            TagRelativeAlbumListModel

public class TagRelativeAlbumListLoader extends MyAsyncTaskLoader
{

    private static final String PATH = "m/tags/get_albums";
    private TagRelativeAlbumListModel mData;
    private int mPage;
    private String mTagName;

    public TagRelativeAlbumListLoader(Context context, String s, int i)
    {
        super(context);
        mTagName = s;
        mPage = i;
    }

    public void deliverResult(TagRelativeAlbumListModel tagrelativealbumlistmodel)
    {
        super.deliverResult(tagrelativealbumlistmodel);
        mData = tagrelativealbumlistmodel;
    }

    public volatile void deliverResult(Object obj)
    {
        deliverResult((TagRelativeAlbumListModel)obj);
    }

    public TagRelativeAlbumListModel loadInBackground()
    {
        Object obj;
        obj = new RequestParams();
        ((RequestParams) (obj)).put("tname", mTagName);
        ((RequestParams) (obj)).put("page", mPage);
        ((RequestParams) (obj)).put("sort", "hot");
        obj = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("m/tags/get_albums").toString(), ((RequestParams) (obj)), fromBindView, toBindView, false);
        fromBindView = null;
        toBindView = null;
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_143;
        }
        if (JSONObject.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a).getInteger("ret").intValue() != 0)
        {
            break MISSING_BLOCK_LABEL_143;
        }
        mData = TagRelativeAlbumListModel.getFromJson(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        obj = mData;
        return ((TagRelativeAlbumListModel) (obj));
        Exception exception;
        exception;
        exception.printStackTrace();
        return null;
    }

    public volatile Object loadInBackground()
    {
        return loadInBackground();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (mData != null)
        {
            deliverResult(mData);
            return;
        } else
        {
            forceLoad();
            return;
        }
    }
}
