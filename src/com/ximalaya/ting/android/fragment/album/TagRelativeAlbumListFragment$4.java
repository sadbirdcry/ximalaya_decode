// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.support.v4.app.LoaderManager;
import android.view.View;
import android.widget.ListView;
import com.ximalaya.ting.android.util.MyCallback;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            TagRelativeAlbumListModel, TagRelativeAlbumListFragment, GeneralAlbumListApdater

class val.data
    implements MyCallback
{

    final TagRelativeAlbumListFragment this$0;
    final TagRelativeAlbumListModel val$data;

    public void execute()
    {
        if (val$data != null && val$data.getList() != null && val$data.getList().size() > 0)
        {
            TagRelativeAlbumListFragment.access$500(TagRelativeAlbumListFragment.this).setVisibility(8);
            TagRelativeAlbumListFragment.access$102(TagRelativeAlbumListFragment.this, val$data.getCount());
            if (TagRelativeAlbumListFragment.access$000(TagRelativeAlbumListFragment.this) == null)
            {
                TagRelativeAlbumListFragment.access$002(TagRelativeAlbumListFragment.this, new GeneralAlbumListApdater(getActivity(), val$data.getList()));
                mListView.setAdapter(TagRelativeAlbumListFragment.access$000(TagRelativeAlbumListFragment.this));
            } else
            {
                TagRelativeAlbumListFragment.access$000(TagRelativeAlbumListFragment.this).addData(val$data.getList());
            }
            if (TagRelativeAlbumListFragment.access$000(TagRelativeAlbumListFragment.this).getCount() > 0)
            {
                if (TagRelativeAlbumListFragment.access$600(TagRelativeAlbumListFragment.this) == null)
                {
                    TagRelativeAlbumListFragment.access$602(TagRelativeAlbumListFragment.this, getLoaderManager().initLoader(1, null, TagRelativeAlbumListFragment.access$700(TagRelativeAlbumListFragment.this)));
                } else
                {
                    TagRelativeAlbumListFragment.access$602(TagRelativeAlbumListFragment.this, getLoaderManager().restartLoader(1, null, TagRelativeAlbumListFragment.access$700(TagRelativeAlbumListFragment.this)));
                }
            }
            showFooterView(com.ximalaya.ting.android.fragment.E_ALL);
        } else
        {
            int _tmp = TagRelativeAlbumListFragment.access$806(TagRelativeAlbumListFragment.this);
            if (TagRelativeAlbumListFragment.access$800(TagRelativeAlbumListFragment.this) > 0)
            {
                showFooterView(com.ximalaya.ting.android.fragment.E);
            } else
            {
                TagRelativeAlbumListFragment.access$500(TagRelativeAlbumListFragment.this).setVisibility(0);
            }
        }
        TagRelativeAlbumListFragment.access$202(TagRelativeAlbumListFragment.this, false);
    }

    ()
    {
        this$0 = final_tagrelativealbumlistfragment;
        val$data = TagRelativeAlbumListModel.this;
        super();
    }
}
