// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.view.View;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumFragment

class this._cls0 extends MyAsyncTask
{

    final AlbumFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        avoid = AlbumModelManage.getInstance();
        AlbumModel albummodel = AlbumFragment.access$000(AlbumFragment.this);
        boolean flag;
        if (!AlbumFragment.access$000(AlbumFragment.this).isFavorite)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        albummodel.isFavorite = flag;
        if (!AlbumFragment.access$000(AlbumFragment.this).isFavorite)
        {
            avoid.deleteAlbumInLocalAlbumList(AlbumFragment.access$000(AlbumFragment.this));
        } else
        {
            avoid.saveAlbumModel(AlbumFragment.access$000(AlbumFragment.this));
        }
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        AlbumFragment.access$600(AlbumFragment.this, AlbumFragment.access$000(AlbumFragment.this).isFavorite);
        AlbumFragment.access$400(AlbumFragment.this).setEnabled(true);
        AlbumFragment.access$500(AlbumFragment.this).setEnabled(true);
    }

    ()
    {
        this$0 = AlbumFragment.this;
        super();
    }
}
