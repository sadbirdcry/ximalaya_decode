// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumFragment

class this._cls0 extends MyAsyncTask
{

    final AlbumFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient String doInBackground(Void avoid[])
    {
        avoid = ApiUtil.getApiHost();
        if (!AlbumFragment.access$000(AlbumFragment.this).isFavorite) goto _L2; else goto _L1
_L1:
        Object obj = (new StringBuilder()).append(avoid).append("mobile/album/subscribe/delete").toString();
        avoid = ((Void []) (obj));
_L4:
        obj = new RequestParams();
        ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append("").append(AlbumFragment.access$000(AlbumFragment.this).albumId).toString());
        avoid = f.a().b(avoid, ((RequestParams) (obj)), AlbumFragment.access$400(AlbumFragment.this), AlbumFragment.access$400(AlbumFragment.this));
        if (TextUtils.isEmpty(avoid))
        {
            break MISSING_BLOCK_LABEL_182;
        }
        avoid = JSON.parseObject(avoid);
        if (avoid.getInteger("ret").intValue() == 0)
        {
            return "0";
        }
        break; /* Loop/switch isn't completed */
_L2:
        obj = (new StringBuilder()).append(avoid).append("mobile/album/subscribe/create").toString();
        avoid = ((Void []) (obj));
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        exception.printStackTrace();
        if (true) goto _L4; else goto _L3
_L3:
        avoid = avoid.getString("msg");
        return avoid;
        avoid;
        Logger.e("AlbumFragment_favorite", "\u89E3\u6790JSON\u5F02\u5E38", avoid);
        return "\u7F51\u7EDC\u8BBF\u95EE\u5F02\u5E38";
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((String)obj);
    }

    protected void onPostExecute(String s)
    {
        boolean flag = true;
        if (!isAdded())
        {
            return;
        }
        AlbumFragment.access$400(AlbumFragment.this).setEnabled(true);
        AlbumFragment.access$500(AlbumFragment.this).setEnabled(true);
        if (!"0".equals(s))
        {
            showToast(s);
            AlbumFragment.access$600(AlbumFragment.this, AlbumFragment.access$000(AlbumFragment.this).isFavorite);
            return;
        }
        s = AlbumFragment.access$000(AlbumFragment.this);
        if (AlbumFragment.access$000(AlbumFragment.this).isFavorite)
        {
            flag = false;
        }
        s.isFavorite = flag;
    }

    ()
    {
        this$0 = AlbumFragment.this;
        super();
    }
}
