// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import com.ximalaya.ting.android.activity.share.ShareToWeixinDialogFragment;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumSectionDownloadFragment, SectionContentAdapter

class this._cls0
    implements com.ximalaya.ting.android.communication.allback
{

    final AlbumSectionDownloadFragment this$0;

    public void onFinishSaveTask()
    {
        if (isAdded() && getFragmentManager() != null)
        {
            if (mSectionContentAdapter != null)
            {
                mSectionContentAdapter.refreshDownloadStatus();
                updateBottomBarInfo();
                AlbumSectionDownloadFragment.access$600(AlbumSectionDownloadFragment.this);
            }
            if (AlbumSectionDownloadFragment.access$900(AlbumSectionDownloadFragment.this) != null)
            {
                ShareToWeixinDialogFragment sharetoweixindialogfragment = ShareToWeixinDialogFragment.getInstance(AlbumSectionDownloadFragment.access$900(AlbumSectionDownloadFragment.this));
                FragmentTransaction fragmenttransaction = getFragmentManager().beginTransaction();
                fragmenttransaction.add(sharetoweixindialogfragment, "dialog");
                fragmenttransaction.commitAllowingStateLoss();
                return;
            }
        }
    }

    llback()
    {
        this$0 = AlbumSectionDownloadFragment.this;
        super();
    }
}
