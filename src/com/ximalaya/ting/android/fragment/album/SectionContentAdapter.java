// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.ximalaya.ting.android.model.album.AlbumSectionModel;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SectionContentAdapter extends BaseAdapter
{
    public class ViewHolder
    {

        public CheckBox mCheckBox;
        public TextView mSize;
        public TextView mTitle;
        final SectionContentAdapter this$0;

        public ViewHolder()
        {
            this$0 = SectionContentAdapter.this;
            super();
        }
    }


    protected Context mContext;
    protected AlbumSectionModel mData;
    private LayoutInflater mInflater;

    public SectionContentAdapter(Context context, AlbumSectionModel albumsectionmodel)
    {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mData = albumsectionmodel;
    }

    public void checkAll()
    {
        if (getCount() <= 0)
        {
            return;
        }
        Iterator iterator = mData.getTracks().getList().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
            if (!track.isDownload())
            {
                track.setIsChecked(true);
            }
        } while (true);
        notifyDataSetChanged();
    }

    public List getCheckedTracks()
    {
        ArrayList arraylist = null;
        if (getCount() > 0)
        {
            arraylist = new ArrayList();
            Iterator iterator = mData.getTracks().getList().iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
                if (track.getIsChecked())
                {
                    arraylist.add(track);
                }
            } while (true);
        }
        return arraylist;
    }

    public int getCount()
    {
        if (mData == null || mData.getTracks() == null || mData.getTracks().getList() == null)
        {
            return 0;
        } else
        {
            return mData.getTracks().getList().size();
        }
    }

    public AlbumSectionModel getData()
    {
        return mData;
    }

    public Object getItem(int i)
    {
        return mData.getTracks().getList().get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track;
        if (view == null)
        {
            ViewHolder viewholder = new ViewHolder();
            view = mInflater.inflate(0x7f030110, viewgroup, false);
            viewholder.mTitle = (TextView)view.findViewById(0x7f0a0175);
            viewholder.mSize = (TextView)view.findViewById(0x7f0a045d);
            viewholder.mCheckBox = (CheckBox)view.findViewById(0x7f0a045e);
            view.setTag(viewholder);
            viewgroup = viewholder;
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)mData.getTracks().getList().get(i);
        ((ViewHolder) (viewgroup)).mTitle.setText(track.getTitle());
        ((ViewHolder) (viewgroup)).mSize.setText(ToolUtil.formatFileSize(track.getDownloadSize()));
        if (track.isDownload())
        {
            ((ViewHolder) (viewgroup)).mTitle.setTextColor(Color.parseColor("#b2bac8"));
            ((ViewHolder) (viewgroup)).mCheckBox.setButtonDrawable(0x7f020151);
        } else
        {
            ((ViewHolder) (viewgroup)).mTitle.setTextColor(Color.parseColor("#394257"));
            ((ViewHolder) (viewgroup)).mCheckBox.setButtonDrawable(0x7f02014e);
        }
        ((ViewHolder) (viewgroup)).mCheckBox.setChecked(track.getIsChecked());
        return view;
    }

    public boolean isAllChecked()
    {
        if (getCount() > 0)
        {
            for (Iterator iterator = mData.getTracks().getList().iterator(); iterator.hasNext();)
            {
                com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
                if (!track.isDownload() && !track.getIsChecked())
                {
                    return false;
                }
            }

            return true;
        } else
        {
            return false;
        }
    }

    public boolean isItemClickable(int i)
    {
        if (i < 0 || i >= getCount())
        {
            return false;
        }
        return !((com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)mData.getTracks().getList().get(i)).isDownload();
    }

    public boolean isOneChecked()
    {
label0:
        {
            if (getCount() <= 0)
            {
                break label0;
            }
            Iterator iterator = mData.getTracks().getList().iterator();
            com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track;
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
                track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
            } while (track.isDownload() || !track.getIsChecked());
            return true;
        }
        return false;
    }

    public void refreshDownloadStatus()
    {
        if (getCount() > 0)
        {
            for (Iterator iterator = mData.getTracks().getList().iterator(); iterator.hasNext();)
            {
                com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
                if (DownloadHandler.getInstance(mContext).isInDownloadList(track.getTrackId()))
                {
                    track.setIsDownload(true);
                    track.setIsChecked(false);
                } else
                {
                    track.setIsDownload(false);
                }
            }

        }
        notifyDataSetChanged();
    }

    public void reset(AlbumSectionModel albumsectionmodel)
    {
        mData = albumsectionmodel;
        notifyDataSetChanged();
    }

    public void toggle(int i)
    {
        if (i < 0 || i >= getCount())
        {
            return;
        }
        com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)mData.getTracks().getList().get(i);
        boolean flag;
        if (!track.getIsChecked())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        track.setIsChecked(flag);
        notifyDataSetChanged();
    }

    public void uncheckAll()
    {
        if (getCount() <= 0)
        {
            return;
        }
        Iterator iterator = mData.getTracks().getList().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
            if (!track.isDownload())
            {
                track.setIsChecked(false);
            }
        } while (true);
        notifyDataSetChanged();
    }
}
