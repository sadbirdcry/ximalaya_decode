// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.album.AlbumSectionModel;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.MultiDirectionSlidingDrawer;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            SectionContentAdapter, AlbumSectionModelLoader, SectionAdapter

public class AlbumSectionDownloadFragment extends BaseActivityLikeFragment
    implements android.support.v4.app.LoaderManager.LoaderCallbacks, android.view.View.OnClickListener, com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
{
    private class LoadingDialog extends AlertDialog
    {

        final AlbumSectionDownloadFragment this$0;

        public LoadingDialog(Context context, int i)
        {
            this$0 = AlbumSectionDownloadFragment.this;
            super(context, i);
        }
    }


    private static final int LOAD_SECTION_DATA = 1;
    private String from;
    private long mAlbumId;
    private AlbumModel mAlbumModel;
    private View mBottomBar;
    protected TextView mBottomInfoBar;
    private CheckBox mCheckAllCheckBox;
    private ListView mContentListView;
    private int mCurrPage;
    private TextView mCurrSection;
    private Button mDownloadBtn;
    private Button mDownloadingBtn;
    public TextView mDownloadingCount;
    private Loader mLoader;
    private boolean mLoadingData;
    private View mNoNetworkLayout;
    private AlertDialog mProgressDialog;
    private MultiDirectionSlidingDrawer mPullDownContainer;
    private SectionAdapter mSectionAdapter;
    protected SectionContentAdapter mSectionContentAdapter;
    private GridView mSectionGridView;
    private View mTopBar;
    private View mTopInfoBar;
    private TextView mTotalTrackCount;

    public AlbumSectionDownloadFragment()
    {
        from = "";
    }

    private void changeEnvWithCheckStatus()
    {
label0:
        {
            if (mSectionContentAdapter != null)
            {
                if (!mSectionContentAdapter.isOneChecked())
                {
                    break label0;
                }
                mDownloadBtn.setEnabled(true);
            }
            return;
        }
        mDownloadBtn.setEnabled(false);
    }

    private void dismissDialog()
    {
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }
    }

    public static AlbumSectionDownloadFragment newInstance(AlbumModel albummodel)
    {
        AlbumSectionDownloadFragment albumsectiondownloadfragment = new AlbumSectionDownloadFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", albummodel);
        albumsectiondownloadfragment.setArguments(bundle);
        return albumsectiondownloadfragment;
    }

    private void showContentLayout(boolean flag)
    {
        if (flag)
        {
            mContentListView.setVisibility(0);
            mTopInfoBar.setVisibility(0);
            mBottomInfoBar.setVisibility(0);
            mBottomBar.setVisibility(0);
            return;
        } else
        {
            mContentListView.setVisibility(4);
            mTopInfoBar.setVisibility(4);
            mBottomInfoBar.setVisibility(4);
            mBottomBar.setVisibility(4);
            return;
        }
    }

    private void showLoadingDialog()
    {
        if (mProgressDialog == null)
        {
            ProgressBar progressbar = new ProgressBar(getActivity());
            progressbar.setIndeterminate(false);
            progressbar.setLayoutParams(new android.view.ViewGroup.LayoutParams(-2, -2));
            progressbar.setBackgroundResource(0x106000d);
            mProgressDialog = new LoadingDialog(getActivity(), 0x7f0b000e);
            mProgressDialog.setView(progressbar);
            mProgressDialog.getWindow().setBackgroundDrawableResource(0x7f070003);
        }
        mProgressDialog.show();
    }

    private void showNoNetworkLayout(boolean flag)
    {
        View view = mNoNetworkLayout;
        int i;
        if (flag)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        view.setVisibility(i);
    }

    private void toggleCheckAll(boolean flag)
    {
        if (mSectionContentAdapter != null)
        {
            if (flag)
            {
                mSectionContentAdapter.checkAll();
            } else
            {
                mSectionContentAdapter.uncheckAll();
            }
            updateBottomBarInfo();
        }
    }

    protected SectionContentAdapter getContentAdapter(AlbumSectionModel albumsectionmodel)
    {
        if (mSectionContentAdapter == null)
        {
            mSectionContentAdapter = new SectionContentAdapter(getActivity(), albumsectionmodel);
        }
        return mSectionContentAdapter;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mCheckAllCheckBox.setOnClickListener(this);
        mDownloadBtn.setOnClickListener(this);
        mDownloadingBtn.setOnClickListener(this);
        mCurrSection.setOnClickListener(this);
        mNoNetworkLayout.setOnClickListener(this);
        findViewById(0x7f0a02d3).setOnClickListener(this);
        updateDownloadingCount(DownloadHandler.getInstance(getActivity()).getIncompleteTaskCount());
        DownloadHandler.getInstance(getActivity()).addDownloadListeners(this);
        mCurrPage = 0;
        bundle = new Bundle();
        bundle.putLong("album_id", mAlbumId);
        bundle.putInt("page_id", mCurrPage + 1);
        mLoadingData = true;
        mLoader = getLoaderManager().initLoader(1, bundle, this);
        ((MyAsyncTaskLoader)mLoader).setXDCSBindView(fragmentBaseContainerView, fragmentBaseContainerView);
        mSectionGridView.setOnItemClickListener(new _cls1());
        mContentListView.setOnItemClickListener(new _cls2());
        DownloadHandler.getInstance(getActivity()).addDownloadListeners(this);
        mPullDownContainer.setCallback(new _cls3());
    }

    public boolean onBackPressed()
    {
        if (mPullDownContainer != null && mPullDownContainer.isShowing())
        {
            mPullDownContainer.closePullDownPanel();
            return true;
        } else
        {
            return super.onBackPressed();
        }
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 7: default 72
    //                   2131361915: 129
    //                   2131362504: 134
    //                   2131362508: 194
    //                   2131362513: 89
    //                   2131362514: 73
    //                   2131362515: 227
    //                   2131362518: 236;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8
_L1:
        return;
_L6:
        toggleCheckAll(((CheckBox)view).isChecked());
        changeEnvWithCheckStatus();
        return;
_L5:
        view = new _cls4();
        List list = ModelHelper.toDownloadTaskList(mSectionContentAdapter.getCheckedTracks());
        if (list != null)
        {
            DownLoadTools.getInstance().goDownload(list, getActivity().getApplicationContext(), view);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        finishFragment();
        return;
_L3:
        Intent intent = new Intent(getActivity(), com/ximalaya/ting/android/activity/MainTabActivity2);
        intent.putExtra("show_main_tab", true);
        intent.putExtra("download", true);
        intent.putExtra("page", 2);
        intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        startActivity(intent);
        return;
_L4:
        if (mPullDownContainer != null)
        {
            if (mPullDownContainer.isShowing())
            {
                mPullDownContainer.closePullDownPanel();
                return;
            } else
            {
                mPullDownContainer.openPullDownPanel();
                return;
            }
        }
        if (true) goto _L1; else goto _L7
_L7:
        mCheckAllCheckBox.performClick();
        return;
_L8:
        if (NetworkUtils.isNetworkAvaliable(getActivity()))
        {
            Bundle bundle = new Bundle();
            bundle.putLong("album_id", mAlbumId);
            bundle.putInt("page_id", mCurrPage + 1);
            mLoadingData = true;
            if (mLoader == null)
            {
                mLoader = getLoaderManager().initLoader(1, bundle, this);
            } else
            {
                mLoader = getLoaderManager().restartLoader(1, bundle, this);
            }
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, fragmentBaseContainerView);
            return;
        }
        if (true) goto _L1; else goto _L9
_L9:
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mAlbumModel = (AlbumModel)getArguments().getSerializable("data");
        mAlbumId = mAlbumModel.albumId;
        hidePlayButton();
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        switch (i)
        {
        default:
            return null;

        case 1: // '\001'
            showNoNetworkLayout(false);
            break;
        }
        showContentLayout(true);
        showLoadingDialog();
        long l = bundle.getLong("album_id");
        i = bundle.getInt("page_id");
        return new AlbumSectionModelLoader(getActivity(), l, i);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f03009c, viewgroup, false);
        fragmentBaseContainerView = layoutinflater;
        mSectionGridView = (GridView)layoutinflater.findViewById(0x7f0a02cf);
        mContentListView = (ListView)layoutinflater.findViewById(0x7f0a0021);
        mBottomInfoBar = (TextView)layoutinflater.findViewById(0x7f0a02d4);
        mCheckAllCheckBox = (CheckBox)layoutinflater.findViewById(0x7f0a02d2);
        mDownloadingCount = (TextView)layoutinflater.findViewById(0x7f0a02c9);
        mDownloadBtn = (Button)layoutinflater.findViewById(0x7f0a02d1);
        mDownloadingBtn = (Button)layoutinflater.findViewById(0x7f0a02c8);
        mPullDownContainer = (MultiDirectionSlidingDrawer)layoutinflater.findViewById(0x7f0a02cd);
        mPullDownContainer.disallowInterceptTouchEvent(true);
        mTotalTrackCount = (TextView)layoutinflater.findViewById(0x7f0a02cb);
        mCurrSection = (TextView)layoutinflater.findViewById(0x7f0a02cc);
        mNoNetworkLayout = layoutinflater.findViewById(0x7f0a02d6);
        mBottomBar = layoutinflater.findViewById(0x7f0a02ce);
        mTopBar = layoutinflater.findViewById(0x7f0a005f);
        mTopInfoBar = layoutinflater.findViewById(0x7f0a02ca);
        return layoutinflater;
    }

    public void onDestroy()
    {
        super.onDestroy();
        showPlayButton();
        DownloadHandler.getInstance(getActivity()).removeDownloadListeners(this);
    }

    public void onLoadFinished(Loader loader, final AlbumSectionModel data)
    {
        switch (loader.getId())
        {
        default:
            return;

        case 1: // '\001'
            doAfterAnimation(new _cls5());
            break;
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (AlbumSectionModel)obj);
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void onTaskComplete()
    {
    }

    public void onTaskDelete()
    {
    }

    public void updateActionInfo()
    {
    }

    protected void updateBottomBarInfo()
    {
        if (mSectionContentAdapter != null)
        {
            if (mSectionContentAdapter.getCount() > 0)
            {
                Iterator iterator = mSectionContentAdapter.getData().getTracks().getList().iterator();
                int i = 0;
                long l = 0L;
                do
                {
                    if (!iterator.hasNext())
                    {
                        break;
                    }
                    com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track = (com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)iterator.next();
                    if (track.getIsChecked())
                    {
                        l += track.getDownloadSize();
                        i++;
                    }
                } while (true);
                if (i > 0)
                {
                    mBottomInfoBar.setVisibility(0);
                    double d = StorageUtils.getFreeStorageSize();
                    if ((double)l > d)
                    {
                        mBottomInfoBar.setText(0x7f0901f2);
                        return;
                    } else
                    {
                        String s = getString(0x7f0901f1, new Object[] {
                            Integer.valueOf(i), ToolUtil.formatFileSize(l), ToolUtil.formatFriendlyFileSize(d)
                        });
                        mBottomInfoBar.setText(s);
                        return;
                    }
                } else
                {
                    mBottomInfoBar.setVisibility(8);
                    return;
                }
            } else
            {
                mBottomInfoBar.setVisibility(8);
                return;
            }
        } else
        {
            mBottomInfoBar.setVisibility(8);
            return;
        }
    }

    public void updateDownloadInfo(final int count)
    {
        FragmentActivity fragmentactivity = getActivity();
        if (fragmentactivity != null && isAdded())
        {
            fragmentactivity.runOnUiThread(new _cls6());
        }
    }

    protected void updateDownloadingCount(int i)
    {
        if (i <= 0)
        {
            mDownloadingCount.setVisibility(4);
            return;
        } else
        {
            mDownloadingCount.setText(String.valueOf(i));
            mDownloadingCount.setVisibility(0);
            return;
        }
    }



/*
    static SectionAdapter access$002(AlbumSectionDownloadFragment albumsectiondownloadfragment, SectionAdapter sectionadapter)
    {
        albumsectiondownloadfragment.mSectionAdapter = sectionadapter;
        return sectionadapter;
    }

*/












/*
    static boolean access$302(AlbumSectionDownloadFragment albumsectiondownloadfragment, boolean flag)
    {
        albumsectiondownloadfragment.mLoadingData = flag;
        return flag;
    }

*/



/*
    static int access$402(AlbumSectionDownloadFragment albumsectiondownloadfragment, int i)
    {
        albumsectiondownloadfragment.mCurrPage = i;
        return i;
    }

*/






    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final AlbumSectionDownloadFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (mSectionAdapter != null)
            {
                mSectionAdapter.selectItemAt(i);
            }
            adapterview = new Bundle();
            adapterview.putLong("album_id", mAlbumId);
            adapterview.putInt("page_id", i + 1);
            getLoaderManager().restartLoader(1, adapterview, AlbumSectionDownloadFragment.this);
            ((MyAsyncTaskLoader)mLoader).setXDCSBindView(view, fragmentBaseContainerView);
            mLoadingData = true;
            mCurrPage = i;
        }

        _cls1()
        {
            this$0 = AlbumSectionDownloadFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final AlbumSectionDownloadFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (mSectionContentAdapter != null && mSectionContentAdapter.isItemClickable(i))
            {
                mSectionContentAdapter.toggle(i);
                mCheckAllCheckBox.setChecked(mSectionContentAdapter.isAllChecked());
                updateBottomBarInfo();
                changeEnvWithCheckStatus();
            }
        }

        _cls2()
        {
            this$0 = AlbumSectionDownloadFragment.this;
            super();
        }
    }


    private class _cls3
        implements com.ximalaya.ting.android.view.MultiDirectionSlidingDrawer.Callback
    {

        final AlbumSectionDownloadFragment this$0;

        public void onCompletePullBack()
        {
            if (mCurrSection != null)
            {
                mCurrSection.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f0202a1, 0);
            }
            mCheckAllCheckBox.setEnabled(true);
            if (mSectionContentAdapter != null && mSectionContentAdapter.isOneChecked() && !mLoadingData)
            {
                mBottomInfoBar.setVisibility(0);
                mDownloadBtn.setEnabled(true);
            }
        }

        public void onCompletePullDown()
        {
            if (mCurrSection != null)
            {
                mCurrSection.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f0202a3, 0);
            }
            mDownloadBtn.setEnabled(false);
            mCheckAllCheckBox.setEnabled(false);
            mBottomInfoBar.setVisibility(8);
        }

        public void onStartPullDown()
        {
        }

        _cls3()
        {
            this$0 = AlbumSectionDownloadFragment.this;
            super();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.communication.DownLoadTools.OnFinishSaveTaskCallback
    {

        final AlbumSectionDownloadFragment this$0;

        public void onFinishSaveTask()
        {
            if (isAdded() && getFragmentManager() != null)
            {
                if (mSectionContentAdapter != null)
                {
                    mSectionContentAdapter.refreshDownloadStatus();
                    updateBottomBarInfo();
                    changeEnvWithCheckStatus();
                }
                if (mAlbumModel != null)
                {
                    ShareToWeixinDialogFragment sharetoweixindialogfragment = ShareToWeixinDialogFragment.getInstance(mAlbumModel);
                    FragmentTransaction fragmenttransaction = getFragmentManager().beginTransaction();
                    fragmenttransaction.add(sharetoweixindialogfragment, "dialog");
                    fragmenttransaction.commitAllowingStateLoss();
                    return;
                }
            }
        }

        _cls4()
        {
            this$0 = AlbumSectionDownloadFragment.this;
            super();
        }
    }


    private class _cls5
        implements MyCallback
    {

        final AlbumSectionDownloadFragment this$0;
        final AlbumSectionModel val$data;

        public void execute()
        {
            if (data != null && data.getTracks() != null && data.getTracks().getList() != null && data.getTracks().getList().size() > 0)
            {
                showContentLayout(true);
                if (mSectionAdapter == null)
                {
                    mSectionAdapter = new SectionAdapter(getActivity(), data.getTracks().getTotalCount(), data.getTracks().getPageSize());
                    mSectionGridView.setAdapter(mSectionAdapter);
                    int i = mSectionAdapter.getCount();
                    int j = (getResources().getDisplayMetrics().widthPixels - Utilities.dip2px(getActivity(), 20F)) / Utilities.dip2px(getActivity(), 80F);
                    TextView textview;
                    AlbumSectionDownloadFragment albumsectiondownloadfragment;
                    StringBuilder stringbuilder;
                    if (i % j == 0)
                    {
                        i /= j;
                    } else
                    {
                        i = i / j + 1;
                    }
                    i *= Utilities.dip2px(getActivity(), 48F);
                    if (mPullDownContainer.getHeight() - Utilities.dip2px(getActivity(), 200F) > i)
                    {
                        mPullDownContainer.setPullDownViewMarginBottom(mPullDownContainer.getHeight() - i - Utilities.dip2px(getActivity(), 20F));
                    } else
                    {
                        mPullDownContainer.setPullDownViewMarginBottom(Utilities.dip2px(getActivity(), 180F));
                    }
                } else
                {
                    mSectionAdapter.reset(data.getTracks().getTotalCount(), data.getTracks().getPageSize());
                }
                if (mSectionContentAdapter == null)
                {
                    mContentListView.setAdapter(getContentAdapter(data));
                } else
                {
                    mSectionContentAdapter.reset(data);
                }
                mTotalTrackCount.setText(getString(0x7f0901f3, new Object[] {
                    Integer.valueOf(data.getTracks().getTotalCount())
                }));
                j = data.getTracks().getPageSize() * mCurrPage + data.getTracks().getPageSize();
                textview = mCurrSection;
                albumsectiondownloadfragment = AlbumSectionDownloadFragment.this;
                stringbuilder = (new StringBuilder()).append(data.getTracks().getPageSize() * mCurrPage + 1).append("~");
                i = j;
                if (j > data.getTracks().getTotalCount())
                {
                    i = data.getTracks().getTotalCount();
                }
                textview.setText(albumsectiondownloadfragment.getString(0x7f0901f4, new Object[] {
                    stringbuilder.append(i).toString()
                }));
            } else
            {
                showContentLayout(false);
                showNoNetworkLayout(true);
            }
            mLoadingData = false;
            mCheckAllCheckBox.setChecked(false);
            mDownloadBtn.setEnabled(false);
            updateBottomBarInfo();
            changeEnvWithCheckStatus();
            dismissDialog();
            if (mPullDownContainer != null && mPullDownContainer.isShowing())
            {
                mPullDownContainer.closePullDownPanel();
            }
        }

        _cls5()
        {
            this$0 = AlbumSectionDownloadFragment.this;
            data = albumsectionmodel;
            super();
        }
    }


    private class _cls6
        implements Runnable
    {

        final AlbumSectionDownloadFragment this$0;
        final int val$count;

        public void run()
        {
            updateDownloadingCount(count);
        }

        _cls6()
        {
            this$0 = AlbumSectionDownloadFragment.this;
            count = i;
            super();
        }
    }

}
