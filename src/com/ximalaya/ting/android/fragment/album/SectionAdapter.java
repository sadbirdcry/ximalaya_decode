// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;

public class SectionAdapter extends BaseAdapter
{

    private Context mContext;
    private ArrayList mData;
    private int mPageSize;
    private int mSelectedItem;
    private int mTotalCount;

    public SectionAdapter(Context context, int i, int j)
    {
        mContext = context;
        initData(i, j);
    }

    private void initData(int i, int j)
    {
        mTotalCount = i;
        mPageSize = j;
        if (mPageSize > 0);
        if (mTotalCount % mPageSize != 0)
        {
            i = mTotalCount / mPageSize + 1;
        } else
        {
            i = mTotalCount / mPageSize;
        }
        if (i > 0)
        {
            mData = new ArrayList(i);
            int k = 0;
            while (k < i) 
            {
                ArrayList arraylist = mData;
                StringBuilder stringbuilder = (new StringBuilder()).append(String.valueOf(mPageSize * k + 1)).append("~");
                int l;
                if (mPageSize * k + j > mTotalCount)
                {
                    l = mTotalCount;
                } else
                {
                    l = mPageSize * k + j;
                }
                arraylist.add(stringbuilder.append(String.valueOf(l)).toString());
                k++;
            }
        }
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (view == null)
        {
            view = new TextView(mContext);
            view.setGravity(17);
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(Utilities.dip2px(mContext, 70F), Utilities.dip2px(mContext, 28F)));
        } else
        {
            view = (TextView)view;
        }
        if (i == mSelectedItem)
        {
            view.setTextColor(Color.parseColor("#ffffff"));
            view.setBackgroundColor(Color.parseColor("#f86442"));
        } else
        {
            view.setTextColor(Color.parseColor("#707788"));
            view.setBackgroundColor(Color.parseColor("#f5f8fa"));
        }
        view.setText((CharSequence)mData.get(i));
        return view;
    }

    public void reset(int i, int j)
    {
        initData(i, j);
        notifyDataSetChanged();
    }

    public void selectItemAt(int i)
    {
        mSelectedItem = i;
        notifyDataSetChanged();
    }
}
