// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.SoundsAlbumAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.sound.AlbumSoundModel;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.album:
//            AlbumFragment

class val.trackId extends MyAsyncTask
{

    final AlbumFragment this$0;
    final long val$trackId;
    final View val$view;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient List doInBackground(Void avoid[])
    {
        Object obj;
        obj = new RequestParams();
        JSONObject jsonobject;
        if (AlbumFragment.access$1500(AlbumFragment.this))
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/my/album/track").toString();
            ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append(AlbumFragment.access$000(AlbumFragment.this).albumId).append("").toString());
            ((RequestParams) (obj)).put("albumUid", (new StringBuilder()).append(AlbumFragment.access$000(AlbumFragment.this).uid).append("").toString());
            ((RequestParams) (obj)).put("pageId", (new StringBuilder()).append(AlbumFragment.access$1600(AlbumFragment.this).pageId).append("").toString());
            ((RequestParams) (obj)).put("pageSize", (new StringBuilder()).append(AlbumFragment.access$1600(AlbumFragment.this).pageSize).append("").toString());
            ((RequestParams) (obj)).put("isAsc", (new StringBuilder()).append(AlbumFragment.access$1700(AlbumFragment.this)).append("").toString());
        } else
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
            avoid = (new StringBuilder()).append(avoid).append(AlbumFragment.access$000(AlbumFragment.this).albumId).append("/").append(AlbumFragment.access$1700(AlbumFragment.this)).append("/").append(AlbumFragment.access$1600(AlbumFragment.this).pageId).append("/").append(AlbumFragment.access$1600(AlbumFragment.this).pageSize).toString();
            ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append(AlbumFragment.access$000(AlbumFragment.this).albumId).append("").toString());
            ((RequestParams) (obj)).put("isAsc", (new StringBuilder()).append(AlbumFragment.access$1700(AlbumFragment.this)).append("").toString());
            ((RequestParams) (obj)).put("pageSize", (new StringBuilder()).append(AlbumFragment.access$1600(AlbumFragment.this).pageSize).append("").toString());
        }
        avoid = f.a().a(avoid, ((RequestParams) (obj)), val$view, fragmentBaseContainerView);
        Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
        avoid = JSON.parseObject(avoid);
        if (!"0".equals(avoid.get("ret").toString())) goto _L2; else goto _L1
_L1:
        obj = (AlbumModel)JSON.parseObject(avoid.getString("album"), com/ximalaya/ting/android/model/album/AlbumModel);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_299;
        }
        AlbumFragment.access$002(AlbumFragment.this, ((AlbumModel) (obj)));
        jsonobject = avoid.getJSONObject("tracks");
        avoid = JSON.parseArray(jsonobject.getString("list"), com/ximalaya/ting/android/model/sound/AlbumSoundModel);
        if (avoid != null)
        {
            break MISSING_BLOCK_LABEL_332;
        }
        obj = new ArrayList();
        avoid = ((Void []) (obj));
        obj = avoid;
        if (AlbumFragment.access$1600(AlbumFragment.this).totalCount != 0) goto _L4; else goto _L3
_L3:
        AlbumFragment.access$1600(AlbumFragment.this).totalCount = jsonobject.getIntValue("totalCount");
        obj = avoid;
          goto _L4
        obj;
        avoid = null;
_L5:
        Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
        obj = avoid;
_L4:
        if (AlbumFragment.access$1500(AlbumFragment.this) || obj == null || ((List) (obj)).size() == 0 || UserInfoMannage.hasLogined() || AlbumFragment.access$000(AlbumFragment.this) == null)
        {
            return ((List) (obj));
        }
        avoid = AlbumFragment.access$000(AlbumFragment.this);
        boolean flag;
        if (AlbumModelManage.getInstance().isHadCollected(AlbumFragment.access$000(AlbumFragment.this).albumId) != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        avoid.isFavorite = flag;
        return ((List) (obj));
        obj;
          goto _L5
        obj;
          goto _L5
_L2:
        obj = null;
        if (true) goto _L4; else goto _L6
_L6:
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((List)obj);
    }

    protected void onPostExecute(final List result)
    {
        if (!isAdded())
        {
            return;
        }
        AlbumFragment.access$1900(AlbumFragment.this).setClickable(true);
        AlbumFragment.access$2000(AlbumFragment.this).setClickable(true);
        AlbumFragment.access$1600(AlbumFragment.this).loadingNextPage = false;
        if (result == null)
        {
            showToast("\u65E0\u7F51\u7EDC\u6570\u636E");
            showFooterView(com.ximalaya.ting.android.fragment.FooterView.NO_DATA);
            return;
        }
        if (AlbumFragment.access$2100(AlbumFragment.this) != null)
        {
            AlbumFragment.access$2100(AlbumFragment.this).notifyDataSetChanged();
        }
        if (AlbumFragment.access$1600(AlbumFragment.this).pageId == 1)
        {
            class _cls1
                implements Runnable
            {

                final AlbumFragment._cls15 this$1;
                final List val$result;

                public void run()
                {
                    if (isAdded())
                    {
                        AlbumFragment.access$1100(this$0).clear();
                        AlbumFragment.access$1100(this$0).addAll(result);
                        AlbumFragment.access$100(this$0).notifyDataSetChanged();
                        AlbumFragment.access$2200(this$0);
                        AlbumFragment.access$2300(this$0);
                        Object obj = AlbumFragment.access$1600(this$0);
                        obj.pageId = ((AlbumFragment.LoadingData) (obj)).pageId + 1;
                        HashMap hashmap = new HashMap();
                        if (AlbumFragment.access$1500(this$0))
                        {
                            obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/my/album/track/").toString();
                            hashmap.put("albumId", (new StringBuilder()).append(AlbumFragment.access$000(this$0).albumId).append("").toString());
                            hashmap.put("albumUid", (new StringBuilder()).append(AlbumFragment.access$000(this$0).uid).append("").toString());
                            hashmap.put("pageId", (new StringBuilder()).append(AlbumFragment.access$1600(this$0).pageId).append("").toString());
                            hashmap.put("pageSize", (new StringBuilder()).append(AlbumFragment.access$1600(this$0).pageSize).append("").toString());
                            hashmap.put("isAsc", (new StringBuilder()).append(AlbumFragment.access$1700(this$0)).append("").toString());
                        } else
                        {
                            obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/others/ca/album/track/").toString();
                            hashmap.put("albumId", (new StringBuilder()).append(AlbumFragment.access$000(this$0).albumId).append("").toString());
                            hashmap.put("isAsc", (new StringBuilder()).append(AlbumFragment.access$1700(this$0)).append("").toString());
                            hashmap.put("pageSize", (new StringBuilder()).append(AlbumFragment.access$1600(this$0).pageSize).append("").toString());
                        }
                        AlbumFragment.access$100(this$0).setDataSource(((String) (obj)));
                        AlbumFragment.access$100(this$0).setPageId(AlbumFragment.access$1600(this$0).pageId);
                        AlbumFragment.access$100(this$0).setParams(hashmap);
                        doPlay(trackId);
                    }
                }

            _cls1()
            {
                this$1 = AlbumFragment._cls15.this;
                result = list;
                super();
            }
            }

            result = new _cls1();
            long l = getAnimationLeftTime();
            if (l > 0L)
            {
                mListView.postDelayed(result, l);
            } else
            {
                result.run();
            }
            AlbumFragment.access$2400(AlbumFragment.this);
        } else
        {
            if (AlbumFragment.access$1600(AlbumFragment.this).clear)
            {
                AlbumFragment.access$1100(AlbumFragment.this).clear();
                AlbumFragment.access$1600(AlbumFragment.this).clear = false;
            }
            AlbumFragment.access$1100(AlbumFragment.this).addAll(result);
            AlbumFragment.access$100(AlbumFragment.this).notifyDataSetChanged();
            AlbumFragment.access$2300(AlbumFragment.this);
            result = AlbumFragment.access$1600(AlbumFragment.this);
            result.pageId = ((dingData) (result)).pageId + 1;
        }
        AlbumFragment.access$600(AlbumFragment.this, AlbumFragment.access$000(AlbumFragment.this).isFavorite);
    }

    protected void onPreExecute()
    {
        AlbumFragment.access$1600(AlbumFragment.this).loadingNextPage = true;
        AlbumFragment.access$1900(AlbumFragment.this).setClickable(false);
        AlbumFragment.access$2000(AlbumFragment.this).setClickable(false);
    }

    _cls1()
    {
        this$0 = final_albumfragment;
        val$view = view1;
        val$trackId = J.this;
        super();
    }
}
