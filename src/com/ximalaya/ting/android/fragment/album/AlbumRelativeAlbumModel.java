// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.album;

import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.model.Collectable;
import com.ximalaya.ting.android.model.album.IAlbum;
import java.util.List;

public class AlbumRelativeAlbumModel
    implements Collectable, IAlbum
{

    private long albumId;
    private String coverSmall;
    private String intro;
    private boolean isCollected;
    private long playsCounts;
    private String recSrc;
    private String recTrack;
    private String tags;
    private String title;
    private int tracks;
    private long updatedAt;

    public AlbumRelativeAlbumModel()
    {
    }

    public static List getListFromJson(String s)
    {
        return JSON.parseArray(s, com/ximalaya/ting/android/fragment/album/AlbumRelativeAlbumModel);
    }

    public long getAlbumId()
    {
        return albumId;
    }

    public long getCId()
    {
        return albumId;
    }

    public String getCoverSmall()
    {
        return coverSmall;
    }

    public String getIAlbumCoverUrl()
    {
        return coverSmall;
    }

    public long getIAlbumId()
    {
        return albumId;
    }

    public long getIAlbumLastUpdateAt()
    {
        return updatedAt;
    }

    public long getIAlbumPlayCount()
    {
        return playsCounts;
    }

    public String getIAlbumTag()
    {
        return tags;
    }

    public String getIAlbumTitle()
    {
        return title;
    }

    public long getIAlbumTrackCount()
    {
        return (long)tracks;
    }

    public String getIIntro()
    {
        return getIntro();
    }

    public String getIntro()
    {
        return intro;
    }

    public long getPlaysCounts()
    {
        return playsCounts;
    }

    public String getRecSrc()
    {
        return recSrc;
    }

    public String getRecTrack()
    {
        return recTrack;
    }

    public String getTags()
    {
        return tags;
    }

    public String getTitle()
    {
        return title;
    }

    public int getTracks()
    {
        return tracks;
    }

    public long getUpdatedAt()
    {
        return updatedAt;
    }

    public boolean isCCollected()
    {
        return isCollected;
    }

    public boolean isIAlbumCollect()
    {
        return isCollected;
    }

    public void setAlbumId(long l)
    {
        albumId = l;
    }

    public void setCCollected(boolean flag)
    {
        isCollected = flag;
    }

    public void setCoverSmall(String s)
    {
        coverSmall = s;
    }

    public void setIAlbumCollect(boolean flag)
    {
        isCollected = flag;
    }

    public void setIntro(String s)
    {
        intro = s;
    }

    public void setPlaysCounts(long l)
    {
        playsCounts = l;
    }

    public void setRecSrc(String s)
    {
        recSrc = s;
    }

    public void setRecTrack(String s)
    {
        recTrack = s;
    }

    public void setTags(String s)
    {
        tags = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTracks(int i)
    {
        tracks = i;
    }

    public void setUpdatedAt(long l)
    {
        updatedAt = l;
    }
}
