// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.sina.weibo.sdk.auth.WeiboAuth;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.Tencent;
import com.xiaomi.account.openauth.k;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.login.AuthorizeActivity;
import com.ximalaya.ting.android.activity.setting.SettingActivity;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.album.LocalCollectAlbumUploader;
import com.ximalaya.ting.android.model.auth.AuthInfo;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseActivityLikeFragment

public class BaseLoginFragment extends BaseActivityLikeFragment
{
    public static class LoginTask extends MyAsyncTask
    {

        AuthInfo authInfo;
        private Bundle bundle;
        int loginFlag;
        private Activity mActivity;
        private boolean mFinishActivity;
        String name;
        String passwd;
        ProgressDialog pd;

        protected transient LoginInfoModel doInBackground(Object aobj[])
        {
            int i = ((Integer)aobj[0]).intValue();
            if (i == 0)
            {
                name = (String)aobj[1];
                passwd = (String)aobj[2];
                return CommonRequest.doLogin(mActivity, i, name, passwd, null);
            } else
            {
                authInfo = (AuthInfo)aobj[1];
                return CommonRequest.doLogin(mActivity, i, null, null, authInfo);
            }
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(LoginInfoModel logininfomodel)
        {
            if (mActivity != null && !mActivity.isFinishing())
            {
                pd.cancel();
                if (logininfomodel != null)
                {
                    if (logininfomodel.ret == 0)
                    {
                        UserInfoMannage.getInstance().setUser(logininfomodel);
                        if (logininfomodel.isFirst)
                        {
                            (new LocalCollectAlbumUploader(mActivity.getApplicationContext())).myexec(new Void[0]);
                        }
                        ((MyApplication)mActivity.getApplication()).e = 1;
                        Object obj = ScoreManage.getInstance(mActivity.getApplicationContext());
                        if (obj != null)
                        {
                            ((ScoreManage) (obj)).initBehaviorScore();
                            ((ScoreManage) (obj)).updateScore();
                        }
                        Intent intent = new Intent(mActivity, com/ximalaya/ting/android/activity/MainTabActivity2);
                        obj = null;
                        boolean flag;
                        if (bundle != null)
                        {
                            if (bundle.containsKey("ExtraUrl"))
                            {
                                obj = bundle.getString("ExtraUrl");
                            }
                            flag = bundle.getBoolean("login_from_setting", false);
                        } else
                        {
                            obj = null;
                            flag = false;
                        }
                        if (logininfomodel.isFirst)
                        {
                            if (logininfomodel.loginFromId == 1)
                            {
                                (new ToDoFollowed(mActivity)).myexec(new String[] {
                                    authInfo.access_token
                                });
                            }
                        } else
                        if (flag)
                        {
                            intent.setClass(mActivity, com/ximalaya/ting/android/activity/setting/SettingActivity);
                        } else
                        {
                            if (!TextUtils.isEmpty(((CharSequence) (obj))))
                            {
                                mActivity.finish();
                                return;
                            }
                            intent.putExtra("isLogin", true);
                        }
                        mActivity.startActivity(intent);
                    } else
                    {
                        Toast.makeText(mActivity, (new StringBuilder()).append("\u767B\u5F55\u5931\u8D25\uFF0C").append(logininfomodel.msg).toString(), 1).show();
                    }
                } else
                {
                    Toast.makeText(mActivity, "\u7F51\u7EDC\u8D85\u65F6\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01", 1).show();
                }
                if (mFinishActivity && mActivity != null)
                {
                    mActivity.finish();
                    return;
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((LoginInfoModel)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            if (mActivity == null || mActivity.isFinishing())
            {
                return;
            }
            if (pd == null)
            {
                pd = new MyProgressDialog(mActivity);
            } else
            {
                pd.cancel();
            }
            pd.setTitle("\u767B\u5F55");
            pd.setMessage("\u6B63\u5728\u767B\u5F55\uFF0C\u8BF7\u7A0D\u540E...");
            pd.show();
        }

        public LoginTask(Activity activity, Bundle bundle1)
        {
            this(activity, bundle1, false);
        }

        public LoginTask(Activity activity, Bundle bundle1, boolean flag)
        {
            mFinishActivity = false;
            pd = null;
            mActivity = activity;
            bundle = bundle1;
            mFinishActivity = flag;
        }
    }

    public static class ToDoFollowed extends MyAsyncTask
    {

        private Activity mActivity;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected transient Void doInBackground(String as[])
        {
            CommonRequest.DoFollowedAutor(mActivity, as[0]);
            return null;
        }

        public ToDoFollowed(Activity activity)
        {
            mActivity = activity;
        }
    }


    private static final String TAG = com/ximalaya/ting/android/fragment/BaseLoginFragment.getSimpleName();
    private Tencent mTencent;
    private SsoHandler mWeiboHandler;

    public BaseLoginFragment()
    {
    }

    public static Tencent getTecent(Context context)
    {
        return Tencent.createInstance(b.c, context);
    }

    protected void QQLogin()
    {
        if (a.e)
        {
            Intent intent = new Intent(mCon, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
            intent.putExtra("lgflag", 2);
            startActivity(intent);
            return;
        }
        mTencent = Tencent.createInstance(b.c, mActivity);
        if (!mTencent.isSessionValid())
        {
            mTencent.login(mActivity, "get_simple_userinfo,get_user_info,add_share,get_fanslist,check_page_fans,add_t,add_pic_t", new _cls1());
            return;
        } else
        {
            mTencent.logout(mActivity);
            return;
        }
    }

    protected void loginWithWX()
    {
        IWXAPI iwxapi = WXAPIFactory.createWXAPI(mActivity, b.b, true);
        if (!iwxapi.isWXAppInstalled())
        {
            showToast("\u8BF7\u5B89\u88C5\u5FAE\u4FE1");
            return;
        }
        if (!iwxapi.isWXAppSupportAPI())
        {
            showToast("\u5FAE\u4FE1\u7248\u672C\u8FC7\u4F4E");
            return;
        } else
        {
            ((MyApplication)(MyApplication)mActivity.getApplication()).b = 5;
            iwxapi.registerApp(b.b);
            com.tencent.mm.sdk.modelmsg.SendAuth.Req req = new com.tencent.mm.sdk.modelmsg.SendAuth.Req();
            req.scope = "snsapi_userinfo";
            req.state = "ximalaya_ting";
            iwxapi.sendReq(req);
            return;
        }
    }

    protected void loginWithXiaomi()
    {
        (new _cls3()).myexec(new Void[0]);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        super.onActivityResult(i, j, intent);
        if (mWeiboHandler != null)
        {
            mWeiboHandler.authorizeCallBack(i, j, intent);
        }
        if (mTencent != null)
        {
            mTencent.onActivityResult(i, j, intent);
        }
    }

    public void onAttach(Activity activity)
    {
        super.onAttach(activity);
    }

    public void onDetach()
    {
        super.onDetach();
        mWeiboHandler = null;
        mTencent = null;
        mActivity = null;
    }

    protected void sinaLogin()
    {
        Intent intent;
        try
        {
            WeiboAuth weiboauth = new WeiboAuth(mActivity, b.a, "http://www.ximalaya.com", "email,direct_messages_read,direct_messages_write,friendships_groups_read,friendships_groups_write,statuses_to_me_read,follow_app_official_microblog,invitation_write");
            mWeiboHandler = new SsoHandler(mActivity, weiboauth);
            mWeiboHandler.authorize(new _cls2());
            return;
        }
        catch (Exception exception) { }
        finally
        {
            throw exception1;
        }
        intent = new Intent(mActivity, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
        intent.putExtra("lgflag", 1);
        intent.setFlags(0x20000000);
        mActivity.startActivity(intent);
        return;
    }

    protected void ximalayaLogin(String s, String s1)
    {
        (new LoginTask(mActivity, getArguments())).myexec(new Object[] {
            Integer.valueOf(0), s, s1
        });
    }



    private class _cls1
        implements IUiListener
    {

        final BaseLoginFragment this$0;

        public void onCancel()
        {
        }

        public void onComplete(Object obj)
        {
            obj = (JSONObject)obj;
            if (obj == null)
            {
                showToast("\u83B7\u53D6QQ\u4FE1\u606F\u5931\u8D25\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01");
                return;
            }
            AuthInfo authinfo;
            authinfo = new AuthInfo();
            if (!((JSONObject) (obj)).has("ret"))
            {
                break MISSING_BLOCK_LABEL_141;
            }
            if (((JSONObject) (obj)).getInt("ret") == 0)
            {
                authinfo.access_token = ((JSONObject) (obj)).getString("access_token");
                authinfo.expires_in = ((JSONObject) (obj)).getString("expires_in");
                authinfo.openid = ((JSONObject) (obj)).getString("openid");
                (new LoginTask(mActivity, getArguments())).myexec(new Object[] {
                    Integer.valueOf(2), authinfo
                });
                return;
            }
            try
            {
                showToast(((JSONObject) (obj)).getString("msg"));
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                showToast("\u89E3\u6790QQ\u4FE1\u606F\u5931\u8D25\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01");
                return;
            }
            authinfo.access_token = ((JSONObject) (obj)).getString("access_token");
            authinfo.expires_in = ((JSONObject) (obj)).getString("expires_in");
            authinfo.openid = ((JSONObject) (obj)).getString("openid");
            (new LoginTask(mActivity, getArguments())).myexec(new Object[] {
                Integer.valueOf(2), authinfo
            });
            return;
        }

        public void onError(UiError uierror)
        {
            Logger.e(BaseLoginFragment.TAG, uierror.errorMessage);
        }

        _cls1()
        {
            this$0 = BaseLoginFragment.this;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final BaseLoginFragment this$0;
        final f val$future;

        protected transient AuthInfo doInBackground(Void avoid[])
        {
            avoid = (i)future.a();
            if (avoid == null)
            {
                break MISSING_BLOCK_LABEL_142;
            }
            String s;
            boolean flag;
            s = (String)(new k()).a(getActivity(), 0x2800000000003757L, "/user/openidV2", avoid.a(), avoid.c(), avoid.d()).a();
            flag = TextUtils.isEmpty(s);
            if (flag)
            {
                break MISSING_BLOCK_LABEL_142;
            }
            AuthInfo authinfo;
            s = com.alibaba.fastjson.JSONObject.parseObject(s).getString("data");
            if (TextUtils.isEmpty(s))
            {
                break MISSING_BLOCK_LABEL_142;
            }
            s = com.alibaba.fastjson.JSONObject.parseObject(s).getString("openId");
            authinfo = new AuthInfo();
            authinfo.access_token = avoid.a();
            authinfo.expires_in = avoid.b();
            authinfo.openid = s;
            authinfo.macKey = avoid.c();
            return authinfo;
            avoid;
            try
            {
                avoid.printStackTrace();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
            }
            return null;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(AuthInfo authinfo)
        {
            super.onPostExecute(authinfo);
            if (authinfo != null)
            {
                (new LoginTask(mActivity, getArguments(), true)).myexec(new Object[] {
                    Integer.valueOf(5), authinfo
                });
                return;
            } else
            {
                showToast("\u5C0F\u7C73\u8D26\u6237\u767B\u5F55\u5931\u8D25");
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((AuthInfo)obj);
        }

        _cls3()
        {
            this$0 = BaseLoginFragment.this;
            future = f1;
            super();
        }
    }


    private class _cls2
        implements WeiboAuthListener
    {

        final BaseLoginFragment this$0;

        public void onCancel()
        {
        }

        public void onComplete(Bundle bundle)
        {
            if (Oauth2AccessToken.parseAccessToken(bundle).isSessionValid())
            {
                AuthInfo authinfo = new AuthInfo();
                authinfo.access_token = bundle.getString("access_token");
                authinfo.expires_in = bundle.getString("expires_in");
                authinfo.openid = bundle.getString("uid");
                (new LoginTask(mActivity, getArguments())).myexec(new Object[] {
                    Integer.valueOf(1), authinfo
                });
                return;
            } else
            {
                bundle = bundle.getString("code", "");
                showToast((new StringBuilder()).append("Weibo error code:").append(bundle).toString());
                return;
            }
        }

        public void onWeiboException(WeiboException weiboexception)
        {
            showToast((new StringBuilder()).append("weibo\u5F02\u5E38:").append(weiboexception.getMessage()).toString());
        }

        _cls2()
        {
            this$0 = BaseLoginFragment.this;
            super();
        }
    }

}
