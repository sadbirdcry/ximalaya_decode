// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.subject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.homepage.TalkViewAct;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.adapter.SubjectDetailAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.model.subject.SubjectModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.modelnew.AlbumModelNew;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SubjectFragment extends BaseListFragment
{
    class PlayerServiceListener
        implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
    {

        final SubjectFragment this$0;

        public void onPlayCanceled()
        {
            if (mContentType == 2)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        public void onSoundChanged(int i)
        {
            if (mContentType == 2)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        public void onSoundInfoChanged(int i, SoundInfo soundinfo)
        {
            if (mContentType == 2)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        PlayerServiceListener()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }

    class PlayerStatusListener
        implements com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener
    {

        final SubjectFragment this$0;

        public void onBufferUpdated(int i)
        {
        }

        public void onLogoPlayFinished()
        {
        }

        public void onPlayCompleted()
        {
            if (mContentType == 2 && mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        public void onPlayPaused()
        {
            if (mContentType == 2 && mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        public void onPlayProgressUpdate(int i, int j)
        {
        }

        public void onPlayStarted()
        {
            if (mContentType == 2 && mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
            }
        }

        public void onPlayerBuffering(boolean flag)
        {
        }

        public void onSoundPrepared(int i)
        {
        }

        public void onStartPlayLogo()
        {
        }

        PlayerStatusListener()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }


    private SubjectDetailAdapter mAdapter;
    private int mContentType;
    private ImageView mEditorIcon;
    private ImageView mEmptyView;
    private View mFooter;
    private View mHeader;
    private boolean mIsLoading;
    private View mMessageToOwner;
    private ImageView mOwnerIcon;
    private TextView mOwnerName;
    private TextView mPersonalSignature;
    private View mSendMessageLayout;
    private PlayerServiceListener mServiceListener;
    private ImageView mShareBtn;
    private List mSoundInfos;
    private PlayerStatusListener mStatusListener;
    private SubjectModel mSubject;
    private TextView mSubjectEditor;
    private long mSubjectId;
    private TextView mSubjectIntro;
    private ArrayList mSubjectItems;
    private TextView mSubjectTitle;
    private String mXDCS_DATA;
    private String mtitle;
    private String trsPosition;

    public SubjectFragment()
    {
        mSubjectItems = new ArrayList();
        mSoundInfos = null;
        mIsLoading = false;
    }

    private String getPageUrl()
    {
        return (new StringBuilder()).append(a.u).append("m/subject_detail").append("?id=").append(mSubjectId).toString();
    }

    private void initDate()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            mSubjectId = bundle.getLong("subjectId");
            mContentType = bundle.getInt("contentType");
            trsPosition = bundle.getString("position");
            mtitle = bundle.getString("title");
        }
    }

    private void initView()
    {
        mEmptyView = (ImageView)findViewById(0x7f0a0060);
        mHeader = View.inflate(mActivity, 0x7f0301d5, null);
        mSubjectTitle = (TextView)mHeader.findViewById(0x7f0a0293);
        mSubjectIntro = (TextView)mHeader.findViewById(0x7f0a06e8);
        mSubjectEditor = (TextView)mHeader.findViewById(0x7f0a06eb);
        mEditorIcon = (ImageView)mHeader.findViewById(0x7f0a06ea);
        mHeader.findViewById(0x7f0a0444).setVisibility(8);
        ((TextView)mHeader.findViewById(0x7f0a0158)).setText("\u542C\u5355\u5217\u8868");
        mHeader.setVisibility(8);
        mListView.addHeaderView(mHeader);
        mFooter = View.inflate(mActivity, 0x7f0301d4, null);
        mFooter.findViewById(0x7f0a0444).setVisibility(8);
        ((TextView)mFooter.findViewById(0x7f0a0158)).setText("\u7ED9\u5C0F\u7F16\u7559\u8A00");
        mOwnerIcon = (ImageView)mFooter.findViewById(0x7f0a0623);
        mOwnerName = (TextView)mFooter.findViewById(0x7f0a0624);
        mSendMessageLayout = mFooter.findViewById(0x7f0a06e2);
        mMessageToOwner = mFooter.findViewById(0x7f0a06e3);
        mPersonalSignature = (TextView)mFooter.findViewById(0x7f0a06e4);
        mFooter.setVisibility(8);
        mListView.addFooterView(mFooter);
        View view = new View(mActivity);
        view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ToolUtil.dp2px(mActivity, 60F)));
        mListView.addFooterView(view);
        mAdapter = new SubjectDetailAdapter(mActivity, mListView, mContentType, mSubjectItems);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new _cls1());
        mListView.setOnItemLongClickListener(new _cls2());
        mSendMessageLayout.setOnClickListener(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
        mShareBtn = (ImageView)findViewById(0x7f0a0710);
        mShareBtn.setImageResource(0x7f020537);
        mShareBtn.setVisibility(0);
        mShareBtn.setOnClickListener(new _cls5());
    }

    private void loadData()
    {
        if (mIsLoading)
        {
            return;
        } else
        {
            RequestParams requestparams = new RequestParams();
            requestparams.add("id", (new StringBuilder()).append("").append(mSubjectId).toString());
            requestparams.add("title", mtitle);
            requestparams.add("position", trsPosition);
            f.a().a("m/subject_detail", requestparams, null, new _cls6());
            return;
        }
    }

    private void loadLikeStatus()
    {
        RequestParams requestparams = new RequestParams();
        StringBuilder stringbuilder = new StringBuilder();
        if (mSubjectItems == null)
        {
            return;
        }
        Iterator iterator = mSubjectItems.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj = iterator.next();
            if (obj != null && (obj instanceof SoundInfoNew))
            {
                stringbuilder.append(((SoundInfoNew)obj).id).append(",");
            }
        } while (true);
        requestparams.put("trackIds", stringbuilder.toString());
        f.a().a("mobile/track/relation", requestparams, DataCollectUtil.getDataFromView(mListView), new _cls8());
    }

    private void loadRSSStatus()
    {
        RequestParams requestparams = new RequestParams();
        requestparams.add("uid", (new StringBuilder()).append("").append(UserInfoMannage.getInstance().getUser().uid).toString());
        StringBuilder stringbuilder = new StringBuilder();
        for (Iterator iterator = mSubjectItems.iterator(); iterator.hasNext(); stringbuilder.append(((AlbumModelNew)iterator.next()).id).append(",")) { }
        requestparams.add("album_ids", stringbuilder.toString());
        f.a().a("m/album_subscribe_status", requestparams, null, new _cls9());
    }

    private void loadRSSStatusFromLocal()
    {
        (new _cls7()).myexec(new Void[0]);
    }

    private void refreshNickName(List list)
    {
        if (mSubject != null)
        {
            list = list.iterator();
            while (list.hasNext()) 
            {
                Object obj = list.next();
                if (obj instanceof AlbumModelNew)
                {
                    ((AlbumModelNew)obj).nickname = mSubject.nickname;
                }
            }
        }
    }

    private void registePlayerListener()
    {
        if (mContentType == 2)
        {
            if (mServiceListener == null)
            {
                mServiceListener = new PlayerServiceListener();
            }
            if (mStatusListener == null)
            {
                mStatusListener = new PlayerStatusListener();
            }
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            if (localmediaservice != null)
            {
                localmediaservice.setOnPlayerStatusUpdateListener(mStatusListener);
                localmediaservice.setOnPlayServiceUpdateListener(mServiceListener);
            }
        }
    }

    private void toPrivateMsg()
    {
        if (mSubject == null || mActivity == null)
        {
            return;
        }
        Intent intent = new Intent();
        if (UserInfoMannage.hasLogined())
        {
            LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
            intent.setClass(mActivity, com/ximalaya/ting/android/activity/homepage/TalkViewAct);
            intent.putExtra("title", mSubject.nickname);
            intent.putExtra("toUid", mSubject.uid);
            intent.putExtra("specialId", mSubject.specialId);
            intent.putExtra("meHeadUrl", logininfomodel.smallLogo);
            intent.putExtra("subjectTitle", mSubject.title);
        } else
        {
            intent.setClass(mActivity, com/ximalaya/ting/android/activity/login/LoginActivity);
        }
        startActivity(intent);
    }

    private void unregistePlayerListener()
    {
        if (mContentType == 2)
        {
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            if (localmediaservice != null)
            {
                if (mServiceListener != null)
                {
                    localmediaservice.removeOnPlayServiceUpdateListener(mServiceListener);
                }
                if (mStatusListener != null)
                {
                    localmediaservice.removeOnPlayerUpdateListener(mStatusListener);
                }
            }
        }
    }

    private void updateSubjectHeader()
    {
        mHeader.setVisibility(0);
        mHeader.findViewById(0x7f0a0276).setVisibility(0);
        mFooter.setVisibility(0);
        mFooter.findViewById(0x7f0a0276).setVisibility(0);
        mSubjectTitle.setText(mSubject.title);
        mSubjectIntro.setText(mSubject.intro);
        mSubjectEditor.setText(mSubject.nickname);
        mEditorIcon.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mActivity).displayImage(mEditorIcon, mSubject.smallLogo, 0x7f0202df);
        mOwnerIcon.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mActivity).displayImage(mOwnerIcon, mSubject.smallLogo, 0x7f0202df);
        TextView textview = mOwnerName;
        String s;
        if (TextUtils.isEmpty(mSubject.nickname))
        {
            s = "";
        } else
        {
            s = (new StringBuilder()).append(mSubject.nickname).append("\uFF1A").toString();
        }
        textview.setText(s);
        textview = mPersonalSignature;
        if (TextUtils.isEmpty(mSubject.personalSignature))
        {
            s = "\u8FD9\u5BB6\u4F19\u5F88\u61D2\uFF0C\u4EC0\u4E48\u4E5F\u6CA1\u8BF4\u3002";
        } else
        {
            s = mSubject.personalSignature;
        }
        textview.setText(s);
    }

    public void onActivityCreated(Bundle bundle)
    {
        mListView = (ListView)findViewById(0x7f0a0129);
        super.onActivityCreated(bundle);
        initDate();
        initView();
        registePlayerListener();
        setTitleText("\u542C\u5355\u8BE6\u60C5");
        loadData();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300fe, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        unregistePlayerListener();
        super.onDestroyView();
    }




/*
    static boolean access$1002(SubjectFragment subjectfragment, boolean flag)
    {
        subjectfragment.mIsLoading = flag;
        return flag;
    }

*/











/*
    static SubjectModel access$402(SubjectFragment subjectfragment, SubjectModel subjectmodel)
    {
        subjectfragment.mSubject = subjectmodel;
        return subjectmodel;
    }

*/



/*
    static List access$502(SubjectFragment subjectfragment, List list)
    {
        subjectfragment.mSoundInfos = list;
        return list;
    }

*/





    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final SubjectFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i -= mListView.getHeaderViewsCount();
            if (i >= 0 && i < mSubjectItems.size()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            adapterview = ((AdapterView) (mSubjectItems.get(i)));
            if (!(adapterview instanceof AlbumModelNew)) goto _L4; else goto _L3
_L3:
            AlbumModelNew albummodelnew = (AlbumModelNew)adapterview;
            CommonRequestNew.subjectClickCount(mSubjectId, albummodelnew.id, view, "\u542C\u5355\u8BE6\u60C5", i + 1);
_L6:
            Bundle bundle = new Bundle();
            if (mSubject.contentType == 1)
            {
                adapterview = (AlbumModelNew)adapterview;
                AlbumModel albummodel = new AlbumModel();
                albummodel.albumId = ((AlbumModelNew) (adapterview)).id;
                bundle.putString("album", JSON.toJSONString(albummodel));
                bundle.putInt("from", 5);
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
                return;
            }
            break; /* Loop/switch isn't completed */
_L4:
            if (adapterview instanceof SoundInfoNew)
            {
                SoundInfoNew soundinfonew = (SoundInfoNew)adapterview;
                CommonRequestNew.subjectClickCount(mSubjectId, soundinfonew.id, view, "\u542C\u5355\u8BE6\u60C5", i + 1);
            }
            if (true) goto _L6; else goto _L5
_L5:
            if (mSubject.contentType != 2) goto _L1; else goto _L7
_L7:
            if (mSoundInfos == null || mSoundInfos.size() != mSubjectItems.size())
            {
                mSoundInfos = ModelHelper.toSoundInfo(Arrays.asList(mSubjectItems.toArray(new SoundInfoNew[0])));
            }
            PlayTools.gotoPlay(22, mSoundInfos, i, 
// JavaClassFileOutputException: get_constant: invalid tag

        _cls1()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.widget.AdapterView.OnItemLongClickListener
    {

        final SubjectFragment this$0;

        public boolean onItemLongClick(AdapterView adapterview, View view, int i, long l)
        {
label0:
            {
                if (mSubject.contentType == 2)
                {
                    i -= mListView.getHeaderViewsCount();
                    if (i >= 0 && i <= mAdapter.getCount())
                    {
                        break label0;
                    }
                }
                return false;
            }
            mAdapter.handleItemLongClick((SoundInfoNew)mAdapter.getData().get(i), view);
            return true;
        }

        _cls2()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final SubjectFragment this$0;

        public void onClick(View view)
        {
            toPrivateMsg();
        }

        _cls3()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final SubjectFragment this$0;

        public void onClick(View view)
        {
            loadData();
        }

        _cls4()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final SubjectFragment this$0;

        public void onClick(View view)
        {
            if (mSubject != null)
            {
                (new BaseShareDialog(getActivity(), mSubject, mShareBtn)).show();
                return;
            } else
            {
                showToast(getString(0x7f0901eb));
                return;
            }
        }

        _cls5()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }


    private class _cls6 extends com.ximalaya.ting.android.b.a
    {

        final SubjectFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, fragmentBaseContainerView);
        }

        public void onFinish()
        {
            super.onFinish();
            mIsLoading = false;
        }

        public void onNetError(int i, String s)
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_CONNECTION);
        }

        public void onStart()
        {
            super.onStart();
            mIsLoading = true;
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        }

        public void onSuccess(final String list)
        {
            if (!canGoon())
            {
                return;
            }
            if (TextUtils.isEmpty(list))
            {
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            try
            {
                list = JSON.parseObject(list);
            }
            // Misplaced declaration of an exception variable
            catch (final String list)
            {
                list.printStackTrace();
                list = null;
            }
            if (list == null || list.getIntValue("ret") != 0)
            {
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
                return;
            }
            String s = list.getString("info");
            if (!TextUtils.isEmpty(s))
            {
                mSubject = (SubjectModel)JSON.parseObject(s, com/ximalaya/ting/android/model/subject/SubjectModel);
                updateSubjectHeader();
            }
            s = list.getString("list");
            if (mSubject.contentType == 2)
            {
                list = com/ximalaya/ting/android/model/sound/SoundInfoNew;
            } else
            if (mSubject.contentType == 1)
            {
                list = com/ximalaya/ting/android/modelnew/AlbumModelNew;
            } else
            {
                list = java/lang/Object;
                Log.e("", "Xm wrong content type on parse json");
            }
            list = JSON.parseArray(s, list);
            if (list != null && list.size() > 0)
            {
                refreshNickName(list);
                class _cls1
                    implements Runnable
                {

                    final _cls6 this$1;
                    final List val$list;

                    public void run()
                    {
                        mHeader.findViewById(0x7f0a06ec).setVisibility(0);
                        mSubjectItems.clear();
                        mSubjectItems.addAll(list);
                        mAdapter.notifyDataSetChanged();
                        if (mContentType != 1) goto _L2; else goto _L1
_L1:
                        if (!UserInfoMannage.hasLogined()) goto _L4; else goto _L3
_L3:
                        loadRSSStatus();
_L6:
                        return;
_L4:
                        loadRSSStatusFromLocal();
                        return;
_L2:
                        if (UserInfoMannage.hasLogined() && mContentType == 2)
                        {
                            loadLikeStatus();
                            return;
                        }
                        if (true) goto _L6; else goto _L5
_L5:
                    }

                _cls1()
                {
                    this$1 = _cls6.this;
                    list = list1;
                    super();
                }
                }

                mListView.postDelayed(new _cls1(), getAnimationLeftTime());
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls6()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }


    private class _cls8 extends com.ximalaya.ting.android.b.a
    {

        final SubjectFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, mListView);
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            Object obj = null;
            s = JSON.parseObject(s);
_L12:
            if (s == null || s.getIntValue("ret") != 0) goto _L1; else goto _L3
_L3:
            s = s.getJSONArray("data");
            if (s == null || mSubjectItems == null) goto _L1; else goto _L4
_L4:
            int i = 0;
_L14:
            if (i >= s.size()) goto _L6; else goto _L5
_L5:
            int j = 0;
_L11:
            if (j >= mSubjectItems.size())
            {
                continue; /* Loop/switch isn't completed */
            }
            obj = mSubjectItems.get(j);
            if (obj == null || !(obj instanceof SoundInfoNew)) goto _L8; else goto _L7
_L7:
            obj = (SoundInfoNew)obj;
            JSONObject jsonobject = s.getJSONObject(i);
            if (jsonobject != null) goto _L9; else goto _L8
_L8:
            j++;
            if (true) goto _L11; else goto _L10
_L10:
            continue; /* Loop/switch isn't completed */
            s;
            s.printStackTrace();
            s = ((String) (obj));
              goto _L12
_L9:
            if (((SoundInfoNew) (obj)).id != (long)jsonobject.getIntValue("trackId")) goto _L8; else goto _L13
_L13:
            obj.isFavorite = jsonobject.getBooleanValue("isLike");
              goto _L8
            Exception exception;
            exception;
            exception.printStackTrace();
              goto _L8
            i++;
              goto _L14
_L6:
            mAdapter.notifyDataSetChanged();
            return;
              goto _L12
        }

        _cls8()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }


    private class _cls9 extends com.ximalaya.ting.android.b.a
    {

        final SubjectFragment this$0;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onNetError(int i, String s)
        {
        }

        public void onSuccess(String s)
        {
_L2:
            return;
            if (!canGoon() || TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            AlbumModelNew albummodelnew = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = albummodelnew;
            }
            if (s != null && s.getIntValue("ret") == 0)
            {
                s = s.getJSONObject("status");
                if (s != null)
                {
                    int i = 0;
                    while (i < mSubjectItems.size()) 
                    {
                        albummodelnew = (AlbumModelNew)mSubjectItems.get(i);
                        albummodelnew.isFavorite = "1".equals((new StringBuilder()).append("").append(s.get((new StringBuilder()).append("").append(albummodelnew.id).toString())).toString());
                        mAdapter.updateItem(i);
                        i++;
                    }
                }
            }
            if (true) goto _L2; else goto _L3
_L3:
        }

        _cls9()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }


    private class _cls7 extends MyAsyncTask
    {

        final SubjectFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            int i = 0;
            while (i < mSubjectItems.size()) 
            {
                avoid = (AlbumModelNew)mSubjectItems.get(i);
                boolean flag;
                if (AlbumModelManage.getInstance().isHadCollected(((AlbumModelNew) (avoid)).id) != null)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                avoid.isFavorite = flag;
                i++;
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            mAdapter.notifyDataSetChanged();
        }

        _cls7()
        {
            this$0 = SubjectFragment.this;
            super();
        }
    }

}
