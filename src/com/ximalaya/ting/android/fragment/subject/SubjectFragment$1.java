// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.subject;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.model.subject.SubjectModel;
import com.ximalaya.ting.android.modelnew.AlbumModelNew;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.CommonRequestNew;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.subject:
//            SubjectFragment

class this._cls0
    implements android.widget.ClickListener
{

    final SubjectFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        i -= mListView.getHeaderViewsCount();
        if (i >= 0 && i < SubjectFragment.access$200(SubjectFragment.this).size()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        adapterview = ((AdapterView) (SubjectFragment.access$200(SubjectFragment.this).get(i)));
        if (!(adapterview instanceof AlbumModelNew)) goto _L4; else goto _L3
_L3:
        AlbumModelNew albummodelnew = (AlbumModelNew)adapterview;
        CommonRequestNew.subjectClickCount(SubjectFragment.access$300(SubjectFragment.this), albummodelnew.id, view, "\u542C\u5355\u8BE6\u60C5", i + 1);
_L6:
        Bundle bundle = new Bundle();
        if (SubjectFragment.access$400(SubjectFragment.this).contentType == 1)
        {
            adapterview = (AlbumModelNew)adapterview;
            AlbumModel albummodel = new AlbumModel();
            albummodel.albumId = ((AlbumModelNew) (adapterview)).id;
            bundle.putString("album", JSON.toJSONString(albummodel));
            bundle.putInt("from", 5);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
            return;
        }
        break; /* Loop/switch isn't completed */
_L4:
        if (adapterview instanceof SoundInfoNew)
        {
            SoundInfoNew soundinfonew = (SoundInfoNew)adapterview;
            CommonRequestNew.subjectClickCount(SubjectFragment.access$300(SubjectFragment.this), soundinfonew.id, view, "\u542C\u5355\u8BE6\u60C5", i + 1);
        }
        if (true) goto _L6; else goto _L5
_L5:
        if (SubjectFragment.access$400(SubjectFragment.this).contentType != 2) goto _L1; else goto _L7
_L7:
        if (SubjectFragment.access$500(SubjectFragment.this) == null || SubjectFragment.access$500(SubjectFragment.this).size() != SubjectFragment.access$200(SubjectFragment.this).size())
        {
            SubjectFragment.access$502(SubjectFragment.this, ModelHelper.toSoundInfo(Arrays.asList(SubjectFragment.access$200(SubjectFragment.this).toArray(new SoundInfoNew[0]))));
        }
        PlayTools.gotoPlay(22, SubjectFragment.access$500(SubjectFragment.this), i, SubjectFragment.access$600(SubjectFragment.this), DataCollectUtil.getDataFromView(view));
        return;
    }

    ()
    {
        this$0 = SubjectFragment.this;
        super();
    }
}
