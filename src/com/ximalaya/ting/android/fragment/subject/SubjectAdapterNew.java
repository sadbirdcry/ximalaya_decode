// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.subject;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.ListItemTitleModel;
import com.ximalaya.ting.android.model.subject.SubjectModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

public class SubjectAdapterNew extends BaseAdapter
{
    protected static class ViewHolder
    {

        ImageView cover;
        TextView desc;
        TextView name;
        TextView subtitle;

        protected ViewHolder()
        {
        }
    }


    private static final int TYPE_CONTENT = 1;
    private static final int TYPE_TITLE = 0;
    private Context mContext;
    private List mData;
    private BaseFragment mFragment;
    private LayoutInflater mInflater;

    public SubjectAdapterNew(Context context, BaseFragment basefragment, List list)
    {
        mInflater = LayoutInflater.from(context);
        mData = list;
        mContext = context;
        mFragment = basefragment;
    }

    private View getContentView(int i, View view, ViewGroup viewgroup)
    {
        boolean flag2 = true;
        Object obj;
        SubjectModel subjectmodel;
        TextView textview;
        boolean flag;
        boolean flag1;
        if (view == null || view.getTag() == null)
        {
            view = mInflater.inflate(0x7f030140, viewgroup, false);
            viewgroup = new ViewHolder();
            viewgroup.cover = (ImageView)view.findViewById(0x7f0a0177);
            mFragment.markImageView(((ViewHolder) (viewgroup)).cover);
            viewgroup.name = (TextView)view.findViewById(0x7f0a0449);
            viewgroup.desc = (TextView)view.findViewById(0x7f0a044a);
            viewgroup.subtitle = (TextView)view.findViewById(0x7f0a01ae);
            view.setTag(viewgroup);
            view.setClickable(false);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        obj = mContext;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        flag1 = flag2;
        if (i + 1 != mData.size())
        {
            if (nextIsTitle(i))
            {
                flag1 = flag2;
            } else
            {
                flag1 = false;
            }
        }
        ViewUtil.buildAlbumItemSpace(((Context) (obj)), view, flag, flag1, 81);
        subjectmodel = (SubjectModel)mData.get(i);
        ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).cover, subjectmodel.coverPathSmall, 0x7f0202e0);
        textview = ((ViewHolder) (viewgroup)).name;
        if (subjectmodel.title == null)
        {
            obj = "";
        } else
        {
            obj = subjectmodel.title;
        }
        textview.setText(((CharSequence) (obj)));
        textview = ((ViewHolder) (viewgroup)).subtitle;
        if (subjectmodel.subtitle == null)
        {
            obj = "";
        } else
        {
            obj = subjectmodel.subtitle.replace("\r\n", " ");
        }
        textview.setText(((CharSequence) (obj)));
        if (TextUtils.isEmpty(subjectmodel.footnote))
        {
            ((ViewHolder) (viewgroup)).desc.setVisibility(8);
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).desc.setVisibility(0);
            ((ViewHolder) (viewgroup)).desc.setText(subjectmodel.footnote);
            return view;
        }
    }

    private View getTitleView(int i, View view, ViewGroup viewgroup)
    {
        view = mInflater.inflate(0x7f030106, viewgroup, false);
        view.findViewById(0x7f0a0444).setVisibility(8);
        viewgroup = (ListItemTitleModel)mData.get(i);
        ((TextView)view.findViewById(0x7f0a0158)).setText(viewgroup.getTitleText());
        if (i != 0)
        {
            view.findViewById(0x7f0a0276).setVisibility(0);
            return view;
        } else
        {
            view.findViewById(0x7f0a0276).setVisibility(8);
            return view;
        }
    }

    private boolean nextIsTitle(int i)
    {
        return i + 1 < mData.size() && (mData.get(i + 1) instanceof ListItemTitleModel);
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public Object getItem(int i)
    {
        if (mData == null)
        {
            return null;
        } else
        {
            return mData.get(i);
        }
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public int getItemViewType(int i)
    {
        for (Object obj = mData.get(i); (obj instanceof ListItemTitleModel) || !(obj instanceof SubjectModel);)
        {
            return 0;
        }

        return 1;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (getItemViewType(i) == 0)
        {
            return getTitleView(i, view, viewgroup);
        } else
        {
            return getContentView(i, view, viewgroup);
        }
    }

    public int getViewTypeCount()
    {
        return 2;
    }
}
