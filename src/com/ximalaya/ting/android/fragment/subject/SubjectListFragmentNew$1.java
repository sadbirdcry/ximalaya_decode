// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.subject;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.subject.SubjectModel;
import java.util.Collections;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.fragment.subject:
//            SubjectListFragmentNew

class val.loadMore extends a
{

    final SubjectListFragmentNew this$0;
    final boolean val$loadMore;

    public void onBindXDCS(Header aheader[])
    {
    }

    public void onFinish()
    {
        SubjectListFragmentNew.access$002(SubjectListFragmentNew.this, false);
    }

    public void onNetError(int i, String s)
    {
        if (!canGoon())
        {
            return;
        } else
        {
            showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
            SubjectListFragmentNew.access$100(SubjectListFragmentNew.this, val$loadMore);
            return;
        }
    }

    public void onStart()
    {
        SubjectListFragmentNew.access$002(SubjectListFragmentNew.this, true);
    }

    public void onSuccess(final String newList)
    {
        if (!canGoon())
        {
            return;
        }
        if (TextUtils.isEmpty(newList))
        {
            showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
            SubjectListFragmentNew.access$100(SubjectListFragmentNew.this, val$loadMore);
            return;
        }
        Object obj = null;
        try
        {
            newList = JSON.parseObject(newList);
        }
        // Misplaced declaration of an exception variable
        catch (final String newList)
        {
            newList.printStackTrace();
            newList = obj;
        }
        if (newList == null || newList.getIntValue("ret") != 0)
        {
            showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
            SubjectListFragmentNew.access$100(SubjectListFragmentNew.this, val$loadMore);
            return;
        }
        SubjectListFragmentNew.access$202(SubjectListFragmentNew.this, newList.getIntValue("count"));
        if (newList.getString("list") == null)
        {
            showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
            SubjectListFragmentNew.access$100(SubjectListFragmentNew.this, val$loadMore);
            return;
        } else
        {
            newList = JSON.parseArray(newList.getString("list"), com/ximalaya/ting/android/model/subject/SubjectModel);
            Collections.sort(newList);
            newList = SubjectListFragmentNew.access$300(SubjectListFragmentNew.this, newList);
            class _cls1
                implements MyCallback
            {

                final SubjectListFragmentNew._cls1 this$1;
                final List val$newList;

                public void execute()
                {
                    if (SubjectListFragmentNew.access$400(this$0) != null)
                    {
                        if (SubjectListFragmentNew.access$500(this$0) == 1)
                        {
                            SubjectListFragmentNew.access$600(this$0).clear();
                        }
                        if (newList != null && newList.size() > 0)
                        {
                            SubjectListFragmentNew.access$600(this$0).addAll(newList);
                            SubjectListFragmentNew.access$400(this$0).notifyDataSetChanged();
                            int _tmp = SubjectListFragmentNew.access$508(this$0);
                            return;
                        }
                    }
                }

            _cls1()
            {
                this$1 = SubjectListFragmentNew._cls1.this;
                newList = list;
                super();
            }
            }

            SubjectListFragmentNew.access$700(SubjectListFragmentNew.this, new _cls1());
            showFooterView(com.ximalaya.ting.android.fragment.ew.HIDE_ALL);
            return;
        }
    }

    _cls1()
    {
        this$0 = final_subjectlistfragmentnew;
        val$loadMore = Z.this;
        super();
    }
}
