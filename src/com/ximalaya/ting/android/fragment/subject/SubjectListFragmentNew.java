// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.subject;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseListFragment;
import com.ximalaya.ting.android.fragment.ReloadFragment;
import com.ximalaya.ting.android.model.ListItemTitleModel;
import com.ximalaya.ting.android.model.subject.SubjectModel;
import com.ximalaya.ting.android.util.MyCallback;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.SlideView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.subject:
//            SubjectAdapterNew

public class SubjectListFragmentNew extends BaseListFragment
    implements com.ximalaya.ting.android.fragment.ReloadFragment.Callback
{

    public static final String HEADERMORE = "headermore";
    private String bundleInfo;
    private boolean isHideHeaderAndSlide;
    private SubjectAdapterNew mAdapter;
    private long mCategoryId;
    private ArrayList mDataList;
    private String mDateRegx;
    private boolean mIsLoading;
    private int mPageId;
    private int mPageSize;
    private int mTotal;

    public SubjectListFragmentNew()
    {
        mPageId = 1;
        mPageSize = 10;
        mTotal = 0;
        mIsLoading = false;
        mDateRegx = "MM/dd/yyyy";
        mDataList = new ArrayList();
        isHideHeaderAndSlide = false;
        mCategoryId = -1L;
        bundleInfo = null;
    }

    private List buildTitleItem(List list)
    {
        ArrayList arraylist = new ArrayList();
        if (list == null || list.isEmpty())
        {
            return arraylist;
        }
        Object obj;
        String s;
        Iterator iterator;
        if (mDataList != null && mDataList.size() > 0 && (mDataList.get(mDataList.size() - 1) instanceof SubjectModel))
        {
            obj = ToolUtil.formatTime(((SubjectModel)mDataList.get(mDataList.size() - 1)).releasedAt, mDateRegx);
        } else
        {
            obj = null;
        }
        s = ToolUtil.formatTime(((SubjectModel)list.get(0)).releasedAt, mDateRegx);
        if (TextUtils.isEmpty(((CharSequence) (obj))) || !((String) (obj)).equals(s))
        {
            obj = new ListItemTitleModel();
            ((ListItemTitleModel) (obj)).setTitleText(s);
            arraylist.add(obj);
        }
        iterator = list.iterator();
        arraylist.add(iterator.next());
        for (list = s; iterator.hasNext(); list = ((List) (obj)))
        {
            SubjectModel subjectmodel = (SubjectModel)iterator.next();
            obj = ToolUtil.formatTime(subjectmodel.releasedAt, mDateRegx);
            if (!TextUtils.equals(list, ((CharSequence) (obj))))
            {
                list = new ListItemTitleModel();
                list.setTitleText(((String) (obj)));
                arraylist.add(list);
            }
            arraylist.add(subjectmodel);
        }

        return arraylist;
    }

    private void initTitleBarAndSlide()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            isHideHeaderAndSlide = bundle.getBoolean("isHideHeaderAndSlide", false);
            if (isHideHeaderAndSlide)
            {
                if (findViewById(0x7f0a005f) != null)
                {
                    findViewById(0x7f0a005f).setVisibility(8);
                }
                if (findViewById(0x7f0a0055) != null)
                {
                    ((SlideView)findViewById(0x7f0a0055)).setSlide(false);
                }
            }
        }
    }

    private void initView()
    {
        mAdapter = new SubjectAdapterNew(getActivity(), this, mDataList);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new _cls2());
        mListView.setOnScrollListener(new _cls3());
        mFooterViewLoading.setOnClickListener(new _cls4());
    }

    private void loadData(final boolean loadMore)
    {
        if (mIsLoading)
        {
            return;
        }
        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
        mIsLoading = true;
        String s = "mobile/discovery/v2/category/subjects";
        if (mCategoryId == -1L)
        {
            s = "m/subject_list";
        }
        Log.e("", (new StringBuilder()).append("Xm url ").append(s).toString());
        RequestParams requestparams = new RequestParams();
        if (bundleInfo != null)
        {
            requestparams.add("title", bundleInfo);
        }
        requestparams.add("page", (new StringBuilder()).append("").append(mPageId).toString());
        requestparams.add("per_page", (new StringBuilder()).append("").append(mPageSize).toString());
        if (mCategoryId != -1L)
        {
            requestparams.put("scale", 2);
            requestparams.put("categoryId", mCategoryId);
        }
        f.a().a(s, requestparams, null, new _cls1());
    }

    private boolean moreDataAvailable()
    {
        return mDataList.size() < mTotal;
    }

    public static SubjectListFragmentNew newInstance(long l)
    {
        SubjectListFragmentNew subjectlistfragmentnew = new SubjectListFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putLong("category_id", l);
        subjectlistfragmentnew.setArguments(bundle);
        return subjectlistfragmentnew;
    }

    public static SubjectListFragmentNew newInstance(boolean flag)
    {
        SubjectListFragmentNew subjectlistfragmentnew = new SubjectListFragmentNew();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isHideHeaderAndSlide", flag);
        subjectlistfragmentnew.setArguments(bundle);
        return subjectlistfragmentnew;
    }

    private void showNoData(boolean flag)
    {
        if (flag)
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.NO_DATA);
            return;
        } else
        {
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
            ReloadFragment.show(getChildFragmentManager(), 0x7f0a012d);
            return;
        }
    }

    public void initCommon()
    {
        mListView = (ListView)findViewById(0x7f0a0025);
        super.initCommon();
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initTitleBarAndSlide();
        setTitleText("\u7CBE\u54C1\u542C\u5355");
        initView();
        loadData(false);
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        bundle = getArguments();
        if (bundle != null)
        {
            mCategoryId = bundle.getLong("category_id", -1L);
            bundleInfo = bundle.getString("headermore");
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300c1, null);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        super.onDestroyView();
    }

    public void reload(View view)
    {
        loadData(false);
    }



/*
    static boolean access$002(SubjectListFragmentNew subjectlistfragmentnew, boolean flag)
    {
        subjectlistfragmentnew.mIsLoading = flag;
        return flag;
    }

*/



/*
    static int access$202(SubjectListFragmentNew subjectlistfragmentnew, int i)
    {
        subjectlistfragmentnew.mTotal = i;
        return i;
    }

*/





/*
    static int access$508(SubjectListFragmentNew subjectlistfragmentnew)
    {
        int i = subjectlistfragmentnew.mPageId;
        subjectlistfragmentnew.mPageId = i + 1;
        return i;
    }

*/





    private class _cls2
        implements android.widget.AdapterView.OnItemClickListener
    {

        final SubjectListFragmentNew this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (OneClickHelper.getInstance().onClick(view) && mDataList.size() != 0)
            {
                adapterview = ((AdapterView) (mDataList.get(i - mListView.getHeaderViewsCount())));
                if (adapterview instanceof SubjectModel)
                {
                    int j = i - mListView.getHeaderViewsCount();
                    int k;
                    int i1;
                    for (k = 0; j > 0; k = i1)
                    {
                        i1 = k;
                        if (mDataList.get(j - 1) instanceof ListItemTitleModel)
                        {
                            i1 = k + 1;
                        }
                        j--;
                    }

                    adapterview = (SubjectModel)adapterview;
                    view = new Bundle();
                    view.putLong("subjectId", ((SubjectModel) (adapterview)).specialId);
                    view.putInt("contentType", ((SubjectModel) (adapterview)).contentType);
                    view.putString("position", (new StringBuilder()).append((i - mListView.getHeaderViewsCount() - k) + 1).append("").toString());
                    view.putString("title", "\u7CBE\u54C1\u542C\u5355");
                    startFragment(com/ximalaya/ting/android/fragment/subject/SubjectFragment, view);
                    return;
                }
            }
        }

        _cls2()
        {
            this$0 = SubjectListFragmentNew.this;
            super();
        }
    }


    private class _cls3
        implements android.widget.AbsListView.OnScrollListener
    {

        final SubjectListFragmentNew this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                if (i == 0)
                {
                    i = abslistview.getCount();
                    if (i > 5)
                    {
                        i -= 5;
                    } else
                    {
                        i--;
                    }
                    if (abslistview.getLastVisiblePosition() <= i || !moreDataAvailable())
                    {
                        break label0;
                    }
                    if (!mIsLoading)
                    {
                        showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.LOADING);
                        loadData(true);
                    }
                }
                return;
            }
            showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
        }

        _cls3()
        {
            this$0 = SubjectListFragmentNew.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final SubjectListFragmentNew this$0;

        public void onClick(View view)
        {
            loadData(true);
        }

        _cls4()
        {
            this$0 = SubjectListFragmentNew.this;
            super();
        }
    }


    private class _cls1 extends a
    {

        final SubjectListFragmentNew this$0;
        final boolean val$loadMore;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onFinish()
        {
            mIsLoading = false;
        }

        public void onNetError(int i, String s)
        {
            if (!canGoon())
            {
                return;
            } else
            {
                showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\u8BD5\uFF01");
                showNoData(loadMore);
                return;
            }
        }

        public void onStart()
        {
            mIsLoading = true;
        }

        public void onSuccess(final String newList)
        {
            if (!canGoon())
            {
                return;
            }
            if (TextUtils.isEmpty(newList))
            {
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
                showNoData(loadMore);
                return;
            }
            Object obj = null;
            try
            {
                newList = JSON.parseObject(newList);
            }
            // Misplaced declaration of an exception variable
            catch (final String newList)
            {
                newList.printStackTrace();
                newList = obj;
            }
            if (newList == null || newList.getIntValue("ret") != 0)
            {
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
                showNoData(loadMore);
                return;
            }
            mTotal = newList.getIntValue("count");
            if (newList.getString("list") == null)
            {
                showToast("\u65E0\u7F51\u7EDC\u6570\u636E\uFF01");
                showNoData(loadMore);
                return;
            } else
            {
                newList = JSON.parseArray(newList.getString("list"), com/ximalaya/ting/android/model/subject/SubjectModel);
                Collections.sort(newList);
                newList = buildTitleItem(newList);
                class _cls1
                    implements MyCallback
                {

                    final _cls1 this$1;
                    final List val$newList;

                    public void execute()
                    {
                        if (mAdapter != null)
                        {
                            if (mPageId == 1)
                            {
                                mDataList.clear();
                            }
                            if (newList != null && newList.size() > 0)
                            {
                                mDataList.addAll(newList);
                                mAdapter.notifyDataSetChanged();
                                int i = ((access._cls400) (this)).access$400;
                                return;
                            }
                        }
                    }

                _cls1()
                {
                    this$1 = _cls1.this;
                    newList = list;
                    super();
                }
                }

                doAfterAnimation(new _cls1());
                showFooterView(com.ximalaya.ting.android.fragment.BaseListFragment.FooterView.HIDE_ALL);
                return;
            }
        }

        _cls1()
        {
            this$0 = SubjectListFragmentNew.this;
            loadMore = flag;
            super();
        }
    }

}
