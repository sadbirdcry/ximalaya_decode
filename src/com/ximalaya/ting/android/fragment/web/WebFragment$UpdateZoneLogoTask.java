// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.web;

import android.app.ProgressDialog;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import java.io.File;

// Referenced classes of package com.ximalaya.ting.android.fragment.web:
//            WebFragment

class pd extends MyAsyncTask
{

    BaseModel bm;
    String imgInfoString;
    ProgressDialog pd;
    final WebFragment this$0;

    protected transient Integer doInBackground(Object aobj[])
    {
        Object obj = (new StringBuilder()).append(a.F).append("dtres/").append((String)aobj[0]).append("/upload").toString();
        if (loginInfoModel == null)
        {
            loginInfoModel = UserInfoMannage.getInstance().getUser();
        }
        aobj = (File)aobj[1];
        try
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("fileSize", (new StringBuilder()).append(((File) (aobj)).length()).append("").toString());
            requestparams.put("uid", (new StringBuilder()).append(loginInfoModel.uid).append("").toString());
            requestparams.put("token", loginInfoModel.token);
            requestparams.put("myfile", ((File) (aobj)));
            aobj = f.a().b(((String) (obj)), requestparams, WebFragment.access$000(WebFragment.this), WebFragment.access$000(WebFragment.this));
        }
        // Misplaced declaration of an exception variable
        catch (Object aobj[])
        {
            aobj = null;
        }
        obj = WebFragment.access$2100(WebFragment.this, WebFragment.access$2000(WebFragment.this));
        if (((File) (obj)).exists())
        {
            ((File) (obj)).delete();
        }
        if (Utilities.isBlank(((String) (aobj))))
        {
            return Integer.valueOf(1);
        }
        aobj = JSON.parseObject(((String) (aobj)));
        if (aobj == null)
        {
            break MISSING_BLOCK_LABEL_401;
        }
        bm = new BaseModel();
        bm.ret = ((JSONObject) (aobj)).getInteger("ret").intValue();
        if (bm.ret == -1)
        {
            break MISSING_BLOCK_LABEL_401;
        }
        bm.msg = ((JSONObject) (aobj)).getString("msg");
        if (bm.ret != 0 || bm.msg == null)
        {
            break MISSING_BLOCK_LABEL_401;
        }
        aobj = JSONObject.parseArray(bm.msg);
        if (((JSONArray) (aobj)).size() <= 0)
        {
            break MISSING_BLOCK_LABEL_401;
        }
        aobj = (JSONObject)((JSONArray) (aobj)).get(0);
        if (!((JSONObject) (aobj)).containsKey("uploadTrack"))
        {
            break MISSING_BLOCK_LABEL_401;
        }
        aobj = ((JSONObject) (aobj)).getJSONObject("uploadTrack");
        if (aobj == null)
        {
            break MISSING_BLOCK_LABEL_401;
        }
        imgInfoString = ((JSONObject) (aobj)).toString();
        return Integer.valueOf(3);
        aobj;
        Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
        return Integer.valueOf(2);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onPostExecute(Integer integer)
    {
        if (pd != null)
        {
            pd.cancel();
            pd = null;
        }
        if (isAdded() && mCon != null)
        {
            if (integer.intValue() == 3)
            {
                if (bm != null && bm.ret == 0)
                {
                    WebFragment.access$2200(WebFragment.this, imgInfoString, WebFragment.access$1500(WebFragment.this));
                    return;
                }
                if (bm != null)
                {
                    Toast.makeText(mCon, bm.msg, 1).show();
                    return;
                }
            } else
            {
                Toast.makeText(mCon, "\u4E0A\u4F20\u56FE\u7247\u5931\u8D25", 1).show();
                return;
            }
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        if (pd == null)
        {
            pd = new MyProgressDialog(getActivity());
        }
        class _cls1
            implements android.content.DialogInterface.OnKeyListener
        {

            final WebFragment.UpdateZoneLogoTask this$1;

            public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
            {
                if (i == 4)
                {
                    dialoginterface.dismiss();
                    return true;
                } else
                {
                    return false;
                }
            }

            _cls1()
            {
                this$1 = WebFragment.UpdateZoneLogoTask.this;
                super();
            }
        }

        pd.setOnKeyListener(new _cls1());
        pd.setCanceledOnTouchOutside(true);
        pd.setTitle("\u4E0A\u4F20");
        pd.setMessage("\u56FE\u7247\u4E0A\u4F20\u4E2D");
        pd.show();
    }

    _cls1()
    {
        this$0 = WebFragment.this;
        super();
        pd = null;
    }
}
