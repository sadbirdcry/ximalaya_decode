// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.web;

import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;

// Referenced classes of package com.ximalaya.ting.android.fragment.web:
//            WebFragment

class this._cls0 extends WebChromeClient
{

    final WebFragment this$0;

    public void onProgressChanged(WebView webview, int i)
    {
        super.onProgressChanged(webview, i);
        WebFragment.access$300(WebFragment.this).setProgress(i);
    }

    public void onReceivedTitle(WebView webview, String s)
    {
        super.onReceivedTitle(webview, s);
        setTitleText(s);
    }

    public boolean onShowFileChooser(WebView webview, ValueCallback valuecallback, android.webkit.erParams erparams)
    {
        openFileChooser(valuecallback, "");
        return true;
    }

    public void openFileChooser(ValueCallback valuecallback)
    {
        openFileChooser(valuecallback, "");
    }

    public void openFileChooser(ValueCallback valuecallback, String s)
    {
        if (WebFragment.access$400(WebFragment.this) != null)
        {
            return;
        } else
        {
            WebFragment.access$402(WebFragment.this, valuecallback);
            WebFragment.access$500(WebFragment.this, 1);
            return;
        }
    }

    public void openFileChooser(ValueCallback valuecallback, String s, String s1)
    {
        openFileChooser(valuecallback, s);
    }

    ()
    {
        this$0 = WebFragment.this;
        super();
    }
}
