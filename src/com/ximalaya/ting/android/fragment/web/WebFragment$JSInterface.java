// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.web;

import android.support.v4.app.FragmentActivity;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.d.a;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.web:
//            WebFragment

public final class this._cls0
{

    final WebFragment this$0;

    public void appPay(String s)
    {
        WebFragment.access$1700(WebFragment.this).a(s);
    }

    public String appReady()
    {
        if (!isAdded())
        {
            return "";
        }
        JSONObject jsonobject = new JSONObject();
        jsonobject.put("platform", "android");
        jsonobject.put("version", ((MyApplication)getActivity().getApplication()).m());
        jsonobject.put("platformVersion", ToolUtil.getSimpleSystemVersion());
        jsonobject.put("deviceId", ToolUtil.getDeviceToken(WebFragment.access$1100(WebFragment.this)));
        if (UserInfoMannage.hasLogined())
        {
            jsonobject.put("uid", Long.valueOf(UserInfoMannage.getInstance().getUser().uid));
            jsonobject.put("token", UserInfoMannage.getInstance().getUser().token);
        }
        return jsonobject.toString();
    }

    public void appShareActivity(String s)
    {
        class _cls2
            implements Runnable
        {

            final WebFragment.JSInterface this$1;
            final int val$activityIdFinal;

            public void run()
            {
                if (activityIdFinal < 0)
                {
                    showToast("\u6D3B\u52A8\u4E0D\u5B58\u5728\uFF01");
                    return;
                } else
                {
                    (new BaseShareDialog(WebFragment.access$1300(this$0), activityIdFinal)).show();
                    return;
                }
            }

            _cls2()
            {
                this$1 = WebFragment.JSInterface.this;
                activityIdFinal = i;
                super();
            }
        }

        final int activityIdFinal;
        try
        {
            activityIdFinal = Integer.valueOf(s).intValue();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            activityIdFinal = -1;
        }
        getActivity().runOnUiThread(new _cls2());
    }

    public void appShareSound(String s, String s1)
    {
        class _cls1
            implements Runnable
        {

            final WebFragment.JSInterface this$1;
            final int val$activityIdFinal;
            final long val$soundIdFinal;

            public void run()
            {
                if (activityIdFinal < 0 || soundIdFinal < 0L)
                {
                    showToast("\u6D3B\u52A8\u4E0D\u5B58\u5728\uFF01");
                    return;
                } else
                {
                    (new BaseShareDialog(WebFragment.access$1200(this$0), activityIdFinal, soundIdFinal, 2)).show();
                    return;
                }
            }

            _cls1()
            {
                this$1 = WebFragment.JSInterface.this;
                activityIdFinal = i;
                soundIdFinal = l;
                super();
            }
        }

        final int activityIdFinal;
        final long soundIdFinal;
        try
        {
            activityIdFinal = Integer.valueOf(s).intValue();
            soundIdFinal = Long.valueOf(s1).longValue();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            activityIdFinal = -1;
            soundIdFinal = -1L;
        }
        getActivity().runOnUiThread(new _cls1());
    }

    public void appShareVote(String s, String s1)
    {
        class _cls3
            implements Runnable
        {

            final WebFragment.JSInterface this$1;
            final int val$activityIdFinal;
            final long val$soundIdFinal;

            public void run()
            {
                if (activityIdFinal < 0 || soundIdFinal < 0L)
                {
                    showToast("\u6D3B\u52A8\u4E0D\u5B58\u5728\uFF01");
                    return;
                } else
                {
                    (new BaseShareDialog(WebFragment.access$1400(this$0), activityIdFinal, soundIdFinal, 1)).show();
                    return;
                }
            }

            _cls3()
            {
                this$1 = WebFragment.JSInterface.this;
                activityIdFinal = i;
                soundIdFinal = l;
                super();
            }
        }

        final int activityIdFinal;
        final long soundIdFinal;
        try
        {
            activityIdFinal = Integer.valueOf(s).intValue();
            soundIdFinal = Long.valueOf(s1).longValue();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            activityIdFinal = -1;
            soundIdFinal = -1L;
        }
        getActivity().runOnUiThread(new _cls3());
    }

    public void audioPause()
    {
        class _cls5
            implements Runnable
        {

            final WebFragment.JSInterface this$1;

            public void run()
            {
                LocalMediaService localmediaservice = LocalMediaService.getInstance();
                if (localmediaservice != null)
                {
                    localmediaservice.pause();
                }
            }

            _cls5()
            {
                this$1 = WebFragment.JSInterface.this;
                super();
            }
        }

        getActivity().runOnUiThread(new _cls5());
    }

    public void audioPlay(final String trackId)
    {
        class _cls4
            implements Runnable
        {

            final WebFragment.JSInterface this$1;
            final String val$trackId;

            public void run()
            {
                LocalMediaService localmediaservice = LocalMediaService.getInstance();
                SoundInfo soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
                if (localmediaservice == null || TextUtils.isEmpty(trackId))
                {
                    return;
                }
                if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && Long.parseLong(trackId) == soundinfo1.trackId)
                {
                    localmediaservice.start();
                    return;
                } else
                {
                    SoundInfo soundinfo = new SoundInfo();
                    soundinfo.trackId = Long.parseLong(trackId);
                    PlayTools.gotoPlayWithoutUrl(24, soundinfo, getActivity(), false, null);
                    return;
                }
            }

            _cls4()
            {
                this$1 = WebFragment.JSInterface.this;
                trackId = s;
                super();
            }
        }

        getActivity().runOnUiThread(new _cls4());
    }

    public void captureImage(String s, String s1, String s2)
    {
        WebFragment.access$1502(WebFragment.this, s);
        WebFragment.access$1602(WebFragment.this, s1);
        WebFragment.access$500(WebFragment.this, 2);
    }

    public String getSupportPayType()
    {
        String s = WebFragment.access$1700(WebFragment.this).a();
        Logger.log((new StringBuilder()).append("getSupportPayType=").append(s).toString());
        return s;
    }

    public void getTitle(String s)
    {
    }

    public void notificationToast(final String msg)
    {
        if (getActivity() == null || getActivity().isFinishing())
        {
            return;
        } else
        {
            class _cls6
                implements Runnable
            {

                final WebFragment.JSInterface this$1;
                final String val$msg;

                public void run()
                {
                    try
                    {
                        showToast(URLDecoder.decode(msg, "UTF-8"));
                        return;
                    }
                    catch (UnsupportedEncodingException unsupportedencodingexception)
                    {
                        unsupportedencodingexception.printStackTrace();
                    }
                }

            _cls6()
            {
                this$1 = WebFragment.JSInterface.this;
                msg = s;
                super();
            }
            }

            getActivity().runOnUiThread(new _cls6());
            return;
        }
    }

    public void onerror(String s)
    {
    }

    public void payFinished()
    {
        WebFragment.access$1700(WebFragment.this).b(null);
    }

    public void payFinished(String s)
    {
        WebFragment.access$1700(WebFragment.this).b(s);
    }

    public _cls6()
    {
        this$0 = WebFragment.this;
        super();
    }
}
