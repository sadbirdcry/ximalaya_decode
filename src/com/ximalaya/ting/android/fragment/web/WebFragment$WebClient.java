// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.web;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.FreeFlowUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.web:
//            WebFragment

class this._cls0 extends WebViewClient
{

    final WebFragment this$0;

    public void onPageFinished(WebView webview, String s)
    {
        s = CookieManager.getInstance().getCookie(s);
        Logger.log((new StringBuilder()).append("cookie = ").append(s).toString());
        if (!TextUtils.isEmpty(s))
        {
            int i = s.indexOf("phonenum=");
            if (i >= 0)
            {
                s = s.substring(i);
                i = s.indexOf('=') + 1;
                if (s.length() > i + 10 && s.charAt(i) == '1')
                {
                    s = s.substring(i, i + 11);
                    FreeFlowUtil.getInstance().savePhoneNumber(s);
                    s = ((TelephonyManager)MyApplication.b().getSystemService("phone")).getSubscriberId();
                    if (!TextUtils.isEmpty(s))
                    {
                        FreeFlowUtil.getInstance().saveIMSI(s);
                    }
                }
                if (getActivity() != null)
                {
                    getActivity().setResult(-1);
                }
            }
        }
        setTitleText(webview.getTitle());
    }

    public void onPageStarted(WebView webview, String s, Bitmap bitmap)
    {
        if (isAdded() && s != null)
        {
            if (s.contains("iting://"))
            {
                try
                {
                    webview = new Intent("android.intent.action.VIEW");
                    webview.setData(Uri.parse(s));
                    startActivity(webview);
                    return;
                }
                // Misplaced declaration of an exception variable
                catch (WebView webview)
                {
                    return;
                }
            }
            if (s.startsWith("http"))
            {
                super.onPageStarted(webview, s, bitmap);
                return;
            }
        }
    }

    public void onReceivedError(WebView webview, int i, String s, String s1)
    {
        super.onReceivedError(webview, i, s, s1);
        WebFragment.access$1000(WebFragment.this, true);
    }

    public boolean shouldOverrideUrlLoading(WebView webview, String s)
    {
        if (isAdded() && s != null)
        {
            if (s.contains("iting://"))
            {
                try
                {
                    webview = new Intent("android.intent.action.VIEW");
                    webview.setData(Uri.parse(s));
                    startActivity(webview);
                }
                // Misplaced declaration of an exception variable
                catch (WebView webview)
                {
                    return true;
                }
                return true;
            }
            if (s.startsWith("http"))
            {
                WebFragment.access$900(WebFragment.this, s);
                return true;
            }
        }
        return true;
    }

    ()
    {
        this$0 = WebFragment.this;
        super();
    }
}
