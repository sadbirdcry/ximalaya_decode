// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.web;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.support.v4.app.FragmentActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebHistoryItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.d.a;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.fragment.ManageFragment;
import com.ximalaya.ting.android.kdt.KDTAction;
import com.ximalaya.ting.android.library.service.DownloadService;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.BitmapUtils;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.wxapi.XMWXPayEntryActivity;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WebFragment extends BaseActivityLikeFragment
    implements android.view.View.OnClickListener, DownloadListener
{
    class ChromeClient extends WebChromeClient
    {

        final WebFragment this$0;

        public void onProgressChanged(WebView webview, int i)
        {
            super.onProgressChanged(webview, i);
            mProgressBar.setProgress(i);
        }

        public void onReceivedTitle(WebView webview, String s)
        {
            super.onReceivedTitle(webview, s);
            setTitleText(s);
        }

        public boolean onShowFileChooser(WebView webview, ValueCallback valuecallback, android.webkit.WebChromeClient.FileChooserParams filechooserparams)
        {
            openFileChooser(valuecallback, "");
            return true;
        }

        public void openFileChooser(ValueCallback valuecallback)
        {
            openFileChooser(valuecallback, "");
        }

        public void openFileChooser(ValueCallback valuecallback, String s)
        {
            if (mUploadMessage != null)
            {
                return;
            } else
            {
                mUploadMessage = valuecallback;
                showSelectDialog(1);
                return;
            }
        }

        public void openFileChooser(ValueCallback valuecallback, String s, String s1)
        {
            openFileChooser(valuecallback, s);
        }

        ChromeClient()
        {
            this$0 = WebFragment.this;
            super();
        }
    }

    public final class JSInterface
    {

        final WebFragment this$0;

        public void appPay(String s)
        {
            mDaShangAction.a(s);
        }

        public String appReady()
        {
            if (!isAdded())
            {
                return "";
            }
            JSONObject jsonobject = new JSONObject();
            jsonobject.put("platform", "android");
            jsonobject.put("version", ((MyApplication)getActivity().getApplication()).m());
            jsonobject.put("platformVersion", ToolUtil.getSimpleSystemVersion());
            jsonobject.put("deviceId", ToolUtil.getDeviceToken(_cls2));
            if (UserInfoMannage.hasLogined())
            {
                jsonobject.put("uid", Long.valueOf(UserInfoMannage.getInstance().getUser().uid));
                jsonobject.put("token", UserInfoMannage.getInstance().getUser().token);
            }
            return jsonobject.toString();
        }

        public void appShareActivity(String s)
        {
            final int activityIdFinal;
            try
            {
                activityIdFinal = Integer.valueOf(s).intValue();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                activityIdFinal = -1;
            }
            getActivity().runOnUiThread(new _cls2());
        }

        public void appShareSound(String s, String s1)
        {
            final int activityIdFinal;
            final long soundIdFinal;
            try
            {
                activityIdFinal = Integer.valueOf(s).intValue();
                soundIdFinal = Long.valueOf(s1).longValue();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                activityIdFinal = -1;
                soundIdFinal = -1L;
            }
            getActivity().runOnUiThread(new _cls1());
        }

        public void appShareVote(String s, String s1)
        {
            final int activityIdFinal;
            final long soundIdFinal;
            try
            {
                activityIdFinal = Integer.valueOf(s).intValue();
                soundIdFinal = Long.valueOf(s1).longValue();
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                activityIdFinal = -1;
                soundIdFinal = -1L;
            }
            getActivity().runOnUiThread(new _cls3());
        }

        public void audioPause()
        {
            class _cls5
                implements Runnable
            {

                final JSInterface this$1;

                public void run()
                {
                    LocalMediaService localmediaservice = LocalMediaService.getInstance();
                    if (localmediaservice != null)
                    {
                        localmediaservice.pause();
                    }
                }

                _cls5()
                {
                    this$1 = JSInterface.this;
                    Object();
                }
            }

            getActivity().runOnUiThread(new _cls5());
        }

        public void audioPlay(final String trackId)
        {
            class _cls4
                implements Runnable
            {

                final JSInterface this$1;
                final String val$trackId;

                public void run()
                {
                    LocalMediaService localmediaservice = LocalMediaService.getInstance();
                    SoundInfo soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
                    if (localmediaservice == null || TextUtils.isEmpty(trackId))
                    {
                        return;
                    }
                    if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && Long.parseLong(trackId) == soundinfo1.trackId)
                    {
                        localmediaservice.start();
                        return;
                    } else
                    {
                        SoundInfo soundinfo = new SoundInfo();
                        soundinfo.trackId = Long.parseLong(trackId);
                        PlayTools.gotoPlayWithoutUrl(24, soundinfo, getActivity(), false, null);
                        return;
                    }
                }

                _cls4()
                {
                    this$1 = JSInterface.this;
                    trackId = s;
                    Object();
                }
            }

            getActivity().runOnUiThread(new _cls4());
        }

        public void captureImage(String s, String s1, String s2)
        {
            mCallbackName = s;
            mUploadSourceType = s1;
            showSelectDialog(2);
        }

        public String getSupportPayType()
        {
            String s = mDaShangAction.a();
            Logger.log((new StringBuilder()).append("getSupportPayType=").append(s).toString());
            return s;
        }

        public void getTitle(String s)
        {
        }

        public void notificationToast(final String msg)
        {
            if (getActivity() == null || getActivity().isFinishing())
            {
                return;
            } else
            {
                class _cls6
                    implements Runnable
                {

                    final JSInterface this$1;
                    final String val$msg;

                    public void run()
                    {
                        try
                        {
                            showToast(URLDecoder.decode(msg, "UTF-8"));
                            return;
                        }
                        catch (UnsupportedEncodingException unsupportedencodingexception)
                        {
                            unsupportedencodingexception.printStackTrace();
                        }
                    }

                _cls6()
                {
                    this$1 = JSInterface.this;
                    msg = s;
                    super();
                }
                }

                getActivity().runOnUiThread(new _cls6());
                return;
            }
        }

        public void onerror(String s)
        {
        }

        public void payFinished()
        {
            mDaShangAction.b(null);
        }

        public void payFinished(String s)
        {
            mDaShangAction.b(s);
        }

        public JSInterface()
        {
            this$0 = WebFragment.this;
            super();
        }
    }

    class UpdateZoneLogoTask extends MyAsyncTask
    {

        BaseModel bm;
        String imgInfoString;
        ProgressDialog pd;
        final WebFragment this$0;

        protected transient Integer doInBackground(Object aobj[])
        {
            Object obj = (new StringBuilder()).append(com.ximalaya.ting.android.a.F).append("dtres/").append((String)aobj[0]).append("/upload").toString();
            if (loginInfoModel == null)
            {
                loginInfoModel = UserInfoMannage.getInstance().getUser();
            }
            aobj = (File)aobj[1];
            try
            {
                RequestParams requestparams = new RequestParams();
                requestparams.put("fileSize", (new StringBuilder()).append(((File) (aobj)).length()).append("").toString());
                requestparams.put("uid", (new StringBuilder()).append(loginInfoModel.uid).append("").toString());
                requestparams.put("token", loginInfoModel.token);
                requestparams.put("myfile", ((File) (aobj)));
                aobj = f.a().b(((String) (obj)), requestparams, mWebView, mWebView);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                aobj = null;
            }
            obj = getTempFile(tempFileName);
            if (((File) (obj)).exists())
            {
                ((File) (obj)).delete();
            }
            if (Utilities.isBlank(((String) (aobj))))
            {
                return Integer.valueOf(1);
            }
            aobj = JSON.parseObject(((String) (aobj)));
            if (aobj == null)
            {
                break MISSING_BLOCK_LABEL_401;
            }
            bm = new BaseModel();
            bm.ret = ((JSONObject) (aobj)).getInteger("ret").intValue();
            if (bm.ret == -1)
            {
                break MISSING_BLOCK_LABEL_401;
            }
            bm.msg = ((JSONObject) (aobj)).getString("msg");
            if (bm.ret != 0 || bm.msg == null)
            {
                break MISSING_BLOCK_LABEL_401;
            }
            aobj = JSONObject.parseArray(bm.msg);
            if (((JSONArray) (aobj)).size() <= 0)
            {
                break MISSING_BLOCK_LABEL_401;
            }
            aobj = (JSONObject)((JSONArray) (aobj)).get(0);
            if (!((JSONObject) (aobj)).containsKey("uploadTrack"))
            {
                break MISSING_BLOCK_LABEL_401;
            }
            aobj = ((JSONObject) (aobj)).getJSONObject("uploadTrack");
            if (aobj == null)
            {
                break MISSING_BLOCK_LABEL_401;
            }
            imgInfoString = ((JSONObject) (aobj)).toString();
            return Integer.valueOf(3);
            aobj;
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
            return Integer.valueOf(2);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            if (pd != null)
            {
                pd.cancel();
                pd = null;
            }
            if (isAdded() && mCon != null)
            {
                if (integer.intValue() == 3)
                {
                    if (bm != null && bm.ret == 0)
                    {
                        doJsCallback(imgInfoString, mCallbackName);
                        return;
                    }
                    if (bm != null)
                    {
                        Toast.makeText(mCon, bm.msg, 1).show();
                        return;
                    }
                } else
                {
                    Toast.makeText(mCon, "\u4E0A\u4F20\u56FE\u7247\u5931\u8D25", 1).show();
                    return;
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            if (pd == null)
            {
                pd = new MyProgressDialog(getActivity());
            }
            class _cls1
                implements android.content.DialogInterface.OnKeyListener
            {

                final UpdateZoneLogoTask this$1;

                public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
                {
                    if (i == 4)
                    {
                        dialoginterface.dismiss();
                        return true;
                    } else
                    {
                        return false;
                    }
                }

                _cls1()
                {
                    this$1 = UpdateZoneLogoTask.this;
                    super();
                }
            }

            pd.setOnKeyListener(new _cls1());
            pd.setCanceledOnTouchOutside(true);
            pd.setTitle("\u4E0A\u4F20");
            pd.setMessage("\u56FE\u7247\u4E0A\u4F20\u4E2D");
            pd.show();
        }

        UpdateZoneLogoTask()
        {
            this$0 = WebFragment.this;
            super();
            pd = null;
        }
    }

    class WebClient extends WebViewClient
    {

        final WebFragment this$0;

        public void onPageFinished(WebView webview, String s)
        {
            s = CookieManager.getInstance().getCookie(s);
            Logger.log((new StringBuilder()).append("cookie = ").append(s).toString());
            if (!TextUtils.isEmpty(s))
            {
                int i = s.indexOf("phonenum=");
                if (i >= 0)
                {
                    s = s.substring(i);
                    i = s.indexOf('=') + 1;
                    if (s.length() > i + 10 && s.charAt(i) == '1')
                    {
                        s = s.substring(i, i + 11);
                        FreeFlowUtil.getInstance().savePhoneNumber(s);
                        s = ((TelephonyManager)MyApplication.b().getSystemService("phone")).getSubscriberId();
                        if (!TextUtils.isEmpty(s))
                        {
                            FreeFlowUtil.getInstance().saveIMSI(s);
                        }
                    }
                    if (getActivity() != null)
                    {
                        getActivity().setResult(-1);
                    }
                }
            }
            setTitleText(webview.getTitle());
        }

        public void onPageStarted(WebView webview, String s, Bitmap bitmap)
        {
            if (isAdded() && s != null)
            {
                if (s.contains("iting://"))
                {
                    try
                    {
                        webview = new Intent("android.intent.action.VIEW");
                        webview.setData(Uri.parse(s));
                        startActivity(webview);
                        return;
                    }
                    // Misplaced declaration of an exception variable
                    catch (WebView webview)
                    {
                        return;
                    }
                }
                if (s.startsWith("http"))
                {
                    super.onPageStarted(webview, s, bitmap);
                    return;
                }
            }
        }

        public void onReceivedError(WebView webview, int i, String s, String s1)
        {
            super.onReceivedError(webview, i, s, s1);
            showNoNetworkLayout(true);
        }

        public boolean shouldOverrideUrlLoading(WebView webview, String s)
        {
            if (isAdded() && s != null)
            {
                if (s.contains("iting://"))
                {
                    try
                    {
                        webview = new Intent("android.intent.action.VIEW");
                        webview.setData(Uri.parse(s));
                        startActivity(webview);
                    }
                    // Misplaced declaration of an exception variable
                    catch (WebView webview)
                    {
                        return true;
                    }
                    return true;
                }
                if (s.startsWith("http"))
                {
                    loadPage(s);
                    return true;
                }
            }
            return true;
        }

        WebClient()
        {
            this$0 = WebFragment.this;
            super();
        }
    }


    public static final String BUNDLE_EXTRA = "BundleExtra";
    public static final String EXTRA_URL = "ExtraUrl";
    private static final String IMAGE_UNSPECIFIED = "image/*";
    public static final String IS_EXTERNAL_URL = "is_external_url";
    public static final int PAGE_ACTIVITY = 8;
    private static final int REQ_CAMERA = 1;
    private static final int REQ_CAMERA_ZONE_LOGO = 4;
    private static final int REQ_CHOOSE = 2;
    private static final int REQ_CHOOSE_ZONE_LOGO = 5;
    private static final int REQ_CROP_ZONE_LOGO = 6;
    private static final int REQ_LOGIN = 3;
    public static final String SHARE_COVER_PATH = "share_cover_path";
    public static final String SHOW_SHARE_BTN = "show_share_btn";
    private static final String SHOW_TITLE = "show_title";
    private static final int TYPE_PIC = 1;
    private static final int TYPE_PIC_ZONE_LOGO = 2;
    public static final String WEB_TITLE_BAR_VISIBLE = "WEB_TITLE_BAR_VISIBLE";
    public static final String WEB_VIEW_TYPE = "web_view_type";
    private Uri cameraUri;
    private ImageView mBackPage;
    private String mCallbackName;
    private ChromeClient mChromeClient;
    private a mDaShangAction;
    private ImageView mForwardPage;
    private boolean mIsShowShareBtn;
    private boolean mIsShowTitleBar;
    private KDTAction mKDTAction;
    private View mNoNetworkLayout;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private ProgressBar mProgressBar;
    private ImageView mRefreshPage;
    private String mRootUrl;
    private MenuDialog mSelectImgDialog;
    private ImageView mShareBtn;
    private String mShareCoverPath;
    private boolean mShowTitle;
    private ValueCallback mUploadMessage;
    private String mUploadSourceType;
    private WebSettings mWebSettings;
    private WebView mWebView;
    private WebViewClient mWebViewClient;
    private String tempFileName;
    private Uri tempImgUri;
    private View title_bar;
    private int type;

    public WebFragment()
    {
        mIsShowTitleBar = true;
        mIsShowShareBtn = false;
    }

    private String checkProtocol(String s)
    {
        String s1 = s;
        if (!s.contains("://"))
        {
            s1 = (new StringBuilder()).append("http://").append(s).toString();
        }
        return s1;
    }

    private void chosePic(int i)
    {
        Intent intent = new Intent("android.intent.action.PICK", null);
        intent.setDataAndType(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        if (i == 1)
        {
            startActivityForResult(intent, 2);
        } else
        if (i == 2)
        {
            startActivityForResult(intent, 5);
            return;
        }
    }

    private void deleteTempImg()
    {
        if (tempImgUri != null)
        {
            File file = new File(URI.create(tempImgUri.toString()));
            if (file.exists())
            {
                file.delete();
            }
        }
    }

    private void doCompressAndUpload(Uri uri, boolean flag)
    {
        BitmapUtils.compressImage(uri, flag, new _cls6());
    }

    private void doJsCallback(final String param, final String callbackName)
    {
        if (TextUtils.isEmpty(callbackName))
        {
            return;
        } else
        {
            mWebView.post(new _cls7());
            return;
        }
    }

    private Uri getRealUri(Uri uri)
    {
        Object obj;
        Object obj1;
        int i;
        if (android.os.Build.VERSION.SDK_INT >= 19)
        {
            i = 1;
        } else
        {
            i = 0;
        }
        if (i == 0 || !DocumentsContract.isDocumentUri(getActivity(), uri)) goto _L2; else goto _L1
_L1:
        obj = DocumentsContract.getDocumentId(uri).split(":")[1];
        uri = new String[1];
        uri[0] = "_data";
        obj = getActivity().getContentResolver().query(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, uri, "_id=?", new String[] {
            obj
        }, null);
        if (obj == null) goto _L4; else goto _L3
_L3:
        i = ((Cursor) (obj)).getColumnIndex(uri[0]);
        if (!((Cursor) (obj)).moveToFirst()) goto _L4; else goto _L5
_L5:
        uri = ((Cursor) (obj)).getString(i);
_L11:
        obj1 = uri;
        if (obj != null)
        {
            ((Cursor) (obj)).close();
            obj1 = uri;
        }
_L8:
        uri = Uri.fromFile(new File(((String) (obj1))));
_L7:
        return uri;
        uri;
        obj = null;
_L9:
        if (obj != null)
        {
            ((Cursor) (obj)).close();
        }
        throw uri;
_L2:
        obj = getActivity().getContentResolver().query(uri, new String[] {
            "_data"
        }, null, null, null);
        if (obj == null) goto _L7; else goto _L6
_L6:
        int j = ((Cursor) (obj)).getColumnIndexOrThrow("_data");
        ((Cursor) (obj)).moveToFirst();
        obj1 = ((Cursor) (obj)).getString(j);
        ((Cursor) (obj)).close();
          goto _L8
        uri;
          goto _L9
_L4:
        uri = null;
        if (true) goto _L11; else goto _L10
_L10:
    }

    private File getTempFile(String s)
    {
        File file;
        if (Environment.getExternalStorageState().equals("mounted"))
        {
            file = new File(Environment.getExternalStorageDirectory(), "/ting/images");
        } else
        {
            file = mCon.getCacheDir();
        }
        if (file.exists()) goto _L2; else goto _L1
_L1:
        file.mkdirs();
_L4:
        try
        {
            s = new File(file, s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s == null || s.exists())
        {
            break MISSING_BLOCK_LABEL_64;
        }
        s.createNewFile();
        return s;
_L2:
        if (file.isFile())
        {
            file.deleteOnExit();
            file.mkdirs();
        }
        continue; /* Loop/switch isn't completed */
        IOException ioexception;
        ioexception;
        ioexception.printStackTrace();
        return s;
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void initBarVisible()
    {
        if (mIsShowTitleBar)
        {
            title_bar.setVisibility(0);
            return;
        } else
        {
            title_bar.setVisibility(8);
            return;
        }
    }

    private void initData()
    {
        Map map = null;
        tempImgUri = null;
        Object obj2 = getArguments();
        Object obj1;
        boolean flag1;
        if (obj2 != null)
        {
            Object obj;
            int i;
            boolean flag;
            if (((Bundle) (obj2)).containsKey("ExtraUrl"))
            {
                obj = ((Bundle) (obj2)).getString("ExtraUrl");
            } else
            {
                obj = null;
            }
            if (((Bundle) (obj2)).containsKey("web_view_type"))
            {
                type = ((Bundle) (obj2)).getInt("web_view_type");
                if (type == 8)
                {
                    findViewById(0x7f0a0435).setVisibility(8);
                }
            }
            if (((Bundle) (obj2)).containsKey("WEB_TITLE_BAR_VISIBLE"))
            {
                mIsShowTitleBar = ((Bundle) (obj2)).getBoolean("WEB_TITLE_BAR_VISIBLE");
                initBarVisible();
            }
            if (((Bundle) (obj2)).containsKey("is_external_url"))
            {
                flag = ((Bundle) (obj2)).getBoolean("is_external_url", false);
            } else
            {
                flag = false;
            }
            if (((Bundle) (obj2)).containsKey("show_share_btn"))
            {
                mIsShowShareBtn = ((Bundle) (obj2)).getBoolean("show_share_btn", false);
            }
            obj1 = obj;
            flag1 = flag;
            if (((Bundle) (obj2)).containsKey("share_cover_path"))
            {
                mShareCoverPath = ((Bundle) (obj2)).getString("share_cover_path");
                flag1 = flag;
                obj1 = obj;
            }
        } else
        {
            flag1 = false;
            obj1 = null;
        }
        mRootUrl = ((String) (obj1));
        if (TextUtils.isEmpty(((CharSequence) (obj1))))
        {
            obj = "www.ximalaya.com";
        } else
        {
            obj = obj1;
        }
        obj1 = obj;
        if (flag1) goto _L2; else goto _L1
_L1:
        i = ((String) (obj)).lastIndexOf("#");
        if (i > 0)
        {
            obj2 = ((String) (obj)).substring(i, ((String) (obj)).length());
            obj = ((String) (obj)).substring(0, i);
        } else
        {
            obj2 = "";
        }
        if (((String) (obj)).contains("?")) goto _L4; else goto _L3
_L3:
        obj = (new StringBuilder()).append(((String) (obj))).append("?app=iting&version=").append(((MyApplication)(MyApplication)getActivity().getApplication()).m()).toString();
_L6:
        obj1 = (new StringBuilder()).append(((String) (obj))).append(((String) (obj2))).toString();
_L2:
        obj = title_bar;
        if (mShowTitle)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        ((View) (obj)).setVisibility(i);
        mShareBtn = (ImageView)findViewById(0x7f0a0710);
        if (mIsShowShareBtn && title_bar.getVisibility() == 0)
        {
            mShareBtn.setImageResource(0x7f020537);
            mShareBtn.setVisibility(0);
        }
        mShareBtn.setOnClickListener(new _cls1());
        loadPage(((String) (obj1)));
        return;
_L4:
        try
        {
            obj1 = Uri.parse(((String) (obj)));
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            obj1 = null;
        }
        if (obj1 != null)
        {
            map = ToolUtil.getQueryMap(((Uri) (obj1)).getQuery());
        }
        obj1 = obj;
        if (map != null)
        {
            obj1 = obj;
            if (!map.containsKey("app"))
            {
                obj1 = (new StringBuilder()).append(((String) (obj))).append("&app=iting").toString();
            }
        }
        obj = obj1;
        if (map != null)
        {
            obj = obj1;
            if (!map.containsKey("version"))
            {
                obj = (new StringBuilder()).append(((String) (obj1))).append("&version=").append(((MyApplication)(MyApplication)getActivity().getApplication()).m()).toString();
            }
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    private void initListener()
    {
        title_bar.findViewById(0x7f0a007b).setOnClickListener(this);
        mBackPage.setOnClickListener(this);
        mForwardPage.setOnClickListener(this);
        mRefreshPage.setOnClickListener(this);
        registerListener();
        mWebSettings = mWebView.getSettings();
        mWebSettings.setUseWideViewPort(true);
        mWebSettings.setLoadWithOverviewMode(true);
        mWebSettings.setDomStorageEnabled(true);
        mWebSettings.setJavaScriptEnabled(true);
        Object obj = (new StringBuilder()).append(mWebSettings.getUserAgentString()).append(" kdtunion_iting/").append(((MyApplication)getActivity().getApplication()).m()).toString();
        mWebSettings.setUserAgentString(((String) (obj)));
        mWebSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        mWebSettings.setBlockNetworkLoads(false);
        mWebSettings.setAppCacheEnabled(true);
        obj = getActivity().getDir("cache", 0).getPath();
        mWebSettings.setAppCachePath(((String) (obj)));
        mWebSettings.setAppCacheMaxSize(0x800000L);
        mWebSettings.setAllowFileAccess(true);
        mChromeClient = new ChromeClient();
        mWebViewClient = new WebClient();
        mWebView.setWebChromeClient(mChromeClient);
        mWebView.setWebViewClient(mWebViewClient);
        mWebView.setDownloadListener(this);
        mWebView.addJavascriptInterface(new JSInterface(), "jscall");
        mKDTAction = new KDTAction(mWebView, getActivity());
        obj = mWebView;
        KDTAction kdtaction = mKDTAction;
        kdtaction.getClass();
        ((WebView) (obj)).addJavascriptInterface(new com.ximalaya.ting.android.kdt.KDTAction.KDTJsInterface(kdtaction, mCon), "JSInterface");
        obj = mWebView;
        kdtaction = mKDTAction;
        kdtaction.getClass();
        ((WebView) (obj)).addJavascriptInterface(new com.ximalaya.ting.android.kdt.KDTAction.KDTPayJSInterface(kdtaction), "android");
        mNoNetworkLayout.setOnClickListener(new _cls2());
        mDaShangAction = new a(getActivity(), mWebView);
    }

    private void initViews()
    {
        mWebView = (WebView)findViewById(0x7f0a0096);
        mBackPage = (ImageView)findViewById(0x7f0a00e2);
        mForwardPage = (ImageView)findViewById(0x7f0a0436);
        mRefreshPage = (ImageView)findViewById(0x7f0a0437);
        mProgressBar = (ProgressBar)findViewById(0x7f0a0093);
        title_bar = findViewById(0x7f0a00de);
        ((ImageView)title_bar.findViewById(0x7f0a007b)).setImageResource(0x7f020161);
    }

    private boolean isTaobaoAd()
    {
        return "http://ai.m.taobao.com/bu.html?id=1&pid=mm_96188024_8732596_29522856".equals(mRootUrl);
    }

    private void loadPage(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        } else
        {
            s = checkProtocol(s);
            mWebView.stopLoading();
            mProgressBar.setProgress(0);
            showNoNetworkLayout(false);
            mWebView.loadUrl(s);
            mKDTAction.setCurrentUrl(s);
            setTitleText(s);
            return;
        }
    }

    public static WebFragment newInstance(String s)
    {
        WebFragment webfragment = new WebFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ExtraUrl", s);
        webfragment.setArguments(bundle);
        return webfragment;
    }

    public static WebFragment newInstance(String s, boolean flag)
    {
        WebFragment webfragment = new WebFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ExtraUrl", s);
        bundle.putBoolean("show_title", flag);
        webfragment.setArguments(bundle);
        return webfragment;
    }

    private void openCamera(int i)
    {
        Intent intent;
        File file;
        intent = new Intent("android.media.action.IMAGE_CAPTURE");
        file = new File((new StringBuilder()).append(com.ximalaya.ting.android.a.af).append(File.separator).append(System.currentTimeMillis()).append(".jpg").toString());
        if (file.exists()) goto _L2; else goto _L1
_L1:
        file.getParentFile().mkdirs();
_L8:
        cameraUri = Uri.fromFile(file);
        intent.putExtra("output", cameraUri);
        if (i != 1) goto _L4; else goto _L3
_L3:
        startActivityForResult(intent, 1);
_L6:
        return;
_L2:
        if (file.exists())
        {
            file.delete();
        }
        continue; /* Loop/switch isn't completed */
_L4:
        if (i != 2) goto _L6; else goto _L5
_L5:
        startActivityForResult(intent, 4);
        return;
        if (true) goto _L8; else goto _L7
_L7:
    }

    private void registerListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            if (mOnPlayerStatusUpdateListener == null)
            {
                mOnPlayerStatusUpdateListener = new _cls3();
            }
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void showNoNetworkLayout(boolean flag)
    {
        byte byte0 = 8;
        Object obj = mNoNetworkLayout;
        int i;
        if (flag)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        ((View) (obj)).setVisibility(i);
        obj = mWebView;
        if (flag)
        {
            i = byte0;
        } else
        {
            i = 0;
        }
        ((WebView) (obj)).setVisibility(i);
    }

    private void showSelectDialog(final int type)
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add("\u4ECE\u76F8\u518C\u9009\u62E9");
        arraylist.add("\u62CD\u7167");
        if (mSelectImgDialog == null)
        {
            mSelectImgDialog = new MenuDialog(getActivity(), arraylist);
        } else
        {
            mSelectImgDialog.setSelections(arraylist);
        }
        mSelectImgDialog.setOnItemClickListener(new _cls4());
        mSelectImgDialog.setCanceledOnTouchOutside(true);
        mSelectImgDialog.setOnCancelListener(new _cls5());
        mSelectImgDialog.show();
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
    }

    private void uploadMsg(Uri uri)
    {
        if (uri == null) goto _L2; else goto _L1
_L1:
        Logger.d("uploadMsg", uri.toString());
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            mUploadMessage.onReceiveValue(new Uri[] {
                uri
            });
        } else
        {
            mUploadMessage.onReceiveValue(uri);
        }
_L4:
        mUploadMessage = null;
        return;
_L2:
        if (mUploadMessage != null)
        {
            mUploadMessage.onReceiveValue(null);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public a getDaShangAction()
    {
        return mDaShangAction;
    }

    public WebView getWebView()
    {
        return mWebView;
    }

    public boolean isPlaying(long l)
    {
        return PlayListControl.getPlayListManager().getCurSound() != null && l == PlayListControl.getPlayListManager().getCurSound().trackId;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        initViews();
        initListener();
        initData();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (i != 1) goto _L2; else goto _L1
_L1:
        if (mUploadMessage != null) goto _L4; else goto _L3
_L3:
        return;
_L4:
        if (j == -1)
        {
            doCompressAndUpload(cameraUri, true);
            return;
        } else
        {
            uploadMsg(null);
            return;
        }
_L2:
        if (i != 2)
        {
            break; /* Loop/switch isn't completed */
        }
        if (mUploadMessage != null)
        {
            if (j == -1 && intent != null)
            {
                doCompressAndUpload(getRealUri(intent.getData()), false);
                return;
            } else
            {
                uploadMsg(null);
                return;
            }
        }
        if (true) goto _L3; else goto _L5
_L5:
        if (i != 3)
        {
            break; /* Loop/switch isn't completed */
        }
        intent = UserInfoMannage.getInstance().getUser();
        if (intent != null)
        {
            mKDTAction.outputAppUserInfo(intent);
            return;
        }
        if (true) goto _L3; else goto _L6
_L6:
        if (i != 4)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == -1)
        {
            startPhotoZoom(cameraUri);
            return;
        }
        if (true) goto _L3; else goto _L7
_L7:
        if (i != 5)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (j == -1 && intent != null)
        {
            startPhotoZoom(intent.getData());
            return;
        }
        continue; /* Loop/switch isn't completed */
        if (i != 6) goto _L3; else goto _L8
_L8:
        if (j != -1)
        {
            continue; /* Loop/switch isn't completed */
        }
        intent = getTempFile(tempFileName);
        if (intent != null)
        {
            (new UpdateZoneLogoTask()).myexec(new Object[] {
                mUploadSourceType, intent
            });
            return;
        }
        continue; /* Loop/switch isn't completed */
        if (TextUtils.isEmpty(tempFileName)) goto _L3; else goto _L9
_L9:
        intent = new File(tempFileName);
        if (intent.exists())
        {
            intent.delete();
            return;
        }
        if (true) goto _L3; else goto _L10
_L10:
    }

    public boolean onBackPressed()
    {
        return onBackPressedFragment();
    }

    public boolean onBackPressedFragment()
    {
        if (mWebView != null) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        if (!isTaobaoAd())
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!mWebView.canGoBack()) goto _L1; else goto _L3
_L3:
        WebBackForwardList webbackforwardlist = mWebView.copyBackForwardList();
        String s = webbackforwardlist.getItemAtIndex(webbackforwardlist.getCurrentIndex() - 1).getUrl();
        mWebView.goBack();
        while (!TextUtils.isEmpty(s) && s.contains("s.click.taobao.com")) 
        {
            mWebView.goBack();
            if (mWebView.canGoBack())
            {
                s = webbackforwardlist.getItemAtIndex(webbackforwardlist.getCurrentIndex() - 1).getUrl();
            } else
            {
                s = null;
            }
        }
        return true;
        if (!mWebView.canGoBack()) goto _L1; else goto _L4
_L4:
        mWebView.goBack();
        return true;
    }

    public void onClick(View view)
    {
        int i = view.getId();
        if (i != mBackPage.getId()) goto _L2; else goto _L1
_L1:
        if (mWebView.canGoBack())
        {
            mWebView.goBack();
        }
_L4:
        return;
_L2:
        if (i != mForwardPage.getId())
        {
            break; /* Loop/switch isn't completed */
        }
        if (mWebView.canGoForward())
        {
            mWebView.goForward();
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (i == mRefreshPage.getId())
        {
            mWebView.stopLoading();
            mWebView.reload();
            return;
        }
        if (i == 0x7f0a007b)
        {
            removeFromStackTop();
            return;
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    public void onCreate(Bundle bundle)
    {
label0:
        {
            super.onCreate(bundle);
            Bundle bundle1 = getArguments();
            Object obj = null;
            if (bundle1 != null)
            {
                mShowTitle = bundle1.getBoolean("show_title", true);
            } else
            {
                mShowTitle = true;
            }
            bundle = obj;
            if (bundle1 != null)
            {
                bundle = obj;
                if (bundle1.containsKey("ExtraUrl"))
                {
                    bundle = bundle1.getString("ExtraUrl");
                }
            }
            if (bundle != null && bundle.contains("wemart"))
            {
                Logger.d("web", "\u8DF3\u8F6C\u5230wemart");
                bundle = new Intent(getActivity(), com/ximalaya/ting/android/wxapi/XMWXPayEntryActivity);
                bundle1.putBoolean("From_WeMart", true);
                bundle.putExtras(bundle1);
                Logger.d("web", "WebActivityNew Finish");
                getActivity().startActivity(bundle);
                if (getActivity() instanceof MainTabActivity2)
                {
                    break label0;
                }
                Logger.d("web", "instanceof MainTabActivity2");
                getActivity().finish();
            }
            return;
        }
        Logger.d("web", "finish()");
        finish();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        fragmentBaseContainerView = layoutinflater.inflate(0x7f0300fa, viewgroup, false);
        mNoNetworkLayout = fragmentBaseContainerView.findViewById(0x7f0a02d6);
        return fragmentBaseContainerView;
    }

    public void onDestroyView()
    {
        unRegisterListener();
        deleteTempImg();
        if (mWebView != null)
        {
            mWebView.loadUrl("about:blank");
        }
        super.onDestroyView();
    }

    public void onDownloadStart(String s, String s1, String s2, String s3, long l)
    {
        if (isAdded())
        {
            s1 = "";
            try
            {
                if (!TextUtils.isEmpty(s2))
                {
                    s1 = URLDecoder.decode(s2.substring(s2.indexOf("filename") + 10, s2.length() - 1), "utf-8");
                }
                s2 = new Intent(getActivity().getApplicationContext(), com/ximalaya/ting/android/library/service/DownloadService);
                s2.putExtra("download_url", s);
                s2.putExtra("file_name", s1);
                getActivity().getApplicationContext().startService(s2);
                if (s.equals(mWebView.getUrl()) || !mWebView.canGoBack())
                {
                    finish();
                    if (getActivity() instanceof WebActivityNew)
                    {
                        getActivity().finish();
                        return;
                    }
                }
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                return;
            }
        }
    }

    public void onPause()
    {
        if (mWebView != null)
        {
            mWebView.onPause();
        }
        super.onPause();
    }

    public void onResume()
    {
        mWebView.onResume();
        super.onResume();
        if (isAdded() && mActivity != null && ((MyApplication)mActivity.getApplication()).e == 1)
        {
            initData();
            LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
            if (logininfomodel != null)
            {
                mKDTAction.outputAppUserInfo(logininfomodel);
            }
            ((MyApplication)mActivity.getApplication()).e = 0;
        }
    }

    public void removeFromStackTop()
    {
        FragmentActivity fragmentactivity = getActivity();
        if (fragmentactivity instanceof MainTabActivity2)
        {
            ((MainTabActivity2)fragmentactivity).getManageFragment().removeTopFragment();
        } else
        if (fragmentactivity != null)
        {
            fragmentactivity.finish();
            return;
        }
    }

    public void startPhotoZoom(Uri uri)
    {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 640);
        intent.putExtra("outputY", 640);
        intent.putExtra("scale", true);
        tempFileName = (new StringBuilder()).append("upload_temp_").append(System.currentTimeMillis()).append(".jpg").toString();
        intent.putExtra("output", Uri.fromFile(getTempFile(tempFileName)));
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", android.graphics.Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        startActivityForResult(intent, 6);
    }










/*
    static String access$1502(WebFragment webfragment, String s)
    {
        webfragment.mCallbackName = s;
        return s;
    }

*/


/*
    static String access$1602(WebFragment webfragment, String s)
    {
        webfragment.mUploadSourceType = s;
        return s;
    }

*/



/*
    static Uri access$1802(WebFragment webfragment, Uri uri)
    {
        webfragment.tempImgUri = uri;
        return uri;
    }

*/









/*
    static ValueCallback access$402(WebFragment webfragment, ValueCallback valuecallback)
    {
        webfragment.mUploadMessage = valuecallback;
        return valuecallback;
    }

*/






/*
    static MenuDialog access$802(WebFragment webfragment, MenuDialog menudialog)
    {
        webfragment.mSelectImgDialog = menudialog;
        return menudialog;
    }

*/


    private class _cls6
        implements com.ximalaya.ting.android.util.BitmapUtils.CompressCallback
    {

        final WebFragment this$0;

        public void onFinished(final Uri result, final boolean canDelete)
        {
            if (!isAdded())
            {
                return;
            } else
            {
                class _cls1
                    implements Runnable
                {

                    final _cls6 this$1;
                    final boolean val$canDelete;
                    final Uri val$result;

                    public void run()
                    {
                        WebFragment webfragment = _fld0;
                        Uri uri;
                        if (canDelete)
                        {
                            uri = result;
                        } else
                        {
                            uri = null;
                        }
                        webfragment.tempImgUri = uri;
                        uploadMsg(result);
                    }

                _cls1()
                {
                    this$1 = _cls6.this;
                    canDelete = flag;
                    result = uri;
                    super();
                }
                }

                getActivity().runOnUiThread(new _cls1());
                return;
            }
        }

        _cls6()
        {
            this$0 = WebFragment.this;
            super();
        }
    }


    private class _cls7
        implements Runnable
    {

        final WebFragment this$0;
        final String val$callbackName;
        final String val$param;

        public void run()
        {
            String s = (new StringBuilder()).append("javascript:nativeCallBack.").append(callbackName).append("('").append(param).append("')").toString();
            mWebView.loadUrl(s);
        }

        _cls7()
        {
            this$0 = WebFragment.this;
            callbackName = s;
            param = s1;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final WebFragment this$0;

        public void onClick(View view)
        {
            view = new SimpleShareData();
            view.title = mWebView.getTitle();
            view.picUrl = mShareCoverPath;
            view.url = mWebView.getUrl();
            (new BaseShareDialog(getActivity(), 19, view)).show();
        }

        _cls1()
        {
            this$0 = WebFragment.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final WebFragment this$0;

        public void onClick(View view)
        {
            initData();
        }

        _cls2()
        {
            this$0 = WebFragment.this;
            super();
        }
    }


    private class _cls3 extends OnPlayerStatusUpdateListenerProxy
    {

        final WebFragment this$0;

        public void onPlayStateChange()
        {
            if (PlayListControl.getPlayListManager().getCurSound() != null && LocalMediaService.getInstance() != null)
            {
                long l = PlayListControl.getPlayListManager().getCurSound().trackId;
                boolean flag = LocalMediaService.getInstance().isPlaying();
                JSONObject jsonobject = new JSONObject();
                jsonobject.put("soundId", Long.valueOf(l));
                jsonobject.put("isPlaying", Boolean.valueOf(flag));
                mWebView.loadUrl((new StringBuilder()).append("javascript:nativeCall.onAudioStatusChange('").append(jsonobject.toJSONString()).append("')").toString());
            }
        }

        _cls3()
        {
            this$0 = WebFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.widget.AdapterView.OnItemClickListener
    {

        final WebFragment this$0;
        final int val$type;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            i;
            JVM INSTR tableswitch 0 1: default 24
        //                       0 88
        //                       1 44;
               goto _L1 _L2 _L3
_L1:
            mSelectImgDialog.dismiss();
            mSelectImgDialog = null;
            return;
_L3:
            if (Environment.getExternalStorageState().equals("mounted"))
            {
                openCamera(type);
            } else
            {
                Toast.makeText(mCon, "\u624B\u673A\u6CA1\u6709SD\u5361", 0).show();
            }
            continue; /* Loop/switch isn't completed */
_L2:
            chosePic(type);
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls4()
        {
            this$0 = WebFragment.this;
            type = i;
            super();
        }
    }


    private class _cls5
        implements android.content.DialogInterface.OnCancelListener
    {

        final WebFragment this$0;

        public void onCancel(DialogInterface dialoginterface)
        {
            if (mUploadMessage != null)
            {
                mUploadMessage.onReceiveValue(null);
                mUploadMessage = null;
            }
        }

        _cls5()
        {
            this$0 = WebFragment.this;
            super();
        }
    }

}
