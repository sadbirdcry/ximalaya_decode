// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.web;

import android.webkit.WebView;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.OnPlayerStatusUpdateListenerProxy;
import com.ximalaya.ting.android.service.play.PlayListControl;

// Referenced classes of package com.ximalaya.ting.android.fragment.web:
//            WebFragment

class UpdateListenerProxy extends OnPlayerStatusUpdateListenerProxy
{

    final WebFragment this$0;

    public void onPlayStateChange()
    {
        if (PlayListControl.getPlayListManager().getCurSound() != null && LocalMediaService.getInstance() != null)
        {
            long l = PlayListControl.getPlayListManager().getCurSound().trackId;
            boolean flag = LocalMediaService.getInstance().isPlaying();
            JSONObject jsonobject = new JSONObject();
            jsonobject.put("soundId", Long.valueOf(l));
            jsonobject.put("isPlaying", Boolean.valueOf(flag));
            WebFragment.access$000(WebFragment.this).loadUrl((new StringBuilder()).append("javascript:nativeCall.onAudioStatusChange('").append(jsonobject.toJSONString()).append("')").toString());
        }
    }

    ice()
    {
        this$0 = WebFragment.this;
        super();
    }
}
