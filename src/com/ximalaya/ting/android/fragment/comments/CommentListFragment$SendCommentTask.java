// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.comments;

import android.app.ProgressDialog;
import android.text.TextUtils;
import android.widget.LinearLayout;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.CommentListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.comment.CommentModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.emotion.EmotionSelector;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.comments:
//            CommentListFragment

class this._cls0 extends MyAsyncTask
{

    ProgressDialog pd;
    final CommentListFragment this$0;

    protected transient CommentModel doInBackground(String as[])
    {
        if (UserInfoMannage.getInstance().getUser() == null || Utilities.isBlank(CommentListFragment.access$300(CommentListFragment.this)))
        {
            return null;
        }
        String s = as[0];
        Object obj = as[1];
        String s1 = as[2];
        String s2 = as[3];
        String s3 = as[4];
        int i;
        if (!TextUtils.isEmpty(((CharSequence) (obj))) && !"null".equals(obj))
        {
            i = Integer.parseInt(((String) (obj)));
        } else
        {
            i = 1;
        }
        as = ApiUtil.getApiHost();
        if (i == 1)
        {
            as = (new StringBuilder()).append(as).append("mobile/comment/create").toString();
        } else
        {
            as = (new StringBuilder()).append(as).append("mobile/track/relay").toString();
        }
        obj = new RequestParams();
        ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append("").append(CommentListFragment.access$300(CommentListFragment.this)).toString());
        ((RequestParams) (obj)).put("content", s);
        if (Utilities.isNotBlank(s1))
        {
            ((RequestParams) (obj)).put("parentId", s1);
        }
        if (Utilities.isNotBlank(s3))
        {
            ((RequestParams) (obj)).put("second", s3);
        }
        if (Utilities.isNotBlank(s2))
        {
            ((RequestParams) (obj)).put("shareList", s2);
        }
        as = f.a().b(as, ((RequestParams) (obj)), CommentListFragment.access$000(CommentListFragment.this), CommentListFragment.access$600(CommentListFragment.this));
        Logger.d("resultJson:", as);
        if (!Utilities.isNotBlank(as))
        {
            break MISSING_BLOCK_LABEL_277;
        }
        as = (CommentModel)JSON.parseObject(as, com/ximalaya/ting/android/model/comment/CommentModel);
        return as;
        as;
        as.printStackTrace();
        return null;
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((String[])aobj);
    }

    protected void onPostExecute(CommentModel commentmodel)
    {
        if (CommentListFragment.access$5300(CommentListFragment.this) == null || !isAdded())
        {
            return;
        }
        if (pd != null)
        {
            pd.cancel();
            pd = null;
        }
        if (commentmodel == null)
        {
            showToast("\u56DE\u590D\u5931\u8D25");
            return;
        }
        if (commentmodel.ret != 0) goto _L2; else goto _L1
_L1:
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (CommentListFragment.access$3200(CommentListFragment.this) != 1) goto _L4; else goto _L3
_L3:
        showToast("\u56DE\u590D\u6210\u529F");
        if (soundinfo != null)
        {
            soundinfo.comments_counts = soundinfo.comments_counts + 1;
        }
        CommentListFragment.access$900(CommentListFragment.this).add(0, commentmodel);
        CommentListFragment.access$1100(CommentListFragment.this).notifyDataSetChanged();
        CommentListFragment.access$1300(CommentListFragment.this);
_L5:
        CommentListFragment.access$000(CommentListFragment.this).setText("");
        CommentListFragment.access$000(CommentListFragment.this).hideEmotionPanel();
        CommentListFragment.access$3100(CommentListFragment.this).setVisibility(8);
        return;
_L4:
        if (CommentListFragment.access$3200(CommentListFragment.this) == 2)
        {
            showToast("\u8F6C\u91C7\u6210\u529F");
            if (soundinfo != null)
            {
                soundinfo.shares_counts = soundinfo.shares_counts + 1;
            }
        }
        if (true) goto _L5; else goto _L2
_L2:
        showToast(commentmodel.msg);
        return;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((CommentModel)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        pd = new MyProgressDialog(CommentListFragment.access$5200(CommentListFragment.this));
        if (CommentListFragment.access$3200(CommentListFragment.this) != 1) goto _L2; else goto _L1
_L1:
        pd.setTitle("\u56DE\u590D");
        pd.setMessage("\u6B63\u5728\u5E2E\u60A8\u52AA\u529B\u56DE\u590D\u4E2D...");
_L4:
        pd.show();
        return;
_L2:
        if (CommentListFragment.access$3200(CommentListFragment.this) == 2)
        {
            pd.setTitle("\u8F6C\u91C7");
            pd.setMessage("\u6B63\u5728\u5E2E\u60A8\u52AA\u529B\u8F6C\u91C7\u4E2D...");
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    ()
    {
        this$0 = CommentListFragment.this;
        super();
    }
}
