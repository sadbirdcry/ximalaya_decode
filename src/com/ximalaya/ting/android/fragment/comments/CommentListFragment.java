// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.comments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.CommentListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.comment.CommentModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.setting.ShareSettingModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.emotion.EmotionSelector;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CommentListFragment extends BaseActivityLikeFragment
{
    private static final class FooterView extends Enum
    {

        private static final FooterView $VALUES[];
        public static final FooterView HIDE_ALL;
        public static final FooterView LOADING;
        public static final FooterView MORE;
        public static final FooterView NO_CONNECTION;
        public static final FooterView NO_DATA;

        public static FooterView valueOf(String s)
        {
            return (FooterView)Enum.valueOf(com/ximalaya/ting/android/fragment/comments/CommentListFragment$FooterView, s);
        }

        public static FooterView[] values()
        {
            return (FooterView[])$VALUES.clone();
        }

        static 
        {
            MORE = new FooterView("MORE", 0);
            LOADING = new FooterView("LOADING", 1);
            NO_CONNECTION = new FooterView("NO_CONNECTION", 2);
            HIDE_ALL = new FooterView("HIDE_ALL", 3);
            NO_DATA = new FooterView("NO_DATA", 4);
            $VALUES = (new FooterView[] {
                MORE, LOADING, NO_CONNECTION, HIDE_ALL, NO_DATA
            });
        }

        private FooterView(String s, int i)
        {
            super(s, i);
        }
    }

    class SendCommentTask extends MyAsyncTask
    {

        ProgressDialog pd;
        final CommentListFragment this$0;

        protected transient CommentModel doInBackground(String as[])
        {
            if (UserInfoMannage.getInstance().getUser() == null || Utilities.isBlank(trackId))
            {
                return null;
            }
            String s = as[0];
            Object obj = as[1];
            String s1 = as[2];
            String s2 = as[3];
            String s3 = as[4];
            int i;
            if (!TextUtils.isEmpty(((CharSequence) (obj))) && !"null".equals(obj))
            {
                i = Integer.parseInt(((String) (obj)));
            } else
            {
                i = 1;
            }
            as = ApiUtil.getApiHost();
            if (i == 1)
            {
                as = (new StringBuilder()).append(as).append("mobile/comment/create").toString();
            } else
            {
                as = (new StringBuilder()).append(as).append("mobile/track/relay").toString();
            }
            obj = new RequestParams();
            ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append("").append(trackId).toString());
            ((RequestParams) (obj)).put("content", s);
            if (Utilities.isNotBlank(s1))
            {
                ((RequestParams) (obj)).put("parentId", s1);
            }
            if (Utilities.isNotBlank(s3))
            {
                ((RequestParams) (obj)).put("second", s3);
            }
            if (Utilities.isNotBlank(s2))
            {
                ((RequestParams) (obj)).put("shareList", s2);
            }
            as = f.a().b(as, ((RequestParams) (obj)), mEmotionSelector, myListView);
            Logger.d("resultJson:", as);
            if (!Utilities.isNotBlank(as))
            {
                break MISSING_BLOCK_LABEL_277;
            }
            as = (CommentModel)JSON.parseObject(as, com/ximalaya/ting/android/model/comment/CommentModel);
            return as;
            as;
            as.printStackTrace();
            return null;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(CommentModel commentmodel)
        {
            if (
// JavaClassFileOutputException: get_constant: invalid tag

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((CommentModel)obj);
        }

        protected void onPreExecute()
        {
            onPreExecute();
            pd = new MyProgressDialog(
// JavaClassFileOutputException: get_constant: invalid tag

        SendCommentTask()
        {
            this$0 = CommentListFragment.this;
            super();
        }
    }


    private static final int GO_BIND_WEIBO_REQUESTCODE = 1114;
    private static final int GO_SELECT_CAREPERSON_REQUESTCODE = 175;
    private static final int MAX_COMMENT_CHAR_COUNT = 140;
    private final int GO_SEND_COMMENT = 1379;
    private String array[] = {
        "\u67E5\u770B\u8D44\u6599", "\u56DE\u590D"
    };
    private int commentIndex;
    private int lastBindWeibo;
    private List list;
    private CommentListAdapter listAdapter;
    private boolean loadingNextPage;
    private ImageView mBindQzoneImg;
    private ImageView mBindRennImg;
    private ImageView mBindSinaImg;
    private ImageView mBindTqqImg;
    private ImageView mCarePeopleImg;
    private TextView mCommentTips;
    private int mCommentType;
    private MyAsyncTask mDataLoadTask;
    private EmotionSelector mEmotionSelector;
    private LinearLayout mEmptyView;
    private RelativeLayout mFooterViewLoading;
    private boolean mIsBindQQ;
    private boolean mIsBindQZone;
    private ImageView mIsBindQzoneImg;
    private boolean mIsBindRenn;
    private ImageView mIsBindRennImg;
    private boolean mIsBindSina;
    private ImageView mIsBindSinaImg;
    private ImageView mIsBindTqqImg;
    private boolean mIsLoadShareSetting;
    private boolean mIsShareQQ;
    private boolean mIsShareQZone;
    private boolean mIsShareRenn;
    private boolean mIsShareSina;
    private LoadShareSetting mLoadShareSetting;
    private SendCommentTask mSendCommentTask;
    private List mShareSettingList;
    private LinearLayout mSyncBar;
    private PullToRefreshListView myListView;
    private ViewGroup noNetLayout;
    private int pageId;
    private int pageSize;
    private int totalCount;
    private String trackId;

    public CommentListFragment()
    {
        totalCount = 0;
        pageId = 1;
        pageSize = 15;
        mCommentType = 1;
        mIsBindQQ = false;
        mIsBindSina = false;
        mIsBindQZone = false;
        mIsBindRenn = false;
        mIsShareQQ = false;
        mIsShareSina = false;
        mIsShareQZone = false;
        mIsShareRenn = false;
        loadingNextPage = false;
        commentIndex = 0;
        mIsLoadShareSetting = false;
    }

    private void doShareQQ(boolean flag)
    {
        if (flag)
        {
            mIsShareQQ = true;
            mBindTqqImg.setImageResource(0x7f02058c);
            mIsBindTqqImg.setVisibility(0);
            return;
        } else
        {
            mIsShareQQ = false;
            mBindTqqImg.setImageResource(0x7f02058d);
            mIsBindTqqImg.setVisibility(4);
            return;
        }
    }

    private void doShareQzone(boolean flag)
    {
        if (flag)
        {
            mIsShareQZone = true;
            mBindQzoneImg.setImageResource(0x7f02045d);
            mIsBindQzoneImg.setVisibility(0);
            return;
        } else
        {
            mIsShareQZone = false;
            mBindQzoneImg.setImageResource(0x7f02045e);
            mIsBindQzoneImg.setVisibility(4);
            return;
        }
    }

    private void doShareRenn(boolean flag)
    {
        if (flag)
        {
            mIsShareRenn = true;
            mBindRennImg.setImageResource(0x7f0204d8);
            mIsBindRennImg.setVisibility(0);
            return;
        } else
        {
            mIsShareRenn = false;
            mBindRennImg.setImageResource(0x7f0204d9);
            mIsBindRennImg.setVisibility(4);
            return;
        }
    }

    private void doShareSina(boolean flag)
    {
        if (flag)
        {
            mIsShareSina = true;
            mBindSinaImg.setImageResource(0x7f020556);
            mIsBindSinaImg.setVisibility(0);
            return;
        } else
        {
            mIsShareSina = false;
            mBindSinaImg.setImageResource(0x7f020557);
            mIsBindSinaImg.setVisibility(4);
            return;
        }
    }

    private void getParentInfo()
    {
        Bundle bundle = getArguments();
        if (bundle != null)
        {
            trackId = bundle.getString("trackId");
        }
    }

    private void initData()
    {
        myListView.toRefreshing();
        loadDataListData(myListView);
        showEmptyView();
    }

    private void initListener()
    {
        mBindSinaImg.setOnClickListener(new _cls3());
        mBindTqqImg.setOnClickListener(new _cls4());
        mBindQzoneImg.setOnClickListener(new _cls5());
        mBindRennImg.setOnClickListener(new _cls6());
        mCarePeopleImg.setOnClickListener(new _cls7());
        mEmotionSelector.setOnInputBoxFocusChangeListener(new _cls8());
        mEmotionSelector.setOnSendButtonClickListener(new _cls9());
        mEmotionSelector.setOnEmotionTextChange(new _cls10());
        noNetLayout.setOnClickListener(new _cls11());
        mFooterViewLoading.setOnClickListener(new _cls12());
        myListView.setOnScrollListener(new _cls13());
        myListView.setOnItemClickListener(new _cls14());
        myListView.setOnRefreshListener(new _cls15());
    }

    private void initLoginUserBindData()
    {
        Object obj;
        if (UserInfoMannage.hasLogined())
        {
            if (((LoginInfoModel) (obj = UserInfoMannage.getInstance().getUser())).bindStatus != null && ((LoginInfoModel) (obj)).bindStatus != null)
            {
                obj = ((LoginInfoModel) (obj)).bindStatus.iterator();
                while (((Iterator) (obj)).hasNext()) 
                {
                    ThirdPartyUserInfo thirdpartyuserinfo = (ThirdPartyUserInfo)((Iterator) (obj)).next();
                    if (!thirdpartyuserinfo.isExpired())
                    {
                        if (thirdpartyuserinfo.getIdentity().equals("1"))
                        {
                            mIsBindSina = true;
                        } else
                        if (thirdpartyuserinfo.getIdentity().equals("2"))
                        {
                            mIsBindQQ = true;
                        } else
                        if (thirdpartyuserinfo.getIdentity().equals("3"))
                        {
                            mIsBindRenn = true;
                        }
                    }
                }
            }
        }
    }

    private void initUi()
    {
        setTitleText(getString(0x7f0900f8));
        myListView = (PullToRefreshListView)findViewById(0x7f0a01e6);
        list = new ArrayList();
        listAdapter = new CommentListAdapter(mActivity, list, myListView);
        myListView.setAdapter(listAdapter);
        noNetLayout = (ViewGroup)findViewById(0x7f0a035b);
        mEmptyView = (LinearLayout)LayoutInflater.from(mActivity).inflate(0x7f03007c, myListView, false);
        ((TextView)mEmptyView.findViewById(0x7f0a011a)).setText("\u4EB2~ \u8FD9\u6761\u58F0\u97F3\u8FD8\u6CA1\u6709\u8BC4\u8BBA\u54E6");
        ((TextView)mEmptyView.findViewById(0x7f0a0231)).setText("\u8FD8\u7B49\u4EC0\u4E48\uFF0C\u8D76\u7D27\u53BB\u62A2\u6C99\u53D1\u5427");
        Button button = (Button)mEmptyView.findViewById(0x7f0a0232);
        button.setText("\u53BB\u8BC4\u8BBA");
        button.setOnClickListener(new _cls1());
        myListView.addFooterView(mEmptyView);
        mFooterViewLoading = (RelativeLayout)LayoutInflater.from(mCon).inflate(0x7f0301fb, null);
        myListView.addFooterView(mFooterViewLoading);
        showFooterView(FooterView.HIDE_ALL);
        mEmotionSelector = (EmotionSelector)findViewById(0x7f0a0199);
        mSyncBar = (LinearLayout)findViewById(0x7f0a01e8);
        mBindSinaImg = (ImageView)mSyncBar.findViewById(0x7f0a018f);
        mIsBindSinaImg = (ImageView)mSyncBar.findViewById(0x7f0a0190);
        mBindTqqImg = (ImageView)mSyncBar.findViewById(0x7f0a0191);
        mIsBindTqqImg = (ImageView)mSyncBar.findViewById(0x7f0a0192);
        mBindQzoneImg = (ImageView)mSyncBar.findViewById(0x7f0a0193);
        mIsBindQzoneImg = (ImageView)mSyncBar.findViewById(0x7f0a0194);
        mBindRennImg = (ImageView)mSyncBar.findViewById(0x7f0a0195);
        mIsBindRennImg = (ImageView)mSyncBar.findViewById(0x7f0a0196);
        mCommentTips = (TextView)mSyncBar.findViewById(0x7f0a0198);
        mCarePeopleImg = (ImageView)mSyncBar.findViewById(0x7f0a0197);
    }

    private void loadDataListData(final View view)
    {
        if (ToolUtil.isConnectToNetwork(mCon))
        {
            mDataLoadTask = (new _cls2()).myexec(new Void[0]);
            return;
        } else
        {
            myListView.onRefreshComplete();
            showFooterView(FooterView.NO_CONNECTION);
            showReloadLayout(true);
            return;
        }
    }

    private void loadDefaultShareSetting()
    {
        parseData(SharedPreferencesUtil.getInstance(mActivity).getString("share_setting"));
        refreshShareSetting();
    }

    private boolean moreDataAvailable()
    {
        return pageId * pageSize < totalCount;
    }

    private void parseData(String s)
    {
        if (s != null)
        {
            try
            {
                mShareSettingList = JSON.parseArray(JSON.parseObject(s).get("data").toString(), com/ximalaya/ting/android/model/setting/ShareSettingModel);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return;
            }
        } else
        {
            mShareSettingList = new ArrayList();
            s = new ShareSettingModel();
            s.thirdpartyId = 0;
            s.relay = true;
            s.webAlbum = true;
            s.webComment = true;
            s.webTrack = true;
            mShareSettingList.add(s);
            return;
        }
    }

    private void refreshShareSetting()
    {
        if (mShareSettingList != null && mShareSettingList.size() > 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Iterator iterator = mShareSettingList.iterator();
_L6:
        if (!iterator.hasNext()) goto _L1; else goto _L3
_L3:
        ShareSettingModel sharesettingmodel;
        boolean flag;
        sharesettingmodel = (ShareSettingModel)iterator.next();
        switch (mCommentType)
        {
        default:
            flag = sharesettingmodel.relay;
            break;

        case 1: // '\001'
            break MISSING_BLOCK_LABEL_119;
        }
_L4:
        switch (sharesettingmodel.thirdpartyId)
        {
        case 1: // '\001'
            doShareSina(flag);
            break;

        case 2: // '\002'
            if (sharesettingmodel.thirdpartyName.equals("tQQ"))
            {
                doShareQQ(flag);
            }
            if (sharesettingmodel.thirdpartyName.equals("qzone"))
            {
                doShareQzone(flag);
            }
            break;

        case 3: // '\003'
            doShareRenn(flag);
            break;
        }
        continue; /* Loop/switch isn't completed */
        flag = sharesettingmodel.webComment;
          goto _L4
        if (true) goto _L6; else goto _L5
_L5:
    }

    private void showEmptyView()
    {
        if (list.size() == 0)
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(0);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(0);
            mEmptyView.findViewById(0x7f0a0232).setVisibility(0);
            mEmptyView.setVisibility(0);
            return;
        } else
        {
            mEmptyView.findViewById(0x7f0a011a).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0231).setVisibility(8);
            mEmptyView.findViewById(0x7f0a0232).setVisibility(8);
            mEmptyView.setVisibility(8);
            return;
        }
    }

    private void showFooterView(FooterView footerview)
    {
        myListView.setFooterDividersEnabled(false);
        mFooterViewLoading.setVisibility(0);
        if (footerview == FooterView.MORE)
        {
            mFooterViewLoading.setClickable(true);
            mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
            ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
            mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
        } else
        {
            if (footerview == FooterView.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerview == FooterView.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == FooterView.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerview == FooterView.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }

    private void showReloadLayout(boolean flag)
    {
        if (flag && (list == null || list.size() == 0))
        {
            noNetLayout.setVisibility(0);
            showFooterView(FooterView.HIDE_ALL);
            return;
        } else
        {
            noNetLayout.setVisibility(8);
            return;
        }
    }

    public void onActivityCreated(Bundle bundle)
    {
        onActivityCreated(bundle);
        getParentInfo();
        initUi();
        initData();
        initListener();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        if (i != 1114) goto _L2; else goto _L1
_L1:
        if (j != 0) goto _L4; else goto _L3
_L3:
        if (lastBindWeibo != 12) goto _L6; else goto _L5
_L5:
        mIsBindSina = true;
_L9:
        if (mLoadShareSetting == null || mLoadShareSetting.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mLoadShareSetting = new LoadShareSetting();
            mLoadShareSetting.myexec(new Object[0]);
        }
_L4:
        return;
_L6:
        if (lastBindWeibo == 13)
        {
            mIsBindQQ = true;
        } else
        if (lastBindWeibo == 14)
        {
            mIsBindRenn = true;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        if (i != 175 || j != 86) goto _L4; else goto _L7
_L7:
        mEmotionSelector.setText((new StringBuilder()).append("@").append(intent.getStringExtra("name")).append(":").toString());
        return;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public boolean onBackPressed()
    {
        if (mSyncBar.getVisibility() == 0 || mEmotionSelector.getEmotionPanelStatus() == 0)
        {
            mSyncBar.setVisibility(8);
            mEmotionSelector.hideEmotionPanel();
            return true;
        } else
        {
            return onBackPressed();
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030063, null);
        return fragmentBaseContainerView;
    }

    public void onPause()
    {
        onPause();
        showPlayButton();
    }

    public void onResume()
    {
        onResume();
        myListView.onRefresh();
        hidePlayButton();
    }





/*
    static boolean access$102(CommentListFragment commentlistfragment, boolean flag)
    {
        commentlistfragment.loadingNextPage = flag;
        return flag;
    }

*/










/*
    static int access$1802(CommentListFragment commentlistfragment, int i)
    {
        commentlistfragment.lastBindWeibo = i;
        return i;
    }

*/


















/*
    static int access$3202(CommentListFragment commentlistfragment, int i)
    {
        commentlistfragment.mCommentType = i;
        return i;
    }

*/





/*
    static LoadShareSetting access$3502(CommentListFragment commentlistfragment, LoadShareSetting loadsharesetting)
    {
        commentlistfragment.mLoadShareSetting = loadsharesetting;
        return loadsharesetting;
    }

*/




/*
    static SendCommentTask access$3702(CommentListFragment commentlistfragment, SendCommentTask sendcommenttask)
    {
        commentlistfragment.mSendCommentTask = sendcommenttask;
        return sendcommenttask;
    }

*/






/*
    static int access$402(CommentListFragment commentlistfragment, int i)
    {
        commentlistfragment.pageId = i;
        return i;
    }

*/


/*
    static int access$408(CommentListFragment commentlistfragment)
    {
        int i = commentlistfragment.pageId;
        commentlistfragment.pageId = i + 1;
        return i;
    }

*/








/*
    static boolean access$4702(CommentListFragment commentlistfragment, boolean flag)
    {
        commentlistfragment.mIsLoadShareSetting = flag;
        return flag;
    }

*/











/*
    static int access$702(CommentListFragment commentlistfragment, int i)
    {
        commentlistfragment.totalCount = i;
        return i;
    }

*/









    private class _cls9
        implements com.ximalaya.ting.android.view.emotion.EmotionSelector.OnSendButtonClickListener
    {

        final CommentListFragment this$0;

        public void onClick(View view, CharSequence charsequence)
        {
            view = charsequence.toString();
            if (mCommentType == 1 && TextUtils.isEmpty(view))
            {
                showToast("\u8BF7\u8F93\u5165\u8BC4\u8BBA");
            } else
            if (ToolUtil.isConnectToNetwork(mCon))
            {
                if (mSendCommentTask == null || mSendCommentTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
                {
                    mSendCommentTask = new SendCommentTask();
                    if (mCommentType == 1)
                    {
                        charsequence = new StringBuilder();
                        if (mIsShareQQ)
                        {
                            charsequence.append("tQQ");
                        }
                        if (mIsShareQZone)
                        {
                            if (!TextUtils.isEmpty(charsequence.toString()) && !charsequence.toString().endsWith(","))
                            {
                                charsequence.append(",");
                            }
                            charsequence.append("qzone");
                        }
                        if (mIsShareSina)
                        {
                            if (!TextUtils.isEmpty(charsequence.toString()) && !charsequence.toString().endsWith(","))
                            {
                                charsequence.append(",");
                            }
                            charsequence.append("tSina");
                        }
                        if (mIsShareRenn)
                        {
                            if (!TextUtils.isEmpty(charsequence.toString()) && !charsequence.toString().endsWith(","))
                            {
                                charsequence.append(",");
                            }
                            charsequence.append("renn");
                        }
                        mSendCommentTask.myexec(new String[] {
                            view, "1", null, charsequence.toString(), null
                        });
                        return;
                    } else
                    {
                        mSendCommentTask.myexec(new String[] {
                            view, "2", null, null, null
                        });
                        return;
                    }
                }
            } else
            {
                showToast("\u6CA1\u6709\u53EF\u7528\u7F51\u7EDC");
                return;
            }
        }

        _cls9()
        {
            this$0 = CommentListFragment.this;
            Object();
        }
    }



    private class _cls11
        implements android.view.View.OnClickListener
    {

        final CommentListFragment this$0;

        public void onClick(View view)
        {
            myListView.toRefreshing();
            loadDataListData(noNetLayout);
        }

        _cls11()
        {
            this$0 = CommentListFragment.this;
            Object();
        }
    }


    private class _cls12
        implements android.view.View.OnClickListener
    {

        final CommentListFragment this$0;

        public void onClick(View view)
        {
            if (!loadingNextPage)
            {
                showFooterView(FooterView.LOADING);
                loadDataListData(mFooterViewLoading);
            }
        }

        _cls12()
        {
            this$0 = CommentListFragment.this;
            Object();
        }
    }


    private class _cls13
        implements android.widget.AbsListView.OnScrollListener
    {

        final CommentListFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
            myListView.onScroll(abslistview, i, j, k);
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
            if (i == 0)
            {
                i = abslistview.getCount();
                if (i > 5)
                {
                    i -= 5;
                } else
                {
                    i--;
                }
                if (abslistview.getLastVisiblePosition() > i && (pageId - 1) * pageSize < totalCount && (mDataLoadTask == null || mDataLoadTask.getStatus() != android.os.AsyncTask.Status.RUNNING) && !loadingNextPage)
                {
                    showFooterView(FooterView.LOADING);
                    loadDataListData(myListView);
                }
            }
        }

        _cls13()
        {
            this$0 = CommentListFragment.this;
            Object();
        }
    }



    private class _cls15
        implements com.ximalaya.ting.android.view.listview.PullToRefreshListView.OnRefreshListener
    {

        final CommentListFragment this$0;

        public void onRefresh()
        {
            if (!loadingNextPage)
            {
                pageId = 1;
                loadDataListData(myListView);
            }
        }

        _cls15()
        {
            this$0 = CommentListFragment.this;
            Object();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final CommentListFragment this$0;

        public void onClick(View view)
        {
            if (mEmotionSelector != null)
            {
                mEmotionSelector.appendText("");
            }
        }

        _cls1()
        {
            this$0 = CommentListFragment.this;
            Object();
        }
    }


    private class _cls2 extends MyAsyncTask
    {

        final CommentListFragment this$0;
        final View val$view;

        protected transient CommentModelList doInBackground(Void avoid[])
        {
label0:
            {
                avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/track/comment").toString();
                Object obj = new RequestParams();
                ((RequestParams) (obj)).put("trackId", trackId);
                ((RequestParams) (obj)).put("pageId", (new StringBuilder()).append(pageId).append("").toString());
                ((RequestParams) (obj)).put("pageSize", (new StringBuilder()).append(pageSize).append("").toString());
                obj = f.a().a(avoid, ((RequestParams) (obj)), view, myListView);
                Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
                try
                {
                    avoid = JSON.parseObject(((String) (obj)));
                    if (avoid.getIntValue("ret") != 0)
                    {
                        break label0;
                    }
                    obj = (CommentModelList)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/comment/CommentModelList);
                    if (totalCount == 0)
                    {
                        totalCount = avoid.getIntValue("maxPageId") * pageSize;
                    }
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
                    return null;
                }
                return ((CommentModelList) (obj));
            }
            return null;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(CommentModelList commentmodellist)
        {
            if (
// JavaClassFileOutputException: get_constant: invalid tag

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((CommentModelList)obj);
        }

        protected void onPreExecute()
        {
            loadingNextPage = true;
            showReloadLayout(false);
        }

        _cls2()
        {
            this$0 = CommentListFragment.this;
            view = view1;
            MyAsyncTask();
        }
    }

}
