// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.comments;

import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.CommentListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.comment.CommentModelList;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.comments:
//            CommentListFragment

class val.view extends MyAsyncTask
{

    final CommentListFragment this$0;
    final View val$view;

    protected transient CommentModelList doInBackground(Void avoid[])
    {
label0:
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/track/comment").toString();
            Object obj = new RequestParams();
            ((RequestParams) (obj)).put("trackId", CommentListFragment.access$300(CommentListFragment.this));
            ((RequestParams) (obj)).put("pageId", (new StringBuilder()).append(CommentListFragment.access$400(CommentListFragment.this)).append("").toString());
            ((RequestParams) (obj)).put("pageSize", (new StringBuilder()).append(CommentListFragment.access$500(CommentListFragment.this)).append("").toString());
            obj = f.a().a(avoid, ((RequestParams) (obj)), val$view, CommentListFragment.access$600(CommentListFragment.this));
            Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
            try
            {
                avoid = JSON.parseObject(((String) (obj)));
                if (avoid.getIntValue("ret") != 0)
                {
                    break label0;
                }
                obj = (CommentModelList)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/comment/CommentModelList);
                if (CommentListFragment.access$700(CommentListFragment.this) == 0)
                {
                    CommentListFragment.access$702(CommentListFragment.this, avoid.getIntValue("maxPageId") * CommentListFragment.access$500(CommentListFragment.this));
                }
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(avoid.getMessage()).append(Logger.getLineInfo()).toString());
                return null;
            }
            return ((CommentModelList) (obj));
        }
        return null;
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(CommentModelList commentmodellist)
    {
        if (CommentListFragment.access$800(CommentListFragment.this) != null && isAdded())
        {
            CommentListFragment.access$102(CommentListFragment.this, false);
            CommentListFragment.access$600(CommentListFragment.this).onRefreshComplete();
            if (commentmodellist == null)
            {
                CommentListFragment.access$200(CommentListFragment.this, true);
                if (CommentListFragment.access$900(CommentListFragment.this).size() > 0)
                {
                    CommentListFragment.access$1000(CommentListFragment.this, oterView.NO_DATA);
                    return;
                }
            } else
            {
                if (CommentListFragment.access$400(CommentListFragment.this) == 1)
                {
                    CommentListFragment.access$900(CommentListFragment.this).clear();
                    CommentListFragment.access$900(CommentListFragment.this).addAll(commentmodellist.list);
                    CommentListFragment.access$1100(CommentListFragment.this).notifyDataSetChanged();
                } else
                {
                    CommentListFragment.access$900(CommentListFragment.this).addAll(commentmodellist.list);
                    CommentListFragment.access$1100(CommentListFragment.this).notifyDataSetChanged();
                }
                if (CommentListFragment.access$1200(CommentListFragment.this))
                {
                    CommentListFragment.access$1000(CommentListFragment.this, oterView.MORE);
                } else
                {
                    CommentListFragment.access$1000(CommentListFragment.this, oterView.HIDE_ALL);
                }
                int _tmp = CommentListFragment.access$408(CommentListFragment.this);
                CommentListFragment.access$200(CommentListFragment.this, false);
                CommentListFragment.access$1300(CommentListFragment.this);
                return;
            }
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((CommentModelList)obj);
    }

    protected void onPreExecute()
    {
        CommentListFragment.access$102(CommentListFragment.this, true);
        CommentListFragment.access$200(CommentListFragment.this, false);
    }

    oterView()
    {
        this$0 = final_commentlistfragment;
        val$view = View.this;
        super();
    }
}
