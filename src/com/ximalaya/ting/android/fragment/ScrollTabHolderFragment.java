// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.widget.AbsListView;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseFragment, ScrollTabHolder

public abstract class ScrollTabHolderFragment extends BaseFragment
    implements ScrollTabHolder
{

    protected ScrollTabHolder mScrollTabHolder;

    public ScrollTabHolderFragment()
    {
    }

    public void onScroll(AbsListView abslistview, int i, int j, int k, int l)
    {
    }

    public void setScrollTabHolder(ScrollTabHolder scrolltabholder)
    {
        mScrollTabHolder = scrolltabholder;
    }
}
