// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.setting.SettingActivity;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.album.LocalCollectAlbumUploader;
import com.ximalaya.ting.android.model.auth.AuthInfo;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.fragment:
//            BaseLoginFragment

public static class mFinishActivity extends MyAsyncTask
{

    AuthInfo authInfo;
    private Bundle bundle;
    int loginFlag;
    private Activity mActivity;
    private boolean mFinishActivity;
    String name;
    String passwd;
    ProgressDialog pd;

    protected transient LoginInfoModel doInBackground(Object aobj[])
    {
        int i = ((Integer)aobj[0]).intValue();
        if (i == 0)
        {
            name = (String)aobj[1];
            passwd = (String)aobj[2];
            return CommonRequest.doLogin(mActivity, i, name, passwd, null);
        } else
        {
            authInfo = (AuthInfo)aobj[1];
            return CommonRequest.doLogin(mActivity, i, null, null, authInfo);
        }
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onPostExecute(LoginInfoModel logininfomodel)
    {
        if (mActivity != null && !mActivity.isFinishing())
        {
            pd.cancel();
            if (logininfomodel != null)
            {
                if (logininfomodel.ret == 0)
                {
                    UserInfoMannage.getInstance().setUser(logininfomodel);
                    if (logininfomodel.isFirst)
                    {
                        (new LocalCollectAlbumUploader(mActivity.getApplicationContext())).myexec(new Void[0]);
                    }
                    ((MyApplication)mActivity.getApplication()).e = 1;
                    Object obj = ScoreManage.getInstance(mActivity.getApplicationContext());
                    if (obj != null)
                    {
                        ((ScoreManage) (obj)).initBehaviorScore();
                        ((ScoreManage) (obj)).updateScore();
                    }
                    Intent intent = new Intent(mActivity, com/ximalaya/ting/android/activity/MainTabActivity2);
                    obj = null;
                    boolean flag;
                    if (bundle != null)
                    {
                        if (bundle.containsKey("ExtraUrl"))
                        {
                            obj = bundle.getString("ExtraUrl");
                        }
                        flag = bundle.getBoolean("login_from_setting", false);
                    } else
                    {
                        obj = null;
                        flag = false;
                    }
                    if (logininfomodel.isFirst)
                    {
                        if (logininfomodel.loginFromId == 1)
                        {
                            (new ed(mActivity)).myexec(new String[] {
                                authInfo.access_token
                            });
                        }
                    } else
                    if (flag)
                    {
                        intent.setClass(mActivity, com/ximalaya/ting/android/activity/setting/SettingActivity);
                    } else
                    {
                        if (!TextUtils.isEmpty(((CharSequence) (obj))))
                        {
                            mActivity.finish();
                            return;
                        }
                        intent.putExtra("isLogin", true);
                    }
                    mActivity.startActivity(intent);
                } else
                {
                    Toast.makeText(mActivity, (new StringBuilder()).append("\u767B\u5F55\u5931\u8D25\uFF0C").append(logininfomodel.msg).toString(), 1).show();
                }
            } else
            {
                Toast.makeText(mActivity, "\u7F51\u7EDC\u8D85\u65F6\uFF0C\u8BF7\u7A0D\u5019\u518D\u8BD5\uFF01", 1).show();
            }
            if (mFinishActivity && mActivity != null)
            {
                mActivity.finish();
                return;
            }
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((LoginInfoModel)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        if (mActivity == null || mActivity.isFinishing())
        {
            return;
        }
        if (pd == null)
        {
            pd = new MyProgressDialog(mActivity);
        } else
        {
            pd.cancel();
        }
        pd.setTitle("\u767B\u5F55");
        pd.setMessage("\u6B63\u5728\u767B\u5F55\uFF0C\u8BF7\u7A0D\u540E...");
        pd.show();
    }

    public ed(Activity activity, Bundle bundle1)
    {
        this(activity, bundle1, false);
    }

    public <init>(Activity activity, Bundle bundle1, boolean flag)
    {
        mFinishActivity = false;
        pd = null;
        mActivity = activity;
        bundle = bundle1;
        mFinishActivity = flag;
    }
}
