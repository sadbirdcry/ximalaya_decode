// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment;

import android.widget.AbsListView;
import android.widget.ListView;

public interface ScrollTabHolder
{

    public abstract void adjustScroll(int i);

    public abstract ListView getListView();

    public abstract void onScroll(AbsListView abslistview, int i, int j, int k, int l);
}
