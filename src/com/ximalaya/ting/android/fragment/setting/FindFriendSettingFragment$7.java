// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.adapter.setting.FindFriendSettingAdapter;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.model.personal_setting.FindFriendModel;
import com.ximalaya.ting.android.util.DataCollectUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.setting:
//            FindFriendSettingFragment

class this._cls0
    implements android.widget.ner
{

    final FindFriendSettingFragment this$0;

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        if (i != 0)
        {
            adapterview = FindFriendSettingFragment.access$700(FindFriendSettingFragment.this).getItem(i - 1);
            if (((FindFriendModel) (adapterview)).isRealData)
            {
                Bundle bundle = new Bundle();
                bundle.putLong("toUid", Long.valueOf(((FindFriendModel) (adapterview)).uid).longValue());
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
            }
        }
    }

    ()
    {
        this$0 = FindFriendSettingFragment.this;
        super();
    }
}
