// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.setting;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.BitmapFactory;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.fragment.setting:
//            FindFriendSettingFragment

class buttonIndex extends MyAsyncTask
{

    int buttonIndex;
    ProgressDialog mProgressDialog;
    final FindFriendSettingFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected transient String doInBackground(Object aobj[])
    {
        buttonIndex = ((Integer)aobj[0]).intValue();
        Object obj = new WXWebpageObject();
        obj.webpageUrl = "http://m.ximalaya.com/?src=findfriend_invite_weixin";
        aobj = new WXMediaMessage();
        aobj.title = "\u559C\u9A6C\u62C9\u96C5";
        if (buttonIndex == 0)
        {
            aobj.description = "\u73A9\u8F6C\u559C\u9A6C\u62C9\u96C5\uFF0C\u5185\u5BB9\u4E30\u5BCC\u7684\u624B\u673A\u968F\u8EAB\u542C\uFF0C\u60F3\u542C\u5565\u5565\u90FD\u6709\uFF0C\u8D76\u7D27\u6765\u8BD5\u8BD5\u5427\uFF01";
        } else
        {
            aobj.description = "\u559C\u9A6C\u62C9\u96C5";
        }
        aobj.thumbData = ToolUtil.imageZoom32(BitmapFactory.decodeResource(getResources(), 0x7f020598));
        aobj.mediaObject = ((com.tencent.mm.sdk.modelmsg.tResources) (obj));
        obj = new com.tencent.mm.sdk.modelmsg.tResources();
        obj.tResources = ((WXMediaMessage) (aobj));
        if (buttonIndex == 0)
        {
            obj.buttonIndex = 0;
        } else
        {
            obj.buttonIndex = 1;
        }
        aobj = WXAPIFactory.createWXAPI(FindFriendSettingFragment.access$1000(FindFriendSettingFragment.this).getApplicationContext(), b.b, false);
        if (aobj == null)
        {
            return "\u5206\u4EAB\u5931\u8D25\uFF0C\u53EF\u80FD\u6CA1\u6709\u5B89\u88C5\u5FAE\u4FE1";
        }
        ((IWXAPI) (aobj)).registerApp(b.b);
        if (((IWXAPI) (aobj)).sendReq(((com.tencent.mm.sdk.modelbase.BaseReq) (obj))))
        {
            return "0";
        } else
        {
            return "\u5206\u4EAB\u5931\u8D25\uFF0C\u53EF\u80FD\u6CA1\u6709\u5B89\u88C5\u5FAE\u4FE1";
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((String)obj);
    }

    protected void onPostExecute(String s)
    {
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }
        if (!"0".equals(s))
        {
            super.onPostExecute(s);
        }
    }

    protected void onPreExecute()
    {
        if (mProgressDialog == null)
        {
            mProgressDialog = ToolUtil.createProgressDialog(FindFriendSettingFragment.access$1100(FindFriendSettingFragment.this), 0, true, true);
        }
        mProgressDialog.setMessage("\u6B63\u5728\u542F\u52A8\u5FAE\u4FE1...");
        mProgressDialog.show();
    }

    ()
    {
        this$0 = FindFriendSettingFragment.this;
        super();
        buttonIndex = 0;
    }
}
