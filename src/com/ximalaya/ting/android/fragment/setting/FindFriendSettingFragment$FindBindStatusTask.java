// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.setting;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.thirdBind.ResponseFindBindStatusInfo;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.setting:
//            FindFriendSettingFragment

private class <init> extends MyAsyncTask
{

    final FindFriendSettingFragment this$0;

    protected transient ResponseFindBindStatusInfo doInBackground(Void avoid[])
    {
        Object obj;
        ResponseFindBindStatusInfo responsefindbindstatusinfo;
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/auth/bindStatus").toString();
        obj = new RequestParams();
        obj = f.a().a(avoid, ((RequestParams) (obj)), fragmentBaseContainerView, fragmentBaseContainerView);
        responsefindbindstatusinfo = new ResponseFindBindStatusInfo();
        if (obj == null) goto _L2; else goto _L1
_L1:
        avoid = null;
        obj = JSON.parseObject(((String) (obj)));
        avoid = ((Void []) (obj));
_L8:
        if (avoid != null) goto _L4; else goto _L3
_L3:
        responsefindbindstatusinfo.ret = -1;
        responsefindbindstatusinfo.msg = "";
_L6:
        return responsefindbindstatusinfo;
_L4:
        if (avoid.getInteger("ret").intValue() != 0) goto _L6; else goto _L5
_L5:
        try
        {
            responsefindbindstatusinfo.list = JSON.parseArray(avoid.get("data").toString(), com/ximalaya/ting/android/model/personal_setting/ThirdPartyUserInfo);
            responsefindbindstatusinfo.ret = 0;
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            responsefindbindstatusinfo.ret = -1;
            responsefindbindstatusinfo.msg = "";
            return responsefindbindstatusinfo;
        }
        return responsefindbindstatusinfo;
_L2:
        responsefindbindstatusinfo.ret = -1;
        responsefindbindstatusinfo.msg = "";
        return responsefindbindstatusinfo;
        Exception exception;
        exception;
        if (true) goto _L8; else goto _L7
_L7:
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(ResponseFindBindStatusInfo responsefindbindstatusinfo)
    {
        if (FindFriendSettingFragment.access$000(FindFriendSettingFragment.this) != null && isAdded() && responsefindbindstatusinfo != null && loginInfoModel != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (responsefindbindstatusinfo.ret == 0)
        {
            FindFriendSettingFragment.access$102(FindFriendSettingFragment.this, responsefindbindstatusinfo);
        } else
        {
            FindFriendSettingFragment.access$102(FindFriendSettingFragment.this, new ResponseFindBindStatusInfo());
            FindFriendSettingFragment.access$100(FindFriendSettingFragment.this).list = loginInfoModel.bindStatus;
        }
        responsefindbindstatusinfo = FindFriendSettingFragment.access$100(FindFriendSettingFragment.this).list;
        if (responsefindbindstatusinfo != null && responsefindbindstatusinfo.size() > 0)
        {
            int i = 0;
            while (i < responsefindbindstatusinfo.size()) 
            {
                if ("1".equals(((ThirdPartyUserInfo)responsefindbindstatusinfo.get(i)).thirdpartyId) && !((ThirdPartyUserInfo)responsefindbindstatusinfo.get(i)).isExpired)
                {
                    FindFriendSettingFragment.access$200(FindFriendSettingFragment.this)[0] = 1;
                }
                if ("2".equals(((ThirdPartyUserInfo)responsefindbindstatusinfo.get(i)).thirdpartyId) && !((ThirdPartyUserInfo)responsefindbindstatusinfo.get(i)).isExpired)
                {
                    FindFriendSettingFragment.access$200(FindFriendSettingFragment.this)[1] = 1;
                }
                if ("3".equals(((ThirdPartyUserInfo)responsefindbindstatusinfo.get(i)).thirdpartyId) && !((ThirdPartyUserInfo)responsefindbindstatusinfo.get(i)).isExpired)
                {
                    FindFriendSettingFragment.access$200(FindFriendSettingFragment.this)[2] = 1;
                }
                i++;
            }
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((ResponseFindBindStatusInfo)obj);
    }

    private ()
    {
        this$0 = FindFriendSettingFragment.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
