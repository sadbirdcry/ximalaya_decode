// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.setting;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.activity.login.AuthorizeActivity;
import com.ximalaya.ting.android.activity.setting.FindFriendActivity;
import com.ximalaya.ting.android.adapter.setting.FindFriendSettingAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseActivityLikeFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.FindFriendModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.setting.FindFriendCollection;
import com.ximalaya.ting.android.model.thirdBind.ResponseFindBindStatusInfo;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FindFriendSettingFragment extends BaseActivityLikeFragment
{
    private class FindBindStatusTask extends MyAsyncTask
    {

        final FindFriendSettingFragment this$0;

        protected transient ResponseFindBindStatusInfo doInBackground(Void avoid[])
        {
            Object obj;
            ResponseFindBindStatusInfo responsefindbindstatusinfo;
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/auth/bindStatus").toString();
            obj = new RequestParams();
            obj = f.a().a(avoid, ((RequestParams) (obj)), fragmentBaseContainerView, fragmentBaseContainerView);
            responsefindbindstatusinfo = new ResponseFindBindStatusInfo();
            if (obj == null) goto _L2; else goto _L1
_L1:
            avoid = null;
            obj = JSON.parseObject(((String) (obj)));
            avoid = ((Void []) (obj));
_L8:
            if (avoid != null) goto _L4; else goto _L3
_L3:
            responsefindbindstatusinfo.ret = -1;
            responsefindbindstatusinfo.msg = "";
_L6:
            return responsefindbindstatusinfo;
_L4:
            if (avoid.getInteger("ret").intValue() != 0) goto _L6; else goto _L5
_L5:
            try
            {
                responsefindbindstatusinfo.list = JSON.parseArray(avoid.get("data").toString(), com/ximalaya/ting/android/model/personal_setting/ThirdPartyUserInfo);
                responsefindbindstatusinfo.ret = 0;
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                responsefindbindstatusinfo.ret = -1;
                responsefindbindstatusinfo.msg = "";
                return responsefindbindstatusinfo;
            }
            return responsefindbindstatusinfo;
_L2:
            responsefindbindstatusinfo.ret = -1;
            responsefindbindstatusinfo.msg = "";
            return responsefindbindstatusinfo;
            Exception exception;
            exception;
            if (true) goto _L8; else goto _L7
_L7:
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(ResponseFindBindStatusInfo responsefindbindstatusinfo)
        {
            if (
// JavaClassFileOutputException: get_constant: invalid tag

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((ResponseFindBindStatusInfo)obj);
        }

        private FindBindStatusTask()
        {
            this$0 = FindFriendSettingFragment.this;
            super();
        }

        FindBindStatusTask(_cls1 _pcls1)
        {
            this();
        }
    }

    class ShareWXToFriends extends MyAsyncTask
    {

        int buttonIndex;
        ProgressDialog mProgressDialog;
        final FindFriendSettingFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected transient String doInBackground(Object aobj[])
        {
            buttonIndex = ((Integer)aobj[0]).intValue();
            Object obj = new WXWebpageObject();
            obj.webpageUrl = "http://m.ximalaya.com/?src=findfriend_invite_weixin";
            aobj = new WXMediaMessage();
            aobj.title = "\u559C\u9A6C\u62C9\u96C5";
            if (buttonIndex == 0)
            {
                aobj.description = "\u73A9\u8F6C\u559C\u9A6C\u62C9\u96C5\uFF0C\u5185\u5BB9\u4E30\u5BCC\u7684\u624B\u673A\u968F\u8EAB\u542C\uFF0C\u60F3\u542C\u5565\u5565\u90FD\u6709\uFF0C\u8D76\u7D27\u6765\u8BD5\u8BD5\u5427\uFF01";
            } else
            {
                aobj.description = "\u559C\u9A6C\u62C9\u96C5";
            }
            aobj.thumbData = ToolUtil.imageZoom32(BitmapFactory.decodeResource(getResources(), 0x7f020598));
            aobj.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
            obj = new Req();
            obj.message = ((WXMediaMessage) (aobj));
            if (buttonIndex == 0)
            {
                obj.scene = 0;
            } else
            {
                obj.scene = 1;
            }
            aobj = WXAPIFactory.createWXAPI(
// JavaClassFileOutputException: get_constant: invalid tag

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if (mProgressDialog != null && mProgressDialog.isShowing())
            {
                mProgressDialog.dismiss();
            }
            if (!"0".equals(s))
            {
                onPostExecute(s);
            }
        }

        protected void onPreExecute()
        {
            if (mProgressDialog == null)
            {
                mProgressDialog = ToolUtil.createProgressDialog(
// JavaClassFileOutputException: get_constant: invalid tag

        ShareWXToFriends()
        {
            this$0 = FindFriendSettingFragment.this;
            super();
            buttonIndex = 0;
        }
    }

    private class getFriendship_unfollowTask extends MyAsyncTask
    {

        FindFriendCollection ffc;
        ProgressDialog pd;
        final FindFriendSettingFragment this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((String[])aobj);
        }

        protected transient String doInBackground(String as[])
        {
            Object obj;
            obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/friendship/unfollow").toString();
            RequestParams requestparams = new RequestParams();
            requestparams.put("pageId", as[0]);
            requestparams.put("pageSize", "30");
            obj = f.a().a(((String) (obj)), requestparams, mListView, mListView);
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_233;
            }
            as = null;
            obj = JSON.parseObject(((String) (obj)));
            as = ((String []) (obj));
_L2:
            if (as == null)
            {
                return "\u6CA1\u6709\u8FD4\u56DE\u6570\u636E";
            }
            break; /* Loop/switch isn't completed */
            Exception exception;
            exception;
            exception.printStackTrace();
            if (true) goto _L2; else goto _L1
_L1:
            if (as.getInteger("ret").intValue() == 0)
            {
                ffc = new FindFriendCollection();
                try
                {
                    if (as.containsKey("unfollow"))
                    {
                        ffc.unfollowList = JSON.parseArray(as.get("unfollow").toString(), com/ximalaya/ting/android/model/personal_setting/FindFriendModel);
                    }
                    as = JSON.parseObject(as.get("hotUsers").toString());
                    ffc.hotUsersList = JSON.parseArray(as.get("list").toString(), com/ximalaya/ting/android/model/personal_setting/FindFriendModel);
                    ffc.ret = 0;
                    ffc.pageId = as.getIntValue("pageId");
                    ffc.maxPageId = as.getIntValue("maxPageId");
                }
                // Misplaced declaration of an exception variable
                catch (String as[])
                {
                    return "\u89E3\u6790\u6570\u636E\u51FA\u9519";
                }
                return "0";
            } else
            {
                return as.getString("msg");
            }
            return "\u7F51\u7EDC\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if (access$300 == null || !isAdded())
            {
                return;
            }
            if (pd != null)
            {
                pd.dismiss();
                pd = null;
            }
            if (!"0".equals(s))
            {
                break MISSING_BLOCK_LABEL_515;
            }
            if (ffc == null)
            {
                break MISSING_BLOCK_LABEL_497;
            }
            if (ffc.ret != 0) goto _L2; else goto _L1
_L1:
            curPageId = ffc.pageId;
            maxPageId = ffc.maxPageId;
            s = new ArrayList();
            if (ffc.unfollowList != null && ffc.unfollowList.size() > 0 && curPageId == 1)
            {
                FindFriendModel findfriendmodel = new FindFriendModel();
                findfriendmodel.isRealData = false;
                findfriendmodel.intro = "\u5DF2\u52A0\u5165\u559C\u9A6C\u62C9\u96C5\u7684\u597D\u53CB";
                ffc.unfollowList.add(0, findfriendmodel);
                s.addAll(ffc.unfollowList);
            }
            if (ffc.hotUsersList != null && ffc.hotUsersList.size() > 0)
            {
                if (curPageId == 1)
                {
                    FindFriendModel findfriendmodel1 = new FindFriendModel();
                    findfriendmodel1.isRealData = false;
                    findfriendmodel1.intro = "\u63A8\u8350\u5173\u6CE8";
                    ffc.hotUsersList.add(0, findfriendmodel1);
                }
                s.addAll(ffc.hotUsersList);
            }
            if (ffsa != null) goto _L4; else goto _L3
_L3:
            ffsa = new FindFriendSettingAdapter(access$300, s);
            mListView.setAdapter(ffsa);
_L5:
            ffsa.notifyDataSetChanged();
            return;
_L4:
            if (curPageId == 1)
            {
                ffsa.ffmList = s;
            } else
            if (ffsa.ffmList != null)
            {
                FindFriendModel findfriendmodel2 = (FindFriendModel)ffsa.ffmList.get(ffsa.ffmList.size() - 1);
                if (!findfriendmodel2.isRealData && Utilities.isBlank(findfriendmodel2.intro))
                {
                    ffsa.ffmList.remove(ffsa.ffmList.size() - 1);
                }
                ffsa.ffmList.addAll(s);
            }
            if (true) goto _L5; else goto _L2
_L2:
            Toast.makeText(mCon, ffc.msg, 0).show();
            return;
            Toast.makeText(mCon, "\u52A0\u8F7D\u6570\u636E\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5...", 0).show();
            return;
            Toast.makeText(mCon, "\u52A0\u8F7D\u6570\u636E\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5...", 0).show();
            return;
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            if (curPageId == 1)
            {
                pd = new MyProgressDialog(access$300);
                pd.setMessage("\u6B63\u5728\u52AA\u529B\u4E3A\u60A8\u52A0\u8F7D\u4FE1\u606F...");
                pd.show();
            }
        }

        private getFriendship_unfollowTask()
        {
            this$0 = FindFriendSettingFragment.this;
            super();
        }

        getFriendship_unfollowTask(_cls1 _pcls1)
        {
            this();
        }
    }


    public static final String P_TURN_FINDFRIEND_DATE = "P_TURN_FINDFRIEND_DATE";
    public static final String P_TURN_FINDFRIEND_TIMES = "in_find_friend_times";
    private static final String TAG = com/ximalaya/ting/android/fragment/setting/FindFriendSettingFragment.getSimpleName();
    private int OPEN_TIMES_FOR_INVITE;
    private ResponseFindBindStatusInfo bindStatusInfo;
    private FindBindStatusTask bindStatusTask;
    private View byPhone;
    private View byQQ;
    private View byRenn;
    private View bySina;
    private View byWeixin;
    private int curPageId;
    private FindFriendSettingAdapter ffsa;
    private int isbind[] = {
        0, 0, 0
    };
    private ListView mListView;
    private ScoreManage mScoreManage;
    private SharedPreferencesUtil mSharedPreferencesUtil;
    private int maxPageId;
    private final int pageSize = 30;

    public FindFriendSettingFragment()
    {
        OPEN_TIMES_FOR_INVITE = 3;
        curPageId = 1;
        maxPageId = 1;
    }

    private boolean checkHaveSimCard()
    {
        return "1".equals(ToolUtil.isSimExist(mCon));
    }

    private void findViews()
    {
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        nextButton.setVisibility(4);
        mListView = (ListView)findViewById(0x7f0a0273);
        View view = View.inflate(mCon, 0x7f030086, null);
        mListView.addHeaderView(view);
        byWeixin = view.findViewById(0x7f0a0261);
        byPhone = view.findViewById(0x7f0a0263);
        byQQ = view.findViewById(0x7f0a0265);
        bySina = view.findViewById(0x7f0a0264);
        byRenn = view.findViewById(0x7f0a0266);
    }

    private void initViews()
    {
        topTextView.setText(getString(0x7f090111));
        if (ffsa == null)
        {
            ffsa = new FindFriendSettingAdapter(mActivity, null);
        }
        mListView.setAdapter(ffsa);
        byWeixin.setOnClickListener(new _cls1());
        byPhone.setOnClickListener(new _cls2());
        bySina.setOnClickListener(new _cls3());
        byQQ.setOnClickListener(new _cls4());
        byRenn.setOnClickListener(new _cls5());
        mListView.setOnScrollListener(new _cls6());
        mListView.setOnItemClickListener(new _cls7());
    }

    private void isBind(int i)
    {
        if (isbind[i] != 0) goto _L2; else goto _L1
_L1:
        Intent intent = new Intent(mCon, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
        i;
        JVM INSTR tableswitch 0 2: default 52
    //                   0 78
    //                   1 84
    //                   2 90;
           goto _L3 _L4 _L5 _L6
_L3:
        int j = 0;
_L8:
        intent.putExtra("lgflag", j);
        intent.setFlags(0x20000000);
        startActivityForResult(intent, i);
_L2:
        return;
_L4:
        j = 12;
        continue; /* Loop/switch isn't completed */
_L5:
        j = 13;
        continue; /* Loop/switch isn't completed */
_L6:
        j = 14;
        if (true) goto _L8; else goto _L7
_L7:
    }

    private void jugdeInviteDialog()
    {
        if (mScoreManage != null && mSharedPreferencesUtil != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int i;
        if (!TimeHelper.isToday(mSharedPreferencesUtil.getLong("P_TURN_FINDFRIEND_DATE"))) goto _L4; else goto _L3
_L3:
        if ((i = mSharedPreferencesUtil.getInt("in_find_friend_times", 0)) >= OPEN_TIMES_FOR_INVITE - 1) goto _L1; else goto _L5
_L5:
        mSharedPreferencesUtil.saveInt("in_find_friend_times", i + 1);
_L7:
        ToolUtil.showUploadPhoneNumDialog(getActivity());
        return;
_L4:
        mSharedPreferencesUtil.saveLong("P_TURN_FINDFRIEND_DATE", (new Date()).getTime());
        if (true) goto _L7; else goto _L6
_L6:
    }

    private void toFind(int i)
    {
        Intent intent = new Intent(mCon, com/ximalaya/ting/android/activity/setting/FindFriendActivity);
        intent.setFlags(0x20000000);
        intent.putExtra("flag", i);
        startActivity(intent);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        bindStatusTask = new FindBindStatusTask(null);
        bindStatusTask.myexec(new Void[0]);
        findViews();
        initViews();
        mScoreManage = ScoreManage.getInstance(mActivity.getApplicationContext());
        mSharedPreferencesUtil = SharedPreferencesUtil.getInstance(mActivity.getApplicationContext());
        (new getFriendship_unfollowTask(null)).myexec(new String[] {
            (new StringBuilder()).append(curPageId).append("").toString()
        });
        jugdeInviteDialog();
    }

    public void onActivityResult(int i, int j, Intent intent)
    {
        i;
        JVM INSTR tableswitch 0 2: default 28
    //                   0 29
    //                   1 46
    //                   2 63;
           goto _L1 _L2 _L3 _L4
_L1:
        return;
_L2:
        if (j == 0)
        {
            isbind[0] = 1;
            toFind(2);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (j == 0)
        {
            toFind(3);
            isbind[1] = 1;
            return;
        }
        continue; /* Loop/switch isn't completed */
_L4:
        if (j == 0)
        {
            toFind(4);
            isbind[2] = 1;
            return;
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        fragmentBaseContainerView = layoutinflater.inflate(0x7f030089, null);
        return fragmentBaseContainerView;
    }






/*
    static ResponseFindBindStatusInfo access$102(FindFriendSettingFragment findfriendsettingfragment, ResponseFindBindStatusInfo responsefindbindstatusinfo)
    {
        findfriendsettingfragment.bindStatusInfo = responsefindbindstatusinfo;
        return responsefindbindstatusinfo;
    }

*/












/*
    static int access$502(FindFriendSettingFragment findfriendsettingfragment, int i)
    {
        findfriendsettingfragment.curPageId = i;
        return i;
    }

*/


/*
    static int access$504(FindFriendSettingFragment findfriendsettingfragment)
    {
        int i = findfriendsettingfragment.curPageId + 1;
        findfriendsettingfragment.curPageId = i;
        return i;
    }

*/



/*
    static int access$602(FindFriendSettingFragment findfriendsettingfragment, int i)
    {
        findfriendsettingfragment.maxPageId = i;
        return i;
    }

*/



/*
    static FindFriendSettingAdapter access$702(FindFriendSettingFragment findfriendsettingfragment, FindFriendSettingAdapter findfriendsettingadapter)
    {
        findfriendsettingfragment.ffsa = findfriendsettingadapter;
        return findfriendsettingadapter;
    }

*/





    private class _cls3
        implements android.view.View.OnClickListener
    {

        final FindFriendSettingFragment this$0;

        public void onClick(View view)
        {
            if (isbind[0] == 0)
            {
                isBind(0);
                return;
            } else
            {
                toFind(2);
                return;
            }
        }

        _cls3()
        {
            this$0 = FindFriendSettingFragment.this;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final FindFriendSettingFragment this$0;

        public void onClick(View view)
        {
            if (isbind[1] == 0)
            {
                isBind(1);
                return;
            } else
            {
                toFind(3);
                return;
            }
        }

        _cls4()
        {
            this$0 = FindFriendSettingFragment.this;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final FindFriendSettingFragment this$0;

        public void onClick(View view)
        {
            if (isbind[2] == 0)
            {
                isBind(2);
                return;
            } else
            {
                toFind(4);
                return;
            }
        }

        _cls5()
        {
            this$0 = FindFriendSettingFragment.this;
            super();
        }
    }


    private class _cls6
        implements android.widget.AbsListView.OnScrollListener
    {

        final FindFriendSettingFragment this$0;

        public void onScroll(AbsListView abslistview, int i, int j, int k)
        {
        }

        public void onScrollStateChanged(AbsListView abslistview, int i)
        {
label0:
            {
                switch (i)
                {
                }
                if (i == 0 && abslistview.getLastVisiblePosition() == ffsa.getCount() && curPageId < maxPageId && ffsa.ffmList != null)
                {
                    abslistview = ffsa.getItem(ffsa.getCount() - 1);
                    if (((FindFriendModel) (abslistview)).isRealData || !Utilities.isBlank(((FindFriendModel) (abslistview)).intro))
                    {
                        break label0;
                    }
                }
                return;
            }
            abslistview = new FindFriendModel();
            abslistview.isRealData = false;
            ffsa.ffmList.add(abslistview);
            ffsa.notifyDataSetChanged();
            mListView.setSelectionFromTop(ffsa.ffmList.size() - 1, 0);
            (new getFriendship_unfollowTask(null)).myexec(new String[] {
                (new StringBuilder()).append(int i = ((isBlank) (this)).isBlank + 1).append("").toString()
            });
        }

        _cls6()
        {
            this$0 = FindFriendSettingFragment.this;
            super();
        }
    }


    private class _cls7
        implements android.widget.AdapterView.OnItemClickListener
    {

        final FindFriendSettingFragment this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            if (i != 0)
            {
                adapterview = ffsa.getItem(i - 1);
                if (((FindFriendModel) (adapterview)).isRealData)
                {
                    Bundle bundle = new Bundle();
                    bundle.putLong("toUid", Long.valueOf(((FindFriendModel) (adapterview)).uid).longValue());
                    bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                    startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
                }
            }
        }

        _cls7()
        {
            this$0 = FindFriendSettingFragment.this;
            super();
        }
    }

}
