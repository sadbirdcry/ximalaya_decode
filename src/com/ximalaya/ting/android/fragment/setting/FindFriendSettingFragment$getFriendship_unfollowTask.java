// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.fragment.setting;

import android.app.ProgressDialog;
import android.widget.ListView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.setting.FindFriendSettingAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_setting.FindFriendModel;
import com.ximalaya.ting.android.model.setting.FindFriendCollection;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.fragment.setting:
//            FindFriendSettingFragment

private class <init> extends MyAsyncTask
{

    FindFriendCollection ffc;
    ProgressDialog pd;
    final FindFriendSettingFragment this$0;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((String[])aobj);
    }

    protected transient String doInBackground(String as[])
    {
        Object obj;
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/friendship/unfollow").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("pageId", as[0]);
        requestparams.put("pageSize", "30");
        obj = f.a().a(((String) (obj)), requestparams, FindFriendSettingFragment.access$300(FindFriendSettingFragment.this), FindFriendSettingFragment.access$300(FindFriendSettingFragment.this));
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_233;
        }
        as = null;
        obj = JSON.parseObject(((String) (obj)));
        as = ((String []) (obj));
_L2:
        if (as == null)
        {
            return "\u6CA1\u6709\u8FD4\u56DE\u6570\u636E";
        }
        break; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        exception.printStackTrace();
        if (true) goto _L2; else goto _L1
_L1:
        if (as.getInteger("ret").intValue() == 0)
        {
            ffc = new FindFriendCollection();
            try
            {
                if (as.containsKey("unfollow"))
                {
                    ffc.unfollowList = JSON.parseArray(as.get("unfollow").toString(), com/ximalaya/ting/android/model/personal_setting/FindFriendModel);
                }
                as = JSON.parseObject(as.get("hotUsers").toString());
                ffc.hotUsersList = JSON.parseArray(as.get("list").toString(), com/ximalaya/ting/android/model/personal_setting/FindFriendModel);
                ffc.ret = 0;
                ffc.pageId = as.getIntValue("pageId");
                ffc.maxPageId = as.getIntValue("maxPageId");
            }
            // Misplaced declaration of an exception variable
            catch (String as[])
            {
                return "\u89E3\u6790\u6570\u636E\u51FA\u9519";
            }
            return "0";
        } else
        {
            return as.getString("msg");
        }
        return "\u7F51\u7EDC\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((String)obj);
    }

    protected void onPostExecute(String s)
    {
        if (FindFriendSettingFragment.access$400(FindFriendSettingFragment.this) == null || !isAdded())
        {
            return;
        }
        if (pd != null)
        {
            pd.dismiss();
            pd = null;
        }
        if (!"0".equals(s))
        {
            break MISSING_BLOCK_LABEL_515;
        }
        if (ffc == null)
        {
            break MISSING_BLOCK_LABEL_497;
        }
        if (ffc.ret != 0) goto _L2; else goto _L1
_L1:
        FindFriendSettingFragment.access$502(FindFriendSettingFragment.this, ffc.pageId);
        FindFriendSettingFragment.access$602(FindFriendSettingFragment.this, ffc.maxPageId);
        s = new ArrayList();
        if (ffc.unfollowList != null && ffc.unfollowList.size() > 0 && FindFriendSettingFragment.access$500(FindFriendSettingFragment.this) == 1)
        {
            FindFriendModel findfriendmodel = new FindFriendModel();
            findfriendmodel.isRealData = false;
            findfriendmodel.intro = "\u5DF2\u52A0\u5165\u559C\u9A6C\u62C9\u96C5\u7684\u597D\u53CB";
            ffc.unfollowList.add(0, findfriendmodel);
            s.addAll(ffc.unfollowList);
        }
        if (ffc.hotUsersList != null && ffc.hotUsersList.size() > 0)
        {
            if (FindFriendSettingFragment.access$500(FindFriendSettingFragment.this) == 1)
            {
                FindFriendModel findfriendmodel1 = new FindFriendModel();
                findfriendmodel1.isRealData = false;
                findfriendmodel1.intro = "\u63A8\u8350\u5173\u6CE8";
                ffc.hotUsersList.add(0, findfriendmodel1);
            }
            s.addAll(ffc.hotUsersList);
        }
        if (FindFriendSettingFragment.access$700(FindFriendSettingFragment.this) != null) goto _L4; else goto _L3
_L3:
        FindFriendSettingFragment.access$702(FindFriendSettingFragment.this, new FindFriendSettingAdapter(FindFriendSettingFragment.access$800(FindFriendSettingFragment.this), s));
        FindFriendSettingFragment.access$300(FindFriendSettingFragment.this).setAdapter(FindFriendSettingFragment.access$700(FindFriendSettingFragment.this));
_L5:
        FindFriendSettingFragment.access$700(FindFriendSettingFragment.this).notifyDataSetChanged();
        return;
_L4:
        if (FindFriendSettingFragment.access$500(FindFriendSettingFragment.this) == 1)
        {
            FindFriendSettingFragment.access$700(FindFriendSettingFragment.this).ffmList = s;
        } else
        if (FindFriendSettingFragment.access$700(FindFriendSettingFragment.this).ffmList != null)
        {
            FindFriendModel findfriendmodel2 = (FindFriendModel)FindFriendSettingFragment.access$700(FindFriendSettingFragment.this).ffmList.get(FindFriendSettingFragment.access$700(FindFriendSettingFragment.this).ffmList.size() - 1);
            if (!findfriendmodel2.isRealData && Utilities.isBlank(findfriendmodel2.intro))
            {
                FindFriendSettingFragment.access$700(FindFriendSettingFragment.this).ffmList.remove(FindFriendSettingFragment.access$700(FindFriendSettingFragment.this).ffmList.size() - 1);
            }
            FindFriendSettingFragment.access$700(FindFriendSettingFragment.this).ffmList.addAll(s);
        }
        if (true) goto _L5; else goto _L2
_L2:
        Toast.makeText(mCon, ffc.msg, 0).show();
        return;
        Toast.makeText(mCon, "\u52A0\u8F7D\u6570\u636E\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5...", 0).show();
        return;
        Toast.makeText(mCon, "\u52A0\u8F7D\u6570\u636E\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5...", 0).show();
        return;
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        if (FindFriendSettingFragment.access$500(FindFriendSettingFragment.this) == 1)
        {
            pd = new MyProgressDialog(FindFriendSettingFragment.access$900(FindFriendSettingFragment.this));
            pd.setMessage("\u6B63\u5728\u52AA\u529B\u4E3A\u60A8\u52A0\u8F7D\u4FE1\u606F...");
            pd.show();
        }
    }

    private ()
    {
        this$0 = FindFriendSettingFragment.this;
        super();
    }

    this._cls0(this._cls0 _pcls0)
    {
        this();
    }
}
