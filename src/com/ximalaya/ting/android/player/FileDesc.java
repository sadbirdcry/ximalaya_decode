// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;

// Referenced classes of package com.ximalaya.ting.android.player:
//            Logger, MD5, XMediaPlayerConstants, PlayerUtil

public class FileDesc
{

    private static final String TAG = "dl_mp3";
    public volatile BitSet chunkExist;
    private int chunkNum;
    public volatile ArrayList chunkOffset;
    private int comChunkNum;
    private long comFileLen;
    private String dirPath;
    public volatile ArrayList downloadedChunks;
    private String eTag;
    public volatile int statusCode;
    private String url;
    public volatile boolean valid;

    public FileDesc(String s, String s1)
        throws IOException
    {
        Object obj1;
        ArrayList arraylist;
        File file;
        File file1;
        arraylist = null;
        obj1 = null;
        super();
        valid = false;
        Logger.log("dl_mp3", "======================FileDesc Constructor()");
        dirPath = s;
        url = s1;
        createCacheDir();
        file = new File((new StringBuilder(String.valueOf(s))).append("/").append(MD5.md5(s1)).append(".index").toString());
        file1 = new File((new StringBuilder(String.valueOf(s))).append("/").append(MD5.md5(s1)).append(".chunk").toString());
        if (file.exists() != file1.exists())
        {
            file.delete();
            file1.delete();
        }
        if (file.exists()) goto _L2; else goto _L1
_L1:
        initFileDescFormNet(s, s1);
_L4:
        return;
_L2:
        Object obj = new FileInputStream((new StringBuilder(String.valueOf(s))).append("/").append(MD5.md5(s1)).append(".index").toString());
        obj1 = new DataInputStream(((java.io.InputStream) (obj)));
        url = ((DataInputStream) (obj1)).readUTF();
        if (url != null && url.equals(s1))
        {
            break MISSING_BLOCK_LABEL_270;
        }
        file.delete();
        file1.delete();
        initFileDescFormNet(s, s1);
        if (obj != null)
        {
            ((FileInputStream) (obj)).close();
        }
        if (obj1 == null) goto _L4; else goto _L3
_L3:
        ((DataInputStream) (obj1)).close();
        return;
        int j;
        comFileLen = ((DataInputStream) (obj1)).readLong();
        eTag = ((DataInputStream) (obj1)).readUTF();
        j = (int)Math.ceil((float)comFileLen / 65536F);
        Logger.log("dl_mp3", (new StringBuilder("calc002==comChunkNum==:")).append(comFileLen).append(", ").append(j).toString());
        comChunkNum = j;
        chunkNum = ((DataInputStream) (obj1)).readInt();
        arraylist = new ArrayList();
        int i = 0;
_L6:
        if (i < chunkNum)
        {
            break MISSING_BLOCK_LABEL_412;
        }
        if (comChunkNum > 0)
        {
            valid = true;
            initFromArray(arraylist, j);
        }
        if (obj != null)
        {
            ((FileInputStream) (obj)).close();
        }
        if (obj1 == null) goto _L4; else goto _L5
_L5:
        ((DataInputStream) (obj1)).close();
        return;
        arraylist.add(new Integer(((DataInputStream) (obj1)).readInt()));
        i++;
          goto _L6
        obj;
        obj = null;
_L10:
        file.delete();
        file1.delete();
        initFileDescFormNet(s, s1);
        if (obj1 != null)
        {
            ((FileInputStream) (obj1)).close();
        }
        if (obj == null) goto _L4; else goto _L7
_L7:
        ((DataInputStream) (obj)).close();
        return;
        s;
        obj = null;
        s1 = arraylist;
_L9:
        if (obj != null)
        {
            ((FileInputStream) (obj)).close();
        }
        if (s1 != null)
        {
            s1.close();
        }
        throw s;
        s;
        s1 = arraylist;
        continue; /* Loop/switch isn't completed */
        s;
        s1 = ((String) (obj1));
        continue; /* Loop/switch isn't completed */
        s;
        s1 = ((String) (obj));
        obj = obj1;
        if (true) goto _L9; else goto _L8
_L8:
        obj1;
        Object obj2 = null;
        obj1 = obj;
        obj = obj2;
          goto _L10
        IOException ioexception;
        ioexception;
        Object obj3 = obj;
        obj = obj1;
        obj1 = obj3;
          goto _L10
    }

    private boolean createCacheDir()
    {
        File file = new File(XMediaPlayerConstants.INCOM_AUDIO_FILE_DIRECTORY);
        if (!file.exists() && !file.mkdirs())
        {
            Logger.log("dl_mp3", (new StringBuilder("\u76EE\u5F55\u4E0D\u5B58\u5728\uFF0C\u521B\u5EFA\u5931\u8D25\uFF01")).append(XMediaPlayerConstants.INCOM_AUDIO_FILE_DIRECTORY).toString());
            return false;
        } else
        {
            return true;
        }
    }

    private void initFromArray(ArrayList arraylist, int i)
    {
        int j;
        chunkExist = new BitSet(i);
        chunkOffset = new ArrayList(i);
        j = 0;
_L3:
        if (j < i) goto _L2; else goto _L1
_L1:
        downloadedChunks = new ArrayList();
        downloadedChunks = arraylist;
        i = 0;
_L4:
        if (i >= arraylist.size())
        {
            return;
        }
        break MISSING_BLOCK_LABEL_77;
_L2:
        chunkOffset.add(j, Integer.valueOf(-1));
        j++;
          goto _L3
        chunkExist.set(((Integer)arraylist.get(i)).intValue());
        chunkOffset.set(((Integer)arraylist.get(i)).intValue(), Integer.valueOf(i));
        i++;
          goto _L4
    }

    public boolean equals(Object obj)
    {
        boolean flag1 = false;
        boolean flag;
        if (obj == this)
        {
            flag = true;
        } else
        {
            flag = flag1;
            if (obj instanceof FileDesc)
            {
                obj = (FileDesc)obj;
                String s = getUrl();
                flag = flag1;
                if (s != null)
                {
                    return s.equals(((FileDesc) (obj)).getUrl());
                }
            }
        }
        return flag;
    }

    public int getComChunkNum()
    {
        return comChunkNum;
    }

    public int getComFileLen()
    {
        return (int)comFileLen;
    }

    public int getDownloadedChunks()
    {
        this;
        JVM INSTR monitorenter ;
        if (downloadedChunks == null) goto _L2; else goto _L1
_L1:
        int i = downloadedChunks.size();
_L4:
        this;
        JVM INSTR monitorexit ;
        return i;
_L2:
        i = 0;
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    public String getETag()
    {
        return eTag;
    }

    public int getErrorCode()
    {
        return statusCode;
    }

    public String getUrl()
    {
        return url;
    }

    public int hashCode()
    {
        if (url == null)
        {
            return 0;
        } else
        {
            return url.hashCode();
        }
    }

    public void initFileDescFormNet(String s, String s1)
        throws IOException
    {
        int i;
        boolean flag;
        flag = false;
        i = 3;
_L7:
        if (i > 0) goto _L2; else goto _L1
_L1:
        if (!valid)
        {
            Logger.log("dl_mp3", (new StringBuilder("valid0:")).append(valid).append("comFileLen:").append(comFileLen).append("statusCode:").append(statusCode).toString());
            return;
        }
        break MISSING_BLOCK_LABEL_249;
_L2:
        HttpURLConnection httpurlconnection;
        httpurlconnection = PlayerUtil.getHttpURLConnection(s1);
        httpurlconnection.setRequestMethod("HEAD");
        httpurlconnection.connect();
        statusCode = httpurlconnection.getResponseCode();
        if (statusCode < 400)
        {
            break; /* Loop/switch isn't completed */
        }
        Logger.w("dl_mp3", (new StringBuilder("Error response code for get head for url: ")).append(s1).append(",status code is: ").append(statusCode).append(" in handler thread").toString());
        valid = false;
        statusCode = statusCode;
_L4:
        comFileLen = httpurlconnection.getContentLength();
        Logger.log("dl_mp3", (new StringBuilder("conn.getContentLength():")).append(comFileLen).toString());
        if (comFileLen <= 0L)
        {
            valid = false;
        }
        eTag = httpurlconnection.getHeaderField("ETag");
        httpurlconnection.disconnect();
        if (!valid)
        {
            break MISSING_BLOCK_LABEL_481;
        }
        if (true) goto _L1; else goto _L3
_L3:
        try
        {
            valid = true;
            statusCode = statusCode;
        }
        catch (Exception exception)
        {
            valid = false;
            break MISSING_BLOCK_LABEL_481;
        }
          goto _L4
        if (true) goto _L1; else goto _L5
_L5:
        Logger.log("dl_mp3", (new StringBuilder("valid1:")).append(valid).append("comFileLen:").append(comFileLen).append("statusCode:").append(statusCode).toString());
        comChunkNum = (int)Math.ceil((float)comFileLen / 65536F);
        Logger.log("dl_mp3", (new StringBuilder("calc001==comFileLen==:")).append(comFileLen).append(",comChunkNum:").append(comChunkNum).append("statusCode:").append(statusCode).toString());
        chunkNum = 0;
        chunkExist = new BitSet(comChunkNum);
        if (comChunkNum > 0)
        {
            chunkOffset = new ArrayList(comChunkNum);
            i = ((flag) ? 1 : 0);
            do
            {
                if (i >= comChunkNum)
                {
                    downloadedChunks = new ArrayList();
                    saveDescHeadToDisk(s, MD5.md5(s1));
                    valid = true;
                    return;
                }
                chunkOffset.add(i, Integer.valueOf(-1));
                i++;
            } while (true);
        } else
        {
            valid = false;
            statusCode = 408;
            return;
        }
        i--;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public boolean isChunkDownloaded(int i)
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag;
        boolean flag1;
        flag1 = false;
        flag = flag1;
        if (chunkExist == null)
        {
            break MISSING_BLOCK_LABEL_45;
        }
        flag = flag1;
        if (i < 0)
        {
            break MISSING_BLOCK_LABEL_45;
        }
        flag = flag1;
        if (i < chunkExist.length())
        {
            flag = chunkExist.get(i);
        }
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    public boolean isIndexAvaliable(int i)
    {
        boolean flag1 = false;
        boolean flag = flag1;
        if (chunkExist != null)
        {
            flag = flag1;
            if (i >= 0)
            {
                flag = flag1;
                if (i < chunkExist.length())
                {
                    flag = true;
                }
            }
        }
        return flag;
    }

    public boolean isValid()
    {
        return valid && comChunkNum > 0;
    }

    public void saveDescHeadToDisk(String s, String s1)
    {
        this;
        JVM INSTR monitorenter ;
        Logger.log("dl_mp3", (new StringBuilder("saveDescHeadToDisk(")).append(s).append(", ").append(s1).append(", ").append(comFileLen).append(")").toString());
        DataOutputStream dataoutputstream;
        s1 = new FileOutputStream((new StringBuilder(String.valueOf(s))).append("/").append(s1).append(".index").toString(), false);
        dataoutputstream = new DataOutputStream(s1);
        dataoutputstream.writeUTF(url);
        dataoutputstream.writeLong(comFileLen);
        if (eTag != null) goto _L2; else goto _L1
_L1:
        s = "";
_L6:
        dataoutputstream.writeUTF(s);
        dataoutputstream.writeInt(0);
        dataoutputstream.flush();
        dataoutputstream.close();
        s1.close();
_L4:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        try
        {
            s = eTag;
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (String s) { }
        finally
        {
            this;
        }
        Logger.log("dl_mp3", (new StringBuilder("exception: saveDescHeadToDisk")).append(s.getMessage()).toString());
        if (true) goto _L4; else goto _L3
_L3:
        throw s;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public boolean saveDescToDisk(String s, String s1)
    {
        this;
        JVM INSTR monitorenter ;
        Logger.log("dl_mp3", (new StringBuilder("======================saveDescToDisk(")).append(s).append(", ").append(s1).append(")").toString());
        DataOutputStream dataoutputstream;
        s1 = new FileOutputStream((new StringBuilder(String.valueOf(s))).append("/").append(s1).append(".index").toString(), false);
        dataoutputstream = new DataOutputStream(s1);
        dataoutputstream.writeUTF(url);
        dataoutputstream.writeLong(comFileLen);
        if (eTag != null) goto _L2; else goto _L1
_L1:
        s = "\"\"";
_L9:
        dataoutputstream.writeUTF(s);
        dataoutputstream.writeInt(chunkNum);
        s = downloadedChunks.iterator();
_L7:
        if (s.hasNext()) goto _L4; else goto _L3
_L3:
        dataoutputstream.flush();
        dataoutputstream.close();
        s1.close();
        boolean flag = true;
_L5:
        this;
        JVM INSTR monitorexit ;
        return flag;
_L2:
        try
        {
            s = (new StringBuilder("\"")).append(eTag).append("\"").toString();
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            flag = false;
        }
        finally
        {
            this;
        }
        if (true) goto _L5; else goto _L4
_L4:
        dataoutputstream.writeInt(((Integer)s.next()).intValue());
        if (true) goto _L7; else goto _L6
_L6:
        JVM INSTR monitorexit ;
        throw s;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public void update(int i)
    {
        this;
        JVM INSTR monitorenter ;
        BitSet bitset = chunkExist;
        if (bitset != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        int j = downloadedChunks.size();
        chunkExist.set(i);
        chunkOffset.set(i, Integer.valueOf(j));
        downloadedChunks.add(new Integer(i));
        chunkNum = chunkNum + 1;
        saveDescToDisk(dirPath, MD5.md5(url));
        if (true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }
}
