// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.os.Environment;
import android.text.TextUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaPlayerConstants, MD5

public class PlayerUtil
{

    private static String cpuInfo = "";
    private static String mAuthorization;
    private static String mProxyHost;
    private static int mProxyPort;

    public PlayerUtil()
    {
    }

    public static boolean checkSdcard()
    {
        return "mounted".equals(Environment.getExternalStorageState()) && Environment.getExternalStorageDirectory().canWrite();
    }

    public static void cleanUpCacheSound(String s)
    {
        String s1;
        File file;
        s1 = null;
        file = new File(XMediaPlayerConstants.INCOM_AUDIO_FILE_DIRECTORY);
        if (file == null || !file.isDirectory()) goto _L2; else goto _L1
_L1:
        File afile[];
        int i;
        int j;
        if (!TextUtils.isEmpty(s))
        {
            s = MD5.md5(s);
            s1 = (new StringBuilder()).append(s).append(".chunk").toString();
            s = (new StringBuilder()).append(s).append(".index").toString();
        } else
        {
            Object obj = null;
            s = s1;
            s1 = obj;
        }
        afile = file.listFiles();
        j = afile.length;
        i = 0;
        if (i < j) goto _L3; else goto _L2
_L2:
        s = new File(XMediaPlayerConstants.HLS_TS_DIR);
        if (s == null || !s.isDirectory()) goto _L5; else goto _L4
_L4:
        s = s.listFiles();
        j = s.length;
        i = 0;
_L7:
        if (i < j)
        {
            break MISSING_BLOCK_LABEL_189;
        }
_L5:
        return;
_L3:
        file = afile[i];
        if ((s1 == null || !s1.equalsIgnoreCase(file.getName())) && (s == null || !s.equalsIgnoreCase(file.getName())))
        {
            file.delete();
        }
        i++;
        break MISSING_BLOCK_LABEL_88;
        s[i].delete();
        i++;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public static boolean existSDCard()
    {
        return Environment.getExternalStorageState().equals("mounted");
    }

    public static String formatDuring(long l)
    {
        if (l >= 3600L)
        {
            int i = (int)(l / 3600L);
            int j1 = (int)(l % 3600L);
            if (j1 >= 60)
            {
                int k = j1 / 60;
                j1 %= 60;
                String s;
                StringBuilder stringbuilder;
                if (i < 10)
                {
                    s = (new StringBuilder("0")).append(i).toString();
                } else
                {
                    s = (new StringBuilder()).append(i).toString();
                }
                stringbuilder = (new StringBuilder(String.valueOf(s))).append(":");
                if (k < 10)
                {
                    s = (new StringBuilder("0")).append(k).toString();
                } else
                {
                    s = (new StringBuilder()).append(k).toString();
                }
                stringbuilder = stringbuilder.append(s).append(":");
                if (j1 < 10)
                {
                    s = (new StringBuilder("0")).append(j1).toString();
                } else
                {
                    s = (new StringBuilder()).append(j1).toString();
                }
                return stringbuilder.append(s).toString();
            }
            String s1;
            StringBuilder stringbuilder1;
            if (i < 10)
            {
                s1 = (new StringBuilder("0")).append(i).toString();
            } else
            {
                s1 = (new StringBuilder()).append(i).toString();
            }
            stringbuilder1 = (new StringBuilder(String.valueOf(s1))).append(":");
            if (j1 < 10)
            {
                s1 = (new StringBuilder("00:0")).append(j1).toString();
            } else
            {
                s1 = (new StringBuilder("00:")).append(j1).toString();
            }
            return stringbuilder1.append(s1).toString();
        }
        if (l >= 60L)
        {
            int j = (int)(l / 60L);
            int i1 = (int)(l % 60L);
            String s2;
            StringBuilder stringbuilder2;
            if (j < 10)
            {
                s2 = (new StringBuilder("0")).append(j).toString();
            } else
            {
                s2 = (new StringBuilder()).append(j).toString();
            }
            stringbuilder2 = (new StringBuilder(String.valueOf(s2))).append(":");
            if (i1 < 10)
            {
                s2 = (new StringBuilder("0")).append(i1).toString();
            } else
            {
                s2 = (new StringBuilder()).append(i1).toString();
            }
            return stringbuilder2.append(s2).toString();
        }
        if (l < 10L)
        {
            return (new StringBuilder("00:0")).append((int)l).toString();
        } else
        {
            return (new StringBuilder("00:")).append((int)l).toString();
        }
    }

    public static String getCpuInfo()
    {
        if (!TextUtils.isEmpty(cpuInfo))
        {
            return cpuInfo;
        }
        BufferedReader bufferedreader;
        String as[];
        bufferedreader = new BufferedReader(new FileReader("/proc/cpuinfo"), 8192);
        as = bufferedreader.readLine().split("\\s+");
        int i = 2;
_L3:
        if (i < as.length) goto _L2; else goto _L1
_L1:
        bufferedreader.close();
_L4:
        return cpuInfo;
_L2:
        cpuInfo = (new StringBuilder(String.valueOf(cpuInfo))).append(as[i]).append(" ").toString();
        i++;
          goto _L3
        IOException ioexception;
        ioexception;
          goto _L4
    }

    public static String getHlsFilePath(String s)
    {
        MD5.md5(s);
        (new File(XMediaPlayerConstants.HLS_TS_DIR)).mkdirs();
        return (new StringBuilder(String.valueOf(XMediaPlayerConstants.HLS_TS_DIR))).append(File.separatorChar).append(s.substring(s.length() - 20, s.length())).toString();
    }

    public static HttpURLConnection getHttpURLConnection(String s)
        throws IOException
    {
        s = new URL(s);
        if (!TextUtils.isEmpty(mProxyHost))
        {
            HttpURLConnection httpurlconnection = (HttpURLConnection)s.openConnection(new Proxy(java.net.Proxy.Type.HTTP, new InetSocketAddress(mProxyHost, mProxyPort)));
            s = httpurlconnection;
            if (!TextUtils.isEmpty(mAuthorization))
            {
                httpurlconnection.setRequestProperty("Authorization", mAuthorization);
                s = httpurlconnection;
            }
        } else
        {
            s = (HttpURLConnection)s.openConnection();
        }
        s.setConnectTimeout(10000);
        s.setReadTimeout(10000);
        s.setUseCaches(true);
        return s;
    }

    public static final long getPlayCacheSize()
    {
        float f = 0.0F;
        if (checkSdcard())
        {
            f = 0.0F + (float)sizeOfDirectory(new File(XMediaPlayerConstants.INCOM_AUDIO_FILE_DIRECTORY)) + (float)sizeOfDirectory(new File(XMediaPlayerConstants.HLS_TS_DIR));
        }
        return (long)f;
    }

    public static boolean isDownloadHlsTs(String s)
    {
        s = new File(getHlsFilePath(s));
        return s.exists() && s.length() > 0L;
    }

    public static boolean isLocalFile(String s)
    {
        return s == null || !s.startsWith("http://");
    }

    public static boolean isRrmV7Plus()
    {
        String s = System.getProperty("os.arch");
        if (s == null || !s.contains("arm")) goto _L2; else goto _L1
_L1:
        int i = 0;
_L5:
        if (i < s.length()) goto _L3; else goto _L2
_L2:
        return false;
_L3:
        if (s.charAt(i) >= '0' && s.charAt(i) <= '9' && s.charAt(i) >= '7')
        {
            return true;
        }
        i++;
        if (true) goto _L5; else goto _L4
_L4:
    }

    public static boolean isX86Arch()
    {
        String s = System.getProperty("os.arch");
        return s != null && s.contains("86");
    }

    public static void setAuthorization(String s)
    {
        mAuthorization = s;
    }

    public static void setProxyHost(String s)
    {
        mProxyHost = s;
    }

    public static void setProxyPort(int i)
    {
        mProxyPort = i;
    }

    public static long sizeOfDirectory(File file)
    {
        long l;
        long l1;
        l = 0L;
        l1 = l;
        if (file == null) goto _L2; else goto _L1
_L1:
        if (file.exists()) goto _L4; else goto _L3
_L3:
        l1 = l;
_L2:
        return l1;
_L4:
        Object obj;
        if (!file.isDirectory())
        {
            return file.length();
        }
        obj = file.listFiles();
        if (obj != null)
        {
            break; /* Loop/switch isn't completed */
        }
        obj = null;
        if (!file.exists())
        {
            obj = (new StringBuilder()).append(file).append(" does not exist").toString();
        } else
        if (!file.isDirectory())
        {
            obj = (new StringBuilder()).append(file).append(" is not a directory").toString();
        }
        l1 = l;
        if (obj != null)
        {
            return 0L;
        }
        if (true) goto _L2; else goto _L5
_L5:
        int i = 0;
        do
        {
            l1 = l;
            if (i >= obj.length)
            {
                continue;
            }
            file = obj[i];
            if (file.isDirectory())
            {
                l += sizeOfDirectory(file);
            } else
            {
                l += file.length();
            }
            i++;
        } while (true);
        if (true) goto _L2; else goto _L6
_L6:
    }

    public static void writeFile(String s, byte abyte0[], int i)
    {
        String s1 = s;
        try
        {
            if (s.contains("http"))
            {
                s1 = s.substring(s.length() - 10, s.length());
            }
            s = new File((new StringBuilder(String.valueOf(XMediaPlayerConstants.APP_BASE_DIR))).append("/log/test/").append(i).append("_").append(s1).toString());
            s.getParentFile().mkdirs();
            s = new FileOutputStream(s);
            s.write(abyte0);
            s.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
    }

}
