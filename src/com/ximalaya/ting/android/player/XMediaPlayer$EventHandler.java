// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaPlayer, XMediaplayerJNI, Logger

private class mMediaPlayer extends Handler
{

    private XMediaPlayer mMediaPlayer;
    final XMediaPlayer this$0;

    public void handleMessage(Message message)
    {
        if (XMediaPlayer.access$0(XMediaPlayer.this) != 12 || message.what == 100) goto _L2; else goto _L1
_L1:
        Logger.log(XMediaplayerJNI.Tag, "handleMessage11 mPlayState NOT_ARCH_SUPPORT");
_L4:
        return;
_L2:
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("handleMessage11 mPlayState:")).append(XMediaPlayer.access$0(XMediaPlayer.this)).toString());
        switch (message.what)
        {
        default:
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("Unknown message type ")).append(message.what).toString());
            return;

        case 0: // '\0'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
        case 9: // '\t'
        case 99: // 'c'
        case 201: 
            break;

        case 202: 
            continue; /* Loop/switch isn't completed */

        case 1: // '\001'
            if (XMediaPlayer.access$1(XMediaPlayer.this) != null)
            {
                XMediaPlayer.access$1(XMediaPlayer.this).onPrepared(mMediaPlayer);
                return;
            }
            break;

        case 2: // '\002'
            if (XMediaPlayer.access$2(XMediaPlayer.this) != null)
            {
                XMediaPlayer.access$2(XMediaPlayer.this).onCompletion(mMediaPlayer);
                return;
            }
            break;

        case 3: // '\003'
            if (XMediaPlayer.access$3(XMediaPlayer.this) != null)
            {
                XMediaPlayer.access$3(XMediaPlayer.this).onBufferingUpdate(mMediaPlayer, message.arg1);
                return;
            }
            break;

        case 4: // '\004'
            if (XMediaPlayer.access$4(XMediaPlayer.this) != null)
            {
                XMediaPlayer.access$4(XMediaPlayer.this).onSeekComplete(mMediaPlayer);
                return;
            }
            break;

        case 100: // 'd'
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("Error (")).append(message.arg1).append(",").append(message.arg2).append(")").toString());
            boolean flag = false;
            XMediaPlayer.access$5(XMediaPlayer.this, 8);
            if (XMediaPlayer.access$6(XMediaPlayer.this) != null)
            {
                flag = XMediaPlayer.access$6(XMediaPlayer.this).onError(mMediaPlayer, message.arg1, message.arg2);
            }
            if (XMediaPlayer.access$2(XMediaPlayer.this) != null && !flag)
            {
                XMediaPlayer.access$2(XMediaPlayer.this).onCompletion(mMediaPlayer);
                return;
            }
            break;

        case 200: 
            if (XMediaPlayer.access$7(XMediaPlayer.this) != null)
            {
                break; /* Loop/switch isn't completed */
            }
            break;
        }
        if (true) goto _L4; else goto _L3
_L3:
        XMediaPlayer.access$7(XMediaPlayer.this).onInfo(mMediaPlayer, message.arg1, message.arg2);
        return;
        if (XMediaPlayer.access$8(XMediaPlayer.this) == null || isSeeking()) goto _L4; else goto _L5
_L5:
        XMediaPlayer.access$8(XMediaPlayer.this).onPositionChange(mMediaPlayer, mMediaPlayer.getCurrentPosition());
        return;
    }

    public ngeListener(XMediaPlayer xmediaplayer1, Looper looper)
    {
        this$0 = XMediaPlayer.this;
        super(looper);
        mMediaPlayer = xmediaplayer1;
    }
}
