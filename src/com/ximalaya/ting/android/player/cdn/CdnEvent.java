// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player.cdn;


// Referenced classes of package com.ximalaya.ting.android.player.cdn:
//            CdnCollectDataForPlay

public class CdnEvent
{

    private String parentSpanId;
    private CdnCollectDataForPlay props;
    private int seqId;
    private String spanId;
    private String traceId;
    private long ts;
    private String type;
    private String viewId;

    public CdnEvent()
    {
    }

    public String getParentSpanId()
    {
        return parentSpanId;
    }

    public CdnCollectDataForPlay getProps()
    {
        return props;
    }

    public int getSeqId()
    {
        return seqId;
    }

    public String getSpanId()
    {
        return spanId;
    }

    public String getTraceId()
    {
        return traceId;
    }

    public long getTs()
    {
        return ts;
    }

    public String getType()
    {
        return type;
    }

    public String getViewId()
    {
        return viewId;
    }

    public void setParentSpanId(String s)
    {
        parentSpanId = s;
    }

    public void setProps(CdnCollectDataForPlay cdncollectdataforplay)
    {
        props = cdncollectdataforplay;
    }

    public void setSeqId(int i)
    {
        seqId = i;
    }

    public void setSpanId(String s)
    {
        spanId = s;
    }

    public void setTraceId(String s)
    {
        traceId = s;
    }

    public void setTs(long l)
    {
        ts = l;
    }

    public void setType(String s)
    {
        type = s;
    }

    public void setViewId(String s)
    {
        viewId = s;
    }

    public String toString()
    {
        return (new StringBuilder(String.valueOf(super.toString()))).append("CdnEvent [props=").append(props).append("]").toString();
    }
}
