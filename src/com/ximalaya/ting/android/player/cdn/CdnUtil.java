// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player.cdn;

import android.text.TextUtils;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.ximalaya.ting.android.player.cdn:
//            CdnEvent, CdnCollectDataForPlay, CdnConfigModel, CdnCookie

public class CdnUtil
{

    private static final String cdnUrl = "http://xdcs-collector.test.ximalaya.com/api/v1/cdnAndroid";
    private static InputStream is;
    private static CdnConfigModel mCdnConfigModel;
    private static HttpURLConnection mConn;
    private static OutputStream os;

    private CdnUtil()
    {
    }

    public static String ObjectTOJSON(List list)
        throws JSONException
    {
        JSONObject jsonobject = new JSONObject();
        JSONArray jsonarray = new JSONArray();
        list = list.iterator();
        do
        {
            Object obj;
            do
            {
                if (!list.hasNext())
                {
                    jsonobject.put("events", jsonarray);
                    return jsonobject.toString();
                }
                obj = (CdnEvent)list.next();
            } while (obj == null);
            JSONObject jsonobject1 = new JSONObject();
            if (!TextUtils.isEmpty(((CdnEvent) (obj)).getViewId()))
            {
                jsonobject1.put("viewId", ((CdnEvent) (obj)).getViewId());
            }
            if (!TextUtils.isEmpty(((CdnEvent) (obj)).getParentSpanId()))
            {
                jsonobject1.put("parentSpanId", ((CdnEvent) (obj)).getParentSpanId());
            }
            if (!TextUtils.isEmpty(((CdnEvent) (obj)).getSpanId()))
            {
                jsonobject1.put("viewId", ((CdnEvent) (obj)).getSpanId());
            }
            if (!TextUtils.isEmpty(((CdnEvent) (obj)).getTraceId()))
            {
                jsonobject1.put("viewId", ((CdnEvent) (obj)).getTraceId());
            }
            if (((CdnEvent) (obj)).getTs() >= 0L)
            {
                jsonobject1.put("ts", ((CdnEvent) (obj)).getTs());
            }
            if (((CdnEvent) (obj)).getSeqId() >= 0)
            {
                jsonobject1.put("seqId", ((CdnEvent) (obj)).getSeqId());
            }
            if (!TextUtils.isEmpty(((CdnEvent) (obj)).getType()))
            {
                jsonobject1.put("type", ((CdnEvent) (obj)).getType());
            }
            if (((CdnEvent) (obj)).getProps() != null)
            {
                obj = ((CdnEvent) (obj)).getProps();
                JSONObject jsonobject2 = new JSONObject();
                if (!TextUtils.isEmpty(((CdnCollectDataForPlay) (obj)).getAudioUrl()))
                {
                    jsonobject2.put("audioUrl", ((CdnCollectDataForPlay) (obj)).getAudioUrl());
                }
                if (!TextUtils.isEmpty(((CdnCollectDataForPlay) (obj)).getCdnIP()))
                {
                    jsonobject2.put("cdnIP", ((CdnCollectDataForPlay) (obj)).getCdnIP());
                }
                if (!TextUtils.isEmpty(((CdnCollectDataForPlay) (obj)).getDownloadSpeed()))
                {
                    jsonobject2.put("downloadSpeed", (new StringBuilder(String.valueOf(((CdnCollectDataForPlay) (obj)).getDownloadSpeed()))).toString());
                }
                if (!TextUtils.isEmpty(((CdnCollectDataForPlay) (obj)).getErrorType()))
                {
                    jsonobject2.put("errorType", ((CdnCollectDataForPlay) (obj)).getErrorType());
                }
                if (!TextUtils.isEmpty(((CdnCollectDataForPlay) (obj)).getExceptionReason()))
                {
                    jsonobject2.put("exceptionReason", ((CdnCollectDataForPlay) (obj)).getExceptionReason());
                }
                if (!TextUtils.isEmpty(((CdnCollectDataForPlay) (obj)).getStatusCode()))
                {
                    jsonobject2.put("statusCode", ((CdnCollectDataForPlay) (obj)).getStatusCode());
                }
                if (!TextUtils.isEmpty(((CdnCollectDataForPlay) (obj)).getType()))
                {
                    jsonobject2.put("type", ((CdnCollectDataForPlay) (obj)).getType());
                }
                if (!TextUtils.isEmpty(((CdnCollectDataForPlay) (obj)).getViaInfo()))
                {
                    jsonobject2.put("viaInfo", ((CdnCollectDataForPlay) (obj)).getViaInfo());
                }
                if (((CdnCollectDataForPlay) (obj)).getAudioBytes() > 0L)
                {
                    jsonobject2.put("audioBytes", (new StringBuilder(String.valueOf(((CdnCollectDataForPlay) (obj)).getAudioBytes()))).toString());
                } else
                {
                    jsonobject2.put("audioBytes", 0);
                }
                if (((CdnCollectDataForPlay) (obj)).getConnectedTime() >= 0.0F)
                {
                    jsonobject2.put("connectedTime", (new StringBuilder(String.valueOf(((CdnCollectDataForPlay) (obj)).getConnectedTime()))).toString());
                }
                if (((CdnCollectDataForPlay) (obj)).getTimestamp() >= 0L)
                {
                    jsonobject2.put("timestamp", ((CdnCollectDataForPlay) (obj)).getTimestamp());
                }
                if (((CdnCollectDataForPlay) (obj)).getRange() != null)
                {
                    jsonobject2.put("range", ((CdnCollectDataForPlay) (obj)).getRange());
                }
                if (((CdnCollectDataForPlay) (obj)).getFileSize() != null)
                {
                    jsonobject2.put("fileSize", ((CdnCollectDataForPlay) (obj)).getFileSize());
                }
                jsonobject2.put("timeout", ((CdnCollectDataForPlay) (obj)).isTimeout());
                jsonobject1.put("props", jsonobject2);
            }
            jsonarray.put(jsonobject1);
        } while (true);
    }

    public static CdnConfigModel getCdnConfigModel()
    {
        return mCdnConfigModel;
    }

    private static String getHostName(String s)
    {
        if (s != null && !s.trim().equals(""))
        {
            if ((s = Pattern.compile("(?<=//|)((\\w)+\\.)+\\w+").matcher(s)).find())
            {
                return s.group();
            }
        }
        return "";
    }

    public static String getUrlIp(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return null;
        }
        try
        {
            s = InetAddress.getByName(getHostName(s)).getHostAddress();
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return s;
    }

    public static float oneDecimal(float f, boolean flag)
    {
        if (f <= 0.0F)
        {
            return 0.0F;
        }
        String s;
        if (flag)
        {
            s = (new DecimalFormat(".0")).format(f);
        } else
        {
            if ((double)f < 0.10000000000000001D)
            {
                return 0.001F;
            }
            s = (new DecimalFormat(".000")).format(f / 1000F);
        }
        return Float.valueOf(s).floatValue();
    }

    public static void setCdnConfigModel(CdnConfigModel cdnconfigmodel)
    {
        mCdnConfigModel = cdnconfigmodel;
    }

    public static void statDownLoadCDN(CdnCollectDataForPlay cdncollectdataforplay, CdnConfigModel cdnconfigmodel)
    {
        if (cdncollectdataforplay != null && cdnconfigmodel != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        obj = new CdnEvent();
        ((CdnEvent) (obj)).setType("CDN");
        ((CdnEvent) (obj)).setProps(cdncollectdataforplay);
        ((CdnEvent) (obj)).setTs(System.currentTimeMillis());
        cdncollectdataforplay = new ArrayList();
        cdncollectdataforplay.add(obj);
        obj = ObjectTOJSON(cdncollectdataforplay);
        if (!TextUtils.isEmpty(cdnconfigmodel.getCdnUrl())) goto _L4; else goto _L3
_L3:
        cdncollectdataforplay = "http://xdcs-collector.test.ximalaya.com/api/v1/cdnAndroid";
_L8:
        Object obj1;
        mConn = (HttpURLConnection)(new URL(cdncollectdataforplay)).openConnection();
        obj1 = cdnconfigmodel.getCookie();
        if (obj1 == null) goto _L6; else goto _L5
_L5:
        String s;
        StringBuffer stringbuffer;
        Iterator iterator;
        cdncollectdataforplay = ((CdnCookie) (obj1)).getDoMain();
        s = ((CdnCookie) (obj1)).getPath();
        stringbuffer = new StringBuffer();
        obj1 = ((CdnCookie) (obj1)).getMap();
        iterator = ((HashMap) (obj1)).keySet().iterator();
_L9:
        if (iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_361;
        }
        mConn.setRequestProperty("Cookie", (new StringBuilder(String.valueOf(stringbuffer.toString()))).append("domain=").append(cdncollectdataforplay).append(";path=").append(s).toString());
_L6:
        mConn.setRequestProperty("user-agent", cdnconfigmodel.getUserAgent());
        mConn.setRequestProperty("Content-Length", String.valueOf(((String) (obj)).length()));
        mConn.setRequestProperty("Content-Type", "application/json");
        mConn.setRequestMethod("POST");
        mConn.setConnectTimeout(5000);
        mConn.setReadTimeout(5000);
        mConn.connect();
        os = mConn.getOutputStream();
        os.write(((String) (obj)).getBytes("utf-8"));
        os.flush();
        os.close();
        is = mConn.getInputStream();
        String s1;
        if (os != null)
        {
            try
            {
                os.close();
            }
            // Misplaced declaration of an exception variable
            catch (CdnCollectDataForPlay cdncollectdataforplay)
            {
                cdncollectdataforplay.printStackTrace();
            }
        }
        if (is != null)
        {
            try
            {
                is.close();
            }
            // Misplaced declaration of an exception variable
            catch (CdnCollectDataForPlay cdncollectdataforplay)
            {
                cdncollectdataforplay.printStackTrace();
            }
        }
        if (mConn == null) goto _L1; else goto _L7
_L7:
        mConn.disconnect();
        mConn = null;
        return;
_L4:
        cdncollectdataforplay = cdnconfigmodel.getCdnUrl();
          goto _L8
        s1 = (String)iterator.next();
        stringbuffer.append(s1).append("=").append((String)((HashMap) (obj1)).get(s1)).append(";");
          goto _L9
        cdncollectdataforplay;
        cdncollectdataforplay.printStackTrace();
        if (os != null)
        {
            try
            {
                os.close();
            }
            // Misplaced declaration of an exception variable
            catch (CdnCollectDataForPlay cdncollectdataforplay)
            {
                cdncollectdataforplay.printStackTrace();
            }
        }
        if (is != null)
        {
            try
            {
                is.close();
            }
            // Misplaced declaration of an exception variable
            catch (CdnCollectDataForPlay cdncollectdataforplay)
            {
                cdncollectdataforplay.printStackTrace();
            }
        }
        if (mConn == null) goto _L1; else goto _L10
_L10:
        mConn.disconnect();
        mConn = null;
        return;
        cdncollectdataforplay;
        if (os != null)
        {
            try
            {
                os.close();
            }
            // Misplaced declaration of an exception variable
            catch (CdnConfigModel cdnconfigmodel)
            {
                cdnconfigmodel.printStackTrace();
            }
        }
        if (is != null)
        {
            try
            {
                is.close();
            }
            // Misplaced declaration of an exception variable
            catch (CdnConfigModel cdnconfigmodel)
            {
                cdnconfigmodel.printStackTrace();
            }
        }
        if (mConn != null)
        {
            mConn.disconnect();
            mConn = null;
        }
        throw cdncollectdataforplay;
    }
}
