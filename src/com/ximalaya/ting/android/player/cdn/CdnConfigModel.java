// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player.cdn;


// Referenced classes of package com.ximalaya.ting.android.player.cdn:
//            CdnCookie

public class CdnConfigModel
{

    public int cdnNotWifiAlertRate;
    public int cdnNotWifiConnectTimeout;
    public String cdnUrl;
    public int cdnWifiAlertRate;
    public int cdnWifiConnectTimeout;
    public CdnCookie cookie;
    public int netType;
    public String userAgent;

    public CdnConfigModel()
    {
    }

    public int getCdnNotWifiAlertRate()
    {
        return cdnNotWifiAlertRate;
    }

    public int getCdnNotWifiConnectTimeout()
    {
        return cdnNotWifiConnectTimeout;
    }

    public String getCdnUrl()
    {
        return cdnUrl;
    }

    public int getCdnWifiAlertRate()
    {
        return cdnWifiAlertRate;
    }

    public int getCdnWifiConnectTimeout()
    {
        return cdnWifiConnectTimeout;
    }

    public CdnCookie getCookie()
    {
        return cookie;
    }

    public int getNetType()
    {
        return netType;
    }

    public String getUserAgent()
    {
        return userAgent;
    }

    public void setCdnNotWifiAlertRate(int i)
    {
        cdnNotWifiAlertRate = i;
    }

    public void setCdnNotWifiConnectTimeout(int i)
    {
        cdnNotWifiConnectTimeout = i;
    }

    public void setCdnUrl(String s)
    {
        cdnUrl = s;
    }

    public void setCdnWifiAlertRate(int i)
    {
        cdnWifiAlertRate = i;
    }

    public void setCdnWifiConnectTimeout(int i)
    {
        cdnWifiConnectTimeout = i;
    }

    public void setCookie(CdnCookie cdncookie)
    {
        cookie = cdncookie;
    }

    public void setNetType(int i)
    {
        netType = i;
    }

    public void setUserAgent(String s)
    {
        userAgent = s;
    }
}
