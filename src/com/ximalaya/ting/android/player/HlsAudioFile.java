// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import com.ximalaya.ting.android.player.model.JNIDataModel;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.ximalaya.ting.android.player:
//            HlsReadThread, XMediaplayerJNI, Logger, BufferItem

public class HlsAudioFile
{

    private LinkedBlockingQueue buffItemQueue;
    private boolean isRunning;
    private int lastIndex;
    private List mHlsPlayUrls;
    private HlsReadThread mHlsReadThread;
    private String mPlayUrl;
    private String mSourceUrl;
    private XMediaplayerJNI mXMediaplayerJNI;

    public HlsAudioFile(String s, XMediaplayerJNI xmediaplayerjni)
    {
        mHlsPlayUrls = new ArrayList();
        isRunning = false;
        lastIndex = -1;
        mSourceUrl = s;
        mXMediaplayerJNI = xmediaplayerjni;
    }

    private void startChacheFile()
    {
        if (mHlsReadThread == null || mHlsReadThread.isClose())
        {
            buffItemQueue = new LinkedBlockingQueue(3);
            mHlsReadThread = new HlsReadThread(this, mXMediaplayerJNI, mSourceUrl, buffItemQueue);
            isRunning = false;
        }
        if (!mHlsReadThread.isAlive() && !isRunning && getPlayIndex() >= 0)
        {
            isRunning = true;
            mHlsReadThread.start();
        }
        mHlsReadThread.notifyDownload();
    }

    public void addPlayUrls(String as[])
    {
        if (as != null && as.length > 0)
        {
            mHlsPlayUrls.addAll(Arrays.asList(as));
            startChacheFile();
        }
    }

    public int getCachePercent()
    {
        if (mHlsReadThread != null && getPlayUrlsLength() != 0)
        {
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls mHlsReadThread.getCacheIndex():")).append(mHlsReadThread.getCacheIndex()).append("getPlayUrlsLength():").append(getPlayUrlsLength()).toString());
            int i = (int)(((float)(mHlsReadThread.getCacheIndex() - 1) / (float)getPlayUrlsLength()) * 100F);
            if (i >= 0)
            {
                return i;
            }
        }
        return 0;
    }

    public List getHlsPlayUrls()
    {
        return mHlsPlayUrls;
    }

    public int getPlayIndex()
    {
        if (mPlayUrl == null)
        {
            return -1;
        } else
        {
            return mHlsPlayUrls.indexOf(mPlayUrl);
        }
    }

    public String getPlayUrl()
    {
        return mPlayUrl;
    }

    public String getPlayUrl(int i)
    {
        if (i < mHlsPlayUrls.size())
        {
            return (String)mHlsPlayUrls.get(i);
        } else
        {
            return null;
        }
    }

    public int getPlayUrlsLength()
    {
        return mHlsPlayUrls.size();
    }

    public long readData(JNIDataModel jnidatamodel)
    {
        boolean flag = false;
        boolean flag1 = false;
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("hls readData callback:")).append(System.currentTimeMillis()).toString());
        mPlayUrl = jnidatamodel.filePath;
        if (!mXMediaplayerJNI.getAudioType().equals(XMediaplayerJNI.AudioType.HLS_FILE))
        {
            int i = getPlayIndex();
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("HlsReadThread notify555 curIndex:")).append(i).append("lastIndex:").append(lastIndex).toString());
            BufferItem bufferitem;
            long l;
            if (lastIndex + 1 == i)
            {
                flag = flag1;
            } else
            {
                flag = true;
            }
            lastIndex = i;
        }
        startChacheFile();
        if (flag)
        {
            buffItemQueue = new LinkedBlockingQueue(3);
            mHlsReadThread.resetIndex(buffItemQueue);
        }
        try
        {
            bufferitem = (BufferItem)buffItemQueue.poll(30000L, TimeUnit.MILLISECONDS);
            Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT 3");
        }
        // Misplaced declaration of an exception variable
        catch (JNIDataModel jnidatamodel)
        {
            return -1L;
        }
        if (bufferitem != null)
        {
            break MISSING_BLOCK_LABEL_186;
        }
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT timeout item null");
        return -1L;
        jnidatamodel.buf = bufferitem.getBuffer().array();
        jnidatamodel.fileSize = jnidatamodel.buf.length;
        l = jnidatamodel.fileSize;
        return l;
    }

    public void release()
    {
        if (mHlsReadThread != null)
        {
            mHlsReadThread.close();
        }
        if (buffItemQueue != null)
        {
            buffItemQueue.clear();
        }
    }

    public void setPlayUrl(String s)
    {
        mPlayUrl = s;
    }
}
