// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.concurrent.LinkedBlockingQueue;

// Referenced classes of package com.ximalaya.ting.android.player:
//            BufferItem, HlsAudioFile, Logger, XMediaplayerJNI, 
//            XMediaPlayerConstants, PlayerUtil, HlsDownloadThread

public class HlsReadThread extends Thread
{

    private static final String TAG = "dl_hls";
    private volatile LinkedBlockingQueue buffItemQueue;
    private volatile int curIndex;
    private volatile int fromIndex;
    private volatile boolean isResetIndex;
    private boolean isStop;
    public volatile boolean isWaiting;
    private int mDownloadIndex;
    private HlsAudioFile mHlsAudioFile;
    private String mSourceUrl;
    private XMediaplayerJNI mXMediaplayerJNI;
    private volatile Object synKey;
    public Object synObject;
    public Object synWaitObject;

    public HlsReadThread(HlsAudioFile hlsaudiofile, XMediaplayerJNI xmediaplayerjni, String s, LinkedBlockingQueue linkedblockingqueue)
    {
        isStop = false;
        synObject = new Object();
        synWaitObject = new Object();
        isWaiting = false;
        synKey = new Object();
        mHlsAudioFile = hlsaudiofile;
        mXMediaplayerJNI = xmediaplayerjni;
        mSourceUrl = s;
        buffItemQueue = linkedblockingqueue;
        isResetIndex = true;
    }

    private void putItem(BufferItem bufferitem)
    {
        if (!isResetIndex)
        {
            Logger.log("dl_hls", (new StringBuilder("putItem url:")).append(mHlsAudioFile.getPlayUrl(bufferitem.getIndex())).append(" item Index:").append(bufferitem.getIndex()).toString());
            Logger.log("dl_hls", (new StringBuilder("putItem buffItemQueue.size()0:")).append(buffItemQueue.size()).toString());
            try
            {
                buffItemQueue.put(bufferitem);
            }
            // Misplaced declaration of an exception variable
            catch (BufferItem bufferitem) { }
            Logger.log("dl_hls", (new StringBuilder("putItem buffItemQueue.size()1:")).append(buffItemQueue.size()).toString());
            return;
        } else
        {
            Logger.log("dl_hls", (new StringBuilder("putItem buffItemQueue.size()2:")).append(buffItemQueue.size()).toString());
            return;
        }
    }

    private int readChunkData(File file, BufferItem bufferitem)
    {
        byte abyte0[] = null;
        file = new RandomAccessFile(file, "rw");
        abyte0 = new byte[(int)file.length()];
        int i = 0;
_L2:
        int k = file.read(abyte0, i, abyte0.length - i);
        int j;
        j = i + k;
        i = j;
        if (k > 0) goto _L2; else goto _L1
_L1:
        bufferitem.setBuffer(abyte0);
        file.close();
        if (file != null)
        {
            try
            {
                file.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file)
            {
                return j;
            }
        }
        return j;
        file;
        file = abyte0;
_L6:
        if (file != null)
        {
            try
            {
                file.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file) { }
        }
        return -1;
        bufferitem;
        file = null;
_L4:
        if (file != null)
        {
            try
            {
                file.close();
            }
            // Misplaced declaration of an exception variable
            catch (File file) { }
        }
        throw bufferitem;
        bufferitem;
        if (true) goto _L4; else goto _L3
_L3:
        bufferitem;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void close()
    {
        isStop = true;
        if (buffItemQueue != null)
        {
            buffItemQueue.clear();
        }
        Logger.log(XMediaplayerJNI.Tag, "HlsReadThread hls readData close");
    }

    public int getCacheIndex()
    {
        if (mDownloadIndex == 0)
        {
            return mHlsAudioFile.getPlayIndex();
        } else
        {
            return mDownloadIndex;
        }
    }

    public boolean isClose()
    {
        return isStop;
    }

    public void notifyDownload()
    {
        if (isWaiting)
        {
            synchronized (synWaitObject)
            {
                isWaiting = false;
                synWaitObject.notify();
            }
            return;
        } else
        {
            return;
        }
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void resetIndex(LinkedBlockingQueue linkedblockingqueue)
    {
        synchronized (synKey)
        {
            Logger.log("dl_hls", (new StringBuilder("resetIndex bq.size()0:")).append(linkedblockingqueue.size()).toString());
            isResetIndex = true;
            fromIndex = mHlsAudioFile.getPlayIndex();
            if (buffItemQueue != null)
            {
                buffItemQueue.clear();
            }
            buffItemQueue = linkedblockingqueue;
            Logger.log("dl_hls", (new StringBuilder("resetIndex bq.size()1:")).append(linkedblockingqueue.size()).toString());
        }
        return;
        linkedblockingqueue;
        obj;
        JVM INSTR monitorexit ;
        throw linkedblockingqueue;
    }

    public void run()
    {
        fromIndex = mHlsAudioFile.getPlayIndex();
        isResetIndex = true;
_L6:
        if (!isStop && mXMediaplayerJNI.getPlayUrl().equals(mSourceUrl)) goto _L2; else goto _L1
_L1:
        isStop = true;
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("HlsReadThread isStop:")).append(isStop).append("cacheIndex:").append(curIndex).toString());
        return;
_L2:
        synchronized (synKey)
        {
            if (isResetIndex)
            {
                Logger.log("dl_hls", (new StringBuilder("resetIndex isResetIndex buffItemQueue.size():")).append(buffItemQueue.size()).toString());
                isResetIndex = false;
                curIndex = fromIndex;
                mDownloadIndex = fromIndex;
            }
        }
        if (curIndex >= mHlsAudioFile.getPlayUrlsLength() && !mXMediaplayerJNI.getAudioType().equals(XMediaplayerJNI.AudioType.HLS_FILE) && !isResetIndex) goto _L1; else goto _L3
_L3:
        int i;
        if (mDownloadIndex < curIndex)
        {
            mDownloadIndex = curIndex;
        }
        i = XMediaPlayerConstants.DOWNLOAD_QUEUE_SIZE;
_L8:
        if (mDownloadIndex - curIndex < i - 3 && buffItemQueue.size() >= 3 && !isStop && mDownloadIndex < mHlsAudioFile.getPlayUrlsLength() && !isResetIndex)
        {
            break MISSING_BLOCK_LABEL_389;
        }
        if (isStop) goto _L1; else goto _L4
_L4:
        if (isResetIndex) goto _L6; else goto _L5
_L5:
        obj = mHlsAudioFile.getPlayUrl(curIndex);
        Logger.log("dl_hls", (new StringBuilder("HlsReadThread downUrl0:")).append(((String) (obj))).append("    cacheIndex:").append(curIndex).append("getPlayUrlsLength:").append(mHlsAudioFile.getPlayUrlsLength()).toString());
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_612;
        }
        if (!mXMediaplayerJNI.getAudioType().equals(XMediaplayerJNI.AudioType.HLS_FILE)) goto _L1; else goto _L7
_L7:
        obj = synWaitObject;
        obj;
        JVM INSTR monitorenter ;
        isWaiting = true;
        synWaitObject.wait();
          goto _L6
        Object obj1;
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        obj = mHlsAudioFile.getPlayUrl(mDownloadIndex);
        if (!PlayerUtil.isDownloadHlsTs(((String) (obj))))
        {
            if ((new HlsDownloadThread(((String) (obj)), null)).download() > 0)
            {
                Logger.log("dl_hls", (new StringBuilder("url:")).append(((String) (obj))).append(" downloadIndex:").append(mDownloadIndex).append("\u4E0B\u8F7D\u5E76\u4E14\u7F13\u5B58\u6210\u529F1").toString());
                mDownloadIndex = mDownloadIndex + 1;
                mXMediaplayerJNI.onBufferingUpdateInner(mHlsAudioFile.getCachePercent());
            } else
            {
                Logger.log("dl_hls", (new StringBuilder("url:")).append(((String) (obj))).append(" downloadIndex:").append(mDownloadIndex).append("\u4E0B\u8F7D\u5931\u8D25error1").toString());
            }
        } else
        {
            Logger.log("dl_hls", (new StringBuilder("url:")).append(((String) (obj))).append(" downloadIndex:").append(mDownloadIndex).append("\u672C\u5730\u5DF2\u7F13\u51B2").toString());
            mDownloadIndex = mDownloadIndex + 1;
        }
        Logger.log("dl_hls", (new StringBuilder("getCachePercent percent mDownloadIndex:")).append(mDownloadIndex).toString());
          goto _L8
        exception;
        exception.printStackTrace();
          goto _L6
label0:
        {
            if (!PlayerUtil.isDownloadHlsTs(((String) (obj))))
            {
                break MISSING_BLOCK_LABEL_806;
            }
            Logger.log("dl_hls", (new StringBuilder("url:")).append(((String) (obj))).append(" curIndex:").append(curIndex).append("\u7F13\u5B58\u547D\u4E2D\u6210\u529F").toString());
            BufferItem bufferitem = new BufferItem();
            bufferitem.setIndex(curIndex);
            if (readChunkData(new File(PlayerUtil.getHlsFilePath(((String) (obj)))), bufferitem) <= 0)
            {
                break label0;
            }
            Logger.log("dl_hls", (new StringBuilder("url:")).append(((String) (obj))).append(" curIndex:").append(curIndex).append("\u7F13\u5B58\u83B7\u53D6\u6210\u529F").toString());
            putItem(bufferitem);
            curIndex = curIndex + 1;
            mXMediaplayerJNI.onBufferingUpdateInner(mHlsAudioFile.getCachePercent());
        }
        if (true) goto _L6; else goto _L9
_L9:
        Logger.log("dl_hls", (new StringBuilder("url:")).append(((String) (obj))).append(" curIndex:").append(curIndex).append("\u7F13\u5B58\u83B7\u53D6\u5931\u8D25error").toString());
label1:
        {
            BufferItem bufferitem1 = new BufferItem();
            bufferitem1.setIndex(curIndex);
            if ((new HlsDownloadThread(((String) (obj)), bufferitem1)).download() <= 0)
            {
                break label1;
            }
            putItem(bufferitem1);
            Logger.log("dl_hls", (new StringBuilder("url:")).append(((String) (obj))).append(" curIndex:").append(curIndex).append("\u4E0B\u8F7D\u5E76\u4E14\u7F13\u5B58\u6210\u529F2").toString());
            curIndex = curIndex + 1;
            mXMediaplayerJNI.onBufferingUpdateInner(mHlsAudioFile.getCachePercent());
        }
        if (true) goto _L6; else goto _L10
_L10:
        Logger.log("dl_hls", (new StringBuilder("url:")).append(((String) (obj))).append(" curIndex:").append(curIndex).append("\u4E0B\u8F7D\u5E76\u4E14\u7F13\u5B58\u5931\u8D252").toString());
          goto _L1
    }
}
