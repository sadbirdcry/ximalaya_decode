// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import java.io.FileDescriptor;
import java.io.IOException;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaplayerImpl, PriorityHandlerThread, XMediaPlayer, Logger, 
//            PlayerUtil, XMediaplayerJNI

public class SMediaPlayer
    implements android.os.Handler.Callback, XMediaplayerImpl
{

    private static final int MSG_COMPLETE = 9;
    private static final int MSG_INCREMENTAL_PREPARE = 2;
    private static final int MSG_PAUSE = 3;
    private static final int MSG_PREPARE = 1;
    private static final int MSG_RELEASE = 5;
    private static final int MSG_RESET = 7;
    private static final int MSG_SEEK_TO = 6;
    private static final int MSG_SET_DATA_SOURCE = 8;
    private static final int MSG_START = 0;
    private static final int MSG_STOP = 4;
    private boolean hasSetDataSource;
    private boolean isBuffering;
    private boolean isPrepareing;
    private boolean isSeeking;
    private XMediaplayerJNI.AudioType mAudioType;
    private int mCurrentPosition;
    private int mDuration;
    private Handler mEventHandler;
    private final Handler mHandler;
    private final HandlerThread mInternalPlaybackThread = new PriorityHandlerThread((new StringBuilder(String.valueOf(getClass().getSimpleName()))).append(":Handler").toString(), -16);
    public MediaPlayer mMediaPlayer;
    private XMediaPlayer.OnPositionChangeListener mOnPositionChangeListener;
    private int mPlayState;
    private String mPlayUrl;
    private Runnable runnable;
    private long time;

    public SMediaPlayer()
    {
        isSeeking = false;
        isBuffering = false;
        isPrepareing = false;
        mCurrentPosition = 0;
        mDuration = 0;
        time = System.currentTimeMillis();
        mAudioType = XMediaplayerJNI.AudioType.NORMAL_FILE;
        hasSetDataSource = false;
        runnable = new _cls1();
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setAudioStreamType(3);
        mEventHandler = new Handler(Looper.myLooper());
        mPlayState = 1;
        mInternalPlaybackThread.start();
        mHandler = new Handler(mInternalPlaybackThread.getLooper(), this);
    }

    private void printTime(String s)
    {
        Logger.log(XMediaPlayer.Tag, (new StringBuilder(String.valueOf(s))).append(" nowtime:").append(System.currentTimeMillis()).append("long:").append(System.currentTimeMillis() - time).toString());
        time = System.currentTimeMillis();
    }

    private void resetState()
    {
        isSeeking = false;
        isBuffering = false;
        isPrepareing = false;
    }

    public XMediaplayerJNI.AudioType getAudioType()
    {
        return mAudioType;
    }

    public int getCurrentPosition()
    {
        if (!isSeeking && !isBuffering && !isPrepareing)
        {
            mCurrentPosition = mMediaPlayer.getCurrentPosition();
        }
        return mCurrentPosition;
    }

    public int getDuration()
    {
        if (!isSeeking && !isBuffering && !isPrepareing || mDuration == 0)
        {
            mDuration = mMediaPlayer.getDuration();
        }
        return mDuration;
    }

    public int getPlayState()
    {
        return mPlayState;
    }

    public boolean handleMessage(Message message)
    {
        message.what;
        JVM INSTR tableswitch 0 8: default 290
    //                   0 82
    //                   1 56
    //                   2 290
    //                   3 108
    //                   4 122
    //                   5 137
    //                   6 198
    //                   7 166
    //                   8 219;
           goto _L1 _L2 _L3 _L1 _L4 _L5 _L6 _L7 _L8 _L9
_L3:
        mPlayState = 2;
        printTime("MSG_PREPARE start");
        mMediaPlayer.prepareAsync();
        printTime("MSG_PREPARE end");
        return true;
_L2:
        Object obj;
        int i;
        try
        {
            mPlayState = 4;
            printTime("MSG_START start");
            mMediaPlayer.start();
            printTime("MSG_START end");
        }
        // Misplaced declaration of an exception variable
        catch (Message message)
        {
            return true;
        }
        return true;
_L4:
        mPlayState = 5;
        mMediaPlayer.pause();
        return true;
_L5:
        mPlayState = 6;
        mMediaPlayer.stop();
        return true;
_L6:
        mPlayState = 9;
        printTime("MSG_RELEASE start");
        mMediaPlayer.release();
        printTime("MSG_RELEASE end");
        return true;
_L8:
        mPlayState = 0;
        printTime("MSG_RESET start");
        mMediaPlayer.reset();
        resetState();
        printTime("MSG_RESET end");
        return true;
_L7:
        i = ((Integer)message.obj).intValue();
        mMediaPlayer.seekTo(i);
        return true;
_L9:
        printTime("MSG_SET_DATA_SOURCE start");
        obj = message.obj;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_249;
        }
        mMediaPlayer.setDataSource(message.obj.toString());
_L10:
        printTime("MSG_SET_DATA_SOURCE end");
        return true;
        message;
        message.printStackTrace();
          goto _L10
        message;
        message.printStackTrace();
          goto _L10
        message;
        message.printStackTrace();
          goto _L10
        message;
        message.printStackTrace();
          goto _L10
_L1:
        return false;
    }

    public boolean isPlaying()
    {
        if (!isSeeking && !isBuffering && !isPrepareing)
        {
            return mMediaPlayer.isPlaying();
        } else
        {
            return false;
        }
    }

    public boolean isUseSystemPlayer()
    {
        return true;
    }

    public void pause()
    {
        mHandler.obtainMessage(3).sendToTarget();
    }

    public void prepareAsync()
    {
        isPrepareing = true;
        printTime("prepareAsync");
        mHandler.obtainMessage(1).sendToTarget();
    }

    public void release()
    {
        printTime("release");
        resetState();
        mHandler.obtainMessage(5).sendToTarget();
        mOnPositionChangeListener = null;
        mInternalPlaybackThread.getLooper().quit();
        mInternalPlaybackThread.interrupt();
    }

    public void removeProxy()
    {
        PlayerUtil.setProxyHost(null);
        PlayerUtil.setProxyPort(0);
        PlayerUtil.setAuthorization(null);
    }

    public void reset()
    {
        mDuration = 0;
        if (hasSetDataSource)
        {
            printTime("reset");
            mHandler.obtainMessage(7).sendToTarget();
        }
    }

    public void seekTo(int i)
    {
        isSeeking = true;
        mHandler.obtainMessage(6, Integer.valueOf(i)).sendToTarget();
    }

    public void setAudioStreamType(int i)
    {
        mMediaPlayer.setAudioStreamType(i);
    }

    public void setDataSource(FileDescriptor filedescriptor, String s)
    {
        hasSetDataSource = true;
        try
        {
            mMediaPlayer.setDataSource(filedescriptor);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (FileDescriptor filedescriptor)
        {
            filedescriptor.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (FileDescriptor filedescriptor)
        {
            filedescriptor.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (FileDescriptor filedescriptor)
        {
            filedescriptor.printStackTrace();
        }
    }

    public void setDataSource(String s)
    {
        printTime("setDataSource");
        mDuration = 0;
        if (s.contains("m3u8"))
        {
            mAudioType = XMediaplayerJNI.AudioType.M3U8_FILE;
        } else
        {
            mAudioType = XMediaplayerJNI.AudioType.NORMAL_FILE;
        }
        hasSetDataSource = true;
        mHandler.obtainMessage(8, s).sendToTarget();
    }

    public void setDownloadBufferSize(long l)
    {
    }

    public void setOnBufferingUpdateListener(final XMediaPlayer.OnBufferingUpdateListener listener)
    {
        mMediaPlayer.setOnBufferingUpdateListener(new _cls2());
    }

    public void setOnCompletionListener(final XMediaPlayer.OnCompletionListener listener)
    {
        mMediaPlayer.setOnCompletionListener(new _cls3());
    }

    public void setOnErrorListener(final XMediaPlayer.OnErrorListener listener)
    {
        mMediaPlayer.setOnErrorListener(new _cls4());
    }

    public void setOnInfoListener(final XMediaPlayer.OnInfoListener listener)
    {
        Logger.log(XMediaplayerJNI.Tag, "SMediaPlayer setOnInfoListener");
        mMediaPlayer.setOnInfoListener(new _cls5());
    }

    public void setOnPositionChangeListener(XMediaPlayer.OnPositionChangeListener onpositionchangelistener)
    {
        mOnPositionChangeListener = onpositionchangelistener;
        if (mOnPositionChangeListener != null)
        {
            mEventHandler.postDelayed(runnable, 1000L);
        }
    }

    public void setOnPreparedListener(final XMediaPlayer.OnPreparedListener listener)
    {
        mMediaPlayer.setOnPreparedListener(new _cls6());
    }

    public void setOnSeekCompleteListener(final XMediaPlayer.OnSeekCompleteListener listener)
    {
        mMediaPlayer.setOnSeekCompleteListener(new _cls7());
    }

    public void setProxy(String s, int i, String s1)
    {
        PlayerUtil.setProxyHost(s);
        PlayerUtil.setProxyPort(i);
        PlayerUtil.setAuthorization(s1);
    }

    public void setVolume(float f, float f1)
    {
        mMediaPlayer.setVolume(f, f1);
    }

    public void setWakeMode(Context context, int i)
    {
        mMediaPlayer.setWakeMode(context, i);
    }

    public void start()
    {
        printTime("start");
        if (mPlayState == 5 || mPlayState == 3)
        {
            mHandler.obtainMessage(0).sendToTarget();
        }
    }

    public void stop()
    {
        mHandler.obtainMessage(4).sendToTarget();
    }















    private class _cls1
        implements Runnable
    {

        final SMediaPlayer this$0;

        public void run()
        {
            if (mOnPositionChangeListener == null)
            {
                return;
            }
            if (mPlayState == 4 && !isBuffering && !isSeeking)
            {
                mOnPositionChangeListener.onPositionChange(SMediaPlayer.this, getCurrentPosition());
            }
            mEventHandler.postDelayed(runnable, 1000L);
        }

        _cls1()
        {
            this$0 = SMediaPlayer.this;
            super();
        }
    }


    private class _cls2
        implements android.media.MediaPlayer.OnBufferingUpdateListener
    {

        final SMediaPlayer this$0;
        private final XMediaPlayer.OnBufferingUpdateListener val$listener;

        public void onBufferingUpdate(MediaPlayer mediaplayer, int i)
        {
            listener.onBufferingUpdate(SMediaPlayer.this, i);
        }

        _cls2()
        {
            this$0 = SMediaPlayer.this;
            listener = onbufferingupdatelistener;
            super();
        }
    }


    private class _cls3
        implements android.media.MediaPlayer.OnCompletionListener
    {

        final SMediaPlayer this$0;
        private final XMediaPlayer.OnCompletionListener val$listener;

        public void onCompletion(MediaPlayer mediaplayer)
        {
            mPlayState = 11;
            listener.onCompletion(SMediaPlayer.this);
        }

        _cls3()
        {
            this$0 = SMediaPlayer.this;
            listener = oncompletionlistener;
            super();
        }
    }


    private class _cls4
        implements android.media.MediaPlayer.OnErrorListener
    {

        final SMediaPlayer this$0;
        private final XMediaPlayer.OnErrorListener val$listener;

        public boolean onError(MediaPlayer mediaplayer, int i, int j)
        {
            resetState();
            if (listener != null)
            {
                boolean flag = listener.onError(SMediaPlayer.this, i, j);
                if (!flag)
                {
                    mPlayState = 8;
                }
                return flag;
            } else
            {
                return false;
            }
        }

        _cls4()
        {
            this$0 = SMediaPlayer.this;
            listener = onerrorlistener;
            super();
        }
    }


    private class _cls5
        implements android.media.MediaPlayer.OnInfoListener
    {

        final SMediaPlayer this$0;
        private final XMediaPlayer.OnInfoListener val$listener;

        public boolean onInfo(MediaPlayer mediaplayer, int i, int j)
        {
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("SMediaPlayer OnInfoListener:")).append(i).append("extra:").append(j).toString());
            i;
            JVM INSTR tableswitch 701 702: default 56
        //                       701 73
        //                       702 84;
               goto _L1 _L2 _L3
_L1:
            return listener.onInfo(SMediaPlayer.this, 10, i);
_L2:
            isBuffering = true;
            continue; /* Loop/switch isn't completed */
_L3:
            isBuffering = false;
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls5()
        {
            this$0 = SMediaPlayer.this;
            listener = oninfolistener;
            super();
        }
    }


    private class _cls6
        implements android.media.MediaPlayer.OnPreparedListener
    {

        final SMediaPlayer this$0;
        private final XMediaPlayer.OnPreparedListener val$listener;

        public void onPrepared(MediaPlayer mediaplayer)
        {
            printTime("onPrepared");
            mPlayState = 3;
            isPrepareing = false;
            mDuration = mMediaPlayer.getDuration();
            mCurrentPosition = mMediaPlayer.getCurrentPosition();
            listener.onPrepared(SMediaPlayer.this);
        }

        _cls6()
        {
            this$0 = SMediaPlayer.this;
            listener = onpreparedlistener;
            super();
        }
    }


    private class _cls7
        implements android.media.MediaPlayer.OnSeekCompleteListener
    {

        final SMediaPlayer this$0;
        private final XMediaPlayer.OnSeekCompleteListener val$listener;

        public void onSeekComplete(MediaPlayer mediaplayer)
        {
            isSeeking = false;
            listener.onSeekComplete(SMediaPlayer.this);
        }

        _cls7()
        {
            this$0 = SMediaPlayer.this;
            listener = onseekcompletelistener;
            super();
        }
    }

}
