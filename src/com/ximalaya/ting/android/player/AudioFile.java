// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.BitSet;

// Referenced classes of package com.ximalaya.ting.android.player:
//            Logger, MD5, FileDesc

public class AudioFile
{

    private static final String TAG = "dl_mp3";
    private ByteBuffer bBuffer;
    private String dirPath;
    private FileDesc fileInfo;
    private String fileName;
    private String mUrl;

    private AudioFile(String s, String s1)
        throws FileNotFoundException, IOException
    {
        mUrl = null;
        Logger.log("dl_mp3", "======================AudioFile Constructor()");
        dirPath = s;
        fileName = MD5.md5(s1);
        mUrl = s1;
        fileInfo = new FileDesc(s, s1);
    }

    public static AudioFile getAudioFile(String s, String s1)
        throws FileNotFoundException, IOException
    {
        return new AudioFile(s, s1);
    }

    private int readChunkData(int i, byte abyte0[], int j, int k)
        throws IOException
    {
        this;
        JVM INSTR monitorenter ;
        RandomAccessFile randomaccessfile = new RandomAccessFile((new StringBuilder(String.valueOf(dirPath))).append("/").append(fileName).append(".chunk").toString(), "r");
        Logger.log("dl_mp3", (new StringBuilder("======================readChunkData0(")).append(i).append(":").append(randomaccessfile.length()).append(")").toString());
        randomaccessfile.seek(i);
        i = randomaccessfile.read(abyte0, j, k);
        randomaccessfile.close();
        this;
        JVM INSTR monitorexit ;
        return i;
        abyte0;
        throw abyte0;
    }

    private int writeChunkData(byte abyte0[], int i, int j)
    {
        this;
        JVM INSTR monitorenter ;
        try
        {
            RandomAccessFile randomaccessfile = new RandomAccessFile((new StringBuilder(String.valueOf(dirPath))).append("/").append(fileName).append(".chunk").toString(), "rw");
            randomaccessfile.seek(fileInfo.getDownloadedChunks() * 0x10000);
            randomaccessfile.write(abyte0, i, j);
            randomaccessfile.close();
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            j = -1;
        }
        finally
        {
            this;
        }
        return j;
        throw abyte0;
    }

    public String getDirPath()
    {
        return dirPath;
    }

    public FileDesc getFileInfo()
    {
        this;
        JVM INSTR monitorenter ;
        FileDesc filedesc = fileInfo;
        this;
        JVM INSTR monitorexit ;
        return filedesc;
        Exception exception;
        exception;
        throw exception;
    }

    public String getFileName()
    {
        return fileName;
    }

    public String getFilePath()
    {
        return (new StringBuilder(String.valueOf(dirPath))).append("/").append(fileName).toString();
    }

    public String getUrl()
    {
        return mUrl;
    }

    public ByteBuffer getbBuffer()
    {
        return bBuffer;
    }

    public final boolean isChunkExists(int i)
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag = fileInfo.isChunkDownloaded(i);
        if (flag)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        this;
        JVM INSTR monitorexit ;
        return flag;
        Exception exception;
        exception;
        throw exception;
    }

    public int readChunkData(int i, int j, byte abyte0[], int k)
        throws IOException
    {
        this;
        JVM INSTR monitorenter ;
        if (fileInfo.chunkExist.get(i)) goto _L2; else goto _L1
_L1:
        Logger.log("dl_mp3", (new StringBuilder("fileInfo.chunkExist.get(")).append(i).append(")false").toString());
        i = -1;
_L4:
        this;
        JVM INSTR monitorexit ;
        return i;
_L2:
        j = readChunkData(((Integer)fileInfo.chunkOffset.get(i)).intValue() * j, abyte0, k, j);
        Logger.log("dl_mp3", (new StringBuilder("======================readChunkData(")).append(i).append(":").append(j).append(")").toString());
        i = j;
        if (true) goto _L4; else goto _L3
_L3:
        abyte0;
        throw abyte0;
    }

    public void setbBuffer(ByteBuffer bytebuffer)
    {
        bBuffer = bytebuffer;
    }

    public void writeChunkData(int i, byte abyte0[], int j, int k)
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag = fileInfo.isChunkDownloaded(i);
        if (!flag) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (writeChunkData(abyte0, j, k) > 0)
        {
            fileInfo.update(i);
        }
        if (true) goto _L1; else goto _L3
_L3:
        abyte0;
        throw abyte0;
    }

    public void writeFails(int i)
    {
        this;
        JVM INSTR monitorenter ;
        fileInfo.valid = false;
        fileInfo.statusCode = i;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }
}
