// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.LinkedBlockingQueue;

// Referenced classes of package com.ximalaya.ting.android.player:
//            AudioFile, BufferItem, Logger, DownloadThread, 
//            FileDesc, XMediaplayerJNI, XMediaPlayerConstants

public class ReadThread extends Thread
{

    private static final String TAG = "dl_mp3";
    private volatile LinkedBlockingQueue buffItemQueue;
    private volatile int curIndex;
    private DownloadThread currentDownload;
    private volatile int fromIndex;
    private volatile boolean isResetIndex;
    private int mDownloadIndex;
    private AudioFile mFile;
    private XMediaplayerJNI mXMediaplayerJNI;
    private volatile boolean stopFlag;
    private volatile Object synKey;

    public ReadThread(AudioFile audiofile, int i, LinkedBlockingQueue linkedblockingqueue, XMediaplayerJNI xmediaplayerjni)
    {
        curIndex = 0;
        synKey = new Object();
        mFile = audiofile;
        fromIndex = i;
        buffItemQueue = linkedblockingqueue;
        stopFlag = false;
        isResetIndex = true;
        mXMediaplayerJNI = xmediaplayerjni;
    }

    private void putItem(BufferItem bufferitem)
        throws InterruptedException
    {
        if (!isResetIndex)
        {
            Logger.log("dl_mp3", (new StringBuilder("putItem url:")).append(mFile.getUrl()).append(" item Index:").append(bufferitem.getIndex()).toString());
            Logger.log("dl_mp3", (new StringBuilder("resetIndex count3:")).append(buffItemQueue.size()).toString());
            buffItemQueue.put(bufferitem);
            Logger.log("dl_mp3", (new StringBuilder("resetIndex count4:")).append(buffItemQueue.size()).toString());
            return;
        } else
        {
            Logger.log("dl_mp3", (new StringBuilder("resetIndex count5:")).append(buffItemQueue.size()).toString());
            return;
        }
    }

    private BufferItem readChunk(int i)
    {
        ByteBuffer bytebuffer = ByteBuffer.allocate(0x10000);
        BufferItem bufferitem;
        if (mFile.readChunkData(i, 0x10000, bytebuffer.array(), 0) != 0x10000)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        bufferitem = new BufferItem();
        bufferitem.setBuffer(bytebuffer);
        bufferitem.setIndex(i);
        return bufferitem;
        IOException ioexception;
        ioexception;
        return null;
    }

    public void close()
    {
        stopFlag = true;
        buffItemQueue.clear();
        if (currentDownload != null)
        {
            currentDownload.close();
        }
        mXMediaplayerJNI = null;
    }

    public AudioFile getAudioFile()
    {
        return mFile;
    }

    public int getCacheIndex()
    {
        return mDownloadIndex;
    }

    public int getCachePercent()
    {
        int j = (int)(((float)(mDownloadIndex - 1) / (float)mFile.getFileInfo().getComChunkNum()) * 100F);
        Logger.log("dl_mp3", (new StringBuilder("getCachePercent percent:")).append(j).append(" mDownloadIndex:").append(mDownloadIndex).append("getComChunkNum:").append(mFile.getFileInfo().getComChunkNum()).toString());
        int i = j;
        if (j < 0)
        {
            i = 0;
        }
        return i;
    }

    public boolean isClosed()
    {
        return stopFlag;
    }

    public void resetIndex(int i, LinkedBlockingQueue linkedblockingqueue)
    {
        synchronized (synKey)
        {
            Logger.log("dl_mp3", (new StringBuilder("resetIndex count0:")).append(linkedblockingqueue.size()).toString());
            isResetIndex = true;
            fromIndex = i;
            if (buffItemQueue != null)
            {
                buffItemQueue.clear();
            }
            buffItemQueue = linkedblockingqueue;
            Logger.log("dl_mp3", (new StringBuilder("resetIndex count1:")).append(linkedblockingqueue.size()).toString());
        }
        return;
        linkedblockingqueue;
        obj;
        JVM INSTR monitorexit ;
        throw linkedblockingqueue;
    }

    public void run()
    {
        Logger.log("dl_mp3", "======================ReadThread run() start");
_L10:
        if (stopFlag) goto _L2; else goto _L1
_L1:
        boolean flag = mXMediaplayerJNI.getPlayUrl().equals(mFile.getFileInfo().getUrl());
        if (flag) goto _L3; else goto _L2
_L2:
        stopFlag = true;
        Logger.log("dl_mp3", "======================ReadThread run() end");
        return;
_L3:
        Logger.log("dl_mp3", (new StringBuilder("======================ReadThread while(")).append(curIndex).append(":").append(mFile.getFileInfo().getComChunkNum()).append(")").toString());
        synchronized (synKey)
        {
            Logger.log("dl_mp3", (new StringBuilder("resetIndex count6:")).append(buffItemQueue.size()).toString());
            if (isResetIndex)
            {
                isResetIndex = false;
                curIndex = fromIndex;
                mDownloadIndex = fromIndex;
            }
            Logger.log("dl_mp3", (new StringBuilder("resetIndex count7:")).append(buffItemQueue.size()).toString());
        }
        obj = mFile.getFileInfo();
        if (((FileDesc) (obj)).isValid())
        {
            break MISSING_BLOCK_LABEL_286;
        }
        obj = new BufferItem();
        obj.fails = true;
        obj.errorCode = mFile.getFileInfo().statusCode;
        try
        {
            buffItemQueue.put(obj);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            try
            {
                ((InterruptedException) (obj)).printStackTrace();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                Logger.log("dl_mp3", (new StringBuilder("ReadThread Exception:")).append(((Exception) (obj)).getMessage()).toString());
            }
        }
          goto _L2
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        if (curIndex < ((FileDesc) (obj)).getComChunkNum()) goto _L5; else goto _L4
_L4:
        BufferItem bufferitem3;
        bufferitem3 = new BufferItem();
        bufferitem3.setBuffer(ByteBuffer.allocate(0x10000));
        bufferitem3.setIndex(curIndex);
        bufferitem3.setLastChunk();
        putItem(bufferitem3);
_L14:
        if (!isResetIndex) goto _L2; else goto _L5
_L5:
        int i;
        if (mDownloadIndex < curIndex)
        {
            mDownloadIndex = curIndex;
        }
        i = XMediaPlayerConstants.DOWNLOAD_QUEUE_SIZE;
_L17:
        if (mDownloadIndex - curIndex < i - 3 && buffItemQueue.size() >= 3 && !stopFlag && mDownloadIndex < ((FileDesc) (obj)).getComChunkNum() && !isResetIndex) goto _L7; else goto _L6
_L6:
        if (stopFlag) goto _L2; else goto _L8
_L8:
        if (isResetIndex) goto _L10; else goto _L9
_L9:
        mXMediaplayerJNI.onBufferingUpdateInner(getCachePercent());
        Logger.log("dl_mp3", (new StringBuilder("\u5F00\u59CB\u83B7\u53D6\u5206\u6BB5\u6570\u636E\uFF1Aurl:")).append(mFile.getUrl()).append(" curIndex:").append(curIndex).toString());
        if (mFile.getFileInfo().isChunkDownloaded(curIndex)) goto _L12; else goto _L11
_L11:
        Logger.log("dl_mp3", (new StringBuilder("url:")).append(mFile.getUrl()).append(" curIndex:").append(curIndex).append("\u7F13\u5B58\u547D\u4E2D\u5931\u8D25").toString());
        DownloadThread downloadthread = new DownloadThread(mFile, curIndex);
        currentDownload = downloadthread;
        if (downloadthread.download() == 200)
        {
            throw new IOException();
        }
          goto _L13
        InterruptedException interruptedexception;
        interruptedexception;
        interruptedexception.printStackTrace();
          goto _L14
_L7:
        flag = mFile.getFileInfo().isChunkDownloaded(mDownloadIndex);
        mXMediaplayerJNI.onBufferingUpdateInner(getCachePercent());
        if (flag)
        {
            break MISSING_BLOCK_LABEL_832;
        }
        if ((new DownloadThread(mFile, mDownloadIndex)).download() == 200)
        {
            throw new IOException();
        }
        if (mFile.getbBuffer() == null) goto _L16; else goto _L15
_L15:
        mFile.writeChunkData(mDownloadIndex, mFile.getbBuffer().array(), 0, mFile.getbBuffer().array().length);
        mFile.setbBuffer(null);
        Logger.log("dl_mp3", (new StringBuilder("url:")).append(mFile.getUrl()).append(" downloadIndex:").append(mDownloadIndex).append("\u4E0B\u8F7D\u5E76\u4E14\u7F13\u5B58\u6210\u529F").toString());
        mDownloadIndex = mDownloadIndex + 1;
_L18:
        Logger.log("dl_mp3", (new StringBuilder("getCachePercent percent mDownloadIndex0:")).append(mDownloadIndex).toString());
          goto _L17
_L16:
        Logger.log("dl_mp3", (new StringBuilder("url:")).append(mFile.getUrl()).append(" downloadIndex:").append(mDownloadIndex).append("\u4E0B\u8F7D\u5931\u8D25error").toString());
          goto _L6
        mDownloadIndex = mDownloadIndex + 1;
          goto _L18
_L13:
        if (mFile.getbBuffer() == null) goto _L20; else goto _L19
_L19:
        BufferItem bufferitem = new BufferItem();
        bufferitem.setBuffer(mFile.getbBuffer());
        bufferitem.setIndex(curIndex);
        putItem(bufferitem);
        mFile.writeChunkData(curIndex, mFile.getbBuffer().array(), 0, mFile.getbBuffer().array().length);
        mFile.setbBuffer(null);
        Logger.log("dl_mp3", (new StringBuilder("url:")).append(mFile.getUrl()).append(" curIndex:").append(curIndex).append("\u4E0B\u8F7D\u5E76\u4E14\u7F13\u5B58\u6210\u529F").toString());
        curIndex = curIndex + 1;
_L21:
        Logger.log("dl_mp3", (new StringBuilder("\u7ED3\u675F\u83B7\u53D6\u5206\u6BB5\u6570\u636E\uFF1Aurl:")).append(mFile.getUrl()).append(" curIndex:").append(curIndex).toString());
          goto _L10
_L20:
        Logger.log("dl_mp3", (new StringBuilder("url:")).append(mFile.getUrl()).append(" curIndex:").append(curIndex).append("\u4E0B\u8F7D\u5931\u8D25error").toString());
          goto _L2
_L12:
        BufferItem bufferitem1;
        Logger.log("dl_mp3", (new StringBuilder("url:")).append(mFile.getUrl()).append(" curIndex:").append(curIndex).append("\u7F13\u5B58\u547D\u4E2D\u6210\u529F").toString());
        bufferitem1 = readChunk(curIndex);
        if (bufferitem1 == null)
        {
            break MISSING_BLOCK_LABEL_1195;
        }
        Logger.log("dl_mp3", (new StringBuilder("url:")).append(mFile.getUrl()).append(" curIndex:").append(curIndex).append("\u7F13\u5B58\u83B7\u53D6\u6210\u529F").toString());
        putItem(bufferitem1);
        curIndex = curIndex + 1;
          goto _L21
        Logger.log("dl_mp3", (new StringBuilder("url:")).append(mFile.getUrl()).append(" curIndex:").append(curIndex).append("\u7F13\u5B58\u83B7\u53D6\u5931\u8D25error").toString());
        DownloadThread downloadthread1 = new DownloadThread(mFile, curIndex);
        currentDownload = downloadthread1;
        downloadthread1.download();
        if (mFile.getbBuffer() != null)
        {
            BufferItem bufferitem2 = new BufferItem();
            bufferitem2.setBuffer(mFile.getbBuffer());
            bufferitem2.setIndex(curIndex);
            putItem(bufferitem2);
            mFile.setbBuffer(null);
            curIndex = curIndex + 1;
        }
          goto _L21
    }
}
