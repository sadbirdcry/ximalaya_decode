// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.os.Environment;
import android.os.Process;
import android.util.Log;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaPlayerConstants

public class Logger
{

    public static final String JSON_ERROR = "\u89E3\u6790json\u5F02\u5E38";
    private static final int LOG_LEVEL = 0;

    public Logger()
    {
    }

    public static void d(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 3))
        {
            Log.d(s, s1);
        }
    }

    public static void d(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 3))
        {
            Log.d(s, s1, throwable);
        }
    }

    public static void e(Exception exception)
    {
        e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder("\u89E3\u6790json\u5F02\u5E38")).append(exception.getMessage()).append(getLineInfo()).toString());
    }

    public static void e(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 6))
        {
            Log.e(s, s1);
        }
    }

    public static void e(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 6))
        {
            Log.e(s, s1, throwable);
        }
    }

    public static String getLineInfo()
    {
        if (XMediaPlayerConstants.isDebug)
        {
            StackTraceElement stacktraceelement = (new Throwable()).getStackTrace()[1];
            return (new StringBuilder("@")).append(stacktraceelement.getFileName()).append(": Line ").append(stacktraceelement.getLineNumber()).toString();
        } else
        {
            return "";
        }
    }

    public static File getLogFilePath()
    {
        File file;
        if (!Environment.getExternalStorageState().equals("mounted"))
        {
            file = null;
        } else
        {
            File file1 = new File("/sdcard/ting/errorLog/infor.log");
            if (!file1.getParentFile().getParentFile().exists())
            {
                file1.getParentFile().getParentFile().mkdir();
            }
            file = file1;
            if (!file1.getParentFile().exists())
            {
                file1.getParentFile().mkdir();
                return file1;
            }
        }
        return file;
    }

    public static void i(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 4))
        {
            Log.i(s, s1);
        }
    }

    public static void i(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 4))
        {
            Log.i(s, s1, throwable);
        }
    }

    public static boolean isLoggable(String s, int j)
    {
        while (!XMediaPlayerConstants.isDebug || j < 0) 
        {
            return false;
        }
        return true;
    }

    public static void log(Object obj)
    {
        if (XMediaPlayerConstants.isDebug)
        {
            Log.i("ting", (new StringBuilder()).append(obj).toString());
        }
    }

    public static void log(String s, Object obj)
    {
        if (XMediaPlayerConstants.isDebug && obj != null && !s.equals("dl_mp3"))
        {
            Log.i(s, (new StringBuilder("JTid(")).append(Long.toString(Thread.currentThread().getId())).append(")STid(").append(Process.myTid()).append(")SPid(").append(Process.myPid()).append(")").append(obj).toString());
        }
    }

    public static void logToSd(String s)
    {
        if (!XMediaPlayerConstants.isDebug) goto _L2; else goto _L1
_L1:
        Object obj = getLogFilePath();
        if (obj != null) goto _L3; else goto _L2
_L2:
        return;
_L3:
        Object obj1 = null;
        obj = new PrintWriter(new FileWriter(((File) (obj)), true));
        ((PrintWriter) (obj)).println(s);
        if (obj != null)
        {
            ((PrintWriter) (obj)).close();
            return;
        }
        if (true) goto _L2; else goto _L4
_L4:
        s;
        obj = null;
_L8:
        if (obj == null) goto _L2; else goto _L5
_L5:
        ((PrintWriter) (obj)).close();
        return;
        s;
        obj = obj1;
_L7:
        if (obj != null)
        {
            ((PrintWriter) (obj)).close();
        }
        throw s;
        s;
        if (true) goto _L7; else goto _L6
_L6:
        s;
          goto _L8
    }

    public static void throwRuntimeException(Object obj)
    {
        if (XMediaPlayerConstants.isDebug)
        {
            throw new RuntimeException((new StringBuilder("\u51FA\u73B0\u5F02\u5E38\uFF1A")).append(obj).toString());
        } else
        {
            return;
        }
    }

    public static void v(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 2))
        {
            Log.v(s, s1);
        }
    }

    public static void v(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 2))
        {
            Log.v(s, s1, throwable);
        }
    }

    public static void w(String s, String s1)
    {
        if (s1 != null && isLoggable(s, 5))
        {
            Log.w(s, s1);
        }
    }

    public static void w(String s, String s1, Throwable throwable)
    {
        if (s1 != null && isLoggable(s, 5))
        {
            Log.w(s, s1, throwable);
        }
    }

    public static void w(String s, Throwable throwable)
    {
        if (throwable != null && isLoggable(s, 5))
        {
            Log.w(s, throwable);
        }
    }
}
