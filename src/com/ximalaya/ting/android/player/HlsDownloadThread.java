// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.text.TextUtils;
import com.ximalaya.ting.android.player.cdn.CdnCollectDataForPlay;
import com.ximalaya.ting.android.player.cdn.CdnConfigModel;
import com.ximalaya.ting.android.player.cdn.CdnUtil;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaplayerJNI, Logger, PlayerUtil, BufferItem

public class HlsDownloadThread
{

    CdnCollectDataForPlay data;
    private boolean isForceCloseCdn;
    private boolean isSendCDN;
    private boolean isSystemException;
    private BufferItem mBufferItem;
    private CdnConfigModel mCdnConfigModel;
    private long mConnectTime;
    private String mCurrentDownloadUrl;
    private float mDownloadSpeed;
    private long mDownloadTime;
    private int mDownloaded;
    private long mDsTime;
    private long mETime;
    private long mLimitConnectTime;
    private long mLimitDownloadSpeed;
    private long mSTime;

    public HlsDownloadThread(String s, BufferItem bufferitem)
    {
        isSystemException = false;
        isSendCDN = false;
        data = null;
        isForceCloseCdn = false;
        mCurrentDownloadUrl = s;
        mBufferItem = bufferitem;
    }

    public int download()
    {
        int i;
        isForceCloseCdn = false;
        isSendCDN = false;
        isSystemException = false;
        mCdnConfigModel = CdnUtil.getCdnConfigModel();
        if (mCdnConfigModel == null)
        {
            isForceCloseCdn = true;
        }
        i = 3;
_L3:
        String s;
        Object obj2;
        InputStream inputstream;
        HttpURLConnection httpurlconnection;
        HttpURLConnection httpurlconnection1;
        String s1;
        Object obj3;
        Object obj4;
        Object obj5;
        Object obj6;
        HttpURLConnection httpurlconnection2;
        HttpURLConnection httpurlconnection3;
        HttpURLConnection httpurlconnection4;
        InputStream inputstream2;
        HttpURLConnection httpurlconnection5;
        InputStream inputstream3;
        InputStream inputstream4;
        InputStream inputstream5;
        Object obj7;
        String s2;
        Object obj9;
        Object obj10;
        Object obj11;
        Object obj12;
        Object obj13;
        Object obj14;
        Object obj15;
        Object obj16;
        Object obj17;
        Object obj18;
        Object obj19;
        int j;
        j = i - 1;
        if (i <= 0)
        {
            return -1;
        }
        mSTime = System.currentTimeMillis();
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData start:")).append(mSTime).toString());
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData mCurrentDownloadUrl:")).append(mCurrentDownloadUrl).toString());
        s2 = null;
        s1 = null;
        s = null;
        obj7 = null;
        httpurlconnection1 = null;
        obj18 = null;
        obj15 = null;
        obj19 = null;
        obj16 = null;
        obj17 = null;
        obj10 = null;
        obj13 = null;
        obj12 = null;
        obj9 = null;
        obj11 = null;
        obj14 = null;
        if (!isForceCloseCdn)
        {
            data = new CdnCollectDataForPlay();
        }
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = s2;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = s1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = s;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = ((HttpURLConnection) (obj7));
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("HlsDownloadThread mPlayUrl:")).append(mCurrentDownloadUrl).toString());
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = s2;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = s1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = s;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = ((HttpURLConnection) (obj7));
        httpurlconnection1 = PlayerUtil.getHttpURLConnection(mCurrentDownloadUrl);
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        httpurlconnection1.setUseCaches(true);
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        httpurlconnection1.setRequestMethod("GET");
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        httpurlconnection1.setRequestProperty("Accept-Encoding", "identity");
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        if (data == null)
        {
            break MISSING_BLOCK_LABEL_1131;
        }
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        mETime = System.currentTimeMillis();
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        mConnectTime = mETime - mSTime;
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setAudioUrl(mCurrentDownloadUrl);
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setCdnIP(CdnUtil.getUrlIp(mCurrentDownloadUrl));
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setType("play");
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        s = httpurlconnection1.getHeaderField("via");
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setViaInfo(s);
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        i = httpurlconnection1.getResponseCode();
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        if (data == null)
        {
            break MISSING_BLOCK_LABEL_1683;
        }
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        mETime = System.currentTimeMillis();
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        mConnectTime = mETime - mSTime;
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        s = httpurlconnection1.getHeaderField("via");
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setViaInfo(s);
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setStatusCode((new StringBuilder(String.valueOf(i))).toString());
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        s = httpurlconnection1.getHeaderField("Content-Range");
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        if (TextUtils.isEmpty(s))
        {
            break MISSING_BLOCK_LABEL_2460;
        }
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        String as[] = s.split("/");
        String s3;
        s1 = "";
        s3 = "";
        s2 = s3;
        obj7 = s1;
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        if (as.length < 2)
        {
            break MISSING_BLOCK_LABEL_2258;
        }
        s = s1;
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        if (TextUtils.isEmpty(as[0]))
        {
            break MISSING_BLOCK_LABEL_2174;
        }
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        obj7 = as[0].split(" ");
        s = s1;
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        if (obj7.length >= 2)
        {
            s = obj7[1];
        }
        s2 = s3;
        obj7 = s;
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        if (!TextUtils.isEmpty(as[1]))
        {
            s2 = as[1];
            obj7 = s;
        }
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        if (data == null)
        {
            break MISSING_BLOCK_LABEL_2460;
        }
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setRange(((String) (obj7)));
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setFileSize(s2);
        while (i != 200 && i != 206) 
        {
            if (!isForceCloseCdn && data != null)
            {
                if (TextUtils.isEmpty(data.getViaInfo()))
                {
                    data.setViaInfo(null);
                }
                if (TextUtils.isEmpty(data.getStatusCode()))
                {
                    data.setStatusCode("");
                }
                data.setTimestamp(System.currentTimeMillis());
                if (!isSystemException)
                {
                    i = mCdnConfigModel.getNetType();
                    if (i == -1)
                    {
                        isForceCloseCdn = true;
                    } else
                    if (i == 0)
                    {
                        mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                        mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                        if (mConnectTime > mLimitConnectTime * 1000L)
                        {
                            isSendCDN = true;
                            data.setErrorType("cdn_connected_too_slow");
                            data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                        } else
                        if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                        {
                            isSendCDN = true;
                            data.setErrorType("cdn_download_too_slow");
                            data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                        }
                    } else
                    if (i == 1)
                    {
                        mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                        mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                        mLimitConnectTime = 0L;
                        if (mConnectTime > mLimitConnectTime * 1000L)
                        {
                            isSendCDN = true;
                            data.setErrorType("cdn_connected_too_slow");
                            data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                        } else
                        if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                        {
                            isSendCDN = true;
                            data.setErrorType("cdn_download_too_slow");
                            data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                        }
                    }
                }
            }
            if (httpurlconnection1 != null)
            {
                httpurlconnection1.disconnect();
            }
            IOException ioexception;
            IOException ioexception3;
            IOException ioexception4;
            if (false)
            {
                try
                {
                    throw new NullPointerException();
                }
                catch (IOException ioexception1) { }
            }
            if (false)
            {
                try
                {
                    throw new NullPointerException();
                }
                catch (IOException ioexception2) { }
            }
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData end:")).append(System.currentTimeMillis() - mSTime).toString());
            break MISSING_BLOCK_LABEL_5577;
        }
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        i = httpurlconnection1.getContentLength();
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        if (data == null) goto _L2; else goto _L1
_L1:
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        data.setAudioBytes(i);
          goto _L2
_L13:
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("HlsDownloadThread fail contentLength:")).append(i).toString());
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    mLimitConnectTime = 0L;
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (httpurlconnection1 != null)
        {
            httpurlconnection1.disconnect();
        }
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData end:")).append(System.currentTimeMillis() - mSTime).toString());
        i = j;
          goto _L3
_L14:
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        mDsTime = System.currentTimeMillis();
        obj4 = obj10;
        inputstream3 = obj17;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = obj18;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = obj15;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = obj19;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = obj16;
        httpurlconnection4 = httpurlconnection1;
        inputstream1 = httpurlconnection1.getInputStream();
        obj4 = obj10;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        obj8 = PlayerUtil.getHlsFilePath(mCurrentDownloadUrl);
        obj4 = obj10;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("filePath:")).append(((String) (obj8))).toString());
        obj4 = obj10;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        obj = new File(((String) (obj8)));
        obj4 = obj10;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        if (!((File) (obj)).exists())
        {
            break MISSING_BLOCK_LABEL_3775;
        }
        obj4 = obj10;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        ((File) (obj)).delete();
        obj4 = obj10;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        obj8 = new FileOutputStream(((String) (obj8)));
_L4:
        obj4 = obj10;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        Logger.log(XMediaplayerJNI.Tag, "HlsDownloadThread 0");
        obj = obj14;
        obj4 = obj10;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        if (mBufferItem == null)
        {
            break MISSING_BLOCK_LABEL_4045;
        }
        obj4 = obj10;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj13;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj12;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj9;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj11;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        obj = new ByteArrayOutputStream();
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        abyte0 = new byte[1024];
        i = 0;
_L5:
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        k = inputstream1.read(abyte0);
        if (k != -1)
        {
            break MISSING_BLOCK_LABEL_4989;
        }
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        if (data == null)
        {
            break MISSING_BLOCK_LABEL_4575;
        }
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        mDownloaded = i;
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        mDownloadTime = System.currentTimeMillis() - mDsTime;
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        if (mDownloadTime == 0L)
        {
            break MISSING_BLOCK_LABEL_4494;
        }
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        mDownloadSpeed = ((float)mDownloaded + 0.0F) / ((float)mDownloadTime + 0.0F);
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        data.setDownloadSpeed((new StringBuilder(String.valueOf(CdnUtil.oneDecimal(mDownloadSpeed, true)))).toString());
        if (obj8 == null)
        {
            break MISSING_BLOCK_LABEL_4638;
        }
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        ((FileOutputStream) (obj8)).close();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_6036;
        }
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        if (mBufferItem == null)
        {
            break MISSING_BLOCK_LABEL_6036;
        }
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        mBufferItem.setBuffer(((ByteArrayOutputStream) (obj)).toByteArray());
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        i = mBufferItem.getDataSize();
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                j = mCdnConfigModel.getNetType();
                if (j == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (j == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (j == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    mLimitConnectTime = 0L;
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (httpurlconnection1 != null)
        {
            httpurlconnection1.disconnect();
        }
        if (inputstream1 != null)
        {
            try
            {
                inputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception3) { }
        }
        if (obj != null)
        {
            try
            {
                ((ByteArrayOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData end:")).append(System.currentTimeMillis() - mSTime).toString());
        return i;
        obj;
        obj8 = null;
          goto _L4
        i += k;
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_5062;
        }
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        ((ByteArrayOutputStream) (obj)).write(abyte0, 0, k);
        if (obj8 == null)
        {
            break MISSING_BLOCK_LABEL_5130;
        }
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        ((FileOutputStream) (obj8)).write(abyte0, 0, k);
        obj4 = obj;
        inputstream3 = inputstream1;
        httpurlconnection5 = httpurlconnection1;
        obj6 = obj;
        inputstream2 = inputstream1;
        httpurlconnection3 = httpurlconnection1;
        obj2 = obj;
        inputstream = inputstream1;
        httpurlconnection = httpurlconnection1;
        obj3 = obj;
        inputstream4 = inputstream1;
        httpurlconnection2 = httpurlconnection1;
        obj5 = obj;
        inputstream5 = inputstream1;
        httpurlconnection4 = httpurlconnection1;
        mDownloaded = i;
          goto _L5
        obj;
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        isSystemException = true;
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        isSendCDN = true;
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        if (isForceCloseCdn)
        {
            break MISSING_BLOCK_LABEL_5388;
        }
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        if (data == null)
        {
            break MISSING_BLOCK_LABEL_5388;
        }
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        if (data.getConnectedTime() > 0.0F)
        {
            break MISSING_BLOCK_LABEL_5327;
        }
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        mConnectTime = System.currentTimeMillis() - mSTime;
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        data.setErrorType("cdn_connect_fail ");
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        data.setExceptionReason(((MalformedURLException) (obj)).getMessage());
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        data.setDownloadSpeed("0.0");
        obj2 = obj4;
        inputstream = inputstream3;
        httpurlconnection = httpurlconnection5;
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("HlsDownloadThread MalformedURLException:")).append(((MalformedURLException) (obj)).toString()).toString());
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    mLimitConnectTime = 0L;
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (httpurlconnection5 != null)
        {
            httpurlconnection5.disconnect();
        }
        if (inputstream3 != null)
        {
            try
            {
                inputstream3.close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        if (obj4 != null)
        {
            try
            {
                ((ByteArrayOutputStream) (obj4)).close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData end:")).append(System.currentTimeMillis() - mSTime).toString());
        if (data != null && isSendCDN && !isForceCloseCdn && !TextUtils.isEmpty(data.getErrorType()) && !TextUtils.isEmpty(data.getExceptionReason()))
        {
            CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
        }
        i = j;
          goto _L3
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                j = mCdnConfigModel.getNetType();
                if (j == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (j == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (j == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    mLimitConnectTime = 0L;
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (httpurlconnection1 != null)
        {
            httpurlconnection1.disconnect();
        }
        if (inputstream1 != null)
        {
            try
            {
                inputstream1.close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception3) { }
        }
        if (obj != null)
        {
            try
            {
                ((ByteArrayOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData end:")).append(System.currentTimeMillis() - mSTime).toString());
        return i;
        obj1;
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        isSystemException = true;
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        isSendCDN = true;
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        if (isForceCloseCdn) goto _L7; else goto _L6
_L6:
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        if (data == null) goto _L7; else goto _L8
_L8:
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        data.setDownloadSpeed("0.0");
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        if (data.getConnectedTime() > 0.0F)
        {
            break MISSING_BLOCK_LABEL_7133;
        }
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        mConnectTime = System.currentTimeMillis() - mSTime;
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        if (!TextUtils.isEmpty(((SocketTimeoutException) (obj1)).getMessage())) goto _L10; else goto _L9
_L9:
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        data.setErrorType("cdn_socket_timeout");
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        data.setExceptionReason(String.valueOf(obj1));
_L12:
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        data.setTimeout(true);
_L7:
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    mLimitConnectTime = 0L;
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (httpurlconnection3 != null)
        {
            httpurlconnection3.disconnect();
        }
        if (inputstream2 != null)
        {
            try
            {
                inputstream2.close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        if (obj6 != null)
        {
            try
            {
                ((ByteArrayOutputStream) (obj6)).close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData end:")).append(System.currentTimeMillis() - mSTime).toString());
        break MISSING_BLOCK_LABEL_5577;
_L10:
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        data.setErrorType("cdn_connect_timeout");
        obj2 = obj6;
        inputstream = inputstream2;
        httpurlconnection = httpurlconnection3;
        data.setExceptionReason(((SocketTimeoutException) (obj1)).getMessage());
        if (true) goto _L12; else goto _L11
_L11:
        obj1;
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    mLimitConnectTime = 0L;
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (httpurlconnection != null)
        {
            httpurlconnection.disconnect();
        }
        if (inputstream != null)
        {
            try
            {
                inputstream.close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception4) { }
        }
        if (obj2 != null)
        {
            try
            {
                ((ByteArrayOutputStream) (obj2)).close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception3) { }
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData end:")).append(System.currentTimeMillis() - mSTime).toString());
        throw obj1;
        obj1;
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        isSendCDN = true;
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        isSystemException = true;
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        if (isForceCloseCdn)
        {
            break MISSING_BLOCK_LABEL_8265;
        }
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        if (data == null)
        {
            break MISSING_BLOCK_LABEL_8265;
        }
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        if (!TextUtils.isEmpty(data.getDownloadSpeed()))
        {
            break MISSING_BLOCK_LABEL_8154;
        }
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        mDownloadTime = System.currentTimeMillis() - mDsTime;
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        if (mDownloadTime == 0L)
        {
            break MISSING_BLOCK_LABEL_8116;
        }
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        mDownloadSpeed = ((float)mDownloaded + 0.0F) / ((float)mDownloadTime + 0.0F);
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        data.setDownloadSpeed((new StringBuilder(String.valueOf(CdnUtil.oneDecimal(mDownloadSpeed, true)))).toString());
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        if (data.getConnectedTime() > 0.0F)
        {
            break MISSING_BLOCK_LABEL_8224;
        }
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        mConnectTime = System.currentTimeMillis() - mSTime;
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        data.setErrorType("cdn_io_exception");
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        data.setExceptionReason(((IOException) (obj1)).getMessage());
        obj2 = obj3;
        inputstream = inputstream4;
        httpurlconnection = httpurlconnection2;
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("HlsDownloadThread IOException:")).append(((IOException) (obj1)).toString()).toString());
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    mLimitConnectTime = 0L;
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (httpurlconnection2 != null)
        {
            httpurlconnection2.disconnect();
        }
        if (inputstream4 != null)
        {
            try
            {
                inputstream4.close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        if (obj3 != null)
        {
            try
            {
                ((ByteArrayOutputStream) (obj3)).close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData end:")).append(System.currentTimeMillis() - mSTime).toString());
        break MISSING_BLOCK_LABEL_5577;
        obj1;
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        isSendCDN = true;
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        isSystemException = true;
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        if (isForceCloseCdn)
        {
            break MISSING_BLOCK_LABEL_9158;
        }
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        if (data == null)
        {
            break MISSING_BLOCK_LABEL_9158;
        }
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        if (!TextUtils.isEmpty(data.getDownloadSpeed()))
        {
            break MISSING_BLOCK_LABEL_9047;
        }
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        mDownloadTime = System.currentTimeMillis() - mDsTime;
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        if (mDownloadTime == 0L)
        {
            break MISSING_BLOCK_LABEL_9009;
        }
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        mDownloadSpeed = ((float)mDownloaded + 0.0F) / ((float)mDownloadTime + 0.0F);
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        data.setDownloadSpeed((new StringBuilder(String.valueOf(CdnUtil.oneDecimal(mDownloadSpeed, true)))).toString());
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        if (data.getConnectedTime() > 0.0F)
        {
            break MISSING_BLOCK_LABEL_9117;
        }
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        mConnectTime = System.currentTimeMillis() - mSTime;
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        data.setErrorType("cdn_unknown_exception");
        obj2 = obj5;
        inputstream = inputstream5;
        httpurlconnection = httpurlconnection4;
        data.setExceptionReason(((Exception) (obj1)).getMessage());
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    mLimitConnectTime = 0L;
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append(CdnUtil.oneDecimal(mConnectTime, false)).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (httpurlconnection4 != null)
        {
            httpurlconnection4.disconnect();
        }
        if (inputstream5 != null)
        {
            try
            {
                inputstream5.close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        if (obj5 != null)
        {
            try
            {
                ((ByteArrayOutputStream) (obj5)).close();
            }
            // Misplaced declaration of an exception variable
            catch (IOException ioexception) { }
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread hls readData end:")).append(System.currentTimeMillis() - mSTime).toString());
        break MISSING_BLOCK_LABEL_5577;
_L2:
        if (i > 0 && i <= 0x40000) goto _L14; else goto _L13
    }
}
