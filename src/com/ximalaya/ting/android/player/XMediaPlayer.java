// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.content.Context;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import java.io.FileDescriptor;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaplayerJNI, PriorityHandlerThread, Logger, PlayerUtil, 
//            XMediaPlayerConstants, XMediaplayerImpl

public class XMediaPlayer extends XMediaplayerJNI
    implements android.os.Handler.Callback
{
    private class EventHandler extends Handler
    {

        private XMediaPlayer mMediaPlayer;
        final XMediaPlayer this$0;

        public void handleMessage(Message message)
        {
            if (mPlayState != 12 || message.what == 100) goto _L2; else goto _L1
_L1:
            Logger.log(XMediaplayerJNI.Tag, "handleMessage11 mPlayState NOT_ARCH_SUPPORT");
_L4:
            return;
_L2:
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("handleMessage11 mPlayState:")).append(mPlayState).toString());
            switch (message.what)
            {
            default:
                Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("Unknown message type ")).append(message.what).toString());
                return;

            case 0: // '\0'
            case 6: // '\006'
            case 7: // '\007'
            case 8: // '\b'
            case 9: // '\t'
            case 99: // 'c'
            case 201: 
                break;

            case 202: 
                continue; /* Loop/switch isn't completed */

            case 1: // '\001'
                if (mOnPreparedListener != null)
                {
                    mOnPreparedListener.onPrepared(mMediaPlayer);
                    return;
                }
                break;

            case 2: // '\002'
                if (mOnCompletionListener != null)
                {
                    mOnCompletionListener.onCompletion(mMediaPlayer);
                    return;
                }
                break;

            case 3: // '\003'
                if (mOnBufferingUpdateListener != null)
                {
                    mOnBufferingUpdateListener.onBufferingUpdate(mMediaPlayer, message.arg1);
                    return;
                }
                break;

            case 4: // '\004'
                if (mOnSeekCompleteListener != null)
                {
                    mOnSeekCompleteListener.onSeekComplete(mMediaPlayer);
                    return;
                }
                break;

            case 100: // 'd'
                Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("Error (")).append(message.arg1).append(",").append(message.arg2).append(")").toString());
                boolean flag = false;
                mPlayState = 8;
                if (mOnErrorListener != null)
                {
                    flag = mOnErrorListener.onError(mMediaPlayer, message.arg1, message.arg2);
                }
                if (mOnCompletionListener != null && !flag)
                {
                    mOnCompletionListener.onCompletion(mMediaPlayer);
                    return;
                }
                break;

            case 200: 
                if (mOnInfoListener != null)
                {
                    break; /* Loop/switch isn't completed */
                }
                break;
            }
            if (true) goto _L4; else goto _L3
_L3:
            mOnInfoListener.onInfo(mMediaPlayer, message.arg1, message.arg2);
            return;
            if (mOnTimedChangeListener == null || isSeeking()) goto _L4; else goto _L5
_L5:
            mOnTimedChangeListener.onPositionChange(mMediaPlayer, mMediaPlayer.getCurrentPosition());
            return;
        }

        public EventHandler(XMediaPlayer xmediaplayer1, Looper looper)
        {
            this$0 = XMediaPlayer.this;
            super(looper);
            mMediaPlayer = xmediaplayer1;
        }
    }

    public static interface OnBufferingUpdateListener
    {

        public abstract void onBufferingUpdate(XMediaplayerImpl xmediaplayerimpl, int i);
    }

    public static interface OnCompletionListener
    {

        public abstract void onCompletion(XMediaplayerImpl xmediaplayerimpl);
    }

    public static interface OnErrorListener
    {

        public abstract boolean onError(XMediaplayerImpl xmediaplayerimpl, int i, int j);
    }

    public static interface OnInfoListener
    {

        public abstract boolean onInfo(XMediaplayerImpl xmediaplayerimpl, int i, int j);
    }

    public static interface OnPositionChangeListener
    {

        public abstract void onPositionChange(XMediaplayerImpl xmediaplayerimpl, int i);
    }

    public static interface OnPreparedListener
    {

        public abstract void onPrepared(XMediaplayerImpl xmediaplayerimpl);
    }

    public static interface OnSeekCompleteListener
    {

        public abstract void onSeekComplete(XMediaplayerImpl xmediaplayerimpl);
    }


    private static final int MEDIA_BUFFERING_UPDATE = 3;
    private static final int MEDIA_ERROR = 100;
    public static final int MEDIA_ERROR_ARCH_UNSUPPORTED = -1011;
    public static final int MEDIA_ERROR_IO = -1004;
    public static final int MEDIA_ERROR_MALFORMED = -1007;
    public static final int MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK = 200;
    public static final int MEDIA_ERROR_SERVER_DIED = 100;
    public static final int MEDIA_ERROR_TIMED_OUT = -110;
    public static final int MEDIA_ERROR_UNKNOWN = 1;
    public static final int MEDIA_ERROR_UNSUPPORTED = -1010;
    private static final int MEDIA_INFO = 200;
    public static final int MEDIA_INFO_BUFFERING_END = 702;
    public static final int MEDIA_INFO_BUFFERING_START = 701;
    public static final int MEDIA_INFO_DOWNLOAD_RATE_CHANGED = 901;
    public static final int MEDIA_INFO_NOT_SEEKABLE = 801;
    public static final int MEDIA_INFO_VIDEO_TRACK_LAGGING = 700;
    private static final int MEDIA_NOP = 0;
    private static final int MEDIA_PAUSED = 7;
    private static final int MEDIA_PLAYBACK_COMPLETE = 2;
    private static final int MEDIA_PREPARED = 1;
    private static final int MEDIA_SEEK_COMPLETE = 4;
    private static final int MEDIA_SET_VIDEO_SIZE = 5;
    private static final int MEDIA_SKIPPED = 9;
    private static final int MEDIA_STARTED = 6;
    private static final int MEDIA_STOPPED = 8;
    private static final int MEDIA_SUBTITLE_DATA = 201;
    private static final int MEDIA_TIMED_CHANGE = 202;
    private static final int MEDIA_TIMED_TEXT = 99;
    private static final int MSG_COMPLETE = 9;
    private static final int MSG_INCREMENTAL_PREPARE = 2;
    private static final int MSG_INIT = 10;
    private static final int MSG_PAUSE = 3;
    private static final int MSG_PREPARE = 1;
    private static final int MSG_RELEASE = 5;
    private static final int MSG_RESET = 7;
    private static final int MSG_SEEK_TO = 6;
    private static final int MSG_SET_DATA_SOURCE = 8;
    private static final int MSG_START = 0;
    private static final int MSG_STOP = 4;
    private Context mContext;
    private EventHandler mEventHandler;
    private Handler mHandler;
    private HandlerThread mInternalPlaybackThread;
    private OnBufferingUpdateListener mOnBufferingUpdateListener;
    private OnCompletionListener mOnCompletionListener;
    private OnErrorListener mOnErrorListener;
    private OnInfoListener mOnInfoListener;
    private OnPreparedListener mOnPreparedListener;
    private OnSeekCompleteListener mOnSeekCompleteListener;
    private OnPositionChangeListener mOnTimedChangeListener;
    private volatile int mPlayState;
    private boolean mStayAwake;
    private android.os.PowerManager.WakeLock mWakeLock;

    public XMediaPlayer()
    {
        mWakeLock = null;
        init();
    }

    public XMediaPlayer(Context context)
    {
        mWakeLock = null;
        if (context != null)
        {
            mContext = context.getApplicationContext();
        }
        init();
    }

    private void init()
    {
        mPlayState = 1;
        mInternalPlaybackThread = new PriorityHandlerThread((new StringBuilder(String.valueOf(getClass().getSimpleName()))).append(":Handler").toString(), -16);
        mInternalPlaybackThread.start();
        mHandler = new Handler(mInternalPlaybackThread.getLooper(), this);
        mHandler.obtainMessage(10).sendToTarget();
        Looper looper = Looper.myLooper();
        if (looper != null)
        {
            mEventHandler = new EventHandler(this, looper);
            return;
        }
        looper = Looper.getMainLooper();
        if (looper != null)
        {
            mEventHandler = new EventHandler(this, looper);
            return;
        } else
        {
            mEventHandler = null;
            return;
        }
    }

    private void stayAwake(boolean flag)
    {
        if (mWakeLock == null) goto _L2; else goto _L1
_L1:
        if (!flag || mWakeLock.isHeld()) goto _L4; else goto _L3
_L3:
        mWakeLock.acquire();
_L2:
        mStayAwake = flag;
        return;
_L4:
        if (!flag && mWakeLock.isHeld())
        {
            mWakeLock.release();
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    public int getCurrentPosition()
    {
        if (mPlayState == 12)
        {
            return 0;
        } else
        {
            return super.getCurrentPosition();
        }
    }

    public int getDuration()
    {
        if (mPlayState == 12)
        {
            return 0;
        } else
        {
            return super.getDuration();
        }
    }

    public int getPlayState()
    {
        return mPlayState;
    }

    public boolean handleMessage(Message message)
    {
        if (mPlayState == 12)
        {
            Logger.log(XMediaplayerJNI.Tag, "handleMessage00 mPlayState NOT_ARCH_SUPPORT");
            return true;
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("handleMessage00 mPlayState:")).append(mPlayState).toString());
        message.what;
        JVM INSTR tableswitch 0 10: default 413
    //                   0 108
    //                   1 161
    //                   2 413
    //                   3 190
    //                   4 219
    //                   5 249
    //                   6 325
    //                   7 296
    //                   8 359
    //                   9 380
    //                   10 137;
           goto _L1 _L2 _L3 _L1 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11
_L2:
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 STARTED start");
        mPlayState = 4;
        super.start();
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 STARTED end");
        return true;
_L11:
        try
        {
            Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_INIT start");
            xmediaplayerInit();
            Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_INIT end");
        }
        // Misplaced declaration of an exception variable
        catch (Message message)
        {
            return true;
        }
        return true;
_L3:
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_PREPARE start");
        mPlayState = 2;
        super.prepareAsync();
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_PREPARE end");
        return true;
_L4:
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_PAUSE start");
        mPlayState = 5;
        super.pause();
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_PAUSE end");
        return true;
_L5:
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_STOP start");
        mPlayState = 6;
        super.stop();
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_STOP end");
        return true;
_L6:
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_RELEASE start");
        mPlayState = 9;
        super.release();
        mInternalPlaybackThread.getLooper().quit();
        mInternalPlaybackThread.interrupt();
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_RELEASE end");
        return true;
_L8:
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_RESET start");
        mPlayState = 0;
        super.reset();
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_RESET end");
        return true;
_L7:
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_SEEK_TO start");
        super.seekTo(((Integer)message.obj).intValue());
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_SEEK_TO end");
        return true;
_L9:
        if (message.obj != null)
        {
            super.setDataSource(message.obj.toString());
        }
        break; /* Loop/switch isn't completed */
_L10:
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_COMPLETE start");
        mPlayState = 11;
        super.onCompletionInner();
        Logger.log(XMediaplayerJNI.Tag, "handleMessage00 MSG_COMPLETE end");
        return true;
_L1:
        return false;
        return true;
    }

    public boolean isPlaying()
    {
        while (mPlayState == 12 || mPlayState == 3 || !super.isPlaying() || mPlayState != 4) 
        {
            return false;
        }
        return true;
    }

    public boolean isUseSystemPlayer()
    {
        return false;
    }

    public final void mOnTimedChangeListenerInner()
    {
        if (mEventHandler != null)
        {
            mEventHandler.obtainMessage(202).sendToTarget();
        }
    }

    public final void onBufferingUpdateInner(int i)
    {
        if (mEventHandler != null)
        {
            mEventHandler.obtainMessage(3, i, 0).sendToTarget();
        }
    }

    public final void onCompletionInner()
    {
        stayAwake(false);
        mHandler.obtainMessage(9).sendToTarget();
        if (mEventHandler != null)
        {
            mEventHandler.obtainMessage(2).sendToTarget();
        }
    }

    public final void onErrorInner(int i, int j)
    {
        if (j == -1011)
        {
            mPlayState = 12;
            Logger.log(XMediaplayerJNI.Tag, "onErrorInner mPlayState NOT_ARCH_SUPPORT");
        } else
        {
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("onErrorInner errorCode:")).append(i).append("extra:").append(j).toString());
        }
        super.onErrorInner(i, j);
        stayAwake(false);
        if (mEventHandler != null)
        {
            mEventHandler.obtainMessage(100, i, j).sendToTarget();
        }
    }

    public final void onInfoInner(int i)
    {
        if (mPlayState == 4 || mPlayState == 2)
        {
            if (i == 701)
            {
                isBuffing = true;
            } else
            if (i == 702)
            {
                isBuffing = false;
            }
            if (mEventHandler != null)
            {
                mEventHandler.obtainMessage(200, i, i).sendToTarget();
                return;
            }
        }
    }

    public final void onPreparedInner()
    {
        super.onPreparedInner();
        mPlayState = 3;
        if (mEventHandler != null)
        {
            mEventHandler.obtainMessage(1).sendToTarget();
        }
    }

    public final void onSeekCompletedInner()
    {
        Logger.log(Tag, "onSeekCompletedInner");
        if (mEventHandler != null)
        {
            mEventHandler.obtainMessage(4).sendToTarget();
        }
    }

    public void pause()
    {
        stayAwake(false);
        mHandler.obtainMessage(3).sendToTarget();
    }

    public void prepareAsync()
    {
        mHandler.obtainMessage(1).sendToTarget();
    }

    public void release()
    {
        stayAwake(false);
        mOnPreparedListener = null;
        mOnBufferingUpdateListener = null;
        mOnCompletionListener = null;
        mOnSeekCompleteListener = null;
        mOnErrorListener = null;
        mOnInfoListener = null;
        mOnTimedChangeListener = null;
        mHandler.obtainMessage(5).sendToTarget();
    }

    public void removeProxy()
    {
        PlayerUtil.setProxyHost(null);
        PlayerUtil.setProxyPort(0);
        PlayerUtil.setAuthorization(null);
    }

    public void reset()
    {
        stayAwake(false);
        mHandler.obtainMessage(7).sendToTarget();
    }

    public void seekTo(int i)
    {
        mHandler.obtainMessage(6, Integer.valueOf(i)).sendToTarget();
    }

    public void setAudioStreamType(int i)
    {
    }

    public void setDataSource(FileDescriptor filedescriptor, String s)
    {
        setDataSource(s);
    }

    public void setDataSource(String s)
    {
        mHandler.obtainMessage(8, s).sendToTarget();
    }

    public void setDownloadBufferSize(long l)
    {
        if (l > 0L)
        {
            XMediaPlayerConstants.DOWNLOAD_QUEUE_SIZE = (int)(l / 0x10000L);
        }
    }

    public void setOnBufferingUpdateListener(OnBufferingUpdateListener onbufferingupdatelistener)
    {
        mOnBufferingUpdateListener = onbufferingupdatelistener;
    }

    public void setOnCompletionListener(OnCompletionListener oncompletionlistener)
    {
        mOnCompletionListener = oncompletionlistener;
    }

    public void setOnErrorListener(OnErrorListener onerrorlistener)
    {
        mOnErrorListener = onerrorlistener;
    }

    public void setOnInfoListener(OnInfoListener oninfolistener)
    {
        mOnInfoListener = oninfolistener;
    }

    public void setOnPositionChangeListener(OnPositionChangeListener onpositionchangelistener)
    {
        mOnTimedChangeListener = onpositionchangelistener;
    }

    public void setOnPreparedListener(OnPreparedListener onpreparedlistener)
    {
        mOnPreparedListener = onpreparedlistener;
    }

    public void setOnSeekCompleteListener(OnSeekCompleteListener onseekcompletelistener)
    {
        mOnSeekCompleteListener = onseekcompletelistener;
    }

    public void setProxy(String s, int i, String s1)
    {
        PlayerUtil.setProxyHost(s);
        PlayerUtil.setProxyPort(i);
        PlayerUtil.setAuthorization(s1);
    }

    public void setVolume(float f, float f1)
    {
        super.setVolume(f, f1);
    }

    public void setWakeMode(Context context, int i)
    {
        if (context != null)
        {
            boolean flag;
            if (mWakeLock != null)
            {
                if (mWakeLock.isHeld())
                {
                    flag = true;
                    mWakeLock.release();
                } else
                {
                    flag = false;
                }
                mWakeLock = null;
            } else
            {
                flag = false;
            }
            context = (PowerManager)context.getSystemService("power");
            if (context != null)
            {
                mWakeLock = context.newWakeLock(0x20000000 | i, android/media/MediaPlayer.getName());
                mWakeLock.setReferenceCounted(false);
                if (flag)
                {
                    mWakeLock.acquire();
                    return;
                }
            }
        }
    }

    public void start()
    {
        stayAwake(true);
        mHandler.obtainMessage(0).sendToTarget();
    }

    public void stop()
    {
        stayAwake(false);
        mHandler.obtainMessage(4).sendToTarget();
    }









}
