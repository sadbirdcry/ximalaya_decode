// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player.model;


public class JNIDataModel
{

    public byte buf[];
    public int bufSize;
    public String filePath;
    public long fileSize;
    public int status;

    public JNIDataModel()
    {
        bufSize = 0;
    }

    public JNIDataModel(String s, long l, byte abyte0[], int i, int j)
    {
        bufSize = 0;
        filePath = s;
        fileSize = l;
        buf = abyte0;
        bufSize = i;
        status = j;
    }

    public JNIDataModel(byte abyte0[], int i, int j)
    {
        bufSize = 0;
        buf = abyte0;
        bufSize = i;
        status = j;
    }
}
