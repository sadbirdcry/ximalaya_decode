// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;


// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaplayerJNI

public static final class value extends Enum
{

    private static final M3U8_FILE ENUM$VALUES[];
    public static final M3U8_FILE HLS_FILE;
    public static final M3U8_FILE M3U8_FILE;
    public static final M3U8_FILE M3U8_STATIC_FILE;
    public static final M3U8_FILE NORMAL_FILE;
    private int value;

    public static value valueOf(int i)
    {
        switch (i)
        {
        default:
            return null;

        case 0: // '\0'
            return NORMAL_FILE;

        case 1: // '\001'
            return M3U8_STATIC_FILE;

        case 2: // '\002'
            return HLS_FILE;

        case 3: // '\003'
            return M3U8_FILE;
        }
    }

    public static M3U8_FILE valueOf(String s)
    {
        return (M3U8_FILE)Enum.valueOf(com/ximalaya/ting/android/player/XMediaplayerJNI$AudioType, s);
    }

    public static M3U8_FILE[] values()
    {
        M3U8_FILE am3u8_file[] = ENUM$VALUES;
        int i = am3u8_file.length;
        M3U8_FILE am3u8_file1[] = new ENUM.VALUES[i];
        System.arraycopy(am3u8_file, 0, am3u8_file1, 0, i);
        return am3u8_file1;
    }

    public int value()
    {
        return value;
    }

    static 
    {
        NORMAL_FILE = new <init>("NORMAL_FILE", 0, 0);
        M3U8_STATIC_FILE = new <init>("M3U8_STATIC_FILE", 1, 1);
        HLS_FILE = new <init>("HLS_FILE", 2, 2);
        M3U8_FILE = new <init>("M3U8_FILE", 3, 3);
        ENUM$VALUES = (new ENUM.VALUES[] {
            NORMAL_FILE, M3U8_STATIC_FILE, HLS_FILE, M3U8_FILE
        });
    }

    private (String s, int i, int j)
    {
        super(s, i);
        value = 0;
        value = j;
    }
}
