// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import java.nio.ByteBuffer;

// Referenced classes of package com.ximalaya.ting.android.player:
//            Logger

class BufferItem
{

    private static final String TAG = "dl_mp3";
    public ByteBuffer bBuffer;
    private int dataSize;
    public int errorCode;
    public boolean fails;
    private int index;
    private boolean lastChunk;

    public BufferItem()
    {
        index = -1;
        Logger.log("dl_mp3", "======================BufferItem Constructor()");
        lastChunk = false;
        dataSize = 0;
    }

    public ByteBuffer getBuffer()
    {
        if (bBuffer.hasArray())
        {
            return ByteBuffer.wrap(bBuffer.array());
        } else
        {
            return ByteBuffer.allocate(0);
        }
    }

    public int getDataSize()
    {
        return dataSize;
    }

    public int getIndex()
    {
        return index;
    }

    public boolean isLastChunk()
    {
        return lastChunk;
    }

    public void setBuffer(ByteBuffer bytebuffer)
    {
        if (bytebuffer.hasArray())
        {
            bBuffer = ByteBuffer.wrap(bytebuffer.array());
            dataSize = bytebuffer.array().length;
            Logger.log("dl_mp3", (new StringBuilder("======================BufferItem setBuffer0(")).append(dataSize).append(")").toString());
        }
    }

    public void setBuffer(byte abyte0[])
    {
        if (abyte0 != null)
        {
            bBuffer = ByteBuffer.wrap(abyte0);
            dataSize = abyte0.length;
            Logger.log("dl_mp3", (new StringBuilder("======================BufferItem setBuffer1(")).append(dataSize).append(")").toString());
        }
    }

    public void setIndex(int i)
    {
        index = i;
    }

    public void setLastChunk()
    {
        lastChunk = true;
    }
}
