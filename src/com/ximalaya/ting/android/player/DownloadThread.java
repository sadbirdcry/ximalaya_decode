// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.text.TextUtils;
import com.ximalaya.ting.android.player.cdn.CdnCollectDataForPlay;
import com.ximalaya.ting.android.player.cdn.CdnConfigModel;
import com.ximalaya.ting.android.player.cdn.CdnUtil;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;

// Referenced classes of package com.ximalaya.ting.android.player:
//            Logger, AudioFile, PlayerUtil, FileDesc, 
//            XMediaplayerJNI

public class DownloadThread
{

    private static final String TAG = "dl_mp3";
    private ByteBuffer bBuffer;
    CdnCollectDataForPlay data;
    private boolean isForceCloseCdn;
    private boolean isSendCDN;
    private boolean isSystemException;
    private CdnConfigModel mCdnConfigModel;
    private long mConnectTime;
    private float mDownloadSpeed;
    private long mDownloadTime;
    private int mDownloaded;
    private long mDsTime;
    private long mETime;
    private AudioFile mFile;
    private int mIndex;
    private long mLimitConnectTime;
    private long mLimitDownloadSpeed;
    private long mSTime;
    private boolean stopFlag;

    public DownloadThread(AudioFile audiofile, int i)
    {
        isSystemException = false;
        isSendCDN = false;
        data = null;
        isForceCloseCdn = false;
        Logger.log("dl_mp3", (new StringBuilder("======================DownloadThread Constructor(")).append(i).append(")").toString());
        mFile = audiofile;
        mIndex = i;
        bBuffer = ByteBuffer.allocate(0x10000);
        stopFlag = false;
    }

    public void close()
    {
        stopFlag = true;
    }

    public int download()
    {
        if (!stopFlag && mFile != null && mIndex >= 0 && !TextUtils.isEmpty(mFile.getUrl())) goto _L2; else goto _L1
_L1:
        int i = -1;
_L9:
        return i;
_L2:
        isSendCDN = false;
        isSystemException = false;
        isForceCloseCdn = false;
        mCdnConfigModel = CdnUtil.getCdnConfigModel();
        if (mCdnConfigModel == null)
        {
            isForceCloseCdn = true;
        }
        i = 3;
_L5:
        int j;
        j = i - 1;
        if (i <= 0)
        {
            return -1;
        }
        HttpURLConnection httpurlconnection;
        if (!isForceCloseCdn)
        {
            data = new CdnCollectDataForPlay();
        }
        mSTime = System.currentTimeMillis();
        httpurlconnection = PlayerUtil.getHttpURLConnection(mFile.getUrl());
        httpurlconnection.setRequestMethod("GET");
        httpurlconnection.setRequestProperty("Accept-Encoding", "identity");
        if (mIndex != mFile.getFileInfo().getComChunkNum() - 1) goto _L4; else goto _L3
_L3:
        int k;
        k = 0x10000 * mIndex;
        i = mFile.getFileInfo().getComFileLen() - 1;
_L6:
        httpurlconnection.addRequestProperty("Range", String.format("bytes=%d-%d", new Object[] {
            Integer.valueOf(k), Integer.valueOf(i)
        }));
        if (data != null)
        {
            mETime = System.currentTimeMillis();
            mConnectTime = mETime - mSTime;
            data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
            data.setAudioUrl(mFile.getUrl());
            data.setCdnIP(CdnUtil.getUrlIp(mFile.getUrl()));
            data.setType("play");
            String s = httpurlconnection.getHeaderField("via");
            data.setViaInfo(s);
            data.setStatusCode((new StringBuilder(String.valueOf(httpurlconnection.getResponseCode()))).toString());
        }
        k = httpurlconnection.getResponseCode();
        if (data != null)
        {
            mETime = System.currentTimeMillis();
            mConnectTime = mETime - mSTime;
            data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
            String s1 = httpurlconnection.getHeaderField("via");
            data.setViaInfo(s1);
            data.setStatusCode((new StringBuilder(String.valueOf(httpurlconnection.getResponseCode()))).toString());
        }
        if (k == 206)
        {
            break MISSING_BLOCK_LABEL_1042;
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread fail responseCode:")).append(k).toString());
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (data == null || !isSendCDN || isForceCloseCdn || TextUtils.isEmpty(data.getErrorType()) || TextUtils.isEmpty(data.getExceptionReason()))
        {
            break MISSING_BLOCK_LABEL_6452;
        }
        CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
        i = j;
          goto _L5
_L4:
        k = 0x10000 * mIndex;
        i = mIndex;
        i = (i + 1) * 0x10000 - 1;
          goto _L6
        String as[];
        int i1;
        i1 = httpurlconnection.getContentLength();
        String s2 = httpurlconnection.getHeaderField("Content-Range");
        if (TextUtils.isEmpty(s2))
        {
            break MISSING_BLOCK_LABEL_1177;
        }
        as = s2.split("/");
        String s3;
        Object obj2;
        String s4;
        String s5;
        s3 = "";
        s5 = "";
        s4 = s5;
        obj2 = s3;
        if (as.length < 2)
        {
            break MISSING_BLOCK_LABEL_1153;
        }
        Object obj = s3;
        if (TextUtils.isEmpty(as[0]))
        {
            break MISSING_BLOCK_LABEL_1129;
        }
        obj2 = as[0].split(" ");
        obj = s3;
        if (obj2.length >= 2)
        {
            obj = obj2[1];
        }
        s4 = s5;
        obj2 = obj;
        if (!TextUtils.isEmpty(as[1]))
        {
            s4 = as[1];
            obj2 = obj;
        }
        if (data != null)
        {
            data.setRange(((String) (obj2)));
            data.setFileSize(s4);
        }
        if (i1 > 0)
        {
            break MISSING_BLOCK_LABEL_1756;
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread fail contentLength0:")).append(i1).toString());
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (data == null || !isSendCDN || isForceCloseCdn || TextUtils.isEmpty(data.getErrorType()) || TextUtils.isEmpty(data.getExceptionReason()))
        {
            break MISSING_BLOCK_LABEL_6452;
        }
        CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
        i = j;
          goto _L5
        if (i1 == 0x10000)
        {
            break MISSING_BLOCK_LABEL_2356;
        }
        if (mIndex == mFile.getFileInfo().getComChunkNum() - 1)
        {
            break MISSING_BLOCK_LABEL_2356;
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread fail contentLength1:")).append(i1).toString());
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (data == null || !isSendCDN || isForceCloseCdn || TextUtils.isEmpty(data.getErrorType()) || TextUtils.isEmpty(data.getExceptionReason()))
        {
            break MISSING_BLOCK_LABEL_6452;
        }
        CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
        i = j;
          goto _L5
        mDsTime = System.currentTimeMillis();
        obj = httpurlconnection.getInputStream();
        i = 0;
_L8:
        int j1 = ((InputStream) (obj)).read(bBuffer.array(), i, 0x10000 - i);
        int l = i + j1;
        mDownloaded = l;
        i = l;
        if (j1 > 0) goto _L8; else goto _L7
_L7:
        if (data != null)
        {
            mDownloaded = l;
            mDownloadTime = System.currentTimeMillis() - mDsTime;
            if (mDownloadTime != 0L)
            {
                mDownloadSpeed = ((float)mDownloaded + 0.0F) / ((float)mDownloadTime + 0.0F);
            }
            data.setDownloadSpeed((new StringBuilder(String.valueOf(CdnUtil.oneDecimal(mDownloadSpeed, true)))).toString());
            data.setAudioBytes(i1);
        }
        ((InputStream) (obj)).close();
        mFile.setbBuffer(bBuffer);
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        i = k;
        if (data != null)
        {
            i = k;
            if (isSendCDN)
            {
                i = k;
                if (!isForceCloseCdn)
                {
                    i = k;
                    if (!TextUtils.isEmpty(data.getErrorType()))
                    {
                        i = k;
                        if (!TextUtils.isEmpty(data.getExceptionReason()))
                        {
                            CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
                            return k;
                        }
                    }
                }
            }
        }
          goto _L9
        Object obj1;
        obj1;
        isSystemException = true;
        isSendCDN = true;
        if (!isForceCloseCdn && data != null)
        {
            if (data.getConnectedTime() <= 0.0F)
            {
                mConnectTime = System.currentTimeMillis() - mSTime;
                data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
            }
            data.setErrorType("cdn_connect_fail ");
            data.setExceptionReason(((MalformedURLException) (obj1)).getMessage());
            data.setDownloadSpeed("0.0");
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread MalformedURLException:")).append(((MalformedURLException) (obj1)).toString()).toString());
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (data == null || !isSendCDN || isForceCloseCdn || TextUtils.isEmpty(data.getErrorType()) || TextUtils.isEmpty(data.getExceptionReason()))
        {
            break MISSING_BLOCK_LABEL_6452;
        }
        CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
        i = j;
          goto _L5
        obj1;
        isSystemException = true;
        isSendCDN = true;
        if (isForceCloseCdn || data == null) goto _L11; else goto _L10
_L10:
        data.setDownloadSpeed("0.0");
        if (data.getConnectedTime() <= 0.0F)
        {
            mConnectTime = System.currentTimeMillis() - mSTime;
            data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
        }
        if (!TextUtils.isEmpty(((SocketTimeoutException) (obj1)).getMessage()))
        {
            break MISSING_BLOCK_LABEL_4031;
        }
        data.setErrorType("cdn_socket_timeout");
        data.setExceptionReason(String.valueOf(obj1));
_L12:
        data.setTimeout(true);
_L11:
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (data == null || !isSendCDN || isForceCloseCdn || TextUtils.isEmpty(data.getErrorType()) || TextUtils.isEmpty(data.getExceptionReason()))
        {
            break MISSING_BLOCK_LABEL_6452;
        }
        CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
        i = j;
          goto _L5
        data.setErrorType("cdn_connect_timeout");
        data.setExceptionReason(((SocketTimeoutException) (obj1)).getMessage());
          goto _L12
        obj1;
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (data != null && isSendCDN && !isForceCloseCdn && !TextUtils.isEmpty(data.getErrorType()) && !TextUtils.isEmpty(data.getExceptionReason()))
        {
            CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
        }
        throw obj1;
        obj1;
        isSendCDN = true;
        isSystemException = true;
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getDownloadSpeed()))
            {
                mDownloadTime = System.currentTimeMillis() - mDsTime;
                if (mDownloadTime != 0L)
                {
                    mDownloadSpeed = ((float)mDownloaded + 0.0F) / ((float)mDownloadTime + 0.0F);
                }
                data.setDownloadSpeed((new StringBuilder(String.valueOf(CdnUtil.oneDecimal(mDownloadSpeed, true)))).toString());
            }
            if (data.getConnectedTime() <= 0.0F)
            {
                mConnectTime = System.currentTimeMillis() - mSTime;
                data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
            }
            data.setErrorType("cdn_io_exception");
            data.setExceptionReason(((IOException) (obj1)).getMessage());
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("DownloadThread IOException:")).append(((IOException) (obj1)).toString()).toString());
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (data == null || !isSendCDN || isForceCloseCdn || TextUtils.isEmpty(data.getErrorType()) || TextUtils.isEmpty(data.getExceptionReason()))
        {
            break MISSING_BLOCK_LABEL_6452;
        }
        CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
        i = j;
          goto _L5
        obj1;
        isSendCDN = true;
        isSystemException = true;
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getDownloadSpeed()))
            {
                mDownloadTime = System.currentTimeMillis() - mDsTime;
                if (mDownloadTime != 0L)
                {
                    mDownloadSpeed = ((float)mDownloaded + 0.0F) / ((float)mDownloadTime + 0.0F);
                }
                data.setDownloadSpeed((new StringBuilder(String.valueOf(CdnUtil.oneDecimal(mDownloadSpeed, true)))).toString());
            }
            if (data.getConnectedTime() <= 0.0F)
            {
                mConnectTime = System.currentTimeMillis() - mSTime;
                data.setConnectedTime(CdnUtil.oneDecimal(mConnectTime, false));
            }
            data.setErrorType("cdn_unknown_exception");
            data.setExceptionReason(((Exception) (obj1)).getMessage());
        }
        if (!isForceCloseCdn && data != null)
        {
            if (TextUtils.isEmpty(data.getViaInfo()))
            {
                data.setViaInfo(null);
            }
            if (TextUtils.isEmpty(data.getStatusCode()))
            {
                data.setStatusCode("");
            }
            data.setTimestamp(System.currentTimeMillis());
            if (!isSystemException)
            {
                i = mCdnConfigModel.getNetType();
                if (i == -1)
                {
                    isForceCloseCdn = true;
                } else
                if (i == 0)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnNotWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnNotWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                } else
                if (i == 1)
                {
                    mLimitConnectTime = mCdnConfigModel.getCdnWifiConnectTimeout();
                    mLimitDownloadSpeed = mCdnConfigModel.getCdnWifiAlertRate();
                    if (mConnectTime > mLimitConnectTime * 1000L)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_connected_too_slow");
                        data.setExceptionReason((new StringBuilder("connected_time=")).append((float)mConnectTime / 1000F).append("s, connected_time_threshold=").append(mLimitConnectTime).append("s").toString());
                    } else
                    if ((float)mLimitDownloadSpeed > mDownloadSpeed)
                    {
                        isSendCDN = true;
                        data.setErrorType("cdn_download_too_slow");
                        data.setExceptionReason((new StringBuilder("download_speed=")).append(CdnUtil.oneDecimal(mDownloadSpeed, true)).append("KB/s, download_speed_threshold=").append(mLimitDownloadSpeed).append("KB/s").toString());
                    }
                }
            }
        }
        if (data == null || !isSendCDN || isForceCloseCdn || TextUtils.isEmpty(data.getErrorType()) || TextUtils.isEmpty(data.getExceptionReason()))
        {
            break MISSING_BLOCK_LABEL_6452;
        }
        CdnUtil.statDownLoadCDN(data, mCdnConfigModel);
        i = j;
          goto _L5
        i = j;
          goto _L5
    }
}
