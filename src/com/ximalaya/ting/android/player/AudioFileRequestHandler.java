// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import com.ximalaya.ting.android.player.model.JNIDataModel;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaplayerJNI, Logger, ReadThread, XMediaPlayerConstants, 
//            AudioFile, FileDesc, PlayerUtil, BufferItem

public class AudioFileRequestHandler
{

    private LinkedBlockingQueue buffItemQueue;
    private AtomicInteger mHandlerCnt;
    private ReadThread mReadThread;
    private XMediaplayerJNI mXMediaplayerJNI;

    public AudioFileRequestHandler(XMediaplayerJNI xmediaplayerjni)
    {
        mHandlerCnt = new AtomicInteger(0);
        mXMediaplayerJNI = xmediaplayerjni;
    }

    private int readDataFromSD(String s, int i, byte abyte0[])
        throws IOException
    {
        this;
        JVM INSTR monitorenter ;
        s = new RandomAccessFile(s, "r");
        mXMediaplayerJNI.setCurFileSize(s.length());
        s.seek(i);
        i = 0;
_L2:
        int k = s.read(abyte0, i, abyte0.length - i);
        int j;
        j = i + k;
        i = j;
        if (k > 0) goto _L2; else goto _L1
_L1:
        s.close();
        this;
        JVM INSTR monitorexit ;
        return j;
        s;
        throw s;
    }

    private boolean resetLoadDataPosition(int i, JNIDataModel jnidatamodel)
    {
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT resetLoadDataPosition");
        buffItemQueue = new LinkedBlockingQueue(3);
        if (mReadThread == null || mReadThread.isClosed())
        {
            Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT resetLoadDataPosition0");
            AudioFile audiofile;
            try
            {
                audiofile = AudioFile.getAudioFile(XMediaPlayerConstants.INCOM_AUDIO_FILE_DIRECTORY, mXMediaplayerJNI.getPlayUrl());
            }
            // Misplaced declaration of an exception variable
            catch (JNIDataModel jnidatamodel)
            {
                jnidatamodel.printStackTrace();
                return false;
            }
            // Misplaced declaration of an exception variable
            catch (JNIDataModel jnidatamodel)
            {
                jnidatamodel.printStackTrace();
                return false;
            }
            if (audiofile == null || !audiofile.getFileInfo().isValid())
            {
                Logger.log(XMediaplayerJNI.Tag, "mFile.getFileInfo().inValid()");
                return false;
            }
            if (mReadThread != null)
            {
                mReadThread.close();
            }
            int k;
            if (audiofile.getFileInfo().isValid())
            {
                int j = audiofile.getFileInfo().getComFileLen();
                jnidatamodel.fileSize = j;
                mXMediaplayerJNI.setCurFileSize(j);
                i /= 0x10000;
                mReadThread = new ReadThread(audiofile, i, buffItemQueue, mXMediaplayerJNI);
                jnidatamodel = (new StringBuilder(String.valueOf(UUID.randomUUID().toString()))).append(mHandlerCnt.get()).toString();
                mReadThread.setName((new StringBuilder("t_Read_")).append(i).append("_").append(jnidatamodel).toString());
                Logger.log(XMediaplayerJNI.Tag, "ReadThread.start()");
                mReadThread.start();
                return true;
            } else
            {
                return false;
            }
        } else
        {
            Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT resetLoadDataPosition1");
            k = mReadThread.getAudioFile().getFileInfo().getComFileLen();
            jnidatamodel.fileSize = k;
            mXMediaplayerJNI.setCurFileSize(k);
            i /= 0x10000;
            mReadThread.resetIndex(i, buffItemQueue);
            return true;
        }
    }

    private boolean shallReloadData()
    {
        return buffItemQueue == null || mReadThread != null && mReadThread.isClosed() && buffItemQueue.size() == 0;
    }

    public int getCachePercent()
    {
        if (PlayerUtil.isLocalFile(mXMediaplayerJNI.getPlayUrl()))
        {
            return 100;
        }
        if (mReadThread == null)
        {
            return 0;
        } else
        {
            return mReadThread.getCachePercent();
        }
    }

    public int readData(JNIDataModel jnidatamodel, boolean flag, int i)
    {
        long l1;
        int j = 0x10000;
        if (PlayerUtil.isLocalFile(jnidatamodel.filePath))
        {
            if (jnidatamodel.bufSize <= 0x10000)
            {
                j = jnidatamodel.bufSize;
            }
            jnidatamodel.buf = new byte[j];
            try
            {
                readDataFromSD(jnidatamodel.filePath, i, jnidatamodel.buf);
            }
            // Misplaced declaration of an exception variable
            catch (JNIDataModel jnidatamodel)
            {
                return -1;
            }
            jnidatamodel.fileSize = mXMediaplayerJNI.getCurFileSize();
            mXMediaplayerJNI.onBufferingUpdateInner(getCachePercent());
            return jnidatamodel.buf.length;
        }
        if ((flag || shallReloadData()) && !resetLoadDataPosition(i, jnidatamodel))
        {
            return -1;
        }
        l1 = mXMediaplayerJNI.getCurFileSize();
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("dataStreamInputFuncCallBackT 0 tCurFileSize:")).append(l1).toString());
        if (l1 <= 0L)
        {
            return -1;
        }
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT 1");
        if (flag) goto _L2; else goto _L1
_L1:
        if (mXMediaplayerJNI.tmepBuf == null) goto _L2; else goto _L3
_L3:
        jnidatamodel.buf = mXMediaplayerJNI.tmepBuf;
        mXMediaplayerJNI.tmepBuf = null;
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT read temp buf");
_L4:
        jnidatamodel.fileSize = l1;
_L5:
        Object obj;
        int l;
        int i1;
        int j1;
        if (jnidatamodel.buf == null)
        {
            return -1;
        } else
        {
            return jnidatamodel.buf.length;
        }
_L2:
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT 2");
        mXMediaplayerJNI.tmepBuf = null;
        obj = (BufferItem)buffItemQueue.poll(30000L, TimeUnit.MILLISECONDS);
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT 3");
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_271;
        }
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT timeout item null");
        return -1;
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT 4");
        int k = ((BufferItem) (obj)).getIndex();
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("dataStreamInputFuncCallBackT seekParaTimeStampMs index:")).append(k).toString());
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_609;
        }
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT seekParaTimeStampMs 1");
        j1 = i % 0x10000;
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("dataStreamInputFuncCallBackT offset:")).append(j1).toString());
        i1 = ((BufferItem) (obj)).getBuffer().array().length;
        l = i1;
        if (l1 == 0L)
        {
            break MISSING_BLOCK_LABEL_437;
        }
        l = i1;
        if ((long)(i / 0x10000) != l1 / 0x10000L)
        {
            break MISSING_BLOCK_LABEL_437;
        }
        l = (int)(l1 % 0x10000L);
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("dataStreamInputFuncCallBackT lastChunkLength:")).append(l).toString());
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT seekParaTimeStampMs 2");
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("dataStreamInputFuncCallBackT offset:")).append(j1).append("length:").append(l).toString());
        obj = ((BufferItem) (obj)).getBuffer();
        ((ByteBuffer) (obj)).position(j1).limit(l);
        obj = ((ByteBuffer) (obj)).slice();
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("dataStreamInputFuncCallBackT slice remaining:")).append(((ByteBuffer) (obj)).remaining()).toString());
        jnidatamodel.buf = new byte[((ByteBuffer) (obj)).remaining()];
        ((ByteBuffer) (obj)).get(jnidatamodel.buf);
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("dataStreamInputFuncCallBackT slice buf size:")).append(jnidatamodel.buf.length).toString());
          goto _L4
        obj;
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("dataStreamInputFuncCallBackT 19")).append(((InterruptedException) (obj)).getMessage()).toString());
          goto _L5
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT seekParaTimeStampMs 3");
        if (l1 == 0L)
        {
            break MISSING_BLOCK_LABEL_706;
        }
        if ((long)(i / 0x10000) != l1 / 0x10000L)
        {
            break MISSING_BLOCK_LABEL_706;
        }
        i = (int)(l1 % 0x10000L);
        obj = ((BufferItem) (obj)).getBuffer();
        ((ByteBuffer) (obj)).position(0).limit(i);
        obj = ((ByteBuffer) (obj)).slice();
        jnidatamodel.buf = new byte[((ByteBuffer) (obj)).remaining()];
        ((ByteBuffer) (obj)).get(jnidatamodel.buf);
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT seekParaTimeStampMs 4");
          goto _L4
        jnidatamodel.buf = ((BufferItem) (obj)).getBuffer().array();
        Logger.log(XMediaplayerJNI.Tag, "dataStreamInputFuncCallBackT seekParaTimeStampMs 5");
          goto _L4
    }

    public void release()
    {
        if (mReadThread != null)
        {
            mReadThread.close();
            mReadThread = null;
        }
        if (buffItemQueue != null)
        {
            buffItemQueue.clear();
        }
    }

    public void reset()
    {
        release();
    }
}
