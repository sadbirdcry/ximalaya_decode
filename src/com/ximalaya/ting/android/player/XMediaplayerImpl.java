// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.content.Context;
import java.io.FileDescriptor;

public interface XMediaplayerImpl
{

    public static final int COMPLETE = 11;
    public static final int ERROR = 8;
    public static final int IDLE = 0;
    public static final int INFO = 10;
    public static final int INITIALIZED = 1;
    public static final int NOT_ARCH_SUPPORT = 12;
    public static final int PAUSED = 5;
    public static final int PLAYBACK_COMPLETED = 7;
    public static final int PREPARED = 3;
    public static final int PREPARING = 2;
    public static final int RELEASED = 9;
    public static final int STARTED = 4;
    public static final int STOPPED = 6;

    public abstract XMediaplayerJNI.AudioType getAudioType();

    public abstract int getCurrentPosition();

    public abstract int getDuration();

    public abstract int getPlayState();

    public abstract boolean isPlaying();

    public abstract boolean isUseSystemPlayer();

    public abstract void pause();

    public abstract void prepareAsync();

    public abstract void release();

    public abstract void removeProxy();

    public abstract void reset();

    public abstract void seekTo(int i);

    public abstract void setAudioStreamType(int i);

    public abstract void setDataSource(FileDescriptor filedescriptor, String s);

    public abstract void setDataSource(String s);

    public abstract void setDownloadBufferSize(long l);

    public abstract void setOnBufferingUpdateListener(XMediaPlayer.OnBufferingUpdateListener onbufferingupdatelistener);

    public abstract void setOnCompletionListener(XMediaPlayer.OnCompletionListener oncompletionlistener);

    public abstract void setOnErrorListener(XMediaPlayer.OnErrorListener onerrorlistener);

    public abstract void setOnInfoListener(XMediaPlayer.OnInfoListener oninfolistener);

    public abstract void setOnPositionChangeListener(XMediaPlayer.OnPositionChangeListener onpositionchangelistener);

    public abstract void setOnPreparedListener(XMediaPlayer.OnPreparedListener onpreparedlistener);

    public abstract void setOnSeekCompleteListener(XMediaPlayer.OnSeekCompleteListener onseekcompletelistener);

    public abstract void setProxy(String s, int i, String s1);

    public abstract void setVolume(float f, float f1);

    public abstract void setWakeMode(Context context, int i);

    public abstract void start();

    public abstract void stop();
}
