// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.media.AudioTrack;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaplayerJNI, Logger, XMediaPlayerConstants

public class AudioTrackPlayThread extends Thread
{

    public static final int AUDIO_CHANNEL = 12;
    public static final int AUDIO_CHANNEL_NUM = 2;
    public static final int AUDIO_FORMAT = 2;
    public static final int BUFFER_TIME = 1;
    public static final int BYTES_PER_SAMPLE = 2;
    public static final int DEFAULT_MIN_BUFFER_MULTIPLICATION_FACTOR = 4;
    private static int DEF_BUFF_SIZE = 0;
    public static final int PLAYBACK_STREAM = 3;
    public static final int SAMPLE_RATE = 44100;
    public static final int WRITE_NON_BLOCKING = 1;
    private volatile boolean isPlaying;
    private volatile boolean isRelaseFlag;
    private boolean isRunning;
    private boolean isWaiting;
    private long lastUpdateTime;
    private AudioTrack mAudioTrack;
    private byte mAudiodata[];
    private int mBufferSize;
    private float mVolume;
    private XMediaplayerJNI mXMediaplayerJNI;
    private Object syncObject;

    public AudioTrackPlayThread(XMediaplayerJNI xmediaplayerjni)
    {
        isRelaseFlag = false;
        isRunning = false;
        syncObject = new Object();
        isWaiting = false;
        lastUpdateTime = 0L;
        isPlaying = false;
        mXMediaplayerJNI = xmediaplayerjni;
        initAudioTrack();
    }

    private void audioTrackRelease()
    {
        isPlaying = false;
        Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread audioTrackRelease");
        if (mAudioTrack != null)
        {
            mAudioTrack.release();
        }
        mAudioTrack = null;
    }

    private void audioTrackStart()
    {
        if (mAudioTrack == null)
        {
            return;
        }
        if (mAudioTrack.getState() == 1 && mAudioTrack.getPlayState() != 3)
        {
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay3");
            mAudioTrack.play();
            isPlaying = true;
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay4");
            return;
        }
        if (mAudioTrack != null)
        {
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay5");
            mAudioTrack.release();
            initAudioTrack();
            mAudioTrack.play();
            isPlaying = true;
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay6");
            return;
        } else
        {
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay7");
            initAudioTrack();
            mAudioTrack.play();
            isPlaying = true;
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay8");
            return;
        }
    }

    private int audioTrackWrite(byte abyte0[], int i, int j)
    {
        int k;
        int l;
        Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread audioTrackWrite start");
        l = 0;
        k = l;
        if (mAudioTrack == null) goto _L2; else goto _L1
_L1:
        k = l;
        if (mAudioTrack.getPlayState() != 3) goto _L2; else goto _L3
_L3:
        l = 0;
        k = i;
        i = l;
_L8:
        if (j > 0 && isPlaying) goto _L5; else goto _L4
_L4:
        k = i;
_L2:
        Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread audioTrackWrite end");
        return k;
_L5:
        int i1;
        if (j > mBufferSize)
        {
            l = mBufferSize;
        } else
        {
            l = j;
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("AudioTrackPlayThread audioTrackWrite 0 written\uFF1A")).append(l).toString());
        if (mAudioTrack == null || mAudioTrack.getPlayState() != 3)
        {
            continue; /* Loop/switch isn't completed */
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("AudioTrackPlayThread audioTrackWrite 22 written\uFF1A")).append(l).toString());
        i1 = mAudioTrack.write(abyte0, k, l);
        if (i1 >= 0)
        {
            break; /* Loop/switch isn't completed */
        }
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("AudioTrackPlayThread audioTrackWrite 1 error wriBytes\uFF1A")).append(i1).toString());
        k = i;
        if (true) goto _L2; else goto _L6
_L6:
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("AudioTrackPlayThread audioTrackWrite 1 wriBytes\uFF1A")).append(i1).toString());
        i += i1;
        j -= l;
        k += l;
        if (true) goto _L8; else goto _L7
_L7:
    }

    private void initAudioTrack()
    {
        Logger.log(XMediaplayerJNI.Tag, "initAudioTrack");
        int i = AudioTrack.getMinBufferSize(44100, 12, 2);
        mBufferSize = i * 4;
        mBufferSize = Math.max(i, DEF_BUFF_SIZE);
        mAudioTrack = new AudioTrack(3, 44100, 12, 2, mBufferSize, 1);
        mAudiodata = new byte[mBufferSize];
    }

    private static void setVolumeV21(AudioTrack audiotrack, float f)
    {
        audiotrack.setVolume(f);
    }

    private static void setVolumeV3(AudioTrack audiotrack, float f)
    {
        if (audiotrack == null)
        {
            return;
        } else
        {
            audiotrack.setStereoVolume(f, f);
            return;
        }
    }

    private void writePcm(byte abyte0[])
    {
        if (abyte0 == null)
        {
            return;
        }
        try
        {
            FileOutputStream fileoutputstream = new FileOutputStream((new StringBuilder(String.valueOf(XMediaPlayerConstants.APP_BASE_DIR))).append("/log/pcm.pcm").toString());
            fileoutputstream.write(abyte0);
            fileoutputstream.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            abyte0.printStackTrace();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (byte abyte0[])
        {
            abyte0.printStackTrace();
        }
    }

    public void dataReady()
    {
        Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread dataReady");
        if (isWaiting)
        {
            synchronized (syncObject)
            {
                Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread notify");
                syncObject.notify();
                isWaiting = false;
            }
            return;
        } else
        {
            return;
        }
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public int getMinOutputBufferSize()
    {
        return mBufferSize;
    }

    public float getVolume()
    {
        return mVolume;
    }

    public boolean isPlaying()
    {
        return mAudioTrack != null && mAudioTrack.getPlayState() == 3;
    }

    public void pausePlay()
    {
        if (mAudioTrack != null)
        {
            isPlaying = false;
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread pausePlay0");
            if (mAudioTrack.getPlayState() == 3)
            {
                mAudioTrack.pause();
                Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread pausePlay1");
                return;
            }
        }
    }

    public void releasePlay()
    {
        isPlaying = false;
        isRelaseFlag = true;
        Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread releasePlay");
        interrupt();
    }

    public void run()
    {
        isRunning = true;
_L3:
        if (!isRelaseFlag) goto _L2; else goto _L1
_L1:
        isRunning = false;
        stopPlay();
        audioTrackRelease();
        return;
_L2:
        synchronized (syncObject)
        {
            if (!isRelaseFlag)
            {
                break MISSING_BLOCK_LABEL_58;
            }
        }
          goto _L1
        exception;
        obj;
        JVM INSTR monitorexit ;
        try
        {
            throw exception;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((InterruptedException) (obj)).printStackTrace();
        }
          goto _L3
        int i;
        i = mXMediaplayerJNI.getOutputData(mAudiodata, mAudiodata.length);
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("AudioTrackPlayThread result0:")).append(i).toString());
        int j = i;
        if (i <= 0) goto _L5; else goto _L4
_L4:
        j = i;
        if (i >= mBufferSize) goto _L5; else goto _L6
_L6:
        if (!isRelaseFlag) goto _L8; else goto _L7
_L7:
        j = i;
_L5:
        if (!isRelaseFlag)
        {
            break MISSING_BLOCK_LABEL_312;
        }
        obj;
        JVM INSTR monitorexit ;
          goto _L1
_L8:
        byte abyte0[] = new byte[mBufferSize - i];
        j = i;
        if (isRelaseFlag) goto _L5; else goto _L9
_L9:
        j = mXMediaplayerJNI.getOutputData(abyte0, abyte0.length);
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("AudioTrackPlayThread result1 reLength:")).append(j).toString());
        if (j <= 0)
        {
            break MISSING_BLOCK_LABEL_241;
        }
        System.arraycopy(abyte0, 0, mAudiodata, i, abyte0.length);
        i += j;
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("AudioTrackPlayThread result2:")).append(j).toString());
          goto _L4
        isWaiting = true;
        Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread mAudioTrack wait0");
        mXMediaplayerJNI.outputDataAppointment();
        if (!mXMediaplayerJNI.isBuffing && mXMediaplayerJNI.getPlayState() == 4)
        {
            mXMediaplayerJNI.onInfoInner(701);
        }
        syncObject.wait(30000L);
        isWaiting = false;
          goto _L4
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("AudioTrackPlayThread result3:")).append(j).toString());
        if (j != -2) goto _L11; else goto _L10
_L10:
        Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread decode over");
        if (mXMediaplayerJNI.isBuffing)
        {
            mXMediaplayerJNI.onInfoInner(702);
        }
        audioTrackWrite(mAudiodata, 0, mAudiodata.length);
        mAudioTrack.stop();
        mXMediaplayerJNI.onCompletionInner();
        obj;
        JVM INSTR monitorexit ;
          goto _L1
_L14:
        if (i >= 0) goto _L13; else goto _L12
_L12:
        mXMediaplayerJNI.onErrorInner(8, 1);
        obj;
        JVM INSTR monitorexit ;
          goto _L1
_L17:
        if (j != 0)
        {
            break MISSING_BLOCK_LABEL_504;
        }
        isWaiting = true;
        Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread wait");
        mXMediaplayerJNI.outputDataAppointment();
        if (!mXMediaplayerJNI.isBuffing && mXMediaplayerJNI.getPlayState() == 4)
        {
            mXMediaplayerJNI.onInfoInner(701);
        }
        syncObject.wait(30000L);
        isWaiting = false;
        i = j;
          goto _L14
        if (j <= 0)
        {
            break MISSING_BLOCK_LABEL_621;
        }
        if (mXMediaplayerJNI.isBuffing)
        {
            mXMediaplayerJNI.onInfoInner(702);
        }
        j = audioTrackWrite(mAudiodata, 0, j);
        i = j;
        if (System.currentTimeMillis() - lastUpdateTime <= 1000L) goto _L14; else goto _L15
_L15:
        lastUpdateTime = System.currentTimeMillis();
        Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("AudioTrackPlayThread ttseek3:")).append(System.currentTimeMillis()).toString());
        mXMediaplayerJNI.mOnTimedChangeListenerInner();
        i = j;
          goto _L14
_L13:
        obj;
        JVM INSTR monitorexit ;
          goto _L3
_L11:
        if (j != -1) goto _L17; else goto _L16
_L16:
        i = -1;
          goto _L14
        i = -1;
          goto _L14
    }

    public void seekPlay()
    {
        if (mAudioTrack != null)
        {
            if (mAudioTrack.getPlayState() == 3)
            {
                mAudioTrack.pause();
                mAudioTrack.flush();
                audioTrackStart();
                return;
            }
            if (mAudioTrack.getPlayState() == 2)
            {
                mAudioTrack.flush();
                return;
            }
        }
    }

    public void setVolume(float f)
    {
        mVolume = f;
        if (android.os.Build.VERSION.SDK_INT >= 21)
        {
            setVolumeV21(mAudioTrack, f);
        } else
        {
            setVolumeV3(mAudioTrack, f);
        }
        setVolumeV3(mAudioTrack, f);
    }

    public void startPlay()
    {
        if (mAudioTrack != null)
        {
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay0");
            if (mAudioTrack.getPlayState() != 3)
            {
                audioTrackStart();
                Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay1");
            }
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay isRunning0");
            if (!isRunning)
            {
                Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread startPlay isRunning1");
                isRunning = true;
                start();
                return;
            }
        }
    }

    public void stopPlay()
    {
        if (mAudioTrack != null)
        {
            isPlaying = false;
            Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread stopPlay0");
            if (mAudioTrack.getPlayState() != 1)
            {
                mAudioTrack.stop();
                mAudioTrack.flush();
                Logger.log(XMediaplayerJNI.Tag, "AudioTrackPlayThread stopPlay1");
                return;
            }
        }
    }

    static 
    {
        DEF_BUFF_SIZE = 8192;
    }
}
