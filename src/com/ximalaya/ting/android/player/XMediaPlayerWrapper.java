// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.content.Context;
import android.text.TextUtils;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaplayerImpl, PlayerUtil, XMediaplayerJNI, Logger, 
//            SMediaPlayer, XMediaPlayer

public class XMediaPlayerWrapper
    implements XMediaplayerImpl
{

    private boolean isUseSystemPlayer;
    private XMediaPlayer.OnBufferingUpdateListener mOnBufferingUpdateListener;
    private XMediaPlayer.OnCompletionListener mOnCompletionListener;
    private XMediaPlayer.OnErrorListener mOnErrorListener;
    private XMediaPlayer.OnInfoListener mOnInfoListener;
    private XMediaPlayer.OnPositionChangeListener mOnPositionChangeListener;
    private XMediaPlayer.OnPreparedListener mOnPreparedListener;
    private XMediaPlayer.OnSeekCompleteListener mOnSeekCompleteListener;
    private String mPlayUrl;
    private XMediaplayerImpl mXMediaplayerImpl;

    public XMediaPlayerWrapper()
    {
        isUseSystemPlayer = false;
        init();
    }

    public XMediaPlayerWrapper(boolean flag)
    {
        isUseSystemPlayer = false;
        if (flag)
        {
            isUseSystemPlayer = true;
            mXMediaplayerImpl = newXMediaplayer();
            return;
        } else
        {
            init();
            return;
        }
    }

    private void init()
    {
        isUseSystemPlayer = false;
        String s = PlayerUtil.getCpuInfo();
        String s1 = System.getProperty("os.arch");
        if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s1))
        {
            if (s.contains("Marvell") && s1.contains("armv5tel"))
            {
                isUseSystemPlayer = true;
            }
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("cpuinfo:")).append(s).append("cpuArch:").append(s1).toString());
        } else
        {
            Logger.log(XMediaplayerJNI.Tag, (new StringBuilder("cpuinfo null:")).append(s).append("cpuArch:").append(s1).toString());
        }
        mXMediaplayerImpl = newXMediaplayer();
    }

    private XMediaplayerImpl newXMediaplayer()
    {
        if (isUseSystemPlayer)
        {
            mXMediaplayerImpl = new SMediaPlayer();
        } else
        {
            mXMediaplayerImpl = new XMediaPlayer();
        }
        return mXMediaplayerImpl;
    }

    private void reUseSMediaplayer()
    {
        Object obj1;
        obj1 = null;
        isUseSystemPlayer = true;
        mXMediaplayerImpl.setOnBufferingUpdateListener(null);
        mXMediaplayerImpl.setOnCompletionListener(null);
        mXMediaplayerImpl.setOnErrorListener(null);
        mXMediaplayerImpl.setOnInfoListener(null);
        mXMediaplayerImpl.setOnPreparedListener(null);
        mXMediaplayerImpl.setOnSeekCompleteListener(null);
        mXMediaplayerImpl.setOnPositionChangeListener(null);
        mXMediaplayerImpl = newXMediaplayer();
        mXMediaplayerImpl.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
        mXMediaplayerImpl.setOnCompletionListener(mOnCompletionListener);
        mXMediaplayerImpl.setOnErrorListener(mOnErrorListener);
        mXMediaplayerImpl.setOnInfoListener(mOnInfoListener);
        mXMediaplayerImpl.setOnPreparedListener(mOnPreparedListener);
        mXMediaplayerImpl.setOnSeekCompleteListener(mOnSeekCompleteListener);
        mXMediaplayerImpl.setOnPositionChangeListener(mOnPositionChangeListener);
        if (TextUtils.isEmpty(mPlayUrl)) goto _L2; else goto _L1
_L1:
        if (!mPlayUrl.startsWith("http")) goto _L4; else goto _L3
_L3:
        mXMediaplayerImpl.setDataSource(mPlayUrl);
_L6:
        mXMediaplayerImpl.prepareAsync();
_L2:
        return;
_L4:
        Object obj = new FileInputStream(mPlayUrl);
        mXMediaplayerImpl.setDataSource(((FileInputStream) (obj)).getFD(), mPlayUrl);
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        continue; /* Loop/switch isn't completed */
        obj;
        obj = null;
_L9:
        if (obj != null)
        {
            try
            {
                ((FileInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        if (true) goto _L6; else goto _L5
_L5:
        obj;
_L8:
        if (obj1 != null)
        {
            try
            {
                ((FileInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1) { }
        }
        throw obj;
        Exception exception1;
        exception1;
        obj1 = obj;
        obj = exception1;
        if (true) goto _L8; else goto _L7
_L7:
        Exception exception;
        exception;
          goto _L9
    }

    public XMediaplayerJNI.AudioType getAudioType()
    {
        return mXMediaplayerImpl.getAudioType();
    }

    public int getCurrentPosition()
    {
        return mXMediaplayerImpl.getCurrentPosition();
    }

    public int getDuration()
    {
        return mXMediaplayerImpl.getDuration();
    }

    public int getPlayState()
    {
        return mXMediaplayerImpl.getPlayState();
    }

    public boolean isPlaying()
    {
        return mXMediaplayerImpl.isPlaying();
    }

    public boolean isUseSystemPlayer()
    {
        return isUseSystemPlayer;
    }

    public void pause()
    {
        mXMediaplayerImpl.pause();
    }

    public void prepareAsync()
    {
        mXMediaplayerImpl.prepareAsync();
    }

    public void release()
    {
        mXMediaplayerImpl.release();
    }

    public void removeProxy()
    {
        PlayerUtil.setProxyHost(null);
        PlayerUtil.setProxyPort(0);
        PlayerUtil.setAuthorization(null);
    }

    public void reset()
    {
        mXMediaplayerImpl.reset();
    }

    public void seekTo(int i)
    {
        mXMediaplayerImpl.seekTo(i);
    }

    public void setAudioStreamType(int i)
    {
        mXMediaplayerImpl.setAudioStreamType(i);
    }

    public void setDataSource(FileDescriptor filedescriptor, String s)
    {
        mPlayUrl = s;
        mXMediaplayerImpl.setDataSource(filedescriptor, s);
    }

    public void setDataSource(String s)
    {
        mPlayUrl = s;
        mXMediaplayerImpl.setDataSource(s);
    }

    public void setDownloadBufferSize(long l)
    {
        mXMediaplayerImpl.setDownloadBufferSize(l);
    }

    public void setOnBufferingUpdateListener(XMediaPlayer.OnBufferingUpdateListener onbufferingupdatelistener)
    {
        mOnBufferingUpdateListener = onbufferingupdatelistener;
        mXMediaplayerImpl.setOnBufferingUpdateListener(mOnBufferingUpdateListener);
    }

    public void setOnCompletionListener(XMediaPlayer.OnCompletionListener oncompletionlistener)
    {
        mOnCompletionListener = oncompletionlistener;
        mXMediaplayerImpl.setOnCompletionListener(mOnCompletionListener);
    }

    public void setOnErrorListener(XMediaPlayer.OnErrorListener onerrorlistener)
    {
        mOnErrorListener = onerrorlistener;
        mXMediaplayerImpl.setOnErrorListener(new _cls1());
    }

    public void setOnInfoListener(XMediaPlayer.OnInfoListener oninfolistener)
    {
        mOnInfoListener = oninfolistener;
        mXMediaplayerImpl.setOnInfoListener(mOnInfoListener);
    }

    public void setOnPositionChangeListener(XMediaPlayer.OnPositionChangeListener onpositionchangelistener)
    {
        mOnPositionChangeListener = onpositionchangelistener;
        mXMediaplayerImpl.setOnPositionChangeListener(mOnPositionChangeListener);
    }

    public void setOnPreparedListener(XMediaPlayer.OnPreparedListener onpreparedlistener)
    {
        mOnPreparedListener = onpreparedlistener;
        mXMediaplayerImpl.setOnPreparedListener(mOnPreparedListener);
    }

    public void setOnSeekCompleteListener(XMediaPlayer.OnSeekCompleteListener onseekcompletelistener)
    {
        mOnSeekCompleteListener = onseekcompletelistener;
        mXMediaplayerImpl.setOnSeekCompleteListener(mOnSeekCompleteListener);
    }

    public void setProxy(String s, int i, String s1)
    {
        PlayerUtil.setProxyHost(s);
        PlayerUtil.setProxyPort(i);
        PlayerUtil.setAuthorization(s1);
    }

    public void setVolume(float f, float f1)
    {
        mXMediaplayerImpl.setVolume(f, f1);
    }

    public void setWakeMode(Context context, int i)
    {
        mXMediaplayerImpl.setWakeMode(context, i);
    }

    public void start()
    {
        mXMediaplayerImpl.start();
    }

    public void stop()
    {
        mXMediaplayerImpl.stop();
    }




    private class _cls1
        implements XMediaPlayer.OnErrorListener
    {

        final XMediaPlayerWrapper this$0;

        public boolean onError(XMediaplayerImpl xmediaplayerimpl, int i, int j)
        {
            if (j == -1011)
            {
                reUseSMediaplayer();
                return true;
            }
            if (mOnErrorListener != null)
            {
                return mOnErrorListener.onError(mXMediaplayerImpl, i, j);
            } else
            {
                return false;
            }
        }

        _cls1()
        {
            this$0 = XMediaPlayerWrapper.this;
            super();
        }
    }

}
