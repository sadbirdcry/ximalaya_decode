// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;


// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaplayerJNI

public static final class value extends Enum
{

    private static final ERR_ARCH_NOT_SUPPORT ENUM$VALUES[];
    public static final ERR_ARCH_NOT_SUPPORT ERR_ARCH_NOT_SUPPORT;
    public static final ERR_ARCH_NOT_SUPPORT ERR_DECODEDATA_FILLIO_FAIL;
    public static final ERR_ARCH_NOT_SUPPORT ERR_DECODE_NOT_SUPPORT;
    public static final ERR_ARCH_NOT_SUPPORT ERR_FILE_MANAGER_INNER_ERR;
    public static final ERR_ARCH_NOT_SUPPORT ERR_M3U8STREAM_FILLIO_FAIL;
    public static final ERR_ARCH_NOT_SUPPORT ERR_M3U8_FILE_CONTENT_INVALID;
    public static final ERR_ARCH_NOT_SUPPORT ERR_NOTOK;
    public static final ERR_ARCH_NOT_SUPPORT NO_ERR;
    private int value;

    public static value valueOf(int i)
    {
        switch (i)
        {
        default:
            return null;

        case 0: // '\0'
            return NO_ERR;

        case -1: 
            return ERR_NOTOK;

        case -2: 
            return ERR_DECODE_NOT_SUPPORT;

        case -3: 
            return ERR_M3U8_FILE_CONTENT_INVALID;

        case -4: 
            return ERR_FILE_MANAGER_INNER_ERR;

        case -5: 
            return ERR_DECODEDATA_FILLIO_FAIL;

        case -6: 
            return ERR_M3U8STREAM_FILLIO_FAIL;

        case -7: 
            return ERR_ARCH_NOT_SUPPORT;
        }
    }

    public static ERR_ARCH_NOT_SUPPORT valueOf(String s)
    {
        return (ERR_ARCH_NOT_SUPPORT)Enum.valueOf(com/ximalaya/ting/android/player/XMediaplayerJNI$NativeErrorType, s);
    }

    public static ERR_ARCH_NOT_SUPPORT[] values()
    {
        ERR_ARCH_NOT_SUPPORT aerr_arch_not_support[] = ENUM$VALUES;
        int i = aerr_arch_not_support.length;
        ERR_ARCH_NOT_SUPPORT aerr_arch_not_support1[] = new ENUM.VALUES[i];
        System.arraycopy(aerr_arch_not_support, 0, aerr_arch_not_support1, 0, i);
        return aerr_arch_not_support1;
    }

    public int value()
    {
        return value;
    }

    static 
    {
        NO_ERR = new <init>("NO_ERR", 0, 0);
        ERR_NOTOK = new <init>("ERR_NOTOK", 1, -1);
        ERR_DECODE_NOT_SUPPORT = new <init>("ERR_DECODE_NOT_SUPPORT", 2, -2);
        ERR_M3U8_FILE_CONTENT_INVALID = new <init>("ERR_M3U8_FILE_CONTENT_INVALID", 3, -3);
        ERR_FILE_MANAGER_INNER_ERR = new <init>("ERR_FILE_MANAGER_INNER_ERR", 4, -4);
        ERR_DECODEDATA_FILLIO_FAIL = new <init>("ERR_DECODEDATA_FILLIO_FAIL", 5, -5);
        ERR_M3U8STREAM_FILLIO_FAIL = new <init>("ERR_M3U8STREAM_FILLIO_FAIL", 6, -6);
        ERR_ARCH_NOT_SUPPORT = new <init>("ERR_ARCH_NOT_SUPPORT", 7, -7);
        ENUM$VALUES = (new ENUM.VALUES[] {
            NO_ERR, ERR_NOTOK, ERR_DECODE_NOT_SUPPORT, ERR_M3U8_FILE_CONTENT_INVALID, ERR_FILE_MANAGER_INNER_ERR, ERR_DECODEDATA_FILLIO_FAIL, ERR_M3U8STREAM_FILLIO_FAIL, ERR_ARCH_NOT_SUPPORT
        });
    }

    private (String s, int i, int j)
    {
        super(s, i);
        value = 0;
        value = j;
    }
}
