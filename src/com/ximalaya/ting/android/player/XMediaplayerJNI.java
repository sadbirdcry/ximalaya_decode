// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.text.TextUtils;
import com.ximalaya.ting.android.player.model.JNIDataModel;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.nio.Buffer;
import java.nio.ByteBuffer;

// Referenced classes of package com.ximalaya.ting.android.player:
//            XMediaplayerImpl, PlayerUtil, Logger, HlsAudioFile, 
//            AudioTrackPlayThread, XMediaPlayerConstants, AudioFileRequestHandler

public abstract class XMediaplayerJNI
    implements XMediaplayerImpl
{
    public static final class AudioType extends Enum
    {

        private static final AudioType ENUM$VALUES[];
        public static final AudioType HLS_FILE;
        public static final AudioType M3U8_FILE;
        public static final AudioType M3U8_STATIC_FILE;
        public static final AudioType NORMAL_FILE;
        private int value;

        public static AudioType valueOf(int i)
        {
            switch (i)
            {
            default:
                return null;

            case 0: // '\0'
                return NORMAL_FILE;

            case 1: // '\001'
                return M3U8_STATIC_FILE;

            case 2: // '\002'
                return HLS_FILE;

            case 3: // '\003'
                return M3U8_FILE;
            }
        }

        public static AudioType valueOf(String s)
        {
            return (AudioType)Enum.valueOf(com/ximalaya/ting/android/player/XMediaplayerJNI$AudioType, s);
        }

        public static AudioType[] values()
        {
            AudioType aaudiotype[] = ENUM$VALUES;
            int i = aaudiotype.length;
            AudioType aaudiotype1[] = new AudioType[i];
            System.arraycopy(aaudiotype, 0, aaudiotype1, 0, i);
            return aaudiotype1;
        }

        public int value()
        {
            return value;
        }

        static 
        {
            NORMAL_FILE = new AudioType("NORMAL_FILE", 0, 0);
            M3U8_STATIC_FILE = new AudioType("M3U8_STATIC_FILE", 1, 1);
            HLS_FILE = new AudioType("HLS_FILE", 2, 2);
            M3U8_FILE = new AudioType("M3U8_FILE", 3, 3);
            ENUM$VALUES = (new AudioType[] {
                NORMAL_FILE, M3U8_STATIC_FILE, HLS_FILE, M3U8_FILE
            });
        }

        private AudioType(String s, int i, int j)
        {
            super(s, i);
            value = 0;
            value = j;
        }
    }

    public static final class NativeErrorType extends Enum
    {

        private static final NativeErrorType ENUM$VALUES[];
        public static final NativeErrorType ERR_ARCH_NOT_SUPPORT;
        public static final NativeErrorType ERR_DECODEDATA_FILLIO_FAIL;
        public static final NativeErrorType ERR_DECODE_NOT_SUPPORT;
        public static final NativeErrorType ERR_FILE_MANAGER_INNER_ERR;
        public static final NativeErrorType ERR_M3U8STREAM_FILLIO_FAIL;
        public static final NativeErrorType ERR_M3U8_FILE_CONTENT_INVALID;
        public static final NativeErrorType ERR_NOTOK;
        public static final NativeErrorType NO_ERR;
        private int value;

        public static NativeErrorType valueOf(int i)
        {
            switch (i)
            {
            default:
                return null;

            case 0: // '\0'
                return NO_ERR;

            case -1: 
                return ERR_NOTOK;

            case -2: 
                return ERR_DECODE_NOT_SUPPORT;

            case -3: 
                return ERR_M3U8_FILE_CONTENT_INVALID;

            case -4: 
                return ERR_FILE_MANAGER_INNER_ERR;

            case -5: 
                return ERR_DECODEDATA_FILLIO_FAIL;

            case -6: 
                return ERR_M3U8STREAM_FILLIO_FAIL;

            case -7: 
                return ERR_ARCH_NOT_SUPPORT;
            }
        }

        public static NativeErrorType valueOf(String s)
        {
            return (NativeErrorType)Enum.valueOf(com/ximalaya/ting/android/player/XMediaplayerJNI$NativeErrorType, s);
        }

        public static NativeErrorType[] values()
        {
            NativeErrorType anativeerrortype[] = ENUM$VALUES;
            int i = anativeerrortype.length;
            NativeErrorType anativeerrortype1[] = new NativeErrorType[i];
            System.arraycopy(anativeerrortype, 0, anativeerrortype1, 0, i);
            return anativeerrortype1;
        }

        public int value()
        {
            return value;
        }

        static 
        {
            NO_ERR = new NativeErrorType("NO_ERR", 0, 0);
            ERR_NOTOK = new NativeErrorType("ERR_NOTOK", 1, -1);
            ERR_DECODE_NOT_SUPPORT = new NativeErrorType("ERR_DECODE_NOT_SUPPORT", 2, -2);
            ERR_M3U8_FILE_CONTENT_INVALID = new NativeErrorType("ERR_M3U8_FILE_CONTENT_INVALID", 3, -3);
            ERR_FILE_MANAGER_INNER_ERR = new NativeErrorType("ERR_FILE_MANAGER_INNER_ERR", 4, -4);
            ERR_DECODEDATA_FILLIO_FAIL = new NativeErrorType("ERR_DECODEDATA_FILLIO_FAIL", 5, -5);
            ERR_M3U8STREAM_FILLIO_FAIL = new NativeErrorType("ERR_M3U8STREAM_FILLIO_FAIL", 6, -6);
            ERR_ARCH_NOT_SUPPORT = new NativeErrorType("ERR_ARCH_NOT_SUPPORT", 7, -7);
            ENUM$VALUES = (new NativeErrorType[] {
                NO_ERR, ERR_NOTOK, ERR_DECODE_NOT_SUPPORT, ERR_M3U8_FILE_CONTENT_INVALID, ERR_FILE_MANAGER_INNER_ERR, ERR_DECODEDATA_FILLIO_FAIL, ERR_M3U8STREAM_FILLIO_FAIL, ERR_ARCH_NOT_SUPPORT
            });
        }

        private NativeErrorType(String s, int i, int j)
        {
            super(s, i);
            value = 0;
            value = j;
        }
    }


    private static int $SWITCH_TABLE$com$ximalaya$ting$android$player$XMediaplayerJNI$NativeErrorType[];
    public static String Tag;
    public boolean isBuffing;
    private boolean isSeeking;
    private AudioFileRequestHandler mAudioFileRequestHandler;
    private AudioTrackPlayThread mAudioTrackPlayThread;
    private AudioType mAudioType;
    private long mCurFileSize;
    public String mCurrentDataDecodeUrl;
    private byte mCurrentDecodeData[];
    private int mCurrentSeekPosition;
    private int mCurrentSeekPositionState;
    private int mDownItemSize;
    private boolean mHasSeek;
    private HlsAudioFile mHlsAudioFile;
    private long mJniHandler;
    public String mPlayUrl;
    public byte tmepBuf[];

    static int[] $SWITCH_TABLE$com$ximalaya$ting$android$player$XMediaplayerJNI$NativeErrorType()
    {
        int ai[] = $SWITCH_TABLE$com$ximalaya$ting$android$player$XMediaplayerJNI$NativeErrorType;
        if (ai != null)
        {
            return ai;
        }
        ai = new int[NativeErrorType.values().length];
        try
        {
            ai[NativeErrorType.ERR_ARCH_NOT_SUPPORT.ordinal()] = 8;
        }
        catch (NoSuchFieldError nosuchfielderror7) { }
        try
        {
            ai[NativeErrorType.ERR_DECODEDATA_FILLIO_FAIL.ordinal()] = 6;
        }
        catch (NoSuchFieldError nosuchfielderror6) { }
        try
        {
            ai[NativeErrorType.ERR_DECODE_NOT_SUPPORT.ordinal()] = 3;
        }
        catch (NoSuchFieldError nosuchfielderror5) { }
        try
        {
            ai[NativeErrorType.ERR_FILE_MANAGER_INNER_ERR.ordinal()] = 5;
        }
        catch (NoSuchFieldError nosuchfielderror4) { }
        try
        {
            ai[NativeErrorType.ERR_M3U8STREAM_FILLIO_FAIL.ordinal()] = 7;
        }
        catch (NoSuchFieldError nosuchfielderror3) { }
        try
        {
            ai[NativeErrorType.ERR_M3U8_FILE_CONTENT_INVALID.ordinal()] = 4;
        }
        catch (NoSuchFieldError nosuchfielderror2) { }
        try
        {
            ai[NativeErrorType.ERR_NOTOK.ordinal()] = 2;
        }
        catch (NoSuchFieldError nosuchfielderror1) { }
        try
        {
            ai[NativeErrorType.NO_ERR.ordinal()] = 1;
        }
        catch (NoSuchFieldError nosuchfielderror) { }
        $SWITCH_TABLE$com$ximalaya$ting$android$player$XMediaplayerJNI$NativeErrorType = ai;
        return ai;
    }

    public XMediaplayerJNI()
    {
        mDownItemSize = 32768;
        mCurrentSeekPosition = -1;
        mCurrentSeekPositionState = 0;
        mCurrentDataDecodeUrl = mPlayUrl;
        mHasSeek = true;
        isSeeking = false;
        isBuffing = false;
        mAudioType = AudioType.NORMAL_FILE;
        int i = MediaplayerFrameworkInit();
        Logger.log(Tag, (new StringBuilder("MediaplayerFrameworkInit result:")).append(i).toString());
        if (i < 0)
        {
            handlePlayerStatus(NativeErrorType.ERR_ARCH_NOT_SUPPORT.value());
        }
    }

    private String getCurPlayUrl()
    {
        if (mAudioType != AudioType.NORMAL_FILE)
        {
            if (mHlsAudioFile != null)
            {
                return mHlsAudioFile.getPlayUrl();
            } else
            {
                throw new RuntimeException("getCurPlayUrl \u9519\u8BEF\uFF01");
            }
        } else
        {
            return mPlayUrl;
        }
    }

    private void handleSmallBufRequest(JNIDataModel jnidatamodel)
    {
        if (jnidatamodel.buf.length > jnidatamodel.bufSize)
        {
            ByteBuffer bytebuffer = ByteBuffer.wrap(jnidatamodel.buf);
            bytebuffer.clear();
            bytebuffer.position(jnidatamodel.bufSize).limit(jnidatamodel.buf.length);
            ByteBuffer bytebuffer1 = bytebuffer.slice();
            tmepBuf = new byte[bytebuffer1.remaining()];
            bytebuffer1.get(tmepBuf);
            Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT length diff tJNIDataModel.bufSize:")).append(jnidatamodel.bufSize).toString());
            Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT length diff tJNIDataModel.buf:")).append(jnidatamodel.buf.length).toString());
            if (tmepBuf != null)
            {
                Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT length diff tmepBuf:")).append(tmepBuf.length).toString());
            }
            bytebuffer.clear();
            bytebuffer.position(0).limit(jnidatamodel.bufSize);
            bytebuffer = bytebuffer.slice();
            jnidatamodel.buf = new byte[bytebuffer.remaining()];
            bytebuffer.get(jnidatamodel.buf);
            Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT length diff tJNIDataModel.buf:")).append(jnidatamodel.buf.length).toString());
            if (tmepBuf != null)
            {
                Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT length diff all1:")).append(jnidatamodel.buf.length + tmepBuf.length).toString());
            }
        }
    }

    private void releaseAudioTrackPlayThread()
    {
        if (mAudioTrackPlayThread != null)
        {
            mAudioTrackPlayThread.releasePlay();
            mAudioTrackPlayThread = null;
        }
        Logger.log(Tag, "releaseAudioTrackPlayThread releasePlay");
    }

    public native int MediaplayerComplete(long l);

    public native int MediaplayerDestroy(long l);

    public native int MediaplayerFrameworkInit();

    public native int MediaplayerGetCurPlayingFileType(long l);

    public native long MediaplayerGetCurrentDecodedDataTime(long l);

    public native long MediaplayerGetCurrentTime(long l);

    public native long MediaplayerGetMediaDuration(long l);

    public native int MediaplayerGetOutputData(byte abyte0[], int i, long l);

    public native int MediaplayerInit(int i, int j, int k, int l);

    public native int MediaplayerOutputDataAppointment(long l);

    public native int MediaplayerPause(long l);

    public native int MediaplayerPlay(long l);

    public native int MediaplayerPrepareAsync(long l);

    public native int MediaplayerReset(long l);

    public native int MediaplayerSeek(long l, long l1);

    public native int MediaplayerSetDataSourceInfo(String s, int i, long l);

    public native int MediaplayerStop(long l);

    public int bufferedDataReachThresholdCallBackT(int i)
    {
        if (!NativeErrorType.NO_ERR.equals(NativeErrorType.valueOf(i)))
        {
            handlePlayerStatus(i);
            return -1;
        } else
        {
            getAudioTrackPlayThread().dataReady();
            Logger.log(Tag, "dataStreamInputFuncCallBackT bufferedDataReachThresholdCallBackT");
            return 1;
        }
    }

    public int dataStreamInputFuncCallBackT(JNIDataModel jnidatamodel)
    {
        if (NativeErrorType.NO_ERR.equals(NativeErrorType.valueOf(jnidatamodel.status))) goto _L2; else goto _L1
_L1:
        Logger.log(Tag, "dataStreamInputFuncCallBackT readData start error:");
        handlePlayerStatus(jnidatamodel.status);
_L10:
        return -1;
_L2:
        long l;
        l = System.currentTimeMillis();
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT readData start:")).append(l).toString());
        if (jnidatamodel.filePath == null)
        {
            throw new RuntimeException("dataStreamInputFuncCallBackT tJNIDataModel.filePath==null \u5F02\u5E38\uFF01\uFF01\uFF01");
        }
        Logger.log(Tag, "dataStreamInputFuncCallBackT xx seekParaTimeStampMs fill io start");
        if (!jnidatamodel.filePath.equals(getCurPlayUrl()))
        {
            mCurrentSeekPosition = -1;
            mCurFileSize = 0L;
            mCurrentSeekPositionState = 0;
            mHasSeek = true;
            tmepBuf = null;
            Logger.log(Tag, "dataStreamInputFuncCallBackT \u91CD\u7F6E");
        }
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT filePath:")).append(jnidatamodel.filePath).toString());
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT seekParaTimeStampMs mCurrentSeekPositionState:")).append(mCurrentSeekPositionState).toString());
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT seekParaTimeStampMs mCurrentSeekPosition:")).append(mCurrentSeekPosition).toString());
        if (mHasSeek && mCurrentSeekPosition != mCurrentSeekPositionState)
        {
            mCurrentSeekPosition = mCurrentSeekPositionState;
            Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT seekParaTimeStampMs true mCurrentSeekPosition:")).append(mCurrentSeekPosition).toString());
            Logger.log(Tag, "dataStreamInputFuncCallBackT length diff seek true");
        } else
        {
            mHasSeek = false;
        }
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT mCurFileSize:")).append(mCurFileSize).append("mCurrentSeekPosition:").append(mCurrentSeekPosition).toString());
        if (mCurFileSize > 0L && (long)mCurrentSeekPosition >= mCurFileSize)
        {
            Logger.log(Tag, "dataStreamInputFuncCallBackT xx return 0");
            return 0;
        }
        if (isHls()) goto _L4; else goto _L3
_L3:
        Logger.log(Tag, "dataStreamInputFuncCallBackT NORMAL_FILE");
        if (mAudioFileRequestHandler == null)
        {
            if (XMediaPlayerConstants.isDebug)
            {
                throw new RuntimeException("mAudioFileRequestHandler==null");
            } else
            {
                Logger.log(Tag, "dataStreamInputFuncCallBackT xx 19");
                return -1;
            }
        }
        if (mAudioFileRequestHandler.readData(jnidatamodel, mHasSeek, mCurrentSeekPosition) <= 0) goto _L6; else goto _L5
_L5:
        mHasSeek = false;
_L8:
        if (jnidatamodel.buf == null)
        {
            Logger.log(Tag, "dataStreamInputFuncCallBackT xx tJNIDataModel.buf==null");
            return -1;
        }
        break MISSING_BLOCK_LABEL_585;
_L6:
        Logger.log(Tag, "dataStreamInputFuncCallBackT xx 20");
        return -1;
_L4:
        if (mHlsAudioFile == null)
        {
            if (XMediaPlayerConstants.isDebug)
            {
                throw new RuntimeException("mHlsAudioFile==null");
            } else
            {
                Logger.log(Tag, "dataStreamInputFuncCallBackT xx 21");
                return -1;
            }
        }
        if (mHasSeek || tmepBuf == null)
        {
            break; /* Loop/switch isn't completed */
        }
        jnidatamodel.buf = tmepBuf;
        tmepBuf = null;
        jnidatamodel.fileSize = mCurFileSize;
        Logger.log(Tag, "dataStreamInputFuncCallBackT read temp buf");
_L11:
        mHasSeek = false;
        if (true) goto _L8; else goto _L7
_L7:
        tmepBuf = null;
        Logger.log(Tag, "dataStreamInputFuncCallBackT M3U8_FILE");
        if (mHlsAudioFile.readData(jnidatamodel) < 0L) goto _L10; else goto _L9
_L9:
        mCurFileSize = jnidatamodel.fileSize;
          goto _L11
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT buf.length")).append(jnidatamodel.buf.length).toString());
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT mCurFileSize:")).append(mCurFileSize).toString());
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT tJNIDataModel.buf.length:")).append(jnidatamodel.buf.length).toString());
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT tJNIDataModel.bufSize:")).append(jnidatamodel.bufSize).toString());
        handleSmallBufRequest(jnidatamodel);
        if (jnidatamodel.buf != null)
        {
            mCurrentSeekPosition = mCurrentSeekPosition + jnidatamodel.buf.length;
        }
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT tJNIDataModel.buf.length:")).append(jnidatamodel.buf.length).toString());
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT mCurFileSize2:")).append(mCurFileSize).toString());
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT tJNIDataModel filesize:")).append(jnidatamodel.fileSize).toString());
        Logger.log(Tag, "dataStreamInputFuncCallBackT xx seekParaTimeStampMs fill io end");
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT readData end:")).append(System.currentTimeMillis() - l).toString());
        return jnidatamodel.buf.length;
    }

    public int dataStreamOutReadyFuncCallBackT(int i, int j)
    {
        Logger.log(Tag, "dataStreamOutReadyFuncCallBackT");
        if (!NativeErrorType.NO_ERR.equals(NativeErrorType.valueOf(j)))
        {
            handlePlayerStatus(j);
            return -1;
        } else
        {
            onPreparedInner();
            return 0;
        }
    }

    public int dataStreamSeekFuncCallBackT(String s, long l, int i)
    {
        Logger.log(Tag, (new StringBuilder("dataStreamSeekFuncCallBackT 0 offset:")).append(l).toString());
        if (!NativeErrorType.NO_ERR.equals(NativeErrorType.valueOf(i)))
        {
            handlePlayerStatus(i);
            return -1;
        }
        if (!TextUtils.isEmpty(s))
        {
            mCurrentDataDecodeUrl = s;
        }
        mHasSeek = true;
        mCurrentSeekPositionState = (int)l;
        Logger.log(Tag, (new StringBuilder("dataStreamInputFuncCallBackT seekParaTimeStampMs true offset:")).append(l).toString());
        return 0;
    }

    public AudioTrackPlayThread getAudioTrackPlayThread()
    {
        if (mAudioTrackPlayThread == null)
        {
            Logger.log(Tag, "dataStreamInputFuncCallBackT getAudioTrackPlayThread new");
            mAudioTrackPlayThread = new AudioTrackPlayThread(this);
        }
        return mAudioTrackPlayThread;
    }

    public AudioType getAudioType()
    {
        return mAudioType;
    }

    public int getCachePercent()
    {
        if (!isHls()) goto _L2; else goto _L1
_L1:
        if (mHlsAudioFile != null) goto _L4; else goto _L3
_L3:
        return 0;
_L4:
        return mHlsAudioFile.getCachePercent();
_L2:
        if (mAudioFileRequestHandler != null)
        {
            return mAudioFileRequestHandler.getCachePercent();
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    public long getCurFileSize()
    {
        return mCurFileSize;
    }

    public int getCurrentPosition()
    {
        return (int)MediaplayerGetCurrentTime(mJniHandler);
    }

    public int getDuration()
    {
        return (int)MediaplayerGetMediaDuration(mJniHandler);
    }

    public int getOutputData(byte abyte0[], int i)
    {
        return MediaplayerGetOutputData(abyte0, i, mJniHandler);
    }

    public String getPlayUrl()
    {
        return mPlayUrl;
    }

    public void handlePlayerStatus(int i)
    {
        NativeErrorType nativeerrortype;
        nativeerrortype = NativeErrorType.valueOf(i);
        Logger.log(Tag, (new StringBuilder("handlePlayerStatus status:")).append(i).toString());
        if (nativeerrortype != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        switch ($SWITCH_TABLE$com$ximalaya$ting$android$player$XMediaplayerJNI$NativeErrorType()[nativeerrortype.ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            break;

        case 2: // '\002'
            onErrorInner(8, 1);
            return;

        case 6: // '\006'
            onErrorInner(8, -1004);
            return;

        case 3: // '\003'
            onErrorInner(8, -1010);
            return;

        case 5: // '\005'
            onErrorInner(8, 100);
            return;

        case 7: // '\007'
            onErrorInner(8, -1004);
            return;

        case 4: // '\004'
            onErrorInner(8, -1004);
            return;

        case 8: // '\b'
            onErrorInner(8, -1011);
            break; /* Loop/switch isn't completed */
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public boolean isHls()
    {
        return mAudioType.value() > 0 && mAudioType.value() < 4;
    }

    public boolean isPlaying()
    {
        Logger.log(Tag, "dataStreamInputFuncCallBackT isPlaying");
        return getAudioTrackPlayThread().isPlaying();
    }

    public boolean isSeeking()
    {
        return isSeeking;
    }

    public int m3u8FileStreamInputFuncCallBackT(JNIDataModel jnidatamodel)
    {
        int i;
        i = 1;
        if (!NativeErrorType.NO_ERR.equals(NativeErrorType.valueOf(jnidatamodel.status)))
        {
            handlePlayerStatus(jnidatamodel.status);
            return -1;
        }
        if (jnidatamodel == null || jnidatamodel.bufSize <= 0)
        {
            Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tJNIDataModel null");
            return -1;
        }
        Logger.log(Tag, "HlsReadThread downUrl0  m3u8FileStreamInputFuncCallBackT");
_L16:
        if (i > 0) goto _L2; else goto _L1
_L1:
        Object obj;
        Object obj1;
        Object obj2;
        Object obj3;
        Object obj4;
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 4");
        byte abyte0[];
        int j;
        if (jnidatamodel.buf == null || jnidatamodel.buf.length > jnidatamodel.bufSize || jnidatamodel.buf.length == 0)
        {
            Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT null end 4");
            return 0;
        } else
        {
            Logger.log(Tag, (new StringBuilder("m3u8FileStreamInputFuncCallBackT contentLength 5:")).append(jnidatamodel.buf.length).toString());
            Logger.log(Tag, (new StringBuilder("m3u8FileStreamInputFuncCallBackT buf:")).append(new String(jnidatamodel.buf)).toString());
            return jnidatamodel.buf.length;
        }
_L2:
        obj1 = PlayerUtil.getHttpURLConnection(mPlayUrl);
        ((HttpURLConnection) (obj1)).setUseCaches(true);
        j = ((HttpURLConnection) (obj1)).getResponseCode();
        Logger.log(Tag, (new StringBuilder("m3u8FileStreamInputFuncCallBackT 0 responseCode:")).append(j).toString());
        if (j != 200 && j != 206) goto _L4; else goto _L3
_L3:
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT 1");
        obj = ((HttpURLConnection) (obj1)).getInputStream();
        obj2 = new ByteArrayOutputStream();
        abyte0 = new byte[1024];
_L9:
        j = ((InputStream) (obj)).read(abyte0);
        if (j != -1) goto _L6; else goto _L5
_L5:
        Logger.log(Tag, (new StringBuilder("m3u8FileStreamInputFuncCallBackT len000:")).append(j).toString());
        jnidatamodel.buf = ((ByteArrayOutputStream) (obj2)).toByteArray();
        ((ByteArrayOutputStream) (obj2)).close();
_L17:
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 0");
        if (obj1 != null)
        {
            ((HttpURLConnection) (obj1)).disconnect();
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 1");
        if (obj != null)
        {
            try
            {
                ((InputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 2");
_L10:
        if (jnidatamodel.buf == null || jnidatamodel.buf.length <= 0) goto _L8; else goto _L7
_L7:
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 3");
          goto _L1
_L6:
        ((ByteArrayOutputStream) (obj2)).write(abyte0, 0, j);
        Logger.log(Tag, (new StringBuilder("m3u8FileStreamInputFuncCallBackT len:")).append(j).toString());
          goto _L9
        obj3;
        obj4 = obj1;
        obj2 = obj;
_L15:
        obj1 = obj2;
        obj = obj4;
        Logger.log(Tag, (new StringBuilder("m3u8FileStreamInputFuncCallBackT MalformedURLException:")).append(((MalformedURLException) (obj3)).toString()).toString());
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 0");
        if (obj4 != null)
        {
            ((HttpURLConnection) (obj4)).disconnect();
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 1");
        if (obj2 != null)
        {
            try
            {
                ((InputStream) (obj2)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 2");
          goto _L10
_L4:
        if (j < 400)
        {
            break MISSING_BLOCK_LABEL_957;
        }
        Logger.log(Tag, (new StringBuilder("m3u8FileStreamInputFuncCallBackT HTTP_BAD_REQUEST responseCode:")).append(j).toString());
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 0");
        if (obj1 != null)
        {
            ((HttpURLConnection) (obj1)).disconnect();
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 1");
        if (false)
        {
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (JNIDataModel jnidatamodel) { }
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 2");
        return -1;
        obj3;
        obj2 = null;
        obj4 = null;
_L14:
        obj1 = obj2;
        obj = obj4;
        Logger.log(Tag, (new StringBuilder("m3u8FileStreamInputFuncCallBackT IOException:")).append(((IOException) (obj3)).toString()).toString());
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 0");
        if (obj4 != null)
        {
            ((HttpURLConnection) (obj4)).disconnect();
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 1");
        if (obj2 != null)
        {
            try
            {
                ((InputStream) (obj2)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 2");
          goto _L10
        obj3;
        obj2 = null;
        obj4 = null;
_L13:
        obj1 = obj2;
        obj = obj4;
        Logger.log(Tag, (new StringBuilder("m3u8FileStreamInputFuncCallBackT Exception:")).append(((Exception) (obj3)).toString()).toString());
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 0");
        if (obj4 != null)
        {
            ((HttpURLConnection) (obj4)).disconnect();
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 1");
        if (obj2 != null)
        {
            try
            {
                ((InputStream) (obj2)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 2");
          goto _L10
        jnidatamodel;
        obj2 = null;
        obj = null;
_L12:
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 0");
        if (obj != null)
        {
            ((HttpURLConnection) (obj)).disconnect();
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 1");
        if (obj2 != null)
        {
            try
            {
                ((InputStream) (obj2)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }
        Logger.log(Tag, "m3u8FileStreamInputFuncCallBackT tt 2");
        throw jnidatamodel;
        jnidatamodel;
        obj2 = null;
        obj = obj1;
        continue; /* Loop/switch isn't completed */
        jnidatamodel;
        obj2 = obj;
        obj = obj1;
        continue; /* Loop/switch isn't completed */
        jnidatamodel;
        obj2 = obj1;
        if (true) goto _L12; else goto _L11
_L11:
        obj3;
        obj2 = null;
        obj4 = obj1;
          goto _L13
        obj3;
        obj2 = obj;
        obj4 = obj1;
          goto _L13
        obj3;
        obj2 = null;
        obj4 = obj1;
          goto _L14
        obj3;
        obj2 = obj;
        obj4 = obj1;
          goto _L14
        obj3;
        obj2 = null;
        obj4 = null;
          goto _L15
        obj3;
        obj2 = null;
        obj4 = obj1;
          goto _L15
_L8:
        i--;
          goto _L16
        obj = null;
          goto _L17
    }

    public int m3u8ParsedNewMediaItemInfoFuncCallBackT(String as[], int i, int j)
    {
        if (!NativeErrorType.NO_ERR.equals(NativeErrorType.valueOf(j)))
        {
            Logger.log(Tag, "m3u8ParsedNewMediaItemInfoFuncCallBackT onErrorInner");
            handlePlayerStatus(j);
        } else
        if (mHlsAudioFile != null)
        {
            Logger.log(Tag, (new StringBuilder("HlsReadThread downUrl0 m3u8ParsedNewMediaItemInfoFuncCallBackT length:")).append(as.length).toString());
            i = MediaplayerGetCurPlayingFileType(mJniHandler);
            if (i >= 0)
            {
                mAudioType = AudioType.valueOf(i);
                Logger.log(Tag, (new StringBuilder("m3u8ParsedNewMediaItemInfoFuncCallBackT mAudioType0:")).append(mAudioType.value()).toString());
            } else
            {
                onErrorInner(8, 1);
                Logger.log(Tag, "m3u8ParsedNewMediaItemInfoFuncCallBackT mAudioType0 error");
            }
            mHlsAudioFile.addPlayUrls(as);
            return 0;
        }
        return -1;
    }

    public abstract void mOnTimedChangeListenerInner();

    public abstract void onBufferingUpdateInner(int i);

    public void onCompletionInner()
    {
        resetAllData();
        MediaplayerComplete(mJniHandler);
        Logger.log(Tag, "dataStreamInputFuncCallBackT onCompletionInner");
        releaseAudioTrackPlayThread();
    }

    public void onErrorInner(int i, int j)
    {
        Logger.log(Tag, "onErrorInner");
    }

    public abstract void onInfoInner(int i);

    public void onPreparedInner()
    {
        if (isBuffing)
        {
            onInfoInner(702);
        }
        int i = MediaplayerGetCurPlayingFileType(mJniHandler);
        if (i >= 0)
        {
            mAudioType = AudioType.valueOf(i);
            Logger.log(Tag, (new StringBuilder("onPreparedInner mAudioType:")).append(mAudioType.value()).toString());
            return;
        } else
        {
            onErrorInner(8, 1);
            Logger.log(Tag, "onPreparedInner mAudioType error");
            return;
        }
    }

    public abstract void onSeekCompletedInner();

    public int outputDataAppointment()
    {
        return MediaplayerOutputDataAppointment(mJniHandler);
    }

    public void pause()
    {
        Logger.log(Tag, "dataStreamInputFuncCallBackT pause");
        getAudioTrackPlayThread().pausePlay();
        MediaplayerPause(mJniHandler);
    }

    public void prepareAsync()
    {
        Logger.log(Tag, "prepareAsync");
        if (!isBuffing)
        {
            onInfoInner(701);
        }
        MediaplayerPrepareAsync(mJniHandler);
    }

    public void release()
    {
        do
        {
            if (MediaplayerReset(mJniHandler) >= 0)
            {
                Logger.log(Tag, "dataStreamInputFuncCallBackT release");
                releaseAudioTrackPlayThread();
                if (mHlsAudioFile != null)
                {
                    mHlsAudioFile.release();
                }
                if (mAudioFileRequestHandler != null)
                {
                    mAudioFileRequestHandler.release();
                }
                MediaplayerDestroy(mJniHandler);
                tmepBuf = null;
                return;
            }
            try
            {
                Thread.sleep(10000L);
            }
            catch (InterruptedException interruptedexception) { }
        } while (true);
    }

    public void reset()
    {
        Logger.log(Tag, "reset");
        MediaplayerReset(mJniHandler);
        resetAllData();
    }

    public void resetAllData()
    {
        mHasSeek = true;
        tmepBuf = null;
        mCurrentSeekPosition = -1;
        mCurrentSeekPositionState = 0;
        mCurFileSize = 0L;
        if (mAudioFileRequestHandler != null)
        {
            mAudioFileRequestHandler.reset();
        }
        if (mHlsAudioFile != null)
        {
            mHlsAudioFile.release();
        }
        Logger.log(Tag, "dataStreamInputFuncCallBackT initAllData \u91CD\u7F6E0");
    }

    public void seekTo(int i)
    {
        Logger.log(Tag, (new StringBuilder("seekParaTimeStampMs msec:")).append(i).toString());
        isSeeking = true;
        MediaplayerSeek(i, mJniHandler);
        isSeeking = false;
        onSeekCompletedInner();
        mOnTimedChangeListenerInner();
        Logger.log(Tag, (new StringBuilder("ttseek2:")).append(System.currentTimeMillis()).toString());
    }

    public void setCurFileSize(long l)
    {
        mCurFileSize = l;
    }

    public void setDataSource(String s)
    {
        Logger.log(Tag, (new StringBuilder("setDataSource src:")).append(s).toString());
        mPlayUrl = s;
        if (mPlayUrl.contains("m3u8"))
        {
            mHlsAudioFile = new HlsAudioFile(mPlayUrl, this);
            mAudioType = AudioType.M3U8_FILE;
            MediaplayerSetDataSourceInfo(mPlayUrl, mAudioType.value(), mJniHandler);
        } else
        {
            mAudioFileRequestHandler = new AudioFileRequestHandler(this);
            mAudioType = AudioType.NORMAL_FILE;
            MediaplayerSetDataSourceInfo(mPlayUrl, mAudioType.value(), mJniHandler);
        }
        Logger.log(Tag, (new StringBuilder("setDataSource mAudioType:")).append(mAudioType.value()).toString());
    }

    public void setVolume(float f, float f1)
    {
        getAudioTrackPlayThread().setVolume(f);
    }

    public void start()
    {
        Logger.log(Tag, "dataStreamInputFuncCallBackT start");
        getAudioTrackPlayThread().startPlay();
        MediaplayerPlay(mJniHandler);
    }

    public void stop()
    {
        Logger.log(Tag, "dataStreamInputFuncCallBackT stop");
        getAudioTrackPlayThread().stopPlay();
        MediaplayerStop(mJniHandler);
        resetAllData();
    }

    public void xmediaplayerInit()
    {
        mJniHandler = MediaplayerInit(getAudioTrackPlayThread().getMinOutputBufferSize(), 0x10000, 0x10000, 0x10000);
        Logger.log(Tag, (new StringBuilder("MediaplayerInit mJniHandler:")).append(mJniHandler).toString());
        if (mJniHandler == 0L)
        {
            handlePlayerStatus(NativeErrorType.ERR_ARCH_NOT_SUPPORT.value());
        }
    }

    static 
    {
        Tag = "XMPLAY";
        if (PlayerUtil.isX86Arch())
        {
            Logger.log(Tag, (new StringBuilder("loadLibrary xmediaplayer_x isRrmV7Plus:")).append(PlayerUtil.isRrmV7Plus()).toString());
            System.loadLibrary("xmediaplayer_x");
        } else
        {
            Logger.log(Tag, (new StringBuilder("loadLibrary xmediaplayer isRrmV7Plus:")).append(PlayerUtil.isRrmV7Plus()).toString());
            System.loadLibrary("xmediaplayer");
        }
    }
}
