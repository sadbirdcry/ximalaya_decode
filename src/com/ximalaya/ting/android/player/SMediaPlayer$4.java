// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.media.MediaPlayer;

// Referenced classes of package com.ximalaya.ting.android.player:
//            SMediaPlayer

class ErrorListener
    implements android.media.rrorListener
{

    final SMediaPlayer this$0;
    private final ErrorListener val$listener;

    public boolean onError(MediaPlayer mediaplayer, int i, int j)
    {
        SMediaPlayer.access$7(SMediaPlayer.this);
        if (val$listener != null)
        {
            boolean flag = val$listener.onError(SMediaPlayer.this, i, j);
            if (!flag)
            {
                SMediaPlayer.access$6(SMediaPlayer.this, 8);
            }
            return flag;
        } else
        {
            return false;
        }
    }

    ErrorListener()
    {
        this$0 = final_smediaplayer;
        val$listener = ErrorListener.this;
        super();
    }
}
