// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.player;

import android.os.Environment;
import java.io.File;

public class XMediaPlayerConstants
{

    public static String APP_BASE_DIR;
    public static final String CACHE_BASE_DIR;
    public static final int CON_TIME_OUT = 10000;
    public static final int DEFAULT_CHUNK_SIZE = 0x10000;
    public static int DOWNLOAD_QUEUE_SIZE = 0;
    public static final String HLS_TS_DIR;
    public static final String INCOM_AUDIO_FILE_DIRECTORY;
    public static final int SEND_BUFFER_QUEUE_SIZE = 3;
    public static final int TIME_OUT = 30000;
    public static boolean isDebug = false;
    public static final boolean isDebugPlayer = false;

    public XMediaPlayerConstants()
    {
    }

    static 
    {
        DOWNLOAD_QUEUE_SIZE = 10;
        APP_BASE_DIR = (new StringBuilder(String.valueOf(Environment.getExternalStorageDirectory().getPath()))).append("/ting").toString();
        CACHE_BASE_DIR = (new StringBuilder(String.valueOf(APP_BASE_DIR))).append("/player_caching").toString();
        INCOM_AUDIO_FILE_DIRECTORY = (new StringBuilder(String.valueOf(CACHE_BASE_DIR))).append("/audio").toString();
        HLS_TS_DIR = (new StringBuilder(String.valueOf(CACHE_BASE_DIR))).append("/hls").toString();
    }
}
