// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.dl;

import java.util.Arrays;

// Referenced classes of package com.ximalaya.ting.android.dl:
//            PluginModel

public class PluginConstants
{

    public static final PluginModel PLUGIN_ALIPAY;
    public static final PluginModel PLUGIN_APPLINK;
    public static final PluginModel PLUGIN_APPTRACK;
    public static final PluginModel PLUGIN_CARLIFE;
    public static final PluginModel PLUGIN_COMMON;
    public static final PluginModel PLUGIN_EGUAN;
    public static final String PLUGIN_NAME_ALIPAY = "plugin_alipay";
    public static final String PLUGIN_NAME_APPLINK = "plugin_applink";
    public static final String PLUGIN_NAME_APPTRACK = "plugin_iresearch";
    public static final String PLUGIN_NAME_CARLIFE = "plugin_carlife";
    public static final String PLUGIN_NAME_COMMON = "plugin_common";
    public static final String PLUGIN_NAME_EGUAN = "plugin_eguan";
    public static final String PLUGIN_NAME_SDL = "plugin_sdl";
    public static final PluginModel PLUGIN_SDL;

    public PluginConstants()
    {
    }

    static 
    {
        PLUGIN_COMMON = new PluginModel();
        PLUGIN_COMMON.setPluginName("plugin_common");
        PLUGIN_COMMON.setOpen(true);
        PLUGIN_COMMON.setDexList(Arrays.asList(new String[] {
            "gson-2.2.4_dex.jar"
        }));
        PLUGIN_SDL = new PluginModel();
        PLUGIN_SDL.setPluginName("plugin_sdl");
        PLUGIN_SDL.setOpen(true);
        PLUGIN_SDL.setDexList(Arrays.asList(new String[] {
            "mxsdlutils_dex.jar", "sdl_android_lib_dex.jar"
        }));
        PLUGIN_APPLINK = new PluginModel();
        PLUGIN_APPLINK.setPluginName("plugin_applink");
        PLUGIN_APPLINK.setOpen(true);
        PLUGIN_APPLINK.setDexList(Arrays.asList(new String[] {
            "AppLinkSDKAndroid-2-3-2_dex.jar"
        }));
        PLUGIN_CARLIFE = new PluginModel();
        PLUGIN_CARLIFE.setPluginName("plugin_carlife");
        PLUGIN_CARLIFE.setOpen(true);
        PLUGIN_CARLIFE.setDexList(Arrays.asList(new String[] {
            "CarlifePlatformSDK_1_0_5_dex.jar"
        }));
        PLUGIN_ALIPAY = new PluginModel();
        PLUGIN_ALIPAY.setPluginName("plugin_alipay");
        PLUGIN_ALIPAY.setOpen(true);
        PLUGIN_ALIPAY.setDexList(Arrays.asList(new String[] {
            "alipaySDK-20150610_dex.jar"
        }));
        PLUGIN_APPTRACK = new PluginModel();
        PLUGIN_APPTRACK.setPluginName("plugin_iresearch");
        PLUGIN_APPTRACK.setOpen(true);
        PLUGIN_APPTRACK.setDexList(Arrays.asList(new String[] {
            "mAppTracker_dex.jar"
        }));
        PLUGIN_EGUAN = new PluginModel();
        PLUGIN_EGUAN.setPluginName("plugin_eguan");
        PLUGIN_EGUAN.setOpen(true);
        PLUGIN_EGUAN.setDexList(Arrays.asList(new String[] {
            "OneServiceSDK20151020_V2.0.2_dex.jar"
        }));
    }
}
