// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.dl;

import android.content.Context;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.dl:
//            DexJarLoader, PluginModel

public class PluginManager
{

    private static PluginManager sInstance;
    private static byte sLock[] = new byte[0];
    private Context mContext;
    private DexJarLoader mDexLoader;
    private List mLoadedPlugin;
    private SharedPreferencesUtil mPreference;

    private PluginManager(Context context)
    {
        mLoadedPlugin = new ArrayList();
        mContext = context.getApplicationContext();
        mDexLoader = DexJarLoader.getInstance(mContext);
        mPreference = SharedPreferencesUtil.getInstance(mContext);
    }

    public static PluginManager getInstance(Context context)
    {
        if (sInstance == null)
        {
            synchronized (sLock)
            {
                if (sInstance == null)
                {
                    sInstance = new PluginManager(context);
                }
            }
        }
        return sInstance;
        context;
        abyte0;
        JVM INSTR monitorexit ;
        throw context;
    }

    public boolean isPluginLoaded(PluginModel pluginmodel)
    {
        boolean flag;
        synchronized (mLoadedPlugin)
        {
            flag = mLoadedPlugin.contains(pluginmodel);
        }
        return flag;
        pluginmodel;
        list;
        JVM INSTR monitorexit ;
        throw pluginmodel;
    }

    public boolean isPluginLoaded(String s)
    {
        return isPluginLoaded(new PluginModel(s));
    }

    public boolean isPluginOpen(String s)
    {
        return mPreference.getBoolean(s);
    }

    public void loadPluginAsync(final PluginModel plugin, final Runnable r)
    {
        if (isPluginLoaded(plugin.getPluginName()))
        {
            r.run();
            return;
        } else
        {
            r = new _cls1();
            mDexLoader.loadDexAsync(plugin.getDexList(), r);
            return;
        }
    }

    public void loadPluginSync(PluginModel pluginmodel)
    {
        if (isPluginLoaded(pluginmodel))
        {
            return;
        } else
        {
            mDexLoader.loadDexSync(pluginmodel.getDexList());
            setPluginLoaded(pluginmodel);
            return;
        }
    }

    public void setPluginLoaded(PluginModel pluginmodel)
    {
        synchronized (mLoadedPlugin)
        {
            if (!mLoadedPlugin.contains(pluginmodel))
            {
                mLoadedPlugin.add(pluginmodel);
            }
        }
        return;
        pluginmodel;
        list;
        JVM INSTR monitorexit ;
        throw pluginmodel;
    }

    public void setPluginOpen(String s, boolean flag)
    {
        mPreference.saveBoolean(s, flag);
    }


    private class _cls1
        implements Runnable
    {

        final PluginManager this$0;
        final PluginModel val$plugin;
        final Runnable val$r;

        public void run()
        {
            setPluginLoaded(plugin);
            if (r != null)
            {
                r.run();
            }
        }

        _cls1()
        {
            this$0 = PluginManager.this;
            plugin = pluginmodel;
            r = runnable;
            super();
        }
    }

}
