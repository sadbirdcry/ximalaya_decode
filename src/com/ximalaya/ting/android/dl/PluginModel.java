// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.dl;

import java.util.List;

public class PluginModel
{

    private List dexList;
    private boolean isOpen;
    private String pluginName;

    public PluginModel()
    {
    }

    public PluginModel(String s)
    {
        pluginName = s;
    }

    public boolean equals(Object obj)
    {
        if (this != obj) goto _L2; else goto _L1
_L1:
        return true;
_L2:
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        obj = (PluginModel)obj;
        if (pluginName != null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (((PluginModel) (obj)).pluginName == null) goto _L1; else goto _L3
_L3:
        return false;
        if (pluginName.equals(((PluginModel) (obj)).pluginName)) goto _L1; else goto _L4
_L4:
        return false;
    }

    public List getDexList()
    {
        return dexList;
    }

    public String getPluginName()
    {
        return pluginName;
    }

    public int hashCode()
    {
        int i;
        if (pluginName == null)
        {
            i = 0;
        } else
        {
            i = pluginName.hashCode();
        }
        return i + 31;
    }

    public boolean isOpen()
    {
        return isOpen;
    }

    public void setDexList(List list)
    {
        dexList = list;
    }

    public void setOpen(boolean flag)
    {
        isOpen = flag;
    }

    public void setPluginName(String s)
    {
        pluginName = s;
    }
}
