// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.dl;

import android.content.Context;
import android.content.res.AssetManager;
import android.text.TextUtils;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import dalvik.system.DexClassLoader;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.dl:
//            PluginModel

public class DexJarLoader
{
    private class LoadTask extends MyAsyncTask
    {

        private TaskWrapper mTaskWrapper;
        final DexJarLoader this$0;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            mTaskWrapper.run();
            return mTaskWrapper.getResult();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            super.onPostExecute(s);
            if (!TextUtils.isEmpty(s) && mTaskWrapper.mCallback != null)
            {
                mTaskWrapper.mCallback.run();
            }
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        public LoadTask(TaskWrapper taskwrapper)
        {
            this$0 = DexJarLoader.this;
            super();
            mTaskWrapper = taskwrapper;
        }
    }

    private class TaskWrapper
        implements Runnable
    {

        private Runnable mCallback;
        private List mDexs;
        private String mResult;
        private int mTaskType;
        final DexJarLoader this$0;

        public String getResult()
        {
            return mResult;
        }

        public void run()
        {
            int j = mDexs.size();
            Iterator iterator = mDexs.iterator();
            String s;
            int i;
            for (i = 0; iterator.hasNext(); i = copyToInternalPath(s) + i)
            {
                s = (String)iterator.next();
            }

            if (i != j)
            {
                Logger.e("DexJarLoader", (new StringBuilder()).append("copyToInternalPath fail ").append(getMultiDexName(mDexs)).toString());
                return;
            } else
            {
                loadLibraryToClassPath(mDexs);
                mResult = getMultiDexName(mDexs);
                return;
            }
        }



        public TaskWrapper(List list, int i, Runnable runnable)
        {
            this$0 = DexJarLoader.this;
            super();
            mTaskType = 0;
            mDexs = list;
            mTaskType = i;
            mCallback = runnable;
        }
    }


    public static final int BUF_SIZE = 8192;
    private static final String TAG = "DexJarLoader";
    public static final int Type_Async = 2;
    public static final int Type_None = 0;
    public static final int Type_Sync = 1;
    private static DexJarLoader sInstance;
    private static byte sLock[] = new byte[0];
    private final String dexInAssert = "dex/";
    private Context mContext;
    private File mDexCachePath;
    private File mInternalDexPath;
    private List mLoadedDexs;

    private DexJarLoader(Context context)
    {
        mContext = context;
        mLoadedDexs = new ArrayList();
        mDexCachePath = context.getDir("outdex", 0);
        mInternalDexPath = context.getDir("dex", 0);
    }

    private int copyToInternalPath(String s)
    {
        s = new File(mInternalDexPath, s);
        if (s.exists())
        {
            return 1;
        } else
        {
            return prepareDex(s);
        }
    }

    private void execute(TaskWrapper taskwrapper)
    {
        if (taskwrapper.mTaskType == 2)
        {
            (new LoadTask(taskwrapper)).myexec(new Void[0]);
            return;
        } else
        {
            taskwrapper.run();
            return;
        }
    }

    public static DexJarLoader getInstance(Context context)
    {
        if (sInstance == null)
        {
            synchronized (sLock)
            {
                if (sInstance == null)
                {
                    sInstance = new DexJarLoader(context.getApplicationContext());
                }
            }
        }
        return sInstance;
        context;
        abyte0;
        JVM INSTR monitorexit ;
        throw context;
    }

    private void loadDexInternal(List list)
    {
        ClassLoader classloader = getClass().getClassLoader();
        classloader;
        JVM INSTR monitorenter ;
        DexClassLoader dexclassloader = new DexClassLoader(getMultiDexName(list), mDexCachePath.getAbsolutePath(), null, classloader.getParent());
        Field field = java/lang/ClassLoader.getDeclaredField("parent");
        field.setAccessible(true);
        field.set(classloader, dexclassloader);
        synchronized (mLoadedDexs)
        {
            mLoadedDexs.addAll(list);
        }
_L1:
        classloader;
        JVM INSTR monitorexit ;
        return;
        list;
        list1;
        JVM INSTR monitorexit ;
        try
        {
            throw list;
        }
        // Misplaced declaration of an exception variable
        catch (List list) { }
        finally { }
        list.printStackTrace();
          goto _L1
        classloader;
        JVM INSTR monitorexit ;
        throw list;
    }

    private void loadLibraryToClassPath(List list)
    {
        ArrayList arraylist = new ArrayList();
        Iterator iterator = list.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            String s = (String)iterator.next();
            if (!isDexLoaded(s))
            {
                arraylist.add(s);
            }
        } while (true);
        loadDexInternal(list);
    }

    private int prepareDex(File file)
    {
        Object obj;
        Object obj2;
        obj2 = null;
        Exception exception = null;
        int i = 0;
        Logger.e("DexJarLoader", (new StringBuilder()).append("prepareDex start:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString());
        Object obj1;
        int j;
        try
        {
            obj1 = new BufferedInputStream(mContext.getAssets().open((new StringBuilder()).append("dex/").append(file.getName()).toString()));
        }
        // Misplaced declaration of an exception variable
        catch (Object obj2)
        {
            obj = null;
            obj1 = exception;
            continue; /* Loop/switch isn't completed */
        }
        finally
        {
            obj1 = null;
        }
        obj = new BufferedOutputStream(new FileOutputStream(file));
        obj2 = new byte[8192];
_L3:
        j = ((BufferedInputStream) (obj1)).read(((byte []) (obj2)), 0, 8192);
        if (j <= 0) goto _L2; else goto _L1
_L1:
        ((OutputStream) (obj)).write(((byte []) (obj2)), 0, j);
          goto _L3
        obj2;
_L9:
        ((Exception) (obj2)).printStackTrace();
        if (obj != null)
        {
            try
            {
                ((OutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        if (obj1 != null)
        {
            try
            {
                ((BufferedInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        file = (new StringBuilder()).append("prepareDex end:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString();
_L5:
        Logger.e("DexJarLoader", file);
        return i;
_L2:
        ((OutputStream) (obj)).close();
        ((BufferedInputStream) (obj1)).close();
        i = 1;
        if (obj != null)
        {
            try
            {
                ((OutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        if (obj1 != null)
        {
            try
            {
                ((BufferedInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        file = (new StringBuilder()).append("prepareDex end:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString();
        if (true) goto _L5; else goto _L4
_L4:
        break MISSING_BLOCK_LABEL_87;
_L7:
        if (obj2 != null)
        {
            try
            {
                ((OutputStream) (obj2)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj2)
            {
                ((IOException) (obj2)).printStackTrace();
            }
        }
        if (obj1 != null)
        {
            try
            {
                ((BufferedInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((IOException) (obj1)).printStackTrace();
            }
        }
        Logger.e("DexJarLoader", (new StringBuilder()).append("prepareDex end:").append(file.getName()).append(",").append(System.currentTimeMillis()).toString());
        throw obj;
        obj;
        continue; /* Loop/switch isn't completed */
        exception;
        obj2 = obj;
        obj = exception;
        continue; /* Loop/switch isn't completed */
        exception;
        obj2 = obj;
        obj = exception;
        if (true) goto _L7; else goto _L6
_L6:
        break MISSING_BLOCK_LABEL_87;
        obj2;
        obj = null;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public void clear()
    {
        mLoadedDexs = null;
        sInstance = null;
    }

    public String getMultiDexName(List list)
    {
        StringBuilder stringbuilder = new StringBuilder();
        int j = list.size();
        for (int i = 0; i < j; i++)
        {
            String s = (String)list.get(i);
            stringbuilder.append(mInternalDexPath).append(File.separator).append(s);
            if (i < j - 1)
            {
                stringbuilder.append(File.pathSeparator);
            }
        }

        return stringbuilder.toString();
    }

    public boolean isDexLoaded(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return false;
        }
        boolean flag;
        synchronized (mLoadedDexs)
        {
            flag = mLoadedDexs.contains(s);
        }
        return flag;
        s;
        list;
        JVM INSTR monitorexit ;
        throw s;
    }

    public boolean isModuleLoaded(PluginModel pluginmodel)
    {
        boolean flag;
        synchronized (mLoadedDexs)
        {
            flag = mLoadedDexs.contains(pluginmodel);
        }
        return flag;
        pluginmodel;
        list;
        JVM INSTR monitorexit ;
        throw pluginmodel;
    }

    public boolean isModuleLoaded(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return false;
        } else
        {
            return isModuleLoaded(new PluginModel(s));
        }
    }

    public void loadDexAsync(List list, Runnable runnable)
    {
        execute(new TaskWrapper(list, 2, runnable));
    }

    public void loadDexSync(List list)
    {
        execute(new TaskWrapper(list, 1, null));
    }



}
