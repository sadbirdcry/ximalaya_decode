// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.nineoldandroids.animation.ObjectAnimator;
import com.umeng.analytics.MobclickAgent;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.homepage.BindIPhoneActivity;
import com.ximalaya.ting.android.activity.homepage.TalkViewAct;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.activity.recording.RecordingActivity;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.applink.LockScreenFragment;
import com.ximalaya.ting.android.applink.SdlConnectManager;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.carlink.carlife.CarlifeManager;
import com.ximalaya.ting.android.dialog.PreinstallDialog;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.ManageFragment;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.che.MyBluetoothManager2;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.dlna.IDlnaController;
import com.ximalaya.ting.android.fragment.device.dlna.module.BaseVolumeControlModule;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.finding2.category.CategoryContentFragment;
import com.ximalaya.ting.android.fragment.findings.WeekHotSoundListFragment;
import com.ximalaya.ting.android.fragment.livefm.LivePlayerFragment;
import com.ximalaya.ting.android.fragment.login.AppIntroductionFragment;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import com.ximalaya.ting.android.fragment.search.WordAssociatedFragment;
import com.ximalaya.ting.android.fragment.setting.FindFriendSettingFragment;
import com.ximalaya.ting.android.fragment.tab.AppTabBFragment;
import com.ximalaya.ting.android.fragment.tab.FindingFragmentV3;
import com.ximalaya.ting.android.fragment.tab.MySpaceFragmentNew;
import com.ximalaya.ting.android.fragment.ting.feed.FeedMainFragment;
import com.ximalaya.ting.android.fragment.userspace.CommentNoticeFragment;
import com.ximalaya.ting.android.fragment.userspace.ManageCenterFragment;
import com.ximalaya.ting.android.fragment.userspace.MyAttentionFragment;
import com.ximalaya.ting.android.fragment.userspace.NewThingFragment;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.fragment.userspace.PrivateMsgFragment;
import com.ximalaya.ting.android.fragment.web.WebFragment;
import com.ximalaya.ting.android.fragment.zone.ZoneFragment;
import com.ximalaya.ting.android.fragment.zone.ZoneMessageFragment;
import com.ximalaya.ting.android.library.model.AppAd;
import com.ximalaya.ting.android.library.service.DownloadService;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.NoReadModel;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.check_version.MsgCarry;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.BaiduLocationManager;
import com.ximalaya.ting.android.modelmanage.DexManager;
import com.ximalaya.ting.android.modelmanage.MsgManager;
import com.ximalaya.ting.android.modelmanage.NoReadManage;
import com.ximalaya.ting.android.modelmanage.PushStat;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.service.play.Playlist;
import com.ximalaya.ting.android.service.push.MsgCount;
import com.ximalaya.ting.android.service.push.PushModel;
import com.ximalaya.ting.android.transaction.d.z;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.UpgradeFileUtil;
import com.ximalaya.ting.android.view.RoundedImageView;
import com.ximalaya.ting.android.view.menu.TabMenu;
import java.io.File;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.activity:
//            BaseFragmentActivity, e, p, s, 
//            r, w, x, y, 
//            u, t, ab, ac, 
//            k, z, f, aa, 
//            m, n, q, h, 
//            g, i, j, l, 
//            o

public class MainTabActivity2 extends BaseFragmentActivity
    implements com.ximalaya.ting.android.fragment.login.AppIntroductionFragment.OnAppIntroDismissCallback, com.ximalaya.ting.android.modelmanage.NoReadManage.NoReadUpdateListener, com.ximalaya.ting.android.transaction.download.DownloadHandler.DownloadSoundsListener
{
    private static class a extends Handler
    {

        private final WeakReference a;

        public void handleMessage(Message message)
        {
            MainTabActivity2 maintabactivity2 = (MainTabActivity2)a.get();
            if (maintabactivity2 != null && !maintabactivity2.isFinishing()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            MsgCarry msgcarry = (MsgCarry)message.obj;
            message.what;
            JVM INSTR tableswitch 2 4: default 60
        //                       2 61
        //                       3 70
        //                       4 115;
               goto _L3 _L4 _L5 _L6
_L6:
            continue; /* Loop/switch isn't completed */
_L3:
            return;
_L4:
            maintabactivity2.updateProgressDialog(msgcarry.getPercent());
            return;
_L5:
            if (maintabactivity2.m_pDialog == null) goto _L1; else goto _L7
_L7:
            maintabactivity2.m_pDialog.dismiss();
            message = Uri.fromFile(UpgradeFileUtil.updateFile);
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(message, "application/vnd.android.package-archive");
            maintabactivity2.startActivity(intent);
            return;
            if (maintabactivity2.m_pDialog == null) goto _L1; else goto _L8
_L8:
            maintabactivity2.m_pDialog.dismiss();
            return;
        }

        public a(MainTabActivity2 maintabactivity2)
        {
            a = new WeakReference(maintabactivity2);
        }
    }


    private static final String TAG = com/ximalaya/ting/android/activity/MainTabActivity2.getSimpleName();
    public static MainTabActivity2 mainTabActivity;
    private boolean isAddLivePlayFragment;
    private boolean isAddLockScreen;
    private boolean isAddPlayFragment;
    android.widget.TabHost.OnTabChangeListener listener;
    private RoundedImageView mAnimView;
    private AppAd mAppAd;
    private String mCurrentTab;
    private DexManager mDexManager;
    private boolean mIsEverPlayed;
    private LivePlayerFragment mLivePlayFragment;
    private BaseFragment mLockScreenFragment;
    private ManageFragment mManageFragment;
    private List mMenuIcon;
    private List mMenuTitle;
    private MsgManager mMsgManager;
    private com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener mOnPlayServiceUpdateListener;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private PlayerFragment mPlayerFragment;
    private long mPlayingRadioDuration;
    private long mPlayingRadioStartTime;
    private View mSmartBarTabs[];
    public HashMap mStacks;
    public TabHost mTabHost;
    private TabMenu mTabMenu;
    WifiManager mWiFiManager;
    private ProgressDialog m_pDialog;
    private FrameLayout playButtonLayout;
    private ImageView playIconImg;
    private Animation rotateAm;
    private a uphandler;
    private LoginInfoModel userInfoModel;

    public MainTabActivity2()
    {
        mMenuTitle = Arrays.asList(new String[] {
            "\u64AD\u653E\u5386\u53F2", "\u5B9A\u65F6\u5173\u95ED", "\u6E05\u7406\u7A7A\u95F4", "\u7279\u8272\u95F9\u94C3", "\u68C0\u67E5\u66F4\u65B0", "\u9000\u51FA"
        });
        mMenuIcon = Arrays.asList(new Integer[] {
            Integer.valueOf(0x7f020343), Integer.valueOf(0x7f02034a), Integer.valueOf(0x7f02033a), Integer.valueOf(0x7f020350), Integer.valueOf(0x7f02034d), Integer.valueOf(0x7f020340)
        });
        isAddPlayFragment = false;
        isAddLivePlayFragment = false;
        listener = new e(this);
        mPlayingRadioDuration = 0L;
        mPlayingRadioStartTime = 0L;
        isAddLockScreen = false;
        uphandler = new a(this);
    }

    private void addTab(String s1, int i1, String s2)
    {
        android.widget.TabHost.TabSpec tabspec = mTabHost.newTabSpec(s1);
        tabspec.setContent(new p(this));
        tabspec.setIndicator(createTabView(i1, s2, s1));
        mTabHost.addTab(tabspec);
    }

    private void askVersionUpdate(String s1, String s2, String s3)
    {
        String s5 = getApkName(s1);
        DialogBuilder dialogbuilder = new DialogBuilder(this);
        String s4 = s3;
        if (TextUtils.isEmpty(s3))
        {
            s4 = (new StringBuilder()).append("\u559C\u9A6C\u62C9\u96C5").append(s1).append("\u7248\u5DF2\u7ECF\u53D1\u5E03,\u5FEB\u53BB\u4F53\u9A8C\u65B0\u529F\u80FD\u5427").toString();
        }
        dialogbuilder.setMessage(s4).setOkBtn("\u5347\u7EA7\u65B0\u7248", new s(this, s5, s2)).setCancelBtn("\u4E0B\u6B21\u518D\u8BF4").setNeutralBtn("\u4E0D\u518D\u63D0\u9192", new r(this, s1)).showMultiButton();
    }

    private void clearNotice()
    {
        NoReadModel noreadmodel = NoReadManage.getInstance().getNoReadModel();
        if (noreadmodel == null)
        {
            return;
        } else
        {
            noreadmodel.newFeedCount = 0;
            noreadmodel.newEventCount = 0;
            noreadmodel.newEventCountChanged = false;
            noreadmodel.followers = 0;
            noreadmodel.messages = 0;
            noreadmodel.leters = 0;
            noreadmodel.noReadFollowers = 0;
            noreadmodel.newThirdRegisters = 0;
            noreadmodel.favoriteAlbumIsUpdate = false;
            NoReadManage.getInstance().updateNoReadManageManual();
            return;
        }
    }

    private void createProgressDialog()
    {
        if (this == null)
        {
            return;
        } else
        {
            m_pDialog = new ProgressDialog(this);
            m_pDialog.setProgressStyle(1);
            m_pDialog.setTitle("\u8F6F\u4EF6\u5347\u7EA7");
            m_pDialog.setMessage("\u6B63\u5728\u4E3A\u60A8\u4E0B\u8F7D\u6700\u65B0\u7248\u672C\u7684\u559C\u9A6C\u62C9\u96C5...");
            m_pDialog.setIndeterminate(false);
            m_pDialog.setCancelable(false);
            m_pDialog.show();
            return;
        }
    }

    private View createSmartBarItem(int i1, String s1)
    {
        View view = getLayoutInflater().inflate(0x7f030139, null);
        TextView textview = (TextView)view.findViewById(0x7f0a04e5);
        textview.setText(s1);
        textview.setCompoundDrawablesWithIntrinsicBounds(0, i1, 0, 0);
        return view;
    }

    private View createTabView(int i1, String s1, String s2)
    {
        if ("tab_c".equals(s2))
        {
            return new View(this);
        } else
        {
            s2 = LayoutInflater.from(this).inflate(0x7f0301dc, null);
            ((TextView)s2.findViewById(0x7f0a04e5)).setText(s1);
            ((ImageView)s2.findViewById(0x7f0a06f4)).setImageResource(i1);
            return s2;
        }
    }

    private void doFollow()
    {
        long l1;
        for (l1 = ((MyApplication)getApplication()).k(); l1 == -1L || !UserInfoMannage.hasLogined();)
        {
            return;
        }

        Logger.v("MainTabActivity2", "\u5173\u6CE8Feed\u9875\u5DF2\u9009\u63A8\u8350\u4E3B\u64AD");
        RequestParams requestparams = new RequestParams();
        requestparams.put("toUid", (new StringBuilder()).append("").append(l1).toString());
        requestparams.put("isFollow", "true");
        f.a().b("mobile/follow", requestparams, DataCollectUtil.getDataFromView(rootView), new w(this));
    }

    private void doSomethingByIntent(Intent intent)
    {
        Logger.d(TAG, "doSomethingByIntent IN");
        if (intent != null)
        {
            Logger.d("WIFI", "doSomethingByIntent getIntent() != null");
            if (!TextUtils.isEmpty(intent.getStringExtra("PLAYINGNOW")))
            {
                Logger.d("WIFI", "doSomethingByIntent PLAYINGNOW");
                if (mTabHost != null)
                {
                    mTabHost.postDelayed(new x(this), 3000L);
                }
            } else
            {
                Logger.d("WIFI", "doSomethingByIntent PLAYINGNOW is null");
            }
        }
        if (getIntent() == null || getIntent().getAction() == null || !getIntent().getAction().contains("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_START_PLAY")) goto _L2; else goto _L1
_L1:
        Logger.d(TAG, "hasExtra(AppConstants.PLAYINGNOW)");
        if (mTabHost != null)
        {
            mTabHost.postDelayed(new y(this), 2000L);
        }
_L4:
        return;
_L2:
        if (!intent.hasExtra(com.ximalaya.ting.android.a.t))
        {
            break; /* Loop/switch isn't completed */
        }
        Bundle bundle = intent.getBundleExtra(com.ximalaya.ting.android.a.s);
        intent = (Class)intent.getSerializableExtra(com.ximalaya.ting.android.a.t);
        if (intent != null)
        {
            if (intent.equals(com/ximalaya/ting/android/fragment/play/PlayerFragment))
            {
                showPlayFragment(null);
                return;
            }
            if (intent.equals(com/ximalaya/ting/android/fragment/livefm/LivePlayerFragment))
            {
                showLivePlayFragment(null);
                return;
            } else
            {
                startFragment(intent, bundle);
                return;
            }
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (getIntent() != null && getIntent().getBooleanExtra("show_recommend_sound", false))
        {
            showRecommendSoundListFragment();
        }
        Object obj1 = intent.getData();
        Object obj;
        Bundle bundle1;
        int i1;
        byte byte0;
        long l2;
        if (obj1 != null)
        {
            String s1 = ((Uri) (obj1)).getQueryParameter("msg_type");
            String s2 = ((Uri) (obj1)).getQueryParameter("uid");
            String s3 = ((Uri) (obj1)).getQueryParameter("album_id");
            String s5 = ((Uri) (obj1)).getQueryParameter("track_id");
            String s6 = ((Uri) (obj1)).getQueryParameter("act_id");
            String s7 = ((Uri) (obj1)).getQueryParameter("zone_id");
            String s8 = ((Uri) (obj1)).getQueryParameter("recSrc");
            String s9 = ((Uri) (obj1)).getQueryParameter("recTrack");
            String s10 = ((Uri) (obj1)).getQueryParameter("term");
            String s11 = ((Uri) (obj1)).getQueryParameter("source");
            String s12 = ((Uri) (obj1)).getQueryParameter("type");
            intent = new PushModel();
            intent.url = ((Uri) (obj1)).getQueryParameter("url");
            intent.title = ((Uri) (obj1)).getQueryParameter("title");
            intent.activityName = ((Uri) (obj1)).getQueryParameter("act_name");
            intent.mRecSrc = s8;
            intent.mRecTrack = s9;
            intent.term = s10;
            intent.source = s11;
            intent.type = s12;
            if (!TextUtils.isEmpty(((PushModel) (intent)).source))
            {
                i1 = 1;
            } else
            {
                i1 = 0;
            }
            if (!TextUtils.isEmpty(s1))
            {
                intent.messageType = Integer.parseInt(s1);
            }
            if (!TextUtils.isEmpty(s2))
            {
                intent.uid = Long.parseLong(s2);
            }
            if (!TextUtils.isEmpty(s3))
            {
                intent.albumId = Long.parseLong(s3);
            }
            if (!TextUtils.isEmpty(s5))
            {
                intent.trackId = Long.parseLong(s5);
            }
            if (!TextUtils.isEmpty(s6))
            {
                intent.activityId = Long.parseLong(s6);
            }
            if (!TextUtils.isEmpty(s7))
            {
                intent.zoneId = Long.parseLong(s7);
            }
            byte0 = 0;
        } else
        if (intent.hasExtra("NOTIFICATION") && intent.getBooleanExtra("NOTIFICATION", false) && intent.hasExtra("push_message"))
        {
            PushModel pushmodel = (PushModel)JSON.parseObject(intent.getStringExtra("push_message"), com/ximalaya/ting/android/service/push/PushModel);
            i1 = intent.getIntExtra("push_provider", 1);
            android.content.Context context = getApplicationContext();
            long l1 = pushmodel.bid;
            String s4 = pushmodel.msgId;
            if (i1 == 1)
            {
                intent = "baidu";
            } else
            {
                intent = "getui";
            }
            (new PushStat(context, l1, s4, intent)).statClick();
            i1 = 0;
            byte0 = 1;
            intent = pushmodel;
        } else
        {
            byte0 = 0;
            intent = null;
            i1 = 0;
        }
        if (intent == null) goto _L4; else goto _L5
_L5:
        if (((PushModel) (intent)).nType == 1)
        {
            MobclickAgent.onEvent(getApplicationContext(), "push_open", (new StringBuilder()).append("message type:").append(((PushModel) (intent)).messageType).toString());
        } else
        {
            MobclickAgent.onEvent(getApplicationContext(), "push_open_other", (new StringBuilder()).append("message type:").append(((PushModel) (intent)).messageType).toString());
        }
        if (mMsgManager == null)
        {
            mMsgManager = MsgManager.getInstance(this);
        }
        mMsgManager.sendCallBackMsg(this, intent);
        ((PushModel) (intent)).messageType;
        JVM INSTR tableswitch 0 32: default 760
    //                   0 761
    //                   1 760
    //                   2 760
    //                   3 760
    //                   4 760
    //                   5 760
    //                   6 760
    //                   7 760
    //                   8 760
    //                   9 761
    //                   10 1059
    //                   11 1184
    //                   12 1113
    //                   13 1325
    //                   14 1420
    //                   15 1011
    //                   16 968
    //                   17 1038
    //                   18 1086
    //                   19 1493
    //                   20 1643
    //                   21 1728
    //                   22 1744
    //                   23 1760
    //                   24 1949
    //                   25 1922
    //                   26 760
    //                   27 1990
    //                   28 1983
    //                   29 1995
    //                   30 2047
    //                   31 2060
    //                   32 2077;
           goto _L6 _L7 _L6 _L6 _L6 _L6 _L6 _L6 _L6 _L6 _L7 _L8 _L9 _L10 _L11 _L12 _L13 _L14 _L15 _L16 _L17 _L18 _L19 _L20 _L21 _L22 _L23 _L6 _L24 _L25 _L26 _L27 _L28 _L29
_L6:
        return;
_L7:
        if (i1 != 0)
        {
            DataCollectUtil.getInstance(mActivity).statIting(((PushModel) (intent)).messageType, ((PushModel) (intent)).activityId, ((PushModel) (intent)).trackId, ((PushModel) (intent)).source, ((PushModel) (intent)).type, ((PushModel) (intent)).albumId, ((PushModel) (intent)).uid);
            return;
        }
          goto _L4
_L14:
        if (UserInfoMannage.hasLogined())
        {
            MsgCount.sCount_Fans = 0;
            mTabHost.setCurrentTab(4);
            intent = new Bundle();
            intent.putInt("flag", 1);
            startFragment(com/ximalaya/ting/android/fragment/userspace/MyAttentionFragment, intent);
            return;
        }
          goto _L4
_L13:
        if (UserInfoMannage.hasLogined())
        {
            MsgCount.sCount_Comments = 0;
            mTabHost.setCurrentTab(4);
            startFragment(com/ximalaya/ting/android/fragment/userspace/CommentNoticeFragment, null);
            return;
        }
          goto _L4
_L15:
        if (UserInfoMannage.hasLogined())
        {
            startFragment(com/ximalaya/ting/android/fragment/userspace/NewThingFragment, new Bundle());
            return;
        }
_L8:
        if (!UserInfoMannage.hasLogined()) goto _L4; else goto _L30
_L30:
        MsgCount.sCount_Letter = 0;
        mTabHost.setCurrentTab(4);
        startFragment(com/ximalaya/ting/android/fragment/userspace/PrivateMsgFragment, null);
        return;
_L16:
        if (UserInfoMannage.hasLogined())
        {
            MsgCount.sCount_Friends = 0;
            mTabHost.setCurrentTab(0);
            startFragment(com/ximalaya/ting/android/fragment/setting/FindFriendSettingFragment, null);
            return;
        }
          goto _L4
_L10:
        if (i1 != 0)
        {
            DataCollectUtil.getInstance(mActivity).statIting(((PushModel) (intent)).messageType, ((PushModel) (intent)).activityId, ((PushModel) (intent)).trackId, ((PushModel) (intent)).source, ((PushModel) (intent)).type, ((PushModel) (intent)).albumId, ((PushModel) (intent)).uid);
        }
        obj = new Bundle();
        ((Bundle) (obj)).putLong("toUid", ((PushModel) (intent)).uid);
        startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, ((Bundle) (obj)));
        return;
_L9:
        obj = new SoundInfo();
        obj.trackId = ((PushModel) (intent)).trackId;
        obj.recSrc = ((PushModel) (intent)).mRecSrc;
        obj.recTrack = ((PushModel) (intent)).mRecTrack;
        if (byte0 != 0)
        {
            byte0 = 25;
        } else
        if (!TextUtils.isEmpty(((Uri) (obj1)).getQueryParameter("act_id")))
        {
            byte0 = 24;
        } else
        {
            byte0 = 13;
        }
        if (i1 != 0)
        {
            DataCollectUtil.getInstance(mActivity).statIting(((PushModel) (intent)).messageType, ((PushModel) (intent)).activityId, ((PushModel) (intent)).trackId, ((PushModel) (intent)).source, ((PushModel) (intent)).type, ((PushModel) (intent)).albumId, ((PushModel) (intent)).uid);
        }
        PlayTools.gotoPlayWithoutUrl(byte0, ((SoundInfo) (obj)), this, true, null);
        intent = PlayListControl.getPlayListManager().getCurSound();
        if (intent != null && ((SoundInfo) (intent)).trackId != ((SoundInfo) (obj)).trackId)
        {
            startPlayAnimation();
            return;
        }
          goto _L4
_L11:
        if (i1 != 0)
        {
            DataCollectUtil.getInstance(mActivity).statIting(((PushModel) (intent)).messageType, ((PushModel) (intent)).activityId, ((PushModel) (intent)).trackId, ((PushModel) (intent)).source, ((PushModel) (intent)).type, ((PushModel) (intent)).albumId, ((PushModel) (intent)).uid);
        }
        obj = new Bundle();
        obj1 = new AlbumModel();
        obj1.albumId = ((PushModel) (intent)).albumId;
        obj1.uid = ((PushModel) (intent)).uid;
        ((Bundle) (obj)).putString("album", JSON.toJSONString(obj1));
        startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj)));
        return;
_L12:
        obj = new Intent(this, com/ximalaya/ting/android/activity/web/WebActivityNew);
        if (i1 != 0)
        {
            DataCollectUtil.getInstance(mActivity).statIting(((PushModel) (intent)).messageType, ((PushModel) (intent)).activityId, ((PushModel) (intent)).trackId, ((PushModel) (intent)).source, ((PushModel) (intent)).type, ((PushModel) (intent)).albumId, ((PushModel) (intent)).uid);
        }
        ((Intent) (obj)).putExtra("ExtraUrl", ((PushModel) (intent)).url);
        startActivity(((Intent) (obj)));
        return;
_L17:
        if (i1 != 0)
        {
            DataCollectUtil.getInstance(mActivity).statIting(((PushModel) (intent)).messageType, ((PushModel) (intent)).activityId, ((PushModel) (intent)).trackId, ((PushModel) (intent)).source, ((PushModel) (intent)).type, ((PushModel) (intent)).albumId, ((PushModel) (intent)).uid);
        }
        bundle1 = new Bundle();
        obj1 = (new StringBuilder()).append(com.ximalaya.ting.android.a.S).append("activity-web/activity/").append(((PushModel) (intent)).activityId).toString();
        obj = obj1;
        if (((PushModel) (intent)).trackId > 0L)
        {
            obj = (new StringBuilder()).append(((String) (obj1))).append("/detail/").append(((PushModel) (intent)).trackId).toString();
        }
        bundle1.putString("ExtraUrl", ((String) (obj)));
        bundle1.putInt("web_view_type", 8);
        startFragment(com/ximalaya/ting/android/fragment/web/WebFragment, bundle1);
        return;
_L18:
        if (i1 != 0)
        {
            DataCollectUtil.getInstance(mActivity).statIting(((PushModel) (intent)).messageType, ((PushModel) (intent)).activityId, ((PushModel) (intent)).trackId, ((PushModel) (intent)).source, ((PushModel) (intent)).type, ((PushModel) (intent)).albumId, ((PushModel) (intent)).uid);
        }
        obj = new Intent(this, com/ximalaya/ting/android/activity/recording/RecordingActivity);
        ((Intent) (obj)).putExtra("activity_id", ((PushModel) (intent)).activityId);
        ((Intent) (obj)).putExtra("activity_name", ((PushModel) (intent)).activityName);
        startActivity(((Intent) (obj)));
        return;
_L19:
        startActivity(new Intent(this, com/ximalaya/ting/android/activity/login/LoginActivity));
        return;
_L20:
        startActivity(new Intent(this, com/ximalaya/ting/android/activity/homepage/BindIPhoneActivity));
        return;
_L21:
        intent = ((Uri) (obj1)).getQueryParameter("toUid");
        obj1 = ((Uri) (obj1)).getQueryParameter("title");
        obj = new Intent(this, com/ximalaya/ting/android/activity/homepage/TalkViewAct);
        l2 = 2L;
        if (TextUtils.isEmpty(intent) || TextUtils.isEmpty(((CharSequence) (obj1))))
        {
            ((Intent) (obj)).putExtra("title", "\u62C9\u96C5");
            ((Intent) (obj)).putExtra("toUid", 2L);
        } else
        {
            l2 = Long.parseLong(intent);
            ((Intent) (obj)).putExtra("title", ((String) (obj1)));
            ((Intent) (obj)).putExtra("toUid", l2);
        }
        if (l2 == 2L)
        {
            ((Intent) (obj)).putExtra("isOfficialAccount", true);
        }
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        if (loginInfoModel == null)
        {
            intent = "";
        } else
        {
            intent = loginInfoModel.smallLogo;
        }
        ((Intent) (obj)).putExtra("meHeadUrl", intent);
        startActivity(((Intent) (obj)));
        return;
_L23:
        if (UserInfoMannage.hasLogined())
        {
            MsgCount.sCount_ZoneComments = 0;
            mTabHost.setCurrentTab(4);
            startFragment(com/ximalaya/ting/android/fragment/zone/ZoneMessageFragment, null);
            return;
        }
          goto _L4
_L22:
        if (com.ximalaya.ting.android.a.o)
        {
            obj = new Bundle();
            ((Bundle) (obj)).putLong("zoneId", ((PushModel) (intent)).zoneId);
            startFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment, ((Bundle) (obj)));
            return;
        }
          goto _L4
_L25:
        moveTaskToBack(true);
        return;
_L24:
        playLastPlayedSound();
        return;
_L26:
        obj = new Bundle();
        obj1 = new AlbumModel();
        obj1.albumId = ((PushModel) (intent)).albumId;
        ((Bundle) (obj)).putString("album", JSON.toJSONString(obj1));
        ((Bundle) (obj)).putLong("track_id_to_play", 0L);
        startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj)));
        return;
_L27:
        clearAllFramentFromManageFragment();
        mTabHost.setCurrentTab(4);
        return;
_L28:
        mTabHost.setCurrentTab(4);
        startFragment(ManageCenterFragment.newInstance(0));
        return;
_L29:
        obj = new Bundle();
        ((Bundle) (obj)).putString("key", ((PushModel) (intent)).term);
        startFragment(com/ximalaya/ting/android/fragment/search/WordAssociatedFragment, ((Bundle) (obj)));
        return;
    }

    private void forceVersionUpdate(String s1, String s2)
    {
        s1 = getApkName(s1);
        (new DialogBuilder(this)).setCancelable(false).setMessage("\u5F53\u524D\u7248\u672C\u592A\u65E7\u4E86\uFF0C\u65E0\u6CD5\u6B63\u5E38\u4F7F\u7528\u559C\u9A6C\u62C9\u96C5\uFF0C\u8BF7\u5347\u7EA7\u5E94\u7528").setOkBtn("\u5347\u7EA7", new u(this, s1, s2)).setCancelBtn("\u9000\u51FA", new t(this)).showConfirm();
    }

    private String getApkName(String s1)
    {
        return (new StringBuilder()).append(getResources().getString(0x7f09009e)).append(s1).toString();
    }

    private void initPlayFragment()
    {
        LocalMediaService localmediaservice;
        mAnimView = (RoundedImageView)playButtonLayout.findViewById(0x7f0a00c2);
        playIconImg = (ImageView)playButtonLayout.findViewById(0x7f0a00c3);
        rotateAm = AnimationUtils.loadAnimation(getApplicationContext(), 0x7f040007);
        mAnimView.setOnClickListener(new ab(this));
        localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice == null) goto _L2; else goto _L1
_L1:
        localmediaservice.getPlayServiceState();
        JVM INSTR tableswitch 1 3: default 100
    //                   1 105
    //                   2 100
    //                   3 105;
           goto _L2 _L3 _L2 _L3
_L2:
        registerPlayListener();
        return;
_L3:
        startPlayAnimation();
        if (true) goto _L2; else goto _L4
_L4:
    }

    private void initPreinstall()
    {
        if (!SharedPreferencesUtil.getInstance(this).getBoolean(PreinstallDialog.ISPREINSTALLED, false) && NetworkUtils.getNetType(getApplicationContext()) == 1)
        {
            boolean flag = SharedPreferencesUtil.getInstance(this).getBoolean(PreinstallDialog.ISPREINSTALLDOWNLOAD, false);
            boolean flag1 = SharedPreferencesUtil.getInstance(mainTabActivity).getBoolean(PreinstallDialog.ISPREINSTALLSHOW, false);
            if (flag || flag1)
            {
                String s1 = SharedPreferencesUtil.getInstance(mActivity).getString(PreinstallDialog.ISPREINSTALLAPPAD);
                if (!TextUtils.isEmpty(s1))
                {
                    mAppAd = (AppAd)JSON.parseObject(s1, com/ximalaya/ting/android/library/model/AppAd);
                }
                if (!flag)
                {
                    Intent intent = new Intent(mainTabActivity, com/ximalaya/ting/android/library/service/DownloadService);
                    intent.putExtra(DownloadService.SHALLKEEPKEY, PreinstallDialog.ISPREINSTALLDOWNLOAD);
                    intent.putExtra(DownloadService.APPAD, JSON.toJSONString(mAppAd));
                    startService(intent);
                    return;
                }
            } else
            {
                f.a().a("m/android_channel_binding", null, null, new ac(this));
                return;
            }
        }
    }

    private void initSmartBarTabs()
    {
        if (getActionBar().getTabCount() != 4)
        {
            ActionBar actionbar = getActionBar();
            actionbar.removeAllTabs();
            mSmartBarTabs = new View[4];
            k k1 = new k(this);
            View view = createSmartBarItem(0x7f02056f, "\u53D1\u73B0");
            mSmartBarTabs[0] = view;
            actionbar.addTab(actionbar.newTab().setCustomView(view).setTabListener(k1), false);
            view = createSmartBarItem(0x7f020569, "\u5B9A\u5236\u542C");
            mSmartBarTabs[1] = view;
            actionbar.addTab(actionbar.newTab().setCustomView(view).setTabListener(k1), false);
            view = createSmartBarItem(0x7f02056c, "\u4E0B\u8F7D\u542C");
            mSmartBarTabs[2] = view;
            actionbar.addTab(actionbar.newTab().setCustomView(view).setTabListener(k1), false);
            view = createSmartBarItem(0x7f020572, "\u6211");
            mSmartBarTabs[3] = view;
            actionbar.addTab(actionbar.newTab().setCustomView(view).setTabListener(k1), false);
            if (mTabHost != null)
            {
                int j1 = mTabHost.getCurrentTab();
                int i1 = j1;
                if (j1 > 2)
                {
                    i1 = j1 - 1;
                }
                selectSmartTab(i1);
            }
        }
    }

    private void initTabListeners()
    {
        View view = mTabHost.getTabWidget().getChildTabViewAt(1);
        if (view != null)
        {
            view.setOnClickListener(new com.ximalaya.ting.android.activity.z(this));
        }
    }

    private void installAppPre()
    {
_L2:
        return;
        if (mAppAd == null || SharedPreferencesUtil.getInstance(this).getBoolean(PreinstallDialog.ISPREINSTALLED, false) || !SharedPreferencesUtil.getInstance(getApplicationContext()).getBoolean(PreinstallDialog.ISPREINSTALLDOWNLOAD, false)) goto _L2; else goto _L1
_L1:
        Object obj;
label0:
        {
            String s1 = mAppAd.getName();
            if (s1 == null)
            {
                continue; /* Loop/switch isn't completed */
            }
            if (s1.lastIndexOf(".apk") != -1)
            {
                obj = s1;
                if (s1.lastIndexOf(".apk") == s1.length() - 4)
                {
                    break label0;
                }
            }
            obj = (new StringBuilder()).append(s1).append(".apk").toString();
        }
        String s2 = DownloadService.getFilePath(((String) (obj)));
        obj = new File(s2);
        if (!TextUtils.isEmpty(s2) && ((File) (obj)).exists())
        {
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setDataAndType(Uri.fromFile(((File) (obj))), "application/vnd.android.package-archive");
            startActivity(intent);
            SharedPreferencesUtil.getInstance(getApplicationContext()).saveBoolean(PreinstallDialog.ISPREINSTALLED, true);
            return;
        }
        if (true) goto _L2; else goto _L3
_L3:
    }

    public static boolean isMainTabActivityAvaliable()
    {
        return mainTabActivity != null && !mainTabActivity.isFinishing();
    }

    private void judgeLogin()
    {
        Object obj = SharedPreferencesUtil.getInstance(this).getString("loginforesult");
        if (obj != null && !"".equals(obj))
        {
            obj = parseLoginfo(((String) (obj)));
            if (obj != null)
            {
                UserInfoMannage.getInstance().setUser(((LoginInfoModel) (obj)));
            }
        }
    }

    private void loginCompletedInit()
    {
        if (mTabHost != null)
        {
            mTabHost.postDelayed(new com.ximalaya.ting.android.activity.f(this), 200L);
        }
    }

    private LoginInfoModel parseLoginfo(String s1)
    {
label0:
        {
            try
            {
                if (!"0".equals(JSON.parseObject(s1).get("ret").toString()))
                {
                    break label0;
                }
                s1 = (LoginInfoModel)JSON.parseObject(s1, com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s1)
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
                return null;
            }
            return s1;
        }
        return null;
    }

    private void playLastPlayedSound()
    {
        registerPlayListener();
        Object obj = LocalMediaService.getInstance();
        if (obj == null)
        {
            mAnimView.clearAnimation();
            playIconImg.setVisibility(0);
        } else
        {
            switch (((LocalMediaService) (obj)).getPlayServiceState())
            {
            default:
                return;

            case 0: // '\0'
                if (mLivePlayFragment != null && mLivePlayFragment.isAdded())
                {
                    showLivePlayFragment(DataCollectUtil.getDataFromView(mAnimView));
                    return;
                } else
                {
                    showRecommendSoundListFragment();
                    return;
                }

            case 1: // '\001'
            case 3: // '\003'
                obj = PlayListControl.getPlayListManager().getCurSound();
                if (obj == null || ((SoundInfo) (obj)).category == 0)
                {
                    showPlayFragment(DataCollectUtil.getDataFromView(mAnimView));
                    return;
                } else
                {
                    showLivePlayFragment(DataCollectUtil.getDataFromView(mAnimView));
                    return;
                }

            case 2: // '\002'
                break;
            }
            if (obj != null && ((LocalMediaService) (obj)).hasValidatePlayUrl())
            {
                int i1 = NetworkUtils.getNetType(getApplicationContext());
                boolean flag = SharedPreferencesUtil.getInstance(getApplicationContext()).getBoolean("is_download_enabled_in_3g", false);
                if (1 == i1 || flag || ((LocalMediaService) (obj)).isPlayFromLocal())
                {
                    if (!mIsEverPlayed)
                    {
                        PlayListControl.getPlayListManager().listType = 26;
                    }
                    mIsEverPlayed = true;
                    ((LocalMediaService) (obj)).start();
                    obj = PlayListControl.getPlayListManager().getCurSound();
                    if (obj == null || ((SoundInfo) (obj)).category == 0)
                    {
                        showPlayFragment(DataCollectUtil.getDataFromView(mAnimView));
                        return;
                    } else
                    {
                        showLivePlayFragment(DataCollectUtil.getDataFromView(mAnimView));
                        return;
                    }
                } else
                {
                    NetworkUtils.showChangeNetWorkSetConfirm(new aa(this, ((LocalMediaService) (obj))), null);
                    return;
                }
            }
        }
    }

    private void refreshCurrentFeedTab()
    {
        if ("tab_d".equals(mCurrentTab))
        {
            Fragment fragment = (Fragment)mStacks.get(mCurrentTab);
            if (fragment != null && fragment.isAdded() && (fragment instanceof FeedMainFragment))
            {
                ((FeedMainFragment)fragment).onRefresh();
            }
            return;
        } else
        {
            setCurrentTab(1);
            return;
        }
    }

    private void refreshLocalFavFragment()
    {
        if (!UserInfoMannage.hasLogined() && mStacks != null && !mStacks.isEmpty())
        {
            Fragment fragment = (Fragment)mStacks.get(mCurrentTab);
            if (fragment != null && (fragment instanceof FeedMainFragment) && fragment.isAdded())
            {
                ((FeedMainFragment)fragment).refreshLocalFav();
            }
        }
    }

    private void registerPlayListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null && mOnPlayServiceUpdateListener == null && mOnPlayerStatusUpdateListener == null)
        {
            mOnPlayServiceUpdateListener = new m(this);
            localmediaservice.setOnPlayServiceUpdateListener(mOnPlayServiceUpdateListener);
            mOnPlayerStatusUpdateListener = new n(this);
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
            if (DexManager.getInstance(this).isSuichetingInit() && MyBluetoothManager2.getInstance(mAppContext).isSuichetingPlaying())
            {
                MyBluetoothManager2.getInstance(mAppContext).registerPlayListener();
            }
        }
    }

    private void removeFragmentFromStacks(String s1)
    {
        Fragment fragment = (Fragment)mStacks.get(s1);
        if (fragment != null)
        {
            mStacks.remove(s1);
            s1 = getSupportFragmentManager().beginTransaction();
            s1.remove(fragment);
            s1.commitAllowingStateLoss();
        }
    }

    private void selectSmartTab(int i1)
    {
        int j1 = 0;
        while (j1 < mSmartBarTabs.length) 
        {
            View view = mSmartBarTabs[j1];
            if (view != null)
            {
                boolean flag;
                if (i1 == j1)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                view.setSelected(flag);
            }
            j1++;
        }
        ActionBar actionbar = getActionBar();
        android.app.ActionBar.Tab tab = actionbar.getTabAt(i1);
        if (tab != null)
        {
            actionbar.selectTab(tab);
        }
    }

    private void setCurrentFragment(String s1, Bundle bundle)
    {
        if (mCurrentTab != s1) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        if (mStacks.get(mCurrentTab) != null)
        {
            ((Fragment)mStacks.get(mCurrentTab)).onPause();
        }
        mCurrentTab = s1;
        obj = (Fragment)mStacks.get(s1);
        if (obj == null)
        {
            getSupportFragmentManager().findFragmentByTag(s1);
        }
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_333;
        }
        Iterator iterator;
        if ("tab_a".equals(s1))
        {
            obj = new FindingFragmentV3();
        } else
        {
            if (!"tab_b".equals(s1))
            {
                continue; /* Loop/switch isn't completed */
            }
            obj = new AppTabBFragment();
        }
_L5:
        if (obj == null || s1 == null) goto _L1; else goto _L3
_L3:
        ((Fragment) (obj)).setArguments(bundle);
        bundle = getSupportFragmentManager().beginTransaction();
        bundle.add(0x7f0a00bd, ((Fragment) (obj)), s1);
        iterator = mStacks.entrySet().iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            Object obj1 = (java.util.Map.Entry)iterator.next();
            if (((java.util.Map.Entry) (obj1)).getKey() != null && ((java.util.Map.Entry) (obj1)).getValue() != null)
            {
                if (((String)((java.util.Map.Entry) (obj1)).getKey()).equals(mCurrentTab))
                {
                    obj1 = (Fragment)((java.util.Map.Entry) (obj1)).getValue();
                    bundle.show(((Fragment) (obj1)));
                    ((Fragment) (obj1)).onResume();
                } else
                {
                    bundle.hide((Fragment)((java.util.Map.Entry) (obj1)).getValue());
                }
            }
        } while (true);
        break MISSING_BLOCK_LABEL_317;
        if ("tab_c".equals(s1)) goto _L1; else goto _L4
_L4:
        if ("tab_d".equals(s1))
        {
            obj = new FeedMainFragment();
        } else
        if ("tab_e".equals(s1))
        {
            obj = new MySpaceFragmentNew();
        } else
        {
            obj = null;
        }
          goto _L5
        bundle.commitAllowingStateLoss();
        mStacks.put(s1, obj);
        return;
        showFragmentById(((Fragment) (obj)));
        return;
    }

    private void showAppIntroPage()
    {
        if (ToolUtil.isFirstLaunch(getApplicationContext()) && com.ximalaya.ting.android.a.n)
        {
            startFragment(AppIntroductionFragment.newInstance(), 0x7f040004, 0x7f040005);
        }
    }

    private void showFragmentById(Fragment fragment)
    {
        FragmentManager fragmentmanager = getSupportFragmentManager();
        FragmentTransaction fragmenttransaction = fragmentmanager.beginTransaction();
        if (fragment != null)
        {
            if (fragment.isHidden())
            {
                fragmenttransaction.show(fragment);
                fragment.onResume();
            }
            if (fragment.isDetached())
            {
                fragment = fragmentmanager.findFragmentByTag(mCurrentTab);
                if (fragmenttransaction != null)
                {
                    fragmenttransaction.attach(fragment);
                }
            }
        }
        fragment = mStacks.entrySet().iterator();
        do
        {
            if (!fragment.hasNext())
            {
                break;
            }
            Object obj = (java.util.Map.Entry)fragment.next();
            if (((java.util.Map.Entry) (obj)).getKey() != null && ((java.util.Map.Entry) (obj)).getValue() != null)
            {
                if (((String)((java.util.Map.Entry) (obj)).getKey()).equals(mCurrentTab))
                {
                    obj = (Fragment)((java.util.Map.Entry) (obj)).getValue();
                    if (((Fragment) (obj)).isDetached())
                    {
                        obj = fragmentmanager.findFragmentByTag(mCurrentTab);
                        if (fragmenttransaction != null)
                        {
                            fragmenttransaction.attach(((Fragment) (obj)));
                        }
                    } else
                    {
                        fragmenttransaction.show(((Fragment) (obj)));
                        ((Fragment) (obj)).onResume();
                    }
                } else
                {
                    fragmenttransaction.hide((Fragment)((java.util.Map.Entry) (obj)).getValue());
                }
            }
        } while (true);
        fragmenttransaction.commitAllowingStateLoss();
    }

    private void showPlayHistoryTip()
    {
        Object obj = PlayListControl.getPlayListManager().getPlaylist().getData();
        int i1 = PlayListControl.getPlayListManager().curIndex;
        if (obj == null || ((List) (obj)).size() == 0 || MyApplication.b() == null || i1 < 0 || i1 >= ((List) (obj)).size())
        {
            return;
        }
        android.content.Context context = MyApplication.b();
        obj = (SoundInfo)((List) (obj)).get(i1);
        Toast toast = new Toast(context);
        i1 = ToolUtil.dp2px(context, 65F);
        int j1 = ToolUtil.dp2px(context, 5F);
        int k1 = ToolUtil.dp2px(context, 10F);
        toast.setGravity(80, 0, i1);
        toast.setDuration(1);
        TextView textview = new TextView(context);
        textview.setBackgroundResource(0x7f02009a);
        textview.setPadding(k1, j1, k1, j1);
        textview.setGravity(49);
        textview.setTextColor(-1);
        textview.setTextSize(14F);
        textview.setMaxWidth((ToolUtil.getScreenWidth(context) * 3) / 4);
        textview.setMaxLines(1);
        if (((SoundInfo) (obj)).category == 0)
        {
            textview.setText((new StringBuilder()).append("\u4E0A\u6B21\u64AD\u653E:").append(((SoundInfo) (obj)).title).toString());
        } else
        {
            textview.setText((new StringBuilder()).append("\u4E0A\u6B21\u64AD\u653E\uFF1A").append(((SoundInfo) (obj)).radioName).toString());
        }
        toast.setView(textview);
        toast.show();
    }

    private void showRecommendSoundListFragment()
    {
        startFragment(WeekHotSoundListFragment.getInstance(), 0x7f040009, 0x7f040008);
    }

    private void unRegisterListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayServiceUpdateListener(mOnPlayServiceUpdateListener);
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
        }
        if (NoReadManage.getInstance() != null)
        {
            NoReadManage.getInstance().removeNoReadUpdateListenerListener(this);
        }
        if (DownloadHandler.getInstance(getApplicationContext()) != null)
        {
            DownloadHandler.getInstance(getApplicationContext()).removeDownloadListeners(this);
        }
    }

    private void updateProgressDialog(int i1)
    {
        if (m_pDialog != null && i1 >= 0 && i1 <= 100)
        {
            m_pDialog.setProgress(i1);
            if (100 == i1)
            {
                uphandler.sendEmptyMessage(3);
                return;
            }
        }
    }

    private void upgrade()
    {
        f.a().a(2000);
        f.a().a("mobile/version", null, null, new q(this));
    }

    public void clearAllFramentFromManageFragment()
    {
        if (mManageFragment != null)
        {
            mManageFragment.clearAllFragmentFromStacks();
        }
        BaseFragment basefragment = (BaseFragment)mStacks.get(mCurrentTab);
        if (basefragment != null)
        {
            basefragment.onViewResume();
        }
    }

    public void clearLockScreen()
    {
        this;
        JVM INSTR monitorenter ;
        BaseFragment basefragment = mLockScreenFragment;
        if (basefragment != null) goto _L2; else goto _L1
_L1:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        if (mLockScreenFragment.isAdded())
        {
            ((LockScreenFragment)mLockScreenFragment).setLockScreenImage(0);
            hideFragment(mLockScreenFragment, 0x7f040009, 0x7f040008);
        }
        if (true) goto _L1; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    public boolean containsFragment(Class class1)
    {
        return mManageFragment != null && mManageFragment.getCurrentFragment() != null && mManageFragment.getCurrentFragment().getClass().equals(class1);
    }

    public void finish()
    {
        SdlConnectManager.getInstance(mAppContext).release();
        super.finish();
        installAppPre();
    }

    public ManageFragment getManageFragment()
    {
        return mManageFragment;
    }

    public void goToFindingPage()
    {
        setCurrentTab(0);
        clearAllFramentFromManageFragment();
    }

    public void hidePlayButton()
    {
        if (playButtonLayout != null)
        {
            ObjectAnimator.ofFloat(playButtonLayout, "y", new float[] {
                (float)playButtonLayout.getHeight()
            }).start();
            if (android.os.Build.VERSION.SDK_INT < 11)
            {
                playButtonLayout.setVisibility(8);
            }
        }
    }

    public void initTingshubao()
    {
        mDexManager = DexManager.getInstance(getApplicationContext());
        if (mDexManager.isDlnaOpen())
        {
            tingshubaoBind();
        }
    }

    public void initializeTabs()
    {
        addTab("tab_a", 0x7f02056f, "\u53D1\u73B0");
        addTab("tab_d", 0x7f020569, "\u5B9A\u5236\u542C");
        addTab("tab_c", -1, "\u5F55\u97F3");
        addTab("tab_b", 0x7f02056c, "\u4E0B\u8F7D\u542C");
        addTab("tab_e", 0x7f020572, "\u6211");
        initTabListeners();
    }

    public void logout()
    {
        TextView textview1 = null;
        clearNotice();
        removeFragmentFromStacks("tab_d");
        mTabHost.setCurrentTab(0);
        TextView textview;
        if (ToolUtil.isUseSmartbarAsTab())
        {
            if (mSmartBarTabs != null)
            {
                textview = (TextView)mSmartBarTabs[1].findViewById(0x7f0a06f2);
                textview1 = (TextView)mSmartBarTabs[3].findViewById(0x7f0a06f2);
            } else
            {
                textview = null;
            }
        } else
        {
            textview = (TextView)(TextView)mTabHost.getTabWidget().getChildTabViewAt(1).findViewById(0x7f0a06f2);
            textview1 = (TextView)(TextView)mTabHost.getTabWidget().getChildTabViewAt(4).findViewById(0x7f0a06f2);
        }
        if (textview != null)
        {
            textview.setVisibility(4);
        }
        if (textview1 != null)
        {
            textview1.setVisibility(4);
        }
    }

    protected void onActivityResult(int i1, int j1, Intent intent)
    {
        if (i1 == 9527)
        {
            Logger.log((new StringBuilder()).append("requestCode = ").append(i1).append(", resultCode = ").append(j1).toString());
            if (j1 == -1 && NetworkUtils.getNetType(this) == 0)
            {
                FreeFlowUtil.getInstance().useFreeFlow();
            }
        }
        if (mPlayerFragment != null && mPlayerFragment.isVisible())
        {
            mPlayerFragment.onActivityResult(i1, j1, intent);
        } else
        {
            if (mManageFragment != null && mManageFragment.getCurrentFragment() != null)
            {
                mManageFragment.getCurrentFragment().onActivityResult(i1, j1, intent);
                return;
            }
            if (mStacks.get(mCurrentTab) != null)
            {
                ((Fragment)mStacks.get(mCurrentTab)).onActivityResult(i1, j1, intent);
                return;
            }
        }
    }

    public void onBack()
    {
_L2:
        return;
        if (isFinishing() || mLockScreenFragment != null && mLockScreenFragment.isVisible() && mLockScreenFragment.onBackPressed()) goto _L2; else goto _L1
_L1:
        if (mPlayerFragment != null && mPlayerFragment.isVisible())
        {
            onbackPlayFragment();
            return;
        }
        if (mLivePlayFragment != null && mLivePlayFragment.isVisible())
        {
            onbackLivePlayFragment();
            return;
        }
        if (mManageFragment != null && mManageFragment.getFragmentCount() == 1)
        {
            Fragment fragment = (Fragment)mStacks.get(mCurrentTab);
            if (fragment != null && fragment.isAdded() && (fragment instanceof BaseFragment))
            {
                ((BaseFragment)fragment).restoreViewTreeImageView();
            }
        }
        if (mManageFragment == null || !mManageFragment.onBackPressed())
        {
            continue; /* Loop/switch isn't completed */
        }
        if (mManageFragment.getFragmentCount() == com.ximalaya.ting.android.a.i - 1 && com.ximalaya.ting.android.a.f)
        {
            BaseFragment basefragment = (BaseFragment)mStacks.get(mCurrentTab);
            if (basefragment != null)
            {
                basefragment.onViewResume();
            }
        }
        if (PackageUtil.isPostHB())
        {
            invalidateOptionsMenu();
        }
        if (mManageFragment.getFragmentCount() != 0 || mStacks.get(mCurrentTab) == null || !((Fragment)mStacks.get(mCurrentTab)).isAdded()) goto _L2; else goto _L3
_L3:
        ((Fragment)mStacks.get(mCurrentTab)).onResume();
        return;
        if (mStacks.get(mCurrentTab) != null && ((BaseFragment)mStacks.get(mCurrentTab)).onBackPressed()) goto _L2; else goto _L4
_L4:
        String s1;
        if (z.a(mActivity).c() > 0)
        {
            s1 = "\u5F53\u524D\u6B63\u6709\u5F55\u97F3\u6587\u4EF6\u6B63\u5728\u4E0A\u4F20\uFF0C\u73B0\u5728\u9000\u51FA\u4F1A\u4E2D\u65AD\u4E0A\u4F20\uFF0C\u786E\u5B9A\u9000\u51FA\u559C\u9A6C\u62C9\u96C5FM?";
        } else
        {
            s1 = getResources().getString(0x7f0900a3);
        }
        (new DialogBuilder(this)).setMessage(s1).setOkBtn(new h(this)).setNeutralBtn(0x7f0900a2, new g(this)).showMultiButton();
        return;
    }

    public void onBackPressed()
    {
        onBack();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(new Bundle());
        mainTabActivity = this;
        if (!ToolUtil.isUseSmartbarAsTab())
        {
            setTheme(0x7f0b0042);
            requestWindowFeature(1);
        } else
        {
            if (android.os.Build.VERSION.SDK_INT >= 11)
            {
                getActionBar().hide();
            }
            getActionBar().setNavigationMode(2);
            ToolUtil.setActionBarTabsShowAtBottom(getActionBar(), true);
        }
        setContentView(0x7f03001c);
        mManageFragment = new ManageFragment();
        mManageFragment.setOnFragmentStackChangeListener(new i(this));
        addFragmentToLayout(0x7f0a00bf, mManageFragment, 0, 0);
        playButtonLayout = (FrameLayout)findViewById(0x7f0a00c1);
        DataCollectUtil.bindViewIdDataToView(playButtonLayout);
        mStacks = new HashMap();
        mTabHost = (TabHost)findViewById(0x1020012);
        mTabHost.setOnTabChangedListener(listener);
        mTabHost.setup();
        initializeTabs();
        if (ToolUtil.isUseSmartbarAsTab())
        {
            mTabHost.getTabWidget().setVisibility(8);
            findViewById(0x7f0a00be).setVisibility(8);
        } else
        {
            mTabHost.getTabWidget().setVisibility(0);
        }
        judgeLogin();
        BaiduLocationManager.getInstance().requestLocationInfo(getApplicationContext());
        userInfoModel = UserInfoMannage.getInstance().getUser();
        NoReadManage.getInstance().setOnNoReadUpdateListenerListener(this);
        DownloadHandler.getInstance(getApplicationContext()).addDownloadListeners(this);
        playButtonLayout.postDelayed(new j(this), 300L);
        initPlayFragment();
        initPreinstall();
        initTingshubao();
        if (NetworkUtils.getNetType(getApplicationContext()) == 1)
        {
            upgrade();
        }
        try
        {
            XiMaoBTManager.getInstance(mAppContext).registerXiMaoMainReceiver(this);
        }
        // Misplaced declaration of an exception variable
        catch (Bundle bundle)
        {
            Logger.d(TAG, bundle.toString());
        }
        SdlConnectManager.getInstance(mAppContext).startController();
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        if (ToolUtil.isUseSmartbarAsTab())
        {
            return super.onCreateOptionsMenu(menu);
        } else
        {
            menu.add("menu");
            return super.onCreateOptionsMenu(menu);
        }
    }

    protected void onDestroy()
    {
        super.onDestroy();
        z.a(getApplicationContext()).a();
        CarlifeManager.getInstance(mActivity).release();
        unRegisterListener();
        uphandler = null;
        if (mPlayingRadioStartTime > 0L)
        {
            mPlayingRadioDuration = mPlayingRadioDuration + (System.currentTimeMillis() - mPlayingRadioStartTime);
            mPlayingRadioStartTime = 0L;
        }
        if (mPlayingRadioDuration > 0x927c0L)
        {
            ToolUtil.onEvent(this, "LanRenRadio_Play_MoreThan10min");
        }
        if (MyDeviceManager.getInstance(this).isDlnaInit())
        {
            DlnaManager.getInstance(this).unBindService();
        }
        ((MyApplication)getApplication()).g();
    }

    public void onDismiss()
    {
        Fragment fragment = (Fragment)mStacks.get("tab_a");
        if (fragment instanceof FindingFragmentV3)
        {
            ((FindingFragmentV3)fragment).updateGuessLikeData();
        }
    }

    public boolean onKeyDown(int i1, KeyEvent keyevent)
    {
        if (!MyDeviceManager.getInstance(mAppContext).isDlnaInit()) goto _L2; else goto _L1
_L1:
        if (!DlnaManager.getInstance(mAppContext).isInWifiControlPage() || !DlnaManager.getInstance(mAppContext).isOperatedDevicePlaying() || DlnaManager.getInstance(mAppContext).getController().getModule(BaseVolumeControlModule.NAME) == null) goto _L4; else goto _L3
_L3:
        BaseVolumeControlModule basevolumecontrolmodule = (BaseVolumeControlModule)DlnaManager.getInstance(mAppContext).getController().getModule(BaseVolumeControlModule.NAME);
        i1;
        JVM INSTR tableswitch 24 25: default 104
    //                   24 118
    //                   25 111;
           goto _L2 _L5 _L6
_L2:
        return super.onKeyDown(i1, keyevent);
_L6:
        basevolumecontrolmodule.volumePlus(false);
        return true;
_L5:
        basevolumecontrolmodule.volumePlus(true);
        return true;
_L4:
        if (!DlnaManager.getInstance(mAppContext).isLinkedDeviceValid()) goto _L2; else goto _L7
_L7:
        switch (i1)
        {
        case 24: // '\030'
            DlnaManager.getInstance(mAppContext).changeVolume(true);
            return true;

        case 25: // '\031'
            DlnaManager.getInstance(mAppContext).changeVolume(false);
            return true;
        }
        if (true) goto _L2; else goto _L8
_L8:
    }

    public boolean onMenuOpened(int i1, Menu menu)
    {
        if (ToolUtil.isUseSmartbarAsTab())
        {
            return super.onMenuOpened(i1, menu);
        }
        if (mTabMenu == null)
        {
            mTabMenu = new TabMenu(getApplicationContext());
            mTabMenu.setMenuTitle(mMenuTitle);
            mTabMenu.setMenuIcon(mMenuIcon);
            mTabMenu.setOnItemClickListener(new l(this));
            mTabMenu.setBackground(0x7f07006c);
            mTabMenu.setMenuSelector(0x7f070003);
        }
        if (mTabMenu.isShowing())
        {
            mTabMenu.dismiss();
        } else
        {
            mTabMenu.showAt(playButtonLayout);
        }
        return false;
    }

    public void onNewIntent(Intent intent)
    {
        Intent intent1;
        super.onNewIntent(intent);
        setIntent(intent);
        if (intent.hasExtra("FINDING"))
        {
            mTabHost.setCurrentTab(0);
            return;
        }
        if (intent.hasExtra("download"))
        {
            Object obj = new Bundle();
            ((Bundle) (obj)).putInt("page", 2);
            mTabHost.setTag(obj);
            mTabHost.setCurrentTab(3);
            obj = (Fragment)mStacks.get("tab_b");
            if (obj instanceof AppTabBFragment)
            {
                ((AppTabBFragment)obj).setCurrPage(intent.getIntExtra("page", 0));
            }
        }
        if (intent.hasExtra("isLogin") && intent.getBooleanExtra("isLogin", false))
        {
            userInfoModel = UserInfoMannage.getInstance().getUser();
            if (userInfoModel != null)
            {
                loginCompletedInit();
                removeFragmentFromStacks("tab_d");
            }
            if (mCurrentTab.equals("tab_e"))
            {
                mTabHost.setCurrentTabByTag("tab_e");
            } else
            if (mCurrentTab.equals("tab_b"))
            {
                mTabHost.setCurrentTabByTag("tab_b");
            } else
            {
                mTabHost.setCurrentTab(0);
            }
        }
        intent1 = intent;
        if (!intent.hasExtra("shouldRelogin")) goto _L2; else goto _L1
_L1:
        intent1 = intent;
        if (!intent.getBooleanExtra("shouldRelogin", false)) goto _L2; else goto _L3
_L3:
        userInfoModel = UserInfoMannage.getInstance().getUser();
        if (userInfoModel != null) goto _L5; else goto _L4
_L4:
        intent1 = new Intent(this, com/ximalaya/ting/android/activity/login/LoginActivity);
        startActivity(intent1);
_L2:
        if (intent1.hasExtra("show_main_tab"))
        {
            clearAllFramentFromManageFragment();
        }
        doSomethingByIntent(intent1);
        return;
_L5:
        removeFragmentFromStacks("tab_d");
        mTabHost.setCurrentTab(0);
        intent1 = intent;
        if (ToolUtil.isUseSmartbarAsTab())
        {
            selectSmartTab(0);
            intent1 = intent;
        }
        if (true) goto _L2; else goto _L6
_L6:
    }

    public boolean onPrepareOptionsMenu(Menu menu)
    {
        if (ToolUtil.isUseSmartbarAsTab())
        {
            if (mManageFragment != null && mManageFragment.getFragmentCount() > 0)
            {
                getActionBar().removeAllTabs();
                mSmartBarTabs = null;
            } else
            {
                initSmartBarTabs();
                refreshIncompleteTaskNum();
            }
        }
        return super.onPrepareOptionsMenu(menu);
    }

    public void onResume()
    {
        super.onResume();
        refreshIncompleteTaskNum();
        NoReadManage.getInstance().updateNoReadManageFromNet(this);
        doFollow();
        ((MyApplication)getApplication()).o();
    }

    protected void onStart()
    {
        super.onStart();
    }

    public void onTaskComplete()
    {
    }

    public void onTaskDelete()
    {
    }

    public void onbackLivePlayFragment()
    {
        if (mLivePlayFragment != null && mLivePlayFragment.isAdded() && !mLivePlayFragment.onBackPressed())
        {
            hideFragment(mLivePlayFragment, 0x7f040009, 0x7f040008);
            mLivePlayFragment.onViewPause();
            mLivePlayFragment.onPause();
        }
    }

    public void onbackPlayFragment()
    {
        if (mPlayerFragment != null && mPlayerFragment.isAdded() && !mPlayerFragment.onBackPressed())
        {
            hideFragment(mPlayerFragment, 0x7f040009, 0x7f040008);
            mPlayerFragment.onViewPause();
            mPlayerFragment.onPause();
        }
    }

    public void refreshIncompleteTaskNum()
    {
        DownloadHandler downloadhandler = DownloadHandler.getInstance(getApplicationContext());
        if (downloadhandler != null)
        {
            updateIncompleteTaskNum(downloadhandler.getIncompleteTaskCount());
        }
    }

    public void removeFramentFromManageFragment(Fragment fragment)
    {
        if (mManageFragment != null)
        {
            mManageFragment.removeFragmentFromStacks(fragment, false);
        }
    }

    public void removeTopFramentFromManageFragment()
    {
        if (mManageFragment != null)
        {
            mManageFragment.removeTopFragment();
        }
    }

    public void setCurrentTab(int i1)
    {
        mTabHost.setCurrentTab(i1);
    }

    public void setPlayButtonCover(String s1)
    {
        if (TextUtils.isEmpty(s1) || mAnimView == null)
        {
            return;
        } else
        {
            ImageManager2.from(this).displayImage(mAnimView, s1, 0x7f0200db);
            return;
        }
    }

    public void showLivePlayFragment(String s1)
    {
        if (mPlayerFragment != null && MyDeviceManager.getInstance(this).isDlnaInit() && DlnaManager.getInstance(mAppContext).getLinkedDeviceModel() != null && DlnaManager.getInstance(mAppContext).isLinkedDeviceValid())
        {
            CustomToast.showToast(mAppContext, "\u60A8\u6B63\u5728\u4F7F\u7528WIFI\u8BBE\u5907\u8FDB\u884C\u64AD\u653E\uFF0C\u6B63\u5728\u4E3A\u60A8\u5207\u56DE\u672C\u5730\u64AD\u653E", 0);
        }
        if (mPlayerFragment != null)
        {
            FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();
            fragmenttransaction.remove(mPlayerFragment);
            fragmenttransaction.commitAllowingStateLoss();
            mPlayerFragment = null;
            isAddPlayFragment = false;
        }
        if (mLivePlayFragment == null)
        {
            mLivePlayFragment = new LivePlayerFragment();
            Bundle bundle = new Bundle();
            bundle.putString("xdcs_data_bundle", s1);
            mLivePlayFragment.setArguments(bundle);
        }
        if (!mLivePlayFragment.isAdded() && !isAddLivePlayFragment)
        {
            isAddLivePlayFragment = true;
            addFragmentToLayout(0x7f0a00c4, mLivePlayFragment, 0x7f040009, 0x7f040008);
        } else
        {
            showFragment(mLivePlayFragment, 0x7f040009, 0x7f040008);
            if (mLivePlayFragment != null && mLivePlayFragment.isAdded())
            {
                mLivePlayFragment.onViewResume();
                mLivePlayFragment.onResume();
                mLivePlayFragment.onBind(s1);
                return;
            }
        }
    }

    public void showLockScreen()
    {
        this;
        JVM INSTR monitorenter ;
        if (mLockScreenFragment == null)
        {
            mLockScreenFragment = new LockScreenFragment();
        }
        ((LockScreenFragment)mLockScreenFragment).setLockScreenImage(0x7f020270);
        if (mLockScreenFragment.isAdded() || isAddLockScreen) goto _L2; else goto _L1
_L1:
        isAddLockScreen = true;
        addFragmentToLayout(0x7f0a00c5, mLockScreenFragment, 0x7f040009, 0x7f040008);
_L4:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        showFragment(mLockScreenFragment, 0x7f040009, 0x7f040008);
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        throw exception;
    }

    public void showPlayButton()
    {
        if (playButtonLayout != null)
        {
            if (android.os.Build.VERSION.SDK_INT < 11)
            {
                playButtonLayout.setVisibility(0);
            }
            ObjectAnimator.ofFloat(playButtonLayout, "y", new float[] {
                0.0F
            }).start();
        }
    }

    public void showPlayFragment(String s1)
    {
        MyLogger.getLogger().d((new StringBuilder()).append("PlayerFragment=").append(mPlayerFragment).toString());
        if (mLivePlayFragment != null)
        {
            FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();
            fragmenttransaction.remove(mLivePlayFragment);
            fragmenttransaction.commitAllowingStateLoss();
            mLivePlayFragment = null;
            isAddLivePlayFragment = false;
        }
        if (mPlayerFragment == null)
        {
            mPlayerFragment = new PlayerFragment();
            Bundle bundle = new Bundle();
            bundle.putString("xdcs_data_bundle", s1);
            DataCollectUtil.bindDataToView(s1, mPlayerFragment.getView());
            mPlayerFragment.setArguments(bundle);
        }
        if (mPlayerFragment.isAdded() || isAddPlayFragment) goto _L2; else goto _L1
_L1:
        isAddPlayFragment = true;
        MyLogger.getLogger().d((new StringBuilder()).append("PlayerFragment=").append(mPlayerFragment).toString());
        if (isFinishing() || android.os.Build.VERSION.SDK_INT >= 17 && isDestroyed())
        {
            return;
        }
        addFragmentToLayout(0x7f0a00c4, mPlayerFragment, 0x7f040009, 0x7f040008);
_L4:
        Logger.e("BaseFragment", "start play fragment");
        return;
_L2:
        MyLogger.getLogger().d((new StringBuilder()).append("PlayerFragment=").append(mPlayerFragment).toString());
        showFragment(mPlayerFragment, 0x7f040009, 0x7f040008);
        if (mPlayerFragment != null && mPlayerFragment.isAdded())
        {
            mPlayerFragment.onViewResume();
            mPlayerFragment.onResume();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void startFragment(Fragment fragment)
    {
        startFragment(fragment, 0, 0);
    }

    public void startFragment(Fragment fragment, int i1, int j1)
    {
        if (mManageFragment != null && mManageFragment.isAdded())
        {
            Fragment fragment1 = null;
            if (mManageFragment.getFragmentCount() == 0)
            {
                Fragment fragment2 = (Fragment)mStacks.get(mCurrentTab);
                fragment1 = fragment2;
                if (fragment2 != null)
                {
                    fragment2.onPause();
                    fragment1 = fragment2;
                }
            }
            onbackPlayFragment();
            mManageFragment.startFragment(fragment, i1, j1);
            if (fragment1 != null && (fragment1 instanceof BaseFragment))
            {
                ((BaseFragment)fragment1).releaseViewTreeImageView();
            }
            if (com.ximalaya.ting.android.a.f && mManageFragment.getFragmentCount() == com.ximalaya.ting.android.a.i && mStacks.get(mCurrentTab) != null)
            {
                ((BaseFragment)mStacks.get(mCurrentTab)).onViewPause();
            }
            Log.e(TAG, (new StringBuilder()).append("start Fragment ").append(fragment.getClass().getSimpleName()).toString());
        }
        if (PackageUtil.isPostHB())
        {
            invalidateOptionsMenu();
        }
    }

    public void startFragment(Class class1, Bundle bundle)
    {
        if (mManageFragment != null && mManageFragment.isAdded())
        {
            Fragment fragment = null;
            if (mManageFragment.getFragmentCount() == 0)
            {
                Fragment fragment1 = (Fragment)mStacks.get(mCurrentTab);
                fragment = fragment1;
                if (fragment1 != null)
                {
                    fragment1.onPause();
                    fragment = fragment1;
                }
            }
            onbackPlayFragment();
            onbackLivePlayFragment();
            mManageFragment.startFragment(class1, bundle);
            if (fragment != null && (fragment instanceof BaseFragment))
            {
                ((BaseFragment)fragment).releaseViewTreeImageView();
            }
            if (com.ximalaya.ting.android.a.f && mManageFragment.getFragmentCount() == com.ximalaya.ting.android.a.i && mStacks.get(mCurrentTab) != null)
            {
                ((BaseFragment)mStacks.get(mCurrentTab)).onViewPause();
            }
            Log.e(TAG, (new StringBuilder()).append("start Fragment ").append(class1.getSimpleName()).toString());
        }
        if (PackageUtil.isPostHB())
        {
            invalidateOptionsMenu();
        }
    }

    public void startPlayAnimation()
    {
        Logger.d("tuisong", "startPlayAnimation");
        break MISSING_BLOCK_LABEL_9;
        if (playButtonLayout != null && playIconImg.getVisibility() == 0)
        {
            mAnimView.startAnimation(rotateAm);
            playIconImg.setVisibility(8);
            if (PlayListControl.getPlayListManager().getCurSound() != null)
            {
                setPlayButtonCover(PlayListControl.getPlayListManager().getCurSound().coverLarge);
                return;
            }
        }
        return;
    }

    public void stopPlayAnimation()
    {
        Logger.d("tuisong", "stopPlayAnimation IN");
        if (playButtonLayout == null)
        {
            return;
        } else
        {
            mAnimView.clearAnimation();
            playIconImg.setVisibility(0);
            return;
        }
    }

    public void switchToCategoryFragment()
    {
        while (isFinishing() || mStacks == null || mStacks.get(mCurrentTab) == null || !((Fragment)mStacks.get(mCurrentTab)).isAdded() || !(mStacks.get(mCurrentTab) instanceof FindingFragmentV3)) 
        {
            return;
        }
        ((FindingFragmentV3)mStacks.get(mCurrentTab)).switchToCategoryFragment();
    }

    public void switchToLiveFragment(String s1)
    {
        while (isFinishing() || mStacks == null || mStacks.get(mCurrentTab) == null || !((Fragment)mStacks.get(mCurrentTab)).isAdded() || !(mStacks.get(mCurrentTab) instanceof FindingFragmentV3)) 
        {
            return;
        }
        ((FindingFragmentV3)mStacks.get(mCurrentTab)).switchToLiveFragment(s1);
    }

    public void switchToTagFragment(String s1)
    {
        while (isFinishing() || mManageFragment == null || !mManageFragment.isAdded() || !mManageFragment.getCurrentFragment().isAdded() || !(mManageFragment.getCurrentFragment() instanceof CategoryContentFragment)) 
        {
            return;
        }
        ((CategoryContentFragment)mManageFragment.getCurrentFragment()).changeTagFragment(s1);
    }

    public void tingshubaoBind()
    {
        Logger.d("WIFI", "tingshubaoBind IN");
        DlnaManager.getInstance(getApplicationContext()).bindService();
        try
        {
            DlnaManager.getInstance(getApplicationContext()).bindService();
            Logger.d("WIFI", "isCanUse bind");
            return;
        }
        catch (NoClassDefFoundError noclassdeffounderror)
        {
            noclassdeffounderror.printStackTrace();
            Logger.d(TAG, "tingshubaoBind isCanUseByError = false");
            Logger.d("NoClassDefFoundError", "mFunction", noclassdeffounderror.getCause());
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public void update(NoReadModel noreadmodel)
    {
        Object obj1 = null;
        if (this != null && !isFinishing())
        {
            Object obj;
            TextView textview;
            int i1;
            int j1;
            int k1;
            int l1;
            if (ToolUtil.isUseSmartbarAsTab())
            {
                if (mSmartBarTabs != null)
                {
                    textview = (TextView)mSmartBarTabs[1].findViewById(0x7f0a06f2);
                    obj = mSmartBarTabs[1].findViewById(0x7f0a04e6);
                } else
                {
                    obj = null;
                    textview = null;
                }
            } else
            {
                textview = (TextView)(TextView)mTabHost.getTabWidget().getChildTabViewAt(1).findViewById(0x7f0a06f2);
                obj = mTabHost.getTabWidget().getChildTabViewAt(1).findViewById(0x7f0a04e6);
            }
            if (noreadmodel.newFeedCount > 0)
            {
                if (PackageUtil.isMeizu())
                {
                    if (textview != null)
                    {
                        textview.setVisibility(8);
                    }
                    if (obj != null)
                    {
                        ((View) (obj)).setVisibility(0);
                    }
                } else
                {
                    if (textview != null)
                    {
                        String s1;
                        if (noreadmodel.newFeedCount >= 99)
                        {
                            s1 = "N";
                        } else
                        {
                            s1 = (new StringBuilder()).append(noreadmodel.newFeedCount).append("").toString();
                        }
                        textview.setText(s1);
                        textview.setVisibility(0);
                    }
                    if (obj != null)
                    {
                        ((View) (obj)).setVisibility(8);
                    }
                }
            } else
            {
                if (textview != null)
                {
                    textview.setVisibility(8);
                }
                if (obj != null)
                {
                    if (noreadmodel.favoriteAlbumIsUpdate || noreadmodel.newEventCountChanged)
                    {
                        ((View) (obj)).setVisibility(0);
                    } else
                    {
                        ((View) (obj)).setVisibility(8);
                    }
                }
            }
            i1 = noreadmodel.noReadFollowers;
            j1 = noreadmodel.messages;
            k1 = noreadmodel.leters;
            l1 = noreadmodel.newThirdRegisters;
            i1 = noreadmodel.newZoneCommentCount + (i1 + j1 + k1 + l1);
            if (PackageUtil.isMeizu())
            {
                noreadmodel = obj1;
                if (mSmartBarTabs != null)
                {
                    noreadmodel = (TextView)mSmartBarTabs[3].findViewById(0x7f0a06f2);
                }
            } else
            {
                noreadmodel = (TextView)(TextView)mTabHost.getTabWidget().getChildTabViewAt(4).findViewById(0x7f0a06f2);
            }
            if (noreadmodel != null)
            {
                if (i1 > 0)
                {
                    if (PackageUtil.isMeizu())
                    {
                        noreadmodel.setVisibility(8);
                        return;
                    }
                    if (i1 > 99)
                    {
                        obj = "N";
                    } else
                    {
                        obj = (new StringBuilder()).append(i1).append("").toString();
                    }
                    noreadmodel.setText(((CharSequence) (obj)));
                    noreadmodel.setVisibility(0);
                    return;
                } else
                {
                    noreadmodel.setVisibility(8);
                    return;
                }
            }
        }
    }

    public void updateActionInfo()
    {
    }

    public void updateDownloadInfo(int i1)
    {
        if (this == null || isFinishing())
        {
            return;
        } else
        {
            runOnUiThread(new o(this, i1));
            return;
        }
    }

    public void updateIncompleteTaskNum(int i1)
    {
        TextView textview = null;
        View view = null;
        if (ToolUtil.isUseSmartbarAsTab())
        {
            if (mSmartBarTabs != null)
            {
                textview = (TextView)mSmartBarTabs[2].findViewById(0x7f0a06f2);
                view = mSmartBarTabs[2].findViewById(0x7f0a04e6);
            }
        } else
        {
            textview = (TextView)(TextView)mTabHost.getTabWidget().getChildTabViewAt(3).findViewById(0x7f0a06f2);
            view = mTabHost.getTabWidget().getChildTabViewAt(3).findViewById(0x7f0a04e6);
        }
        if (ToolUtil.isUseSmartbarAsTab())
        {
            if (view != null)
            {
                if (i1 > 0)
                {
                    view.setVisibility(0);
                } else
                {
                    view.setVisibility(8);
                }
            }
            if (textview != null)
            {
                textview.setVisibility(8);
            }
        } else
        {
            if (view != null)
            {
                view.setVisibility(8);
            }
            if (textview != null)
            {
                if (i1 > 0)
                {
                    String s1;
                    if (i1 >= 99)
                    {
                        s1 = "N";
                    } else
                    {
                        s1 = (new StringBuilder()).append(i1).append("").toString();
                    }
                    textview.setText(s1);
                    textview.setVisibility(0);
                    return;
                } else
                {
                    textview.setVisibility(8);
                    return;
                }
            }
        }
    }






/*
    static PlayerFragment access$1002(MainTabActivity2 maintabactivity2, PlayerFragment playerfragment)
    {
        maintabactivity2.mPlayerFragment = playerfragment;
        return playerfragment;
    }

*/




/*
    static AppAd access$1202(MainTabActivity2 maintabactivity2, AppAd appad)
    {
        maintabactivity2.mAppAd = appad;
        return appad;
    }

*/











/*
    static long access$2002(MainTabActivity2 maintabactivity2, long l1)
    {
        maintabactivity2.mPlayingRadioStartTime = l1;
        return l1;
    }

*/


/*
    static long access$2114(MainTabActivity2 maintabactivity2, long l1)
    {
        l1 = maintabactivity2.mPlayingRadioDuration + l1;
        maintabactivity2.mPlayingRadioDuration = l1;
        return l1;
    }

*/














/*
    static boolean access$802(MainTabActivity2 maintabactivity2, boolean flag)
    {
        maintabactivity2.mIsEverPlayed = flag;
        return flag;
    }

*/

}
