// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.web;

import android.view.View;
import cn.wemart.sdk.WemartWebView;
import com.ximalaya.ting.android.activity.share.BaseShareDialog;
import com.ximalaya.ting.android.activity.share.SimpleShareData;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.activity.web:
//            WebWemartActivityNew, f

class e
    implements android.view.View.OnClickListener
{

    final WebWemartActivityNew a;

    e(WebWemartActivityNew webwemartactivitynew)
    {
        a = webwemartactivitynew;
        super();
    }

    public void onClick(View view)
    {
        Logger.d("web", a.mWebView.getUrl());
        if (a.mWebView.getUrl() != null && a.mWebView.getUrl().contains("wemart"))
        {
            a.mWebView.b("getShareData", new f(this));
        } else
        if (a.mWebView.getUrl() != null)
        {
            view = new SimpleShareData();
            view.title = a.mWebView.getTitle();
            view.picUrl = MyDeviceManager.getInstance(a.getApplicationContext()).sharePicUrl;
            view.url = a.mWebView.getUrl();
            view.content = (new StringBuilder()).append("\u6211\u6B63\u5728\u770B\u3010").append(a.mWebView.getTitle()).append("\u3011\uFF0C\u5206\u4EAB\u7ED9\u4F60\uFF0C\u4E00\u8D77\u770B\u5427").toString();
            (new BaseShareDialog(a, 19, view)).show();
            return;
        }
    }
}
