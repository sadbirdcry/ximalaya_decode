// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.web;

import android.text.TextUtils;
import cn.wemart.sdk.WemartWebView;
import cn.wemart.sdk.b.b;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.activity.share.BaseShareDialog;
import com.ximalaya.ting.android.activity.share.SimpleShareData;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.share.WemartShareModel;

// Referenced classes of package com.ximalaya.ting.android.activity.web:
//            e, WebWemartActivityNew

class f
    implements b
{

    final e a;

    f(e e1)
    {
        a = e1;
        super();
    }

    public void a(String s)
    {
        SimpleShareData simplesharedata;
        Logger.d("web", (new StringBuilder()).append("\u5206\u4EAB\u6570\u636E\uFF1A").append(s).toString());
        simplesharedata = new SimpleShareData();
        s = (WemartShareModel)JSON.parseObject(s, com/ximalaya/ting/android/model/share/WemartShareModel);
        if (TextUtils.isEmpty(((WemartShareModel) (s)).title)) goto _L2; else goto _L1
_L1:
        simplesharedata.title = ((WemartShareModel) (s)).title;
_L6:
        if (TextUtils.isEmpty(((WemartShareModel) (s)).thumbData)) goto _L4; else goto _L3
_L3:
        simplesharedata.picUrl = ((WemartShareModel) (s)).thumbData;
_L7:
        simplesharedata.url = ((WemartShareModel) (s)).shareUrl;
        if (TextUtils.isEmpty(((WemartShareModel) (s)).content))
        {
            break MISSING_BLOCK_LABEL_215;
        }
        simplesharedata.content = ((WemartShareModel) (s)).content;
          goto _L5
_L2:
        simplesharedata.title = WebWemartActivityNew.access$000(a.a);
          goto _L6
        s;
        simplesharedata.title = a.a.mWebView.getTitle();
        simplesharedata.url = a.a.mWebView.getUrl();
_L5:
        try
        {
            (new BaseShareDialog(a.a, 19, simplesharedata)).show();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
        return;
_L4:
        simplesharedata.picUrl = MyDeviceManager.getInstance(a.a.getApplicationContext()).sharePicUrl;
          goto _L7
        s;
        s.printStackTrace();
        return;
        simplesharedata.content = simplesharedata.title;
          goto _L5
    }
}
