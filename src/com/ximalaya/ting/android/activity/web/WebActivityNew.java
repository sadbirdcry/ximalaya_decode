// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.web;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebView;
import com.ximalaya.ting.android.activity.BaseFragmentActivity;
import com.ximalaya.ting.android.fragment.web.WebFragment;

public class WebActivityNew extends BaseFragmentActivity
{

    private WebFragment webFragment;

    public WebActivityNew()
    {
        webFragment = null;
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        if (webFragment != null)
        {
            webFragment.onActivityResult(i, j, intent);
        }
        super.onActivityResult(i, j, intent);
    }

    public void onBackPressed()
    {
        if (webFragment != null)
        {
            if (!webFragment.onBackPressedFragment())
            {
                super.onBackPressed();
            }
            return;
        } else
        {
            super.onBackPressed();
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        boolean flag = false;
        super.onCreate(bundle);
        setContentView(0x7f030032);
        Object obj1 = getIntent();
        Object obj = null;
        bundle = ((Bundle) (obj));
        if (obj1 != null)
        {
            bundle = ((Bundle) (obj));
            if (((Intent) (obj1)).hasExtra("ExtraUrl"))
            {
                bundle = ((Intent) (obj1)).getStringExtra("ExtraUrl");
            }
        }
        obj = bundle;
        if (TextUtils.isEmpty(bundle))
        {
            Bundle bundle1 = ((Intent) (obj1)).getBundleExtra("BundleExtra");
            obj = bundle;
            if (bundle1 != null)
            {
                obj = bundle1.getString("ExtraUrl");
            }
        }
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            bundle = "www.ximalaya.com";
        } else
        {
            bundle = ((Bundle) (obj));
        }
        if (obj1 != null)
        {
            flag = ((Intent) (obj1)).getBooleanExtra("show_share_btn", false);
        }
        if (obj1 == null)
        {
            obj = "";
        } else
        {
            obj = ((Intent) (obj1)).getStringExtra("share_cover_path");
        }
        webFragment = new WebFragment();
        obj1 = new Bundle();
        ((Bundle) (obj1)).putString("ExtraUrl", bundle);
        ((Bundle) (obj1)).putBoolean("show_share_btn", flag);
        ((Bundle) (obj1)).putString("share_cover_path", ((String) (obj)));
        webFragment.setArguments(((Bundle) (obj1)));
        addFragmentToLayout(0x7f0a0089, webFragment);
    }

    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        intent = intent.getStringExtra("ExtraUrl");
        if (webFragment != null && !TextUtils.isEmpty(intent))
        {
            WebView webview = webFragment.getWebView();
            if (webview != null)
            {
                webview.loadUrl(intent);
            }
        }
    }
}
