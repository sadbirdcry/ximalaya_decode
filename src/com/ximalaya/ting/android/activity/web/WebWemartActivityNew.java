// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.web;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import cn.wemart.sdk.WemartWebView;
import cn.wemart.sdk.b.a;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.activity.web:
//            a, d, e

public class WebWemartActivityNew extends BaseActivity
    implements android.view.View.OnClickListener
{

    public static final String EXTRA_FROM_WEMART = "From_WeMart";
    ImageView closeImg;
    private ImageView mBackPage;
    private ImageView mForwardPage;
    private View mNoNetworkLayout;
    private ProgressBar mProgressBar;
    private ImageView mRefreshPage;
    private ImageView mShareBtn;
    private TextView mTopTv;
    protected WemartWebView mWebView;
    String shareCover;
    boolean showShare;
    private String title;
    private View title_bar;

    public WebWemartActivityNew()
    {
        showShare = true;
    }

    private String checkProtocol(String s)
    {
        String s1 = s;
        if (!s.contains("://"))
        {
            s1 = (new StringBuilder()).append("http://").append(s).toString();
        }
        return s1;
    }

    private void initWemartData()
    {
        Intent intent = getIntent();
        String s1 = null;
        String s = s1;
        if (intent != null)
        {
            s = s1;
            if (intent != null)
            {
                s = s1;
                if (intent.hasExtra("ExtraUrl"))
                {
                    s = intent.getStringExtra("ExtraUrl");
                }
            }
        }
        s1 = s;
        if (TextUtils.isEmpty(s))
        {
            s1 = "www.ximalaya.com";
        }
        s1 = handleURL(s1);
        showShare = true;
        if (intent == null)
        {
            s = "";
        } else
        {
            s = intent.getStringExtra("share_cover_path");
        }
        shareCover = s;
        title_bar.setVisibility(0);
        Logger.d("web", s1);
        loadPage(s1);
        mTopTv.setText("\u559C\u9A6C\u62C9\u96C5\u65B0\u58F0\u6D3B\u5546\u57CE");
        mWebView.a("syncTitle", new com.ximalaya.ting.android.activity.web.a(this));
    }

    private void initWemartListener()
    {
        mBackPage.setOnClickListener(this);
        mForwardPage.setOnClickListener(this);
        mRefreshPage.setOnClickListener(this);
        mNoNetworkLayout.setOnClickListener(new d(this));
        try
        {
            mShareBtn.setOnClickListener(new e(this));
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            return;
        }
        catch (Error error)
        {
            error.printStackTrace();
        }
    }

    private void initWemartViews()
    {
        mWebView = (WemartWebView)findViewById(0x7f0a0096);
        mWebView.a(this);
        mWebView.b(this);
        mBackPage = (ImageView)findViewById(0x7f0a00e2);
        mForwardPage = (ImageView)findViewById(0x7f0a0436);
        mRefreshPage = (ImageView)findViewById(0x7f0a0437);
        mProgressBar = (ProgressBar)findViewById(0x7f0a0093);
        title_bar = findViewById(0x7f0a00de);
        mTopTv = (TextView)findViewById(0x7f0a00ae);
        closeImg = (ImageView)title_bar.findViewById(0x7f0a007b);
        closeImg.setImageResource(0x7f020161);
        closeImg.setOnClickListener(this);
        mShareBtn = (ImageView)findViewById(0x7f0a0710);
        if (showShare && title_bar.getVisibility() == 0)
        {
            mShareBtn.setImageResource(0x7f020537);
            mShareBtn.setVisibility(0);
        }
    }

    private void loadPage(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        } else
        {
            s = checkProtocol(s);
            mWebView.stopLoading();
            mProgressBar.setProgress(0);
            showNoNetworkLayout(false);
            mWebView.loadUrl(s);
            return;
        }
    }

    private void showNoNetworkLayout(boolean flag)
    {
        byte byte0 = 8;
        Object obj = mNoNetworkLayout;
        int i;
        if (flag)
        {
            i = 0;
        } else
        {
            i = 8;
        }
        ((View) (obj)).setVisibility(i);
        obj = mWebView;
        if (flag)
        {
            i = byte0;
        } else
        {
            i = 0;
        }
        ((WemartWebView) (obj)).setVisibility(i);
    }

    protected String handleURL(String s)
    {
        String s1;
        String s2;
        int i = s.lastIndexOf("#");
        s2 = "";
        s1 = s;
        if (i > 0)
        {
            s2 = s.substring(i, s.length());
            s1 = s.substring(0, i);
        }
        if (s1.contains("?")) goto _L2; else goto _L1
_L1:
        s = (new StringBuilder()).append(s1).append("?syncTitle=true").toString();
        s = (new StringBuilder()).append(s).append("&wmode=app").toString();
        s1 = (new StringBuilder()).append(s).append("&supportWXPay=true").toString();
_L4:
        return (new StringBuilder()).append(s1).append(s2).toString();
_L2:
        Map map;
        try
        {
            s = Uri.parse(s1);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s = null;
        }
        if (s != null)
        {
            map = ToolUtil.getQueryMap(s.getQuery());
        } else
        {
            map = null;
        }
        if (map != null && !map.containsKey("syncTitle"))
        {
            s1 = (new StringBuilder()).append(s1).append("&syncTitle=true").toString();
        }
        s = s1;
        if (map != null)
        {
            s = s1;
            if (!map.containsKey("wmode"))
            {
                s = (new StringBuilder()).append(s1).append("&wmode=app").toString();
            }
        }
        s1 = s;
        if (map != null)
        {
            s1 = s;
            if (!map.containsKey("supportWXPay"))
            {
                s1 = (new StringBuilder()).append(s).append("&supportWXPay=true").toString();
            }
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void onBackPressed()
    {
        if (mWebView.canGoBack())
        {
            Logger.d("web", "canGoBack");
            mWebView.goBack();
            return;
        } else
        {
            Logger.d("web", "WebWemartActivityNew Finish");
            finish();
            return;
        }
    }

    public void onClick(View view)
    {
        int i = view.getId();
        if (i != mBackPage.getId()) goto _L2; else goto _L1
_L1:
        if (mWebView.canGoBack())
        {
            mWebView.goBack();
        }
_L4:
        return;
_L2:
        if (i != mForwardPage.getId())
        {
            break; /* Loop/switch isn't completed */
        }
        if (mWebView.canGoForward())
        {
            mWebView.goForward();
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (i == mRefreshPage.getId())
        {
            mWebView.stopLoading();
            mWebView.reload();
            return;
        }
        if (i == closeImg.getId())
        {
            finish();
            return;
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        a.a(3);
        setContentView(0x7f0300fb);
        mNoNetworkLayout = findViewById(0x7f0a02d6);
        bundle = getIntent();
        if (bundle != null && bundle.hasExtra("From_WeMart") && bundle.getBooleanExtra("From_WeMart", false))
        {
            initWemartViews();
            initWemartListener();
            initWemartData();
            ((MyApplication)getApplication()).c = 2;
        }
    }

    public void onDestroy()
    {
        Logger.d("web", "Destroy");
        super.onDestroy();
    }



/*
    static String access$002(WebWemartActivityNew webwemartactivitynew, String s)
    {
        webwemartactivitynew.title = s;
        return s;
    }

*/


}
