// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import cn.com.iresearch.mapptracker.IRMonitor;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.dl.PluginConstants;
import com.ximalaya.ting.android.dl.PluginManager;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MD5;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity:
//            a, b, MainTabActivity2

public class BaseActivity extends Activity
{

    public View homeButton;
    public LoginInfoModel loginInfoModel;
    public Context mActivity;
    public Context mAppContext;
    public ImageView nextButton;
    public ImageView retButton;
    public View rootView;
    public TextView topTextView;
    public View top_bar;

    public BaseActivity()
    {
    }

    public void finish()
    {
        super.finish();
        overridePendingTransition(0, 0);
    }

    public void initCommon()
    {
        if (findViewById(0x7f0a007b) != null)
        {
            retButton = (ImageView)findViewById(0x7f0a007b);
            retButton.setOnClickListener(new com.ximalaya.ting.android.activity.a(this));
        }
        if (findViewById(0x7f0a00ae) != null)
        {
            topTextView = (TextView)findViewById(0x7f0a00ae);
        }
        if (findViewById(0x7f0a0710) != null)
        {
            nextButton = (ImageView)findViewById(0x7f0a0710);
        }
        top_bar = findViewById(0x7f0a0066);
        if (findViewById(0x7f0a070e) != null)
        {
            homeButton = findViewById(0x7f0a070e);
            homeButton.setOnClickListener(new b(this));
        }
    }

    public void onBackPressed()
    {
        super.onBackPressed();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mAppContext = getApplicationContext();
        mActivity = this;
        if (getApplication() instanceof MyApplication)
        {
            ((MyApplication)getApplication()).a((MyApplication)getApplication());
        }
        requestWindowFeature(1);
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        si0urPpa();
    }

    protected void onPause()
    {
        super.onPause();
        TCAgent.onPause(this);
        MobclickAgent.onPause(this);
        IRMonitor.getInstance(this).onPause();
        if (MyApplication.f == this)
        {
            MyApplication.f = null;
        }
    }

    protected void onResume()
    {
        super.onResume();
        MyApplication.f = this;
        TCAgent.onResume(this);
        MobclickAgent.onResume(this);
        if (PluginManager.getInstance(getApplicationContext()).isPluginLoaded(PluginConstants.PLUGIN_APPTRACK))
        {
            IRMonitor.getInstance(this).onResume();
        }
        if (loginInfoModel == null)
        {
            loginInfoModel = UserInfoMannage.getInstance().getUser();
        }
    }

    protected void onStart()
    {
        MyApplication.e();
        super.onStart();
    }

    protected void onStop()
    {
        super.onStop();
        MyApplication.d();
    }

    public void setContentView(int i)
    {
        super.setContentView(i);
        rootView = ((ViewGroup)findViewById(0x1020002)).getChildAt(0);
        Intent intent = getIntent();
        if (intent != null)
        {
            DataCollectUtil.bindDataToView(intent.getStringExtra("xdcs_data_bundle"), rootView);
        }
    }

    public void setTitleText(String s)
    {
        if (topTextView != null)
        {
            topTextView.setText(s);
        }
    }

    public void showToast(String s)
    {
        if (this != null && !isFinishing())
        {
            CustomToast.showToast(this, s, 0);
        }
    }

    public void showToastTest(String s)
    {
    }

    public void si0urPpa()
    {
        if (!MD5.md5(getPackageName()).equals(ToolUtil.getPackageMD5()))
        {
            throw new RuntimeException((new StringBuilder()).append("t").append(MD5.md5("second package")).append("t").toString());
        }
        if (!ToolUtil.getSingInfoMd5(this).equals(a.p))
        {
            throw new RuntimeException((new StringBuilder()).append("t").append(MD5.md5("second sign")).append("t").toString());
        }
        boolean flag;
        if ((getApplicationInfo().flags & 2) != 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (flag)
        {
            throw new RuntimeException((new StringBuilder()).append("t").append(MD5.md5("smali debug")).append("t").toString());
        } else
        {
            return;
        }
    }

    public void startActivity(Intent intent)
    {
        intent.setFlags(0x4000000);
        super.startActivity(intent);
    }

    public void startActivityForResult(Intent intent, int i)
    {
        intent.setFlags(0x4000000);
        super.startActivityForResult(intent, i);
    }

    public void startFragment(Class class1, Bundle bundle)
    {
        Intent intent = new Intent(this, com/ximalaya/ting/android/activity/MainTabActivity2);
        intent.addFlags(0x4000000);
        intent.putExtra(a.s, bundle);
        intent.putExtra(a.t, class1);
        startActivity(intent);
    }
}
