// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import cn.com.iresearch.mapptracker.IRMonitor;
import com.tendcloud.tenddata.TCAgent;
import com.umeng.analytics.MobclickAgent;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.login.WelcomeActivity;
import com.ximalaya.ting.android.dl.PluginConstants;
import com.ximalaya.ting.android.dl.PluginManager;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.CustomToast;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MD5;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity:
//            c, d, MainTabActivity2

public class BaseFragmentActivity extends FragmentActivity
{

    public ImageView homeButton;
    public LoginInfoModel loginInfoModel;
    protected Context mActivity;
    protected Context mAppContext;
    private Handler mHandler;
    public ImageView nextButton;
    public ImageView retButton;
    public View rootView;
    public TextView topTextView;
    public View top_bar;

    public BaseFragmentActivity()
    {
        mHandler = new Handler();
    }

    private boolean isInitApp()
    {
        return !a.b || SharedPreferencesUtil.getInstance(mAppContext).getBoolean("P_IS_SURE_NO_3G_DIALOG_NOTIFY") || !(this instanceof WelcomeActivity);
    }

    protected void addFragmentToLayout(int i, Fragment fragment)
    {
        addFragmentToLayout(i, fragment, 0, 0);
    }

    protected void addFragmentToLayout(int i, Fragment fragment, int j, int k)
    {
        if (fragment != null)
        {
            FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();
            if (j != 0 && k != 0)
            {
                fragmenttransaction.setCustomAnimations(j, k, j, k);
            }
            fragmenttransaction.add(i, fragment);
            fragmenttransaction.commitAllowingStateLoss();
            if (PackageUtil.isPostHB())
            {
                invalidateOptionsMenu();
            }
        }
    }

    public void finish()
    {
        super.finish();
        overridePendingTransition(0, 0);
    }

    protected void hideFragment(Fragment fragment)
    {
        hideFragment(fragment, 0, 0);
    }

    protected void hideFragment(Fragment fragment, int i, int j)
    {
        if (fragment == null || !fragment.isAdded() || fragment.isHidden())
        {
            return;
        }
        FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();
        if (i != 0 && j != 0)
        {
            fragmenttransaction.setCustomAnimations(i, j, i, j);
        }
        fragmenttransaction.hide(fragment);
        fragmenttransaction.commitAllowingStateLoss();
    }

    public void initCommon()
    {
        retButton = (ImageView)findViewById(0x7f0a007b);
        if (retButton != null)
        {
            retButton.setOnClickListener(new c(this));
        }
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        top_bar = findViewById(0x7f0a0066);
        if (findViewById(0x7f0a070e) != null)
        {
            homeButton = (ImageView)findViewById(0x7f0a070e);
            homeButton.setOnClickListener(new d(this));
        }
    }

    public void onBackPressed()
    {
        finish();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        mAppContext = getApplicationContext();
        mActivity = this;
        if (getApplication() instanceof MyApplication)
        {
            ((MyApplication)getApplication()).f();
            if (isInitApp())
            {
                ((MyApplication)getApplication()).a((MyApplication)getApplication());
            }
        }
        mActivity = this;
        si0urPpa();
    }

    protected void onPause()
    {
        super.onPause();
        if (isInitApp())
        {
            MobclickAgent.onPause(this);
            if (PluginManager.getInstance(getApplicationContext()).isPluginLoaded(PluginConstants.PLUGIN_APPTRACK))
            {
                IRMonitor.getInstance(this).onPause();
            }
            if (this instanceof MainTabActivity2)
            {
                TCAgent.onPause(this);
            }
        }
        if (MyApplication.f == this)
        {
            MyApplication.f = null;
        }
    }

    protected void onResume()
    {
        super.onResume();
        MyApplication.f = this;
        if (isInitApp())
        {
            MobclickAgent.onResume(this);
            if (PluginManager.getInstance(getApplicationContext()).isPluginLoaded(PluginConstants.PLUGIN_APPTRACK))
            {
                try
                {
                    IRMonitor.getInstance(this).onResume();
                }
                catch (NoClassDefFoundError noclassdeffounderror)
                {
                    noclassdeffounderror.printStackTrace();
                }
            }
            if (this instanceof MainTabActivity2)
            {
                TCAgent.onResume(this);
            }
        }
    }

    protected void onStart()
    {
        MyApplication.e();
        super.onStart();
    }

    protected void onStop()
    {
        super.onStop();
        MyApplication.d();
    }

    public void setContentView(int i)
    {
        super.setContentView(i);
        rootView = ((ViewGroup)findViewById(0x1020002)).getChildAt(0);
        Intent intent = getIntent();
        if (intent != null)
        {
            DataCollectUtil.bindDataToView(intent.getStringExtra("xdcs_data_bundle"), rootView);
        }
    }

    public void setTitleText(String s)
    {
        if (topTextView != null)
        {
            topTextView.setText(s);
        }
    }

    protected void showFragment(Fragment fragment)
    {
        showFragment(fragment, 0, 0);
    }

    protected void showFragment(Fragment fragment, int i, int j)
    {
        if (fragment == null || !fragment.isAdded())
        {
            return;
        }
        FragmentTransaction fragmenttransaction = getSupportFragmentManager().beginTransaction();
        if (i != 0 && j != 0)
        {
            fragmenttransaction.setCustomAnimations(i, j, i, j);
        }
        fragmenttransaction.show(fragment);
        fragmenttransaction.commitAllowingStateLoss();
    }

    public void showToast(String s)
    {
        if (this != null && !isFinishing())
        {
            CustomToast.showToast(this, s, 0);
        }
    }

    public void si0urPpa()
    {
        if (!MD5.md5(getPackageName()).equals(ToolUtil.getPackageMD5()))
        {
            throw new RuntimeException((new StringBuilder()).append("t").append(MD5.md5("second package")).append("t").toString());
        }
        if (!ToolUtil.getSingInfoMd5(this).equals(a.p))
        {
            throw new RuntimeException((new StringBuilder()).append("t").append(MD5.md5("second sign")).append("t").toString());
        }
        boolean flag;
        if ((getApplicationInfo().flags & 2) != 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (flag)
        {
            throw new RuntimeException((new StringBuilder()).append("t").append(MD5.md5("smali debug")).append("t").toString());
        } else
        {
            return;
        }
    }

    public void startActivity(Intent intent)
    {
        intent.setFlags(0x4000000);
        super.startActivity(intent);
    }

    public void startActivityForResult(Intent intent, int i)
    {
        intent.setFlags(0x4000000);
        super.startActivityForResult(intent, i);
    }
}
