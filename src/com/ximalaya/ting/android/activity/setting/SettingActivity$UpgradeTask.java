// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.os.Handler;
import android.os.Message;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.check_version.CheckVersionResult;
import com.ximalaya.ting.android.model.check_version.MsgCarry;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            SettingActivity

public class this._cls0 extends MyAsyncTask
{

    final SettingActivity this$0;

    protected transient CheckVersionResult doInBackground(Void avoid[])
    {
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/version").toString();
        RequestParams requestparams = new RequestParams();
        avoid = f.a().a(avoid, requestparams, null, null);
        if (avoid == null)
        {
            break MISSING_BLOCK_LABEL_67;
        }
        int i;
        avoid = (CheckVersionResult)JSON.parseObject(avoid, com/ximalaya/ting/android/model/check_version/CheckVersionResult);
        i = ((CheckVersionResult) (avoid)).ret;
        if (i == 0)
        {
            return avoid;
        }
        break MISSING_BLOCK_LABEL_67;
        avoid;
        return null;
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(CheckVersionResult checkversionresult)
    {
        if (this == null || isFinishing())
        {
            return;
        }
        Message message = new Message();
        if (checkversionresult != null && checkversionresult.version != null && !"".equalsIgnoreCase(checkversionresult.version))
        {
            MsgCarry msgcarry;
            if (checkversionresult.forceUpdate)
            {
                message.what = 1;
            } else
            {
                message.what = 0;
            }
            msgcarry = new MsgCarry(checkversionresult.version, checkversionresult.download, 0);
            msgcarry.setInfo(checkversionresult.upgradeDesc);
            message.obj = msgcarry;
        } else
        {
            message.what = 163;
        }
        uphandler.sendMessage(message);
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((CheckVersionResult)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
    }

    public ()
    {
        this$0 = SettingActivity.this;
        super();
    }
}
