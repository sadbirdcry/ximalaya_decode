// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.adapter.setting.PushSettingAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.setting.PushSettingInfo;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.setting.CornerListView;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import com.ximalaya.ting.android.view.wheel.NumericWheelAdapter;
import com.ximalaya.ting.android.view.wheel.WheelView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            ah, ai, aj, ak, 
//            al, am, an, ao

public class PushSettingActivity extends BaseActivity
{
    class a extends MyAsyncTask
    {

        final PushSettingActivity a;

        protected transient PushSettingInfo a(Void avoid[])
        {
            avoid = null;
            if (ToolUtil.isConnectToNetwork(a.mCt))
            {
                String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/apn/get").toString();
                RequestParams requestparams = new RequestParams();
                s = f.a().a(s, requestparams, a.rootView, a.rootView);
                if (s != null)
                {
                    try
                    {
                        if (JSON.parseObject(s).getInteger("ret").intValue() == 0)
                        {
                            avoid = (PushSettingInfo)JSON.parseObject(s, com/ximalaya/ting/android/model/setting/PushSettingInfo);
                        }
                    }
                    // Misplaced declaration of an exception variable
                    catch (Void avoid[])
                    {
                        avoid = new PushSettingInfo();
                        avoid.ret = -1;
                        avoid.msg = "";
                        return avoid;
                    }
                    return avoid;
                } else
                {
                    avoid = new PushSettingInfo();
                    avoid.ret = -1;
                    avoid.msg = "";
                    return avoid;
                }
            } else
            {
                avoid = new PushSettingInfo();
                avoid.ret = -1;
                avoid.msg = "";
                return avoid;
            }
        }

        protected void a(PushSettingInfo pushsettinginfo)
        {
            if (a == null || a.isFinishing())
            {
                return;
            } else
            {
                a.pushSettingInfo = pushsettinginfo;
                a.initUI();
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((PushSettingInfo)obj);
        }

        a()
        {
            a = PushSettingActivity.this;
            super();
        }
    }

    class b extends MyAsyncTask
    {

        PushSettingInfo a;
        final PushSettingActivity b;

        protected transient Void a(Object aobj[])
        {
            a = (PushSettingInfo)aobj[0];
            if (a == null)
            {
                return null;
            } else
            {
                aobj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/apn/set").toString();
                RequestParams requestparams = new RequestParams();
                requestparams.put("noPushStartHour", "0");
                requestparams.put("noPushEndHour", "0");
                requestparams.put("isPushNewFollower", String.valueOf(a.isPushNewFollower()));
                requestparams.put("isPushNewComment", String.valueOf(a.isPushNewComment()));
                requestparams.put("isPushNewMessage", String.valueOf(a.isPushNewMessage()));
                requestparams.put("isPushNewQuan", String.valueOf(a.isPushNewQuan()));
                requestparams.put("isPushZoneComment", String.valueOf(a.isPushZoneComment()));
                f.a().b(((String) (aobj)), requestparams, b.rootView, null);
                return null;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        b()
        {
            b = PushSettingActivity.this;
            super();
        }
    }


    private LinearLayout all_set_ll;
    private CornerListView cornerListView;
    private ImageButton doneButton;
    private int durtion[];
    private WheelView durtionTime;
    private boolean isPush;
    private List list;
    private Context mCt;
    private TextView pushDurtionTime;
    private PushSettingAdapter pushSettingAdapter;
    private PushSettingInfo pushSettingInfo;
    private SwitchButton pushStopSlipSwitch;
    private RelativeLayout pushStopTime;
    private SwitchButton pushSwitch;
    private TextView pushtime;
    private WheelView startTime;
    private View timeView;
    private PopupWindow timeWindow;

    public PushSettingActivity()
    {
        isPush = false;
        durtion = new int[2];
        pushSettingInfo = null;
    }

    private void addChangingListener(WheelView wheelview, int i)
    {
        wheelview.addChangingListener(new ah(this, i));
    }

    private void findViews()
    {
        all_set_ll = (LinearLayout)findViewById(0x7f0a064d);
        pushSwitch = (SwitchButton)findViewById(0x7f0a064c);
        pushStopSlipSwitch = (SwitchButton)findViewById(0x7f0a0650);
        retButton = (ImageView)findViewById(0x7f0a007b);
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
    }

    private int[] getPushDurtion()
    {
        int ai1[] = new int[2];
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCt);
        ai1[0] = sharedpreferencesutil.getInt("start", 0);
        ai1[1] = sharedpreferencesutil.getInt("end", 0);
        if (ai1[0] == 0 && ai1[1] == 0)
        {
            sharedpreferencesutil.saveInt("start", 22);
            sharedpreferencesutil.saveInt("end", 8);
            ai1[0] = 22;
            ai1[1] = 8;
        }
        return ai1;
    }

    private String getTimeViewText(int i, int j)
    {
        if (i + j < 24)
        {
            return (new StringBuilder()).append("\u6BCF\u65E5 ").append(String.format("%02d", new Object[] {
                Integer.valueOf(i)
            })).append(":00-").append(String.format("%02d", new Object[] {
                Integer.valueOf(i + j)
            })).append(":00").toString();
        } else
        {
            return (new StringBuilder()).append("\u6BCF\u65E5 ").append(String.format("%02d", new Object[] {
                Integer.valueOf(i)
            })).append(":00-").append("\u6B21\u65E5 ").append(String.format("%02d", new Object[] {
                Integer.valueOf((i + j) - 24)
            })).append(":00").toString();
        }
    }

    private void initData()
    {
        mCt = getApplicationContext();
        isPush = isPush();
        durtion = getPushDurtion();
    }

    private void initUI()
    {
        pushStopTime = (RelativeLayout)findViewById(0x7f0a064e);
        pushStopTime.setOnClickListener(new ai(this));
        pushtime = (TextView)findViewById(0x7f0a064f);
        if (isPush)
        {
            pushtime.setText(getTimeViewText(durtion[0], durtion[1]));
        }
        pushStopSlipSwitch.setChecked(isPush);
        pushStopSlipSwitch.setOnCheckedChangeListener(new aj(this));
        pushSwitch.setOnCheckedChangeListener(new ak(this));
        cornerListView = (CornerListView)findViewById(0x7f0a0651);
        String as[] = getResources().getStringArray(0x7f0c0023);
        boolean aflag[] = isSetting();
        list = new ArrayList();
        for (int i = 0; i < aflag.length; i++)
        {
            if (i == 0)
            {
                as[i] = (new StringBuilder()).append("@").append(as[i]).toString();
            }
            SettingInfo settinginfo = new SettingInfo(as[i], aflag[i]);
            list.add(settinginfo);
        }

        pushSettingAdapter = new PushSettingAdapter(this, list);
        cornerListView.setAdapter(pushSettingAdapter);
    }

    private void initViews()
    {
        topTextView.setText(getString(0x7f090146));
        boolean flag = SharedPreferencesUtil.getInstance(getApplicationContext()).getBoolean("is_push_all", true);
        pushSwitch.setChecked(flag);
        android.widget.RelativeLayout.LayoutParams layoutparams;
        if (flag)
        {
            pushAllOn();
        } else
        {
            pushAllOff();
        }
        nextButton.setImageResource(0x7f02006a);
        layoutparams = (android.widget.RelativeLayout.LayoutParams)nextButton.getLayoutParams();
        layoutparams.rightMargin = ToolUtil.dp2px(this, 10F);
        nextButton.setLayoutParams(layoutparams);
        nextButton.setOnClickListener(new al(this));
        retButton.setOnClickListener(new am(this));
    }

    private boolean isPush()
    {
        return SharedPreferencesUtil.getInstance(mCt).getBoolean("isPush", true);
    }

    private boolean[] isSetting()
    {
        boolean aflag[];
        byte byte0;
        if (com.ximalaya.ting.android.a.o)
        {
            byte0 = 5;
        } else
        {
            byte0 = 4;
        }
        aflag = new boolean[byte0];
        if (pushSettingInfo != null && pushSettingInfo.ret == 0)
        {
            aflag[0] = pushSettingInfo.isPushNewQuan();
            saveIsPush(pushSettingInfo.isPushNewQuan(), "noticeMe");
            aflag[1] = pushSettingInfo.isPushNewFollower();
            saveIsPush(pushSettingInfo.isPushNewFollower(), "newFans");
            aflag[2] = pushSettingInfo.isPushNewComment();
            saveIsPush(pushSettingInfo.isPushNewComment(), "newComment");
            aflag[3] = pushSettingInfo.isPushNewMessage();
            saveIsPush(pushSettingInfo.isPushNewMessage(), "newTr");
            if (com.ximalaya.ting.android.a.o)
            {
                aflag[4] = pushSettingInfo.isPushZoneComment();
                saveIsPush(pushSettingInfo.isPushZoneComment(), "newZoneComment");
            }
        } else
        {
            SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCt);
            aflag[0] = sharedpreferencesutil.getBoolean("noticeMe", true);
            aflag[1] = sharedpreferencesutil.getBoolean("newFans", true);
            aflag[2] = sharedpreferencesutil.getBoolean("newComment", true);
            aflag[3] = sharedpreferencesutil.getBoolean("newTr", true);
            if (com.ximalaya.ting.android.a.o)
            {
                aflag[4] = sharedpreferencesutil.getBoolean("newZoneComment", true);
                return aflag;
            }
        }
        return aflag;
    }

    private void pushAllOff()
    {
        all_set_ll.setVisibility(8);
    }

    private void pushAllOn()
    {
        all_set_ll.setVisibility(0);
    }

    private int[] returnStartAndEnd()
    {
        return (new int[] {
            startTime.getCurrentItem(), durtionTime.getCurrentItem() + 1
        });
    }

    private void saveIsPush(boolean flag, String s)
    {
        (new an(this, s, flag)).start();
    }

    private void savePushDurtion(int i, int j)
    {
        durtion[0] = i;
        durtion[1] = j;
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCt);
        sharedpreferencesutil.saveInt("start", i);
        sharedpreferencesutil.saveInt("end", j);
    }

    private void showTimeWindow()
    {
        if (timeView == null)
        {
            timeView = ((LayoutInflater)getSystemService("layout_inflater")).inflate(0x7f03019f, (ViewGroup)findViewById(0x7f0a0056), false);
            startTime = (WheelView)timeView.findViewById(0x7f0a0656);
            startTime.setAdapter(new NumericWheelAdapter(0, 23, "00:00"));
            durtionTime = (WheelView)timeView.findViewById(0x7f0a0657);
            durtionTime.setAdapter(new NumericWheelAdapter(1, 24, "hour"));
            addChangingListener(startTime, 0);
            addChangingListener(durtionTime, 1);
            pushDurtionTime = (TextView)timeView.findViewById(0x7f0a0654);
            doneButton = (ImageButton)timeView.findViewById(0x7f0a0655);
            doneButton.setOnClickListener(new ao(this));
        }
        startTime.setCurrentItem(durtion[0]);
        durtionTime.setCurrentItem(durtion[1] - 1);
        pushDurtionTime.setText(pushtime.getText());
        if (timeWindow == null)
        {
            timeWindow = new PopupWindow(timeView, -1, -2);
            timeWindow.setAnimationStyle(0x7f0b0012);
            timeWindow.setBackgroundDrawable(new BitmapDrawable());
            timeWindow.setFocusable(true);
            timeWindow.setOutsideTouchable(true);
        }
        timeWindow.showAtLocation(timeView, 80, 0, 0);
    }

    public void finish()
    {
        savePushDurtion(durtion[0], durtion[1]);
        (new b()).myexec(new Object[] {
            pushSettingInfo
        });
        super.finish();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03019d);
        initData();
        findViews();
        initViews();
        (new a()).myexec(new Void[0]);
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            finish();
            return true;
        } else
        {
            return super.onKeyDown(i, keyevent);
        }
    }

    public void saveSwitched(boolean flag, int i)
    {
        switch (i)
        {
        default:
            return;

        case 0: // '\0'
            pushSettingInfo.setPushNewQuan(flag);
            saveIsPush(flag, "noticeMe");
            return;

        case 1: // '\001'
            pushSettingInfo.setPushNewFollower(flag);
            saveIsPush(flag, "newFans");
            return;

        case 2: // '\002'
            pushSettingInfo.setPushNewComment(flag);
            saveIsPush(flag, "newComment");
            return;

        case 3: // '\003'
            pushSettingInfo.setPushNewMessage(flag);
            saveIsPush(flag, "newTr");
            return;

        case 4: // '\004'
            pushSettingInfo.setPushZoneComment(flag);
            break;
        }
        saveIsPush(flag, "newZoneComment");
    }




/*
    static int[] access$1002(PushSettingActivity pushsettingactivity, int ai1[])
    {
        pushsettingactivity.durtion = ai1;
        return ai1;
    }

*/


/*
    static PushSettingInfo access$102(PushSettingActivity pushsettingactivity, PushSettingInfo pushsettinginfo)
    {
        pushsettingactivity.pushSettingInfo = pushsettinginfo;
        return pushsettinginfo;
    }

*/















/*
    static boolean access$702(PushSettingActivity pushsettingactivity, boolean flag)
    {
        pushsettingactivity.isPush = flag;
        return flag;
    }

*/


}
