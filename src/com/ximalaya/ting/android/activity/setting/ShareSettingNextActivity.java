// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.model.setting.ShareSettingModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.setting.CornerListView;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            az, ba, bb, bc

public class ShareSettingNextActivity extends BaseActivity
{
    class a extends MyAsyncTask
    {

        ProgressDialog a;
        final ShareSettingNextActivity b;

        protected transient String a(Void avoid[])
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/sync/get").toString();
            RequestParams requestparams = new RequestParams();
            avoid = f.a().a(avoid, requestparams, b.cornerListView, b.cornerListView);
            if (avoid != null)
            {
                JSONObject jsonobject = JSON.parseObject(avoid);
                if (jsonobject.getInteger("ret").intValue() == 0)
                {
                    try
                    {
                        b.parseData(avoid);
                        SharedPreferencesUtil.getInstance(b.mCt).saveString("share_setting", avoid);
                    }
                    // Misplaced declaration of an exception variable
                    catch (Void avoid[])
                    {
                        return "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
                    }
                    return "0";
                } else
                {
                    return jsonobject.getString("msg");
                }
            } else
            {
                return "\u7F51\u7EDC\u8BBF\u95EE\u5F02\u5E38";
            }
        }

        protected void a(String s)
        {
            if (a != null)
            {
                a.cancel();
                a = null;
            }
            if (b == null || b.isFinishing())
            {
                return;
            }
            if (!"0".equals(s))
            {
                s = SharedPreferencesUtil.getInstance(b.mCt).getString("share_setting");
                b.parseData(s);
            }
            b.refreshData(b.type);
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((String)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            a = new MyProgressDialog(b);
            a.setMessage("\u6B63\u5728\u52A0\u8F7D\u6570\u636E\uFF0C\u8BF7\u7B49\u5F85...");
            a.show();
        }

        a()
        {
            b = ShareSettingNextActivity.this;
            super();
            a = null;
        }
    }

    class b extends MyAsyncTask
    {

        String a;
        boolean b;
        final ShareSettingNextActivity c;

        protected transient String a(Object aobj[])
        {
            String s;
            RequestParams requestparams;
            a = (String)aobj[0];
            b = ((Boolean)aobj[1]).booleanValue();
            aobj = (View)aobj[2];
            s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/sync/").append(c.type).append("/set").toString();
            requestparams = new RequestParams();
            requestparams.put("type", a);
            requestparams.put("isChecked", String.valueOf(b));
            c.type;
            JVM INSTR tableswitch 1 3: default 132
        //                       1 197
        //                       2 208
        //                       3 261;
               goto _L1 _L2 _L3 _L4
_L1:
            break; /* Loop/switch isn't completed */
_L4:
            break MISSING_BLOCK_LABEL_261;
_L5:
            aobj = f.a().b(s, requestparams, ((View) (aobj)), ((View) (aobj)));
            Logger.d("resultJson:", ((String) (aobj)));
            if (aobj != null)
            {
                JSONObject jsonobject = JSON.parseObject(((String) (aobj)));
                if (jsonobject.getInteger("ret").intValue() == 0)
                {
                    try
                    {
                        SharedPreferencesUtil.getInstance(c.mCt).saveString("share_setting", ((String) (aobj)));
                        c.parseData(((String) (aobj)));
                    }
                    // Misplaced declaration of an exception variable
                    catch (Object aobj[])
                    {
                        return "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
                    }
                    return "0";
                } else
                {
                    return jsonobject.getString("msg");
                }
            } else
            {
                return "\u7F51\u7EDC\u8BBF\u95EE\u5F02\u5E38";
            }
_L2:
            requestparams.put("thirdpartyName", "tSina");
              goto _L5
_L3:
            switch (c.subType)
            {
            case 1: // '\001'
                requestparams.put("thirdpartyName", "tQQ");
                break;

            case 2: // '\002'
                requestparams.put("thirdpartyName", "qzone");
                break;
            }
            if (true) goto _L5; else goto _L6
_L6:
            requestparams.put("thirdpartyName", "renren");
              goto _L5
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        b()
        {
            c = ShareSettingNextActivity.this;
            super();
        }
    }

    class c extends BaseAdapter
    {

        final ShareSettingNextActivity a;
        private LayoutInflater b;

        public int getCount()
        {
            return a.list.size();
        }

        public Object getItem(int i)
        {
            return a.list.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            SettingInfo settinginfo;
            if (view == null)
            {
                viewgroup = new a(this);
                view = b.inflate(0x7f03019e, null);
                viewgroup.a = (TextView)view.findViewById(0x7f0a0652);
                viewgroup.b = (SwitchButton)view.findViewById(0x7f0a0653);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (a)view.getTag();
            }
            settinginfo = (SettingInfo)getItem(i);
            if (settinginfo.getNameWake() != null)
            {
                ((a) (viewgroup)).a.setText(settinginfo.getNameWake());
                ((a) (viewgroup)).b.initCheckedState(settinginfo.isSetting());
                ((a) (viewgroup)).b.setOnCheckedChangeListener(new bc(this, i));
            }
            return view;
        }

        public c(Activity activity)
        {
            a = ShareSettingNextActivity.this;
            super();
            b = LayoutInflater.from(activity);
        }
    }

    class c.a
    {

        TextView a;
        SwitchButton b;
        final c c;

        c.a(c c1)
        {
            c = c1;
            super();
        }
    }


    private CornerListView cornerListView;
    private List list;
    Context mCt;
    private ImageView nextImg;
    private c pushSettingAdapter;
    private RadioButton qqWeiboBtn;
    private RadioButton qzoneBtn;
    private RadioGroup radioGroup4;
    private ImageView retButton;
    private ImageView retButton4;
    private List shareList;
    private String str[];
    private int subType;
    private RelativeLayout titleBar;
    private LinearLayout titleBar4;
    public TextView topTextView;
    private int type;

    public ShareSettingNextActivity()
    {
        type = 0;
        subType = 0;
        shareList = null;
    }

    private void findViews()
    {
        titleBar = (RelativeLayout)findViewById(0x7f0a0066);
        retButton = (ImageView)findViewById(0x7f0a007b);
        nextImg = (ImageView)findViewById(0x7f0a0710);
        topTextView = (TextView)findViewById(0x7f0a00ae);
        titleBar4 = (LinearLayout)findViewById(0x7f0a0711);
        retButton4 = (ImageView)findViewById(0x7f0a0697);
        radioGroup4 = (RadioGroup)findViewById(0x7f0a0698);
        qqWeiboBtn = (RadioButton)findViewById(0x7f0a0712);
        qzoneBtn = (RadioButton)findViewById(0x7f0a0713);
        cornerListView = (CornerListView)findViewById(0x7f0a06d3);
    }

    private void getParentInfo()
    {
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        type = getIntent().getIntExtra("SettingFlag", 0);
        str = getResources().getStringArray(0x7f0c0024);
        switch (type)
        {
        default:
            return;

        case 1: // '\001'
            titleBar.setVisibility(0);
            nextImg.setVisibility(4);
            topTextView.setText("\u65B0\u6D6A\u5FAE\u535A\u5206\u4EAB\u8BBE\u7F6E");
            titleBar4.setVisibility(8);
            return;

        case 2: // '\002'
            subType = 1;
            qqWeiboBtn.setChecked(true);
            titleBar4.setVisibility(0);
            titleBar.setVisibility(8);
            return;

        case 3: // '\003'
            titleBar.setVisibility(0);
            break;
        }
        nextImg.setVisibility(4);
        topTextView.setText("\u4EBA\u4EBA\u7F51\u5206\u4EAB\u8BBE\u7F6E");
        titleBar4.setVisibility(8);
    }

    private void initData()
    {
        pushSettingAdapter = new c(this);
        cornerListView.setAdapter(pushSettingAdapter);
    }

    private void initListeners()
    {
        if (retButton != null)
        {
            retButton.setOnClickListener(new az(this));
        }
        if (retButton4 != null)
        {
            retButton4.setOnClickListener(new ba(this));
        }
        if (radioGroup4 != null)
        {
            radioGroup4.setOnCheckedChangeListener(new bb(this));
        }
    }

    private void makeAdapterData(ShareSettingModel sharesettingmodel)
    {
        int i = 0;
_L7:
        SettingInfo settinginfo;
        if (i >= str.length)
        {
            break MISSING_BLOCK_LABEL_122;
        }
        settinginfo = new SettingInfo();
        settinginfo.setNameWake(str[i]);
        i;
        JVM INSTR tableswitch 0 3: default 60
    //                   0 78
    //                   1 89
    //                   2 100
    //                   3 111;
           goto _L1 _L2 _L3 _L4 _L5
_L5:
        break MISSING_BLOCK_LABEL_111;
_L1:
        break; /* Loop/switch isn't completed */
_L2:
        break; /* Loop/switch isn't completed */
_L8:
        list.add(settinginfo);
        i++;
        if (true) goto _L7; else goto _L6
_L6:
        settinginfo.setSetting(sharesettingmodel.webTrack);
          goto _L8
_L3:
        settinginfo.setSetting(sharesettingmodel.webFavorite);
          goto _L8
_L4:
        settinginfo.setSetting(sharesettingmodel.webComment);
          goto _L8
        settinginfo.setSetting(sharesettingmodel.relay);
          goto _L8
    }

    private void parseData(String s)
    {
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_26;
        }
        shareList = JSON.parseArray(JSON.parseObject(s).get("data").toString(), com/ximalaya/ting/android/model/setting/ShareSettingModel);
        return;
        s;
    }

    private void refreshData(int i)
    {
        if (shareList == null || shareList.size() <= 0)
        {
            return;
        }
        list.clear();
        if (i != 2) goto _L2; else goto _L1
_L1:
        Iterator iterator = shareList.iterator();
_L4:
        ShareSettingModel sharesettingmodel;
        if (iterator.hasNext())
        {
            sharesettingmodel = (ShareSettingModel)iterator.next();
            continue;
        }
        break; /* Loop/switch isn't completed */
_L8:
        do
        {
            if (sharesettingmodel != null)
            {
                makeAdapterData(sharesettingmodel);
            }
            pushSettingAdapter.notifyDataSetChanged();
            return;
        } while (sharesettingmodel.thirdpartyName.equals("qzone") && radioGroup4.getCheckedRadioButtonId() == 0x7f0a0713 || sharesettingmodel.thirdpartyName.equals("tQQ") && radioGroup4.getCheckedRadioButtonId() == 0x7f0a0712);
        if (true) goto _L4; else goto _L3
_L2:
        if (i != 1) goto _L6; else goto _L5
_L5:
        iterator = shareList.iterator();
_L9:
        if (!iterator.hasNext()) goto _L3; else goto _L7
_L7:
        sharesettingmodel = (ShareSettingModel)iterator.next();
        if (!sharesettingmodel.thirdpartyName.equals("tSina")) goto _L9; else goto _L8
_L6:
        if (i != 3) goto _L3; else goto _L10
_L10:
        iterator = shareList.iterator();
_L12:
        if (!iterator.hasNext()) goto _L3; else goto _L11
_L11:
        sharesettingmodel = (ShareSettingModel)iterator.next();
        if (!sharesettingmodel.thirdpartyName.equals("renren")) goto _L12; else goto _L8
_L3:
        sharesettingmodel = null;
          goto _L8
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301c8);
        mCt = getApplicationContext();
        list = new ArrayList();
        findViews();
        initListeners();
        initData();
        getParentInfo();
        (new a()).myexec(new Void[0]);
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            finish();
            return true;
        } else
        {
            return super.onKeyDown(i, keyevent);
        }
    }







/*
    static int access$402(ShareSettingNextActivity sharesettingnextactivity, int i)
    {
        sharesettingnextactivity.subType = i;
        return i;
    }

*/



}
