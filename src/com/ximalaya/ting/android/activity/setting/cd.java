// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.DialogInterface;
import android.content.Intent;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.livefm.LivePlayerFragment;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.service.play.LocalMediaService;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            WakeUpSettingActivity

class cd
    implements android.content.DialogInterface.OnClickListener
{

    final WakeUpSettingActivity a;

    cd(WakeUpSettingActivity wakeupsettingactivity)
    {
        a = wakeupsettingactivity;
        super();
    }

    public void onClick(DialogInterface dialoginterface, int i)
    {
        if (WakeUpSettingActivity.access$600(a) != null)
        {
            WakeUpSettingActivity.access$1200(a);
            a.finish();
            return;
        }
        if (LocalMediaService.getInstance() != null)
        {
            LocalMediaService.getInstance().setMediaPlayerErrorHandler(null);
        }
        dialoginterface = new Intent(a, com/ximalaya/ting/android/activity/MainTabActivity2);
        if (SharedPreferencesUtil.getInstance(WakeUpSettingActivity.access$000(a)).getInt("type", -1) == 3)
        {
            dialoginterface.putExtra(a.t, com/ximalaya/ting/android/fragment/livefm/LivePlayerFragment);
        } else
        {
            dialoginterface.putExtra(a.t, com/ximalaya/ting/android/fragment/play/PlayerFragment);
        }
        dialoginterface.setFlags(0x4000000);
        a.startActivity(dialoginterface);
    }
}
