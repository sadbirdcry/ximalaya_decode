// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.model.alarm.Alarms;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.LiveHistoryManage;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            LiveRingtoneActivity

class v
    implements android.widget.AdapterView.OnItemClickListener
{

    final LiveRingtoneActivity a;

    v(LiveRingtoneActivity liveringtoneactivity)
    {
        a = liveringtoneactivity;
        super();
    }

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        adapterview = LiveHistoryManage.getInstance(a.mAppContext).getSoundInfoList();
        if (adapterview.size() != 0)
        {
            if ((i -= LiveRingtoneActivity.access$000(a).getHeaderViewsCount()) >= 0 && i < adapterview.size())
            {
                adapterview = (SoundInfo)adapterview.get(i);
                view = (new StringBuilder()).append(((SoundInfo) (adapterview)).radioId).append("").toString();
                Alarms.setAlarmSound(a, ((SoundInfo) (adapterview)).liveUrl, view, ((SoundInfo) (adapterview)).radioName, 3);
                view = new Intent();
                view.putExtra("sound_name", ((SoundInfo) (adapterview)).radioName);
                a.setResult(-1, view);
                a.finish();
                return;
            }
        }
    }
}
