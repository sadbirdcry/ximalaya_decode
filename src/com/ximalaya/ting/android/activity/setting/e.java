// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.setting.BindListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.LoginInfoUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            BindActivity

class e extends MyAsyncTask
{

    final BindActivity a;

    e(BindActivity bindactivity)
    {
        a = bindactivity;
        super();
    }

    protected transient UserInfoModel a(Void avoid[])
    {
        avoid = new RequestParams();
        if (a.loginInfoModel == null)
        {
            a.loginInfoModel = UserInfoMannage.getInstance().getUser();
            if (a.loginInfoModel == null)
            {
                return null;
            }
        }
        avoid = f.a().a(com.ximalaya.ting.android.a.e.J, avoid, BindActivity.access$000(a), BindActivity.access$000(a));
        if (Utilities.isNotBlank(avoid))
        {
            try
            {
                avoid = (UserInfoModel)JSON.parseObject(avoid, com/ximalaya/ting/android/model/personal_info/UserInfoModel);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid = null;
            }
        } else
        {
            avoid = null;
        }
        return avoid;
    }

    protected void a(UserInfoModel userinfomodel)
    {
        while (a == null || a.isFinishing() || userinfomodel == null) 
        {
            return;
        }
        LoginInfoUtil.refreshEmailAndPhone(a, a.loginInfoModel, userinfomodel);
        int j = BindActivity.access$100(a).size();
        int i = 0;
        while (i != j) 
        {
            userinfomodel = (SettingInfo)BindActivity.access$100(a).get(i);
            if (i == 0)
            {
                a.loginInfoModel = UserInfoMannage.getInstance().getUser();
                if (a.loginInfoModel != null && a.loginInfoModel.mPhone != null)
                {
                    userinfomodel.isSetting = true;
                    userinfomodel.textWake = (new StringBuilder()).append("(").append(a.loginInfoModel.mPhone).append(")").toString();
                } else
                {
                    userinfomodel.isSetting = false;
                    userinfomodel.textWake = "\u7ED1\u5B9A";
                }
            } else
            if (i == 1)
            {
                a.loginInfoModel = UserInfoMannage.getInstance().getUser();
                if (a.loginInfoModel != null && a.loginInfoModel.email != null)
                {
                    userinfomodel.isSetting = true;
                    userinfomodel.textWake = (new StringBuilder()).append("(").append(a.loginInfoModel.email).append(")").toString();
                    userinfomodel.isVerified = a.loginInfoModel.isVEmail;
                } else
                {
                    userinfomodel.isSetting = false;
                    userinfomodel.textWake = "\u9A8C\u8BC1";
                }
            }
            i++;
        }
        BindActivity.access$200(a).notifyDataSetChanged();
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((UserInfoModel)obj);
    }
}
