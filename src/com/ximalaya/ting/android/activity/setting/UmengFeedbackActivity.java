// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.umeng.fb.FeedbackAgent;
import com.umeng.fb.model.Conversation;
import com.umeng.fb.model.Reply;
import com.umeng.fb.model.UserInfo;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.view.ResizeRelativeLayout;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            bd, be, bf, bg, 
//            bh, bi, bk

public class UmengFeedbackActivity extends BaseActivity
{
    class a extends BaseAdapter
    {

        final UmengFeedbackActivity a;

        public int getCount()
        {
            return a.mComversation.getReplyList().size();
        }

        public Object getItem(int i)
        {
            return a.mComversation.getReplyList().get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public int getItemViewType(int i)
        {
            return !"dev_reply".equals(((Reply)a.mComversation.getReplyList().get(i)).type) ? 0 : 1;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            Reply reply;
label0:
            {
                reply = (Reply)a.mComversation.getReplyList().get(i);
                if (view == null)
                {
                    if (getItemViewType(i) == 1)
                    {
                        view = LayoutInflater.from(a).inflate(0x7f030144, null);
                    } else
                    {
                        view = LayoutInflater.from(a).inflate(0x7f030145, null);
                    }
                    viewgroup = new a(this);
                    viewgroup.a = (TextView)view.findViewById(0x7f0a04f5);
                    viewgroup.b = (ProgressBar)view.findViewById(0x7f0a04f7);
                    viewgroup.c = (ImageView)view.findViewById(0x7f0a04f6);
                    viewgroup.d = (TextView)view.findViewById(0x7f0a04f3);
                    view.setTag(viewgroup);
                } else
                {
                    viewgroup = (a)view.getTag();
                }
                ((a) (viewgroup)).a.setText(reply.content);
                if (!"dev_reply".equals(reply.type))
                {
                    if ("not_sent".equals(reply.status))
                    {
                        ((a) (viewgroup)).c.setVisibility(0);
                    } else
                    {
                        ((a) (viewgroup)).c.setVisibility(8);
                    }
                    if ("sending".equals(reply.status))
                    {
                        ((a) (viewgroup)).b.setVisibility(0);
                    } else
                    {
                        ((a) (viewgroup)).b.setVisibility(8);
                    }
                }
                if (i + 1 <= a.mComversation.getReplyList().size())
                {
                    if (i != 0)
                    {
                        break label0;
                    }
                    ((a) (viewgroup)).d.setText(TimeHelper.countTime2(reply.created_at));
                    ((a) (viewgroup)).d.setVisibility(0);
                }
                return view;
            }
            Reply reply1 = (Reply)a.mComversation.getReplyList().get(i - 1);
            if (reply.created_at - reply1.created_at > 0x186a0L)
            {
                ((a) (viewgroup)).d.setText(TimeHelper.countTime2(reply.created_at));
                ((a) (viewgroup)).d.setVisibility(0);
                return view;
            } else
            {
                ((a) (viewgroup)).d.setVisibility(8);
                return view;
            }
        }

        public int getViewTypeCount()
        {
            return 2;
        }

        a()
        {
            a = UmengFeedbackActivity.this;
            super();
        }
    }

    class a.a
    {

        TextView a;
        ProgressBar b;
        ImageView c;
        TextView d;
        final a e;

        a.a(a a1)
        {
            e = a1;
            super();
        }
    }


    private static final String KEY_EMAIL = "email";
    private static final String KEY_PHONE = "phone";
    private static final String KEY_QQ = "qq";
    private final int VIEW_TYPE_COUNT = 2;
    private final int VIEW_TYPE_DEV = 1;
    private final int VIEW_TYPE_USER = 0;
    private a mAdapter;
    private FeedbackAgent mAgent;
    private Conversation mComversation;
    private EditText mEmailEt;
    private EditText mInputEdit;
    private ListView mListView;
    private EditText mPhoneEt;
    private EditText mQQEt;
    private Button mSendBtn;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    public UmengFeedbackActivity()
    {
    }

    private UserInfo buildNewContact(UserInfo userinfo, String s)
    {
        String s1 = null;
        if (!"email".equals(s)) goto _L2; else goto _L1
_L1:
        Object obj;
        Object obj1;
        if (mEmailEt.getEditableText() == null)
        {
            s1 = "";
        } else
        {
            s1 = mEmailEt.getEditableText().toString();
        }
_L4:
        obj1 = userinfo.getContact();
        obj = obj1;
        if (obj1 == null)
        {
            obj = new HashMap();
        }
        obj1 = s1;
        if (TextUtils.isEmpty(s1))
        {
            obj1 = "";
        }
        ((Map) (obj)).put(s, obj1);
        return userinfo;
_L2:
        if ("phone".equals(s))
        {
            if (mPhoneEt.getEditableText() == null)
            {
                s1 = "";
            } else
            {
                s1 = mPhoneEt.getEditableText().toString();
            }
        } else
        if ("qq".equals(s))
        {
            if (mQQEt.getEditableText() == null)
            {
                s1 = "";
            } else
            {
                s1 = mQQEt.getEditableText().toString();
            }
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private UserInfo getUserInfo()
    {
        UserInfo userinfo1 = mAgent.getUserInfo();
        UserInfo userinfo = userinfo1;
        if (userinfo1 == null)
        {
            userinfo = new UserInfo();
        }
        return userinfo;
    }

    private void initContactInfo()
    {
        Object obj = mAgent.getUserInfo();
        if (obj != null && ((UserInfo) (obj)).getContact() != null)
        {
            String s = (String)((UserInfo) (obj)).getContact().get("phone");
            if (!TextUtils.isEmpty(s))
            {
                mPhoneEt.setText(s);
            }
            s = (String)((UserInfo) (obj)).getContact().get("email");
            if (!TextUtils.isEmpty(s))
            {
                mEmailEt.setText(s);
            }
            obj = (String)((UserInfo) (obj)).getContact().get("qq");
            if (!TextUtils.isEmpty(((CharSequence) (obj))))
            {
                mQQEt.setText(((CharSequence) (obj)));
            }
        }
    }

    private void initListener()
    {
        mSendBtn.setOnClickListener(new bd(this));
        mSwipeRefreshLayout.setOnRefreshListener(new be(this));
        mEmailEt.setOnFocusChangeListener(new bf(this));
        mQQEt.setOnFocusChangeListener(new bg(this));
        mPhoneEt.setOnFocusChangeListener(new bh(this));
        ((ResizeRelativeLayout)findViewById(0x7f0a00f1)).setOnResizeListener(new bi(this));
    }

    private void initView()
    {
        mListView = (ListView)findViewById(0x7f0a011f);
        mSendBtn = (Button)findViewById(0x7f0a011c);
        mInputEdit = (EditText)findViewById(0x7f0a011d);
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(0x7f0a011e);
        mSwipeRefreshLayout.setColorScheme(new int[] {
            0x7f070004, 0x7f070004, 0x7f070004, 0x7f070006
        });
        View view = LayoutInflater.from(this).inflate(0x7f0301fc, null);
        mPhoneEt = (EditText)view.findViewById(0x7f0a073e);
        mEmailEt = (EditText)view.findViewById(0x7f0a0740);
        mQQEt = (EditText)view.findViewById(0x7f0a0742);
        mListView.addHeaderView(view);
    }

    private void sync(boolean flag)
    {
        mComversation.sync(new bk(this, flag));
    }

    private void updateUserInfo(UserInfo userinfo)
    {
        mAgent.setUserInfo(userinfo);
        mAgent.updateUserInfo();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030031);
        initCommon();
        setTitleText("\u610F\u89C1\u53CD\u9988");
        initView();
        mAgent = new FeedbackAgent(this);
        mComversation = (new FeedbackAgent(this)).getDefaultConversation();
        mAdapter = new a();
        mListView.setAdapter(mAdapter);
        mInputEdit.requestFocus();
        initListener();
        sync(true);
        initContactInfo();
    }









}
