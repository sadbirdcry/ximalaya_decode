// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.activity.login.LoginActivity;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            UsedSpaceSizeAct, SettingActivity, DownloadLocationActivity, PushSettingActivity, 
//            ModifyPwdActivity, FeedRecycleActivity

class av
    implements android.widget.AdapterView.OnItemClickListener
{

    final SettingActivity a;

    av(SettingActivity settingactivity)
    {
        a = settingactivity;
        super();
    }

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        adapterview = new Intent();
        switch (i)
        {
        default:
            return;

        case 2: // '\002'
            adapterview.setClass(a, com/ximalaya/ting/android/activity/setting/UsedSpaceSizeAct);
            a.startActivityForResult(adapterview, 162);
            return;

        case 3: // '\003'
            adapterview.setClass(a, com/ximalaya/ting/android/activity/setting/DownloadLocationActivity);
            a.startActivityForResult(adapterview, 163);
            return;

        case 4: // '\004'
            if (a.loginInfoModel == null)
            {
                adapterview.setClass(a, com/ximalaya/ting/android/activity/login/LoginActivity);
                adapterview.putExtra("login_from_setting", true);
            } else
            {
                adapterview.setClass(a, com/ximalaya/ting/android/activity/setting/PushSettingActivity);
            }
            a.startActivity(adapterview);
            return;

        case 5: // '\005'
            if (a.loginInfoModel == null)
            {
                adapterview.setClass(a, com/ximalaya/ting/android/activity/login/LoginActivity);
                adapterview.putExtra("login_from_setting", true);
            } else
            {
                adapterview.setClass(a, com/ximalaya/ting/android/activity/setting/ModifyPwdActivity);
            }
            a.startActivity(adapterview);
            return;

        case 6: // '\006'
            break;
        }
        if (a.loginInfoModel == null)
        {
            adapterview.setClass(a, com/ximalaya/ting/android/activity/login/LoginActivity);
            adapterview.putExtra("login_from_setting", true);
        } else
        {
            adapterview.setClass(a, com/ximalaya/ting/android/activity/setting/FeedRecycleActivity);
        }
        a.startActivity(adapterview);
    }
}
