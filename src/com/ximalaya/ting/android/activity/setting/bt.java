// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import com.ximalaya.ting.android.model.UserSpace.CommonItem;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.StorageUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            UsedSpaceSizeAct

class bt extends MyAsyncTask
{

    final UsedSpaceSizeAct a;

    bt(UsedSpaceSizeAct usedspacesizeact)
    {
        a = usedspacesizeact;
        super();
    }

    protected transient List a(Void avoid[])
    {
        long l = StorageUtils.getTotalDownloadSize();
        long l1 = StorageUtils.getCachesSize();
        avoid = new ArrayList();
        avoid.add(Long.valueOf(l + l1));
        avoid.add(Long.valueOf(l));
        avoid.add(Long.valueOf(l1));
        return avoid;
    }

    protected void a(List list)
    {
        if (UsedSpaceSizeAct.access$600(a) == null || UsedSpaceSizeAct.access$600(a).size() < 3 || list.size() < 3)
        {
            return;
        } else
        {
            ((CommonItem)UsedSpaceSizeAct.access$600(a).get(0)).spaceOccupySize = (new BigDecimal(((Long)list.get(0)).longValue())).setScale(1, 4).floatValue();
            ((CommonItem)UsedSpaceSizeAct.access$600(a).get(1)).spaceOccupySize = (new BigDecimal(((Long)list.get(1)).longValue())).setScale(1, 4).floatValue();
            ((CommonItem)UsedSpaceSizeAct.access$600(a).get(2)).spaceOccupySize = (new BigDecimal(((Long)list.get(2)).longValue())).setScale(1, 4).floatValue();
            UsedSpaceSizeAct.access$700(a).notifyDataSetChanged();
            a.downloadSize = ((CommonItem)UsedSpaceSizeAct.access$600(a).get(1)).spaceOccupySize;
            a.cachesSize = ((CommonItem)UsedSpaceSizeAct.access$600(a).get(2)).spaceOccupySize;
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((List)obj);
    }
}
