// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.adapter.feed.FeedRecycleAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.feed2.FeedRecycleModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.pulltorefreshgridview.RefreshLoadMoreListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            q, o, n, m, 
//            l, j, k, p

public class FeedRecycleActivity extends BaseActivity
{
    public static class FeedRecycleListModel extends BaseModel
    {

        public List datas;

        public FeedRecycleListModel()
        {
        }
    }


    private LoginInfoModel loginInfoModel;
    private FeedRecycleAdapter mAdapter;
    private Context mContext;
    private List mDataList;
    private boolean mHasMore;
    private RelativeLayout mHeader;
    private boolean mIsLoading;
    private RefreshLoadMoreListView mListView;
    private ProgressBar pb;

    public FeedRecycleActivity()
    {
        mDataList = new ArrayList();
        mIsLoading = false;
        pb = null;
        mHasMore = true;
    }

    private void doRecycle(FeedRecycleModel feedrecyclemodel, View view)
    {
        RequestParams requestparams = new RequestParams();
        if (loginInfoModel == null)
        {
            loginInfoModel = UserInfoMannage.getInstance().getUser();
        }
        if (loginInfoModel != null)
        {
            requestparams.put("uid", (new StringBuilder()).append("").append(loginInfoModel.uid).toString());
            requestparams.put("token", loginInfoModel.token);
        }
        requestparams.put("albumUid", feedrecyclemodel.albumUid);
        requestparams.put("albumId", feedrecyclemodel.albumId);
        f.a().b("mobile/api/1/feed/block/delete", requestparams, DataCollectUtil.getDataFromView(view), new q(this, feedrecyclemodel));
    }

    private void initData()
    {
        mListView.postDelayed(new o(this), 150L);
    }

    private void initHeaderView()
    {
        mHeader = (RelativeLayout)View.inflate(mContext, 0x7f030084, null);
        ((ListView)mListView.getRefreshableView()).addHeaderView(mHeader);
    }

    private void initItemClickListener()
    {
        mListView.setOnItemClickListener(new n(this));
    }

    private void initListeners()
    {
        initScrollListener();
        initRefreshListener();
        initItemClickListener();
    }

    private void initRefreshListener()
    {
        mListView.setRefreshLoadMoreListener(new m(this));
    }

    private void initScrollListener()
    {
        mListView.setOnScrollListener(new l(this));
    }

    private void initUi()
    {
        ((TextView)findViewById(0x7f0a00ae)).setText("\u52A8\u6001\u56DE\u6536");
        mListView = (RefreshLoadMoreListView)findViewById(0x7f0a005c);
        mListView.setMode(com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.DISABLED);
        ((ListView)mListView.getRefreshableView()).setFastScrollEnabled(false);
        retButton = (ImageView)findViewById(0x7f0a007b);
        pb = (ProgressBar)findViewById(0x7f0a0093);
        initHeaderView();
        mAdapter = new FeedRecycleAdapter(mContext, mDataList, new j(this));
        mListView.setAdapter(mAdapter);
        retButton.setOnClickListener(new k(this));
    }

    private void loadDataList()
    {
        mIsLoading = true;
        RequestParams requestparams = new RequestParams();
        if (loginInfoModel == null)
        {
            loginInfoModel = UserInfoMannage.getInstance().getUser();
        }
        if (loginInfoModel != null)
        {
            requestparams.put("uid", (new StringBuilder()).append("").append(loginInfoModel.uid).toString());
            requestparams.put("token", loginInfoModel.token);
        }
        requestparams.put("size", 15);
        if (mDataList.size() > 0)
        {
            requestparams.put("cursor", ((FeedRecycleModel)mDataList.get(mDataList.size() - 1)).cursor);
        }
        f.a().a("mobile/api/1/feed/block/list", requestparams, DataCollectUtil.getDataFromView(mListView), new p(this));
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030012);
        mContext = getApplicationContext();
        initUi();
        initData();
        initListeners();
    }




/*
    static boolean access$102(FeedRecycleActivity feedrecycleactivity, boolean flag)
    {
        feedrecycleactivity.mIsLoading = flag;
        return flag;
    }

*/



/*
    static boolean access$202(FeedRecycleActivity feedrecycleactivity, boolean flag)
    {
        feedrecycleactivity.mHasMore = flag;
        return flag;
    }

*/






}
