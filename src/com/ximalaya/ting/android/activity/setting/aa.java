// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            ModifyPwdActivity

class aa
    implements android.view.View.OnClickListener
{

    final ModifyPwdActivity a;

    aa(ModifyPwdActivity modifypwdactivity)
    {
        a = modifypwdactivity;
        super();
    }

    public void onClick(View view)
    {
        if (ToolUtil.isConnectToNetwork(ModifyPwdActivity.access$300(a)))
        {
            if (TextUtils.isEmpty(ModifyPwdActivity.access$000(a).getText()) || TextUtils.isEmpty(ModifyPwdActivity.access$100(a).getText()) || TextUtils.isEmpty(ModifyPwdActivity.access$200(a).getText()))
            {
                Toast.makeText(ModifyPwdActivity.access$300(a), 0x7f090139, 0).show();
                return;
            }
            if (ModifyPwdActivity.access$100(a).getText().toString().trim().length() < 6)
            {
                Toast.makeText(ModifyPwdActivity.access$300(a), "\u5BC6\u7801\u4E0D\u80FD\u5C11\u4E8E\u516D\u4F4D", 0).show();
                return;
            }
            if (!ModifyPwdActivity.access$100(a).getText().toString().trim().equals(ModifyPwdActivity.access$100(a).getText().toString().trim()))
            {
                Toast.makeText(ModifyPwdActivity.access$300(a), "\u65B0\u5BC6\u7801\u548C\u786E\u8BA4\u5BC6\u7801\u8F93\u5165\u4E0D\u4E00\u81F4", 0).show();
                return;
            } else
            {
                (new ModifyPwdActivity.a(a)).myexec(new Void[0]);
                return;
            }
        } else
        {
            Toast.makeText(ModifyPwdActivity.access$300(a), a.getString(0x7f0900a0), 1).show();
            return;
        }
    }
}
