// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.model.alarm.Alarm;
import com.ximalaya.ting.android.model.alarm.Alarms;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.Utilities;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            WakeUpSettingActivity, LocalRingtoneActivity, LiveRingtoneActivity

public class SelectRingtoneActivity extends BaseActivity
    implements android.view.View.OnClickListener
{

    private static final String LOCAL_RINGTONS[] = {
        "\u4E0D\u60F3\u8D77\u5E8A\u94C3\u58F0", "\u8D85\u840C\u67E5\u6C34\u8868", "\u96F6\u667A\u5546-\u901A\u7528\u7248", "\u5341\u4E07\u4E2A\u51B7\u7B11\u8BDD-\u5927\u738B\u9189\u9152", "\u53F2\u4E0A\u6700\u6E29\u67D4\u6700\u751C\u7F8E\u8D77\u5E8A\u94C3", "\u53F6\u6E05\u8D77\u5E8A\u95F9\u94C3", "\u81EA\u6302\u4E1C\u5357\u679D", "Jarvis\u8D77\u5E8A\u94C3"
    };
    public static final int REQUEST_CODE_SELECT_DOWNLOAD_SOUND = 1;
    public static final int REQUEST_CODE_SELECT_RADIO = 2;
    private TextView mCheckedEntry;
    private TextView mCrosstalkSounds;
    private TextView mDefaultSound;
    private TextView mDownloadedSounds;
    private TextView mLiveRadioSounds;
    private MediaPlayer mMp;
    private TextView mMusicSounds;
    private TextView mNewsSounds;
    private ArrayList mTextViews;

    public SelectRingtoneActivity()
    {
    }

    private void changeTextViewCheckStatus(TextView textview)
    {
        if (mCheckedEntry != null)
        {
            if (mCheckedEntry == mDownloadedSounds)
            {
                mCheckedEntry.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f020041, 0);
                mCheckedEntry.setText(0x7f090107);
            } else
            {
                mCheckedEntry.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }
        mCheckedEntry = textview;
        mCheckedEntry.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f020482, 0);
    }

    private void checkTheCorrEntry()
    {
        Object obj = Alarm.getAlarmFromPref(this);
        ((Alarm) (obj)).mType;
        JVM INSTR tableswitch 0 3: default 40
    //                   0 175
    //                   1 195
    //                   2 49
    //                   3 272;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        changeTextViewCheckStatus(mDefaultSound);
_L7:
        return;
_L4:
        if (TextUtils.isEmpty(((Alarm) (obj)).mUrl))
        {
            changeTextViewCheckStatus(mDefaultSound);
            return;
        }
        int i = Integer.valueOf(((Alarm) (obj)).mUrl.substring("buildin://".length())).intValue();
        if (i == -1)
        {
            try
            {
                changeTextViewCheckStatus(mDefaultSound);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                changeTextViewCheckStatus(mDefaultSound);
            }
            return;
        }
        TextView textview;
        obj = mTextViews.iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                continue; /* Loop/switch isn't completed */
            }
            textview = (TextView)((Iterator) (obj)).next();
        } while (textview.getTag() == null || !(textview.getTag() instanceof Integer) || ((Integer)textview.getTag()).intValue() != i);
        break; /* Loop/switch isn't completed */
        if (true) goto _L7; else goto _L6
_L6:
        changeTextViewCheckStatus(textview);
        return;
_L2:
        changeTextViewCheckStatus(mDownloadedSounds);
        mDownloadedSounds.setText(((Alarm) (obj)).mTitle);
        return;
_L3:
        switch (Integer.valueOf(Uri.parse(((Alarm) (obj)).mUrl).getQueryParameter("type")).intValue())
        {
        case 3: // '\003'
        default:
            return;

        case 1: // '\001'
            changeTextViewCheckStatus(mNewsSounds);
            return;

        case 2: // '\002'
            changeTextViewCheckStatus(mMusicSounds);
            return;

        case 4: // '\004'
            changeTextViewCheckStatus(mCrosstalkSounds);
            break;
        }
        return;
_L5:
        changeTextViewCheckStatus(mLiveRadioSounds);
        return;
    }

    private void loadLocalRingtones()
    {
        ViewGroup viewgroup = (ViewGroup)findViewById(0x7f0a005a);
        if (mTextViews == null)
        {
            mTextViews = new ArrayList();
        }
        for (int i = 0; i < LOCAL_RINGTONS.length; i++)
        {
            Object obj = new View(this);
            ((View) (obj)).setBackgroundColor(getResources().getColor(0x7f0700be));
            android.widget.LinearLayout.LayoutParams layoutparams = new android.widget.LinearLayout.LayoutParams(-1, 1);
            layoutparams.setMargins(Utilities.dip2px(this, 15F), 0, 0, 0);
            viewgroup.addView(((View) (obj)), layoutparams);
            obj = new TextView(this);
            ((TextView) (obj)).setText(LOCAL_RINGTONS[i]);
            ((TextView) (obj)).setBackgroundResource(0x7f0205ab);
            ((TextView) (obj)).setPadding(Utilities.dip2px(this, 15F), 0, Utilities.dip2px(this, 15F), 0);
            ((TextView) (obj)).setGravity(16);
            ((TextView) (obj)).setTextColor(Color.parseColor("#333333"));
            ((TextView) (obj)).setTextSize(16F);
            ((TextView) (obj)).setTag(Integer.valueOf(i));
            ((TextView) (obj)).setClickable(true);
            ((TextView) (obj)).setOnClickListener(this);
            viewgroup.addView(((View) (obj)), new android.widget.LinearLayout.LayoutParams(-1, Utilities.dip2px(this, 45F)));
            mTextViews.add(obj);
        }

    }

    private void previewRingtone(int i)
    {
        Object obj;
        obj = null;
        AssetFileDescriptor assetfiledescriptor;
        if (mMp == null)
        {
            mMp = new MediaPlayer();
            mMp.setLooping(true);
        } else
        {
            mMp.reset();
        }
        ((AudioManager)getSystemService("audio")).requestAudioFocus(null, 4, 3);
        assetfiledescriptor = getResources().openRawResourceFd(i);
        obj = assetfiledescriptor;
        if (obj != null) goto _L2; else goto _L1
_L1:
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_68;
        }
        ((AssetFileDescriptor) (obj)).close();
_L4:
        return;
_L2:
        Object obj1 = obj;
        java.io.FileDescriptor filedescriptor = ((AssetFileDescriptor) (obj)).getFileDescriptor();
        obj1 = obj;
        mMp.setDataSource(filedescriptor, ((AssetFileDescriptor) (obj)).getStartOffset(), ((AssetFileDescriptor) (obj)).getLength());
        obj1 = obj;
        mMp.prepare();
        obj1 = obj;
        mMp.start();
        if (obj == null) goto _L4; else goto _L3
_L3:
        try
        {
            ((AssetFileDescriptor) (obj)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
_L6:
        ((IOException) (obj)).printStackTrace();
        return;
        obj1;
_L14:
        ((IllegalArgumentException) (obj1)).printStackTrace();
        if (obj == null) goto _L4; else goto _L5
_L5:
        try
        {
            ((AssetFileDescriptor) (obj)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L6
        Object obj2;
        obj2;
        obj = null;
_L13:
        obj1 = obj;
        ((IllegalStateException) (obj2)).printStackTrace();
        if (obj == null) goto _L4; else goto _L7
_L7:
        try
        {
            ((AssetFileDescriptor) (obj)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L6
        obj2;
        obj = null;
_L12:
        obj1 = obj;
        ((android.content.res.Resources.NotFoundException) (obj2)).printStackTrace();
        if (obj == null) goto _L4; else goto _L8
_L8:
        try
        {
            ((AssetFileDescriptor) (obj)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L6
        obj2;
        obj = null;
_L11:
        obj1 = obj;
        ((IOException) (obj2)).printStackTrace();
        if (obj == null) goto _L4; else goto _L9
_L9:
        try
        {
            ((AssetFileDescriptor) (obj)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L6
        obj;
        obj1 = null;
_L10:
        if (obj1 != null)
        {
            try
            {
                ((AssetFileDescriptor) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((IOException) (obj1)).printStackTrace();
            }
        }
        throw obj;
        obj;
          goto _L6
        obj;
          goto _L10
        obj2;
        obj1 = obj;
        obj = obj2;
          goto _L10
        obj2;
          goto _L11
        obj2;
          goto _L12
        obj2;
          goto _L13
        obj1;
          goto _L14
    }

    private void stopPreviewRingtone()
    {
        if (mMp != null && mMp.isPlaying())
        {
            mMp.stop();
            ((AudioManager)getSystemService("audio")).abandonAudioFocus(null);
        }
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        super.onActivityResult(i, j, intent);
        i;
        JVM INSTR tableswitch 1 2: default 32
    //                   1 33
    //                   2 74;
           goto _L1 _L2 _L3
_L1:
        return;
_L2:
        if (j == 11)
        {
            changeTextViewCheckStatus(mDownloadedSounds);
            mDownloadedSounds.setText(intent.getStringExtra("sound_name"));
            mDownloadedSounds.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f020482, 0);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (j == -1)
        {
            changeTextViewCheckStatus(mLiveRadioSounds);
            mLiveRadioSounds.setText(intent.getStringExtra("sound_name"));
            mLiveRadioSounds.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f020482, 0);
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    public void onClick(View view)
    {
        switch (view.getId())
        {
        default:
            changeTextViewCheckStatus((TextView)view);
            Alarm alarm = new Alarm();
            alarm.mType = 2;
            alarm.mUrl = (new StringBuilder()).append("buildin://").append(view.getTag()).toString();
            alarm.mTitle = LOCAL_RINGTONS[((Integer)view.getTag()).intValue()];
            alarm.mLocationDir = "";
            Alarms.setAlarm(this, alarm);
            previewRingtone(WakeUpSettingActivity.BUILDIN_RINGTONS[((Integer)view.getTag()).intValue()]);
            return;

        case 2131362050: 
            view = new Intent();
            view.setClass(this, com/ximalaya/ting/android/activity/setting/LocalRingtoneActivity);
            startActivityForResult(view, 1);
            stopPreviewRingtone();
            return;

        case 2131362051: 
            view = new Intent();
            view.setClass(this, com/ximalaya/ting/android/activity/setting/LiveRingtoneActivity);
            startActivityForResult(view, 2);
            stopPreviewRingtone();
            return;

        case 2131362052: 
            changeTextViewCheckStatus((TextView)view);
            view = new Alarm();
            view.mType = 1;
            view.mUrl = (new StringBuilder()).append(Alarm.ONLINE_ALARM).append("?type=").append(1).toString();
            if (UserInfoMannage.hasLogined())
            {
                view.mUrl = (new StringBuilder()).append(((Alarm) (view)).mUrl).append("&uid=").append(UserInfoMannage.getInstance().getUser().uid).toString();
            }
            view.mTitle = getString(0x7f09010b);
            view.mLocationDir = "";
            Alarms.setAlarm(this, view);
            stopPreviewRingtone();
            return;

        case 2131362053: 
            changeTextViewCheckStatus((TextView)view);
            view = new Alarm();
            view.mType = 1;
            view.mUrl = (new StringBuilder()).append(Alarm.ONLINE_ALARM).append("?type=").append(2).toString();
            if (UserInfoMannage.hasLogined())
            {
                view.mUrl = (new StringBuilder()).append(((Alarm) (view)).mUrl).append("&uid=").append(UserInfoMannage.getInstance().getUser().uid).toString();
            }
            view.mTitle = getString(0x7f09010c);
            view.mLocationDir = "";
            Alarms.setAlarm(this, view);
            stopPreviewRingtone();
            return;

        case 2131362054: 
            changeTextViewCheckStatus((TextView)view);
            view = new Alarm();
            view.mType = 1;
            view.mUrl = (new StringBuilder()).append(Alarm.ONLINE_ALARM).append("?type=").append(4).toString();
            if (UserInfoMannage.hasLogined())
            {
                view.mUrl = (new StringBuilder()).append(((Alarm) (view)).mUrl).append("&uid=").append(UserInfoMannage.getInstance().getUser().uid).toString();
            }
            view.mTitle = getString(0x7f09010d);
            view.mLocationDir = "";
            Alarms.setAlarm(this, view);
            stopPreviewRingtone();
            return;

        case 2131362055: 
            changeTextViewCheckStatus((TextView)view);
            view = new Alarm();
            view.mType = 2;
            view.mUrl = "buildin://-1";
            view.mTitle = getString(0x7f09010f);
            view.mLocationDir = "";
            Alarms.setAlarm(this, view);
            previewRingtone(0x7f060001);
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03002b);
        initCommon();
        setTitleText(getString(0x7f090110));
        loadLocalRingtones();
        if (mTextViews == null)
        {
            mTextViews = new ArrayList();
        }
        mDownloadedSounds = (TextView)findViewById(0x7f0a0102);
        mTextViews.add(mDownloadedSounds);
        mDownloadedSounds.setOnClickListener(this);
        mLiveRadioSounds = (TextView)findViewById(0x7f0a0103);
        mTextViews.add(mLiveRadioSounds);
        mLiveRadioSounds.setOnClickListener(this);
        mNewsSounds = (TextView)findViewById(0x7f0a0104);
        mTextViews.add(mNewsSounds);
        mNewsSounds.setOnClickListener(this);
        mMusicSounds = (TextView)findViewById(0x7f0a0105);
        mTextViews.add(mMusicSounds);
        mMusicSounds.setOnClickListener(this);
        mCrosstalkSounds = (TextView)findViewById(0x7f0a0106);
        mTextViews.add(mCrosstalkSounds);
        mCrosstalkSounds.setOnClickListener(this);
        mDefaultSound = (TextView)findViewById(0x7f0a0107);
        mTextViews.add(mDefaultSound);
        mDefaultSound.setOnClickListener(this);
    }

    protected void onPause()
    {
        super.onPause();
        stopPreviewRingtone();
    }

    protected void onResume()
    {
        super.onResume();
        checkTheCorrEntry();
    }

    protected void onStop()
    {
        super.onStop();
        if (mMp != null)
        {
            mMp.release();
            mMp = null;
        }
    }

}
