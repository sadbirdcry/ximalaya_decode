// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.adapter.setting.NoRegisterAdapter;
import com.ximalaya.ting.android.adapter.setting.NoRegisterThirdAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.setting.UnregisterList;
import com.ximalaya.ting.android.model.thirdBind.ThirdPartyUserInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            r, s

public class FindFriendActivity extends BaseActivity
{
    private class a extends MyAsyncTask
    {

        UnregisterList a;
        ProgressDialog b;
        final FindFriendActivity c;

        protected transient String a(String as[])
        {
            Object obj = null;
            if (c.flag == 1)
            {
                c.getContentMsg();
                List list = ToolUtil.getSimContacts(c);
                as = obj;
                if (list != null)
                {
                    as = obj;
                    if (list.size() > 0)
                    {
                        a = new UnregisterList();
                        a.tpUerInfo = list;
                        as = "0";
                    }
                }
            } else
            {
                String s1 = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/friendship/unregister").toString();
                RequestParams requestparams = new RequestParams();
                requestparams.put("type", (new StringBuilder()).append(c.flag).append("").toString());
                requestparams.put("pageId", as[0]);
                requestparams.put("pageSize", (new StringBuilder()).append(c.pageSize).append("").toString());
                s1 = f.a().a(s1, requestparams, c.noRegisterListView, c.noRegisterListView);
                as = obj;
                if (s1 != null)
                {
                    as = JSON.parseObject(s1);
                    if (as.getInteger("ret").intValue() == 0)
                    {
                        if (as.get("data") != null)
                        {
                            a = new UnregisterList();
                            try
                            {
                                a.tpUerInfo = JSON.parseArray(as.get("data").toString(), com/ximalaya/ting/android/model/thirdBind/ThirdPartyUserInfo);
                                a.maxPageId = as.getIntValue("maxPageId");
                                a.pageId = as.getIntValue("pageId");
                            }
                            // Misplaced declaration of an exception variable
                            catch (String as[])
                            {
                                return "\u89E3\u6790\u6570\u636E\u51FA\u9519";
                            }
                            return "0";
                        } else
                        {
                            return "\u6CA1\u6709\u597D\u53CB";
                        }
                    } else
                    {
                        return as.getString("msg");
                    }
                }
            }
            return as;
        }

        protected void a(String s1)
        {
            if (c == null || c.isFinishing())
            {
                return;
            }
            if (b != null)
            {
                b.dismiss();
                b = null;
            }
            if (s1 != null)
            {
                if ("0".equals(s1))
                {
                    c.toast_02.setVisibility(0);
                    if (c.flag == 1)
                    {
                        if (c.noRegisterAdapter == null)
                        {
                            c.noRegisterAdapter = new NoRegisterAdapter(c, a.tpUerInfo, c.contentMsg);
                            c.noRegisterListView.setAdapter(c.noRegisterAdapter);
                            return;
                        } else
                        {
                            c.noRegisterAdapter.list = a.tpUerInfo;
                            c.noRegisterAdapter.notifyDataSetChanged();
                            return;
                        }
                    }
                    if (c.noRegisterThirdAdapter == null)
                    {
                        c.noRegisterThirdAdapter = new NoRegisterThirdAdapter(c, a.tpUerInfo, c.flag);
                        c.noRegisterListView.setAdapter(c.noRegisterThirdAdapter);
                        return;
                    } else
                    {
                        c.noRegisterThirdAdapter.list = a.tpUerInfo;
                        c.noRegisterThirdAdapter.notifyDataSetChanged();
                        return;
                    }
                } else
                {
                    Toast.makeText(c.getApplicationContext(), s1, 0).show();
                    return;
                }
            } else
            {
                Toast.makeText(c.getApplicationContext(), "\u52A0\u8F7D\u6570\u636E\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5...", 0).show();
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((String[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((String)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            if (c.curPageId == 1)
            {
                b = new MyProgressDialog(c);
                b.setMessage("\u6B63\u5728\u52AA\u529B\u4E3A\u60A8\u52A0\u8F7D\u4FE1\u606F...");
                b.show();
            }
        }

        private a()
        {
            c = FindFriendActivity.this;
            super();
        }

        a(r r1)
        {
            this();
        }
    }


    private static final String TAG = com/ximalaya/ting/android/activity/setting/FindFriendActivity.getSimpleName();
    private String contentMsg;
    private int curPageId;
    private int flag;
    private Context mCt;
    private NoRegisterAdapter noRegisterAdapter;
    private PullToRefreshListView noRegisterListView;
    private NoRegisterThirdAdapter noRegisterThirdAdapter;
    private int pageSize;
    private LinearLayout toast_02;

    public FindFriendActivity()
    {
        contentMsg = "";
        flag = 0;
        curPageId = 1;
        pageSize = 200;
    }

    private void findViews()
    {
        retButton = (ImageView)findViewById(0x7f0a007b);
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        nextButton.setVisibility(4);
        toast_02 = (LinearLayout)findViewById(0x7f0a026f);
        noRegisterListView = (PullToRefreshListView)findViewById(0x7f0a0272);
    }

    private void getContentMsg()
    {
        JSONObject jsonobject = null;
        if (flag == 1) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/message/invite/content").toString();
        RequestParams requestparams = new RequestParams();
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        obj = f.a().a(((String) (obj)), requestparams, rootView, null);
        if (obj == null) goto _L1; else goto _L3
_L3:
        obj = JSON.parseObject(((String) (obj)));
        jsonobject = ((JSONObject) (obj));
_L4:
        Exception exception;
        if (jsonobject.getInteger("ret").intValue() == 0)
        {
            contentMsg = jsonobject.getString("content");
            return;
        } else
        {
            Toast.makeText(getApplicationContext(), jsonobject.getString("msg"), 1).show();
            return;
        }
        exception;
        exception.printStackTrace();
          goto _L4
    }

    private void getParentInfo()
    {
        flag = getIntent().getIntExtra("flag", 1);
    }

    private void initViews()
    {
        retButton.setOnClickListener(new r(this));
        if (flag != 1) goto _L2; else goto _L1
_L1:
        topTextView.setText(getString(0x7f090114));
_L4:
        noRegisterListView.setOnScrollListener(new s(this));
        TextView textview = new TextView(this);
        textview.setGravity(17);
        textview.setTextColor(0xff000000);
        textview.setText("\u6682\u65F6\u6CA1\u6709\u6570\u636E...");
        noRegisterListView.setEmptyView(textview);
        addContentView(textview, new android.widget.LinearLayout.LayoutParams(-1, -1));
        return;
_L2:
        if (flag == 2)
        {
            topTextView.setText(getString(0x7f090119));
        } else
        if (flag == 3)
        {
            topTextView.setText(getString(0x7f09011a));
        } else
        if (flag == 4)
        {
            topTextView.setText(getString(0x7f09011b));
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        if (i != 0);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030088);
        mCt = getApplicationContext();
        getParentInfo();
        findViews();
        initViews();
        (new a(null)).myexec(new String[] {
            (new StringBuilder()).append(curPageId).append("").toString()
        });
    }









/*
    static NoRegisterAdapter access$502(FindFriendActivity findfriendactivity, NoRegisterAdapter noregisteradapter)
    {
        findfriendactivity.noRegisterAdapter = noregisteradapter;
        return noregisteradapter;
    }

*/




/*
    static NoRegisterThirdAdapter access$702(FindFriendActivity findfriendactivity, NoRegisterThirdAdapter noregisterthirdadapter)
    {
        findfriendactivity.noRegisterThirdAdapter = noregisterthirdadapter;
        return noregisterthirdadapter;
    }

*/

}
