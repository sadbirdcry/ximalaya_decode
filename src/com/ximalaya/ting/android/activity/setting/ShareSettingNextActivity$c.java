// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            ShareSettingNextActivity, bc

class b extends BaseAdapter
{
    class a
    {

        TextView a;
        SwitchButton b;
        final ShareSettingNextActivity.c c;

        a()
        {
            c = ShareSettingNextActivity.c.this;
            super();
        }
    }


    final ShareSettingNextActivity a;
    private LayoutInflater b;

    public int getCount()
    {
        return ShareSettingNextActivity.access$500(a).size();
    }

    public Object getItem(int i)
    {
        return ShareSettingNextActivity.access$500(a).get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        SettingInfo settinginfo;
        if (view == null)
        {
            viewgroup = new a();
            view = b.inflate(0x7f03019e, null);
            viewgroup.a = (TextView)view.findViewById(0x7f0a0652);
            viewgroup.b = (SwitchButton)view.findViewById(0x7f0a0653);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (a)view.getTag();
        }
        settinginfo = (SettingInfo)getItem(i);
        if (settinginfo.getNameWake() != null)
        {
            ((a) (viewgroup)).a.setText(settinginfo.getNameWake());
            ((a) (viewgroup)).b.initCheckedState(settinginfo.isSetting());
            ((a) (viewgroup)).b.setOnCheckedChangeListener(new bc(this, i));
        }
        return view;
    }

    public a.c(ShareSettingNextActivity sharesettingnextactivity, Activity activity)
    {
        a = sharesettingnextactivity;
        super();
        b = LayoutInflater.from(activity);
    }
}
