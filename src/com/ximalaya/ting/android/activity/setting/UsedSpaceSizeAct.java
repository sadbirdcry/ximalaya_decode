// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.UserSpace.CommonItem;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Session;
import com.ximalaya.ting.android.view.setting.CornerListView;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            bl, bm, bt, bq, 
//            bn

public class UsedSpaceSizeAct extends BaseActivity
{
    class a extends BaseAdapter
    {

        List a;
        final UsedSpaceSizeAct b;

        public CommonItem a(int i)
        {
            return (CommonItem)a.get(i);
        }

        public int getCount()
        {
            return a.size();
        }

        public Object getItem(int i)
        {
            return a(i);
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            CommonItem commonitem;
            if (view == null)
            {
                viewgroup = new a(this);
                view = b.getLayoutInflater().inflate(0x7f0301d2, null);
                viewgroup.a = (TextView)view.findViewById(0x7f0a06e0);
                viewgroup.b = (TextView)view.findViewById(0x7f0a06ca);
                viewgroup.c = view.findViewById(0x7f0a06e1);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (a)view.getTag();
            }
            commonitem = a(i);
            ((a) (viewgroup)).a.setText(commonitem.name);
            ((a) (viewgroup)).b.setText((new StringBuilder()).append(commonitem.spaceOccupySize).append("M").toString());
            switch (i)
            {
            default:
                return view;

            case 0: // '\0'
                ((a) (viewgroup)).c.setVisibility(4);
                return view;

            case 1: // '\001'
                if (commonitem.spaceOccupySize > 0.0F)
                {
                    ((a) (viewgroup)).c.setVisibility(0);
                    return view;
                } else
                {
                    ((a) (viewgroup)).c.setVisibility(4);
                    return view;
                }

            case 2: // '\002'
                break;
            }
            if (commonitem.spaceOccupySize > 0.0F)
            {
                ((a) (viewgroup)).c.setVisibility(0);
                return view;
            } else
            {
                ((a) (viewgroup)).c.setVisibility(4);
                return view;
            }
        }

        public a(List list)
        {
            b = UsedSpaceSizeAct.this;
            super();
            a = null;
            a = list;
        }
    }

    class a.a
    {

        TextView a;
        TextView b;
        View c;
        final a d;

        a.a(a a1)
        {
            d = a1;
            super();
        }
    }


    float cachesSize;
    String chunkFileName;
    float downloadSize;
    String headName;
    String indexFileName;
    private LocalMediaService localMediaService;
    private MyAsyncTask mClearCacheTask;
    private a spaceSizeAdapter;
    private List spacesizeList;
    private CornerListView spacesizeListview;

    public UsedSpaceSizeAct()
    {
        spacesizeList = null;
        spacesizeListview = null;
        spaceSizeAdapter = null;
        downloadSize = 0.0F;
        cachesSize = 0.0F;
        localMediaService = null;
        headName = null;
        chunkFileName = null;
        indexFileName = null;
    }

    private void findViews()
    {
        spacesizeListview = (CornerListView)findViewById(0x7f0a06df);
    }

    private void initDatas()
    {
        localMediaService = LocalMediaService.getInstance();
        if (Session.getSession().containsKey("TOTAL_DOWNLOAD_SIZE"))
        {
            downloadSize = ((Long)Session.getSession().get("TOTAL_DOWNLOAD_SIZE")).longValue();
        }
        if (Session.getSession().containsKey("TOTAL_CACHE_SIZE"))
        {
            cachesSize = ((Long)Session.getSession().get("TOTAL_CACHE_SIZE")).longValue();
        }
    }

    private void initViews()
    {
        int i;
        retButton = (ImageView)findViewById(0x7f0a007b);
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        nextButton.setVisibility(4);
        topTextView.setText("\u4E0B\u8F7D\u548C\u7F13\u5B58\u8BBE\u7F6E");
        retButton.setOnClickListener(new bl(this));
        spacesizeList = new ArrayList();
        i = 0;
_L6:
        CommonItem commonitem;
        if (i >= 3)
        {
            break MISSING_BLOCK_LABEL_204;
        }
        commonitem = new CommonItem();
        commonitem.name = (new String[] {
            "\u603B\u5360\u7528\u7A7A\u95F4", "\u4E0B\u8F7D\u5360\u7528", "\u7F13\u5B58\u5360\u7528"
        })[i];
        i;
        JVM INSTR tableswitch 0 2: default 148
    //                   0 166
    //                   1 182
    //                   2 193;
           goto _L1 _L2 _L3 _L4
_L4:
        break MISSING_BLOCK_LABEL_193;
_L1:
        break; /* Loop/switch isn't completed */
_L2:
        break; /* Loop/switch isn't completed */
_L7:
        spacesizeList.add(commonitem);
        i++;
        if (true) goto _L6; else goto _L5
_L5:
        commonitem.spaceOccupySize = downloadSize + cachesSize;
          goto _L7
_L3:
        commonitem.spaceOccupySize = downloadSize;
          goto _L7
        commonitem.spaceOccupySize = cachesSize;
          goto _L7
        spaceSizeAdapter = new a(spacesizeList);
        spacesizeListview.setAdapter(spaceSizeAdapter);
        spacesizeListview.setOnItemClickListener(new bm(this));
        return;
    }

    private void refreshDownloadAndCacheSize()
    {
        (new bt(this)).myexec(new Void[0]);
    }

    private void showClearCacheConfirm()
    {
        (new DialogBuilder(this)).setMessage("\u786E\u5B9A\u8981\u6E05\u9664\u7F13\u5B58\uFF1F").setOkBtn("\u6E05\u7A7A", new bq(this)).showConfirm();
    }

    private void showClearDownloadConfirm()
    {
        (new DialogBuilder(this)).setMessage("\u786E\u5B9A\u8981\u6E05\u7A7A\u5DF2\u4E0B\u8F7D\u6587\u4EF6\uFF1F").setOkBtn("\u6E05\u7A7A", new bn(this)).showConfirm();
    }

    private void updateCacheSize(float f)
    {
        if (spacesizeList == null || spacesizeList.size() < 3)
        {
            return;
        } else
        {
            ((CommonItem)spacesizeList.get(0)).spaceOccupySize = (new BigDecimal(downloadSize + f)).setScale(0, 4).floatValue();
            ((CommonItem)spacesizeList.get(2)).spaceOccupySize = (new BigDecimal(f)).setScale(1, 4).floatValue();
            cachesSize = ((CommonItem)spacesizeList.get(2)).spaceOccupySize;
            spaceSizeAdapter.notifyDataSetChanged();
            return;
        }
    }

    private void updateDownloadSize(float f)
    {
        if (spacesizeList == null || spacesizeList.size() < 3)
        {
            return;
        } else
        {
            ((CommonItem)spacesizeList.get(0)).spaceOccupySize = (new BigDecimal(cachesSize + f)).setScale(0, 4).floatValue();
            ((CommonItem)spacesizeList.get(1)).spaceOccupySize = (new BigDecimal(f)).setScale(1, 4).floatValue();
            downloadSize = ((CommonItem)spacesizeList.get(1)).spaceOccupySize;
            spaceSizeAdapter.notifyDataSetChanged();
            return;
        }
    }

    public void finish()
    {
        Intent intent = new Intent();
        intent.putExtra("curSize", downloadSize + cachesSize);
        setResult(0, intent);
        super.finish();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301d1);
        initDatas();
        findViews();
        initViews();
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            finish();
            return true;
        } else
        {
            return super.onKeyDown(i, keyevent);
        }
    }

    protected void onResume()
    {
        super.onResume();
        refreshDownloadAndCacheSize();
    }




/*
    static MyAsyncTask access$202(UsedSpaceSizeAct usedspacesizeact, MyAsyncTask myasynctask)
    {
        usedspacesizeact.mClearCacheTask = myasynctask;
        return myasynctask;
    }

*/





}
