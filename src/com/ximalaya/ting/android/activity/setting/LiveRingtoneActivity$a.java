// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.LiveHistoryManage;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            LiveRingtoneActivity

class foList extends BaseAdapter
{

    final LiveRingtoneActivity a;
    private List b;

    public int getCount()
    {
        return b.size();
    }

    public Object getItem(int i)
    {
        return b.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        viewgroup = view;
        if (view == null)
        {
            viewgroup = LayoutInflater.from(a.mAppContext).inflate(0x7f030160, null);
        }
        view = (TextView)viewgroup.findViewById(0x7f0a01ed);
        SoundInfo soundinfo = (SoundInfo)b.get(i);
        if (soundinfo != null)
        {
            view.setText(soundinfo.radioName);
        }
        return viewgroup;
    }

    public (LiveRingtoneActivity liveringtoneactivity)
    {
        a = liveringtoneactivity;
        super();
        b = LiveHistoryManage.getInstance(liveringtoneactivity.mAppContext).getSoundInfoList();
    }
}
