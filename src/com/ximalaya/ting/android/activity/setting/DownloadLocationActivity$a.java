// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.UserSpace.CommonItem;
import com.ximalaya.ting.android.util.StorageUtils;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            DownloadLocationActivity

class a extends BaseAdapter
{
    class a
    {

        TextView a;
        TextView b;
        ImageView c;
        final DownloadLocationActivity.a d;

        a()
        {
            d = DownloadLocationActivity.a.this;
            super();
        }
    }


    List a;
    final DownloadLocationActivity b;

    public CommonItem a(int i)
    {
        return (CommonItem)a.get(i);
    }

    public int getCount()
    {
        return a.size();
    }

    public Object getItem(int i)
    {
        return a(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        CommonItem commonitem;
        if (view == null)
        {
            view = b.getLayoutInflater().inflate(0x7f030162, null);
            viewgroup = new a();
            viewgroup.b = (TextView)view.findViewById(0x7f0a0539);
            viewgroup.a = (TextView)view.findViewById(0x7f0a053a);
            viewgroup.c = (ImageView)view.findViewById(0x7f0a053b);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (a)view.getTag();
        }
        commonitem = a(i);
        ((a) (viewgroup)).b.setText(commonitem.name);
        ((a) (viewgroup)).a.setText(StorageUtils.getDownloadDir(commonitem.text));
        if (commonitem.boolValue)
        {
            ((a) (viewgroup)).c.setVisibility(0);
            return view;
        } else
        {
            ((a) (viewgroup)).c.setVisibility(4);
            return view;
        }
    }

    public a.d(DownloadLocationActivity downloadlocationactivity, List list)
    {
        b = downloadlocationactivity;
        super();
        a = list;
    }
}
