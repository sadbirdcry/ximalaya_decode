// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.model.UserSpace.CommonItem;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            UsedSpaceSizeAct

class a extends BaseAdapter
{
    class a
    {

        TextView a;
        TextView b;
        View c;
        final UsedSpaceSizeAct.a d;

        a()
        {
            d = UsedSpaceSizeAct.a.this;
            super();
        }
    }


    List a;
    final UsedSpaceSizeAct b;

    public CommonItem a(int i)
    {
        return (CommonItem)a.get(i);
    }

    public int getCount()
    {
        return a.size();
    }

    public Object getItem(int i)
    {
        return a(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        CommonItem commonitem;
        if (view == null)
        {
            viewgroup = new a();
            view = b.getLayoutInflater().inflate(0x7f0301d2, null);
            viewgroup.a = (TextView)view.findViewById(0x7f0a06e0);
            viewgroup.b = (TextView)view.findViewById(0x7f0a06ca);
            viewgroup.c = view.findViewById(0x7f0a06e1);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (a)view.getTag();
        }
        commonitem = a(i);
        ((a) (viewgroup)).a.setText(commonitem.name);
        ((a) (viewgroup)).b.setText((new StringBuilder()).append(commonitem.spaceOccupySize).append("M").toString());
        switch (i)
        {
        default:
            return view;

        case 0: // '\0'
            ((a) (viewgroup)).c.setVisibility(4);
            return view;

        case 1: // '\001'
            if (commonitem.spaceOccupySize > 0.0F)
            {
                ((a) (viewgroup)).c.setVisibility(0);
                return view;
            } else
            {
                ((a) (viewgroup)).c.setVisibility(4);
                return view;
            }

        case 2: // '\002'
            break;
        }
        if (commonitem.spaceOccupySize > 0.0F)
        {
            ((a) (viewgroup)).c.setVisibility(0);
            return view;
        } else
        {
            ((a) (viewgroup)).c.setVisibility(4);
            return view;
        }
    }

    public a.d(UsedSpaceSizeAct usedspacesizeact, List list)
    {
        b = usedspacesizeact;
        super();
        a = null;
        a = list;
    }
}
