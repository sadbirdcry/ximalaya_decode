// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.ProgressDialog;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.setting.NoRegisterAdapter;
import com.ximalaya.ting.android.adapter.setting.NoRegisterThirdAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.setting.UnregisterList;
import com.ximalaya.ting.android.model.thirdBind.ThirdPartyUserInfo;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            FindFriendActivity, r

private class <init> extends MyAsyncTask
{

    UnregisterList a;
    ProgressDialog b;
    final FindFriendActivity c;

    protected transient String a(String as[])
    {
        Object obj = null;
        if (FindFriendActivity.access$000(c) == 1)
        {
            FindFriendActivity.access$100(c);
            List list = ToolUtil.getSimContacts(c);
            as = obj;
            if (list != null)
            {
                as = obj;
                if (list.size() > 0)
                {
                    a = new UnregisterList();
                    a.tpUerInfo = list;
                    as = "0";
                }
            }
        } else
        {
            String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/friendship/unregister").toString();
            RequestParams requestparams = new RequestParams();
            requestparams.put("type", (new StringBuilder()).append(FindFriendActivity.access$000(c)).append("").toString());
            requestparams.put("pageId", as[0]);
            requestparams.put("pageSize", (new StringBuilder()).append(FindFriendActivity.access$200(c)).append("").toString());
            s = f.a().a(s, requestparams, FindFriendActivity.access$300(c), FindFriendActivity.access$300(c));
            as = obj;
            if (s != null)
            {
                as = JSON.parseObject(s);
                if (as.getInteger("ret").intValue() == 0)
                {
                    if (as.get("data") != null)
                    {
                        a = new UnregisterList();
                        try
                        {
                            a.tpUerInfo = JSON.parseArray(as.get("data").toString(), com/ximalaya/ting/android/model/thirdBind/ThirdPartyUserInfo);
                            a.maxPageId = as.getIntValue("maxPageId");
                            a.pageId = as.getIntValue("pageId");
                        }
                        // Misplaced declaration of an exception variable
                        catch (String as[])
                        {
                            return "\u89E3\u6790\u6570\u636E\u51FA\u9519";
                        }
                        return "0";
                    } else
                    {
                        return "\u6CA1\u6709\u597D\u53CB";
                    }
                } else
                {
                    return as.getString("msg");
                }
            }
        }
        return as;
    }

    protected void a(String s)
    {
        if (c == null || c.isFinishing())
        {
            return;
        }
        if (b != null)
        {
            b.dismiss();
            b = null;
        }
        if (s != null)
        {
            if ("0".equals(s))
            {
                FindFriendActivity.access$400(c).setVisibility(0);
                if (FindFriendActivity.access$000(c) == 1)
                {
                    if (FindFriendActivity.access$500(c) == null)
                    {
                        FindFriendActivity.access$502(c, new NoRegisterAdapter(c, a.tpUerInfo, FindFriendActivity.access$600(c)));
                        FindFriendActivity.access$300(c).setAdapter(FindFriendActivity.access$500(c));
                        return;
                    } else
                    {
                        FindFriendActivity.access$500(c).list = a.tpUerInfo;
                        FindFriendActivity.access$500(c).notifyDataSetChanged();
                        return;
                    }
                }
                if (FindFriendActivity.access$700(c) == null)
                {
                    FindFriendActivity.access$702(c, new NoRegisterThirdAdapter(c, a.tpUerInfo, FindFriendActivity.access$000(c)));
                    FindFriendActivity.access$300(c).setAdapter(FindFriendActivity.access$700(c));
                    return;
                } else
                {
                    FindFriendActivity.access$700(c).list = a.tpUerInfo;
                    FindFriendActivity.access$700(c).notifyDataSetChanged();
                    return;
                }
            } else
            {
                Toast.makeText(c.getApplicationContext(), s, 0).show();
                return;
            }
        } else
        {
            Toast.makeText(c.getApplicationContext(), "\u52A0\u8F7D\u6570\u636E\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5...", 0).show();
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((String[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((String)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        if (FindFriendActivity.access$800(c) == 1)
        {
            b = new MyProgressDialog(c);
            b.setMessage("\u6B63\u5728\u52AA\u529B\u4E3A\u60A8\u52A0\u8F7D\u4FE1\u606F...");
            b.show();
        }
    }

    private (FindFriendActivity findfriendactivity)
    {
        c = findfriendactivity;
        super();
    }

    c(FindFriendActivity findfriendactivity, r r)
    {
        this(findfriendactivity);
    }
}
