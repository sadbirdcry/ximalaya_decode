// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.ProgressDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            a, b, c

public class AboutActivity extends BaseActivity
{

    private static final String url = "http://m.ximalaya.com/help/contact_us";
    public ProgressDialog m_pDialog;
    private TextView txt_version;

    public AboutActivity()
    {
    }

    private void initUI()
    {
        txt_version = (TextView)findViewById(0x7f0a0057);
        try
        {
            PackageInfo packageinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            txt_version.setText((new StringBuilder()).append("V").append(packageinfo.versionName).toString());
        }
        catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception)
        {
            namenotfoundexception.printStackTrace();
        }
        retButton = (ImageView)findViewById(0x7f0a007b);
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        nextButton.setVisibility(4);
        retButton.setOnClickListener(new a(this));
        topTextView.setText(getString(0x7f090144));
        findViewById(0x7f0a0059).setOnClickListener(new b(this));
        findViewById(0x7f0a0058).setOnClickListener(new c(this));
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030000);
        initUI();
    }
}
