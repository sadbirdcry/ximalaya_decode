// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.adapter.feed.FeedRecycleAdapter;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.pulltorefreshgridview.RefreshLoadMoreListView;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            FeedRecycleActivity

class p extends a
{

    final FeedRecycleActivity a;

    p(FeedRecycleActivity feedrecycleactivity)
    {
        a = feedrecycleactivity;
        super();
    }

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, FeedRecycleActivity.access$400(a));
    }

    public void onNetError(int i, String s)
    {
        if (!a.isFinishing())
        {
            FeedRecycleActivity.access$400(a).onRefreshComplete();
        }
        FeedRecycleActivity.access$102(a, false);
        Toast.makeText(FeedRecycleActivity.access$700(a), "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
    }

    public void onSuccess(String s)
    {
        FeedRecycleActivity.access$102(a, false);
        if (s == null)
        {
            FeedRecycleActivity.access$400(a).onRefreshComplete();
            FeedRecycleActivity.access$400(a).finishLoadingMore();
            return;
        }
        try
        {
            s = (FeedRecycleActivity.FeedRecycleListModel)JSON.parseObject(s, com/ximalaya/ting/android/activity/setting/FeedRecycleActivity$FeedRecycleListModel);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e(s);
            s = null;
        }
        if (s == null || ((FeedRecycleActivity.FeedRecycleListModel) (s)).ret != 0)
        {
            FeedRecycleActivity.access$400(a).onRefreshComplete();
            FeedRecycleActivity.access$400(a).setHasMore(false);
            FeedRecycleActivity.access$202(a, false);
            return;
        }
        if (((FeedRecycleActivity.FeedRecycleListModel) (s)).datas == null || ((FeedRecycleActivity.FeedRecycleListModel) (s)).datas.size() < 15)
        {
            FeedRecycleActivity.access$202(a, false);
            if (FeedRecycleActivity.access$600(a).isEmpty())
            {
                FeedRecycleActivity.access$400(a).setHasMore(false);
            } else
            {
                FeedRecycleActivity.access$400(a).setHasMoreNoFooterView(false);
            }
        } else
        {
            FeedRecycleActivity.access$202(a, true);
            FeedRecycleActivity.access$400(a).setHasMore(true);
        }
        FeedRecycleActivity.access$600(a).addAll(((FeedRecycleActivity.FeedRecycleListModel) (s)).datas);
        FeedRecycleActivity.access$500(a).notifyDataSetChanged();
        FeedRecycleActivity.access$400(a).onRefreshComplete();
    }
}
