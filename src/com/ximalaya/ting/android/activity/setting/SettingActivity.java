// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.adapter.setting.AboutSettingAdapter;
import com.ximalaya.ting.android.adapter.setting.AlarmAdapter;
import com.ximalaya.ting.android.adapter.setting.OtherSettingAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.check_version.CheckVersionResult;
import com.ximalaya.ting.android.model.check_version.MsgCarry;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.Session;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.setting.CornerListView;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            ay, aq, as, at, 
//            au, av, aw, ax, 
//            ar

public class SettingActivity extends BaseActivity
{
    public class UpgradeTask extends MyAsyncTask
    {

        final SettingActivity this$0;

        protected transient CheckVersionResult doInBackground(Void avoid[])
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/version").toString();
            RequestParams requestparams = new RequestParams();
            avoid = f.a().a(avoid, requestparams, null, null);
            if (avoid == null)
            {
                break MISSING_BLOCK_LABEL_67;
            }
            int i;
            avoid = (CheckVersionResult)JSON.parseObject(avoid, com/ximalaya/ting/android/model/check_version/CheckVersionResult);
            i = ((CheckVersionResult) (avoid)).ret;
            if (i == 0)
            {
                return avoid;
            }
            break MISSING_BLOCK_LABEL_67;
            avoid;
            return null;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(CheckVersionResult checkversionresult)
        {
            if (this == null || isFinishing())
            {
                return;
            }
            Message message = new Message();
            if (checkversionresult != null && checkversionresult.version != null && !"".equalsIgnoreCase(checkversionresult.version))
            {
                MsgCarry msgcarry;
                if (checkversionresult.forceUpdate)
                {
                    message.what = 1;
                } else
                {
                    message.what = 0;
                }
                msgcarry = new MsgCarry(checkversionresult.version, checkversionresult.download, 0);
                msgcarry.setInfo(checkversionresult.upgradeDesc);
                message.obj = msgcarry;
            } else
            {
                message.what = 163;
            }
            uphandler.sendMessage(message);
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((CheckVersionResult)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        public UpgradeTask()
        {
            this$0 = SettingActivity.this;
            super();
        }
    }


    private static final int GO_LOCATION = 163;
    private static final int GO_SPACESIZE = 162;
    public static final String LOGIN_FROM_SETTING = "login_from_setting";
    private AboutSettingAdapter aboutAdapter;
    private CornerListView aboutListView;
    private AlarmAdapter alarAdapter;
    private CornerListView alarmListView;
    private TextView login_clike;
    public ProgressDialog m_pDialog;
    private OtherSettingAdapter otherAdapter;
    private List otherInfos;
    private CornerListView otherListView;
    final Handler uphandler = new ay(this);

    public SettingActivity()
    {
    }

    private void askVersionUpdate(String s, String s1, String s2)
    {
        String s4 = getApkName(s);
        DialogBuilder dialogbuilder = new DialogBuilder(this);
        String s3 = s2;
        if (TextUtils.isEmpty(s2))
        {
            s3 = (new StringBuilder()).append("\u559C\u9A6C\u62C9\u96C5").append(s).append("\u7248\u5DF2\u7ECF\u53D1\u5E03,\u5FEB\u53BB\u4F53\u9A8C\u65B0\u529F\u80FD\u5427").toString();
        }
        dialogbuilder.setMessage(s3).setOkBtn("\u5347\u7EA7\u65B0\u7248", new aq(this, s4, s1)).setCancelBtn("\u4E0B\u6B21\u518D\u8BF4").showConfirm();
    }

    private String getApkName(String s)
    {
        return (new StringBuilder()).append(getResources().getString(0x7f09009e)).append(s).toString();
    }

    private void initData()
    {
    }

    private void initListener()
    {
        retButton.setOnClickListener(new as(this));
        login_clike.setOnClickListener(new at(this));
        alarmListView.setOnItemClickListener(new au(this));
        otherListView.setOnItemClickListener(new av(this));
        aboutListView.setOnItemClickListener(new aw(this));
    }

    private void initUI()
    {
        switchOnlineCode();
        switchEnableSoundAd();
        retButton = (ImageView)findViewById(0x7f0a007b);
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        nextButton.setVisibility(4);
        topTextView.setText("\u8BBE\u7F6E");
        login_clike = (TextView)findViewById(0x7f0a06c8);
        if (loginInfoModel == null)
        {
            login_clike.setVisibility(8);
        }
        alarmListView = (CornerListView)findViewById(0x7f0a06c5);
        Object obj;
        String as2[];
        int i;
        if (!FreeFlowUtil.getInstance().isChinaUnicomUser() || !FreeFlowUtil.unicomFreeFlowSwitchOn())
        {
            obj = getResources().getStringArray(0x7f0c0012);
        } else
        {
            obj = getResources().getStringArray(0x7f0c0013);
        }
        alarAdapter = new AlarmAdapter(this, ((String []) (obj)));
        alarmListView.setAdapter(alarAdapter);
        as2 = getResources().getStringArray(0x7f0c0016);
        otherInfos = new ArrayList();
        i = 0;
        while (i < as2.length) 
        {
            if (i == 0)
            {
                obj = new SettingInfo(as2[i], SharedPreferencesUtil.getInstance(this).getBoolean("isRadioAlert", true));
            } else
            if (i == 1)
            {
                obj = new SettingInfo(as2[i], SharedPreferencesUtil.getInstance(this).getBoolean("is_download_enabled_in_3g", false));
            } else
            if (i == 2)
            {
                obj = new SettingInfo(as2[i], false);
                String as1[];
                long l;
                long l1;
                if (Session.getSession().containsKey("TOTAL_DOWNLOAD_SIZE"))
                {
                    l = ((Long)Session.getSession().get("TOTAL_DOWNLOAD_SIZE")).longValue();
                } else
                {
                    l = 0L;
                }
                if (Session.getSession().containsKey("TOTAL_CACHE_SIZE"))
                {
                    l1 = ((Long)Session.getSession().get("TOTAL_CACHE_SIZE")).longValue();
                } else
                {
                    l1 = 0L;
                }
                obj.spaceOccupySize = l + l1;
            } else
            {
                obj = new SettingInfo(as2[i], false);
            }
            otherInfos.add(obj);
            i++;
        }
        otherListView = (CornerListView)findViewById(0x7f0a06c6);
        otherAdapter = new OtherSettingAdapter(this, otherInfos);
        otherListView.setAdapter(otherAdapter);
        as1 = getResources().getStringArray(0x7f0c0018);
        aboutListView = (CornerListView)findViewById(0x7f0a06c7);
        aboutAdapter = new AboutSettingAdapter(this, as1);
        aboutListView.setAdapter(aboutAdapter);
    }

    private void logout(View view)
    {
        ((MyApplication)getApplication()).a(this, view);
        finish();
    }

    private void onCanceled()
    {
        (new DialogBuilder(this)).setMessage("\u786E\u5B9A\u8981\u9000\u51FA\u767B\u5F55\uFF1F").setOkBtn(new ax(this)).showConfirm();
    }

    private void refreshDownloadAndCacheSize()
    {
        (new ar(this)).myexec(new Void[0]);
    }

    private void refreshDownloadLocation()
    {
        String s;
        int i;
        i = 0;
        s = StorageUtils.getCurrentDownloadLocation();
        if (Utilities.isNotBlank(s)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        String s1;
        String s2;
        s2 = s.substring(0, s.indexOf("/ting"));
        s1 = "\u624B\u673A";
_L4:
        s = s1;
        if (i >= StorageUtils.count)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!Utilities.isNotBlank(s2) || !s2.equals(StorageUtils.paths[i]))
        {
            break MISSING_BLOCK_LABEL_114;
        }
        s = StorageUtils.labels[i];
        if (otherInfos == null || otherInfos.size() < 4) goto _L1; else goto _L3
_L3:
        ((SettingInfo)otherInfos.get(3)).textWake = s;
        otherAdapter.notifyDataSetChanged();
        return;
        i++;
          goto _L4
    }

    private void switchEnableSoundAd()
    {
        RelativeLayout relativelayout = (RelativeLayout)findViewById(0x7f0a06c4);
        Object obj = (SwitchButton)relativelayout.findViewById(0x7f0a06d6);
        obj = (TextView)relativelayout.findViewById(0x7f0a06d5);
        relativelayout.setVisibility(8);
    }

    private void switchOnlineCode()
    {
        RelativeLayout relativelayout = (RelativeLayout)findViewById(0x7f0a0412);
        Object obj = (SwitchButton)findViewById(0x7f0a01ee);
        obj = (TextView)findViewById(0x7f0a01ed);
        relativelayout.setVisibility(8);
    }

    private void updateProgressDialog(int i)
    {
        if (m_pDialog != null && i >= 0 && i <= 100)
        {
            m_pDialog.setProgress(i);
            if (100 == i)
            {
                uphandler.sendEmptyMessage(3);
                return;
            }
        }
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        if (this != null && !isFinishing()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (i != 1)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == 1)
        {
            logout(rootView);
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        if (i != 162)
        {
            break; /* Loop/switch isn't completed */
        }
        if (intent != null)
        {
            float f = (new BigDecimal(intent.getFloatExtra("curSize", 0.0F))).setScale(1, 4).floatValue();
            ((SettingInfo)otherInfos.get(2)).textWake = (new StringBuilder()).append(f).append("M").toString();
            otherAdapter.notifyDataSetChanged();
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
        if (i != 163)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (intent != null)
        {
            intent = intent.getStringExtra("curLocation");
            ((SettingInfo)otherInfos.get(3)).textWake = intent;
            otherAdapter.notifyDataSetChanged();
            return;
        }
        continue; /* Loop/switch isn't completed */
        if (i != 9527) goto _L1; else goto _L5
_L5:
        Logger.log((new StringBuilder()).append("requestCode = ").append(i).append(", resultCode = ").append(j).toString());
        if (j == -1 && NetworkUtils.getNetType(this) == 0)
        {
            FreeFlowUtil.getInstance().useFreeFlow();
            return;
        }
        if (true) goto _L1; else goto _L6
_L6:
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301c3);
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        initUI();
        initData();
        initListener();
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            setResult(0);
            finish();
        }
        return false;
    }

    protected void onResume()
    {
        super.onResume();
        refreshDownloadLocation();
        refreshDownloadAndCacheSize();
    }







}
