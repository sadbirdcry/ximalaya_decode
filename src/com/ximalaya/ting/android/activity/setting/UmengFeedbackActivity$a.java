// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.umeng.fb.model.Conversation;
import com.umeng.fb.model.Reply;
import com.ximalaya.ting.android.util.TimeHelper;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            UmengFeedbackActivity

class a extends BaseAdapter
{
    class a
    {

        TextView a;
        ProgressBar b;
        ImageView c;
        TextView d;
        final UmengFeedbackActivity.a e;

        a()
        {
            e = UmengFeedbackActivity.a.this;
            super();
        }
    }


    final UmengFeedbackActivity a;

    public int getCount()
    {
        return UmengFeedbackActivity.access$400(a).getReplyList().size();
    }

    public Object getItem(int i)
    {
        return UmengFeedbackActivity.access$400(a).getReplyList().get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public int getItemViewType(int i)
    {
        return !"dev_reply".equals(((Reply)UmengFeedbackActivity.access$400(a).getReplyList().get(i)).type) ? 0 : 1;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        Reply reply;
label0:
        {
            reply = (Reply)UmengFeedbackActivity.access$400(a).getReplyList().get(i);
            if (view == null)
            {
                if (getItemViewType(i) == 1)
                {
                    view = LayoutInflater.from(a).inflate(0x7f030144, null);
                } else
                {
                    view = LayoutInflater.from(a).inflate(0x7f030145, null);
                }
                viewgroup = new a();
                viewgroup.a = (TextView)view.findViewById(0x7f0a04f5);
                viewgroup.b = (ProgressBar)view.findViewById(0x7f0a04f7);
                viewgroup.c = (ImageView)view.findViewById(0x7f0a04f6);
                viewgroup.d = (TextView)view.findViewById(0x7f0a04f3);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (a)view.getTag();
            }
            ((a) (viewgroup)).a.setText(reply.content);
            if (!"dev_reply".equals(reply.type))
            {
                if ("not_sent".equals(reply.status))
                {
                    ((a) (viewgroup)).c.setVisibility(0);
                } else
                {
                    ((a) (viewgroup)).c.setVisibility(8);
                }
                if ("sending".equals(reply.status))
                {
                    ((a) (viewgroup)).b.setVisibility(0);
                } else
                {
                    ((a) (viewgroup)).b.setVisibility(8);
                }
            }
            if (i + 1 <= UmengFeedbackActivity.access$400(a).getReplyList().size())
            {
                if (i != 0)
                {
                    break label0;
                }
                ((a) (viewgroup)).d.setText(TimeHelper.countTime2(reply.created_at));
                ((a) (viewgroup)).d.setVisibility(0);
            }
            return view;
        }
        Reply reply1 = (Reply)UmengFeedbackActivity.access$400(a).getReplyList().get(i - 1);
        if (reply.created_at - reply1.created_at > 0x186a0L)
        {
            ((a) (viewgroup)).d.setText(TimeHelper.countTime2(reply.created_at));
            ((a) (viewgroup)).d.setVisibility(0);
            return view;
        } else
        {
            ((a) (viewgroup)).d.setVisibility(8);
            return view;
        }
    }

    public int getViewTypeCount()
    {
        return 2;
    }

    a.e(UmengFeedbackActivity umengfeedbackactivity)
    {
        a = umengfeedbackactivity;
        super();
    }
}
