// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.adapter.setting.BindListAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.model.thirdBind.ResponseFindBindStatusInfo;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            BindActivity

class d extends MyAsyncTask
{

    SharedPreferencesUtil a;
    final BindActivity b;

    d(BindActivity bindactivity)
    {
        b = bindactivity;
        super();
        a = null;
    }

    protected transient ResponseFindBindStatusInfo a(Void avoid[])
    {
        Object obj;
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/auth/bindStatus").toString();
        obj = new RequestParams();
        obj = f.a().a(avoid, ((RequestParams) (obj)), BindActivity.access$000(b), BindActivity.access$000(b));
        avoid = new ResponseFindBindStatusInfo();
        if (obj == null) goto _L2; else goto _L1
_L1:
        Exception exception;
        JSONObject jsonobject;
        int i;
        try
        {
            jsonobject = JSON.parseObject(((String) (obj)));
            i = jsonobject.getInteger("ret").intValue();
        }
        catch (Exception exception1)
        {
            return avoid;
        }
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_119;
        }
        avoid.list = JSON.parseArray(jsonobject.get("data").toString(), com/ximalaya/ting/android/model/personal_setting/ThirdPartyUserInfo);
        avoid.ret = 0;
        a.saveString("BINDSETTINGINFO", ((String) (obj)));
_L4:
        return avoid;
        exception;
        avoid.ret = -1;
        avoid.msg = "";
        return avoid;
_L2:
        exception = a.getString("BINDSETTINGINFO");
        if (TextUtils.isEmpty(exception))
        {
            break MISSING_BLOCK_LABEL_212;
        }
        exception = JSON.parseObject(exception);
        i = exception.getInteger("ret").intValue();
        if (i != 0) goto _L4; else goto _L3
_L3:
        avoid.list = JSON.parseArray(exception.get("data").toString(), com/ximalaya/ting/android/model/personal_setting/ThirdPartyUserInfo);
        avoid.ret = 0;
        return avoid;
        exception;
        avoid.ret = -1;
        avoid.msg = "";
        return avoid;
        avoid.ret = -1;
        avoid.msg = "";
        return avoid;
    }

    protected void a(ResponseFindBindStatusInfo responsefindbindstatusinfo)
    {
        while (b == null || b.isFinishing() || responsefindbindstatusinfo.ret != 0 || responsefindbindstatusinfo.list.size() <= 0) 
        {
            return;
        }
        responsefindbindstatusinfo = responsefindbindstatusinfo.list.iterator();
        do
        {
            if (responsefindbindstatusinfo.hasNext())
            {
                ThirdPartyUserInfo thirdpartyuserinfo = (ThirdPartyUserInfo)responsefindbindstatusinfo.next();
                switch (Integer.valueOf(thirdpartyuserinfo.thirdpartyId).intValue())
                {
                case 1: // '\001'
                    ((SettingInfo)BindActivity.access$100(b).get(3)).isSetting = true;
                    ((SettingInfo)BindActivity.access$100(b).get(3)).isExpired = thirdpartyuserinfo.isExpired;
                    ((SettingInfo)BindActivity.access$100(b).get(3)).textWake = (new StringBuilder()).append("(").append(thirdpartyuserinfo.thirdpartyNickname).append(")").toString();
                    break;

                case 2: // '\002'
                    ((SettingInfo)BindActivity.access$100(b).get(2)).isSetting = true;
                    ((SettingInfo)BindActivity.access$100(b).get(2)).isExpired = thirdpartyuserinfo.isExpired;
                    ((SettingInfo)BindActivity.access$100(b).get(2)).textWake = (new StringBuilder()).append("(").append(thirdpartyuserinfo.thirdpartyNickname).append(")").toString();
                    break;

                case 3: // '\003'
                    ((SettingInfo)BindActivity.access$100(b).get(4)).isSetting = true;
                    ((SettingInfo)BindActivity.access$100(b).get(4)).isExpired = thirdpartyuserinfo.isExpired;
                    ((SettingInfo)BindActivity.access$100(b).get(4)).textWake = (new StringBuilder()).append("(").append(thirdpartyuserinfo.thirdpartyNickname).append(")").toString();
                    break;
                }
            } else
            {
                BindActivity.access$200(b).notifyDataSetChanged();
                return;
            }
        } while (true);
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((ResponseFindBindStatusInfo)obj);
    }

    protected void onPreExecute()
    {
        a = SharedPreferencesUtil.getInstance(BindActivity.access$300(b));
    }
}
