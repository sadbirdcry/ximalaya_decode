// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.BaseFragmentActivity;
import com.ximalaya.ting.android.adapter.setting.WakeAdapter;
import com.ximalaya.ting.android.broadcast.AlarmReceiver;
import com.ximalaya.ting.android.broadcast.AlarmReceiver2;
import com.ximalaya.ting.android.library.service.DownloadLiteManager;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.alarm.Alarm;
import com.ximalaya.ting.android.model.alarm.Alarms;
import com.ximalaya.ting.android.model.setting.DaysOfWeek;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MD5;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.setting.CornerListView;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            bu, bx, by, bz, 
//            ce, ca, cb, cd, 
//            cc, bw, bv

public class WakeUpSettingActivity extends BaseFragmentActivity
    implements android.app.TimePickerDialog.OnTimeSetListener
{

    public static final int BUILDIN_RINGTONS[] = {
        0x7f060002, 0x7f060003, 0x7f060004, 0x7f060005, 0x7f060006, 0x7f060007, 0x7f060008, 0x7f060009
    };
    public static final String FROM = "from";
    public static final int FROM_LIVE_PLAYING_SOUND = 2;
    public static final int FROM_PLAYING_SOUND = 1;
    private static Alarm sAlarm;
    private static DownloadLiteManager sDownloadLiteManager;
    private int alarmHour;
    private int alarmMinute;
    private int from;
    private boolean isAlarm;
    private List list;
    private Context mContext;
    private com.ximalaya.ting.android.library.service.DownloadLiteManager.DownloadCallback mDownloadCallback;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.MediaPlayerCompleteHandler mMediaPlayerCompleteHandler;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.MediaPlayerErrorHandler mMediaPlayerErrorHandler;
    private int mOnlineSoundsPage;
    private Handler mUpdateUIHandler;
    private AlarmManager malarm;
    private MediaPlayer mp;
    private WakeAdapter wakeAdapter;
    private CornerListView wakeListView;

    public WakeUpSettingActivity()
    {
        isAlarm = false;
        mOnlineSoundsPage = 0;
        mDownloadCallback = new bu(this);
        mUpdateUIHandler = new bx(this, Looper.getMainLooper());
        mMediaPlayerErrorHandler = new by(this);
        mMediaPlayerCompleteHandler = new bz(this);
    }

    private void addHelpButtonToTitleBarOrNot()
    {
        int i;
        int j;
        if (PackageUtil.isMIUI())
        {
            j = 1;
        } else
        {
            j = 0;
        }
        i = j;
        if (PackageUtil.isAppInstalled(this, "com.qihoo360.mobilesafe"))
        {
            i = j | 2;
        }
        j = i;
        if (PackageUtil.isAppInstalled(this, "com.tencent.qqpimsecure"))
        {
            j = i | 4;
        }
        i = j;
        if (PackageUtil.isAppInstalled(this, "com.lbe.security"))
        {
            i = j | 8;
        }
        j = i;
        if (PackageUtil.isAppInstalled(this, "cn.opda.a.phonoalbumshoushou"))
        {
            j = i | 0x10;
        }
        if (PackageUtil.isAppInstalled(this, "com.cleanmaster.mguard_cn"))
        {
            i = j | 0x20;
        } else
        {
            i = j;
        }
        if (i != 0)
        {
            ViewGroup viewgroup = (ViewGroup)findViewById(0x7f0a0054);
            android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-2, -2);
            layoutparams.addRule(11);
            layoutparams.addRule(15);
            layoutparams.rightMargin = Utilities.dip2px(this, 8F);
            Button button = new Button(this);
            button.setText(0x7f090086);
            button.setBackgroundColor(0);
            button.setTextSize(14F);
            button.setTextColor(Color.parseColor("#ff6000"));
            button.setOnClickListener(new ce(this, i));
            viewgroup.addView(button, layoutparams);
        }
    }

    private MediaPlayer create(Context context, int i, int j)
    {
        context = context.getResources().openRawResourceFd(j);
        if (context == null)
        {
            return null;
        }
        MediaPlayer mediaplayer;
        mediaplayer = new MediaPlayer();
        mediaplayer.setDataSource(context.getFileDescriptor(), context.getStartOffset(), context.getLength());
        context.close();
        mediaplayer.setLooping(true);
        mediaplayer.setAudioStreamType(i);
        mediaplayer.prepare();
        return mediaplayer;
        context;
        Log.d("Alarm", "create failed:", context);
_L2:
        return null;
        context;
        Log.d("Alarm", "create failed:", context);
        continue; /* Loop/switch isn't completed */
        context;
        Log.d("Alarm", "create failed:", context);
        if (true) goto _L2; else goto _L1
_L1:
    }

    private MediaPlayer create(Context context, int i, Uri uri)
    {
        MediaPlayer mediaplayer;
        mediaplayer = new MediaPlayer();
        mediaplayer.setDataSource(context, uri);
        mediaplayer.setLooping(true);
        mediaplayer.setAudioStreamType(i);
        mediaplayer.prepare();
        return mediaplayer;
        context;
        Log.d("alarm", "create failed:", context);
_L2:
        return null;
        context;
        Log.d("alarm", "create failed:", context);
        continue; /* Loop/switch isn't completed */
        context;
        Log.d("alarm", "create failed:", context);
        if (true) goto _L2; else goto _L1
_L1:
    }

    private void doPlayRingtone()
    {
        ToolUtil.cancelNotification(mContext, 6);
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mContext);
        String s = sharedpreferencesutil.getString("alarm_ringtone_location");
        String s1 = sharedpreferencesutil.getString("alarm_ringtone_download_url");
        int i = sharedpreferencesutil.getInt("type", -1);
        statisticAlarmRing();
        ((AudioManager)getSystemService("audio")).requestAudioFocus(null, 4, 2);
        switch (i)
        {
        default:
            mp = create(this, 4, 0x7f060001);
            mp.start();
            return;

        case 2: // '\002'
            int j = Integer.valueOf(s1.substring("buildin://".length())).intValue();
            if (j == -1)
            {
                mp = create(this, 4, 0x7f060001);
            } else
            {
                mp = create(this, 4, BUILDIN_RINGTONS[j]);
            }
            mp.start();
            return;

        case 0: // '\0'
            if (s == null || s.equals("") || s1 == null || s1.equals(""))
            {
                mp = create(this, 4, 0x7f060001);
            } else
            {
                File file = new File(s, ToolUtil.md5(s1));
                if (file.exists())
                {
                    mp = create(this, 4, Uri.fromFile(file));
                } else
                {
                    mp = create(this, 4, 0x7f060001);
                }
            }
            mp.start();
            return;

        case 1: // '\001'
            playOnlineSoundList(s1);
            return;

        case 3: // '\003'
            playLiveRadio(s1, Integer.parseInt(s));
            return;
        }
    }

    private void downloadSoundAsRington(SoundInfo soundinfo)
    {
        if (TextUtils.isEmpty(soundinfo.playUrl32))
        {
            showToast(getString(0x7f09008c));
            return;
        } else
        {
            sAlarm = new Alarm();
            sAlarm.mLocalFilePath = (new StringBuilder()).append(a.ak).append("/").append(MD5.md5(soundinfo.downLoadUrl)).toString();
            sAlarm.mUrl = soundinfo.playUrl32;
            sAlarm.mTitle = soundinfo.title;
            sAlarm.mLocationDir = a.ak;
            sDownloadLiteManager = DownloadLiteManager.getInstance();
            sDownloadLiteManager.setCallback(mDownloadCallback);
            sDownloadLiteManager.download(soundinfo.playUrl32, a.ak, MD5.md5(soundinfo.playUrl32), 120, mUpdateUIHandler);
            Alarms.setAlarmSound(this, sAlarm.mUrl, sAlarm.mLocationDir, sAlarm.mTitle, 0);
            updateRingtonTitle();
            return;
        }
    }

    private Intent getAlarmIntent()
    {
        if (PackageUtil.isMeizu())
        {
            return new Intent(this, com/ximalaya/ting/android/broadcast/AlarmReceiver);
        } else
        {
            return new Intent(this, com/ximalaya/ting/android/broadcast/AlarmReceiver2);
        }
    }

    private String getEllipsisStr(String s)
    {
        StringBuilder stringbuilder = new StringBuilder();
        if (s.length() > 10)
        {
            stringbuilder.append(s.substring(0, 9));
            stringbuilder.append("...");
        } else
        {
            stringbuilder.append(s);
        }
        return stringbuilder.toString();
    }

    private void getParentInfo()
    {
        Intent intent = getIntent();
        if (intent.getExtras() != null)
        {
            if ("alarm".equals(intent.getExtras().getString("flag")))
            {
                isAlarm = true;
            }
            from = intent.getIntExtra("from", -1);
        }
    }

    private String getRepeat()
    {
        String s2;
        int i;
        int j;
        j = SharedPreferencesUtil.getInstance(mContext).getInt("repeat_week_days", 0);
        s2 = "";
        i = 0;
_L12:
        String s;
        if (i >= 7)
        {
            break MISSING_BLOCK_LABEL_263;
        }
        s = s2;
        if (!DaysOfWeek.isSet(j, i)) goto _L2; else goto _L1
_L1:
        i;
        JVM INSTR tableswitch 0 6: default 84
    //                   0 95
    //                   1 119
    //                   2 143
    //                   3 167
    //                   4 191
    //                   5 215
    //                   6 239;
           goto _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10
_L10:
        break MISSING_BLOCK_LABEL_239;
_L4:
        break; /* Loop/switch isn't completed */
_L3:
        s = s2;
_L2:
        i++;
        s2 = s;
        if (true) goto _L12; else goto _L11
_L11:
        s = (new StringBuilder()).append(s2).append("\u661F\u671F\u4E00").toString();
          goto _L2
_L5:
        s = (new StringBuilder()).append(s2).append("\u661F\u671F\u4E8C").toString();
          goto _L2
_L6:
        s = (new StringBuilder()).append(s2).append("\u661F\u671F\u4E09").toString();
          goto _L2
_L7:
        s = (new StringBuilder()).append(s2).append("\u661F\u671F\u56DB").toString();
          goto _L2
_L8:
        s = (new StringBuilder()).append(s2).append("\u661F\u671F\u4E94").toString();
          goto _L2
_L9:
        s = (new StringBuilder()).append(s2).append("\u661F\u671F\u516D").toString();
          goto _L2
        s = (new StringBuilder()).append(s2).append("\u661F\u671F\u65E5").toString();
          goto _L2
        String s1;
        if (j == 31)
        {
            s1 = "\u5DE5\u4F5C\u65E5";
        } else
        {
            if (j == 96)
            {
                return "\u5468\u672B";
            }
            if (j == 127)
            {
                return "\u6BCF\u5929";
            }
            s1 = s2;
            if (s2.length() > 6)
            {
                return (new StringBuilder()).append(s2.substring(0, 6)).append("...").toString();
            }
        }
        return s1;
    }

    private String getRingtoneTitle()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mContext);
        if (sharedpreferencesutil.contains("alarm_ringtone_location") && sharedpreferencesutil.contains("alarm_ringtone_download_url"))
        {
            return getEllipsisStr(sharedpreferencesutil.getString("alarm_ringtone_title"));
        } else
        {
            return "\u9ED8\u8BA4";
        }
    }

    private String getTime()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mContext);
        if (sharedpreferencesutil.contains("alarm_hour") && sharedpreferencesutil.contains("alarm_minute"))
        {
            return toTime(sharedpreferencesutil.getInt("alarm_hour", 0), sharedpreferencesutil.getInt("alarm_minute", 0));
        } else
        {
            return "";
        }
    }

    private void initUI()
    {
        String as[];
        int i;
        boolean flag1;
label0:
        {
            boolean flag = false;
            malarm = (AlarmManager)getSystemService("alarm");
            alarmHour = 0;
            alarmMinute = 0;
            retButton = (ImageView)findViewById(0x7f0a007b);
            topTextView = (TextView)findViewById(0x7f0a00ae);
            nextButton = (ImageView)findViewById(0x7f0a0710);
            nextButton.setVisibility(4);
            retButton.setOnClickListener(new ca(this));
            topTextView.setText(getString(0x7f0900fc));
            addHelpButtonToTitleBarOrNot();
            list = new ArrayList();
            as = getResources().getStringArray(0x7f0c0011);
            SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mContext);
            boolean flag2 = sharedpreferencesutil.getBoolean("isOnForWake", false);
            flag1 = flag2;
            i = ((flag) ? 1 : 0);
            if (flag2)
            {
                break label0;
            }
            if (from != 1)
            {
                flag1 = flag2;
                i = ((flag) ? 1 : 0);
                if (from != 2)
                {
                    break label0;
                }
            }
            sharedpreferencesutil.saveBoolean("isOnForWake", true);
            flag1 = true;
            i = ((flag) ? 1 : 0);
        }
        while (i < 4) 
        {
            SettingInfo settinginfo = new SettingInfo();
            if (i == 0)
            {
                settinginfo.setSetting(flag1);
            }
            settinginfo.setNameWake(as[i]);
            if (i == 3)
            {
                settinginfo.setTextWake(getRingtoneTitle());
            } else
            if (i == 2)
            {
                settinginfo.setTextWake(getTime());
            } else
            if (i == 1)
            {
                settinginfo.setTextWake(getRepeat());
            } else
            {
                settinginfo.setTextWake("");
            }
            list.add(settinginfo);
            i++;
        }
        wakeListView = (CornerListView)findViewById(0x7f0a0746);
        wakeAdapter = new WakeAdapter(this, list);
        wakeListView.setAdapter(wakeAdapter);
        wakeListView.setOnItemClickListener(new cb(this));
        playAlarmAndShowDialog();
    }

    private void onActivityStart()
    {
        doPlayRingtone();
        ToolUtil.makeAlarmNotification(mContext, mContext.getString(0x7f09012d), mContext.getString(0x7f09012e), mContext.getString(0x7f09012f));
        ToolUtil.setNextAlarm(this);
    }

    private void playAlarmAndShowDialog()
    {
        if (isAlarm)
        {
            isAlarm = false;
            onActivityStart();
            (new android.app.AlertDialog.Builder(this)).setIcon(0x7f020598).setTitle("\u559C\u9A6C\u62C9\u96C5").setMessage("\u8D77\u5E8A\u5566\uFF01\u559C\u9A6C\u62C9\u96C5\u5DF2\u7ECF\u4E3A\u4F60\u51C6\u5907\u4E86\u4E30\u5BCC\u7684\u6536\u542C\u5185\u5BB9\uFF01").setCancelable(false).setPositiveButton(0x7f0901b3, new cd(this)).setNegativeButton(0x7f0901b4, new cc(this)).show();
        }
    }

    private void playLiveRadio(String s, int i)
    {
        if (NetworkUtils.isNetworkAvaliable(this))
        {
            (new bw(this, i, s)).execute(new Void[0]);
            return;
        } else
        {
            mp = create(this, 4, 0x7f060001);
            mp.setLooping(true);
            mp.start();
            return;
        }
    }

    private void playOnlineSoundList(String s)
    {
        if (NetworkUtils.isNetworkAvaliable(this))
        {
            (new bv(this, s)).execute(new Void[0]);
            return;
        } else
        {
            mp = create(this, 4, 0x7f060001);
            mp.setLooping(true);
            mp.start();
            return;
        }
    }

    private void setLivePlayingSoundAsRington()
    {
        SoundInfo soundinfo;
label0:
        {
            if (LocalMediaService.getInstance() != null)
            {
                soundinfo = PlayListControl.getPlayListManager().getCurSound();
                if (soundinfo != null && soundinfo.category != 0)
                {
                    break label0;
                }
            }
            return;
        }
        String s = Integer.toString(soundinfo.radioId);
        Alarms.setAlarmSound(this, soundinfo.liveUrl, s, soundinfo.radioName, 3);
    }

    private void setPlayingSoundAsRington()
    {
        if (LocalMediaService.getInstance() != null)
        {
            SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
            if (DownloadHandler.getInstance(this).isDownloadCompleted(soundinfo))
            {
                String s = StorageUtils.getCurrentDownloadLocation();
                Alarms.setAlarmSound(this, soundinfo.downLoadUrl, s, soundinfo.title, 0);
                updateRingtonTitle();
            } else
            {
                if (sAlarm == null)
                {
                    downloadSoundAsRington(soundinfo);
                    return;
                }
                if (!soundinfo.playUrl32.equals(sAlarm.mUrl))
                {
                    DownloadLiteManager.getInstance().cancel();
                    downloadSoundAsRington(soundinfo);
                    return;
                }
            }
        }
    }

    private void statisticAlarmRing()
    {
        Object obj = SharedPreferencesUtil.getInstance(mContext);
        int j = ((SharedPreferencesUtil) (obj)).getInt("alarm_continue_ring_time", 0);
        long l = ((SharedPreferencesUtil) (obj)).getLong("alarm_last_ring_millisecond");
        long l1 = System.currentTimeMillis();
        if (TimeHelper.isToday(l))
        {
            return;
        }
        if (TimeHelper.isYesterday(l))
        {
            int i = j;
            if (j <= 7)
            {
                i = j + 1;
            }
            ((SharedPreferencesUtil) (obj)).saveInt("alarm_continue_ring_time", i);
            ((SharedPreferencesUtil) (obj)).saveLong("alarm_last_ring_millisecond", l1);
            Context context = mContext;
            if (i > 7)
            {
                obj = "7+";
            } else
            {
                obj = (new StringBuilder()).append(i).append("").toString();
            }
            ToolUtil.onEvent(context, "Alarmclock_Ring", ((String) (obj)));
            return;
        } else
        {
            ((SharedPreferencesUtil) (obj)).saveInt("alarm_continue_ring_time", 1);
            ((SharedPreferencesUtil) (obj)).saveLong("alarm_last_ring_millisecond", l1);
            ToolUtil.onEvent(mContext, "Alarmclock_Ring", (new StringBuilder()).append(1).append("").toString());
            return;
        }
    }

    private void stopAlarm()
    {
        if (mp != null && mp.isPlaying())
        {
            mp.stop();
            mp.release();
            mp = null;
            ((AudioManager)getSystemService("audio")).abandonAudioFocus(null);
        }
    }

    private String toTime(int i, int j)
    {
        return (new StringBuilder()).append(String.format("%02d", new Object[] {
            Integer.valueOf(i)
        })).append(":").append(String.format("%02d", new Object[] {
            Integer.valueOf(j)
        })).toString();
    }

    private void updateRingtonTitle()
    {
        SettingInfo settinginfo = (SettingInfo)list.get(3);
        if (settinginfo != null)
        {
            settinginfo.setTextWake(getRingtoneTitle());
            wakeAdapter.notifyDataSetChanged();
        }
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        if (i == 0)
        {
            ((SettingInfo)list.get(1)).setTextWake(getRepeat());
            wakeAdapter.notifyDataSetChanged();
        }
        setAlarm();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301fe);
        mContext = getApplicationContext();
        getParentInfo();
        initUI();
        if (from == 1)
        {
            setPlayingSoundAsRington();
        }
        if (from == 2)
        {
            setLivePlayingSoundAsRington();
        }
    }

    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        setIntent(intent);
        getParentInfo();
        playAlarmAndShowDialog();
    }

    protected void onPause()
    {
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null && !TextUtils.isEmpty(soundinfo.playUrl32))
        {
            updateRingtonTitle();
            if (sDownloadLiteManager != null)
            {
                sDownloadLiteManager.setNotifyHandler(sAlarm.mUrl, mUpdateUIHandler);
                return;
            }
        }
    }

    protected void onStop()
    {
        super.onStop();
        if (sDownloadLiteManager != null)
        {
            sDownloadLiteManager.setNotifyHandler(sAlarm.mUrl, null);
        }
    }

    public void onTimeSet(TimePicker timepicker, int i, int j)
    {
        timepicker = SharedPreferencesUtil.getInstance(mContext);
        int k = timepicker.getInt("alarm_hour", 0);
        int l = timepicker.getInt("alarm_minute", 0);
        if (i != k || j != l)
        {
            timepicker.saveInt("alarm_hour", i);
            timepicker.saveInt("alarm_minute", j);
            timepicker.saveBoolean("is_time_modified", true);
            setAlarm();
            ((SettingInfo)list.get(2)).setTextWake(toTime(i, j));
            wakeAdapter.notifyDataSetChanged();
        }
    }

    public void saveWakeupSwitch(boolean flag)
    {
        SharedPreferencesUtil.getInstance(mContext).saveBoolean("isOnForWake", flag);
        wakeAdapter.notifyDataSetChanged();
    }

    public void setAlarm()
    {
        SharedPreferencesUtil sharedpreferencesutil;
        Object obj;
        int i;
        int j;
        int k;
label0:
        {
            sharedpreferencesutil = SharedPreferencesUtil.getInstance(mContext);
            boolean flag = sharedpreferencesutil.getBoolean("is_repeat_modified", false);
            boolean flag1 = sharedpreferencesutil.getBoolean("is_time_modified", false);
            if (flag || flag1)
            {
                obj = getAlarmIntent();
                ((Intent) (obj)).setAction("com.ximalaya.ting.android.action.START_ALARM");
                obj = PendingIntent.getBroadcast(this, 0, ((Intent) (obj)), 0);
                malarm.cancel(((PendingIntent) (obj)));
                i = sharedpreferencesutil.getInt("alarm_hour", -1);
                j = sharedpreferencesutil.getInt("alarm_minute", -1);
                k = sharedpreferencesutil.getInt("repeat_week_days", 0);
                if (i >= 0 && j >= 0)
                {
                    break label0;
                }
            }
            return;
        }
        long l = ToolUtil.calculateAlarm(i, j, new DaysOfWeek(k));
        if (android.os.Build.VERSION.SDK_INT >= 19)
        {
            malarm.setExact(0, l, ((PendingIntent) (obj)));
        } else
        {
            malarm.set(0, l, ((PendingIntent) (obj)));
        }
        Toast.makeText(this, "\u95F9\u949F\u8BBE\u7F6E\u6210\u529F", 0).show();
        sharedpreferencesutil.saveBoolean("is_repeat_modified", false);
        sharedpreferencesutil.saveBoolean("is_time_modified", false);
    }






/*
    static int access$1002(WakeUpSettingActivity wakeupsettingactivity, int i)
    {
        wakeupsettingactivity.alarmHour = i;
        return i;
    }

*/


/*
    static DownloadLiteManager access$102(DownloadLiteManager downloadlitemanager)
    {
        sDownloadLiteManager = downloadlitemanager;
        return downloadlitemanager;
    }

*/



/*
    static int access$1102(WakeUpSettingActivity wakeupsettingactivity, int i)
    {
        wakeupsettingactivity.alarmMinute = i;
        return i;
    }

*/








/*
    static Alarm access$202(Alarm alarm)
    {
        sAlarm = alarm;
        return alarm;
    }

*/






/*
    static MediaPlayer access$602(WakeUpSettingActivity wakeupsettingactivity, MediaPlayer mediaplayer)
    {
        wakeupsettingactivity.mp = mediaplayer;
        return mediaplayer;
    }

*/




/*
    static int access$802(WakeUpSettingActivity wakeupsettingactivity, int i)
    {
        wakeupsettingactivity.mOnlineSoundsPage = i;
        return i;
    }

*/


/*
    static int access$804(WakeUpSettingActivity wakeupsettingactivity)
    {
        int i = wakeupsettingactivity.mOnlineSoundsPage + 1;
        wakeupsettingactivity.mOnlineSoundsPage = i;
        return i;
    }

*/

}
