// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.adapter.setting.SelectRingtoneAdapter;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            w, x, y, z

public class LocalRingtoneActivity extends BaseActivity
{
    class a extends MyAsyncTask
    {

        final LocalRingtoneActivity a;

        protected transient List a(Void avoid[])
        {
            avoid = DownloadHandler.getInstance(a.mAppContext).getSortedFinishedDownloadList();
            Logger.log("140703", (new StringBuilder()).append("====DownloadSoundsListFragment========GET downloadTaskList: ").append(avoid.size()).toString());
            return avoid;
        }

        protected void a(List list)
        {
            a.soundList.clear();
            if (list != null && list.size() > 0)
            {
                a.soundList.addAll(list);
            }
            a.ringtoneAdapter.notifyDataSetChanged();
            a.showEmptyView();
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((List)obj);
        }

        a()
        {
            a = LocalRingtoneActivity.this;
            super();
        }
    }


    public static final int RESULT_CODE_OK = 11;
    private TextView mEmptyTv;
    private com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener mOnPlayServiceUpdateListener;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.OnPlayerStatusUpdateListener mOnPlayerStatusUpdateListener;
    private SelectRingtoneAdapter ringtoneAdapter;
    private List soundList;
    private BounceListView soundListView;

    public LocalRingtoneActivity()
    {
        soundList = new ArrayList();
    }

    private void initData()
    {
        (new a()).myexec(new Void[0]);
    }

    private void initListener()
    {
        soundListView.setOnItemClickListener(new w(this));
    }

    private void initMediaServiceListener()
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            if (mOnPlayerStatusUpdateListener == null)
            {
                mOnPlayerStatusUpdateListener = new x(this);
            }
            localmediaservice.setOnPlayerStatusUpdateListener(mOnPlayerStatusUpdateListener);
            if (mOnPlayServiceUpdateListener == null)
            {
                mOnPlayServiceUpdateListener = new y(this);
            }
            localmediaservice.setOnPlayServiceUpdateListener(mOnPlayServiceUpdateListener);
        }
    }

    private void initUI()
    {
        retButton = (ImageView)findViewById(0x7f0a007b);
        retButton.setOnClickListener(new z(this));
        topTextView = (TextView)findViewById(0x7f0a00ae);
        topTextView.setText(getString(0x7f090101));
        soundListView = (BounceListView)findViewById(0x7f0a0101);
        mEmptyTv = (TextView)findViewById(0x7f0a0060);
        mEmptyTv.setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u4E0B\u8F7D\u8FC7\u58F0\u97F3\u54E6");
        showEmptyView();
        View view = new View(this);
        view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, Utilities.dip2px(this, 10F)));
        soundListView.addHeaderView(view, null, false);
        ringtoneAdapter = new SelectRingtoneAdapter(this, soundList);
        soundListView.setAdapter(ringtoneAdapter);
    }

    private void showEmptyView()
    {
        if (soundList.isEmpty())
        {
            mEmptyTv.setVisibility(0);
            return;
        } else
        {
            mEmptyTv.setVisibility(8);
            return;
        }
    }

    public void finish()
    {
        super.finish();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03002c);
        initUI();
        initListener();
        initData();
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            finish();
        }
        return super.onKeyDown(i, keyevent);
    }

    protected void onPause()
    {
        super.onPause();
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (localmediaservice != null)
        {
            localmediaservice.removeOnPlayerUpdateListener(mOnPlayerStatusUpdateListener);
            localmediaservice.removeOnPlayServiceUpdateListener(mOnPlayServiceUpdateListener);
        }
    }

    protected void onResume()
    {
        super.onResume();
        initMediaServiceListener();
    }




}
