// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.model.UserSpace.CommonItem;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.StorageUtils;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            DownloadLocationActivity

class i
    implements android.widget.AdapterView.OnItemClickListener
{

    final DownloadLocationActivity a;

    i(DownloadLocationActivity downloadlocationactivity)
    {
        a = downloadlocationactivity;
        super();
    }

    public void onItemClick(AdapterView adapterview, View view, int j, long l)
    {
        for (adapterview = DownloadLocationActivity.access$000(a).iterator(); adapterview.hasNext();)
        {
            ((CommonItem)adapterview.next()).boolValue = false;
        }

        adapterview = (CommonItem)DownloadLocationActivity.access$000(a).get(j);
        if (adapterview != null)
        {
            adapterview.boolValue = true;
            view = StorageUtils.getDownloadDir(((CommonItem) (adapterview)).text);
            StorageUtils.setDownloadLocation(view);
            Logger.log("download_path", (new StringBuilder()).append("==SetDownloadDir=").append(view).toString());
            DownloadLocationActivity.access$102(a, ((CommonItem) (adapterview)).name);
        }
        DownloadLocationActivity.access$200(a).notifyDataSetChanged();
    }
}
