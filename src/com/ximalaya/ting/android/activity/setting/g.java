// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.activity.homepage.BindIPhoneActivity;
import com.ximalaya.ting.android.activity.login.AuthorizeActivity;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            BindActivity, ShareSettingNextActivity

class g
    implements android.widget.AdapterView.OnItemClickListener
{

    final BindActivity a;

    g(BindActivity bindactivity)
    {
        a = bindactivity;
        super();
    }

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        adapterview = (SettingInfo)BindActivity.access$100(a).get(i);
        Intent intent2;
        switch (i)
        {
        default:
            return;

        case 0: // '\0'
            if (!((SettingInfo)BindActivity.access$100(a).get(i)).isSetting)
            {
                adapterview = new Intent(a, com/ximalaya/ting/android/activity/homepage/BindIPhoneActivity);
                adapterview.putExtra("FROM", "SettingActivity");
                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                a.startActivityForResult(adapterview, 5);
                return;
            } else
            {
                adapterview = new Intent(a, com/ximalaya/ting/android/activity/web/WebActivityNew);
                adapterview.putExtra("ExtraUrl", e.al);
                a.startActivity(adapterview);
                return;
            }

        case 1: // '\001'
            adapterview = (SettingInfo)BindActivity.access$100(a).get(i);
            if (!((SettingInfo) (adapterview)).isSetting || !((SettingInfo) (adapterview)).isVerified)
            {
                adapterview = new Intent(a, com/ximalaya/ting/android/activity/web/WebActivityNew);
                adapterview.putExtra("ExtraUrl", e.aj);
                a.startActivity(adapterview);
                return;
            } else
            {
                adapterview = new Intent(a, com/ximalaya/ting/android/activity/web/WebActivityNew);
                adapterview.putExtra("ExtraUrl", e.ak);
                a.startActivity(adapterview);
                return;
            }

        case 2: // '\002'
            Intent intent = new Intent(a, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
            if (((SettingInfo) (adapterview)).isSetting && !((SettingInfo) (adapterview)).isExpired)
            {
                intent.setClass(a, com/ximalaya/ting/android/activity/setting/ShareSettingNextActivity);
                intent.setFlags(0x20000000);
                intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                intent.putExtra("SettingFlag", 2);
                a.startActivity(intent);
                return;
            } else
            {
                intent.putExtra("lgflag", 13);
                intent.setFlags(0x20000000);
                a.startActivityForResult(intent, 3);
                return;
            }

        case 3: // '\003'
            Intent intent1 = new Intent(a, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
            if (((SettingInfo) (adapterview)).isSetting && !((SettingInfo) (adapterview)).isExpired)
            {
                intent1.setClass(a, com/ximalaya/ting/android/activity/setting/ShareSettingNextActivity);
                intent1.setFlags(0x20000000);
                intent1.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                intent1.putExtra("SettingFlag", 1);
                a.startActivity(intent1);
                return;
            } else
            {
                intent1.putExtra("lgflag", 12);
                intent1.setFlags(0x20000000);
                a.startActivityForResult(intent1, 4);
                return;
            }

        case 4: // '\004'
            intent2 = new Intent(a, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
            break;
        }
        if (((SettingInfo) (adapterview)).isSetting && !((SettingInfo) (adapterview)).isExpired)
        {
            intent2.setClass(a, com/ximalaya/ting/android/activity/setting/ShareSettingNextActivity);
            intent2.setFlags(0x20000000);
            intent2.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            intent2.putExtra("SettingFlag", 3);
            a.startActivity(intent2);
            return;
        } else
        {
            intent2.putExtra("lgflag", 14);
            intent2.setFlags(0x20000000);
            a.startActivityForResult(intent2, 6);
            return;
        }
    }
}
