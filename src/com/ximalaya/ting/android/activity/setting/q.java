// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.widget.ProgressBar;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.adapter.feed.FeedRecycleAdapter;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.feed2.FeedRecycleModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            FeedRecycleActivity

class q extends a
{

    final FeedRecycleModel a;
    final FeedRecycleActivity b;

    q(FeedRecycleActivity feedrecycleactivity, FeedRecycleModel feedrecyclemodel)
    {
        b = feedrecycleactivity;
        a = feedrecyclemodel;
        super();
    }

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, FeedRecycleActivity.access$400(b));
    }

    public void onNetError(int i, String s)
    {
        FeedRecycleActivity.access$800(b).setVisibility(8);
        Toast.makeText(FeedRecycleActivity.access$700(b), "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
    }

    public void onStart()
    {
        FeedRecycleActivity.access$800(b).setVisibility(0);
    }

    public void onSuccess(String s)
    {
        FeedRecycleActivity.access$800(b).setVisibility(8);
        if (s != null)
        {
            if ((s = (BaseModel)JSON.parseObject(s, com/ximalaya/ting/android/model/BaseModel)) != null && ((BaseModel) (s)).ret == 0)
            {
                int j = FeedRecycleActivity.access$600(b).size();
                int i = 0;
                do
                {
label0:
                    {
                        if (i != j)
                        {
                            if (((FeedRecycleModel)FeedRecycleActivity.access$600(b).get(i)).albumId != a.albumId || ((FeedRecycleModel)FeedRecycleActivity.access$600(b).get(i)).albumUid != a.albumUid)
                            {
                                break label0;
                            }
                            FeedRecycleActivity.access$600(b).remove(i);
                        }
                        FeedRecycleActivity.access$500(b).notifyDataSetChanged();
                        return;
                    }
                    i++;
                } while (true);
            }
        }
    }
}
