// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.Utilities;

public class AlarmHelpingActivity extends BaseActivity
    implements android.view.View.OnClickListener
{

    public static final String APP_INSTALLED = "installed_apps";
    public static final int BAIDU = 16;
    public static final int LBE = 8;
    public static final int LIEBAO = 32;
    public static final int MIUI = 1;
    public static final int TENCENT = 4;
    public static final int _360 = 2;
    private ViewGroup mContainer;

    public AlarmHelpingActivity()
    {
    }

    private void addItem(String s, boolean flag, int i)
    {
        if (!flag)
        {
            View view = new View(this);
            view.setBackgroundDrawable(getResources().getDrawable(0x7f0201e0));
            android.widget.LinearLayout.LayoutParams layoutparams = new android.widget.LinearLayout.LayoutParams(-1, -2);
            mContainer.addView(view, layoutparams);
        }
        TextView textview = new TextView(this);
        textview.setText(s);
        textview.setTextSize(15F);
        textview.setTextColor(Color.parseColor("#333333"));
        textview.setGravity(16);
        textview.setBackgroundDrawable(getResources().getDrawable(0x7f0205ab));
        textview.setPadding(Utilities.dip2px(this, 10F), 0, Utilities.dip2px(this, 10F), 0);
        textview.setTag(Integer.valueOf(i));
        textview.setOnClickListener(this);
        s = new android.widget.LinearLayout.LayoutParams(-1, Utilities.dip2px(this, 52F));
        mContainer.addView(textview, s);
    }

    private void loadAppList()
    {
        boolean flag1 = true;
        boolean flag2 = false;
        int i = getIntent().getIntExtra("installed_apps", 0);
        if ((i & 1) != 0)
        {
            addItem(getString(0x7f0901ac), true, 1);
            flag1 = false;
        }
        boolean flag = flag1;
        if ((i & 2) != 0)
        {
            addItem(getString(0x7f0901ad), flag1, 2);
            flag = false;
        }
        flag1 = flag;
        if ((i & 4) != 0)
        {
            addItem(getString(0x7f0901ae), flag, 4);
            flag1 = false;
        }
        flag = flag1;
        if ((i & 8) != 0)
        {
            addItem(getString(0x7f0901af), flag1, 8);
            flag = false;
        }
        if ((i & 0x10) != 0)
        {
            addItem(getString(0x7f0901b0), flag, 16);
            flag = flag2;
        }
        if ((i & 0x20) != 0)
        {
            addItem(getString(0x7f0901b1), flag, 32);
        }
    }

    public void onClick(View view)
    {
        if (!(view.getTag() instanceof Integer)) goto _L2; else goto _L1
_L1:
        ((Integer)(Integer)view.getTag()).intValue();
        JVM INSTR lookupswitch 6: default 80
    //                   1: 152
    //                   2: 158
    //                   4: 164
    //                   8: 170
    //                   16: 176
    //                   32: 182;
           goto _L3 _L4 _L5 _L6 _L7 _L8 _L9
_L9:
        break MISSING_BLOCK_LABEL_182;
_L3:
        view = null;
_L10:
        if (view != null)
        {
            Object obj = view;
            if (!TextUtils.isEmpty(view))
            {
                if (!NetworkUtils.isNetworkAvaliable(this))
                {
                    obj = (new StringBuilder()).append("file:///android_asset/alarm_help/demo/android/").append(view).append(".html").toString();
                } else
                {
                    obj = (new StringBuilder()).append(a.E).append("demo/andriod/").append(view).append(".html").toString();
                }
            }
            view = new Intent(this, com/ximalaya/ting/android/activity/web/WebActivityNew);
            view.putExtra("ExtraUrl", ((String) (obj)));
            startActivity(view);
        }
_L2:
        return;
_L4:
        view = "help_miui";
          goto _L10
_L5:
        view = "help_360";
          goto _L10
_L6:
        view = "help_tencent";
          goto _L10
_L7:
        view = "help_leb";
          goto _L10
_L8:
        view = "help_baidu";
          goto _L10
        view = "help_liebao";
          goto _L10
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030001);
        initCommon();
        setTitleText(getString(0x7f0901ab));
        mContainer = (ViewGroup)findViewById(0x7f0a005a);
        loadAppList();
    }
}
