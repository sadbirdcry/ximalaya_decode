// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.LiveHistoryManage;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            v, u

public class LiveRingtoneActivity extends BaseActivity
{
    class a extends BaseAdapter
    {

        final LiveRingtoneActivity a;
        private List b;

        public int getCount()
        {
            return b.size();
        }

        public Object getItem(int i)
        {
            return b.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            viewgroup = view;
            if (view == null)
            {
                viewgroup = LayoutInflater.from(a.mAppContext).inflate(0x7f030160, null);
            }
            view = (TextView)viewgroup.findViewById(0x7f0a01ed);
            SoundInfo soundinfo = (SoundInfo)b.get(i);
            if (soundinfo != null)
            {
                view.setText(soundinfo.radioName);
            }
            return viewgroup;
        }

        public a()
        {
            a = LiveRingtoneActivity.this;
            super();
            b = LiveHistoryManage.getInstance(mAppContext).getSoundInfoList();
        }
    }


    private a mAdapter;
    private TextView mEmptyTv;
    private BounceListView soundListView;

    public LiveRingtoneActivity()
    {
    }

    private void initListener()
    {
        soundListView.setOnItemClickListener(new v(this));
    }

    private void initUI()
    {
        retButton = (ImageView)findViewById(0x7f0a007b);
        retButton.setOnClickListener(new u(this));
        topTextView = (TextView)findViewById(0x7f0a00ae);
        topTextView.setText(getString(0x7f090101));
        soundListView = (BounceListView)findViewById(0x7f0a0101);
        mAdapter = new a();
        soundListView.setAdapter(mAdapter);
        mEmptyTv = (TextView)findViewById(0x7f0a0060);
        mEmptyTv.setText("\u4EB2~ \u4F60\u8FD8\u6CA1\u6709\u64AD\u653E\u8BB0\u5F55\u54E6");
        if (LiveHistoryManage.getInstance(mAppContext).getSoundInfoList().isEmpty())
        {
            mEmptyTv.setVisibility(0);
            return;
        } else
        {
            mEmptyTv.setVisibility(8);
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03002a);
        initUI();
        initListener();
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            finish();
        }
        return super.onKeyDown(i, keyevent);
    }

}
