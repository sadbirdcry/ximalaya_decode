// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.setting.PushSettingInfo;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            PushSettingActivity

class b extends MyAsyncTask
{

    PushSettingInfo a;
    final PushSettingActivity b;

    protected transient Void a(Object aobj[])
    {
        a = (PushSettingInfo)aobj[0];
        if (a == null)
        {
            return null;
        } else
        {
            aobj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/apn/set").toString();
            RequestParams requestparams = new RequestParams();
            requestparams.put("noPushStartHour", "0");
            requestparams.put("noPushEndHour", "0");
            requestparams.put("isPushNewFollower", String.valueOf(a.isPushNewFollower()));
            requestparams.put("isPushNewComment", String.valueOf(a.isPushNewComment()));
            requestparams.put("isPushNewMessage", String.valueOf(a.isPushNewMessage()));
            requestparams.put("isPushNewQuan", String.valueOf(a.isPushNewQuan()));
            requestparams.put("isPushZoneComment", String.valueOf(a.isPushZoneComment()));
            f.a().b(((String) (aobj)), requestparams, b.rootView, null);
            return null;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a(aobj);
    }

    (PushSettingActivity pushsettingactivity)
    {
        b = pushsettingactivity;
        super();
    }
}
