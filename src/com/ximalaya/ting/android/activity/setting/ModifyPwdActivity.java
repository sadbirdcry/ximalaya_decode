// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            aa, ab, SettingActivity

public class ModifyPwdActivity extends BaseActivity
{
    class a extends MyAsyncTask
    {

        String a;
        final ModifyPwdActivity b;

        protected transient Boolean a(Void avoid[])
        {
            Object obj;
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/modifyPwd").toString();
            obj = new RequestParams();
            ((RequestParams) (obj)).put("oldPwd", b.oldPwdEdit.getText().toString().trim());
            ((RequestParams) (obj)).put("newPwd", b.newPwdEdit.getText().toString().trim());
            ((RequestParams) (obj)).put("confirmPwd", b.newPwdVerifyEdit.getText().toString().trim());
            obj = f.a().b(avoid, ((RequestParams) (obj)), b.nextButton, b.nextButton);
            Logger.d("result:modifyPwd", ((String) (obj)));
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_170;
            }
            avoid = null;
            obj = JSON.parseObject(((String) (obj)));
            avoid = ((Void []) (obj));
_L2:
            if (avoid.getIntValue("ret") == 0)
            {
                return Boolean.valueOf(true);
            }
            break; /* Loop/switch isn't completed */
            Exception exception;
            exception;
            exception.printStackTrace();
            if (true) goto _L2; else goto _L1
_L1:
            a = avoid.getString("msg");
            return Boolean.valueOf(false);
        }

        protected void a(Boolean boolean1)
        {
            if (b == null || b.isFinishing())
            {
                return;
            }
            if (boolean1.booleanValue())
            {
                boolean1 = new Intent(b, com/ximalaya/ting/android/activity/setting/SettingActivity);
                Bundle bundle = new Bundle();
                bundle.putInt("tab_index", 4);
                boolean1.putExtras(bundle);
                boolean1.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(b.nextButton));
                b.startActivity(boolean1);
                b.finish();
                return;
            } else
            {
                Toast.makeText(b.mContext, a, 0).show();
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((Boolean)obj);
        }

        a()
        {
            b = ModifyPwdActivity.this;
            super();
            a = "\u7F51\u7EDC\u8FDE\u63A5\u5931\u8D25\uFF0C\u8BF7\u91CD\u8BD5";
        }
    }


    private Context mContext;
    private EditText newPwdEdit;
    private EditText newPwdVerifyEdit;
    private EditText oldPwdEdit;

    public ModifyPwdActivity()
    {
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301c5);
        mContext = getApplicationContext();
        topTextView = (TextView)findViewById(0x7f0a00ae);
        retButton = (ImageView)findViewById(0x7f0a007b);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        oldPwdEdit = (EditText)findViewById(0x7f0a06cb);
        newPwdEdit = (EditText)findViewById(0x7f0a06cc);
        newPwdVerifyEdit = (EditText)findViewById(0x7f0a06cd);
        setTitleText(getString(0x7f090130));
        nextButton.setVisibility(0);
        nextButton.setImageResource(0x7f02006a);
        nextButton.setOnClickListener(new aa(this));
        retButton.setOnClickListener(new ab(this));
    }




}
