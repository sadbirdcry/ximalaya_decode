// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            t

public class HelpActivity extends BaseActivity
{
    private class a extends WebViewClient
    {

        final HelpActivity a;

        public void onPageFinished(WebView webview, String s)
        {
            super.onPageFinished(webview, s);
            a.loadBar.setVisibility(4);
        }

        public void onPageStarted(WebView webview, String s, Bitmap bitmap)
        {
            super.onPageStarted(webview, s, bitmap);
        }

        public boolean shouldOverrideUrlLoading(WebView webview, String s)
        {
            a.mWebView.loadUrl(s);
            return true;
        }

        private a()
        {
            a = HelpActivity.this;
            super();
        }

        a(t t1)
        {
            this();
        }
    }


    private int flag;
    private ProgressBar loadBar;
    private WebView mWebView;
    private String url;

    public HelpActivity()
    {
        url = "http://m.ximalaya.com/third/android/helpCenter.html";
        flag = 0;
    }

    private void initUI()
    {
        retButton = (ImageView)findViewById(0x7f0a007b);
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        nextButton.setVisibility(4);
        retButton.setOnClickListener(new t(this));
        if (flag == 0)
        {
            topTextView.setText(getString(0x7f090145));
        } else
        {
            topTextView.setText("\u5173\u6CE8\u559C\u9A6C\u62C9\u96C5\u5B98\u65B9\u5FAE\u535A");
        }
        loadBar = (ProgressBar)findViewById(0x7f0a0446);
        setUpWebView();
    }

    private void setUpWebView()
    {
        mWebView = (WebView)findViewById(0x7f0a0445);
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new a(null));
        mWebView.loadUrl(url);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030107);
        flag = getIntent().getIntExtra("flag", 0);
        flag;
        JVM INSTR tableswitch 0 2: default 56
    //                   0 123
    //                   1 132
    //                   2 141;
           goto _L1 _L2 _L3 _L4
_L1:
        break; /* Loop/switch isn't completed */
_L4:
        break MISSING_BLOCK_LABEL_141;
_L5:
        bundle = Uri.parse(url);
        if (!url.contains("?"))
        {
            url = (new StringBuilder()).append(url).append("?app=iting&version=").append(((MyApplication)(MyApplication)getApplication()).m()).toString();
        } else
        {
            bundle = ToolUtil.getQueryMap(bundle.getQuery());
            if (bundle != null && !bundle.containsKey("app"))
            {
                url = (new StringBuilder()).append(url).append("&app=iting").toString();
            }
            if (bundle != null && !bundle.containsKey("version"))
            {
                url = (new StringBuilder()).append(url).append("&version=").append(((MyApplication)(MyApplication)getApplication()).m()).toString();
            }
        }
        initUI();
        return;
_L2:
        url = "http://m.ximalaya.com/third/android/helpCenter.html";
          goto _L5
_L3:
        url = "http://m.weibo.cn/u/2608693591";
          goto _L5
        url = "http://ti.3g.qq.com/touch/iphone/index.jsp?sid=ATCYx67av4khUDoBxuMdGrWa#guest_home/u=ximalayacom";
          goto _L5
    }


}
