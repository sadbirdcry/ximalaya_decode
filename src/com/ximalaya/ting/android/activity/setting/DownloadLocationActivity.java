// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.model.UserSpace.CommonItem;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.setting.CornerListView;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            h, i

public class DownloadLocationActivity extends BaseActivity
{
    class a extends BaseAdapter
    {

        List a;
        final DownloadLocationActivity b;

        public CommonItem a(int j)
        {
            return (CommonItem)a.get(j);
        }

        public int getCount()
        {
            return a.size();
        }

        public Object getItem(int j)
        {
            return a(j);
        }

        public long getItemId(int j)
        {
            return (long)j;
        }

        public View getView(int j, View view, ViewGroup viewgroup)
        {
            CommonItem commonitem;
            if (view == null)
            {
                view = b.getLayoutInflater().inflate(0x7f030162, null);
                viewgroup = new a(this);
                viewgroup.b = (TextView)view.findViewById(0x7f0a0539);
                viewgroup.a = (TextView)view.findViewById(0x7f0a053a);
                viewgroup.c = (ImageView)view.findViewById(0x7f0a053b);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (a)view.getTag();
            }
            commonitem = a(j);
            ((a) (viewgroup)).b.setText(commonitem.name);
            ((a) (viewgroup)).a.setText(StorageUtils.getDownloadDir(commonitem.text));
            if (commonitem.boolValue)
            {
                ((a) (viewgroup)).c.setVisibility(0);
                return view;
            } else
            {
                ((a) (viewgroup)).c.setVisibility(4);
                return view;
            }
        }

        public a(List list)
        {
            b = DownloadLocationActivity.this;
            super();
            a = list;
        }
    }

    class a.a
    {

        TextView a;
        TextView b;
        ImageView c;
        final a d;

        a.a(a a1)
        {
            d = a1;
            super();
        }
    }


    private String curLocation;
    private List downloadLocations;
    private a locationSelectAdapter;
    private CornerListView locationSelectListView;

    public DownloadLocationActivity()
    {
        locationSelectListView = null;
        downloadLocations = null;
        locationSelectAdapter = null;
    }

    private void findViews()
    {
        locationSelectListView = (CornerListView)findViewById(0x7f0a008d);
    }

    private void initDatas()
    {
        downloadLocations = new ArrayList();
    }

    private void initViews()
    {
        String s;
label0:
        {
            retButton = (ImageView)findViewById(0x7f0a007b);
            topTextView = (TextView)findViewById(0x7f0a00ae);
            nextButton = (ImageView)findViewById(0x7f0a0710);
            nextButton.setVisibility(4);
            topTextView.setText("\u4E0B\u8F7D\u4F4D\u7F6E");
            retButton.setOnClickListener(new h(this));
            if (locationSelectListView != null)
            {
                locationSelectListView.setVisibility(0);
                s = StorageUtils.getCurrentDownloadLocation();
                if (Utilities.isNotBlank(s))
                {
                    break label0;
                }
                Toast.makeText(getApplicationContext(), "\u672A\u68C0\u6D4B\u5230SD\u5361", 0).show();
            }
            return;
        }
        s = s.substring(0, s.indexOf("/ting"));
        downloadLocations.clear();
        for (int j = 0; j < StorageUtils.count; j++)
        {
            CommonItem commonitem = new CommonItem();
            commonitem.boolValue = false;
            if (Utilities.isNotBlank(s) && s.equals(StorageUtils.paths[j]))
            {
                commonitem.boolValue = true;
                curLocation = StorageUtils.labels[j];
            }
            commonitem.name = StorageUtils.labels[j];
            String s1 = StorageUtils.paths[j];
            commonitem.text = s1;
            (new File(s1)).mkdirs();
            downloadLocations.add(commonitem);
        }

        locationSelectAdapter = new a(downloadLocations);
        locationSelectListView.setAdapter(locationSelectAdapter);
        locationSelectListView.setOnItemClickListener(new i(this));
    }

    public void finish()
    {
        Intent intent = new Intent();
        intent.putExtra("curLocation", curLocation);
        setResult(0, intent);
        super.finish();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03000f);
        initDatas();
        findViews();
        initViews();
    }

    public boolean onKeyDown(int j, KeyEvent keyevent)
    {
        if (j == 4)
        {
            finish();
            return true;
        } else
        {
            return super.onKeyDown(j, keyevent);
        }
    }

    protected void onResume()
    {
        super.onResume();
    }



/*
    static String access$102(DownloadLocationActivity downloadlocationactivity, String s)
    {
        downloadlocationactivity.curLocation = s;
        return s;
    }

*/

}
