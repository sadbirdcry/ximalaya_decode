// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.adapter.setting.RepeatSettingAdapter;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.setting.DaysOfWeek;
import com.ximalaya.ting.android.model.setting.WeekDay;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.view.setting.CornerListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            ap

public class RepeatSettingActivity extends BaseActivity
{

    private List list;
    private Context mCt;
    private CornerListView repeatListView;
    private RepeatSettingAdapter repeatSettingAdapter;

    public RepeatSettingActivity()
    {
    }

    private void initUI()
    {
        retButton = (ImageView)findViewById(0x7f0a007b);
        retButton.setOnClickListener(new ap(this));
        topTextView = (TextView)findViewById(0x7f0a00ae);
        topTextView.setText(getString(0x7f090100));
        nextButton = (ImageView)findViewById(0x7f0a0710);
        nextButton.setVisibility(4);
        list = new ArrayList();
        String as[] = getResources().getStringArray(0x7f0c001c);
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCt);
        int j = sharedpreferencesutil.getInt("repeat_week_days", 0);
        boolean flag = sharedpreferencesutil.getBoolean("alarm_once_set_repeat", false);
        int k = TimeHelper.weekOfToday();
        for (int i = 0; i < as.length; i++)
        {
            WeekDay weekday = new WeekDay();
            weekday.setName(as[i]);
            weekday.setSelected(DaysOfWeek.isSet(j, i));
            if (!flag && i == k)
            {
                weekday.setSelected(true);
            }
            list.add(weekday);
        }

        repeatListView = (CornerListView)findViewById(0x7f0a06b3);
        repeatSettingAdapter = new RepeatSettingAdapter(this, list);
        repeatListView.setAdapter(repeatSettingAdapter);
    }

    public void finish()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mCt);
        int i = sharedpreferencesutil.getInt("repeat_week_days", 0);
        int j = DaysOfWeek.getRepeatDays(list);
        if (j != i)
        {
            sharedpreferencesutil.saveInt("repeat_week_days", j);
            sharedpreferencesutil.saveBoolean("is_repeat_modified", true);
        }
        sharedpreferencesutil.saveBoolean("alarm_once_set_repeat", true);
        setResult(0);
        super.finish();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301be);
        mCt = getApplicationContext();
        initUI();
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            finish();
        }
        return super.onKeyDown(i, keyevent);
    }
}
