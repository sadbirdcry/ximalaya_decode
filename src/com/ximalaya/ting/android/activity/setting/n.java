// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.adapter.feed.FeedRecycleAdapter;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.feed2.FeedRecycleModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.view.pulltorefreshgridview.RefreshLoadMoreListView;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            FeedRecycleActivity

class n
    implements android.widget.AdapterView.OnItemClickListener
{

    final FeedRecycleActivity a;

    n(FeedRecycleActivity feedrecycleactivity)
    {
        a = feedrecycleactivity;
        super();
    }

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        i -= ((ListView)FeedRecycleActivity.access$400(a).getRefreshableView()).getHeaderViewsCount();
        if (i >= 0 && FeedRecycleActivity.access$500(a).getCount() >= i)
        {
            if ((adapterview = (FeedRecycleModel)FeedRecycleActivity.access$500(a).getItem(i)) != null)
            {
                Bundle bundle = new Bundle();
                AlbumModel albummodel = new AlbumModel();
                albummodel.albumId = ((FeedRecycleModel) (adapterview)).albumId;
                bundle.putString("album", JSON.toJSONString(albummodel));
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                a.startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, bundle);
                return;
            }
        }
    }
}
