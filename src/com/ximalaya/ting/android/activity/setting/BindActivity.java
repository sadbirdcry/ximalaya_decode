// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.adapter.setting.BindListAdapter;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.view.setting.CornerListView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            d, f, g, e

public class BindActivity extends BaseActivity
{

    private static final int requestCodeP = 5;
    private static final int requestCodeQQ = 3;
    private static final int requestCodeRenn = 6;
    private static final int requestCodeWeibo = 4;
    private BindListAdapter bindAdapter;
    private List bindInfos;
    private CornerListView bindListView;
    private Context context;

    public BindActivity()
    {
    }

    private void initData()
    {
        (new d(this)).myexec(new Void[0]);
    }

    private void initListener()
    {
        retButton.setOnClickListener(new f(this));
        bindListView.setOnItemClickListener(new g(this));
    }

    private void initUI()
    {
        retButton = (ImageView)findViewById(0x7f0a007b);
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        nextButton.setVisibility(4);
        topTextView.setText("\u7ED1\u5B9A\u793E\u4EA4\u5E73\u53F0");
        bindListView = (CornerListView)findViewById(0x7f0a017b);
        String as[] = getResources().getStringArray(0x7f0c0017);
        bindInfos = new ArrayList();
        int i = 0;
        while (i < as.length) 
        {
            SettingInfo settinginfo = new SettingInfo();
            settinginfo.nameWake = as[i];
            if (i == 0)
            {
                loginInfoModel = UserInfoMannage.getInstance().getUser();
                if (loginInfoModel != null && loginInfoModel.mPhone != null)
                {
                    settinginfo.isSetting = true;
                    settinginfo.textWake = (new StringBuilder()).append("(").append(loginInfoModel.mPhone).append(")").toString();
                } else
                {
                    settinginfo.isSetting = false;
                    settinginfo.textWake = "\u7ED1\u5B9A";
                }
            } else
            if (i == 1)
            {
                loginInfoModel = UserInfoMannage.getInstance().getUser();
                if (loginInfoModel != null && loginInfoModel.email != null)
                {
                    settinginfo.isSetting = true;
                    settinginfo.textWake = (new StringBuilder()).append("(").append(loginInfoModel.email).append(")").toString();
                    settinginfo.isVerified = loginInfoModel.isVEmail;
                } else
                {
                    settinginfo.isSetting = false;
                    settinginfo.textWake = "\u9A8C\u8BC1";
                }
            } else
            {
                settinginfo.isSetting = false;
                settinginfo.textWake = "\u7ED1\u5B9A";
            }
            bindInfos.add(settinginfo);
            i++;
        }
        bindAdapter = new BindListAdapter(context, bindInfos);
        bindListView.setAdapter(bindAdapter);
    }

    private void refreshEmailPhone()
    {
        (new e(this)).myexec(new Void[0]);
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        if (this != null && !isFinishing()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (i != 3)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == 0)
        {
            initData();
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        if (i != 4)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == 0)
        {
            initData();
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
        if (i != 6)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (j != 0) goto _L1; else goto _L5
_L5:
        initData();
        return;
        if (i != 5 || j != 1) goto _L1; else goto _L6
_L6:
        intent = intent.getStringExtra("tel");
        ((SettingInfo)bindInfos.get(0)).textWake = (new StringBuilder()).append("(").append(intent).append(")").toString();
        ((SettingInfo)bindInfos.get(0)).isSetting = true;
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        if (loginInfoModel != null)
        {
            loginInfoModel.mPhone = intent;
            loginInfoModel.isVMobile = true;
        }
        bindAdapter.notifyDataSetChanged();
        return;
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        context = getApplicationContext();
        setContentView(0x7f03004a);
        initUI();
        initData();
        initListener();
    }

    protected void onResume()
    {
        super.onResume();
        refreshEmailPhone();
    }




}
