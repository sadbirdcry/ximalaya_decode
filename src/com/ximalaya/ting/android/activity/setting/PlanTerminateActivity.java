// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.adapter.setting.PlanTerminateAdapter;
import com.ximalaya.ting.android.broadcast.AlarmReceiver;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.setting.WeekDay;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.setting.CornerListView;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            ac, ad, ae, af, 
//            ag

public class PlanTerminateActivity extends BaseActivity
{

    private PendingIntent alarmIntent;
    private boolean isLiving;
    private List list;
    private PlanTerminateAdapter mAdapter;
    private int mIndexDelayMinutes;
    private boolean mIsOn;
    private CornerListView mListView;
    private com.ximalaya.ting.android.service.play.TingMediaPlayer.SimpleOnPlayerStatusUpdateListener mPlayerStatusCallback;
    private Timer mTimer;

    public PlanTerminateActivity()
    {
        mTimer = null;
        alarmIntent = null;
        isLiving = false;
    }

    private final int getDelayMinutes(int i)
    {
        if (i != 0) goto _L2; else goto _L1
_L1:
        return 0;
_L2:
        int j = i;
        if (isLiving)
        {
            j = i + 1;
        }
        switch (j)
        {
        default:
            return 90;

        case 1: // '\001'
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            if (localmediaservice != null)
            {
                i = localmediaservice.getDuration() - localmediaservice.getCurPosition();
                if (i > 0)
                {
                    return i / 60000;
                }
            }
            break;

        case 2: // '\002'
            return 10;

        case 3: // '\003'
            return 20;

        case 4: // '\004'
            return 30;

        case 5: // '\005'
            return 60;

        case 6: // '\006'
            return 90;
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    private long getTimeLeft()
    {
        long l1 = 0L;
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(this);
        long l2 = sharedpreferencesutil.getLong("plan_play_stop_time");
        long l3 = System.currentTimeMillis();
        Logger.log("dl_alarm", (new StringBuilder()).append("getTimeLeft:").append(ToolUtil.formatTime(l2)).append(", now is ").append(ToolUtil.formatTime(l3)).toString());
        long l = l1;
        if (sharedpreferencesutil.contains("plan_play_stop_time"))
        {
            l = l1;
            if (l2 > l3)
            {
                l = (l2 - l3) / 1000L;
            }
        }
        return l;
    }

    private void initData()
    {
        initAlarmPendingIntent();
    }

    private void initUI()
    {
        retButton = (ImageView)findViewById(0x7f0a007b);
        topTextView = (TextView)findViewById(0x7f0a00ae);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        topTextView.setText("\u5B9A\u65F6\u505C\u6B62\u64AD\u653E");
        retButton.setOnClickListener(new ac(this));
        mIsOn = false;
        list = new ArrayList();
        String as[] = getResources().getStringArray(0x7f0c001d);
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(this);
        mIndexDelayMinutes = sharedpreferencesutil.getInt("delay_minutes_index", -1);
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null && soundinfo.category != 0)
        {
            isLiving = true;
            if (mIndexDelayMinutes == 1)
            {
                mIndexDelayMinutes = -1;
            }
        }
        if (sharedpreferencesutil.contains("isOnForPlan"))
        {
            mIsOn = sharedpreferencesutil.getBoolean("isOnForPlan", false);
        }
        int i = 0;
        while (i < as.length) 
        {
            WeekDay weekday = new WeekDay();
            if (i == 0 && mIndexDelayMinutes != -1)
            {
                weekday.setSwitchOn(mIsOn);
                weekday.timeLeft = getTimeLeft();
            }
            weekday.setName(as[i]);
            weekday.setIndexSelected(mIndexDelayMinutes);
            boolean flag;
            if (mIndexDelayMinutes == i)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            weekday.setSelected(flag);
            list.add(weekday);
            i++;
        }
        if (isLiving)
        {
            list.remove(1);
        }
        mListView = (CornerListView)findViewById(0x7f0a06b3);
        mAdapter = new PlanTerminateAdapter(this, list);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new ad(this));
    }

    private void startTimer()
    {
        stopTimer();
        if (mIndexDelayMinutes == 1)
        {
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            if (localmediaservice != null)
            {
                updateTimeLeftView((localmediaservice.getDuration() - localmediaservice.getCurPosition()) / 1000);
                if (mPlayerStatusCallback == null)
                {
                    mPlayerStatusCallback = new ae(this);
                }
                localmediaservice.setOnPlayerStatusUpdateListener(mPlayerStatusCallback);
            }
            return;
        } else
        {
            mTimer = new Timer();
            mTimer.scheduleAtFixedRate(new af(this), 0L, 1000L);
            return;
        }
    }

    private void stopTimer()
    {
        if (mTimer != null)
        {
            mTimer.cancel();
            mTimer = null;
        }
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        if (mPlayerStatusCallback != null && localmediaservice != null)
        {
            localmediaservice.removeOnPlayerUpdateListener(mPlayerStatusCallback);
        }
    }

    private boolean updateTimeLeftView(long l)
    {
        if (list == null || list.size() <= 0 || mAdapter == null)
        {
            return false;
        }
        ((WeekDay)list.get(0)).timeLeft = l;
        boolean flag;
        if (((WeekDay)list.get(0)).timeLeft > 0L)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        runOnUiThread(new ag(this));
        return flag;
    }

    public final void cancelAlarmTask()
    {
        stopTimer();
        ((AlarmManager)getSystemService("alarm")).cancel(getAlarmPendingIntent());
    }

    public final PendingIntent getAlarmPendingIntent()
    {
        if (alarmIntent == null)
        {
            initAlarmPendingIntent();
        }
        return alarmIntent;
    }

    public final void initAlarmPendingIntent()
    {
        Intent intent = new Intent(this, com/ximalaya/ting/android/broadcast/AlarmReceiver);
        intent.setAction("com.ximalaya.ting.android.action.SCHEDULE_PAUSE_PLAYER");
        alarmIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301be);
        initUI();
        initData();
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            finish();
            return true;
        } else
        {
            return super.onKeyDown(i, keyevent);
        }
    }

    protected void onPause()
    {
        super.onPause();
        stopTimer();
    }

    protected void onResume()
    {
        startTimer();
        super.onResume();
    }

    public final void setAlarmTask(int i)
    {
        if (i <= 0)
        {
            return;
        }
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(12, i);
        SharedPreferencesUtil.getInstance(this).saveLong("plan_play_stop_time", calendar.getTimeInMillis());
        Logger.log("dl_alarm", (new StringBuilder()).append("setAlarmTask:").append(ToolUtil.formatTime(calendar.getTimeInMillis())).toString());
        if (mIndexDelayMinutes != 1)
        {
            AlarmManager alarmmanager = (AlarmManager)getSystemService("alarm");
            if (android.os.Build.VERSION.SDK_INT >= 19)
            {
                alarmmanager.setExact(0, calendar.getTimeInMillis(), getAlarmPendingIntent());
            } else
            {
                alarmmanager.set(0, calendar.getTimeInMillis(), getAlarmPendingIntent());
            }
        }
        startTimer();
    }

    public final void updateSelected(int i)
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(this);
        if (i >= 1)
        {
            int j = 0;
            while (j < list.size()) 
            {
                WeekDay weekday = (WeekDay)list.get(j);
                weekday.setSwitchOn(true);
                boolean flag;
                if (j == i)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                weekday.isSelected = flag;
                weekday.indexSelected = i;
                j++;
            }
            if (isLiving)
            {
                mIndexDelayMinutes = i + 1;
            } else
            {
                mIndexDelayMinutes = i;
            }
            sharedpreferencesutil.saveBoolean("isOnForPlan", true);
            sharedpreferencesutil.saveInt("delay_minutes_index", mIndexDelayMinutes);
            return;
        }
        for (i = 0; i < list.size(); i++)
        {
            WeekDay weekday1 = (WeekDay)list.get(i);
            weekday1.setSwitchOn(false);
            weekday1.isSelected = false;
            weekday1.indexSelected = -1;
            weekday1.timeLeft = 0L;
        }

        sharedpreferencesutil.saveBoolean("isOnForPlan", false);
        sharedpreferencesutil.saveInt("delay_minutes_index", -1);
        mIndexDelayMinutes = -1;
    }









/*
    static Timer access$602(PlanTerminateActivity planterminateactivity, Timer timer)
    {
        planterminateactivity.mTimer = timer;
        return timer;
    }

*/
}
