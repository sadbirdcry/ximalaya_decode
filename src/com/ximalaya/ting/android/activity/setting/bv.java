// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.media.MediaPlayer;
import com.ximalaya.ting.android.model.alarm.Alarms;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            WakeUpSettingActivity

class bv extends MyAsyncTask
{

    final String a;
    final WakeUpSettingActivity b;

    bv(WakeUpSettingActivity wakeupsettingactivity, String s)
    {
        b = wakeupsettingactivity;
        a = s;
        super();
    }

    protected transient ArrayList a(Void avoid[])
    {
        if (a.contains("?"))
        {
            avoid = (new StringBuilder()).append(a).append("&page=").append(WakeUpSettingActivity.access$804(b)).toString();
        } else
        {
            avoid = (new StringBuilder()).append(a).append("?page=").append(WakeUpSettingActivity.access$804(b)).toString();
        }
        return Alarms.getSoundInfoFromOnline(WakeUpSettingActivity.access$000(b), avoid);
    }

    protected void a(ArrayList arraylist)
    {
        if (arraylist != null && arraylist.size() > 0)
        {
            PlayTools.gotoPlay(0, arraylist, 0, WakeUpSettingActivity.access$000(b), null);
            if (LocalMediaService.getInstance() != null)
            {
                LocalMediaService.getInstance().setMediaPlayerErrorHandler(WakeUpSettingActivity.access$1500(b));
                LocalMediaService.getInstance().setMediaPlayerCompleteHandler(WakeUpSettingActivity.access$1600(b));
            }
            return;
        }
        WakeUpSettingActivity.access$802(b, -1);
        if (LocalMediaService.getInstance() != null)
        {
            LocalMediaService.getInstance().setMediaPlayerCompleteHandler(null);
        }
        WakeUpSettingActivity.access$602(b, WakeUpSettingActivity.access$700(b, WakeUpSettingActivity.access$000(b), 4, 0x7f060001));
        WakeUpSettingActivity.access$600(b).setLooping(true);
        WakeUpSettingActivity.access$600(b).start();
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((ArrayList)obj);
    }
}
