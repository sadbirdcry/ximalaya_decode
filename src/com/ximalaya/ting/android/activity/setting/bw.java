// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.media.MediaPlayer;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.LiveHistoryManage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            WakeUpSettingActivity

class bw extends MyAsyncTask
{

    final int a;
    final String b;
    final WakeUpSettingActivity c;

    bw(WakeUpSettingActivity wakeupsettingactivity, int i, String s)
    {
        c = wakeupsettingactivity;
        a = i;
        b = s;
        super();
    }

    public transient SoundInfo a(Void avoid[])
    {
        return LiveHistoryManage.getInstance(WakeUpSettingActivity.access$000(c)).getHistorySound(a);
    }

    public void a(SoundInfo soundinfo)
    {
        if (soundinfo != null)
        {
            soundinfo.playUrl32 = b;
            soundinfo.playUrl64 = b;
            soundinfo.programId = 0L;
            soundinfo.programScheduleId = 0L;
            soundinfo.startTime = "00:00";
            soundinfo.endTime = "24:00";
            soundinfo.playType = 1;
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            if (localmediaservice != null)
            {
                localmediaservice.stopPlayTask();
            }
            PlayTools.gotoPlay(0, soundinfo, WakeUpSettingActivity.access$000(c), true, null);
            LocalMediaService.getInstance().setMediaPlayerErrorHandler(WakeUpSettingActivity.access$1500(c));
            LocalMediaService.getInstance().setMediaPlayerCompleteHandler(WakeUpSettingActivity.access$1600(c));
            return;
        } else
        {
            WakeUpSettingActivity.access$802(c, -1);
            LocalMediaService.getInstance().setMediaPlayerCompleteHandler(null);
            WakeUpSettingActivity.access$602(c, WakeUpSettingActivity.access$700(c, WakeUpSettingActivity.access$000(c), 4, 0x7f060001));
            WakeUpSettingActivity.access$600(c).setLooping(true);
            WakeUpSettingActivity.access$600(c).start();
            return;
        }
    }

    public Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    public void onPostExecute(Object obj)
    {
        a((SoundInfo)obj);
    }
}
