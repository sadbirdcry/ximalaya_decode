// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import java.util.Calendar;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            RepeatSettingActivity, WakeUpSettingActivity, SelectRingtoneActivity

class cb
    implements android.widget.AdapterView.OnItemClickListener
{

    final WakeUpSettingActivity a;

    cb(WakeUpSettingActivity wakeupsettingactivity)
    {
        a = wakeupsettingactivity;
        super();
    }

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        switch (i)
        {
        case 0: // '\0'
        default:
            return;

        case 1: // '\001'
            adapterview = new Intent();
            adapterview.setClass(a, com/ximalaya/ting/android/activity/setting/RepeatSettingActivity);
            a.startActivityForResult(adapterview, 0);
            return;

        case 2: // '\002'
            adapterview = SharedPreferencesUtil.getInstance(WakeUpSettingActivity.access$000(a));
            if (adapterview.contains("alarm_hour") && adapterview.contains("alarm_minute"))
            {
                WakeUpSettingActivity.access$1002(a, adapterview.getInt("alarm_hour", 0));
                WakeUpSettingActivity.access$1102(a, adapterview.getInt("alarm_minute", 0));
            } else
            {
                adapterview = Calendar.getInstance();
                WakeUpSettingActivity.access$1002(a, adapterview.get(11));
                WakeUpSettingActivity.access$1102(a, adapterview.get(12));
            }
            (new TimePickerDialog(a, a, WakeUpSettingActivity.access$1000(a), WakeUpSettingActivity.access$1100(a), true)).show();
            return;

        case 3: // '\003'
            adapterview = new Intent();
            adapterview.setClass(a, com/ximalaya/ting/android/activity/setting/SelectRingtoneActivity);
            a.startActivityForResult(adapterview, 0);
            return;
        }
    }
}
