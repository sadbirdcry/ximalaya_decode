// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.setting;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.setting:
//            WakeUpSettingActivity

class cc
    implements android.content.DialogInterface.OnClickListener
{

    final WakeUpSettingActivity a;

    cc(WakeUpSettingActivity wakeupsettingactivity)
    {
        a = wakeupsettingactivity;
        super();
    }

    public void onClick(DialogInterface dialoginterface, int i)
    {
        if (WakeUpSettingActivity.access$600(a) != null)
        {
            WakeUpSettingActivity.access$1200(a);
        } else
        {
            LocalMediaService.getInstance().stopPlayTask();
            LocalMediaService.getInstance().setMediaPlayerErrorHandler(null);
        }
        dialoginterface = WakeUpSettingActivity.access$1300(a);
        dialoginterface.setAction("com.ximalaya.ting.android.action.ALARM_LATER");
        dialoginterface = PendingIntent.getBroadcast(a, 0, dialoginterface, 0);
        if (android.os.Build.VERSION.SDK_INT >= 19)
        {
            WakeUpSettingActivity.access$1400(a).setExact(0, System.currentTimeMillis() + 0x927c0L, dialoginterface);
        } else
        {
            WakeUpSettingActivity.access$1400(a).set(0, System.currentTimeMillis() + 0x927c0L, dialoginterface);
        }
        ToolUtil.cancelNotification(WakeUpSettingActivity.access$000(a), 6);
        ToolUtil.makeAlarmLaterNotification(a);
    }
}
