// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.zone;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.fragment.zone.ZoneFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.activity.zone:
//            PostCreateActivity

class t extends a
{

    final PostCreateActivity a;

    t(PostCreateActivity postcreateactivity)
    {
        a = postcreateactivity;
        super();
    }

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, PostCreateActivity.access$1700(a));
    }

    public void onFinish()
    {
        super.onFinish();
        if (a.pd != null)
        {
            a.pd.cancel();
            a.pd = null;
        }
    }

    public void onNetError(int i, String s)
    {
        Toast.makeText(a.getApplicationContext(), "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            s = null;
        }
        if (s == null || s.getIntValue("ret") != 0)
        {
            if (s == null)
            {
                s = null;
            } else
            {
                s = s.getString("msg");
            }
            if (TextUtils.isEmpty(s))
            {
                s = "\u53D1\u8868\u5E16\u5B50\u5931\u8D25";
            } else
            {
                s = (new StringBuilder()).append("\u53D1\u8868\u5E16\u5B50\u5931\u8D25:").append(s).toString();
            }
            Toast.makeText(a, s, 0).show();
            return;
        }
        Toast.makeText(a.getApplicationContext(), "\u53D1\u8868\u5E16\u5B50\u6210\u529F", 0).show();
        s = s.getString("result");
        if (TextUtils.isEmpty(s))
        {
            a.finish();
            return;
        }
        try
        {
            s = (PostModel)JSON.parseObject(s, com/ximalaya/ting/android/model/zone/PostModel);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
            s = null;
        }
        if (s == null)
        {
            a.finish();
            return;
        }
        PostCreateActivity.access$1200(a);
        SharedPreferencesUtil.getInstance(a).removeByKey("temp_zone_post");
        if (MainTabActivity2.isMainTabActivityAvaliable() && MainTabActivity2.mainTabActivity.containsFragment(com/ximalaya/ting/android/fragment/zone/ZoneFragment))
        {
            a.setResult(-1);
            a.finish();
            return;
        } else
        {
            s = new Bundle();
            s.putLong("zoneId", PostCreateActivity.access$1600(a));
            Intent intent = new Intent(a, com/ximalaya/ting/android/activity/MainTabActivity2);
            intent.addFlags(0x4000000);
            intent.putExtra(com.ximalaya.ting.android.a.s, s);
            intent.putExtra(com.ximalaya.ting.android.a.t, com/ximalaya/ting/android/fragment/zone/ZoneFragment);
            a.startActivity(intent);
            return;
        }
    }
}
