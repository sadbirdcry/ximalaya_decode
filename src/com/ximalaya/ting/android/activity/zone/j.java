// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.zone;

import android.graphics.drawable.Drawable;
import android.widget.EditText;
import com.ximalaya.ting.android.util.EmotionUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.zone:
//            PostCreateActivity

class j
    implements com.ximalaya.ting.android.view.emotion.EmotionSelectorPanel.OnEditContentListener
{

    final PostCreateActivity a;

    j(PostCreateActivity postcreateactivity)
    {
        a = postcreateactivity;
        super();
    }

    public void insert(String s, Drawable drawable)
    {
        if (PostCreateActivity.access$500(a).isFocused())
        {
            EmotionUtil.getInstance().insertEmotion(PostCreateActivity.access$500(a), s, drawable);
        }
    }

    public void remove()
    {
        if (PostCreateActivity.access$500(a).isFocused())
        {
            EmotionUtil.getInstance().deleteEmotion(PostCreateActivity.access$500(a));
        } else
        if (PostCreateActivity.access$600(a).getVisibility() == 0 && PostCreateActivity.access$600(a).isFocused())
        {
            EmotionUtil.getInstance().deleteEmotion(PostCreateActivity.access$600(a));
            return;
        }
    }
}
