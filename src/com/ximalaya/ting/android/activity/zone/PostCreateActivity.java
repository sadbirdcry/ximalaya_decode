// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.zone;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseFragmentActivity;
import com.ximalaya.ting.android.adapter.zone.PicAddAdapter;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.zone.ZoneConstants;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.model.zone.PostEditorData;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.BitmapUtils;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.emotion.EmotionSelectorPanel;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgItem;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.activity.zone:
//            p, f, g, h, 
//            i, t, a, j, 
//            k, l, m, e, 
//            o, b, c, d, 
//            n, u, v, w, 
//            x

public class PostCreateActivity extends BaseFragmentActivity
    implements android.view.View.OnClickListener, android.view.ViewTreeObserver.OnGlobalLayoutListener
{
    public class PopupWindows extends PopupWindow
    {

        final PostCreateActivity this$0;

        private void takePhoto()
        {
            Intent intent;
            File file;
            intent = new Intent("android.media.action.IMAGE_CAPTURE");
            file = new File((new StringBuilder()).append(com.ximalaya.ting.android.a.af).append(File.separator).append(System.currentTimeMillis()).append(".jpg").toString());
            if (file.exists()) goto _L2; else goto _L1
_L1:
            file.getParentFile().mkdirs();
_L4:
            mPhotoPath = file.getPath();
            intent.putExtra("output", Uri.fromFile(file));
            startActivityForResult(intent, 0);
            return;
_L2:
            if (file.exists())
            {
                file.delete();
            }
            if (true) goto _L4; else goto _L3
_L3:
        }


        public PopupWindows(Context context, View view)
        {
            this$0 = PostCreateActivity.this;
            super(context, null, 0x7f0b0012);
            Object obj = View.inflate(context, 0x7f03012a, null);
            setOutsideTouchable(true);
            setWidth(-1);
            setHeight(-1);
            setContentView(((View) (obj)));
            setAnimationStyle(0x7f0b0012);
            update();
            context = (Button)((View) (obj)).findViewById(0x7f0a04ac);
            view = (Button)((View) (obj)).findViewById(0x7f0a04ad);
            obj = (Button)((View) (obj)).findViewById(0x7f0a04ae);
            setOnDismissListener(new u(this, PostCreateActivity.this));
            context.setOnClickListener(new v(this, PostCreateActivity.this));
            view.setOnClickListener(new w(this, PostCreateActivity.this));
            ((Button) (obj)).setOnClickListener(new x(this, PostCreateActivity.this));
        }
    }

    class a extends MyAsyncTask
    {

        Map a;
        BaseModel b;
        final PostCreateActivity c;

        protected transient Void a(Object aobj[])
        {
            aobj = (List)aobj[0];
            if (aobj != null && !((List) (aobj)).isEmpty()) goto _L2; else goto _L1
_L1:
            return null;
_L2:
            a = new HashMap();
            aobj = ((List) (aobj)).iterator();
_L6:
            if (!((Iterator) (aobj)).hasNext()) goto _L1; else goto _L3
_L3:
            String s;
            Object obj;
            s = (String)((Iterator) (aobj)).next();
            obj = c.uploadFile(s);
            if (obj == null) goto _L1; else goto _L4
_L4:
            obj = JSON.parseObject(((String) (obj)));
            if (obj == null) goto _L6; else goto _L5
_L5:
            b = new BaseModel();
            b.ret = ((JSONObject) (obj)).getInteger("ret").intValue();
            if (b.ret != 0) goto _L1; else goto _L7
_L7:
            b.msg = ((JSONObject) (obj)).getString("msg");
            if (b.msg == null) goto _L6; else goto _L8
_L8:
            obj = JSONObject.parseArray(b.msg);
            if (((JSONArray) (obj)).size() <= 0) goto _L6; else goto _L9
_L9:
            obj = (JSONObject)((JSONArray) (obj)).get(0);
            if (!((JSONObject) (obj)).containsKey("uploadTrack")) goto _L6; else goto _L10
_L10:
            obj = ((JSONObject) (obj)).getJSONObject("uploadTrack");
            if (obj != null)
            {
                try
                {
                    obj = ((JSONObject) (obj)).getString("id");
                    if (!TextUtils.isEmpty(((CharSequence) (obj))))
                    {
                        a.put(s, obj);
                    }
                }
                catch (Exception exception)
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
                }
            }
              goto _L6
        }

        protected void a(Void void1)
        {
            if (c.isFinishing())
            {
                return;
            }
            if (a != null)
            {
                java.util.Map.Entry entry;
                for (void1 = a.entrySet().iterator(); void1.hasNext(); c.mUploadedImgs.put(entry.getKey(), entry.getValue()))
                {
                    entry = (java.util.Map.Entry)void1.next();
                }

            }
            c.verifyImgAllUploaded(false);
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((Void)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        a()
        {
            c = PostCreateActivity.this;
            super();
        }
    }


    private static final int SELECT_PICTURE = 1;
    private static final int TAKE_PICTURE = 0;
    private static final int ZOOM_PICTURE = 2;
    private ImageView mBackImg;
    private EditText mContentEt;
    private String mContentStr;
    private Dialog mDimBgDialog;
    private ImageView mEmotionBtnIv;
    private EmotionSelectorPanel mEmotionSelector;
    private FrameLayout mExtraLayout;
    private View mMainLayout;
    private InputMethodManager mMethodManager;
    private String mPhotoPath;
    private PicAddAdapter mPicAdapter;
    private ImageView mPicBtnIv;
    private GridView mPicGridView;
    private List mPicPathList;
    private PopupWindows mPopupDialog;
    private ImageView mSoftInputBtnIv;
    private View mSubmitTv;
    private EditText mTitleEt;
    private String mTitleStr;
    private TextView mTitleTv;
    private List mToDelPics;
    private a mUploadImgTask;
    private Map mUploadedImgs;
    private long mZoneId;
    ProgressDialog pd;

    public PostCreateActivity()
    {
        mPicPathList = new ArrayList();
        mUploadedImgs = new HashMap();
        mToDelPics = new ArrayList();
        pd = null;
    }

    private String buildImgIdParam()
    {
        StringBuilder stringbuilder = new StringBuilder();
        Iterator iterator = mPicPathList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            String s = (String)iterator.next();
            if (!s.equals("add_defautl"))
            {
                s = (String)mUploadedImgs.get(s);
                if (s != null)
                {
                    stringbuilder.append(s).append(",");
                }
            }
        } while (true);
        if (stringbuilder.length() > 0)
        {
            return stringbuilder.substring(0, stringbuilder.length() - 1);
        } else
        {
            return stringbuilder.toString();
        }
    }

    private void compressImages(List list, boolean flag)
    {
        if (pd == null)
        {
            pd = createProgressDialog("\u6DFB\u52A0\u56FE\u7247", "\u52A0\u8F7D\u4E2D...");
        } else
        {
            pd.setTitle("\u6DFB\u52A0\u56FE\u7247");
            pd.setMessage("\u52A0\u8F7D\u4E2D...");
        }
        pd.show();
        BitmapUtils.compressImages(list, false, new p(this, flag));
    }

    private void createPopupDialog()
    {
        mDimBgDialog = new Dialog(this, 0x1030010);
        View view = LayoutInflater.from(this).inflate(0x7f03006a, null);
        android.view.WindowManager.LayoutParams layoutparams = mDimBgDialog.getWindow().getAttributes();
        layoutparams.dimAmount = 0.5F;
        mDimBgDialog.getWindow().setAttributes(layoutparams);
        mDimBgDialog.getWindow().addFlags(2);
        mDimBgDialog.setContentView(view);
        mDimBgDialog.setCanceledOnTouchOutside(true);
        if (mPopupDialog == null)
        {
            mPopupDialog = new PopupWindows(this, mPicGridView);
        }
        view.setOnClickListener(new com.ximalaya.ting.android.activity.zone.f(this));
        mDimBgDialog.setOnCancelListener(new g(this));
        mDimBgDialog.setOnShowListener(new h(this, view));
    }

    private ProgressDialog createProgressDialog(String s, String s1)
    {
        MyProgressDialog myprogressdialog = new MyProgressDialog(this);
        myprogressdialog.setOnKeyListener(new i(this));
        myprogressdialog.setCanceledOnTouchOutside(false);
        myprogressdialog.setTitle(s);
        myprogressdialog.setMessage(s1);
        return myprogressdialog;
    }

    private void disableWhenEditTitle()
    {
        mEmotionBtnIv.setEnabled(false);
        mPicBtnIv.setEnabled(false);
        mExtraLayout.setVisibility(8);
        mEmotionSelector.hideEmotionPanel();
    }

    private void doSubmit()
    {
        if (mTitleEt.getText() == null || mTitleEt.getText().toString().trim().length() < 4 || mTitleEt.getText().toString().trim().length() > 60)
        {
            Toast.makeText(getApplicationContext(), "\u6807\u9898\u6700\u77ED4\u5B57\uFF0C\u6700\u957F60\u5B57", 0).show();
            return;
        }
        if (!hasAddPic() && (mContentEt.getText() == null || mContentEt.getText().toString().length() == 0))
        {
            showToast("\u5185\u5BB9\u4E0D\u80FD\u4E3A\u7A7A");
            return;
        }
        if (mContentEt.getText() != null && mContentEt.getText().length() > 10000)
        {
            Toast.makeText(getApplicationContext(), "\u5185\u5BB9\u4E0D\u8D85\u8FC710000\u5B57", 0).show();
            return;
        }
        if (pd == null)
        {
            pd = createProgressDialog("\u63D0\u793A", "\u6B63\u5728\u53D1\u8868\u4E2D...");
        } else
        {
            pd.setTitle("\u63D0\u793A");
            pd.setMessage("\u6B63\u5728\u53D1\u8868\u4E2D...");
        }
        pd.show();
        if (!hasAddPic())
        {
            doSubmitPost();
            return;
        } else
        {
            verifyImgAllUploaded(true);
            return;
        }
    }

    private void doSubmitPost()
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("zoneId", mZoneId);
        requestparams.put("title", mTitleEt.getText().toString());
        requestparams.put("content", mContentEt.getText().toString());
        if (!mUploadedImgs.isEmpty())
        {
            requestparams.put("imageIds", buildImgIdParam());
        }
        f.a().b(ZoneConstants.URL_POSTS, requestparams, DataCollectUtil.getDataFromView(mSubmitTv), new t(this));
    }

    private void finishAddPic()
    {
        if (pd != null)
        {
            pd.cancel();
            pd = null;
        }
        genAddBtnToList();
        mPicAdapter.notifyDataSetChanged();
    }

    private void genAddBtnToList()
    {
        mPicPathList.remove("add_default");
        if (mPicPathList.size() < 9)
        {
            mPicPathList.add("add_default");
        }
    }

    private void getTempFromPref()
    {
        LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
        if (logininfomodel != null)
        {
            SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(this);
            Object obj = sharedpreferencesutil.getString("temp_zone_post");
            if (!TextUtils.isEmpty(((CharSequence) (obj))))
            {
                try
                {
                    obj = (PostEditorData)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/zone/PostEditorData);
                }
                // Misplaced declaration of an exception variable
                catch (Object obj)
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
                    obj = null;
                }
                if (obj != null)
                {
                    if (((PostEditorData) (obj)).getUid() != ((UserInfoModel) (logininfomodel)).uid)
                    {
                        sharedpreferencesutil.removeByKey("temp_zone_post");
                        return;
                    }
                    if (((PostEditorData) (obj)).getZoneId() == mZoneId)
                    {
                        mTitleStr = ((PostEditorData) (obj)).getTitle();
                        mContentStr = ((PostEditorData) (obj)).getContent();
                        mToDelPics.clear();
                        mPicPathList.clear();
                        mPicPathList.addAll(((PostEditorData) (obj)).getImagePaths());
                        mUploadedImgs = ((PostEditorData) (obj)).getUploadedImgs();
                        mPicAdapter.notifyDataSetChanged();
                        return;
                    }
                }
            }
        }
    }

    private boolean hasAddPic()
    {
        if (!mPicPathList.contains("add_default")) goto _L2; else goto _L1
_L1:
        if (mPicPathList.size() <= 1) goto _L4; else goto _L3
_L3:
        return true;
_L4:
        return false;
_L2:
        if (mPicPathList.size() <= 0)
        {
            return false;
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    private void hideExtraViews()
    {
        mEmotionSelector.hideEmotionPanel();
        mEmotionSelector.setVisibility(8);
        mPicGridView.setVisibility(8);
        mExtraLayout.setVisibility(8);
    }

    private void initData()
    {
        Object obj = getIntent();
        if (obj != null)
        {
            mZoneId = ((Intent) (obj)).getLongExtra("zoneId", -1L);
        }
        getTempFromPref();
        EditText edittext = mContentEt;
        if (mContentStr == null)
        {
            obj = null;
        } else
        {
            obj = EmotionUtil.getInstance().convertEmotionTextWithChangeLine(mContentStr);
        }
        edittext.setText(((CharSequence) (obj)));
        mTitleEt.setText(mTitleStr);
        setFocusOnTitle();
        genAddBtnToList();
    }

    private void initListener()
    {
        mSubmitTv.setOnClickListener(this);
        mEmotionBtnIv.setOnClickListener(this);
        mPicBtnIv.setOnClickListener(this);
        mSoftInputBtnIv.setOnClickListener(this);
        mBackImg.setOnClickListener(this);
        mMainLayout.getViewTreeObserver().addOnGlobalLayoutListener(this);
        mPicGridView.setOnItemClickListener(new com.ximalaya.ting.android.activity.zone.a(this));
        mEmotionSelector.setOnEditContentListener(new j(this));
        mTitleEt.setOnFocusChangeListener(new k(this));
        mContentEt.setOnClickListener(new l(this));
        mContentEt.setOnFocusChangeListener(new m(this));
    }

    private void initView()
    {
        mTitleTv = (TextView)findViewById(0x7f0a00ae);
        mTitleTv.setText("\u53D1\u8868\u5E16\u5B50");
        mBackImg = (ImageView)findViewById(0x7f0a007b);
        mMethodManager = (InputMethodManager)getSystemService("input_method");
        mMainLayout = findViewById(0x7f0a00ce);
        mSubmitTv = (TextView)findViewById(0x7f0a0750);
        mSubmitTv.setVisibility(0);
        mTitleEt = (EditText)findViewById(0x7f0a00d0);
        mContentEt = (EditText)findViewById(0x7f0a00d2);
        mExtraLayout = (FrameLayout)findViewById(0x7f0a00d3);
        mEmotionBtnIv = (ImageView)findViewById(0x7f0a00d4);
        mSoftInputBtnIv = (ImageView)findViewById(0x7f0a00d6);
        mPicBtnIv = (ImageView)findViewById(0x7f0a00d5);
        mEmotionSelector = (EmotionSelectorPanel)findViewById(0x7f0a00d9);
        mPicGridView = (GridView)findViewById(0x7f0a00d8);
        mPicGridView.setSelector(new ColorDrawable(0));
        mPicAdapter = new PicAddAdapter(this, mPicPathList, mUploadedImgs);
        mPicGridView.setAdapter(mPicAdapter);
    }

    private boolean isAllUploaded()
    {
        for (Iterator iterator = mPicPathList.iterator(); iterator.hasNext();)
        {
            String s = (String)iterator.next();
            if (!s.equals("add_default") && mUploadedImgs.get(s) == null)
            {
                return false;
            }
        }

        return true;
    }

    private boolean isEditting()
    {
        return mContentEt.getText() != null && mContentEt.getText().toString().trim().length() > 0 || mTitleEt.getText() != null && mTitleEt.getText().toString().trim().length() > 0;
    }

    private void removePicDefault()
    {
        mPicPathList.remove("add_default");
    }

    private void resetData()
    {
        mContentStr = null;
        mTitleStr = null;
        mTitleEt.setText(null);
        mContentEt.setText(null);
        SharedPreferencesUtil.getInstance(this).removeByKey("temp_zone_post");
    }

    private void saveTempToPref()
    {
        Object obj1 = null;
        Object obj = UserInfoMannage.getInstance().getUser();
        if (obj != null)
        {
            if (isEditting())
            {
                Object obj2 = new PostEditorData();
                ((PostEditorData) (obj2)).setUid(((UserInfoModel) (obj)).uid);
                ((PostEditorData) (obj2)).setZoneId(mZoneId);
                ((PostEditorData) (obj2)).setImagePaths(mPicPathList);
                ((PostEditorData) (obj2)).setUploadedImgs(mUploadedImgs);
                if (mTitleEt.getText() == null)
                {
                    obj = null;
                } else
                {
                    obj = mTitleEt.getText().toString();
                }
                ((PostEditorData) (obj2)).setTitle(((String) (obj)));
                if (mContentEt.getText() == null)
                {
                    obj = obj1;
                } else
                {
                    obj = mContentEt.getText().toString();
                }
                ((PostEditorData) (obj2)).setContent(((String) (obj)));
                SharedPreferencesUtil.getInstance(this).saveString("temp_zone_post", JSON.toJSONString(obj2));
            }
            if (!mToDelPics.isEmpty())
            {
                obj1 = SharedPreferencesUtil.getInstance(this).getArrayList("temp_zone_img");
                obj = obj1;
                if (obj1 == null)
                {
                    obj = new ArrayList();
                }
                obj1 = mToDelPics.iterator();
                do
                {
                    if (!((Iterator) (obj1)).hasNext())
                    {
                        break;
                    }
                    obj2 = (String)((Iterator) (obj1)).next();
                    if (!((List) (obj)).contains(obj2))
                    {
                        ((List) (obj)).add(obj2);
                    }
                } while (true);
                SharedPreferencesUtil.getInstance(this).saveArrayList("temp_zone_img", (ArrayList)obj);
                return;
            }
        }
    }

    private void setFocusOnTitle()
    {
        mTitleEt.requestFocus();
        mMethodManager.showSoftInput(mTitleEt, 0);
        disableWhenEditTitle();
    }

    private void showEmotionOrPic(boolean flag)
    {
        mExtraLayout.setVisibility(0);
        if (flag)
        {
            mPicGridView.setVisibility(8);
            mEmotionSelector.setVisibility(0);
            return;
        } else
        {
            mPicGridView.setVisibility(0);
            mEmotionSelector.setVisibility(8);
            return;
        }
    }

    private void switchEmotionInput()
    {
        if (!mContentEt.isFocused())
        {
            mContentEt.requestFocus();
        }
        if (mEmotionBtnIv.getVisibility() == 0)
        {
            mMethodManager.hideSoftInputFromWindow(mContentEt.getWindowToken(), 0);
            mContentEt.postDelayed(new e(this), 250L);
            return;
        } else
        {
            mMethodManager.toggleSoftInputFromWindow(mContentEt.getWindowToken(), 0, 0);
            mExtraLayout.setVisibility(8);
            return;
        }
    }

    private void switchEmotionInputBtn(boolean flag)
    {
        if (flag)
        {
            mEmotionBtnIv.setVisibility(0);
            mSoftInputBtnIv.setVisibility(8);
            return;
        } else
        {
            mEmotionBtnIv.setVisibility(8);
            mSoftInputBtnIv.setVisibility(0);
            return;
        }
    }

    private String uploadFile(String s)
    {
        s = new File(s);
        if (!s.exists())
        {
            return null;
        }
        String s1 = ZoneConstants.URL_UPLOAD_IMG;
        if (loginInfoModel == null)
        {
            loginInfoModel = UserInfoMannage.getInstance().getUser();
        }
        try
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("fileSize", (new StringBuilder()).append(s.length()).append("").toString());
            requestparams.put("uid", (new StringBuilder()).append(loginInfoModel.uid).append("").toString());
            requestparams.put("token", loginInfoModel.token);
            requestparams.put("myfile", s);
            s = f.a().b(s1, requestparams, mSubmitTv, null);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return null;
        }
        return s;
    }

    private void verifyImgAllUploaded(boolean flag)
    {
        if (isAllUploaded())
        {
            doSubmitPost();
        } else
        if (flag)
        {
            if (mUploadImgTask == null || mUploadImgTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
            {
                mUploadImgTask = new a();
                mUploadImgTask.myexec(new Object[] {
                    mPicPathList
                });
                return;
            }
        } else
        {
            if (pd != null)
            {
                pd.cancel();
                pd = null;
            }
            showToast("\u56FE\u7247\u672A\u5168\u90E8\u4E0A\u4F20\u6210\u529F\uFF0C\u8BF7\u91CD\u8BD5");
            mPicAdapter.notifyDataSetChanged();
            return;
        }
    }

    protected void onActivityResult(int i1, int j1, Intent intent)
    {
        i1;
        JVM INSTR tableswitch 0 2: default 28
    //                   0 29
    //                   1 91
    //                   2 194;
           goto _L1 _L2 _L3 _L4
_L1:
        return;
_L2:
        if (j1 == -1 && !TextUtils.isEmpty(mPhotoPath))
        {
            mPicPathList.add(mPhotoPath);
            mPicPathList.remove("add_default");
            compressImages(Arrays.asList(new String[] {
                mPhotoPath
            }), true);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (j1 == -1)
        {
            Object obj = (List)intent.getSerializableExtra("image_list");
            if (obj != null)
            {
                intent = new ArrayList();
                for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); intent.add(((ImgItem)((Iterator) (obj)).next()).getPath())) { }
                mPicPathList.addAll(intent);
                mPicPathList.remove("add_default");
                compressImages(intent, false);
                return;
            }
        }
        continue; /* Loop/switch isn't completed */
_L4:
        if (j1 == -1)
        {
            intent = (List)intent.getSerializableExtra("image_list");
            if (intent != null)
            {
                mPicPathList.clear();
                mPicPathList.addAll(intent);
                finishAddPic();
                return;
            }
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    public void onBackPressed()
    {
        if (mPopupDialog != null && mPopupDialog.isShowing())
        {
            mPopupDialog.dismiss();
            return;
        }
        if (mExtraLayout.getVisibility() == 0)
        {
            hideExtraViews();
            return;
        }
        if (mUploadImgTask != null && mUploadImgTask.getStatus() != android.os.AsyncTask.Status.FINISHED)
        {
            mMethodManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
            return;
        }
        if (isEditting())
        {
            (new DialogBuilder(this)).setMessage("\u60A8\u8FD8\u6709\u672A\u53D1\u8868\u7684\u5185\u5BB9\uFF0C\u786E\u8BA4\u653E\u5F03\u5417\uFF1F").setOkBtn(new o(this)).showConfirm();
            return;
        } else
        {
            super.onBackPressed();
            return;
        }
    }

    public void onClick(View view)
    {
        if (!OneClickHelper.getInstance().onClick(view)) goto _L2; else goto _L1
_L1:
        view.getId();
        JVM INSTR lookupswitch 5: default 64
    //                   2131361915: 65
    //                   2131362004: 70
    //                   2131362005: 80
    //                   2131362006: 75
    //                   2131363664: 255;
           goto _L2 _L3 _L4 _L5 _L6 _L7
_L2:
        return;
_L3:
        onBackPressed();
        return;
_L4:
        switchEmotionInput();
        return;
_L6:
        switchEmotionInput();
        return;
_L5:
        if (!mPicPathList.contains("add_default") && mPicPathList.size() == 9)
        {
            mMethodManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
            if (mExtraLayout.getVisibility() == 0 && mPicGridView.getVisibility() == 0)
            {
                showToast("\u6700\u591A\u9009\u62E99\u5F20\u56FE\u7247");
                return;
            } else
            {
                mContentEt.postDelayed(new b(this), 250L);
                return;
            }
        }
        if (!hasAddPic() || mExtraLayout.getVisibility() == 0 && mPicGridView.getVisibility() == 0)
        {
            mContentEt.postDelayed(new c(this), 50L);
        }
        mMethodManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
        mContentEt.postDelayed(new d(this), 250L);
        return;
_L7:
        doSubmit();
        return;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030022);
        initView();
        initData();
        initListener();
        ToolUtil.onEvent(this, "zone_post_create");
    }

    protected void onDestroy()
    {
        if (android.os.Build.VERSION.SDK_INT >= 16)
        {
            mMainLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
        } else
        {
            mMainLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
        super.onDestroy();
    }

    public void onGlobalLayout()
    {
        Rect rect = new Rect();
        mMainLayout.getWindowVisibleDisplayFrame(rect);
        if (Math.abs(mMainLayout.getRootView().getHeight() - (rect.bottom - rect.top)) > 100 || mExtraLayout.getVisibility() != 0 || mPicGridView.getVisibility() == 0)
        {
            switchEmotionInputBtn(true);
            return;
        } else
        {
            switchEmotionInputBtn(false);
            return;
        }
    }

    public void onPause()
    {
        super.onPause();
        if (!isFinishing())
        {
            if (pd != null)
            {
                pd.dismiss();
            }
            if (mPopupDialog != null)
            {
                mPopupDialog.dismiss();
            }
        }
        if (mTitleTv != null)
        {
            mTitleTv.post(new n(this));
        }
    }

    public void onResume()
    {
        super.onResume();
    }

    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        saveTempToPref();
    }









/*
    static String access$1402(PostCreateActivity postcreateactivity, String s)
    {
        postcreateactivity.mPhotoPath = s;
        return s;
    }

*/









/*
    static PopupWindows access$2002(PostCreateActivity postcreateactivity, PopupWindows popupwindows)
    {
        postcreateactivity.mPopupDialog = popupwindows;
        return popupwindows;
    }

*/











}
