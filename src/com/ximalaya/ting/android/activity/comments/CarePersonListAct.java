// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.comments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.comment.CarePersonModel;
import com.ximalaya.ting.android.model.comment.CarePersonModelList;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.RoundedImageView;
import com.ximalaya.ting.android.view.bounceview.BounceListView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.comments:
//            a, b

public class CarePersonListAct extends BaseActivity
{
    class a extends MyAsyncTask
    {

        final CarePersonListAct a;

        protected transient CarePersonModelList a(String as[])
        {
            String s;
            as = null;
            s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/following/user").toString();
            if (a.loginInfoModel == null)
            {
                a.loginInfoModel = UserInfoMannage.getInstance().getUser();
            }
            s = f.a().b(s, null, a.rootView, a.rootView);
            if (s == null) goto _L2; else goto _L1
_L1:
            if (JSON.parseObject(s).getIntValue("ret") != 0) goto _L4; else goto _L3
_L3:
            as = (CarePersonModelList)JSON.parseObject(s, com/ximalaya/ting/android/model/comment/CarePersonModelList);
_L2:
            return as;
            as;
            as.printStackTrace();
            return null;
_L4:
            as = null;
            if (true) goto _L2; else goto _L5
_L5:
        }

        protected void a(CarePersonModelList carepersonmodellist)
        {
            while (a == null || a.isFinishing() || carepersonmodellist == null) 
            {
                return;
            }
            a.userList = carepersonmodellist.list;
            if (a.adapter == null)
            {
                a.adapter = a. new b(a.userList);
                a.listView.setAdapter(a.adapter);
                return;
            } else
            {
                a.adapter.a(a.userList);
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((String[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((CarePersonModelList)obj);
        }

        a()
        {
            a = CarePersonListAct.this;
            super();
        }
    }

    class b extends BaseAdapter
    {

        List a;
        final CarePersonListAct b;

        public CarePersonModel a(int i)
        {
            if (a == null)
            {
                return null;
            } else
            {
                return (CarePersonModel)a.get(i);
            }
        }

        public void a(List list)
        {
            a = list;
        }

        public int getCount()
        {
            if (a == null)
            {
                return 0;
            } else
            {
                return a.size();
            }
        }

        public Object getItem(int i)
        {
            return a(i);
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
label0:
            {
                CarePersonModel carepersonmodel;
                if (view == null)
                {
                    view = b.getLayoutInflater().inflate(0x7f03004f, null);
                    viewgroup = b. new c();
                    viewgroup.a = (RoundedImageView)view.findViewById(0x7f0a019b);
                    viewgroup.b = (ImageView)view.findViewById(0x7f0a019d);
                    viewgroup.c = (TextView)view.findViewById(0x7f0a019c);
                    viewgroup.d = view.findViewById(0x7f0a0645);
                    viewgroup.e = view.findViewById(0x7f0a019a);
                    view.setTag(viewgroup);
                } else
                {
                    viewgroup = (c)view.getTag();
                }
                carepersonmodel = (CarePersonModel)a.get(i);
                if (carepersonmodel != null)
                {
                    if (!carepersonmodel.flag)
                    {
                        break label0;
                    }
                    ((c) (viewgroup)).d.setVisibility(8);
                    ((c) (viewgroup)).e.setVisibility(0);
                    ImageManager2.from(b).displayImage(((c) (viewgroup)).a, carepersonmodel.smallLogo, 0x7f0202e0);
                    if (carepersonmodel.isVerified)
                    {
                        ((c) (viewgroup)).b.setVisibility(0);
                    } else
                    {
                        ((c) (viewgroup)).b.setVisibility(8);
                    }
                    ((c) (viewgroup)).c.setText(carepersonmodel.nickname);
                }
                return view;
            }
            ((c) (viewgroup)).d.setVisibility(0);
            ((c) (viewgroup)).e.setVisibility(8);
            return view;
        }

        b(List list)
        {
            b = CarePersonListAct.this;
            super();
            a = list;
        }
    }

    class c
    {

        RoundedImageView a;
        ImageView b;
        TextView c;
        View d;
        View e;
        final CarePersonListAct f;

        c()
        {
            f = CarePersonListAct.this;
            super();
        }
    }


    private b adapter;
    private BounceListView listView;
    private String meHeadUrl;
    private String title;
    private List userList;

    public CarePersonListAct()
    {
    }

    private void findViews()
    {
        initCommon();
        listView = (BounceListView)findViewById(0x7f0a019e);
        topTextView = (TextView)findViewById(0x7f0a00ae);
    }

    private void initDatas()
    {
        title = getIntent().getStringExtra("title");
        meHeadUrl = getIntent().getStringExtra("meHeadUrl");
    }

    private void initViews()
    {
        topTextView.setText(title);
        retButton.setOnClickListener(new com.ximalaya.ting.android.activity.comments.a(this));
        listView.setOnItemClickListener(new com.ximalaya.ting.android.activity.comments.b(this));
        (new a()).myexec(new String[0]);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030050);
        initDatas();
        findViews();
        initViews();
    }



/*
    static List access$002(CarePersonListAct carepersonlistact, List list)
    {
        carepersonlistact.userList = list;
        return list;
    }

*/



/*
    static b access$102(CarePersonListAct carepersonlistact, b b1)
    {
        carepersonlistact.adapter = b1;
        return b1;
    }

*/



}
