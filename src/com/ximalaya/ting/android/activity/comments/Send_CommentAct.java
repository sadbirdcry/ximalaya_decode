// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.comments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.setting.ShareSettingModel;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Session;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.comments:
//            c, d, e, f, 
//            g, h, i, j

public class Send_CommentAct extends BaseActivity
{
    class a extends MyAsyncTask
    {

        ProgressDialog a;
        final Send_CommentAct b;

        protected transient String a(Void avoid[])
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/sync/get").toString();
            RequestParams requestparams = new RequestParams();
            avoid = f.a().a(avoid, requestparams, b.rootView, b.rootView);
            if (avoid != null)
            {
                JSONObject jsonobject = JSON.parseObject(avoid);
                if (jsonobject.getInteger("ret").intValue() == 0)
                {
                    try
                    {
                        b.parseData(avoid);
                        SharedPreferencesUtil.getInstance(b.mCt).saveString("share_setting", avoid);
                    }
                    // Misplaced declaration of an exception variable
                    catch (Void avoid[])
                    {
                        return "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
                    }
                    return "0";
                } else
                {
                    return jsonobject.getString("msg");
                }
            } else
            {
                return "\u7F51\u7EDC\u8BBF\u95EE\u5F02\u5E38";
            }
        }

        protected void a(String s)
        {
            if (a != null)
            {
                a.cancel();
                a = null;
            }
            if (b == null || b.isFinishing())
            {
                return;
            }
            if (!"0".equals(s))
            {
                s = SharedPreferencesUtil.getInstance(b.mCt).getString("share_setting");
                b.parseData(s);
            }
            b.refreshShareSetting();
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((String)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            a = new MyProgressDialog(b);
            a.setMessage("\u6B63\u5728\u52A0\u8F7D\u6570\u636E\uFF0C\u8BF7\u7B49\u5F85...");
            a.show();
        }

        a()
        {
            b = Send_CommentAct.this;
            super();
            a = null;
        }
    }

    class b extends MyAsyncTask
    {

        ProgressDialog a;
        final Send_CommentAct b;

        protected transient BaseModel a(String as[])
        {
            if (b.loginInfoModel == null)
            {
                return null;
            }
            String s = as[0];
            as = ApiUtil.getApiHost();
            RequestParams requestparams;
            if (b.commentType == 1)
            {
                as = (new StringBuilder()).append(as).append("mobile/comment/create").toString();
            } else
            {
                as = (new StringBuilder()).append(as).append("mobile/track/relay").toString();
            }
            requestparams = new RequestParams();
            requestparams.put("trackId", b.trackId);
            if (!"".equals(s))
            {
                requestparams.put("content", s);
            }
            if (Utilities.isNotBlank(b.parentId))
            {
                requestparams.put("parentId", b.parentId);
            }
            if (Utilities.isNotBlank(b.second))
            {
                requestparams.put("second", b.second);
            }
            s = b.getCommentShareList();
            if (Utilities.isNotBlank(s))
            {
                requestparams.put("shareList", s);
            }
            as = f.a().b(as, requestparams, b.btn_complete, b.btn_complete);
            Logger.d("resultJson:", as);
            if (!Utilities.isNotBlank(as)) goto _L2; else goto _L1
_L1:
            as = (BaseModel)JSON.parseObject(as, com/ximalaya/ting/android/model/BaseModel);
_L4:
            return as;
            as;
            as.printStackTrace();
_L2:
            as = null;
            if (true) goto _L4; else goto _L3
_L3:
        }

        protected void a(BaseModel basemodel)
        {
            if (b == null || b.isFinishing())
            {
                return;
            }
            if (a != null)
            {
                a.cancel();
                a = null;
            }
            if (basemodel == null)
            {
                Toast.makeText(b.getApplicationContext(), "\u56DE\u590D\u5931\u8D25", 1).show();
                return;
            }
            if (basemodel.ret == 0)
            {
                b.isAction = true;
                b.finish();
                return;
            } else
            {
                Toast.makeText(b.getApplicationContext(), basemodel.msg, 1).show();
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((String[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((BaseModel)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            a = new MyProgressDialog(b);
            a.setTitle("\u63D0\u793A");
            a.setMessage("\u6B63\u5728\u5E2E\u60A8\u52AA\u529B\u56DE\u590D\u4E2D...");
            a.show();
        }

        b()
        {
            b = Send_CommentAct.this;
            super();
        }
    }


    private final int GOBINDWEIBO_REQUESTCODE = 1114;
    private final int GOSELECTCAREPERSON_REQUESTCODE = 175;
    private ImageView btn_cancel;
    private ImageView btn_complete;
    private int commentType;
    private EditText et;
    private ImageView img_QQ;
    private ImageView img_qzone;
    private ImageView img_renn;
    private View img_show_care_peoples;
    private ImageView img_sina;
    private boolean isAction;
    private int isBindQQ;
    private int isBindRenn;
    private int isBindSina;
    private boolean isShareQQ;
    private boolean isShareQzone;
    private boolean isShareRenn;
    private boolean isShareSina;
    private ImageView isShareToQQ;
    private ImageView isShareToQzone;
    private ImageView isShareToRenn;
    private ImageView isShareToSina;
    private int lastBindWeibo;
    private Context mCt;
    private String nickName;
    private String parentId;
    private int position;
    private String second;
    private List shareSettingList;
    private TextView top_view;
    private final int totalLength = 140;
    private String trackId;
    private TextView txt_canInputeHowMuchWord;
    private int type;

    public Send_CommentAct()
    {
        isShareSina = false;
        isShareQQ = false;
        isShareRenn = false;
        isShareQzone = false;
        isBindSina = 0;
        isBindQQ = 0;
        isBindRenn = 0;
        lastBindWeibo = -1;
        isAction = false;
        commentType = 1;
    }

    private void doShareQQ(boolean flag)
    {
        if (flag)
        {
            isShareQQ = true;
            img_QQ.setImageResource(0x7f02058c);
            isShareToQQ.setVisibility(0);
            return;
        } else
        {
            isShareQQ = false;
            img_QQ.setImageResource(0x7f02058d);
            isShareToQQ.setVisibility(4);
            return;
        }
    }

    private void doShareQzone(boolean flag)
    {
        if (flag)
        {
            isShareQzone = true;
            img_qzone.setImageResource(0x7f02045d);
            isShareToQzone.setVisibility(0);
            return;
        } else
        {
            isShareQzone = false;
            img_qzone.setImageResource(0x7f02045e);
            isShareToQzone.setVisibility(4);
            return;
        }
    }

    private void doShareRenn(boolean flag)
    {
        if (flag)
        {
            isShareRenn = true;
            img_renn.setImageResource(0x7f0204d8);
            isShareToRenn.setVisibility(0);
            return;
        } else
        {
            isShareRenn = false;
            img_renn.setImageResource(0x7f0204d9);
            isShareToRenn.setVisibility(4);
            return;
        }
    }

    private void doShareSina(boolean flag)
    {
        if (flag)
        {
            isShareSina = true;
            img_sina.setImageResource(0x7f020556);
            isShareToSina.setVisibility(0);
            return;
        } else
        {
            isShareSina = false;
            img_sina.setImageResource(0x7f020557);
            isShareToSina.setVisibility(4);
            return;
        }
    }

    private void findViews()
    {
        img_show_care_peoples = findViewById(0x7f0a06c3);
        txt_canInputeHowMuchWord = (TextView)findViewById(0x7f0a063d);
        et = (EditText)findViewById(0x7f0a063e);
        img_sina = (ImageView)findViewById(0x7f0a06bb);
        img_QQ = (ImageView)findViewById(0x7f0a06bd);
        img_renn = (ImageView)findViewById(0x7f0a06c1);
        img_qzone = (ImageView)findViewById(0x7f0a06bf);
        isShareToSina = (ImageView)findViewById(0x7f0a06bc);
        isShareToQQ = (ImageView)findViewById(0x7f0a06be);
        isShareToRenn = (ImageView)findViewById(0x7f0a06c2);
        isShareToQzone = (ImageView)findViewById(0x7f0a06c0);
        btn_cancel = (ImageView)findViewById(0x7f0a007b);
        btn_complete = (ImageView)findViewById(0x7f0a0710);
        top_view = (TextView)findViewById(0x7f0a00ae);
        android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)btn_cancel.getLayoutParams();
        layoutparams.leftMargin = ToolUtil.dp2px(this, 10F);
        btn_cancel.setLayoutParams(layoutparams);
        layoutparams = (android.widget.RelativeLayout.LayoutParams)btn_complete.getLayoutParams();
        layoutparams.rightMargin = ToolUtil.dp2px(this, 10F);
        btn_complete.setLayoutParams(layoutparams);
    }

    private final String getCommentShareList()
    {
        ArrayList arraylist = new ArrayList();
        arraylist.add(Boolean.valueOf(false));
        arraylist.add(Boolean.valueOf(false));
        arraylist.add(Boolean.valueOf(false));
        arraylist.add(Boolean.valueOf(false));
        if (isShareSina)
        {
            arraylist.set(0, Boolean.valueOf(true));
        }
        if (isShareQQ)
        {
            arraylist.set(1, Boolean.valueOf(true));
        }
        if (isShareQzone)
        {
            arraylist.set(2, Boolean.valueOf(true));
        }
        if (isShareRenn)
        {
            arraylist.set(3, Boolean.valueOf(true));
        }
        ArrayList arraylist1 = new ArrayList();
        arraylist1.add("tSina");
        arraylist1.add("tQQ");
        arraylist1.add("qzone");
        arraylist1.add("renren");
        String s = "";
        int k = 0;
        while (k < arraylist.size()) 
        {
            if (((Boolean)arraylist.get(k)).booleanValue())
            {
                if (Utilities.isBlank(s))
                {
                    s = (String)arraylist1.get(k);
                } else
                {
                    s = (new StringBuilder()).append(s).append(",").toString();
                    s = (new StringBuilder()).append(s).append((String)arraylist1.get(k)).toString();
                }
            }
            k++;
        }
        return s;
    }

    private void initData()
    {
        trackId = getIntent().getStringExtra("trackId");
        parentId = getIntent().getStringExtra("parentId");
        nickName = getIntent().getStringExtra("nickName");
        second = getIntent().getStringExtra("second");
        commentType = getIntent().getIntExtra("flag", 1);
        position = getIntent().getIntExtra("position", -1);
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        if (loginInfoModel != null && loginInfoModel.bindStatus != null)
        {
            Iterator iterator = loginInfoModel.bindStatus.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                ThirdPartyUserInfo thirdpartyuserinfo = (ThirdPartyUserInfo)iterator.next();
                if (!thirdpartyuserinfo.isExpired())
                {
                    if (thirdpartyuserinfo.getIdentity().equals("1"))
                    {
                        isBindSina = 1;
                    } else
                    if (thirdpartyuserinfo.getIdentity().equals("2"))
                    {
                        isBindQQ = 1;
                    } else
                    if (thirdpartyuserinfo.getIdentity().equals("3"))
                    {
                        isBindRenn = 1;
                    }
                }
            } while (true);
        }
    }

    private void initViews()
    {
        img_show_care_peoples.setOnClickListener(new c(this));
        if (nickName != null || "null".equals(nickName))
        {
            et.setText((new StringBuilder()).append("\u56DE\u590D@").append(nickName).append(":").toString());
            et.setSelection(et.getText().length());
        }
        txt_canInputeHowMuchWord.setText((new StringBuilder()).append("\u5269\u4F59").append(140 - et.getText().length()).append("\u5B57").toString());
        et.addTextChangedListener(new d(this));
        img_sina.setOnClickListener(new e(this));
        img_QQ.setOnClickListener(new com.ximalaya.ting.android.activity.comments.f(this));
        img_qzone.setOnClickListener(new g(this));
        img_renn.setOnClickListener(new h(this));
        if (commentType == 1)
        {
            top_view.setText("\u53D1\u8868\u8BC4\u8BBA");
        } else
        {
            top_view.setText("\u8F6C\u91C7\u8BC4\u8BBA");
        }
        btn_cancel.setImageResource(0x7f020111);
        btn_cancel.setOnClickListener(new i(this));
        btn_complete.setVisibility(0);
        btn_complete.setImageResource(0x7f02006a);
        btn_complete.setOnClickListener(new j(this));
    }

    private void loadDefaultShareSetting()
    {
        parseData(SharedPreferencesUtil.getInstance(mCt).getString("share_setting"));
        refreshShareSetting();
    }

    private void parseData(String s)
    {
        if (s != null)
        {
            try
            {
                shareSettingList = JSON.parseArray(JSON.parseObject(s).get("data").toString(), com/ximalaya/ting/android/model/setting/ShareSettingModel);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return;
            }
        } else
        {
            shareSettingList = new ArrayList();
            s = new ShareSettingModel();
            s.thirdpartyId = type;
            s.relay = true;
            s.webAlbum = true;
            s.webComment = true;
            s.webTrack = true;
            shareSettingList.add(s);
            return;
        }
    }

    private void refreshShareSetting()
    {
        if (shareSettingList != null && shareSettingList.size() > 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Iterator iterator = shareSettingList.iterator();
_L6:
        if (!iterator.hasNext()) goto _L1; else goto _L3
_L3:
        ShareSettingModel sharesettingmodel;
        boolean flag;
        sharesettingmodel = (ShareSettingModel)iterator.next();
        switch (commentType)
        {
        default:
            flag = sharesettingmodel.relay;
            break;

        case 1: // '\001'
            break MISSING_BLOCK_LABEL_119;
        }
_L4:
        switch (sharesettingmodel.thirdpartyId)
        {
        case 1: // '\001'
            doShareSina(flag);
            break;

        case 2: // '\002'
            if (sharesettingmodel.thirdpartyName.equals("tQQ"))
            {
                doShareQQ(flag);
            }
            if (sharesettingmodel.thirdpartyName.equals("qzone"))
            {
                doShareQzone(flag);
            }
            break;

        case 3: // '\003'
            doShareRenn(flag);
            break;
        }
        continue; /* Loop/switch isn't completed */
        flag = sharesettingmodel.webComment;
          goto _L4
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void finish()
    {
label0:
        {
            Object obj = new Intent();
            ((Intent) (obj)).putExtra("trackId", Long.valueOf(trackId));
            Iterator iterator;
            RecordingModel recordingmodel;
            if (commentType == 1)
            {
                if (isAction)
                {
                    setResult(1, ((Intent) (obj)));
                } else
                {
                    setResult(0, ((Intent) (obj)));
                }
            } else
            {
                ((Intent) (obj)).putExtra("position", position);
                if (isAction)
                {
                    setResult(1, ((Intent) (obj)));
                } else
                {
                    setResult(0, ((Intent) (obj)));
                }
            }
            if (isAction || Session.getSession().get("RECORDINGMODEL") == null)
            {
                break label0;
            }
            obj = (List)Session.getSession().get("RECORDINGMODEL");
            iterator = ((List) (obj)).iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
                recordingmodel = (RecordingModel)iterator.next();
            } while (!trackId.equals((new StringBuilder()).append(recordingmodel.trackId).append("").toString()));
            ((List) (obj)).remove(recordingmodel);
        }
        super.finish();
    }

    protected void onActivityResult(int k, int l, Intent intent)
    {
        if (k != 1114) goto _L2; else goto _L1
_L1:
        if (l != 0) goto _L4; else goto _L3
_L3:
        if (lastBindWeibo != 12) goto _L6; else goto _L5
_L5:
        isBindSina = 1;
_L9:
        (new a()).myexec(new Void[0]);
_L4:
        return;
_L6:
        if (lastBindWeibo == 13)
        {
            isBindQQ = 1;
        } else
        if (lastBindWeibo == 14)
        {
            isBindRenn = 1;
        }
        continue; /* Loop/switch isn't completed */
_L2:
        if (k != 175 || l != 86) goto _L4; else goto _L7
_L7:
        et.setText((new StringBuilder()).append(et.getText()).append("@").append(intent.getStringExtra("name")).append(" ").toString());
        et.setSelection(et.getText().length());
        return;
        if (true) goto _L9; else goto _L8
_L8:
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301c2);
        mCt = getApplicationContext();
        initData();
        findViews();
        initViews();
        loadDefaultShareSetting();
        (new a()).myexec(new Void[0]);
    }

    protected void onDestroy()
    {
        super.onDestroy();
    }









/*
    static int access$1402(Send_CommentAct send_commentact, int k)
    {
        send_commentact.lastBindWeibo = k;
        return k;
    }

*/


















/*
    static boolean access$902(Send_CommentAct send_commentact, boolean flag)
    {
        send_commentact.isAction = flag;
        return flag;
    }

*/
}
