// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.comments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.comment.CarePersonModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.comments:
//            CarePersonListAct

class a extends BaseAdapter
{

    List a;
    final CarePersonListAct b;

    public CarePersonModel a(int i)
    {
        if (a == null)
        {
            return null;
        } else
        {
            return (CarePersonModel)a.get(i);
        }
    }

    public void a(List list)
    {
        a = list;
    }

    public int getCount()
    {
        if (a == null)
        {
            return 0;
        } else
        {
            return a.size();
        }
    }

    public Object getItem(int i)
    {
        return a(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
label0:
        {
            CarePersonModel carepersonmodel;
            if (view == null)
            {
                view = b.getLayoutInflater().inflate(0x7f03004f, null);
                viewgroup = new <init>(b);
                viewgroup.a = (RoundedImageView)view.findViewById(0x7f0a019b);
                viewgroup.b = (ImageView)view.findViewById(0x7f0a019d);
                viewgroup.c = (TextView)view.findViewById(0x7f0a019c);
                viewgroup.d = view.findViewById(0x7f0a0645);
                viewgroup.e = view.findViewById(0x7f0a019a);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (e)view.getTag();
            }
            carepersonmodel = (CarePersonModel)a.get(i);
            if (carepersonmodel != null)
            {
                if (!carepersonmodel.flag)
                {
                    break label0;
                }
                ((a) (viewgroup)).d.setVisibility(8);
                ((d) (viewgroup)).e.setVisibility(0);
                ImageManager2.from(b).displayImage(((b) (viewgroup)).a, carepersonmodel.smallLogo, 0x7f0202e0);
                if (carepersonmodel.isVerified)
                {
                    ((ed) (viewgroup)).b.setVisibility(0);
                } else
                {
                    ((b) (viewgroup)).b.setVisibility(8);
                }
                ((b) (viewgroup)).c.setText(carepersonmodel.nickname);
            }
            return view;
        }
        (() (viewgroup)).d.setVisibility(0);
        ((d) (viewgroup)).e.setVisibility(8);
        return view;
    }

    (CarePersonListAct carepersonlistact, List list)
    {
        b = carepersonlistact;
        super();
        a = list;
    }
}
