// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.comments;

import android.app.ProgressDialog;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.activity.comments:
//            Send_CommentAct

class b extends MyAsyncTask
{

    ProgressDialog a;
    final Send_CommentAct b;

    protected transient BaseModel a(String as[])
    {
        if (b.loginInfoModel == null)
        {
            return null;
        }
        String s = as[0];
        as = ApiUtil.getApiHost();
        RequestParams requestparams;
        if (Send_CommentAct.access$300(b) == 1)
        {
            as = (new StringBuilder()).append(as).append("mobile/comment/create").toString();
        } else
        {
            as = (new StringBuilder()).append(as).append("mobile/track/relay").toString();
        }
        requestparams = new RequestParams();
        requestparams.put("trackId", Send_CommentAct.access$400(b));
        if (!"".equals(s))
        {
            requestparams.put("content", s);
        }
        if (Utilities.isNotBlank(Send_CommentAct.access$500(b)))
        {
            requestparams.put("parentId", Send_CommentAct.access$500(b));
        }
        if (Utilities.isNotBlank(Send_CommentAct.access$600(b)))
        {
            requestparams.put("second", Send_CommentAct.access$600(b));
        }
        s = Send_CommentAct.access$700(b);
        if (Utilities.isNotBlank(s))
        {
            requestparams.put("shareList", s);
        }
        as = f.a().b(as, requestparams, Send_CommentAct.access$800(b), Send_CommentAct.access$800(b));
        Logger.d("resultJson:", as);
        if (!Utilities.isNotBlank(as)) goto _L2; else goto _L1
_L1:
        as = (BaseModel)JSON.parseObject(as, com/ximalaya/ting/android/model/BaseModel);
_L4:
        return as;
        as;
        as.printStackTrace();
_L2:
        as = null;
        if (true) goto _L4; else goto _L3
_L3:
    }

    protected void a(BaseModel basemodel)
    {
        if (b == null || b.isFinishing())
        {
            return;
        }
        if (a != null)
        {
            a.cancel();
            a = null;
        }
        if (basemodel == null)
        {
            Toast.makeText(b.getApplicationContext(), "\u56DE\u590D\u5931\u8D25", 1).show();
            return;
        }
        if (basemodel.ret == 0)
        {
            Send_CommentAct.access$902(b, true);
            b.finish();
            return;
        } else
        {
            Toast.makeText(b.getApplicationContext(), basemodel.msg, 1).show();
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((String[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((BaseModel)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        a = new MyProgressDialog(b);
        a.setTitle("\u63D0\u793A");
        a.setMessage("\u6B63\u5728\u5E2E\u60A8\u52AA\u529B\u56DE\u590D\u4E2D...");
        a.show();
    }

    (Send_CommentAct send_commentact)
    {
        b = send_commentact;
        super();
    }
}
