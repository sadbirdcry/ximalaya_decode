// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity;

import android.app.FragmentTransaction;
import android.support.v4.app.Fragment;
import com.ximalaya.ting.android.fragment.ting.feed.FeedMainFragment;
import java.util.HashMap;

// Referenced classes of package com.ximalaya.ting.android.activity:
//            MainTabActivity2

class k
    implements android.app.ActionBar.TabListener
{

    final MainTabActivity2 a;

    k(MainTabActivity2 maintabactivity2)
    {
        a = maintabactivity2;
        super();
    }

    public void onTabReselected(android.app.ActionBar.Tab tab, FragmentTransaction fragmenttransaction)
    {
        if (a.mTabHost != null)
        {
            int j = tab.getPosition();
            int i = j;
            if (j >= 2)
            {
                i = j + 1;
            }
            if (i == 2)
            {
                tab = (Fragment)a.mStacks.get("tab_d");
                if (tab != null && tab.isAdded() && (tab instanceof FeedMainFragment))
                {
                    ((FeedMainFragment)tab).onRefresh();
                }
            }
        }
    }

    public void onTabSelected(android.app.ActionBar.Tab tab, FragmentTransaction fragmenttransaction)
    {
        if (a.mTabHost != null)
        {
            int j = tab.getPosition();
            int i = j;
            if (j >= 2)
            {
                i = j + 1;
            }
            a.setCurrentTab(i);
            MainTabActivity2.access$300(a);
        }
    }

    public void onTabUnselected(android.app.ActionBar.Tab tab, FragmentTransaction fragmenttransaction)
    {
    }
}
