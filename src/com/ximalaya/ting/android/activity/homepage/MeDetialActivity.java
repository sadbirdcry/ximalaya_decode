// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_info.HomePageModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            m, l, i, j, 
//            k, h

public class MeDetialActivity extends BaseActivity
{

    private EditText briefEdit;
    private TextView emailEdit;
    private RelativeLayout emailLayout;
    private String flag;
    private HomePageModel homePage;
    private ProgressDialog loadDialog;
    String mBriefIntro;
    private Context mContext;
    String mEmail;
    boolean mEmailVerified;
    private Handler mHandler;
    String mNickname;
    boolean mPhoneVerified;
    private UserInfoModel mProfile;
    String mTelNumber;
    String modifyEmailTelSuccessful;
    String modifyNicknameIntroSuccessful;
    private TextView nickName;
    private EditText nicknameEdit;
    private TextView personalSignature;
    private TextView ptitle;
    private LinearLayout ptitle_lable;
    private TextView telEdit;
    private RelativeLayout telLayout;

    public MeDetialActivity()
    {
        mEmailVerified = false;
        mPhoneVerified = false;
        mHandler = new m(this);
    }

    private void doModifyNicknameAndIntro(String s, String s1)
    {
        (new Thread(new l(this, s, s1))).start();
    }

    private String getEllipsize(String s)
    {
        String s1;
        if (TextUtils.isEmpty(s))
        {
            s1 = "";
        } else
        {
            int i1 = s.length();
            int j1 = s.lastIndexOf("@");
            s1 = s;
            if (s.length() > 38)
            {
                String s2 = s.substring(0, 38 - (i1 - j1) - 1);
                return (new StringBuilder()).append(s2).append("\u2026").append(s.substring(j1, i1)).toString();
            }
        }
        return s1;
    }

    private void getParentInfo()
    {
        Intent intent = getIntent();
        flag = intent.getStringExtra("flag");
        if ("other".equals(flag))
        {
            homePage = (HomePageModel)JSON.parseObject(intent.getStringExtra("homepage"), com/ximalaya/ting/android/model/personal_info/HomePageModel);
        }
    }

    private void initUI()
    {
        if ("other".equals(flag))
        {
            topTextView.setText("\u8BE6\u7EC6\u8D44\u6599");
            nickName = (TextView)findViewById(0x7f0a054b);
            if (homePage == null)
            {
                showToast("\u83B7\u53D6\u4FE1\u606F\u5931\u8D25");
                return;
            }
            nickName.setText(homePage.nickname);
            ptitle = (TextView)findViewById(0x7f0a05df);
            ptitle_lable = (LinearLayout)findViewById(0x7f0a05de);
            if (homePage.getPtitle() == null || "".equals(homePage.getPtitle()))
            {
                ptitle_lable.setVisibility(8);
            } else
            {
                ptitle.setText(homePage.getPtitle());
            }
            personalSignature = (TextView)findViewById(0x7f0a05e0);
            if (homePage.getPersonalSignature() == null || "null".equals(homePage.getPersonalSignature()) || "".equals(homePage.getPersonalSignature()))
            {
                personalSignature.setText(0x7f0900fb);
            } else
            {
                personalSignature.setText(homePage.getPersonalSignature());
            }
            topTextView.setText(homePage.getNickname());
            return;
        } else
        {
            topTextView.setText(0x7f090160);
            nicknameEdit = (EditText)findViewById(0x7f0a054b);
            briefEdit = (EditText)findViewById(0x7f0a054d);
            emailEdit = (TextView)findViewById(0x7f0a0551);
            emailLayout = (RelativeLayout)findViewById(0x7f0a0550);
            telEdit = (TextView)findViewById(0x7f0a0556);
            telLayout = (RelativeLayout)findViewById(0x7f0a0555);
            modifyNicknameIntroSuccessful = getResources().getString(0x7f09016b);
            modifyEmailTelSuccessful = getResources().getString(0x7f09016c);
            mNickname = null;
            mBriefIntro = null;
            mEmail = null;
            mTelNumber = null;
            emailLayout.setOnClickListener(new i(this));
            telLayout.setOnClickListener(new j(this));
            return;
        }
    }

    private void loadData()
    {
        (new Thread(new k(this))).start();
    }

    protected void onActivityResult(int i1, int j1, Intent intent)
    {
        if (i1 == 0)
        {
            if (j1 == 1)
            {
                intent = intent.getStringExtra("tel");
                telEdit.setText((new StringBuilder()).append(intent).append("(\u5DF2\u7ED1\u5B9A)").toString());
            }
        } else
        if (i1 == 1 && j1 == 1)
        {
            intent = intent.getStringExtra("email");
            emailEdit.setText((new StringBuilder()).append(intent).append("(\u672A\u7ED1\u5B9A)").toString());
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        getParentInfo();
        if ("other".equals(flag))
        {
            setContentView(0x7f030186);
            mContext = this;
        } else
        {
            setContentView(0x7f030165);
            loadDialog = new MyProgressDialog(this);
            loadDialog.requestWindowFeature(1);
            loadDialog.setMessage("Loading ...");
            loadDialog.show();
            mContext = this;
        }
        initCommon();
        mProfile = null;
        initUI();
    }

    protected void onResume()
    {
        super.onResume();
        loadData();
        InputMethodManager inputmethodmanager = (InputMethodManager)getSystemService("input_method");
        if (!"other".equals(flag))
        {
            nextButton.setVisibility(0);
            nextButton.setImageResource(0x7f02006a);
            ((android.widget.RelativeLayout.LayoutParams)nextButton.getLayoutParams()).rightMargin = Utilities.dip2px(getApplicationContext(), 10F);
            nextButton.setOnClickListener(new h(this, inputmethodmanager));
        }
    }









/*
    static UserInfoModel access$602(MeDetialActivity medetialactivity, UserInfoModel userinfomodel)
    {
        medetialactivity.mProfile = userinfomodel;
        return userinfomodel;
    }

*/



}
