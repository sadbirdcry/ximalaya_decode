// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            BindEmialActivity, a

private class <init> extends MyAsyncTask
{

    BaseModel a;
    final BindEmialActivity b;

    protected transient Integer a(Object aobj[])
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("email", BindEmialActivity.access$000(b).getText().toString().trim());
        requestparams.put("mobile", "");
        requestparams.put("checkCode", "");
        aobj = f.a().b(e.H, requestparams, (View)aobj[0], (View)aobj[0]);
        if (Utilities.isNotBlank(((String) (aobj))))
        {
            try
            {
                a = (BaseModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/BaseModel);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[]) { }
            if (a != null)
            {
                if (a.ret == 0)
                {
                    return Integer.valueOf(3);
                } else
                {
                    return Integer.valueOf(2);
                }
            }
        }
        return Integer.valueOf(1);
    }

    protected void a(Integer integer)
    {
        if (b == null || b.isFinishing())
        {
            return;
        }
        BindEmialActivity.access$100(b).cancel();
        if (3 == integer.intValue())
        {
            Toast.makeText(b.getApplicationContext(), "\u90AE\u7BB1\u7ED1\u5B9A\u6210\u529F", 1).show();
            integer = new Intent();
            integer.putExtra("email", BindEmialActivity.access$000(b).getText().toString().trim());
            b.setResult(1, integer);
            b.finish();
            return;
        }
        if (1 == integer.intValue())
        {
            Toast.makeText(b.getApplicationContext(), 0x7f0900a0, 0).show();
            return;
        } else
        {
            Toast.makeText(b.getApplicationContext(), a.msg, 0).show();
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a(aobj);
    }

    protected void onCancelled()
    {
        super.onCancelled();
    }

    protected void onPostExecute(Object obj)
    {
        a((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        BindEmialActivity.access$100(b).show();
    }

    private (BindEmialActivity bindemialactivity)
    {
        b = bindemialactivity;
        super();
    }

    b(BindEmialActivity bindemialactivity, a a1)
    {
        this(bindemialactivity);
    }
}
