// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.model.personal_info.HomePageModel;
import com.ximalaya.ting.android.util.ImageManager2;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            n

public class OtherDetailActivity extends BaseActivity
{

    private TextView authTxt;
    private ImageView closeImg;
    private String flag;
    private ImageView headerImg;
    private HomePageModel homePage;
    private TextView introTxt;
    private TextView nickNameTxt;
    private LinearLayout ptitle_lable;

    public OtherDetailActivity()
    {
    }

    private void getParentInfo()
    {
        Intent intent = getIntent();
        flag = intent.getStringExtra("flag");
        if ("other".equals(flag))
        {
            homePage = (HomePageModel)JSON.parseObject(intent.getStringExtra("homepage"), com/ximalaya/ting/android/model/personal_info/HomePageModel);
        }
    }

    private void initViews()
    {
        closeImg = (ImageView)findViewById(0x7f0a05dc);
        closeImg.setOnClickListener(new n(this));
        headerImg = (ImageView)findViewById(0x7f0a05dd);
        nickNameTxt = (TextView)findViewById(0x7f0a054b);
        authTxt = (TextView)findViewById(0x7f0a05df);
        introTxt = (TextView)findViewById(0x7f0a05e0);
        if (homePage == null)
        {
            showToast("\u83B7\u53D6\u4FE1\u606F\u5931\u8D25");
            return;
        }
        ImageManager2.from(this).displayImage(headerImg, homePage.mobileLargeLogo, -1, null);
        nickNameTxt.setText(homePage.nickname);
        ptitle_lable = (LinearLayout)findViewById(0x7f0a05de);
        if (homePage.getPtitle() == null || "".equals(homePage.getPtitle()))
        {
            ptitle_lable.setVisibility(8);
        } else
        {
            authTxt.setText(homePage.getPtitle());
        }
        if (homePage.getPersonalSignature() == null || "null".equals(homePage.getPersonalSignature()) || "".equals(homePage.getPersonalSignature()))
        {
            introTxt.setText(0x7f0900fb);
            return;
        } else
        {
            introTxt.setText(homePage.getPersonalSignature());
            return;
        }
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        getParentInfo();
        setContentView(0x7f030184);
        initViews();
    }

    public void onPause()
    {
        overridePendingTransition(0, 0x7f040008);
        super.onPause();
    }
}
