// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.content.Intent;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.View;
import android.widget.AdapterView;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.TalkModel;
import com.ximalaya.ting.android.model.message.BaseCommentModel;
import com.ximalaya.ting.android.util.DataCollectUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            TalkViewAct

class t
    implements android.widget.AdapterView.OnItemClickListener
{

    final MenuDialog a;
    final int b;
    final TalkViewAct c;

    t(TalkViewAct talkviewact, MenuDialog menudialog, int i)
    {
        c = talkviewact;
        a = menudialog;
        b = i;
        super();
    }

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        if (a != null)
        {
            a.dismiss();
        }
        if (!TalkViewAct.access$2000(c)) goto _L2; else goto _L1
_L1:
        if (i == 0)
        {
            ((ClipboardManager)c.getSystemService("clipboard")).setText(TalkViewAct.access$400(c).a(b).content);
        }
_L4:
        return;
_L2:
        adapterview = TalkViewAct.access$800(c).a(b);
        if (!((TalkModel) (adapterview)).SendMsg_FLAG)
        {
            break; /* Loop/switch isn't completed */
        }
        if (i == 0)
        {
            if (adapterview.getIsIn())
            {
                Bundle bundle = new Bundle();
                bundle.putLong("toUid", Long.valueOf(adapterview.getWithUid()).longValue());
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                c.startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
                return;
            } else
            {
                adapterview = new Intent(c, com/ximalaya/ting/android/activity/MainTabActivity2);
                adapterview.putExtra("show_main_tab", true);
                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                c.startActivity(adapterview);
                return;
            }
        }
        if (i == 1)
        {
            ((ClipboardManager)c.getSystemService("clipboard")).setText(adapterview.getContent());
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (i == 0)
        {
            view = new Bundle();
            if (adapterview.getIsIn())
            {
                view.putLong("toUid", Long.valueOf(adapterview.getWithUid()).longValue());
                c.startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, view);
                return;
            } else
            {
                c.setResult(22);
                c.finish(true);
                return;
            }
        }
        if (i == 1)
        {
            (new TalkViewAct.e(c)).myexec(new Object[] {
                (new StringBuilder()).append(TalkViewAct.access$700(c)).append("").toString(), adapterview.getContent(), ((TalkModel) (adapterview)).myKey
            });
            return;
        }
        if (i == 2)
        {
            ((ClipboardManager)c.getSystemService("clipboard")).setText(adapterview.getContent());
            return;
        }
        if (true) goto _L4; else goto _L5
_L5:
    }
}
