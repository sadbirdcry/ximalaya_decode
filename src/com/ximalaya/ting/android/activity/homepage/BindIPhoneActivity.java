// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;
import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            c, g, e, f, 
//            d

public class BindIPhoneActivity extends BaseActivity
{
    private class a extends MyAsyncTask
    {

        BaseModel a;
        ProgressDialog b;
        final BindIPhoneActivity c;

        protected transient Integer a(Void avoid[])
        {
            if (c.user == null)
            {
                c.user = new UserInfoModel();
                c.user.nickname = "";
                c.user.mPhone = c.iphoneEditText.getText().toString().trim();
                c.user.password = "";
            }
            avoid = new RequestParams();
            avoid.put("email", "");
            avoid.put("mobile", (new StringBuilder()).append(c.user.mPhone).append("").toString());
            avoid.put("checkCode", c.iphoneEditText.getText().toString().trim());
            avoid = f.a().b(e.H, avoid, c.getVCButton, null);
            if (Utilities.isNotBlank(avoid))
            {
                try
                {
                    a = (BaseModel)JSON.parseObject(avoid, com/ximalaya/ting/android/model/BaseModel);
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[]) { }
                if (a != null)
                {
                    if (a.ret == 0)
                    {
                        return Integer.valueOf(3);
                    } else
                    {
                        return Integer.valueOf(2);
                    }
                }
            }
            return Integer.valueOf(1);
        }

        protected void a(Integer integer)
        {
            super.onPostExecute(integer);
            if (c == null || c.isFinishing())
            {
                return;
            }
            if (b != null && b.isShowing())
            {
                b.cancel();
                b = null;
            }
            if (3 == integer.intValue())
            {
                Toast.makeText(c.getApplicationContext(), "\u624B\u673A\u53F7\u7ED1\u5B9A\u6210\u529F", 1).show();
                c.loginInfoModel = UserInfoMannage.getInstance().getUser();
                if (c.loginInfoModel != null)
                {
                    c.loginInfoModel.mPhone = c.user.mPhone;
                    c.loginInfoModel.isVMobile = true;
                }
                if ("SettingActivity".equals(c.getIntent().getStringExtra("FROM")))
                {
                    integer = new Intent();
                    integer.putExtra("tel", c.user.mPhone);
                    c.setResult(1, integer);
                }
                c.finish();
                return;
            }
            if (1 == integer.intValue())
            {
                Toast.makeText(c.getApplicationContext(), 0x7f0900a0, 0).show();
                return;
            } else
            {
                Toast.makeText(c.getApplicationContext(), a.msg, 0).show();
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onCancelled()
        {
            super.onCancelled();
        }

        protected void onPostExecute(Object obj)
        {
            a((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            b = new MyProgressDialog(c);
            b.requestWindowFeature(1);
            b.setMessage("Loading ...");
            b.show();
        }

        private a()
        {
            c = BindIPhoneActivity.this;
            super();
        }

        a(c c1)
        {
            this();
        }
    }

    private class b extends MyAsyncTask
    {

        BaseModel a;
        final BindIPhoneActivity b;

        protected transient Integer a(Object aobj[])
        {
            if (b.user == null)
            {
                b.user = new UserInfoModel();
                b.user.nickname = "";
                b.user.mPhone = b.iphoneEditText.getText().toString().trim();
                b.user.password = "";
            }
            RequestParams requestparams = new RequestParams();
            requestparams.put("phone_num", b.user.mPhone);
            aobj = f.a().b(e.I, requestparams, (View)aobj[0], null);
            if (Utilities.isNotBlank(((String) (aobj))))
            {
                try
                {
                    a = (BaseModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/BaseModel);
                }
                // Misplaced declaration of an exception variable
                catch (Object aobj[]) { }
                if (a != null)
                {
                    if (a.ret == 0)
                    {
                        return Integer.valueOf(3);
                    } else
                    {
                        return Integer.valueOf(2);
                    }
                }
            }
            return Integer.valueOf(1);
        }

        protected void a(Integer integer)
        {
            if (b == null || b.isFinishing())
            {
                return;
            }
            if (3 == integer.intValue())
            {
                b.time = 180;
                b.toVcUI();
                return;
            }
            if (1 == integer.intValue())
            {
                if (b.isVcode)
                {
                    b.user = null;
                }
                Toast.makeText(b.getApplicationContext(), 0x7f0900a0, 0).show();
                return;
            }
            if (b.isVcode)
            {
                b.user = null;
            }
            Toast.makeText(b.getApplicationContext(), a.msg, 0).show();
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onCancelled()
        {
            super.onCancelled();
        }

        protected void onPostExecute(Object obj)
        {
            a((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        private b()
        {
            b = BindIPhoneActivity.this;
            super();
        }

        b(c c1)
        {
            this();
        }
    }


    public static final String FROM = "FROM";
    public static final String FROM_RECORD_ACTIVITY = "RecordingUpLoadActivity";
    public static final String FROM_SETTING_ACTIVITY = "SettingActivity";
    private Button getVCButton;
    private EditText iphoneEditText;
    private boolean isVcode;
    private Button lgButton;
    private Handler mHandler;
    private TimerTask task;
    private int time;
    private Timer timer;
    TextView toastTextView;
    private UserInfoModel user;

    public BindIPhoneActivity()
    {
        time = -1;
        mHandler = new c(this);
        isVcode = true;
    }

    private void findViews()
    {
        iphoneEditText = (EditText)findViewById(0x7f0a0182);
        getVCButton = (Button)findViewById(0x7f0a0184);
    }

    private void initUI()
    {
        nextButton.setVisibility(8);
        topTextView.setText(0x7f090161);
        lgButton = (Button)findViewById(0x7f0a0181);
        lgButton.setOnClickListener(new g(this));
        toastTextView = (TextView)findViewById(0x7f0a0183);
    }

    private void initViews()
    {
        iphoneEditText.addTextChangedListener(new com.ximalaya.ting.android.activity.homepage.e(this));
        getVCButton.setOnClickListener(new com.ximalaya.ting.android.activity.homepage.f(this));
    }

    private void toVcUI()
    {
        iphoneEditText.setText("");
        iphoneEditText.setHint(0x7f0900bd);
        if (toastTextView != null && user != null)
        {
            toastTextView.setText((new StringBuilder()).append("\u9A8C\u8BC1\u7801\u77ED\u4FE1\u5DF2\u53D1\u5230\u624B\u673A").append(user.mPhone).append("\u4E0A\uFF0C\u8BF7\u8F93\u5165\u77ED\u4FE1\u4E2D\u7684\u9A8C\u8BC1\u7801\u6570\u5B57").toString());
        }
        getVCButton.getLayoutParams().width = -2;
        lgButton.setVisibility(0);
        isVcode = false;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03004d);
        initCommon();
        timer = new Timer();
        task = new d(this);
        timer.schedule(task, 0L, 1000L);
        findViews();
        initViews();
        initUI();
    }



/*
    static int access$002(BindIPhoneActivity bindiphoneactivity, int i)
    {
        bindiphoneactivity.time = i;
        return i;
    }

*/


/*
    static int access$006(BindIPhoneActivity bindiphoneactivity)
    {
        int i = bindiphoneactivity.time - 1;
        bindiphoneactivity.time = i;
        return i;
    }

*/








/*
    static UserInfoModel access$802(BindIPhoneActivity bindiphoneactivity, UserInfoModel userinfomodel)
    {
        bindiphoneactivity.user = userinfomodel;
        return userinfomodel;
    }

*/

}
