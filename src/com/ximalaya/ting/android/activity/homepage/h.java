// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            MeDetialActivity

class h
    implements android.view.View.OnClickListener
{

    final InputMethodManager a;
    final MeDetialActivity b;

    h(MeDetialActivity medetialactivity, InputMethodManager inputmethodmanager)
    {
        b = medetialactivity;
        a = inputmethodmanager;
        super();
    }

    public void onClick(View view)
    {
        a.hideSoftInputFromWindow(MeDetialActivity.access$000(b).getWindowToken(), 0);
        a.hideSoftInputFromWindow(MeDetialActivity.access$100(b).getWindowToken(), 0);
        String s1 = MeDetialActivity.access$000(b).getText().toString();
        String s = MeDetialActivity.access$100(b).getText().toString();
        MeDetialActivity.access$200(b).getText().toString();
        MeDetialActivity.access$300(b).getText().toString();
        if (s != null && s.length() > 300)
        {
            Toast.makeText(MeDetialActivity.access$400(b), "\u7B80\u4ECB\u4E0D\u5F97\u8D85\u8FC7300\u4E2A\u5B57", 1).show();
            return;
        }
        if (ToolUtil.isConnectToNetwork(b.getApplicationContext()))
        {
            if (TextUtils.isEmpty(s1))
            {
                (new DialogBuilder(view.getContext())).setMessage((new StringBuilder()).append(b.getString(0x7f090169)).append("\uFF01").toString()).showWarning();
                return;
            }
            if (b.mNickname != null && !b.mNickname.equals(s1) || b.mBriefIntro != null && !b.mBriefIntro.equals(s))
            {
                view = s;
                if (s == null)
                {
                    view = "";
                }
                MeDetialActivity.access$500(b, s1, view);
                return;
            } else
            {
                b.finish();
                return;
            }
        } else
        {
            Toast.makeText(MeDetialActivity.access$400(b), b.getString(0x7f0900a0), 1).show();
            return;
        }
    }
}
