// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.TalkModel;
import com.ximalaya.ting.android.model.message.TalkModelList;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            TalkViewAct, w, v

class a extends BaseAdapter
{

    public TalkModelList a;
    int b;
    final TalkViewAct c;

    public TalkModel a(int i)
    {
        if (a != null && a.list != null)
        {
            if (a.totalCount > b)
            {
                if (i != 0)
                {
                    return (TalkModel)a.list.get(i - 2);
                }
            } else
            {
                return (TalkModel)a.list.get(i - 1);
            }
        }
        return null;
    }

    public String a()
    {
        if (a != null && a.list != null && a.list.size() > 0)
        {
            return ((TalkModel)a.list.get(a.list.size() - 1)).content;
        } else
        {
            return null;
        }
    }

    void a(TalkModel talkmodel,  , int i)
    {
        if (talkmodel.flag)
        {
            .a.setVisibility(8);
            .e.setVisibility(8);
            if (talkmodel.getIsIn())
            {
                .d.setVisibility(8);
                .b.setVisibility(0);
                .c.setVisibility(8);
                if (TalkViewAct.access$1200(c))
                {
                    TalkViewAct.access$1300(c, .f, talkmodel.getContent());
                } else
                {
                    .f.setText(EmotionUtil.getInstance().convertEmotion(talkmodel.getContent()));
                }
                .h.setOnClickListener(new w(this));
                ImageManager2.from(c.getApplicationContext()).displayImage(.h, talkmodel.withAvatarPath, 0x7f0202df);
                return;
            }
            if (talkmodel.SendMsg_FLAG)
            {
                .d.setVisibility(8);
            } else
            {
                .d.setVisibility(0);
            }
            .b.setVisibility(8);
            .c.setVisibility(0);
            .g.setText(EmotionUtil.getInstance().convertEmotion(talkmodel.getContent()));
            ImageManager2.from(c.getApplicationContext()).displayImage(.i, talkmodel.avatarPath, 0x7f0202df);
            return;
        } else
        {
            .a.setVisibility(8);
            .b.setVisibility(8);
            .c.setVisibility(8);
            .e.setVisibility(0);
            .e.setText(talkmodel.time);
            return;
        }
    }

    public int getCount()
    {
        if (a == null || a.list == null)
        {
            return 0;
        }
        if (a.totalCount > b)
        {
            return a.list.size() + 1;
        } else
        {
            return a.list.size();
        }
    }

    public Object getItem(int i)
    {
        return a(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (view == null)
        {
            viewgroup = new <init>();
            view = c.getLayoutInflater().inflate(0x7f0301e2, null);
            viewgroup.a = view.findViewById(0x7f0a0703);
            viewgroup.i = (ImageView)view.findViewById(0x7f0a0708);
            viewgroup.h = (ImageView)view.findViewById(0x7f0a0705);
            viewgroup.b = view.findViewById(0x7f0a0704);
            viewgroup.c = view.findViewById(0x7f0a0707);
            viewgroup.d = view.findViewById(0x7f0a070a);
            viewgroup.g = (TextView)view.findViewById(0x7f0a0709);
            viewgroup.f = (TextView)view.findViewById(0x7f0a0706);
            viewgroup.e = (TextView)view.findViewById(0x7f0a04b2);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (e)view.getTag();
        }
        if (a.totalCount > b)
        {
            if (i == 0)
            {
                ((b) (viewgroup)).a.setVisibility(0);
                ((a) (viewgroup)).a.setOnClickListener(new v(this));
                ((a) (viewgroup)).b.setVisibility(8);
                ((b) (viewgroup)).c.setVisibility(8);
                ((c) (viewgroup)).e.setVisibility(8);
                return view;
            } else
            {
                a((TalkModel)a.list.get(i - 1), viewgroup, i);
                return view;
            }
        } else
        {
            a((TalkModel)a.list.get(i), viewgroup, i);
            return view;
        }
    }

    public (TalkViewAct talkviewact, TalkModelList talkmodellist)
    {
        c = talkviewact;
        super();
        b = 0;
        a = talkmodellist;
    }
}
