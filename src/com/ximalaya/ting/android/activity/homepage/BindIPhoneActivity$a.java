// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            BindIPhoneActivity, c

private class <init> extends MyAsyncTask
{

    BaseModel a;
    ProgressDialog b;
    final BindIPhoneActivity c;

    protected transient Integer a(Void avoid[])
    {
        if (BindIPhoneActivity.access$800(c) == null)
        {
            BindIPhoneActivity.access$802(c, new UserInfoModel());
            BindIPhoneActivity.access$800(c).nickname = "";
            BindIPhoneActivity.access$800(c).mPhone = BindIPhoneActivity.access$500(c).getText().toString().trim();
            BindIPhoneActivity.access$800(c).password = "";
        }
        avoid = new RequestParams();
        avoid.put("email", "");
        avoid.put("mobile", (new StringBuilder()).append(BindIPhoneActivity.access$800(c).mPhone).append("").toString());
        avoid.put("checkCode", BindIPhoneActivity.access$500(c).getText().toString().trim());
        avoid = f.a().b(e.H, avoid, BindIPhoneActivity.access$300(c), null);
        if (Utilities.isNotBlank(avoid))
        {
            try
            {
                a = (BaseModel)JSON.parseObject(avoid, com/ximalaya/ting/android/model/BaseModel);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[]) { }
            if (a != null)
            {
                if (a.ret == 0)
                {
                    return Integer.valueOf(3);
                } else
                {
                    return Integer.valueOf(2);
                }
            }
        }
        return Integer.valueOf(1);
    }

    protected void a(Integer integer)
    {
        super.onPostExecute(integer);
        if (c == null || c.isFinishing())
        {
            return;
        }
        if (b != null && b.isShowing())
        {
            b.cancel();
            b = null;
        }
        if (3 == integer.intValue())
        {
            Toast.makeText(c.getApplicationContext(), "\u624B\u673A\u53F7\u7ED1\u5B9A\u6210\u529F", 1).show();
            c.loginInfoModel = UserInfoMannage.getInstance().getUser();
            if (c.loginInfoModel != null)
            {
                c.loginInfoModel.mPhone = BindIPhoneActivity.access$800(c).mPhone;
                c.loginInfoModel.isVMobile = true;
            }
            if ("SettingActivity".equals(c.getIntent().getStringExtra("FROM")))
            {
                integer = new Intent();
                integer.putExtra("tel", BindIPhoneActivity.access$800(c).mPhone);
                c.setResult(1, integer);
            }
            c.finish();
            return;
        }
        if (1 == integer.intValue())
        {
            Toast.makeText(c.getApplicationContext(), 0x7f0900a0, 0).show();
            return;
        } else
        {
            Toast.makeText(c.getApplicationContext(), a.msg, 0).show();
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onCancelled()
    {
        super.onCancelled();
    }

    protected void onPostExecute(Object obj)
    {
        a((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        b = new MyProgressDialog(c);
        b.requestWindowFeature(1);
        b.setMessage("Loading ...");
        b.show();
    }

    private (BindIPhoneActivity bindiphoneactivity)
    {
        c = bindiphoneactivity;
        super();
    }

    c(BindIPhoneActivity bindiphoneactivity, c c1)
    {
        this(bindiphoneactivity);
    }
}
