// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.TalkModel;
import com.ximalaya.ting.android.model.message.SendPrivateMsgModel;
import com.ximalaya.ting.android.model.message.TalkModelList;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            TalkViewAct

class b extends MyAsyncTask
{

    SendPrivateMsgModel a;
    String b;
    final TalkViewAct c;

    protected transient Integer a(Object aobj[])
    {
        b = (String)aobj[2];
        RequestParams requestparams = new RequestParams();
        if (TalkViewAct.access$1500(c) > 0L)
        {
            requestparams.put("specialId", (new StringBuilder()).append("").append(TalkViewAct.access$1500(c)).toString());
        }
        requestparams.put("toUid", (String)aobj[0]);
        requestparams.put("content", (String)aobj[1]);
        requestparams.put("key", (String)aobj[2]);
        aobj = f.a().b(e.l, requestparams, TalkViewAct.access$1600(c), null);
        try
        {
            a = (SendPrivateMsgModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/message/SendPrivateMsgModel);
            if (a == null)
            {
                return Integer.valueOf(0);
            }
        }
        // Misplaced declaration of an exception variable
        catch (Object aobj[])
        {
            Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
            return Integer.valueOf(0);
        }
        return Integer.valueOf(-1);
    }

    protected void a(Integer integer)
    {
        if (c != null && !c.isFinishing() && TalkViewAct.access$800(c) != null && TalkViewAct.access$800(c).a != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Iterator iterator = TalkViewAct.access$800(c).a.list.iterator();
        TalkModel talkmodel;
        do
        {
            if (!iterator.hasNext())
            {
                continue; /* Loop/switch isn't completed */
            }
            talkmodel = (TalkModel)iterator.next();
        } while (!talkmodel.myKey.equals(b));
        if (integer.intValue() == -1 && a.ret == 0)
        {
            talkmodel.setId(a.getId());
            if (!talkmodel.SendMsg_FLAG)
            {
                talkmodel.SendMsg_FLAG = true;
                TalkViewAct.access$800(c).notifyDataSetChanged();
                return;
            }
        } else
        {
            talkmodel.SendMsg_FLAG = false;
            TalkViewAct.access$800(c).notifyDataSetChanged();
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    protected Object doInBackground(Object aobj[])
    {
        return a(aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((Integer)obj);
    }

    l(TalkViewAct talkviewact)
    {
        c = talkviewact;
        super();
        a = null;
        b = null;
    }
}
