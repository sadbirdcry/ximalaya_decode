// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            BindIPhoneActivity, c

private class <init> extends MyAsyncTask
{

    BaseModel a;
    final BindIPhoneActivity b;

    protected transient Integer a(Object aobj[])
    {
        if (BindIPhoneActivity.access$800(b) == null)
        {
            BindIPhoneActivity.access$802(b, new UserInfoModel());
            BindIPhoneActivity.access$800(b).nickname = "";
            BindIPhoneActivity.access$800(b).mPhone = BindIPhoneActivity.access$500(b).getText().toString().trim();
            BindIPhoneActivity.access$800(b).password = "";
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("phone_num", BindIPhoneActivity.access$800(b).mPhone);
        aobj = f.a().b(e.I, requestparams, (View)aobj[0], null);
        if (Utilities.isNotBlank(((String) (aobj))))
        {
            try
            {
                a = (BaseModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/BaseModel);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[]) { }
            if (a != null)
            {
                if (a.ret == 0)
                {
                    return Integer.valueOf(3);
                } else
                {
                    return Integer.valueOf(2);
                }
            }
        }
        return Integer.valueOf(1);
    }

    protected void a(Integer integer)
    {
        if (b == null || b.isFinishing())
        {
            return;
        }
        if (3 == integer.intValue())
        {
            BindIPhoneActivity.access$002(b, 180);
            BindIPhoneActivity.access$900(b);
            return;
        }
        if (1 == integer.intValue())
        {
            if (BindIPhoneActivity.access$400(b))
            {
                BindIPhoneActivity.access$802(b, null);
            }
            Toast.makeText(b.getApplicationContext(), 0x7f0900a0, 0).show();
            return;
        }
        if (BindIPhoneActivity.access$400(b))
        {
            BindIPhoneActivity.access$802(b, null);
        }
        Toast.makeText(b.getApplicationContext(), a.msg, 0).show();
    }

    protected Object doInBackground(Object aobj[])
    {
        return a(aobj);
    }

    protected void onCancelled()
    {
        super.onCancelled();
    }

    protected void onPostExecute(Object obj)
    {
        a((Integer)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
    }

    private (BindIPhoneActivity bindiphoneactivity)
    {
        b = bindiphoneactivity;
        super();
    }

    b(BindIPhoneActivity bindiphoneactivity, c c)
    {
        this(bindiphoneactivity);
    }
}
