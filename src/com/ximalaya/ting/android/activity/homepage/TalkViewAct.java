// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.homepage;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.TalkModel;
import com.ximalaya.ting.android.model.message.BaseCommentModel;
import com.ximalaya.ting.android.model.message.CommentListInCommentNotice;
import com.ximalaya.ting.android.model.message.SendPrivateMsgModel;
import com.ximalaya.ting.android.model.message.TalkModelList;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.ResizeRelativeLayout;
import com.ximalaya.ting.android.view.emotion.EmotionSelector;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.activity.homepage:
//            t, o, p, q, 
//            r, s, u, w, 
//            v

public class TalkViewAct extends BaseActivity
    implements android.view.ViewTreeObserver.OnGlobalLayoutListener
{
    class a extends MyAsyncTask
    {

        final TalkViewAct a;
        private CommentListInCommentNotice b;
        private String c;
        private String d;

        protected transient Integer a(Object aobj[])
        {
            d = (String)aobj[0];
            c = (String)aobj[1];
            RequestParams requestparams = new RequestParams();
            requestparams.put("pageSize", (new StringBuilder()).append(a.msg_list_request_count).append("").toString());
            requestparams.put("key", d);
            requestparams.put("isDown", c);
            aobj = com.ximalaya.ting.android.b.f.a().b(com.ximalaya.ting.android.a.e.k, requestparams, (View)aobj[2], a.rootView);
            try
            {
                b = (CommentListInCommentNotice)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/message/CommentListInCommentNotice);
                if (b != null && b.list != null)
                {
                    aobj = 
// JavaClassFileOutputException: get_constant: invalid tag

        protected void a(Integer integer)
        {
            if (integer.intValue() == -1)
            {
                a.isFirstRequestSucess = true;
                if (a.msma == null)
                {
                    a.msma = a. new c(b);
                    a.msma.b = a.totalCount;
                    a.list.setAdapter(a.msma);
                    a.list.setSelection(a.msma.getCount() - 1);
                } else
                {
                    if (d.equals(Integer.valueOf(0)))
                    {
                        boolean flag = a.isDataListChange(a.msma.a, b);
                        a.msma.a = b;
                        if (flag)
                        {
                            a.list.setSelection(a.msma.getCount() - 1);
                        }
                        a.msma.notifyDataSetChanged();
                        return;
                    }
                    if (c.equals("true"))
                    {
                        b.list.addAll(a.msma.a.list);
                        boolean flag1 = a.isDataListChange(a.msma.a, b);
                        a.msma.a = b;
                        if (flag1)
                        {
                            a.list.setSelection(a.msma.getCount() - 1);
                        }
                        a.msma.notifyDataSetChanged();
                        return;
                    }
                    if (c.equals("false"))
                    {
                        b.list.addAll(0, a.msma.a.list);
                        boolean flag2 = a.isDataListChange(a.msma.a, b);
                        a.msma.a = b;
                        if (flag2)
                        {
                            a.list.setSelection(a.msma.getCount() - 1);
                        }
                        a.msma.notifyDataSetChanged();
                        return;
                    }
                }
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((Integer)obj);
        }

        a()
        {
            a = TalkViewAct.this;
            super();
        }
    }

    class b extends MyAsyncTask
    {

        TalkModelList a;
        final TalkViewAct b;
        private String c;
        private String d;

        protected transient Integer a(Object aobj[])
        {
            d = (String)aobj[0];
            c = (String)aobj[1];
            RequestParams requestparams = new RequestParams();
            requestparams.put("pageSize", (new StringBuilder()).append(b.msg_list_request_count).append("").toString());
            requestparams.put("toUid", (new StringBuilder()).append(b.toUid).append("").toString());
            requestparams.put("lastId", (String)aobj[0]);
            requestparams.put("isDown", (String)aobj[1]);
            aobj = com.ximalaya.ting.android.b.f.a().a(com.ximalaya.ting.android.a.e.j, requestparams, (View)aobj[2], b.rootView, false);
            if (((com.ximalaya.ting.android.b.n.a) (aobj)).b == 1)
            {
                aobj = ((com.ximalaya.ting.android.b.n.a) (aobj)).a;
            } else
            {
                aobj = null;
            }
            try
            {
                a = (TalkModelList)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/message/TalkModelList);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
            }
            if (a != null && a.list != null)
            {
                aobj = 
// JavaClassFileOutputException: get_constant: invalid tag

        protected void a(Integer integer)
        {
            if (integer.intValue() == 3)
            {
                b.isFirstRequestSucess = true;
                if (b.talkAda == null)
                {
                    b.talkAda = b. new f(a);
                    b.talkAda.b = b.totalCount;
                    b.list.setAdapter(b.talkAda);
                } else
                {
                    if (d.equals("0"))
                    {
                        b.talkAda.a = a;
                        b.talkAda.notifyDataSetChanged();
                        return;
                    }
                    if (c.equals("true"))
                    {
                        a.list.addAll(b.talkAda.a.list);
                        b.talkAda.a = a;
                        b.talkAda.notifyDataSetChanged();
                        return;
                    }
                    if (c.equals("false"))
                    {
                        a.list.addAll(0, b.talkAda.a.list);
                        boolean flag = b.isDataListChange(b.talkAda.a, a);
                        b.talkAda.a = a;
                        b.talkAda.notifyDataSetChanged();
                        if (flag)
                        {
                            b.list.setSelectionFromTop(b.talkAda.getCount() - 1, 0);
                            return;
                        }
                    }
                }
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((Integer)obj);
        }

        b()
        {
            b = TalkViewAct.this;
            super();
            a = null;
        }
    }

    class c extends BaseAdapter
    {

        CommentListInCommentNotice a;
        int b;
        final TalkViewAct c;

        public BaseCommentModel a(int i)
        {
            if (a != null && a.list != null)
            {
                if (a.totalCount > b)
                {
                    if (i != 0)
                    {
                        return (BaseCommentModel)a.list.get(i - 2);
                    }
                } else
                {
                    return (BaseCommentModel)a.list.get(i - 1);
                }
            }
            return null;
        }

        void a(BaseCommentModel basecommentmodel, g g1, int i)
        {
            if (basecommentmodel.flag)
            {
                g1.a.setVisibility(8);
                g1.b.setVisibility(0);
                g1.c.setVisibility(8);
                g1.e.setVisibility(8);
                if (c.isOfficialAccount)
                {
                    c.setMsgTextWithHtml(g1.f, basecommentmodel.getContent());
                } else
                {
                    g1.f.setText(basecommentmodel.getContent());
                }
                ImageManager2.from(c.getApplicationContext()).displayImage(g1.h, basecommentmodel.avatarPath, 0x7f0202df);
                return;
            } else
            {
                g1.a.setVisibility(8);
                g1.b.setVisibility(8);
                g1.c.setVisibility(8);
                g1.e.setVisibility(0);
                g1.e.setText(basecommentmodel.time);
                return;
            }
        }

        public int getCount()
        {
            if (a == null || a.list == null)
            {
                return 0;
            }
            if (a.totalCount > b)
            {
                return a.list.size() + 1;
            } else
            {
                return a.list.size();
            }
        }

        public Object getItem(int i)
        {
            return a(i);
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            if (view == null)
            {
                viewgroup = new g();
                view = c.getLayoutInflater().inflate(0x7f0301e2, null);
                viewgroup.a = view.findViewById(0x7f0a0703);
                viewgroup.i = (ImageView)view.findViewById(0x7f0a0708);
                viewgroup.h = (ImageView)view.findViewById(0x7f0a0705);
                viewgroup.b = view.findViewById(0x7f0a0704);
                viewgroup.c = view.findViewById(0x7f0a0707);
                viewgroup.g = (TextView)view.findViewById(0x7f0a0709);
                viewgroup.f = (TextView)view.findViewById(0x7f0a0706);
                viewgroup.e = (TextView)view.findViewById(0x7f0a04b2);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (g)view.getTag();
            }
            if (a.totalCount > b)
            {
                if (i == 0)
                {
                    ((g) (viewgroup)).a.setVisibility(0);
                    ((g) (viewgroup)).a.setOnClickListener(new u(this));
                    ((g) (viewgroup)).b.setVisibility(8);
                    ((g) (viewgroup)).c.setVisibility(8);
                    ((g) (viewgroup)).e.setVisibility(8);
                    return view;
                } else
                {
                    a((BaseCommentModel)a.list.get(i - 1), viewgroup, i);
                    return view;
                }
            } else
            {
                a((BaseCommentModel)a.list.get(i), viewgroup, i);
                return view;
            }
        }

        public c(CommentListInCommentNotice commentlistincommentnotice)
        {
            c = TalkViewAct.this;
            super();
            a = commentlistincommentnotice;
        }
    }

    private class d extends ClickableSpan
    {

        final TalkViewAct a;
        private String b;

        public void onClick(View view)
        {
            a.goToUrl(b);
        }

        d(String s1)
        {
            a = TalkViewAct.this;
            super();
            b = s1;
        }
    }

    class e extends MyAsyncTask
    {

        SendPrivateMsgModel a;
        String b;
        final TalkViewAct c;

        protected transient Integer a(Object aobj[])
        {
            b = (String)aobj[2];
            RequestParams requestparams = new RequestParams();
            if (c.mSpecialId > 0L)
            {
                requestparams.put("specialId", (new StringBuilder()).append("").append(c.mSpecialId).toString());
            }
            requestparams.put("toUid", (String)aobj[0]);
            requestparams.put("content", (String)aobj[1]);
            requestparams.put("key", (String)aobj[2]);
            aobj = com.ximalaya.ting.android.b.f.a().b(com.ximalaya.ting.android.a.e.l, requestparams, c.mEmotionSelector, null);
            try
            {
                a = (SendPrivateMsgModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/message/SendPrivateMsgModel);
                if (a == null)
                {
                    return Integer.valueOf(0);
                }
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (aobj)).getMessage()).append(Logger.getLineInfo()).toString());
                return Integer.valueOf(0);
            }
            return Integer.valueOf(-1);
        }

        protected void a(Integer integer)
        {
            if (c != null && !c.isFinishing() && c.talkAda != null && c.talkAda.a != null) goto _L2; else goto _L1
_L1:
            return;
_L2:
            Iterator iterator = c.talkAda.a.list.iterator();
            TalkModel talkmodel;
            do
            {
                if (!iterator.hasNext())
                {
                    continue; /* Loop/switch isn't completed */
                }
                talkmodel = (TalkModel)iterator.next();
            } while (!talkmodel.myKey.equals(b));
            if (integer.intValue() == -1 && a.ret == 0)
            {
                talkmodel.setId(a.getId());
                if (!talkmodel.SendMsg_FLAG)
                {
                    talkmodel.SendMsg_FLAG = true;
                    c.talkAda.notifyDataSetChanged();
                    return;
                }
            } else
            {
                talkmodel.SendMsg_FLAG = false;
                c.talkAda.notifyDataSetChanged();
                return;
            }
            if (true) goto _L1; else goto _L3
_L3:
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((Integer)obj);
        }

        e()
        {
            c = TalkViewAct.this;
            super();
            a = null;
            b = null;
        }
    }

    class f extends BaseAdapter
    {

        public TalkModelList a;
        int b;
        final TalkViewAct c;

        public TalkModel a(int i)
        {
            if (a != null && a.list != null)
            {
                if (a.totalCount > b)
                {
                    if (i != 0)
                    {
                        return (TalkModel)a.list.get(i - 2);
                    }
                } else
                {
                    return (TalkModel)a.list.get(i - 1);
                }
            }
            return null;
        }

        public String a()
        {
            if (a != null && a.list != null && a.list.size() > 0)
            {
                return ((TalkModel)a.list.get(a.list.size() - 1)).content;
            } else
            {
                return null;
            }
        }

        void a(TalkModel talkmodel, g g1, int i)
        {
            if (talkmodel.flag)
            {
                g1.a.setVisibility(8);
                g1.e.setVisibility(8);
                if (talkmodel.getIsIn())
                {
                    g1.d.setVisibility(8);
                    g1.b.setVisibility(0);
                    g1.c.setVisibility(8);
                    if (c.isOfficialAccount)
                    {
                        c.setMsgTextWithHtml(g1.f, talkmodel.getContent());
                    } else
                    {
                        g1.f.setText(EmotionUtil.getInstance().convertEmotion(talkmodel.getContent()));
                    }
                    g1.h.setOnClickListener(new w(this));
                    ImageManager2.from(c.getApplicationContext()).displayImage(g1.h, talkmodel.withAvatarPath, 0x7f0202df);
                    return;
                }
                if (talkmodel.SendMsg_FLAG)
                {
                    g1.d.setVisibility(8);
                } else
                {
                    g1.d.setVisibility(0);
                }
                g1.b.setVisibility(8);
                g1.c.setVisibility(0);
                g1.g.setText(EmotionUtil.getInstance().convertEmotion(talkmodel.getContent()));
                ImageManager2.from(c.getApplicationContext()).displayImage(g1.i, talkmodel.avatarPath, 0x7f0202df);
                return;
            } else
            {
                g1.a.setVisibility(8);
                g1.b.setVisibility(8);
                g1.c.setVisibility(8);
                g1.e.setVisibility(0);
                g1.e.setText(talkmodel.time);
                return;
            }
        }

        public int getCount()
        {
            if (a == null || a.list == null)
            {
                return 0;
            }
            if (a.totalCount > b)
            {
                return a.list.size() + 1;
            } else
            {
                return a.list.size();
            }
        }

        public Object getItem(int i)
        {
            return a(i);
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            if (view == null)
            {
                viewgroup = new g();
                view = c.getLayoutInflater().inflate(0x7f0301e2, null);
                viewgroup.a = view.findViewById(0x7f0a0703);
                viewgroup.i = (ImageView)view.findViewById(0x7f0a0708);
                viewgroup.h = (ImageView)view.findViewById(0x7f0a0705);
                viewgroup.b = view.findViewById(0x7f0a0704);
                viewgroup.c = view.findViewById(0x7f0a0707);
                viewgroup.d = view.findViewById(0x7f0a070a);
                viewgroup.g = (TextView)view.findViewById(0x7f0a0709);
                viewgroup.f = (TextView)view.findViewById(0x7f0a0706);
                viewgroup.e = (TextView)view.findViewById(0x7f0a04b2);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (g)view.getTag();
            }
            if (a.totalCount > b)
            {
                if (i == 0)
                {
                    ((g) (viewgroup)).a.setVisibility(0);
                    ((g) (viewgroup)).a.setOnClickListener(new v(this));
                    ((g) (viewgroup)).b.setVisibility(8);
                    ((g) (viewgroup)).c.setVisibility(8);
                    ((g) (viewgroup)).e.setVisibility(8);
                    return view;
                } else
                {
                    a((TalkModel)a.list.get(i - 1), viewgroup, i);
                    return view;
                }
            } else
            {
                a((TalkModel)a.list.get(i), viewgroup, i);
                return view;
            }
        }

        public f(TalkModelList talkmodellist)
        {
            c = TalkViewAct.this;
            super();
            b = 0;
            a = talkmodellist;
        }
    }

    static class g
    {

        View a;
        View b;
        View c;
        View d;
        TextView e;
        TextView f;
        TextView g;
        ImageView h;
        ImageView i;

        g()
        {
        }
    }


    private String comingFromSubject;
    private boolean isFirstRequestSucess;
    private boolean isForbidTalk;
    private boolean isOfficialAccount;
    private boolean isSystemMsg;
    private ResizeRelativeLayout linLayout;
    private PullToRefreshListView list;
    private EmotionSelector mEmotionSelector;
    private TextView mReport;
    private View mRootView;
    private long mSpecialId;
    private String meHeadUrl;
    private int msg_list_request_count;
    private c msma;
    private f talkAda;
    private String title;
    private long toUid;
    private int totalCount;

    public TalkViewAct()
    {
        msg_list_request_count = 20;
        isFirstRequestSucess = false;
        mSpecialId = 0L;
    }

    private void findViews()
    {
        linLayout = (ResizeRelativeLayout)findViewById(0x7f0a0701);
        mEmotionSelector = (EmotionSelector)findViewById(0x7f0a0199);
        list = (PullToRefreshListView)findViewById(0x7f0a0702);
        mRootView = findViewById(0x7f0a0055);
        mReport = (TextView)findViewById(0x7f0a070f);
        mReport.setVisibility(8);
    }

    private void goToUrl(String s1)
    {
        Intent intent = new Intent(this, com/ximalaya/ting/android/activity/web/WebActivityNew);
        intent.putExtra("ExtraUrl", s1);
        startActivity(intent);
    }

    private void handleLongClick(int i)
    {
        MenuDialog menudialog = new MenuDialog(this, Arrays.asList(returnStringArrays(isForbidTalk, i)));
        menudialog.setOnItemClickListener(new t(this, menudialog, i));
        menudialog.show();
    }

    private void initDatas()
    {
        meHeadUrl = getIntent().getStringExtra("meHeadUrl");
        title = getIntent().getStringExtra("title");
        toUid = getIntent().getLongExtra("toUid", -1L);
        isSystemMsg = getIntent().getBooleanExtra("isSystemMsg", false);
        isForbidTalk = getIntent().getBooleanExtra("isForbidTalk", false);
        isOfficialAccount = getIntent().getBooleanExtra("isOfficialAccount", false);
        mSpecialId = getIntent().getLongExtra("specialId", 0L);
        comingFromSubject = getIntent().getStringExtra("subjectTitle");
    }

    private void initViews()
    {
        mReport.setOnClickListener(new o(this));
        mRootView.getViewTreeObserver().addOnGlobalLayoutListener(this);
        linLayout.setOnResizeListener(new p(this));
        list.setOnItemLongClickListener(new q(this));
        EmotionSelector emotionselector = mEmotionSelector;
        Object obj;
        boolean flag;
        if (comingFromSubject == null || comingFromSubject.trim().equals(""))
        {
            obj = "";
        } else
        {
            obj = (new StringBuilder()).append("\u3010").append(comingFromSubject).append("\u3011").toString();
        }
        emotionselector.setText(((CharSequence) (obj)));
        mEmotionSelector.setOnEmotionTextChange(new r(this));
        mEmotionSelector.setOnSendButtonClickListener(new s(this));
        topTextView.setText(title);
        if (isForbidTalk)
        {
            mEmotionSelector.setVisibility(8);
        }
        mEmotionSelector.hideEmotionPanel();
        obj = mEmotionSelector;
        if (!TextUtils.isEmpty(mEmotionSelector.getText()))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        ((EmotionSelector) (obj)).enableSendBtn(flag);
    }

    private void insertTimeForSysMsg(List list1)
    {
        Collections.reverse(list1);
        HashMap hashmap = new HashMap();
        long l = 0L;
        for (int i = 0; i < list1.size();)
        {
            BaseCommentModel basecommentmodel = (BaseCommentModel)list1.get(i);
            long l2 = basecommentmodel.createdAt.longValue();
            long l1 = l;
            if (l2 - l >= 0x927c0L)
            {
                hashmap.put(basecommentmodel, TimeHelper.countTime2((new StringBuilder()).append(l2).append("").toString()));
                l1 = l2;
            }
            i++;
            l = l1;
        }

        BaseCommentModel basecommentmodel1;
        BaseCommentModel basecommentmodel2;
        for (Iterator iterator = hashmap.keySet().iterator(); iterator.hasNext(); list1.add(list1.indexOf(basecommentmodel1), basecommentmodel2))
        {
            basecommentmodel1 = (BaseCommentModel)iterator.next();
            basecommentmodel2 = new BaseCommentModel();
            basecommentmodel2.flag = false;
            basecommentmodel2.time = (String)hashmap.get(basecommentmodel1);
        }

        hashmap.clear();
    }

    private void insertTimeForTalkMsg(List list1)
    {
        String s1;
        HashMap hashmap;
        ArrayList arraylist;
        Iterator iterator1;
        hashmap = new HashMap();
        arraylist = new ArrayList();
        s1 = "0";
        iterator1 = list1.iterator();
_L6:
        if (!iterator1.hasNext()) goto _L2; else goto _L1
_L1:
        Object obj1 = (TalkModel)iterator1.next();
        if (!((TalkModel) (obj1)).flag) goto _L4; else goto _L3
_L3:
        Object obj = ((TalkModel) (obj1)).getCreatedAt();
        long l;
        long l1;
        l = Long.valueOf(((String) (obj))).longValue();
        l1 = Long.valueOf(s1).longValue();
        if (l - l1 < 0x927c0L) goto _L6; else goto _L5
_L5:
        hashmap.put(obj1, TimeHelper.countTime2(((String) (obj))));
        s1 = ((String) (obj));
          goto _L6
        obj1;
        s1 = ((String) (obj));
        obj = obj1;
_L7:
        Logger.e(((Exception) (obj)));
          goto _L6
_L4:
        arraylist.add(obj1);
          goto _L6
_L2:
        list1.removeAll(arraylist);
        TalkModel talkmodel;
        for (Iterator iterator = hashmap.keySet().iterator(); iterator.hasNext(); list1.add(list1.indexOf(obj), talkmodel))
        {
            obj = (TalkModel)iterator.next();
            talkmodel = new TalkModel();
            talkmodel.flag = false;
            talkmodel.time = (String)hashmap.get(obj);
        }

        hashmap.clear();
        return;
        obj;
          goto _L7
    }

    private boolean isDataListChange(CommentListInCommentNotice commentlistincommentnotice, CommentListInCommentNotice commentlistincommentnotice1)
    {
        if (commentlistincommentnotice == null || commentlistincommentnotice1 == null || commentlistincommentnotice.list == null || commentlistincommentnotice.list.isEmpty() || commentlistincommentnotice1.list == null || commentlistincommentnotice1.list.isEmpty())
        {
            return true;
        }
        return commentlistincommentnotice.list.size() != commentlistincommentnotice1.list.size() || ((BaseCommentModel)commentlistincommentnotice.list.get(0)).uid != ((BaseCommentModel)commentlistincommentnotice1.list.get(0)).uid;
    }

    private boolean isDataListChange(TalkModelList talkmodellist, TalkModelList talkmodellist1)
    {
        while (talkmodellist == null || talkmodellist1 == null || talkmodellist.list == null || talkmodellist.list.isEmpty() || talkmodellist1.list == null || talkmodellist1.list.isEmpty() || talkmodellist.toUid != talkmodellist1.toUid || talkmodellist.list.size() != talkmodellist1.list.size()) 
        {
            return true;
        }
        return false;
    }

    private void loadAgoSysMsg(View view)
    {
label0:
        {
            if (msma == null)
            {
                break label0;
            }
            Iterator iterator = msma.a.list.iterator();
            BaseCommentModel basecommentmodel;
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
                basecommentmodel = (BaseCommentModel)iterator.next();
            } while (!basecommentmodel.flag);
            (new a()).myexec(new Object[] {
                (new StringBuilder()).append(basecommentmodel.id).append("").toString(), "true", view
            });
        }
    }

    private void loadAgoTalkMsg(View view)
    {
label0:
        {
            if (talkAda == null)
            {
                break label0;
            }
            Iterator iterator = talkAda.a.list.iterator();
            TalkModel talkmodel;
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
                talkmodel = (TalkModel)iterator.next();
            } while (!talkmodel.flag);
            (new b()).myexec(new Object[] {
                talkmodel.getId(), "true", view
            });
        }
    }

    private void loadMsgs()
    {
        if (isSystemMsg)
        {
            loadNewSysMsg();
            return;
        } else
        {
            loadNewTalkMsg();
            return;
        }
    }

    private void loadNewSysMsg()
    {
        if (msma != null)
        {
            (new a()).myexec(new Object[] {
                (new StringBuilder()).append(msma.a(msma.getCount()).id).append("").toString(), "false", list
            });
        }
    }

    private void loadNewTalkMsg()
    {
        if (talkAda == null) goto _L2; else goto _L1
_L1:
        int i = talkAda.a.list.size() - 1;
_L6:
        Object obj;
        if (i < 0)
        {
            break; /* Loop/switch isn't completed */
        }
        obj = (TalkModel)talkAda.a.list.get(i);
        if (!((TalkModel) (obj)).flag || !Utilities.isBlank(((TalkModel) (obj)).myKey)) goto _L4; else goto _L3
_L3:
        obj = ((TalkModel) (obj)).getId();
        (new b()).myexec(new Object[] {
            obj, "false", list
        });
_L2:
        return;
_L4:
        i--;
        if (true) goto _L6; else goto _L5
_L5:
        (new b()).myexec(new Object[] {
            "0", "false", list
        });
        return;
    }

    private String[] returnStringArrays(boolean flag, int i)
    {
        if (flag)
        {
            return (new String[] {
                "\u590D\u5236"
            });
        }
        if (talkAda.a(i).SendMsg_FLAG)
        {
            return (new String[] {
                "\u67E5\u770B\u8D44\u6599", "\u590D\u5236"
            });
        } else
        {
            return (new String[] {
                "\u67E5\u770B\u8D44\u6599", "\u91CD\u65B0\u53D1\u9001", "\u590D\u5236"
            });
        }
    }

    private void saveSomthing()
    {
    }

    private void setMsgTextWithHtml(TextView textview, String s1)
    {
        int i = 0;
        textview.setText(Html.fromHtml(s1));
        textview.setMovementMethod(LinkMovementMethod.getInstance());
        Object obj = textview.getText();
        if (obj instanceof Spannable)
        {
            int j = ((CharSequence) (obj)).length();
            s1 = (Spannable)textview.getText();
            URLSpan aurlspan[] = (URLSpan[])s1.getSpans(0, j, android/text/style/URLSpan);
            obj = new SpannableStringBuilder(((CharSequence) (obj)));
            ((SpannableStringBuilder) (obj)).clearSpans();
            for (int k = aurlspan.length; i < k; i++)
            {
                URLSpan urlspan = aurlspan[i];
                ((SpannableStringBuilder) (obj)).setSpan(new d(urlspan.getURL()), s1.getSpanStart(urlspan), s1.getSpanEnd(urlspan), 34);
            }

            textview.setText(((CharSequence) (obj)));
        }
    }

    public void finish()
    {
        if (isFirstRequestSucess)
        {
            if (talkAda != null)
            {
                Intent intent = new Intent();
                intent.putExtra("lastMsg", talkAda.a());
                setResult(-1, intent);
            } else
            {
                setResult(-1);
            }
        } else
        {
            setResult(0);
        }
        super.finish();
    }

    public void finish(boolean flag)
    {
        super.finish();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301e1);
        if (loginInfoModel == null)
        {
            Toast.makeText(getApplicationContext(), "\u6CA1\u6709\u767B\u5F55\uFF0C\u8BF7\u767B\u5F55", 0).show();
            finish();
            return;
        }
        initCommon();
        initDatas();
        findViews();
        initViews();
        if (isSystemMsg)
        {
            (new a()).myexec(new Object[] {
                "0", "false", list
            });
            return;
        }
        if (talkAda == null)
        {
            (new b()).myexec(new Object[] {
                "0", "false", list
            });
            return;
        } else
        {
            list.setAdapter(talkAda);
            return;
        }
    }

    protected void onDestroy()
    {
        super.onDestroy();
    }

    public void onGlobalLayout()
    {
        Rect rect = new Rect();
        mRootView.getWindowVisibleDisplayFrame(rect);
        if (Math.abs(mRootView.getRootView().getHeight() - (rect.bottom - rect.top)) > 100)
        {
            mEmotionSelector.setEmotionSelectorIcon(0x7f02020a);
            return;
        } else
        {
            mEmotionSelector.setEmotionSelectorIcon(0x7f0202ff);
            return;
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
label0:
        {
            boolean flag = false;
            if (i == 4)
            {
                if (mEmotionSelector.getEmotionPanelStatus() != 0)
                {
                    break label0;
                }
                mEmotionSelector.shouldHandleFocusChangeEvent(false);
                mEmotionSelector.hideEmotionPanel();
                flag = true;
            }
            return flag;
        }
        finish();
        return false;
    }

    protected void onPause()
    {
        super.onPause();
        saveSomthing();
    }

    protected void onResume()
    {
        super.onResume();
        loadMsgs();
    }

    protected void onStop()
    {
        super.onStop();
    }






/*
    static int access$112(TalkViewAct talkviewact, int i)
    {
        i = talkviewact.totalCount + i;
        talkviewact.totalCount = i;
        return i;
    }

*/












/*
    static boolean access$302(TalkViewAct talkviewact, boolean flag)
    {
        talkviewact.isFirstRequestSucess = flag;
        return flag;
    }

*/



/*
    static c access$402(TalkViewAct talkviewact, c c)
    {
        talkviewact.msma = c;
        return c;
    }

*/






/*
    static f access$802(TalkViewAct talkviewact, f f)
    {
        talkviewact.talkAda = f;
        return f;
    }

*/

}
