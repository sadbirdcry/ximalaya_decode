// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.category.CategoryModel;
import com.ximalaya.ting.android.transaction.d.z;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.SelectImageUtil;
import com.ximalaya.ting.android.util.Session;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            b, c, d, e, 
//            a, f

public class CreateAlbumAct extends BaseActivity
{
    class a extends MyAsyncTask
    {

        final CreateAlbumAct a;
        private ProgressDialog b;

        protected transient Integer a(Void avoid[])
        {
            long l;
            if (!a.mCoverOK || a.mImageId > 0L)
            {
                break MISSING_BLOCK_LABEL_319;
            }
            RequestParams requestparams;
            try
            {
                publishProgress(new Integer[] {
                    Integer.valueOf(0)
                });
                avoid = z.a(com.ximalaya.ting.android.transaction.d.z.e.g, null, new String[] {
                    a.mCachePath
                });
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
                avoid = null;
            }
            if (avoid == null || avoid.b() != 0) goto _L2; else goto _L1
_L1:
            l = avoid.a();
            a.mImageId = l;
_L3:
            publishProgress(new Integer[] {
                Integer.valueOf(1)
            });
            a.mAlbum = null;
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/api1/upload/album_form").toString();
            requestparams = new RequestParams();
            requestparams.put("title", a.mAlbumTitle.getEditableText().toString().trim());
            requestparams.put("imageId", (new StringBuilder()).append("").append(l).toString());
            requestparams.put("isPublic", (new StringBuilder()).append("").append(a.mIsPublic).toString());
            requestparams.put("categoryId", (new StringBuilder()).append("").append(a.mCategoryId).toString());
            f.a().a(avoid, requestparams, DataCollectUtil.getDataFromView(a.mBtnCreate), new com.ximalaya.ting.android.activity.recording.f(this));
            if (a.mAlbum != null)
            {
                return Integer.valueOf(0);
            } else
            {
                return Integer.valueOf(2);
            }
_L2:
            if (avoid != null)
            {
                a.mErrMsg = avoid.c();
            }
            return Integer.valueOf(1);
            l = a.mImageId;
              goto _L3
        }

        protected void a(Integer integer)
        {
            super.onPostExecute(integer);
            if (a.mAct == null || a.mAct.isFinishing())
            {
                return;
            }
            b.cancel();
            if (integer.intValue() == 1)
            {
                integer = "\u4E0A\u4F20\u4E13\u8F91\u5C01\u9762\u5931\u8D25,\u8BF7\u91CD\u65B0\u4E0A\u4F20";
                if (!TextUtils.isEmpty(a.mErrMsg))
                {
                    integer = (new StringBuilder()).append("\u4E0A\u4F20\u4E13\u8F91\u5C01\u9762\u5931\u8D25,\u8BF7\u91CD\u65B0\u4E0A\u4F20").append(", ").append(a.mErrMsg).toString();
                }
                a.showToast(integer);
                return;
            }
            if (integer.intValue() == 2)
            {
                integer = "\u4E0A\u4F20\u4E13\u8F91\u4FE1\u606F\u5931\u8D25,\u8BF7\u91CD\u65B0\u4E0A\u4F20";
                if (!TextUtils.isEmpty(a.mErrMsg))
                {
                    integer = (new StringBuilder()).append("\u4E0A\u4F20\u4E13\u8F91\u4FE1\u606F\u5931\u8D25,\u8BF7\u91CD\u65B0\u4E0A\u4F20").append(", ").append(a.mErrMsg).toString();
                }
                a.showToast(integer);
                return;
            }
            if (integer.intValue() != 0)
            {
                a.showToast("\u521B\u5EFA\u4E13\u8F91\u5931\u8D25,\u8BF7\u91CD\u65B0\u521B\u5EFA");
                return;
            } else
            {
                Session.getSession().put("KEY_CREATE_ALBUM", a.mAlbum);
                a.setResult(-1);
                a.finish();
                return;
            }
        }

        protected transient void a(Integer ainteger[])
        {
            int i = ainteger[0].intValue();
            if (i == 0)
            {
                b.setMessage("\u6B63\u5728\u4E0A\u4F20\u4E13\u8F91\u5C01\u9762...");
            } else
            if (i == 1)
            {
                b.setMessage("\u6B63\u5728\u4E0A\u4F20\u4E13\u8F91\u4FE1\u606F...");
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((Integer)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            b = new MyProgressDialog(a.mActivity);
            b.setMessage("\u6B63\u5728\u521B\u5EFA\u4E13\u8F91\uFF0C\u8BF7\u7A0D\u5019...");
            b.setCancelable(false);
            b.setCanceledOnTouchOutside(false);
            b.show();
            a.mErrMsg = null;
        }

        protected void onProgressUpdate(Object aobj[])
        {
            a((Integer[])aobj);
        }

        a()
        {
            a = CreateAlbumAct.this;
            super();
        }
    }


    public static final String KEY_ALBUM_CATEGORY_CACHE = "KEY_CATEGORY_CACHE";
    public static final String KEY_CREATE_ALBUM = "KEY_CREATE_ALBUM";
    private static final String TAG = "CreateAlbumAct";
    private Activity mAct;
    private ArrayAdapter mAdapter;
    private AlbumModel mAlbum;
    private Spinner mAlbumCategory;
    private ImageView mAlbumCover;
    private RadioGroup mAlbumPublic;
    private EditText mAlbumTitle;
    private Button mBtnCreate;
    private File mCacheFile;
    private String mCachePath;
    private long mCategoryId;
    private List mCategorys;
    private boolean mCoverOK;
    private String mErrMsg;
    private long mImageId;
    private int mIsPublic;
    private boolean mNeedRefrsh;

    public CreateAlbumAct()
    {
        mCoverOK = false;
        mIsPublic = 1;
        mCategoryId = -1L;
        mNeedRefrsh = false;
        mCategorys = new ArrayList();
    }

    private boolean checkParam()
    {
        if (mIsPublic >= 1)
        {
            if (mIsPublic <= 2);
        }
        String s = mAlbumTitle.getEditableText().toString().trim();
        if (s.length() == 0)
        {
            showToast("\u8BF7\u8F93\u5165\u4E13\u8F91\u6807\u9898");
            return false;
        }
        if (s.length() > 80)
        {
            showToast("\u4E13\u8F91\u540D\u79F0\u4E0D\u5F97\u8D85\u8FC780\u4E2A\u5B57\u7B26\u621640\u4E2A\u6C49\u5B57");
            return false;
        }
        if (mCategoryId < 0L)
        {
            showToast("\u8BF7\u9009\u62E9\u4E13\u8F91\u5206\u7C7B");
            return false;
        } else
        {
            return true;
        }
    }

    private void initUI()
    {
        mAlbumCover = (ImageView)findViewById(0x7f0a0023);
        mAlbumPublic = (RadioGroup)findViewById(0x7f0a007f);
        mAlbumTitle = (EditText)findViewById(0x7f0a0084);
        mAlbumCategory = (Spinner)findViewById(0x7f0a0087);
        mBtnCreate = (Button)findViewById(0x7f0a0088);
        initCommon();
        setTitleText("\u521B\u5EFA\u4E13\u8F91");
        if (android.os.Build.VERSION.SDK_INT >= 19)
        {
            mCachePath = MyApplication.b().getExternalCacheDir().getAbsolutePath();
        } else
        {
            mCachePath = com.ximalaya.ting.android.a.ac;
        }
        mCachePath = (new StringBuilder()).append(mCachePath).append(File.separator).append("album_cover.jpg").toString();
        mCacheFile = new File(mCachePath);
        Logger.e("CreateAlbumAct", mCachePath);
        mAdapter = new ArrayAdapter(mAct, 0x1090008, mCategorys);
        mAdapter.setDropDownViewResource(0x1090009);
        mAlbumCategory.setAdapter(mAdapter);
        mAlbumCategory.setOnItemSelectedListener(new b(this));
        mAlbumCover.setOnClickListener(new c(this));
        mAlbumPublic.setOnCheckedChangeListener(new d(this));
        mBtnCreate.setOnClickListener(new e(this));
    }

    private void loadCategory()
    {
        Object obj;
        try
        {
            obj = JSON.parseArray(SharedPreferencesUtil.getInstance(mActivity).getString("KEY_CATEGORY_CACHE"), com/ximalaya/ting/android/model/category/CategoryModel);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            exception = null;
        }
        if (obj != null && ((List) (obj)).size() > 0)
        {
            mCategorys.clear();
            mCategorys.addAll(((java.util.Collection) (obj)));
            mAdapter.notifyDataSetChanged();
            mNeedRefrsh = false;
        } else
        {
            mNeedRefrsh = true;
        }
        obj = new RequestParams();
        ((RequestParams) (obj)).put("hasHot", "false");
        f.a().a("mobile/category/sound", ((RequestParams) (obj)), DataCollectUtil.getDataFromView(rootView), new com.ximalaya.ting.android.activity.recording.a(this));
    }

    private void loadData()
    {
        loadCategory();
    }

    private void saveToCache(Uri uri)
    {
        Object obj1;
        Exception exception;
        exception = null;
        obj1 = null;
        if (uri != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Object obj;
        if (mCacheFile == null)
        {
            mCacheFile = new File(mCachePath);
        }
        obj = new FileOutputStream(mCacheFile);
        obj1 = new byte[8192];
        uri = getContentResolver().openInputStream(uri);
_L5:
        int i = uri.read(((byte []) (obj1)));
        if (i <= 0) goto _L4; else goto _L3
_L3:
        ((FileOutputStream) (obj)).write(((byte []) (obj1)), 0, i);
          goto _L5
        exception;
        obj1 = obj;
        obj = uri;
        uri = exception;
_L11:
        uri.printStackTrace();
        if (obj1 != null)
        {
            try
            {
                ((FileOutputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Uri uri)
            {
                uri.printStackTrace();
            }
        }
        if (obj == null) goto _L1; else goto _L6
_L6:
        try
        {
            ((InputStream) (obj)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Uri uri) { }
_L8:
        uri.printStackTrace();
        return;
_L4:
        ((FileOutputStream) (obj)).flush();
        if (obj != null)
        {
            try
            {
                ((FileOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        if (uri == null) goto _L1; else goto _L7
_L7:
        try
        {
            uri.close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Uri uri) { }
          goto _L8
          goto _L1
        uri;
        obj = null;
        obj1 = exception;
_L10:
        if (obj1 != null)
        {
            try
            {
                ((FileOutputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((IOException) (obj1)).printStackTrace();
            }
        }
        if (obj != null)
        {
            try
            {
                ((InputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        throw uri;
        uri;
        Object obj2 = null;
        obj1 = obj;
        obj = obj2;
        continue; /* Loop/switch isn't completed */
        obj1;
        Uri uri1 = uri;
        uri = ((Uri) (obj1));
        obj1 = obj;
        obj = uri1;
        continue; /* Loop/switch isn't completed */
        uri;
        if (true) goto _L10; else goto _L9
_L9:
        uri;
        obj = null;
          goto _L11
        uri;
        Object obj3 = null;
        obj1 = obj;
        obj = obj3;
          goto _L11
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        if (i != 161) goto _L2; else goto _L1
_L1:
        if (j == -1)
        {
            SelectImageUtil.cropImage(mAct, Uri.fromFile(mCacheFile), mCachePath, 1, 1, 640, 640);
        }
_L4:
        return;
_L2:
        if (i != 4066)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == -1)
        {
            SelectImageUtil.cropImage(mAct, intent.getData(), mCachePath, 1, 1, 640, 640);
            return;
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (i == 2803)
        {
            if (j == -1)
            {
                intent = BitmapFactory.decodeFile(mCachePath);
                if (intent != null)
                {
                    mCoverOK = true;
                    mAlbumCover.setImageBitmap(intent);
                    return;
                } else
                {
                    mCoverOK = false;
                    return;
                }
            }
        } else
        {
            super.onActivityResult(i, j, intent);
            return;
        }
        if (true) goto _L4; else goto _L5
_L5:
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03000d);
        mAct = this;
        initUI();
        loadData();
    }





/*
    static long access$1002(CreateAlbumAct createalbumact, long l)
    {
        createalbumact.mImageId = l;
        return l;
    }

*/



/*
    static AlbumModel access$1102(CreateAlbumAct createalbumact, AlbumModel albummodel)
    {
        createalbumact.mAlbum = albummodel;
        return albummodel;
    }

*/






/*
    static long access$302(CreateAlbumAct createalbumact, long l)
    {
        createalbumact.mCategoryId = l;
        return l;
    }

*/





/*
    static int access$602(CreateAlbumAct createalbumact, int i)
    {
        createalbumact.mIsPublic = i;
        return i;
    }

*/




/*
    static String access$802(CreateAlbumAct createalbumact, String s)
    {
        createalbumact.mErrMsg = s;
        return s;
    }

*/

}
