// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseFragmentActivity;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.login.AuthorizeActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.userspace.SoundListFragmentNew;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.record.FormResult;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.transaction.d.r;
import com.ximalaya.ting.android.transaction.d.z;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Session;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.MyVerificationCodeDialogFragment;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            k, v, ab, aa, 
//            ad, ae, af, ag, 
//            ah, ac, s, t, 
//            u, w, x, l, 
//            m, n, o, p, 
//            q, r, ai, z, 
//            y, aj, ak

public class RecUploadFormAct extends BaseFragmentActivity
{
    class b extends MyAsyncTask
    {

        final RecUploadFormAct a;
        private MyVerificationCodeDialogFragment b;
        private ProgressDialog c;

        protected transient FormResult a(View aview[])
        {
            FormResult formresult = new FormResult();
            View view = aview[0];
            RequestParams requestparams = new RequestParams();
            requestparams.put("title", (new StringBuilder()).append("").append(a.mRecordingModel.title).toString());
            requestparams.put("albumId", (new StringBuilder()).append("").append(a.mRecordingModel.albumId).toString());
            if (a.mRecordingModel.activityId > 0L)
            {
                requestparams.put("activityId", (new StringBuilder()).append("").append(a.mRecordingModel.activityId).toString());
            }
            if (a.mActivityId > 0L)
            {
                requestparams.put("activityId", (new StringBuilder()).append("").append(a.mActivityId).toString());
            }
            Object obj = "";
            if (a.mIsShareSina)
            {
                obj = (new StringBuilder()).append("").append("tSina").toString();
            }
            aview = ((View []) (obj));
            if (a.mIsShareTQQ)
            {
                aview = ((View []) (obj));
                if (!TextUtils.isEmpty(((CharSequence) (obj))))
                {
                    aview = (new StringBuilder()).append(((String) (obj))).append(",").toString();
                }
                aview = (new StringBuilder()).append(aview).append("tQQ").toString();
            }
            obj = aview;
            if (a.mIsShareRenn)
            {
                obj = aview;
                if (!TextUtils.isEmpty(aview))
                {
                    obj = (new StringBuilder()).append(aview).append(",").toString();
                }
                obj = (new StringBuilder()).append(((String) (obj))).append("renren").toString();
            }
            if (!TextUtils.isEmpty(((CharSequence) (obj))))
            {
                requestparams.put("shareThirdpartyNames", ((String) (obj)));
                if (!TextUtils.isEmpty(a.mShareContent))
                {
                    requestparams.put("shareContent", a.mShareContent);
                }
            }
            if (!TextUtils.isEmpty(a.mRecordingModel.checkUUID))
            {
                requestparams.put("checkCode", a.mRecordingModel.checkCode);
                requestparams.put("checkUUID", a.mRecordingModel.checkUUID);
            }
            f.a().a("mobile/api1/upload/track_form", requestparams, DataCollectUtil.getDataFromView(view), new aj(this, formresult, view));
            return formresult;
        }

        protected void a(FormResult formresult)
        {
            if (a.mAct == null || a.mAct.isFinishing())
            {
                return;
            }
            c.cancel();
            if (formresult == null)
            {
                Toast.makeText(a.getApplicationContext(), "\u4E0A\u4F20\u9519\u8BEF\uFF0C\u8BF7\u91CD\u8BD5", 0).show();
                return;
            }
            if (formresult.ret == 0)
            {
                a.mRecordingModel.checkUUID = null;
                a.mRecordingModel.checkCode = null;
                a.mRecordingModel.formId = formresult.formId;
                a.doActionUploadBody();
                return;
            }
            if (formresult.ret == 211)
            {
                a.mRecordingModel.checkUUID = formresult.checkUUID;
                if (b == null)
                {
                    b = new MyVerificationCodeDialogFragment("from_sound", formresult.checkCodeUrl, new ak(this));
                }
                b.show(a.getSupportFragmentManager(), "from_sound", formresult.checkCodeUrl);
                return;
            }
            if (formresult.ret == 212)
            {
                a.mRecordingModel.checkUUID = null;
                a.mRecordingModel.checkCode = null;
                Toast.makeText(a.getApplicationContext(), "\u9A8C\u8BC1\u7801\u9519\u8BEF\uFF01", 1).show();
                return;
            }
            if (formresult.ret == 4)
            {
                a.handleClick();
                return;
            } else
            {
                a.mRecordingModel.checkUUID = null;
                a.mRecordingModel.checkCode = null;
                Toast.makeText(a.getApplicationContext(), formresult.msg, 0).show();
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((View[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((FormResult)obj);
        }

        protected void onPreExecute()
        {
            a.loginInfoModel = UserInfoMannage.getInstance().getUser();
            if (a.loginInfoModel == null)
            {
                return;
            } else
            {
                c = new MyProgressDialog(a.mAct);
                c.setMessage("\u6B63\u5728\u51C6\u5907\u4E0A\u4F20...");
                c.setCancelable(false);
                c.setCanceledOnTouchOutside(false);
                c.show();
                return;
            }
        }

        b()
        {
            a = RecUploadFormAct.this;
            super();
        }
    }

    private class c
    {

        public ImageView a;
        public TextView b;
        public TextView c;
        final RecUploadFormAct d;

        private c()
        {
            d = RecUploadFormAct.this;
            super();
        }

        c(k k1)
        {
            this();
        }
    }


    private static final String CONTENT_TYPE = "image/*";
    private static final int CREATE_ALBUM = 2804;
    private static final int GET_IMAGE_FROM_CAMERA = 161;
    private static final int GET_IMAGE_FROM_DCIM = 4066;
    private static final int PHOTO_CROP = 2803;
    private static final int REQUEST_BIND_PHONE = 2805;
    private static final int REQUEST_BIND_QQ = 2806;
    private static final int REQUEST_BIND_RENN = 2808;
    private static final int REQUEST_BIND_SINA = 2807;
    private static final String TAG = "RecUploadFormAct";
    private Activity mAct;
    private long mActivityId;
    private ViewGroup mAddAlbum;
    private ViewGroup mAddCover;
    private android.view.View.OnClickListener mAddCoverListener;
    private a mAlbumAdapter;
    private TextView mAlbumCount;
    private long mAlbumId;
    private ListView mAlbumListView;
    private List mAlbums;
    private TextView mBtnShareCancel;
    private TextView mBtnShareOK;
    private ImageView mBtnShareRenn;
    private ImageView mBtnShareSina;
    private ImageView mBtnShareTQQ;
    private View mBtnUpload;
    private ImageView mCacheImageView;
    private r mCacheManager;
    private String mCachePath;
    private MenuDialog mChooseCover;
    private ViewGroup mContainer;
    private List mCovers;
    private View mCreateAlbum;
    private String mErrMsg;
    private EditText mEtShareContent;
    public RelativeLayout mFooterViewLoading;
    private boolean mFromCamera;
    private boolean mIsBindQQ;
    private boolean mIsBindRenn;
    private boolean mIsBindSina;
    private boolean mIsShareRenn;
    private boolean mIsShareSina;
    private boolean mIsShareTQQ;
    private ImageView mLabelRenn;
    private ImageView mLabelSina;
    private ImageView mLabelTQQ;
    private int mLastBindFlag;
    private boolean mLoadingAlbum;
    private int mMaxContentLen;
    private int mPageId;
    private int mPageSize;
    private RecordingModel mRecordingModel;
    private android.view.View.OnClickListener mReloadListener;
    private PopupWindow mSelectAlbum;
    private PopupWindow mSelectCover;
    private PopupWindow mSelectShare;
    private ViewGroup mShareContainer;
    private String mShareContent;
    private View mShareFlagContainer;
    private TextView mSoundAlbum;
    private ImageView mSoundCover;
    private EditText mSoundTitle;
    private int mTotal;
    private b mUploadFormTask;
    private z mUploadManager;
    private LoginInfoModel mUser;

    public RecUploadFormAct()
    {
        mMaxContentLen = 300;
        mActivityId = 0L;
        mFromCamera = false;
        mCovers = new ArrayList();
        mAlbums = new ArrayList();
        mLoadingAlbum = false;
        mErrMsg = "\u7F51\u7EDC\u4E0D\u7ED9\u529B\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
        mPageId = 1;
        mPageSize = 15;
        mIsShareSina = false;
        mIsShareTQQ = false;
        mIsShareRenn = false;
        mIsBindSina = false;
        mIsBindQQ = false;
        mIsBindRenn = false;
        mAddCoverListener = new k(this);
        mReloadListener = new v(this);
    }

    private boolean checkParam()
    {
        String s1 = mSoundTitle.getEditableText().toString().trim();
        if (s1.length() == 0)
        {
            showToast("\u8BF7\u8F93\u5165\u58F0\u97F3\u6807\u9898");
            return false;
        }
        mRecordingModel.title = s1;
        if (mAlbumId <= 0L)
        {
            showToast("\u8BF7\u9009\u62E9\u6240\u5C5E\u4E13\u8F91");
            return false;
        } else
        {
            return true;
        }
    }

    private boolean checkShare()
    {
        if (!mIsShareSina && !mIsShareTQQ && !mIsShareRenn)
        {
            showToast("\u8BF7\u9009\u62E9\u8981\u5206\u4EAB\u7684\u7B2C\u4E09\u65B9\u5E73\u53F0\uFF01");
            return false;
        }
        if (!TextUtils.isEmpty(mShareContent) && mShareContent.length() > mMaxContentLen)
        {
            showToast("\u4EB2\uFF0C\u5206\u4EAB\u7C7B\u5BB9\u6700\u591A300\u5B57\u54E6~");
            return false;
        } else
        {
            return true;
        }
    }

    private void doActionBindQQ()
    {
        Intent intent = new Intent(mActivity, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
        mLastBindFlag = 13;
        intent.putExtra("lgflag", mLastBindFlag);
        startActivityForResult(intent, 2806);
    }

    private void doActionBindRenn()
    {
        Intent intent = new Intent(mActivity, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
        mLastBindFlag = 14;
        intent.putExtra("lgflag", mLastBindFlag);
        startActivityForResult(intent, 2808);
    }

    private void doActionBindSina()
    {
        Intent intent = new Intent(mActivity, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
        mLastBindFlag = 12;
        intent.putExtra("lgflag", mLastBindFlag);
        startActivityForResult(intent, 2807);
    }

    private int doActionSaveToDrafts()
    {
        String s1 = mSoundTitle.getEditableText().toString().trim();
        mRecordingModel.title = s1;
        mRecordingModel.isNeedUpload = false;
        mUploadManager.c(mRecordingModel, false);
        return -1;
    }

    private void doActionUploadBody()
    {
        mUploadManager.a(mRecordingModel);
        if (mRecordingModel.activityId <= 0L)
        {
            goSoundListFragment();
        }
        mAct.finish();
    }

    private void doActionUploadButton(View view)
    {
        while (!checkParam() || mUploadFormTask != null && mUploadFormTask.getStatus() != android.os.AsyncTask.Status.FINISHED) 
        {
            return;
        }
        mUploadFormTask = new b();
        mUploadFormTask.myexec(new View[] {
            view
        });
    }

    private void getFromCamera()
    {
        try
        {
            mFromCamera = true;
            Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
            intent.putExtra("output", Uri.fromFile(getTempCoverFile()));
            startActivityForResult(intent, 161);
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void getFromDcim()
    {
        Intent intent = new Intent("android.intent.action.PICK", null);
        intent.setDataAndType(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");
        startActivityForResult(intent, 4066);
    }

    private File getTempCoverFile()
    {
        Object obj = mCacheManager.f();
        mCachePath = (new StringBuilder()).append(((String) (obj))).append("cover").append(System.currentTimeMillis()).append(".jpg").toString();
        obj = new File(mCachePath);
        if (!((File) (obj)).exists())
        {
            try
            {
                ((File) (obj)).createNewFile();
            }
            catch (IOException ioexception)
            {
                ioexception.printStackTrace();
            }
        }
        Logger.e("RecUploadFormAct", (new StringBuilder()).append("getTempCoverFile ").append(((File) (obj)).getAbsolutePath()).toString());
        return ((File) (obj));
    }

    private void goSoundListFragment()
    {
        if (MainTabActivity2.mainTabActivity != null)
        {
            Bundle bundle = new Bundle();
            bundle.putInt("DEST_FLAG", 1);
            MainTabActivity2.mainTabActivity.startFragment(com/ximalaya/ting/android/fragment/userspace/SoundListFragmentNew, bundle);
        }
    }

    private void handleClick()
    {
        (new DialogBuilder(mAct)).setMessage("\u53EA\u6709\u5148\u7ED1\u5B9A\u624B\u673A\u53F7\u624D\u80FD\u4E0A\u4F20\u58F0\u97F3\u54E6").setOutsideTouchCancel(false).setOkBtn("\u53BB\u7ED1\u5B9A", new ab(this)).setCancelBtn("\u53D6\u6D88\u4E0A\u4F20", new aa(this)).showConfirm();
    }

    private void init()
    {
        mContainer = (ViewGroup)findViewById(0x7f0a005a);
        mSoundCover = (ImageView)findViewById(0x7f0a00e4);
        mSoundTitle = (EditText)findViewById(0x7f0a00e7);
        mSoundAlbum = (TextView)findViewById(0x7f0a00ea);
        mBtnUpload = findViewById(0x7f0a00f0);
        mShareFlagContainer = findViewById(0x7f0a00eb);
        mLabelSina = (ImageView)findViewById(0x7f0a00ed);
        mLabelTQQ = (ImageView)findViewById(0x7f0a00ee);
        mLabelRenn = (ImageView)findViewById(0x7f0a00ef);
        initCommon();
        setTitleText("\u58F0\u97F3\u4FE1\u606F");
        nextButton.setImageResource(0x7f020101);
        nextButton.setScaleType(android.widget.ImageView.ScaleType.FIT_CENTER);
        nextButton.setVisibility(0);
        ((android.view.ViewGroup.MarginLayoutParams)nextButton.getLayoutParams()).rightMargin = ToolUtil.dp2px(mAct, 15F);
        mSoundTitle.setText(mRecordingModel.title);
        if (mRecordingModel.covers != null && mRecordingModel.covers.length > 0)
        {
            try
            {
                mSoundCover.setImageBitmap(BitmapFactory.decodeFile(mRecordingModel.covers[0]));
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        nextButton.setOnClickListener(new ad(this));
        mShareFlagContainer.setOnClickListener(new ae(this));
        mSoundCover.setOnClickListener(new af(this));
        mSoundAlbum.setOnClickListener(new ag(this));
        mBtnUpload.setOnClickListener(new ah(this));
    }

    private void initChooseCoverDialog()
    {
        if (mChooseCover != null)
        {
            return;
        } else
        {
            ArrayList arraylist = new ArrayList();
            arraylist.add("\u4ECE\u76F8\u518C\u9009\u62E9");
            arraylist.add("\u62CD\u7167");
            mChooseCover = new MenuDialog(mAct, arraylist);
            mChooseCover.setOnItemClickListener(new ac(this));
            return;
        }
    }

    private void initSelectAlbumWindow()
    {
        if (mSelectAlbum != null)
        {
            return;
        } else
        {
            mAddAlbum = (ViewGroup)LayoutInflater.from(mActivity).inflate(0x7f0301a9, mContainer, false);
            mAlbumCount = (TextView)mAddAlbum.findViewById(0x7f0a0239);
            mAlbumListView = (ListView)mAddAlbum.findViewById(0x7f0a00dc);
            mCreateAlbum = mAddAlbum.findViewById(0x7f0a0088);
            mFooterViewLoading = (RelativeLayout)LayoutInflater.from(this).inflate(0x7f0301fb, mAlbumListView, false);
            mAlbumListView.addFooterView(mFooterViewLoading);
            showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.HIDE_ALL);
            mAlbumAdapter = new a(null);
            mAlbumListView.setAdapter(mAlbumAdapter);
            mAddAlbum.setOnClickListener(new s(this));
            mAlbumListView.setOnItemClickListener(new t(this));
            mAlbumListView.setOnScrollListener(new u(this));
            mCreateAlbum.setOnClickListener(new w(this));
            mSelectAlbum = new PopupWindow(mAct);
            mSelectAlbum.setContentView(mAddAlbum);
            mSelectAlbum.setAnimationStyle(0);
            mSelectAlbum.setWidth(-1);
            mSelectAlbum.setHeight(-1);
            mSelectAlbum.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#b0000000")));
            mSelectAlbum.setOutsideTouchable(true);
            mSelectAlbum.setFocusable(true);
            return;
        }
    }

    private void initSelectCoverWindow()
    {
        if (mSelectCover != null)
        {
            return;
        }
        mAddCover = (ViewGroup)LayoutInflater.from(mActivity).inflate(0x7f0301a8, mContainer, false);
        ViewGroup viewgroup = (ViewGroup)mAddCover.getChildAt(0);
        for (int i = 0; i < viewgroup.getChildCount(); i++)
        {
            View view = viewgroup.getChildAt(i);
            if (!(view instanceof ImageView))
            {
                continue;
            }
            if (mRecordingModel.covers != null && i <= mRecordingModel.covers.length)
            {
                view.setVisibility(0);
                if (i < mRecordingModel.covers.length)
                {
                    ((ImageView)view).setImageBitmap(BitmapFactory.decodeFile(mRecordingModel.covers[i]));
                    mCovers.add(mRecordingModel.covers[i]);
                    view.setTag(mRecordingModel.covers[i]);
                }
            }
            view.setOnClickListener(mAddCoverListener);
        }

        mAddCover.setOnClickListener(new x(this));
        mSelectCover = new PopupWindow(mAct);
        mSelectCover.setContentView(mAddCover);
        mSelectCover.setAnimationStyle(0);
        mSelectCover.setWidth(-1);
        mSelectCover.setHeight(-1);
        mSelectCover.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#b0000000")));
        mSelectCover.setOutsideTouchable(true);
        mSelectCover.setFocusable(true);
    }

    private void initSelectShareWindow()
    {
        if (mSelectShare != null)
        {
            return;
        }
        mShareContainer = (ViewGroup)LayoutInflater.from(mActivity).inflate(0x7f0301a7, mContainer, false);
        mBtnShareSina = (ImageView)mShareContainer.findViewById(0x7f0a0665);
        mBtnShareTQQ = (ImageView)mShareContainer.findViewById(0x7f0a0666);
        mBtnShareRenn = (ImageView)mShareContainer.findViewById(0x7f0a0667);
        mEtShareContent = (EditText)mShareContainer.findViewById(0x7f0a066a);
        mBtnShareCancel = (TextView)mShareContainer.findViewById(0x7f0a0668);
        mBtnShareOK = (TextView)mShareContainer.findViewById(0x7f0a0669);
        updateUserInfo();
        updateShareFlag();
        if (!TextUtils.isEmpty(mShareContent))
        {
            mEtShareContent.setText(mShareContent);
        } else
        {
            String s1;
            if (mUser.isVerified)
            {
                s1 = "\u58F0\u97F3\u8F6C\u7801\u5B8C\u6210\u540E\u6211\u4EEC\u5C06\u81EA\u52A8\u4E3A\u60A8\u540C\u6B65\u5230\u7B2C\u4E09\u65B9~";
            } else
            {
                s1 = "\u58F0\u97F3\u8F6C\u7801\u5E76\u4E14\u5BA1\u6838\u5B8C\u6210\u540E\u6211\u4EEC\u5C06\u81EA\u52A8\u4E3A\u60A8\u540C\u6B65\u5230\u7B2C\u4E09\u65B9~";
            }
            mEtShareContent.setHint(s1);
        }
        mShareContainer.setOnClickListener(new l(this));
        mEtShareContent.addTextChangedListener(new m(this));
        mBtnShareSina.setOnClickListener(new n(this));
        mBtnShareTQQ.setOnClickListener(new o(this));
        mBtnShareRenn.setOnClickListener(new p(this));
        mBtnShareCancel.setOnClickListener(new q(this));
        mBtnShareOK.setOnClickListener(new com.ximalaya.ting.android.activity.recording.r(this));
        mSelectShare = new PopupWindow(mAct);
        mSelectShare.setContentView(mShareContainer);
        mSelectShare.setAnimationStyle(0);
        mSelectShare.setWidth(-1);
        mSelectShare.setHeight(-1);
        mSelectShare.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#b0000000")));
        mSelectShare.setOutsideTouchable(true);
        mSelectShare.setFocusable(true);
        mSelectShare.update();
    }

    private void loadAlbum(boolean flag, View view)
    {
        while (flag && mAlbums.size() > 0 || mLoadingAlbum) 
        {
            return;
        }
        mLoadingAlbum = true;
        RequestParams requestparams = new RequestParams();
        requestparams.put("pageId", mPageId);
        requestparams.put("pageSize", mPageSize);
        f.a().a("mobile/api1/upload/albums", requestparams, DataCollectUtil.getDataFromView(view), new ai(this));
    }

    private void releaseRes()
    {
        mUploadManager.a(mRecordingModel, false);
    }

    private void showErr(String s1, String s2)
    {
        showToast(s1);
        showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.NO_CONNECTION);
        mFooterViewLoading.setOnClickListener(mReloadListener);
    }

    private void updateShareFlag()
    {
        if (mIsShareSina)
        {
            mBtnShareSina.setImageResource(0x7f0204a2);
            mLabelSina.setImageResource(0x7f0204a2);
        } else
        {
            mBtnShareSina.setImageResource(0x7f0204a1);
            mLabelSina.setImageResource(0x7f0204a1);
        }
        if (mIsShareTQQ)
        {
            mBtnShareTQQ.setImageResource(0x7f0204a0);
            mLabelTQQ.setImageResource(0x7f0204a0);
        } else
        {
            mBtnShareTQQ.setImageResource(0x7f02049f);
            mLabelTQQ.setImageResource(0x7f02049f);
        }
        if (mIsShareRenn)
        {
            mBtnShareRenn.setImageResource(0x7f02049e);
            mLabelRenn.setImageResource(0x7f02049e);
            return;
        } else
        {
            mBtnShareRenn.setImageResource(0x7f02049d);
            mLabelRenn.setImageResource(0x7f02049d);
            return;
        }
    }

    private void updateUserInfo()
    {
        mUser = UserInfoMannage.getInstance().getUser();
        loginInfoModel = mUser;
        if (mUser.bindStatus != null)
        {
            Iterator iterator = mUser.bindStatus.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                ThirdPartyUserInfo thirdpartyuserinfo = (ThirdPartyUserInfo)iterator.next();
                if (!thirdpartyuserinfo.isExpired())
                {
                    if (thirdpartyuserinfo.getIdentity().equals("1"))
                    {
                        mIsBindSina = true;
                    } else
                    if (thirdpartyuserinfo.getIdentity().equals("2"))
                    {
                        mIsBindQQ = true;
                    } else
                    if (thirdpartyuserinfo.getIdentity().equals("3"))
                    {
                        mIsBindRenn = true;
                    }
                }
            } while (true);
        }
    }

    public void finish()
    {
        super.finish();
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        if (i != 161) goto _L2; else goto _L1
_L1:
        Logger.e("RecUploadFormAct", (new StringBuilder()).append("mCachePath ").append(mCachePath).toString());
        intent = new File(mCachePath);
        if (j != -1) goto _L4; else goto _L3
_L3:
        startPhotoZoom(Uri.fromFile(intent));
_L6:
        return;
_L4:
        intent.delete();
        return;
_L2:
        if (i != 4066)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == -1)
        {
            startPhotoZoom(intent.getData());
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
        if (i != 2803)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == -1)
        {
            mFromCamera = false;
            intent = (String)mCacheImageView.getTag();
            if (!TextUtils.isEmpty(intent))
            {
                mCovers.remove(intent);
            }
            intent = BitmapFactory.decodeFile(mCachePath);
            mCacheImageView.setImageBitmap(intent);
            mCacheImageView.setTag(mCachePath);
            mCovers.add(mCachePath);
            if (mCovers.size() > 0)
            {
                intent = BitmapFactory.decodeFile((String)mCovers.get(0));
                mSoundCover.setImageBitmap(intent);
            }
            mRecordingModel.covers = (String[])mCovers.toArray(new String[0]);
            intent = ((ViewGroup)mAddCover.getChildAt(0)).getChildAt(mCovers.size());
            if (intent != null && (intent instanceof ImageView))
            {
                intent.setVisibility(0);
                return;
            }
        } else
        {
            (new File(mCachePath)).delete();
            return;
        }
        if (true) goto _L6; else goto _L7
_L7:
        if (i != 2804)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == -1)
        {
            intent = ((Intent) (Session.getSession().remove("KEY_CREATE_ALBUM")));
            if (intent != null && (intent instanceof AlbumModel))
            {
                intent = (AlbumModel)intent;
                mAlbums.add(intent);
                mTotal = mTotal + 1;
                mAlbumCount.setText((new StringBuilder()).append("\u9009\u62E9\u4E13\u8F91(").append(mTotal).append(")").toString());
                if (mAlbumAdapter != null)
                {
                    mAlbumAdapter.notifyDataSetChanged();
                    return;
                }
            }
        }
        if (true) goto _L6; else goto _L8
_L8:
        if (i != 2805)
        {
            break; /* Loop/switch isn't completed */
        }
        if (j == -1)
        {
            doActionUploadButton(rootView);
            return;
        }
        if (true) goto _L6; else goto _L9
_L9:
        if (i == 2806 || i == 2807)
        {
            if (j == -1)
            {
                updateUserInfo();
                return;
            }
        } else
        {
            super.onActivityResult(i, j, intent);
            return;
        }
        if (true) goto _L6; else goto _L10
_L10:
    }

    public void onBackPressed()
    {
        DialogBuilder dialogbuilder = new DialogBuilder(mActivity);
        dialogbuilder.setOutsideTouchCancel(false);
        dialogbuilder.setTitle("\u6E29\u99A8\u63D0\u793A").setMessage("\u76F4\u63A5\u9000\u51FA\u5C06\u4E22\u5931\u5F55\u97F3\u6570\u636E\uFF0C\u662F\u5426\u5B58\u5728\u8349\u7A3F\u7BB1\u7559\u7740\u4E0B\u6B21\u4F7F\u7528\uFF1F").setCancelBtn("\u9000\u51FA", new com.ximalaya.ting.android.activity.recording.z(this)).setOkBtn("\u5B58\u8349\u7A3F\u7BB1", new y(this)).showConfirm();
    }

    public void onConfigurationChanged(Configuration configuration)
    {
        super.onConfigurationChanged(configuration);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030027);
        mAct = this;
        mCacheManager = r.a();
        mUploadManager = z.a(mActivity);
        mRecordingModel = null;
        mUser = UserInfoMannage.getInstance().getUser();
        bundle = ((Bundle) (Session.getSession().get("soundInfo")));
        if (bundle != null && (bundle instanceof RecordingModel))
        {
            mRecordingModel = (RecordingModel)bundle;
            if (mRecordingModel.uid != mUser.uid)
            {
                mRecordingModel.uid = mUser.uid;
                mRecordingModel.nickname = mUser.nickname;
            }
            mRecordingModel.albumTitle = "";
            mRecordingModel.albumId = 0L;
            init();
            return;
        } else
        {
            showToast("\u672A\u627E\u5230\u5F55\u97F3\u6587\u4EF6");
            return;
        }
    }

    protected void onDestroy()
    {
        super.onDestroy();
    }

    protected void onPause()
    {
        super.onPause();
    }

    protected void onResume()
    {
        super.onResume();
    }

    public void showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView footerview)
    {
        mAlbumListView.setFooterDividersEnabled(false);
        mFooterViewLoading.setVisibility(0);
        if (footerview == com.ximalaya.ting.android.activity.BaseListActivity.FooterView.MORE)
        {
            mFooterViewLoading.setClickable(true);
            mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
            ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u70B9\u51FB\u8F7D\u5165\u66F4\u591A...");
            mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
        } else
        {
            if (footerview == com.ximalaya.ting.android.activity.BaseListActivity.FooterView.LOADING)
            {
                mFooterViewLoading.setClickable(false);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(0);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u52AA\u529B\u52A0\u8F7D\u4E2D...");
                return;
            }
            if (footerview == com.ximalaya.ting.android.activity.BaseListActivity.FooterView.NO_CONNECTION)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u7F51\u7EDC\u72B6\u6001\u4E0D\u4F73,\u70B9\u51FB\u91CD\u65B0\u8F7D\u5165");
                return;
            }
            if (footerview == com.ximalaya.ting.android.activity.BaseListActivity.FooterView.NO_DATA)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(0);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                ((TextView)mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
                return;
            }
            if (footerview == com.ximalaya.ting.android.activity.BaseListActivity.FooterView.HIDE_ALL)
            {
                mFooterViewLoading.setClickable(true);
                mFooterViewLoading.findViewById(0x7f0a073c).setVisibility(8);
                mFooterViewLoading.findViewById(0x7f0a073b).setVisibility(8);
                mFooterViewLoading.setVisibility(8);
                return;
            }
        }
    }

    public void startPhotoZoom(Uri uri)
    {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 640);
        intent.putExtra("outputY", 640);
        intent.putExtra("scale", true);
        if (mFromCamera)
        {
            uri = new File(mCachePath);
        } else
        {
            uri = getTempCoverFile();
        }
        intent.putExtra("output", Uri.fromFile(uri));
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", android.graphics.Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true);
        startActivityForResult(intent, 2803);
    }




/*
    static ImageView access$102(RecUploadFormAct recuploadformact, ImageView imageview)
    {
        recuploadformact.mCacheImageView = imageview;
        return imageview;
    }

*/


















/*
    static int access$2502(RecUploadFormAct recuploadformact, int i)
    {
        recuploadformact.mTotal = i;
        return i;
    }

*/




/*
    static int access$2708(RecUploadFormAct recuploadformact)
    {
        int i = recuploadformact.mPageId;
        recuploadformact.mPageId = i + 1;
        return i;
    }

*/






/*
    static boolean access$3102(RecUploadFormAct recuploadformact, boolean flag)
    {
        recuploadformact.mLoadingAlbum = flag;
        return flag;
    }

*/




/*
    static String access$3302(RecUploadFormAct recuploadformact, String s1)
    {
        recuploadformact.mShareContent = s1;
        return s1;
    }

*/





/*
    static boolean access$3602(RecUploadFormAct recuploadformact, boolean flag)
    {
        recuploadformact.mIsShareSina = flag;
        return flag;
    }

*/







/*
    static boolean access$4002(RecUploadFormAct recuploadformact, boolean flag)
    {
        recuploadformact.mIsShareTQQ = flag;
        return flag;
    }

*/





/*
    static boolean access$4302(RecUploadFormAct recuploadformact, boolean flag)
    {
        recuploadformact.mIsShareRenn = flag;
        return flag;
    }

*/





/*
    static long access$4802(RecUploadFormAct recuploadformact, long l1)
    {
        recuploadformact.mAlbumId = l1;
        return l1;
    }

*/

















}
