// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.model.record.BgSound;
import java.io.File;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            RecLocalBgAct

class j
    implements android.view.View.OnClickListener
{

    final RecLocalBgAct.a a;

    j(RecLocalBgAct.a a1)
    {
        a = a1;
        super();
    }

    public void onClick(View view)
    {
        RecLocalBgAct.c c;
        c = (RecLocalBgAct.c)view.getTag();
        break MISSING_BLOCK_LABEL_8;
        if (c != null && c.f >= 0 && c.f <= RecLocalBgAct.access$200(a.a).size())
        {
            view = (BgSound)RecLocalBgAct.access$200(a.a).get(c.f);
            if (view != null)
            {
                int i = RecLocalBgAct.access$700(a.a).indexOf(view);
                if (i >= 0)
                {
                    view = (BgSound)RecLocalBgAct.access$700(a.a).get(i);
                }
                if (!(new File(((BgSound) (view)).path)).exists())
                {
                    Toast.makeText(RecLocalBgAct.access$800(a.a), (new StringBuilder()).append("\u6587\u4EF6  ").append(((BgSound) (view)).title).append(" \u5728\u5B58\u50A8\u5361\u4E0A\u5DF2\u4E0D\u5B58\u5728\uFF0C\u65E0\u6CD5\u6DFB\u52A0\uFF01").toString(), 1).show();
                    RecLocalBgAct.access$200(a.a).remove(view);
                    RecLocalBgAct.access$400(a.a).notifyDataSetChanged();
                    return;
                }
                if (!RecLocalBgAct.access$900(a.a, view))
                {
                    c.e.setCompoundDrawablesWithIntrinsicBounds(0x7f020490, 0, 0, 0);
                    RecLocalBgAct.access$700(a.a).add(0, view);
                    RecLocalBgAct.access$1002(a.a, true);
                    return;
                }
                if (!((BgSound) (view)).candelete)
                {
                    a.a.showToast("\u7CFB\u7EDF\u9ED8\u8BA4\u80CC\u666F,\u65E0\u6CD5\u53D6\u6D88");
                    return;
                }
                if (!RecLocalBgAct.access$1100(a.a))
                {
                    a.a.showToast("\u4EB2~\u5DF2\u7ECF\u5728\u5F55\u97F3\u4E86\u54E6\uFF0C\u4E0D\u80FD\u53D6\u6D88\u5566");
                    return;
                } else
                {
                    c.e.setCompoundDrawablesWithIntrinsicBounds(0x7f02048c, 0, 0, 0);
                    RecLocalBgAct.access$700(a.a).remove(view);
                    RecLocalBgAct.access$1002(a.a, true);
                    return;
                }
            }
        }
        return;
    }
}
