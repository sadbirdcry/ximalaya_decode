// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.record.BgSound;
import com.ximalaya.ting.android.transaction.d.f;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            RecLocalBgAct, i, j, g

private class <init> extends BaseAdapter
{

    final RecLocalBgAct a;

    public int getCount()
    {
        if (RecLocalBgAct.access$200(a) == null)
        {
            return 0;
        } else
        {
            return RecLocalBgAct.access$200(a).size();
        }
    }

    public Object getItem(int k)
    {
        return RecLocalBgAct.access$200(a).get(k);
    }

    public long getItemId(int k)
    {
        return (long)k;
    }

    public View getView(int k, View view, ViewGroup viewgroup)
    {
label0:
        {
            viewgroup = view;
            if (view == null)
            {
                viewgroup = LayoutInflater.from(a.mActivity).inflate(0x7f0301a4, a.mListView, false);
                view = new <init>(a, null);
                view.a = (ImageView)viewgroup.findViewById(0x7f0a065e);
                view.b = (ImageView)viewgroup.findViewById(0x7f0a00f9);
                view.c = (TextView)viewgroup.findViewById(0x7f0a00e6);
                view.d = (TextView)viewgroup.findViewById(0x7f0a0660);
                view.e = (TextView)viewgroup.findViewById(0x7f0a065f);
                ((e) (view)).e.setTag(view);
                ((e) (view)).b.setTag(view);
                viewgroup.setTag(view);
                ((b) (view)).b.setOnClickListener(new i(this));
                ((b) (view)).e.setOnClickListener(new j(this));
            }
            view = (e)viewgroup.getTag();
            view.f = k;
            BgSound bgsound = (BgSound)RecLocalBgAct.access$200(a).get(k);
            if (bgsound != null)
            {
                ((a) (view)).c.setText(bgsound.title);
                ((c) (view)).d.setText((new StringBuilder()).append(bgsound.author).append("  ").append(bgsound.album_title).toString());
                if (RecLocalBgAct.access$900(a, bgsound))
                {
                    ((a) (view)).e.setCompoundDrawablesWithIntrinsicBounds(0x7f020490, 0, 0, 0);
                } else
                {
                    ((ds) (view)).e.setCompoundDrawablesWithIntrinsicBounds(0x7f02048c, 0, 0, 0);
                }
                if (!bgsound.equals(RecLocalBgAct.access$300(a).e()) || !RecLocalBgAct.access$300(a).a())
                {
                    break label0;
                }
                ((a) (view)).b.setImageResource(0x7f02048e);
            }
            return viewgroup;
        }
        ((b) (view)).b.setImageResource(0x7f02048f);
        return viewgroup;
    }

    private (RecLocalBgAct reclocalbgact)
    {
        a = reclocalbgact;
        super();
    }

    a(RecLocalBgAct reclocalbgact, g g)
    {
        this(reclocalbgact);
    }
}
