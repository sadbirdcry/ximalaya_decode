// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.model.record.BgSound;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            RecLocalBgAct, g

private class <init> extends MyAsyncTask
{

    final RecLocalBgAct a;

    protected transient List a(Void avoid[])
    {
        avoid = a.mActivity.getContentResolver().query(android.provider..Media.EXTERNAL_CONTENT_URI, RecLocalBgAct.access$1200(a), null, null, "title_key");
        if (avoid == null)
        {
            break MISSING_BLOCK_LABEL_233;
        }
        Object obj;
        try
        {
            obj = new ArrayList();
            do
            {
                if (!avoid.moveToNext())
                {
                    break;
                }
                String s = avoid.getString(avoid.getColumnIndexOrThrow("_data"));
                if (!TextUtils.isEmpty(s) && s.endsWith(".mp3"))
                {
                    BgSound bgsound = new BgSound();
                    bgsound.path = s;
                    bgsound.id = avoid.getLong(avoid.getColumnIndexOrThrow("_id"));
                    bgsound.title = avoid.getString(avoid.getColumnIndexOrThrow("title"));
                    bgsound.author = avoid.getString(avoid.getColumnIndexOrThrow("artist"));
                    bgsound.album_title = avoid.getString(avoid.getColumnIndexOrThrow("album"));
                    bgsound.duration = avoid.getInt(avoid.getColumnIndexOrThrow("duration"));
                    bgsound.duration = bgsound.duration / 1000F;
                    bgsound.type = 3;
                    ((List) (obj)).add(bgsound);
                }
            } while (true);
            break MISSING_BLOCK_LABEL_235;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
        finally
        {
            avoid.close();
            throw exception;
        }
        ((Exception) (obj)).printStackTrace();
        avoid.close();
        return null;
        avoid.close();
        return ((List) (obj));
    }

    protected void a(List list)
    {
        super.onPostExecute(list);
        if (list != null && list.size() > 0)
        {
            RecLocalBgAct.access$200(a).addAll(list);
            RecLocalBgAct.access$400(a).notifyDataSetChanged();
        }
        if (RecLocalBgAct.access$200(a).size() == 0)
        {
            a.showFooterView(com.ximalaya.ting.android.activity..FooterView.NO_DATA);
            ((TextView)a.mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u6CA1\u6709\u672C\u5730\u97F3\u4E50\u6587\u4EF6");
            return;
        } else
        {
            a.showFooterView(com.ximalaya.ting.android.activity..FooterView.HIDE_ALL);
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((List)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        a.showFooterView(com.ximalaya.ting.android.activity..FooterView.LOADING);
    }

    private w(RecLocalBgAct reclocalbgact)
    {
        a = reclocalbgact;
        super();
    }

    a(RecLocalBgAct reclocalbgact, g g)
    {
        this(reclocalbgact);
    }
}
