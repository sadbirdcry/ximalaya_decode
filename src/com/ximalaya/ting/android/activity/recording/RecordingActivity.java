// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import com.ximalaya.ting.android.activity.BaseFragmentActivity;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.record.RecordingFragment;

public class RecordingActivity extends BaseFragmentActivity
{

    private long mActivityId;
    private Fragment mFragment;

    public RecordingActivity()
    {
        mActivityId = -1L;
    }

    public void onBackPressed()
    {
        if (mFragment != null && (mFragment instanceof BaseFragment) && ((BaseFragment)mFragment).onBackPressed())
        {
            return;
        } else
        {
            super.onBackPressed();
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        if (getIntent() != null)
        {
            mActivityId = getIntent().getLongExtra("activity_id", -1L);
        }
        setContentView(0x7f0301ba);
        mFragment = getSupportFragmentManager().findFragmentById(0x7f0a06a6);
        if (mActivityId >= 0L)
        {
            ((RecordingFragment)mFragment).setActivityId(mActivityId);
        }
    }

    protected void onResume()
    {
        super.onResume();
    }
}
