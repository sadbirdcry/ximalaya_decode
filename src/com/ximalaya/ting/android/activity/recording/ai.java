// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.text.TextUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            RecUploadFormAct

class ai extends a
{

    final RecUploadFormAct a;

    ai(RecUploadFormAct recuploadformact)
    {
        a = recuploadformact;
        super();
    }

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, RecUploadFormAct.access$3200(a));
    }

    public void onFinish()
    {
        super.onFinish();
        RecUploadFormAct.access$3102(a, false);
    }

    public void onNetError(int i, String s)
    {
        RecUploadFormAct.access$2400(a, RecUploadFormAct.access$2300(a), s);
    }

    public void onStart()
    {
        super.onStart();
        a.showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.LOADING);
        a.mFooterViewLoading.setOnClickListener(null);
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            RecUploadFormAct.access$2400(a, RecUploadFormAct.access$2300(a), null);
            return;
        }
        Object obj;
        try
        {
            obj = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            ((Exception) (obj)).printStackTrace();
            obj = null;
        }
        if (obj == null || ((JSONObject) (obj)).getIntValue("ret") != 0)
        {
            RecUploadFormAct.access$2400(a, RecUploadFormAct.access$2300(a), s);
            return;
        }
        RecUploadFormAct.access$2502(a, ((JSONObject) (obj)).getIntValue("totalCount"));
        RecUploadFormAct.access$2600(a).setText((new StringBuilder()).append("\u9009\u62E9\u4E13\u8F91(").append(RecUploadFormAct.access$2500(a)).append(")\u4E2A").toString());
        obj = ((JSONObject) (obj)).getString("list");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            RecUploadFormAct.access$2400(a, RecUploadFormAct.access$2300(a), s);
            return;
        }
        try
        {
            s = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/album/AlbumModel);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = null;
        }
        if (s != null && s.size() > 0)
        {
            if (RecUploadFormAct.access$2700(a) == 1)
            {
                RecUploadFormAct.access$2800(a).clear();
            }
            RecUploadFormAct.access$2800(a).addAll(s);
            RecUploadFormAct.access$2900(a).notifyDataSetChanged();
            int _tmp = RecUploadFormAct.access$2708(a);
            a.showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.HIDE_ALL);
            a.mFooterViewLoading.setOnClickListener(null);
            return;
        }
        if (RecUploadFormAct.access$2500(a) == 0)
        {
            a.showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.HIDE_ALL);
            a.mFooterViewLoading.setOnClickListener(null);
            return;
        } else
        {
            a.showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.NO_CONNECTION);
            a.mFooterViewLoading.setOnClickListener(RecUploadFormAct.access$3000(a));
            return;
        }
    }
}
