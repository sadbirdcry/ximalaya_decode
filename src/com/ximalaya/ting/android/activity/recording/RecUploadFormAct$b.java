// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.app.Activity;
import android.app.ProgressDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.record.FormResult;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.MyVerificationCodeDialogFragment;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            RecUploadFormAct, aj, ak

class a extends MyAsyncTask
{

    final RecUploadFormAct a;
    private MyVerificationCodeDialogFragment b;
    private ProgressDialog c;

    protected transient FormResult a(View aview[])
    {
        FormResult formresult = new FormResult();
        View view = aview[0];
        RequestParams requestparams = new RequestParams();
        requestparams.put("title", (new StringBuilder()).append("").append(RecUploadFormAct.access$700(a).title).toString());
        requestparams.put("albumId", (new StringBuilder()).append("").append(RecUploadFormAct.access$700(a).albumId).toString());
        if (RecUploadFormAct.access$700(a).activityId > 0L)
        {
            requestparams.put("activityId", (new StringBuilder()).append("").append(RecUploadFormAct.access$700(a).activityId).toString());
        }
        if (RecUploadFormAct.access$5700(a) > 0L)
        {
            requestparams.put("activityId", (new StringBuilder()).append("").append(RecUploadFormAct.access$5700(a)).toString());
        }
        Object obj = "";
        if (RecUploadFormAct.access$3600(a))
        {
            obj = (new StringBuilder()).append("").append("tSina").toString();
        }
        aview = ((View []) (obj));
        if (RecUploadFormAct.access$4000(a))
        {
            aview = ((View []) (obj));
            if (!TextUtils.isEmpty(((CharSequence) (obj))))
            {
                aview = (new StringBuilder()).append(((String) (obj))).append(",").toString();
            }
            aview = (new StringBuilder()).append(aview).append("tQQ").toString();
        }
        obj = aview;
        if (RecUploadFormAct.access$4300(a))
        {
            obj = aview;
            if (!TextUtils.isEmpty(aview))
            {
                obj = (new StringBuilder()).append(aview).append(",").toString();
            }
            obj = (new StringBuilder()).append(((String) (obj))).append("renren").toString();
        }
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            requestparams.put("shareThirdpartyNames", ((String) (obj)));
            if (!TextUtils.isEmpty(RecUploadFormAct.access$3300(a)))
            {
                requestparams.put("shareContent", RecUploadFormAct.access$3300(a));
            }
        }
        if (!TextUtils.isEmpty(RecUploadFormAct.access$700(a).checkUUID))
        {
            requestparams.put("checkCode", RecUploadFormAct.access$700(a).checkCode);
            requestparams.put("checkUUID", RecUploadFormAct.access$700(a).checkUUID);
        }
        f.a().a("mobile/api1/upload/track_form", requestparams, DataCollectUtil.getDataFromView(view), new aj(this, formresult, view));
        return formresult;
    }

    protected void a(FormResult formresult)
    {
        if (RecUploadFormAct.access$600(a) == null || RecUploadFormAct.access$600(a).isFinishing())
        {
            return;
        }
        c.cancel();
        if (formresult == null)
        {
            Toast.makeText(a.getApplicationContext(), "\u4E0A\u4F20\u9519\u8BEF\uFF0C\u8BF7\u91CD\u8BD5", 0).show();
            return;
        }
        if (formresult.ret == 0)
        {
            RecUploadFormAct.access$700(a).checkUUID = null;
            RecUploadFormAct.access$700(a).checkCode = null;
            RecUploadFormAct.access$700(a).formId = formresult.formId;
            RecUploadFormAct.access$5800(a);
            return;
        }
        if (formresult.ret == 211)
        {
            RecUploadFormAct.access$700(a).checkUUID = formresult.checkUUID;
            if (b == null)
            {
                b = new MyVerificationCodeDialogFragment("from_sound", formresult.checkCodeUrl, new ak(this));
            }
            b.show(a.getSupportFragmentManager(), "from_sound", formresult.checkCodeUrl);
            return;
        }
        if (formresult.ret == 212)
        {
            RecUploadFormAct.access$700(a).checkUUID = null;
            RecUploadFormAct.access$700(a).checkCode = null;
            Toast.makeText(a.getApplicationContext(), "\u9A8C\u8BC1\u7801\u9519\u8BEF\uFF01", 1).show();
            return;
        }
        if (formresult.ret == 4)
        {
            RecUploadFormAct.access$5900(a);
            return;
        } else
        {
            RecUploadFormAct.access$700(a).checkUUID = null;
            RecUploadFormAct.access$700(a).checkCode = null;
            Toast.makeText(a.getApplicationContext(), formresult.msg, 0).show();
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((View[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((FormResult)obj);
    }

    protected void onPreExecute()
    {
        a.loginInfoModel = UserInfoMannage.getInstance().getUser();
        if (a.loginInfoModel == null)
        {
            return;
        } else
        {
            c = new MyProgressDialog(RecUploadFormAct.access$600(a));
            c.setMessage("\u6B63\u5728\u51C6\u5907\u4E0A\u4F20...");
            c.setCancelable(false);
            c.setCanceledOnTouchOutside(false);
            c.show();
            return;
        }
    }

    (RecUploadFormAct recuploadformact)
    {
        a = recuploadformact;
        super();
    }
}
