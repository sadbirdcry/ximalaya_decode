// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.ImageManager2;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            RecUploadFormAct, k

private class <init> extends BaseAdapter
{

    final RecUploadFormAct a;

    public int getCount()
    {
        return RecUploadFormAct.access$2800(a).size();
    }

    public Object getItem(int i)
    {
        return RecUploadFormAct.access$2800(a).get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        viewgroup = view;
        if (view == null)
        {
            viewgroup = LayoutInflater.from(RecUploadFormAct.access$5400(a)).inflate(0x7f0301aa, RecUploadFormAct.access$3200(a), false);
            view = new <init>(a, null);
            view.a = (ImageView)viewgroup.findViewById(0x7f0a0023);
            view.b = (TextView)viewgroup.findViewById(0x7f0a0083);
            view.c = (TextView)viewgroup.findViewById(0x7f0a066b);
            viewgroup.setTag(view);
        }
        view = (c)viewgroup.getTag();
        AlbumModel albummodel = (AlbumModel)RecUploadFormAct.access$2800(a).get(i);
        ImageManager2.from(RecUploadFormAct.access$5600(a)).displayImage(((a) (view)).a, albummodel.coverSmall, 0x7f0202de);
        ((a) (view)).b.setText(albummodel.title);
        ((b) (view)).c.setText((new StringBuilder()).append(albummodel.tracks).append("\u6761\u58F0\u97F3").toString());
        if (albummodel.isPublic)
        {
            ((c) (view)).b.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            return viewgroup;
        } else
        {
            ((b) (view)).b.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f020034, 0);
            return viewgroup;
        }
    }

    private (RecUploadFormAct recuploadformact)
    {
        a = recuploadformact;
        super();
    }

    a(RecUploadFormAct recuploadformact, k k)
    {
        this(recuploadformact);
    }
}
