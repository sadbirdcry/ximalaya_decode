// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.activity.BaseListActivity;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.record.BgSound;
import com.ximalaya.ting.android.transaction.d.f;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Session;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            g, h, i, j

public class RecLocalBgAct extends BaseListActivity
{
    private class a extends BaseAdapter
    {

        final RecLocalBgAct a;

        public int getCount()
        {
            if (a.mSounds == null)
            {
                return 0;
            } else
            {
                return a.mSounds.size();
            }
        }

        public Object getItem(int k)
        {
            return a.mSounds.get(k);
        }

        public long getItemId(int k)
        {
            return (long)k;
        }

        public View getView(int k, View view, ViewGroup viewgroup)
        {
label0:
            {
                viewgroup = view;
                if (view == null)
                {
                    viewgroup = LayoutInflater.from(a.mActivity).inflate(0x7f0301a4, a.mListView, false);
                    view = a. new c(null);
                    view.a = (ImageView)viewgroup.findViewById(0x7f0a065e);
                    view.b = (ImageView)viewgroup.findViewById(0x7f0a00f9);
                    view.c = (TextView)viewgroup.findViewById(0x7f0a00e6);
                    view.d = (TextView)viewgroup.findViewById(0x7f0a0660);
                    view.e = (TextView)viewgroup.findViewById(0x7f0a065f);
                    ((c) (view)).e.setTag(view);
                    ((c) (view)).b.setTag(view);
                    viewgroup.setTag(view);
                    ((c) (view)).b.setOnClickListener(new i(this));
                    ((c) (view)).e.setOnClickListener(new j(this));
                }
                view = (c)viewgroup.getTag();
                view.f = k;
                BgSound bgsound = (BgSound)a.mSounds.get(k);
                if (bgsound != null)
                {
                    ((c) (view)).c.setText(bgsound.title);
                    ((c) (view)).d.setText((new StringBuilder()).append(bgsound.author).append("  ").append(bgsound.album_title).toString());
                    if (a.isSelected(bgsound))
                    {
                        ((c) (view)).e.setCompoundDrawablesWithIntrinsicBounds(0x7f020490, 0, 0, 0);
                    } else
                    {
                        ((c) (view)).e.setCompoundDrawablesWithIntrinsicBounds(0x7f02048c, 0, 0, 0);
                    }
                    if (!bgsound.equals(a.mPlayer.e()) || !a.mPlayer.a())
                    {
                        break label0;
                    }
                    ((c) (view)).b.setImageResource(0x7f02048e);
                }
                return viewgroup;
            }
            ((c) (view)).b.setImageResource(0x7f02048f);
            return viewgroup;
        }

        private a()
        {
            a = RecLocalBgAct.this;
            super();
        }

        a(g g1)
        {
            this();
        }
    }

    private class b extends MyAsyncTask
    {

        final RecLocalBgAct a;

        protected transient List a(Void avoid[])
        {
            avoid = a.mActivity.getContentResolver().query(android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, a.mProjection, null, null, "title_key");
            if (avoid == null)
            {
                break MISSING_BLOCK_LABEL_233;
            }
            Object obj;
            try
            {
                obj = new ArrayList();
                do
                {
                    if (!avoid.moveToNext())
                    {
                        break;
                    }
                    String s = avoid.getString(avoid.getColumnIndexOrThrow("_data"));
                    if (!TextUtils.isEmpty(s) && s.endsWith(".mp3"))
                    {
                        BgSound bgsound = new BgSound();
                        bgsound.path = s;
                        bgsound.id = avoid.getLong(avoid.getColumnIndexOrThrow("_id"));
                        bgsound.title = avoid.getString(avoid.getColumnIndexOrThrow("title"));
                        bgsound.author = avoid.getString(avoid.getColumnIndexOrThrow("artist"));
                        bgsound.album_title = avoid.getString(avoid.getColumnIndexOrThrow("album"));
                        bgsound.duration = avoid.getInt(avoid.getColumnIndexOrThrow("duration"));
                        bgsound.duration = bgsound.duration / 1000F;
                        bgsound.type = 3;
                        ((List) (obj)).add(bgsound);
                    }
                } while (true);
                break MISSING_BLOCK_LABEL_235;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
            finally
            {
                avoid.close();
                throw exception;
            }
            ((Exception) (obj)).printStackTrace();
            avoid.close();
            return null;
            avoid.close();
            return ((List) (obj));
        }

        protected void a(List list)
        {
            super.onPostExecute(list);
            if (list != null && list.size() > 0)
            {
                a.mSounds.addAll(list);
                a.mAdapter.notifyDataSetChanged();
            }
            if (a.mSounds.size() == 0)
            {
                a.showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.NO_DATA);
                ((TextView)a.mFooterViewLoading.findViewById(0x7f0a073b)).setText("\u6CA1\u6709\u672C\u5730\u97F3\u4E50\u6587\u4EF6");
                return;
            } else
            {
                a.showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.HIDE_ALL);
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((List)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            a.showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.LOADING);
        }

        private b()
        {
            a = RecLocalBgAct.this;
            super();
        }

        b(g g1)
        {
            this();
        }
    }

    private class c
    {

        public ImageView a;
        public ImageView b;
        public TextView c;
        public TextView d;
        public TextView e;
        public int f;
        final RecLocalBgAct g;

        private c()
        {
            g = RecLocalBgAct.this;
            super();
        }

        c(g g1)
        {
            this();
        }
    }


    private Activity mAct;
    private a mAdapter;
    private boolean mCanDelete;
    private BgSound mCurrSound;
    private b mLoadLocalSounds;
    private f mPlayer;
    private String mProjection[] = {
        "_id", "title", "duration", "_data", "album", "artist"
    };
    private boolean mSelectChanged;
    private List mSelected;
    private SharedPreferencesUtil mSharedPreferences;
    private List mSounds;

    public RecLocalBgAct()
    {
        mSounds = new ArrayList();
        mSelectChanged = false;
        mSelected = new ArrayList();
        mCanDelete = true;
    }

    private void initUi()
    {
        mListView = (ListView)findViewById(0x7f0a00dc);
        initCommon();
        setTitleText("\u672C\u5730\u97F3\u4E50");
        mAdapter = new a(null);
        mListView.setAdapter(mAdapter);
        mPlayer = new f(mActivity);
        mListView.setOnItemClickListener(new g(this));
        mPlayer.a(new h(this));
    }

    private boolean isSelected(BgSound bgsound)
    {
        return mSelected.contains(bgsound);
    }

    private void loadLocalSounds()
    {
        if (mLoadLocalSounds == null || mLoadLocalSounds.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mLoadLocalSounds = new b(null);
            mLoadLocalSounds.myexec(new Void[0]);
        }
    }

    private void loadSelectedSounds()
    {
        Object obj = mSharedPreferences.getString("BG_SOUND_CACHE");
        if (!TextUtils.isEmpty(((CharSequence) (obj)))) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if ((obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/record/BgSound)) == null) goto _L1; else goto _L3
_L3:
        if (((List) (obj)).size() <= 0) goto _L1; else goto _L4
_L4:
        mSelected.addAll(((java.util.Collection) (obj)));
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
        return;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030026);
        mAct = this;
        mSharedPreferences = SharedPreferencesUtil.getInstance(mActivity);
        boolean flag;
        if (!mSharedPreferences.getBoolean("REC_RECORDING"))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        mCanDelete = flag;
        initUi();
        loadLocalSounds();
        loadSelectedSounds();
    }

    protected void onDestroy()
    {
        super.onDestroy();
        if (mPlayer != null)
        {
            mPlayer.i();
            mPlayer.j();
        }
    }

    protected void onPause()
    {
        super.onPause();
        if (mSelectChanged)
        {
            Session.getSession().put("BG_SOUND_CACHE_CHANGE", Boolean.valueOf(true));
            mSharedPreferences.saveString("BG_SOUND_CACHE", JSON.toJSONString(mSelected));
        }
    }


/*
    static boolean access$1002(RecLocalBgAct reclocalbgact, boolean flag)
    {
        reclocalbgact.mSelectChanged = flag;
        return flag;
    }

*/








/*
    static BgSound access$602(RecLocalBgAct reclocalbgact, BgSound bgsound)
    {
        reclocalbgact.mCurrSound = bgsound;
        return bgsound;
    }

*/



}
