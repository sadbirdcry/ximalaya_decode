// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.recording;

import android.text.TextUtils;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.h;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.activity.recording:
//            CreateAlbumAct

class f extends h
{

    final CreateAlbumAct.a a;

    f(CreateAlbumAct.a a1)
    {
        a = a1;
        super();
    }

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, CreateAlbumAct.access$1300(a.a));
    }

    public void onFinish()
    {
    }

    public void onNetError(int i, String s)
    {
    }

    public void onStart()
    {
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        }
        try
        {
            s = JSONObject.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return;
        }
        if (s.getIntValue("ret") != 0)
        {
            CreateAlbumAct.access$802(a.a, s.getString("msg"));
            return;
        } else
        {
            CreateAlbumAct.access$1102(a.a, new AlbumModel());
            CreateAlbumAct.access$1100(a.a).title = s.getString("title");
            CreateAlbumAct.access$1100(a.a).uid = s.getLongValue("uid");
            CreateAlbumAct.access$1100(a.a).albumId = s.getLongValue("albumId");
            CreateAlbumAct.access$1100(a.a).coverSmall = s.getString("coverSmall");
            CreateAlbumAct.access$1100(a.a).categoryId = s.getLongValue("categoryId");
            CreateAlbumAct.access$1100(a.a).isPublic = s.getBooleanValue("isPublic");
            return;
        }
    }
}
