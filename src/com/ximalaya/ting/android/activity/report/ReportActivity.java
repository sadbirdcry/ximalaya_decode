// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.report;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseListActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.zone.ZoneConstants;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.report.ReportModel;
import com.ximalaya.ting.android.model.zone.PostReportInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package com.ximalaya.ting.android.activity.report:
//            a, b, c, d

public class ReportActivity extends BaseListActivity
{
    class a extends MyAsyncTask
    {

        final ReportActivity a;

        protected transient Integer a(Void avoid[])
        {
            RequestParams requestparams;
            avoid = null;
            requestparams = new RequestParams();
            if (a.mReportType != 3) goto _L2; else goto _L1
_L1:
            String s = ZoneConstants.URL_ZONE_REPORT;
            requestparams.put("title", a.mCheckText);
            requestparams.put("reporterId", (new StringBuilder()).append(a.mPostInfo.getReporterId()).append("").toString());
            requestparams.put("reportedId", (new StringBuilder()).append(a.mPostInfo.getReportedId()).append("").toString());
            requestparams.put("type", (new StringBuilder()).append(a.mPostInfo.getType()).append("").toString());
            requestparams.put("zoneId", (new StringBuilder()).append(a.mPostInfo.getZoneId()).append("").toString());
            requestparams.put("postId", (new StringBuilder()).append(a.mPostInfo.getPostId()).append("").toString());
            avoid = s;
            if (a.mPostInfo.getType() == 2)
            {
                requestparams.put("commentId", (new StringBuilder()).append(a.mPostInfo.getCommentId()).append("").toString());
                avoid = s;
            }
_L11:
            if (a.mReportType != 3) goto _L4; else goto _L3
_L3:
            avoid = f.a().b(avoid, requestparams, a.mSendReport, a.mSendReport);
_L9:
            if (Utilities.isBlank(avoid))
            {
                return Integer.valueOf(1);
            }
              goto _L5
_L2:
            LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
            requestparams.put("uid", (new StringBuilder()).append("").append(logininfomodel.uid).toString());
            requestparams.put("token", logininfomodel.token);
            if (a.mReportType != 0) goto _L7; else goto _L6
_L6:
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/report/track/create").toString();
            requestparams.put("trackId", (new StringBuilder()).append("").append(a.mTrackId).toString());
_L8:
            requestparams.put("contentId", (new StringBuilder()).append("").append(a.mCheckedId).toString());
            continue; /* Loop/switch isn't completed */
_L7:
            if (a.mReportType == 1)
            {
                avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/report/track/create").toString();
                requestparams.put("trackId", (new StringBuilder()).append("").append(a.mTrackId).toString());
                requestparams.put("commentId", (new StringBuilder()).append("").append(a.mCommentId).toString());
            } else
            if (a.mReportType == 2)
            {
                avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/report/album/create").toString();
                requestparams.put("albumId", (new StringBuilder()).append("").append(a.mAlbumId).toString());
            }
            if (true) goto _L8; else goto _L4
_L4:
            try
            {
                avoid = f.a().a(avoid, requestparams, a.mSendReport, a.mSendReport);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
                return Integer.valueOf(2);
            }
            if (true) goto _L9; else goto _L5
_L5:
            if (JSON.parseObject(avoid).getIntValue("ret") != 0)
            {
                return Integer.valueOf(4);
            }
            return Integer.valueOf(3);
            if (true) goto _L11; else goto _L10
_L10:
        }

        protected void a(Integer integer)
        {
            if (a.mProgressDialog != null)
            {
                a.mProgressDialog.dismiss();
            }
            switch (integer.intValue())
            {
            default:
                return;

            case 2: // '\002'
                a.showToast("\u89E3\u6790\u6570\u636E\u5931\u8D25");
                return;

            case 1: // '\001'
                a.showToast("\u7F51\u7EDC\u9519\u8BEF");
                return;

            case 4: // '\004'
                a.showToast("\u63D0\u4EA4\u5931\u8D25");
                return;

            case 3: // '\003'
                a.showToast("\u63D0\u4EA4\u6210\u529F\uFF0C\u8C22\u8C22\u4F60\u7684\u53CD\u9988");
                a.topTextView.postDelayed(new d(this), 1000L);
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((Integer)obj);
        }

        protected void onPreExecute()
        {
            if (a.mProgressDialog == null)
            {
                a.mProgressDialog = new MyProgressDialog(a.mContext);
            }
            a.mProgressDialog.setCancelable(false);
            a.mProgressDialog.setCanceledOnTouchOutside(false);
            a.mProgressDialog.setTitle("\u8BF7\u7A0D\u5019");
            a.mProgressDialog.setMessage("\u6B63\u5728\u63D0\u4EA4\u60A8\u7684\u53CD\u9988...");
            a.mProgressDialog.show();
        }

        a()
        {
            a = ReportActivity.this;
            super();
        }
    }


    public static final String EXTRA_ALBUM_ID = "album_id";
    public static final String EXTRA_COMMENT_ID = "comment_id";
    public static final String EXTRA_POST_DATA = "post_data";
    public static final String EXTRA_REPORT_TYPE = "report_type";
    public static final String EXTRA_TRACK_ID = "track_id";
    private static final int MODELS[] = {
        0x7f09007e, 0x7f09007f, 0x7f090080, 0x7f090081
    };
    public static final int TYPE_REPORT_ALBUM = 2;
    public static final int TYPE_REPORT_COMMENT = 1;
    public static final int TYPE_REPORT_POST = 3;
    public static final int TYPE_REPORT_SOUND = 0;
    private BaseAdapter mAdapter;
    private long mAlbumId;
    private String mCheckText;
    private int mCheckedId;
    private long mCommentId;
    private ArrayList mContents;
    private Activity mContext;
    private a mCreateReportTask;
    private PostReportInfo mPostInfo;
    private ProgressDialog mProgressDialog;
    private ArrayList mReportModel;
    private int mReportType;
    private ImageView mSendReport;
    private long mTrackId;

    public ReportActivity()
    {
        mReportType = 0;
        mTrackId = 0L;
        mCommentId = 0L;
        mAlbumId = 0L;
        mReportModel = new ArrayList();
        mContents = new ArrayList(mReportModel.size());
        mCheckedId = -1;
    }

    private boolean checkEmailFormat(String s)
    {
        return Pattern.compile("^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$").matcher(s).matches();
    }

    private void checkLogin()
    {
    }

    private boolean checkMobilephoneFormat(String s)
    {
        return Pattern.compile("1[3|4|5|8][0-9][0-9]{8}").matcher(s).matches();
    }

    private boolean checkTelphoneFormat(String s)
    {
        return Pattern.compile("0\\d{2,3}-\\d{7,8}").matcher(s).matches();
    }

    private boolean checkTelphoneFormat2(String s)
    {
        return Pattern.compile("0\\d{2,3}\\d{7,8}").matcher(s).matches();
    }

    private List getStringArray()
    {
        mContents.clear();
        for (Iterator iterator = mReportModel.iterator(); iterator.hasNext(); mContents.add(((ReportModel)iterator.next()).title)) { }
        return mContents;
    }

    private void initData()
    {
        Intent intent = getIntent();
        mReportType = intent.getIntExtra("report_type", 0);
        mAlbumId = intent.getLongExtra("album_id", 0L);
        mTrackId = intent.getLongExtra("track_id", 0L);
        mCommentId = intent.getLongExtra("comment_id", 0L);
        mPostInfo = (PostReportInfo)intent.getSerializableExtra("post_data");
        loadReportProperty();
    }

    private void initUI()
    {
        String s;
        mListView = (ListView)findViewById(0x7f0a0025);
        mSendReport = (ImageView)findViewById(0x7f0a0710);
        initCommon();
        ((android.widget.RelativeLayout.LayoutParams)mSendReport.getLayoutParams()).setMargins(0, 0, ToolUtil.dp2px(mContext, 5F), 0);
        mSendReport.setVisibility(0);
        mSendReport.setImageResource(0x7f0200c9);
        mAdapter = new ArrayAdapter(mContext, 0x109000f, mContents);
        mListView.setAdapter(mAdapter);
        mListView.setChoiceMode(1);
        s = "\u53CD\u9988\u4E0E\u4E3E\u62A5";
        mReportType;
        JVM INSTR tableswitch 0 3: default 152
    //                   0 223
    //                   1 230
    //                   2 237
    //                   3 244;
           goto _L1 _L2 _L3 _L4 _L5
_L1:
        topTextView.setText(s);
        if (mListView != null)
        {
            ((PullToRefreshListView)mListView).setOnRefreshListener(new com.ximalaya.ting.android.activity.report.a(this));
            mListView.setOnItemClickListener(new b(this));
        }
        if (mSendReport != null)
        {
            mSendReport.setOnClickListener(new c(this));
        }
        return;
_L2:
        s = "\u4E3E\u62A5\u58F0\u97F3";
        continue; /* Loop/switch isn't completed */
_L3:
        s = "\u4E3E\u62A5\u8BC4\u8BBA";
        continue; /* Loop/switch isn't completed */
_L4:
        s = "\u4E3E\u62A5\u4E13\u8F91";
        continue; /* Loop/switch isn't completed */
_L5:
        s = "\u4E3E\u62A5\u5E16\u5B50";
        if (true) goto _L1; else goto _L6
_L6:
    }

    private void loadReportProperty()
    {
        try
        {
            String s = getResources().getString(MODELS[mReportType]);
            mReportModel.clear();
            mReportModel.addAll(JSON.parseArray(s, com/ximalaya/ting/android/model/report/ReportModel));
            getStringArray();
            if (mAdapter != null)
            {
                mAdapter.notifyDataSetChanged();
            }
            showFooterView(com.ximalaya.ting.android.activity.BaseListActivity.FooterView.HIDE_ALL);
            if (mListView != null)
            {
                ((PullToRefreshListView)mListView).onRefreshComplete();
            }
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    private void sendReport()
    {
        if (mCreateReportTask == null || mCreateReportTask.getStatus() == android.os.AsyncTask.Status.FINISHED)
        {
            mCreateReportTask = new a();
            mCreateReportTask.myexec(new Void[0]);
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        checkLogin();
        setContentView(0x7f030029);
        mContext = this;
        initData();
        initUI();
    }

    protected void onResume()
    {
        super.onResume();
    }









/*
    static int access$202(ReportActivity reportactivity, int i)
    {
        reportactivity.mCheckedId = i;
        return i;
    }

*/



/*
    static String access$302(ReportActivity reportactivity, String s)
    {
        reportactivity.mCheckText = s;
        return s;
    }

*/




/*
    static ProgressDialog access$502(ReportActivity reportactivity, ProgressDialog progressdialog)
    {
        reportactivity.mProgressDialog = progressdialog;
        return progressdialog;
    }

*/




}
