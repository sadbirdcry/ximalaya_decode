// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.report;

import android.app.ProgressDialog;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.zone.ZoneConstants;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.zone.PostReportInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.activity.report:
//            ReportActivity, d

class a extends MyAsyncTask
{

    final ReportActivity a;

    protected transient Integer a(Void avoid[])
    {
        RequestParams requestparams;
        avoid = null;
        requestparams = new RequestParams();
        if (ReportActivity.access$700(a) != 3) goto _L2; else goto _L1
_L1:
        String s = ZoneConstants.URL_ZONE_REPORT;
        requestparams.put("title", ReportActivity.access$300(a));
        requestparams.put("reporterId", (new StringBuilder()).append(ReportActivity.access$800(a).getReporterId()).append("").toString());
        requestparams.put("reportedId", (new StringBuilder()).append(ReportActivity.access$800(a).getReportedId()).append("").toString());
        requestparams.put("type", (new StringBuilder()).append(ReportActivity.access$800(a).getType()).append("").toString());
        requestparams.put("zoneId", (new StringBuilder()).append(ReportActivity.access$800(a).getZoneId()).append("").toString());
        requestparams.put("postId", (new StringBuilder()).append(ReportActivity.access$800(a).getPostId()).append("").toString());
        avoid = s;
        if (ReportActivity.access$800(a).getType() == 2)
        {
            requestparams.put("commentId", (new StringBuilder()).append(ReportActivity.access$800(a).getCommentId()).append("").toString());
            avoid = s;
        }
_L11:
        if (ReportActivity.access$700(a) != 3) goto _L4; else goto _L3
_L3:
        avoid = f.a().b(avoid, requestparams, ReportActivity.access$1200(a), ReportActivity.access$1200(a));
_L9:
        if (Utilities.isBlank(avoid))
        {
            return Integer.valueOf(1);
        }
          goto _L5
_L2:
        LoginInfoModel logininfomodel = UserInfoMannage.getInstance().getUser();
        requestparams.put("uid", (new StringBuilder()).append("").append(logininfomodel.uid).toString());
        requestparams.put("token", logininfomodel.token);
        if (ReportActivity.access$700(a) != 0) goto _L7; else goto _L6
_L6:
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/report/track/create").toString();
        requestparams.put("trackId", (new StringBuilder()).append("").append(ReportActivity.access$900(a)).toString());
_L8:
        requestparams.put("contentId", (new StringBuilder()).append("").append(ReportActivity.access$200(a)).toString());
        continue; /* Loop/switch isn't completed */
_L7:
        if (ReportActivity.access$700(a) == 1)
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/report/track/create").toString();
            requestparams.put("trackId", (new StringBuilder()).append("").append(ReportActivity.access$900(a)).toString());
            requestparams.put("commentId", (new StringBuilder()).append("").append(ReportActivity.access$1000(a)).toString());
        } else
        if (ReportActivity.access$700(a) == 2)
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/report/album/create").toString();
            requestparams.put("albumId", (new StringBuilder()).append("").append(ReportActivity.access$1100(a)).toString());
        }
        if (true) goto _L8; else goto _L4
_L4:
        try
        {
            avoid = f.a().a(avoid, requestparams, ReportActivity.access$1200(a), ReportActivity.access$1200(a));
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            avoid.printStackTrace();
            return Integer.valueOf(2);
        }
        if (true) goto _L9; else goto _L5
_L5:
        if (JSON.parseObject(avoid).getIntValue("ret") != 0)
        {
            return Integer.valueOf(4);
        }
        return Integer.valueOf(3);
        if (true) goto _L11; else goto _L10
_L10:
    }

    protected void a(Integer integer)
    {
        if (ReportActivity.access$500(a) != null)
        {
            ReportActivity.access$500(a).dismiss();
        }
        switch (integer.intValue())
        {
        default:
            return;

        case 2: // '\002'
            a.showToast("\u89E3\u6790\u6570\u636E\u5931\u8D25");
            return;

        case 1: // '\001'
            a.showToast("\u7F51\u7EDC\u9519\u8BEF");
            return;

        case 4: // '\004'
            a.showToast("\u63D0\u4EA4\u5931\u8D25");
            return;

        case 3: // '\003'
            a.showToast("\u63D0\u4EA4\u6210\u529F\uFF0C\u8C22\u8C22\u4F60\u7684\u53CD\u9988");
            a.topTextView.postDelayed(new d(this), 1000L);
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((Integer)obj);
    }

    protected void onPreExecute()
    {
        if (ReportActivity.access$500(a) == null)
        {
            ReportActivity.access$502(a, new MyProgressDialog(ReportActivity.access$600(a)));
        }
        ReportActivity.access$500(a).setCancelable(false);
        ReportActivity.access$500(a).setCanceledOnTouchOutside(false);
        ReportActivity.access$500(a).setTitle("\u8BF7\u7A0D\u5019");
        ReportActivity.access$500(a).setMessage("\u6B63\u5728\u63D0\u4EA4\u60A8\u7684\u53CD\u9988...");
        ReportActivity.access$500(a).show();
    }

    log(ReportActivity reportactivity)
    {
        a = reportactivity;
        super();
    }
}
