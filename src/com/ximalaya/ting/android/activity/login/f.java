// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            RegisterActivity

class f
    implements android.view.View.OnClickListener
{

    final RegisterActivity a;

    f(RegisterActivity registeractivity)
    {
        a = registeractivity;
        super();
    }

    public void onClick(View view)
    {
        if (ToolUtil.isConnectToNetwork(a))
        {
            if (a.type == 1)
            {
                if (TextUtils.isEmpty(a.edText1.getText()))
                {
                    a.dialog("\u8BF7\u8F93\u5165\u90AE\u7BB1\u5730\u5740\uFF01");
                    return;
                }
                if (TextUtils.isEmpty(a.edText2.getText()))
                {
                    a.dialog("\u8BF7\u8F93\u5165\u6635\u79F0\uFF01");
                    return;
                }
                if (TextUtils.isEmpty(a.edText3.getText()))
                {
                    a.dialog("\u8BF7\u8F93\u5165\u5BC6\u7801!");
                    return;
                }
                if (a.edText2.getText().toString().trim().length() > 20)
                {
                    a.dialog(" \u6635\u79F0\u4E0D\u80FD\u5927\u4E8E20\u4E2A\u5B57\u7B26\uFF01");
                    return;
                }
                if (!ToolUtil.verifiEmail(a.edText1.getText().toString().trim()))
                {
                    a.dialog(" \u90AE\u7BB1\u683C\u5F0F\u8F93\u5165\u6709\u8BEF\uFF01");
                    return;
                }
            } else
            {
                if (TextUtils.isEmpty(a.edText1.getText()))
                {
                    a.dialog("\u8BF7\u8F93\u5165\u624B\u673A\u53F7\uFF01");
                    return;
                }
                if (TextUtils.isEmpty(a.edText2.getText()))
                {
                    a.dialog("\u8BF7\u8F93\u5165\u6635\u79F0\uFF01");
                    return;
                }
                if (TextUtils.isEmpty(a.edText3.getText()))
                {
                    a.dialog("\u8BF7\u8F93\u5165\u5BC6\u7801!");
                    return;
                }
                if (a.edText2.getText().toString().trim().length() > 20)
                {
                    a.dialog(" \u6635\u79F0\u4E0D\u80FD\u5927\u4E8E20\u4E2A\u5B57\u7B26\uFF01");
                    return;
                }
                if (!ToolUtil.verifiPhone(a.edText1.getText().toString().trim()))
                {
                    a.dialog(" \u624B\u673A\u53F7\u7801\u8F93\u5165\u6709\u8BEF\uFF01");
                    return;
                }
            }
            if (a.edText3.getText().toString().trim().length() < 6 || a.edText3.getText().toString().trim().length() > 16)
            {
                a.dialog(" \u5BC6\u7801\u957F\u5EA6\u4E0D\u57286\u523016\u4E2A\u5B57\u7B26\u5185\uFF01");
                return;
            }
            if (a.type == 0)
            {
                ToolUtil.onEvent(a, "login_phone");
            } else
            {
                ToolUtil.onEvent(a, "login_Mailbox");
            }
            if (a.type == 1)
            {
                (new RegisterActivity.a(a)).myexec(new Object[] {
                    a.btnSbm
                });
                return;
            } else
            {
                (new RegisterActivity.RegisterTask(a)).myexec(new Void[0]);
                return;
            }
        } else
        {
            Toast.makeText(a, a.getString(0x7f0900a0), 1).show();
            return;
        }
    }
}
