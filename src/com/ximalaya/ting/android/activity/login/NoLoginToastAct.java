// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.BaseActivity;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            a

public class NoLoginToastAct extends BaseActivity
{
    private class a extends WebViewClient
    {

        final NoLoginToastAct a;

        public void onPageFinished(WebView webview, String s)
        {
            super.onPageFinished(webview, s);
            a.loadBar.setVisibility(4);
            a.login.setVisibility(0);
        }

        public void onPageStarted(WebView webview, String s, Bitmap bitmap)
        {
            super.onPageStarted(webview, s, bitmap);
        }

        public boolean shouldOverrideUrlLoading(WebView webview, String s)
        {
            return super.shouldOverrideUrlLoading(webview, s);
        }

        private a()
        {
            a = NoLoginToastAct.this;
            super();
        }

        a(com.ximalaya.ting.android.activity.login.a a1)
        {
            this();
        }
    }


    private ProgressBar loadBar;
    private Button login;
    private WebView mWebView;
    private String url;

    public NoLoginToastAct()
    {
        url = (new StringBuilder()).append(com.ximalaya.ting.android.a.v).append("third/android/customhelper2.html").toString();
    }

    private void initUI()
    {
        initCommon();
        setTitleText("\u5B9A\u5236\u542C");
        loadBar = (ProgressBar)findViewById(0x7f0a0446);
        login = (Button)findViewById(0x7f0a00bc);
        login.setOnClickListener(new com.ximalaya.ting.android.activity.login.a(this));
        setUpWebView();
    }

    private void setUpWebView()
    {
        mWebView = (WebView)findViewById(0x7f0a0445);
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setCacheMode(2);
        mWebView.setWebViewClient(new a(null));
        if (!url.contains("?"))
        {
            url = (new StringBuilder()).append(url).append("?version=").append(((MyApplication)(MyApplication)getApplication()).m()).toString();
        }
        mWebView.loadUrl(url);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03017c);
        initUI();
    }

    public void relayout()
    {
    }


}
