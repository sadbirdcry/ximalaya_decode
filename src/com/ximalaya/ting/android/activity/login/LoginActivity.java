// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.KeyEvent;
import com.ximalaya.ting.android.activity.BaseFragmentActivity;
import com.ximalaya.ting.android.fragment.login.LoginFragment;

public class LoginActivity extends BaseFragmentActivity
{

    private Fragment mFragment;

    public LoginActivity()
    {
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        super.onActivityResult(i, j, intent);
        mFragment.onActivityResult(i, j, intent);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03001b);
        mFragment = new LoginFragment();
        bundle = getSupportFragmentManager().beginTransaction();
        bundle.replace(0x7f0a00bc, mFragment);
        bundle.commit();
        Intent intent = getIntent();
        Bundle bundle1 = null;
        bundle = bundle1;
        if (intent != null)
        {
            bundle = bundle1;
            if (intent.hasExtra("ExtraUrl"))
            {
                bundle = intent.getStringExtra("ExtraUrl");
            }
        }
        if (TextUtils.isEmpty(bundle))
        {
            bundle1 = intent.getBundleExtra("BundleExtra");
            if (bundle1 != null)
            {
                bundle = bundle1.getString("ExtraUrl");
            }
        }
        boolean flag;
        if (intent != null && intent.hasExtra("login_from_setting"))
        {
            flag = intent.getBooleanExtra("login_from_setting", false);
        } else
        {
            flag = false;
        }
        bundle1 = new Bundle();
        bundle1.putString("ExtraUrl", bundle);
        bundle1.putBoolean("login_from_setting", flag);
        mFragment.setArguments(bundle1);
    }

    protected void onDestroy()
    {
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            overridePendingTransition(0x7f040013, 0x7f040014);
            finish();
            return true;
        } else
        {
            return false;
        }
    }
}
