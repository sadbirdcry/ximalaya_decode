// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import cn.com.iresearch.mapptracker.IRMonitor;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.eguan.drivermonitor.manager.ServiceManager;
import com.igexin.sdk.PushManager;
import com.loopj.android.http.RequestParams;
import com.umeng.fb.FeedbackAgent;
import com.umeng.fb.model.Conversation;
import com.umeng.fb.model.Reply;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.activity.BaseFragmentActivity;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.setting.UmengFeedbackActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.dl.PluginConstants;
import com.ximalaya.ting.android.dl.PluginManager;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.library.model.AppAd;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.AdManager;
import com.ximalaya.ting.android.modelmanage.FilterManage;
import com.ximalaya.ting.android.modelmanage.MyLocationManager;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.player.cdn.CdnConfigModel;
import com.ximalaya.ting.android.player.cdn.CdnUtil;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            CallbackDelegateable, ad, aa, u, 
//            s, q, z, y, 
//            ac, ae, o, p, 
//            n, w, x, ActivityCallbackDelegate

public class WelcomeActivity extends BaseFragmentActivity
    implements CallbackDelegateable, com.ximalaya.ting.android.util.FreeFlowUtil.FlowProxyListener
{

    public static final String IRESEARCH_KEY = "9660cc20ce4b6142";
    public static final String IS_INIT = "isInit";
    public static final String IS_SHOW_AD_IMMEDIATELY = "isShowAdImmediately";
    protected static final String TAG = com/ximalaya/ting/android/activity/login/WelcomeActivity.getSimpleName();
    public static WelcomeActivity welcomeActivity;
    private final String IS_Send_TO_2345 = "IS_Send_TO_2345";
    public boolean IS_TO_FINISH_APP;
    private ImageView adImg;
    private boolean isFinish;
    private int mAdShowTime;
    private long mAdStartShowTime;
    private AppAd mAppAd;
    private ActivityCallbackDelegate mDelegate;
    private boolean mIsDestroyed;
    private boolean mIsInit;
    private boolean mIsShowAdImmediately;
    private SharedPreferencesUtil mSharedPreferencesUtil;
    private ProgressDialog m_pDialog;

    public WelcomeActivity()
    {
        mAdShowTime = 2500;
        mAdStartShowTime = 0L;
        IS_TO_FINISH_APP = false;
        mIsInit = true;
        mIsShowAdImmediately = true;
        mIsDestroyed = false;
        isFinish = false;
    }

    private void activatePhone()
    {
        String s3;
        s3 = (new StringBuilder()).append(a.R).append("adrecord/channel").toString();
        String s1;
        try
        {
            s1 = getApplicationContext().getPackageManager().getApplicationInfo(getApplicationContext().getPackageName(), 128).metaData.getString("UMENG_CHANNEL");
        }
        catch (Exception exception)
        {
            exception = null;
        }
        break MISSING_BLOCK_LABEL_51;
        if (s1 != null && !SharedPreferencesUtil.getInstance(getApplicationContext()).getBoolean("IS_ACTIVATED_TAG", false))
        {
            Object obj = (TelephonyManager)getSystemService("phone");
            if (obj != null)
            {
                String s2 = ((TelephonyManager) (obj)).getDeviceId();
                String s4 = ((TelephonyManager) (obj)).getSimSerialNumber();
                obj = ((TelephonyManager) (obj)).getSubscriberId();
                String s5 = android.provider.Settings.Secure.getString(getContentResolver(), "android_id");
                RequestParams requestparams = new RequestParams();
                requestparams.put("deviceId", s2);
                requestparams.put("simSerialNumber", s4);
                requestparams.put("imsi", ((String) (obj)));
                requestparams.put("androidId", s5);
                f.a().a(s3, requestparams, null, new ad(this), false);
                return;
            }
        }
        return;
    }

    private void addShortcut()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(this);
        if (sharedpreferencesutil.getBoolean("isCreatedShortcut", false))
        {
            return;
        } else
        {
            Intent intent = new Intent("com.android.launcher.action.INSTALL_SHORTCUT");
            Intent intent1 = new Intent("android.intent.action.MAIN");
            intent1.setComponent(new ComponentName(getPackageName(), (new StringBuilder()).append(getPackageName()).append(".").append(getClass().getSimpleName()).toString()));
            intent1.setClass(getApplicationContext(), com/ximalaya/ting/android/activity/login/WelcomeActivity);
            intent1.setAction("android.intent.action.MAIN");
            intent1.addCategory("android.intent.category.LAUNCHER");
            intent1.setFlags(0x200000);
            intent.putExtra("android.intent.extra.shortcut.INTENT", intent1);
            intent.putExtra("android.intent.extra.shortcut.NAME", getString(0x7f090000));
            intent.putExtra("duplicate", false);
            intent.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", android.content.Intent.ShortcutIconResource.fromContext(getApplicationContext(), 0x7f020598));
            sendBroadcast(intent);
            sharedpreferencesutil.saveBoolean("isCreatedShortcut", true);
            return;
        }
    }

    private boolean checkNeedUpdateAppConfig()
    {
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(mAppContext);
        return !TimeHelper.getDateFriendlyStr().equals(sharedpreferencesutil.getString("app_set_update_date_when_loading"));
    }

    private void clearAdCache()
    {
        Object obj = getAdsFromPref();
        if (obj != null)
        {
            obj = ((List) (obj)).iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                AppAd appad = (AppAd)((Iterator) (obj)).next();
                if (appad != null)
                {
                    ImageManager2.from(getApplication()).downLoadBitmap(appad.getCover());
                }
            } while (true);
        }
        SharedPreferencesUtil.getInstance(getApplicationContext()).removeByKey("loadingAd_list");
        SharedPreferencesUtil.getInstance(getApplicationContext()).removeByKey("loadingAd_next_position");
    }

    private void deleteOldAd(List list, List list1)
    {
        if (list1 != null)
        {
            list1 = list1.iterator();
            while (list1.hasNext()) 
            {
                AppAd appad = (AppAd)list1.next();
                if (appad != null)
                {
                    Iterator iterator = list.iterator();
                    boolean flag = false;
                    do
                    {
                        if (!iterator.hasNext())
                        {
                            break;
                        }
                        AppAd appad1 = (AppAd)iterator.next();
                        if (appad1 != null && TextUtils.equals(appad1.getCover(), appad.getCover()))
                        {
                            flag = true;
                        }
                    } while (true);
                    if (!flag)
                    {
                        ImageManager2.from(getApplication()).deleteBitmapFromDownloadCache(appad.getCover());
                    }
                }
            }
        }
    }

    private void doFinish(Intent intent)
    {
        if (isFinish)
        {
            return;
        } else
        {
            startActivity(intent);
            overridePendingTransition(0, 0);
            super.finish();
            isFinish = true;
            return;
        }
    }

    private void downloadFile(String s1)
    {
        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(s1, new aa(this));
    }

    private List getAdsFromPref()
    {
        String s1 = SharedPreferencesUtil.getInstance(getApplicationContext()).getString("loadingAd_list");
        List list = null;
        if (!TextUtils.isEmpty(s1))
        {
            try
            {
                list = JSON.parseArray(s1, com/ximalaya/ting/android/library/model/AppAd);
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
                return null;
            }
        }
        return list;
    }

    private int getAvailablePosition(int i, int j, List list)
    {
        int k = list.size();
        if (i + 1 <= k)
        {
            for (; i != j; i++)
            {
                if (i + 1 <= k && timeMatch((AppAd)list.get(i)))
                {
                    return i;
                }
            }

        }
        return -1;
    }

    private AppAd getNextAd(List list)
    {
        if (list != null && !list.isEmpty())
        {
            SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(getApplicationContext());
            int k = sharedpreferencesutil.getInt("loadingAd_next_position", 0);
            int j = getAvailablePosition(k, list.size(), list);
            int i = j;
            if (j == -1)
            {
                i = getAvailablePosition(0, k, list);
            }
            if (i != -1)
            {
                sharedpreferencesutil.saveInt("loadingAd_next_position", i + 1);
                return (AppAd)list.get(i);
            }
        }
        return null;
    }

    private void goToWeb(String s1)
    {
        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(s1, new u(this));
    }

    private void goToWebWithMain(Intent intent)
    {
        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(mAppAd.getILink(), new s(this, intent));
    }

    private void gotoNextActivity()
    {
        (new q(this)).myexec(new Void[0]);
    }

    private void gotoNextActivity(boolean flag)
    {
        if (mIsInit)
        {
            toMainTabActivity2(flag);
            return;
        } else
        {
            toCurrentAcitivty(flag);
            return;
        }
    }

    private void initAd()
    {
        adImg = (ImageView)findViewById(0x7f0a0120);
        if (a.e)
        {
            android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)adImg.getLayoutParams();
            layoutparams.bottomMargin = ToolUtil.dp2px(mActivity, 130F);
            adImg.setLayoutParams(layoutparams);
        }
        adImg.setOnClickListener(new z(this));
    }

    private void initDeviceInfo()
    {
        MyDeviceManager.getInstance(getApplicationContext()).parseDeviceInfo();
    }

    private void initInThread()
    {
        if (mIsInit)
        {
            activatePhone();
            requestAd();
            AdManager.getInstance().initCommTopAd((MyApplication)getApplicationContext());
            statsAd();
            MyLocationManager.getInstance(getApplicationContext()).requestUpdateLocation(true);
            updateAppSet();
            return;
        } else
        {
            requestAd();
            statsAd();
            updateAppConfig(true);
            return;
        }
    }

    private void initWithNetwork()
    {
        if (getApplicationContext() instanceof MyApplication)
        {
            ((MyApplication)getApplicationContext()).q();
        }
        if (PluginManager.getInstance(getApplicationContext()).isPluginLoaded(PluginConstants.PLUGIN_APPTRACK))
        {
            IRMonitor.getInstance(getApplicationContext()).Init("9660cc20ce4b6142", null, false);
        }
        if (android.os.Build.VERSION.SDK_INT >= 9)
        {
            Conversation conversation = (new FeedbackAgent(getApplicationContext())).getDefaultConversation();
            if (conversation != null)
            {
                conversation.sync(new y(this));
            }
        }
        FreeFlowUtil.getInstance().useFreeFlow(true);
    }

    private boolean isEndTimeExpired(AppAd appad)
    {
        long l = (new Date()).getTime();
        return appad.getEndAt() < l;
    }

    private boolean judgeLogin()
    {
        boolean flag1 = false;
        Object obj = SharedPreferencesUtil.getInstance(mAppContext);
        Object obj1 = ((SharedPreferencesUtil) (obj)).getString("loginforesult");
        boolean flag = flag1;
        if (obj1 != null)
        {
            flag = flag1;
            if (!"".equals(obj1))
            {
                obj1 = parseLoginfo(((String) (obj1)));
                flag = flag1;
                if (obj1 != null)
                {
                    UserInfoMannage.getInstance().setUser(((LoginInfoModel) (obj1)));
                    flag = flag1;
                    if (ToolUtil.isConnectToNetwork(mAppContext))
                    {
                        String s1 = ((SharedPreferencesUtil) (obj)).getString("account");
                        obj = ((SharedPreferencesUtil) (obj)).getString("password");
                        flag = flag1;
                        if (((LoginInfoModel) (obj1)).loginFromId == 0)
                        {
                            flag = flag1;
                            if (CommonRequest.doLogin(getApplicationContext(), ((LoginInfoModel) (obj1)).loginFromId, s1, ((String) (obj)), null, false) != null)
                            {
                                flag = true;
                            }
                        }
                    }
                }
            }
        }
        return flag;
    }

    private String login(SharedPreferencesUtil sharedpreferencesutil)
    {
        Object obj = null;
        Object obj1 = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/login").toString();
        String s1 = sharedpreferencesutil.getString("account");
        String s2 = sharedpreferencesutil.getString("password");
        RequestParams requestparams = new RequestParams();
        sharedpreferencesutil = obj;
        if (s1 != null)
        {
            sharedpreferencesutil = obj;
            if (s2 != null)
            {
                requestparams.put("account", s1);
                requestparams.put("password", s2);
                obj1 = f.a().a(((String) (obj1)), requestparams, null);
                sharedpreferencesutil = obj;
                if (((com.ximalaya.ting.android.b.n.a) (obj1)).b == 1)
                {
                    sharedpreferencesutil = ((com.ximalaya.ting.android.b.n.a) (obj1)).a;
                }
            }
        }
        return sharedpreferencesutil;
    }

    private void onStartApp()
    {
        FilterManage.getInstance().updateMenuTag();
        PushManager.getInstance().initialize(getApplicationContext());
        Logger.e("", "getui init");
        addShortcut();
        initAd();
        showAd();
        initDeviceInfo();
        finish();
        if (NetworkUtils.isNetworkAvaliable(getApplicationContext()))
        {
            DataCollectUtil.getInstance(getApplicationContext()).statOfflineEvent();
        }
    }

    private LoginInfoModel parseLoginfo(String s1)
    {
label0:
        {
            try
            {
                if (!"0".equals(JSON.parseObject(s1).get("ret").toString()))
                {
                    break label0;
                }
                s1 = (LoginInfoModel)JSON.parseObject(s1, com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s1)
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
                return null;
            }
            return s1;
        }
        return null;
    }

    private void refreshLoacalAds(List list, List list1)
    {
        Iterator iterator = list.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            AppAd appad = (AppAd)iterator.next();
            if (appad != null)
            {
                ImageManager2.from(getApplication()).downLoadBitmap(appad.getCover());
            }
        } while (true);
        deleteOldAd(list, list1);
    }

    private void requestAd()
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("appid", "0");
        requestparams.put("device", "android");
        requestparams.put("name", "loading");
        requestparams.put("version", ((MyApplication)getApplicationContext()).m());
        requestparams.put("network", NetworkUtils.getNetworkClass(this));
        requestparams.put("operator", NetworkUtils.getOperator(this));
        String s1 = (new StringBuilder()).append(a.Q).append("ting/loading").toString();
        f.a().a(s1, requestparams, null, new ac(this), false);
    }

    private void saveUpdateDate()
    {
        String s1 = TimeHelper.getDateFriendlyStr();
        if (TextUtils.isEmpty(s1))
        {
            return;
        } else
        {
            SharedPreferencesUtil.getInstance(mAppContext).saveString("app_set_update_date_when_loading", s1);
            return;
        }
    }

    private void sendFeedBackNotification(List list)
    {
        Context context = getApplicationContext();
        if (list.size() == 1)
        {
            list = context.getString(0x7f09025c, new Object[] {
                ((Reply)list.get(0)).content
            });
        } else
        {
            list = context.getString(0x7f09025d, new Object[] {
                Integer.valueOf(list.size())
            });
        }
        try
        {
            NotificationManager notificationmanager = (NotificationManager)context.getSystemService("notification");
            Object obj = new Intent(getApplicationContext(), com/ximalaya/ting/android/activity/setting/UmengFeedbackActivity);
            ((Intent) (obj)).setFlags(0x20000);
            obj = PendingIntent.getActivity(context, (int)SystemClock.uptimeMillis(), ((Intent) (obj)), 0x8000000);
            int i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).applicationInfo.icon;
            notificationmanager.notify(0, (new android.support.v4.app.NotificationCompat.Builder(context)).setSmallIcon(i).setContentTitle(getString(0x7f09025b)).setTicker(getString(0x7f09025b)).setContentText(list).setAutoCancel(true).setContentIntent(((PendingIntent) (obj))).build());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (List list)
        {
            list.printStackTrace();
        }
    }

    private void showAd()
    {
        List list = getAdsFromPref();
        if (list != null)
        {
            mAppAd = getNextAd(list);
            if (mAppAd != null)
            {
                mAdStartShowTime = System.currentTimeMillis();
                android.graphics.Bitmap bitmap = ImageManager2.from(getApplication()).loadBitmapFromDownLoadCache(mAppAd.getCover());
                if (bitmap != null)
                {
                    adImg.setImageBitmap(bitmap);
                }
            }
        }
    }

    private void statsAd()
    {
        if (mAppAd != null)
        {
            ThirdAdStatUtil.getInstance().thirdLoadingAdStatRequest(mAppAd.getThirdStatUrl());
            AdManager.getInstance().adrecordForShow(mAppAd, (MyApplication)getApplicationContext());
            AdCollectData adcollectdata = new AdCollectData();
            adcollectdata.setAdItemId((new StringBuilder()).append(mAppAd.getAdid()).append("").toString());
            adcollectdata.setAdSource("0");
            adcollectdata.setAndroidId(ToolUtil.getAndroidId(getApplicationContext()));
            adcollectdata.setLogType("tingShow");
            adcollectdata.setPositionName("loading");
            adcollectdata.setResponseId((new StringBuilder()).append(mAppAd.getAdid()).append("").toString());
            adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
            adcollectdata.setTrackId("-1");
            if (NetworkUtils.isNetworkAvaliable(getApplicationContext()))
            {
                DataCollectUtil.getInstance(getApplicationContext()).statOnlineAd(adcollectdata);
                return;
            }
        }
    }

    private boolean timeMatch(AppAd appad)
    {
        long l = (new Date()).getTime();
        return !isEndTimeExpired(appad) && appad.getStartAt() <= l;
    }

    private void toCurrentAcitivty(boolean flag)
    {
        if (flag)
        {
            if (mAppAd.getILinkType() == 1 || mAppAd.getILinkType() == 0)
            {
                goToWeb(mAppAd.getILink());
                return;
            }
            if (mAppAd.getILinkType() == 2)
            {
                downloadFile(mAppAd.getILink());
            }
        }
        super.finish();
    }

    private void toMainTabActivity2(boolean flag)
    {
        Intent intent = new Intent(mActivity, com/ximalaya/ting/android/activity/MainTabActivity2);
        intent.setFlags(0x4010000);
        if (getIntent() != null && "com.ximalaya.ting.android.launch_from_widget".equals(getIntent().getAction()))
        {
            intent.putExtra("show_recommend_sound", true);
        }
        if (getIntent() != null && getIntent().getAction() != null)
        {
            Logger.d(TAG, (new StringBuilder()).append("gotoMainTabActivity2():action:").append(getIntent().getAction()).toString());
            if (getIntent().getAction().contains("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_START_PLAY"))
            {
                intent.setAction("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_START_PLAY");
            }
        }
        if (flag)
        {
            if (mAppAd.getILinkType() == 1 || mAppAd.getILinkType() == 0)
            {
                goToWebWithMain(intent);
                return;
            }
            if (mAppAd.getILinkType() == 2)
            {
                downloadFile(mAppAd.getILink());
            }
        }
        doFinish(intent);
    }

    private void updateAppConfig(boolean flag)
    {
        String s1 = e.b;
        f.a().a(s1, null, null, new ae(this, flag));
    }

    private void updateAppSet()
    {
        (new o(this)).myexec(new Void[0]);
        updateAppConfig(false);
    }

    private void updateCdnConfigModel()
    {
        CdnConfigModel cdnconfigmodel = new CdnConfigModel();
        int i = SharedPreferencesUtil.getInstance(mAppContext).getInt("cdnNotWifiAlertRate", 32);
        int l = SharedPreferencesUtil.getInstance(mAppContext).getInt("cdnNotWifiConnectTimeout", 10);
        int k = SharedPreferencesUtil.getInstance(mAppContext).getInt("cdnWifiAlertRate", 50);
        int j = SharedPreferencesUtil.getInstance(mAppContext).getInt("cdnWifiConnectTimeout", 2);
        if (i <= 0)
        {
            i = 32;
        }
        cdnconfigmodel.setCdnNotWifiAlertRate(i);
        if (l > 0)
        {
            i = l;
        } else
        {
            i = 10;
        }
        cdnconfigmodel.setCdnNotWifiConnectTimeout(i);
        if (k > 0)
        {
            i = k;
        } else
        {
            i = 50;
        }
        cdnconfigmodel.setCdnWifiAlertRate(i);
        if (j > 0)
        {
            i = j;
        } else
        {
            i = 2;
        }
        cdnconfigmodel.setCdnWifiConnectTimeout(i);
        cdnconfigmodel.setCdnUrl(e.an);
        if (getApplication() != null)
        {
            cdnconfigmodel.setNetType(NetworkUtils.getNetType(getApplication()));
            cdnconfigmodel.setUserAgent(((MyApplication)getApplication()).l());
            cdnconfigmodel.setCookie((new com.ximalaya.ting.android.b.e()).a((MyApplication)getApplicationContext()));
        }
        CdnUtil.setCdnConfigModel(cdnconfigmodel);
    }

    public void finish()
    {
        if (IS_TO_FINISH_APP)
        {
            super.finish();
            return;
        }
        f.a().a(f.d);
        if (mAdStartShowTime != 0L && mAppAd != null)
        {
            long l = (long)mAdShowTime - (System.currentTimeMillis() - mAdStartShowTime);
            if (l > 500L && adImg != null)
            {
                adImg.postDelayed(new p(this), l);
            } else
            {
                gotoNextActivity();
            }
        } else
        {
            gotoNextActivity();
        }
        initInThread();
    }

    public void onBackPressed()
    {
        super.onBackPressed();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030033);
        Logger.e(TAG, (new StringBuilder()).append("Build.MODEL:").append(Build.MODEL).toString());
        Logger.e(TAG, (new StringBuilder()).append("ACTION WELCOM:").append(getIntent().getAction()).toString());
        if (PluginManager.getInstance(getApplicationContext()).isPluginLoaded(PluginConstants.PLUGIN_EGUAN))
        {
            ServiceManager.getInstance().startProgress(this);
        }
        CheckBox checkbox;
        android.app.AlertDialog.Builder builder;
        boolean flag;
        if (getIntent() != null)
        {
            mIsInit = getIntent().getBooleanExtra("isInit", true);
            mIsShowAdImmediately = getIntent().getBooleanExtra("isShowAdImmediately", true);
        } else
        {
            mIsInit = true;
            mIsShowAdImmediately = true;
        }
        if (!mIsInit) goto _L2; else goto _L1
_L1:
        FreeFlowUtil.getInstance().init(getApplicationContext());
        FreeFlowUtil.getInstance().setFlowProxyListener(this);
        welcomeActivity = this;
        mSharedPreferencesUtil = SharedPreferencesUtil.getInstance(getApplicationContext());
        flag = mSharedPreferencesUtil.getBoolean("P_IS_SURE_NO_3G_DIALOG_NOTIFY");
        if (!a.b || flag) goto _L4; else goto _L3
_L3:
        bundle = (LinearLayout)getLayoutInflater().inflate(0x7f03006c, null);
        checkbox = (CheckBox)bundle.findViewById(0x7f0a0206);
        builder = new android.app.AlertDialog.Builder(this);
        builder.setTitle("\u63D0\u793A");
        builder.setView(bundle);
        builder.setPositiveButton("\u540C\u610F", new n(this, checkbox));
        builder.setNegativeButton("\u9000\u51FA", new w(this));
        builder.setOnKeyListener(new x(this));
        bundle = builder.create();
        bundle.setCanceledOnTouchOutside(false);
        bundle.show();
_L6:
        return;
_L4:
        initWithNetwork();
        return;
_L2:
        initAd();
        if (mIsShowAdImmediately)
        {
            showAd();
            finish();
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    protected void onDestroy()
    {
        super.onDestroy();
        mIsDestroyed = true;
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (mDelegate != null)
        {
            return mDelegate.onKeyDown(i, keyevent);
        } else
        {
            return super.onKeyDown(i, keyevent);
        }
    }

    protected void onResume()
    {
        super.onResume();
        if (!mIsInit && !mIsShowAdImmediately && mAppAd == null)
        {
            showAd();
            finish();
        }
    }

    protected void onStart()
    {
        MyApplication.k = true;
        super.onStart();
    }

    protected void onStop()
    {
        MyApplication.j = true;
        super.onStop();
    }

    public void setCallbackDelegate(ActivityCallbackDelegate activitycallbackdelegate)
    {
        mDelegate = activitycallbackdelegate;
    }

    public void stopHandleFreeFlow()
    {
        if (isFinishing())
        {
            return;
        } else
        {
            Logger.log("FreeFlowUtil stopHandleFreeFlow welcome");
            FreeFlowUtil.getInstance().removeFlowProxyListener();
            onStartApp();
            welcomeActivity = null;
            return;
        }
    }




















}
