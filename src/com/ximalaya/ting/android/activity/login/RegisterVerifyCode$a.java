// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            RegisterVerifyCode, i

private class <init> extends MyAsyncTask
{

    final RegisterVerifyCode a;

    protected transient LoginInfoModel a(Void avoid[])
    {
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/register").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("account", a.loginInfoModel.account);
        requestparams.put("nickname", a.loginInfoModel.nickname);
        requestparams.put("password", a.loginInfoModel.password);
        requestparams.put("checkCode", RegisterVerifyCode.access$000(a).getText().toString().trim());
        avoid = f.a().b(avoid, requestparams, RegisterVerifyCode.access$100(a), null);
        Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
        a.loginInfoModel.ret = -1;
        a.loginInfoModel.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5931\u8D25\uFF0C\u8BF7\u68C0\u67E5\u662F\u5426\u8FDE\u63A5\u7F51\u7EDC";
        if (avoid == null) goto _L2; else goto _L1
_L1:
        Object obj = JSON.parseObject(avoid);
        if (!"0".equals(((JSONObject) (obj)).get("ret").toString())) goto _L4; else goto _L3
_L3:
        obj = SharedPreferencesUtil.getInstance(RegisterVerifyCode.access$200(a));
        ((SharedPreferencesUtil) (obj)).saveString("account", a.loginInfoModel.account);
        ((SharedPreferencesUtil) (obj)).saveString("password", a.loginInfoModel.password);
        a.loginInfoModel = (LoginInfoModel)JSON.parseObject(avoid, com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
        ((SharedPreferencesUtil) (obj)).saveString("loginforesult", avoid);
        UserInfoMannage.getInstance().setUser(a.loginInfoModel);
_L2:
        return a.loginInfoModel;
_L4:
        try
        {
            a.loginInfoModel.ret = -1;
            a.loginInfoModel.msg = ((JSONObject) (obj)).getString("msg");
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
            a.loginInfoModel.ret = -1;
            a.loginInfoModel.msg = "\u89E3\u6790json\u5F02\u5E38";
        }
        if (true) goto _L2; else goto _L5
_L5:
    }

    protected void a(LoginInfoModel logininfomodel)
    {
        super.onPostExecute(logininfomodel);
        if (a == null || a.isFinishing())
        {
            return;
        }
        RegisterVerifyCode.access$300(a);
        if (logininfomodel.ret == 0)
        {
            Toast.makeText(a, a.getString(0x7f0900b4), 1).show();
            logininfomodel = new Intent(a, com/ximalaya/ting/android/activity/MainTabActivity2);
            logininfomodel.putExtra("isLogin", true);
            logininfomodel.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(RegisterVerifyCode.access$100(a)));
            a.startActivity(logininfomodel);
            a.finish();
            return;
        } else
        {
            a.dialog(logininfomodel.msg);
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onCancelled()
    {
        super.onCancelled();
        RegisterVerifyCode.access$300(a);
    }

    protected void onPostExecute(Object obj)
    {
        a((LoginInfoModel)obj);
    }

    protected void onPreExecute()
    {
        android.app.ProgressDialog _tmp = RegisterVerifyCode.access$400(a);
        super.onPreExecute();
    }

    private (RegisterVerifyCode registerverifycode)
    {
        a = registerverifycode;
        super();
    }

    a(RegisterVerifyCode registerverifycode, i i)
    {
        this(registerverifycode);
    }
}
