// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ximalaya.ting.android.fragment.login.CollectUserInformationFragment;
import com.ximalaya.ting.android.fragment.login.UserInformationFragment;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            WelcomeActivity, r

class q extends MyAsyncTask
{

    final WelcomeActivity a;

    q(WelcomeActivity welcomeactivity)
    {
        a = welcomeactivity;
        super();
    }

    protected transient Boolean a(Void avoid[])
    {
        return Boolean.valueOf(CollectUserInformationFragment.shouldShow(a.getApplicationContext()));
    }

    protected void a(Boolean boolean1)
    {
        if (WelcomeActivity.access$1700(a))
        {
            return;
        }
        a.findViewById(0x7f0a0093).setVisibility(4);
        if (boolean1.booleanValue())
        {
            boolean1 = UserInformationFragment.newInstance();
            boolean1.setFragmentCallback(new r(this));
            a.getSupportFragmentManager().beginTransaction().replace(0x7f0a00f1, boolean1).commitAllowingStateLoss();
            return;
        } else
        {
            CollectUserInformationFragment.postDataBackground(a.getApplicationContext());
            WelcomeActivity.access$400(a, false);
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((Boolean)obj);
    }

    protected void onPreExecute()
    {
        LayoutInflater.from(a.getApplicationContext()).inflate(0x7f030198, (ViewGroup)a.findViewById(0x7f0a00f1), true);
    }
}
