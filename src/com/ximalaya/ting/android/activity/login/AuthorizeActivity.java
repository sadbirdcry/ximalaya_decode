// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.auth.AuthInfo;
import com.ximalaya.ting.android.model.auth.RennAuthResponse;
import com.ximalaya.ting.android.model.auth.RennUser;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.BindStatus;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class AuthorizeActivity extends BaseActivity
{
    public class BindTask extends MyAsyncTask
    {

        AuthInfo authInfo;
        final AuthorizeActivity this$0;

        protected transient BindStatus doInBackground(Object aobj[])
        {
            Object obj;
            JSONObject jsonobject;
            RequestParams requestparams;
            jsonobject = null;
            authInfo = (AuthInfo)aobj[0];
            requestparams = new RequestParams();
            if (lgFlag == 12)
            {
                aobj = (new StringBuilder()).append(com.ximalaya.ting.android.a.u).append("mobile/").append(1).append("/bind").toString();
                requestparams.put("openid", authInfo.uid);
                requestparams.put("expiresIn", authInfo.expires_in);
            } else
            {
label0:
                {
                    if (lgFlag != 13)
                    {
                        break label0;
                    }
                    aobj = (new StringBuilder()).append(com.ximalaya.ting.android.a.u).append("mobile/").append(2).append("/bind").toString();
                    requestparams.put("openid", authInfo.openid);
                    requestparams.put("expiresIn", authInfo.expires_in);
                }
            }
_L4:
            requestparams.put("accessToken", authInfo.access_token);
            requestparams.put("deviceToken", ToolUtil.getDeviceToken(mCt));
            aobj = f.a().a(((String) (aobj)), requestparams, null);
            if (((com.ximalaya.ting.android.b.n.a) (aobj)).b == 1)
            {
                aobj = ((com.ximalaya.ting.android.b.n.a) (aobj)).a;
            } else
            {
                aobj = null;
            }
            Logger.log((new StringBuilder()).append("result:").append(((String) (aobj))).toString());
            obj = jsonobject;
            if (aobj == null) goto _L2; else goto _L1
_L1:
            obj = new BindStatus();
            jsonobject = JSON.parseObject(((String) (aobj)));
            if (!"0".equals(jsonobject.get("ret").toString()))
            {
                break MISSING_BLOCK_LABEL_369;
            }
            aobj = (BindStatus)JSONObject.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/personal_setting/BindStatus);
_L5:
            obj = ((Object) (aobj));
_L2:
            return ((BindStatus) (obj));
            obj = jsonobject;
            if (lgFlag != 14) goto _L2; else goto _L3
_L3:
            aobj = (new StringBuilder()).append(com.ximalaya.ting.android.a.u).append("mobile/").append(3).append("/bind").toString();
            requestparams.put("openid", authInfo.openid);
            requestparams.put("macKey", authInfo.macKey);
              goto _L4
            try
            {
                obj.ret = jsonobject.getIntValue("ret");
                obj.msg = jsonobject.getString("msg");
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
                obj.ret = -1;
                obj.msg = "\u89E3\u6790json\u5F02\u5E38";
                return ((BindStatus) (obj));
            }
            aobj = ((Object []) (obj));
              goto _L5
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onCancelled()
        {
            super.onCancelled();
            dismissProgressDialog();
        }

        protected void onPostExecute(BindStatus bindstatus)
        {
            if (AuthorizeActivity.this == null || isFinishing())
            {
                return;
            }
            dismissProgressDialog();
            if (bindstatus != null)
            {
                if (loginInfoModel != null && bindstatus.ret == 0 && bindstatus.data != null && bindstatus.data.size() > 0)
                {
                    if (loginInfoModel.bindStatus == null)
                    {
                        loginInfoModel.bindStatus = new ArrayList();
                    }
                    loginInfoModel.bindStatus.add(bindstatus.data.get(0));
                    bindStatus = bindstatus;
                    isBind = true;
                    finish();
                    return;
                } else
                {
                    dialog(bindstatus.msg);
                    return;
                }
            } else
            {
                dialog("\u7F51\u7EDC\u8BBF\u95EE\u8D85\u65F6");
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((BindStatus)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            showProgressDialog();
        }

        public BindTask()
        {
            this$0 = AuthorizeActivity.this;
            super();
        }
    }

    public class LoginTask extends MyAsyncTask
    {

        AuthInfo authInfo;
        final AuthorizeActivity this$0;

        protected transient LoginInfoModel doInBackground(Object aobj[])
        {
            Object obj;
            Object obj1;
            obj = null;
            obj1 = new RequestParams();
            authInfo = (AuthInfo)aobj[0];
            if (lgFlag == 1)
            {
                aobj = (new StringBuilder()).append(com.ximalaya.ting.android.a.u).append("mobile/").append(1).append("/access").toString();
                ((RequestParams) (obj1)).put("openid", authInfo.uid);
            } else
            {
label0:
                {
                    if (lgFlag != 2)
                    {
                        break label0;
                    }
                    aobj = (new StringBuilder()).append(com.ximalaya.ting.android.a.u).append("mobile/").append(2).append("/access").toString();
                    ((RequestParams) (obj1)).put("openid", authInfo.openid);
                }
            }
_L5:
            ((RequestParams) (obj1)).put("accessToken", authInfo.access_token);
            ((RequestParams) (obj1)).put("expiresIn", authInfo.expires_in);
            ((RequestParams) (obj1)).put("deviceToken", ToolUtil.getDeviceToken(mCt));
            aobj = f.a().a(((String) (aobj)), ((RequestParams) (obj1)), null);
            if (((com.ximalaya.ting.android.b.n.a) (aobj)).b == 1)
            {
                obj = ((com.ximalaya.ting.android.b.n.a) (aobj)).a;
            } else
            {
                obj = null;
            }
            Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_412;
            }
            aobj = new LoginInfoModel();
            obj1 = JSON.parseObject(((String) (obj)));
            if (!"0".equals(((JSONObject) (obj1)).get("ret").toString())) goto _L2; else goto _L1
_L1:
            obj1 = (LoginInfoModel)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
            SharedPreferencesUtil.getInstance(mCt).saveString("loginforesult", ((String) (obj)));
            aobj = ((Object []) (obj1));
_L4:
            return ((LoginInfoModel) (aobj));
            aobj = ((Object []) (obj));
            if (lgFlag != 3) goto _L4; else goto _L3
_L3:
            aobj = (new StringBuilder()).append(com.ximalaya.ting.android.a.u).append("mobile/").append(3).append("/access").toString();
            ((RequestParams) (obj1)).put("openid", authInfo.openid);
              goto _L5
_L2:
            aobj.ret = ((JSONObject) (obj1)).getIntValue("ret");
            aobj.msg = ((JSONObject) (obj1)).getString("msg");
              goto _L4
            obj;
_L6:
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
            aobj.ret = -1;
            aobj.msg = "\u89E3\u6790json\u5F02\u5E38";
              goto _L4
            aobj;
            aobj = ((Object []) (obj1));
              goto _L6
            aobj = null;
              goto _L4
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected void onCancelled()
        {
            super.onCancelled();
            dismissProgressDialog();
        }

        protected void onPostExecute(LoginInfoModel logininfomodel)
        {
            if (AuthorizeActivity.this == null || isFinishing())
            {
                return;
            }
            dismissProgressDialog();
            if (logininfomodel != null)
            {
                if (logininfomodel.ret == 0)
                {
                    UserInfoMannage.getInstance().setUser(logininfomodel);
                    logininfomodel = new Intent(AuthorizeActivity.this, com/ximalaya/ting/android/activity/MainTabActivity2);
                    logininfomodel.putExtra("isLogin", true);
                    startActivity(logininfomodel);
                    finish();
                    return;
                } else
                {
                    dialog(logininfomodel.msg);
                    return;
                }
            } else
            {
                dialog("\u7F51\u7EDC\u8BBF\u95EE\u8D85\u65F6");
                return;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((LoginInfoModel)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            showProgressDialog();
        }

        public LoginTask()
        {
            this$0 = AuthorizeActivity.this;
            super();
        }
    }

    class a extends MyAsyncTask
    {

        String a;
        final AuthorizeActivity b;

        protected transient AuthInfo a(Object aobj[])
        {
            Object obj;
            Object obj2;
            com.ximalaya.ting.android.b.n.a a1;
            obj2 = null;
            a1 = null;
            obj = null;
            a = (String)aobj[0];
            if (b.lgFlag != 12 && b.lgFlag != 1) goto _L2; else goto _L1
_L1:
            aobj = (new StringBuilder()).append("https://api.weibo.com/oauth2/access_token?client_id=2793726141&client_secret=5286380458aa97b4f8bbf18a81a52c9b&grant_type=authorization_code&redirect_uri=http%3A%2F%2Fwww.ximalaya.com&display=mobile&code=").append(a).toString();
            obj2 = f.a().a(((String) (aobj)), null, null);
            aobj = obj;
            if (((com.ximalaya.ting.android.b.n.a) (obj2)).b == 1)
            {
                aobj = ((com.ximalaya.ting.android.b.n.a) (obj2)).a;
            }
            if (aobj == null) goto _L4; else goto _L3
_L3:
            try
            {
                aobj = (AuthInfo)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/auth/AuthInfo);
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
                aobj = new AuthInfo();
                aobj.ret = -1;
                aobj.msg = "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
                return ((AuthInfo) (aobj));
            }
_L6:
            return ((AuthInfo) (aobj));
_L4:
            aobj = new AuthInfo();
            aobj.ret = -1;
            aobj.msg = "\u7F51\u7EDC\u8BBF\u95EE\u5931\u8D25\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
            return ((AuthInfo) (aobj));
_L2:
            if (b.lgFlag != 13 && b.lgFlag != 2)
            {
                break MISSING_BLOCK_LABEL_567;
            }
            aobj = (new StringBuilder()).append("https://graph.qq.com/oauth2.0/token?grant_type=authorization_code&client_id=").append(com.ximalaya.ting.android.a.b.c).append("&client_secret=9478fae5575482a632a7b8984def11e2&redirect_uri=http%3A%2F%2Fwww.ximalaya.com&display=mobile").append("&code=").append(a).toString();
            a1 = f.a().a(((String) (aobj)), null, null, null, false);
            Object obj1;
            com.ximalaya.ting.android.b.n.a a2;
            int j;
            if (a1.b == 1)
            {
                aobj = a1.a;
            } else
            {
                aobj = null;
            }
            if (aobj == null)
            {
                break MISSING_BLOCK_LABEL_546;
            }
            if ("".equals(((Object) (aobj))) || ((String) (aobj)).contains("error"))
            {
                break MISSING_BLOCK_LABEL_526;
            }
            obj1 = new AuthInfo();
            aobj = ((String) (aobj)).split("&");
            j = aobj.length;
            for (int i = 0; i < j; i++)
            {
                String as[] = ((String) (aobj[i])).split("=");
                if ("access_token".equals(as[0]))
                {
                    obj1.access_token = as[1];
                }
                if ("expires_in".equals(as[0]))
                {
                    obj1.expires_in = as[1];
                }
            }

            if (((AuthInfo) (obj1)).access_token == null)
            {
                break MISSING_BLOCK_LABEL_513;
            }
            aobj = new RequestParams();
            ((RequestParams) (aobj)).put("access_token", ((AuthInfo) (obj1)).access_token);
            a2 = f.a().a("https://graph.qq.com/oauth2.0/me", ((RequestParams) (aobj)), null, null, true);
            aobj = ((Object []) (obj2));
            if (a1.b == 1)
            {
                aobj = a2.a;
            }
            if (aobj == null)
            {
                break MISSING_BLOCK_LABEL_500;
            }
            aobj = ((String) (aobj)).substring(((String) (aobj)).indexOf("(") + 1, ((String) (aobj)).indexOf(")"));
            try
            {
                obj2 = JSON.parseObject(((String) (aobj)));
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                obj1.ret = -1;
                obj1.msg = ((Exception) (aobj)).toString();
                return ((AuthInfo) (obj1));
            }
            aobj = ((Object []) (obj1));
            if (((JSONObject) (obj2)).getString("openid") == null) goto _L6; else goto _L5
_L5:
            obj1.openid = ((JSONObject) (obj2)).getString("openid");
            obj1.ret = 0;
            return ((AuthInfo) (obj1));
            obj1.ret = -1;
            obj1.msg = "\u83B7\u53D6\u7B2C\u4E09\u65B9\u4FE1\u606F\u51FA\u9519\uFF01";
            return ((AuthInfo) (obj1));
            obj1.ret = -1;
            obj1.msg = "\u83B7\u53D6\u7B2C\u4E09\u65B9\u4FE1\u606F\u51FA\u9519\uFF01";
            return ((AuthInfo) (obj1));
            obj1 = new AuthInfo();
            obj1.msg = ((String) (aobj));
            obj1.ret = -1;
            return ((AuthInfo) (obj1));
            aobj = new AuthInfo();
            aobj.ret = -1;
            aobj.msg = "\u7F51\u7EDC\u8BBF\u95EE\u5931\u8D25\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
            return ((AuthInfo) (aobj));
            if (b.lgFlag == 14 || b.lgFlag == 3)
            {
                aobj = "https://graph.renren.com/oauth/token?grant_type=authorization_code&client_id=" + "0722943e9e3e4eafb5d38ed5e0ed3bfe" + "&redirect_uri=" + Uri.encode("http://www.ximalaya.com/passport/3/access") + "&client_secret=" + "49bba2c4d0ae4a4c8e406602b40c9501" + "&token_type=mac" + "&code=" + a;
                obj1 = f.a().a(((String) (aobj)), null, null, null, false);
                aobj = a1;
                if (((com.ximalaya.ting.android.b.n.a) (obj1)).b == 1)
                {
                    aobj = ((com.ximalaya.ting.android.b.n.a) (obj1)).a;
                }
                if (aobj != null)
                {
                    try
                    {
                        aobj = (RennAuthResponse)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/auth/RennAuthResponse);
                        obj1 = new AuthInfo();
                        obj1.token_type = ((RennAuthResponse) (aobj)).token_type;
                        obj1.access_token = ((RennAuthResponse) (aobj)).access_token;
                        obj1.openid = Long.toString(((RennAuthResponse) (aobj)).user.id.longValue());
                        obj1.macKey = ((RennAuthResponse) (aobj)).mac_key;
                        obj1.ret = 0;
                    }
                    // Misplaced declaration of an exception variable
                    catch (Object aobj[])
                    {
                        Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
                        aobj = new AuthInfo();
                        aobj.ret = -1;
                        aobj.msg = "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
                        return ((AuthInfo) (aobj));
                    }
                    return ((AuthInfo) (obj1));
                } else
                {
                    aobj = new AuthInfo();
                    aobj.ret = -1;
                    aobj.msg = "\u7F51\u7EDC\u8BBF\u95EE\u5931\u8D25\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
                    return ((AuthInfo) (aobj));
                }
            } else
            {
                return null;
            }
        }

        protected void a(AuthInfo authinfo)
        {
            if (b != null && !b.isFinishing())
            {
                b.mGetAccessTokenTask = null;
                if (authinfo.ret == 0)
                {
                    if (b.lgFlag == 1 || b.lgFlag == 2 || b.lgFlag == 3)
                    {
                        (b. new LoginTask()).myexec(new Object[] {
                            authinfo
                        });
                        return;
                    }
                    if (b.lgFlag == 12 || b.lgFlag == 13 || b.lgFlag == 14)
                    {
                        (b. new BindTask()).myexec(new Object[] {
                            authinfo
                        });
                        return;
                    }
                } else
                {
                    Toast.makeText(b.getApplicationContext(), authinfo.msg, 0).show();
                    return;
                }
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((AuthInfo)obj);
        }

        a()
        {
            b = AuthorizeActivity.this;
            super();
        }
    }

    private class b extends WebViewClient
    {

        final AuthorizeActivity a;

        public void onPageFinished(WebView webview, String s)
        {
            if (a != null && !a.isFinishing())
            {
                if (a.mSpinner.isShowing())
                {
                    a.mSpinner.dismiss();
                }
                if (s.contains("error"))
                {
                    Toast.makeText(a, a.getString(0x7f0900ac), 1).show();
                }
                if ((s.startsWith("http://www.ximalaya.com") || s.startsWith("http://test.ximalaya.com") || s.startsWith("http://m.ximalaya.com") && a.mGetAccessTokenTask == null) && !s.contains("error") && s.contains("code") && !a.hasAccessToken)
                {
                    webview = s.substring(s.indexOf("code="), s.length());
                    webview = webview.substring(5, webview.length());
                    a.mGetAccessTokenTask = a. new a();
                    a.mGetAccessTokenTask.myexec(new Object[] {
                        webview
                    });
                    return;
                }
            }
        }

        public void onPageStarted(WebView webview, String s, Bitmap bitmap)
        {
            if (a == null || a.isFinishing())
            {
                return;
            }
            a.mSpinner.show();
            a.hasAccessToken = false;
            if ((s.startsWith("http://www.ximalaya.com") || s.startsWith("http://test.ximalaya.com") || s.startsWith("http://m.ximalaya.com")) && !s.contains("error") && s.contains("code") && a.mGetAccessTokenTask == null)
            {
                bitmap = s.substring(s.indexOf("code="), s.length());
                bitmap = bitmap.substring(5, bitmap.length());
                a.hasAccessToken = true;
                a.mGetAccessTokenTask = a. new a();
                a.mGetAccessTokenTask.myexec(new Object[] {
                    bitmap
                });
                webview.stopLoading();
                Logger.log((new StringBuilder()).append("url1:").append(s).toString());
                return;
            } else
            {
                Logger.log((new StringBuilder()).append("url0:").append(s).toString());
                super.onPageStarted(webview, s, bitmap);
                return;
            }
        }

        private b()
        {
            a = AuthorizeActivity.this;
            super();
        }

    }


    public static final String SINA_SERVER_KEY = "2793726141";
    public static String URL_OAUTH2_ACCESS_AUTHORIZE = "https://api.weibo.com/oauth2/authorize";
    private AuthInfo authInfo;
    private BindStatus bindStatus;
    private boolean hasAccessToken;
    boolean isBind;
    private int lgFlag;
    private Context mCt;
    private a mGetAccessTokenTask;
    private ProgressDialog mProgressDialog;
    private ProgressDialog mSpinner;
    private WebView mWebView;
    private String url;

    public AuthorizeActivity()
    {
        authInfo = null;
        isBind = false;
        hasAccessToken = false;
    }

    private void dismissProgressDialog()
    {
        try
        {
            mProgressDialog.dismiss();
            return;
        }
        catch (IllegalArgumentException illegalargumentexception)
        {
            return;
        }
    }

    private void getParentInfo()
    {
        lgFlag = getIntent().getIntExtra("lgflag", 0);
    }

    private void intiUI()
    {
        initCommon();
        switch (lgFlag)
        {
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 7: // '\007'
        case 8: // '\b'
        case 9: // '\t'
        case 10: // '\n'
        case 11: // '\013'
        default:
            return;

        case 12: // '\f'
            setTitleText(getString(0x7f090140));
            return;

        case 13: // '\r'
            setTitleText(getString(0x7f090141));
            return;

        case 14: // '\016'
            setTitleText(getString(0x7f090142));
            return;

        case 1: // '\001'
            setTitleText(getString(0x7f0900ab));
            return;

        case 2: // '\002'
            setTitleText(getString(0x7f0900ad));
            return;

        case 3: // '\003'
            setTitleText(getString(0x7f0900ae));
            return;
        }
    }

    private void parseUrl()
    {
        if (lgFlag == 1 || lgFlag == 12)
        {
            url = "https://api.weibo.com/oauth2/authorize?client_id=2793726141&response_type=code&redirect_uri=http%3A%2F%2Fwww.ximalaya.com&display=mobile&scope=follow_app_official_microblog";
        } else
        {
            if (lgFlag == 2 || lgFlag == 13)
            {
                url = (new StringBuilder()).append("https://graph.qq.com/oauth2.0/authorize?client_id=").append(com.ximalaya.ting.android.a.b.c).append("&response_type=code&redirect_uri=http%3A%2F%2Fwww.ximalaya.com&display=mobile&scope=get_simple_userinfo,get_user_info,add_share,get_fanslist,check_page_fans,add_t,add_pic_t").toString();
                return;
            }
            if (lgFlag == 3 || lgFlag == 14)
            {
                url = "https://graph.renren.com/oauth/authorize?client_id=" + "0722943e9e3e4eafb5d38ed5e0ed3bfe" + "&response_type=code&token&redirect_uri=" + Uri.encode("http://www.ximalaya.com/passport/3/access") + "&display=mobile&token_type=mac&scope=" + Uri.encode("publish_share publish_feed send_invitation operate_like");
                return;
            }
        }
    }

    private void setUpWebView()
    {
        mWebView = (WebView)findViewById(0x7f0a004f);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.setWebViewClient(new b());
        try
        {
            mWebView.loadUrl(new String(url.getBytes("UTF-8")));
            return;
        }
        catch (UnsupportedEncodingException unsupportedencodingexception)
        {
            unsupportedencodingexception.printStackTrace();
        }
    }

    private ProgressDialog showProgressDialog()
    {
        if (mProgressDialog == null)
        {
            MyProgressDialog myprogressdialog = new MyProgressDialog(this);
            myprogressdialog.setTitle(0x7f0900b2);
            myprogressdialog.setMessage(getString(0x7f0900b7));
            myprogressdialog.setIndeterminate(true);
            myprogressdialog.setCancelable(true);
            mProgressDialog = myprogressdialog;
        }
        mProgressDialog.show();
        return mProgressDialog;
    }

    public void dialog(String s)
    {
        (new DialogBuilder(this)).setMessage(s).showWarning();
    }

    public void finish()
    {
        mGetAccessTokenTask = null;
        if (lgFlag == 12 || lgFlag == 13 || lgFlag == 14)
        {
            if (isBind)
            {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("info", bindStatus);
                intent.putExtras(bundle);
                setResult(0, intent);
            } else
            {
                setResult(1);
            }
        }
        super.finish();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        mSpinner = new ProgressDialog(this);
        mSpinner.requestWindowFeature(1);
        mSpinner.setMessage("\u52A0\u8F7D\u4E2D...");
        setContentView(0x7f030046);
        mCt = getApplicationContext();
        getParentInfo();
        parseUrl();
        intiUI();
        setUpWebView();
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        if (i == 4)
        {
            if (lgFlag == 12 || lgFlag == 13 || lgFlag == 14)
            {
                setResult(1);
            }
            finish();
            return true;
        } else
        {
            return false;
        }
    }

    public void onPause()
    {
        if (FreeFlowUtil.getInstance().isNeedFreeFlowProxy())
        {
            FreeFlowUtil.getInstance().setProxyForGlobalHttpClient();
        }
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        if (FreeFlowUtil.getInstance().isNeedFreeFlowProxy())
        {
            FreeFlowUtil.getInstance().removeProxyForGlobalHttpClient();
        }
    }






/*
    static BindStatus access$302(AuthorizeActivity authorizeactivity, BindStatus bindstatus)
    {
        authorizeactivity.bindStatus = bindstatus;
        return bindstatus;
    }

*/




/*
    static a access$502(AuthorizeActivity authorizeactivity, a a)
    {
        authorizeactivity.mGetAccessTokenTask = a;
        return a;
    }

*/




/*
    static boolean access$702(AuthorizeActivity authorizeactivity, boolean flag)
    {
        authorizeactivity.hasAccessToken = flag;
        return flag;
    }

*/
}
