// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            i, j, k, m

public class RegisterVerifyCode extends BaseActivity
{
    private class a extends MyAsyncTask
    {

        final RegisterVerifyCode a;

        protected transient LoginInfoModel a(Void avoid[])
        {
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/register").toString();
            RequestParams requestparams = new RequestParams();
            requestparams.put("account", a.loginInfoModel.account);
            requestparams.put("nickname", a.loginInfoModel.nickname);
            requestparams.put("password", a.loginInfoModel.password);
            requestparams.put("checkCode", a.vcEditText.getText().toString().trim());
            avoid = f.a().b(avoid, requestparams, a.submitButton, null);
            Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
            a.loginInfoModel.ret = -1;
            a.loginInfoModel.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5931\u8D25\uFF0C\u8BF7\u68C0\u67E5\u662F\u5426\u8FDE\u63A5\u7F51\u7EDC";
            if (avoid == null) goto _L2; else goto _L1
_L1:
            Object obj = JSON.parseObject(avoid);
            if (!"0".equals(((JSONObject) (obj)).get("ret").toString())) goto _L4; else goto _L3
_L3:
            obj = SharedPreferencesUtil.getInstance(a.mCt);
            ((SharedPreferencesUtil) (obj)).saveString("account", a.loginInfoModel.account);
            ((SharedPreferencesUtil) (obj)).saveString("password", a.loginInfoModel.password);
            a.loginInfoModel = (LoginInfoModel)JSON.parseObject(avoid, com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
            ((SharedPreferencesUtil) (obj)).saveString("loginforesult", avoid);
            UserInfoMannage.getInstance().setUser(a.loginInfoModel);
_L2:
            return a.loginInfoModel;
_L4:
            try
            {
                a.loginInfoModel.ret = -1;
                a.loginInfoModel.msg = ((JSONObject) (obj)).getString("msg");
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
                a.loginInfoModel.ret = -1;
                a.loginInfoModel.msg = "\u89E3\u6790json\u5F02\u5E38";
            }
            if (true) goto _L2; else goto _L5
_L5:
        }

        protected void a(LoginInfoModel logininfomodel)
        {
            super.onPostExecute(logininfomodel);
            if (a == null || a.isFinishing())
            {
                return;
            }
            a.dismissProgressDialog();
            if (logininfomodel.ret == 0)
            {
                Toast.makeText(a, a.getString(0x7f0900b4), 1).show();
                logininfomodel = new Intent(a, com/ximalaya/ting/android/activity/MainTabActivity2);
                logininfomodel.putExtra("isLogin", true);
                logininfomodel.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(a.submitButton));
                a.startActivity(logininfomodel);
                a.finish();
                return;
            } else
            {
                a.dialog(logininfomodel.msg);
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onCancelled()
        {
            super.onCancelled();
            a.dismissProgressDialog();
        }

        protected void onPostExecute(Object obj)
        {
            a((LoginInfoModel)obj);
        }

        protected void onPreExecute()
        {
            a.showProgressDialog();
            super.onPreExecute();
        }

        private a()
        {
            a = RegisterVerifyCode.this;
            super();
        }

        a(i l)
        {
            this();
        }
    }


    private Context mCt;
    private Handler mHandler;
    private ProgressDialog mProgressDialog;
    private TextView phone;
    private Button reGetVcButton;
    private Button submitButton;
    private TimerTask task;
    private int time;
    private Timer timer;
    private EditText vcEditText;

    public RegisterVerifyCode()
    {
        time = 180;
        mHandler = new i(this);
    }

    private void dismissProgressDialog()
    {
        try
        {
            mProgressDialog.dismiss();
            return;
        }
        catch (IllegalArgumentException illegalargumentexception)
        {
            return;
        }
    }

    private void initData()
    {
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        if (loginInfoModel != null)
        {
            phone.setText(loginInfoModel.account);
        }
    }

    private void initListener()
    {
        submitButton.setOnClickListener(new j(this));
        reGetVcButton.setOnClickListener(new k(this));
    }

    private void initUI()
    {
        initCommon();
        setTitleText("\u6CE8\u518C");
        phone = (TextView)findViewById(0x7f0a06af);
        vcEditText = (EditText)findViewById(0x7f0a06b0);
        submitButton = (Button)findViewById(0x7f0a06ad);
        reGetVcButton = (Button)findViewById(0x7f0a0184);
    }

    private ProgressDialog showProgressDialog()
    {
        if (mProgressDialog == null)
        {
            MyProgressDialog myprogressdialog = new MyProgressDialog(this);
            myprogressdialog.setTitle(0x7f0900b6);
            myprogressdialog.setMessage(getString(0x7f0900b7));
            myprogressdialog.setIndeterminate(true);
            myprogressdialog.setCancelable(true);
            mProgressDialog = myprogressdialog;
        }
        mProgressDialog.show();
        return mProgressDialog;
    }

    public void dialog(String s)
    {
        vcEditText.setText("");
        (new DialogBuilder(this)).setMessage(s).showWarning();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f0301bc);
        mCt = getApplicationContext();
        initUI();
        initData();
        initListener();
        timer = new Timer();
        task = new m(this);
        timer.schedule(task, 0L, 1000L);
    }








/*
    static int access$502(RegisterVerifyCode registerverifycode, int l)
    {
        registerverifycode.time = l;
        return l;
    }

*/


/*
    static int access$506(RegisterVerifyCode registerverifycode)
    {
        int l = registerverifycode.time - 1;
        registerverifycode.time = l;
        return l;
    }

*/


}
