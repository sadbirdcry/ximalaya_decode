// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.ximalaya.ting.android.activity.BaseActivity;

public class Reg_AgreementActivity extends BaseActivity
{
    private class a extends WebViewClient
    {

        final Reg_AgreementActivity a;

        public void onPageFinished(WebView webview, String s)
        {
            super.onPageFinished(webview, s);
            a.loadBar.setVisibility(4);
        }

        public void onPageStarted(WebView webview, String s, Bitmap bitmap)
        {
            super.onPageStarted(webview, s, bitmap);
        }

        public boolean shouldOverrideUrlLoading(WebView webview, String s)
        {
            return super.shouldOverrideUrlLoading(webview, s);
        }

        private a()
        {
            a = Reg_AgreementActivity.this;
            super();
        }

    }


    private static final String url = "http://m.ximalaya.com/passport/register_rule";
    private ProgressBar loadBar;
    private WebView mWebView;

    public Reg_AgreementActivity()
    {
    }

    private void initUI()
    {
        initCommon();
        setTitleText("\u6CE8\u518C\u534F\u8BAE");
        loadBar = (ProgressBar)findViewById(0x7f0a0446);
        setUpWebView();
    }

    private void setUpWebView()
    {
        mWebView = (WebView)findViewById(0x7f0a0445);
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new a());
        mWebView.loadUrl("http://m.ximalaya.com/passport/register_rule");
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030107);
        initUI();
    }

}
