// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseFragmentActivity;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.login.RegisterStepOneFragment;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.verification_code.VerificationCodeModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.MyVerificationCodeDialogFragment;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            d, e, f, g, 
//            b, RegisterVerifyCode, h

public class RegisterActivity extends BaseFragmentActivity
{
    public class RegisterTask extends MyAsyncTask
    {

        final RegisterActivity this$0;

        protected transient LoginInfoModel doInBackground(Void avoid[])
        {
            Object obj;
            Object obj1;
            obj = null;
            avoid = null;
            obj1 = new RequestParams();
            if (type != 1) goto _L2; else goto _L1
_L1:
            obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/register").toString();
            ((RequestParams) (obj1)).put("account", edText1.getText().toString().trim());
            ((RequestParams) (obj1)).put("nickname", edText2.getText().toString().trim());
            ((RequestParams) (obj1)).put("password", edText3.getText().toString().trim());
            ((RequestParams) (obj1)).put("checkUUID", checkUUId);
            ((RequestParams) (obj1)).put("checkCode", checkCode);
            ((RequestParams) (obj1)).put("xum", ToolUtil.getLocalMacAddress(mCt));
            obj1 = f.a().b(((String) (obj)), ((RequestParams) (obj1)), btnSbm, null);
            Logger.log((new StringBuilder()).append("result:").append(((String) (obj1))).toString());
            if (!Utilities.isNotBlank(((String) (obj1))))
            {
                break MISSING_BLOCK_LABEL_288;
            }
            try
            {
                obj = (LoginInfoModel)JSON.parseObject(((String) (obj1)), com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
                avoid = new LoginInfoModel();
                avoid.msg = "\u89E3\u6790json\u5F02\u5E38";
                return avoid;
            }
            avoid = ((Void []) (obj));
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_288;
            }
            avoid = ((Void []) (obj));
            if (((LoginInfoModel) (obj)).ret != 0)
            {
                break MISSING_BLOCK_LABEL_288;
            }
            avoid = SharedPreferencesUtil.getInstance(mCt);
            avoid.saveString("account", edText1.getText().toString().trim());
            avoid.saveString("password", edText3.getText().toString().trim());
            avoid.saveString("loginforesult", ((String) (obj1)));
            avoid = ((Void []) (obj));
_L4:
            return avoid;
_L2:
            String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/register/checkinfo").toString();
            ((RequestParams) (obj1)).put("account", edText1.getText().toString().trim());
            ((RequestParams) (obj1)).put("nickname", edText2.getText().toString().trim());
            ((RequestParams) (obj1)).put("password", edText3.getText().toString().trim());
            obj1 = f.a().b(s, ((RequestParams) (obj1)), btnSbm, null);
            Logger.log((new StringBuilder()).append("result:").append(((String) (obj1))).toString());
            if (!Utilities.isNotBlank(((String) (obj1)))) goto _L4; else goto _L3
_L3:
            avoid = new LoginInfoModel();
            obj = JSON.parseObject(((String) (obj1)));
            if (!"0".equals(((JSONObject) (obj)).get("ret").toString())) goto _L6; else goto _L5
_L5:
            avoid.ret = 0;
            avoid.account = edText1.getText().toString().trim();
            avoid.nickname = edText2.getText().toString().trim();
            avoid.password = edText3.getText().toString().trim();
              goto _L7
_L6:
            avoid.msg = ((JSONObject) (obj)).getString("msg");
              goto _L7
            obj;
_L8:
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
            avoid.ret = -1;
            avoid.msg = "\u89E3\u6790json\u5F02\u5E38";
            return avoid;
            avoid;
            avoid = ((Void []) (obj));
            if (true) goto _L8; else goto _L7
_L7:
            return avoid;
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onCancelled()
        {
            super.onCancelled();
            dismissProgressDialog();
        }

        protected void onPostExecute(LoginInfoModel logininfomodel)
        {
            super.onPostExecute(logininfomodel);
            if (RegisterActivity.this != null && !isFinishing())
            {
                dismissProgressDialog();
                if (logininfomodel != null)
                {
                    if (logininfomodel.ret == 0)
                    {
                        UserInfoMannage.getInstance().setUser(logininfomodel);
                        if (type == 1)
                        {
                            logininfomodel = new Intent(RegisterActivity.this, com/ximalaya/ting/android/activity/MainTabActivity2);
                            logininfomodel.putExtra("isLogin", true);
                            startActivity(logininfomodel);
                            finish();
                            return;
                        }
                        if (type == 0)
                        {
                            Toast.makeText(mCt, 0x7f0900be, 1).show();
                            logininfomodel = new Intent(RegisterActivity.this, com/ximalaya/ting/android/activity/login/RegisterVerifyCode);
                            startActivity(logininfomodel);
                            return;
                        }
                    } else
                    {
                        dismissProgressDialog();
                        dialog(logininfomodel);
                        return;
                    }
                } else
                {
                    dialog("\u7F51\u7EDC\u8FDE\u63A5\u5931\u8D25\uFF0C\u8BF7\u68C0\u67E5\u662F\u5426\u8FDE\u63A5\u7F51\u7EDC");
                    return;
                }
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((LoginInfoModel)obj);
        }

        protected void onPreExecute()
        {
            showProgressDialog();
            super.onPreExecute();
        }

        public RegisterTask()
        {
            this$0 = RegisterActivity.this;
            super();
        }
    }

    class a extends MyAsyncTask
    {

        final RegisterActivity a;

        protected transient VerificationCodeModel a(Object aobj[])
        {
            aobj = (View)aobj[0];
            return CommonRequest.getCheckCode(a.getApplicationContext(), "from_register", 0L, null, ((View) (aobj)));
        }

        protected void a(VerificationCodeModel verificationcodemodel)
        {
            a.dismissProgressDialog();
            if (a.isFinishing())
            {
                return;
            }
            if (verificationcodemodel == null)
            {
                Toast.makeText(a.getApplicationContext(), "\u83B7\u53D6\u9A8C\u8BC1\u7801\u9519\u8BEF\uFF0C\u8BF7\u91CD\u8BD5", 0).show();
                return;
            }
            if (verificationcodemodel.ret == 0)
            {
                a.checkUUId = verificationcodemodel.checkUUID;
                if (a.dialogFra == null)
                {
                    a.dialogFra = new MyVerificationCodeDialogFragment("from_register", verificationcodemodel.checkCodeUrl, new h(this));
                }
                if (!a.dialogFra.isAdded())
                {
                    a.dialogFra.show(a.getSupportFragmentManager(), "from_register", verificationcodemodel.checkCodeUrl);
                    return;
                } else
                {
                    a.dialogFra.dismiss();
                    a.dialogFra.show(a.getSupportFragmentManager(), "from_register", verificationcodemodel.checkCodeUrl);
                    return;
                }
            } else
            {
                Toast.makeText(a.getApplicationContext(), verificationcodemodel.msg, 0).show();
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((VerificationCodeModel)obj);
        }

        protected void onPreExecute()
        {
            a.showProgressDialog();
        }

        a()
        {
            a = RegisterActivity.this;
            super();
        }
    }


    Button btnE;
    Button btnP;
    Button btnSbm;
    private String checkCode;
    private String checkUUId;
    private MyVerificationCodeDialogFragment dialogFra;
    EditText edText1;
    EditText edText2;
    EditText edText3;
    private Context mCt;
    private ProgressDialog mProgressDialog;
    TextView reg_agreement;
    int type;

    public RegisterActivity()
    {
        type = 0;
    }

    private void dismissProgressDialog()
    {
        try
        {
            mProgressDialog.dismiss();
            return;
        }
        catch (IllegalArgumentException illegalargumentexception)
        {
            return;
        }
    }

    private void initData()
    {
        btnE.setSelected(true);
    }

    private void initListener()
    {
        btnE.setOnClickListener(new d(this));
        btnP.setOnClickListener(new e(this));
        btnSbm.setOnClickListener(new com.ximalaya.ting.android.activity.login.f(this));
        reg_agreement.setOnClickListener(new g(this));
    }

    private void initUi()
    {
        initCommon();
        setTitleText("\u6CE8\u518C");
        reg_agreement = (TextView)findViewById(0x7f0a06ae);
        btnE = (Button)findViewById(0x7f0a06a7);
        btnP = (Button)findViewById(0x7f0a06a8);
        btnSbm = (Button)findViewById(0x7f0a06ad);
        edText1 = (EditText)findViewById(0x7f0a06aa);
        edText2 = (EditText)findViewById(0x7f0a06ab);
        edText3 = (EditText)findViewById(0x7f0a06ac);
    }

    private void setEdittextHint()
    {
        if (type == 0)
        {
            edText1.setHint(0x7f0900bc);
            edText2.setHint(0x7f0900c1);
        } else
        {
            edText1.setHint(0x7f0900c2);
            edText2.setHint(0x7f0900c1);
        }
        edText1.setText("");
        edText2.setText("");
        edText3.setText("");
    }

    private ProgressDialog showProgressDialog()
    {
        if (mProgressDialog == null)
        {
            mProgressDialog = new MyProgressDialog(this);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
        }
        mProgressDialog.show();
        return mProgressDialog;
    }

    public void dialog(LoginInfoModel logininfomodel)
    {
        (new DialogBuilder(this)).setMessage(logininfomodel.msg).setOkBtn(new b(this, logininfomodel)).showWarning();
    }

    public void dialog(String s)
    {
        (new DialogBuilder(this)).setMessage(s).showWarning();
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03001b);
        mCt = getApplicationContext();
        addFragmentToLayout(0x7f0a00bc, RegisterStepOneFragment.newInstance());
    }




/*
    static String access$102(RegisterActivity registeractivity, String s)
    {
        registeractivity.checkUUId = s;
        return s;
    }

*/



/*
    static MyVerificationCodeDialogFragment access$202(RegisterActivity registeractivity, MyVerificationCodeDialogFragment myverificationcodedialogfragment)
    {
        registeractivity.dialogFra = myverificationcodedialogfragment;
        return myverificationcodedialogfragment;
    }

*/



/*
    static String access$302(RegisterActivity registeractivity, String s)
    {
        registeractivity.checkCode = s;
        return s;
    }

*/



}
