// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.auth.AuthInfo;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.BindStatus;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            AuthorizeActivity

public class this._cls0 extends MyAsyncTask
{

    AuthInfo authInfo;
    final AuthorizeActivity this$0;

    protected transient BindStatus doInBackground(Object aobj[])
    {
        Object obj;
        JSONObject jsonobject;
        RequestParams requestparams;
        jsonobject = null;
        authInfo = (AuthInfo)aobj[0];
        requestparams = new RequestParams();
        if (AuthorizeActivity.access$000(AuthorizeActivity.this) == 12)
        {
            aobj = (new StringBuilder()).append(a.u).append("mobile/").append(1).append("/bind").toString();
            requestparams.put("openid", authInfo.uid);
            requestparams.put("expiresIn", authInfo.expires_in);
        } else
        {
label0:
            {
                if (AuthorizeActivity.access$000(AuthorizeActivity.this) != 13)
                {
                    break label0;
                }
                aobj = (new StringBuilder()).append(a.u).append("mobile/").append(2).append("/bind").toString();
                requestparams.put("openid", authInfo.openid);
                requestparams.put("expiresIn", authInfo.expires_in);
            }
        }
_L4:
        requestparams.put("accessToken", authInfo.access_token);
        requestparams.put("deviceToken", ToolUtil.getDeviceToken(AuthorizeActivity.access$100(AuthorizeActivity.this)));
        aobj = f.a().a(((String) (aobj)), requestparams, null);
        if (((com.ximalaya.ting.android.b.ity) (aobj)).ity == 1)
        {
            aobj = ((com.ximalaya.ting.android.b.ity) (aobj)).ity;
        } else
        {
            aobj = null;
        }
        Logger.log((new StringBuilder()).append("result:").append(((String) (aobj))).toString());
        obj = jsonobject;
        if (aobj == null) goto _L2; else goto _L1
_L1:
        obj = new BindStatus();
        jsonobject = JSON.parseObject(((String) (aobj)));
        if (!"0".equals(jsonobject.get("ret").toString()))
        {
            break MISSING_BLOCK_LABEL_369;
        }
        aobj = (BindStatus)JSONObject.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/personal_setting/BindStatus);
_L5:
        obj = ((Object) (aobj));
_L2:
        return ((BindStatus) (obj));
        obj = jsonobject;
        if (AuthorizeActivity.access$000(AuthorizeActivity.this) != 14) goto _L2; else goto _L3
_L3:
        aobj = (new StringBuilder()).append(a.u).append("mobile/").append(3).append("/bind").toString();
        requestparams.put("openid", authInfo.openid);
        requestparams.put("macKey", authInfo.macKey);
          goto _L4
        try
        {
            obj.ret = jsonobject.getIntValue("ret");
            obj.msg = jsonobject.getString("msg");
        }
        // Misplaced declaration of an exception variable
        catch (Object aobj[])
        {
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
            obj.ret = -1;
            obj.msg = "\u89E3\u6790json\u5F02\u5E38";
            return ((BindStatus) (obj));
        }
        aobj = ((Object []) (obj));
          goto _L5
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onCancelled()
    {
        super.onCancelled();
        AuthorizeActivity.access$200(AuthorizeActivity.this);
    }

    protected void onPostExecute(BindStatus bindstatus)
    {
        if (AuthorizeActivity.this == null || isFinishing())
        {
            return;
        }
        AuthorizeActivity.access$200(AuthorizeActivity.this);
        if (bindstatus != null)
        {
            if (loginInfoModel != null && bindstatus.ret == 0 && bindstatus.data != null && bindstatus.data.size() > 0)
            {
                if (loginInfoModel.bindStatus == null)
                {
                    loginInfoModel.bindStatus = new ArrayList();
                }
                loginInfoModel.bindStatus.add(bindstatus.data.get(0));
                AuthorizeActivity.access$302(AuthorizeActivity.this, bindstatus);
                isBind = true;
                finish();
                return;
            } else
            {
                dialog(bindstatus.msg);
                return;
            }
        } else
        {
            dialog("\u7F51\u7EDC\u8BBF\u95EE\u8D85\u65F6");
            return;
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((BindStatus)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        android.app.ProgressDialog _tmp = AuthorizeActivity.access$400(AuthorizeActivity.this);
    }

    public I()
    {
        this$0 = AuthorizeActivity.this;
        super();
    }
}
