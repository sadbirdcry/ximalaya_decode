// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            k, RegisterVerifyCode

class l extends MyAsyncTask
{

    final k a;

    l(k k1)
    {
        a = k1;
        super();
    }

    protected transient String a(Void avoid[])
    {
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/register/checkinfo").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("phone_num", a.a.loginInfoModel.account);
        requestparams.put("account", a.a.loginInfoModel.account);
        requestparams.put("nickname", a.a.loginInfoModel.nickname);
        requestparams.put("password", a.a.loginInfoModel.password);
        avoid = f.a().b(avoid, requestparams, RegisterVerifyCode.access$600(a.a), RegisterVerifyCode.access$600(a.a));
        Logger.log((new StringBuilder()).append("result:").append(avoid).toString());
        if (avoid == null)
        {
            break MISSING_BLOCK_LABEL_215;
        }
        avoid = JSON.parseObject(avoid);
        if (avoid.getIntValue("ret") == 0)
        {
            return "0";
        }
        try
        {
            avoid = avoid.getString("msg");
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
            return "\u89E3\u6790json\u5F02\u5E38";
        }
        return avoid;
        return "\u7F51\u7EDC\u8FDE\u63A5\u5931\u8D25\uFF0C\u8BF7\u68C0\u67E5\u662F\u5426\u8FDE\u63A5\u7F51\u7EDC";
    }

    protected void a(String s)
    {
        if (a.a == null || a.a.isFinishing())
        {
            return;
        }
        if ("0".equals(s))
        {
            Toast.makeText(RegisterVerifyCode.access$200(a.a), 0x7f0900be, 1).show();
            return;
        } else
        {
            a.a.dialog(s);
            return;
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((String)obj);
    }
}
