// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            AuthorizeActivity

private class <init> extends WebViewClient
{

    final AuthorizeActivity a;

    public void onPageFinished(WebView webview, String s)
    {
        if (a != null && !a.isFinishing())
        {
            if (AuthorizeActivity.access$600(a).isShowing())
            {
                AuthorizeActivity.access$600(a).dismiss();
            }
            if (s.contains("error"))
            {
                Toast.makeText(a, a.getString(0x7f0900ac), 1).show();
            }
            if ((s.startsWith("http://www.ximalaya.com") || s.startsWith("http://test.ximalaya.com") || s.startsWith("http://m.ximalaya.com") && AuthorizeActivity.access$500(a) == null) && !s.contains("error") && s.contains("code") && !AuthorizeActivity.access$700(a))
            {
                webview = s.substring(s.indexOf("code="), s.length());
                webview = webview.substring(5, webview.length());
                AuthorizeActivity.access$502(a, new <init>(a));
                AuthorizeActivity.access$500(a).myexec(new Object[] {
                    webview
                });
                return;
            }
        }
    }

    public void onPageStarted(WebView webview, String s, Bitmap bitmap)
    {
        if (a == null || a.isFinishing())
        {
            return;
        }
        AuthorizeActivity.access$600(a).show();
        AuthorizeActivity.access$702(a, false);
        if ((s.startsWith("http://www.ximalaya.com") || s.startsWith("http://test.ximalaya.com") || s.startsWith("http://m.ximalaya.com")) && !s.contains("error") && s.contains("code") && AuthorizeActivity.access$500(a) == null)
        {
            bitmap = s.substring(s.indexOf("code="), s.length());
            bitmap = bitmap.substring(5, bitmap.length());
            AuthorizeActivity.access$702(a, true);
            AuthorizeActivity.access$502(a, new <init>(a));
            AuthorizeActivity.access$500(a).myexec(new Object[] {
                bitmap
            });
            webview.stopLoading();
            Logger.log((new StringBuilder()).append("url1:").append(s).toString());
            return;
        } else
        {
            Logger.log((new StringBuilder()).append("url0:").append(s).toString());
            super.onPageStarted(webview, s, bitmap);
            return;
        }
    }

    private (AuthorizeActivity authorizeactivity)
    {
        a = authorizeactivity;
        super();
    }

    a(AuthorizeActivity authorizeactivity, a a1)
    {
        this(authorizeactivity);
    }
}
