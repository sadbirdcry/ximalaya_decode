// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.content.Intent;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.auth.AuthInfo;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            AuthorizeActivity

public class this._cls0 extends MyAsyncTask
{

    AuthInfo authInfo;
    final AuthorizeActivity this$0;

    protected transient LoginInfoModel doInBackground(Object aobj[])
    {
        Object obj;
        Object obj1;
        obj = null;
        obj1 = new RequestParams();
        authInfo = (AuthInfo)aobj[0];
        if (AuthorizeActivity.access$000(AuthorizeActivity.this) == 1)
        {
            aobj = (new StringBuilder()).append(a.u).append("mobile/").append(1).append("/access").toString();
            ((RequestParams) (obj1)).put("openid", authInfo.uid);
        } else
        {
label0:
            {
                if (AuthorizeActivity.access$000(AuthorizeActivity.this) != 2)
                {
                    break label0;
                }
                aobj = (new StringBuilder()).append(a.u).append("mobile/").append(2).append("/access").toString();
                ((RequestParams) (obj1)).put("openid", authInfo.openid);
            }
        }
_L5:
        ((RequestParams) (obj1)).put("accessToken", authInfo.access_token);
        ((RequestParams) (obj1)).put("expiresIn", authInfo.expires_in);
        ((RequestParams) (obj1)).put("deviceToken", ToolUtil.getDeviceToken(AuthorizeActivity.access$100(AuthorizeActivity.this)));
        aobj = f.a().a(((String) (aobj)), ((RequestParams) (obj1)), null);
        if (((com.ximalaya.ting.android.b.ty) (aobj)).ty == 1)
        {
            obj = ((com.ximalaya.ting.android.b.ty) (aobj)).ty;
        } else
        {
            obj = null;
        }
        Logger.log((new StringBuilder()).append("result:").append(((String) (obj))).toString());
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_412;
        }
        aobj = new LoginInfoModel();
        obj1 = JSON.parseObject(((String) (obj)));
        if (!"0".equals(((JSONObject) (obj1)).get("ret").toString())) goto _L2; else goto _L1
_L1:
        obj1 = (LoginInfoModel)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
        SharedPreferencesUtil.getInstance(AuthorizeActivity.access$100(AuthorizeActivity.this)).saveString("loginforesult", ((String) (obj)));
        aobj = ((Object []) (obj1));
_L4:
        return ((LoginInfoModel) (aobj));
        aobj = ((Object []) (obj));
        if (AuthorizeActivity.access$000(AuthorizeActivity.this) != 3) goto _L4; else goto _L3
_L3:
        aobj = (new StringBuilder()).append(a.u).append("mobile/").append(3).append("/access").toString();
        ((RequestParams) (obj1)).put("openid", authInfo.openid);
          goto _L5
_L2:
        aobj.ret = ((JSONObject) (obj1)).getIntValue("ret");
        aobj.msg = ((JSONObject) (obj1)).getString("msg");
          goto _L4
        obj;
_L6:
        Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
        aobj.ret = -1;
        aobj.msg = "\u89E3\u6790json\u5F02\u5E38";
          goto _L4
        aobj;
        aobj = ((Object []) (obj1));
          goto _L6
        aobj = null;
          goto _L4
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onCancelled()
    {
        super.onCancelled();
        AuthorizeActivity.access$200(AuthorizeActivity.this);
    }

    protected void onPostExecute(LoginInfoModel logininfomodel)
    {
        if (AuthorizeActivity.this == null || isFinishing())
        {
            return;
        }
        AuthorizeActivity.access$200(AuthorizeActivity.this);
        if (logininfomodel != null)
        {
            if (logininfomodel.ret == 0)
            {
                UserInfoMannage.getInstance().setUser(logininfomodel);
                logininfomodel = new Intent(AuthorizeActivity.this, com/ximalaya/ting/android/activity/MainTabActivity2);
                logininfomodel.putExtra("isLogin", true);
                startActivity(logininfomodel);
                finish();
                return;
            } else
            {
                dialog(logininfomodel.msg);
                return;
            }
        } else
        {
            dialog("\u7F51\u7EDC\u8BBF\u95EE\u8D85\u65F6");
            return;
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((LoginInfoModel)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        android.app.ProgressDialog _tmp = AuthorizeActivity.access$400(AuthorizeActivity.this);
    }

    public ()
    {
        this$0 = AuthorizeActivity.this;
        super();
    }
}
