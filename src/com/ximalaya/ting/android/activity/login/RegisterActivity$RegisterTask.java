// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.login;

import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.activity.login:
//            RegisterActivity, RegisterVerifyCode

public class this._cls0 extends MyAsyncTask
{

    final RegisterActivity this$0;

    protected transient LoginInfoModel doInBackground(Void avoid[])
    {
        Object obj;
        Object obj1;
        obj = null;
        avoid = null;
        obj1 = new RequestParams();
        if (type != 1) goto _L2; else goto _L1
_L1:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/register").toString();
        ((RequestParams) (obj1)).put("account", edText1.getText().toString().trim());
        ((RequestParams) (obj1)).put("nickname", edText2.getText().toString().trim());
        ((RequestParams) (obj1)).put("password", edText3.getText().toString().trim());
        ((RequestParams) (obj1)).put("checkUUID", RegisterActivity.access$100(RegisterActivity.this));
        ((RequestParams) (obj1)).put("checkCode", RegisterActivity.access$300(RegisterActivity.this));
        ((RequestParams) (obj1)).put("xum", ToolUtil.getLocalMacAddress(RegisterActivity.access$500(RegisterActivity.this)));
        obj1 = f.a().b(((String) (obj)), ((RequestParams) (obj1)), btnSbm, null);
        Logger.log((new StringBuilder()).append("result:").append(((String) (obj1))).toString());
        if (!Utilities.isNotBlank(((String) (obj1))))
        {
            break MISSING_BLOCK_LABEL_288;
        }
        try
        {
            obj = (LoginInfoModel)JSON.parseObject(((String) (obj1)), com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
            avoid = new LoginInfoModel();
            avoid.msg = "\u89E3\u6790json\u5F02\u5E38";
            return avoid;
        }
        avoid = ((Void []) (obj));
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_288;
        }
        avoid = ((Void []) (obj));
        if (((LoginInfoModel) (obj)).ret != 0)
        {
            break MISSING_BLOCK_LABEL_288;
        }
        avoid = SharedPreferencesUtil.getInstance(RegisterActivity.access$500(RegisterActivity.this));
        avoid.saveString("account", edText1.getText().toString().trim());
        avoid.saveString("password", edText3.getText().toString().trim());
        avoid.saveString("loginforesult", ((String) (obj1)));
        avoid = ((Void []) (obj));
_L4:
        return avoid;
_L2:
        String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/register/checkinfo").toString();
        ((RequestParams) (obj1)).put("account", edText1.getText().toString().trim());
        ((RequestParams) (obj1)).put("nickname", edText2.getText().toString().trim());
        ((RequestParams) (obj1)).put("password", edText3.getText().toString().trim());
        obj1 = f.a().b(s, ((RequestParams) (obj1)), btnSbm, null);
        Logger.log((new StringBuilder()).append("result:").append(((String) (obj1))).toString());
        if (!Utilities.isNotBlank(((String) (obj1)))) goto _L4; else goto _L3
_L3:
        avoid = new LoginInfoModel();
        obj = JSON.parseObject(((String) (obj1)));
        if (!"0".equals(((JSONObject) (obj)).get("ret").toString())) goto _L6; else goto _L5
_L5:
        avoid.ret = 0;
        avoid.account = edText1.getText().toString().trim();
        avoid.nickname = edText2.getText().toString().trim();
        avoid.password = edText3.getText().toString().trim();
          goto _L7
_L6:
        avoid.msg = ((JSONObject) (obj)).getString("msg");
          goto _L7
        obj;
_L8:
        Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
        avoid.ret = -1;
        avoid.msg = "\u89E3\u6790json\u5F02\u5E38";
        return avoid;
        avoid;
        avoid = ((Void []) (obj));
        if (true) goto _L8; else goto _L7
_L7:
        return avoid;
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onCancelled()
    {
        super.onCancelled();
        RegisterActivity.access$000(RegisterActivity.this);
    }

    protected void onPostExecute(LoginInfoModel logininfomodel)
    {
        super.onPostExecute(logininfomodel);
        if (RegisterActivity.this != null && !isFinishing())
        {
            RegisterActivity.access$000(RegisterActivity.this);
            if (logininfomodel != null)
            {
                if (logininfomodel.ret == 0)
                {
                    UserInfoMannage.getInstance().setUser(logininfomodel);
                    if (type == 1)
                    {
                        logininfomodel = new Intent(RegisterActivity.this, com/ximalaya/ting/android/activity/MainTabActivity2);
                        logininfomodel.putExtra("isLogin", true);
                        startActivity(logininfomodel);
                        finish();
                        return;
                    }
                    if (type == 0)
                    {
                        Toast.makeText(RegisterActivity.access$500(RegisterActivity.this), 0x7f0900be, 1).show();
                        logininfomodel = new Intent(RegisterActivity.this, com/ximalaya/ting/android/activity/login/RegisterVerifyCode);
                        startActivity(logininfomodel);
                        return;
                    }
                } else
                {
                    RegisterActivity.access$000(RegisterActivity.this);
                    dialog(logininfomodel);
                    return;
                }
            } else
            {
                dialog("\u7F51\u7EDC\u8FDE\u63A5\u5931\u8D25\uFF0C\u8BF7\u68C0\u67E5\u662F\u5426\u8FDE\u63A5\u7F51\u7EDC");
                return;
            }
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((LoginInfoModel)obj);
    }

    protected void onPreExecute()
    {
        android.app.ProgressDialog _tmp = RegisterActivity.access$400(RegisterActivity.this);
        super.onPreExecute();
    }

    public ()
    {
        this$0 = RegisterActivity.this;
        super();
    }
}
