// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity;

import android.widget.ImageView;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.view.RoundedImageView;

// Referenced classes of package com.ximalaya.ting.android.activity:
//            MainTabActivity2

class m
    implements com.ximalaya.ting.android.service.play.LocalMediaService.OnPlayServiceUpdateListener
{

    final MainTabActivity2 a;

    m(MainTabActivity2 maintabactivity2)
    {
        a = maintabactivity2;
        super();
    }

    public void onPlayCanceled()
    {
        if (MainTabActivity2.access$1800(a) == null)
        {
            return;
        } else
        {
            MainTabActivity2.access$900(a).clearAnimation();
            MainTabActivity2.access$900(a).setImageDrawable(null);
            MainTabActivity2.access$900(a).setTag(null);
            MainTabActivity2.access$1900(a).setVisibility(0);
            return;
        }
    }

    public void onSoundChanged(int i)
    {
        if (PlayListControl.getPlayListManager().getCurSound() != null)
        {
            a.setPlayButtonCover(PlayListControl.getPlayListManager().getCurSound().coverSmall);
        }
    }

    public void onSoundInfoChanged(int i, SoundInfo soundinfo)
    {
    }
}
