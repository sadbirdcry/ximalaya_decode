// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.play;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import com.ximalaya.ting.android.util.ToolUtil;

public class HtmlImageGetter
    implements android.text.Html.ImageGetter
{

    private Context mContext;
    private int mImageHeight;
    private int mImageWidth;
    private Resources mResources;

    public HtmlImageGetter(Context context, int i, int j)
    {
        mContext = context;
        mResources = context.getResources();
        mImageWidth = ToolUtil.dp2px(mContext, i);
        mImageHeight = ToolUtil.dp2px(mContext, j);
    }

    public Drawable getDrawable(String s)
    {
        Object obj = "";
        if (TextUtils.isEmpty(s))
        {
            s = null;
        } else
        {
            if (s.contains("."))
            {
                obj = s.substring(0, s.indexOf("."));
            }
            if (s.contains("/"))
            {
                obj = s.substring(s.lastIndexOf("/") + 1, s.length());
            }
            s = mResources.getResourcePackageName(0x7f0a003d);
            if (TextUtils.isEmpty(((CharSequence) (obj))))
            {
                return null;
            }
            obj = mResources.getDrawable(mResources.getIdentifier(((String) (obj)), "drawable", s));
            s = ((String) (obj));
            if (obj != null)
            {
                if (mImageWidth == 0 || mImageHeight == 0)
                {
                    mImageWidth = ((Drawable) (obj)).getIntrinsicWidth();
                    mImageHeight = ((Drawable) (obj)).getIntrinsicHeight();
                }
                ((Drawable) (obj)).setBounds(0, 0, mImageWidth, mImageHeight);
                return ((Drawable) (obj));
            }
        }
        return s;
    }
}
