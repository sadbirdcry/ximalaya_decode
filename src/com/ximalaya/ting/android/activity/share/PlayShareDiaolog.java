// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.share.ShareContentModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            PlayShareActivity, i

public class PlayShareDiaolog extends Dialog
{
    private class a
        implements IUiListener
    {

        final PlayShareDiaolog a;

        protected void a(org.json.JSONObject jsonobject)
        {
            int j;
            try
            {
                j = jsonobject.getInt("ret");
            }
            // Misplaced declaration of an exception variable
            catch (org.json.JSONObject jsonobject)
            {
                j = -10;
            }
            if (j == 0)
            {
                ToolUtil.onEvent(a.mContext, "SHARE_QQ_FRIEND_SUCCESS");
            }
            a.dismiss();
        }

        public void onCancel()
        {
        }

        public void onComplete(Object obj)
        {
            a((org.json.JSONObject)obj);
        }

        public void onError(UiError uierror)
        {
            Toast.makeText(a.mContext, uierror.errorMessage, 1).show();
        }

        private a()
        {
            a = PlayShareDiaolog.this;
            super();
        }

        a(i j)
        {
            this();
        }
    }

    private class b extends MyAsyncTask
    {

        String a;
        ProgressDialog b;
        final PlayShareDiaolog c;

        protected transient ShareContentModel a(Object aobj[])
        {
            ShareContentModel sharecontentmodel;
            Object obj = null;
            sharecontentmodel = new ShareContentModel();
            if (c.mSoundInfo == null)
            {
                sharecontentmodel.ret = -1;
                sharecontentmodel.msg = (new StringBuffer("\u627E\u4E0D\u5230\u58F0\u97F3\u4FE1\u606F,\u8BF7\u8FD4\u56DE\u5217\u8868,\u8FDB\u5165\u64AD\u653E\u518D\u91CD\u8BD5")).toString();
                return sharecontentmodel;
            }
            if (c.loginInfoModel == null)
            {
                return null;
            }
            a = (String)aobj[0];
            aobj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/track/share/content").toString();
            Object obj2 = new RequestParams();
            ((RequestParams) (obj2)).put("trackId", (new StringBuilder()).append("").append(c.mSoundInfo.trackId).toString());
            ((RequestParams) (obj2)).put("tpName", a);
            obj2 = f.a().a(((String) (aobj)), ((RequestParams) (obj2)), null, null, false);
            aobj = obj;
            if (((com.ximalaya.ting.android.b.n.a) (obj2)).b == 1)
            {
                aobj = ((com.ximalaya.ting.android.b.n.a) (obj2)).a;
            }
            if (aobj == null) goto _L2; else goto _L1
_L1:
            Object obj1;
            int j;
            obj1 = JSON.parseObject(((String) (aobj)));
            j = ((JSONObject) (obj1)).getIntValue("ret");
            if (j != 0) goto _L4; else goto _L3
_L3:
            obj1 = (ShareContentModel)JSON.parseObject(((String) (aobj)), com/ximalaya/ting/android/model/share/ShareContentModel);
            c.contentStr = ((String) (aobj));
            aobj = ((Object []) (obj1));
_L5:
            return ((ShareContentModel) (aobj));
_L4:
            sharecontentmodel.ret = j;
            sharecontentmodel.msg = ((JSONObject) (obj1)).getString("msg");
_L6:
            aobj = sharecontentmodel;
              goto _L5
            aobj;
            aobj = sharecontentmodel;
_L7:
            aobj.ret = -1;
            aobj.msg = "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
              goto _L5
_L2:
            sharecontentmodel.ret = -1;
            sharecontentmodel.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
              goto _L6
            aobj;
            aobj = ((Object []) (obj1));
              goto _L7
        }

        protected void a(ShareContentModel sharecontentmodel)
        {
            if (c != null && c.isShowing())
            {
                c.dismiss();
            }
            if (sharecontentmodel == null)
            {
                return;
            }
            if (b != null)
            {
                b.cancel();
                b = null;
            }
            if (sharecontentmodel.ret == 0)
            {
                c.content = sharecontentmodel;
                if ("qq".equals(a))
                {
                    c.onClickShareToQQ(sharecontentmodel);
                    return;
                } else
                {
                    c.toShare(a);
                    return;
                }
            } else
            {
                c.showToast(sharecontentmodel.msg);
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((ShareContentModel)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            b = new MyProgressDialog(c.mContext);
            b.setTitle("\u63D0\u793A");
            b.setMessage("\u6B63\u5728\u5E2E\u60A8\u83B7\u53D6\u5206\u4EAB\u5185\u5BB9\u4E2D...");
            b.show();
        }

        private b()
        {
            c = PlayShareDiaolog.this;
            super();
        }

        b(i j)
        {
            this();
        }
    }

    class c extends BaseAdapter
    {

        final PlayShareDiaolog a;

        public int getCount()
        {
            return a.shareStr.length;
        }

        public Object getItem(int j)
        {
            return a.shareStr[j];
        }

        public long getItemId(int j)
        {
            return (long)j;
        }

        public View getView(int j, View view, ViewGroup viewgroup)
        {
            view = new a(this);
            viewgroup = a.layoutInflater.inflate(0x7f030195, null);
            view.b = (ImageView)viewgroup.findViewById(0x7f0a063f);
            view.a = (TextView)viewgroup.findViewById(0x7f0a05f6);
            ((a) (view)).a.setText(a.shareStr[j]);
            switch (j)
            {
            default:
                return viewgroup;

            case 2: // '\002'
                ((a) (view)).b.setImageResource(0x7f020424);
                return viewgroup;

            case 3: // '\003'
                ((a) (view)).b.setImageResource(0x7f020425);
                return viewgroup;

            case 4: // '\004'
                ((a) (view)).b.setImageResource(0x7f020428);
                return viewgroup;

            case 5: // '\005'
                ((a) (view)).b.setImageResource(0x7f020427);
                return viewgroup;

            case 0: // '\0'
                ((a) (view)).b.setImageResource(0x7f020429);
                return viewgroup;

            case 1: // '\001'
                ((a) (view)).b.setImageResource(0x7f02042a);
                return viewgroup;

            case 6: // '\006'
                ((a) (view)).b.setImageResource(0x7f020426);
                return viewgroup;

            case 7: // '\007'
                ((a) (view)).b.setImageResource(0x7f020423);
                break;
            }
            return viewgroup;
        }

        c()
        {
            a = PlayShareDiaolog.this;
            super();
        }
    }

    class c.a
    {

        TextView a;
        ImageView b;
        final c c;

        c.a(c c1)
        {
            c = c1;
            super();
        }
    }

    class d extends MyAsyncTask
    {

        int a;
        ProgressDialog b;
        byte c[];
        final PlayShareDiaolog d;

        protected transient ShareContentModel a(Object aobj[])
        {
            a = ((Integer)aobj[0]).intValue();
            ((MyApplication)(MyApplication)d.mContext.getApplication()).b = a;
            aobj = d.getWXContent(a);
            if (((ShareContentModel) (aobj)).picUrl != null)
            {
                c = ToolUtil.loadByteArrayFromNetwork(((ShareContentModel) (aobj)).picUrl);
            }
            return ((ShareContentModel) (aobj));
        }

        protected void a(ShareContentModel sharecontentmodel)
        {
            if (b != null && b.isShowing())
            {
                b.dismiss();
            }
            if (sharecontentmodel == null);
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((ShareContentModel)obj);
        }

        protected void onPreExecute()
        {
            if (b == null)
            {
                b = ToolUtil.createProgressDialog(d.mContext, 0, true, true);
            }
            b.setMessage("\u6B63\u5728\u4E3A\u60A8\u52AA\u529B\u5206\u4EAB...");
            b.show();
        }

        d()
        {
            d = PlayShareDiaolog.this;
            super();
            a = 0;
        }
    }


    private static final int SHARE_DUANXIN = 7;
    private static final int SHARE_QQ_FRIEND = 2;
    private static final int SHARE_QQ_ZONE = 3;
    private static final int SHARE_RENREN = 6;
    private static final int SHARE_SINA_WEIBO = 4;
    public static final String SHARE_STRING_MESSAGE = "message";
    public static final String SHARE_STRING_QQ = "qq";
    public static final String SHARE_STRING_QQ_ZONE = "qzone";
    public static final String SHARE_STRING_QZONE = "qzone";
    public static final String SHARE_STRING_RENREN = "renren";
    public static final String SHARE_STRING_T_QQ = "tQQ";
    public static final String SHARE_STRING_T_SINA = "tSina";
    private static final int SHARE_TENCENT_WEIBO = 5;
    private static final int SHARE_WEIXING_FRIEND = 0;
    private static final int SHARE_WEIXING_GROUP = 1;
    private ShareContentModel content;
    private String contentStr;
    private LayoutInflater layoutInflater;
    private ListView listView;
    private LoginInfoModel loginInfoModel;
    private Activity mContext;
    private SoundInfo mSoundInfo;
    private Tencent mTencent;
    private PlayerFragment playerFragment;
    private String shareStr[];

    public PlayShareDiaolog(Activity activity, SoundInfo soundinfo)
    {
        super(activity, 0x7f0b0027);
        content = null;
        mContext = activity;
        layoutInflater = LayoutInflater.from(activity);
        mSoundInfo = soundinfo;
    }

    public PlayShareDiaolog(Activity activity, SoundInfo soundinfo, PlayerFragment playerfragment)
    {
        super(activity, 0x7f0b0027);
        content = null;
        mContext = activity;
        layoutInflater = LayoutInflater.from(activity);
        mSoundInfo = soundinfo;
        playerFragment = playerfragment;
    }

    private ShareContentModel getWXContent(int j)
    {
        Object obj;
        Object obj1;
        obj = new ShareContentModel();
        if (mSoundInfo == null)
        {
            obj.ret = -1;
            obj.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
            return ((ShareContentModel) (obj));
        }
        obj1 = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/track/share/content").toString();
        Object obj2 = new RequestParams();
        ((RequestParams) (obj2)).put("trackId", (new StringBuilder()).append("").append(mSoundInfo.trackId).toString());
        if (j == 0)
        {
            ((RequestParams) (obj2)).put("tpName", "weixin");
        } else
        {
            ((RequestParams) (obj2)).put("tpName", "weixinGroup");
        }
        obj2 = f.a().a(((String) (obj1)), ((RequestParams) (obj2)), null, null);
        if (obj2 == null) goto _L2; else goto _L1
_L1:
        obj1 = JSON.parseObject(((String) (obj2)));
        j = ((JSONObject) (obj1)).getIntValue("ret");
        if (j != 0) goto _L4; else goto _L3
_L3:
        obj1 = (ShareContentModel)JSON.parseObject(((String) (obj2)), com/ximalaya/ting/android/model/share/ShareContentModel);
        contentStr = ((String) (obj2));
        obj = obj1;
_L5:
        return ((ShareContentModel) (obj));
_L4:
        obj.ret = j;
        obj.msg = ((JSONObject) (obj1)).getString("msg");
          goto _L5
        obj1;
_L6:
        obj.ret = -1;
        obj.msg = "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
          goto _L5
_L2:
        obj.ret = -1;
        obj.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
          goto _L5
        Exception exception;
        exception;
        exception = ((Exception) (obj1));
          goto _L6
    }

    private void onClickShareToQQ(ShareContentModel sharecontentmodel)
    {
        mTencent = Tencent.createInstance(com.ximalaya.ting.android.a.b.c, mContext);
        Bundle bundle = new Bundle();
        bundle.putString("title", (new StringBuilder()).append(mSoundInfo.title).append("").toString());
        bundle.putString("imageUrl", (new StringBuilder()).append(sharecontentmodel.picUrl).append("").toString());
        bundle.putString("targetUrl", (new StringBuilder()).append(sharecontentmodel.url).append("").toString());
        if (TextUtils.isEmpty(mSoundInfo.nickname))
        {
            bundle.putString("summary", (new StringBuilder()).append(mSoundInfo.title).append("").toString());
        } else
        {
            bundle.putString("summary", (new StringBuilder()).append(mSoundInfo.nickname).append("").toString());
        }
        bundle.putString("appName", "\u559C\u9A6C\u62C9\u96C5");
        mTencent.shareToQQ(mContext, bundle, new a(null));
    }

    private void sendMsg2(String s)
    {
        try
        {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("smsto:"));
            intent.putExtra("sms_body", s);
            ((ClipboardManager)mContext.getSystemService("clipboard")).setText(s);
            Toast.makeText(mContext, "\u6D88\u606F\u5DF2\u590D\u5236\u5230\u7CFB\u7EDF\u526A\u8D34\u677F", 1).show();
            mContext.startActivityForResult(intent, 2);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Toast.makeText(mContext, "\u60A8\u7684\u8BBE\u5907\u4E0D\u652F\u6301\u77ED\u4FE1\u529F\u80FD", 1).show();
        }
    }

    private void toShare(String s)
    {
        if (!"message".equals(s))
        {
            Intent intent = new Intent(mContext, com/ximalaya/ting/android/activity/share/PlayShareActivity);
            intent.putExtra("content", contentStr);
            intent.putExtra("thirdpartyNames", s);
            intent.putExtra("shareFlag", "trackId");
            intent.putExtra("trackId", mSoundInfo.trackId);
            mContext.startActivity(intent);
            dismiss();
            return;
        } else
        {
            sendMsg2(content.content);
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(0x7f030193);
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        listView = (ListView)findViewById(0x7f0a063a);
        shareStr = mContext.getResources().getStringArray(0x7f0c000f);
        listView.setAdapter(new c());
        listView.setOnItemClickListener(new i(this));
    }

    public void showToast(String s)
    {
        if (mContext != null)
        {
            Toast.makeText(mContext, s, 0).show();
        }
    }





/*
    static String access$302(PlayShareDiaolog playsharediaolog, String s)
    {
        playsharediaolog.contentStr = s;
        return s;
    }

*/


/*
    static ShareContentModel access$402(PlayShareDiaolog playsharediaolog, ShareContentModel sharecontentmodel)
    {
        playsharediaolog.content = sharecontentmodel;
        return sharecontentmodel;
    }

*/





}
