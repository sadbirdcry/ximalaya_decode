// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;
import com.tencent.tauth.Tencent;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.share.ShareContentModel;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.ArrayList;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            e

class d extends MyAsyncTask
{

    ProgressDialog a;
    boolean b;
    String c;
    Activity d;
    private Tencent e;
    private ShareContentModel f;
    private String g;
    private ScoreManage h;

    public d(Activity activity, ShareContentModel sharecontentmodel, String s)
    {
        b = false;
        c = null;
        d = activity;
        g = s;
        f = sharecontentmodel;
        h = ScoreManage.getInstance(activity.getApplicationContext());
    }

    static ShareContentModel a(d d1)
    {
        return d1.f;
    }

    static ScoreManage b(d d1)
    {
        return d1.h;
    }

    private void b(Bundle bundle)
    {
        e.shareToQzone(d, bundle, new e(this));
    }

    protected transient Bundle a(Void avoid[])
    {
        avoid = new Bundle();
        avoid.putInt("req_type", 1);
        ArrayList arraylist;
        if (!TextUtils.isEmpty(f.title))
        {
            avoid.putString("title", f.title);
            avoid.putString("summary", g);
        } else
        {
            avoid.putString("title", g);
        }
        avoid.putString("targetUrl", f.url);
        arraylist = new ArrayList();
        if (!TextUtils.isEmpty(f.picUrl))
        {
            arraylist.add(f.picUrl);
        } else
        {
            arraylist.add("http://s1.xmcdn.com/css/img/common/track_640.jpg");
        }
        avoid.putStringArrayList("imageUrl", arraylist);
        return avoid;
    }

    protected void a(Bundle bundle)
    {
        if (d != null)
        {
            if (a != null && !d.isFinishing())
            {
                a.cancel();
                a = null;
            }
            b(bundle);
            if (b)
            {
                Toast.makeText(d, (new StringBuilder()).append("\u5206\u4EAB\u9519\u8BEF:").append(c).toString(), 0).show();
                return;
            }
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((Bundle)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        a = new MyProgressDialog(d);
        a.setTitle("\u63D0\u793A");
        a.setMessage("\u6B63\u5728\u5E2E\u60A8\u5206\u4EAB\u5185\u5BB9\u4E2D...");
        a.show();
        if (e == null)
        {
            e = Tencent.createInstance(b.c, d);
        }
    }
}
