// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.activity.login.AuthorizeActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.share.ShareContentModel;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            f, g, h

public class PlayShareActivity extends BaseActivity
{
    private class a extends MyAsyncTask
    {

        ProgressDialog a;
        final PlayShareActivity b;

        protected transient String a(Void avoid[])
        {
            Object obj;
            avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/auth/feed").toString();
            obj = new RequestParams();
            ((RequestParams) (obj)).put("content", b.et.getText().toString().trim());
            ((RequestParams) (obj)).put("picUrl", b.contentModel.picUrl);
            ((RequestParams) (obj)).put("tpName", b.thirdpartyNames);
            ((RequestParams) (obj)).put("rowKey", b.contentModel.rowKey);
            b.mShareType;
            JVM INSTR tableswitch 11 19: default 156
        //                       11 205
        //                       12 239
        //                       13 273
        //                       14 307
        //                       15 341
        //                       16 406
        //                       17 156
        //                       18 471
        //                       19 490;
               goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L1 _L8 _L9
_L1:
            avoid = f.a().b(avoid, ((RequestParams) (obj)), b.nextButton, null);
            if (avoid == null) goto _L11; else goto _L10
_L10:
            boolean flag;
            try
            {
                avoid = JSON.parseObject(avoid);
                obj = avoid.get("ret").toString();
                flag = "0".equals(obj);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                return "\u89E3\u6790\u6570\u636E\u51FA\u9519";
            }
            if (!flag) goto _L13; else goto _L12
_L12:
            return ((String) (obj));
_L2:
            ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append(b.trackId).append("").toString());
            continue; /* Loop/switch isn't completed */
_L3:
            ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append(b.albumId).append("").toString());
            continue; /* Loop/switch isn't completed */
_L4:
            ((RequestParams) (obj)).put("shareUid", (new StringBuilder()).append(b.bloggerId).append("").toString());
            continue; /* Loop/switch isn't completed */
_L5:
            ((RequestParams) (obj)).put("activityId", (new StringBuilder()).append(b.activityId).append("").toString());
            continue; /* Loop/switch isn't completed */
_L6:
            ((RequestParams) (obj)).put("activityId", (new StringBuilder()).append(b.activityId).append("").toString());
            ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append(b.trackId).append("").toString());
            continue; /* Loop/switch isn't completed */
_L7:
            ((RequestParams) (obj)).put("activityId", (new StringBuilder()).append(b.activityId).append("").toString());
            ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append(b.trackId).append("").toString());
            continue; /* Loop/switch isn't completed */
_L8:
            ((RequestParams) (obj)).put("specialId", String.valueOf(b.specialId));
            continue; /* Loop/switch isn't completed */
_L9:
            ((RequestParams) (obj)).put("specialId", b.contentModel.url);
            ((RequestParams) (obj)).put("specialId", b.contentModel.title);
            continue; /* Loop/switch isn't completed */
_L13:
            if ("222".equals(obj)) goto _L12; else goto _L14
_L14:
            avoid = avoid.getString("msg");
            return avoid;
_L11:
            return "\u7F51\u7EDC\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
            if (true) goto _L1; else goto _L15
_L15:
        }

        protected void a(String s)
        {
            if (b != null && !b.isFinishing()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if (a != null)
            {
                a.cancel();
                a = null;
            }
            if (!"0".equals(s)) goto _L4; else goto _L3
_L3:
            b.showToast("\u5206\u4EAB\u6210\u529F");
            if (!"tSina".equals(b.thirdpartyNames)) goto _L6; else goto _L5
_L5:
            ToolUtil.onEvent(b, "SHARE_T_SINA_SUCCESS");
            if (b.mScoreManage != null)
            {
                b.mScoreManage.onShareFinishMain(2);
            }
_L7:
            b.finish();
            return;
_L6:
            if ("tQQ".equals(b.thirdpartyNames))
            {
                if (b.mScoreManage != null)
                {
                    b.mScoreManage.onShareFinishMain(9);
                }
                ToolUtil.onEvent(b, "SHARE_T_QQ_SUCCESS");
            } else
            if ("renren".equals(b.thirdpartyNames))
            {
                if (b.mScoreManage != null)
                {
                    b.mScoreManage.onShareFinishMain(11);
                }
                ToolUtil.onEvent(b, "SHARE_RENREN_SUCCESS");
            }
            if (true) goto _L7; else goto _L4
_L4:
            if ("222".equals(s))
            {
                if ("tSina".equals(b.thirdpartyNames))
                {
                    b.bindSina();
                    return;
                }
                if ("tQQ".equals(b.thirdpartyNames))
                {
                    b.bindQQ();
                    return;
                }
                if ("renren".equals(b.thirdpartyNames))
                {
                    b.bindRenren();
                    return;
                }
            } else
            {
                b.showToast(s);
                return;
            }
            if (true) goto _L1; else goto _L8
_L8:
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((String)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            a = new MyProgressDialog(b);
            a.setTitle("\u63D0\u793A");
            a.setMessage("\u6B63\u5728\u5E2E\u60A8\u5206\u4EAB\u5185\u5BB9\u4E2D...");
            a.show();
        }

        private a()
        {
            b = PlayShareActivity.this;
            super();
        }

        a(com.ximalaya.ting.android.activity.share.f f1)
        {
            this();
        }
    }


    public static final String BUNDLE_ACTIVITY_ID = "activityId";
    public static final String BUNDLE_ALBUM_ID = "albumId";
    public static final String BUNDLE_BLOGGER_ID = "shareUid";
    public static final String BUNDLE_SPECIAL_ID = "specialId";
    public static final String BUNDLE_TRACK_ID = "trackId";
    public static final String SHARE_WHAT = "SHARE_WHAT";
    protected static final String TAG = "PlayShareActivity";
    private long activityId;
    private long albumId;
    private long bloggerId;
    private ShareContentModel contentModel;
    private EditText et;
    private final int goBindQQ = 13;
    private final int goBindRenn = 14;
    private final int goBindSina = 12;
    private int isBindQQ;
    private int isBindRenn;
    private int isBindSina;
    private int lastBindWeibo;
    private Context mCt;
    private ScoreManage mScoreManage;
    private int mShareType;
    private String picUrl;
    private long specialId;
    private String thirdpartyNames;
    private final int totalLength = 140;
    private long trackId;
    private TextView txt_canInputeHowMuchWord;

    public PlayShareActivity()
    {
        isBindSina = 0;
        isBindQQ = 0;
        isBindRenn = 0;
        lastBindWeibo = -1;
    }

    private void bindQQ()
    {
        Intent intent = new Intent(this, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
        lastBindWeibo = 13;
        intent.putExtra("lgflag", lastBindWeibo);
        startActivityForResult(intent, 13);
    }

    private void bindRenren()
    {
        Intent intent = new Intent(this, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
        lastBindWeibo = 14;
        intent.putExtra("lgflag", lastBindWeibo);
        startActivityForResult(intent, 14);
    }

    private void bindSina()
    {
        Intent intent = new Intent(this, com/ximalaya/ting/android/activity/login/AuthorizeActivity);
        lastBindWeibo = 12;
        intent.putExtra("lgflag", lastBindWeibo);
        startActivityForResult(intent, 12);
    }

    private void findViews()
    {
        txt_canInputeHowMuchWord = (TextView)findViewById(0x7f0a063d);
        et = (EditText)findViewById(0x7f0a063e);
        retButton = (ImageView)findViewById(0x7f0a007b);
        nextButton = (ImageView)findViewById(0x7f0a0710);
        nextButton.setVisibility(0);
        topTextView = (TextView)findViewById(0x7f0a00ae);
    }

    private void initData()
    {
        String s;
        if (loginInfoModel != null && loginInfoModel.bindStatus != null)
        {
            Iterator iterator = loginInfoModel.bindStatus.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                ThirdPartyUserInfo thirdpartyuserinfo = (ThirdPartyUserInfo)iterator.next();
                if (thirdpartyuserinfo.getIdentity().equals("1"))
                {
                    isBindSina = 1;
                    thirdpartyuserinfo.isExpired();
                } else
                if (thirdpartyuserinfo.getIdentity().equals("2"))
                {
                    isBindQQ = 1;
                    thirdpartyuserinfo.isExpired();
                } else
                if (thirdpartyuserinfo.getIdentity().equals("3"))
                {
                    isBindRenn = 1;
                    thirdpartyuserinfo.isExpired();
                }
            } while (true);
        }
        s = getIntent().getStringExtra("content");
        thirdpartyNames = getIntent().getStringExtra("thirdpartyNames");
        mShareType = getIntent().getIntExtra("SHARE_WHAT", 0);
        mShareType;
        JVM INSTR tableswitch 11 18: default 212
    //                   11 230
    //                   12 247
    //                   13 264
    //                   14 281
    //                   15 298
    //                   16 329
    //                   17 212
    //                   18 360;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L1 _L8
_L1:
        if (s != null)
        {
            contentModel = (ShareContentModel)JSONObject.parseObject(s, com/ximalaya/ting/android/model/share/ShareContentModel);
        }
        return;
_L2:
        trackId = getIntent().getLongExtra("trackId", 0L);
        continue; /* Loop/switch isn't completed */
_L3:
        albumId = getIntent().getLongExtra("albumId", 0L);
        continue; /* Loop/switch isn't completed */
_L4:
        bloggerId = getIntent().getLongExtra("shareUid", 0L);
        continue; /* Loop/switch isn't completed */
_L5:
        activityId = getIntent().getLongExtra("activityId", 0L);
        continue; /* Loop/switch isn't completed */
_L6:
        activityId = getIntent().getLongExtra("activityId", 0L);
        trackId = getIntent().getLongExtra("trackId", 0L);
        continue; /* Loop/switch isn't completed */
_L7:
        activityId = getIntent().getLongExtra("activityId", 0L);
        trackId = getIntent().getLongExtra("trackId", 0L);
        continue; /* Loop/switch isn't completed */
_L8:
        specialId = getIntent().getLongExtra("specialId", 0L);
        if (true) goto _L1; else goto _L9
_L9:
    }

    private void initViews()
    {
        et.setText((new StringBuilder()).append("  ").append(contentModel.content).toString());
        txt_canInputeHowMuchWord.setText((new StringBuilder()).append("\u5269\u4F59").append(140 - ToolUtil.getShareContentLength(et.getText().toString().trim())).append("\u5B57").toString());
        et.addTextChangedListener(new com.ximalaya.ting.android.activity.share.f(this));
        retButton.setImageResource(0x7f020111);
        retButton.setOnClickListener(new g(this));
        nextButton.setImageResource(0x7f02006a);
        nextButton.setOnClickListener(new h(this));
        if ("tSina".equals(thirdpartyNames))
        {
            topTextView.setText("\u5206\u4EAB\u5230\u65B0\u6D6A\u5FAE\u535A");
        } else
        {
            if ("tQQ".equals(thirdpartyNames))
            {
                topTextView.setText("\u5206\u4EAB\u5230\u817E\u8BAF\u5FAE\u535A");
                return;
            }
            if ("qzone".equals(thirdpartyNames))
            {
                topTextView.setText("\u5206\u4EAB\u5230QQ\u7A7A\u95F4");
                return;
            }
            if ("renren".equals(thirdpartyNames))
            {
                topTextView.setText("\u5206\u4EAB\u5230\u4EBA\u4EBA\u7F51");
                return;
            }
        }
    }

    private void saveBind(int i)
    {
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        if (i != 13) goto _L2; else goto _L1
_L1:
        if (j == 0)
        {
            isBindQQ = 1;
            saveBind(i);
        }
_L4:
        return;
_L2:
        if (i != 12)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (j != 0) goto _L4; else goto _L3
_L3:
        isBindSina = 1;
        saveBind(i);
        return;
        if (i != 14 || j != 0) goto _L4; else goto _L5
_L5:
        isBindRenn = 1;
        saveBind(i);
        return;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030194);
        mCt = getApplicationContext();
        mScoreManage = ScoreManage.getInstance(mCt);
        initData();
        findViews();
        initViews();
    }

















}
