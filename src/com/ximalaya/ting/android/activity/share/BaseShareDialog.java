// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXMusicObject;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.personal_info.HomePageModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.share.ShareContentModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.subject.SubjectModel;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import java.net.URLEncoder;
import java.util.HashMap;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            SimpleShareData, b, a, PlayShareActivity, 
//            c

public class BaseShareDialog extends Dialog
{
    private class a
        implements IUiListener
    {

        final BaseShareDialog a;
        private ShareContentModel b;

        protected void a(org.json.JSONObject jsonobject)
        {
            int i;
            try
            {
                i = jsonobject.getInt("ret");
            }
            // Misplaced declaration of an exception variable
            catch (org.json.JSONObject jsonobject)
            {
                i = -10;
            }
            if (i == 0)
            {
                a.showToast("\u5206\u4EABQQ\u6210\u529F");
                if (a.mScoreManage != null)
                {
                    a.mScoreManage.onShareFinishMain(10);
                }
                ToolUtil.onEvent(a.mActivity, "SHARE_QQ_FRIEND_SUCCESS");
                BaseShareDialog.statShare(a.mActivity, b);
            }
            a.dismiss();
        }

        public void onCancel()
        {
        }

        public void onComplete(Object obj)
        {
            a((org.json.JSONObject)obj);
        }

        public void onError(UiError uierror)
        {
            Toast.makeText(a.mActivity, uierror.errorMessage, 1).show();
        }

        public a(ShareContentModel sharecontentmodel)
        {
            a = BaseShareDialog.this;
            super();
            b = sharecontentmodel;
        }
    }

    private class b extends MyAsyncTask
    {

        String a;
        ProgressDialog b;
        final BaseShareDialog c;

        protected transient ShareContentModel a(String as[])
        {
            ShareContentModel sharecontentmodel;
            JSONObject jsonobject;
            RequestParams requestparams;
            jsonobject = null;
            a = as[0];
            if (c.mShareData != null)
            {
                c.contentStr = JSON.toJSONString(c.mShareData);
                return c.mShareData;
            }
            sharecontentmodel = new ShareContentModel();
            sharecontentmodel.thirdPartyName = a;
            if (c.isInfoNull())
            {
                sharecontentmodel.ret = -1;
                sharecontentmodel.msg = (new StringBuffer("\u627E\u4E0D\u5230\u4FE1\u606F,\u8BF7\u8FD4\u56DE\u5217\u8868,\u8FDB\u5165\u64AD\u653E\u518D\u91CD\u8BD5")).toString();
                return sharecontentmodel;
            }
            requestparams = new RequestParams();
            c.mShareType;
            JVM INSTR tableswitch 11 19: default 160
        //                       11 240
        //                       12 332
        //                       13 424
        //                       14 516
        //                       15 602
        //                       16 720
        //                       17 160
        //                       18 838
        //                       19 915;
               goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L1 _L8 _L9
_L1:
            as = null;
_L14:
            int i;
            requestparams.put("tpName", a);
            String s;
            String s1;
            if (as == null)
            {
                as = c.contentStr;
            } else
            {
                com.ximalaya.ting.android.b.n.a a1 = f.a().a(as, requestparams, c.fromBindView, null, false);
                as = jsonobject;
                if (a1.b == 1)
                {
                    as = a1.a;
                }
            }
            if (as == null) goto _L11; else goto _L10
_L10:
            jsonobject = JSON.parseObject(as);
            i = jsonobject.getIntValue("ret");
            if (i != 0) goto _L13; else goto _L12
_L12:
            as = (ShareContentModel)JSON.parseObject(as, com/ximalaya/ting/android/model/share/ShareContentModel);
            c.copyValue(as, sharecontentmodel);
            c.contentStr = JSON.toJSONString(as);
_L15:
            return as;
_L2:
            as = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/track/share/content").toString();
            requestparams.put("trackId", (new StringBuilder()).append("").append(c.mSoundInfo.trackId).toString());
            sharecontentmodel.trackId = (new StringBuilder()).append(c.mSoundInfo.trackId).append("").toString();
              goto _L14
_L3:
            as = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/album/share/content").toString();
            requestparams.put("albumId", (new StringBuilder()).append("").append(c.mAlbumModel.albumId).toString());
            sharecontentmodel.albumId = (new StringBuilder()).append(c.mAlbumModel.albumId).append("").toString();
              goto _L14
_L4:
            as = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/user/share/content").toString();
            requestparams.put("shareUid", (new StringBuilder()).append("").append(c.mHomeModel.uid).toString());
            sharecontentmodel.shareUid = (new StringBuilder()).append(c.mHomeModel.uid).append("").toString();
              goto _L14
_L5:
            as = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/activity/share/content").toString();
            requestparams.put("activityId", (new StringBuilder()).append("").append(c.mActivityId).toString());
            sharecontentmodel.activityId = (new StringBuilder()).append(c.mActivityId).append("").toString();
              goto _L14
_L6:
            as = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/activity/vote/share/content").toString();
            requestparams.put("activityId", (new StringBuilder()).append("").append(c.mActivityId).toString());
            requestparams.put("trackId", (new StringBuilder()).append("").append(c.mTrackId).toString());
            sharecontentmodel.activityId = (new StringBuilder()).append(c.mActivityId).append("").toString();
              goto _L14
_L7:
            as = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/activity/track/share/content").toString();
            requestparams.put("activityId", (new StringBuilder()).append("").append(c.mActivityId).toString());
            requestparams.put("trackId", (new StringBuilder()).append("").append(c.mTrackId).toString());
            sharecontentmodel.activityId = (new StringBuilder()).append(c.mActivityId).append("").toString();
              goto _L14
_L8:
            as = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/special/share/content").toString();
            requestparams.put("specialId", String.valueOf(c.mSubjectModel.specialId));
            sharecontentmodel.specialId = (new StringBuilder()).append(c.mSubjectModel.specialId).append("").toString();
              goto _L14
_L9:
            s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/link/share/content").toString();
            as = c.mSimpleData.url;
            s1 = URLEncoder.encode(c.mSimpleData.url, "utf-8");
            as = s1;
_L18:
            requestparams.put("linkUrl", as);
            requestparams.put("linkTitle", c.mSimpleData.title);
            requestparams.put("linkCoverPath", c.mSimpleData.picUrl);
            as = s;
              goto _L14
_L13:
            sharecontentmodel.ret = i;
            sharecontentmodel.msg = jsonobject.getString("msg");
_L16:
            as = sharecontentmodel;
              goto _L15
            as;
            as = sharecontentmodel;
_L17:
            as.ret = -1;
            as.msg = "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
              goto _L15
_L11:
            sharecontentmodel.ret = -1;
            sharecontentmodel.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A53";
              goto _L16
            Exception exception;
            exception;
              goto _L17
            Exception exception1;
            exception1;
              goto _L18
        }

        protected void a(ShareContentModel sharecontentmodel)
        {
            if (c != null && c.isShowing())
            {
                c.dismiss();
            }
            if (b != null)
            {
                b.cancel();
                b = null;
            }
            if (sharecontentmodel != null && sharecontentmodel.ret == 0)
            {
                c.content = sharecontentmodel;
                if ("qq".equals(a))
                {
                    c.onClickShareToQQ(sharecontentmodel);
                    return;
                } else
                {
                    c.toShare(a);
                    return;
                }
            } else
            {
                c.showToast(sharecontentmodel.msg);
                return;
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((String[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((ShareContentModel)obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
            b = new MyProgressDialog(c.mActivity);
            b.setTitle("\u63D0\u793A");
            b.setMessage("\u6B63\u5728\u5E2E\u60A8\u83B7\u53D6\u5206\u4EAB\u5185\u5BB9\u4E2D...");
            b.show();
        }

        private b()
        {
            c = BaseShareDialog.this;
            super();
        }

        b(com.ximalaya.ting.android.activity.share.a a1)
        {
            this();
        }
    }

    class c extends BaseAdapter
    {

        boolean a[];
        final BaseShareDialog b;

        public void a(boolean aflag[])
        {
            a = aflag;
            if (aflag == null)
            {
                aflag = new boolean[8];
            }
            notifyDataSetChanged();
        }

        public int getCount()
        {
            return 8;
        }

        public Object getItem(int i)
        {
            return "";
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            a a1;
            a1 = new a(this);
            viewgroup = view;
            if (view == null)
            {
                viewgroup = b.layoutInflater.inflate(0x7f030195, null);
            }
            a1.b = (ImageView)viewgroup.findViewById(0x7f0a063f);
            a1.c = (ImageView)viewgroup.findViewById(0x7f0a0640);
            a1.a = (TextView)viewgroup.findViewById(0x7f0a05f6);
            i;
            JVM INSTR tableswitch 0 7: default 120
        //                       0 322
        //                       1 162
        //                       2 202
        //                       3 282
        //                       4 122
        //                       5 242
        //                       6 362
        //                       7 403;
               goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9
_L1:
            return viewgroup;
_L6:
            a1.b.setImageResource(0x7f020424);
            a1.a.setText("\u5206\u4EAB\u7ED9QQ\u597D\u53CB");
            if (a[4])
            {
                a1.c.setVisibility(0);
                return viewgroup;
            }
            continue; /* Loop/switch isn't completed */
_L3:
            a1.b.setImageResource(0x7f020425);
            a1.a.setText("\u5206\u4EAB\u5230QQ\u7A7A\u95F4");
            if (a[1])
            {
                a1.c.setVisibility(0);
                return viewgroup;
            }
            if (true) goto _L1; else goto _L4
_L4:
            a1.b.setImageResource(0x7f020428);
            a1.a.setText("\u5206\u4EAB\u5230\u65B0\u6D6A\u5FAE\u535A");
            if (a[2])
            {
                a1.c.setVisibility(0);
                return viewgroup;
            }
            if (true)
            {
                continue; /* Loop/switch isn't completed */
            }
_L7:
            a1.b.setImageResource(0x7f020427);
            a1.a.setText("\u5206\u4EAB\u5230\u817E\u8BAF\u5FAE\u535A");
            if (a[5])
            {
                a1.c.setVisibility(0);
                return viewgroup;
            }
            if (true) goto _L1; else goto _L5
_L5:
            a1.b.setImageResource(0x7f020429);
            a1.a.setText("\u5206\u4EAB\u7ED9\u5FAE\u4FE1\u597D\u53CB");
            if (a[3])
            {
                a1.c.setVisibility(0);
                return viewgroup;
            }
            if (true)
            {
                continue; /* Loop/switch isn't completed */
            }
_L2:
            a1.b.setImageResource(0x7f02042a);
            a1.a.setText("\u5206\u4EAB\u5230\u5FAE\u4FE1\u670B\u53CB\u5708");
            if (a[0])
            {
                a1.c.setVisibility(0);
                return viewgroup;
            }
            if (true) goto _L1; else goto _L8
_L8:
            a1.b.setImageResource(0x7f020426);
            a1.a.setText("\u5206\u4EAB\u5230\u4EBA\u4EBA\u7F51");
            if (a[6])
            {
                a1.c.setVisibility(0);
                return viewgroup;
            }
            if (true) goto _L1; else goto _L9
_L9:
            a1.b.setImageResource(0x7f020423);
            a1.a.setText("\u5206\u4EAB\u5230\u77ED\u4FE1");
            return viewgroup;
        }

        c()
        {
            b = BaseShareDialog.this;
            super();
            a = new boolean[8];
        }
    }

    class c.a
    {

        TextView a;
        ImageView b;
        ImageView c;
        final c d;

        c.a(c c1)
        {
            d = c1;
            super();
        }
    }

    class d extends MyAsyncTask
    {

        int a;
        ProgressDialog b;
        byte c[];
        final BaseShareDialog d;

        protected transient ShareContentModel a(Object aobj[])
        {
            a = ((Integer)aobj[0]).intValue();
            ((MyApplication)(MyApplication)d.mActivity.getApplication()).b = a;
            if (d.mShareData == null)
            {
                aobj = d.getWXContent(a);
            } else
            {
                aobj = d.mShareData;
                d.contentStr = JSON.toJSONString(((Object) (aobj)));
            }
            if (aobj != null && !TextUtils.isEmpty(((ShareContentModel) (aobj)).picUrl))
            {
                c = ToolUtil.loadByteArrayFromNetwork(((ShareContentModel) (aobj)).picUrl);
            }
            return ((ShareContentModel) (aobj));
        }

        protected void a(ShareContentModel sharecontentmodel)
        {
            if (b != null && b.isShowing())
            {
                b.dismiss();
            }
            if (sharecontentmodel != null)
            {
                d.sendWXShare(a, sharecontentmodel, c);
            }
        }

        protected Object doInBackground(Object aobj[])
        {
            return a(aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((ShareContentModel)obj);
        }

        protected void onPreExecute()
        {
            if (b == null)
            {
                b = ToolUtil.createProgressDialog(d.mActivity, 0, true, true);
            }
            b.setMessage("\u6B63\u5728\u4E3A\u60A8\u52AA\u529B\u5206\u4EAB...");
            b.show();
        }

        d()
        {
            d = BaseShareDialog.this;
            super();
            a = 0;
        }
    }


    private static final int SHARE_COUNT = 8;
    private static final int SHARE_DUANXIN = 7;
    private static final int SHARE_QQ_FRIEND = 4;
    private static final int SHARE_QQ_ZONE = 1;
    private static final int SHARE_RENREN = 6;
    private static final int SHARE_SINA_WEIBO = 2;
    public static final String SHARE_STRING_MESSAGE = "message";
    public static final String SHARE_STRING_QQ = "qq";
    public static final String SHARE_STRING_QZONE = "qzone";
    public static final String SHARE_STRING_RENREN = "renren";
    public static final String SHARE_STRING_T_QQ = "tQQ";
    public static final String SHARE_STRING_T_SINA = "tSina";
    private static final int SHARE_TENCENT_WEIBO = 5;
    public static final int SHARE_TYPE_ACTIVITY = 14;
    public static final int SHARE_TYPE_ACTIVITY_TRACK = 16;
    public static final int SHARE_TYPE_ACTIVITY_VOTE = 15;
    public static final int SHARE_TYPE_ALBUM = 12;
    public static final int SHARE_TYPE_H5 = 19;
    public static final int SHARE_TYPE_MODEL = 17;
    public static final int SHARE_TYPE_SUBJECT = 18;
    public static final int SHARE_TYPE_TRACK = 11;
    public static final int SHARE_TYPE_USER = 13;
    private static final int SHARE_WEIXING_FRIEND = 3;
    private static final int SHARE_WEIXING_GROUP = 0;
    protected static final String TAG = "BaseShareDialog";
    private ShareContentModel content;
    private String contentStr;
    private View fromBindView;
    private LayoutInflater layoutInflater;
    private ListView listView;
    private LoginInfoModel loginInfoModel;
    private Activity mActivity;
    private long mActivityId;
    private AlbumModel mAlbumModel;
    private boolean mHasLogined;
    private TextView mHeadTv;
    private HomePageModel mHomeModel;
    private ScoreManage mScoreManage;
    private ShareContentModel mShareData;
    private c mShareListAdapter;
    private int mShareType;
    private SimpleShareData mSimpleData;
    private SoundInfo mSoundInfo;
    private SubjectModel mSubjectModel;
    private Tencent mTencent;
    private long mTrackId;

    public BaseShareDialog(Activity activity, int i, SimpleShareData simplesharedata)
    {
        super(activity, 0x7f0b0027);
        mActivityId = -1L;
        mTrackId = -1L;
        content = null;
        mShareType = -1;
        mHasLogined = false;
        mActivity = activity;
        layoutInflater = LayoutInflater.from(activity);
        mShareType = i;
        mSimpleData = simplesharedata;
    }

    public BaseShareDialog(Activity activity, long l)
    {
        super(activity, 0x7f0b0027);
        mActivityId = -1L;
        mTrackId = -1L;
        content = null;
        mShareType = -1;
        mHasLogined = false;
        mActivity = activity;
        layoutInflater = LayoutInflater.from(activity);
        mActivityId = l;
        mShareType = 14;
    }

    public BaseShareDialog(Activity activity, long l, long l1, int i)
    {
        super(activity, 0x7f0b0027);
        mActivityId = -1L;
        mTrackId = -1L;
        content = null;
        mShareType = -1;
        mHasLogined = false;
        mActivity = activity;
        layoutInflater = LayoutInflater.from(activity);
        mActivityId = l;
        mTrackId = l1;
        if (i == 1)
        {
            mShareType = 15;
        }
        if (i == 2)
        {
            mShareType = 16;
        }
    }

    public BaseShareDialog(Activity activity, AlbumModel albummodel, View view)
    {
        super(activity, 0x7f0b0027);
        mActivityId = -1L;
        mTrackId = -1L;
        content = null;
        mShareType = -1;
        mHasLogined = false;
        mActivity = activity;
        layoutInflater = LayoutInflater.from(activity);
        mAlbumModel = albummodel;
        mSoundInfo = null;
        mShareType = 12;
        fromBindView = view;
    }

    public BaseShareDialog(Activity activity, HomePageModel homepagemodel, View view)
    {
        super(activity, 0x7f0b0027);
        mActivityId = -1L;
        mTrackId = -1L;
        content = null;
        mShareType = -1;
        mHasLogined = false;
        mActivity = activity;
        layoutInflater = LayoutInflater.from(activity);
        mSoundInfo = null;
        mHomeModel = homepagemodel;
        mShareType = 13;
        fromBindView = view;
    }

    public BaseShareDialog(Activity activity, ShareContentModel sharecontentmodel)
    {
        super(activity, 0x7f0b0027);
        mActivityId = -1L;
        mTrackId = -1L;
        content = null;
        mShareType = -1;
        mHasLogined = false;
        mActivity = activity;
        layoutInflater = LayoutInflater.from(activity);
        mShareType = 17;
        mShareData = sharecontentmodel;
        mShareData.ret = 0;
    }

    public BaseShareDialog(Activity activity, SoundInfo soundinfo, View view)
    {
        super(activity, 0x7f0b0027);
        mActivityId = -1L;
        mTrackId = -1L;
        content = null;
        mShareType = -1;
        mHasLogined = false;
        mActivity = activity;
        layoutInflater = LayoutInflater.from(activity);
        mSoundInfo = soundinfo;
        mAlbumModel = null;
        mShareType = 11;
        fromBindView = view;
    }

    public BaseShareDialog(Activity activity, SubjectModel subjectmodel, View view)
    {
        super(activity, 0x7f0b0027);
        mActivityId = -1L;
        mTrackId = -1L;
        content = null;
        mShareType = -1;
        mHasLogined = false;
        mActivity = activity;
        layoutInflater = LayoutInflater.from(mActivity);
        mSubjectModel = subjectmodel;
        mShareType = 18;
        fromBindView = view;
    }

    private String buildTransaction(String s)
    {
        if (s == null)
        {
            return String.valueOf(System.currentTimeMillis());
        } else
        {
            return (new StringBuilder()).append(s).append(System.currentTimeMillis()).toString();
        }
    }

    private void copyValue(ShareContentModel sharecontentmodel, ShareContentModel sharecontentmodel1)
    {
        sharecontentmodel.activityId = sharecontentmodel1.activityId;
        sharecontentmodel.albumId = sharecontentmodel1.albumId;
        sharecontentmodel.shareUid = sharecontentmodel1.shareUid;
        sharecontentmodel.specialId = sharecontentmodel1.specialId;
        sharecontentmodel.thirdPartyName = sharecontentmodel1.thirdPartyName;
        sharecontentmodel.trackId = sharecontentmodel1.trackId;
        sharecontentmodel.shareFrom = mShareType;
    }

    private ShareContentModel getWXContent(int i)
    {
        ShareContentModel sharecontentmodel;
        RequestParams requestparams;
        sharecontentmodel = new ShareContentModel();
        if (isInfoNull())
        {
            sharecontentmodel.ret = -1;
            sharecontentmodel.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A51";
            return sharecontentmodel;
        }
        requestparams = new RequestParams();
        mShareType;
        JVM INSTR tableswitch 11 19: default 92
    //                   11 197
    //                   12 287
    //                   13 377
    //                   14 467
    //                   15 551
    //                   16 666
    //                   17 92
    //                   18 781
    //                   19 855;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L1 _L8 _L9
_L1:
        Object obj = null;
_L14:
        Object obj1;
        if (i == 0)
        {
            requestparams.put("tpName", "weixin");
            sharecontentmodel.thirdPartyName = "weixin";
        } else
        {
            requestparams.put("tpName", "weixinGroup");
            sharecontentmodel.thirdPartyName = "weixinGroup";
        }
        if (obj == null)
        {
            obj = contentStr;
        } else
        {
            obj = f.a().a(((String) (obj)), requestparams, fromBindView, null);
        }
        if (obj == null) goto _L11; else goto _L10
_L10:
        obj1 = JSON.parseObject(((String) (obj)));
        i = ((JSONObject) (obj1)).getIntValue("ret");
        if (i != 0) goto _L13; else goto _L12
_L12:
        obj1 = (ShareContentModel)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/share/ShareContentModel);
        contentStr = ((String) (obj));
        if (mShareType == 19)
        {
            obj1.content = mSimpleData.content;
        }
        copyValue(((ShareContentModel) (obj1)), sharecontentmodel);
        obj = obj1;
_L15:
        return ((ShareContentModel) (obj));
_L2:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/track/share/content").toString();
        requestparams.put("trackId", (new StringBuilder()).append("").append(mSoundInfo.trackId).toString());
        sharecontentmodel.trackId = (new StringBuilder()).append(mSoundInfo.trackId).append("").toString();
          goto _L14
_L3:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/album/share/content").toString();
        requestparams.put("albumId", (new StringBuilder()).append("").append(mAlbumModel.albumId).toString());
        sharecontentmodel.albumId = (new StringBuilder()).append(mAlbumModel.albumId).append("").toString();
          goto _L14
_L4:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/user/share/content").toString();
        requestparams.put("shareUid", (new StringBuilder()).append("").append(mHomeModel.uid).toString());
        sharecontentmodel.shareUid = (new StringBuilder()).append(mHomeModel.uid).append("").toString();
          goto _L14
_L5:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/activity/share/content").toString();
        requestparams.put("activityId", (new StringBuilder()).append("").append(mActivityId).toString());
        sharecontentmodel.activityId = (new StringBuilder()).append(mActivityId).append("").toString();
          goto _L14
_L6:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/activity/vote/share/content").toString();
        requestparams.put("activityId", (new StringBuilder()).append("").append(mActivityId).toString());
        requestparams.put("trackId", (new StringBuilder()).append("").append(mTrackId).toString());
        sharecontentmodel.activityId = (new StringBuilder()).append(mActivityId).append("").toString();
          goto _L14
_L7:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/activity/track/share/content").toString();
        requestparams.put("activityId", (new StringBuilder()).append("").append(mActivityId).toString());
        requestparams.put("trackId", (new StringBuilder()).append("").append(mTrackId).toString());
        sharecontentmodel.activityId = (new StringBuilder()).append(mActivityId).append("").toString();
          goto _L14
_L8:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/special/share/content").toString();
        requestparams.put("specialId", String.valueOf(mSubjectModel.specialId));
        sharecontentmodel.specialId = (new StringBuilder()).append(mSubjectModel.specialId).append("").toString();
          goto _L14
_L9:
        obj = mSimpleData.url;
        obj1 = URLEncoder.encode(mSimpleData.url, "utf-8");
        obj = obj1;
_L18:
        obj1 = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/link/share/content").toString();
        requestparams.put("linkUrl", ((String) (obj)));
        requestparams.put("linkTitle", mSimpleData.title);
        requestparams.put("linkCoverPath", mSimpleData.picUrl);
        obj = obj1;
          goto _L14
_L13:
        sharecontentmodel.ret = i;
        sharecontentmodel.msg = ((JSONObject) (obj1)).getString("msg");
_L16:
        obj = sharecontentmodel;
          goto _L15
        Exception exception;
        exception;
        exception = sharecontentmodel;
_L17:
        exception.ret = -1;
        exception.msg = "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
          goto _L15
_L11:
        sharecontentmodel.ret = -1;
        sharecontentmodel.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A52";
          goto _L16
        exception;
        exception = ((Exception) (obj1));
          goto _L17
        Exception exception1;
        exception1;
          goto _L18
    }

    private void initHeadView()
    {
        if (mScoreManage == null)
        {
            return;
        }
        String s;
        if (mScoreManage.getObtainScoreTotalTimes(1) <= 1)
        {
            s = "\u9996\u6B21";
        } else
        {
            s = (new StringBuilder()).append("\u524D").append(mScoreManage.getObtainScoreTotalTimes(1)).append("\u6B21").toString();
        }
        mHeadTv.setText(Html.fromHtml((new StringBuilder()).append("\u6BCF\u5929<font color=\"#ff6000\",textSize=\"16\">").append(s).append("</font>\u5206\u4EAB\u5230").append("<font color=\"#ff6000\",textSize=\"16\">\u5FAE\u4FE1\u670B\u53CB\u5708\uFF0CQQ\u7A7A\u95F4\uFF0C\u65B0\u6D6A\u5FAE\u535A</font>").append("\u53EF\u4EE5\u83B7\u5F97").append("<font color=\"#ff6000\",textSize=\"16\">100\u79EF\u5206</font>").append("\u54E6~").toString()));
    }

    private boolean isInfoNull()
    {
        return mSoundInfo == null && mAlbumModel == null && mHomeModel == null && mTrackId < 0L && mActivityId < 0L && mSubjectModel == null && mSimpleData == null;
    }

    private void loadData()
    {
        if (!UserInfoMannage.hasLogined())
        {
            return;
        } else
        {
            RequestParams requestparams = new RequestParams();
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.append(2);
            stringbuilder.append(",");
            stringbuilder.append(1);
            stringbuilder.append(",");
            stringbuilder.append(3);
            stringbuilder.append(",");
            stringbuilder.append(10);
            stringbuilder.append(",");
            stringbuilder.append(8);
            stringbuilder.append(",");
            stringbuilder.append(11);
            stringbuilder.append(",");
            stringbuilder.append(9);
            requestparams.put("behaviors", stringbuilder.toString());
            f.a().a("mobile/api1/point/query/multi/earn/rest", requestparams, DataCollectUtil.getDataFromView(fromBindView), new com.ximalaya.ting.android.activity.share.b(this));
            return;
        }
    }

    private void onClickShareToQQ(ShareContentModel sharecontentmodel)
    {
        Bundle bundle;
        mTencent = Tencent.createInstance(com.ximalaya.ting.android.a.b.c, mActivity);
        bundle = new Bundle();
        bundle.putString("imageUrl", (new StringBuilder()).append(sharecontentmodel.picUrl).append("").toString());
        bundle.putString("targetUrl", (new StringBuilder()).append(sharecontentmodel.url).append("").toString());
        sharecontentmodel.shareFrom = mShareType;
        mShareType;
        JVM INSTR tableswitch 11 19: default 144
    //                   11 176
    //                   12 294
    //                   13 412
    //                   14 507
    //                   15 597
    //                   16 687
    //                   17 777
    //                   18 867
    //                   19 957;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10
_L1:
        bundle.putString("appName", "\u559C\u9A6C\u62C9\u96C5");
        mTencent.shareToQQ(mActivity, bundle, new a(sharecontentmodel));
        return;
_L2:
        bundle.putString("title", (new StringBuilder()).append(mSoundInfo.title).append("").toString());
        if (TextUtils.isEmpty(mSoundInfo.nickname))
        {
            bundle.putString("summary", (new StringBuilder()).append(mSoundInfo.title).append("").toString());
        } else
        {
            bundle.putString("summary", (new StringBuilder()).append(mSoundInfo.nickname).append("").toString());
        }
        continue; /* Loop/switch isn't completed */
_L3:
        bundle.putString("title", (new StringBuilder()).append(mAlbumModel.title).append("").toString());
        if (TextUtils.isEmpty(mAlbumModel.nickname))
        {
            bundle.putString("summary", (new StringBuilder()).append(mAlbumModel.title).append("").toString());
        } else
        {
            bundle.putString("summary", (new StringBuilder()).append(mAlbumModel.nickname).append("").toString());
        }
        continue; /* Loop/switch isn't completed */
_L4:
        bundle.putString("title", (new StringBuilder()).append(mHomeModel.nickname).append("").toString());
        if (!TextUtils.isEmpty(mHomeModel.nickname))
        {
            bundle.putString("summary", (new StringBuilder()).append(mHomeModel.nickname).append("").toString());
        } else
        {
            bundle.putString("summary", "\u65E0\u540D\u6C0F- -\uFF01");
        }
        continue; /* Loop/switch isn't completed */
_L5:
        if (!TextUtils.isEmpty(sharecontentmodel.title))
        {
            bundle.putString("title", (new StringBuilder()).append(sharecontentmodel.title).append("").toString());
        } else
        {
            bundle.putString("title", "\u6D3B\u52A8");
        }
        if (!TextUtils.isEmpty(sharecontentmodel.content))
        {
            bundle.putString("summary", sharecontentmodel.content);
        } else
        {
            bundle.putString("summary", "\u5FEB\u6765\u53C2\u52A0\u6D3B\u52A8\u5427- -\uFF01");
        }
        continue; /* Loop/switch isn't completed */
_L6:
        if (!TextUtils.isEmpty(sharecontentmodel.title))
        {
            bundle.putString("title", (new StringBuilder()).append(sharecontentmodel.title).append("").toString());
        } else
        {
            bundle.putString("title", "\u6D3B\u52A8");
        }
        if (!TextUtils.isEmpty(sharecontentmodel.content))
        {
            bundle.putString("summary", sharecontentmodel.content);
        } else
        {
            bundle.putString("summary", "\u8D5E\uFF01\u4E00\u8D77\u5E2E\u6211\u7ED9\u58F0\u97F3\u6295\u4E2A\u7968!");
        }
        continue; /* Loop/switch isn't completed */
_L7:
        if (!TextUtils.isEmpty(sharecontentmodel.title))
        {
            bundle.putString("title", (new StringBuilder()).append(sharecontentmodel.title).append("").toString());
        } else
        {
            bundle.putString("title", "\u6D3B\u52A8");
        }
        if (!TextUtils.isEmpty(sharecontentmodel.content))
        {
            bundle.putString("summary", sharecontentmodel.content);
        } else
        {
            bundle.putString("summary", "\u6211\u6B63\u5728\u542C\uFF0C\u5FEB\u6765\u770B\u770B\u5427\uFF01");
        }
        continue; /* Loop/switch isn't completed */
_L8:
        if (!TextUtils.isEmpty(sharecontentmodel.title))
        {
            bundle.putString("title", (new StringBuilder()).append(sharecontentmodel.title).append("").toString());
        } else
        {
            bundle.putString("title", "\u542C\u5355");
        }
        if (!TextUtils.isEmpty(sharecontentmodel.content))
        {
            bundle.putString("summary", sharecontentmodel.content);
        } else
        {
            bundle.putString("summary", "\u6211\u6B63\u5728\u542C\uFF0C\u5FEB\u6765\u770B\u770B\u5427\uFF01");
        }
        continue; /* Loop/switch isn't completed */
_L9:
        if (!TextUtils.isEmpty(sharecontentmodel.title))
        {
            bundle.putString("title", (new StringBuilder()).append(sharecontentmodel.title).append("").toString());
        } else
        {
            bundle.putString("title", "\u542C\u5355");
        }
        if (!TextUtils.isEmpty(sharecontentmodel.content))
        {
            bundle.putString("summary", sharecontentmodel.content);
        } else
        {
            bundle.putString("summary", "\u6211\u6B63\u5728\u542C\uFF0C\u5FEB\u6765\u770B\u770B\u5427\uFF01");
        }
        continue; /* Loop/switch isn't completed */
_L10:
        if (!TextUtils.isEmpty(sharecontentmodel.title))
        {
            bundle.putString("title", (new StringBuilder()).append(sharecontentmodel.title).append("").toString());
        } else
        {
            bundle.putString("title", "\u6765\u81EA\u559C\u9A6C\u62C9\u96C5\u7684\u5206\u4EAB");
        }
        if (!TextUtils.isEmpty(sharecontentmodel.content))
        {
            bundle.putString("summary", sharecontentmodel.content);
        } else
        {
            bundle.putString("summary", "");
        }
        if (true) goto _L1; else goto _L11
_L11:
    }

    private void sendMsg2(String s)
    {
        try
        {
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("smsto:"));
            intent.putExtra("sms_body", s);
            ((ClipboardManager)mActivity.getSystemService("clipboard")).setText(s);
            Toast.makeText(mActivity, "\u6D88\u606F\u5DF2\u590D\u5236\u5230\u7CFB\u7EDF\u526A\u8D34\u677F", 1).show();
            mActivity.startActivityForResult(intent, 2);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Toast.makeText(mActivity, "\u60A8\u7684\u8BBE\u5907\u4E0D\u652F\u6301\u77ED\u4FE1\u529F\u80FD", 1).show();
        }
    }

    public static void statShare(Context context, ShareContentModel sharecontentmodel)
    {
        RequestParams requestparams;
        HashMap hashmap;
        hashmap = new HashMap();
        requestparams = new RequestParams();
        requestparams.put("tpName", sharecontentmodel.thirdPartyName);
        requestparams.put("rowKey", sharecontentmodel.rowKey);
        hashmap.put("tpName", sharecontentmodel.thirdPartyName);
        hashmap.put("rowKey", sharecontentmodel.rowKey);
        sharecontentmodel.shareFrom;
        JVM INSTR tableswitch 11 18: default 112
    //                   11 164
    //                   12 190
    //                   13 242
    //                   14 216
    //                   15 216
    //                   16 216
    //                   17 112
    //                   18 268;
           goto _L1 _L2 _L3 _L4 _L5 _L5 _L5 _L1 _L6
_L1:
        requestparams.put("signature", ToolUtil.genSignature(context, hashmap));
        context = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/share/client/stat").toString();
        f.a().b(context, requestparams, null, new com.ximalaya.ting.android.activity.share.a());
        return;
_L2:
        requestparams.put("trackId", sharecontentmodel.trackId);
        hashmap.put("trackId", sharecontentmodel.trackId);
        continue; /* Loop/switch isn't completed */
_L3:
        requestparams.put("albumId", sharecontentmodel.albumId);
        hashmap.put("albumId", sharecontentmodel.albumId);
        continue; /* Loop/switch isn't completed */
_L5:
        requestparams.put("activityId", sharecontentmodel.activityId);
        hashmap.put("activityId", sharecontentmodel.activityId);
        continue; /* Loop/switch isn't completed */
_L4:
        requestparams.put("shareUid", sharecontentmodel.shareUid);
        hashmap.put("shareUid", sharecontentmodel.shareUid);
        continue; /* Loop/switch isn't completed */
_L6:
        requestparams.put("specialId", sharecontentmodel.specialId);
        hashmap.put("specialId", sharecontentmodel.specialId);
        if (true) goto _L1; else goto _L7
_L7:
    }

    private void toShare(String s)
    {
        if ("message".equals(s)) goto _L2; else goto _L1
_L1:
        Intent intent;
        intent = new Intent(mActivity, com/ximalaya/ting/android/activity/share/PlayShareActivity);
        intent.putExtra("content", contentStr);
        intent.putExtra("thirdpartyNames", s);
        intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(fromBindView));
        mShareType;
        JVM INSTR tableswitch 11 18: default 112
    //                   11 125
    //                   12 155
    //                   13 185
    //                   14 215
    //                   15 242
    //                   16 281
    //                   17 112
    //                   18 320;
           goto _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L3 _L10
_L3:
        mActivity.startActivity(intent);
        dismiss();
        return;
_L4:
        intent.putExtra("SHARE_WHAT", mShareType);
        intent.putExtra("trackId", mSoundInfo.trackId);
        continue; /* Loop/switch isn't completed */
_L5:
        intent.putExtra("SHARE_WHAT", mShareType);
        intent.putExtra("albumId", mAlbumModel.albumId);
        continue; /* Loop/switch isn't completed */
_L6:
        intent.putExtra("SHARE_WHAT", mShareType);
        intent.putExtra("shareUid", mHomeModel.uid);
        continue; /* Loop/switch isn't completed */
_L7:
        intent.putExtra("SHARE_WHAT", mShareType);
        intent.putExtra("activityId", mActivityId);
        continue; /* Loop/switch isn't completed */
_L8:
        intent.putExtra("SHARE_WHAT", mShareType);
        intent.putExtra("activityId", mActivityId);
        intent.putExtra("trackId", mTrackId);
        continue; /* Loop/switch isn't completed */
_L9:
        intent.putExtra("SHARE_WHAT", mShareType);
        intent.putExtra("activityId", mActivityId);
        intent.putExtra("trackId", mTrackId);
        continue; /* Loop/switch isn't completed */
_L10:
        intent.putExtra("SHARE_WHAT", mShareType);
        intent.putExtra("specialId", mSubjectModel.specialId);
        if (true) goto _L3; else goto _L2
_L2:
        sendMsg2(content.content);
        return;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        int i = ToolUtil.getScreenHeight(mActivity);
        int j = ToolUtil.dp2px(mActivity, 60F);
        bundle = new android.view.ViewGroup.LayoutParams(ToolUtil.getScreenWidth(mActivity) - ToolUtil.dp2px(mActivity, 60F), i - j);
        setContentView(layoutInflater.inflate(0x7f0301c7, null), bundle);
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        listView = (ListView)findViewById(0x7f0a063a);
        mHeadTv = (TextView)findViewById(0x7f0a06d2);
        mHasLogined = UserInfoMannage.hasLogined();
        mScoreManage = ScoreManage.getInstance(mActivity.getApplicationContext());
        initHeadView();
        loadData();
        mShareListAdapter = new c();
        listView.setAdapter(mShareListAdapter);
        listView.setOnItemClickListener(new com.ximalaya.ting.android.activity.share.c(this));
    }

    public void sendWXShare(int i, ShareContentModel sharecontentmodel, byte abyte0[])
    {
        if (sharecontentmodel != null) goto _L2; else goto _L1
_L1:
        showToast("\u83B7\u53D6\u5206\u4EAB\u5185\u5BB9\u5F02\u5E38\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5");
_L12:
        return;
_L2:
        WXMediaMessage wxmediamessage;
        if (isInfoNull())
        {
            showToast(sharecontentmodel.msg);
            return;
        }
        if (sharecontentmodel.ret != 0)
        {
            break MISSING_BLOCK_LABEL_885;
        }
        sharecontentmodel.shareFrom = mShareType;
        ((MyApplication)(MyApplication)mActivity.getApplication()).d = sharecontentmodel;
        wxmediamessage = new WXMediaMessage();
        mShareType;
        JVM INSTR tableswitch 11 19: default 124
    //                   11 222
    //                   12 322
    //                   13 414
    //                   14 506
    //                   15 567
    //                   16 636
    //                   17 124
    //                   18 697
    //                   19 758;
           goto _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L3 _L10 _L11
_L3:
        Object obj;
        if (abyte0 != null)
        {
            if (abyte0.length > 32768)
            {
                wxmediamessage.thumbData = ToolUtil.imageZoom32(abyte0);
            } else
            {
                wxmediamessage.thumbData = abyte0;
            }
        } else
        {
            wxmediamessage.thumbData = ToolUtil.imageZoom32(BitmapFactory.decodeResource(mActivity.getResources(), 0x7f02001e));
        }
        sharecontentmodel = new com.tencent.mm.sdk.modelmsg.SendMessageToWX.Req();
        sharecontentmodel.transaction = buildTransaction("webpage");
        sharecontentmodel.message = wxmediamessage;
        if (i == 0)
        {
            sharecontentmodel.scene = 0;
        } else
        {
            sharecontentmodel.scene = 1;
        }
        abyte0 = WXAPIFactory.createWXAPI(mActivity.getApplicationContext(), com.ximalaya.ting.android.a.b.b, false);
        abyte0.registerApp(com.ximalaya.ting.android.a.b.b);
        if (!abyte0.sendReq(sharecontentmodel))
        {
            showToast("\u5206\u4EAB\u5931\u8D25\uFF0C\u8BF7\u5148\u5B89\u88C5\u5FAE\u4FE1");
            return;
        }
        if (true) goto _L12; else goto _L4
_L4:
        obj = new WXMusicObject();
        if (sharecontentmodel.url != null)
        {
            obj.musicUrl = sharecontentmodel.url;
        }
        if (sharecontentmodel.audioUrl != null)
        {
            obj.musicDataUrl = sharecontentmodel.audioUrl;
        }
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        if (i == 0)
        {
            wxmediamessage.title = mSoundInfo.title;
            wxmediamessage.description = sharecontentmodel.content;
        } else
        {
            wxmediamessage.title = mSoundInfo.title;
            wxmediamessage.description = sharecontentmodel.content;
        }
          goto _L3
_L5:
        obj = new WXWebpageObject();
        obj.webpageUrl = "http://www.ximalaya.com/";
        if (sharecontentmodel.url != null)
        {
            obj.webpageUrl = sharecontentmodel.url;
        }
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        if (i == 0)
        {
            wxmediamessage.title = mAlbumModel.title;
            wxmediamessage.description = sharecontentmodel.content;
        } else
        {
            wxmediamessage.description = sharecontentmodel.content;
            wxmediamessage.title = mAlbumModel.title;
        }
          goto _L3
_L6:
        obj = new WXWebpageObject();
        obj.webpageUrl = "http://www.ximalaya.com/";
        if (sharecontentmodel.url != null)
        {
            obj.webpageUrl = sharecontentmodel.url;
        }
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        if (i == 0)
        {
            wxmediamessage.title = mHomeModel.nickname;
            wxmediamessage.description = sharecontentmodel.content;
        } else
        {
            wxmediamessage.title = mHomeModel.nickname;
            wxmediamessage.description = sharecontentmodel.content;
        }
          goto _L3
_L7:
        obj = new WXWebpageObject();
        obj.webpageUrl = "http://www.ximalaya.com/";
        if (sharecontentmodel.url != null)
        {
            obj.webpageUrl = sharecontentmodel.url;
        }
        wxmediamessage.title = sharecontentmodel.title;
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        wxmediamessage.description = sharecontentmodel.content;
          goto _L3
_L8:
        obj = new WXMusicObject();
        if (sharecontentmodel.url != null)
        {
            obj.musicUrl = sharecontentmodel.url;
        }
        if (sharecontentmodel.audioUrl != null)
        {
            obj.musicDataUrl = sharecontentmodel.audioUrl;
        }
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        wxmediamessage.title = sharecontentmodel.title;
        wxmediamessage.description = sharecontentmodel.content;
          goto _L3
_L9:
        obj = new WXWebpageObject();
        obj.webpageUrl = "http://www.ximalaya.com/";
        if (sharecontentmodel.url != null)
        {
            obj.webpageUrl = sharecontentmodel.url;
        }
        wxmediamessage.title = sharecontentmodel.title;
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        wxmediamessage.description = sharecontentmodel.content;
          goto _L3
_L10:
        obj = new WXWebpageObject();
        obj.webpageUrl = "http://www.ximalaya.com/";
        if (sharecontentmodel.url != null)
        {
            obj.webpageUrl = sharecontentmodel.url;
        }
        wxmediamessage.title = sharecontentmodel.title;
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        wxmediamessage.description = sharecontentmodel.content;
          goto _L3
_L11:
        obj = new WXWebpageObject();
        obj.webpageUrl = "http://www.ximalaya.com/";
        if (sharecontentmodel.url != null)
        {
            obj.webpageUrl = sharecontentmodel.url;
        }
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        if (i == 0)
        {
            wxmediamessage.title = sharecontentmodel.title;
            wxmediamessage.description = sharecontentmodel.content;
        } else
        {
            wxmediamessage.title = sharecontentmodel.title;
            wxmediamessage.description = sharecontentmodel.content;
        }
          goto _L3
        showToast(sharecontentmodel.msg);
        return;
    }

    void share2WeixinGroup()
    {
        (new d()).myexec(new Object[] {
            Integer.valueOf(1)
        });
    }

    public void showToast(String s)
    {
        if (mActivity != null)
        {
            Toast.makeText(mActivity, s, 0).show();
        }
    }









/*
    static ShareContentModel access$1502(BaseShareDialog basesharedialog, ShareContentModel sharecontentmodel)
    {
        basesharedialog.content = sharecontentmodel;
        return sharecontentmodel;
    }

*/










/*
    static String access$302(BaseShareDialog basesharedialog, String s)
    {
        basesharedialog.contentStr = s;
        return s;
    }

*/






}
