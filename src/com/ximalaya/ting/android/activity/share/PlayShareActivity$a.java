// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.app.ProgressDialog;
import android.widget.EditText;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.share.ShareContentModel;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            PlayShareActivity, f

private class <init> extends MyAsyncTask
{

    ProgressDialog a;
    final PlayShareActivity b;

    protected transient String a(Void avoid[])
    {
        Object obj;
        avoid = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/auth/feed").toString();
        obj = new RequestParams();
        ((RequestParams) (obj)).put("content", PlayShareActivity.access$000(b).getText().toString().trim());
        ((RequestParams) (obj)).put("picUrl", PlayShareActivity.access$100(b).picUrl);
        ((RequestParams) (obj)).put("tpName", PlayShareActivity.access$200(b));
        ((RequestParams) (obj)).put("rowKey", PlayShareActivity.access$100(b).rowKey);
        PlayShareActivity.access$300(b);
        JVM INSTR tableswitch 11 19: default 156
    //                   11 205
    //                   12 239
    //                   13 273
    //                   14 307
    //                   15 341
    //                   16 406
    //                   17 156
    //                   18 471
    //                   19 490;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L1 _L8 _L9
_L1:
        avoid = f.a().b(avoid, ((RequestParams) (obj)), b.nextButton, null);
        if (avoid == null) goto _L11; else goto _L10
_L10:
        boolean flag;
        try
        {
            avoid = JSON.parseObject(avoid);
            obj = avoid.get("ret").toString();
            flag = "0".equals(obj);
        }
        // Misplaced declaration of an exception variable
        catch (Void avoid[])
        {
            return "\u89E3\u6790\u6570\u636E\u51FA\u9519";
        }
        if (!flag) goto _L13; else goto _L12
_L12:
        return ((String) (obj));
_L2:
        ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append(PlayShareActivity.access$400(b)).append("").toString());
        continue; /* Loop/switch isn't completed */
_L3:
        ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append(PlayShareActivity.access$500(b)).append("").toString());
        continue; /* Loop/switch isn't completed */
_L4:
        ((RequestParams) (obj)).put("shareUid", (new StringBuilder()).append(PlayShareActivity.access$600(b)).append("").toString());
        continue; /* Loop/switch isn't completed */
_L5:
        ((RequestParams) (obj)).put("activityId", (new StringBuilder()).append(PlayShareActivity.access$700(b)).append("").toString());
        continue; /* Loop/switch isn't completed */
_L6:
        ((RequestParams) (obj)).put("activityId", (new StringBuilder()).append(PlayShareActivity.access$700(b)).append("").toString());
        ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append(PlayShareActivity.access$400(b)).append("").toString());
        continue; /* Loop/switch isn't completed */
_L7:
        ((RequestParams) (obj)).put("activityId", (new StringBuilder()).append(PlayShareActivity.access$700(b)).append("").toString());
        ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append(PlayShareActivity.access$400(b)).append("").toString());
        continue; /* Loop/switch isn't completed */
_L8:
        ((RequestParams) (obj)).put("specialId", String.valueOf(PlayShareActivity.access$800(b)));
        continue; /* Loop/switch isn't completed */
_L9:
        ((RequestParams) (obj)).put("specialId", PlayShareActivity.access$100(b).url);
        ((RequestParams) (obj)).put("specialId", PlayShareActivity.access$100(b).title);
        continue; /* Loop/switch isn't completed */
_L13:
        if ("222".equals(obj)) goto _L12; else goto _L14
_L14:
        avoid = avoid.getString("msg");
        return avoid;
_L11:
        return "\u7F51\u7EDC\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
        if (true) goto _L1; else goto _L15
_L15:
    }

    protected void a(String s)
    {
        if (b != null && !b.isFinishing()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (a != null)
        {
            a.cancel();
            a = null;
        }
        if (!"0".equals(s)) goto _L4; else goto _L3
_L3:
        b.showToast("\u5206\u4EAB\u6210\u529F");
        if (!"tSina".equals(PlayShareActivity.access$200(b))) goto _L6; else goto _L5
_L5:
        ToolUtil.onEvent(b, "SHARE_T_SINA_SUCCESS");
        if (PlayShareActivity.access$900(b) != null)
        {
            PlayShareActivity.access$900(b).onShareFinishMain(2);
        }
_L7:
        b.finish();
        return;
_L6:
        if ("tQQ".equals(PlayShareActivity.access$200(b)))
        {
            if (PlayShareActivity.access$900(b) != null)
            {
                PlayShareActivity.access$900(b).onShareFinishMain(9);
            }
            ToolUtil.onEvent(b, "SHARE_T_QQ_SUCCESS");
        } else
        if ("renren".equals(PlayShareActivity.access$200(b)))
        {
            if (PlayShareActivity.access$900(b) != null)
            {
                PlayShareActivity.access$900(b).onShareFinishMain(11);
            }
            ToolUtil.onEvent(b, "SHARE_RENREN_SUCCESS");
        }
        if (true) goto _L7; else goto _L4
_L4:
        if ("222".equals(s))
        {
            if ("tSina".equals(PlayShareActivity.access$200(b)))
            {
                PlayShareActivity.access$1000(b);
                return;
            }
            if ("tQQ".equals(PlayShareActivity.access$200(b)))
            {
                PlayShareActivity.access$1100(b);
                return;
            }
            if ("renren".equals(PlayShareActivity.access$200(b)))
            {
                PlayShareActivity.access$1200(b);
                return;
            }
        } else
        {
            b.showToast(s);
            return;
        }
        if (true) goto _L1; else goto _L8
_L8:
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((String)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        a = new MyProgressDialog(b);
        a.setTitle("\u63D0\u793A");
        a.setMessage("\u6B63\u5728\u5E2E\u60A8\u5206\u4EAB\u5185\u5BB9\u4E2D...");
        a.show();
    }

    private g(PlayShareActivity playshareactivity)
    {
        b = playshareactivity;
        super();
    }

    b(PlayShareActivity playshareactivity, com.ximalaya.ting.android.activity.share.f f1)
    {
        this(playshareactivity);
    }
}
