// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            PlayShareActivity

class f
    implements TextWatcher
{

    final PlayShareActivity a;

    f(PlayShareActivity playshareactivity)
    {
        a = playshareactivity;
        super();
    }

    public void afterTextChanged(Editable editable)
    {
        PlayShareActivity.access$1300(a).setText((new StringBuilder()).append("\u5269\u4F59").append(140 - ToolUtil.getShareContentLength(PlayShareActivity.access$000(a).getText().toString().trim())).append("\u5B57").toString());
        if (editable.length() == 0)
        {
            a.nextButton.setEnabled(false);
        } else
        if (!a.nextButton.isEnabled())
        {
            a.nextButton.setEnabled(true);
            return;
        }
    }

    public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
    {
    }

    public void onTextChanged(CharSequence charsequence, int i, int j, int k)
    {
    }
}
