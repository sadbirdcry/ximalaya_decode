// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import org.json.JSONObject;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            d, BaseShareDialog

class e
    implements IUiListener
{

    final d a;

    e(d d1)
    {
        a = d1;
        super();
    }

    public void onCancel()
    {
        Logger.d("DoShareQQZoneContentTask", "qq ZONE \u53D6\u6D88\u5206\u4EAB");
    }

    public void onComplete(Object obj)
    {
        Logger.d("DoShareQQZoneContentTask", (new StringBuilder()).append("\u5206\u4EABQQ\u7A7A\u95F4\u540E\uFF1AresponseObject=").append(obj).toString());
        int i;
        try
        {
            i = ((JSONObject)obj).getInt("ret");
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            i = -10;
        }
        Logger.d("DoShareQQZoneContentTask", (new StringBuilder()).append("\u5206\u4EABQQ\u7A7A\u95F4\u540E\uFF1Aret=").append(i).toString());
        if (i == 0)
        {
            ToolUtil.onEvent(a.d, "SHARE_QZONE_SUCCESS");
            BaseShareDialog.statShare(a.d, d.a(a));
            if (d.b(a) != null)
            {
                d.b(a).onShareFinishMain(3);
            }
        }
    }

    public void onError(UiError uierror)
    {
        a.b = true;
        a.c = uierror.errorMessage;
    }
}
