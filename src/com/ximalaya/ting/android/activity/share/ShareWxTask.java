// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.BitmapFactory;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXMusicObject;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.ProductModel;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.share.ShareContentModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

public class ShareWxTask extends MyAsyncTask
{

    private static final int SHARE_TYPE_ALBUM = 2;
    public static final int SHARE_TYPE_QSUICHETING = 5;
    public static final int SHARE_TYPE_SUICHETING = 4;
    private static final int SHARE_TYPE_TRACK = 1;
    private static final int SHARE_TYPE_USER = 3;
    byte b[];
    int buttonIndex;
    private String contentStr;
    private LoginInfoModel loginInfoModel;
    Activity mActivity;
    AlbumModel mAlbumModel;
    ProgressDialog mProgressDialog;
    private int mShareType;
    SoundInfo mSoundInfo;

    public ShareWxTask(Activity activity)
    {
        mShareType = -1;
        buttonIndex = 0;
        mActivity = activity;
    }

    public ShareWxTask(Activity activity, int i)
    {
        mShareType = -1;
        buttonIndex = 0;
        mActivity = activity;
        mAlbumModel = null;
        mShareType = i;
    }

    public ShareWxTask(Activity activity, AlbumModel albummodel)
    {
        mShareType = -1;
        buttonIndex = 0;
        mActivity = activity;
        mAlbumModel = albummodel;
        mSoundInfo = null;
        mShareType = 2;
    }

    public ShareWxTask(Activity activity, SoundInfo soundinfo)
    {
        mShareType = -1;
        buttonIndex = 0;
        mActivity = activity;
        mSoundInfo = soundinfo;
        mAlbumModel = null;
        mShareType = 1;
    }

    private String buildTransaction(String s)
    {
        if (s == null)
        {
            return String.valueOf(System.currentTimeMillis());
        } else
        {
            return (new StringBuilder()).append(s).append(System.currentTimeMillis()).toString();
        }
    }

    private ShareContentModel getWXContent(int i)
    {
        ShareContentModel sharecontentmodel;
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        sharecontentmodel = new ShareContentModel();
        if (1 != mShareType && 2 != mShareType || mAlbumModel != null || mSoundInfo != null) goto _L2; else goto _L1
_L1:
        sharecontentmodel.ret = -1;
        sharecontentmodel.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
_L14:
        return sharecontentmodel;
_L2:
        Object obj1 = new RequestParams();
        mShareType;
        JVM INSTR tableswitch 1 5: default 108
    //                   1 177
    //                   2 234
    //                   3 541
    //                   4 291
    //                   5 416;
           goto _L3 _L4 _L5 _L6 _L7 _L8
_L3:
        Object obj = null;
_L13:
        if (i == 0)
        {
            ((RequestParams) (obj1)).put("tpName", "weixin");
        } else
        {
            ((RequestParams) (obj1)).put("tpName", "weixinGroup");
        }
        obj1 = f.a().a(((String) (obj)), ((RequestParams) (obj1)), null, null);
        if (obj1 == null) goto _L10; else goto _L9
_L9:
        obj = JSON.parseObject(((String) (obj1)));
        i = ((JSONObject) (obj)).getIntValue("ret");
        if (i != 0) goto _L12; else goto _L11
_L11:
        obj = (ShareContentModel)JSON.parseObject(((String) (obj1)), com/ximalaya/ting/android/model/share/ShareContentModel);
        contentStr = ((String) (obj1));
_L15:
        return ((ShareContentModel) (obj));
_L4:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/track/share/content").toString();
        ((RequestParams) (obj1)).put("trackId", (new StringBuilder()).append("").append(mSoundInfo.trackId).toString());
          goto _L13
_L5:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/v1/album/share/content").toString();
        ((RequestParams) (obj1)).put("albumId", (new StringBuilder()).append("").append(mAlbumModel.albumId).toString());
          goto _L13
_L7:
        sharecontentmodel.ret = -1;
        obj = MyDeviceManager.getInstance(mActivity.getApplicationContext()).getProductModels();
        if (obj != null)
        {
            i = 0;
            while (i < ((List) (obj)).size()) 
            {
                obj1 = (ProductModel)((List) (obj)).get(i);
                if (((ProductModel) (obj1)).title != null && ((ProductModel) (obj1)).title.endsWith("\u968F\u8F66\u542C"))
                {
                    sharecontentmodel.picUrl = ((ProductModel) (obj1)).logo;
                    sharecontentmodel.ret = 0;
                    sharecontentmodel.msg = ((ProductModel) (obj1)).subtitle;
                    sharecontentmodel.url = ((ProductModel) (obj1)).url;
                    sharecontentmodel.title = ((ProductModel) (obj1)).title;
                    sharecontentmodel.content = ((ProductModel) (obj1)).subtitle;
                }
                i++;
            }
        }
          goto _L14
_L8:
        sharecontentmodel.ret = -1;
        obj = MyDeviceManager.getInstance(mActivity.getApplicationContext()).getProductModels();
        if (obj != null)
        {
            i = 0;
            while (i < ((List) (obj)).size()) 
            {
                obj1 = (ProductModel)((List) (obj)).get(i);
                if (((ProductModel) (obj1)).title != null && ((ProductModel) (obj1)).title.endsWith("\u968F\u8F66\u542CQ\u7248"))
                {
                    sharecontentmodel.picUrl = ((ProductModel) (obj1)).logo;
                    sharecontentmodel.ret = 0;
                    sharecontentmodel.msg = ((ProductModel) (obj1)).subtitle;
                    sharecontentmodel.url = ((ProductModel) (obj1)).url;
                    sharecontentmodel.title = ((ProductModel) (obj1)).title;
                    sharecontentmodel.content = ((ProductModel) (obj1)).subtitle;
                }
                i++;
            }
        }
          goto _L14
_L6:
        obj = null;
          goto _L13
_L12:
        sharecontentmodel.ret = i;
        sharecontentmodel.msg = ((JSONObject) (obj)).getString("msg");
_L16:
        obj = sharecontentmodel;
          goto _L15
        obj;
        obj = sharecontentmodel;
_L17:
        obj.ret = -1;
        obj.msg = "\u89E3\u6790\u6570\u636E\u5F02\u5E38";
          goto _L15
_L10:
        sharecontentmodel.ret = -1;
        sharecontentmodel.msg = "\u7F51\u7EDC\u8FDE\u63A5\u5F02\u5E38\uFF0C\u8BF7\u68C0\u67E5\u7F51\u7EDC\u662F\u5426\u8FDE\u63A5";
          goto _L16
        Exception exception;
        exception;
          goto _L17
    }

    protected transient ShareContentModel doInBackground(Object aobj[])
    {
        buttonIndex = ((Integer)aobj[0]).intValue();
        ShareContentModel sharecontentmodel = getWXContent(buttonIndex);
        aobj = sharecontentmodel;
        if (sharecontentmodel == null)
        {
            aobj = new ShareContentModel();
            aobj.ret = 0;
        }
        if (((ShareContentModel) (aobj)).picUrl != null)
        {
            b = ToolUtil.loadByteArrayFromNetwork(((ShareContentModel) (aobj)).picUrl);
        }
        return ((ShareContentModel) (aobj));
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground(aobj);
    }

    protected void onPostExecute(ShareContentModel sharecontentmodel)
    {
        if (mProgressDialog != null && mProgressDialog.isShowing())
        {
            mProgressDialog.dismiss();
        }
        if (sharecontentmodel != null)
        {
            sendWXShare(buttonIndex, sharecontentmodel, b);
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((ShareContentModel)obj);
    }

    protected void onPreExecute()
    {
        if (mProgressDialog == null)
        {
            mProgressDialog = ToolUtil.createProgressDialog(mActivity, 0, true, true);
        }
        mProgressDialog.setMessage("\u6B63\u5728\u4E3A\u60A8\u52AA\u529B\u5206\u4EAB...");
        mProgressDialog.show();
    }

    public void sendWXShare(int i, ShareContentModel sharecontentmodel, byte abyte0[])
    {
        if (1 != mShareType && 2 != mShareType || mAlbumModel != null || mSoundInfo != null) goto _L2; else goto _L1
_L1:
        showToast("\u83B7\u53D6\u4FE1\u606F\u5931\u8D25");
_L7:
        return;
_L2:
        WXMediaMessage wxmediamessage;
        if (sharecontentmodel.ret != 0)
        {
            break MISSING_BLOCK_LABEL_509;
        }
        wxmediamessage = new WXMediaMessage();
        mShareType;
        JVM INSTR tableswitch 1 5: default 92
    //                   1 190
    //                   2 290
    //                   3 92
    //                   4 382
    //                   5 382;
           goto _L3 _L4 _L5 _L3 _L6 _L6
_L3:
        Object obj;
        if (abyte0 != null)
        {
            if (abyte0.length > 32768)
            {
                wxmediamessage.thumbData = ToolUtil.imageZoom32(abyte0);
            } else
            {
                wxmediamessage.thumbData = abyte0;
            }
        } else
        {
            wxmediamessage.thumbData = ToolUtil.imageZoom32(BitmapFactory.decodeResource(mActivity.getResources(), 0x7f02001e));
        }
        sharecontentmodel = new com.tencent.mm.sdk.modelmsg.SendMessageToWX.Req();
        sharecontentmodel.transaction = buildTransaction("webpage");
        sharecontentmodel.message = wxmediamessage;
        if (i == 0)
        {
            sharecontentmodel.scene = 0;
        } else
        {
            sharecontentmodel.scene = 1;
        }
        abyte0 = WXAPIFactory.createWXAPI(mActivity.getApplicationContext(), b.b, false);
        abyte0.registerApp(b.b);
        if (!abyte0.sendReq(sharecontentmodel))
        {
            showToast("\u5206\u4EAB\u5931\u8D25\uFF0C\u8BF7\u5148\u5B89\u88C5\u5FAE\u4FE1");
            return;
        }
        if (true) goto _L7; else goto _L4
_L4:
        obj = new WXMusicObject();
        if (sharecontentmodel.url != null)
        {
            obj.musicUrl = sharecontentmodel.url;
        }
        if (sharecontentmodel.audioUrl != null)
        {
            obj.musicDataUrl = sharecontentmodel.audioUrl;
        }
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        if (i == 0)
        {
            wxmediamessage.title = mSoundInfo.title;
            wxmediamessage.description = sharecontentmodel.content;
        } else
        {
            wxmediamessage.title = sharecontentmodel.content;
            wxmediamessage.description = mSoundInfo.title;
        }
          goto _L3
_L5:
        obj = new WXWebpageObject();
        obj.webpageUrl = "http://www.ximalaya.com/";
        if (sharecontentmodel.url != null)
        {
            obj.webpageUrl = sharecontentmodel.url;
        }
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        if (i == 0)
        {
            wxmediamessage.title = mAlbumModel.title;
            wxmediamessage.description = sharecontentmodel.content;
        } else
        {
            wxmediamessage.title = sharecontentmodel.content;
            wxmediamessage.description = mAlbumModel.title;
        }
          goto _L3
_L6:
        obj = new WXWebpageObject();
        obj.webpageUrl = "http://www.ximalaya.com/";
        if (sharecontentmodel.url != null)
        {
            obj.webpageUrl = sharecontentmodel.url;
        }
        wxmediamessage.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj));
        if (i == 0)
        {
            wxmediamessage.title = sharecontentmodel.title;
            wxmediamessage.description = sharecontentmodel.content;
        } else
        {
            wxmediamessage.title = sharecontentmodel.content;
            wxmediamessage.description = sharecontentmodel.title;
        }
          goto _L3
        showToast("\u83B7\u53D6\u6570\u636E\u5931\u8D25");
        return;
    }

    public void sendWXShare(boolean flag)
    {
        Object obj1 = new WXWebpageObject();
        Object obj = new WXMediaMessage();
        if (flag)
        {
            ((MyApplication)(MyApplication)mActivity.getApplication()).b = 4;
            obj1.webpageUrl = "http://m.ximalaya.com/?src=myscore_invite_weixingroup";
        } else
        {
            ((MyApplication)(MyApplication)mActivity.getApplication()).b = 2;
            obj1.webpageUrl = "http://m.ximalaya.com/?src=myscore_invite_weixin";
        }
        obj.title = "\u559C\u9A6C\u62C9\u96C5";
        obj.description = "\u8FD9\u662F\u4E2A\u6253\u53D1\u65E0\u804A\u7684\u795E\u5668\uFF0C\u6211\u6700\u8FD1\u5929\u5929\u7528\uFF0C\u771F8\u9519\uFF0C\u4F60\u8BD5\u8BD5\u770B";
        obj.thumbData = ToolUtil.imageZoom32(BitmapFactory.decodeResource(mActivity.getResources(), 0x7f020598));
        obj.mediaObject = ((com.tencent.mm.sdk.modelmsg.WXMediaMessage.IMediaObject) (obj1));
        obj1 = new com.tencent.mm.sdk.modelmsg.SendMessageToWX.Req();
        obj1.transaction = buildTransaction("webpage");
        obj1.message = ((WXMediaMessage) (obj));
        if (flag)
        {
            obj1.scene = 1;
        } else
        {
            obj1.scene = 0;
        }
        obj = WXAPIFactory.createWXAPI(mActivity.getApplicationContext(), b.b, false);
        ((IWXAPI) (obj)).registerApp(b.b);
        if (!((IWXAPI) (obj)).sendReq(((com.tencent.mm.sdk.modelbase.BaseReq) (obj1))))
        {
            showToast("\u5206\u4EAB\u5931\u8D25\uFF0C\u8BF7\u5148\u5B89\u88C5\u5FAE\u4FE1");
        }
    }

    public void showToast(String s)
    {
        if (mActivity != null)
        {
            Toast.makeText(mActivity, s, 0).show();
        }
    }
}
