// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.widget.Toast;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import com.ximalaya.ting.android.model.share.ShareContentModel;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.util.ToolUtil;
import org.json.JSONObject;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            BaseShareDialog

private class b
    implements IUiListener
{

    final BaseShareDialog a;
    private ShareContentModel b;

    protected void a(JSONObject jsonobject)
    {
        int i;
        try
        {
            i = jsonobject.getInt("ret");
        }
        // Misplaced declaration of an exception variable
        catch (JSONObject jsonobject)
        {
            i = -10;
        }
        if (i == 0)
        {
            a.showToast("\u5206\u4EABQQ\u6210\u529F");
            if (BaseShareDialog.access$000(a) != null)
            {
                BaseShareDialog.access$000(a).onShareFinishMain(10);
            }
            ToolUtil.onEvent(BaseShareDialog.access$100(a), "SHARE_QQ_FRIEND_SUCCESS");
            BaseShareDialog.statShare(BaseShareDialog.access$100(a), b);
        }
        a.dismiss();
    }

    public void onCancel()
    {
    }

    public void onComplete(Object obj)
    {
        a((JSONObject)obj);
    }

    public void onError(UiError uierror)
    {
        Toast.makeText(BaseShareDialog.access$100(a), uierror.errorMessage, 1).show();
    }

    public (BaseShareDialog basesharedialog, ShareContentModel sharecontentmodel)
    {
        a = basesharedialog;
        super();
        b = sharecontentmodel;
    }
}
