// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import com.ximalaya.ting.android.model.album.AlbumModel;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            ShareWxTask

public class ShareToWeixinDialogFragment extends DialogFragment
    implements android.view.View.OnClickListener
{

    private Button mCancelBtn;
    private ImageButton mCloseBtn;
    private Button mConfirmBtn;
    private AlbumModel mData;

    public ShareToWeixinDialogFragment()
    {
    }

    public static ShareToWeixinDialogFragment getInstance(AlbumModel albummodel)
    {
        ShareToWeixinDialogFragment sharetoweixindialogfragment = new ShareToWeixinDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", albummodel);
        sharetoweixindialogfragment.setArguments(bundle);
        return sharetoweixindialogfragment;
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        mCancelBtn.setOnClickListener(this);
        mConfirmBtn.setOnClickListener(this);
        mCloseBtn.setOnClickListener(this);
    }

    public void onClick(View view)
    {
        view.getId();
        JVM INSTR lookupswitch 3: default 40
    //                   2131362109: 41
    //                   2131362117: 46
    //                   2131362316: 41;
           goto _L1 _L2 _L3 _L2
_L1:
        return;
_L2:
        dismissAllowingStateLoss();
        return;
_L3:
        if (mData != null)
        {
            (new ShareWxTask(getActivity(), mData)).execute(new Object[] {
                Integer.valueOf(1)
            });
            dismissAllowingStateLoss();
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setStyle(0x7f0b0027, 0x7f0b0027);
        bundle = getArguments();
        if (bundle != null)
        {
            mData = (AlbumModel)bundle.getSerializable("data");
        }
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        layoutinflater = layoutinflater.inflate(0x7f030071, viewgroup, false);
        mCancelBtn = (Button)layoutinflater.findViewById(0x7f0a020c);
        mConfirmBtn = (Button)layoutinflater.findViewById(0x7f0a0145);
        mCloseBtn = (ImageButton)layoutinflater.findViewById(0x7f0a013d);
        return layoutinflater;
    }
}
