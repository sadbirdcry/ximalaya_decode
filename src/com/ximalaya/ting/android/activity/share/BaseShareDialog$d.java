// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.activity.share;

import android.app.Activity;
import android.app.ProgressDialog;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.model.share.ShareContentModel;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.activity.share:
//            BaseShareDialog

class a extends MyAsyncTask
{

    int a;
    ProgressDialog b;
    byte c[];
    final BaseShareDialog d;

    protected transient ShareContentModel a(Object aobj[])
    {
        a = ((Integer)aobj[0]).intValue();
        ((MyApplication)(MyApplication)BaseShareDialog.access$100(d).getApplication()).b = a;
        if (BaseShareDialog.access$200(d) == null)
        {
            aobj = BaseShareDialog.access$1900(d, a);
        } else
        {
            aobj = BaseShareDialog.access$200(d);
            BaseShareDialog.access$302(d, JSON.toJSONString(((Object) (aobj))));
        }
        if (aobj != null && !TextUtils.isEmpty(((ShareContentModel) (aobj)).picUrl))
        {
            c = ToolUtil.loadByteArrayFromNetwork(((ShareContentModel) (aobj)).picUrl);
        }
        return ((ShareContentModel) (aobj));
    }

    protected void a(ShareContentModel sharecontentmodel)
    {
        if (b != null && b.isShowing())
        {
            b.dismiss();
        }
        if (sharecontentmodel != null)
        {
            d.sendWXShare(a, sharecontentmodel, c);
        }
    }

    protected Object doInBackground(Object aobj[])
    {
        return a(aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((ShareContentModel)obj);
    }

    protected void onPreExecute()
    {
        if (b == null)
        {
            b = ToolUtil.createProgressDialog(BaseShareDialog.access$100(d), 0, true, true);
        }
        b.setMessage("\u6B63\u5728\u4E3A\u60A8\u52AA\u529B\u5206\u4EAB...");
        b.show();
    }

    (BaseShareDialog basesharedialog)
    {
        d = basesharedialog;
        super();
        a = 0;
    }
}
