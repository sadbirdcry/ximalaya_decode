// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.broadcast;

import android.a.a;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.os.IBinder;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.util.Logger;
import java.lang.reflect.Method;

// Referenced classes of package com.ximalaya.ting.android.broadcast:
//            BluetoothReciever

class b
    implements Runnable
{

    final BluetoothAdapter a;
    final BluetoothDevice b;
    final Context c;
    final BluetoothReciever d;

    b(BluetoothReciever bluetoothreciever, BluetoothAdapter bluetoothadapter, BluetoothDevice bluetoothdevice, Context context)
    {
        d = bluetoothreciever;
        a = bluetoothadapter;
        b = bluetoothdevice;
        c = context;
        super();
    }

    private void a(BluetoothDevice bluetoothdevice)
    {
        XiMaoBTManager.getInstance(c).connectSpp(bluetoothdevice.getAddress(), null);
    }

    public void run()
    {
        int i = 0;
_L2:
        if (i >= 400)
        {
            break MISSING_BLOCK_LABEL_237;
        }
        Logger.d("ximao", "a2dp judge conn");
        IBinder ibinder;
        Method method;
        a a1;
        int j;
        try
        {
            Thread.sleep(100L);
        }
        catch (InterruptedException interruptedexception)
        {
            interruptedexception.printStackTrace();
        }
        if (!a.isEnabled())
        {
            break MISSING_BLOCK_LABEL_267;
        }
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            break; /* Loop/switch isn't completed */
        }
        ibinder = (IBinder)Class.forName("android.os.ServiceManager").getDeclaredMethod("getService", new Class[] {
            java/lang/String
        }).invoke(null, new Object[] {
            "bluetooth_a2dp"
        });
        method = Class.forName("android.a.a").getDeclaredClasses()[0].getDeclaredMethod("asInterface", new Class[] {
            android/os/IBinder
        });
        method.setAccessible(true);
        a1 = (a)method.invoke(null, new Object[] {
            ibinder
        });
        j = ((Integer)a1.getClass().getDeclaredMethod("getSinkState", new Class[] {
            android/bluetooth/BluetoothDevice
        }).invoke((a)method.invoke(null, new Object[] {
            ibinder
        }), new Object[] {
            b
        })).intValue();
        if (j != 2 && j != 1)
        {
            break MISSING_BLOCK_LABEL_267;
        }
        Logger.d("ximao", (new StringBuilder()).append("a2dp conn success:").append(a1).toString());
        BluetoothReciever.access$200(d, c, false);
        a(b);
        return;
        Exception exception;
        exception;
        MyDeviceManager.showVersionWarning(MyApplication.a());
        exception.printStackTrace();
        Logger.e(BluetoothReciever.access$000(), exception.toString());
_L3:
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        int k = a.getProfileConnectionState(2);
        if (k == 2)
        {
            BluetoothManager.getInstance(c).cleanup();
            BluetoothManager.getInstance(c).init(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.ximao, b, false, false);
            Logger.d("ximao", (new StringBuilder()).append("a2dp conn success:").append(k).toString());
            BluetoothReciever.access$200(d, c, false);
            a(b);
            return;
        }
          goto _L3
        if (true) goto _L2; else goto _L4
_L4:
    }
}
