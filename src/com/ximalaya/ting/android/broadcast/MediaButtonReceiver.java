// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.KeyEvent;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import java.util.Timer;
import java.util.TimerTask;

// Referenced classes of package com.ximalaya.ting.android.broadcast:
//            p, o

public class MediaButtonReceiver extends BroadcastReceiver
{

    private static final int CLICK_TWO_TIMES = 3001;
    private static final String TAG = "MediaButtonReceiver";
    private static boolean firstTime = false;
    private static boolean secondTime = false;
    private static TimerTask task;
    private static boolean thirdTime = false;
    private static Timer timer;

    public MediaButtonReceiver()
    {
    }

    public void onReceive(Context context, Intent intent)
    {
        boolean flag;
        flag = false;
        task = new p(this, new o(this));
        if ("android.intent.action.MEDIA_BUTTON".equals(intent.getAction())) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if ((context = (KeyEvent)intent.getParcelableExtra("android.intent.extra.KEY_EVENT")) == null) goto _L1; else goto _L3
_L3:
        Logger.d("MediaButtonReceiver", (new StringBuilder()).append("key is:").append(context).toString());
        int i;
        boolean flag1;
        if (context.getAction() == 1)
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        Logger.d("MediaButtonReceiver", (new StringBuilder()).append("isActionUp is:").append(flag1).toString());
        if (!flag1) goto _L1; else goto _L4
_L4:
        i = context.getKeyCode();
        if (context.getEventTime() - context.getDownTime() > 2000L)
        {
            flag = true;
        }
        context = LocalMediaService.getInstance();
        if (context == null) goto _L1; else goto _L5
_L5:
        i;
        JVM INSTR lookupswitch 6: default 204
    //                   79: 225
    //                   85: 225
    //                   87: 418
    //                   88: 431
    //                   126: 225
    //                   127: 225;
           goto _L6 _L7 _L7 _L8 _L9 _L7 _L7
_L6:
        try
        {
            abortBroadcast();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            Logger.e("MediaButtonReceiver", "catch", context);
        }
        return;
_L7:
        if (!firstTime)
        {
            firstTime = true;
            if (context.isPaused())
            {
                Log.d("MediaButtonReceiver", "play");
                context.start();
            } else
            {
                Log.d("MediaButtonReceiver", "pause");
                context.pause();
            }
            if (timer != null)
            {
                timer.cancel();
                timer = null;
            }
            timer = new Timer();
            timer.schedule(task, 400L);
        } else
        if (!secondTime)
        {
            if (timer != null)
            {
                timer.cancel();
                timer = null;
            }
            timer = new Timer();
            timer.schedule(task, 400L);
            secondTime = true;
        } else
        if (!thirdTime)
        {
            if (timer != null)
            {
                timer.cancel();
                timer = null;
            }
            timer = new Timer();
            timer.schedule(task, 400L);
            thirdTime = true;
            context.playPrev(true);
        }
        continue; /* Loop/switch isn't completed */
_L8:
        if (!flag)
        {
            context.playNext(true);
        }
        continue; /* Loop/switch isn't completed */
_L9:
        if (!flag)
        {
            context.playPrev(true);
        }
        if (true) goto _L6; else goto _L10
_L10:
    }




/*
    static boolean access$002(boolean flag)
    {
        thirdTime = flag;
        return flag;
    }

*/



/*
    static boolean access$102(boolean flag)
    {
        secondTime = flag;
        return flag;
    }

*/



/*
    static boolean access$202(boolean flag)
    {
        firstTime = flag;
        return flag;
    }

*/


/*
    static Timer access$302(Timer timer1)
    {
        timer = timer1;
        return timer1;
    }

*/
}
