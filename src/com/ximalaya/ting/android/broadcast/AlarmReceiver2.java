// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.broadcast;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.setting.WakeUpSettingActivity;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.util.Utilities;

public class AlarmReceiver2 extends BroadcastReceiver
{

    public static final String ACTION_CANCEL_LATER_ALARM = "com.ximalaya.ting.android.activity.alarm.Cancel_Alarm";
    public static final int REQUEST_CODE_CANCEL_LATER_ALARM = 11;

    public AlarmReceiver2()
    {
    }

    public void onReceive(Context context, Intent intent)
    {
        intent = intent.getAction();
        if (!Utilities.isBlank(intent)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (!intent.equals("com.ximalaya.ting.android.action.START_ALARM") && !"com.ximalaya.ting.android.action.ALARM_LATER".equals(intent))
        {
            continue; /* Loop/switch isn't completed */
        }
        intent = SharedPreferencesUtil.getInstance(context);
        Object obj;
        boolean flag;
        if (intent.contains("isOnForWake"))
        {
            flag = intent.getBoolean("isOnForWake", true);
        } else
        {
            flag = false;
        }
        if (!flag) goto _L1; else goto _L3
_L3:
        obj = new Bundle();
        ((Bundle) (obj)).putString("flag", "alarm");
        intent = new Intent();
        intent.putExtras(((Bundle) (obj)));
        obj = MyApplication.a();
        if (obj != null)
        {
            intent.setClass(((Context) (obj)), com/ximalaya/ting/android/activity/setting/WakeUpSettingActivity);
            ((Activity) (obj)).startActivity(intent);
            return;
        } else
        {
            intent.addFlags(0x10000000);
            intent.addFlags(0x200000);
            intent.setComponent(new ComponentName("com.ximalaya.ting.android", "com.ximalaya.ting.android.activity.setting.WakeUpSettingActivity"));
            context.startActivity(intent);
            return;
        }
        if (!"com.ximalaya.ting.android.activity.alarm.Cancel_Alarm".equals(intent)) goto _L1; else goto _L4
_L4:
        intent = (AlarmManager)context.getSystemService("alarm");
        obj = new Intent(context, com/ximalaya/ting/android/broadcast/AlarmReceiver2);
        ((Intent) (obj)).setAction("com.ximalaya.ting.android.action.ALARM_LATER");
        intent.cancel(PendingIntent.getBroadcast(context, 0, ((Intent) (obj)), 0x8000000));
        ((NotificationManager)context.getSystemService("notification")).cancel(6);
        return;
    }
}
