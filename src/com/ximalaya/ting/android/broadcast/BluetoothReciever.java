// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.broadcast;

import android.a.a;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.ManageFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.che.CheTingIntroFragment;
import com.ximalaya.ting.android.fragment.device.che.MyBluetoothManager2;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoIntroFragment;
import com.ximalaya.ting.android.modelmanage.DexManager;
import com.ximalaya.ting.android.util.Logger;
import java.lang.ref.SoftReference;
import java.lang.reflect.Method;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.broadcast:
//            c, b, a

public class BluetoothReciever extends BroadcastReceiver
{

    private static final String TAG = com/ximalaya/ting/android/broadcast/BluetoothReciever.getSimpleName();

    public BluetoothReciever()
    {
    }

    private boolean judgeA2dpConn(BluetoothDevice bluetoothdevice)
    {
        if (BluetoothAdapter.getDefaultAdapter() == null)
        {
            return false;
        }
        if (android.os.Build.VERSION.SDK_INT >= 11) goto _L2; else goto _L1
_L1:
        a a1;
        int i;
        IBinder ibinder = (IBinder)Class.forName("android.os.ServiceManager").getDeclaredMethod("getService", new Class[] {
            java/lang/String
        }).invoke(null, new Object[] {
            "bluetooth_a2dp"
        });
        Method method = Class.forName("android.a.a").getDeclaredClasses()[0].getDeclaredMethod("asInterface", new Class[] {
            android/os/IBinder
        });
        method.setAccessible(true);
        a1 = (a)method.invoke(null, new Object[] {
            ibinder
        });
        i = ((Integer)a1.getClass().getDeclaredMethod("getSinkState", new Class[] {
            android/bluetooth/BluetoothDevice
        }).invoke((a)method.invoke(null, new Object[] {
            ibinder
        }), new Object[] {
            bluetoothdevice
        })).intValue();
        if (i != 2 && i != 1)
        {
            break MISSING_BLOCK_LABEL_214;
        }
        Logger.d("ximao", (new StringBuilder()).append("a2dp conn success:").append(a1).toString());
        return true;
        bluetoothdevice;
        MyDeviceManager.showVersionWarning(MyApplication.a());
        bluetoothdevice.printStackTrace();
        Logger.e(TAG, bluetoothdevice.toString());
_L4:
        return false;
_L2:
        int j = BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(2);
        if (j == 2)
        {
            Logger.d("ximao", (new StringBuilder()).append("a2dp conn success:").append(j).toString());
            return true;
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private void scanConnDeviceSync(Context context, BluetoothDevice bluetoothdevice)
    {
        Logger.d(TAG, "scanConnDeviceSync IN");
        (new Thread(new c(this, bluetoothdevice, context))).start();
    }

    private void scanConnDeviceSyncXimaConn(BluetoothAdapter bluetoothadapter, BluetoothDevice bluetoothdevice, Context context)
    {
        (new Thread(new b(this, bluetoothadapter, bluetoothdevice, context))).start();
    }

    private void toMainAppPlay(Context context, boolean flag)
    {
        Logger.d(TAG, "toMainAppPlay IN");
        Intent intent = new Intent();
        Context context1 = context;
        if (context == null)
        {
            context1 = MyApplication.b();
        }
        intent.setClass(context1, com/ximalaya/ting/android/activity/MainTabActivity2);
        if (flag)
        {
            intent.putExtra("PLAYINGNOW", "PLAYINGNOW");
        }
        intent.addCategory("android.intent.category.LAUNCHER");
        intent.addFlags(0x14000000);
        if (MainTabActivity2.isMainTabActivityAvaliable())
        {
            Logger.d(TAG, "MainTabActivity2.isMainTabActivityAvaliable() True");
            context1.startActivity(intent);
            return;
        } else
        {
            Logger.d(TAG, "MainTabActivity2.isMainTabActivityAvaliable() False");
            context1.startActivity(intent);
            return;
        }
    }

    public void freshDeviceFragment()
    {
        Object obj = MyApplication.a();
        if (obj != null && (obj instanceof MainTabActivity2))
        {
            int i;
            if ((MainTabActivity2)obj != null)
            {
                i = ((MainTabActivity2)obj).getManageFragment().mStacks.size();
            } else
            {
                i = 0;
            }
            if (i >= 1)
            {
                obj = (Fragment)((SoftReference)((MainTabActivity2)obj).getManageFragment().mStacks.get(i - 1)).get();
                if ((obj instanceof XiMaoIntroFragment) || (obj instanceof CheTingIntroFragment))
                {
                    ((Fragment) (obj)).onResume();
                }
            }
        }
    }

    public void onReceive(Context context, Intent intent)
    {
        String s;
        freshDeviceFragment();
        s = intent.getAction();
        Log.e(TAG, (new StringBuilder()).append("action:").append(s).toString());
        intent = (BluetoothDevice)intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
        if (!s.equals("android.bluetooth.adapter.action.DISCOVERY_STARTED"));
        if (intent != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType devicetype;
        Log.e(TAG, (new StringBuilder()).append("BluetoothReciever onReceive:btd:").append(intent.getName()).append(",").append(intent.getAddress()).toString());
        devicetype = MyDeviceManager.getInstance(context).getBluetoothDeviceType(intent);
        if ("android.bluetooth.device.action.PAIRING_REQUEST".equals(s) || "android.bluetooth.device.action.ACL_DISCONNECTED".equals(s) || !"android.bluetooth.device.action.ACL_CONNECTED".equals(s)) goto _L1; else goto _L3
_L3:
        if (DexManager.getInstance(context).isSuichetingOpen())
        {
            MyBluetoothManager2.getInstance(context).setSuichetingPlaying(false);
        }
        static class _cls1
        {

            static final int a[];

            static 
            {
                a = new int[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.values().length];
                try
                {
                    a[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.suicheting.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    a[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.ximao.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    a[com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.Qsuicheting.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        switch (com.ximalaya.ting.android.broadcast._cls1.a[devicetype.ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            BluetoothManager.getInstance(context).cleanup();
            BluetoothManager.getInstance(context).init(com.ximalaya.ting.android.fragment.device.MyDeviceManager.DeviceType.suicheting, intent, false, true);
            Logger.e(TAG, "BluetoothReciever to suicheting success");
            DexManager.getInstance(context).loadSuicheting(new com.ximalaya.ting.android.broadcast.a(this, context, intent));
            return;

        case 2: // '\002'
            if (BluetoothAdapter.getDefaultAdapter() != null)
            {
                scanConnDeviceSyncXimaConn(BluetoothAdapter.getDefaultAdapter(), intent, context);
                return;
            }
            break;

        case 3: // '\003'
            scanConnDeviceSync(context, intent);
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }





}
