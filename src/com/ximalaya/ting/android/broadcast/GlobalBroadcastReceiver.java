// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.broadcast;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;

// Referenced classes of package com.ximalaya.ting.android.broadcast:
//            n, l, m, f, 
//            h, d, e

public class GlobalBroadcastReceiver extends BroadcastReceiver
{

    public static boolean isVerifyForContinueDialogShowing;

    public GlobalBroadcastReceiver()
    {
    }

    private void informMobileNetworkStart()
    {
        DownloadHandler downloadhandler = DownloadHandler.getCurrentInstance();
        if (downloadhandler != null && downloadhandler.isDownloading() && MyApplication.b() != null)
        {
            verifyForContinueDownload(MyApplication.b());
        }
    }

    private void informNetworkShutdown()
    {
        if (MyApplication.c())
        {
            MyApplication.f.runOnUiThread(new n(this));
        }
    }

    private void informWhileUsingNetWork(Context context)
    {
        boolean flag = SharedPreferencesUtil.getInstance(context).getBoolean("is_download_enabled_in_3g", false);
        DownloadHandler downloadhandler = DownloadHandler.getCurrentInstance();
        if (flag)
        {
            informMobileNetworkStart();
        } else
        {
            if (downloadhandler != null && downloadhandler.isDownloading() && MyApplication.b() != null)
            {
                downloadhandler.pauseAllDownload();
                showConfirmDialog(context, new l(this, context));
                return;
            }
            if (LocalMediaService.getInstance() != null && LocalMediaService.getInstance().isPlaying() && LocalMediaService.getInstance().isPlayFromNetwork())
            {
                showConfirmDialog(context, new m(this));
                return;
            }
        }
    }

    private void informWifiNetworkStart()
    {
    }

    public static void onAppExit()
    {
        isVerifyForContinueDialogShowing = true;
    }

    public static void onAppStart()
    {
        isVerifyForContinueDialogShowing = false;
    }

    private void showConfirmDialog(Context context, com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback)
    {
        if (!isVerifyForContinueDialogShowing && MyApplication.c())
        {
            isVerifyForContinueDialogShowing = true;
            MyApplication.f.runOnUiThread(new f(this, dialogcallback));
        }
    }

    private void verifyForContinueDownload(Context context)
    {
        if (context != null && !isVerifyForContinueDialogShowing)
        {
            DownloadHandler downloadhandler = DownloadHandler.getCurrentInstance();
            if (downloadhandler != null)
            {
                downloadhandler.pauseAllDownload();
            }
            if (MyApplication.f != null && !MyApplication.f.isFinishing() && downloadhandler != null && downloadhandler.getIncompleteTaskCount() > 0)
            {
                MyApplication.f.runOnUiThread(new h(this, context));
                return;
            }
        }
    }

    public void onReceive(Context context, Intent intent)
    {
        intent = intent.getAction();
        if (!intent.equals("android.net.conn.CONNECTIVITY_CHANGE")) goto _L2; else goto _L1
_L1:
        intent = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (intent == null || !intent.isAvailable()) goto _L4; else goto _L3
_L3:
        if (NetworkUtils.getNetType(context) != 1) goto _L6; else goto _L5
_L5:
        Logger.log("NetWorkTest", "====NETWORK_TYPE_WIFI");
        (new d(this, context)).start();
        FreeFlowUtil.getInstance().removeProxy();
        informWifiNetworkStart();
_L8:
        return;
_L6:
        if (NetworkUtils.getNetType(context) == 0)
        {
            Logger.log("NetWorkTest", "====NETWORK_TYPE_MOBILE");
            FreeFlowUtil.getInstance().useFreeFlow(true, 1);
            informWhileUsingNetWork(context);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L4:
        Logger.log("NetWorkTest", "====NETWORK_TYPE_NONE");
        (new e(this)).start();
        informNetworkShutdown();
        return;
_L2:
        if (intent.equals("com.ximalaya.ting.android.action.ACTION_NOTIFICATION_EXIT"))
        {
            intent = MainTabActivity2.mainTabActivity;
            if (intent != null)
            {
                intent.finish();
                return;
            } else
            {
                ((MyApplication)context.getApplicationContext()).g();
                return;
            }
        }
        if (true) goto _L8; else goto _L7
_L7:
    }
}
