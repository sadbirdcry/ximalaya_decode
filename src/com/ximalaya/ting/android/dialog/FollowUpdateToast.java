// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.dialog:
//            d

public class FollowUpdateToast extends Toast
{

    private static boolean isSound = true;
    private MediaPlayer mPlayer;

    public FollowUpdateToast(Context context)
    {
        super(context);
    }

    public FollowUpdateToast(Context context, boolean flag)
    {
        super(context);
        isSound = flag;
    }

    public static FollowUpdateToast makeText(Context context, CharSequence charsequence, int i)
    {
        FollowUpdateToast followupdatetoast = new FollowUpdateToast(context);
        TextView textview = new TextView(context);
        textview.setMinHeight((int)context.getResources().getDimension(0x7f080025));
        textview.setText(charsequence);
        textview.setTextSize(2, 17F);
        textview.setTextColor(Color.parseColor("#1087a3"));
        textview.setBackgroundColor(Color.parseColor("#f6e4a3"));
        textview.getBackground().setAlpha(216);
        textview.setGravity(17);
        followupdatetoast.setView(textview);
        followupdatetoast.setDuration(i);
        followupdatetoast.setGravity(55, 0, (int)context.getResources().getDimension(0x7f080025) - ToolUtil.dp2px(context, 3F));
        return followupdatetoast;
    }

    public static void setIsSound(boolean flag)
    {
        isSound = flag;
    }

    public void show(Context context)
    {
        super.show();
        if (isSound)
        {
            if (mPlayer == null)
            {
                mPlayer = MediaPlayer.create(context, 0x7f060000);
                if (mPlayer != null)
                {
                    mPlayer.setOnCompletionListener(new d(this));
                }
            }
            if (mPlayer != null)
            {
                mPlayer.start();
            }
        }
    }



/*
    static MediaPlayer access$002(FollowUpdateToast followupdatetoast, MediaPlayer mediaplayer)
    {
        followupdatetoast.mPlayer = mediaplayer;
        return mediaplayer;
    }

*/
}
