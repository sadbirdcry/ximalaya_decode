// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.dialog;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.library.service.DownloadLiteManager;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URI;

// Referenced classes of package com.ximalaya.ting.android.dialog:
//            a, b, c

public class CallingRingtoneDownloadDialog extends Dialog
{
    static class a extends Handler
    {

        private WeakReference a;

        public void handleMessage(Message message)
        {
            CallingRingtoneDownloadDialog callingringtonedownloaddialog = (CallingRingtoneDownloadDialog)a.get();
            if (callingringtonedownloaddialog == null)
            {
                return;
            }
            switch (message.what)
            {
            default:
                return;

            case 0: // '\0'
                callingringtonedownloaddialog.updateProgress(message.arg1, message.arg2);
                return;

            case 1: // '\001'
                callingringtonedownloaddialog.dismiss();
                if (message.getData() != null)
                {
                    callingringtonedownloaddialog.doSetRingtone(message.getData().getString("msg_data_filepath"));
                    return;
                } else
                {
                    callingringtonedownloaddialog.showErrorMsg();
                    return;
                }

            case 2: // '\002'
                callingringtonedownloaddialog.dismiss();
                callingringtonedownloaddialog.showErrorMsg();
                return;

            case 3: // '\003'
                ToolUtil.onEvent(callingringtonedownloaddialog.getContext(), "Set_Ringtone_Success");
                Toast.makeText(callingringtonedownloaddialog.getContext(), (new StringBuilder()).append("\u5DF2\u5C07\"").append(callingringtonedownloaddialog.mSoundInfo.title).append("\"\u8BBE\u4E3A\u624B\u673A\u94C3\u58F0").toString(), 0).show();
                return;
            }
        }

        public a(CallingRingtoneDownloadDialog callingringtonedownloaddialog)
        {
            a = new WeakReference(callingringtonedownloaddialog);
        }
    }


    public static final String CONSTANTS_RINGTONE_ALBUM = "\u559C\u9A6C\u62C9\u96C5\u94C3\u58F0";
    public static final int MESSAGE_COMPLETE_SETTING = 3;
    private SoundInfo mSoundInfo;
    private Handler mUiHandler;

    public CallingRingtoneDownloadDialog(Context context)
    {
        super(context);
    }

    private void deleteInfoFromMediaLibrary(String s)
    {
        Object obj = new File(URI.create(s));
        s = android.provider.MediaStore.Audio.Media.getContentUriForPath(((File) (obj)).getAbsolutePath());
        obj = ((File) (obj)).getAbsolutePath().substring(0, ((File) (obj)).getAbsolutePath().lastIndexOf("ting/ringtones/") + 14);
        Logger.v("deleteFromMediaLibrary:", String.valueOf(getContext().getContentResolver().delete(s, (new StringBuilder()).append("_data LIKE '").append(((String) (obj))).append("%'").toString(), null)));
    }

    private void deleteOldRingtones(String s)
    {
        File afile[] = (new File(URI.create(s))).getParentFile().listFiles(new com.ximalaya.ting.android.dialog.a(this));
        if (afile != null)
        {
            int j = afile.length;
            for (int i = 0; i < j; i++)
            {
                afile[i].delete();
            }

        }
        deleteInfoFromMediaLibrary(s);
    }

    private void doSetRingtone(String s)
    {
        (new b(this, s)).start();
    }

    private static String getDownloadedText(int i, int j)
    {
        StringBuilder stringbuilder = new StringBuilder();
        if (i == 100)
        {
            i = j;
        } else
        {
            i = (j * i) / 100;
        }
        stringbuilder.append(i);
        stringbuilder.append("K/");
        stringbuilder.append(j);
        stringbuilder.append("K");
        return stringbuilder.toString();
    }

    private void showErrorMsg()
    {
        Toast.makeText(getContext(), "\u8BBE\u7F6E\u624B\u673A\u94C3\u58F0\u5931\u8D25\uFF0C\u8BF7\u91CD\u65B0\u5C1D\u8BD5", 0).show();
    }

    private void updateProgress(int i, int j)
    {
        Object obj = (ProgressBar)findViewById(0x7f0a0091);
        if (obj != null)
        {
            ((ProgressBar) (obj)).setProgress(i);
        }
        obj = (TextView)findViewById(0x7f0a0201);
        if (obj != null)
        {
            ((TextView) (obj)).setText(getDownloadedText(i, j));
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        requestWindowFeature(1);
        setContentView(0x7f030069);
        bundle = (Button)findViewById(0x7f0a015b);
        bundle.setText("\u53D6\u6D88");
        bundle.setOnClickListener(new c(this));
        mUiHandler = new a(this);
    }

    protected void onStart()
    {
        super.onStart();
        DownloadLiteManager.getInstance().download(mSoundInfo.playUrl64, StorageUtils.getRingtoneDir(), (new StringBuilder()).append(String.valueOf(mSoundInfo.trackId)).append(".mp3").toString(), 30, mUiHandler);
    }

    public void setDownloadInfo(SoundInfo soundinfo)
    {
        mSoundInfo = soundinfo;
    }






}
