// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.dialog;

import android.view.View;
import android.widget.CheckBox;
import com.umeng.analytics.MobclickAgent;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;

// Referenced classes of package com.ximalaya.ting.android.dialog:
//            PreinstallDialog

class e
    implements android.view.View.OnClickListener
{

    final PreinstallDialog a;

    e(PreinstallDialog preinstalldialog)
    {
        a = preinstalldialog;
        super();
    }

    public void onClick(View view)
    {
        boolean flag = ((CheckBox)a.findViewById(0x7f0a0209)).isChecked();
        a.dismiss();
        if (flag)
        {
            PreinstallDialog.access$000(a);
            MobclickAgent.onEvent(PreinstallDialog.access$100(a), "Tied_app", "true");
        } else
        {
            MobclickAgent.onEvent(PreinstallDialog.access$100(a), "Tied_app", "false");
            SharedPreferencesUtil.getInstance(PreinstallDialog.access$100(a)).saveBoolean(PreinstallDialog.ISPREINSTALLED, true);
        }
        PreinstallDialog.access$102(a, null);
    }
}
