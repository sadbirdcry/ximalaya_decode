// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.library.model.AppAd;
import com.ximalaya.ting.android.library.service.DownloadService;
import com.ximalaya.ting.android.util.ImageManager2;

// Referenced classes of package com.ximalaya.ting.android.dialog:
//            e

public class PreinstallDialog extends Dialog
{

    public static String ISPREINSTALLAPPAD = "isPreinstallAppAd";
    public static String ISPREINSTALLDOWNLOAD = "isPreinstallDownload";
    public static String ISPREINSTALLED = "isPreinstalled";
    public static String ISPREINSTALLSHOW = "isPreinstallShow";
    private AppAd mAppAd;
    private Context mContext;

    public PreinstallDialog(Context context, int i)
    {
        super(context, i);
    }

    public PreinstallDialog(Context context, AppAd appad)
    {
        super(context, 0x7f0b000d);
        mContext = context;
        setCanceledOnTouchOutside(true);
        mAppAd = appad;
    }

    private void startDownloadAdApk()
    {
        if (mAppAd == null || mContext == null)
        {
            return;
        } else
        {
            mAppAd.setIsAutoNotifyInstall(false);
            Intent intent = new Intent(mContext, com/ximalaya/ting/android/library/service/DownloadService);
            intent.putExtra(DownloadService.SHOWINNOTIFICATION, true);
            intent.putExtra(DownloadService.SHALLKEEPKEY, ISPREINSTALLDOWNLOAD);
            intent.putExtra(DownloadService.APPAD, JSON.toJSONString(mAppAd));
            mContext.startService(intent);
            return;
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03006d);
        getWindow().setLayout(-1, -2);
        findViewById(0x7f0a015f).setOnClickListener(new e(this));
        if (mAppAd != null)
        {
            setAdImg(mAppAd.getCover());
            setAppName(mAppAd.getName());
        }
    }

    public void setAdImg(String s)
    {
        ImageView imageview = (ImageView)findViewById(0x7f0a0120);
        ImageManager2.from(getOwnerActivity()).displayImage(imageview, s, -1);
    }

    public void setAppName(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            return;
        } else
        {
            ((TextView)findViewById(0x7f0a020a)).setText(s);
            return;
        }
    }

    public void show()
    {
        super.show();
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }





/*
    static Context access$102(PreinstallDialog preinstalldialog, Context context)
    {
        preinstalldialog.mContext = context;
        return context;
    }

*/
}
