// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.dialog;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.ToolUtil;
import java.lang.ref.WeakReference;

// Referenced classes of package com.ximalaya.ting.android.dialog:
//            CallingRingtoneDownloadDialog

static class a extends Handler
{

    private WeakReference a;

    public void handleMessage(Message message)
    {
        CallingRingtoneDownloadDialog callingringtonedownloaddialog = (CallingRingtoneDownloadDialog)a.get();
        if (callingringtonedownloaddialog == null)
        {
            return;
        }
        switch (message.what)
        {
        default:
            return;

        case 0: // '\0'
            CallingRingtoneDownloadDialog.access$000(callingringtonedownloaddialog, message.arg1, message.arg2);
            return;

        case 1: // '\001'
            callingringtonedownloaddialog.dismiss();
            if (message.getData() != null)
            {
                CallingRingtoneDownloadDialog.access$100(callingringtonedownloaddialog, message.getData().getString("msg_data_filepath"));
                return;
            } else
            {
                CallingRingtoneDownloadDialog.access$200(callingringtonedownloaddialog);
                return;
            }

        case 2: // '\002'
            callingringtonedownloaddialog.dismiss();
            CallingRingtoneDownloadDialog.access$200(callingringtonedownloaddialog);
            return;

        case 3: // '\003'
            ToolUtil.onEvent(callingringtonedownloaddialog.getContext(), "Set_Ringtone_Success");
            Toast.makeText(callingringtonedownloaddialog.getContext(), (new StringBuilder()).append("\u5DF2\u5C07\"").append(CallingRingtoneDownloadDialog.access$300(callingringtonedownloaddialog).title).append("\"\u8BBE\u4E3A\u624B\u673A\u94C3\u58F0").toString(), 0).show();
            return;
        }
    }

    public (CallingRingtoneDownloadDialog callingringtonedownloaddialog)
    {
        a = new WeakReference(callingringtonedownloaddialog);
    }
}
