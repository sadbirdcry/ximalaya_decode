// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.d;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.webkit.WebView;
import com.alipay.sdk.app.PayTask;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.d:
//            e, a, c

class b extends MyAsyncTask
{

    final String a;
    final a b;

    b(a a1, String s)
    {
        b = a1;
        a = s;
        super();
    }

    public transient e a(Void avoid[])
    {
        Logger.log((new StringBuilder()).append("payInfo=").append(a).toString());
        avoid = new e((new PayTask(com.ximalaya.ting.android.d.a.a(b))).pay(a));
        Logger.log((new StringBuilder()).append("status=").append(avoid.a()).append(", result=").append(avoid.b()).append(", returnUrl=").append(avoid.c()).toString());
        return avoid;
    }

    public void a(e e1)
    {
        String s = e1.b();
        if (!TextUtils.isEmpty(s) && s.contains("&success=\"true\"&") && TextUtils.equals(e1.a(), "9000"))
        {
            e1 = new Intent();
            e1.setAction("com.ximalaya.ting.android.pay.ACTION_PAY_FINISH");
            e1.putExtra("amount", com.ximalaya.ting.android.d.a.b(b));
            e1.putExtra("mark", com.ximalaya.ting.android.d.a.c(b));
            e1.putExtra("track_id", com.ximalaya.ting.android.d.a.d(b));
            com.ximalaya.ting.android.d.a.a(b).sendBroadcast(e1);
            com.ximalaya.ting.android.d.a.f(b).post(new c(this));
            return;
        }
        if (TextUtils.equals(e1.a(), "8000"))
        {
            Logger.log("\u652F\u4ED8\u786E\u8BA4\u4E2D");
            return;
        } else
        {
            Logger.log((new StringBuilder()).append("\u652F\u4ED8\u5931\u8D25, resultStatus=").append(e1.a()).toString());
            return;
        }
    }

    public Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    public void onPostExecute(Object obj)
    {
        a((e)obj);
    }
}
