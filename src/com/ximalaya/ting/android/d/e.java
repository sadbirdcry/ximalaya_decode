// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.d;

import android.text.TextUtils;

public class e
{

    private String a;
    private String b;
    private String c;
    private String d;

    public e(String s)
    {
        boolean flag;
        flag = false;
        super();
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        s = s.split(";");
        int k = s.length;
        for (int i = 0; i < k; i++)
        {
            String s1 = s[i];
            if (s1.startsWith("resultStatus"))
            {
                a = a(s1, "resultStatus");
            }
            if (s1.startsWith("result"))
            {
                b = a(s1, "result");
            }
            if (s1.startsWith("memo"))
            {
                c = a(s1, "memo");
            }
        }

        if (!TextUtils.isEmpty(b))
        {
            s = b.split("&");
            int l = s.length;
            int j = ((flag) ? 1 : 0);
            while (j < l) 
            {
                String s2 = s[j];
                if (s2.startsWith("return_url"))
                {
                    j = s2.indexOf("return_url=\"");
                    d = s2.substring("return_url=\"".length() + j, s2.lastIndexOf("\""));
                    return;
                }
                j++;
            }
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    private String a(String s, String s1)
    {
        s1 = (new StringBuilder()).append(s1).append("={").toString();
        int i = s.indexOf(s1);
        return s.substring(s1.length() + i, s.lastIndexOf("}"));
    }

    public String a()
    {
        return a;
    }

    public String b()
    {
        return b;
    }

    public String c()
    {
        return d;
    }

    public String toString()
    {
        return (new StringBuilder()).append("resultStatus={").append(a).append("};memo={").append(c).append("};result={").append(b).append("}").toString();
    }
}
