// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.d;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.webkit.WebView;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a.b;
import com.ximalaya.ting.android.dl.PluginConstants;
import com.ximalaya.ting.android.dl.PluginManager;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.dashang.WxpayModel;
import java.net.URLDecoder;

// Referenced classes of package com.ximalaya.ting.android.d:
//            b, d

public class com.ximalaya.ting.android.d.a
{
    public static interface a
    {

        public abstract void a();
    }


    private Activity a;
    private WebView b;
    private IWXAPI c;
    private a d;
    private String e;
    private String f;
    private String g;
    private String h;

    public com.ximalaya.ting.android.d.a(Activity activity, WebView webview)
    {
        a = activity;
        b = webview;
        c = WXAPIFactory.createWXAPI(a.getApplicationContext(), b.b, false);
    }

    static Activity a(com.ximalaya.ting.android.d.a a1)
    {
        return a1.a;
    }

    static String b(com.ximalaya.ting.android.d.a a1)
    {
        return a1.f;
    }

    static String c(com.ximalaya.ting.android.d.a a1)
    {
        return a1.g;
    }

    private void c(String s)
    {
        (new com.ximalaya.ting.android.d.b(this, s)).myexec(new Void[0]);
    }

    static String d(com.ximalaya.ting.android.d.a a1)
    {
        return a1.h;
    }

    static String e(com.ximalaya.ting.android.d.a a1)
    {
        return a1.e;
    }

    static WebView f(com.ximalaya.ting.android.d.a a1)
    {
        return a1.b;
    }

    public String a()
    {
        boolean flag1 = false;
        boolean flag;
        if (!c.isWXAppInstalled() || c.getWXAppSupportAPI() < 0x22000001)
        {
            flag = false;
        } else
        {
            flag = true;
        }
        if (PluginManager.getInstance(a.getApplicationContext()).isPluginLoaded(PluginConstants.PLUGIN_ALIPAY))
        {
            flag1 = true;
        }
        return (new StringBuilder()).append("{\"wxpay\":").append(flag).append(", \"wxid\": \"").append(b.b).append("\", \"alipay\": ").append(flag1).append("}").toString();
    }

    public void a(WxpayModel wxpaymodel)
    {
        ((MyApplication)a.getApplication()).c = 1;
        PayReq payreq = new PayReq();
        payreq.appId = wxpaymodel.getAppid();
        payreq.partnerId = wxpaymodel.getPartnerid();
        payreq.prepayId = wxpaymodel.getPrepayid();
        payreq.nonceStr = wxpaymodel.getNoncestr();
        payreq.timeStamp = wxpaymodel.getTimestamp();
        payreq.packageValue = wxpaymodel.getPackageValue();
        payreq.sign = wxpaymodel.getSign();
        wxpaymodel = new StringBuffer();
        wxpaymodel.append("{").append("\"amount\":").append(f).append(",").append("\"mark\":").append(g).append(",").append("\"order_num\":").append(e).append(",").append("\"track_id\":").append(h).append("}");
        payreq.extData = wxpaymodel.toString();
        c.sendReq(payreq);
    }

    public void a(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        s = URLDecoder.decode(s);
        Logger.log((new StringBuilder()).append("appPay params=").append(s).toString());
        JSONObject jsonobject = JSON.parseObject(s);
        if (jsonobject == null) goto _L1; else goto _L3
_L3:
        JSONObject jsonobject1;
        int i;
        i = jsonobject.getIntValue("payType");
        jsonobject1 = jsonobject.getJSONObject("params");
        s = null;
        if (jsonobject1 == null)
        {
            break MISSING_BLOCK_LABEL_113;
        }
        s = jsonobject1.getString("payInfo");
        e = jsonobject1.getString("merchantOrderNo");
        f = jsonobject1.getString("amount");
        g = jsonobject1.getString("mark");
        h = jsonobject1.getString("trackId");
        if (i != 2)
        {
            continue; /* Loop/switch isn't completed */
        }
        s = (WxpayModel)JSON.parseObject(jsonobject.getString("params"), com/ximalaya/ting/android/model/dashang/WxpayModel);
        if (s == null) goto _L1; else goto _L4
_L4:
        try
        {
            a(((WxpayModel) (s)));
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            Logger.log((new StringBuilder()).append("appPay exception=").append(s.getMessage()).toString());
        }
        return;
        if (i != 1) goto _L1; else goto _L5
_L5:
        if (TextUtils.isEmpty(s)) goto _L1; else goto _L6
_L6:
        c(s);
        return;
    }

    public void b(String s)
    {
        if (a != null)
        {
            if (!TextUtils.isEmpty(s))
            {
                try
                {
                    if (JSON.parseObject(s).getBooleanValue("share"))
                    {
                        s = new Intent();
                        s.setAction("com.ximalaya.ting.android.pay.ACTION_PAY_SHARE");
                        a.sendBroadcast(s);
                    }
                }
                // Misplaced declaration of an exception variable
                catch (String s) { }
            }
            a.runOnUiThread(new d(this));
        }
        if (d != null)
        {
            d.a();
        }
    }
}
