// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.wheel;

import android.os.Handler;
import android.os.Message;
import android.widget.Scroller;

// Referenced classes of package com.ximalaya.ting.android.view.wheel:
//            WheelView

class b extends Handler
{

    final WheelView a;

    b(WheelView wheelview)
    {
        a = wheelview;
        super();
    }

    public void handleMessage(Message message)
    {
        WheelView.access$100(a).computeScrollOffset();
        int i = WheelView.access$100(a).getCurrY();
        int j = WheelView.access$300(a) - i;
        WheelView.access$302(a, i);
        if (j != 0)
        {
            WheelView.access$1000(a, j);
        }
        if (Math.abs(i - WheelView.access$100(a).getFinalY()) < 1)
        {
            WheelView.access$100(a).getFinalY();
            WheelView.access$100(a).forceFinished(true);
        }
        if (!WheelView.access$100(a).isFinished())
        {
            WheelView.access$1100(a).sendEmptyMessage(message.what);
            return;
        }
        if (message.what == 0)
        {
            WheelView.access$1200(a);
            return;
        } else
        {
            a.finishScrolling();
            return;
        }
    }
}
