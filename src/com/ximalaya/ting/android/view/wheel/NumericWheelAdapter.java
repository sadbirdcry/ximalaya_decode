// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.wheel;


// Referenced classes of package com.ximalaya.ting.android.view.wheel:
//            WheelAdapter

public class NumericWheelAdapter
    implements WheelAdapter
{

    public static final int DEFAULT_MAX_VALUE = 9;
    private static final int DEFAULT_MIN_VALUE = 0;
    private String format;
    private int maxValue;
    private int minValue;

    public NumericWheelAdapter()
    {
        this(0, 9);
    }

    public NumericWheelAdapter(int i, int j)
    {
        this(i, j, null);
    }

    public NumericWheelAdapter(int i, int j, String s)
    {
        minValue = i;
        maxValue = j;
        format = s;
    }

    public String getItem(int i)
    {
        if (i >= 0 && i < getItemsCount())
        {
            i = minValue + i;
            if ("00:00".equals(format))
            {
                return (new StringBuilder()).append(String.format("%02d", new Object[] {
                    Integer.valueOf(i)
                })).append(":00").toString();
            }
            if ("hour".equals(format))
            {
                return (new StringBuilder()).append(Integer.toString(i)).append("\u5C0F\u65F6").toString();
            }
            if (format != null)
            {
                return String.format(format, new Object[] {
                    Integer.valueOf(i)
                });
            } else
            {
                return Integer.toString(i);
            }
        } else
        {
            return null;
        }
    }

    public int getItemsCount()
    {
        return (maxValue - minValue) + 1;
    }

    public int getMaximumLength()
    {
        int j = Integer.toString(Math.max(Math.abs(maxValue), Math.abs(minValue))).length();
        int i = j;
        if (minValue < 0)
        {
            i = j + 1;
        }
        return i;
    }
}
