// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.wheel;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Interpolator;
import android.widget.Scroller;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.wheel:
//            a, b, WheelAdapter, OnWheelChangedListener, 
//            OnWheelScrollListener

public class WheelView extends View
{

    private static final int ADDITIONAL_ITEMS_SPACE = 10;
    private static final int ADDITIONAL_ITEM_HEIGHT = 15;
    private static final int DEF_VISIBLE_ITEMS = 5;
    private static final int ITEMS_TEXT_COLOR = 0xff000000;
    private static final int ITEM_OFFSET = 3;
    private static final int LABEL_OFFSET = 8;
    private static final int MIN_DELTA_FOR_SCROLLING = 1;
    private static final int PADDING = 10;
    private static final int SCROLLING_DURATION = 400;
    private static final int SHADOWS_COLORS[] = {
        0xff111111, 0xaaaaaa, 0xaaaaaa
    };
    private static final int TEXT_SIZE = 18;
    private static final int VALUE_TEXT_COLOR = 0xf0000000;
    private final int MESSAGE_JUSTIFY;
    private final int MESSAGE_SCROLL;
    private WheelAdapter adapter;
    private Handler animationHandler;
    private GradientDrawable bottomShadow;
    private Drawable centerDrawable;
    private List changingListeners;
    private Context context;
    private int currentItem;
    private GestureDetector gestureDetector;
    private android.view.GestureDetector.SimpleOnGestureListener gestureListener;
    boolean isCyclic;
    private boolean isScrollingPerformed;
    private int itemHeight;
    private StaticLayout itemsLayout;
    private TextPaint itemsPaint;
    private int itemsWidth;
    private String label;
    private StaticLayout labelLayout;
    private int labelWidth;
    private int lastScrollY;
    private Scroller scroller;
    private List scrollingListeners;
    private int scrollingOffset;
    private GradientDrawable topShadow;
    private StaticLayout valueLayout;
    private TextPaint valuePaint;
    private int visibleItems;

    public WheelView(Context context1)
    {
        super(context1);
        adapter = null;
        currentItem = 0;
        itemsWidth = 0;
        labelWidth = 0;
        visibleItems = 5;
        itemHeight = 0;
        isCyclic = false;
        changingListeners = new LinkedList();
        scrollingListeners = new LinkedList();
        gestureListener = new a(this);
        MESSAGE_SCROLL = 0;
        MESSAGE_JUSTIFY = 1;
        animationHandler = new b(this);
        initData(context1);
    }

    public WheelView(Context context1, AttributeSet attributeset)
    {
        super(context1, attributeset);
        adapter = null;
        currentItem = 0;
        itemsWidth = 0;
        labelWidth = 0;
        visibleItems = 5;
        itemHeight = 0;
        isCyclic = false;
        changingListeners = new LinkedList();
        scrollingListeners = new LinkedList();
        gestureListener = new a(this);
        MESSAGE_SCROLL = 0;
        MESSAGE_JUSTIFY = 1;
        animationHandler = new b(this);
        initData(context1);
    }

    public WheelView(Context context1, AttributeSet attributeset, int i)
    {
        super(context1, attributeset, i);
        adapter = null;
        currentItem = 0;
        itemsWidth = 0;
        labelWidth = 0;
        visibleItems = 5;
        itemHeight = 0;
        isCyclic = false;
        changingListeners = new LinkedList();
        scrollingListeners = new LinkedList();
        gestureListener = new a(this);
        MESSAGE_SCROLL = 0;
        MESSAGE_JUSTIFY = 1;
        animationHandler = new b(this);
        initData(context1);
    }

    private String buildText(boolean flag)
    {
        StringBuilder stringbuilder = new StringBuilder();
        int j = visibleItems / 2 + 1;
        for (int i = currentItem - j; i <= currentItem + j; i++)
        {
            if (flag || i != currentItem)
            {
                String s = getTextItem(i);
                if (s != null)
                {
                    stringbuilder.append(s);
                }
            }
            if (i < currentItem + j)
            {
                stringbuilder.append("\n");
            }
        }

        return stringbuilder.toString();
    }

    private int calculateLayoutWidth(int i, int j)
    {
        initResourcesIfNecessary();
        int k = getMaxTextLength();
        if (k > 0)
        {
            float f = FloatMath.ceil(Layout.getDesiredWidth("0", itemsPaint));
            itemsWidth = (int)((float)k * f);
        } else
        {
            itemsWidth = 0;
        }
        itemsWidth = itemsWidth + 10;
        labelWidth = 0;
        if (label != null && label.length() > 0)
        {
            labelWidth = (int)FloatMath.ceil(Layout.getDesiredWidth(label, valuePaint));
        }
        if (j == 0x40000000)
        {
            j = 1;
        } else
        {
            int i1 = itemsWidth + labelWidth + 20;
            int l = i1;
            if (labelWidth > 0)
            {
                l = i1 + 8;
            }
            l = Math.max(l, getSuggestedMinimumWidth());
            if (j == 0x80000000 && i < l)
            {
                j = 1;
            } else
            {
                i = l;
                j = 0;
            }
        }
        if (j != 0)
        {
            j = i - 8 - 20;
            if (j <= 0)
            {
                labelWidth = 0;
                itemsWidth = 0;
            }
            if (labelWidth > 0)
            {
                itemsWidth = (int)(((double)itemsWidth * (double)j) / (double)(itemsWidth + labelWidth));
                labelWidth = j - itemsWidth;
            } else
            {
                itemsWidth = j + 8;
            }
        }
        if (itemsWidth > 0)
        {
            createLayouts(itemsWidth, labelWidth);
        }
        return i;
    }

    private void clearMessages()
    {
        animationHandler.removeMessages(0);
        animationHandler.removeMessages(1);
    }

    private void createLayouts(int i, int j)
    {
label0:
        {
            Object obj;
            if (itemsLayout == null || itemsLayout.getWidth() > i)
            {
                String s = buildText(isScrollingPerformed);
                TextPaint textpaint = itemsPaint;
                if (j > 0)
                {
                    obj = android.text.Layout.Alignment.ALIGN_OPPOSITE;
                } else
                {
                    obj = android.text.Layout.Alignment.ALIGN_CENTER;
                }
                itemsLayout = new StaticLayout(s, textpaint, i, ((android.text.Layout.Alignment) (obj)), 1.0F, 15F, false);
            } else
            {
                itemsLayout.increaseWidthTo(i);
            }
            if (!isScrollingPerformed && (valueLayout == null || valueLayout.getWidth() > i))
            {
                android.text.Layout.Alignment alignment;
                if (getAdapter() != null)
                {
                    obj = getAdapter().getItem(currentItem);
                } else
                {
                    obj = null;
                }
                if (obj == null)
                {
                    obj = "";
                }
                textpaint = valuePaint;
                if (j > 0)
                {
                    alignment = android.text.Layout.Alignment.ALIGN_OPPOSITE;
                } else
                {
                    alignment = android.text.Layout.Alignment.ALIGN_CENTER;
                }
                valueLayout = new StaticLayout(((CharSequence) (obj)), textpaint, i, alignment, 1.0F, 15F, false);
            } else
            if (isScrollingPerformed)
            {
                valueLayout = null;
            } else
            {
                valueLayout.increaseWidthTo(i);
            }
            if (j > 0)
            {
                if (labelLayout != null && labelLayout.getWidth() <= j)
                {
                    break label0;
                }
                labelLayout = new StaticLayout(label, valuePaint, j, android.text.Layout.Alignment.ALIGN_NORMAL, 1.0F, 15F, false);
            }
            return;
        }
        labelLayout.increaseWidthTo(j);
    }

    private void doScroll(int i)
    {
        scrollingOffset = scrollingOffset + i;
        int j = scrollingOffset / getItemHeight();
        int k = currentItem - j;
        if (isCyclic && adapter.getItemsCount() > 0)
        {
            for (; k < 0; k += adapter.getItemsCount()) { }
            i = k % adapter.getItemsCount();
        } else
        if (isScrollingPerformed)
        {
            if (k < 0)
            {
                j = currentItem;
                i = 0;
            } else
            {
                i = k;
                if (k >= adapter.getItemsCount())
                {
                    j = (currentItem - adapter.getItemsCount()) + 1;
                    i = adapter.getItemsCount() - 1;
                }
            }
        } else
        {
            i = Math.min(Math.max(k, 0), adapter.getItemsCount() - 1);
        }
        k = scrollingOffset;
        if (i != currentItem)
        {
            setCurrentItem(i, false);
        } else
        {
            invalidate();
        }
        scrollingOffset = k - getItemHeight() * j;
        if (scrollingOffset > getHeight())
        {
            scrollingOffset = scrollingOffset % getHeight() + getHeight();
        }
    }

    private void drawCenterRect(Canvas canvas)
    {
        int i = getHeight() / 2;
        int j = getItemHeight() / 2;
        centerDrawable.setBounds(0, i - j, getWidth(), i + j);
        centerDrawable.draw(canvas);
    }

    private void drawItems(Canvas canvas)
    {
        canvas.save();
        canvas.translate(0.0F, -itemsLayout.getLineTop(1) + scrollingOffset);
        itemsPaint.setColor(0xff000000);
        itemsPaint.drawableState = getDrawableState();
        itemsLayout.draw(canvas);
        canvas.restore();
    }

    private void drawShadows(Canvas canvas)
    {
        topShadow.setBounds(0, 0, getWidth(), getHeight() / visibleItems);
        topShadow.draw(canvas);
        bottomShadow.setBounds(0, getHeight() - getHeight() / visibleItems, getWidth(), getHeight());
        bottomShadow.draw(canvas);
    }

    private void drawValue(Canvas canvas)
    {
        valuePaint.setColor(0xf0000000);
        valuePaint.drawableState = getDrawableState();
        Rect rect = new Rect();
        itemsLayout.getLineBounds(visibleItems / 2, rect);
        if (labelLayout != null)
        {
            canvas.save();
            canvas.translate(itemsLayout.getWidth() + 8, rect.top);
            labelLayout.draw(canvas);
            canvas.restore();
        }
        if (valueLayout != null)
        {
            canvas.save();
            canvas.translate(0.0F, rect.top + scrollingOffset);
            valueLayout.draw(canvas);
            canvas.restore();
        }
    }

    private int getDesiredHeight(Layout layout)
    {
        if (layout == null)
        {
            return 0;
        } else
        {
            return Math.max(getItemHeight() * visibleItems - 6 - 15, getSuggestedMinimumHeight());
        }
    }

    private int getItemHeight()
    {
        if (itemHeight != 0)
        {
            return itemHeight;
        }
        if (itemsLayout != null && itemsLayout.getLineCount() > 2)
        {
            itemHeight = itemsLayout.getLineTop(2) - itemsLayout.getLineTop(1);
            return itemHeight;
        } else
        {
            return getHeight() / visibleItems;
        }
    }

    private int getMaxTextLength()
    {
        WheelAdapter wheeladapter = getAdapter();
        if (wheeladapter == null)
        {
            return 0;
        }
        int i = wheeladapter.getMaximumLength();
        if (i > 0)
        {
            return i;
        }
        i = visibleItems / 2;
        i = Math.max(currentItem - i, 0);
        String s;
        String s1;
        for (s = null; i < Math.min(currentItem + visibleItems, wheeladapter.getItemsCount()); s = s1)
        {
label0:
            {
                String s2 = wheeladapter.getItem(i);
                s1 = s;
                if (s2 == null)
                {
                    break label0;
                }
                if (s != null)
                {
                    s1 = s;
                    if (s.length() >= s2.length())
                    {
                        break label0;
                    }
                }
                s1 = s2;
            }
            i++;
        }

        if (s != null)
        {
            i = s.length();
        } else
        {
            i = 0;
        }
        return i;
    }

    private String getTextItem(int i)
    {
        if (adapter != null && adapter.getItemsCount() != 0) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        int j;
        int k;
        k = adapter.getItemsCount();
        if (i < 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        j = i;
        if (i < k)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!isCyclic) goto _L1; else goto _L3
_L3:
        for (j = i; j < 0; j += k) { }
        return adapter.getItem(j % k);
    }

    private void initData(Context context1)
    {
        gestureDetector = new GestureDetector(context1, gestureListener);
        gestureDetector.setIsLongpressEnabled(false);
        scroller = new Scroller(context1);
        context = context1;
    }

    private void initResourcesIfNecessary()
    {
        if (itemsPaint == null)
        {
            itemsPaint = new TextPaint(33);
            itemsPaint.setTextSize(ToolUtil.dp2px(context, 18F));
        }
        if (valuePaint == null)
        {
            valuePaint = new TextPaint(37);
            valuePaint.setTextSize(ToolUtil.dp2px(context, 18F));
            valuePaint.setShadowLayer(0.1F, 0.0F, 0.1F, 0xffc0c0c0);
        }
        if (centerDrawable == null)
        {
            centerDrawable = getContext().getResources().getDrawable(0x7f0205c5);
        }
        if (topShadow == null)
        {
            topShadow = new GradientDrawable(android.graphics.drawable.GradientDrawable.Orientation.TOP_BOTTOM, SHADOWS_COLORS);
        }
        if (bottomShadow == null)
        {
            bottomShadow = new GradientDrawable(android.graphics.drawable.GradientDrawable.Orientation.BOTTOM_TOP, SHADOWS_COLORS);
        }
        setBackgroundResource(0x7f0205c4);
    }

    private void invalidateLayouts()
    {
        itemsLayout = null;
        valueLayout = null;
        scrollingOffset = 0;
    }

    private void justify()
    {
label0:
        {
            if (adapter == null)
            {
                return;
            }
            lastScrollY = 0;
            int j = scrollingOffset;
            int k = getItemHeight();
            boolean flag;
            int i;
            if (j > 0)
            {
                if (currentItem < adapter.getItemsCount())
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
            } else
            if (currentItem > 0)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            if (!isCyclic)
            {
                i = j;
                if (!flag)
                {
                    break label0;
                }
            }
            i = j;
            if (Math.abs(j) > (float)k / 2.0F)
            {
                if (j < 0)
                {
                    i = j + (k + 1);
                } else
                {
                    i = j - (k + 1);
                }
            }
        }
        if (Math.abs(i) > 1)
        {
            scroller.startScroll(0, 0, 0, i, 400);
            setNextMessage(1);
            return;
        } else
        {
            finishScrolling();
            return;
        }
    }

    private void setNextMessage(int i)
    {
        clearMessages();
        animationHandler.sendEmptyMessage(i);
    }

    private void startScrolling()
    {
        if (!isScrollingPerformed)
        {
            isScrollingPerformed = true;
            notifyScrollingListenersAboutStart();
        }
    }

    public void addChangingListener(OnWheelChangedListener onwheelchangedlistener)
    {
        changingListeners.add(onwheelchangedlistener);
    }

    public void addScrollingListener(OnWheelScrollListener onwheelscrolllistener)
    {
        scrollingListeners.add(onwheelscrolllistener);
    }

    void finishScrolling()
    {
        if (isScrollingPerformed)
        {
            notifyScrollingListenersAboutEnd();
            isScrollingPerformed = false;
        }
        invalidateLayouts();
        invalidate();
    }

    public WheelAdapter getAdapter()
    {
        return adapter;
    }

    public int getCurrentItem()
    {
        return currentItem;
    }

    public String getLabel()
    {
        return label;
    }

    public int getVisibleItems()
    {
        return visibleItems;
    }

    public boolean isCyclic()
    {
        return isCyclic;
    }

    protected void notifyChangingListeners(int i, int j)
    {
        for (Iterator iterator = changingListeners.iterator(); iterator.hasNext(); ((OnWheelChangedListener)iterator.next()).onChanged(this, i, j)) { }
    }

    protected void notifyScrollingListenersAboutEnd()
    {
        for (Iterator iterator = scrollingListeners.iterator(); iterator.hasNext(); ((OnWheelScrollListener)iterator.next()).onScrollingFinished(this)) { }
    }

    protected void notifyScrollingListenersAboutStart()
    {
        for (Iterator iterator = scrollingListeners.iterator(); iterator.hasNext(); ((OnWheelScrollListener)iterator.next()).onScrollingStarted(this)) { }
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if (itemsLayout == null)
        {
            if (itemsWidth == 0)
            {
                calculateLayoutWidth(getWidth(), 0x40000000);
            } else
            {
                createLayouts(itemsWidth, labelWidth);
            }
        }
        if (itemsWidth > 0)
        {
            canvas.save();
            canvas.translate(10F, -3F);
            drawItems(canvas);
            drawValue(canvas);
            canvas.restore();
        }
        drawCenterRect(canvas);
        drawShadows(canvas);
    }

    protected void onMeasure(int i, int j)
    {
        int l = android.view.View.MeasureSpec.getMode(i);
        int k = android.view.View.MeasureSpec.getMode(j);
        int i1 = android.view.View.MeasureSpec.getSize(i);
        i = android.view.View.MeasureSpec.getSize(j);
        l = calculateLayoutWidth(i1, l);
        if (k != 0x40000000)
        {
            j = getDesiredHeight(itemsLayout);
            if (k == 0x80000000)
            {
                i = Math.min(j, i);
            } else
            {
                i = j;
            }
        }
        setMeasuredDimension(l, i);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        while (getAdapter() == null || gestureDetector.onTouchEvent(motionevent) || motionevent.getAction() != 1) 
        {
            return true;
        }
        justify();
        return true;
    }

    public void removeChangingListener(OnWheelChangedListener onwheelchangedlistener)
    {
        changingListeners.remove(onwheelchangedlistener);
    }

    public void removeScrollingListener(OnWheelScrollListener onwheelscrolllistener)
    {
        scrollingListeners.remove(onwheelscrolllistener);
    }

    public void scroll(int i, int j)
    {
        scroller.forceFinished(true);
        lastScrollY = scrollingOffset;
        int k = getItemHeight();
        scroller.startScroll(0, lastScrollY, 0, i * k - lastScrollY, j);
        setNextMessage(0);
        startScrolling();
    }

    public void setAdapter(WheelAdapter wheeladapter)
    {
        adapter = wheeladapter;
        invalidateLayouts();
        invalidate();
    }

    public void setCurrentItem(int i)
    {
        setCurrentItem(i, false);
    }

    public void setCurrentItem(int i, boolean flag)
    {
        if (adapter != null && adapter.getItemsCount() != 0) goto _L2; else goto _L1
_L1:
        return;
_L2:
        int j;
        if (i < 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        j = i;
        if (i < adapter.getItemsCount())
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!isCyclic) goto _L1; else goto _L3
_L3:
        for (; i < 0; i += adapter.getItemsCount()) { }
        j = i % adapter.getItemsCount();
        if (j == currentItem) goto _L1; else goto _L4
_L4:
        if (flag)
        {
            scroll(j - currentItem, 400);
            return;
        } else
        {
            invalidateLayouts();
            i = currentItem;
            currentItem = j;
            notifyChangingListeners(i, currentItem);
            invalidate();
            return;
        }
    }

    public void setCyclic(boolean flag)
    {
        isCyclic = flag;
        invalidate();
        invalidateLayouts();
    }

    public void setInterpolator(Interpolator interpolator)
    {
        scroller.forceFinished(true);
        scroller = new Scroller(getContext(), interpolator);
    }

    public void setLabel(String s)
    {
        if (label == null || !label.equals(s))
        {
            label = s;
            labelLayout = null;
            invalidate();
        }
    }

    public void setVisibleItems(int i)
    {
        visibleItems = i;
        invalidate();
    }










/*
    static int access$302(WheelView wheelview, int i)
    {
        wheelview.lastScrollY = i;
        return i;
    }

*/






}
