// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.wheel;

import android.view.MotionEvent;
import android.widget.Scroller;

// Referenced classes of package com.ximalaya.ting.android.view.wheel:
//            WheelView, WheelAdapter

class a extends android.view.GestureDetector.SimpleOnGestureListener
{

    final WheelView a;

    a(WheelView wheelview)
    {
        a = wheelview;
        super();
    }

    public boolean onDown(MotionEvent motionevent)
    {
        if (WheelView.access$000(a))
        {
            WheelView.access$100(a).forceFinished(true);
            WheelView.access$200(a);
            return true;
        } else
        {
            return false;
        }
    }

    public boolean onFling(MotionEvent motionevent, MotionEvent motionevent1, float f, float f1)
    {
        WheelView.access$302(a, WheelView.access$400(a) * WheelView.access$500(a) + WheelView.access$600(a));
        int i;
        int j;
        if (a.isCyclic)
        {
            i = 0x7fffffff;
        } else
        {
            i = WheelView.access$700(a).getItemsCount() * WheelView.access$500(a);
        }
        if (a.isCyclic)
        {
            j = -i;
        } else
        {
            j = 0;
        }
        WheelView.access$100(a).fling(0, WheelView.access$300(a), 0, (int)(-f1) / 2, 0, 0, j, i);
        WheelView.access$800(a, 0);
        return true;
    }

    public boolean onScroll(MotionEvent motionevent, MotionEvent motionevent1, float f, float f1)
    {
        WheelView.access$900(a);
        WheelView.access$1000(a, (int)(-f1));
        return true;
    }
}
