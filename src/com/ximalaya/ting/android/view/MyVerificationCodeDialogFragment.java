// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

// Referenced classes of package com.ximalaya.ting.android.view:
//            d, e, f, c

public class MyVerificationCodeDialogFragment extends DialogFragment
{
    public static interface DialogFragmenButtonClickListener
    {

        public abstract void onAffirmButtonClick(String s);

        public abstract void onChangeButtonClick();
    }


    public static final String from_register = "from_register";
    public static final String from_sound = "from_sound";
    private String checkCodeUrl;
    private DialogFragmenButtonClickListener dialogFragmenButtonClickListener;
    private EditText et_verificationCode;
    private String fromTag;
    private ImageView img_verificationCode;
    private View main_view;
    private View txt_affirm;
    private View txt_cancel;
    private View txt_changeVerificationCode;
    private TextView txt_noticeWord;

    private MyVerificationCodeDialogFragment()
    {
    }

    public MyVerificationCodeDialogFragment(String s, String s1, DialogFragmenButtonClickListener dialogfragmenbuttonclicklistener)
    {
        fromTag = s;
        checkCodeUrl = s1;
        dialogFragmenButtonClickListener = dialogfragmenbuttonclicklistener;
    }

    private void findViews()
    {
        txt_changeVerificationCode = main_view.findViewById(0x7f0a01b2);
        txt_cancel = main_view.findViewById(0x7f0a01b3);
        txt_affirm = main_view.findViewById(0x7f0a01b4);
        txt_noticeWord = (TextView)main_view.findViewById(0x7f0a01af);
        img_verificationCode = (ImageView)main_view.findViewById(0x7f0a01b1);
        et_verificationCode = (EditText)main_view.findViewById(0x7f0a01b0);
    }

    private void initViews()
    {
        if (!fromTag.equals("from_register")) goto _L2; else goto _L1
_L1:
        txt_noticeWord.setText(0x7f0901a6);
_L4:
        txt_cancel.setOnClickListener(new d(this));
        txt_affirm.setOnClickListener(new e(this));
        txt_changeVerificationCode.setOnClickListener(new f(this));
        changeVerificationCode();
        return;
_L2:
        if (fromTag.equals("from_sound"))
        {
            txt_noticeWord.setText(0x7f0901a5);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void changeVerificationCode()
    {
        (new c(this)).myexec(new Void[0]);
    }

    public void onActivityCreated(Bundle bundle)
    {
        super.onActivityCreated(bundle);
        findViews();
        initViews();
    }

    public Dialog onCreateDialog(Bundle bundle)
    {
        bundle = super.onCreateDialog(bundle);
        bundle.getWindow().requestFeature(1);
        setCancelable(false);
        return bundle;
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        super.onCreateView(layoutinflater, viewgroup, bundle);
        main_view = layoutinflater.inflate(0x7f030058, null);
        return main_view;
    }

    public void show(FragmentManager fragmentmanager, String s, String s1)
    {
        checkCodeUrl = s1;
        super.show(fragmentmanager, s);
    }




}
