// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

// Referenced classes of package com.ximalaya.ting.android.view:
//            o

public class TouchableImage extends ImageView
{

    private WeakReference mCurrDrawable;

    public TouchableImage(Context context)
    {
        super(context);
    }

    public TouchableImage(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    public TouchableImage(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
    }

    private Drawable getCurrDrawable()
    {
        Drawable drawable1 = getDrawable();
        Drawable drawable = drawable1;
        if (drawable1 == null)
        {
            drawable = getBackground();
        }
        return drawable;
    }

    private void removeShadow()
    {
        Drawable drawable = (Drawable)mCurrDrawable.get();
        if (drawable != null)
        {
            drawable.clearColorFilter();
            if (drawable == getDrawable())
            {
                setImageDrawable(drawable);
                return;
            }
        }
    }

    private void setShadow()
    {
        Drawable drawable1 = getDrawable();
        Drawable drawable = drawable1;
        if (drawable1 == null)
        {
            drawable = getBackground();
        }
        if (drawable == null)
        {
            return;
        } else
        {
            drawable.setColorFilter(0xff888888, android.graphics.PorterDuff.Mode.MULTIPLY);
            setImageDrawable(null);
            setImageDrawable(drawable);
            mCurrDrawable = new WeakReference(drawable);
            postDelayed(new o(this), 100L);
            return;
        }
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (motionevent.getAction() == 0)
        {
            setShadow();
        }
        return super.onTouchEvent(motionevent);
    }

}
