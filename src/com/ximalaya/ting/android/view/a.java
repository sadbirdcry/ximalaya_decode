// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Handler;
import android.os.Looper;
import com.ximalaya.ting.android.util.Blur;
import com.ximalaya.ting.android.util.ImageManager2;
import java.lang.ref.WeakReference;

// Referenced classes of package com.ximalaya.ting.android.view:
//            BlurableImageView, b

class a
    implements Runnable
{

    final WeakReference a;
    final boolean b;
    final String c;
    final BlurableImageView d;

    a(BlurableImageView blurableimageview, WeakReference weakreference, boolean flag, String s)
    {
        d = blurableimageview;
        a = weakreference;
        b = flag;
        c = s;
        super();
    }

    public void run()
    {
        if (a.get() != null)
        {
            Object obj;
            try
            {
                obj = Blur.fastBlur(d.getContext(), (Bitmap)a.get(), BlurableImageView.access$000());
            }
            catch (OutOfMemoryError outofmemoryerror)
            {
                outofmemoryerror.printStackTrace();
                outofmemoryerror = null;
            }
            if (obj != null)
            {
                if (b)
                {
                    ImageManager2.from(d.getContext()).put(c, ((Bitmap) (obj)));
                }
                obj = new BitmapDrawable(d.getResources(), ((Bitmap) (obj)));
                if (!Thread.currentThread().isInterrupted())
                {
                    (new Handler(Looper.getMainLooper())).post(new b(this, ((android.graphics.drawable.Drawable) (obj))));
                    return;
                }
            }
        }
    }
}
