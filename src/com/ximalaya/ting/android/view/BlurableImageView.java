// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.ImageView;
import java.lang.ref.WeakReference;

// Referenced classes of package com.ximalaya.ting.android.view:
//            a

public class BlurableImageView extends ImageView
{

    private static int radius = 40;
    private Thread blurThread;
    private Drawable mDrawable;
    private String mUrl;

    public BlurableImageView(Context context)
    {
        super(context);
    }

    public BlurableImageView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    public BlurableImageView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
    }

    public void blur()
    {
        mDrawable = getDrawable();
        blur(mDrawable, "", false);
    }

    public void blur(Drawable drawable, String s, boolean flag)
    {
        if (drawable != null && (drawable instanceof BitmapDrawable) && (TextUtils.isEmpty(s) || !s.equals(mUrl)))
        {
            mUrl = s;
            drawable = ((BitmapDrawable)drawable).getBitmap();
            WeakReference weakreference = new WeakReference(drawable);
            if (drawable != null)
            {
                if (blurThread != null)
                {
                    blurThread.interrupt();
                }
                blurThread = new Thread(new a(this, weakreference, flag, s));
                blurThread.start();
                return;
            }
        }
    }

    public Bitmap justBlur(Drawable drawable)
    {
        return null;
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
        if (blurThread != null)
        {
            blurThread.interrupt();
        }
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
    }

    public void recover()
    {
        if (blurThread != null)
        {
            blurThread.interrupt();
        }
        if (mDrawable != null)
        {
            setImageDrawable(mDrawable);
        }
    }

    public void setImageDrawable(Drawable drawable)
    {
        super.setImageDrawable(drawable);
    }

    public void setResourceUrl(String s)
    {
        mUrl = s;
    }



}
