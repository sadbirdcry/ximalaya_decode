// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.menu;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.menu:
//            a

public class TabMenu
{
    private class a extends BaseAdapter
    {

        final TabMenu a;

        public int getCount()
        {
            return a.mTitle.size();
        }

        public Object getItem(int i)
        {
            return null;
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            if (view == null)
            {
                view = View.inflate(a.mContext, 0x7f0301dd, null);
                view.setBackgroundResource(a.mItemBackground);
                viewgroup = a. new b(null);
                viewgroup.a = (ImageView)view.findViewById(0x7f0a06f5);
                viewgroup.b = (TextView)view.findViewById(0x7f0a06f6);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (b)view.getTag();
            }
            if (a.mIcon.size() > i)
            {
                ((b) (viewgroup)).a.setImageResource(((Integer)a.mIcon.get(i)).intValue());
            }
            if (a.mTitle.size() > i)
            {
                ((b) (viewgroup)).b.setText((CharSequence)a.mTitle.get(i));
            }
            return view;
        }

        public a()
        {
            a = TabMenu.this;
            super();
        }
    }

    private class b
    {

        ImageView a;
        TextView b;
        final TabMenu c;

        private b()
        {
            c = TabMenu.this;
            super();
        }

        b(com.ximalaya.ting.android.view.menu.a a1)
        {
            this();
        }
    }


    private int mColNumber;
    private Context mContext;
    private int mGridBackground;
    private GridView mGridView;
    private List mIcon;
    private int mItemBackground;
    private a mMenuAdapter;
    private int mMenuClickSelector;
    private PopupWindow mMenuWindow;
    private android.widget.AdapterView.OnItemClickListener mOnItemClickListener;
    private android.view.View.OnKeyListener mOnKeyListener;
    private List mTitle;

    public TabMenu(Context context)
    {
        mTitle = new ArrayList();
        mIcon = new ArrayList();
        mColNumber = 3;
        mContext = context;
    }

    public TabMenu(Context context, List list, List list1, android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        mTitle = new ArrayList();
        mIcon = new ArrayList();
        mColNumber = 3;
        mContext = context;
        mTitle.clear();
        mTitle.addAll(list);
        mIcon.clear();
        mIcon.addAll(list1);
        mOnItemClickListener = onitemclicklistener;
    }

    private void init()
    {
        if (mMenuWindow == null)
        {
            mMenuWindow = new PopupWindow(mContext);
            mGridView = new GridView(mContext);
            mGridView.setNumColumns(mColNumber);
            mGridView.setStretchMode(2);
            mGridView.setVerticalSpacing(ToolUtil.dp2px(mContext, 10F));
            mGridView.setHorizontalSpacing(1);
            mGridView.setGravity(17);
            if (mGridBackground > 0)
            {
                mGridView.setBackgroundResource(mGridBackground);
            }
            if (mMenuClickSelector > 0)
            {
                mGridView.setSelector(mMenuClickSelector);
            }
            int i = ToolUtil.dp2px(mContext, 10F);
            android.view.ViewGroup.LayoutParams layoutparams = new android.view.ViewGroup.LayoutParams(-1, -2);
            mGridView.setLayoutParams(layoutparams);
            mGridView.setPadding(i, i, i, i);
            mOnKeyListener = new com.ximalaya.ting.android.view.menu.a(this);
            mMenuAdapter = new a();
            mGridView.setAdapter(mMenuAdapter);
            mMenuWindow.setContentView(mGridView);
            mMenuWindow.setWidth(-1);
            mMenuWindow.setHeight(-2);
            mMenuWindow.setBackgroundDrawable(new BitmapDrawable());
            mMenuWindow.setAnimationStyle(0x7f0b0012);
            mMenuWindow.setOutsideTouchable(true);
            mMenuWindow.setFocusable(true);
        }
        mMenuAdapter.notifyDataSetChanged();
    }

    public void dismiss()
    {
        if (mMenuWindow != null)
        {
            mGridView.setOnItemClickListener(null);
            mGridView.setOnKeyListener(null);
            mMenuWindow.dismiss();
        }
    }

    public boolean isShowing()
    {
        if (mMenuWindow == null)
        {
            return false;
        } else
        {
            return mMenuWindow.isShowing();
        }
    }

    public void setBackground(int i)
    {
        mGridBackground = i;
    }

    public void setItemBackgroud(int i)
    {
        mItemBackground = i;
    }

    public void setMenuColumn(int i)
    {
        if (i > 0)
        {
            mColNumber = i;
        }
    }

    public void setMenuIcon(List list)
    {
        mIcon.clear();
        mIcon.addAll(list);
    }

    public void setMenuSelector(int i)
    {
        mMenuClickSelector = i;
    }

    public void setMenuTitle(List list)
    {
        mTitle.clear();
        mTitle.addAll(list);
    }

    public void setOnItemClickListener(android.widget.AdapterView.OnItemClickListener onitemclicklistener)
    {
        mOnItemClickListener = onitemclicklistener;
    }

    public void showAt(View view)
    {
        init();
        mGridView.setOnKeyListener(mOnKeyListener);
        mGridView.setOnItemClickListener(mOnItemClickListener);
        mMenuWindow.showAtLocation(view, 80, 0, 0);
    }





}
