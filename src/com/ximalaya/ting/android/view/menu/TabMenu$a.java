// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.menu;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.menu:
//            TabMenu

private class a extends BaseAdapter
{

    final TabMenu a;

    public int getCount()
    {
        return TabMenu.access$100(a).size();
    }

    public Object getItem(int i)
    {
        return null;
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (view == null)
        {
            view = View.inflate(TabMenu.access$200(a), 0x7f0301dd, null);
            view.setBackgroundResource(TabMenu.access$300(a));
            viewgroup = new <init>(a, null);
            viewgroup.a = (ImageView)view.findViewById(0x7f0a06f5);
            viewgroup.b = (TextView)view.findViewById(0x7f0a06f6);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (b)view.getTag();
        }
        if (TabMenu.access$500(a).size() > i)
        {
            ((a) (viewgroup)).a.setImageResource(((Integer)TabMenu.access$500(a).get(i)).intValue());
        }
        if (TabMenu.access$100(a).size() > i)
        {
            ((a) (viewgroup)).b.setText((CharSequence)TabMenu.access$100(a).get(i));
        }
        return view;
    }

    public (TabMenu tabmenu)
    {
        a = tabmenu;
        super();
    }
}
