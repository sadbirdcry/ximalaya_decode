// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;

public class RoundedDrawable extends Drawable
{

    private Paint colorPaint;
    private boolean isDown;
    private int mBitmapHeight;
    private final Paint mBitmapPaint;
    private final RectF mBitmapRect;
    private final BitmapShader mBitmapShader;
    private int mBitmapWidth;
    private int mBorderColor;
    private final Paint mBorderPaint;
    private final RectF mBorderRect;
    private int mBorderWidth;
    private final RectF mBounds;
    private float mCornerRadius;
    private final RectF mDrawableRect;
    private android.widget.ImageView.ScaleType mScaleType;
    private final Matrix mShaderMatrix;

    RoundedDrawable(Bitmap bitmap, float f, int i, int j)
    {
        this(bitmap, f, i, j, false);
    }

    RoundedDrawable(Bitmap bitmap, float f, int i, int j, boolean flag)
    {
        mBounds = new RectF();
        mDrawableRect = new RectF();
        mBitmapRect = new RectF();
        mBorderRect = new RectF();
        mScaleType = android.widget.ImageView.ScaleType.FIT_XY;
        mShaderMatrix = new Matrix();
        isDown = false;
        mBorderWidth = i;
        mBorderColor = j;
        mBitmapWidth = bitmap.getWidth();
        mBitmapHeight = bitmap.getHeight();
        if (flag)
        {
            float f1 = (float)mBitmapWidth / f;
            float f2 = (float)mBitmapHeight / f;
            if (f1 > 1.0F || f2 > 1.0F)
            {
                if (f1 > f2)
                {
                    mBitmapWidth = (int)((float)mBitmapWidth / f1);
                    mBitmapHeight = (int)((float)mBitmapHeight / f1);
                } else
                {
                    mBitmapWidth = (int)((float)mBitmapWidth / f2);
                    mBitmapHeight = (int)((float)mBitmapHeight / f2);
                }
            }
        }
        mBitmapRect.set(0.0F, 0.0F, mBitmapWidth, mBitmapHeight);
        mCornerRadius = f;
        mBitmapShader = new BitmapShader(bitmap, android.graphics.Shader.TileMode.CLAMP, android.graphics.Shader.TileMode.CLAMP);
        mBitmapShader.setLocalMatrix(mShaderMatrix);
        mBitmapPaint = new Paint();
        mBitmapPaint.setAntiAlias(true);
        mBitmapPaint.setShader(mBitmapShader);
        mBorderPaint = new Paint();
        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setColor(mBorderColor);
        mBorderPaint.setStrokeWidth(i);
        colorPaint = new Paint();
        colorPaint.setAntiAlias(true);
        colorPaint.setColor(0xff000000);
        colorPaint.setAlpha(100);
    }

    public static Bitmap drawableToBitmap(Drawable drawable)
    {
        if (drawable instanceof BitmapDrawable)
        {
            return ((BitmapDrawable)drawable).getBitmap();
        }
        int i = drawable.getIntrinsicWidth();
        int j = drawable.getIntrinsicHeight();
        if (i > 0 && j > 0)
        {
            Bitmap bitmap = Bitmap.createBitmap(i, j, android.graphics.Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } else
        {
            return null;
        }
    }

    public static Drawable fromDrawable(Drawable drawable, float f)
    {
        return fromDrawable(drawable, f, 0, 0, false);
    }

    public static Drawable fromDrawable(Drawable drawable, float f, int i, int j, boolean flag)
    {
        if (drawable != null)
        {
            if (drawable instanceof TransitionDrawable)
            {
                drawable = (TransitionDrawable)drawable;
                int l = drawable.getNumberOfLayers();
                Drawable adrawable[] = new Drawable[l];
                int k = 0;
                while (k < l) 
                {
                    Drawable drawable1 = drawable.getDrawable(k);
                    if (drawable1 instanceof ColorDrawable)
                    {
                        adrawable[k] = drawable1;
                    } else
                    {
                        adrawable[k] = new RoundedDrawable(drawableToBitmap(drawable.getDrawable(k)), f, i, j, flag);
                    }
                    k++;
                }
                return new TransitionDrawable(adrawable);
            }
            Bitmap bitmap = drawableToBitmap(drawable);
            if (bitmap != null)
            {
                return new RoundedDrawable(bitmap, f, i, j, flag);
            }
        }
        return drawable;
    }

    private void setMatrix()
    {
        float f;
        f = 0.0F;
        mBorderRect.set(mBounds);
        mDrawableRect.set(mBorderWidth + 0, mBorderWidth + 0, mBorderRect.width() - (float)mBorderWidth, mBorderRect.height() - (float)mBorderWidth);
        static class _cls1
        {

            static final int a[];

            static 
            {
                a = new int[android.widget.ImageView.ScaleType.values().length];
                try
                {
                    a[android.widget.ImageView.ScaleType.CENTER.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror6) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.CENTER_CROP.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror5) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror4) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.FIT_CENTER.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.FIT_END.ordinal()] = 5;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.FIT_START.ordinal()] = 6;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.FIT_XY.ordinal()] = 7;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls1.a[mScaleType.ordinal()];
        JVM INSTR tableswitch 1 6: default 108
    //                   1 205
    //                   2 323
    //                   3 538
    //                   4 784
    //                   5 907
    //                   6 1030;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7
_L1:
        mBorderRect.set(mBounds);
        mDrawableRect.set(mBorderWidth + 0, mBorderWidth + 0, mBorderRect.width() - (float)mBorderWidth, mBorderRect.height() - (float)mBorderWidth);
        mShaderMatrix.set(null);
        mShaderMatrix.setRectToRect(mBitmapRect, mDrawableRect, android.graphics.Matrix.ScaleToFit.FILL);
_L9:
        mBitmapShader.setLocalMatrix(mShaderMatrix);
        return;
_L2:
        mBorderRect.set(mBounds);
        mDrawableRect.set(mBorderWidth + 0, mBorderWidth + 0, mBorderRect.width() - (float)mBorderWidth, mBorderRect.height() - (float)mBorderWidth);
        mShaderMatrix.set(null);
        mShaderMatrix.setTranslate((int)((mDrawableRect.width() - (float)mBitmapWidth) * 0.5F + 0.5F), (int)((mDrawableRect.height() - (float)mBitmapHeight) * 0.5F + 0.5F));
        continue; /* Loop/switch isn't completed */
_L3:
        mBorderRect.set(mBounds);
        mDrawableRect.set(mBorderWidth + 0, mBorderWidth + 0, mBorderRect.width() - (float)mBorderWidth, mBorderRect.height() - (float)mBorderWidth);
        mShaderMatrix.set(null);
        float f2;
        float f4;
        if ((float)mBitmapWidth * mDrawableRect.height() > mDrawableRect.width() * (float)mBitmapHeight)
        {
            f4 = mDrawableRect.height() / (float)mBitmapHeight;
            f2 = (mDrawableRect.width() - (float)mBitmapWidth * f4) * 0.5F;
        } else
        {
            f4 = mDrawableRect.width() / (float)mBitmapWidth;
            f = mDrawableRect.height();
            float f6 = mBitmapHeight;
            f2 = 0.0F;
            f = (f - f6 * f4) * 0.5F;
        }
        mShaderMatrix.setScale(f4, f4);
        mShaderMatrix.postTranslate((int)(f2 + 0.5F) + mBorderWidth, (int)(f + 0.5F) + mBorderWidth);
        continue; /* Loop/switch isn't completed */
_L4:
        mShaderMatrix.set(null);
        float f1;
        float f3;
        float f5;
        if ((float)mBitmapWidth <= mBounds.width() && (float)mBitmapHeight <= mBounds.height())
        {
            f1 = 1.0F;
        } else
        {
            f1 = Math.min(mBounds.width() / (float)mBitmapWidth, mBounds.height() / (float)mBitmapHeight);
        }
        f3 = (int)((mBounds.width() - (float)mBitmapWidth * f1) * 0.5F + 0.5F);
        f5 = (int)((mBounds.height() - (float)mBitmapHeight * f1) * 0.5F + 0.5F);
        mShaderMatrix.setScale(f1, f1);
        mShaderMatrix.postTranslate(f3, f5);
        mBorderRect.set(mBitmapRect);
        mShaderMatrix.mapRect(mBorderRect);
        mDrawableRect.set(mBorderRect.left + (float)mBorderWidth, mBorderRect.top + (float)mBorderWidth, mBorderRect.right - (float)mBorderWidth, mBorderRect.bottom - (float)mBorderWidth);
        mShaderMatrix.setRectToRect(mBitmapRect, mDrawableRect, android.graphics.Matrix.ScaleToFit.FILL);
        continue; /* Loop/switch isn't completed */
_L5:
        mBorderRect.set(mBitmapRect);
        mShaderMatrix.setRectToRect(mBitmapRect, mBounds, android.graphics.Matrix.ScaleToFit.CENTER);
        mShaderMatrix.mapRect(mBorderRect);
        mDrawableRect.set(mBorderRect.left + (float)mBorderWidth, mBorderRect.top + (float)mBorderWidth, mBorderRect.right - (float)mBorderWidth, mBorderRect.bottom - (float)mBorderWidth);
        mShaderMatrix.setRectToRect(mBitmapRect, mDrawableRect, android.graphics.Matrix.ScaleToFit.FILL);
        continue; /* Loop/switch isn't completed */
_L6:
        mBorderRect.set(mBitmapRect);
        mShaderMatrix.setRectToRect(mBitmapRect, mBounds, android.graphics.Matrix.ScaleToFit.END);
        mShaderMatrix.mapRect(mBorderRect);
        mDrawableRect.set(mBorderRect.left + (float)mBorderWidth, mBorderRect.top + (float)mBorderWidth, mBorderRect.right - (float)mBorderWidth, mBorderRect.bottom - (float)mBorderWidth);
        mShaderMatrix.setRectToRect(mBitmapRect, mDrawableRect, android.graphics.Matrix.ScaleToFit.FILL);
        continue; /* Loop/switch isn't completed */
_L7:
        mBorderRect.set(mBitmapRect);
        mShaderMatrix.setRectToRect(mBitmapRect, mBounds, android.graphics.Matrix.ScaleToFit.START);
        mShaderMatrix.mapRect(mBorderRect);
        mDrawableRect.set(mBorderRect.left + (float)mBorderWidth, mBorderRect.top + (float)mBorderWidth, mBorderRect.right - (float)mBorderWidth, mBorderRect.bottom - (float)mBorderWidth);
        mShaderMatrix.setRectToRect(mBitmapRect, mDrawableRect, android.graphics.Matrix.ScaleToFit.FILL);
        if (true) goto _L9; else goto _L8
_L8:
    }

    public void draw(Canvas canvas)
    {
        if (mBorderWidth > 0)
        {
            canvas.drawRoundRect(mBorderRect, mCornerRadius, mCornerRadius, mBorderPaint);
            canvas.drawRoundRect(mDrawableRect, Math.max(mCornerRadius - (float)mBorderWidth, 0.0F), Math.max(mCornerRadius - (float)mBorderWidth, 0.0F), mBitmapPaint);
        } else
        {
            canvas.drawRoundRect(mDrawableRect, mCornerRadius, mCornerRadius, mBitmapPaint);
        }
        if (isDown)
        {
            canvas.drawRoundRect(mDrawableRect, mCornerRadius, mCornerRadius, colorPaint);
        }
    }

    public int getBorderColor()
    {
        return mBorderColor;
    }

    public int getBorderWidth()
    {
        return mBorderWidth;
    }

    public float getCornerRadius()
    {
        return mCornerRadius;
    }

    public int getIntrinsicHeight()
    {
        return mBitmapHeight;
    }

    public int getIntrinsicWidth()
    {
        return mBitmapWidth;
    }

    public int getOpacity()
    {
        return -3;
    }

    protected android.widget.ImageView.ScaleType getScaleType()
    {
        return mScaleType;
    }

    protected void onBoundsChange(Rect rect)
    {
        super.onBoundsChange(rect);
        mBounds.set(rect);
        setMatrix();
    }

    public void setAlpha(int i)
    {
        mBitmapPaint.setAlpha(i);
    }

    public void setBorderColor(int i)
    {
        mBorderColor = i;
        mBorderPaint.setColor(i);
    }

    public void setBorderWidth(int i)
    {
        mBorderWidth = i;
        mBorderPaint.setStrokeWidth(mBorderWidth);
    }

    public void setColorFilter(ColorFilter colorfilter)
    {
        mBitmapPaint.setColorFilter(colorfilter);
    }

    public void setCornerRadius(float f)
    {
        mCornerRadius = f;
    }

    public void setPressDown(boolean flag)
    {
        isDown = flag;
        invalidateSelf();
    }

    protected void setScaleType(android.widget.ImageView.ScaleType scaletype)
    {
        android.widget.ImageView.ScaleType scaletype1 = scaletype;
        if (scaletype == null)
        {
            scaletype1 = android.widget.ImageView.ScaleType.FIT_XY;
        }
        if (mScaleType != scaletype1)
        {
            mScaleType = scaletype1;
            setMatrix();
        }
    }
}
