// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Scroller;

public class MultiDirectionSlidingDrawer extends FrameLayout
{
    public static interface Callback
    {

        public abstract void onCompletePullBack();

        public abstract void onCompletePullDown();

        public abstract void onStartPullDown();
    }


    public static final int FROM_LEFT = 1;
    public static final int FROM_TOP = 0;
    private static final int VELOCITY_THRESHOLD = 300;
    private boolean mAlwaysGestureControl;
    private Callback mCallback;
    private View mContentView;
    private boolean mDisallow;
    private boolean mEnableInterceptTouchEvent;
    private int mFrom;
    private boolean mInititaledPanel;
    private float mLastX;
    private float mLastY;
    private boolean mPrepareClose;
    private View mPullDownContentView;
    private int mPullDownViewMarginBottom;
    private Scroller mScroller;
    private ImageView mShadowView;
    private boolean mShowing;
    private int mTouchSlop;
    private int mUpDistance;
    private VelocityTracker mVelocityTracker;

    public MultiDirectionSlidingDrawer(Context context)
    {
        super(context);
        mEnableInterceptTouchEvent = true;
        mFrom = 0;
        mAlwaysGestureControl = false;
        mDisallow = false;
        mShowing = false;
        init(context);
    }

    public MultiDirectionSlidingDrawer(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        mEnableInterceptTouchEvent = true;
        mFrom = 0;
        mAlwaysGestureControl = false;
        mDisallow = false;
        mShowing = false;
        init(context);
    }

    public MultiDirectionSlidingDrawer(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mEnableInterceptTouchEvent = true;
        mFrom = 0;
        mAlwaysGestureControl = false;
        mDisallow = false;
        mShowing = false;
        init(context);
    }

    private void changeDim()
    {
        float f = 0.0F;
        mFrom;
        JVM INSTR tableswitch 0 1: default 28
    //                   0 41
    //                   1 64;
           goto _L1 _L2 _L3
_L1:
        mShadowView.setAlpha((int)(f * 200F));
        return;
_L2:
        f = (float)(mUpDistance - mPullDownContentView.getScrollY()) / (float)mUpDistance;
        continue; /* Loop/switch isn't completed */
_L3:
        f = (float)(mUpDistance - mPullDownContentView.getScrollX()) / (float)mUpDistance;
        if (true) goto _L1; else goto _L4
_L4:
    }

    private void init(Context context)
    {
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        mScroller = new Scroller(context);
        mShadowView = new ImageView(context);
        mShadowView.setImageResource(0x7f020530);
        mShadowView.setAlpha(0);
    }

    public void addView(View view, int i, android.view.ViewGroup.LayoutParams layoutparams)
    {
        if (getChildCount() > 3)
        {
            throw new IllegalArgumentException("The widget only and must only have 2 child views");
        } else
        {
            super.addView(view, i, layoutparams);
            return;
        }
    }

    public void closePullDownPanel()
    {
        mScroller = new Scroller(getContext(), new AccelerateInterpolator());
        mFrom;
        JVM INSTR tableswitch 0 1: default 48
    //                   0 79
    //                   1 113;
           goto _L1 _L2 _L3
_L1:
        mShowing = false;
        if (mCallback != null)
        {
            mCallback.onCompletePullBack();
        }
        enableInterceptTouchEvent(true);
        invalidate();
        return;
_L2:
        mScroller.startScroll(0, mPullDownContentView.getScrollY(), 0, mUpDistance - mPullDownContentView.getScrollY(), 400);
        continue; /* Loop/switch isn't completed */
_L3:
        mScroller.startScroll(mPullDownContentView.getScrollX(), 0, mUpDistance - mPullDownContentView.getScrollX(), 0, 400);
        if (true) goto _L1; else goto _L4
_L4:
    }

    public void computeScroll()
    {
        if (!mScroller.computeScrollOffset()) goto _L2; else goto _L1
_L1:
        mFrom;
        JVM INSTR tableswitch 0 1: default 36
    //                   0 45
    //                   1 63;
           goto _L3 _L4 _L5
_L3:
        changeDim();
        invalidate();
_L2:
        return;
_L4:
        mPullDownContentView.scrollTo(0, mScroller.getCurrY());
        continue; /* Loop/switch isn't completed */
_L5:
        mPullDownContentView.scrollTo(mScroller.getCurrX(), 0);
        if (true) goto _L3; else goto _L6
_L6:
    }

    public void disallowInterceptTouchEvent(boolean flag)
    {
        mDisallow = flag;
    }

    protected void dispatchDraw(Canvas canvas)
    {
        if (mInititaledPanel) goto _L2; else goto _L1
_L1:
        mFrom;
        JVM INSTR tableswitch 0 1: default 32
    //                   0 38
    //                   1 58;
           goto _L2 _L3 _L4
_L2:
        super.dispatchDraw(canvas);
        return;
_L3:
        mPullDownContentView.scrollTo(0, mUpDistance);
        mInititaledPanel = true;
        continue; /* Loop/switch isn't completed */
_L4:
        mPullDownContentView.scrollTo(mUpDistance, 0);
        mInititaledPanel = true;
        if (true) goto _L2; else goto _L5
_L5:
    }

    public void enableInterceptTouchEvent(boolean flag)
    {
        mEnableInterceptTouchEvent = flag;
    }

    public boolean isShowing()
    {
        return mShowing;
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        mPullDownContentView = findViewById(0x7f0a0020);
        mContentView = findViewById(0x7f0a0021);
        addView(mShadowView, 1);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        if (mDisallow && !mShowing)
        {
            return super.onInterceptTouchEvent(motionevent);
        }
        mFrom;
        JVM INSTR tableswitch 0 1: default 48
    //                   0 68
    //                   1 101;
           goto _L1 _L2 _L3
_L1:
        if (!mEnableInterceptTouchEvent && !mAlwaysGestureControl)
        {
            return super.onInterceptTouchEvent(motionevent);
        }
        break; /* Loop/switch isn't completed */
_L2:
        if (mPullDownContentView.getScrollY() == 0 && motionevent.getY() > (float)mPullDownContentView.getHeight())
        {
            mPrepareClose = true;
            return true;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (mPullDownContentView.getScrollX() == 0 && motionevent.getX() > (float)mPullDownContentView.getWidth())
        {
            mPrepareClose = true;
            return true;
        }
        if (true) goto _L1; else goto _L4
_L4:
        float f;
        float f1;
        f = motionevent.getX();
        f1 = motionevent.getY();
        switch (motionevent.getAction())
        {
        case 0: // '\0'
            mLastX = f;
            mLastY = f1;
            if (!mScroller.isFinished())
            {
                return true;
            }
            break;

        case 2: // '\002'
            switch (mFrom)
            {
            default:
                break;

            case 1: // '\001'
                continue;

            case 0: // '\0'
                if (Math.abs(f1 - mLastY) > Math.abs(f - mLastX) && Math.abs(f1 - mLastY) > (float)mTouchSlop)
                {
                    return true;
                }
                break;
            }
            break;
        }
        while (true) 
        {
            do
            {
                return super.onInterceptTouchEvent(motionevent);
            } while (Math.abs(f - mLastX) <= Math.abs(f1 - mLastY) || Math.abs(f - mLastX) <= (float)mTouchSlop);
            return true;
        }
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l)
    {
        l = getChildCount();
        if (l > 3)
        {
            throw new IllegalArgumentException("The widget only and must only have 2 child views");
        }
        j = 0;
_L7:
        View view;
        if (j >= l)
        {
            break MISSING_BLOCK_LABEL_193;
        }
        view = getChildAt(j);
        if (view.getVisibility() == 8) goto _L2; else goto _L1
_L1:
        if (view != mPullDownContentView)
        {
            break MISSING_BLOCK_LABEL_135;
        }
        mFrom;
        JVM INSTR tableswitch 0 1: default 84
    //                   0 91
    //                   1 113;
           goto _L3 _L4 _L5
_L3:
        break; /* Loop/switch isn't completed */
_L4:
        break; /* Loop/switch isn't completed */
_L2:
        j++;
        if (true) goto _L7; else goto _L6
_L6:
        view.layout(i, 0, view.getMeasuredWidth() + i, view.getMeasuredHeight());
          goto _L2
_L5:
        view.layout(i, 0, k - mPullDownViewMarginBottom, view.getMeasuredHeight());
          goto _L2
        if (view == mContentView)
        {
            view.layout(i, 0, view.getMeasuredWidth() + i, view.getMeasuredHeight());
        } else
        if (view == mShadowView)
        {
            mShadowView.layout(i, 0, k, getMeasuredHeight());
        }
          goto _L2
        if (mUpDistance != 0) goto _L9; else goto _L8
_L8:
        mFrom;
        JVM INSTR tableswitch 0 1: default 228
    //                   0 229
    //                   1 243;
           goto _L9 _L10 _L11
_L9:
        return;
_L10:
        mUpDistance = getHeight() - mPullDownViewMarginBottom;
        return;
_L11:
        mUpDistance = getWidth() - mPullDownViewMarginBottom;
        return;
    }

    protected void onMeasure(int i, int j)
    {
        super.onMeasure(i, j);
        int k = android.view.View.MeasureSpec.getSize(j);
        int l = android.view.View.MeasureSpec.getSize(i);
        if (mPullDownContentView == null)
        {
            throw new IllegalArgumentException("Pull down panel can't be null");
        }
        switch (mFrom)
        {
        default:
            return;

        case 1: // '\001'
            mPullDownContentView.measure(android.view.View.MeasureSpec.makeMeasureSpec(l - mPullDownViewMarginBottom, 0x40000000), j);
            return;

        case 0: // '\0'
            mPullDownContentView.measure(i, android.view.View.MeasureSpec.makeMeasureSpec(k - mPullDownViewMarginBottom, 0x40000000));
            return;
        }
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        float f2;
        float f3;
        f2 = motionevent.getX();
        f3 = motionevent.getY();
        if (mVelocityTracker == null)
        {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(motionevent);
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 68
    //                   0 70
    //                   1 545
    //                   2 118
    //                   3 545;
           goto _L1 _L2 _L3 _L4 _L3
_L1:
        return true;
_L2:
        if (!mScroller.isFinished())
        {
            mScroller.abortAnimation();
        }
        if (mCallback != null)
        {
            mCallback.onStartPullDown();
        }
        mLastX = f2;
        mLastY = f3;
        continue; /* Loop/switch isn't completed */
_L4:
        float f1 = mLastY - f3;
        float f = mLastX - f2;
        mLastX = f2;
        mLastY = f3;
        switch (mFrom)
        {
        default:
            continue; /* Loop/switch isn't completed */

        case 0: // '\0'
            if (mPullDownContentView.getScrollY() <= 0 && f1 < 0.0F || mPullDownContentView.getScrollY() >= mUpDistance && f1 > 0.0F)
            {
                return mPrepareClose;
            }
            if ((float)mPullDownContentView.getScrollY() + f1 <= 0.0F && f1 < 0.0F)
            {
                f = -mPullDownContentView.getScrollY();
                mShowing = true;
                if (mCallback != null)
                {
                    mCallback.onCompletePullDown();
                }
                enableInterceptTouchEvent(false);
            } else
            {
                f = f1;
                if ((float)mPullDownContentView.getScrollY() + f1 >= (float)mUpDistance)
                {
                    f = f1;
                    if (f1 > 0.0F)
                    {
                        f = mUpDistance - mPullDownContentView.getScrollY();
                        mShowing = false;
                        if (mCallback != null)
                        {
                            mCallback.onCompletePullBack();
                        }
                        enableInterceptTouchEvent(true);
                    }
                }
            }
            mPullDownContentView.scrollBy(0, (int)f);
            changeDim();
            continue; /* Loop/switch isn't completed */

        case 1: // '\001'
            if (mPullDownContentView.getScrollX() <= 0 && f < 0.0F || mPullDownContentView.getScrollX() >= mUpDistance && f > 0.0F)
            {
                return mPrepareClose;
            }
            break;
        }
        if ((float)mPullDownContentView.getScrollX() + f <= 0.0F && f < 0.0F)
        {
            f = -mPullDownContentView.getScrollX();
            mShowing = true;
            if (mCallback != null)
            {
                mCallback.onCompletePullDown();
            }
            enableInterceptTouchEvent(false);
        } else
        if ((float)mPullDownContentView.getScrollX() + f >= (float)mUpDistance && f > 0.0F)
        {
            f = mUpDistance - mPullDownContentView.getScrollX();
            mShowing = false;
            if (mCallback != null)
            {
                mCallback.onCompletePullBack();
            }
            enableInterceptTouchEvent(true);
        }
        mPullDownContentView.scrollBy((int)f, 0);
        changeDim();
        continue; /* Loop/switch isn't completed */
_L3:
        if (mPrepareClose)
        {
            closePullDownPanel();
            mPrepareClose = false;
            return true;
        }
        mLastX = 0.0F;
        mLastY = 0.0F;
        mVelocityTracker.computeCurrentVelocity(1000);
        int i = (int)mVelocityTracker.getYVelocity();
        int j = (int)mVelocityTracker.getXVelocity();
        switch (mFrom)
        {
        default:
            break;

        case 0: // '\0'
            if (i > 300)
            {
                if (mPullDownContentView.getScrollY() > 0)
                {
                    mScroller = new Scroller(getContext(), new AccelerateDecelerateInterpolator());
                    mScroller.startScroll(0, mPullDownContentView.getScrollY(), 0, -mPullDownContentView.getScrollY(), (mPullDownContentView.getScrollY() * 400) / mUpDistance);
                    mShowing = true;
                    if (mCallback != null)
                    {
                        mCallback.onCompletePullDown();
                    }
                    enableInterceptTouchEvent(false);
                    invalidate();
                }
                break;
            }
            if (i < -300)
            {
                if (mPullDownContentView.getScrollY() < mUpDistance)
                {
                    mScroller = new Scroller(getContext(), new AccelerateInterpolator());
                    mScroller.startScroll(0, mPullDownContentView.getScrollY(), 0, mUpDistance - mPullDownContentView.getScrollY(), ((mUpDistance - mPullDownContentView.getScrollY()) * 400) / mUpDistance);
                    mShowing = false;
                    if (mCallback != null)
                    {
                        mCallback.onCompletePullBack();
                    }
                    enableInterceptTouchEvent(true);
                    invalidate();
                }
                break;
            }
            if (mPullDownContentView.getScrollY() > mUpDistance - (mUpDistance >> 2))
            {
                if (mPullDownContentView.getScrollY() < mUpDistance)
                {
                    mScroller = new Scroller(getContext(), new AccelerateInterpolator());
                    mScroller.startScroll(0, mPullDownContentView.getScrollY(), 0, mUpDistance - mPullDownContentView.getScrollY(), ((mUpDistance - mPullDownContentView.getScrollY()) * 400) / mUpDistance);
                    mShowing = false;
                    if (mCallback != null)
                    {
                        mCallback.onCompletePullBack();
                    }
                    enableInterceptTouchEvent(true);
                    invalidate();
                }
                break;
            }
            if (mPullDownContentView.getScrollY() > 0)
            {
                mScroller = new Scroller(getContext(), new AccelerateDecelerateInterpolator());
                mScroller.startScroll(0, mPullDownContentView.getScrollY(), 0, -mPullDownContentView.getScrollY(), (mPullDownContentView.getScrollY() * 400) / mUpDistance);
                mShowing = true;
                if (mCallback != null)
                {
                    mCallback.onCompletePullDown();
                }
                enableInterceptTouchEvent(false);
                invalidate();
            }
            break;

        case 1: // '\001'
            if (j > 300)
            {
                if (mPullDownContentView.getScrollX() > 0)
                {
                    mScroller = new Scroller(getContext(), new AccelerateDecelerateInterpolator());
                    mScroller.startScroll(mPullDownContentView.getScrollX(), 0, -mPullDownContentView.getScrollX(), 0, (mPullDownContentView.getScrollX() * 400) / mUpDistance);
                    mShowing = true;
                    if (mCallback != null)
                    {
                        mCallback.onCompletePullDown();
                    }
                    enableInterceptTouchEvent(false);
                    invalidate();
                }
                break;
            }
            if (j < -300)
            {
                if (mPullDownContentView.getScrollX() < mUpDistance)
                {
                    mScroller = new Scroller(getContext(), new AccelerateInterpolator());
                    mScroller.startScroll(mPullDownContentView.getScrollX(), 0, mUpDistance - mPullDownContentView.getScrollX(), 0, ((mUpDistance - mPullDownContentView.getScrollX()) * 400) / mUpDistance);
                    mShowing = false;
                    if (mCallback != null)
                    {
                        mCallback.onCompletePullBack();
                    }
                    enableInterceptTouchEvent(true);
                    invalidate();
                }
                break;
            }
            if (mPullDownContentView.getScrollX() > mUpDistance - (mUpDistance >> 2))
            {
                if (mPullDownContentView.getScrollX() < mUpDistance)
                {
                    mScroller = new Scroller(getContext(), new AccelerateInterpolator());
                    mScroller.startScroll(mPullDownContentView.getScrollX(), 0, mUpDistance - mPullDownContentView.getScrollX(), 0, ((mUpDistance - mPullDownContentView.getScrollX()) * 400) / mUpDistance);
                    mShowing = false;
                    if (mCallback != null)
                    {
                        mCallback.onCompletePullBack();
                    }
                    enableInterceptTouchEvent(true);
                    invalidate();
                }
                break;
            }
            if (mPullDownContentView.getScrollX() > 0)
            {
                mScroller = new Scroller(getContext(), new AccelerateDecelerateInterpolator());
                mScroller.startScroll(mPullDownContentView.getScrollX(), 0, -mPullDownContentView.getScrollX(), 0, (mPullDownContentView.getScrollX() * 400) / mUpDistance);
                mShowing = true;
                if (mCallback != null)
                {
                    mCallback.onCompletePullDown();
                }
                enableInterceptTouchEvent(false);
                invalidate();
            }
            break;
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    public void openPullDownPanel()
    {
        mScroller = new Scroller(getContext(), new AccelerateDecelerateInterpolator());
        mFrom;
        JVM INSTR tableswitch 0 1: default 48
    //                   0 79
    //                   1 122;
           goto _L1 _L2 _L3
_L1:
        mShowing = true;
        if (mCallback != null)
        {
            mCallback.onCompletePullDown();
        }
        enableInterceptTouchEvent(false);
        invalidate();
        return;
_L2:
        mScroller.startScroll(0, mPullDownContentView.getScrollY(), 0, -mPullDownContentView.getScrollY(), (mPullDownContentView.getScrollY() * 400) / mUpDistance);
        continue; /* Loop/switch isn't completed */
_L3:
        mScroller.startScroll(mPullDownContentView.getScrollX(), 0, -mPullDownContentView.getScrollX(), 0, (mPullDownContentView.getScrollX() * 400) / mUpDistance);
        if (true) goto _L1; else goto _L4
_L4:
    }

    public void setAlwaysGesureControl(boolean flag)
    {
        mAlwaysGestureControl = flag;
    }

    public void setCallback(Callback callback)
    {
        mCallback = callback;
    }

    public void setContentView(View view)
    {
        if (mContentView != null)
        {
            removeView(mContentView);
        }
        mContentView = view;
        addView(view);
    }

    public void setFrom(int i)
    {
        mFrom = i;
    }

    public void setPullDownContentView(View view)
    {
        if (mPullDownContentView != null)
        {
            removeView(mPullDownContentView);
        }
        mPullDownContentView = view;
        if (mContentView != null)
        {
            addView(mShadowView, 1);
            addView(mPullDownContentView, 2);
            return;
        } else
        {
            addView(view);
            return;
        }
    }

    public void setPullDownViewMarginBottom(int i)
    {
        mPullDownViewMarginBottom = i;
        requestLayout();
    }

    public void setUpDistance(int i)
    {
        mUpDistance = i;
    }
}
