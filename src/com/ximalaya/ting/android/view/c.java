// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.view:
//            MyVerificationCodeDialogFragment

class c extends MyAsyncTask
{

    ProgressDialog a;
    final MyVerificationCodeDialogFragment b;

    c(MyVerificationCodeDialogFragment myverificationcodedialogfragment)
    {
        b = myverificationcodedialogfragment;
        super();
    }

    protected transient Bitmap a(Void avoid[])
    {
        if (MyApplication.b() == null)
        {
            return null;
        } else
        {
            return ImageManager2.from(b.getActivity().getApplicationContext()).getBitmapFromUrl(MyVerificationCodeDialogFragment.access$000(b));
        }
    }

    protected void a(Bitmap bitmap)
    {
        if (a != null)
        {
            a.cancel();
            a = null;
        }
        while (!b.isAdded() || b.getActivity() == null || bitmap == null) 
        {
            return;
        }
        MyVerificationCodeDialogFragment.access$100(b).setImageBitmap(bitmap);
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((Bitmap)obj);
    }

    protected void onPreExecute()
    {
        a = new MyProgressDialog(b.getActivity());
        a.setCanceledOnTouchOutside(false);
        a.show();
    }
}
