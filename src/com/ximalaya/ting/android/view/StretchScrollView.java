// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ScrollView;

// Referenced classes of package com.ximalaya.ting.android.view:
//            n

public class StretchScrollView extends ScrollView
{

    private static int MAX_SCROLL_HEIGHT = 0;
    private static final int MSG_REST_POSITION = 1;
    private static final float SCROLL_RATIO = 0.2F;
    private View mChildRootView;
    private Handler mHandler;
    private int mScrollDy;
    private int mScrollY;
    private boolean mTouchStop;
    private float mTouchY;

    public StretchScrollView(Context context)
    {
        super(context);
        mTouchStop = false;
        mScrollY = 0;
        mScrollDy = 0;
        mHandler = new n(this);
        init();
    }

    public StretchScrollView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        mTouchStop = false;
        mScrollY = 0;
        mScrollDy = 0;
        mHandler = new n(this);
        init();
    }

    public StretchScrollView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mTouchStop = false;
        mScrollY = 0;
        mScrollDy = 0;
        mHandler = new n(this);
        init();
    }

    private void doTouchEvent(MotionEvent motionevent)
    {
        motionevent.getAction();
        JVM INSTR tableswitch 1 2: default 28
    //                   1 29
    //                   2 75;
           goto _L1 _L2 _L3
_L1:
        return;
_L2:
        mScrollY = mChildRootView.getScrollY();
        if (mScrollY != 0)
        {
            mTouchStop = true;
            mScrollDy = (int)((float)mScrollY / 10F);
            mHandler.sendEmptyMessage(1);
            return;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        float f = motionevent.getY();
        int i = (int)(mTouchY - f);
        mTouchY = f;
        if (isNeedMove())
        {
            int j = mChildRootView.getScrollY();
            if (j < MAX_SCROLL_HEIGHT && j > -MAX_SCROLL_HEIGHT)
            {
                mChildRootView.scrollBy(0, (int)((float)i * 0.2F));
                mTouchStop = false;
                return;
            }
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    private void init()
    {
        if (android.os.Build.VERSION.SDK_INT >= 9)
        {
            setOverScrollMode(2);
        }
    }

    private boolean isNeedMove()
    {
        int i = mChildRootView.getMeasuredHeight();
        int j = getHeight();
        int k = getScrollY();
        return k == 0 || k == i - j;
    }

    protected void onFinishInflate()
    {
        if (getChildCount() > 0)
        {
            mChildRootView = getChildAt(0);
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        if (motionevent.getAction() == 0)
        {
            mTouchY = motionevent.getY();
        }
        return super.onInterceptTouchEvent(motionevent);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (mChildRootView != null)
        {
            doTouchEvent(motionevent);
        }
        return super.onTouchEvent(motionevent);
    }

    public void setMaxScrollHeight(int i)
    {
        MAX_SCROLL_HEIGHT = i;
    }

    static 
    {
        MAX_SCROLL_HEIGHT = 400;
    }



/*
    static int access$002(StretchScrollView stretchscrollview, int i)
    {
        stretchscrollview.mScrollY = i;
        return i;
    }

*/


/*
    static int access$020(StretchScrollView stretchscrollview, int i)
    {
        i = stretchscrollview.mScrollY - i;
        stretchscrollview.mScrollY = i;
        return i;
    }

*/



}
