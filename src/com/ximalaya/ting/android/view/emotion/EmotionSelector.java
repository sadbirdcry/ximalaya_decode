// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.emotion;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.viewpagerindicator.CirclePageIndicator;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.emotion:
//            a, b, c, d, 
//            e, f

public class EmotionSelector extends LinearLayout
{
    public static interface OnFocusChangeListener
    {

        public abstract void onFocusChange(View view, boolean flag);
    }

    public static interface OnSendButtonClickListener
    {

        public abstract void onClick(View view, CharSequence charsequence);
    }

    public static interface OnTextChangeListener
    {

        public abstract void afterTextChanged(Editable editable);

        public abstract void beforeTextChanged(CharSequence charsequence, int i, int j, int k);

        public abstract void onTextChanged(CharSequence charsequence, int i, int j, int k);
    }

    class a extends BaseAdapter
    {

        final EmotionSelector a;
        private int b;
        private int c;
        private int d;

        public int a()
        {
            return d - c;
        }

        public int a(int i)
        {
            return c + i;
        }

        public int getCount()
        {
            return 28;
        }

        public Object getItem(int i)
        {
            return null;
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            View view1;
            if (view == null)
            {
                view = new ImageView(a.getContext());
                view.setScaleType(android.widget.ImageView.ScaleType.FIT_CENTER);
                view.setLayoutParams(new android.widget.AbsListView.LayoutParams(ToolUtil.dp2px(a.getContext(), 35F), ToolUtil.dp2px(a.getContext(), 35F)));
                view1 = view;
                viewgroup = view;
            } else
            {
                viewgroup = (ImageView)view;
                view1 = view;
            }
            if (i < a())
            {
                viewgroup.setImageResource(a.mEmotionUtil.getEmotionIconIdAt(a(i)));
                return view1;
            }
            if (i == getCount() - 1)
            {
                viewgroup.setImageResource(0x7f0201c4);
                return view1;
            } else
            {
                viewgroup.setImageDrawable(null);
                return view1;
            }
        }

        public a(int i, int j, int k)
        {
            a = EmotionSelector.this;
            super();
            b = i;
            c = j;
            d = k;
        }
    }

    class b extends PagerAdapter
    {

        final EmotionSelector a;

        public void destroyItem(ViewGroup viewgroup, int i, Object obj)
        {
            viewgroup.removeView((View)a.mPages.get(i));
        }

        public int getCount()
        {
            return a.mPages.size();
        }

        public Object instantiateItem(ViewGroup viewgroup, int i)
        {
            View view = (View)a.mPages.get(i);
            viewgroup.addView(view);
            return view;
        }

        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }

        public b()
        {
            a = EmotionSelector.this;
            super();
            mPages.clear();
            int l = mEmotionUtil.getEmotionCount();
            int i;
            int j;
            if (l % 27 == 0)
            {
                i = l / 27;
            } else
            {
                i = l / 27 + 1;
            }
            for (j = 0; j < i; j++)
            {
                int i1 = (j + 1) * 27;
                int k = i1;
                if (i1 > l)
                {
                    k = l;
                }
                GridView gridview = new GridView(getContext());
                gridview.setGravity(17);
                gridview.setNumColumns(7);
                gridview.setVerticalSpacing(ToolUtil.dp2px(getContext(), 5F));
                gridview.setHorizontalSpacing(ToolUtil.dp2px(getContext(), 5F));
                gridview.setPadding(ToolUtil.dp2px(getContext(), 3F), ToolUtil.dp2px(getContext(), 3F), ToolUtil.dp2px(getContext(), 3F), ToolUtil.dp2px(getContext(), 3F));
                gridview.setSelector(0x7f070003);
                gridview.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, -2));
                gridview.setAdapter(new a(j, j * 27, k));
                gridview.setOnItemClickListener(mEmotionClickListener);
                mPages.add(gridview);
            }

        }
    }


    private static final int EMOTION_COL_NUBMER = 7;
    private static final int EMOTION_ROW_NUMBER = 4;
    private View mContent;
    private EditText mEditText;
    private LinearLayout mEmotionBar;
    private android.widget.AdapterView.OnItemClickListener mEmotionClickListener;
    private ImageView mEmotionSelector;
    private EmotionUtil mEmotionUtil;
    private boolean mHandleFocusChangeEvent;
    private CirclePageIndicator mIndicator;
    private LinearLayout mInputBar;
    private int mMaxChars;
    private int mMaxLines;
    private InputMethodManager mMethodManager;
    private OnFocusChangeListener mOnFocusChangeListener;
    private OnSendButtonClickListener mOnSendButtonClickListener;
    private OnTextChangeListener mOnTextChangeListener;
    private b mPagerAdapter;
    private List mPages;
    private ViewGroup mParent;
    private ImageView mSendBtn;
    private boolean mShowEmotionBar;
    private ViewPager mViewPager;
    private TextWatcher mWatcher;

    public EmotionSelector(Context context)
    {
        this(context, null);
    }

    public EmotionSelector(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        mShowEmotionBar = false;
        mMaxLines = 5;
        mMaxChars = 100;
        mPages = new ArrayList();
        mHandleFocusChangeEvent = true;
        mEmotionClickListener = new com.ximalaya.ting.android.view.emotion.a(this);
        mWatcher = new com.ximalaya.ting.android.view.emotion.b(this);
        parseXML(getContext(), attributeset, 0);
        mEmotionUtil = EmotionUtil.getInstance();
        initUI();
    }

    protected EmotionSelector(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mShowEmotionBar = false;
        mMaxLines = 5;
        mMaxChars = 100;
        mPages = new ArrayList();
        mHandleFocusChangeEvent = true;
        mEmotionClickListener = new com.ximalaya.ting.android.view.emotion.a(this);
        mWatcher = new com.ximalaya.ting.android.view.emotion.b(this);
        parseXML(getContext(), attributeset, i);
        initUI();
    }

    private boolean deleteEmotion(EditText edittext)
    {
        this;
        JVM INSTR monitorenter ;
        boolean flag = mEmotionUtil.deleteEmotion(edittext);
        this;
        JVM INSTR monitorexit ;
        return flag;
        edittext;
        throw edittext;
    }

    private int getEmotionIndex(String s)
    {
        return mEmotionUtil.getEmotionIndex(s);
    }

    private void initUI()
    {
        mMethodManager = (InputMethodManager)getContext().getSystemService("input_method");
        mContent = View.inflate(getContext(), 0x7f03007a, this);
        mInputBar = (LinearLayout)mContent.findViewById(0x7f0a022a);
        mEmotionBar = (LinearLayout)mContent.findViewById(0x7f0a022e);
        mEmotionSelector = (ImageView)mContent.findViewById(0x7f0a022b);
        mEditText = (EditText)mContent.findViewById(0x7f0a022c);
        mSendBtn = (ImageView)mContent.findViewById(0x7f0a022d);
        mViewPager = (ViewPager)mContent.findViewById(0x7f0a022f);
        mIndicator = (CirclePageIndicator)mContent.findViewById(0x7f0a0230);
        if (mShowEmotionBar)
        {
            mEmotionBar.setVisibility(0);
        } else
        {
            mEmotionBar.setVisibility(8);
        }
        mEditText.setMaxLines(mMaxLines);
        mEditText.setHint("");
        mPagerAdapter = new b();
        mViewPager.setAdapter(mPagerAdapter);
        mIndicator.setViewPager(mViewPager);
    }

    private void insertEmotion(EditText edittext, String s, Drawable drawable)
    {
        this;
        JVM INSTR monitorenter ;
        mEmotionUtil.insertEmotion(edittext, s, drawable);
        this;
        JVM INSTR monitorexit ;
        return;
        edittext;
        throw edittext;
    }

    private void parseXML(Context context, AttributeSet attributeset, int i)
    {
        context = context.getResources().obtainAttributes(attributeset, com.ximalaya.ting.android.R.styleable.EmotionSelector);
        mShowEmotionBar = context.getBoolean(0, mShowEmotionBar);
        mMaxLines = context.getInt(1, mMaxLines);
        mMaxChars = context.getInt(2, mMaxChars);
        context.recycle();
    }

    private void registerListener()
    {
        mEmotionSelector.setOnClickListener(new c(this));
        mEditText.setOnFocusChangeListener(new d(this));
        mEditText.addTextChangedListener(mWatcher);
        mEditText.setOnKeyListener(new e(this));
        mSendBtn.setOnClickListener(new f(this));
    }

    private void unregisterListener()
    {
        for (int i = 0; i < mPages.size(); i++)
        {
            ((GridView)mPages.get(i)).setOnItemClickListener(null);
        }

        mEmotionSelector.setOnClickListener(null);
        mSendBtn.setOnClickListener(null);
        mEditText.setOnFocusChangeListener(null);
        if (mWatcher != null)
        {
            mEditText.removeTextChangedListener(mWatcher);
        }
        mEditText.setOnKeyListener(null);
        mOnSendButtonClickListener = null;
        mOnFocusChangeListener = null;
        mOnTextChangeListener = null;
    }

    public void appendText(CharSequence charsequence)
    {
        this;
        JVM INSTR monitorenter ;
        int i = mEditText.getSelectionStart();
        mEditText.requestFocus();
        mEditText.getEditableText().insert(i, charsequence);
        mEditText.setSelection(i + charsequence.length());
        this;
        JVM INSTR monitorexit ;
        return;
        charsequence;
        throw charsequence;
    }

    public void dismiss()
    {
        ((InputMethodManager)getContext().getSystemService("input_method")).hideSoftInputFromWindow(mEditText.getWindowToken(), 2);
        setVisibility(8);
        setText("");
    }

    public void enableSendBtn(boolean flag)
    {
        mSendBtn.setEnabled(flag);
    }

    public int getEmotionId(String s)
    {
        return mEmotionUtil.getEmotionIconId(s);
    }

    public int getEmotionPanelStatus()
    {
        return mEmotionBar.getVisibility();
    }

    public String getText()
    {
        return mEditText.getText().toString();
    }

    public void hideEmotionPanel()
    {
        mEmotionBar.setVisibility(8);
        mEditText.clearFocus();
    }

    public boolean isEmotionName(String s)
    {
        return mEmotionUtil.isEmotionName(s);
    }

    public boolean isPositionInEmotion(String s, int i, int ai[])
    {
        return mEmotionUtil.isPositionInEmotion(s, i, ai);
    }

    protected void onAttachedToWindow()
    {
        super.onAttachedToWindow();
        registerListener();
        mParent = (ViewGroup)getParent();
    }

    protected void onDetachedFromWindow()
    {
        mParent = null;
        unregisterListener();
        super.onDetachedFromWindow();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        if (mParent == null) goto _L2; else goto _L1
_L1:
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 40
    //                   0 46
    //                   1 57
    //                   2 46
    //                   3 57;
           goto _L2 _L3 _L4 _L3 _L4
_L2:
        return super.onInterceptTouchEvent(motionevent);
_L3:
        mParent.requestDisallowInterceptTouchEvent(true);
        continue; /* Loop/switch isn't completed */
_L4:
        mParent.requestDisallowInterceptTouchEvent(false);
        if (true) goto _L2; else goto _L5
_L5:
    }

    public boolean onKeyDown(int i, KeyEvent keyevent)
    {
        return super.onKeyDown(i, keyevent);
    }

    public void setEmotionSelectorIcon(int i)
    {
        if (i != 0 && mEmotionSelector != null)
        {
            mEmotionSelector.setImageResource(i);
        }
    }

    public void setHint(String s)
    {
        if (mEditText != null && !TextUtils.isEmpty(s))
        {
            mEditText.setHint(s);
        }
    }

    public void setOnEmotionTextChange(OnTextChangeListener ontextchangelistener)
    {
        mOnTextChangeListener = ontextchangelistener;
    }

    public void setOnInputBoxFocusChangeListener(OnFocusChangeListener onfocuschangelistener)
    {
        mOnFocusChangeListener = onfocuschangelistener;
    }

    public void setOnSendButtonClickListener(OnSendButtonClickListener onsendbuttonclicklistener)
    {
        mOnSendButtonClickListener = onsendbuttonclicklistener;
    }

    public void setText(CharSequence charsequence)
    {
        mEditText.requestFocus();
        mEditText.setText(charsequence);
        mEditText.setSelection(mEditText.getEditableText().length());
    }

    public void setVisibility(int i)
    {
        if (i == 0)
        {
            mEditText.requestFocus();
            mMethodManager.showSoftInput(mEditText, 0);
        } else
        {
            mMethodManager.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
        }
        super.setVisibility(i);
    }

    public void shouldHandleFocusChangeEvent(boolean flag)
    {
        mHandleFocusChangeEvent = flag;
    }

    public void showEmotionPanel()
    {
        mEmotionBar.setVisibility(0);
        mEditText.requestFocus();
    }

    public void toggleSoftInput()
    {
        if (getVisibility() == 0)
        {
            mEditText.requestFocus();
            mMethodManager.showSoftInput(mEditText, 0);
            return;
        } else
        {
            mMethodManager.hideSoftInputFromWindow(mEditText.getWindowToken(), 0);
            return;
        }
    }














}
