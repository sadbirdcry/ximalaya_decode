// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.emotion;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.view.emotion:
//            EmotionSelector

class d extends BaseAdapter
{

    final EmotionSelector a;
    private int b;
    private int c;
    private int d;

    public int a()
    {
        return d - c;
    }

    public int a(int i)
    {
        return c + i;
    }

    public int getCount()
    {
        return 28;
    }

    public Object getItem(int i)
    {
        return null;
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        View view1;
        if (view == null)
        {
            view = new ImageView(a.getContext());
            view.setScaleType(android.widget.e.FIT_CENTER);
            view.setLayoutParams(new android.widget.Params(ToolUtil.dp2px(a.getContext(), 35F), ToolUtil.dp2px(a.getContext(), 35F)));
            view1 = view;
            viewgroup = view;
        } else
        {
            viewgroup = (ImageView)view;
            view1 = view;
        }
        if (i < a())
        {
            viewgroup.setImageResource(EmotionSelector.access$000(a).getEmotionIconIdAt(a(i)));
            return view1;
        }
        if (i == getCount() - 1)
        {
            viewgroup.setImageResource(0x7f0201c4);
            return view1;
        } else
        {
            viewgroup.setImageDrawable(null);
            return view1;
        }
    }

    public (EmotionSelector emotionselector, int i, int j, int k)
    {
        a = emotionselector;
        super();
        b = i;
        c = j;
        d = k;
    }
}
