// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.emotion;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ImageView;

// Referenced classes of package com.ximalaya.ting.android.view.emotion:
//            EmotionSelector

class b
    implements TextWatcher
{

    final EmotionSelector a;

    b(EmotionSelector emotionselector)
    {
        a = emotionselector;
        super();
    }

    public void afterTextChanged(Editable editable)
    {
        if (editable != null && editable.length() > EmotionSelector.access$600(a))
        {
            EmotionSelector.access$700(a).setEnabled(false);
        } else
        {
            EmotionSelector.access$700(a).setEnabled(true);
        }
        if (EmotionSelector.access$800(a) != null)
        {
            EmotionSelector.access$800(a).afterTextChanged(editable);
        }
    }

    public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
    {
        if (EmotionSelector.access$800(a) != null)
        {
            EmotionSelector.access$800(a).beforeTextChanged(charsequence, i, j, k);
        }
    }

    public void onTextChanged(CharSequence charsequence, int i, int j, int k)
    {
        if (EmotionSelector.access$800(a) != null)
        {
            EmotionSelector.access$800(a).onTextChanged(charsequence, i, j, k);
        }
    }
}
