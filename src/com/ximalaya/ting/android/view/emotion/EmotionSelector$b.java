// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.emotion;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.emotion:
//            EmotionSelector

class cess._cls100 extends PagerAdapter
{

    final EmotionSelector a;

    public void destroyItem(ViewGroup viewgroup, int i, Object obj)
    {
        viewgroup.removeView((View)EmotionSelector.access$100(a).get(i));
    }

    public int getCount()
    {
        return EmotionSelector.access$100(a).size();
    }

    public Object instantiateItem(ViewGroup viewgroup, int i)
    {
        View view = (View)EmotionSelector.access$100(a).get(i);
        viewgroup.addView(view);
        return view;
    }

    public boolean isViewFromObject(View view, Object obj)
    {
        return view == obj;
    }

    public (EmotionSelector emotionselector)
    {
        a = emotionselector;
        super();
        EmotionSelector.access$100(emotionselector).clear();
        int l = EmotionSelector.access$000(emotionselector).getEmotionCount();
        int i;
        int j;
        if (l % 27 == 0)
        {
            i = l / 27;
        } else
        {
            i = l / 27 + 1;
        }
        for (j = 0; j < i; j++)
        {
            int i1 = (j + 1) * 27;
            int k = i1;
            if (i1 > l)
            {
                k = l;
            }
            GridView gridview = new GridView(emotionselector.getContext());
            gridview.setGravity(17);
            gridview.setNumColumns(7);
            gridview.setVerticalSpacing(ToolUtil.dp2px(emotionselector.getContext(), 5F));
            gridview.setHorizontalSpacing(ToolUtil.dp2px(emotionselector.getContext(), 5F));
            gridview.setPadding(ToolUtil.dp2px(emotionselector.getContext(), 3F), ToolUtil.dp2px(emotionselector.getContext(), 3F), ToolUtil.dp2px(emotionselector.getContext(), 3F), ToolUtil.dp2px(emotionselector.getContext(), 3F));
            gridview.setSelector(0x7f070003);
            gridview.setLayoutParams(new android.widget.Params(-1, -2));
            gridview.setAdapter(new <init>(emotionselector, j, j * 27, k));
            gridview.setOnItemClickListener(EmotionSelector.access$200(emotionselector));
            EmotionSelector.access$100(emotionselector).add(gridview);
        }

    }
}
