// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.Scroller;
import com.ximalaya.ting.android.activity.MainTabActivity2;

// Referenced classes of package com.ximalaya.ting.android.view:
//            DetectsSoftKeyboardRelativeLayout, m

public class SlideRightOutView extends DetectsSoftKeyboardRelativeLayout
{
    public static interface OnFinishListener
    {

        public abstract boolean onFinish();
    }

    public static interface SlideListener
    {

        public abstract void slideEnd();

        public abstract void slideStart();
    }


    private static final int VELOCITY = 50;
    private int bgShadeMargin;
    private boolean canSlide;
    private boolean isFinish;
    private boolean isFirst;
    private Context mContext;
    private OnFinishListener mFinishListener;
    private boolean mIsBeingDragged;
    private float mLastMotionX;
    private float mLastMotionY;
    private Scroller mScroller;
    private SlideListener mSlideListener;
    private int mTouchSlop;
    private VelocityTracker mVelocityTracker;
    private View mainView;
    private int screenWidth;

    public SlideRightOutView(Context context)
    {
        super(context);
        bgShadeMargin = 10;
        mIsBeingDragged = false;
        canSlide = true;
        isFinish = false;
        isFirst = true;
        init(context);
    }

    public SlideRightOutView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        bgShadeMargin = 10;
        mIsBeingDragged = false;
        canSlide = true;
        isFinish = false;
        isFirst = true;
        init(context);
    }

    public SlideRightOutView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        bgShadeMargin = 10;
        mIsBeingDragged = false;
        canSlide = true;
        isFinish = false;
        isFirst = true;
        init(context);
    }

    private void changeAlpha()
    {
        if (mainView == null)
        {
            return;
        } else
        {
            Drawable drawable = mainView.getBackground();
            drawable.setAlpha((int)((((double)(screenWidth + mainView.getScrollX()) + 0.0D) / (double)screenWidth) * 225D));
            mainView.setBackgroundDrawable(drawable);
            return;
        }
    }

    private void finish()
    {
        boolean flag;
        flag = false;
        if (mFinishListener != null)
        {
            flag = mFinishListener.onFinish();
        }
        break MISSING_BLOCK_LABEL_19;
        if (!flag && !isFinish)
        {
            isFinish = true;
            if (mainView != null)
            {
                mainView.setVisibility(8);
            }
            if (mContext != null)
            {
                if (mContext instanceof MainTabActivity2)
                {
                    ((MainTabActivity2)mContext).onBack();
                    return;
                }
                if ((mContext instanceof Activity) && !((Activity)mContext).isFinishing())
                {
                    ((Activity)mContext).finish();
                    return;
                }
            }
        }
        return;
    }

    private void init(Context context)
    {
        mContext = context;
        mTouchSlop = ViewConfiguration.get(mContext).getScaledTouchSlop();
        mScroller = new Scroller(mContext);
        if (mContext instanceof Activity)
        {
            screenWidth = ((Activity)mContext).getWindowManager().getDefaultDisplay().getWidth();
        }
        postDelayed(new m(this), 100L);
    }

    private void smoothScrollTo(int i)
    {
        if (mainView == null)
        {
            return;
        } else
        {
            int j = mainView.getScrollX();
            mScroller.startScroll(j, mainView.getScrollY(), i, mainView.getScrollY(), 500);
            invalidate();
            return;
        }
    }

    public void computeScroll()
    {
        while (mainView == null || mScroller.isFinished() || !mScroller.computeScrollOffset()) 
        {
            return;
        }
        int i = mainView.getScrollX();
        int j = mainView.getScrollY();
        int k = mScroller.getCurrX();
        int l = mScroller.getCurrY();
        if (i != k || j != l)
        {
            mainView.scrollTo(k, l);
            changeAlpha();
            if (mainView.getScrollX() < -screenWidth + 10)
            {
                finish();
            }
        }
        postInvalidate();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        float f;
        float f1;
        int i;
        if (!canSlide)
        {
            break MISSING_BLOCK_LABEL_137;
        }
        i = motionevent.getAction();
        f1 = motionevent.getX();
        f = motionevent.getY();
        i;
        JVM INSTR tableswitch 0 2: default 52
    //                   0 64
    //                   1 52
    //                   2 82;
           goto _L1 _L2 _L1 _L3
_L1:
        break; /* Loop/switch isn't completed */
_L3:
        break MISSING_BLOCK_LABEL_82;
_L4:
        if (mIsBeingDragged)
        {
            return mIsBeingDragged;
        } else
        {
            return super.onInterceptTouchEvent(motionevent);
        }
_L2:
        mLastMotionX = f1;
        mLastMotionY = f;
        mIsBeingDragged = false;
          goto _L4
        f1 = Math.abs(f1 - mLastMotionX);
        f = Math.abs(f - mLastMotionY);
        if (f1 > (float)mTouchSlop && f1 > f)
        {
            mIsBeingDragged = true;
            isFirst = true;
        }
          goto _L4
        return super.onInterceptTouchEvent(motionevent);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        float f;
        float f1;
        float f2;
        int i;
        f1 = 0.0F;
        if (mVelocityTracker == null)
        {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(motionevent);
        i = motionevent.getAction();
        f = motionevent.getX();
        f2 = motionevent.getY();
        if (isFirst)
        {
            isFirst = false;
            if (mSlideListener != null)
            {
                mSlideListener.slideStart();
            }
        }
        i;
        JVM INSTR tableswitch 0 3: default 100
    //                   0 102
    //                   1 248
    //                   2 133
    //                   3 248;
           goto _L1 _L2 _L3 _L4 _L3
_L1:
        return true;
_L2:
        if (!mScroller.isFinished())
        {
            mScroller.abortAnimation();
        }
        mLastMotionX = f;
        mLastMotionY = f2;
        continue; /* Loop/switch isn't completed */
_L4:
        if (mainView != null)
        {
            float f5 = mLastMotionX - f;
            mLastMotionX = f;
            float f6 = mainView.getScrollX();
            float f3 = f6 + f5;
            f = f3;
            if (f3 > 0.0F)
            {
                f = 0.0F;
            }
            if (f5 < 0.0F && f6 < 0.0F)
            {
                float f4 = -screenWidth;
                if (f > 0.0F)
                {
                    f = f1;
                } else
                if (f < f4)
                {
                    f = f4;
                }
            }
            mainView.scrollTo((int)f, mainView.getScrollY());
            changeAlpha();
        }
        continue; /* Loop/switch isn't completed */
_L3:
        int j;
        if (mainView == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (mSlideListener != null)
        {
            mSlideListener.slideEnd();
        }
        motionevent = mVelocityTracker;
        motionevent.computeCurrentVelocity(100);
        f = motionevent.getXVelocity();
        j = mainView.getScrollX();
        if (j > 0)
        {
            break MISSING_BLOCK_LABEL_397;
        }
        if (f <= 50F)
        {
            break; /* Loop/switch isn't completed */
        }
        i = -screenWidth - j;
_L6:
        if (j > 0)
        {
            i = -j;
        }
        smoothScrollTo(i);
        if (true) goto _L1; else goto _L5
_L5:
        if (f < -50F)
        {
            i = -j;
        } else
        if (j < -screenWidth / 2)
        {
            i = -screenWidth - j;
        } else
        {
            if (j < -screenWidth / 2)
            {
                break MISSING_BLOCK_LABEL_397;
            }
            i = -j;
        }
          goto _L6
        i = 0;
          goto _L6
    }

    public void setOnFinishListener(OnFinishListener onfinishlistener)
    {
        mFinishListener = onfinishlistener;
    }

    public void setOnSlideListener(SlideListener slidelistener)
    {
        mSlideListener = slidelistener;
    }

    public void setSlide(boolean flag)
    {
        canSlide = flag;
    }


/*
    static View access$002(SlideRightOutView sliderightoutview, View view)
    {
        sliderightoutview.mainView = view;
        return view;
    }

*/
}
