// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.viewpagerindicator;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewConfigurationCompat;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.view.viewpagerindicator:
//            PageIndicator

public class CirclePageIndicator extends View
    implements PageIndicator
{

    private static final int INVALID_POINTER = -1;
    private int mActivePointerId;
    private boolean mCentered;
    private int mCurrentPage;
    private float mExtraSpacing;
    private boolean mIsDragging;
    private float mLastMotionX;
    private int mLastPage;
    private android.support.v4.view.ViewPager.OnPageChangeListener mListener;
    private int mOrientation;
    private float mPageOffset;
    private final Paint mPaintFill;
    private final Paint mPaintPageFill;
    private final Paint mPaintStroke;
    private float mRadius;
    private int mScrollState;
    private boolean mSnap;
    private int mSnapPage;
    private int mTouchSlop;
    private ViewPager mViewPager;
    private int realcount;

    public CirclePageIndicator(Context context)
    {
        this(context, null);
    }

    public CirclePageIndicator(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0x7f010084);
    }

    public CirclePageIndicator(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mPaintPageFill = new Paint(1);
        mPaintStroke = new Paint(1);
        mPaintFill = new Paint(1);
        mLastMotionX = -1F;
        mActivePointerId = -1;
        realcount = -1;
        if (isInEditMode())
        {
            return;
        }
        Object obj = getResources();
        int j = ((Resources) (obj)).getColor(0x7f0700e5);
        int k = ((Resources) (obj)).getColor(0x7f0700e4);
        int l = ((Resources) (obj)).getInteger(0x7f0d0003);
        int i1 = ((Resources) (obj)).getColor(0x7f0700e6);
        float f = ((Resources) (obj)).getDimension(0x7f080043);
        float f1 = ((Resources) (obj)).getDimension(0x7f080042);
        float f2 = ((Resources) (obj)).getDimension(0x7f080041);
        boolean flag = ((Resources) (obj)).getBoolean(0x7f0e0000);
        boolean flag1 = ((Resources) (obj)).getBoolean(0x7f0e0001);
        attributeset = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.R.styleable.CirclePageIndicator, i, 0);
        mCentered = attributeset.getBoolean(2, flag);
        mOrientation = attributeset.getInt(0, l);
        mPaintPageFill.setStyle(android.graphics.Paint.Style.FILL);
        mPaintPageFill.setColor(attributeset.getColor(5, j));
        mPaintStroke.setStyle(android.graphics.Paint.Style.STROKE);
        mPaintStroke.setColor(attributeset.getColor(8, i1));
        mPaintStroke.setStrokeWidth(attributeset.getDimension(3, f1));
        mPaintFill.setStyle(android.graphics.Paint.Style.FILL);
        mPaintFill.setColor(attributeset.getColor(4, k));
        mRadius = attributeset.getDimension(6, f2);
        mSnap = attributeset.getBoolean(7, flag1);
        mExtraSpacing = attributeset.getDimension(9, f);
        obj = attributeset.getDrawable(1);
        if (obj != null)
        {
            setBackgroundDrawable(((android.graphics.drawable.Drawable) (obj)));
        }
        attributeset.recycle();
        mTouchSlop = ViewConfigurationCompat.getScaledPagingTouchSlop(ViewConfiguration.get(context));
    }

    private int measureLong(int i)
    {
        int l = android.view.View.MeasureSpec.getMode(i);
        int j = android.view.View.MeasureSpec.getSize(i);
        if (l == 0x40000000 || mViewPager == null)
        {
            i = j;
        } else
        {
            float f;
            float f1;
            float f2;
            int k;
            if (realcount > 0)
            {
                i = realcount;
            } else
            {
                i = mViewPager.getAdapter().getCount();
            }
            f = getPaddingLeft() + getPaddingRight();
            f1 = i * 2;
            f2 = mRadius;
            k = (int)((float)(i - 1) * (mRadius + mExtraSpacing) + (f + f1 * f2) + 1.0F);
            i = k;
            if (l == 0x80000000)
            {
                return Math.min(k, j);
            }
        }
        return i;
    }

    private int measureShort(int i)
    {
        int j = android.view.View.MeasureSpec.getMode(i);
        i = android.view.View.MeasureSpec.getSize(i);
        if (j == 0x40000000)
        {
            return i;
        }
        int k = (int)(2.0F * mRadius + (float)getPaddingTop() + (float)getPaddingBottom() + 1.0F);
        if (j == 0x80000000)
        {
            return Math.min(k, i);
        } else
        {
            return k;
        }
    }

    public int getFillColor()
    {
        return mPaintFill.getColor();
    }

    public int getOrientation()
    {
        return mOrientation;
    }

    public int getPageColor()
    {
        return mPaintPageFill.getColor();
    }

    public float getRadius()
    {
        return mRadius;
    }

    public int getStrokeColor()
    {
        return mPaintStroke.getColor();
    }

    public float getStrokeWidth()
    {
        return mPaintStroke.getStrokeWidth();
    }

    public boolean isCentered()
    {
        return mCentered;
    }

    public boolean isSnap()
    {
        return mSnap;
    }

    public void notifyDataSetChanged()
    {
        invalidate();
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if (mViewPager != null)
        {
            int i;
            if (realcount > 0)
            {
                i = realcount;
            } else
            {
                i = mViewPager.getAdapter().getCount();
            }
            if (i != 0)
            {
                if (mCurrentPage >= i)
                {
                    setCurrentItem(i - 1);
                    return;
                }
                float f;
                float f1;
                float f3;
                float f4;
                float f6;
                int j;
                int k;
                int l;
                int i1;
                if (mOrientation == 0)
                {
                    i1 = getWidth();
                    l = getPaddingLeft();
                    k = getPaddingRight();
                    j = getPaddingTop();
                } else
                {
                    i1 = getHeight();
                    l = getPaddingTop();
                    k = getPaddingBottom();
                    j = getPaddingLeft();
                }
                f6 = mRadius * 3F + mExtraSpacing;
                f = j;
                f = mRadius + f;
                f1 = (float)l + mRadius;
                if (mCentered)
                {
                    float f2 = mRadius * 2.0F;
                    f1 = f2;
                    if (i > 1)
                    {
                        f1 = f2 + (float)(i - 1) * f6;
                    }
                    f2 = (float)(i1 - l - k) / 2.0F - f1 / 2.0F;
                    f1 = f2;
                    if (i > 1)
                    {
                        f1 = f2 + mRadius;
                    }
                }
                f4 = mRadius;
                f3 = f4;
                if (mPaintStroke.getStrokeWidth() > 0.0F)
                {
                    f3 = f4 - mPaintStroke.getStrokeWidth() / 2.0F;
                }
                j = 0;
                while (j < i) 
                {
                    f4 = (float)j * f6 + f1;
                    float f5;
                    if (mOrientation == 0)
                    {
                        f5 = f4;
                        f4 = f;
                    } else
                    {
                        f5 = f;
                    }
                    if (mPaintPageFill.getAlpha() > 0)
                    {
                        canvas.drawCircle(f5, f4, f3, mPaintPageFill);
                    }
                    if (f3 != mRadius)
                    {
                        canvas.drawCircle(f5, f4, mRadius, mPaintStroke);
                    }
                    j++;
                }
                if (realcount > 0 && mLastPage == realcount - 1 && mCurrentPage == realcount - 1)
                {
                    canvas.drawCircle(f1 + (float)(i - 1) * f6, f, mRadius, mPaintFill);
                    return;
                }
                if (realcount > 0 && mLastPage == 0 && mCurrentPage == realcount - 1)
                {
                    canvas.drawCircle(f1, f, mRadius, mPaintFill);
                    return;
                }
                if (mSnap)
                {
                    i = mSnapPage;
                } else
                {
                    i = mCurrentPage;
                }
                f4 = (float)i * f6;
                f3 = f4;
                if (!mSnap)
                {
                    f3 = f4 + mPageOffset * f6;
                }
                if (mOrientation == 0)
                {
                    f3 = f1 + f3;
                    f1 = f;
                    f = f3;
                } else
                {
                    f1 += f3;
                }
                canvas.drawCircle(f, f1, mRadius, mPaintFill);
                return;
            }
        }
    }

    protected void onMeasure(int i, int j)
    {
        if (mOrientation == 0)
        {
            setMeasuredDimension(measureLong(i), measureShort(j));
            return;
        } else
        {
            setMeasuredDimension(measureShort(i), measureLong(j));
            return;
        }
    }

    public void onPageScrollStateChanged(int i)
    {
        mScrollState = i;
        if (mListener != null)
        {
            mListener.onPageScrollStateChanged(i);
        }
    }

    public void onPageScrolled(int i, float f, int j)
    {
        if (realcount > 0)
        {
            mCurrentPage = i % realcount;
        } else
        {
            mCurrentPage = i;
        }
        mPageOffset = f;
        invalidate();
        if (mListener != null)
        {
            mListener.onPageScrolled(i, f, j);
        }
    }

    public void onPageSelected(int i)
    {
        if (realcount > 0)
        {
            mLastPage = i % realcount;
        }
        if (mSnap || mScrollState == 0)
        {
            if (realcount > 0)
            {
                mCurrentPage = i % realcount;
                mSnapPage = i % realcount;
            } else
            {
                mCurrentPage = i;
                mSnapPage = i;
            }
            invalidate();
        }
        if (mListener != null)
        {
            mListener.onPageSelected(i);
        }
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (!super.onTouchEvent(motionevent)) goto _L2; else goto _L1
_L1:
        return true;
_L2:
        int l;
        if (mViewPager == null || mViewPager.getAdapter().getCount() == 0)
        {
            return false;
        }
        if (realcount == 0)
        {
            return false;
        }
        l = motionevent.getAction() & 0xff;
        l;
        JVM INSTR tableswitch 0 6: default 96
    //                   0 98
    //                   1 204
    //                   2 117
    //                   3 204
    //                   4 96
    //                   5 391
    //                   6 419;
           goto _L3 _L4 _L5 _L6 _L5 _L3 _L7 _L8
_L3:
        return true;
_L4:
        mActivePointerId = MotionEventCompat.getPointerId(motionevent, 0);
        mLastMotionX = motionevent.getX();
        return true;
_L6:
        float f = MotionEventCompat.getX(motionevent, MotionEventCompat.findPointerIndex(motionevent, mActivePointerId));
        float f2 = f - mLastMotionX;
        if (!mIsDragging && Math.abs(f2) > (float)mTouchSlop)
        {
            mIsDragging = true;
        }
        if (mIsDragging)
        {
            mLastMotionX = f;
            if (mViewPager.beginFakeDrag() || mViewPager.isFakeDragging())
            {
                mViewPager.fakeDragBy(f2);
                return true;
            }
        }
        continue; /* Loop/switch isn't completed */
_L5:
        if (mIsDragging)
        {
            break; /* Loop/switch isn't completed */
        }
        float f1;
        float f3;
        int i;
        int i1;
        if (realcount > 0)
        {
            i = realcount;
        } else
        {
            i = mViewPager.getAdapter().getCount();
        }
        i1 = getWidth();
        f1 = (float)i1 / 2.0F;
        f3 = (float)i1 / 6F;
        if (mCurrentPage > 0 && motionevent.getX() < f1 - f3)
        {
            if (l != 3 && realcount < 0)
            {
                mViewPager.setCurrentItem(mCurrentPage - 1);
                return true;
            }
            continue; /* Loop/switch isn't completed */
        }
        if (mCurrentPage >= i - 1 || motionevent.getX() <= f3 + f1)
        {
            break; /* Loop/switch isn't completed */
        }
        if (l != 3 && realcount < 0)
        {
            mViewPager.setCurrentItem(mCurrentPage + 1);
            return true;
        }
        if (true) goto _L1; else goto _L9
_L9:
        mIsDragging = false;
        mActivePointerId = -1;
        if (mViewPager.isFakeDragging())
        {
            try
            {
                mViewPager.endFakeDrag();
            }
            // Misplaced declaration of an exception variable
            catch (MotionEvent motionevent)
            {
                Logger.e(motionevent);
                return true;
            }
            return true;
        }
          goto _L1
_L7:
        int j = MotionEventCompat.getActionIndex(motionevent);
        mLastMotionX = MotionEventCompat.getX(motionevent, j);
        mActivePointerId = MotionEventCompat.getPointerId(motionevent, j);
        return true;
_L8:
        int k = MotionEventCompat.getActionIndex(motionevent);
        if (MotionEventCompat.getPointerId(motionevent, k) == mActivePointerId)
        {
            if (k == 0)
            {
                k = 1;
            } else
            {
                k = 0;
            }
            mActivePointerId = MotionEventCompat.getPointerId(motionevent, k);
        }
        mLastMotionX = MotionEventCompat.getX(motionevent, MotionEventCompat.findPointerIndex(motionevent, mActivePointerId));
        return true;
    }

    public void setCentered(boolean flag)
    {
        mCentered = flag;
        invalidate();
    }

    public void setCurrentItem(int i)
    {
        if (mViewPager == null)
        {
            throw new IllegalStateException("ViewPager has not been bound.");
        } else
        {
            mViewPager.setCurrentItem(i);
            mCurrentPage = i;
            invalidate();
            return;
        }
    }

    public void setExtraSpacing(float f)
    {
        mExtraSpacing = f;
    }

    public void setFillColor(int i)
    {
        mPaintFill.setColor(i);
        invalidate();
    }

    public void setOnPageChangeListener(android.support.v4.view.ViewPager.OnPageChangeListener onpagechangelistener)
    {
        mListener = onpagechangelistener;
    }

    public void setOrientation(int i)
    {
        switch (i)
        {
        default:
            throw new IllegalArgumentException("Orientation must be either HORIZONTAL or VERTICAL.");

        case 0: // '\0'
        case 1: // '\001'
            mOrientation = i;
            break;
        }
        requestLayout();
    }

    public void setPageColor(int i)
    {
        mPaintPageFill.setColor(i);
        invalidate();
    }

    public void setPagerRealCount(int i)
    {
        realcount = i;
    }

    public void setRadius(float f)
    {
        mRadius = f;
        invalidate();
    }

    public void setSnap(boolean flag)
    {
        mSnap = flag;
        invalidate();
    }

    public void setStrokeColor(int i)
    {
        mPaintStroke.setColor(i);
        invalidate();
    }

    public void setStrokeWidth(float f)
    {
        mPaintStroke.setStrokeWidth(f);
        invalidate();
    }

    public void setViewPager(ViewPager viewpager)
    {
        if (mViewPager == viewpager)
        {
            return;
        }
        if (mViewPager != null)
        {
            mViewPager.setOnPageChangeListener(null);
        }
        if (viewpager.getAdapter() == null)
        {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        } else
        {
            mViewPager = viewpager;
            mViewPager.setOnPageChangeListener(this);
            invalidate();
            return;
        }
    }

    public void setViewPager(ViewPager viewpager, int i)
    {
        setViewPager(viewpager);
        setCurrentItem(i);
    }
}
