// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import com.nineoldandroids.animation.ValueAnimator;

// Referenced classes of package com.ximalaya.ting.android.view:
//            i, j, k, l

public class RotateAnimatorImageView extends ImageView
{

    private float mDegree;
    private Drawable mEndImage;
    private int mFromImageRes;
    private boolean mIsAnimating;
    private ValueAnimator mRotateAnimator;
    private Drawable mRotateImage;
    private int mToImageRes;

    public RotateAnimatorImageView(Context context)
    {
        super(context);
    }

    public RotateAnimatorImageView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    public RotateAnimatorImageView(Context context, AttributeSet attributeset, int i1)
    {
        super(context, attributeset, i1);
    }

    public void animateTo(int i1, int j1, float f)
    {
        Drawable drawable;
        Drawable drawable1;
        mFromImageRes = i1;
        mToImageRes = j1;
        drawable = getDrawable();
        drawable1 = getContext().getResources().getDrawable(j1);
        if (drawable == null || drawable1 == null || !drawable.getConstantState().equals(drawable1.getConstantState())) goto _L2; else goto _L1
_L1:
        return;
_L2:
        mIsAnimating = true;
        setImageResource(i1);
        if (mRotateAnimator == null || !mRotateAnimator.isRunning())
        {
            break; /* Loop/switch isn't completed */
        }
        if (mFromImageRes == i1 && mToImageRes == j1)
        {
            continue; /* Loop/switch isn't completed */
        }
        mRotateAnimator.cancel();
        break; /* Loop/switch isn't completed */
        if (true) goto _L1; else goto _L3
_L3:
        mRotateAnimator = ValueAnimator.ofFloat(new float[] {
            0.0F, f
        }).setDuration(500L);
        mRotateAnimator.addUpdateListener(new i(this));
        mRotateAnimator.addListener(new j(this, j1));
        mRotateAnimator.start();
        return;
    }

    public void end()
    {
        if (mRotateAnimator != null && mRotateAnimator.isRunning())
        {
            mRotateAnimator.end();
        }
    }

    public boolean isRotating()
    {
        return mRotateAnimator != null && mRotateAnimator.isRunning();
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
        if (mIsAnimating)
        {
            canvas.save();
            canvas.rotate(mDegree, getWidth() >> 1, getHeight() >> 1);
            mRotateImage.draw(canvas);
            canvas.restore();
        }
    }

    public void setEndImage(int i1)
    {
        mEndImage = getContext().getResources().getDrawable(i1);
    }

    public void setRotateImage(int i1)
    {
        mRotateImage = getContext().getResources().getDrawable(i1);
        i1 = (getWidth() - mRotateImage.getIntrinsicWidth()) / 2;
        int j1 = (getHeight() - mRotateImage.getIntrinsicHeight()) / 2;
        mRotateImage.setBounds(i1, j1, mRotateImage.getIntrinsicWidth() + i1, mRotateImage.getIntrinsicHeight() + j1);
    }

    public void startRotate(int i1)
    {
        if (mRotateAnimator != null && mRotateAnimator.isRunning())
        {
            mRotateAnimator.cancel();
        }
        mIsAnimating = true;
        mRotateAnimator = ValueAnimator.ofFloat(new float[] {
            0.0F, 360F
        }).setDuration(500L);
        mRotateAnimator.addUpdateListener(new k(this));
        mRotateAnimator.addListener(new l(this, i1));
        mRotateAnimator.setRepeatMode(1);
        mRotateAnimator.setRepeatCount(-1);
        mRotateAnimator.setInterpolator(new LinearInterpolator());
        mRotateAnimator.start();
    }


/*
    static float access$002(RotateAnimatorImageView rotateanimatorimageview, float f)
    {
        rotateanimatorimageview.mDegree = f;
        return f;
    }

*/


/*
    static boolean access$102(RotateAnimatorImageView rotateanimatorimageview, boolean flag)
    {
        rotateanimatorimageview.mIsAnimating = flag;
        return flag;
    }

*/

}
