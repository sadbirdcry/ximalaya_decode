// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.dial;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import com.ximalaya.ting.android.util.Logger;

public class CircleProgressBar extends View
{
    public static interface DialListener
    {

        public abstract void onValueAt(float f);
    }


    public static final int FILL = 1;
    private static final float PI = 3.14F;
    public static final int STROKE = 0;
    private static final String TAG = com/ximalaya/ting/android/view/dial/CircleProgressBar.getSimpleName();
    private float mAngle;
    private float mCenterX;
    private float mCenterY;
    private Context mContext;
    private DialListener mDialListenerImp;
    private int mHeight;
    private boolean mIsStartSet;
    private float mLastX;
    private float mLastY;
    Matrix mMatrixPointer;
    Matrix mMatrixScale;
    private Bitmap mPointerBm;
    private Bitmap mPointerBm2;
    private Bitmap mScaleBm;
    private Bitmap mScaleBm2;
    private VelocityTracker mVelocityTracker;
    private int mWidth;
    private float max;
    private float min;
    private Paint paint;
    private float progress;
    private int roundColor;
    private int roundProgressColor;
    private float roundWidth;
    private int style;
    private int textColor;
    private boolean textIsDisplayable;
    private float textSize;

    public CircleProgressBar(Context context)
    {
        this(context, null);
        mContext = context;
    }

    public CircleProgressBar(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
        mContext = context;
    }

    public CircleProgressBar(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mMatrixPointer = new Matrix();
        mMatrixScale = new Matrix();
        mIsStartSet = false;
        mContext = context;
        paint = new Paint();
        mPointerBm = BitmapFactory.decodeResource(getResources(), 0x7f02043e);
        mScaleBm = BitmapFactory.decodeResource(getResources(), 0x7f0204f9);
        context = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.R.styleable.CircleProgressBar);
        roundColor = context.getColor(8, 0xffff0000);
        roundProgressColor = context.getColor(9, 0xff00ff00);
        textColor = context.getColor(11, 0xff00ff00);
        textSize = context.getDimension(12, 15F);
        roundWidth = context.getDimension(10, 5F);
        max = context.getFloat(13, 100F);
        min = context.getFloat(14, 0.0F);
        textIsDisplayable = context.getBoolean(15, true);
        style = context.getInt(16, 0);
        context.recycle();
    }

    private void calculateAngel(float f, float f1)
    {
        if (isValidEvent(f, f1))
        {
            f = (float)(Math.atan2(f1 - mCenterY, f - mCenterX) - Math.atan2(mLastY - mCenterY, mLastX - mCenterX));
            f1 = mAngle;
            mAngle = (f * 360F) / 6.28F + f1;
            if (mAngle < 0.0F)
            {
                mAngle = mAngle + 360F;
            }
            if (mAngle >= 360F)
            {
                mAngle = mAngle - 360F;
            }
            if (mDialListenerImp != null)
            {
                mDialListenerImp.onValueAt(getRateByAngle(mAngle));
                return;
            }
        }
    }

    private void createPointerBm2()
    {
        Logger.d("WIFI", (new StringBuilder()).append("createPointerBm2:width=").append(getWidth()).toString());
        mPointerBm2 = zoomImage(mPointerBm, getWidth() / 2, getWidth() / 2);
    }

    private void createScaleBm2()
    {
        Logger.d("WIFI", (new StringBuilder()).append("createScaleBm2:width=").append(getWidth()).toString());
        mScaleBm2 = zoomImage(mScaleBm, getWidth(), getWidth());
    }

    private int dip2px(Context context, float f)
    {
        return (int)(context.getResources().getDisplayMetrics().density * f + 0.5F);
    }

    private float getRateByAngle(float f)
    {
        return (float)(int)((((max - min) / 360F) * f + min) * 10F) / 10F;
    }

    private boolean isValidEvent(float f, float f1)
    {
        return true;
    }

    public static Bitmap zoomImage(Bitmap bitmap, double d, double d1)
    {
        float f = bitmap.getWidth();
        float f1 = bitmap.getHeight();
        Matrix matrix = new Matrix();
        matrix.postScale((float)d / f, (float)d1 / f1);
        return Bitmap.createBitmap(bitmap, 0, 0, (int)f, (int)f1, matrix, true);
    }

    public int getCricleColor()
    {
        return roundColor;
    }

    public int getCricleProgressColor()
    {
        return roundProgressColor;
    }

    public float getMax()
    {
        this;
        JVM INSTR monitorenter ;
        float f = max;
        this;
        JVM INSTR monitorexit ;
        return f;
        Exception exception;
        exception;
        throw exception;
    }

    public float getProgress()
    {
        this;
        JVM INSTR monitorenter ;
        float f = progress;
        this;
        JVM INSTR monitorexit ;
        return f;
        Exception exception;
        exception;
        throw exception;
    }

    public float getRoundWidth()
    {
        return roundWidth;
    }

    public int getTextColor()
    {
        return textColor;
    }

    public float getTextSize()
    {
        return textSize;
    }

    protected void onDraw(Canvas canvas)
    {
        RectF rectf;
        super.onDraw(canvas);
        mWidth = getWidth();
        mHeight = getHeight();
        mCenterX = (float)mWidth / 2.0F;
        mCenterY = (float)mHeight / 2.0F;
        mMatrixPointer.reset();
        mMatrixScale.reset();
        if (mScaleBm2 == null)
        {
            createScaleBm2();
        }
        canvas.drawBitmap(mScaleBm2, mMatrixScale, null);
        if (mPointerBm2 == null)
        {
            createPointerBm2();
        }
        Logger.d("WIFI", (new StringBuilder()).append("mWidth,mHeight,mPointerBm.getWidth(), mPointerBm.getHeight()").append(mWidth).append(",").append(mHeight).append(",").append(mPointerBm2.getWidth()).append(",").append(mPointerBm2.getHeight()).toString());
        float f = (float)Math.min(mWidth, mHeight) / (float)mPointerBm.getWidth() / 2.0F;
        mMatrixPointer.postTranslate((float)(mWidth - mPointerBm2.getWidth()) / 2.0F, (float)(mHeight - mPointerBm2.getHeight()) / 2.0F);
        mMatrixPointer.postRotate(mAngle, mWidth / 2, mWidth / 2);
        canvas.drawBitmap(mPointerBm2, mMatrixPointer, null);
        int i = getWidth() / 2;
        int j = (getWidth() * 2) / 5;
        paint.setColor(roundColor);
        paint.setStyle(android.graphics.Paint.Style.STROKE);
        paint.setStrokeWidth(roundWidth);
        paint.setAntiAlias(true);
        canvas.drawCircle(i, i, j, paint);
        Logger.d("log", (new StringBuilder()).append(i).append("").toString());
        paint.setStrokeWidth(0.0F);
        paint.setColor(textColor);
        int k = dip2px(mContext, textSize);
        paint.setTextSize(k);
        paint.setTypeface(Typeface.DEFAULT_BOLD);
        k = (int)((progress / max) * 100F);
        paint.measureText((new StringBuilder()).append(k).append("%").toString());
        if (textIsDisplayable && k != 0)
        {
            if (style != 0);
        }
        paint.setStrokeWidth(roundWidth);
        paint.setColor(roundProgressColor);
        rectf = new RectF(i - j, i - j, i + j, i + j);
        Logger.d(TAG, (new StringBuilder()).append("progress=").append(progress).toString());
        style;
        JVM INSTR tableswitch 0 1: default 596
    //                   0 597
    //                   1 625;
           goto _L1 _L2 _L3
_L1:
        return;
_L2:
        paint.setStyle(android.graphics.Paint.Style.STROKE);
        canvas.drawArc(rectf, -90F, mAngle, false, paint);
        return;
_L3:
        paint.setStyle(android.graphics.Paint.Style.FILL_AND_STROKE);
        if (progress != 0.0F)
        {
            canvas.drawArc(rectf, -90F, mAngle, true, paint);
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    protected void onMeasure(int i, int j)
    {
        super.onMeasure(i, i);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        boolean flag = true;
        if (mVelocityTracker == null)
        {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(motionevent);
        float f = motionevent.getX();
        float f1 = motionevent.getY();
        float f2 = mCenterX;
        float f3 = mPointerBm2.getWidth() / 2;
        float f4 = mCenterX;
        float f5 = mPointerBm2.getWidth() / 2;
        float f6 = mCenterY;
        float f7 = mPointerBm2.getHeight() / 2;
        float f8 = mCenterY;
        float f9 = mPointerBm2.getHeight() / 2;
        if (f >= f2 - f3 && f <= f4 + f5 && f1 >= f6 - f7 && f1 <= f8 + f9)
        {
            switch (motionevent.getAction())
            {
            default:
                flag = super.onTouchEvent(motionevent);
                // fall through

            case 1: // '\001'
                return flag;

            case 0: // '\0'
                mLastX = f;
                mLastY = f1;
                return true;

            case 2: // '\002'
                mVelocityTracker.computeCurrentVelocity(1000);
                break;
            }
            calculateAngel(motionevent.getX(), motionevent.getY());
            invalidate();
            mLastX = f;
            mLastY = f1;
            return true;
        } else
        {
            return super.onTouchEvent(motionevent);
        }
    }

    public void removeDialListener(DialListener diallistener)
    {
        mDialListenerImp = null;
    }

    public void setCricleColor(int i)
    {
        roundColor = i;
    }

    public void setCricleProgressColor(int i)
    {
        roundProgressColor = i;
    }

    public void setDialListener(DialListener diallistener)
    {
        mDialListenerImp = diallistener;
    }

    public void setMax(int i)
    {
        this;
        JVM INSTR monitorenter ;
        if (i >= 0)
        {
            break MISSING_BLOCK_LABEL_22;
        }
        throw new IllegalArgumentException("max not less than 0");
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
        float f = i;
        max = f;
        this;
        JVM INSTR monitorexit ;
    }

    public void setRoundWidth(float f)
    {
        roundWidth = f;
    }

    public void setTextColor(int i)
    {
        textColor = i;
    }

    public void setTextSize(float f)
    {
        textSize = f;
    }

    public void setValue(float f)
    {
        float f1 = 0.0F;
        this;
        JVM INSTR monitorenter ;
        Logger.d(TAG, "setValue");
        if (f < 0.0F)
        {
            f = f1;
        }
        f1 = f;
        if (f > max)
        {
            f1 = max;
        }
        if (f1 <= max)
        {
            progress = f1;
            mAngle = ((f1 - min) * 360F) / (max - min);
            postInvalidate();
        }
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

}
