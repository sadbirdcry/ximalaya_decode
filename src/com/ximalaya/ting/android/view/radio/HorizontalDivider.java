// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.radio;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

public class HorizontalDivider extends View
{

    private Paint mPaint;
    private Path mPath;

    public HorizontalDivider(Context context)
    {
        super(context);
        init();
    }

    public HorizontalDivider(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        init();
    }

    public HorizontalDivider(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        init();
    }

    private void init()
    {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setColor(Color.parseColor("#33ffffff"));
    }

    protected void onDraw(Canvas canvas)
    {
        canvas.drawLine(0.0F, 0.0F, (getWidth() >> 1) - getHeight(), 0.0F, mPaint);
        canvas.drawLine((getWidth() >> 1) - getHeight(), 0.0F, getWidth() >> 1, getHeight(), mPaint);
        canvas.drawLine(getWidth() >> 1, getHeight(), (getWidth() >> 1) + getHeight(), 0.0F, mPaint);
        canvas.drawLine((getWidth() >> 1) + getHeight(), 0.0F, getWidth(), 0.0F, mPaint);
    }
}
