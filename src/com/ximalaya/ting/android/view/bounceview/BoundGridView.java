// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.bounceview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.GridView;

// Referenced classes of package com.ximalaya.ting.android.view.bounceview:
//            BounceViewHelper

public class BoundGridView extends GridView
{

    private BounceViewHelper bounceViewHelper;

    public BoundGridView(Context context)
    {
        super(context);
        initBounceListView(context);
    }

    public BoundGridView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        initBounceListView(context);
    }

    public BoundGridView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        initBounceListView(context);
    }

    private void initBounceListView(Context context)
    {
        bounceViewHelper = new BounceViewHelper(context, this, BounceViewHelper.VERTICAL);
    }

    public boolean dispatchTouchEvent(MotionEvent motionevent)
    {
        bounceViewHelper.dispatchTouchEvent(motionevent);
        return super.dispatchTouchEvent(motionevent);
    }

    protected void onOverScrolled(int i, int j, boolean flag, boolean flag1)
    {
        bounceViewHelper.onOverScrolled(i, j, flag, flag1);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        bounceViewHelper.onTouchEvent(motionevent);
        return super.onTouchEvent(motionevent);
    }
}
