// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.bounceview;

import android.content.Context;
import android.view.View;
import android.widget.Scroller;

// Referenced classes of package com.ximalaya.ting.android.view.bounceview:
//            BounceHeadListView

private class cess._cls000
    implements Runnable
{

    final BounceHeadListView a;
    private final Scroller b;
    private View c;
    private int d;

    void a()
    {
        if (!b.isFinished())
        {
            b.abortAnimation();
        }
    }

    public void a(View view, int i)
    {
        if (i == 0)
        {
            return;
        } else
        {
            c = view;
            d = BounceHeadListView.access$200(a).getHeight();
            b.startScroll(0, d, 0, i, BounceHeadListView.access$400(a));
            c.post(this);
            return;
        }
    }

    boolean b()
    {
        return !b.isFinished();
    }

    public void run()
    {
        if (!b.isFinished()) goto _L2; else goto _L1
_L1:
        int i = BounceHeadListView.access$100(a) - BounceHeadListView.access$200(a).getHeight();
        if (Math.abs(i) <= 10) goto _L4; else goto _L3
_L3:
        BounceHeadListView.access$300(a).a(a, i);
_L6:
        return;
_L4:
        a.reSetHeadView();
        return;
_L2:
        boolean flag = b.computeScrollOffset();
        int j = b.getCurrY();
        int k = j - d;
        if (k != 0)
        {
            a.reLayoutHeadView(k);
            d = j;
        }
        if (Math.abs(Math.abs(b.getFinalY() - b.getCurrY()) - Math.abs(BounceHeadListView.access$100(a) - BounceHeadListView.access$200(a).getHeight())) > 10)
        {
            BounceHeadListView.access$300(a).a();
            BounceHeadListView.access$300(a).a(a, BounceHeadListView.access$100(a) - BounceHeadListView.access$200(a).getHeight());
        }
        if (flag)
        {
            c.post(this);
            return;
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    public (BounceHeadListView bounceheadlistview, Context context)
    {
        a = bounceheadlistview;
        super();
        d = 0;
        b = new Scroller(context, BounceHeadListView.access$000(bounceheadlistview));
    }
}
