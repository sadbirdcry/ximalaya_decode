// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.bounceview;

import android.content.Context;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Scroller;

// Referenced classes of package com.ximalaya.ting.android.view.bounceview:
//            BounceViewHelper

private class b
    implements Runnable
{

    final BounceViewHelper a;
    private final Scroller b;
    private View c;
    private int d;
    private int e;
    private boolean f;

    void a()
    {
        if (!b.isFinished())
        {
            b.abortAnimation();
        }
    }

    public void a(View view, int i)
    {
        if (i == 0)
        {
            return;
        }
        c = view;
        if (d == BounceViewHelper.VERTICAL)
        {
            b.startScroll(0, i, 0, i, 500);
        } else
        {
            b.startScroll(i, 0, i, 0, 500);
        }
        e = i;
        if (i < 0)
        {
            f = false;
        } else
        {
            f = true;
        }
        c.post(this);
    }

    boolean b()
    {
        return !b.isFinished();
    }

    public void run()
    {
label0:
        {
label1:
            {
                if (!b.isFinished())
                {
                    break label0;
                }
                c.scrollTo(0, 0);
                if (BounceViewHelper.access$000(a) != null)
                {
                    if (!f)
                    {
                        break label1;
                    }
                    BounceViewHelper.access$000(a).pullUp();
                }
                return;
            }
            BounceViewHelper.access$000(a).pullDown();
            return;
        }
        boolean flag = b.computeScrollOffset();
        if (d == BounceViewHelper.VERTICAL)
        {
            int i = b.getCurrY();
            int k = e - i;
            if (k != 0)
            {
                c.scrollBy(0, k);
                e = i;
            }
        } else
        {
            int j = b.getCurrX();
            int l = e - j;
            if (l != 0)
            {
                c.scrollBy(l, 0);
                e = j;
            }
        }
        if (flag)
        {
            c.post(this);
            return;
        } else
        {
            c.scrollTo(0, 0);
            return;
        }
    }

    public llFinishListener(BounceViewHelper bounceviewhelper, Context context, int i)
    {
        a = bounceviewhelper;
        super();
        e = 0;
        f = false;
        d = i;
        b = new Scroller(context, new OvershootInterpolator(0.75F));
    }
}
