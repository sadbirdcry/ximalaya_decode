// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.bounceview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import com.ximalaya.ting.android.view.listview.FixedListView;

// Referenced classes of package com.ximalaya.ting.android.view.bounceview:
//            BounceViewHelper

public class BounceListView extends FixedListView
{
    class a extends android.view.GestureDetector.SimpleOnGestureListener
    {

        final BounceListView a;

        public boolean onScroll(MotionEvent motionevent, MotionEvent motionevent1, float f, float f1)
        {
            return Math.abs(f1) >= Math.abs(f);
        }

        a()
        {
            a = BounceListView.this;
            super();
        }
    }


    private BounceViewHelper bounceViewHelper;
    private GestureDetector mGestureDetector;

    public BounceListView(Context context)
    {
        super(context);
        initBounceListView(context);
    }

    public BounceListView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        initBounceListView(context);
    }

    public BounceListView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        initBounceListView(context);
    }

    private void initBounceListView(Context context)
    {
        bounceViewHelper = new BounceViewHelper(context, this, BounceViewHelper.VERTICAL);
        mGestureDetector = new GestureDetector(getContext(), new a());
    }

    public boolean dispatchTouchEvent(MotionEvent motionevent)
    {
        bounceViewHelper.dispatchTouchEvent(motionevent);
        return super.dispatchTouchEvent(motionevent);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        boolean flag = super.onInterceptTouchEvent(motionevent);
        boolean flag1 = mGestureDetector.onTouchEvent(motionevent);
        return flag && flag1;
    }

    protected void onOverScrolled(int i, int j, boolean flag, boolean flag1)
    {
        bounceViewHelper.onOverScrolled(i, j, flag, flag1);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        bounceViewHelper.onTouchEvent(motionevent);
        return super.onTouchEvent(motionevent);
    }
}
