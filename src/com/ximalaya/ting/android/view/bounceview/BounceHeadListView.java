// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.bounceview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.view.listview.FixedListView;

// Referenced classes of package com.ximalaya.ting.android.view.bounceview:
//            BounceViewHelper

public class BounceHeadListView extends FixedListView
{
    public static interface OnCloneFloatVisibilityChangedCallback
    {

        public abstract void OnCloneFloatVisibilityChanged(View view, int i);
    }

    private class a
        implements Runnable
    {

        final BounceHeadListView a;
        private final Scroller b;
        private View c;
        private int d;

        void a()
        {
            if (!b.isFinished())
            {
                b.abortAnimation();
            }
        }

        public void a(View view, int i)
        {
            if (i == 0)
            {
                return;
            } else
            {
                c = view;
                d = a.headView.getHeight();
                b.startScroll(0, d, 0, i, a.duratime);
                c.post(this);
                return;
            }
        }

        boolean b()
        {
            return !b.isFinished();
        }

        public void run()
        {
            if (!b.isFinished()) goto _L2; else goto _L1
_L1:
            int i = a.originalHeight - a.headView.getHeight();
            if (Math.abs(i) <= 10) goto _L4; else goto _L3
_L3:
            a.flinger.a(a, i);
_L6:
            return;
_L4:
            a.reSetHeadView();
            return;
_L2:
            boolean flag = b.computeScrollOffset();
            int j = b.getCurrY();
            int k = j - d;
            if (k != 0)
            {
                a.reLayoutHeadView(k);
                d = j;
            }
            if (Math.abs(Math.abs(b.getFinalY() - b.getCurrY()) - Math.abs(a.originalHeight - a.headView.getHeight())) > 10)
            {
                a.flinger.a();
                a.flinger.a(a, a.originalHeight - a.headView.getHeight());
            }
            if (flag)
            {
                c.post(this);
                return;
            }
            if (true) goto _L6; else goto _L5
_L5:
        }

        public a(Context context)
        {
            a = BounceHeadListView.this;
            super();
            d = 0;
            b = new Scroller(context, interpolator);
        }
    }


    private BounceViewHelper bounceViewHelper;
    private OnCloneFloatVisibilityChangedCallback callback;
    private View cloneFloatView;
    private int delY;
    private int duratime;
    private a flinger;
    private View floatView;
    private View headView;
    private Interpolator interpolator;
    private boolean isTouchUp;
    private Context mContext;
    private int originalHeight;
    private float preY;

    public BounceHeadListView(Context context)
    {
        super(context);
        duratime = 800;
        isTouchUp = true;
        originalHeight = 0;
        initBounceListView(context);
    }

    public BounceHeadListView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        duratime = 800;
        isTouchUp = true;
        originalHeight = 0;
        initBounceListView(context);
    }

    public BounceHeadListView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        duratime = 800;
        isTouchUp = true;
        originalHeight = 0;
        initBounceListView(context);
    }

    private void hideCloneFloatView()
    {
        if (cloneFloatView != null && floatView != null)
        {
            floatView.setVisibility(0);
            cloneFloatView.setVisibility(8);
            if (callback != null)
            {
                callback.OnCloneFloatVisibilityChanged(cloneFloatView, 8);
            }
        }
    }

    private void initBounceListView(Context context)
    {
        mContext = context;
        bounceViewHelper = new BounceViewHelper(mContext, this, BounceViewHelper.VERTICAL);
        flinger = new a(mContext);
        interpolator = new LinearInterpolator();
    }

    public View addHeaderView(View view, int i)
    {
        super.addHeaderView(view);
        cloneFloatView = LayoutInflater.from(mContext).inflate(i, null, false);
        headView = view;
        floatView = view.findViewById(0x7f0a0448);
        if (floatView == null)
        {
            Logger.throwRuntimeException("\u6D6E\u5C42\u8DDF\u8282\u70B9id\u5FC5\u987B\u547D\u540D\u4E3A@+id/float_view");
        }
        ((ViewGroup)getParent()).addView(cloneFloatView);
        hideCloneFloatView();
        return cloneFloatView;
    }

    public View addHeaderView(View view, int i, RelativeLayout relativelayout)
    {
        super.addHeaderView(view);
        cloneFloatView = LayoutInflater.from(mContext).inflate(i, null, false);
        headView = view;
        floatView = view.findViewById(0x7f0a0448);
        if (floatView == null)
        {
            Logger.throwRuntimeException("\u6D6E\u5C42\u8DDF\u8282\u70B9id\u5FC5\u987B\u547D\u540D\u4E3A@+id/float_view");
        }
        view = new android.view.ViewGroup.LayoutParams(-1, -2);
        relativelayout.addView(cloneFloatView, view);
        hideCloneFloatView();
        return cloneFloatView;
    }

    public void addHeaderView(View view)
    {
        super.addHeaderView(view);
        headView = view;
    }

    public void addHeaderView(View view, boolean flag)
    {
        super.addHeaderView(view);
        if (flag)
        {
            headView = view;
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionevent)
    {
        bounceViewHelper.dispatchTouchEvent(motionevent);
        if (originalHeight == 0)
        {
            originalHeight = headView.getHeight();
        }
        motionevent.getAction();
        JVM INSTR tableswitch 0 0: default 48
    //                   0 54;
           goto _L1 _L2
_L1:
        return super.dispatchTouchEvent(motionevent);
_L2:
        isTouchUp = false;
        preY = motionevent.getY();
        if (true) goto _L1; else goto _L3
_L3:
    }

    protected void onOverScrolled(int i, int j, boolean flag, boolean flag1)
    {
label0:
        {
label1:
            {
                if (!flinger.b() && headView != null)
                {
                    if (delY <= 0)
                    {
                        break label0;
                    }
                    if (!isTouchUp)
                    {
                        break label1;
                    }
                    flinger.a();
                    flinger.a(this, originalHeight - headView.getHeight());
                }
                return;
            }
            reLayoutHeadView((delY * 2) / 3);
            return;
        }
        bounceViewHelper.onOverScrolled(i, j, flag, flag1);
    }

    protected void onScrollChanged(int i, int j, int k, int l)
    {
        if (cloneFloatView != null && floatView != null)
        {
            if ((headView.getHeight() + headView.getTop()) - floatView.getHeight() < 0)
            {
                showCloneFloatView();
            } else
            {
                hideCloneFloatView();
            }
        }
        super.onScrollChanged(i, j, k, l);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        float f;
        bounceViewHelper.onTouchEvent(motionevent);
        f = motionevent.getY();
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 48
    //                   0 54
    //                   1 93
    //                   2 74
    //                   3 93;
           goto _L1 _L2 _L3 _L4 _L3
_L1:
        return super.onTouchEvent(motionevent);
_L2:
        flinger.a();
        isTouchUp = false;
        preY = f;
        continue; /* Loop/switch isn't completed */
_L4:
        delY = (int)(f - preY);
        preY = f;
        continue; /* Loop/switch isn't completed */
_L3:
        isTouchUp = true;
        flinger.a();
        flinger.a(this, originalHeight - headView.getHeight());
        if (true) goto _L1; else goto _L5
_L5:
    }

    public void reLayoutHeadView(int i)
    {
        i = headView.getHeight() + i;
        if (i < originalHeight)
        {
            return;
        } else
        {
            android.view.ViewGroup.LayoutParams layoutparams = headView.getLayoutParams();
            layoutparams.height = i;
            headView.setLayoutParams(layoutparams);
            return;
        }
    }

    public void reSetHeadView()
    {
        android.view.ViewGroup.LayoutParams layoutparams = headView.getLayoutParams();
        layoutparams.height = originalHeight;
        headView.setLayoutParams(layoutparams);
    }

    public void setCloneFloatViewBackground(Drawable drawable)
    {
        if (cloneFloatView != null)
        {
            cloneFloatView.setBackgroundDrawable(drawable);
        }
    }

    public void setCloneFloatViewBackgroundDefualt()
    {
        if (cloneFloatView != null)
        {
            cloneFloatView.setBackgroundResource(0x7f070060);
        }
    }

    public void setDuratime(int i)
    {
        duratime = i;
    }

    public void setInterpolator(Interpolator interpolator1)
    {
        interpolator = interpolator1;
    }

    public void setOnCloneFloatViewVisibilityChangedCallback(OnCloneFloatVisibilityChangedCallback onclonefloatvisibilitychangedcallback)
    {
        callback = onclonefloatvisibilitychangedcallback;
    }

    public void setOnPullFinishListener(BounceViewHelper.PullFinishListener pullfinishlistener)
    {
        bounceViewHelper.setOnPullFinishListener(pullfinishlistener);
    }

    public void showCloneFloatView()
    {
        if (cloneFloatView != null && floatView != null)
        {
            floatView.setVisibility(8);
            cloneFloatView.setVisibility(0);
            if (callback != null)
            {
                callback.OnCloneFloatVisibilityChanged(cloneFloatView, 0);
            }
        }
    }





}
