// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.bounceview;

import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Scroller;

public class BounceViewHelper
{
    public static interface PullFinishListener
    {

        public abstract void pullDown();

        public abstract void pullUp();
    }

    private class a
        implements Runnable
    {

        final BounceViewHelper a;
        private final Scroller b;
        private View c;
        private int d;
        private int e;
        private boolean f;

        void a()
        {
            if (!b.isFinished())
            {
                b.abortAnimation();
            }
        }

        public void a(View view, int i)
        {
            if (i == 0)
            {
                return;
            }
            c = view;
            if (d == BounceViewHelper.VERTICAL)
            {
                b.startScroll(0, i, 0, i, 500);
            } else
            {
                b.startScroll(i, 0, i, 0, 500);
            }
            e = i;
            if (i < 0)
            {
                f = false;
            } else
            {
                f = true;
            }
            c.post(this);
        }

        boolean b()
        {
            return !b.isFinished();
        }

        public void run()
        {
label0:
            {
label1:
                {
                    if (!b.isFinished())
                    {
                        break label0;
                    }
                    c.scrollTo(0, 0);
                    if (a.mPullFinishListener != null)
                    {
                        if (!f)
                        {
                            break label1;
                        }
                        a.mPullFinishListener.pullUp();
                    }
                    return;
                }
                a.mPullFinishListener.pullDown();
                return;
            }
            boolean flag = b.computeScrollOffset();
            if (d == BounceViewHelper.VERTICAL)
            {
                int i = b.getCurrY();
                int k = e - i;
                if (k != 0)
                {
                    c.scrollBy(0, k);
                    e = i;
                }
            } else
            {
                int j = b.getCurrX();
                int l = e - j;
                if (l != 0)
                {
                    c.scrollBy(l, 0);
                    e = j;
                }
            }
            if (flag)
            {
                c.post(this);
                return;
            } else
            {
                c.scrollTo(0, 0);
                return;
            }
        }

        public a(Context context, int i)
        {
            a = BounceViewHelper.this;
            super();
            e = 0;
            f = false;
            d = i;
            b = new Scroller(context, new OvershootInterpolator(0.75F));
        }
    }


    public static int HORIZONTAL = 0;
    public static int VERTICAL = 0;
    public static final int bounceOffset = 6;
    private int delX;
    private int delY;
    private a flinger;
    private boolean isTouchUp;
    private Context mContext;
    private int mMaxOverscrollDistance;
    private PullFinishListener mPullFinishListener;
    private View mView;
    private int orientation;
    private float preX;
    private float preY;

    public BounceViewHelper(Context context, View view, int i)
    {
        mMaxOverscrollDistance = 400;
        isTouchUp = true;
        orientation = VERTICAL;
        mContext = context;
        mView = view;
        orientation = i;
        flinger = new a(mContext, i);
    }

    public void dispatchTouchEvent(MotionEvent motionevent)
    {
        switch (motionevent.getAction())
        {
        default:
            return;

        case 0: // '\0'
            isTouchUp = false;
            break;
        }
        preY = motionevent.getY();
        preX = motionevent.getX();
    }

    public void onOverScrolled(int i, int j, boolean flag, boolean flag1)
    {
        if (!flinger.b())
        {
            if (orientation == VERTICAL && Math.abs(mView.getScrollY()) < mMaxOverscrollDistance)
            {
                if (!isTouchUp)
                {
                    mView.scrollBy(0, delY / 6);
                }
                if (isTouchUp)
                {
                    flinger.a();
                    flinger.a(mView, mView.getScrollY());
                }
            } else
            if (orientation == HORIZONTAL && Math.abs(mView.getScrollX()) < mMaxOverscrollDistance)
            {
                if (!isTouchUp)
                {
                    mView.scrollBy(delX / 6, 0);
                }
                if (isTouchUp)
                {
                    flinger.a();
                    flinger.a(mView, mView.getScrollX());
                    return;
                }
            }
        }
    }

    public void onTouchEvent(MotionEvent motionevent)
    {
        float f;
        float f1;
        f = motionevent.getY();
        f1 = motionevent.getX();
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 44
    //                   0 45
    //                   1 99
    //                   2 66
    //                   3 99;
           goto _L1 _L2 _L3 _L4 _L3
_L1:
        return;
_L2:
        delY = 0;
        isTouchUp = false;
        preY = f;
        preX = f1;
        return;
_L4:
        delY = (int)(preY - f);
        preY = f;
        delX = (int)(preX - f1);
        preX = f1;
        return;
_L3:
        delY = 0;
        isTouchUp = true;
        flinger.a();
        if (orientation == VERTICAL && mView.getScrollY() != 0)
        {
            flinger.a(mView, mView.getScrollY());
            return;
        }
        if (orientation == HORIZONTAL && mView.getScrollX() != 0)
        {
            flinger.a(mView, mView.getScrollX());
            return;
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    public void setOnPullFinishListener(PullFinishListener pullfinishlistener)
    {
        mPullFinishListener = pullfinishlistener;
    }

    static 
    {
        HORIZONTAL = 0;
        VERTICAL = 1;
    }

}
