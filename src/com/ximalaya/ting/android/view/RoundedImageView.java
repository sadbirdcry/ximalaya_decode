// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

// Referenced classes of package com.ximalaya.ting.android.view:
//            RoundedDrawable

public class RoundedImageView extends ImageView
{

    public static final int DEFAULT_BORDER = 0;
    public static final int DEFAULT_BORDER_COLOR = 0xff000000;
    public static final int DEFAULT_RADIUS = 0;
    public static final String TAG = "RoundedImageView";
    private static final android.widget.ImageView.ScaleType sScaleTypeArray[];
    private boolean hasPressDownShade;
    private Drawable mBackgroundDrawable;
    private int mBorderBgColor;
    private int mBorderColor;
    private int mBorderWidth;
    private int mCornerRadius;
    private Drawable mDrawable;
    private boolean mIsCircle;
    private android.widget.ImageView.ScaleType mScaleType;
    private boolean roundBackground;

    public RoundedImageView(Context context)
    {
        super(context);
        hasPressDownShade = false;
        mCornerRadius = 0;
        mBorderWidth = 0;
        mBorderColor = 0xff000000;
    }

    public RoundedImageView(Context context, AttributeSet attributeset)
    {
        this(context, attributeset, 0);
    }

    public RoundedImageView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        hasPressDownShade = false;
        context = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.R.styleable.RoundedImageView, i, 0);
        i = context.getInt(0, -1);
        if (i >= 0)
        {
            setScaleType(sScaleTypeArray[i]);
        }
        hasPressDownShade = context.getBoolean(6, false);
        mCornerRadius = context.getDimensionPixelSize(1, -1);
        mBorderWidth = context.getDimensionPixelSize(2, -1);
        if (mCornerRadius < 0)
        {
            mCornerRadius = 0;
        }
        if (mBorderWidth < 0)
        {
            mBorderWidth = 0;
        }
        mBorderColor = context.getColor(3, 0xff000000);
        mBorderBgColor = context.getColor(4, 0xff000000);
        roundBackground = context.getBoolean(5, false);
        setBackgroundDrawable(getBackground());
        setImageDrawable(getDrawable());
        context.recycle();
    }

    public int getBorder()
    {
        return mBorderWidth;
    }

    public int getBorderColor()
    {
        return mBorderColor;
    }

    public int getCornerRadius()
    {
        return mCornerRadius;
    }

    public android.widget.ImageView.ScaleType getScaleType()
    {
        return mScaleType;
    }

    public boolean isRoundBackground()
    {
        return roundBackground;
    }

    protected void onDraw(Canvas canvas)
    {
        super.onDraw(canvas);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 36
    //                   0 51
    //                   1 120
    //                   2 36
    //                   3 120;
           goto _L1 _L2 _L3 _L1 _L3
_L1:
        break; /* Loop/switch isn't completed */
_L3:
        break MISSING_BLOCK_LABEL_120;
_L4:
        if (hasPressDownShade)
        {
            super.onTouchEvent(motionevent);
            return true;
        } else
        {
            return super.onTouchEvent(motionevent);
        }
_L2:
        if (hasPressDownShade)
        {
            if (mDrawable != null && (mDrawable instanceof RoundedDrawable))
            {
                ((RoundedDrawable)mDrawable).setPressDown(true);
            } else
            if (mBackgroundDrawable != null && (mBackgroundDrawable instanceof RoundedDrawable))
            {
                ((RoundedDrawable)mBackgroundDrawable).setPressDown(true);
            }
        }
          goto _L4
        if (hasPressDownShade)
        {
            if (mDrawable != null && (mDrawable instanceof RoundedDrawable))
            {
                ((RoundedDrawable)mDrawable).setPressDown(false);
            } else
            if (mBackgroundDrawable != null && (mBackgroundDrawable instanceof RoundedDrawable))
            {
                ((RoundedDrawable)mBackgroundDrawable).setPressDown(false);
            }
        }
          goto _L4
    }

    public void setBackground(Drawable drawable)
    {
        setBackgroundDrawable(drawable);
    }

    public void setBackgroundDrawable(Drawable drawable)
    {
        if (roundBackground && drawable != null)
        {
            mBackgroundDrawable = RoundedDrawable.fromDrawable(drawable, mCornerRadius, mBorderWidth, mBorderBgColor, mIsCircle);
            if (mDrawable instanceof RoundedDrawable)
            {
                ((RoundedDrawable)mBackgroundDrawable).setScaleType(mScaleType);
                ((RoundedDrawable)mBackgroundDrawable).setCornerRadius(mCornerRadius);
                ((RoundedDrawable)mBackgroundDrawable).setBorderWidth(mBorderWidth);
                ((RoundedDrawable)mBackgroundDrawable).setBorderColor(mBorderBgColor);
            }
        } else
        {
            mBackgroundDrawable = drawable;
        }
        super.setBackgroundDrawable(mBackgroundDrawable);
    }

    public void setBorderColor(int i)
    {
        if (mBorderColor != i)
        {
            mBorderColor = i;
            if (mDrawable instanceof RoundedDrawable)
            {
                ((RoundedDrawable)mDrawable).setBorderColor(i);
            }
            if (roundBackground && (mBackgroundDrawable instanceof RoundedDrawable))
            {
                ((RoundedDrawable)mBackgroundDrawable).setBorderColor(i);
            }
            if (mBorderWidth > 0)
            {
                invalidate();
                return;
            }
        }
    }

    public void setBorderWidth(int i)
    {
        if (mBorderWidth == i)
        {
            return;
        }
        mBorderWidth = i;
        if (mDrawable instanceof RoundedDrawable)
        {
            ((RoundedDrawable)mDrawable).setBorderWidth(i);
        }
        if (roundBackground && (mBackgroundDrawable instanceof RoundedDrawable))
        {
            ((RoundedDrawable)mBackgroundDrawable).setBorderWidth(i);
        }
        invalidate();
    }

    public void setCornerRadius(int i)
    {
        if (mCornerRadius != i)
        {
            mCornerRadius = i;
            if (mDrawable instanceof RoundedDrawable)
            {
                ((RoundedDrawable)mDrawable).setCornerRadius(i);
            }
            if (roundBackground && (mBackgroundDrawable instanceof RoundedDrawable))
            {
                ((RoundedDrawable)mBackgroundDrawable).setCornerRadius(i);
                return;
            }
        }
    }

    public void setHasPressDownShade(boolean flag)
    {
        hasPressDownShade = flag;
    }

    public void setImageBitmap(Bitmap bitmap)
    {
        if (bitmap != null)
        {
            mDrawable = new RoundedDrawable(bitmap, mCornerRadius, mBorderWidth, mBorderColor, mIsCircle);
            ((RoundedDrawable)mDrawable).setScaleType(mScaleType);
            ((RoundedDrawable)mDrawable).setCornerRadius(mCornerRadius);
            ((RoundedDrawable)mDrawable).setBorderWidth(mBorderWidth);
            ((RoundedDrawable)mDrawable).setBorderColor(mBorderColor);
        } else
        {
            mDrawable = null;
        }
        super.setImageDrawable(mDrawable);
    }

    public void setImageDrawable(Drawable drawable)
    {
        if (drawable != null)
        {
            mDrawable = RoundedDrawable.fromDrawable(drawable, mCornerRadius, mBorderWidth, mBorderColor, mIsCircle);
            if (mDrawable instanceof RoundedDrawable)
            {
                ((RoundedDrawable)mDrawable).setScaleType(mScaleType);
                ((RoundedDrawable)mDrawable).setCornerRadius(mCornerRadius);
                ((RoundedDrawable)mDrawable).setBorderWidth(mBorderWidth);
                ((RoundedDrawable)mDrawable).setBorderColor(mBorderColor);
            }
        } else
        {
            mDrawable = null;
        }
        super.setImageDrawable(mDrawable);
    }

    public void setImageResource(int i)
    {
        setImageDrawable(getResources().getDrawable(i));
    }

    public void setIsCircle(boolean flag)
    {
        mIsCircle = flag;
    }

    public void setRoundBackground(boolean flag)
    {
        if (roundBackground == flag)
        {
            return;
        }
        roundBackground = flag;
        if (!flag) goto _L2; else goto _L1
_L1:
        if (mBackgroundDrawable instanceof RoundedDrawable)
        {
            ((RoundedDrawable)mBackgroundDrawable).setScaleType(mScaleType);
            ((RoundedDrawable)mBackgroundDrawable).setCornerRadius(mCornerRadius);
            ((RoundedDrawable)mBackgroundDrawable).setBorderWidth(mBorderWidth);
            ((RoundedDrawable)mBackgroundDrawable).setBorderColor(mBorderBgColor);
        } else
        {
            setBackgroundDrawable(mBackgroundDrawable);
        }
_L4:
        invalidate();
        return;
_L2:
        if (mBackgroundDrawable instanceof RoundedDrawable)
        {
            ((RoundedDrawable)mBackgroundDrawable).setBorderWidth(0);
            ((RoundedDrawable)mBackgroundDrawable).setCornerRadius(0.0F);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void setScaleType(android.widget.ImageView.ScaleType scaletype)
    {
        if (scaletype == null)
        {
            throw new NullPointerException();
        }
        if (mScaleType == scaletype) goto _L2; else goto _L1
_L1:
        mScaleType = scaletype;
        static class _cls1
        {

            static final int a[];

            static 
            {
                a = new int[android.widget.ImageView.ScaleType.values().length];
                try
                {
                    a[android.widget.ImageView.ScaleType.CENTER.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror6) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.CENTER_CROP.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror5) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 3;
                }
                catch (NoSuchFieldError nosuchfielderror4) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.FIT_CENTER.ordinal()] = 4;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.FIT_START.ordinal()] = 5;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.FIT_END.ordinal()] = 6;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    a[android.widget.ImageView.ScaleType.FIT_XY.ordinal()] = 7;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls1.a[scaletype.ordinal()];
        JVM INSTR tableswitch 1 7: default 76
    //                   1 165
    //                   2 165
    //                   3 165
    //                   4 165
    //                   5 165
    //                   6 165
    //                   7 165;
           goto _L3 _L4 _L4 _L4 _L4 _L4 _L4 _L4
_L3:
        super.setScaleType(scaletype);
_L6:
        if ((mDrawable instanceof RoundedDrawable) && ((RoundedDrawable)mDrawable).getScaleType() != scaletype)
        {
            ((RoundedDrawable)mDrawable).setScaleType(scaletype);
        }
        if ((mBackgroundDrawable instanceof RoundedDrawable) && ((RoundedDrawable)mBackgroundDrawable).getScaleType() != scaletype)
        {
            ((RoundedDrawable)mBackgroundDrawable).setScaleType(scaletype);
        }
        setWillNotCacheDrawing(true);
        requestLayout();
        invalidate();
_L2:
        return;
_L4:
        super.setScaleType(android.widget.ImageView.ScaleType.FIT_XY);
        if (true) goto _L6; else goto _L5
_L5:
    }

    static 
    {
        sScaleTypeArray = (new android.widget.ImageView.ScaleType[] {
            android.widget.ImageView.ScaleType.MATRIX, android.widget.ImageView.ScaleType.FIT_XY, android.widget.ImageView.ScaleType.FIT_START, android.widget.ImageView.ScaleType.FIT_CENTER, android.widget.ImageView.ScaleType.FIT_END, android.widget.ImageView.ScaleType.CENTER, android.widget.ImageView.ScaleType.CENTER_CROP, android.widget.ImageView.ScaleType.CENTER_INSIDE
        });
    }
}
