// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

// Referenced classes of package com.ximalaya.ting.android.view:
//            p, q, r, s

public class VerificationCodeDialog extends Dialog
{
    public static interface ClickCallback
    {

        public abstract void onAffirmButtonClick(String s1);

        public abstract void onCancelButtonClick();
    }


    private String checkCodeUrl;
    private EditText et_verificationCode;
    private ImageView img_verificationCode;
    private ClickCallback mCallback;
    private Context mContext;
    private View txt_affirm;
    private View txt_cancel;
    private View txt_changeVerificationCode;
    private TextView txt_noticeWord;

    public VerificationCodeDialog(Context context, String s1, ClickCallback clickcallback)
    {
        super(context, 0x7f0b000d);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        checkCodeUrl = s1;
        mCallback = clickcallback;
        mContext = context;
    }

    private void findViews()
    {
        txt_changeVerificationCode = findViewById(0x7f0a01b2);
        txt_cancel = findViewById(0x7f0a01b3);
        txt_affirm = findViewById(0x7f0a01b4);
        txt_noticeWord = (TextView)findViewById(0x7f0a01af);
        img_verificationCode = (ImageView)findViewById(0x7f0a01b1);
        et_verificationCode = (EditText)findViewById(0x7f0a01b0);
    }

    private void initViews()
    {
        txt_noticeWord.setText(0x7f0901a7);
        txt_cancel.setOnClickListener(new p(this));
        txt_affirm.setOnClickListener(new q(this));
        txt_changeVerificationCode.setOnClickListener(new r(this));
        changeVerificationCode();
    }

    public void changeVerificationCode()
    {
        (new s(this)).myexec(new Void[0]);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f03006f);
        findViews();
        initViews();
    }





}
