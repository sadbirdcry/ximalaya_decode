// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

public class EntireRowCenterLayout extends ViewGroup
{

    private int mColumns;
    private int mHorizontalSpace;
    private int mVerticalSpace;

    public EntireRowCenterLayout(Context context)
    {
        super(context);
        mColumns = 1;
    }

    public EntireRowCenterLayout(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        mColumns = 1;
    }

    public EntireRowCenterLayout(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        mColumns = 1;
    }

    protected void onLayout(boolean flag, int i, int j, int k, int l)
    {
        int l1 = getChildCount();
        j = getPaddingTop();
        for (k = 0; k < l1;)
        {
            int i1 = 0;
            for (l = 0; i1 < mColumns; l = i)
            {
                i = l;
                if (i1 + k < l1)
                {
                    View view = getChildAt(i1 + k);
                    i = l;
                    if (view.getVisibility() != 8)
                    {
                        l += view.getMeasuredWidth();
                        i = l;
                        if (i1 < mColumns - 1)
                        {
                            i = l + mHorizontalSpace;
                        }
                    }
                }
                i1++;
            }

            i = getWidth();
            i1 = 0;
            int k1 = i - l >> 1;
            for (l = 0; l < mColumns;)
            {
                int j1 = i1;
                i = k1;
                if (l + k < l1)
                {
                    View view1 = getChildAt(l + k);
                    j1 = i1;
                    i = k1;
                    if (view1.getVisibility() != 8)
                    {
                        view1.layout(k1, j, view1.getMeasuredWidth() + k1, view1.getMeasuredHeight() + j);
                        k1 += view1.getMeasuredWidth();
                        i1 = view1.getMeasuredHeight();
                        j1 = i1;
                        i = k1;
                        if (l < mColumns - 1)
                        {
                            i = k1 + mHorizontalSpace;
                            j1 = i1;
                        }
                    }
                }
                l++;
                i1 = j1;
                k1 = i;
            }

            i = mVerticalSpace;
            k = mColumns + k;
            j += i + i1;
        }

    }

    protected void onMeasure(int i, int j)
    {
        int i1 = 0;
        int j1 = getChildCount();
        for (int k = 0; k < j1; k++)
        {
            measureChild(getChildAt(k), i, j);
        }

        int l;
        if (j1 % mColumns == 0)
        {
            l = j1 / mColumns;
        } else
        {
            l = j1 / mColumns + 1;
        }
        if (j1 > 0)
        {
            i1 = (getChildAt(0).getMeasuredHeight() + mVerticalSpace) * l + getPaddingTop() + getPaddingBottom();
        }
        setMeasuredDimension(resolveSize(getSuggestedMinimumWidth(), i), resolveSize(i1, j));
    }

    public void setColumns(int i)
    {
        mColumns = i;
    }

    public void setHorizontalSpace(int i)
    {
        mHorizontalSpace = i;
    }

    public void setVerticalSpace(int i)
    {
        mVerticalSpace = i;
    }
}
