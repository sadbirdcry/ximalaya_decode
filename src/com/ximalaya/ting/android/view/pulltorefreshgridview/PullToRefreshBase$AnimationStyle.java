// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview;

import android.content.Context;
import android.content.res.TypedArray;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.FlipLoadingLayout;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.LoadingLayout;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.RotateLoadingLayout;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview:
//            PullToRefreshBase

public static final class  extends Enum
{

    private static final FLIP $VALUES[];
    public static final FLIP FLIP;
    public static final FLIP ROTATE;

    static  getDefault()
    {
        return FLIP;
    }

    static FLIP mapIntToValue(int i)
    {
        switch (i)
        {
        default:
            return ROTATE;

        case 1: // '\001'
            return FLIP;
        }
    }

    public static FLIP valueOf(String s)
    {
        return (FLIP)Enum.valueOf(com/ximalaya/ting/android/view/pulltorefreshgridview/PullToRefreshBase$AnimationStyle, s);
    }

    public static FLIP[] values()
    {
        return (FLIP[])$VALUES.clone();
    }

    LoadingLayout createLoadingLayout(Context context, e_3B_.clone clone, e_3B_.clone clone1, TypedArray typedarray)
    {
        switch (e_3B_.clone[ordinal()])
        {
        default:
            return new RotateLoadingLayout(context, clone, clone1, typedarray);

        case 2: // '\002'
            return new FlipLoadingLayout(context, clone, clone1, typedarray);
        }
    }

    static 
    {
        ROTATE = new <init>("ROTATE", 0);
        FLIP = new <init>("FLIP", 1);
        $VALUES = (new .VALUES[] {
            ROTATE, FLIP
        });
    }

    private (String s, int i)
    {
        super(s, i);
    }
}
