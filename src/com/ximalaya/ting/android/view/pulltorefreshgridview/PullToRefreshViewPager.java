// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;
import com.nineoldandroids.view.ViewHelper;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.OverscrollHelper;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview:
//            PullToRefreshBase

public class PullToRefreshViewPager extends PullToRefreshBase
{
    final class a extends ViewPager
    {

        final PullToRefreshViewPager a;

        protected boolean overScrollBy(int i, int j, int k, int l, int i1, int j1, int k1, 
                int l1, boolean flag)
        {
            boolean flag1 = super.overScrollBy(i, j, k, l, i1, j1, k1, l1, flag);
            OverscrollHelper.overScrollBy(a, i, k, j, l, flag);
            View view = (View)((ViewPager)a.getRefreshableView()).getTag(0x7f0a0027);
            if (view != null)
            {
                ViewHelper.setTranslationY(view, (float)j + ViewHelper.getTranslationY(view));
            }
            return flag1;
        }

        public a(Context context, AttributeSet attributeset)
        {
            a = PullToRefreshViewPager.this;
            super(context, attributeset);
        }
    }


    public PullToRefreshViewPager(Context context)
    {
        super(context);
    }

    public PullToRefreshViewPager(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    protected ViewPager createRefreshableView(Context context, AttributeSet attributeset)
    {
        if (android.os.Build.VERSION.SDK_INT >= 9)
        {
            context = new a(context, attributeset);
        } else
        {
            context = new ViewPager(context, attributeset);
        }
        context.setId(0x7f0a0026);
        return context;
    }

    protected volatile View createRefreshableView(Context context, AttributeSet attributeset)
    {
        return createRefreshableView(context, attributeset);
    }

    public final PullToRefreshBase.Orientation getPullToRefreshScrollDirection()
    {
        return PullToRefreshBase.Orientation.VERTICAL;
    }

    protected boolean isReadyForPullEnd()
    {
        ViewPager viewpager = (ViewPager)getRefreshableView();
        PagerAdapter pageradapter = viewpager.getAdapter();
        if (pageradapter != null)
        {
            return viewpager.getCurrentItem() == pageradapter.getCount() - 1;
        } else
        {
            return false;
        }
    }

    protected boolean isReadyForPullStart()
    {
        ViewPager viewpager = (ViewPager)getRefreshableView();
        if (viewpager.getAdapter() != null)
        {
            return viewpager.getCurrentItem() == 0;
        } else
        {
            return false;
        }
    }
}
