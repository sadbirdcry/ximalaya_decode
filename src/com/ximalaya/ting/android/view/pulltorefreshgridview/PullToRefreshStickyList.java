// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListView;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.OverscrollHelper;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview:
//            PullToRefreshBase, d

public class PullToRefreshStickyList extends PullToRefreshBase
{
    final class a extends StickyListHeadersListView
    {

        final PullToRefreshStickyList a;

        private int a()
        {
            int i = 0;
            if (getChildCount() > 0)
            {
                i = Math.max(0, getChildAt(0).getHeight() - (getHeight() - getPaddingBottom() - getPaddingTop()));
            }
            return i;
        }

        protected boolean overScrollBy(int i, int j, int k, int l, int i1, int j1, int k1, 
                int l1, boolean flag)
        {
            boolean flag1 = super.overScrollBy(i, j, k, l, i1, j1, k1, l1, flag);
            OverscrollHelper.overScrollBy(a, i, k, j, a(), flag);
            return flag1;
        }

        public a(Context context, AttributeSet attributeset)
        {
            a = PullToRefreshStickyList.this;
            super(context, attributeset);
        }
    }


    private static final PullToRefreshBase.OnRefreshListener defaultOnRefreshListener = new d();

    public PullToRefreshStickyList(Context context)
    {
        super(context);
        setOnRefreshListener(defaultOnRefreshListener);
    }

    public PullToRefreshStickyList(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        setOnRefreshListener(defaultOnRefreshListener);
    }

    public PullToRefreshStickyList(Context context, PullToRefreshBase.Mode mode)
    {
        super(context, mode);
        setOnRefreshListener(defaultOnRefreshListener);
    }

    public PullToRefreshStickyList(Context context, PullToRefreshBase.Mode mode, PullToRefreshBase.AnimationStyle animationstyle)
    {
        super(context, mode, animationstyle);
        setOnRefreshListener(defaultOnRefreshListener);
    }

    protected volatile View createRefreshableView(Context context, AttributeSet attributeset)
    {
        return createRefreshableView(context, attributeset);
    }

    protected StickyListHeadersListView createRefreshableView(Context context, AttributeSet attributeset)
    {
        if (android.os.Build.VERSION.SDK_INT >= 9)
        {
            context = new a(context, attributeset);
        } else
        {
            context = new StickyListHeadersListView(context, attributeset);
        }
        context.setId(0x7f0a004e);
        return context;
    }

    public final PullToRefreshBase.Orientation getPullToRefreshScrollDirection()
    {
        return PullToRefreshBase.Orientation.VERTICAL;
    }

    protected boolean isReadyForPullEnd()
    {
        boolean flag1 = false;
        int i = ((StickyListHeadersListView)getRefreshableView()).getChildCount();
        View view = ((StickyListHeadersListView)getRefreshableView()).getChildAt(i - 1);
        boolean flag = flag1;
        if (((StickyListHeadersListView)getRefreshableView()).getFirstVisiblePosition() + ((StickyListHeadersListView)getRefreshableView()).getChildCount() >= ((StickyListHeadersListView)getRefreshableView()).getAdapter().getCount())
        {
            flag = flag1;
            if (view != null)
            {
                if (view.getBottom() <= ((StickyListHeadersListView)getRefreshableView()).getHeight())
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
            }
        }
        return flag;
    }

    protected boolean isReadyForPullStart()
    {
        boolean flag2 = true;
        boolean flag3 = false;
        boolean flag;
        if (((StickyListHeadersListView)getRefreshableView()).getWrappedList().getHeaderViewsCount() > 0)
        {
            if (((StickyListHeadersListView)getRefreshableView()).getWrappedList().getChildAt(0).getTop() >= 0)
            {
                flag = true;
            } else
            {
                flag = false;
            }
        } else
        {
            View view = ((StickyListHeadersListView)getRefreshableView()).getChildAt(0);
            flag = flag3;
            if (((StickyListHeadersListView)getRefreshableView()).getFirstVisiblePosition() == 0)
            {
                flag = flag3;
                if (view != null)
                {
                    boolean flag1;
                    if (view.getTop() >= 0)
                    {
                        flag1 = flag2;
                    } else
                    {
                        flag1 = false;
                    }
                    return flag1;
                }
            }
        }
        return flag;
    }

    protected void onPtrRestoreInstanceState(Bundle bundle)
    {
        super.onPtrRestoreInstanceState(bundle);
    }

    protected void onPtrSaveInstanceState(Bundle bundle)
    {
        super.onPtrSaveInstanceState(bundle);
    }

}
