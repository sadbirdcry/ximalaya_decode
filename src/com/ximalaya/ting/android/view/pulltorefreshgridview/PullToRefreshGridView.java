// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.GridView;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.EmptyViewMethodAccessor;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.OverscrollHelper;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview:
//            PullToRefreshAdapterViewBase

public class PullToRefreshGridView extends PullToRefreshAdapterViewBase
{
    class a extends GridView
        implements EmptyViewMethodAccessor
    {

        final PullToRefreshGridView a;

        public void setEmptyView(View view)
        {
            a.setEmptyView(view);
        }

        public void setEmptyViewInternal(View view)
        {
            super.setEmptyView(view);
        }

        public a(Context context, AttributeSet attributeset)
        {
            a = PullToRefreshGridView.this;
            super(context, attributeset);
        }
    }

    final class b extends a
    {

        final PullToRefreshGridView b;

        protected boolean overScrollBy(int i, int j, int k, int l, int i1, int j1, int k1, 
                int l1, boolean flag)
        {
            boolean flag1 = super.overScrollBy(i, j, k, l, i1, j1, k1, l1, flag);
            OverscrollHelper.overScrollBy(b, i, k, j, l, flag);
            return flag1;
        }

        public b(Context context, AttributeSet attributeset)
        {
            b = PullToRefreshGridView.this;
            super(context, attributeset);
        }
    }


    public PullToRefreshGridView(Context context)
    {
        super(context);
    }

    public PullToRefreshGridView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    public PullToRefreshGridView(Context context, PullToRefreshBase.Mode mode)
    {
        super(context, mode);
    }

    public PullToRefreshGridView(Context context, PullToRefreshBase.Mode mode, PullToRefreshBase.AnimationStyle animationstyle)
    {
        super(context, mode, animationstyle);
    }

    protected volatile View createRefreshableView(Context context, AttributeSet attributeset)
    {
        return createRefreshableView(context, attributeset);
    }

    protected final GridView createRefreshableView(Context context, AttributeSet attributeset)
    {
        if (android.os.Build.VERSION.SDK_INT >= 9)
        {
            context = new b(context, attributeset);
            context.setFocusable(false);
            context.setFocusableInTouchMode(false);
        } else
        {
            context = new a(context, attributeset);
            context.setFocusable(false);
            context.setFocusableInTouchMode(false);
        }
        context.setId(0x7f0a004e);
        return context;
    }

    public final PullToRefreshBase.Orientation getPullToRefreshScrollDirection()
    {
        return PullToRefreshBase.Orientation.VERTICAL;
    }
}
