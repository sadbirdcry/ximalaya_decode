// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview;

import android.view.animation.Interpolator;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.ViewCompat;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview:
//            PullToRefreshBase

final class f
    implements Runnable
{

    final PullToRefreshBase a;
    private final Interpolator b;
    private final int c;
    private final int d;
    private final long e;
    private a f;
    private boolean g;
    private long h;
    private int i;

    public void a()
    {
        if (a.callback != null)
        {
            a.callback.onExecute();
            a.callback = null;
        }
        g = false;
        a.removeCallbacks(this);
    }

    public void run()
    {
        if (h == -1L)
        {
            h = System.currentTimeMillis();
        } else
        {
            long l = Math.max(Math.min(((System.currentTimeMillis() - h) * 1000L) / e, 1000L), 0L);
            float f1 = d - c;
            int j = Math.round(b.getInterpolation((float)l / 1000F) * f1);
            i = d - j;
            a.setHeaderScroll(i);
        }
        if (g && c != i)
        {
            ViewCompat.postOnAnimation(a, this);
        } else
        if (f != null)
        {
            f.a();
            return;
        }
    }

    public imateCallback(PullToRefreshBase pulltorefreshbase, int j, int k, long l, imateCallback imatecallback)
    {
        a = pulltorefreshbase;
        super();
        g = true;
        h = -1L;
        i = -1;
        d = j;
        c = k;
        b = PullToRefreshBase.access$200(pulltorefreshbase);
        e = l;
        f = imatecallback;
    }
}
