// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.EmptyViewMethodAccessor;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview:
//            PullToRefreshListView2

protected class mAddedLvFooter extends ListView
    implements EmptyViewMethodAccessor
{

    private boolean mAddedLvFooter;
    final PullToRefreshListView2 this$0;

    protected void dispatchDraw(Canvas canvas)
    {
        try
        {
            super.dispatchDraw(canvas);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Canvas canvas)
        {
            canvas.printStackTrace();
        }
    }

    public boolean dispatchTouchEvent(MotionEvent motionevent)
    {
        boolean flag;
        try
        {
            flag = super.dispatchTouchEvent(motionevent);
        }
        // Misplaced declaration of an exception variable
        catch (MotionEvent motionevent)
        {
            motionevent.printStackTrace();
            return false;
        }
        return flag;
    }

    public volatile void setAdapter(Adapter adapter)
    {
        setAdapter((ListAdapter)adapter);
    }

    public void setAdapter(ListAdapter listadapter)
    {
        if (PullToRefreshListView2.access$000(PullToRefreshListView2.this) != null && !mAddedLvFooter)
        {
            addFooterView(PullToRefreshListView2.access$000(PullToRefreshListView2.this), null, false);
            mAddedLvFooter = true;
        }
        super.setAdapter(listadapter);
    }

    public void setEmptyView(View view)
    {
        PullToRefreshListView2.this.setEmptyView(view);
    }

    public void setEmptyViewInternal(View view)
    {
        super.setEmptyView(view);
    }

    public (Context context, AttributeSet attributeset)
    {
        this$0 = PullToRefreshListView2.this;
        super(context, attributeset);
        mAddedLvFooter = false;
    }
}
