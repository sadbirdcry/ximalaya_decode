// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.FlipLoadingLayout;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.ILoadingLayout;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.LoadingLayout;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.LoadingLayoutProxy;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.OverscrollHelper;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.RotateLoadingLayout;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.Utils;
import com.ximalaya.ting.android.view.pulltorefreshgridview.internal.ViewCompat;
import java.util.Date;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview:
//            IPullToRefresh, c, a, b

public abstract class PullToRefreshBase extends LinearLayout
    implements IPullToRefresh
{
    protected static interface AnimateCallback
    {

        public abstract void onExecute();
    }

    public static final class AnimationStyle extends Enum
    {

        private static final AnimationStyle $VALUES[];
        public static final AnimationStyle FLIP;
        public static final AnimationStyle ROTATE;

        static AnimationStyle getDefault()
        {
            return FLIP;
        }

        static AnimationStyle mapIntToValue(int i)
        {
            switch (i)
            {
            default:
                return ROTATE;

            case 1: // '\001'
                return FLIP;
            }
        }

        public static AnimationStyle valueOf(String s)
        {
            return (AnimationStyle)Enum.valueOf(com/ximalaya/ting/android/view/pulltorefreshgridview/PullToRefreshBase$AnimationStyle, s);
        }

        public static AnimationStyle[] values()
        {
            return (AnimationStyle[])$VALUES.clone();
        }

        LoadingLayout createLoadingLayout(Context context, Mode mode, Orientation orientation, TypedArray typedarray)
        {
            static class _cls1
            {

                static final int a[];
                static final int b[];
                static final int c[];
                static final int d[];

                static 
                {
                    d = new int[AnimationStyle.values().length];
                    try
                    {
                        d[AnimationStyle.ROTATE.ordinal()] = 1;
                    }
                    catch (NoSuchFieldError nosuchfielderror13) { }
                    try
                    {
                        d[AnimationStyle.FLIP.ordinal()] = 2;
                    }
                    catch (NoSuchFieldError nosuchfielderror12) { }
                    c = new int[Mode.values().length];
                    try
                    {
                        c[Mode.PULL_FROM_END.ordinal()] = 1;
                    }
                    catch (NoSuchFieldError nosuchfielderror11) { }
                    try
                    {
                        c[Mode.PULL_FROM_START.ordinal()] = 2;
                    }
                    catch (NoSuchFieldError nosuchfielderror10) { }
                    try
                    {
                        c[Mode.MANUAL_REFRESH_ONLY.ordinal()] = 3;
                    }
                    catch (NoSuchFieldError nosuchfielderror9) { }
                    try
                    {
                        c[Mode.BOTH.ordinal()] = 4;
                    }
                    catch (NoSuchFieldError nosuchfielderror8) { }
                    b = new int[State.values().length];
                    try
                    {
                        b[State.RESET.ordinal()] = 1;
                    }
                    catch (NoSuchFieldError nosuchfielderror7) { }
                    try
                    {
                        b[State.PULL_TO_REFRESH.ordinal()] = 2;
                    }
                    catch (NoSuchFieldError nosuchfielderror6) { }
                    try
                    {
                        b[State.RELEASE_TO_REFRESH.ordinal()] = 3;
                    }
                    catch (NoSuchFieldError nosuchfielderror5) { }
                    try
                    {
                        b[State.REFRESHING.ordinal()] = 4;
                    }
                    catch (NoSuchFieldError nosuchfielderror4) { }
                    try
                    {
                        b[State.MANUAL_REFRESHING.ordinal()] = 5;
                    }
                    catch (NoSuchFieldError nosuchfielderror3) { }
                    try
                    {
                        b[State.OVERSCROLLING.ordinal()] = 6;
                    }
                    catch (NoSuchFieldError nosuchfielderror2) { }
                    a = new int[Orientation.values().length];
                    try
                    {
                        a[Orientation.HORIZONTAL.ordinal()] = 1;
                    }
                    catch (NoSuchFieldError nosuchfielderror1) { }
                    try
                    {
                        a[Orientation.VERTICAL.ordinal()] = 2;
                    }
                    catch (NoSuchFieldError nosuchfielderror)
                    {
                        return;
                    }
                }
            }

            switch (_cls1.d[ordinal()])
            {
            default:
                return new RotateLoadingLayout(context, mode, orientation, typedarray);

            case 2: // '\002'
                return new FlipLoadingLayout(context, mode, orientation, typedarray);
            }
        }

        static 
        {
            ROTATE = new AnimationStyle("ROTATE", 0);
            FLIP = new AnimationStyle("FLIP", 1);
            $VALUES = (new AnimationStyle[] {
                ROTATE, FLIP
            });
        }

        private AnimationStyle(String s, int i)
        {
            super(s, i);
        }
    }

    public static final class Mode extends Enum
    {

        private static final Mode $VALUES[];
        public static final Mode BOTH;
        public static final Mode DISABLED;
        public static final Mode MANUAL_REFRESH_ONLY;
        public static Mode PULL_DOWN_TO_REFRESH;
        public static final Mode PULL_FROM_END;
        public static final Mode PULL_FROM_START;
        public static Mode PULL_UP_TO_REFRESH;
        private int mIntValue;

        static Mode getDefault()
        {
            return PULL_FROM_START;
        }

        static Mode mapIntToValue(int i)
        {
            Mode amode[] = values();
            int k = amode.length;
            for (int j = 0; j < k; j++)
            {
                Mode mode = amode[j];
                if (i == mode.getIntValue())
                {
                    return mode;
                }
            }

            return getDefault();
        }

        public static Mode valueOf(String s)
        {
            return (Mode)Enum.valueOf(com/ximalaya/ting/android/view/pulltorefreshgridview/PullToRefreshBase$Mode, s);
        }

        public static Mode[] values()
        {
            return (Mode[])$VALUES.clone();
        }

        int getIntValue()
        {
            return mIntValue;
        }

        public boolean permitsPullToRefresh()
        {
            return this != DISABLED && this != MANUAL_REFRESH_ONLY;
        }

        public boolean showFooterLoadingLayout()
        {
            return this == PULL_FROM_END || this == BOTH || this == MANUAL_REFRESH_ONLY;
        }

        public boolean showHeaderLoadingLayout()
        {
            return this == PULL_FROM_START || this == BOTH;
        }

        static 
        {
            DISABLED = new Mode("DISABLED", 0, 0);
            PULL_FROM_START = new Mode("PULL_FROM_START", 1, 1);
            PULL_FROM_END = new Mode("PULL_FROM_END", 2, 2);
            BOTH = new Mode("BOTH", 3, 3);
            MANUAL_REFRESH_ONLY = new Mode("MANUAL_REFRESH_ONLY", 4, 4);
            $VALUES = (new Mode[] {
                DISABLED, PULL_FROM_START, PULL_FROM_END, BOTH, MANUAL_REFRESH_ONLY
            });
            PULL_DOWN_TO_REFRESH = PULL_FROM_START;
            PULL_UP_TO_REFRESH = PULL_FROM_END;
        }

        private Mode(String s, int i, int j)
        {
            super(s, i);
            mIntValue = j;
        }
    }

    public static interface OnLastItemVisibleListener
    {

        public abstract void onLastItemVisible();
    }

    public static interface OnPullEventListener
    {

        public abstract void onPullEvent(PullToRefreshBase pulltorefreshbase, State state, Mode mode);
    }

    public static interface OnRefreshListener
    {

        public abstract void onRefresh(PullToRefreshBase pulltorefreshbase);
    }

    public static interface OnRefreshListener2
    {

        public abstract void onPullDownToRefresh(PullToRefreshBase pulltorefreshbase);

        public abstract void onPullUpToRefresh(PullToRefreshBase pulltorefreshbase);
    }

    public static final class Orientation extends Enum
    {

        private static final Orientation $VALUES[];
        public static final Orientation HORIZONTAL;
        public static final Orientation VERTICAL;

        public static Orientation valueOf(String s)
        {
            return (Orientation)Enum.valueOf(com/ximalaya/ting/android/view/pulltorefreshgridview/PullToRefreshBase$Orientation, s);
        }

        public static Orientation[] values()
        {
            return (Orientation[])$VALUES.clone();
        }

        static 
        {
            VERTICAL = new Orientation("VERTICAL", 0);
            HORIZONTAL = new Orientation("HORIZONTAL", 1);
            $VALUES = (new Orientation[] {
                VERTICAL, HORIZONTAL
            });
        }

        private Orientation(String s, int i)
        {
            super(s, i);
        }
    }

    public static final class State extends Enum
    {

        private static final State $VALUES[];
        public static final State MANUAL_REFRESHING;
        public static final State OVERSCROLLING;
        public static final State PULL_TO_REFRESH;
        public static final State REFRESHING;
        public static final State RELEASE_TO_REFRESH;
        public static final State RESET;
        private int mIntValue;

        static State mapIntToValue(int i)
        {
            State astate[] = values();
            int k = astate.length;
            for (int j = 0; j < k; j++)
            {
                State state = astate[j];
                if (i == state.getIntValue())
                {
                    return state;
                }
            }

            return RESET;
        }

        public static State valueOf(String s)
        {
            return (State)Enum.valueOf(com/ximalaya/ting/android/view/pulltorefreshgridview/PullToRefreshBase$State, s);
        }

        public static State[] values()
        {
            return (State[])$VALUES.clone();
        }

        int getIntValue()
        {
            return mIntValue;
        }

        static 
        {
            RESET = new State("RESET", 0, 0);
            PULL_TO_REFRESH = new State("PULL_TO_REFRESH", 1, 1);
            RELEASE_TO_REFRESH = new State("RELEASE_TO_REFRESH", 2, 2);
            REFRESHING = new State("REFRESHING", 3, 8);
            MANUAL_REFRESHING = new State("MANUAL_REFRESHING", 4, 9);
            OVERSCROLLING = new State("OVERSCROLLING", 5, 16);
            $VALUES = (new State[] {
                RESET, PULL_TO_REFRESH, RELEASE_TO_REFRESH, REFRESHING, MANUAL_REFRESHING, OVERSCROLLING
            });
        }

        private State(String s, int i, int j)
        {
            super(s, i);
            mIntValue = j;
        }
    }

    static interface a
    {

        public abstract void a();
    }

    final class b
        implements Runnable
    {

        final PullToRefreshBase a;
        private final Interpolator b;
        private final int c;
        private final int d;
        private final long e;
        private a f;
        private boolean g;
        private long h;
        private int i;

        public void a()
        {
            if (a.callback != null)
            {
                a.callback.onExecute();
                a.callback = null;
            }
            g = false;
            a.removeCallbacks(this);
        }

        public void run()
        {
            if (h == -1L)
            {
                h = System.currentTimeMillis();
            } else
            {
                long l = Math.max(Math.min(((System.currentTimeMillis() - h) * 1000L) / e, 1000L), 0L);
                float f1 = d - c;
                int j = Math.round(b.getInterpolation((float)l / 1000F) * f1);
                i = d - j;
                a.setHeaderScroll(i);
            }
            if (g && c != i)
            {
                ViewCompat.postOnAnimation(a, this);
            } else
            if (f != null)
            {
                f.a();
                return;
            }
        }

        public b(int j, int k, long l, a a1)
        {
            a = PullToRefreshBase.this;
            super();
            g = true;
            h = -1L;
            i = -1;
            d = j;
            c = k;
            b = mScrollAnimationInterpolator;
            e = l;
            f = a1;
        }
    }


    public static final boolean DEBUG = false;
    static final int DEMO_SCROLL_INTERVAL = 225;
    static final float FRICTION = 2F;
    static final String LOG_TAG = "PullToRefresh";
    public static final int SMOOTH_SCROLL_DURATION_MS = 200;
    public static final int SMOOTH_SCROLL_LONG_DURATION_MS = 325;
    static final String STATE_CURRENT_MODE = "ptr_current_mode";
    static final String STATE_MODE = "ptr_mode";
    static final String STATE_SCROLLING_REFRESHING_ENABLED = "ptr_disable_scrolling";
    static final String STATE_SHOW_REFRESHING_VIEW = "ptr_show_refreshing_view";
    static final String STATE_STATE = "ptr_state";
    static final String STATE_SUPER = "ptr_super";
    static final boolean USE_HW_LAYERS = false;
    AnimateCallback callback;
    private Mode mCurrentMode;
    private b mCurrentSmoothScrollRunnable;
    private boolean mFilterTouchEvents;
    private LoadingLayout mFooterLayout;
    private LoadingLayout mHeaderLayout;
    private float mInitialMotionX;
    private float mInitialMotionY;
    private boolean mIsBeingDragged;
    private float mLastMotionX;
    private float mLastMotionY;
    private boolean mLayoutVisibilityChangesEnabled;
    private AnimationStyle mLoadingAnimationStyle;
    private Mode mMode;
    private OnPullEventListener mOnPullEventListener;
    private OnRefreshListener mOnRefreshListener;
    private OnRefreshListener2 mOnRefreshListener2;
    private boolean mOverScrollEnabled;
    View mRefreshableView;
    private FrameLayout mRefreshableViewWrapper;
    private Interpolator mScrollAnimationInterpolator;
    private boolean mScrollingWhileRefreshingEnabled;
    private boolean mShowViewWhileRefreshing;
    private State mState;
    private int mTouchSlop;

    public PullToRefreshBase(Context context)
    {
        super(context);
        mIsBeingDragged = false;
        mState = State.RESET;
        mMode = Mode.getDefault();
        mShowViewWhileRefreshing = true;
        mScrollingWhileRefreshingEnabled = false;
        mFilterTouchEvents = true;
        mOverScrollEnabled = true;
        mLayoutVisibilityChangesEnabled = true;
        mLoadingAnimationStyle = AnimationStyle.getDefault();
        init(context, null);
    }

    public PullToRefreshBase(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        mIsBeingDragged = false;
        mState = State.RESET;
        mMode = Mode.getDefault();
        mShowViewWhileRefreshing = true;
        mScrollingWhileRefreshingEnabled = false;
        mFilterTouchEvents = true;
        mOverScrollEnabled = true;
        mLayoutVisibilityChangesEnabled = true;
        mLoadingAnimationStyle = AnimationStyle.getDefault();
        init(context, attributeset);
    }

    public PullToRefreshBase(Context context, Mode mode)
    {
        super(context);
        mIsBeingDragged = false;
        mState = State.RESET;
        mMode = Mode.getDefault();
        mShowViewWhileRefreshing = true;
        mScrollingWhileRefreshingEnabled = false;
        mFilterTouchEvents = true;
        mOverScrollEnabled = true;
        mLayoutVisibilityChangesEnabled = true;
        mLoadingAnimationStyle = AnimationStyle.getDefault();
        mMode = mode;
        init(context, null);
    }

    public PullToRefreshBase(Context context, Mode mode, AnimationStyle animationstyle)
    {
        super(context);
        mIsBeingDragged = false;
        mState = State.RESET;
        mMode = Mode.getDefault();
        mShowViewWhileRefreshing = true;
        mScrollingWhileRefreshingEnabled = false;
        mFilterTouchEvents = true;
        mOverScrollEnabled = true;
        mLayoutVisibilityChangesEnabled = true;
        mLoadingAnimationStyle = AnimationStyle.getDefault();
        mMode = mode;
        mLoadingAnimationStyle = animationstyle;
        init(context, null);
    }

    private void addRefreshableView(Context context, View view)
    {
        mRefreshableViewWrapper = new FrameLayout(context);
        mRefreshableViewWrapper.addView(view, -1, -1);
        addViewInternal(mRefreshableViewWrapper, new android.widget.LinearLayout.LayoutParams(-1, -1));
    }

    private void callRefreshListener()
    {
        if (mOnRefreshListener != null)
        {
            mOnRefreshListener.onRefresh(this);
        } else
        if (mOnRefreshListener2 != null)
        {
            if (mCurrentMode == Mode.PULL_FROM_START)
            {
                mOnRefreshListener2.onPullDownToRefresh(this);
                return;
            }
            if (mCurrentMode == Mode.PULL_FROM_END)
            {
                mOnRefreshListener2.onPullUpToRefresh(this);
                return;
            }
        }
    }

    private android.widget.LinearLayout.LayoutParams getLoadingLayoutLayoutParams()
    {
        switch (_cls1.a[getPullToRefreshScrollDirection().ordinal()])
        {
        default:
            return new android.widget.LinearLayout.LayoutParams(-1, -2);

        case 1: // '\001'
            return new android.widget.LinearLayout.LayoutParams(-2, -1);
        }
    }

    private int getMaximumPullScroll()
    {
        switch (_cls1.a[getPullToRefreshScrollDirection().ordinal()])
        {
        default:
            return Math.round((float)getHeight() / 2.0F);

        case 1: // '\001'
            return Math.round((float)getWidth() / 2.0F);
        }
    }

    private void init(Context context, AttributeSet attributeset)
    {
        switch (_cls1.a[getPullToRefreshScrollDirection().ordinal()])
        {
        default:
            setOrientation(1);
            break;

        case 1: // '\001'
            break MISSING_BLOCK_LABEL_228;
        }
_L1:
        setGravity(17);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
        TypedArray typedarray = context.obtainStyledAttributes(attributeset, com.ximalaya.ting.android.R.styleable.PullToRefresh);
        if (typedarray.hasValue(4))
        {
            mMode = Mode.mapIntToValue(typedarray.getInteger(4, 0));
        }
        if (typedarray.hasValue(12))
        {
            mLoadingAnimationStyle = AnimationStyle.mapIntToValue(typedarray.getInteger(12, 0));
        }
        mRefreshableView = createRefreshableView(context, attributeset);
        addRefreshableView(context, mRefreshableView);
        mHeaderLayout = createLoadingLayout(context, Mode.PULL_FROM_START, typedarray);
        mFooterLayout = createLoadingLayout(context, Mode.PULL_FROM_END, typedarray);
        if (typedarray.hasValue(0))
        {
            context = typedarray.getDrawable(0);
            if (context != null)
            {
                mRefreshableView.setBackgroundDrawable(context);
            }
        } else
        if (typedarray.hasValue(16))
        {
            Utils.warnDeprecation("ptrAdapterViewBackground", "ptrRefreshableViewBackground");
            context = typedarray.getDrawable(16);
            if (context != null)
            {
                mRefreshableView.setBackgroundDrawable(context);
            }
        }
        if (typedarray.hasValue(9))
        {
            mOverScrollEnabled = typedarray.getBoolean(9, true);
        }
        if (typedarray.hasValue(13))
        {
            mScrollingWhileRefreshingEnabled = typedarray.getBoolean(13, false);
        }
        handleStyledAttributes(typedarray);
        typedarray.recycle();
        updateUIForMode();
        return;
        setOrientation(0);
          goto _L1
    }

    private boolean isReadyForPull()
    {
        _cls1.c[mMode.ordinal()];
        JVM INSTR tableswitch 1 4: default 40
    //                   1 47
    //                   2 42
    //                   3 40
    //                   4 52;
           goto _L1 _L2 _L3 _L1 _L4
_L1:
        return false;
_L3:
        return isReadyForPullStart();
_L2:
        return isReadyForPullEnd();
_L4:
        if (isReadyForPullEnd() || isReadyForPullStart())
        {
            return true;
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    private void pullEvent()
    {
        _cls1.a[getPullToRefreshScrollDirection().ordinal()];
        JVM INSTR tableswitch 1 1: default 28
    //                   1 182;
           goto _L1 _L2
_L1:
        float f;
        float f1;
        f1 = mInitialMotionY;
        f = mLastMotionY;
_L15:
        _cls1.c[mCurrentMode.ordinal()];
        JVM INSTR tableswitch 1 1: default 68
    //                   1 195;
           goto _L3 _L4
_L3:
        int i;
        int j;
        j = Math.round(Math.min(f1 - f, 0.0F) / 2.0F);
        i = getHeaderSize();
_L11:
        setHeaderScroll(j);
        if (j == 0 || isRefreshing()) goto _L6; else goto _L5
_L5:
        f = (float)Math.abs(j) / (float)i;
        _cls1.c[mCurrentMode.ordinal()];
        JVM INSTR tableswitch 1 1: default 144
    //                   1 217;
           goto _L7 _L8
_L7:
        mHeaderLayout.onPull(f);
_L12:
        if (mState == State.PULL_TO_REFRESH || i < Math.abs(j)) goto _L10; else goto _L9
_L9:
        setState(State.PULL_TO_REFRESH, new boolean[0]);
_L6:
        return;
_L2:
        f1 = mInitialMotionX;
        f = mLastMotionX;
        continue; /* Loop/switch isn't completed */
_L4:
        j = Math.round(Math.max(f1 - f, 0.0F) / 2.0F);
        i = getFooterSize();
          goto _L11
_L8:
        mFooterLayout.onPull(f);
          goto _L12
_L10:
        if (mState != State.PULL_TO_REFRESH || i >= Math.abs(j)) goto _L6; else goto _L13
_L13:
        setState(State.RELEASE_TO_REFRESH, new boolean[0]);
        return;
        if (true) goto _L15; else goto _L14
_L14:
    }

    private final void smoothScrollTo(int i, long l)
    {
        smoothScrollTo(i, l, 0L, null);
    }

    private final void smoothScrollTo(int i, long l, long l1, a a1)
    {
        if (mCurrentSmoothScrollRunnable != null)
        {
            mCurrentSmoothScrollRunnable.a();
        }
        _cls1.a[getPullToRefreshScrollDirection().ordinal()];
        JVM INSTR tableswitch 1 1: default 44
    //                   1 111;
           goto _L1 _L2
_L1:
        int j = getScrollY();
_L4:
        if (j != i)
        {
            if (mScrollAnimationInterpolator == null)
            {
                mScrollAnimationInterpolator = new DecelerateInterpolator();
            }
            mCurrentSmoothScrollRunnable = new b(j, i, l, a1);
            if (l1 <= 0L)
            {
                break; /* Loop/switch isn't completed */
            }
            postDelayed(mCurrentSmoothScrollRunnable, l1);
        }
        return;
_L2:
        j = getScrollX();
        if (true) goto _L4; else goto _L3
_L3:
        post(mCurrentSmoothScrollRunnable);
        return;
    }

    private final void smoothScrollToAndBack(int i)
    {
        smoothScrollTo(i, 200L, 0L, new c(this));
    }

    public void addView(View view, int i, android.view.ViewGroup.LayoutParams layoutparams)
    {
        View view1 = getRefreshableView();
        if (view1 instanceof ViewGroup)
        {
            ((ViewGroup)view1).addView(view, i, layoutparams);
            return;
        } else
        {
            throw new UnsupportedOperationException("Refreshable View is not a ViewGroup so can't addView");
        }
    }

    protected final void addViewInternal(View view, int i, android.view.ViewGroup.LayoutParams layoutparams)
    {
        super.addView(view, i, layoutparams);
    }

    protected final void addViewInternal(View view, android.view.ViewGroup.LayoutParams layoutparams)
    {
        super.addView(view, -1, layoutparams);
    }

    public void clearListener()
    {
        mOnRefreshListener2 = null;
        mOnRefreshListener = null;
        mOnPullEventListener = null;
    }

    public final void clearRefreshableView()
    {
        mRefreshableView = null;
    }

    protected LoadingLayout createLoadingLayout(Context context, Mode mode, TypedArray typedarray)
    {
        context = mLoadingAnimationStyle.createLoadingLayout(context, mode, getPullToRefreshScrollDirection(), typedarray);
        context.setVisibility(4);
        return context;
    }

    protected LoadingLayoutProxy createLoadingLayoutProxy(boolean flag, boolean flag1)
    {
        LoadingLayoutProxy loadinglayoutproxy = new LoadingLayoutProxy();
        if (flag && mMode.showHeaderLoadingLayout())
        {
            loadinglayoutproxy.addLayout(mHeaderLayout);
        }
        if (flag1 && mMode.showFooterLoadingLayout())
        {
            loadinglayoutproxy.addLayout(mFooterLayout);
        }
        return loadinglayoutproxy;
    }

    protected abstract View createRefreshableView(Context context, AttributeSet attributeset);

    public final boolean demo()
    {
        if (mMode.showHeaderLoadingLayout() && isReadyForPullStart())
        {
            smoothScrollToAndBack(-getHeaderSize() * 2);
            return true;
        }
        if (mMode.showFooterLoadingLayout() && isReadyForPullEnd())
        {
            smoothScrollToAndBack(getFooterSize() * 2);
            return true;
        } else
        {
            return false;
        }
    }

    protected final void disableLoadingLayoutVisibilityChanges()
    {
        mLayoutVisibilityChangesEnabled = false;
    }

    public final Mode getCurrentMode()
    {
        return mCurrentMode;
    }

    public final boolean getFilterTouchEvents()
    {
        return mFilterTouchEvents;
    }

    protected final LoadingLayout getFooterLayout()
    {
        return mFooterLayout;
    }

    protected final int getFooterSize()
    {
        return mFooterLayout.getContentSize();
    }

    protected final LoadingLayout getHeaderLayout()
    {
        return mHeaderLayout;
    }

    protected final int getHeaderSize()
    {
        return mHeaderLayout.getContentSize();
    }

    public final ILoadingLayout getLoadingLayoutProxy()
    {
        return getLoadingLayoutProxy(true, true);
    }

    public final ILoadingLayout getLoadingLayoutProxy(boolean flag, boolean flag1)
    {
        return createLoadingLayoutProxy(flag, flag1);
    }

    public final Mode getMode()
    {
        return mMode;
    }

    public abstract Orientation getPullToRefreshScrollDirection();

    protected int getPullToRefreshScrollDuration()
    {
        return 200;
    }

    protected int getPullToRefreshScrollDurationLonger()
    {
        return 325;
    }

    public final View getRefreshableView()
    {
        return mRefreshableView;
    }

    protected FrameLayout getRefreshableViewWrapper()
    {
        return mRefreshableViewWrapper;
    }

    public final boolean getShowViewWhileRefreshing()
    {
        return mShowViewWhileRefreshing;
    }

    public final State getState()
    {
        return mState;
    }

    protected void handleStyledAttributes(TypedArray typedarray)
    {
    }

    public final boolean isDisableScrollingWhileRefreshing()
    {
        return !isScrollingWhileRefreshingEnabled();
    }

    public final boolean isPullToRefreshEnabled()
    {
        return mMode.permitsPullToRefresh();
    }

    public final boolean isPullToRefreshOverScrollEnabled()
    {
        return android.os.Build.VERSION.SDK_INT >= 9 && mOverScrollEnabled && OverscrollHelper.isAndroidOverScrollEnabled(mRefreshableView);
    }

    protected abstract boolean isReadyForPullEnd();

    protected abstract boolean isReadyForPullStart();

    public final boolean isRefreshing()
    {
        return mState == State.REFRESHING || mState == State.MANUAL_REFRESHING;
    }

    public final boolean isScrollingWhileRefreshingEnabled()
    {
        return mScrollingWhileRefreshingEnabled;
    }

    public final boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        int i;
        if (!isPullToRefreshEnabled())
        {
            return false;
        }
        i = motionevent.getAction();
        if (i == 3 || i == 1)
        {
            mIsBeingDragged = false;
            return false;
        }
        if (i != 0 && mIsBeingDragged)
        {
            return true;
        }
        i;
        JVM INSTR tableswitch 0 2: default 76
    //                   0 335
    //                   1 76
    //                   2 81;
           goto _L1 _L2 _L1 _L3
_L1:
        return mIsBeingDragged;
_L3:
        float f3;
        float f4;
        if (!mScrollingWhileRefreshingEnabled && isRefreshing())
        {
            return true;
        }
        if (!isReadyForPull())
        {
            continue; /* Loop/switch isn't completed */
        }
        f3 = motionevent.getY();
        f4 = motionevent.getX();
        _cls1.a[getPullToRefreshScrollDirection().ordinal()];
        JVM INSTR tableswitch 1 1: default 144
    //                   1 254;
           goto _L4 _L5
_L4:
        float f;
        float f2;
        f2 = f3 - mLastMotionY;
        f = f4 - mLastMotionX;
_L6:
        float f5 = Math.abs(f2);
        if (f5 > (float)mTouchSlop && (!mFilterTouchEvents || f5 > Math.abs(f)))
        {
            if (mMode.showHeaderLoadingLayout() && f2 >= 1.0F && isReadyForPullStart())
            {
                mLastMotionY = f3;
                mLastMotionX = f4;
                mIsBeingDragged = true;
                if (mMode == Mode.BOTH)
                {
                    mCurrentMode = Mode.PULL_FROM_START;
                }
            } else
            if (mMode.showFooterLoadingLayout() && f2 <= -1F && isReadyForPullEnd())
            {
                mLastMotionY = f3;
                mLastMotionX = f4;
                mIsBeingDragged = true;
                if (mMode == Mode.BOTH)
                {
                    mCurrentMode = Mode.PULL_FROM_END;
                }
            }
        }
        continue; /* Loop/switch isn't completed */
_L5:
        f2 = f4 - mLastMotionX;
        f = f3 - mLastMotionY;
          goto _L6
_L2:
        if (isReadyForPull())
        {
            float f1 = motionevent.getY();
            mInitialMotionY = f1;
            mLastMotionY = f1;
            f1 = motionevent.getX();
            mInitialMotionX = f1;
            mLastMotionX = f1;
            mIsBeingDragged = false;
        }
        if (true) goto _L1; else goto _L7
_L7:
    }

    protected void onPtrRestoreInstanceState(Bundle bundle)
    {
    }

    protected void onPtrSaveInstanceState(Bundle bundle)
    {
    }

    protected void onPullToRefresh()
    {
        switch (_cls1.c[mCurrentMode.ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            mFooterLayout.pullToRefresh();
            return;

        case 2: // '\002'
            mHeaderLayout.pullToRefresh();
            break;
        }
    }

    public final void onRefreshComplete()
    {
        if (isRefreshing())
        {
            setState(State.RESET, new boolean[0]);
            if (mCurrentMode == Mode.PULL_FROM_START)
            {
                mHeaderLayout.setSubHeaderText((new StringBuilder()).append("\u6700\u8FD1\u66F4\u65B0:").append((new Date()).toLocaleString()).toString());
            }
        }
    }

    protected void onRefreshing(boolean flag)
    {
        if (mMode.showHeaderLoadingLayout())
        {
            mHeaderLayout.refreshing();
        }
        if (mMode.showFooterLoadingLayout())
        {
            mFooterLayout.refreshing();
        }
        if (flag)
        {
            if (mShowViewWhileRefreshing)
            {
                com.ximalaya.ting.android.view.pulltorefreshgridview.a a1 = new com.ximalaya.ting.android.view.pulltorefreshgridview.a(this);
                switch (_cls1.c[mCurrentMode.ordinal()])
                {
                case 2: // '\002'
                default:
                    smoothScrollTo(-getHeaderSize(), a1);
                    return;

                case 1: // '\001'
                case 3: // '\003'
                    smoothScrollTo(getFooterSize(), a1);
                    break;
                }
                return;
            } else
            {
                smoothScrollTo(0);
                return;
            }
        } else
        {
            callRefreshListener();
            return;
        }
    }

    protected void onReleaseToRefresh()
    {
        switch (_cls1.c[mCurrentMode.ordinal()])
        {
        default:
            return;

        case 1: // '\001'
            mFooterLayout.releaseToRefresh();
            return;

        case 2: // '\002'
            mHeaderLayout.releaseToRefresh();
            break;
        }
    }

    protected void onReset()
    {
        mIsBeingDragged = false;
        mLayoutVisibilityChangesEnabled = true;
        mHeaderLayout.reset();
        mFooterLayout.reset();
        smoothScrollTo(0);
    }

    protected final void onSizeChanged(int i, int j, int k, int l)
    {
        super.onSizeChanged(i, j, k, l);
        refreshLoadingViewsSize();
        refreshRefreshableViewSize(i, j);
        post(new com.ximalaya.ting.android.view.pulltorefreshgridview.b(this));
    }

    public final boolean onTouchEvent(MotionEvent motionevent)
    {
        if (isPullToRefreshEnabled()) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        if (!mScrollingWhileRefreshingEnabled && isRefreshing())
        {
            return true;
        }
        if (motionevent.getAction() == 0 && motionevent.getEdgeFlags() != 0) goto _L1; else goto _L3
_L3:
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 72
    //                   0 74
    //                   1 142
    //                   2 113
    //                   3 142;
           goto _L4 _L5 _L6 _L7 _L6
_L6:
        continue; /* Loop/switch isn't completed */
_L4:
        return false;
_L5:
        if (!isReadyForPull()) goto _L1; else goto _L8
_L8:
        float f = motionevent.getY();
        mInitialMotionY = f;
        mLastMotionY = f;
        f = motionevent.getX();
        mInitialMotionX = f;
        mLastMotionX = f;
        return true;
_L7:
        if (!mIsBeingDragged) goto _L1; else goto _L9
_L9:
        mLastMotionY = motionevent.getY();
        mLastMotionX = motionevent.getX();
        pullEvent();
        return true;
        if (!mIsBeingDragged) goto _L1; else goto _L10
_L10:
        mIsBeingDragged = false;
        if (mState == State.RELEASE_TO_REFRESH && (mOnRefreshListener != null || mOnRefreshListener2 != null))
        {
            setState(State.REFRESHING, new boolean[] {
                true
            });
            return true;
        }
        if (isRefreshing())
        {
            smoothScrollTo(0);
            return true;
        } else
        {
            setState(State.RESET, new boolean[0]);
            return true;
        }
    }

    protected final void refreshLoadingViewsSize()
    {
        int j;
        int k;
        int l;
        int i1;
        boolean flag;
        int j1;
        flag = false;
        j1 = (int)((float)getMaximumPullScroll() * 1.2F);
        k = getPaddingLeft();
        j = getPaddingTop();
        i1 = getPaddingRight();
        l = getPaddingBottom();
        _cls1.a[getPullToRefreshScrollDirection().ordinal()];
        JVM INSTR tableswitch 1 2: default 72
    //                   1 95
    //                   2 180;
           goto _L1 _L2 _L3
_L1:
        int i;
        i = l;
        l = k;
        k = j;
        j = i;
        i = i1;
_L5:
        setPadding(l, k, i, j);
        return;
_L2:
        if (mMode.showHeaderLoadingLayout())
        {
            mHeaderLayout.setWidth(j1);
            i = -j1;
        } else
        {
            i = 0;
        }
        if (mMode.showFooterLoadingLayout())
        {
            mFooterLayout.setWidth(j1);
            k = -j1;
            i1 = i;
            i = k;
            k = j;
            j = l;
            l = i1;
        } else
        {
            i1 = i;
            i = 0;
            k = j;
            j = l;
            l = i1;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (mMode.showHeaderLoadingLayout())
        {
            mHeaderLayout.setHeight(j1);
            i = -j1;
        } else
        {
            i = 0;
        }
        if (mMode.showFooterLoadingLayout())
        {
            mFooterLayout.setHeight(j1);
            j = -j1;
            l = i;
            i = i1;
            i1 = k;
            k = l;
            l = i1;
        } else
        {
            l = i;
            i = i1;
            i1 = k;
            j = ((flag) ? 1 : 0);
            k = l;
            l = i1;
        }
        if (true) goto _L5; else goto _L4
_L4:
    }

    protected final void refreshRefreshableViewSize(int i, int j)
    {
        android.widget.LinearLayout.LayoutParams layoutparams = (android.widget.LinearLayout.LayoutParams)mRefreshableViewWrapper.getLayoutParams();
        _cls1.a[getPullToRefreshScrollDirection().ordinal()];
        JVM INSTR tableswitch 1 2: default 44
    //                   1 45
    //                   2 66;
           goto _L1 _L2 _L3
_L1:
        return;
_L2:
        if (layoutparams.width != i)
        {
            layoutparams.width = i;
            mRefreshableViewWrapper.requestLayout();
            return;
        }
        continue; /* Loop/switch isn't completed */
_L3:
        if (layoutparams.height != j)
        {
            layoutparams.height = j;
            mRefreshableViewWrapper.requestLayout();
            return;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    public void setAnimateCallback(AnimateCallback animatecallback)
    {
        callback = animatecallback;
    }

    public void setDisableScrollingWhileRefreshing(boolean flag)
    {
        if (!flag)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        setScrollingWhileRefreshingEnabled(flag);
    }

    public final void setFilterTouchEvents(boolean flag)
    {
        mFilterTouchEvents = flag;
    }

    public final void setHeaderScroll(int i)
    {
        int j = getMaximumPullScroll();
        i = Math.min(j, Math.max(-j, i));
        if (mLayoutVisibilityChangesEnabled)
        {
            if (i < 0)
            {
                mHeaderLayout.setVisibility(0);
            } else
            if (i > 0)
            {
                mFooterLayout.setVisibility(0);
            } else
            {
                mHeaderLayout.setVisibility(4);
                mFooterLayout.setVisibility(4);
            }
        }
        switch (_cls1.a[getPullToRefreshScrollDirection().ordinal()])
        {
        default:
            return;

        case 2: // '\002'
            scrollTo(0, i);
            return;

        case 1: // '\001'
            scrollTo(i, 0);
            return;
        }
    }

    public void setLastUpdatedLabel(CharSequence charsequence)
    {
        getLoadingLayoutProxy().setLastUpdatedLabel(charsequence);
    }

    public void setLoadingDrawable(Drawable drawable)
    {
        getLoadingLayoutProxy().setLoadingDrawable(drawable);
    }

    public void setLoadingDrawable(Drawable drawable, Mode mode)
    {
        getLoadingLayoutProxy(mode.showHeaderLoadingLayout(), mode.showFooterLoadingLayout()).setLoadingDrawable(drawable);
    }

    public void setLongClickable(boolean flag)
    {
        getRefreshableView().setLongClickable(flag);
    }

    public final void setMode(Mode mode)
    {
        if (mode != mMode)
        {
            mMode = mode;
            updateUIForMode();
        }
    }

    public final void setMode(Mode mode, boolean flag)
    {
        if (mode != mMode)
        {
            mMode = mode;
            if (flag)
            {
                updateUIForMode();
            }
        }
    }

    public void setOnPullEventListener(OnPullEventListener onpulleventlistener)
    {
        mOnPullEventListener = onpulleventlistener;
    }

    public final void setOnRefreshListener(OnRefreshListener2 onrefreshlistener2)
    {
        mOnRefreshListener2 = onrefreshlistener2;
        mOnRefreshListener = null;
    }

    public final void setOnRefreshListener(OnRefreshListener onrefreshlistener)
    {
        mOnRefreshListener = onrefreshlistener;
        mOnRefreshListener2 = null;
    }

    public void setPullLabel(CharSequence charsequence)
    {
        getLoadingLayoutProxy().setPullLabel(charsequence);
    }

    public void setPullLabel(CharSequence charsequence, Mode mode)
    {
        getLoadingLayoutProxy(mode.showHeaderLoadingLayout(), mode.showFooterLoadingLayout()).setPullLabel(charsequence);
    }

    public final void setPullToRefreshEnabled(boolean flag)
    {
        Mode mode;
        if (flag)
        {
            mode = Mode.getDefault();
        } else
        {
            mode = Mode.DISABLED;
        }
        setMode(mode);
    }

    public final void setPullToRefreshOverScrollEnabled(boolean flag)
    {
        mOverScrollEnabled = flag;
    }

    public final void setRefreshing()
    {
        setRefreshing(true);
    }

    public final void setRefreshing(boolean flag)
    {
        if (!isRefreshing())
        {
            setState(State.MANUAL_REFRESHING, new boolean[] {
                flag
            });
        }
    }

    public void setRefreshingLabel(CharSequence charsequence)
    {
        getLoadingLayoutProxy().setRefreshingLabel(charsequence);
    }

    public void setRefreshingLabel(CharSequence charsequence, Mode mode)
    {
        getLoadingLayoutProxy(mode.showHeaderLoadingLayout(), mode.showFooterLoadingLayout()).setRefreshingLabel(charsequence);
    }

    public void setReleaseLabel(CharSequence charsequence)
    {
        setReleaseLabel(charsequence, Mode.BOTH);
    }

    public void setReleaseLabel(CharSequence charsequence, Mode mode)
    {
        getLoadingLayoutProxy(mode.showHeaderLoadingLayout(), mode.showFooterLoadingLayout()).setReleaseLabel(charsequence);
    }

    public void setScrollAnimationInterpolator(Interpolator interpolator)
    {
        mScrollAnimationInterpolator = interpolator;
    }

    public final void setScrollingWhileRefreshingEnabled(boolean flag)
    {
        mScrollingWhileRefreshingEnabled = flag;
    }

    public final void setShowViewWhileRefreshing(boolean flag)
    {
        mShowViewWhileRefreshing = flag;
    }

    public final transient void setState(State state, boolean aflag[])
    {
        mState = state;
        _cls1.b[mState.ordinal()];
        JVM INSTR tableswitch 1 5: default 52
    //                   1 78
    //                   2 85
    //                   3 92
    //                   4 99
    //                   5 99;
           goto _L1 _L2 _L3 _L4 _L5 _L5
_L1:
        if (mOnPullEventListener != null)
        {
            mOnPullEventListener.onPullEvent(this, mState, mCurrentMode);
        }
        return;
_L2:
        onReset();
        continue; /* Loop/switch isn't completed */
_L3:
        onPullToRefresh();
        continue; /* Loop/switch isn't completed */
_L4:
        onReleaseToRefresh();
        continue; /* Loop/switch isn't completed */
_L5:
        onRefreshing(aflag[0]);
        if (true) goto _L1; else goto _L6
_L6:
    }

    protected final void smoothScrollTo(int i)
    {
        smoothScrollTo(i, getPullToRefreshScrollDuration());
    }

    protected final void smoothScrollTo(int i, a a1)
    {
        smoothScrollTo(i, getPullToRefreshScrollDuration(), 0L, a1);
    }

    protected final void smoothScrollToLonger(int i)
    {
        smoothScrollTo(i, getPullToRefreshScrollDurationLonger());
    }

    protected void updateUIForMode()
    {
        Object obj = getLoadingLayoutLayoutParams();
        if (this == mHeaderLayout.getParent())
        {
            removeView(mHeaderLayout);
        }
        if (mMode.showHeaderLoadingLayout())
        {
            addViewInternal(mHeaderLayout, 0, ((android.view.ViewGroup.LayoutParams) (obj)));
        }
        if (this == mFooterLayout.getParent())
        {
            removeView(mFooterLayout);
        }
        if (mMode.showFooterLoadingLayout())
        {
            addViewInternal(mFooterLayout, ((android.view.ViewGroup.LayoutParams) (obj)));
        }
        refreshLoadingViewsSize();
        if (mMode != Mode.BOTH)
        {
            obj = mMode;
        } else
        {
            obj = Mode.PULL_FROM_START;
        }
        mCurrentMode = ((Mode) (obj));
    }



}
