// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview:
//            PullToRefreshListView2, e, PullToRefreshBase

public class RefreshLoadMoreListView extends PullToRefreshListView2
    implements PullToRefreshBase.OnLastItemVisibleListener, PullToRefreshBase.OnRefreshListener
{
    public static interface RefreshLoadMoreListener
    {

        public abstract void onMore();

        public abstract void onRefresh();
    }


    private ProgressBar footerLoadingBar;
    private TextView footerLoadingTV;
    private View footerView;
    private boolean hasMore;
    public boolean isLoadingMore;
    private RefreshLoadMoreListener refreshLoadMoreListener;

    public RefreshLoadMoreListView(Context context)
    {
        super(context);
        isLoadingMore = true;
        hasMore = true;
        init();
    }

    public RefreshLoadMoreListView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        isLoadingMore = true;
        hasMore = true;
        init();
    }

    public RefreshLoadMoreListView(Context context, PullToRefreshBase.Mode mode)
    {
        super(context, mode);
        isLoadingMore = true;
        hasMore = true;
        init();
    }

    public RefreshLoadMoreListView(Context context, PullToRefreshBase.Mode mode, PullToRefreshBase.AnimationStyle animationstyle)
    {
        super(context, mode, animationstyle);
        isLoadingMore = true;
        hasMore = true;
        init();
    }

    private void init()
    {
        footerView = View.inflate(getContext(), 0x7f0301ee, null);
        footerLoadingBar = (ProgressBar)footerView.findViewById(0x7f0a071d);
        footerLoadingTV = (TextView)footerView.findViewById(0x7f0a071e);
        ((ListView)getRefreshableView()).addFooterView(footerView);
        setOnLastItemVisibleListener(this);
        setOnRefreshListener(this);
        footerView.setOnClickListener(new e(this));
    }

    private boolean isEmpty()
    {
        return ((ListView)getRefreshableView()).getAdapter() == null || ((ListView)getRefreshableView()).getAdapter().getCount() == 0;
    }

    public void finishLoadingMore()
    {
        isLoadingMore = false;
        if (!hasMore)
        {
            footerLoadingBar.setVisibility(8);
            footerLoadingTV.setText(0x7f0901cf);
        }
        if (isEmpty())
        {
            footerLoadingBar.setVisibility(8);
            footerLoadingTV.setText(0x7f0901d0);
        }
    }

    public boolean hasMore()
    {
        return hasMore;
    }

    public void onLastItemVisible()
    {
        if (!isEmpty() && hasMore && !isLoadingMore && refreshLoadMoreListener != null)
        {
            startLoadingMore();
            refreshLoadMoreListener.onMore();
        }
    }

    public void onRefresh(PullToRefreshBase pulltorefreshbase)
    {
        hasMore = true;
        isLoadingMore = false;
        if (refreshLoadMoreListener != null)
        {
            refreshLoadMoreListener.onRefresh();
        }
    }

    public void setFootViewText(int i)
    {
        isLoadingMore = false;
        footerLoadingBar.setVisibility(8);
        footerLoadingTV.setText(i);
    }

    public void setFootViewText(String s)
    {
        footerLoadingBar.setVisibility(8);
        footerLoadingTV.setText(s);
    }

    public void setFooterViewColor(int i)
    {
        if (footerView != null)
        {
            footerView.setBackgroundColor(i);
        }
    }

    public void setHasMore(boolean flag)
    {
        hasMore = flag;
        finishLoadingMore();
        if (flag)
        {
            footerLoadingBar.setVisibility(8);
            footerLoadingTV.setText(0x7f0901ce);
        }
    }

    public void setHasMoreNoFooterView(boolean flag)
    {
        hasMore = flag;
        finishLoadingMore();
        if (flag)
        {
            footerLoadingBar.setVisibility(8);
            footerLoadingTV.setText(0x7f0901ce);
            return;
        } else
        {
            footerLoadingBar.setVisibility(8);
            footerLoadingTV.setVisibility(8);
            return;
        }
    }

    public void setRefreshLoadMoreListener(RefreshLoadMoreListener refreshloadmorelistener)
    {
        refreshLoadMoreListener = refreshloadmorelistener;
    }

    public void startLoadingMore()
    {
        isLoadingMore = true;
        footerLoadingBar.setVisibility(0);
        footerLoadingTV.setVisibility(0);
        footerLoadingTV.setText(0x7f0901cd);
    }
}
