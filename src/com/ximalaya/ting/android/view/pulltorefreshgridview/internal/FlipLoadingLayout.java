// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview.internal;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.ProgressBar;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview.internal:
//            LoadingLayout

public class FlipLoadingLayout extends LoadingLayout
{

    static final int FLIP_ANIMATION_DURATION = 150;
    private final Animation mResetRotateAnimation;
    private final Animation mRotateAnimation;

    public FlipLoadingLayout(Context context, com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode mode, com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation orientation, TypedArray typedarray)
    {
        super(context, mode, orientation, typedarray);
        int i;
        if (mode == com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.PULL_FROM_START)
        {
            i = -180;
        } else
        {
            i = 180;
        }
        mRotateAnimation = new RotateAnimation(0.0F, i, 1, 0.5F, 1, 0.5F);
        mRotateAnimation.setInterpolator(ANIMATION_INTERPOLATOR);
        mRotateAnimation.setDuration(150L);
        mRotateAnimation.setFillAfter(true);
        mResetRotateAnimation = new RotateAnimation(i, 0.0F, 1, 0.5F, 1, 0.5F);
        mResetRotateAnimation.setInterpolator(ANIMATION_INTERPOLATOR);
        mResetRotateAnimation.setDuration(150L);
        mResetRotateAnimation.setFillAfter(true);
    }

    private float getDrawableRotationAngle()
    {
        static class _cls1
        {

            static final int a[];

            static 
            {
                a = new int[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.values().length];
                try
                {
                    a[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.PULL_FROM_END.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    a[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.PULL_FROM_START.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls1.a[mMode.ordinal()];
        JVM INSTR tableswitch 1 2: default 32
    //                   1 34
    //                   2 50;
           goto _L1 _L2 _L3
_L1:
        return 0.0F;
_L2:
        return mScrollDirection != com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.HORIZONTAL ? 180F : 90F;
_L3:
        if (mScrollDirection == com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.HORIZONTAL)
        {
            return 270F;
        }
        if (true) goto _L1; else goto _L4
_L4:
    }

    protected int getDefaultDrawableResId()
    {
        return 0x7f0201b9;
    }

    protected void onLoadingDrawableSet(Drawable drawable)
    {
        if (drawable != null)
        {
            int i = drawable.getIntrinsicHeight();
            int j = drawable.getIntrinsicWidth();
            drawable = mHeaderImage.getLayoutParams();
            int k = Math.max(i, j);
            drawable.height = k;
            drawable.width = k;
            mHeaderImage.requestLayout();
            mHeaderImage.setScaleType(android.widget.ImageView.ScaleType.MATRIX);
            Matrix matrix = new Matrix();
            matrix.postTranslate((float)(((android.view.ViewGroup.LayoutParams) (drawable)).width - j) / 2.0F, (float)(((android.view.ViewGroup.LayoutParams) (drawable)).height - i) / 2.0F);
            matrix.postRotate(getDrawableRotationAngle(), (float)((android.view.ViewGroup.LayoutParams) (drawable)).width / 2.0F, (float)((android.view.ViewGroup.LayoutParams) (drawable)).height / 2.0F);
            mHeaderImage.setImageMatrix(matrix);
        }
    }

    protected void onPullImpl(float f)
    {
    }

    protected void pullToRefreshImpl()
    {
        if (mRotateAnimation == mHeaderImage.getAnimation())
        {
            mHeaderImage.startAnimation(mResetRotateAnimation);
        }
    }

    protected void refreshingImpl()
    {
        mHeaderImage.clearAnimation();
        mHeaderImage.setVisibility(4);
        mHeaderProgress.setVisibility(0);
    }

    protected void releaseToRefreshImpl()
    {
        mHeaderImage.startAnimation(mRotateAnimation);
    }

    protected void resetImpl()
    {
        mHeaderImage.clearAnimation();
        mHeaderProgress.setVisibility(8);
        mHeaderImage.setVisibility(0);
    }
}
