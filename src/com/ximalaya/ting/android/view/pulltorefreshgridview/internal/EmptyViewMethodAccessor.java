// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview.internal;

import android.view.View;

public interface EmptyViewMethodAccessor
{

    public abstract void setEmptyView(View view);

    public abstract void setEmptyViewInternal(View view);
}
