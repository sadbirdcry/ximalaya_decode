// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview.internal;

import android.graphics.drawable.Drawable;
import android.view.View;

public class ViewCompat
{
    static class a
    {

        public static void a(View view, int i)
        {
            view.setLayerType(i, null);
        }
    }

    static class b
    {

        public static void a(View view, Drawable drawable)
        {
            view.setBackground(drawable);
        }

        public static void a(View view, Runnable runnable)
        {
            view.postOnAnimation(runnable);
        }
    }


    public ViewCompat()
    {
    }

    public static void postOnAnimation(View view, Runnable runnable)
    {
        if (android.os.Build.VERSION.SDK_INT >= 16)
        {
            b.a(view, runnable);
            return;
        } else
        {
            view.postDelayed(runnable, 16L);
            return;
        }
    }

    public static void setBackground(View view, Drawable drawable)
    {
        if (android.os.Build.VERSION.SDK_INT >= 16)
        {
            b.a(view, drawable);
            return;
        } else
        {
            view.setBackgroundDrawable(drawable);
            return;
        }
    }

    public static void setLayerType(View view, int i)
    {
        if (android.os.Build.VERSION.SDK_INT >= 11)
        {
            a.a(view, i);
        }
    }
}
