// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview.internal;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

// Referenced classes of package com.ximalaya.ting.android.view.pulltorefreshgridview.internal:
//            ILoadingLayout, ViewCompat, Utils

public abstract class LoadingLayout extends FrameLayout
    implements ILoadingLayout
{

    static final Interpolator ANIMATION_INTERPOLATOR = new LinearInterpolator();
    static final String LOG_TAG = "PullToRefresh-LoadingLayout";
    protected final ImageView mHeaderImage;
    protected final ProgressBar mHeaderProgress;
    private final TextView mHeaderText;
    private FrameLayout mInnerLayout;
    protected final com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode mMode;
    private CharSequence mPullLabel;
    private CharSequence mRefreshingLabel;
    private CharSequence mReleaseLabel;
    protected final com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation mScrollDirection;
    private final TextView mSubHeaderText;
    private boolean mUseIntrinsicAnimation;

    public LoadingLayout(Context context, com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode mode, com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation orientation, TypedArray typedarray)
    {
        super(context);
        mMode = mode;
        mScrollDirection = orientation;
        static class _cls1
        {

            static final int a[];
            static final int b[];

            static 
            {
                b = new int[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.values().length];
                try
                {
                    b[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.PULL_FROM_END.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror3) { }
                try
                {
                    b[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode.PULL_FROM_START.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror2) { }
                a = new int[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.values().length];
                try
                {
                    a[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.HORIZONTAL.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    a[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.VERTICAL.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls1.a[orientation.ordinal()];
        JVM INSTR tableswitch 1 1: default 40
    //                   1 454;
           goto _L1 _L2
_L1:
        LayoutInflater.from(context).inflate(0x7f03019c, this);
_L5:
        android.widget.FrameLayout.LayoutParams layoutparams;
        mInnerLayout = (FrameLayout)findViewById(0x7f0a0646);
        mHeaderText = (TextView)mInnerLayout.findViewById(0x7f0a0649);
        mHeaderProgress = (ProgressBar)mInnerLayout.findViewById(0x7f0a0648);
        mSubHeaderText = (TextView)mInnerLayout.findViewById(0x7f0a064a);
        mHeaderImage = (ImageView)mInnerLayout.findViewById(0x7f0a0647);
        layoutparams = (android.widget.FrameLayout.LayoutParams)mInnerLayout.getLayoutParams();
        _cls1.b[mode.ordinal()];
        JVM INSTR tableswitch 1 1: default 168
    //                   1 468;
           goto _L3 _L4
_L3:
        int i;
        if (orientation == com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.VERTICAL)
        {
            i = 80;
        } else
        {
            i = 5;
        }
        layoutparams.gravity = i;
        mPullLabel = context.getString(0x7f090077);
        mRefreshingLabel = context.getString(0x7f090079);
        mReleaseLabel = context.getString(0x7f090078);
_L6:
        if (typedarray.hasValue(1))
        {
            orientation = typedarray.getDrawable(1);
            if (orientation != null)
            {
                ViewCompat.setBackground(this, orientation);
            }
        }
        if (typedarray.hasValue(10))
        {
            orientation = new TypedValue();
            typedarray.getValue(10, orientation);
            setTextAppearance(((TypedValue) (orientation)).data);
        }
        if (typedarray.hasValue(11))
        {
            orientation = new TypedValue();
            typedarray.getValue(11, orientation);
            setSubTextAppearance(((TypedValue) (orientation)).data);
        }
        if (typedarray.hasValue(2))
        {
            orientation = typedarray.getColorStateList(2);
            if (orientation != null)
            {
                setTextColor(orientation);
            }
        }
        if (typedarray.hasValue(3))
        {
            orientation = typedarray.getColorStateList(3);
            if (orientation != null)
            {
                setSubTextColor(orientation);
            }
        }
        orientation = null;
        if (typedarray.hasValue(6))
        {
            orientation = typedarray.getDrawable(6);
        }
        switch (_cls1.b[mode.ordinal()])
        {
        default:
            if (typedarray.hasValue(7))
            {
                mode = typedarray.getDrawable(7);
            } else
            {
                mode = orientation;
                if (typedarray.hasValue(17))
                {
                    Utils.warnDeprecation("ptrDrawableTop", "ptrDrawableStart");
                    mode = typedarray.getDrawable(17);
                }
            }
            break;

        case 1: // '\001'
            break MISSING_BLOCK_LABEL_561;
        }
_L7:
        orientation = mode;
        if (mode == null)
        {
            orientation = context.getResources().getDrawable(getDefaultDrawableResId());
        }
        setLoadingDrawable(orientation);
        reset();
        return;
_L2:
        LayoutInflater.from(context).inflate(0x7f03019b, this);
          goto _L5
_L4:
        if (orientation == com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.VERTICAL)
        {
            i = 48;
        } else
        {
            i = 3;
        }
        layoutparams.gravity = i;
        mPullLabel = context.getString(0x7f09007a);
        mRefreshingLabel = context.getString(0x7f09007c);
        mReleaseLabel = context.getString(0x7f09007b);
          goto _L6
        if (typedarray.hasValue(8))
        {
            mode = typedarray.getDrawable(8);
        } else
        {
            mode = orientation;
            if (typedarray.hasValue(18))
            {
                Utils.warnDeprecation("ptrDrawableBottom", "ptrDrawableEnd");
                mode = typedarray.getDrawable(18);
            }
        }
          goto _L7
    }

    private void setSubTextAppearance(int i)
    {
        if (mSubHeaderText != null)
        {
            mSubHeaderText.setTextAppearance(getContext(), i);
        }
    }

    private void setSubTextColor(ColorStateList colorstatelist)
    {
        if (mSubHeaderText != null)
        {
            mSubHeaderText.setTextColor(colorstatelist);
        }
    }

    private void setTextAppearance(int i)
    {
        if (mHeaderText != null)
        {
            mHeaderText.setTextAppearance(getContext(), i);
        }
        if (mSubHeaderText != null)
        {
            mSubHeaderText.setTextAppearance(getContext(), i);
        }
    }

    private void setTextColor(ColorStateList colorstatelist)
    {
        if (mHeaderText != null)
        {
            mHeaderText.setTextColor(colorstatelist);
        }
        if (mSubHeaderText != null)
        {
            mSubHeaderText.setTextColor(colorstatelist);
        }
    }

    public final int getContentSize()
    {
        switch (_cls1.a[mScrollDirection.ordinal()])
        {
        default:
            return mInnerLayout.getHeight();

        case 1: // '\001'
            return mInnerLayout.getWidth();
        }
    }

    protected abstract int getDefaultDrawableResId();

    public final void hideAllViews()
    {
        if (mHeaderText.getVisibility() == 0)
        {
            mHeaderText.setVisibility(4);
        }
        if (mHeaderProgress.getVisibility() == 0)
        {
            mHeaderProgress.setVisibility(4);
        }
        if (mHeaderImage.getVisibility() == 0)
        {
            mHeaderImage.setVisibility(8);
        }
        if (mSubHeaderText.getVisibility() == 0)
        {
            mSubHeaderText.setVisibility(4);
        }
    }

    protected abstract void onLoadingDrawableSet(Drawable drawable);

    public final void onPull(float f)
    {
        if (!mUseIntrinsicAnimation)
        {
            onPullImpl(f);
        }
    }

    protected abstract void onPullImpl(float f);

    public final void pullToRefresh()
    {
        if (mHeaderText != null)
        {
            mHeaderText.setText(mPullLabel);
        }
        pullToRefreshImpl();
    }

    protected abstract void pullToRefreshImpl();

    public final void refreshing()
    {
        if (mHeaderText != null)
        {
            mHeaderText.setText(mRefreshingLabel);
        }
        if (mUseIntrinsicAnimation)
        {
            ((AnimationDrawable)mHeaderImage.getDrawable()).start();
        } else
        {
            refreshingImpl();
        }
        if (mSubHeaderText != null)
        {
            mSubHeaderText.setVisibility(8);
        }
    }

    protected abstract void refreshingImpl();

    public final void releaseToRefresh()
    {
        if (mHeaderText != null)
        {
            mHeaderText.setText(mReleaseLabel);
        }
        releaseToRefreshImpl();
    }

    protected abstract void releaseToRefreshImpl();

    public final void reset()
    {
label0:
        {
            if (mHeaderText != null)
            {
                mHeaderText.setText(mPullLabel);
            }
            mHeaderImage.setVisibility(0);
            if (mUseIntrinsicAnimation)
            {
                ((AnimationDrawable)mHeaderImage.getDrawable()).stop();
            } else
            {
                resetImpl();
            }
            if (mSubHeaderText != null)
            {
                if (!TextUtils.isEmpty(mSubHeaderText.getText()))
                {
                    break label0;
                }
                mSubHeaderText.setVisibility(8);
            }
            return;
        }
        mSubHeaderText.setVisibility(0);
    }

    protected abstract void resetImpl();

    public void setHeaderText(String s)
    {
        mHeaderText.setVisibility(0);
        mHeaderText.setText(s);
    }

    public final void setHeight(int i)
    {
        getLayoutParams().height = i;
        requestLayout();
    }

    public void setLastUpdatedLabel(CharSequence charsequence)
    {
        setSubHeaderText(charsequence);
    }

    public final void setLoadingDrawable(Drawable drawable)
    {
        mHeaderImage.setImageDrawable(drawable);
        mUseIntrinsicAnimation = drawable instanceof AnimationDrawable;
        onLoadingDrawableSet(drawable);
    }

    public void setPullLabel(CharSequence charsequence)
    {
        mPullLabel = charsequence;
    }

    public void setRefreshingLabel(CharSequence charsequence)
    {
        mRefreshingLabel = charsequence;
    }

    public void setReleaseLabel(CharSequence charsequence)
    {
        mReleaseLabel = charsequence;
    }

    public void setSubHeaderText(CharSequence charsequence)
    {
        if (mSubHeaderText != null)
        {
            if (TextUtils.isEmpty(charsequence))
            {
                mSubHeaderText.setVisibility(8);
            } else
            {
                mSubHeaderText.setText(charsequence);
                if (8 == mSubHeaderText.getVisibility())
                {
                    mSubHeaderText.setVisibility(0);
                    return;
                }
            }
        }
    }

    public void setTextTypeface(Typeface typeface)
    {
        mHeaderText.setTypeface(typeface);
    }

    public final void setWidth(int i)
    {
        getLayoutParams().width = i;
        requestLayout();
    }

    public final void showInvisibleViews()
    {
        if (4 == mHeaderText.getVisibility())
        {
            mHeaderText.setVisibility(0);
        }
        if (4 == mHeaderProgress.getVisibility())
        {
            mHeaderProgress.setVisibility(0);
        }
        if (4 == mHeaderImage.getVisibility())
        {
            mHeaderImage.setVisibility(0);
        }
        if (4 == mSubHeaderText.getVisibility())
        {
            mSubHeaderText.setVisibility(0);
        }
    }

}
