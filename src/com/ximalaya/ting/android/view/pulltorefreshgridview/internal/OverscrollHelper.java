// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.pulltorefreshgridview.internal;

import android.view.View;
import com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase;

public final class OverscrollHelper
{

    static final float DEFAULT_OVERSCROLL_SCALE = 1F;
    static final String LOG_TAG = "OverscrollHelper";

    public OverscrollHelper()
    {
    }

    public static boolean isAndroidOverScrollEnabled(View view)
    {
        return view.getOverScrollMode() != 2;
    }

    public static void overScrollBy(PullToRefreshBase pulltorefreshbase, int i, int j, int k, int l, int i1, int j1, float f, 
            boolean flag)
    {
        static class _cls1
        {

            static final int a[];

            static 
            {
                a = new int[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.values().length];
                try
                {
                    a[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.HORIZONTAL.ordinal()] = 1;
                }
                catch (NoSuchFieldError nosuchfielderror1) { }
                try
                {
                    a[com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Orientation.VERTICAL.ordinal()] = 2;
                }
                catch (NoSuchFieldError nosuchfielderror)
                {
                    return;
                }
            }
        }

        _cls1.a[pulltorefreshbase.getPullToRefreshScrollDirection().ordinal()];
        JVM INSTR tableswitch 1 1: default 28
    //                   1 126;
           goto _L1 _L2
_L1:
        int k1 = pulltorefreshbase.getScrollY();
        j = l;
        i = k;
        k = k1;
_L13:
        if (!pulltorefreshbase.isPullToRefreshOverScrollEnabled() || pulltorefreshbase.isRefreshing()) goto _L4; else goto _L3
_L3:
        com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.Mode mode = pulltorefreshbase.getMode();
        if (!mode.permitsPullToRefresh() || flag || i == 0) goto _L6; else goto _L5
_L5:
        i += j;
        if (i >= 0 - j1) goto _L8; else goto _L7
_L7:
        if (mode.showHeaderLoadingLayout())
        {
            if (k == 0)
            {
                pulltorefreshbase.setState(com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.State.OVERSCROLLING, new boolean[0]);
            }
            pulltorefreshbase.setHeaderScroll((int)((float)(k + i) * f));
        }
_L4:
        return;
_L2:
        k = pulltorefreshbase.getScrollX();
        continue; /* Loop/switch isn't completed */
_L8:
        if (i <= i1 + j1)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (!mode.showFooterLoadingLayout()) goto _L4; else goto _L9
_L9:
        if (k == 0)
        {
            pulltorefreshbase.setState(com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.State.OVERSCROLLING, new boolean[0]);
        }
        pulltorefreshbase.setHeaderScroll((int)((float)((k + i) - i1) * f));
        return;
        if (Math.abs(i) > j1 && Math.abs(i - i1) > j1) goto _L4; else goto _L10
_L10:
        pulltorefreshbase.setState(com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.State.RESET, new boolean[0]);
        return;
_L6:
        if (!flag || com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.State.OVERSCROLLING != pulltorefreshbase.getState()) goto _L4; else goto _L11
_L11:
        pulltorefreshbase.setState(com.ximalaya.ting.android.view.pulltorefreshgridview.PullToRefreshBase.State.RESET, new boolean[0]);
        return;
        if (true) goto _L13; else goto _L12
_L12:
    }

    public static void overScrollBy(PullToRefreshBase pulltorefreshbase, int i, int j, int k, int l, int i1, boolean flag)
    {
        overScrollBy(pulltorefreshbase, i, j, k, l, i1, 0, 1.0F, flag);
    }

    public static void overScrollBy(PullToRefreshBase pulltorefreshbase, int i, int j, int k, int l, boolean flag)
    {
        overScrollBy(pulltorefreshbase, i, j, k, l, 0, flag);
    }
}
