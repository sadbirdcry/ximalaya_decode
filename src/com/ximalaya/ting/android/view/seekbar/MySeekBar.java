// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.seekbar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewParent;
import android.widget.SeekBar;
import com.ximalaya.ting.android.util.ToolUtil;
import java.text.DecimalFormat;

public class MySeekBar extends SeekBar
{
    public static interface ProgressNubmerFormat
    {

        public abstract String format(int i, int j);

        public abstract String getFormatString(int i, int j);
    }


    private boolean isCanSeek;
    private boolean mIsSetProgress;
    private ProgressNubmerFormat mNubmerFormat;
    private String mText;
    private float mTextPaddingBottom;
    private float mTextPaddingLeft;
    private float mTextPaddingRight;
    private float mTextPaddingTop;
    private TextPaint mTextPaint;
    private float mTextStartX;
    private float mTextStartY;
    private float mThumbHeight;
    private float mThumbWidth;

    public MySeekBar(Context context)
    {
        super(context);
        isCanSeek = true;
    }

    public MySeekBar(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        isCanSeek = true;
    }

    public MySeekBar(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        isCanSeek = true;
    }

    private void initTextPaint()
    {
        if (mTextPaddingLeft == 0.0F && mTextPaddingTop == 0.0F && mTextPaddingRight == 0.0F && mTextPaddingBottom == 0.0F)
        {
            mTextPaddingLeft = ToolUtil.dp2px(getContext(), 10F);
            mTextPaddingTop = ToolUtil.dp2px(getContext(), 2.0F);
            mTextPaddingRight = ToolUtil.dp2px(getContext(), 3F);
            mTextPaddingBottom = ToolUtil.dp2px(getContext(), 2.0F);
        }
        if (mTextPaint == null)
        {
            mTextPaint = new TextPaint();
            mTextPaint.setColor(getResources().getColor(0x7f070030));
            mTextPaint.setAntiAlias(true);
        }
        if (mText.length() > 5)
        {
            mTextPaint.setTextSize(ToolUtil.dp2px(getContext(), 7F));
            return;
        } else
        {
            mTextPaint.setTextSize(ToolUtil.dp2px(getContext(), 10F));
            return;
        }
    }

    private void updateText()
    {
        int i = getProgress();
        int j = getMax();
        float f;
        float f1;
        float f2;
        if (mNubmerFormat != null)
        {
            mText = mNubmerFormat.format(i, j);
        } else
        {
            mText = (new StringBuilder()).append(String.valueOf((new DecimalFormat("0.0")).format((float)(i * 100) / (float)j))).append("%").toString();
        }
        initTextPaint();
        f1 = (getWidth() - getPaddingLeft() - getPaddingRight()) + getThumbOffset() * 2;
        f2 = mThumbWidth;
        f = 0.0F;
        if (getMax() != 0)
        {
            f = (float)getProgress() / (float)getMax();
        }
        mTextStartX = f * (f1 - f2) + mTextPaddingLeft;
        mTextStartY = mThumbHeight / 2.0F + mTextPaddingTop;
    }

    public boolean isCanSeek()
    {
        return isCanSeek;
    }

    protected void onDraw(Canvas canvas)
    {
        this;
        JVM INSTR monitorenter ;
        super.onDraw(canvas);
        if (mIsSetProgress)
        {
            updateText();
        }
        canvas.save();
        if (android.os.Build.VERSION.SDK_INT < 8)
        {
            break MISSING_BLOCK_LABEL_58;
        }
        canvas.drawText(mText, mTextStartX, mTextStartY, mTextPaint);
_L1:
        canvas.restore();
        this;
        JVM INSTR monitorexit ;
        return;
        canvas.drawText(mText, mTextStartX - (float)ToolUtil.dp2px(getContext(), 20F), mTextStartY, mTextPaint);
          goto _L1
        canvas;
        throw canvas;
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        getParent().requestDisallowInterceptTouchEvent(true);
        if (!isCanSeek)
        {
            return true;
        } else
        {
            return super.onTouchEvent(motionevent);
        }
    }

    public void setCanSeek(boolean flag)
    {
        isCanSeek = flag;
    }

    public void setProgress(int i)
    {
        this;
        JVM INSTR monitorenter ;
        super.setProgress(i);
        mIsSetProgress = true;
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    public void setProgressNumberFormat(ProgressNubmerFormat progressnubmerformat)
    {
        mNubmerFormat = progressnubmerformat;
    }

    public void setText(String s)
    {
        mIsSetProgress = false;
        mText = s;
        initTextPaint();
        float f1 = (getWidth() - getPaddingLeft() - getPaddingRight()) + getThumbOffset() * 2;
        float f2 = mThumbWidth;
        float f = 0.0F;
        if (getMax() != 0)
        {
            f = (float)getProgress() / (float)getMax();
        }
        mTextStartX = f * (f1 - f2) + mTextPaddingLeft;
        mTextStartY = mThumbHeight / 2.0F + mTextPaddingTop;
        invalidate();
    }

    public void setTextPadding(float f, float f1, float f2, float f3)
    {
        mTextPaddingLeft = f;
        mTextPaddingTop = f1;
        mTextPaddingRight = f2;
        mTextPaddingBottom = f3;
    }

    public void setThumb(Drawable drawable)
    {
        super.setThumb(drawable);
        if (drawable != null)
        {
            mThumbWidth = drawable.getIntrinsicWidth();
            mThumbHeight = drawable.getIntrinsicHeight();
        }
    }
}
