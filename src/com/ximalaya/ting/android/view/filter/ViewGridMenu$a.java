// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.filter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.model.category.CategoryMenu;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.filter:
//            ViewGridMenu

private class a extends BaseAdapter
{

    public String a;
    final ViewGridMenu b;

    public int getCount()
    {
        if (ViewGridMenu.access$000(b) != null)
        {
            return ViewGridMenu.access$000(b).size();
        } else
        {
            return 0;
        }
    }

    public Object getItem(int i)
    {
        if (ViewGridMenu.access$000(b) == null || i >= ViewGridMenu.access$000(b).size())
        {
            return null;
        } else
        {
            return ViewGridMenu.access$000(b).get(i);
        }
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (view == null)
        {
            view = LayoutInflater.from(ViewGridMenu.access$100(b)).inflate(0x7f03012e, viewgroup, false);
        }
        while (ViewGridMenu.access$000(b) == null || i >= ViewGridMenu.access$000(b).size() || (CategoryMenu)ViewGridMenu.access$000(b).get(i) == null) 
        {
            return view;
        }
        viewgroup = (TextView)view;
        String s = ((CategoryMenu)ViewGridMenu.access$000(b).get(i)).title;
        viewgroup.setText(s);
        if (a.equals(s))
        {
            viewgroup.setTextColor(ViewGridMenu.access$100(b).getResources().getColor(0x7f070004));
            return view;
        } else
        {
            viewgroup.setTextColor(ViewGridMenu.access$100(b).getResources().getColor(0x7f070026));
            return view;
        }
    }

    public (ViewGridMenu viewgridmenu)
    {
        b = viewgridmenu;
        super();
        a = "";
    }
}
