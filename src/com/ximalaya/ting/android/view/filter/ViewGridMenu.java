// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.filter;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.model.category.CategoryMenu;
import com.ximalaya.ting.android.modelmanage.FilterManage;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.filter:
//            FilterViewBaseAction, a

public class ViewGridMenu extends RelativeLayout
    implements FilterViewBaseAction
{
    public static interface OnSelectListener
    {

        public abstract void getValue(String s, String s1);
    }

    private class a extends BaseAdapter
    {

        public String a;
        final ViewGridMenu b;

        public int getCount()
        {
            if (b.menuList != null)
            {
                return b.menuList.size();
            } else
            {
                return 0;
            }
        }

        public Object getItem(int i)
        {
            if (b.menuList == null || i >= b.menuList.size())
            {
                return null;
            } else
            {
                return b.menuList.get(i);
            }
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            if (view == null)
            {
                view = LayoutInflater.from(b.mContext).inflate(0x7f03012e, viewgroup, false);
            }
            while (b.menuList == null || i >= b.menuList.size() || (CategoryMenu)b.menuList.get(i) == null) 
            {
                return view;
            }
            viewgroup = (TextView)view;
            String s = ((CategoryMenu)b.menuList.get(i)).title;
            viewgroup.setText(s);
            if (a.equals(s))
            {
                viewgroup.setTextColor(b.mContext.getResources().getColor(0x7f070004));
                return view;
            } else
            {
                viewgroup.setTextColor(b.mContext.getResources().getColor(0x7f070026));
                return view;
            }
        }

        public a()
        {
            b = ViewGridMenu.this;
            super();
            a = "";
        }
    }


    private GridView gridView;
    private a gridViewAdapter;
    private Context mContext;
    private OnSelectListener mOnSelectListener;
    private List menuList;
    private int type;

    public ViewGridMenu(Context context)
    {
        super(context);
        type = 0;
        init(context);
    }

    public ViewGridMenu(Context context, int i)
    {
        super(context);
        type = 0;
        type = i;
        init(context);
    }

    public ViewGridMenu(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        type = 0;
        init(context);
    }

    private void init(Context context)
    {
        mContext = context;
        if (type == 0)
        {
            menuList = FilterManage.getInstance().getPersonMenuTag(context);
        } else
        {
            menuList = FilterManage.getInstance().getSoundMenuTag(context);
        }
        gridView = (GridView)((RelativeLayout)LayoutInflater.from(context).inflate(0x7f030056, this, true)).findViewById(0x7f0a01a9);
        gridViewAdapter = new a();
        gridView.setAdapter(gridViewAdapter);
        if (menuList != null && menuList.size() > 0)
        {
            gridViewAdapter.a = ((CategoryMenu)menuList.get(0)).title;
        }
        gridView.setOnItemClickListener(new com.ximalaya.ting.android.view.filter.a(this));
    }

    public void hide()
    {
    }

    public void setOnSelectListener(OnSelectListener onselectlistener)
    {
        mOnSelectListener = onselectlistener;
    }

    public void setSelectedString(String s)
    {
        if (gridViewAdapter != null)
        {
            gridViewAdapter.a = s;
            gridViewAdapter.notifyDataSetChanged();
        }
    }

    public void show()
    {
    }




}
