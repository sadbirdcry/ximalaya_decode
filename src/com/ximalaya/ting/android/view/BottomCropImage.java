// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class BottomCropImage extends ImageView
{

    public BottomCropImage(Context context)
    {
        super(context);
        setup();
    }

    public BottomCropImage(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        setup();
    }

    public BottomCropImage(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        setup();
    }

    private void setup()
    {
        setScaleType(android.widget.ImageView.ScaleType.MATRIX);
    }

    protected boolean setFrame(int i, int j, int k, int l)
    {
        Matrix matrix = getImageMatrix();
        int i1 = getWidth() - getPaddingLeft() - getPaddingRight();
        int j1 = getHeight() - getPaddingTop() - getPaddingBottom();
        Drawable drawable = getDrawable();
        if (drawable != null)
        {
            int k1 = drawable.getIntrinsicWidth();
            int l1 = drawable.getIntrinsicHeight();
            float f;
            if (k1 * j1 > l1 * i1)
            {
                f = (float)j1 / (float)l1;
            } else
            {
                f = (float)i1 / (float)k1;
            }
            matrix.setRectToRect(new RectF(0.0F, (float)l1 - (float)j1 / f, k1, l1), new RectF(0.0F, 0.0F, i1, j1), android.graphics.Matrix.ScaleToFit.FILL);
            setImageMatrix(matrix);
        }
        return super.setFrame(i, j, k, l);
    }
}
