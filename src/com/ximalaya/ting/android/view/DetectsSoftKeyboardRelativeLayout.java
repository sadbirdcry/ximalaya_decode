// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import com.ximalaya.ting.android.util.ToolUtil;

public class DetectsSoftKeyboardRelativeLayout extends RelativeLayout
{
    public static interface Listener
    {

        public abstract void onSoftKeyboardShown(boolean flag);
    }


    private Listener listener;

    public DetectsSoftKeyboardRelativeLayout(Context context)
    {
        super(context);
    }

    public DetectsSoftKeyboardRelativeLayout(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    public DetectsSoftKeyboardRelativeLayout(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
    }

    protected void onMeasure(int i, int j)
    {
        if (getContext() instanceof Activity)
        {
            int k = android.view.View.MeasureSpec.getSize(j);
            Activity activity = (Activity)getContext();
            Rect rect = new Rect();
            activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
            int l = rect.top;
            boolean flag;
            if (ToolUtil.getScreenHeight(getContext()) - l - k > 128)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            if (listener != null)
            {
                listener.onSoftKeyboardShown(flag);
            }
        }
        super.onMeasure(i, j);
    }

    public void setListener(Listener listener1)
    {
        listener = listener1;
    }
}
