// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Display;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import com.ximalaya.ting.android.activity.MainTabActivity2;

public class SlideView extends RelativeLayout
{
    public static interface OnFinishListener
    {

        public abstract boolean onFinish();
    }

    public static interface SlideListener
    {

        public abstract void slideEnd();

        public abstract void slideStart();
    }


    private static final int VELOCITY = 50;
    private int bgShadeMargin;
    private boolean canSlide;
    private boolean canSlideRight;
    private RelativeLayout contentView;
    private boolean forbidSlide;
    private boolean isFinish;
    private boolean isFirst;
    private Drawable mBackground;
    private Context mContext;
    private boolean mIsBeingDragged;
    private float mLastMotionX;
    private float mLastMotionY;
    private OnFinishListener mOnFinishListener;
    private Scroller mScroller;
    private SlideListener mSlideListener;
    private int mTouchSlop;
    private VelocityTracker mVelocityTracker;
    private View mainView;
    private int screenWidth;

    public SlideView(Context context)
    {
        super(context);
        bgShadeMargin = 10;
        mIsBeingDragged = false;
        canSlide = true;
        forbidSlide = false;
        canSlideRight = false;
        isFinish = false;
        isFirst = true;
        init(context);
    }

    public SlideView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        bgShadeMargin = 10;
        mIsBeingDragged = false;
        canSlide = true;
        forbidSlide = false;
        canSlideRight = false;
        isFinish = false;
        isFirst = true;
        init(context);
    }

    public SlideView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        bgShadeMargin = 10;
        mIsBeingDragged = false;
        canSlide = true;
        forbidSlide = false;
        canSlideRight = false;
        isFinish = false;
        isFirst = true;
        init(context);
    }

    private void changeAlpha()
    {
        if (mainView == null)
        {
            return;
        } else
        {
            Drawable drawable = mainView.getBackground();
            drawable.setAlpha((int)((((double)(screenWidth + mainView.getScrollX()) + 0.0D) / (double)screenWidth) * 225D));
            mainView.setBackgroundDrawable(drawable);
            return;
        }
    }

    private void finish()
    {
        if (!isFinish)
        {
            isFinish = true;
            if (mContext != null && (mOnFinishListener == null || !mOnFinishListener.onFinish()))
            {
                if (mainView != null)
                {
                    mainView.setVisibility(8);
                }
                if (mContext instanceof MainTabActivity2)
                {
                    ((MainTabActivity2)mContext).onBack();
                    return;
                }
                if ((mContext instanceof Activity) && !((Activity)mContext).isFinishing())
                {
                    ((Activity)mContext).finish();
                    return;
                }
            }
        }
    }

    private void init(Context context)
    {
        mContext = context;
        mTouchSlop = ViewConfiguration.get(mContext).getScaledTouchSlop();
        mScroller = new Scroller(mContext);
        mBackground = getBackground();
        setBackgroundResource(0);
        setBackgroundColor(Color.alpha(0));
        if (mContext instanceof Activity)
        {
            screenWidth = ((Activity)mContext).getWindowManager().getDefaultDisplay().getWidth();
        }
        context = View.inflate(mContext, 0x7f0301c9, null);
        addViewInLayout(context, -1, new android.view.ViewGroup.LayoutParams(-1, -1));
        mainView = context.findViewById(0x7f090085);
        contentView = (RelativeLayout)context.findViewById(0x7f0a0056);
        if (contentView != null && mBackground != null)
        {
            contentView.setBackgroundDrawable(mBackground);
        }
    }

    private void smoothScrollTo(int i)
    {
        if (mainView == null)
        {
            return;
        } else
        {
            int j = mainView.getScrollX();
            mScroller.startScroll(j, mainView.getScrollY(), i, mainView.getScrollY(), 500);
            invalidate();
            return;
        }
    }

    public void addView(View view)
    {
        addView(view, -1);
    }

    public void addView(View view, int i)
    {
        android.view.ViewGroup.LayoutParams layoutparams1 = view.getLayoutParams();
        android.view.ViewGroup.LayoutParams layoutparams = layoutparams1;
        if (layoutparams1 == null)
        {
            android.view.ViewGroup.LayoutParams layoutparams2 = generateDefaultLayoutParams();
            layoutparams = layoutparams2;
            if (layoutparams2 == null)
            {
                throw new IllegalArgumentException("generateDefaultLayoutParams() cannot return null");
            }
        }
        addView(view, i, layoutparams);
    }

    public void addView(View view, int i, int j)
    {
        addView(view, i, j);
    }

    public void addView(View view, int i, android.view.ViewGroup.LayoutParams layoutparams)
    {
        if (contentView != null)
        {
            contentView.addView(view, i, layoutparams);
            return;
        } else
        {
            super.addView(view, i, layoutparams);
            return;
        }
    }

    public void addView(View view, android.view.ViewGroup.LayoutParams layoutparams)
    {
        addView(view, -1, layoutparams);
    }

    public void computeScroll()
    {
        while (mainView == null || mScroller.isFinished() || !mScroller.computeScrollOffset()) 
        {
            return;
        }
        int i = mainView.getScrollX();
        int j = mainView.getScrollY();
        int k = mScroller.getCurrX();
        int l = mScroller.getCurrY();
        if (i != k || j != l)
        {
            mainView.scrollTo(k, l);
            changeAlpha();
            if (mainView.getScrollX() < -screenWidth + 10)
            {
                finish();
            }
        }
        postInvalidate();
    }

    protected void onFinishInflate()
    {
        super.onFinishInflate();
        if (contentView != null && contentView.getChildCount() == 1 && (contentView.getChildAt(0) instanceof RelativeLayout) && (mainView instanceof ViewGroup))
        {
            View view = contentView.getChildAt(0);
            contentView.removeAllViews();
            contentView = null;
            ((ViewGroup)mainView).addView(view);
            contentView = (RelativeLayout)view;
        }
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        if (!forbidSlide) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        if (!canSlide) goto _L4; else goto _L3
_L3:
        float f;
        float f1;
        int i;
        i = motionevent.getAction();
        f1 = motionevent.getX();
        f = motionevent.getY();
        i;
        JVM INSTR tableswitch 0 2: default 60
    //                   0 72
    //                   1 60
    //                   2 90;
           goto _L5 _L6 _L5 _L7
_L5:
        float f2;
        if (mIsBeingDragged)
        {
            return mIsBeingDragged;
        } else
        {
            return super.onInterceptTouchEvent(motionevent);
        }
_L6:
        mLastMotionX = f1;
        mLastMotionY = f;
        mIsBeingDragged = false;
          goto _L5
_L7:
        f1 -= mLastMotionX;
        f2 = Math.abs(f1);
        f = Math.abs(f - mLastMotionY);
        if (canSlideRight && f1 < 0.0F && f2 > (float)mTouchSlop) goto _L1; else goto _L8
_L8:
        if (f2 > (float)mTouchSlop && f2 > f)
        {
            mIsBeingDragged = true;
            isFirst = true;
        }
          goto _L5
_L4:
        return super.onInterceptTouchEvent(motionevent);
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        float f;
        float f1;
        float f2;
        int i;
        f1 = 0.0F;
        if (mVelocityTracker == null)
        {
            mVelocityTracker = VelocityTracker.obtain();
        }
        mVelocityTracker.addMovement(motionevent);
        i = motionevent.getAction();
        f = motionevent.getX();
        f2 = motionevent.getY();
        if (isFirst)
        {
            isFirst = false;
            if (mSlideListener != null)
            {
                mSlideListener.slideStart();
            }
        }
        i;
        JVM INSTR tableswitch 0 3: default 100
    //                   0 102
    //                   1 248
    //                   2 133
    //                   3 248;
           goto _L1 _L2 _L3 _L4 _L3
_L1:
        return true;
_L2:
        if (!mScroller.isFinished())
        {
            mScroller.abortAnimation();
        }
        mLastMotionX = f;
        mLastMotionY = f2;
        continue; /* Loop/switch isn't completed */
_L4:
        if (mainView != null)
        {
            float f5 = mLastMotionX - f;
            mLastMotionX = f;
            float f6 = mainView.getScrollX();
            float f3 = f6 + f5;
            f = f3;
            if (f3 > 0.0F)
            {
                f = 0.0F;
            }
            if (f5 < 0.0F && f6 < 0.0F)
            {
                float f4 = -screenWidth;
                if (f > 0.0F)
                {
                    f = f1;
                } else
                if (f < f4)
                {
                    f = f4;
                }
            }
            mainView.scrollTo((int)f, mainView.getScrollY());
            changeAlpha();
        }
        continue; /* Loop/switch isn't completed */
_L3:
        int j;
        if (mainView == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (mSlideListener != null)
        {
            mSlideListener.slideEnd();
        }
        motionevent = mVelocityTracker;
        motionevent.computeCurrentVelocity(100);
        f = motionevent.getXVelocity();
        j = mainView.getScrollX();
        if (j > 0)
        {
            break MISSING_BLOCK_LABEL_399;
        }
        if (f <= 50F)
        {
            break; /* Loop/switch isn't completed */
        }
        i = -screenWidth - j;
_L6:
        if (j > 0)
        {
            i = -j;
        }
        smoothScrollTo(i);
        if (true) goto _L1; else goto _L5
_L5:
        if (f < -50F)
        {
            i = -j;
        } else
        if (j < -screenWidth / 2)
        {
            i = -screenWidth - j;
        } else
        {
            if (j < -screenWidth / 2)
            {
                break MISSING_BLOCK_LABEL_399;
            }
            i = -j;
        }
          goto _L6
        i = 0;
          goto _L6
    }

    public void setForbidSlide(boolean flag)
    {
        forbidSlide = flag;
    }

    public void setOnFinishListener(OnFinishListener onfinishlistener)
    {
        mOnFinishListener = onfinishlistener;
    }

    public void setOnSlideListener(SlideListener slidelistener)
    {
        mSlideListener = slidelistener;
    }

    public void setSlide(boolean flag)
    {
        canSlide = flag;
    }

    public void setSlideRight(boolean flag)
    {
        canSlideRight = flag;
    }

    public void show()
    {
        if (mainView != null)
        {
            mainView.setVisibility(0);
        }
    }
}
