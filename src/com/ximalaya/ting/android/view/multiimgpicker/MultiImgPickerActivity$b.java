// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;

import android.app.ProgressDialog;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.multiimgpicker.adapter.ImageGridAdapter;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgBucket;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker:
//            MultiImgPickerActivity, ImageFetcher, f

private class <init> extends MyAsyncTask
{

    ProgressDialog a;
    final MultiImgPickerActivity b;

    protected transient List a(Void avoid[])
    {
        return ImageFetcher.getInstance(b.getApplicationContext()).getImagesBucketList();
    }

    protected void a(List list)
    {
        MultiImgPickerActivity.access$102(b, list);
        MultiImgPickerActivity.access$200(b);
        MultiImgPickerActivity.access$300(b).clear();
        if (!MultiImgPickerActivity.access$100(b).isEmpty())
        {
            MultiImgPickerActivity.access$300(b).addAll(((ImgBucket)MultiImgPickerActivity.access$100(b).get(0)).getImageList());
        }
        MultiImgPickerActivity.access$400(b).notifyDataSetChanged();
        a.cancel();
        a = null;
    }

    protected Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((List)obj);
    }

    protected void onPreExecute()
    {
        if (a == null)
        {
            a = ToolUtil.createProgressDialog(MyApplication.a(), 0, true, true);
        }
        a.setMessage("\u52A0\u8F7D\u4E2D");
        a.show();
    }

    private (MultiImgPickerActivity multiimgpickeractivity)
    {
        b = multiimgpickeractivity;
        super();
    }

    b(MultiImgPickerActivity multiimgpickeractivity, f f)
    {
        this(multiimgpickeractivity);
    }
}
