// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.multiimgpicker.adapter.ImageGridAdapter;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgBucket;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker:
//            f, MultiImgZoomPickerActivity, g, h, 
//            i, j, ImageFetcher

public class MultiImgPickerActivity extends BaseActivity
    implements android.view.View.OnClickListener, com.ximalaya.ting.android.view.multiimgpicker.adapter.ImageGridAdapter.OnSelectChangedListener
{
    public class PopupWindows extends PopupWindow
    {

        a bucketAdapter;
        ListView listView;
        final MultiImgPickerActivity this$0;

        public PopupWindows(Context context, View view, List list)
        {
            this$0 = MultiImgPickerActivity.this;
            super(context, null, 0x7f0b0013);
            view = View.inflate(context, 0x7f03011a, null);
            listView = (ListView)view.findViewById(0x7f0a047a);
            bucketAdapter = new a(context, list);
            listView.setAdapter(bucketAdapter);
            listView.setOnItemClickListener(new g(this, MultiImgPickerActivity.this, list));
            listView.setOnKeyListener(new h(this, MultiImgPickerActivity.this));
            setWidth(-1);
            setHeight((ToolUtil.getScreenHeight(context) / 3) * 2);
            setFocusable(true);
            setOutsideTouchable(true);
            setContentView(view);
            setAnimationStyle(0x7f0b0013);
            update();
            getContentView().setOnTouchListener(new i(this, MultiImgPickerActivity.this));
            setOnDismissListener(new j(this, MultiImgPickerActivity.this));
        }
    }

    private class a extends BaseAdapter
    {

        final MultiImgPickerActivity a;
        private List b;
        private Context c;

        public int getCount()
        {
            if (b == null)
            {
                return 0;
            } else
            {
                return b.size();
            }
        }

        public Object getItem(int i)
        {
            if (i + 1 > b.size())
            {
                return Integer.valueOf(0);
            } else
            {
                return b.get(i);
            }
        }

        public long getItemId(int i)
        {
            return 0L;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            Object obj;
            ImgBucket imgbucket;
            Object obj1;
            if (view == null)
            {
                view = LayoutInflater.from(c).inflate(0x7f030119, null);
                viewgroup = new a(this);
                a.a(viewgroup, (ImageView)view.findViewById(0x7f0a0476));
                a.a(viewgroup).setTag(0x7f0a0037, Boolean.valueOf(true));
                a.a(viewgroup).setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
                a.a(viewgroup, (TextView)view.findViewById(0x7f0a0478));
                a.b(viewgroup, (TextView)view.findViewById(0x7f0a0479));
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (a)view.getTag();
            }
            imgbucket = (ImgBucket)b.get(i);
            obj1 = a.b(viewgroup);
            if (imgbucket.getBucketName() == null)
            {
                obj = "";
            } else
            {
                obj = imgbucket.getBucketName();
            }
            ((TextView) (obj1)).setText(((CharSequence) (obj)));
            a.c(viewgroup).setText((new StringBuilder()).append(imgbucket.getCount()).append("\u5F20").toString());
            obj = ImageManager2.from(c);
            obj1 = a.a(viewgroup);
            if (imgbucket.getImageList().isEmpty())
            {
                viewgroup = null;
            } else
            {
                viewgroup = ((ImgItem)imgbucket.getImageList().get(0)).getPath();
            }
            ((ImageManager2) (obj)).displayImage(((ImageView) (obj1)), ToolUtil.addFilePrefix(viewgroup), 0x7f0200dc, ToolUtil.dp2px(c, 60F), ToolUtil.dp2px(c, 60F));
            return view;
        }

        public a(Context context, List list)
        {
            a = MultiImgPickerActivity.this;
            super();
            b = new ArrayList();
            c = context;
            b = list;
        }
    }

    class a.a
    {

        final a a;
        private ImageView b;
        private TextView c;
        private TextView d;

        static ImageView a(a.a a1)
        {
            return a1.b;
        }

        static ImageView a(a.a a1, ImageView imageview)
        {
            a1.b = imageview;
            return imageview;
        }

        static TextView a(a.a a1, TextView textview)
        {
            a1.c = textview;
            return textview;
        }

        static TextView b(a.a a1)
        {
            return a1.c;
        }

        static TextView b(a.a a1, TextView textview)
        {
            a1.d = textview;
            return textview;
        }

        static TextView c(a.a a1)
        {
            return a1.d;
        }

        a.a(a a1)
        {
            a = a1;
            super();
        }
    }

    private class b extends MyAsyncTask
    {

        ProgressDialog a;
        final MultiImgPickerActivity b;

        protected transient List a(Void avoid[])
        {
            return ImageFetcher.getInstance(b.getApplicationContext()).getImagesBucketList();
        }

        protected void a(List list)
        {
            b.mBucketList = list;
            b.buildBucketAll();
            b.mDataList.clear();
            if (!b.mBucketList.isEmpty())
            {
                b.mDataList.addAll(((ImgBucket)b.mBucketList.get(0)).getImageList());
            }
            b.mAdapter.notifyDataSetChanged();
            a.cancel();
            a = null;
        }

        protected Object doInBackground(Object aobj[])
        {
            return a((Void[])aobj);
        }

        protected void onPostExecute(Object obj)
        {
            a((List)obj);
        }

        protected void onPreExecute()
        {
            if (a == null)
            {
                a = ToolUtil.createProgressDialog(MyApplication.a(), 0, true, true);
            }
            a.setMessage("\u52A0\u8F7D\u4E2D");
            a.show();
        }

        private b()
        {
            b = MultiImgPickerActivity.this;
            super();
        }

        b(f f1)
        {
            this();
        }
    }


    private static final int SELECT_PICTURE = 1;
    private ImageGridAdapter mAdapter;
    private List mBucketList;
    private PopupWindows mBucketListMenu;
    private TextView mBucketSelectBtn;
    private int mCanAddSize;
    private List mDataList;
    private View mDimBg;
    private TextView mFinishBtn;
    private GridView mGridView;
    private TextView mPreViewBtn;
    private List mSelectedImgs;
    private TextView mTitleTv;

    public MultiImgPickerActivity()
    {
        mDataList = new ArrayList();
        mBucketList = new ArrayList();
        mSelectedImgs = new ArrayList();
    }

    private void buildBucketAll()
    {
        int k = mBucketList.size();
        ImgBucket imgbucket = new ImgBucket();
        imgbucket.setBucketName(getString(0x7f0901e1));
        ArrayList arraylist = new ArrayList();
        int i = 0;
        int j = 0;
        for (; i != k; i++)
        {
            j += ((ImgBucket)mBucketList.get(i)).getCount();
            arraylist.addAll(((ImgBucket)mBucketList.get(i)).getImageList());
        }

        imgbucket.setCount(j);
        imgbucket.setImageList(arraylist);
        mBucketList.add(0, imgbucket);
    }

    private void initData()
    {
        int i = getIntent().getIntExtra("max_size", 9);
        mCanAddSize = getIntent().getIntExtra("can_add_size", i);
        mSelectedImgs.clear();
        mGridView = (GridView)findViewById(0x7f0a004e);
        mGridView.setSelector(new ColorDrawable(0));
        mAdapter = new ImageGridAdapter(this, mDataList);
        mGridView.setAdapter(mAdapter);
        mDimBg = findViewById(0x7f0a00a7);
        (new b(null)).myexec(new Void[0]);
    }

    private void initListener()
    {
        mFinishBtn.setOnClickListener(this);
        mBucketSelectBtn.setOnClickListener(this);
        retButton.setOnClickListener(this);
        mPreViewBtn.setOnClickListener(this);
        mGridView.setOnItemClickListener(new f(this));
    }

    private void initView()
    {
        mTitleTv = (TextView)findViewById(0x7f0a00ae);
        mTitleTv.setText("\u9009\u62E9\u56FE\u7247");
        mFinishBtn = (TextView)findViewById(0x7f0a00ad);
        mPreViewBtn = (TextView)findViewById(0x7f0a00af);
        mBucketSelectBtn = (TextView)findViewById(0x7f0a00ab);
        refreshBtns();
    }

    private void refreshBtns()
    {
        TextView textview = mPreViewBtn;
        boolean flag;
        if (mSelectedImgs.size() != 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        textview.setEnabled(flag);
        mFinishBtn.setText((new StringBuilder()).append("\u5B8C\u6210  (").append(mSelectedImgs.size()).append("/").append(mCanAddSize).append(")").toString());
    }

    private void syncData(List list)
    {
        if (list == null)
        {
            return;
        }
        int k = list.size();
        ImgBucket imgbucket = (ImgBucket)mBucketList.get(0);
        int l = imgbucket.getImageList().size();
        int i = 0;
label0:
        do
        {
            if (i != k)
            {
                int j = 0;
                do
                {
label1:
                    {
                        if (j != l)
                        {
                            if (!((ImgItem)imgbucket.getImageList().get(j)).equals(list.get(i)))
                            {
                                break label1;
                            }
                            ((ImgItem)imgbucket.getImageList().get(j)).setSelected(((ImgItem)list.get(i)).isSelected());
                            if (((ImgItem)list.get(i)).isSelected())
                            {
                                ((ImgItem)imgbucket.getImageList().get(j)).setSelected(true);
                                if (!mSelectedImgs.contains(list.get(i)))
                                {
                                    mSelectedImgs.add(list.get(i));
                                }
                            } else
                            {
                                ((ImgItem)imgbucket.getImageList().get(j)).setSelected(false);
                                mSelectedImgs.remove(list.get(i));
                            }
                        }
                        i++;
                        continue label0;
                    }
                    j++;
                } while (true);
            }
            mAdapter.notifyDataSetChanged();
            return;
        } while (true);
    }

    protected void onActivityResult(int i, int j, Intent intent)
    {
        i;
        JVM INSTR tableswitch 1 1: default 20
    //                   1 21;
           goto _L1 _L2
_L1:
        return;
_L2:
        if (j == -1)
        {
            syncData((List)intent.getSerializableExtra("image_list"));
            refreshBtns();
            return;
        }
        if (j == 9)
        {
            syncData((List)intent.getSerializableExtra("image_list"));
            refreshBtns();
            intent = new Intent();
            intent.putExtra("image_list", (Serializable)mSelectedImgs);
            setResult(-1, intent);
            finish();
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public void onBackPressed()
    {
        if (mBucketListMenu != null && mBucketListMenu.isShowing())
        {
            mBucketListMenu.dismiss();
            return;
        } else
        {
            super.onBackPressed();
            return;
        }
    }

    public void onClick(View view)
    {
        if (!OneClickHelper.getInstance().onClick(view)) goto _L2; else goto _L1
_L1:
        view.getId();
        JVM INSTR lookupswitch 4: default 56
    //                   2131361915: 247
    //                   2131361963: 91
    //                   2131361965: 57
    //                   2131361967: 185;
           goto _L2 _L3 _L4 _L5 _L6
_L2:
        return;
_L5:
        view = new Intent();
        view.putExtra("image_list", (Serializable)mSelectedImgs);
        setResult(-1, view);
        finish();
        return;
_L4:
        if (mBucketListMenu != null && mBucketListMenu.isShowing())
        {
            mBucketListMenu.dismiss();
            return;
        }
        if (mBucketListMenu == null)
        {
            mBucketListMenu = new PopupWindows(this, mGridView, mBucketList);
        } else
        {
            mBucketListMenu.setFocusable(true);
        }
        mDimBg.setVisibility(0);
        mBucketListMenu.showAtLocation(mGridView, 80, 0, ToolUtil.dp2px(this, 50F));
        return;
_L6:
        view = new Intent(this, com/ximalaya/ting/android/view/multiimgpicker/MultiImgZoomPickerActivity);
        view.putExtra("image_list", (Serializable)mSelectedImgs);
        view.putExtra("selected_size", mSelectedImgs.size());
        view.putExtra("can_add_size", mCanAddSize);
        startActivityForResult(view, 1);
        return;
_L3:
        finish();
        return;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030017);
        initCommon();
        initData();
        initView();
        initListener();
    }

    protected void onResume()
    {
        super.onResume();
    }

    public void onSelectChanged(int i)
    {
        if (i < 0 || i + 1 > mDataList.size())
        {
            return;
        }
        ImgItem imgitem = (ImgItem)mDataList.get(i);
        if (imgitem.isSelected())
        {
            imgitem.setSelected(false);
            mSelectedImgs.remove(imgitem);
        } else
        {
            if (mSelectedImgs.size() >= mCanAddSize)
            {
                Toast.makeText(this, (new StringBuilder()).append("\u6700\u591A\u9009\u62E9").append(mCanAddSize).append("\u5F20\u56FE\u7247").toString(), 0).show();
                return;
            }
            imgitem.setSelected(true);
            mSelectedImgs.add(imgitem);
        }
        refreshBtns();
        mAdapter.notifyDataSetChanged();
    }



/*
    static List access$102(MultiImgPickerActivity multiimgpickeractivity, List list)
    {
        multiimgpickeractivity.mBucketList = list;
        return list;
    }

*/







}
