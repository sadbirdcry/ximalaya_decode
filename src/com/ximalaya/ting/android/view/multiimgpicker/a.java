// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;

import com.ximalaya.ting.android.view.multiimgpicker.model.ImgBucket;
import java.util.Comparator;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker:
//            ImageFetcher

class a
    implements Comparator
{

    final ImageFetcher a;

    a(ImageFetcher imagefetcher)
    {
        a = imagefetcher;
        super();
    }

    public int a(ImgBucket imgbucket, ImgBucket imgbucket1)
    {
        if (imgbucket != null && imgbucket1 != null)
        {
            return Long.valueOf(imgbucket1.getUpdatedTime()).compareTo(Long.valueOf(imgbucket.getUpdatedTime()));
        } else
        {
            return 0;
        }
    }

    public int compare(Object obj, Object obj1)
    {
        return a((ImgBucket)obj, (ImgBucket)obj1);
    }
}
