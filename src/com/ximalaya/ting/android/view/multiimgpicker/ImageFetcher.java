// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgBucket;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgItem;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker:
//            a

public class ImageFetcher
{

    private static ImageFetcher instance;
    private HashMap mBucketList;
    private Context mContext;
    private HashMap mThumbnailList;

    private ImageFetcher()
    {
        mBucketList = new HashMap();
        mThumbnailList = new HashMap();
    }

    private ImageFetcher(Context context)
    {
        mBucketList = new HashMap();
        mThumbnailList = new HashMap();
        mContext = context;
    }

    private void buildImagesBucketList()
    {
        Cursor cursor;
        long l1;
        l1 = System.currentTimeMillis();
        getThumbnail();
        cursor = mContext.getContentResolver().query(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[] {
            "_id", "bucket_id", "_data", "bucket_display_name", "date_modified"
        }, null, null, "date_modified");
        if (cursor == null)
        {
            break MISSING_BLOCK_LABEL_348;
        }
        if (!cursor.moveToFirst()) goto _L2; else goto _L1
_L1:
        int i;
        int j;
        int k;
        int l;
        int i1;
        i = cursor.getColumnIndexOrThrow("_id");
        j = cursor.getColumnIndexOrThrow("_data");
        k = cursor.getColumnIndexOrThrow("date_modified");
        l = cursor.getColumnIndexOrThrow("bucket_display_name");
        i1 = cursor.getColumnIndexOrThrow("bucket_id");
_L3:
        ImgBucket imgbucket;
        String s;
        String s1;
        String s2;
        String s3;
        long l2;
        s = cursor.getString(i);
        s1 = cursor.getString(j);
        l2 = cursor.getLong(k);
        s2 = cursor.getString(l);
        s3 = cursor.getString(i1);
        imgbucket = (ImgBucket)mBucketList.get(s3);
        if (imgbucket != null)
        {
            break MISSING_BLOCK_LABEL_221;
        }
        imgbucket = new ImgBucket();
        mBucketList.put(s3, imgbucket);
        imgbucket.setImageList(new ArrayList());
        imgbucket.setBucketName(s2);
        imgbucket.setCount(imgbucket.getCount() + 1);
        if (imgbucket.getUpdatedTime() < l2)
        {
            imgbucket.setUpdatedTime(l2);
        }
        ImgItem imgitem = new ImgItem();
        imgitem.setImageId(s);
        imgitem.setPath(s1);
        imgitem.setThumbPath((String)mThumbnailList.get(s));
        imgbucket.getImageList().add(imgitem);
        if (cursor.moveToNext()) goto _L3; else goto _L2
_L2:
        long l3 = System.currentTimeMillis();
        Log.d(com/ximalaya/ting/android/view/multiimgpicker/ImageFetcher.getName(), (new StringBuilder()).append("use time: ").append(l3 - l1).append(" ms").toString());
        if (cursor != null)
        {
            cursor.close();
        }
        return;
        Exception exception;
        exception;
        cursor = null;
_L5:
        if (cursor != null)
        {
            cursor.close();
        }
        throw exception;
        exception;
        if (true) goto _L5; else goto _L4
_L4:
    }

    public static ImageFetcher getInstance(Context context)
    {
        Context context1;
        context1 = context;
        if (context == null)
        {
            context1 = MyApplication.b();
        }
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/view/multiimgpicker/ImageFetcher;
        JVM INSTR monitorenter ;
        instance = new ImageFetcher(context1);
        com/ximalaya/ting/android/view/multiimgpicker/ImageFetcher;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        context;
        com/ximalaya/ting/android/view/multiimgpicker/ImageFetcher;
        JVM INSTR monitorexit ;
        throw context;
    }

    private void getThumbnail()
    {
        Cursor cursor = mContext.getContentResolver().query(android.provider.MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, new String[] {
            "image_id", "_data"
        }, null, null, null);
        getThumbnailColumnData(cursor);
        if (cursor != null)
        {
            cursor.close();
        }
        return;
        Exception exception;
        exception;
        cursor = null;
_L2:
        if (cursor != null)
        {
            cursor.close();
        }
        throw exception;
        exception;
        if (true) goto _L2; else goto _L1
_L1:
    }

    private void getThumbnailColumnData(Cursor cursor)
    {
        while (cursor == null || !cursor.moveToFirst()) 
        {
            return;
        }
        int i = cursor.getColumnIndex("image_id");
        int j = cursor.getColumnIndex("_data");
        do
        {
            int k = cursor.getInt(i);
            String s = cursor.getString(j);
            mThumbnailList.put((new StringBuilder()).append("").append(k).toString(), s);
        } while (cursor.moveToNext());
    }

    public List getImagesBucketList()
    {
        mBucketList.clear();
        mThumbnailList.clear();
        buildImagesBucketList();
        ArrayList arraylist = new ArrayList();
        for (Iterator iterator = mBucketList.entrySet().iterator(); iterator.hasNext(); arraylist.add(((java.util.Map.Entry)iterator.next()).getValue())) { }
        Collections.sort(arraylist, new a(this));
        return arraylist;
    }
}
