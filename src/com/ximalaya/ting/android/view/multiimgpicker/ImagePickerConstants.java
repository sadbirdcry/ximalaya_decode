// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;


public class ImagePickerConstants
{

    public static final String EXTRA_CAN_ADD_SIZE = "can_add_size";
    public static final String EXTRA_CURRENT_IMG_POSITION = "current_img_position";
    public static final String EXTRA_IMAGE_LIST = "image_list";
    public static final String EXTRA_MAX_SIZE = "max_size";
    public static final String EXTRA_SELECTED_SIZE = "selected_size";
    public static final int MAX_IMAGE_SIZE_NEW_POST = 9;
    public static final int MAX_IMAGE_SIZE_REPLY = 5;
    public static final int RESULT_FINISH = 9;

    public ImagePickerConstants()
    {
    }
}
