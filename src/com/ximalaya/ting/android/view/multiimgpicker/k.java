// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;

import android.view.View;
import android.widget.Toast;
import com.ximalaya.ting.android.util.OneClickHelper;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgItem;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker:
//            MultiImgZoomPickerActivity

class k
    implements android.view.View.OnClickListener
{

    final MultiImgZoomPickerActivity a;

    k(MultiImgZoomPickerActivity multiimgzoompickeractivity)
    {
        a = multiimgzoompickeractivity;
        super();
    }

    public void onClick(View view)
    {
        while (!OneClickHelper.getInstance().onClick(view) || MultiImgZoomPickerActivity.access$000(a) + 1 > MultiImgZoomPickerActivity.access$100(a).size()) 
        {
            return;
        }
        view = (ImgItem)MultiImgZoomPickerActivity.access$100(a).get(MultiImgZoomPickerActivity.access$000(a));
        if (view.isSelected())
        {
            view.setSelected(false);
            int _tmp = MultiImgZoomPickerActivity.access$210(a);
            MultiImgZoomPickerActivity.access$300(a);
            return;
        }
        if (MultiImgZoomPickerActivity.access$200(a) >= MultiImgZoomPickerActivity.access$400(a))
        {
            Toast.makeText(a, (new StringBuilder()).append("\u6700\u591A\u9009\u62E9").append(MultiImgZoomPickerActivity.access$400(a)).append("\u5F20\u56FE\u7247").toString(), 0).show();
            return;
        } else
        {
            view.setSelected(true);
            int _tmp1 = MultiImgZoomPickerActivity.access$208(a);
            MultiImgZoomPickerActivity.access$300(a);
            return;
        }
    }
}
