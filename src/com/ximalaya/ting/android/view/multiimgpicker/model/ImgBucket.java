// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker.model;

import java.util.List;

public class ImgBucket
{

    private String bucketName;
    private int count;
    private List imageList;
    private boolean selected;
    private long updatedTime;

    public ImgBucket()
    {
        count = 0;
        selected = false;
    }

    public String getBucketName()
    {
        return bucketName;
    }

    public int getCount()
    {
        return count;
    }

    public List getImageList()
    {
        return imageList;
    }

    public long getUpdatedTime()
    {
        return updatedTime;
    }

    public boolean isSelected()
    {
        return selected;
    }

    public void setBucketName(String s)
    {
        bucketName = s;
    }

    public void setCount(int i)
    {
        count = i;
    }

    public void setImageList(List list)
    {
        imageList = list;
    }

    public void setSelected(boolean flag)
    {
        selected = flag;
    }

    public void setUpdatedTime(long l)
    {
        updatedTime = l;
    }
}
