// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker.model;

import java.io.Serializable;

public class ImgItem
    implements Serializable
{

    private static final long serialVersionUID = 0x9c3e22031e8fe6d4L;
    private String imageId;
    private boolean isSelected;
    private String path;
    private String thumbPath;

    public ImgItem()
    {
        isSelected = false;
    }

    public boolean equals(Object obj)
    {
        if (this != obj) goto _L2; else goto _L1
_L1:
        return true;
_L2:
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        obj = (ImgItem)obj;
        if (imageId == null)
        {
            if (((ImgItem) (obj)).imageId != null)
            {
                return false;
            }
        } else
        if (!imageId.equals(((ImgItem) (obj)).imageId))
        {
            return false;
        }
        if (path == null)
        {
            if (((ImgItem) (obj)).path != null)
            {
                return false;
            }
        } else
        if (!path.equals(((ImgItem) (obj)).path))
        {
            return false;
        }
        if (thumbPath != null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (((ImgItem) (obj)).thumbPath == null) goto _L1; else goto _L3
_L3:
        return false;
        if (thumbPath.equals(((ImgItem) (obj)).thumbPath)) goto _L1; else goto _L4
_L4:
        return false;
    }

    public String getImageId()
    {
        return imageId;
    }

    public String getPath()
    {
        return path;
    }

    public String getThumbPath()
    {
        return thumbPath;
    }

    public int hashCode()
    {
        int k = 0;
        int i;
        int j;
        if (imageId == null)
        {
            i = 0;
        } else
        {
            i = imageId.hashCode();
        }
        if (path == null)
        {
            j = 0;
        } else
        {
            j = path.hashCode();
        }
        if (thumbPath != null)
        {
            k = thumbPath.hashCode();
        }
        return (j + (i + 31) * 31) * 31 + k;
    }

    public boolean isSelected()
    {
        return isSelected;
    }

    public void setImageId(String s)
    {
        imageId = s;
    }

    public void setPath(String s)
    {
        path = s;
    }

    public void setSelected(boolean flag)
    {
        isSelected = flag;
    }

    public void setThumbPath(String s)
    {
        thumbPath = s;
    }
}
