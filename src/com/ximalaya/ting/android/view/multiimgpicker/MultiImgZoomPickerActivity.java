// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgItem;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker:
//            n, k, l, m, 
//            o

public class MultiImgZoomPickerActivity extends BaseActivity
{
    class a extends PagerAdapter
    {

        final MultiImgZoomPickerActivity a;

        public void destroyItem(ViewGroup viewgroup, int i, Object obj)
        {
            viewgroup.removeView((View)obj);
        }

        public int getCount()
        {
            return a.mDataList.size();
        }

        public int getItemPosition(Object obj)
        {
            return -2;
        }

        public Object instantiateItem(ViewGroup viewgroup, int i)
        {
            a.iv = new ImageView(a);
            a.iv.setScaleType(android.widget.ImageView.ScaleType.CENTER_INSIDE);
            a.pb.setVisibility(0);
            ImageManager2.from(a).displayImage(a.iv, ToolUtil.addFilePrefix(((ImgItem)a.mDataList.get(i)).getPath()), -1, a.mPager.getMeasuredWidth(), a.mPager.getMeasuredHeight(), false, new o(this));
            viewgroup.addView(a.iv);
            return a.iv;
        }

        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }

        a()
        {
            a = MultiImgZoomPickerActivity.this;
            super();
        }
    }


    private ImageView iv;
    private int mCanAddSize;
    private int mCurrentPosition;
    private List mDataList;
    private TextView mFinishTv;
    private ViewPager mPager;
    private int mSelectCount;
    private RelativeLayout mSelectLayout;
    private ImageView mSelectedBtn;
    private a myPageAdapter;
    private android.support.v4.view.ViewPager.OnPageChangeListener pageChangeListener;
    private ProgressBar pb;

    public MultiImgZoomPickerActivity()
    {
        mDataList = new ArrayList();
        pageChangeListener = new n(this);
    }

    private void initData()
    {
        Intent intent = getIntent();
        mCurrentPosition = intent.getIntExtra("current_img_position", 0);
        mDataList = (List)intent.getSerializableExtra("image_list");
        mSelectCount = intent.getIntExtra("selected_size", 0);
        mCanAddSize = intent.getIntExtra("can_add_size", getIntent().getIntExtra("max_size", 9));
        if (mDataList == null)
        {
            mDataList = new ArrayList();
        }
    }

    private void initListener()
    {
        mSelectLayout.setOnClickListener(new k(this));
        mFinishTv.setOnClickListener(new l(this));
        retButton.setOnClickListener(new m(this));
    }

    private void initView()
    {
        mFinishTv = (TextView)findViewById(0x7f0a00ad);
        mSelectedBtn = (ImageView)findViewById(0x7f0a00b6);
        mSelectLayout = (RelativeLayout)findViewById(0x7f0a00b5);
        refreshCurrentSelected();
        mPager = (ViewPager)findViewById(0x7f0a0026);
        mPager.setOnPageChangeListener(pageChangeListener);
        pb = (ProgressBar)findViewById(0x7f0a00ba);
        myPageAdapter = new a();
        mPager.setAdapter(myPageAdapter);
        myPageAdapter.notifyDataSetChanged();
        mPager.setCurrentItem(mCurrentPosition);
        mPager.measure(0, 0);
    }

    private void refreshCurrentSelected()
    {
        if (mCurrentPosition + 1 <= mDataList.size())
        {
            if (((ImgItem)mDataList.get(mCurrentPosition)).isSelected())
            {
                mSelectedBtn.setImageResource(0x7f0202e3);
            } else
            {
                mSelectedBtn.setImageResource(0x7f0202e4);
            }
        }
        mFinishTv.setText((new StringBuilder()).append("\u5B8C\u6210  (").append(mSelectCount).append("/").append(mCanAddSize).append(")").toString());
    }

    public void onBackPressed()
    {
        Intent intent = new Intent();
        intent.putExtra("image_list", (Serializable)mDataList);
        setResult(-1, intent);
        super.onBackPressed();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030019);
        initCommon();
        initData();
        initView();
        initListener();
    }



/*
    static int access$002(MultiImgZoomPickerActivity multiimgzoompickeractivity, int i)
    {
        multiimgzoompickeractivity.mCurrentPosition = i;
        return i;
    }

*/




/*
    static int access$208(MultiImgZoomPickerActivity multiimgzoompickeractivity)
    {
        int i = multiimgzoompickeractivity.mSelectCount;
        multiimgzoompickeractivity.mSelectCount = i + 1;
        return i;
    }

*/


/*
    static int access$210(MultiImgZoomPickerActivity multiimgzoompickeractivity)
    {
        int i = multiimgzoompickeractivity.mSelectCount;
        multiimgzoompickeractivity.mSelectCount = i - 1;
        return i;
    }

*/





/*
    static ImageView access$502(MultiImgZoomPickerActivity multiimgzoompickeractivity, ImageView imageview)
    {
        multiimgzoompickeractivity.iv = imageview;
        return imageview;
    }

*/


}
