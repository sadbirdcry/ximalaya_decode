// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker.adapter;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgItem;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker.adapter:
//            a

public class ImageGridAdapter extends BaseAdapter
{
    public static interface OnSelectChangedListener
    {

        public abstract void onSelectChanged(int i);
    }

    static class a
    {

        private ImageView a;
        private ImageView b;

        static ImageView a(a a1)
        {
            return a1.a;
        }

        static ImageView a(a a1, ImageView imageview)
        {
            a1.a = imageview;
            return imageview;
        }

        static ImageView b(a a1)
        {
            return a1.b;
        }

        static ImageView b(a a1, ImageView imageview)
        {
            a1.b = imageview;
            return imageview;
        }

        a()
        {
        }
    }


    private Context mContext;
    private List mDataList;
    private OnSelectChangedListener mListener;

    public ImageGridAdapter(Context context, List list)
    {
        mContext = context;
        mDataList = list;
        if (context instanceof android.view.View.OnClickListener)
        {
            mListener = (OnSelectChangedListener)context;
        }
    }

    public int getCount()
    {
        if (mDataList == null)
        {
            return 0;
        } else
        {
            return mDataList.size();
        }
    }

    public Object getItem(int i)
    {
        return mDataList.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        ImgItem imgitem;
        if (view == null)
        {
            view = View.inflate(mContext, 0x7f030129, null);
            viewgroup = new a();
            a.a(viewgroup, (ImageView)view.findViewById(0x7f0a01a0));
            a.a(viewgroup).setTag(0x7f0a0037, Boolean.valueOf(true));
            android.view.ViewGroup.LayoutParams layoutparams = a.a(viewgroup).getLayoutParams();
            layoutparams.width = (ToolUtil.getScreenWidth(mContext) - ToolUtil.dp2px(mContext, 1.0F) * 4) / 3;
            layoutparams.height = layoutparams.width;
            a.a(viewgroup).setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
            a.a(viewgroup).setLayoutParams(layoutparams);
            a.b(viewgroup, (ImageView)view.findViewById(0x7f0a04aa));
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (a)view.getTag();
        }
        imgitem = (ImgItem)mDataList.get(i);
        ImageManager2.from(mContext).displayImage(a.a(viewgroup), ToolUtil.addFilePrefix(imgitem.getPath()), 0x7f020079, a.a(viewgroup).getLayoutParams().width, a.a(viewgroup).getLayoutParams().height);
        a.b(viewgroup).setOnClickListener(new com.ximalaya.ting.android.view.multiimgpicker.adapter.a(this, i));
        if (imgitem.isSelected())
        {
            a.b(viewgroup).setImageResource(0x7f0202e3);
            ColorMatrix colormatrix = new ColorMatrix();
            colormatrix.set(new float[] {
                1.0F, 0.0F, 0.0F, 0.0F, -60F, 0.0F, 1.0F, 0.0F, 0.0F, -60F, 
                0.0F, 0.0F, 1.0F, 0.0F, -60F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F
            });
            a.a(viewgroup).setColorFilter(new ColorMatrixColorFilter(colormatrix));
            a.a(viewgroup).postInvalidate();
            return view;
        } else
        {
            a.b(viewgroup).setImageResource(0x7f0202e4);
            ColorMatrix colormatrix1 = new ColorMatrix();
            colormatrix1.set(new float[] {
                1.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F, 1.0F, 0.0F, 0.0F, 1.0F, 
                0.0F, 0.0F, 1.0F, 0.0F, 1.0F, 0.0F, 0.0F, 0.0F, 1.0F, 0.0F
            });
            a.a(viewgroup).setColorFilter(new ColorMatrixColorFilter(colormatrix1));
            a.a(viewgroup).postInvalidate();
            return view;
        }
    }

}
