// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker:
//            e, b, c

public class ImageZoomActivity extends BaseActivity
{
    class a extends PagerAdapter
    {

        final ImageZoomActivity a;

        public void destroyItem(ViewGroup viewgroup, int i, Object obj)
        {
            viewgroup.removeView((View)obj);
        }

        public int getCount()
        {
            return a.mDataList.size();
        }

        public int getItemPosition(Object obj)
        {
            return -2;
        }

        public Object instantiateItem(ViewGroup viewgroup, int i)
        {
            a.iv = new ImageView(a);
            ImageManager2.from(a).displayImage(a.iv, ToolUtil.addFilePrefix((String)a.mDataList.get(i)), -1);
            viewgroup.addView(a.iv);
            return a.iv;
        }

        public boolean isViewFromObject(View view, Object obj)
        {
            return view == obj;
        }

        a()
        {
            a = ImageZoomActivity.this;
            super();
        }
    }


    private a adapter;
    private ImageView iv;
    private RelativeLayout mBtnLayout;
    private TextView mCountTv;
    private int mCurPosition;
    private List mDataList;
    private android.support.v4.view.ViewPager.OnPageChangeListener pageChangeListener;
    private ViewPager pager;

    public ImageZoomActivity()
    {
        mDataList = new ArrayList();
        pageChangeListener = new e(this);
    }

    private void initData()
    {
        mCurPosition = getIntent().getIntExtra("current_img_position", 0);
        mDataList = (List)getIntent().getSerializableExtra("image_list");
    }

    private void initView()
    {
        mBtnLayout = (RelativeLayout)findViewById(0x7f0a00b0);
        mBtnLayout.setBackgroundColor(0x70000000);
        ((Button)findViewById(0x7f0a00b1)).setOnClickListener(new b(this));
        ((Button)findViewById(0x7f0a00b3)).setOnClickListener(new c(this));
        mCountTv = (TextView)findViewById(0x7f0a00b2);
        refreshCount();
        pager = (ViewPager)findViewById(0x7f0a0026);
        pager.setOnPageChangeListener(pageChangeListener);
        adapter = new a();
        pager.setAdapter(adapter);
        pager.setCurrentItem(mCurPosition);
    }

    private void refreshCount()
    {
        mCountTv.setText((new StringBuilder()).append(mCurPosition + 1).append("/").append(mDataList.size()).toString());
    }

    private void removeImg(int i)
    {
        if (i + 1 <= mDataList.size())
        {
            mDataList.remove(i);
        }
    }

    private void removeImgs()
    {
        mDataList.clear();
    }

    public void onBackPressed()
    {
        Intent intent = new Intent();
        intent.putExtra("image_list", (Serializable)mDataList);
        setResult(-1, intent);
        super.onBackPressed();
    }

    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030018);
        initData();
        initView();
    }






/*
    static int access$302(ImageZoomActivity imagezoomactivity, int i)
    {
        imagezoomactivity.mCurPosition = i;
        return i;
    }

*/





/*
    static ImageView access$602(ImageZoomActivity imagezoomactivity, ImageView imageview)
    {
        imagezoomactivity.iv = imageview;
        return imageview;
    }

*/
}
