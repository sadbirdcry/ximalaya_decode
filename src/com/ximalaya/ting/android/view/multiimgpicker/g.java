// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.view.multiimgpicker.adapter.ImageGridAdapter;
import com.ximalaya.ting.android.view.multiimgpicker.model.ImgBucket;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker:
//            MultiImgPickerActivity

class g
    implements android.widget.AdapterView.OnItemClickListener
{

    final MultiImgPickerActivity a;
    final List b;
    final MultiImgPickerActivity.PopupWindows c;

    g(MultiImgPickerActivity.PopupWindows popupwindows, MultiImgPickerActivity multiimgpickeractivity, List list)
    {
        c = popupwindows;
        a = multiimgpickeractivity;
        b = list;
        super();
    }

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        if (i - c.listView.getHeaderViewsCount() < 0 || (i + 1) - c.listView.getHeaderViewsCount() > b.size())
        {
            return;
        } else
        {
            MultiImgPickerActivity.access$700(c.this$0).setText(((ImgBucket)b.get(i - c.listView.getHeaderViewsCount())).getBucketName());
            MultiImgPickerActivity.access$300(c.this$0).clear();
            MultiImgPickerActivity.access$300(c.this$0).addAll(((ImgBucket)b.get(i - c.listView.getHeaderViewsCount())).getImageList());
            MultiImgPickerActivity.access$400(c.this$0).notifyDataSetChanged();
            c.dismiss();
            return;
        }
    }
}
