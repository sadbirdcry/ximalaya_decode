// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.multiimgpicker;

import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import java.io.Serializable;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.view.multiimgpicker:
//            MultiImgZoomPickerActivity, MultiImgPickerActivity

class f
    implements android.widget.AdapterView.OnItemClickListener
{

    final MultiImgPickerActivity a;

    f(MultiImgPickerActivity multiimgpickeractivity)
    {
        a = multiimgpickeractivity;
        super();
    }

    public void onItemClick(AdapterView adapterview, View view, int i, long l)
    {
        adapterview = new Intent(a, com/ximalaya/ting/android/view/multiimgpicker/MultiImgZoomPickerActivity);
        adapterview.putExtra("image_list", (Serializable)MultiImgPickerActivity.access$300(a));
        adapterview.putExtra("current_img_position", i);
        adapterview.putExtra("selected_size", MultiImgPickerActivity.access$500(a).size());
        adapterview.putExtra("can_add_size", MultiImgPickerActivity.access$600(a));
        a.startActivityForResult(adapterview, 1);
    }
}
