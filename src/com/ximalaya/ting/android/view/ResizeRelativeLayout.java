// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class ResizeRelativeLayout extends RelativeLayout
{
    public static interface OnResizeListener
    {

        public abstract void OnResize(int i, int j, int k, int l);
    }


    private OnResizeListener mListener;

    public ResizeRelativeLayout(Context context)
    {
        super(context);
    }

    public ResizeRelativeLayout(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    protected void onSizeChanged(int i, int j, int k, int l)
    {
        super.onSizeChanged(i, j, k, l);
        if (mListener != null)
        {
            mListener.OnResize(i, j, k, l);
        }
    }

    public void setOnResizeListener(OnResizeListener onresizelistener)
    {
        mListener = onresizelistener;
    }
}
