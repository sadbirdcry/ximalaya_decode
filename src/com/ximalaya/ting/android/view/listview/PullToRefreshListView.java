// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.listview;

import android.content.Context;
import android.content.res.Resources;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import java.util.Date;

// Referenced classes of package com.ximalaya.ting.android.view.listview:
//            FixedListView

public class PullToRefreshListView extends FixedListView
    implements android.widget.AbsListView.OnScrollListener
{
    public static interface OnFloatVisibilityChangedCallback
    {

        public abstract void OnFloatVisibilityChanged(View view, int i);
    }

    public static interface OnRefreshListener
    {

        public abstract void onRefresh();
    }

    public static interface OnScrollListner
    {

        public abstract void onMyScrollStateChanged(AbsListView abslistview, int i);
    }

    public static interface OnStopScrollListner
    {

        public abstract void onStopScroll(AbsListView abslistview);
    }

    private class a
        implements Runnable
    {

        final PullToRefreshListView a;
        private long b;
        private long c;
        private int d;
        private int e;
        private int f;
        private boolean g;

        private double b()
        {
            double d1 = (double)e / (double)d;
            double d2 = c - b;
            b = SystemClock.uptimeMillis();
            return d1 * d2;
        }

        public void a()
        {
            g = true;
        }

        public void run()
        {
            if (g)
            {
                return;
            }
            c = SystemClock.uptimeMillis();
            int i = a.headView.getPaddingTop();
            if (e > 0)
            {
                if (i - f > 0)
                {
                    int j = (int)((double)i - b());
                    i = j;
                    if (j < f)
                    {
                        i = f;
                    }
                    a.headView.setPadding(0, i, 0, 0);
                    a.post(this);
                    return;
                } else
                {
                    a.changeViewState();
                    return;
                }
            }
            if (i - f < 0)
            {
                int k = (int)((double)i - b());
                i = k;
                if (k > f)
                {
                    i = f;
                }
                a.headView.setPadding(0, i, 0, 0);
                a.post(this);
                return;
            } else
            {
                a.changeViewState();
                return;
            }
        }

        public a(int i, int j)
        {
            a = PullToRefreshListView.this;
            super();
            b = SystemClock.uptimeMillis();
            d = 100;
            g = true;
            e = i;
            f = j;
            g = false;
        }
    }


    private static final int DONE = 3;
    private static final int LOADING = 4;
    private static final int PULL_To_REFRESH = 1;
    private static final int RATIO = 3;
    private static final int REFRESHING = 2;
    private static final int RELEASE_To_REFRESH = 0;
    private RotateAnimation animation;
    private ImageView arrowImageView;
    private OnFloatVisibilityChangedCallback callback;
    private int firstItemIndex;
    private int headContentHeight;
    private LinearLayout headView;
    private LayoutInflater inflater;
    private boolean isBack;
    private boolean isRecored;
    private boolean isRefreshable;
    private TextView lastUpdatedTextView;
    private int mCurrentScrollState;
    private View mFloatView;
    private OnStopScrollListner onStopScroll;
    private OnScrollListner onStopScroll2;
    private ProgressBar progressBar;
    private OnRefreshListener refreshListener;
    private RotateAnimation reverseAnimation;
    private a soomthRunnable;
    private int startY;
    private int state;
    private TextView tipsTextview;

    public PullToRefreshListView(Context context)
    {
        super(context);
        init(context);
    }

    public PullToRefreshListView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        init(context);
    }

    private void changeHeaderViewByState()
    {
        switch (state)
        {
        default:
            return;

        case 0: // '\0'
            arrowImageView.setVisibility(0);
            progressBar.setVisibility(8);
            tipsTextview.setVisibility(0);
            lastUpdatedTextView.setVisibility(0);
            arrowImageView.clearAnimation();
            arrowImageView.startAnimation(animation);
            tipsTextview.setText("\u677E\u5F00\u5237\u65B0");
            return;

        case 1: // '\001'
            progressBar.setVisibility(8);
            tipsTextview.setVisibility(0);
            lastUpdatedTextView.setVisibility(0);
            arrowImageView.setVisibility(0);
            arrowImageView.clearAnimation();
            arrowImageView.setVisibility(0);
            if (isBack)
            {
                isBack = false;
                arrowImageView.clearAnimation();
                arrowImageView.startAnimation(reverseAnimation);
                tipsTextview.setText("\u4E0B\u62C9\u5237\u65B0");
                return;
            } else
            {
                tipsTextview.setText("\u4E0B\u62C9\u5237\u65B0");
                return;
            }

        case 2: // '\002'
            smoothPaddingTo(0);
            return;

        case 3: // '\003'
            smoothPaddingTo(headContentHeight * -1);
            return;
        }
    }

    private void changeViewState()
    {
        switch (state)
        {
        default:
            return;

        case 2: // '\002'
            progressBar.setVisibility(0);
            arrowImageView.clearAnimation();
            arrowImageView.setVisibility(8);
            tipsTextview.setText("\u6B63\u5728\u5237\u65B0...");
            lastUpdatedTextView.setVisibility(0);
            return;

        case 3: // '\003'
            progressBar.setVisibility(8);
            break;
        }
        arrowImageView.setVisibility(0);
        arrowImageView.clearAnimation();
        tipsTextview.setText("\u4E0B\u62C9\u5237\u65B0");
        lastUpdatedTextView.setVisibility(0);
    }

    private void init(Context context)
    {
        setCacheColorHint(context.getResources().getColor(0x7f070003));
        inflater = LayoutInflater.from(context);
        headView = (LinearLayout)inflater.inflate(0x7f030153, null);
        arrowImageView = (ImageView)headView.findViewById(0x7f0a0514);
        arrowImageView.setMinimumWidth(70);
        arrowImageView.setMinimumHeight(50);
        progressBar = (ProgressBar)headView.findViewById(0x7f0a0515);
        tipsTextview = (TextView)headView.findViewById(0x7f0a0516);
        lastUpdatedTextView = (TextView)headView.findViewById(0x7f0a0517);
        measureView(headView);
        headContentHeight = headView.getMeasuredHeight();
        headView.setPadding(0, headContentHeight * -1, 0, 0);
        addHeaderView(headView, null, false);
        setOnScrollListener(this);
        animation = new RotateAnimation(0.0F, -180F, 1, 0.5F, 1, 0.5F);
        animation.setInterpolator(new LinearInterpolator());
        animation.setDuration(250L);
        animation.setFillAfter(true);
        reverseAnimation = new RotateAnimation(-180F, 0.0F, 1, 0.5F, 1, 0.5F);
        reverseAnimation.setInterpolator(new LinearInterpolator());
        reverseAnimation.setDuration(200L);
        reverseAnimation.setFillAfter(true);
        state = 3;
        isRefreshable = false;
    }

    private void measureView(View view)
    {
        android.view.ViewGroup.LayoutParams layoutparams1 = view.getLayoutParams();
        android.view.ViewGroup.LayoutParams layoutparams = layoutparams1;
        if (layoutparams1 == null)
        {
            layoutparams = new android.view.ViewGroup.LayoutParams(-1, -2);
        }
        int j = ViewGroup.getChildMeasureSpec(0, 0, layoutparams.width);
        int i = layoutparams.height;
        if (i > 0)
        {
            i = android.view.View.MeasureSpec.makeMeasureSpec(i, 0x40000000);
        } else
        {
            i = android.view.View.MeasureSpec.makeMeasureSpec(0, 0);
        }
        view.measure(j, i);
    }

    private void onStopScroll(AbsListView abslistview)
    {
        if (onStopScroll != null)
        {
            onStopScroll.onStopScroll(abslistview);
        }
    }

    private void smoothPaddingTo(int i)
    {
        int j = headView.getPaddingTop() - i;
        if (soomthRunnable == null)
        {
            soomthRunnable = new a(j, i);
            post(soomthRunnable);
            return;
        } else
        {
            soomthRunnable.a();
            soomthRunnable = new a(j, i);
            post(soomthRunnable);
            return;
        }
    }

    public boolean isRefreshing()
    {
        return state == 2;
    }

    public void onRefresh()
    {
        if (refreshListener != null)
        {
            refreshListener.onRefresh();
        }
    }

    public void onRefreshComplete()
    {
        state = 3;
        lastUpdatedTextView.setText((new StringBuilder()).append("\u6700\u8FD1\u66F4\u65B0:").append((new Date()).toLocaleString()).toString());
        changeHeaderViewByState();
    }

    public void onScroll(AbsListView abslistview, int i, int j, int k)
    {
        if (mFloatView == null) goto _L2; else goto _L1
_L1:
        if (i != 0) goto _L4; else goto _L3
_L3:
        mFloatView.setVisibility(8);
        if (callback != null)
        {
            callback.OnFloatVisibilityChanged(mFloatView, 8);
        }
_L2:
        firstItemIndex = i;
        if (mCurrentScrollState != 1 || state == 2) goto _L6; else goto _L5
_L5:
        if (getLastVisiblePosition() == getCount() - 1)
        {
            onStopScroll(abslistview);
        }
_L8:
        return;
_L4:
        mFloatView.setVisibility(0);
        if (callback != null)
        {
            callback.OnFloatVisibilityChanged(mFloatView, 0);
        }
        continue; /* Loop/switch isn't completed */
_L6:
        if (mCurrentScrollState != 2 || (i + j) - 1 != k - 1) goto _L8; else goto _L7
_L7:
        onStopScroll(abslistview);
        return;
        if (true) goto _L2; else goto _L9
_L9:
    }

    public void onScrollStateChanged(AbsListView abslistview, int i)
    {
        mCurrentScrollState = i;
        if (onStopScroll2 != null)
        {
            onStopScroll2.onMyScrollStateChanged(abslistview, i);
        }
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        if (!isRefreshable) goto _L2; else goto _L1
_L1:
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 40
    //                   0 48
    //                   1 79
    //                   2 153
    //                   3 79;
           goto _L3 _L4 _L5 _L6 _L5
_L3:
        break; /* Loop/switch isn't completed */
_L6:
        break MISSING_BLOCK_LABEL_153;
_L2:
        int i;
        boolean flag;
        try
        {
            flag = super.onTouchEvent(motionevent);
        }
        // Misplaced declaration of an exception variable
        catch (MotionEvent motionevent)
        {
            return true;
        }
        return flag;
_L4:
        if (firstItemIndex == 0 && !isRecored)
        {
            isRecored = true;
            startY = (int)motionevent.getY();
        }
          goto _L2
_L5:
        if (state != 2 && state != 4)
        {
            if (state != 3);
            if (state == 1)
            {
                state = 3;
                changeHeaderViewByState();
            }
            if (state == 0)
            {
                state = 2;
                changeHeaderViewByState();
                onRefresh();
            }
        }
        isRecored = false;
        isBack = false;
          goto _L2
        i = (int)motionevent.getY();
        if (!isRecored && firstItemIndex == 0)
        {
            isRecored = true;
            startY = i;
        }
        if (state != 2 && isRecored && state != 4)
        {
            if (state == 0)
            {
                setSelection(0);
                if ((i - startY) / 3 < headContentHeight && i - startY > 0)
                {
                    state = 1;
                    changeHeaderViewByState();
                } else
                if (i - startY <= 0)
                {
                    state = 3;
                    changeHeaderViewByState();
                }
            }
            if (state == 1)
            {
                setSelection(0);
                if ((i - startY) / 3 >= headContentHeight)
                {
                    state = 0;
                    isBack = true;
                    changeHeaderViewByState();
                } else
                if (i - startY <= 0)
                {
                    state = 3;
                    changeHeaderViewByState();
                }
            }
            if (state == 3 && i - startY > 0)
            {
                state = 1;
                changeHeaderViewByState();
            }
            if (state == 1)
            {
                headView.setPadding(0, headContentHeight * -1 + (i - startY) / 3, 0, 0);
            }
            if (state == 0)
            {
                headView.setPadding(0, (i - startY) / 3 - headContentHeight, 0, 0);
            }
        }
          goto _L2
    }

    public volatile void setAdapter(Adapter adapter)
    {
        setAdapter((ListAdapter)adapter);
    }

    public void setAdapter(ListAdapter listadapter)
    {
        lastUpdatedTextView.setText((new StringBuilder()).append("\u6700\u8FD1\u66F4\u65B0:").append((new Date()).toLocaleString()).toString());
        super.setAdapter(listadapter);
    }

    public void setFloatHeadView(View view)
    {
        mFloatView = view;
    }

    public void setMyScrollListener2(OnScrollListner onscrolllistner)
    {
        onStopScroll2 = onscrolllistner;
    }

    public void setOnFloatViewVisibilityChangedCallback(OnFloatVisibilityChangedCallback onfloatvisibilitychangedcallback)
    {
        callback = onfloatvisibilitychangedcallback;
    }

    public void setOnRefreshListener(OnRefreshListener onrefreshlistener)
    {
        refreshListener = onrefreshlistener;
        isRefreshable = true;
    }

    public void setOnStopScrollListener(OnStopScrollListner onstopscrolllistner)
    {
        onStopScroll = onstopscrolllistner;
    }

    public void toRefreshing()
    {
        if (state != 2)
        {
            state = 2;
            changeHeaderViewByState();
            onRefresh();
        }
    }


}
