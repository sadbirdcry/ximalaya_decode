// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.listview;

import android.os.SystemClock;
import android.widget.LinearLayout;

// Referenced classes of package com.ximalaya.ting.android.view.listview:
//            PullToRefreshListView

private class g
    implements Runnable
{

    final PullToRefreshListView a;
    private long b;
    private long c;
    private int d;
    private int e;
    private int f;
    private boolean g;

    private double b()
    {
        double d1 = (double)e / (double)d;
        double d2 = c - b;
        b = SystemClock.uptimeMillis();
        return d1 * d2;
    }

    public void a()
    {
        g = true;
    }

    public void run()
    {
        if (g)
        {
            return;
        }
        c = SystemClock.uptimeMillis();
        int i = PullToRefreshListView.access$000(a).getPaddingTop();
        if (e > 0)
        {
            if (i - f > 0)
            {
                int j = (int)((double)i - b());
                i = j;
                if (j < f)
                {
                    i = f;
                }
                PullToRefreshListView.access$000(a).setPadding(0, i, 0, 0);
                a.post(this);
                return;
            } else
            {
                PullToRefreshListView.access$100(a);
                return;
            }
        }
        if (i - f < 0)
        {
            int k = (int)((double)i - b());
            i = k;
            if (k > f)
            {
                i = f;
            }
            PullToRefreshListView.access$000(a).setPadding(0, i, 0, 0);
            a.post(this);
            return;
        } else
        {
            PullToRefreshListView.access$100(a);
            return;
        }
    }

    public (PullToRefreshListView pulltorefreshlistview, int i, int j)
    {
        a = pulltorefreshlistview;
        super();
        b = SystemClock.uptimeMillis();
        d = 100;
        g = true;
        e = i;
        f = j;
        g = false;
    }
}
