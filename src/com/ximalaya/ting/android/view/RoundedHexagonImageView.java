// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundedHexagonImageView extends ImageView
{

    private Shader mBG;
    private Bitmap mDst;
    private Bitmap mSrc;

    public RoundedHexagonImageView(Context context)
    {
        super(context);
        setDuplicateParentStateEnabled(true);
    }

    public RoundedHexagonImageView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        setDuplicateParentStateEnabled(true);
    }

    public RoundedHexagonImageView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        setDuplicateParentStateEnabled(true);
    }

    private void drawShade(Canvas canvas)
    {
        try
        {
            mSrc = makeSrc();
            mDst = makeDst();
            mBG = new BitmapShader(Bitmap.createBitmap(100, 100, android.graphics.Bitmap.Config.ARGB_8888), android.graphics.Shader.TileMode.REPEAT, android.graphics.Shader.TileMode.REPEAT);
            canvas.drawColor(0);
            (new Paint(1)).setTextAlign(android.graphics.Paint.Align.CENTER);
            Paint paint = new Paint();
            paint.setFilterBitmap(false);
            paint.setStyle(android.graphics.Paint.Style.FILL);
            paint.setShader(mBG);
            canvas.drawRect(0, 0, getMeasuredWidth() + 0, getMeasuredHeight() + 0, paint);
            int i = canvas.saveLayer(0, 0, getMeasuredWidth() + 0, getMeasuredHeight() + 0, null, 31);
            canvas.drawBitmap(mDst, 0.0F, 0.0F, paint);
            paint.setXfermode(new PorterDuffXfermode(android.graphics.PorterDuff.Mode.DST_IN));
            canvas.drawBitmap(mSrc, 0.0F, 0.0F, paint);
            canvas.restoreToCount(i);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Canvas canvas)
        {
            recycle(mDst);
        }
        recycle(mSrc);
    }

    private Bitmap makeDst()
    {
        Bitmap bitmap = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), android.graphics.Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(1);
        paint.setColor(-13244);
        BitmapDrawable bitmapdrawable = null;
        if (getDrawable() instanceof BitmapDrawable)
        {
            bitmapdrawable = (BitmapDrawable)getDrawable();
        }
        Matrix matrix = new Matrix();
        float f = (float)getMeasuredWidth() / (float)bitmapdrawable.getBitmap().getWidth();
        matrix.postScale(f, f);
        canvas.drawBitmap(bitmapdrawable.getBitmap(), matrix, paint);
        return bitmap;
    }

    private Bitmap makeSrc()
    {
        Bitmap bitmap = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), android.graphics.Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(1);
        paint.setColor(0xff66aaff);
        Bitmap bitmap1 = ((BitmapDrawable)getResources().getDrawable(0x7f020078)).getBitmap();
        Matrix matrix = new Matrix();
        float f = (float)getMeasuredWidth() / (float)bitmap1.getWidth();
        matrix.postScale(f, f);
        canvas.drawBitmap(bitmap1, matrix, paint);
        return bitmap;
    }

    private void recycle(Bitmap bitmap)
    {
        if (bitmap != null && !bitmap.isRecycled())
        {
            bitmap.recycle();
        }
    }

    protected void onDraw(Canvas canvas)
    {
        drawShade(canvas);
    }
}
