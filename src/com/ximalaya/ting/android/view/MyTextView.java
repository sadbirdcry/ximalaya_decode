// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view;

import android.content.Context;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyTextView extends TextView
{

    private int mMaxLines;
    private float mTakenWidth;

    public MyTextView(Context context)
    {
        super(context);
    }

    public MyTextView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    public MyTextView(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
    }

    private float getMaxLineHeight(String s)
    {
        int j = (int)Math.ceil(getPaint().measureText(s) / mTakenWidth);
        int i = j;
        if (j > mMaxLines)
        {
            i = mMaxLines;
        }
        float f = getPaint().getFontMetrics().descent;
        float f1 = getPaint().getFontMetrics().ascent;
        return (float)i * (f - f1);
    }

    protected void onMeasure(int i, int j)
    {
        super.onMeasure(i, j);
        if (getLayout() != null)
        {
            i = (int)Math.ceil(getMaxLineHeight(getText().toString()));
            j = getCompoundPaddingTop();
            int k = getCompoundPaddingBottom();
            setMeasuredDimension(getMeasuredWidth(), i + j + k);
        }
    }

    public void setTakenWidth(float f, int i)
    {
        mTakenWidth = f;
        mMaxLines = i;
    }
}
