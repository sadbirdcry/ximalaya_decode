// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.setting;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

public class CornerListView extends ListView
{

    public CornerListView(Context context)
    {
        super(context);
    }

    public CornerListView(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        setBackgroundResource(0x7f0205aa);
        setSelector(0x7f0205ab);
    }

    protected void onMeasure(int i, int j)
    {
        super.onMeasure(i, android.view.View.MeasureSpec.makeMeasureSpec(0x1fffffff, 0x80000000));
    }
}
