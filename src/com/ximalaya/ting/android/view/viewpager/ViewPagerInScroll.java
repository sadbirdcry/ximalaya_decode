// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.view.viewpager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;

public class ViewPagerInScroll extends ViewPager
{

    private ViewGroup mDisallowInterceptTouchEventView;

    public ViewPagerInScroll(Context context)
    {
        this(context, null);
    }

    public ViewPagerInScroll(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    protected void onDetachedFromWindow()
    {
        super.onDetachedFromWindow();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionevent)
    {
        if (mDisallowInterceptTouchEventView == null) goto _L2; else goto _L1
_L1:
        motionevent.getAction();
        JVM INSTR tableswitch 0 3: default 40
    //                   0 46
    //                   1 57
    //                   2 46
    //                   3 57;
           goto _L2 _L3 _L4 _L3 _L4
_L2:
        return super.onInterceptTouchEvent(motionevent);
_L3:
        mDisallowInterceptTouchEventView.requestDisallowInterceptTouchEvent(true);
        continue; /* Loop/switch isn't completed */
_L4:
        mDisallowInterceptTouchEventView.requestDisallowInterceptTouchEvent(false);
        if (true) goto _L2; else goto _L5
_L5:
    }

    public void setDisallowInterceptTouchEventView(ViewGroup viewgroup)
    {
        mDisallowInterceptTouchEventView = viewgroup;
    }
}
