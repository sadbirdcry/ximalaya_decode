// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.c;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.Iterator;
import java.util.List;

public class d extends MyAsyncTaskLoader
{

    private int a;
    private List b;
    private int c;

    public d(Context context, int i)
    {
        super(context);
        a = 1;
        a = i;
    }

    private void a(List list, String s)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.add("trackIds", (new StringBuilder()).append("").append(s).toString());
        s = f.a().a("mobile/track/relation", requestparams, fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (s)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (s)).a))
        {
            break MISSING_BLOCK_LABEL_200;
        }
        s = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (s)).a);
        if (s == null)
        {
            break MISSING_BLOCK_LABEL_200;
        }
        if (s.getIntValue("ret") != 0)
        {
            return;
        }
        s = s.getJSONArray("data");
        list = list.iterator();
_L4:
        SoundInfoNew soundinfonew;
        if (!list.hasNext())
        {
            break MISSING_BLOCK_LABEL_200;
        }
        soundinfonew = (SoundInfoNew)list.next();
        int i = 0;
_L2:
        if (i >= s.size())
        {
            break; /* Loop/switch isn't completed */
        }
        if (soundinfonew.id == s.getJSONObject(i).getLongValue("trackId"))
        {
            soundinfonew.isFavorite = s.getJSONObject(i).getBooleanValue("isLike");
            soundinfonew.isRelay = s.getJSONObject(i).getBooleanValue("isRelay");
        }
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        if (true) goto _L4; else goto _L3
_L3:
        list;
        list.printStackTrace();
    }

    public int a()
    {
        return c;
    }

    public void a(List list)
    {
        super.deliverResult(list);
        b = list;
    }

    public List b()
    {
        Object obj;
        obj = new RequestParams();
        ((RequestParams) (obj)).put("page", (new StringBuilder()).append("").append(a).toString());
        ((RequestParams) (obj)).put("per_page", "15");
        ((RequestParams) (obj)).put("condition", "hot");
        ((RequestParams) (obj)).put("category_name", "all");
        ((RequestParams) (obj)).put("tag_name", "");
        obj = f.a().a("m/explore_track_list", ((RequestParams) (obj)), fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a)) goto _L2; else goto _L1
_L1:
        obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_299;
        }
        if (((JSONObject) (obj)).getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_299;
        }
        c = ((JSONObject) (obj)).getIntValue("count");
        if (((JSONObject) (obj)).getString("list") == null) goto _L2; else goto _L3
_L3:
        Iterator iterator;
        b = JSON.parseArray(((JSONObject) (obj)).getString("list"), com/ximalaya/ting/android/model/sound/SoundInfoNew);
        if (b == null || b.size() <= 0 || !UserInfoMannage.hasLogined())
        {
            break MISSING_BLOCK_LABEL_282;
        }
        iterator = b.iterator();
        obj = null;
_L4:
        SoundInfoNew soundinfonew;
        if (!iterator.hasNext())
        {
            break MISSING_BLOCK_LABEL_270;
        }
        soundinfonew = (SoundInfoNew)iterator.next();
        if (obj != null)
        {
            break MISSING_BLOCK_LABEL_260;
        }
        obj = new StringBuilder();
_L5:
        ((StringBuilder) (obj)).append(soundinfonew.id);
          goto _L4
        obj;
        ((Exception) (obj)).printStackTrace();
_L2:
        fromBindView = null;
        toBindView = null;
        return null;
        ((StringBuilder) (obj)).append(",");
          goto _L5
        a(b, ((StringBuilder) (obj)).toString());
        fromBindView = null;
        toBindView = null;
        obj = b;
        return ((List) (obj));
        return null;
    }

    public void deliverResult(Object obj)
    {
        a((List)obj);
    }

    public Object loadInBackground()
    {
        return b();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (b != null)
        {
            a(b);
            return;
        } else
        {
            forceLoad();
            return;
        }
    }
}
