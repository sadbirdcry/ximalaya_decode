// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.c;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.List;

public class a extends MyAsyncTaskLoader
{

    private List a;

    public a(Context context)
    {
        super(context);
    }

    public List a()
    {
        Object obj = new RequestParams();
        ((RequestParams) (obj)).add("picVersion", "11");
        ((RequestParams) (obj)).add("scale", "2");
        obj = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/discovery/v1/categories").toString(), ((RequestParams) (obj)), fromBindView, toBindView, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            try
            {
                obj = JSONObject.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
                if (((JSONObject) (obj)).getIntValue("ret") == 0)
                {
                    obj = ((JSONObject) (obj)).getString("list");
                    if (!TextUtils.isEmpty(((CharSequence) (obj))))
                    {
                        a = FindingCategoryModel.getListFromJson(((String) (obj)));
                    }
                }
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
            }
        }
        if (a == null || a.size() <= 0)
        {
            String s = FileUtils.readAssetFileData(getContext(), "finding/category.json");
            if (!TextUtils.isEmpty(s))
            {
                a = FindingCategoryModel.getListFromJson(s);
            }
        }
        return a;
    }

    public void a(List list)
    {
        super.deliverResult(list);
        a = list;
    }

    public void deliverResult(Object obj)
    {
        a((List)obj);
    }

    public Object loadInBackground()
    {
        return a();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (a == null)
        {
            forceLoad();
            return;
        } else
        {
            a(a);
            return;
        }
    }
}
