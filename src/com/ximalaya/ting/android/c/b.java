// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.c;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.Collectable;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.util.Iterator;
import java.util.List;

public class b extends MyAsyncTaskLoader
{

    private List a;
    private boolean b;

    public b(Context context, List list)
    {
        super(context);
        a = list;
        b = false;
    }

    public List a()
    {
        Object obj1 = null;
        if (UserInfoMannage.hasLogined() || a == null) goto _L2; else goto _L1
_L1:
        Object obj;
        obj = a.iterator();
        while (((Iterator) (obj)).hasNext()) 
        {
            obj1 = (Collectable)((Iterator) (obj)).next();
            boolean flag;
            if (AlbumModelManage.getInstance().isHadCollected(((Collectable) (obj1)).getCId()) != null)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            ((Collectable) (obj1)).setCCollected(flag);
        }
        b = true;
        obj = a;
_L4:
        return ((List) (obj));
_L2:
        obj = obj1;
        if (!UserInfoMannage.hasLogined()) goto _L4; else goto _L3
_L3:
        obj = obj1;
        if (a == null) goto _L4; else goto _L5
_L5:
        obj = UserInfoMannage.getInstance().getUser();
        obj1 = new StringBuffer();
        for (Iterator iterator = a.iterator(); iterator.hasNext(); ((StringBuffer) (obj1)).append(((Collectable)iterator.next()).getCId()).append(",")) { }
        RequestParams requestparams = new RequestParams();
        requestparams.add("uid", (new StringBuilder()).append("").append(((LoginInfoModel) (obj)).uid).toString());
        requestparams.add("album_ids", ((StringBuffer) (obj1)).toString());
        obj = f.a().a("m/album_subscribe_status", requestparams, null, null, false);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_335;
        }
        obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        if (((JSONObject) (obj)).getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_348;
        }
        obj = ((JSONObject) (obj)).getJSONObject("status");
        obj1 = a.iterator();
_L6:
        Collectable collectable;
        if (!((Iterator) (obj1)).hasNext())
        {
            break MISSING_BLOCK_LABEL_348;
        }
        collectable = (Collectable)((Iterator) (obj1)).next();
        Exception exception;
        boolean flag1;
        if (((JSONObject) (obj)).getIntValue(String.valueOf(collectable.getCId())) == 1)
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        collectable.setCCollected(flag1);
          goto _L6
        exception;
        exception.printStackTrace();
        b = true;
        return null;
        exception = a;
        return exception;
    }

    public void a(List list)
    {
        super.deliverResult(list);
        a = list;
    }

    public void deliverResult(Object obj)
    {
        a((List)obj);
    }

    public Object loadInBackground()
    {
        return a();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (!b)
        {
            forceLoad();
            return;
        } else
        {
            a(a);
            return;
        }
    }
}
