// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.c;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.finding.FindingTabModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.MD5;
import com.ximalaya.ting.android.util.MyAsyncTaskLoader;
import java.io.File;
import java.util.List;

public class c extends MyAsyncTaskLoader
{

    private List a;
    private boolean b;

    public c(Context context)
    {
        super(context);
        b = false;
    }

    public c(Context context, boolean flag)
    {
        super(context);
        b = flag;
    }

    public List a()
    {
        String s = null;
        if (!b) goto _L2; else goto _L1
_L1:
        s = FileUtils.readStrFromFile((new File(getContext().getCacheDir(), MD5.md5((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/discovery/v1/tabs").toString()))).getAbsolutePath());
        if (TextUtils.isEmpty(s)) goto _L4; else goto _L3
_L3:
        a = FindingTabModel.getListFromJson(s);
_L6:
        return a;
_L4:
        s = FileUtils.readAssetFileData(getContext(), "finding/tabs.json");
        if (!TextUtils.isEmpty(s))
        {
            a = FindingTabModel.getListFromJson(s);
        }
        continue; /* Loop/switch isn't completed */
_L2:
        Object obj;
        obj = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/discovery/v1/tabs").toString(), null, null, null, true);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            continue; /* Loop/switch isn't completed */
        }
        obj = JSONObject.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        if (((JSONObject) (obj)).getIntValue("ret") != 0)
        {
            continue; /* Loop/switch isn't completed */
        }
        obj = ((JSONObject) (obj)).getJSONObject("tabs");
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_187;
        }
        s = ((JSONObject) (obj)).getString("list");
        if (!TextUtils.isEmpty(s))
        {
            FileUtils.writeStr2File(s, (new File(getContext().getCacheDir(), MD5.md5((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/discovery/v1/tabs").toString()))).getAbsolutePath());
            a = FindingTabModel.getListFromJson(s);
        }
        continue; /* Loop/switch isn't completed */
        Exception exception;
        exception;
        exception.printStackTrace();
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void a(List list)
    {
        super.deliverResult(list);
        a = list;
    }

    public void deliverResult(Object obj)
    {
        a((List)obj);
    }

    public Object loadInBackground()
    {
        return a();
    }

    protected void onStartLoading()
    {
        super.onStartLoading();
        if (a == null)
        {
            forceLoad();
            return;
        } else
        {
            a(a);
            return;
        }
    }
}
