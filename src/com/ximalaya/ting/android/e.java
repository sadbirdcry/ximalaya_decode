// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android;

import com.ximalaya.ting.android.database.DataBaseHelper;
import com.ximalaya.ting.android.modelmanage.DexManager;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android:
//            MyApplication

class e
    implements Runnable
{

    final MyApplication a;

    e(MyApplication myapplication)
    {
        a = myapplication;
        super();
    }

    public void run()
    {
        DataCollectUtil.getInstance(com.ximalaya.ting.android.MyApplication.b()).destroy();
        b.d();
        MyApplication.g = -1;
        DownloadHandler downloadhandler = DownloadHandler.getInstance(a.getApplicationContext());
        if (downloadhandler != null)
        {
            downloadhandler.releaseOnExit();
        }
        DataBaseHelper.releaseOnExit();
        com.ximalaya.ting.android.MyApplication.b(a);
        MyApplication.c(a);
        MyApplication.d(a);
        MyApplication.e(a);
        MyApplication.f = null;
        try
        {
            Thread.sleep(500L);
        }
        catch (InterruptedException interruptedexception)
        {
            interruptedexception.printStackTrace();
        }
        if (MyApplication.i)
        {
            if (ToolUtil.getAndroidSDKVersion() < 14)
            {
                System.exit(0);
            }
            if (DexManager.getInstance(a.getApplicationContext()).isNeedSysExit())
            {
                Logger.d("dex", "isDlnaFirstLoad true");
                DexManager.getInstance(a.getApplicationContext()).setSysExit(false);
                System.exit(0);
            }
        }
    }
}
