// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.media.RemoteControlClient;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.baidu.alliance.audio.SDKManager;
import com.tencent.tauth.Tencent;
import com.ximalaya.ting.android.a.c;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.activity.login.WelcomeActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.broadcast.GlobalBroadcastReceiver;
import com.ximalaya.ting.android.broadcast.MediaButtonReceiver;
import com.ximalaya.ting.android.database.DataBaseHelper;
import com.ximalaya.ting.android.database.DownloadTableHandler;
import com.ximalaya.ting.android.dialog.FollowUpdateToast;
import com.ximalaya.ting.android.dl.PluginConstants;
import com.ximalaya.ting.android.dl.PluginManager;
import com.ximalaya.ting.android.fragment.BaseLoginFragment;
import com.ximalaya.ting.android.fragment.device.bluetooth.BluetoothManager;
import com.ximalaya.ting.android.fragment.device.che.MyBluetoothManager2;
import com.ximalaya.ting.android.fragment.device.dlna.DlnaManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoVoiceManager;
import com.ximalaya.ting.android.fragment.play.PlayerFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.AppConfig;
import com.ximalaya.ting.android.model.share.ShareContentModel;
import com.ximalaya.ting.android.model.zone.PostEditorData;
import com.ximalaya.ting.android.modelmanage.DexManager;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.modelmanage.LiveHistoryManage;
import com.ximalaya.ting.android.modelmanage.MyLocationManager;
import com.ximalaya.ting.android.service.LocalRecorderService;
import com.ximalaya.ting.android.service.localalarm.LocalAlarm;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.SoundListenRecord;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.transaction.c.j;
import com.ximalaya.ting.android.transaction.d.d;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.PackageUtil;
import com.ximalaya.ting.android.util.SSLSocketFactoryEx;
import com.ximalaya.ting.android.util.Session;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.io.File;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

// Referenced classes of package com.ximalaya.ting.android:
//            g, h, i, a, 
//            b, d, f, e, 
//            k, c, j

public class MyApplication extends Application
{

    public static Activity f;
    public static int g = -1;
    public static boolean i = true;
    public static boolean j = false;
    public static boolean k = false;
    private static MyApplication q;
    private static long u;
    private static boolean v = false;
    private static Timer w;
    public String a;
    public int b;
    public int c;
    public ShareContentModel d;
    public int e;
    public DexManager h;
    private String l;
    private String m;
    private String n;
    private HttpClient o;
    private long p;
    private final int r = 10000;
    private ComponentName s;
    private RemoteControlClient t;

    public MyApplication()
    {
        p = -1L;
        e = 0;
    }

    private void A()
    {
        h.loadDlna(null, 1);
    }

    private void B()
    {
        h.loadSuicheting(null);
    }

    private void C()
    {
        MyBluetoothManager2.getInstance(getApplicationContext()).checkDevice();
    }

    private void D()
    {
        h.copyLoadDexWithoutLoadSync("Msc_dex.jar", new g(this));
    }

    private void E()
    {
        XiMaoVoiceManager.getInstance(getApplicationContext()).initInUi(getApplicationContext());
        XiMaoBTManager.getInstance(getApplicationContext()).initInUi();
        (new Thread(new h(this))).start();
    }

    private void F()
    {
        BluetoothManager.getInstance(getApplicationContext()).initInUi();
        (new Thread(new i(this))).start();
    }

    private boolean G()
    {
        return !com.ximalaya.ting.android.a.b || SharedPreferencesUtil.getInstance(this).getBoolean("P_IS_SURE_NO_3G_DIALOG_NOTIFY");
    }

    private final void H()
    {
        PlayListControl.savePlayList();
        LocalMediaService.releaseOnExit();
        PlayListControl.releaseOnExit();
        if (com.ximalaya.ting.android.transaction.c.j.b() != null)
        {
            com.ximalaya.ting.android.transaction.c.j.b().d();
        }
    }

    private void I()
    {
        if (o != null && o.getConnectionManager() != null)
        {
            o.getConnectionManager().shutdown();
            o = null;
        }
    }

    public static Activity a()
    {
        if (c())
        {
            return f;
        }
        if (MainTabActivity2.isMainTabActivityAvaliable())
        {
            return MainTabActivity2.mainTabActivity;
        } else
        {
            return null;
        }
    }

    public static void a(String s1, String s2)
    {
        if (c())
        {
            f.runOnUiThread(new com.ximalaya.ting.android.b(s1, s2));
        }
    }

    public static boolean a(Context context)
    {
        if (context == null)
        {
            return false;
        }
        List list = ((ActivityManager)context.getSystemService("activity")).getRunningTasks(1);
        return f == null && !context.getPackageName().equals(((android.app.ActivityManager.RunningTaskInfo)list.get(0)).baseActivity.getPackageName());
    }

    public static ActivityManager b(Context context)
    {
        return (ActivityManager)context.getSystemService("activity");
    }

    public static final Context b()
    {
        if (c())
        {
            return f.getApplicationContext();
        }
        if (MainTabActivity2.isMainTabActivityAvaliable())
        {
            return MainTabActivity2.mainTabActivity.getApplicationContext();
        } else
        {
            return q;
        }
    }

    static void b(MyApplication myapplication)
    {
        myapplication.v();
    }

    static void c(MyApplication myapplication)
    {
        myapplication.w();
    }

    public static final boolean c()
    {
        return f != null && !f.isFinishing();
    }

    public static boolean c(Context context)
    {
        if (context == null)
        {
            return false;
        }
        ActivityManager activitymanager = b(context);
        boolean flag;
        if (d(context).contains(((android.app.ActivityManager.RunningTaskInfo)activitymanager.getRunningTasks(1).get(0)).baseActivity.getPackageName()))
        {
            flag = true;
        } else
        {
            flag = false;
        }
        return flag;
    }

    public static List d(Context context)
    {
        ArrayList arraylist = new ArrayList();
        context = context.getPackageManager();
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.addCategory("android.intent.category.HOME");
        context = context.queryIntentActivities(intent, 0x10000).iterator();
        do
        {
            if (!context.hasNext())
            {
                break;
            }
            ResolveInfo resolveinfo = (ResolveInfo)context.next();
            if (resolveinfo.activityInfo != null)
            {
                arraylist.add(resolveinfo.activityInfo.processName);
                arraylist.add(resolveinfo.activityInfo.packageName);
            }
        } while (true);
        return arraylist;
    }

    public static void d()
    {
        if (!j) goto _L2; else goto _L1
_L1:
        j = false;
        v = false;
_L4:
        return;
_L2:
        if (!a(b())) goto _L4; else goto _L3
_L3:
        v = c(b());
        if (v)
        {
            u = (new Date()).getTime();
            return;
        }
        break MISSING_BLOCK_LABEL_66;
        Exception exception;
        exception;
        Logger.e("startLoadingAd", exception.getMessage(), exception);
        return;
        r();
        return;
    }

    static void d(MyApplication myapplication)
    {
        myapplication.I();
    }

    public static void e(Context context)
    {
        SDKManager.getInstance().setEnvironment(0);
        com.baidu.alliance.audio.SDKConfiguration.Builder builder = new com.baidu.alliance.audio.SDKConfiguration.Builder();
        builder.setDebugMode(false).setContext(context).setHttpConnectionTimeout(3).setHttpReadTimeout(3).setAppKey("zU52H7trYxE4SA05Ny").setAppSecretKey("RsZXvOshaDBO711x7SQzgXH3BgDPZs9a");
        SDKManager.getInstance().init(builder.build());
    }

    static void e(MyApplication myapplication)
    {
        myapplication.u();
    }

    public static boolean e()
    {
        if (w != null)
        {
            w.cancel();
        }
        if (!k) goto _L2; else goto _L1
_L1:
        u = 0L;
        k = false;
_L4:
        return false;
_L2:
        Intent intent;
        if (!v || b() == null || !s())
        {
            break MISSING_BLOCK_LABEL_123;
        }
        ToolUtil.mobileResume();
        v = false;
        intent = new Intent("android.intent.action.MAIN");
        intent.putExtra("isInit", false);
        intent.addFlags(0x10000);
        intent.setComponent(new ComponentName(b(), com/ximalaya/ting/android/activity/login/WelcomeActivity));
        if (a() == null) goto _L4; else goto _L3
_L3:
        Logger.d("LoadingAD", "\u542F\u52A8loading\u5E7F\u544A\uFF0C\u524D\u53F0\u4E3Alauncher");
        a().startActivity(intent);
        return true;
        try
        {
            v = false;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            return false;
        }
        return false;
    }

    static boolean f(MyApplication myapplication)
    {
        return myapplication.t();
    }

    private static void r()
    {
        while (AppConfig.getInstance().adLoadingIntervalTime == 0L || (f instanceof WelcomeActivity) || NetworkUtils.getNetType(b()) == -1) 
        {
            return;
        }
        if (w != null)
        {
            w.cancel();
            w.purge();
        }
        w = new Timer();
        w.schedule(new com.ximalaya.ting.android.d(), AppConfig.getInstance().adLoadingIntervalTime * 1000L);
    }

    private static boolean s()
    {
        boolean flag1 = true;
        if (u <= 0L || AppConfig.getInstance().adLoadingIntervalTime == 0L || (f instanceof WelcomeActivity))
        {
            flag1 = false;
        } else
        {
            long l1 = ((new Date()).getTime() - u) / 1000L;
            boolean flag;
            if (l1 > 0L && l1 >= AppConfig.getInstance().adLoadingIntervalTime)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            if (!flag || NetworkUtils.getNetType(b()) == -1)
            {
                return false;
            }
        }
        return flag1;
    }

    private boolean t()
    {
        if (b() != null)
        {
            SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(b());
            if (sharedpreferencesutil.contains("download_location") && StorageUtils.isDirAvaliable(sharedpreferencesutil.getString("download_location")))
            {
                return true;
            } else
            {
                return StorageUtils.isDirAvaliable(StorageUtils.setDefaultDownloadLocation());
            }
        } else
        {
            return false;
        }
    }

    private void u()
    {
        q = null;
        MainTabActivity2.mainTabActivity = null;
        f = null;
        PlayerFragment.mIsShowAds = true;
    }

    private void v()
    {
        Iterator iterator;
        SharedPreferencesUtil sharedpreferencesutil;
        ArrayList arraylist;
        iterator = null;
        sharedpreferencesutil = SharedPreferencesUtil.getInstance(getApplicationContext());
        arraylist = sharedpreferencesutil.getArrayList("temp_zone_img");
        Object obj = sharedpreferencesutil.getString("temp_zone_post");
        Object obj2 = sharedpreferencesutil.getString("temp_zone_comment_reply");
        if (!TextUtils.isEmpty(((CharSequence) (obj))))
        {
            try
            {
                obj = (PostEditorData)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/zone/PostEditorData);
                break MISSING_BLOCK_LABEL_58;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(((Exception) (obj)).getMessage()).append(Logger.getLineInfo()).toString());
            }
        }
        obj = null;
        continue;
        do
        {
            Object obj1 = iterator;
            if (!TextUtils.isEmpty(((CharSequence) (obj2))))
            {
                try
                {
                    obj1 = (PostEditorData)JSON.parseObject(((String) (obj2)), com/ximalaya/ting/android/model/zone/PostEditorData);
                }
                catch (Exception exception)
                {
                    Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(exception.getMessage()).append(Logger.getLineInfo()).toString());
                    exception = iterator;
                }
            }
            if (arraylist != null)
            {
                iterator = arraylist.iterator();
                do
                {
                    if (!iterator.hasNext())
                    {
                        break;
                    }
                    obj2 = (String)iterator.next();
                    if ((obj == null || ((PostEditorData) (obj)).getImagePaths() == null || !((PostEditorData) (obj)).getImagePaths().contains(obj2)) && (obj1 == null || ((PostEditorData) (obj1)).getImagePaths() == null || !((PostEditorData) (obj1)).getImagePaths().contains(obj2)))
                    {
                        obj2 = new File(((String) (obj2)));
                        if (((File) (obj2)).exists())
                        {
                            ((File) (obj2)).delete();
                        }
                        iterator.remove();
                    }
                } while (true);
                sharedpreferencesutil.saveArrayList("temp_zone_img", (ArrayList)arraylist);
            }
            return;
        } while (true);
    }

    private void w()
    {
        String s1;
        boolean flag;
        s1 = null;
        flag = false;
        if (StorageUtils.getCachesSize() < 200L) goto _L2; else goto _L1
_L1:
        File file1;
        File file = new File("/ting/images");
        if (file != null && file.isDirectory())
        {
            File afile[] = file.listFiles();
            int k1 = afile.length;
            for (int i1 = 0; i1 < k1; i1++)
            {
                afile[i1].delete();
            }

        }
        file1 = new File(c.d);
        if (file1 == null || !file1.isDirectory()) goto _L2; else goto _L3
_L3:
        Object obj = LocalMediaService.getInstance();
        if (obj == null) goto _L5; else goto _L4
_L4:
        String s2 = ToolUtil.getIncomPlayingAudioFilename(((LocalMediaService) (obj)));
        if (!Utilities.isNotBlank(s2)) goto _L5; else goto _L6
_L6:
        obj = (new StringBuilder()).append(s2).append(".chunk").toString();
        s1 = (new StringBuilder()).append(s2).append(".index").toString();
_L8:
        File afile1[] = file1.listFiles();
        int l1 = afile1.length;
        int j1 = ((flag) ? 1 : 0);
        do
        {
            if (j1 >= l1)
            {
                break;
            }
            File file2 = afile1[j1];
            if ((obj == null || !((String) (obj)).equalsIgnoreCase(file2.getName())) && (s1 == null || !s1.equalsIgnoreCase(file2.getName())))
            {
                file2.delete();
            }
            j1++;
        } while (true);
_L2:
        return;
_L5:
        obj = null;
        if (true) goto _L8; else goto _L7
_L7:
    }

    private void x()
    {
label0:
        {
            if (Environment.getExternalStorageState().equals("mounted"))
            {
                if (!Environment.getExternalStorageDirectory().exists())
                {
                    break label0;
                }
                File file = new File(a.ac);
                if (!file.exists())
                {
                    file.mkdirs();
                }
            }
            return;
        }
        Logger.e(getClass().getName(), "sdcard not use!");
    }

    private HttpClient y()
    {
        Object obj = new BasicHttpParams();
        HttpConnectionParams.setStaleCheckingEnabled(((HttpParams) (obj)), false);
        HttpProtocolParams.setVersion(((HttpParams) (obj)), HttpVersion.HTTP_1_1);
        HttpProtocolParams.setContentCharset(((HttpParams) (obj)), "ISO-8859-1");
        HttpProtocolParams.useExpectContinue(((HttpParams) (obj)));
        ConnManagerParams.setTimeout(((HttpParams) (obj)), 10000L);
        HttpConnectionParams.setConnectionTimeout(((HttpParams) (obj)), 10000);
        HttpConnectionParams.setSoTimeout(((HttpParams) (obj)), 10000);
        HttpProtocolParams.setUseExpectContinue(((HttpParams) (obj)), true);
        HttpConnectionParams.setSocketBufferSize(((HttpParams) (obj)), 8192);
        HttpClientParams.setRedirecting(((HttpParams) (obj)), true);
        HttpProtocolParams.setUserAgent(((HttpParams) (obj)), com/ximalaya/ting/android/MyApplication.getName());
        SchemeRegistry schemeregistry = new SchemeRegistry();
        schemeregistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
        try
        {
            Object obj1 = KeyStore.getInstance(KeyStore.getDefaultType());
            ((KeyStore) (obj1)).load(null, null);
            obj1 = new SSLSocketFactoryEx(((KeyStore) (obj1)));
            ((SSLSocketFactory) (obj1)).setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
            schemeregistry.register(new Scheme("https", ((org.apache.http.conn.scheme.SocketFactory) (obj1)), 443));
        }
        catch (Exception exception)
        {
            Logger.log((new StringBuilder()).append("\u521B\u5EFAhttpclient \u53D1\u751F\u9519\u8BEF").append(Logger.getLineInfo()).toString());
        }
        obj = new DefaultHttpClient(new ThreadSafeClientConnManager(((HttpParams) (obj)), schemeregistry), ((HttpParams) (obj)));
        ((HttpClient) (obj)).getParams().setParameter("http.protocol.cookie-policy", "netscape");
        FreeFlowUtil.getInstance();
        FreeFlowUtil.setProxyForHttpClient(((HttpClient) (obj)));
        return ((HttpClient) (obj));
    }

    private void z()
    {
        if (h.isXimaoOpen())
        {
            D();
        }
        if (h.isSuichetingOpen())
        {
            B();
        }
        if (h.isDlnaOpen())
        {
            A();
        }
    }

    public void a(long l1)
    {
        p = l1;
    }

    public final void a(Activity activity, View view)
    {
        Object obj = SharedPreferencesUtil.getInstance(activity);
        ((SharedPreferencesUtil) (obj)).removeByKey("loginforesult");
        ((SharedPreferencesUtil) (obj)).removeByKey("account");
        ((SharedPreferencesUtil) (obj)).removeByKey("password");
        CommonRequest.bingAppLogout(getApplicationContext());
        obj = new Intent(activity, com/ximalaya/ting/android/activity/login/LoginActivity);
        ((Intent) (obj)).putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        activity.startActivity(((Intent) (obj)));
        if (MainTabActivity2.mainTabActivity == null)
        {
            break MISSING_BLOCK_LABEL_94;
        }
        MainTabActivity2.mainTabActivity.logout();
        activity = BaseLoginFragment.getTecent(getApplicationContext());
        if (activity != null)
        {
            try
            {
                activity.logout(getApplicationContext());
            }
            // Misplaced declaration of an exception variable
            catch (Activity activity) { }
        }
        SharedPreferencesUtil.getInstance(getApplicationContext()).removeByKey("P_SHUKE_FIRST_USE");
        return;
    }

    public void a(MyApplication myapplication)
    {
        if (i)
        {
            i = false;
            f();
            q = myapplication;
            f = null;
            n = ToolUtil.getDeviceToken(this);
            x();
            GlobalBroadcastReceiver.onAppStart();
            DataBaseHelper.cleanOnAppStart(this);
            (new DownloadTableHandler(this)).onAppStart();
            LocalMediaService.startLocalMediaService(getApplicationContext(), true);
            FollowUpdateToast.setIsSound(SharedPreferencesUtil.getInstance(this).getBoolean("isRadioAlert", true));
            com.ximalaya.ting.android.transaction.a.b.a().f();
            o();
            HistoryManage.getInstance(this).updateHistoryList();
            LiveHistoryManage.getInstance(this).updateHistoryList();
            DataCollectUtil.getInstance(b()).init();
            EmotionUtil.getInstance().init(getApplicationContext(), 0x7f0c002d, 0x7f0c002c);
            com.ximalaya.ting.android.b.f.a();
            ImageManager2.from(this).reSet();
            ThirdAdStatUtil.getInstance().init(this);
            (new com.ximalaya.ting.android.f(this)).myexec(new Void[0]);
            F();
            if (h.isXimaoOpen())
            {
                E();
            }
            if (h.isSuichetingOpen())
            {
                C();
                return;
            }
        }
    }

    public void f()
    {
    }

    public void g()
    {
        i = true;
        stopService(new Intent(getApplicationContext(), com/ximalaya/ting/android/service/LocalRecorderService));
        SoundListenRecord.getSoundListenRecord(this).onDestory();
        com.ximalaya.ting.android.e.b.a(this).b();
        Session.getSession().cleanUpSession();
        H();
        com.ximalaya.ting.android.transaction.d.d.a().d();
        GlobalBroadcastReceiver.onAppExit();
        MyLocationManager.getInstance(this).destory();
        AudioManager audiomanager = (AudioManager)getSystemService("audio");
        if (audiomanager != null)
        {
            audiomanager.unregisterMediaButtonEventReceiver(s);
        }
        FreeFlowUtil.getInstance().destroy();
        if (DexManager.getInstance(this).isSuichetingInit())
        {
            MyBluetoothManager2.getInstance(getApplicationContext()).releaseBtManager();
        }
        if (DexManager.getInstance(this).isDlnaInit() && DlnaManager.getAvaliableInstance() != null)
        {
            DlnaManager.getAvaliableInstance().clear();
            Logger.e("clear", "MyTingshuDeviceManager.getAvaliableInstance()");
        }
        (new Thread(new e(this))).start();
        BluetoothManager.getInstance(getApplicationContext()).cleanup();
    }

    public String h()
    {
        if (a.e)
        {
            return "androidpad";
        } else
        {
            return "android";
        }
    }

    public String i()
    {
        if (n == null || "".equals(n))
        {
            n = ToolUtil.getDeviceToken(this);
        }
        return n;
    }

    public HttpClient j()
    {
        if (o == null)
        {
            o = y();
        }
        return o;
    }

    public long k()
    {
        return p;
    }

    public String l()
    {
        if (l == null || "".equals(l))
        {
            StringBuilder stringbuilder = new StringBuilder();
            stringbuilder.append("ting_");
            stringbuilder.append(m());
            stringbuilder.append("(");
            stringbuilder.append(ToolUtil.getPhoneVersion());
            stringbuilder.append(",");
            stringbuilder.append(ToolUtil.getSimpleSystemVersion());
            stringbuilder.append(")");
            l = stringbuilder.toString();
        }
        return l;
    }

    public String m()
    {
        this;
        JVM INSTR monitorenter ;
        if (!TextUtils.isEmpty(m)) goto _L2; else goto _L1
_L1:
        m = getString(0x7f090082);
        if (TextUtils.isEmpty(m)) goto _L2; else goto _L3
_L3:
        String as[] = m.split("\\.");
        if (as == null) goto _L2; else goto _L4
_L4:
        if (as.length <= 3) goto _L2; else goto _L5
_L5:
        Object obj;
        int i1;
        obj = null;
        i1 = 0;
_L13:
        if (i1 >= 3) goto _L7; else goto _L6
_L6:
        if (obj != null) goto _L9; else goto _L8
_L8:
        obj = new StringBuilder();
        ((StringBuilder) (obj)).append(as[i1]);
          goto _L10
_L9:
        ((StringBuilder) (obj)).append(".");
        ((StringBuilder) (obj)).append(as[i1]);
          goto _L10
        obj;
        throw obj;
_L7:
        if (obj == null) goto _L2; else goto _L11
_L11:
        m = ((StringBuilder) (obj)).toString();
_L2:
        obj = m;
        this;
        JVM INSTR monitorexit ;
        return ((String) (obj));
_L10:
        i1++;
        if (true) goto _L13; else goto _L12
_L12:
    }

    public RemoteControlClient n()
    {
        return t;
    }

    public void o()
    {
        AudioManager audiomanager = (AudioManager)getSystemService("audio");
        s = new ComponentName(getPackageName(), com/ximalaya/ting/android/broadcast/MediaButtonReceiver.getName());
        audiomanager.registerMediaButtonEventReceiver(s);
        if (PackageUtil.isPostICS())
        {
            if (t == null)
            {
                Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
                intent.setComponent(s);
                t = new RemoteControlClient(PendingIntent.getBroadcast(this, 0, intent, 0));
                audiomanager.registerRemoteControlClient(t);
            }
            t.setTransportControlFlags(181);
        }
        (new IntentFilter("android.intent.action.MEDIA_BUTTON")).setPriority(10000);
        registerReceiver(new MediaButtonReceiver(), new IntentFilter("android.intent.action.MEDIA_BUTTON"));
    }

    public void onCreate()
    {
        super.onCreate();
        FreeFlowUtil.getInstance().init(getApplicationContext());
        if (q == null)
        {
            q = this;
        }
        PluginManager pluginmanager = PluginManager.getInstance(q);
        pluginmanager.loadPluginSync(PluginConstants.PLUGIN_COMMON);
        pluginmanager.loadPluginSync(PluginConstants.PLUGIN_CARLIFE);
        pluginmanager.loadPluginSync(PluginConstants.PLUGIN_SDL);
        pluginmanager.loadPluginSync(PluginConstants.PLUGIN_APPLINK);
        pluginmanager.loadPluginSync(PluginConstants.PLUGIN_ALIPAY);
        pluginmanager.loadPluginSync(PluginConstants.PLUGIN_APPTRACK);
        pluginmanager.loadPluginSync(PluginConstants.PLUGIN_EGUAN);
        h = DexManager.getInstance(getApplicationContext());
        if (G())
        {
            q();
        }
    }

    public void onLowMemory()
    {
        super.onLowMemory();
        (new Thread(new k(this))).start();
        ImageManager2.from(this).clear();
    }

    public void onTerminate()
    {
        super.onTerminate();
        (new Thread(new com.ximalaya.ting.android.c(this))).start();
    }

    public void p()
    {
        (new com.ximalaya.ting.android.j(this)).start();
    }

    public void q()
    {
        z();
        LocalAlarm.startAlarmWithNotification(this, false);
    }

}
