// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.a;

import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.io.File;
import java.io.PrintStream;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

// Referenced classes of package com.ximalaya.ting.android.transaction.a:
//            a, d, c, e

public final class b
{

    private static b e = null;
    public volatile ThreadPoolExecutor a;
    public LinkedList b;
    public boolean c;
    public SoundInfo d;
    private HttpClient f;

    public b()
    {
        a = null;
        b = null;
        f = null;
    }

    public static final b a()
    {
        if (e != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/transaction/a/b;
        JVM INSTR monitorenter ;
        if (e == null)
        {
            ToolUtil.createAppDirectory();
            e = new b();
            e.d = null;
            e.c = false;
            e.b = new LinkedList();
            e.a = new ThreadPoolExecutor(0, 1, 60L, TimeUnit.SECONDS, new LinkedBlockingQueue());
            e.f = new DefaultHttpClient();
            HttpParams httpparams = e.f.getParams();
            HttpConnectionParams.setConnectionTimeout(httpparams, 5000);
            HttpConnectionParams.setSoTimeout(httpparams, 5000);
        }
        com/ximalaya/ting/android/transaction/a/b;
        JVM INSTR monitorexit ;
_L2:
        return e;
        Exception exception;
        exception;
        com/ximalaya/ting/android/transaction/a/b;
        JVM INSTR monitorexit ;
        throw exception;
    }

    static void a(b b1, SoundInfo soundinfo)
    {
        b1.c(soundinfo);
    }

    private final boolean a(com.ximalaya.ting.android.transaction.a.a a1)
    {
        if (b == null || !b.contains(a1))
        {
            return false;
        }
        if (!a1.equals(b.getLast()))
        {
            b.remove(a1);
            b.addLast(a1);
        }
        return true;
    }

    private final boolean b(SoundInfo soundinfo)
    {
        if (MyApplication.b() != null) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        String s;
        int i = NetworkUtils.getNetType(MyApplication.b());
        s = null;
        switch (i)
        {
        default:
            break;

        case 1: // '\001'
            break; /* Loop/switch isn't completed */

        case -1: 
        case 0: // '\0'
            break;
        }
        break MISSING_BLOCK_LABEL_111;
_L4:
        if (soundinfo != null && !Utilities.isBlank(s) && (new File((new StringBuilder()).append(a.ao).append("/").append(ToolUtil.md5(s)).append(".index").toString())).exists())
        {
            return true;
        }
        if (true) goto _L1; else goto _L3
_L3:
        s = soundinfo.playUrl64;
          goto _L4
        s = soundinfo.playUrl32;
          goto _L4
    }

    private final void c(SoundInfo soundinfo)
    {
        if (soundinfo != null && !b(soundinfo) && !soundinfo.equals(d) && f != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        com.ximalaya.ting.android.transaction.a.a a1 = new com.ximalaya.ting.android.transaction.a.a(f, soundinfo);
        if (b == null) goto _L1; else goto _L3
_L3:
        LinkedList linkedlist = b;
        linkedlist;
        JVM INSTR monitorenter ;
        if (b == null || a(a1))
        {
            break MISSING_BLOCK_LABEL_161;
        }
        for (; !c && b.size() >= 15; b.removeFirst()) { }
        break MISSING_BLOCK_LABEL_108;
        soundinfo;
        linkedlist;
        JVM INSTR monitorexit ;
        throw soundinfo;
        b.addLast(a1);
        b.notifyAll();
        Logger.d("CachingTask", (new StringBuilder()).append("Add Task\u3010").append(soundinfo.title).append("\u3011--").append(System.currentTimeMillis()).toString());
        linkedlist;
        JVM INSTR monitorexit ;
    }

    public static final void d()
    {
        if (e != null)
        {
            e.c = true;
            if (e.b != null)
            {
                e.b.clear();
                e.b = null;
            }
            if (e.f != null)
            {
                e.f.getConnectionManager().shutdown();
                e.f = null;
            }
            e.g();
            e = null;
        }
    }

    private final void g()
    {
        if (a != null)
        {
            a.shutdown();
            try
            {
                if (!a.awaitTermination(1000L, TimeUnit.MILLISECONDS))
                {
                    a.shutdownNow();
                    if (!a.awaitTermination(1000L, TimeUnit.MILLISECONDS))
                    {
                        System.err.println("Pool did not terminate");
                    }
                }
            }
            catch (InterruptedException interruptedexception)
            {
                a.shutdownNow();
                Thread.currentThread().interrupt();
            }
            a = null;
        }
    }

    private final com.ximalaya.ting.android.transaction.a.a h()
    {
        if (b == null)
        {
            break MISSING_BLOCK_LABEL_128;
        }
        LinkedList linkedlist = b;
        linkedlist;
        JVM INSTR monitorenter ;
        if (b == null) goto _L2; else goto _L1
_L1:
        int i = b.size();
        if (i > 0)
        {
            break MISSING_BLOCK_LABEL_40;
        }
        b.wait();
_L4:
        if (b.size() <= 0) goto _L2; else goto _L3
_L3:
        Object obj = (com.ximalaya.ting.android.transaction.a.a)b.removeLast();
_L5:
        linkedlist;
        JVM INSTR monitorexit ;
_L6:
        if (obj == null)
        {
            Logger.d("CachingTask", "takeTask NULL");
            return ((com.ximalaya.ting.android.transaction.a.a) (obj));
        } else
        {
            Logger.d("CachingTask", (new StringBuilder()).append("takeTask").append(((com.ximalaya.ting.android.transaction.a.a) (obj)).a.title).toString());
            return ((com.ximalaya.ting.android.transaction.a.a) (obj));
        }
        obj;
        ((InterruptedException) (obj)).printStackTrace();
          goto _L4
        obj;
        linkedlist;
        JVM INSTR monitorexit ;
        throw obj;
_L2:
        obj = null;
          goto _L5
        obj = null;
          goto _L6
    }

    public final void a(SoundInfo soundinfo)
    {
        (new Thread(new d(this, soundinfo))).start();
    }

    public void b()
    {
        FreeFlowUtil.setProxyForHttpClient(a().f);
    }

    public void c()
    {
        if (f != null)
        {
            f.getParams().removeParameter("http.route.default-proxy");
            ((DefaultHttpClient)f).addRequestInterceptor(new c(this));
        }
    }

    public final void e()
    {
        if (a != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        com.ximalaya.ting.android.transaction.a.a a1;
        do
        {
            a1 = h();
        } while (!c && a1 == null);
        if (a1 == null) goto _L1; else goto _L3
_L3:
        if (a.isShutdown() || a.isTerminating() || a.isTerminated()) goto _L1; else goto _L4
_L4:
        a.execute(a1);
        return;
        Exception exception;
        exception;
    }

    public final void f()
    {
        (new Thread(new e(this))).start();
    }

}
