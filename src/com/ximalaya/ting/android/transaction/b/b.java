// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.b;

import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.FreeFlowUtil;
import com.ximalaya.ting.android.util.MD5;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;

// Referenced classes of package com.ximalaya.ting.android.transaction.b:
//            d, c

class com.ximalaya.ting.android.transaction.b.b
    implements Runnable
{
    public static interface a
    {

        public abstract void a();

        public abstract void a(String s);
    }

    class b
    {

        String a;
        long b;
        a c;
        final com.ximalaya.ting.android.transaction.b.b d;

        b()
        {
            d = com.ximalaya.ting.android.transaction.b.b.this;
            super();
        }
    }


    private static String c;
    private static com.ximalaya.ting.android.transaction.b.b h;
    private LinkedList a;
    private Thread b;
    private Object d;
    private Context e;
    private Handler f;
    private List g;

    private com.ximalaya.ting.android.transaction.b.b(Context context)
    {
        f = new Handler(Looper.getMainLooper());
        e = context;
        a = new LinkedList();
        d = new Object();
        if (g == null)
        {
            g = b(context.getApplicationContext());
        }
        if (g == null)
        {
            g = new LinkedList();
        }
        if ("mounted".equals(Environment.getExternalStorageState()))
        {
            c = (new File(Environment.getExternalStorageDirectory(), "ting/cache/")).getAbsolutePath();
        } else
        {
            c = (new File(context.getCacheDir(), "ting/cache/")).getAbsolutePath();
        }
        if (!TextUtils.isEmpty(c) && !c.endsWith("/"))
        {
            c = (new StringBuilder()).append(c).append("/").toString();
        }
        context = new File(c);
        if (!context.exists())
        {
            context.mkdirs();
        }
    }

    public static com.ximalaya.ting.android.transaction.b.b a(Context context)
    {
        com/ximalaya/ting/android/transaction/b/b;
        JVM INSTR monitorenter ;
        if (h == null)
        {
            h = new com.ximalaya.ting.android.transaction.b.b(context);
        }
        context = h;
        com/ximalaya/ting/android/transaction/b/b;
        JVM INSTR monitorexit ;
        return context;
        context;
        throw context;
    }

    private void a()
    {
        for (; a.size() >= 2; a.removeFirst()) { }
    }

    private void a(b b1)
    {
        if (b1.c != null)
        {
            f.post(new d(this, b1));
        }
    }

    private void a(b b1, String s)
    {
        if (b1.c != null)
        {
            f.post(new c(this, b1, s));
        }
    }

    private b b()
    {
        Object obj = d;
        obj;
        JVM INSTR monitorenter ;
        b b1;
        if (a.isEmpty())
        {
            break MISSING_BLOCK_LABEL_32;
        }
        b1 = (b)a.removeLast();
        return b1;
        obj;
        JVM INSTR monitorexit ;
        return null;
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private List b(Context context)
    {
        context = FileUtils.readStrFromFile((new File(c, "ad_sound")).getAbsolutePath());
        if (TextUtils.isEmpty(context))
        {
            break MISSING_BLOCK_LABEL_40;
        }
        context = JSON.parseArray(context, com/ximalaya/ting/android/transaction/b/b$b);
        return context;
        context;
        context.printStackTrace();
        return null;
    }

    private void c(Context context)
    {
        if (g == null || g.size() <= 0)
        {
            break MISSING_BLOCK_LABEL_214;
        }
        context = new StringBuffer();
        context.append("[");
        int k = g.size();
        int i = 0;
        do
        {
label0:
            {
                b b1;
                int j;
                if (g.size() > 2)
                {
                    j = 2;
                } else
                {
                    j = g.size();
                }
                if (i >= j)
                {
                    break label0;
                }
                b1 = (b)g.get(k - 1 - i);
                context.append("{").append("\"url\":").append("\"").append(c).append(MD5.md5(b1.a)).append("\"").append(",").append("\"adId\":").append(b1.b).append("}").append(",");
                i++;
            }
        } while (true);
        context.deleteCharAt(context.length() - 1);
        context.append("]");
        FileUtils.writeStr2File(context.toString(), (new File(c, "ad_sound")).getAbsolutePath());
    }

    public String a(long l)
    {
        if (g != null)
        {
            for (int i = 0; i < g.size(); i++)
            {
                b b1 = (b)g.get(i);
                if (b1.b == l)
                {
                    return b1.a;
                }
            }

        }
        return null;
    }

    public void a(String s, long l, a a1)
    {
        b b1 = new b();
        b1.a = s;
        b1.b = l;
        b1.c = a1;
        synchronized (d)
        {
            a();
            a.addLast(b1);
        }
        if (b == null)
        {
            b = new Thread(this);
            b.start();
        }
        return;
        a1;
        s;
        JVM INSTR monitorexit ;
        throw a1;
    }

    public void run()
    {
        Object obj4;
        int i;
        i = 0;
        obj4 = null;
_L8:
        Object obj;
        Object obj1;
        Object obj2;
        do
        {
            b b2 = b();
            byte abyte0[];
            Exception exception;
            b b1;
            if (b2 != null)
            {
                if (!TextUtils.isEmpty(b2.a))
                {
label0:
                    {
                        String s = a(b2.b);
                        if (TextUtils.isEmpty(s) || !(new File(c, s)).exists())
                        {
                            break label0;
                        }
                        a(b2, (new File(c, s)).getAbsolutePath());
                    }
                } else
                {
                    a(b2, null);
                }
            } else
            {
                if (i != 0)
                {
                    c(e.getApplicationContext());
                }
                b = null;
                return;
            }
        } while (true);
        obj = new HttpGet(b2.a);
        obj1 = new DefaultHttpClient();
        obj2 = ((HttpClient) (obj1)).getParams();
        HttpConnectionParams.setConnectionTimeout(((org.apache.http.params.HttpParams) (obj2)), 3000);
        HttpConnectionParams.setSoTimeout(((org.apache.http.params.HttpParams) (obj2)), 3000);
        FreeFlowUtil.getInstance();
        FreeFlowUtil.setProxyForHttpClient(((HttpClient) (obj1)));
        obj = ((HttpClient) (obj1)).execute(((org.apache.http.client.methods.HttpUriRequest) (obj)));
        if (((HttpResponse) (obj)).getStatusLine().getStatusCode() != 200) goto _L2; else goto _L1
_L1:
        obj = ((HttpResponse) (obj)).getEntity();
        if (obj == null) goto _L4; else goto _L3
_L3:
        obj2 = (new StringBuilder()).append(c).append(MD5.md5(b2.a)).toString();
        obj = new BufferedInputStream(((HttpEntity) (obj)).getContent());
        obj1 = new BufferedOutputStream(new FileOutputStream(((String) (obj2))));
        abyte0 = new byte[1024];
_L7:
        i = ((BufferedInputStream) (obj)).read(abyte0);
        if (i == -1) goto _L6; else goto _L5
_L5:
        ((BufferedOutputStream) (obj1)).write(abyte0, 0, i);
          goto _L7
        exception;
        obj2 = obj1;
        obj1 = obj;
        obj = obj2;
        obj2 = exception;
_L9:
        ((Exception) (obj2)).printStackTrace();
        a(b2);
        if (obj1 != null)
        {
            try
            {
                ((BufferedInputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((IOException) (obj1)).printStackTrace();
            }
        }
        if (obj != null)
        {
            try
            {
                ((BufferedOutputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        i = 1;
          goto _L8
_L6:
        b1 = new b();
        b1.b = b2.b;
        b1.a = MD5.md5(b2.a);
        g.add(b1);
        a(b2, ((String) (obj2)));
_L12:
        if (obj != null)
        {
            try
            {
                ((BufferedInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        if (obj1 != null)
        {
            try
            {
                ((BufferedOutputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        i = 1;
          goto _L8
_L4:
        try
        {
            a(b2);
            break MISSING_BLOCK_LABEL_548;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj2)
        {
            obj = null;
            obj1 = null;
        }
        finally
        {
            obj = null;
            obj2 = obj4;
        }
          goto _L9
_L2:
        a(b2);
        break MISSING_BLOCK_LABEL_548;
_L11:
        if (obj != null)
        {
            try
            {
                ((BufferedInputStream) (obj)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        if (obj2 != null)
        {
            try
            {
                ((BufferedOutputStream) (obj2)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj)
            {
                ((IOException) (obj)).printStackTrace();
            }
        }
        throw obj1;
        obj1;
        obj2 = obj4;
        continue; /* Loop/switch isn't completed */
        Exception exception1;
        exception1;
        obj2 = obj1;
        obj1 = exception1;
        continue; /* Loop/switch isn't completed */
        obj2;
        Object obj3 = obj1;
        obj1 = obj2;
        obj2 = obj;
        obj = obj3;
        if (true) goto _L11; else goto _L10
_L10:
        obj2;
        obj1 = obj;
        obj = null;
          goto _L9
        obj1 = null;
        obj = null;
          goto _L12
    }
}
