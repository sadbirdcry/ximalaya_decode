// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.b;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.ad.BdSoundAdModel;
import com.ximalaya.ting.android.model.ad.XMSoundAd;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.transaction.b:
//            f

public class e extends MyAsyncTask
{
    public static interface a
    {

        public abstract void a();

        public abstract void a(List list);
    }


    private static final String a;
    private long b;
    private Context c;
    private a d;
    private int e;
    private long f;
    private a g;

    public e(Context context, long l)
    {
        e = -1;
        g = new com.ximalaya.ting.android.transaction.b.f(this);
        c = context;
        b = l;
    }

    static a a(e e1)
    {
        return e1.d;
    }

    public static boolean a(Context context)
    {
        return LocalMediaService.getInstance() == null || LocalMediaService.getInstance().isPlayFromNetwork() || "WIFI".equals(NetworkUtils.getNetworkClass(context));
    }

    static Context b(e e1)
    {
        return e1.c;
    }

    public transient List a(Void avoid[])
    {
        if (NetworkUtils.isNetworkAvaliable(c)) goto _L2; else goto _L1
_L1:
        return null;
_L2:
        int i;
        avoid = new RequestParams();
        avoid.put("trackId", b);
        avoid.put("appid", 0);
        avoid.put("version", ToolUtil.getAppVersion(c));
        avoid.put("device", (new StringBuilder()).append("android-").append(android.os.Build.VERSION.RELEASE).toString());
        avoid.put("deviceId", ToolUtil.getDeviceToken(c));
        if (UserInfoMannage.hasLogined())
        {
            avoid.put("uid", UserInfoMannage.getInstance().getUser().getUid());
        }
        avoid.put("network", NetworkUtils.getNetworkClass(c));
        avoid.put("operator", NetworkUtils.getOperator(c));
        Iterator iterator;
        if (LocalMediaService.getInstance() != null && LocalMediaService.getInstance().isPlayFromNetwork())
        {
            avoid.put("mode", 0);
        } else
        {
            avoid.put("mode", 1);
        }
        if (PlayListControl.getPlayListManager().isManualPlay())
        {
            i = 0;
        } else
        {
            i = 1;
        }
        avoid.put("playMethod", i);
        com.ximalaya.ting.android.b.f.a().a(3000);
        avoid = com.ximalaya.ting.android.b.f.a().a(a, avoid, null, null, false);
        com.ximalaya.ting.android.b.f.a().a(f.d);
        if (((com.ximalaya.ting.android.b.n.a) (avoid)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (avoid)).a)) goto _L1; else goto _L3
_L3:
        avoid = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (avoid)).a);
        if (avoid.getIntValue("ret") != 0) goto _L1; else goto _L4
_L4:
        i = avoid.getIntValue("source");
        f = avoid.getLong("responseId").longValue();
        i;
        JVM INSTR tableswitch 0 1: default 377
    //                   0 288
    //                   1 370;
           goto _L5 _L6 _L7
_L6:
        avoid = avoid.getString("data");
        if (TextUtils.isEmpty(avoid)) goto _L1; else goto _L8
_L8:
        avoid = XMSoundAd.getListFromJSONStr(avoid);
        if (avoid != null)
        {
            try
            {
                for (iterator = avoid.iterator(); iterator.hasNext(); ((XMSoundAd)iterator.next()).setResponseId(f)) { }
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
                return null;
            }
        }
        return avoid;
_L7:
        e = i;
        return null;
_L5:
        return null;
    }

    public void a(a a1)
    {
        d = a1;
        myexec(new Void[0]);
    }

    protected void a(List list)
    {
        super.onPostExecute(list);
        if (e == 1)
        {
            BdSoundAdModel.requestData(c, f, g);
            return;
        } else
        {
            g.a(list);
            return;
        }
    }

    public Object doInBackground(Object aobj[])
    {
        return a((Void[])aobj);
    }

    protected void onPostExecute(Object obj)
    {
        a((List)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        if (d != null)
        {
            d.a();
        }
    }

    static 
    {
        a = (new StringBuilder()).append(com.ximalaya.ting.android.a.I).append("soundPatch").toString();
    }
}
