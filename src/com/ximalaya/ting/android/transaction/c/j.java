// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.c;

import android.net.Uri;
import android.text.TextUtils;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.ToolUtil;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ServerSocketChannel;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

// Referenced classes of package com.ximalaya.ting.android.transaction.c:
//            e

public class j
    implements Runnable
{

    private static AtomicInteger e = new AtomicInteger(0);
    private static volatile j f = null;
    private static final Object g = new Object();
    private boolean a;
    private int b;
    private ServerSocketChannel c;
    private Thread d;
    private e h;
    private boolean i;
    private String j;

    public j()
    {
        i = false;
        j = "";
        b = 0;
    }

    public static j a()
    {
        if (f != null) goto _L2; else goto _L1
_L1:
        Object obj = g;
        obj;
        JVM INSTR monitorenter ;
        if (f != null) goto _L2; else goto _L3
_L3:
        ToolUtil.createAppDirectory();
        f = new j();
        f.c();
_L4:
        f.f();
_L2:
        return f;
        Object obj1;
        obj1;
        ((IOException) (obj1)).printStackTrace();
          goto _L4
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
        obj1;
        f = null;
          goto _L2
    }

    public static j b()
    {
        return f;
    }

    private j f()
        throws IllegalStateException
    {
        if (c == null)
        {
            throw new IllegalStateException("Cannot start proxy; it has not been initialized.");
        } else
        {
            a = true;
            d = new Thread(this, "t_StreamProxy");
            d.start();
            return this;
        }
    }

    public String a(String s)
    {
        android.net.Uri.Builder builder = Uri.parse((new StringBuilder()).append("http://127.0.0.1:").append(e()).append("/").toString()).buildUpon();
        builder.appendQueryParameter("streamUrl", s);
        Logger.logToSd((new StringBuilder()).append("getLocalURL paramString:").append(s).append(" lastStreamUrl:").append(j).toString());
        if (TextUtils.isEmpty(j) || j.equals(s))
        {
            i = false;
        } else
        {
            i = true;
        }
        j = s;
        return builder.toString();
    }

    public void a(Socket socket, String s)
    {
        if (h != null && i)
        {
            h.a();
            i = false;
            Logger.logToSd((new StringBuilder()).append("localSocket close:").append(socket.toString()).append(" lastStreamUrl:").append(j).toString());
        }
        h = new e(socket, s);
        h.setName((new StringBuilder()).append("t_handlerThread_").append(e.getAndIncrement()).append("_").append(s).toString());
        h.start();
    }

    public j c()
        throws IOException
    {
        c = ServerSocketChannel.open();
        c.socket().bind(new InetSocketAddress(0));
        b = c.socket().getLocalPort();
        if (Logger.isLoggable("dl_mp3", 3))
        {
            Logger.d("dl_mp3", (new StringBuilder()).append("port ").append(b).append(" obtained").toString());
        }
        return this;
    }

    public void d()
    {
        h = null;
        a = false;
        if (d == null)
        {
            throw new IllegalStateException("Cannot stop proxy; it has not been started.");
        }
        try
        {
            c.close();
        }
        catch (IOException ioexception)
        {
            ioexception.printStackTrace();
        }
        f = null;
        d.interrupt();
        try
        {
            d.join(5000L);
            return;
        }
        catch (InterruptedException interruptedexception)
        {
            interruptedexception.printStackTrace();
        }
    }

    public int e()
    {
        return b;
    }

    public void run()
    {
_L1:
        if (!a)
        {
            break MISSING_BLOCK_LABEL_111;
        }
        Socket socket = c.socket().accept();
        Logger.logToSd((new StringBuilder()).append("localSocket accept:").append(socket.toString()).toString());
        a(socket, (new StringBuilder()).append(UUID.randomUUID().toString()).append(e.get()).toString());
          goto _L1
        Object obj;
        obj;
        Logger.logToSd((new StringBuilder()).append("StreamProxy run exception:").append(((Throwable) (obj)).getMessage()).toString());
          goto _L1
        obj;
        throw obj;
    }

}
