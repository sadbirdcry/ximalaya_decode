// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.c;

import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.util.Logger;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

// Referenced classes of package com.ximalaya.ting.android.transaction.c:
//            b, g, a, d, 
//            h

public class i
{

    private static final HashMap b;
    LinkedBlockingQueue a;
    private volatile boolean c;
    private boolean d;
    private String e;
    private h f;
    private Socket g;

    public i(Socket socket, String s)
    {
        g = socket;
        c = false;
        d = true;
        a = new LinkedBlockingQueue(10);
        e = s;
    }

    private void d()
    {
        a.clear();
        b b1 = new b();
        b1.a(ByteBuffer.allocate(0x10000));
        b1.d();
        try
        {
            a.put(b1);
            return;
        }
        catch (InterruptedException interruptedexception)
        {
            return;
        }
    }

    public void a()
    {
        c = true;
        d();
    }

    public void a(g g1)
        throws IOException
    {
        String s = (String)g1.a("streamUrl");
        Logger.log("dl_mp3", (new StringBuilder()).append("originalUrl===[").append(s).append("]\n").toString());
        Object obj2 = com.ximalaya.ting.android.transaction.c.a.a(a.ao, s);
        g1.a("Content-Type", "audio/mpeg");
        g1.a("Accept-Ranges", "bytes");
        g1.a("Server", "Apache/2");
        if (((com.ximalaya.ting.android.transaction.c.a) (obj2)).a().d())
        {
            int j = ((com.ximalaya.ting.android.transaction.c.a) (obj2)).a().b();
            int l = j - 1;
            boolean flag = g1.c("Range");
            Object obj1 = null;
            Object obj;
            int k;
            char c1;
            if (flag)
            {
                k = Integer.decode((String)g1.b("Range.start")).intValue();
                obj1 = String.format("bytes %d-%d/%d", new Object[] {
                    Integer.valueOf(k), Integer.valueOf(l), Integer.valueOf(j)
                });
                g1.a("Content-Range", ((String) (obj1)));
                c1 = '\316';
                obj = "Partial Content";
                j = k / 0x10000;
            } else
            {
                obj = "OK";
                c1 = '\310';
                j = 0;
                k = 0;
            }
            Logger.logToSd((new StringBuilder()).append("\u65B0\u7684\u8BF7\u6C42\u5F00\u59CBrequest\uFF1ArangeDes").append(((String) (obj1))).toString());
            g1.a("Content-Length", Integer.toString((l - k) + 1));
            g1.a(c1, ((String) (obj)));
            obj1 = new h(((com.ximalaya.ting.android.transaction.c.a) (obj2)), j, a, this);
            f = ((h) (obj1));
            ((h) (obj1)).setName((new StringBuilder()).append("t_Read_").append(j).append("_").append(e).toString());
            ((h) (obj1)).start();
            obj1 = g1.a();
            j = 1;
        } else
        {
            g1.a("Content-Length", Integer.toString(0));
            g1.a(500, "OK");
            g1.b();
            Logger.logToSd("\u65B0\u7684\u8BF7\u6C42\u5F00\u59CBerror");
            return;
        }
        if (c) goto _L2; else goto _L1
_L1:
        obj2 = (b)a.take();
        if (!((b) (obj2)).b) goto _L4; else goto _L3
_L3:
        j = ((b) (obj2)).c;
        g1.a("Content-Length", Integer.toString(0));
        g1.a(j, ((String) (obj)));
        g1.a(false);
_L2:
        g1.c();
_L7:
        d = false;
        return;
_L4:
        if (((b) (obj2)).b()) goto _L2; else goto _L5
_L5:
        if (j == 0)
        {
            break MISSING_BLOCK_LABEL_506;
        }
        j = k % 0x10000;
        ((OutputStream) (obj1)).write(((b) (obj2)).a().array(), j, ((b) (obj2)).a().array().length - j);
        Logger.logToSd((new StringBuilder()).append("\u6D88\u8D39first index:").append(((b) (obj2)).e()).append(" size:").append(((b) (obj2)).c()).append(" offset:").append(j).toString());
        j = 0;
_L6:
        try
        {
            ((OutputStream) (obj1)).flush();
            if (!c)
            {
                g1.b();
            }
            continue; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
        finally
        {
            g1.c();
            d = false;
            throw obj;
        }
        break MISSING_BLOCK_LABEL_563;
        ((OutputStream) (obj1)).write(((b) (obj2)).a().array());
        Logger.logToSd((new StringBuilder()).append("\u6D88\u8D39index:").append(((b) (obj2)).e()).append(" size:").append(((b) (obj2)).c()).toString());
          goto _L6
        Logger.logToSd((new StringBuilder()).append("doExchange run exception:").append(((Exception) (obj)).getMessage()).toString());
        g1.c();
          goto _L7
        if (true) goto _L9; else goto _L8
_L9:
        break MISSING_BLOCK_LABEL_319;
_L8:
    }

    public h b()
    {
        return f;
    }

    public boolean c()
    {
        return g.isClosed() || g.isOutputShutdown();
    }

    static 
    {
        b = new HashMap();
        b.put("", "content/unknown");
        b.put(".js", "application/javascript");
        b.put(".ps", "application/postscript");
        b.put(".css", "text/css");
        b.put(".csv", "text/csv");
        b.put(".htm", "text/html");
        b.put(".html", "text/html");
        b.put(".xhtml", "text/xhtml");
        b.put(".text", "text/plain");
        b.put(".c", "text/plain");
        b.put(".cc", "text/plain");
        b.put(".c++", "text/plain");
        b.put(".h", "text/plain");
        b.put(".pl", "text/plain");
        b.put(".txt", "text/plain");
        b.put(".java", "text/plain");
        b.put(".pdf", "application/pdf");
        b.put(".zip", "application/zip");
        b.put(".gzip", "application/x-gzip");
        b.put(".tar", "application/x-tar");
        b.put(".au", "audio/basic");
        b.put(".mp3", "audio/mp3");
        b.put(".mp4", "audio/mp4");
        b.put(".mpeg", "audio/mpeg");
        b.put(".wav", "audio/x-wav");
        b.put(".gif", "image/gif");
        b.put(".jpg", "image/jpeg");
        b.put(".jpeg", "image/jpeg");
        b.put(".png", "image/png");
        b.put(".tff", "image/tiff");
        b.put(".tiff", "image/tiff");
        b.put(".JS", "application/javascript");
        b.put(".PS", "application/postscript");
        b.put(".CSS", "text/css");
        b.put(".CSV", "text/csv");
        b.put(".HTM", "text/html");
        b.put(".HTML", "text/html");
        b.put(".XHTML", "text/xhtml");
        b.put(".TEXT", "text/plain");
        b.put(".C", "text/plain");
        b.put(".CC", "text/plain");
        b.put(".C++", "text/plain");
        b.put(".H", "text/plain");
        b.put(".PL", "text/plain");
        b.put(".TXT", "text/plain");
        b.put(".JAVA", "text/plain");
        b.put(".PDF", "application/pdf");
        b.put(".ZIP", "application/zip");
        b.put(".GZIP", "application/x-gzip");
        b.put(".TAR", "application/x-tar");
        b.put(".AU", "audio/basic");
        b.put(".MP3", "audio/mp3");
        b.put(".MP4", "audio/mp4");
        b.put(".MPEG", "audio/mpeg");
        b.put(".WAV", "audio/x-wav");
        b.put(".GIF", "image/gif");
        b.put(".JPG", "image/jpeg");
        b.put(".JPEG", "image/jpeg");
        b.put(".PNG", "image/png");
        b.put(".TFF", "image/tiff");
        b.put(".TIFF", "image/tiff");
    }
}
