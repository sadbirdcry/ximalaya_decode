// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.c;

import com.ximalaya.ting.android.util.Logger;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// Referenced classes of package com.ximalaya.ting.android.transaction.c:
//            f

public class g
{

    private static final Pattern k = Pattern.compile("([A-Z]*)[ \t]*([^ \t]*)[ \t]*([^ \t]*)");
    private static final Pattern l = Pattern.compile("([^ \t]*):[ \t]*(.*)");
    private final InputStream a;
    private final OutputStream b;
    private final Properties c;
    private final Properties d;
    private final Properties e;
    private final Properties f;
    private final Properties g;
    private final ByteArrayOutputStream h;
    private Socket i;
    private boolean j;

    private g(InputStream inputstream, OutputStream outputstream)
        throws IOException
    {
        j = true;
        a = inputstream;
        b = outputstream;
        c = new Properties();
        d = new Properties();
        e = new Properties();
        f = new Properties();
        g = new Properties();
        h = new ByteArrayOutputStream();
        try
        {
            j = g();
        }
        // Misplaced declaration of an exception variable
        catch (InputStream inputstream)
        {
            a(outputstream, inputstream);
            throw new IOException(inputstream.toString());
        }
        e();
        if ("GET".equals(c.get("method")))
        {
            f();
        } else
        if ("POST".equals(c.get("method")))
        {
            h();
            return;
        }
    }

    public g(Socket socket)
        throws IOException
    {
        this(socket.getInputStream(), socket.getOutputStream());
        i = socket;
    }

    private void a(int i1)
        throws IOException
    {
        String as[];
        int k1;
        boolean flag = false;
        as = (new String(c(i1), "UTF-8")).split("&");
        k1 = as.length;
        i1 = ((flag) ? 1 : 0);
_L2:
        String s;
        if (i1 >= k1)
        {
            break MISSING_BLOCK_LABEL_115;
        }
        s = as[i1];
        String s2;
        int j1;
        s2 = URLDecoder.decode(s, "UTF=8");
        j1 = s2.indexOf("=");
        if (j1 >= 0)
        {
            try
            {
                String s1 = s2.substring(0, j1);
                s2 = s2.substring(j1 + 1);
                f.put(s1, s2);
            }
            catch (UnsupportedEncodingException unsupportedencodingexception) { }
            break MISSING_BLOCK_LABEL_116;
        }
        f.put(s2, "");
        break MISSING_BLOCK_LABEL_116;
        return;
        i1++;
        if (true) goto _L2; else goto _L1
_L1:
    }

    public static void a(OutputStream outputstream, f f1)
    {
        outputstream = new PrintStream(outputstream);
        outputstream.print((new StringBuilder()).append("HTTP ").append(f1.a).append(" ").append(f1.getMessage()).append("\r\n").toString());
        outputstream.println("\r\n");
        f1.printStackTrace(outputstream);
    }

    private void b(int i1)
        throws IOException
    {
        f.put("octetStream", c(i1));
    }

    private byte[] c(int i1)
        throws IOException
    {
        byte abyte0[] = new byte[i1];
        for (int j1 = 0; j1 < i1; j1 += a.read(abyte0, j1, i1 - j1)) { }
        return abyte0;
    }

    private Date d()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2012, 12, 21);
        return calendar.getTime();
    }

    private void d(String s)
    {
        if (s != null)
        {
            s = s.split(";");
            int j1 = s.length;
            for (int i1 = 0; i1 < j1; i1++)
            {
                String as[] = s[i1].trim().split("=");
                d.put(as[0], as[1]);
            }

        }
    }

    private String e(String s)
    {
        s = Pattern.compile("bytes=(\\d+)-").matcher(s);
        if (s.matches())
        {
            return s.group(1);
        } else
        {
            return "0";
        }
    }

    private void e()
    {
        Object obj = b("Cookie");
        if (obj instanceof Collection)
        {
            for (obj = ((Collection)obj).iterator(); ((Iterator) (obj)).hasNext(); d((String)((Iterator) (obj)).next())) { }
        } else
        {
            d((String)obj);
        }
    }

    private void f()
    {
        String s = (String)b("uri.query");
        if (s == null) goto _L2; else goto _L1
_L1:
        String as[];
        int i1;
        int j1;
        as = s.split("&");
        j1 = as.length;
        i1 = 0;
_L8:
        if (i1 >= j1) goto _L2; else goto _L3
_L3:
        String s1 = as[i1];
        String s3;
        int k1;
        s3 = URLDecoder.decode(s1, "UTF-8");
        k1 = s3.indexOf("=");
        if (k1 < 0) goto _L5; else goto _L4
_L4:
        try
        {
            String s2 = s3.substring(0, k1);
            s3 = s3.substring(k1 + 1);
            e.put(s2, s3);
            Logger.log("dl_mp3", (new StringBuilder()).append("parseGetParameters: key=[").append(s2).append("], value=[").append(s3).append("]\n").toString());
        }
        catch (UnsupportedEncodingException unsupportedencodingexception) { }
          goto _L6
_L5:
        e.put(s3, "");
          goto _L6
_L2:
        return;
_L6:
        i1++;
        if (true) goto _L8; else goto _L7
_L7:
    }

    private boolean g()
        throws f
    {
        Object obj2;
        Object obj3;
        Object obj;
        try
        {
            obj3 = i();
            obj = (new StringBuilder()).append("").append(((String) (obj3))).append("\n").toString();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            throw new f(400, "Failed reading input stream");
        }
        obj2 = k.matcher(((CharSequence) (obj3)));
        if (!((Matcher) (obj2)).matches())
        {
            throw new f(400, ((String) (obj3)));
        }
        c.put("method", ((Matcher) (obj2)).group(1));
        Logger.log("dl_mp3", (new StringBuilder()).append("parse client HeadLine: key=[method], value=[").append(((Matcher) (obj2)).group(1)).append("]\n").toString());
        obj3 = ((Matcher) (obj2)).group(2);
        c.put("uri.string", obj3);
        Logger.log("dl_mp3", (new StringBuilder()).append("parse client HeadLine: key=[uri.string], value=[").append(((Matcher) (obj2)).group(2)).append("]\n").toString());
        URI uri;
        try
        {
            uri = new URI(((Matcher) (obj2)).group(2));
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            throw new f(404, ((String) (obj3)));
        }
        c.put("uri", uri);
        if (uri.getScheme() != null)
        {
            c.put("uri.scheme", uri.getScheme());
        }
        if (uri.getUserInfo() != null)
        {
            c.put("uri.userinfo", uri.getUserInfo());
        }
        if (uri.getHost() != null)
        {
            c.put("uri.host", uri.getHost());
        }
        if (uri.getPort() >= 0)
        {
            c.put("uri.port", Integer.toString(uri.getPort()));
        }
        if (uri.getPath() != null)
        {
            c.put("uri.path", uri.getPath());
            if (uri.getQuery() != null)
            {
                c.put("uri.query", uri.getQuery());
            }
            if (uri.getFragment() != null)
            {
                c.put("uri.fragment", uri.getFragment());
            }
            c.put("version", ((Matcher) (obj2)).group(3));
            obj2 = obj;
            break MISSING_BLOCK_LABEL_394;
        } else
        {
            throw new f(404, (new StringBuilder()).append(((String) (obj3))).append(": path is required").toString());
        }
        do
        {
            Object obj1;
            try
            {
                obj3 = i();
                obj1 = (new StringBuilder()).append(((String) (obj2))).append(((String) (obj3))).append("\n").toString();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                throw new f(400, "Can't read input stream");
            }
            if (((String) (obj3)).length() == 0)
            {
                return true;
            }
            obj3 = l.matcher(((CharSequence) (obj3)));
            obj2 = obj1;
            if (((Matcher) (obj3)).matches())
            {
                obj2 = ((Matcher) (obj3)).group(1);
                String s = ((Matcher) (obj3)).group(2);
                c.put(obj2, s);
                if (((String) (obj2)).equalsIgnoreCase("Range"))
                {
                    c.put("Range.start", e(s));
                }
                Logger.log("dl_mp3", (new StringBuilder()).append("parse client HeadLine: key=[").append(((String) (obj2))).append("], value=[").append(((Matcher) (obj3)).group(2)).append("]\n").toString());
                obj2 = obj1;
            }
        } while (true);
    }

    private void h()
        throws IOException
    {
        String s = (String)b("Content-Type");
        int i1 = Integer.parseInt((String)b("Content-Length"));
        if ("application/x-www-form-urlencoded".equals(s))
        {
            a(i1);
            return;
        }
        if ("application/octet-stream".equals(s))
        {
            b(i1);
            return;
        } else
        {
            throw new UnsupportedOperationException((new StringBuilder()).append(s).append(" not yet implemented").toString());
        }
    }

    private String i()
        throws IOException
    {
        byte abyte0[] = new byte[8192];
        for (int i1 = 0; i1 < abyte0.length; i1++)
        {
            int j1 = a.read();
            if (j1 < 0)
            {
                throw new IOException("Unexpected end of input");
            }
            if (j1 == 10 || j1 == 13)
            {
                a.read();
                return new String(abyte0, 0, i1);
            }
            abyte0[i1] = (byte)j1;
        }

        throw new IOException("Input line too long");
    }

    public OutputStream a()
    {
        return h;
    }

    public Object a(String s)
    {
        return e.get(s);
    }

    public void a(int i1, String s)
    {
        try
        {
            s = (new StringBuilder()).append("HTTP/1.0 ").append(i1).append(" ").append(s).append("\r\n").toString();
            b.write(s.getBytes());
            Logger.log("dl_mp3", s);
            if (!g.containsKey("Last-Modified"))
            {
                g.put("Last-Modified", d());
            }
            if (!g.containsKey("Date"))
            {
                g.put("Date", new Date());
            }
            Object obj;
            for (s = g.keySet().iterator(); s.hasNext(); Logger.log("dl_mp3", obj))
            {
                obj = s.next();
                obj = (new StringBuilder()).append(obj).append(": ").append(g.get(obj)).append("\r\n").toString();
                b.write(((String) (obj)).getBytes());
            }

        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
        b.write("\r\n".getBytes());
        b.flush();
        return;
    }

    public void a(String s, String s1)
    {
        g.put(s, s1);
    }

    public void a(boolean flag)
        throws IOException
    {
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_45;
        }
        if (!i.isClosed() && h.size() > 0)
        {
            b.write(h.toByteArray());
            b.flush();
        }
        h.reset();
        return;
        IOException ioexception;
        ioexception;
        throw ioexception;
    }

    public Object b(String s)
    {
        return c.get(s);
    }

    public void b()
        throws IOException
    {
        a(true);
    }

    public void c()
    {
        Logger.log("dl_mp3", "========sendResponseEnd()");
        try
        {
            h.close();
            b.close();
            return;
        }
        catch (IOException ioexception)
        {
            Logger.log("dl_mp3", (new StringBuilder()).append("!!![sendResponseEnd]: ").append(ioexception.getMessage()).toString());
        }
    }

    public boolean c(String s)
    {
        return c.containsKey(s);
    }

}
