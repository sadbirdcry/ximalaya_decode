// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.c;

import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.Utilities;
import java.io.InputStream;
import java.nio.ByteBuffer;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

// Referenced classes of package com.ximalaya.ting.android.transaction.c:
//            a, d

public class c
{

    private a a;
    private int b;
    private ByteBuffer c;
    private boolean d;
    private volatile HttpClient e;

    public c(HttpClient httpclient, a a1, int i)
    {
        Logger.log("dl_mp3", (new StringBuilder()).append("======================DownloadThread Constructor(").append(i).append(")").toString());
        a = a1;
        b = i;
        c = ByteBuffer.allocate(0x10000);
        d = false;
        e = httpclient;
    }

    public int a()
    {
        Object obj2;
        int l;
        l = 0;
        if (d || a == null || b < 0 || Utilities.isBlank(a.b()) || e == null)
        {
            return -1;
        }
        obj2 = new HttpGet(a.b());
        if (b != a.a().a() - 1) goto _L2; else goto _L1
_L1:
        int i;
        int j;
        j = b;
        i = a.a().b();
        j *= 0x10000;
        i--;
_L8:
        Object obj;
        Header aheader[];
        obj = String.format("bytes=%d-%d", new Object[] {
            Integer.valueOf(j), Integer.valueOf(i)
        });
        ((HttpGet) (obj2)).addHeader("Range", ((String) (obj)));
        obj2 = e.execute(((org.apache.http.client.methods.HttpUriRequest) (obj2)));
        aheader = ((HttpResponse) (obj2)).getHeaders("Content-Range");
        Logger.logToSd((new StringBuilder()).append("\u4E0B\u8F7DContent-Range\u5F00\u59CBurl:").append(a.b()).append(" index:").append(b).toString());
        if (aheader == null) goto _L4; else goto _L3
_L3:
        if (aheader.length <= 0) goto _L4; else goto _L5
_L5:
        int i1 = aheader.length;
        int k = 0;
_L15:
        if (k >= i1) goto _L4; else goto _L6
_L6:
        Header header = aheader[k];
        String s;
        s = header.getValue();
        if (s.contains(((String) (obj)).subSequence("bytes=".length(), ((String) (obj)).length())))
        {
            Logger.logToSd((new StringBuilder()).append("name:").append(header.getName()).append("   value:").append(s).append("  right\uFF1A").append(((String) (obj))).toString());
            break MISSING_BLOCK_LABEL_678;
        }
          goto _L7
_L2:
        j = b;
        i = b;
        j *= 0x10000;
        i = (i + 1) * 0x10000 - 1;
          goto _L8
_L7:
        Logger.logToSd((new StringBuilder()).append("name:").append(header.getName()).append("   value:").append(s).append("  error\uFF1A").append(((String) (obj))).toString());
        break MISSING_BLOCK_LABEL_678;
        Object obj1;
        obj1;
        Logger.logToSd((new StringBuilder()).append("download run exception:").append(((Exception) (obj1)).getMessage()).toString());
_L10:
        return -1;
_L4:
        Logger.logToSd("\u4E0B\u8F7DContent-Range\u7ED3\u675F");
        i1 = ((HttpResponse) (obj2)).getStatusLine().getStatusCode();
        if (i1 != 206)
        {
            break MISSING_BLOCK_LABEL_666;
        }
        obj1 = ((HttpResponse) (obj2)).getEntity();
        if (obj1 == null) goto _L10; else goto _L9
_L9:
        obj2 = ((HttpEntity) (obj1)).getContent();
        k = l;
        if (((HttpEntity) (obj1)).getContentLength() == 0x10000L)
        {
            break MISSING_BLOCK_LABEL_508;
        }
        if (b != a.a().a() - 1) goto _L10; else goto _L11
_L11:
        k = l;
_L13:
        l = ((InputStream) (obj2)).read(c.array(), k, 0x10000 - k);
        k += l;
        if (l > 0) goto _L13; else goto _L12
_L12:
        ((InputStream) (obj2)).close();
        k = c.array().length;
        i -= j;
        if (i + 1 != k)
        {
            break MISSING_BLOCK_LABEL_620;
        }
        Logger.logToSd((new StringBuilder()).append(a.b()).append("\u4E0B\u8F7Dright bLength\uFF1A").append(i).toString());
_L14:
        a.a(c);
        return i1;
        obj1;
        throw obj1;
        Logger.logToSd((new StringBuilder()).append(a.b()).append("\u4E0B\u8F7Derror bLength\uFF1A").append(k).append(" rangeLength").append(i).toString());
          goto _L14
        a.a(i1);
        return i1;
        k++;
          goto _L15
    }

    public void b()
    {
        d = true;
    }
}
