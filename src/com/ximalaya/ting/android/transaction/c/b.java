// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.c;

import com.ximalaya.ting.android.util.Logger;
import java.nio.ByteBuffer;

class b
{

    public ByteBuffer a;
    public boolean b;
    public int c;
    private int d;
    private boolean e;
    private int f;

    public b()
    {
        Logger.log("dl_mp3", "======================BufferItem Constructor()");
        e = false;
        f = 0;
    }

    public ByteBuffer a()
    {
        if (a.hasArray())
        {
            return ByteBuffer.wrap(a.array());
        } else
        {
            return ByteBuffer.allocate(0);
        }
    }

    public void a(int i)
    {
        d = i;
    }

    public void a(ByteBuffer bytebuffer)
    {
        if (bytebuffer.hasArray())
        {
            a = ByteBuffer.wrap(bytebuffer.array());
            f = bytebuffer.array().length;
            Logger.log("dl_mp3", (new StringBuilder()).append("======================BufferItem setBuffer(").append(f).append(")").toString());
        }
    }

    public boolean b()
    {
        return e;
    }

    public int c()
    {
        return f;
    }

    public void d()
    {
        e = true;
    }

    public int e()
    {
        return d;
    }
}
