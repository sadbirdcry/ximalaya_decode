// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.content.Context;
import android.content.IntentFilter;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            c

public abstract class b
{

    protected Context mAppCtx;
    protected TelephonyManager mSIM1Manager;
    protected TelephonyManager mSIM2Manager;
    protected IntentFilter mTelFilter;
    private PhoneStateListener mTelListener;
    protected TelephonyManager mTelephonyManager;

    public b(Context context)
    {
        mTelListener = new c(this);
        mAppCtx = context.getApplicationContext();
        try
        {
            mTelephonyManager = (TelephonyManager)mAppCtx.getSystemService("phone");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
        try
        {
            mSIM1Manager = (TelephonyManager)mAppCtx.getSystemService("phone1");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
        try
        {
            mSIM2Manager = (TelephonyManager)mAppCtx.getSystemService("phone2");
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
    }

    protected abstract void onCallHangup(String s);

    protected abstract void onCallRinging(String s);

    protected abstract void onCalling(String s);

    public void registe()
    {
        if (mTelephonyManager != null)
        {
            mTelephonyManager.listen(mTelListener, 32);
        }
        if (mSIM1Manager != null)
        {
            mSIM1Manager.listen(mTelListener, 32);
        }
        if (mSIM2Manager != null)
        {
            mSIM2Manager.listen(mTelListener, 32);
        }
    }

    public void unRegiste()
    {
        if (mTelephonyManager != null)
        {
            mTelephonyManager.listen(mTelListener, 0);
        }
        if (mSIM1Manager != null)
        {
            mSIM1Manager.listen(mTelListener, 0);
        }
        if (mSIM2Manager != null)
        {
            mSIM2Manager.listen(mTelListener, 0);
        }
    }
}
