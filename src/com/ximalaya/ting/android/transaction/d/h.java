// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.media.MediaPlayer;
import com.ximalaya.ting.android.library.util.Logger;
import java.io.FileDescriptor;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            i, j

public class h
{
    public static interface a
    {

        public abstract void onComplete();

        public abstract void onError(Exception exception, int k, int l);

        public abstract void onPause();

        public abstract void onStart();

        public abstract void onStop();
    }


    private MediaPlayer a;
    private int b;
    private float c;
    private float d;
    private boolean e;
    private android.media.MediaPlayer.OnCompletionListener f;
    private a g;
    private int h;

    public h()
    {
        b = 3;
        c = 1.0F;
        d = 1.0F;
        e = false;
        h = -1;
        c();
    }

    static int a(h h1, int k)
    {
        h1.h = k;
        return k;
    }

    static a a(h h1)
    {
        return h1.g;
    }

    static android.media.MediaPlayer.OnCompletionListener b(h h1)
    {
        return h1.f;
    }

    public int a()
    {
        switch (h)
        {
        default:
            return 0;

        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
            return a.getCurrentPosition();
        }
    }

    public void a(float f1, float f2)
    {
        c = f1;
        d = f2;
        if (h != -1)
        {
            a.setVolume(c, d);
        }
    }

    public void a(android.media.MediaPlayer.OnCompletionListener oncompletionlistener)
    {
        f = oncompletionlistener;
    }

    public void a(a a1)
    {
        g = a1;
    }

    public void a(FileDescriptor filedescriptor, int k)
    {
        Logger.e("MiniPlayer", (new StringBuilder()).append("init seek ").append(k).toString());
        try
        {
            c();
            a.setDataSource(filedescriptor);
            a.prepare();
        }
        // Misplaced declaration of an exception variable
        catch (FileDescriptor filedescriptor)
        {
            filedescriptor.printStackTrace();
            h = -1;
            return;
        }
        if (k <= 0)
        {
            break MISSING_BLOCK_LABEL_55;
        }
        a.seekTo(k);
        h = 1;
        return;
    }

    public void a(FileDescriptor filedescriptor, long l, long l1)
    {
        Logger.e("MiniPlayer", (new StringBuilder()).append("init offset ").append(l).append(", length ").append(l1).toString());
        try
        {
            c();
            a.setDataSource(filedescriptor, l, l1);
            a.prepare();
            h = 1;
            return;
        }
        // Misplaced declaration of an exception variable
        catch (FileDescriptor filedescriptor)
        {
            filedescriptor.printStackTrace();
        }
        h = -1;
    }

    public void a(String s)
    {
        try
        {
            c();
            a.setDataSource(s);
            a.prepare();
            h = 1;
            return;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
        }
        h = -1;
    }

    public void a(boolean flag)
    {
        e = flag;
        if (h != -1)
        {
            a.setLooping(flag);
        }
    }

    public int b()
    {
        switch (h)
        {
        default:
            return 0;

        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
            return a.getDuration();
        }
    }

    public void c()
    {
        if (a == null)
        {
            a = new MediaPlayer();
            h = 0;
            a.setOnErrorListener(new i(this));
            a.setOnCompletionListener(new j(this));
        }
        if (h == 2)
        {
            a.stop();
            h = 4;
            if (g != null)
            {
                g.onStop();
            }
        }
        a.reset();
        a.setLooping(e);
        a.setVolume(c, d);
        h = 0;
_L1:
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
        h = -1;
        if (g != null)
        {
            g.onError(exception, 0, 0);
            return;
        }
          goto _L1
    }

    public int d()
    {
        return h;
    }

    public boolean e()
    {
        return h == 2;
    }

    public boolean f()
    {
        return h == 3;
    }

    public void g()
    {
        if (h == 1 || h == 3 || h == 5)
        {
            a.start();
            h = 2;
            if (g != null)
            {
                g.onStart();
                return;
            }
            break MISSING_BLOCK_LABEL_126;
        }
        try
        {
            if (h == 4)
            {
                a.prepare();
                a.start();
                h = 2;
                if (g != null)
                {
                    g.onStart();
                    return;
                }
            }
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            h = -1;
            if (g != null)
            {
                g.onError(exception, 0, 0);
            }
        }
    }

    public void h()
    {
        if (h == 2)
        {
            a.pause();
            h = 3;
            if (g != null)
            {
                g.onPause();
            }
        }
_L1:
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
        h = -1;
        if (g != null)
        {
            g.onError(exception, 0, 0);
            return;
        }
          goto _L1
    }

    public void i()
    {
        if (h == 2)
        {
            a.stop();
            h = 4;
            if (g != null)
            {
                g.onStop();
            }
        }
_L1:
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
        h = -1;
        if (g != null)
        {
            g.onError(exception, 0, 0);
            return;
        }
          goto _L1
    }

    public void j()
    {
        if (a != null)
        {
            if (h == 2)
            {
                a.stop();
                if (g != null)
                {
                    g.onStop();
                }
            }
            a.release();
        }
_L2:
        a = null;
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
        if (g != null)
        {
            g.onError(exception, 0, 0);
        }
        if (true) goto _L2; else goto _L1
_L1:
    }
}
