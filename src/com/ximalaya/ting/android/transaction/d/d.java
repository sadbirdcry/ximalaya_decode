// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.record.BgSound;
import com.ximalaya.ting.android.util.FileUtils;
import com.ximalaya.ting.android.util.Session;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            x, e, r

public class d
{

    private static d a;
    private static byte b[] = new byte[0];
    private x c;
    private BlockingQueue d;
    private List e;
    private int f;
    private int g;
    private int h;
    private TimeUnit i;
    private String j;
    private List k;
    private x.a l;

    private d()
    {
        f = 1;
        g = 1;
        h = 30;
        i = TimeUnit.SECONDS;
        k = new ArrayList();
        f();
        d = new LinkedBlockingQueue();
        e = new ArrayList();
        c = new x(f, g, h, i, d, e);
        l = new e(this);
        a(l);
        e();
    }

    public static d a()
    {
        if (a == null)
        {
            synchronized (b)
            {
                if (a == null)
                {
                    a = new d();
                }
            }
        }
        return a;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void e()
    {
        obj1 = SharedPreferencesUtil.getInstance(MyApplication.b()).getString("REC_COMPLETED_BG_TASK");
        if (!TextUtils.isEmpty(((CharSequence) (obj1)))) goto _L2; else goto _L1
_L1:
        return;
_L2:
        try
        {
            obj = JSON.parseArray(((String) (obj1)), com/ximalaya/ting/android/model/record/BgSound);
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
            Logger.e("BgSoundManager", (new StringBuilder()).append("").append(((String) (obj1))).toString());
            exception = null;
        }
        if (obj == null) goto _L1; else goto _L3
_L3:
        synchronized (k)
        {
            k.addAll(((java.util.Collection) (obj)));
        }
        return;
        obj;
        obj1;
        JVM INSTR monitorexit ;
        throw obj;
    }

    private void f()
    {
        if (TextUtils.isEmpty(j))
        {
            j = r.a().d();
        }
        File file = new File(j);
        if (file.isFile())
        {
            file.delete();
        }
        if (!file.exists())
        {
            file.mkdirs();
        }
        if (file.isDirectory())
        {
            if (file.canWrite());
        }
    }

    public void a(x.a a1)
    {
        c.a(a1);
    }

    public void a(String s)
    {
        j = s;
        f();
    }

    public void b()
    {
        c.a();
    }

    public void c()
    {
        byte abyte0[] = b;
        abyte0;
        JVM INSTR monitorenter ;
        List list = k;
        list;
        JVM INSTR monitorenter ;
        Object obj;
        b();
        k.clear();
        obj = MyApplication.b();
        if (obj == null)
        {
            break MISSING_BLOCK_LABEL_63;
        }
        obj = SharedPreferencesUtil.getInstance(((android.content.Context) (obj)));
        ((SharedPreferencesUtil) (obj)).removeByKey("REC_COMPLETED_BG_TASK");
        ((SharedPreferencesUtil) (obj)).removeByKey("BG_SOUND_CACHE");
        Session.getSession().put("BG_SOUND_CACHE_CHANGE", Boolean.valueOf(true));
        FileUtils.deleteDir(j);
        list;
        JVM INSTR monitorexit ;
        abyte0;
        JVM INSTR monitorexit ;
        return;
        Exception exception1;
        exception1;
        list;
        JVM INSTR monitorexit ;
        throw exception1;
        Exception exception;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void d()
    {
        synchronized (b)
        {
            c.a();
            c.b();
            c = null;
            a = null;
        }
        return;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

}
