// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            k, s

class m
    implements o.a
{

    final k a;

    m(k k1)
    {
        a = k1;
        super();
    }

    public void onPlayerPaused()
    {
        k.d(a);
        if (k.c(a) != null)
        {
            k.c(a).onPlayerPaused();
        }
    }

    public void onPlayerStart()
    {
        if (k.c(a) != null)
        {
            k.c(a).onPlayerStart();
        }
    }

    public void onPlayerStopped()
    {
        Logger.e("MixPlayback", "Pcm player stop called ");
        if (k.c(a) != null)
        {
            k.c(a).onPlayerStopped();
        }
    }

    public void onProgressUpdate(float f)
    {
        if (k.a(a) != 0L && (float)k.a(a) <= (float)k.b(a).i() * f)
        {
            a.d();
        } else
        if (k.c(a) != null)
        {
            k.c(a).onProgressUpdate(f);
            return;
        }
    }
}
