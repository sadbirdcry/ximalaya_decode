// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.os.SystemClock;
import com.ximalaya.ting.android.lib.ns.NSUtil;
import com.ximalaya.ting.android.library.util.Logger;
import java.io.IOException;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            g, t, u, s

public class p
    implements t.a
{
    public static interface a
    {

        public abstract void onError(Exception exception, int i1, int j1);

        public abstract void onPause();

        public abstract void onRecording(g g1);

        public abstract void onStart();

        public abstract void onStop();
    }


    private int a;
    private int b;
    private int c;
    private int d;
    private boolean e;
    private boolean f;
    private List g;
    private t h;
    private s i;
    private NSUtil j;
    private g k;
    private a l;
    private boolean m;
    private volatile int n;
    private int o;

    public p(s s1)
    {
        a = 1;
        b = 44100;
        c = 16;
        d = 2;
        e = false;
        f = false;
        m = false;
        n = 1;
        i = s1;
        m();
    }

    private void g()
    {
        if (m && (j == null || j.getToken() == 0L))
        {
            j = new NSUtil();
            j.setBufferSize(com.ximalaya.ting.android.transaction.d.g.b());
            j.nsInit();
            k = com.ximalaya.ting.android.transaction.d.g.a();
        }
    }

    private void h()
    {
        if (m && j != null)
        {
            j.nsRelease();
            j = null;
            com.ximalaya.ting.android.transaction.d.g.a(k);
        }
    }

    private void i()
    {
        e = false;
        if (h != null)
        {
            h.a();
            h = null;
        }
        h();
        n = 5;
    }

    private void j()
    {
        e = true;
        f = false;
        if (h == null || !h.b())
        {
            m();
        }
        g();
        h.start();
        n = 3;
    }

    private void k()
    {
        f = false;
        n = 3;
        if (l != null)
        {
            l.onStart();
        }
    }

    private void l()
    {
        f = true;
        n = 4;
        if (l != null)
        {
            l.onPause();
        }
    }

    private void m()
    {
        if (h == null || !h.b())
        {
            h = new t(a, b, c, d);
            h.a(this);
            n = 2;
        }
    }

    public void a(int i1)
    {
        o = i1;
    }

    public void a(int i1, int j1, int k1, int l1)
    {
        a = i1;
        b = j1;
        c = k1;
        d = l1;
        m();
    }

    public void a(g g1)
    {
        if (e && !f) goto _L2; else goto _L1
_L1:
        com.ximalaya.ting.android.transaction.d.g.a(g1);
_L4:
        return;
_L2:
        if (g1 == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (g != null)
        {
            int i1 = u.a(g1.a, g1.c, o);
            g.add(Integer.valueOf(i1));
        }
        int j1;
        if (!m)
        {
            break MISSING_BLOCK_LABEL_172;
        }
        long l1 = SystemClock.uptimeMillis();
        j1 = j.ns(g1.a, g1.c, k.a, 1);
        Logger.e("PcmRecorder", (new StringBuilder()).append("---Ns cost ").append(SystemClock.uptimeMillis() - l1).append("ms, len : ").append(j1).toString());
        if (j1 > 0)
        {
            try
            {
                i.b(k.a, 0, j1);
            }
            catch (IOException ioexception)
            {
                ioexception.printStackTrace();
                if (l != null)
                {
                    l.onError(ioexception, 1, 0);
                }
            }
        }
        if (l != null)
        {
            l.onRecording(g1);
            return;
        }
        continue; /* Loop/switch isn't completed */
        i.b(g1.a, g1.d, g1.c);
        break MISSING_BLOCK_LABEL_154;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void a(a a1)
    {
        l = a1;
    }

    public void a(Exception exception)
    {
        e = false;
        if (l != null)
        {
            l.onError(exception, 2, 0);
        }
    }

    public void a(List list)
    {
        g = list;
    }

    public void a(boolean flag, int i1)
    {
        m = flag;
        if (m)
        {
            g();
            return;
        } else
        {
            h();
            return;
        }
    }

    public boolean a()
    {
        return n == 3;
    }

    public void b()
    {
        switch (n)
        {
        case 3: // '\003'
        default:
            return;

        case 1: // '\001'
            m();
            j();
            return;

        case 2: // '\002'
            j();
            return;

        case 4: // '\004'
            k();
            return;

        case 5: // '\005'
            m();
            break;
        }
        j();
    }

    public void c()
    {
        switch (n)
        {
        case 1: // '\001'
        case 2: // '\002'
        case 4: // '\004'
        case 5: // '\005'
        default:
            return;

        case 3: // '\003'
            l();
            break;
        }
    }

    public void d()
    {
        switch (n)
        {
        case 1: // '\001'
        case 2: // '\002'
        case 5: // '\005'
        default:
            return;

        case 3: // '\003'
        case 4: // '\004'
            i();
            break;
        }
    }

    public void e()
    {
        e = false;
        if (l != null)
        {
            l.onStop();
        }
    }

    public void f()
    {
        e = true;
        if (l != null)
        {
            l.onStart();
        }
    }
}
