// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.util.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;

public class s
{

    private int a;
    private int b;
    private int c;
    private float d;
    private long e;
    private long f;
    private String g;
    private String h;
    private FileInputStream i;
    private FileOutputStream j;
    private File k;
    private byte l[];

    public s(String s1, String s2, int i1, int j1, int k1)
        throws IOException
    {
        l = new byte[0];
        g = s1;
        h = s2;
        a = i1;
        b = j1;
        c = k1;
        d = (float)(a * b * (c / 8)) / 1000F;
        k();
    }

    private long c(long l1)
    {
        long l2 = b * (c / 8);
        return l2 * (long)(new BigDecimal((float)l1 / (float)l2)).setScale(0, 4).intValue();
    }

    private void k()
        throws IOException
    {
        byte abyte0[] = l;
        abyte0;
        JVM INSTR monitorenter ;
        h();
        boolean flag1 = true;
        File file;
        file = new File(g);
        if (file.isFile())
        {
            flag1 = true & file.delete();
        }
        boolean flag = flag1;
        if (!file.exists())
        {
            flag = flag1 & file.mkdirs();
        }
        k = new File(file, h);
        flag1 = flag;
        if (k.exists())
        {
            flag1 = flag & k.delete();
        }
        if (!(flag1 & k.createNewFile()))
        {
            throw new IOException("Init failed, Maybe permission deny!");
        }
        break MISSING_BLOCK_LABEL_127;
        Exception exception;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
        i = new FileInputStream(k);
        j = new FileOutputStream(k);
        e = 0L;
        f = k.length();
        abyte0;
        JVM INSTR monitorexit ;
    }

    public float a()
    {
        return d * 1000F;
    }

    public int a(byte abyte0[], int i1, int j1)
        throws IOException
    {
        synchronized (l)
        {
            i1 = i.read(abyte0, i1, j1);
            e = e + (long)i1;
        }
        return i1;
        abyte0;
        abyte1;
        JVM INSTR monitorexit ;
        throw abyte0;
    }

    public long a(long l1)
        throws IOException
    {
        byte abyte0[] = l;
        abyte0;
        JVM INSTR monitorenter ;
        if (l1 >= 0L) goto _L2; else goto _L1
_L1:
        long l2 = 0L;
_L4:
        d();
        l1 = i.skip(l2);
        e = e + l1;
        abyte0;
        JVM INSTR monitorexit ;
        return l1;
_L2:
        l2 = l1;
        if (l1 > f)
        {
            l2 = f;
        }
        if (true) goto _L4; else goto _L3
_L3:
        Exception exception;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void a(long l1, long l2)
        throws IOException
    {
        byte abyte0[] = l;
        abyte0;
        JVM INSTR monitorenter ;
        if (l1 < 0L)
        {
            break MISSING_BLOCK_LABEL_30;
        }
        if (l2 <= f && l1 <= l2)
        {
            break MISSING_BLOCK_LABEL_86;
        }
        throw new IOException((new StringBuilder()).append("Illegal Arguments. start index is ").append(l1).append(" and end index is ").append(l2).append(", while file length is ").append(f).toString());
        Exception exception;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
        File file;
        FileOutputStream fileoutputstream;
        FileInputStream fileinputstream;
        byte abyte1[];
        l1 = c(l1);
        l2 = c(l2);
        a(l1);
        file = new File(g, (new StringBuilder()).append(h).append("tmp").toString());
        fileoutputstream = new FileOutputStream(file);
        fileinputstream = new FileInputStream(file);
        abyte1 = new byte[8192];
        l1 = l2 - l1;
_L9:
        if (l1 <= (long)abyte1.length) goto _L2; else goto _L1
_L1:
        int i1 = a(abyte1, 0, abyte1.length);
_L6:
        l2 = l1;
        if (i1 <= 0) goto _L4; else goto _L3
_L3:
        l2 = l1 - (long)i1;
        fileoutputstream.write(abyte1, 0, i1);
          goto _L4
_L8:
        i.close();
        j.close();
        if (!k.delete())
        {
            fileinputstream.close();
            fileoutputstream.close();
            throw new IOException("Delete File failed! Maybe permission deny!");
        }
          goto _L5
_L2:
        i1 = a(abyte1, 0, (int)l1);
          goto _L6
_L5:
        if (FileUtils.renameFile(file, k) >= 0)
        {
            i1 = 1;
        } else
        {
            i1 = 0;
        }
        if (i1 != 0)
        {
            break MISSING_BLOCK_LABEL_317;
        }
        fileinputstream.close();
        fileoutputstream.close();
        throw new IOException("Rename File failed! Maybe permission deny!");
        fileinputstream.close();
        fileoutputstream.close();
        file.delete();
        i = new FileInputStream(k);
        j = new FileOutputStream(k, true);
        e = 0L;
        f = k.length();
        abyte0;
        JVM INSTR monitorexit ;
        return;
_L4:
        if (i1 <= 0) goto _L8; else goto _L7
_L7:
        l1 = l2;
        if (l2 > 0L) goto _L9; else goto _L8
    }

    public long b(long l1)
        throws IOException
    {
        synchronized (l)
        {
            l1 = c(l1);
            l1 = i.skip(l1);
            e = e + l1;
        }
        return l1;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void b()
        throws IOException
    {
        k();
    }

    public void b(byte abyte0[], int i1, int j1)
        throws IOException
    {
        synchronized (l)
        {
            j.write(abyte0, i1, j1);
            j.flush();
            f = f + (long)j1;
        }
        return;
        abyte0;
        abyte1;
        JVM INSTR monitorexit ;
        throw abyte0;
    }

    public void c()
    {
        byte abyte0[] = l;
        abyte0;
        JVM INSTR monitorenter ;
        boolean flag = (new File((new StringBuilder()).append(g).append(File.separator).append(h).toString())).delete();
        Logger.e("RecordFile", (new StringBuilder()).append("deleteFile ").append(flag).toString());
        e = 0L;
        f = 0L;
_L1:
        return;
        Object obj;
        obj;
        ((Exception) (obj)).printStackTrace();
          goto _L1
        obj;
        abyte0;
        JVM INSTR monitorexit ;
        throw obj;
    }

    public void d()
        throws IOException
    {
        byte abyte0[] = l;
        abyte0;
        JVM INSTR monitorenter ;
        if (i != null)
        {
            i.close();
        }
_L1:
        i = new FileInputStream(k);
        e = 0L;
        return;
        Object obj;
        obj;
        ((Exception) (obj)).printStackTrace();
          goto _L1
        obj;
        abyte0;
        JVM INSTR monitorexit ;
        throw obj;
    }

    public int e()
    {
        return a;
    }

    public int f()
    {
        return b;
    }

    public int g()
    {
        return c;
    }

    public void h()
        throws IOException
    {
        synchronized (l)
        {
            if (i != null)
            {
                i.close();
            }
            if (j != null)
            {
                j.close();
            }
            i = null;
            j = null;
            k = null;
        }
        return;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public long i()
    {
        long l1;
        synchronized (l)
        {
            l1 = f;
        }
        return l1;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public String j()
    {
        return (new StringBuilder()).append(g).append(File.separator).append(h).toString();
    }
}
