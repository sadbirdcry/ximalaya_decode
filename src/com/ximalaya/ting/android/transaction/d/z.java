// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.util.ApiUtil;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            af, ab, ae, aa, 
//            v, ac, ad, ag, 
//            w, ah

public class z
{
    public static class a
    {

        private e a;
        private String b[];
        private int c;
        private String d;
        private Map e;

        private void b(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                return;
            }
            JSONArray jsonarray;
            s = JSON.parseObject(s);
            c = s.getIntValue("ret");
            jsonarray = s.getJSONArray("data");
            int k = 0;
_L2:
            if (k >= Math.min(b.length, jsonarray.size()))
            {
                break; /* Loop/switch isn't completed */
            }
            e.put(b[k], Long.valueOf(jsonarray.getJSONObject(k).getJSONObject("uploadTrack").getLongValue("id")));
            k++;
            if (true) goto _L2; else goto _L1
_L1:
            try
            {
                d = s.getString("msg");
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
            d = s.getMessage();
            return;
        }

        private void c(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                return;
            }
            JSONArray jsonarray;
            s = JSON.parseObject(s);
            c = s.getIntValue("ret");
            jsonarray = s.getJSONArray("data");
            int k = 0;
_L2:
            if (k >= Math.min(b.length, jsonarray.size()))
            {
                break; /* Loop/switch isn't completed */
            }
            e.put(b[k], Long.valueOf(jsonarray.getJSONObject(k).getJSONObject("uploadTrack").getLongValue("id")));
            k++;
            if (true) goto _L2; else goto _L1
_L1:
            try
            {
                d = s.getString("msg");
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
            d = s.getMessage();
            return;
        }

        public long a()
        {
            if (e.size() > 0)
            {
                return ((Long)e.get(b[0])).longValue();
            } else
            {
                return 0L;
            }
        }

        public void a(String s)
        {
            static class _cls1
            {

                static final int a[];

                static 
                {
                    a = new int[e.values().length];
                    try
                    {
                        a[e.a.ordinal()] = 1;
                    }
                    catch (NoSuchFieldError nosuchfielderror) { }
                }
            }

            switch (_cls1.a[a.ordinal()])
            {
            default:
                c(s);
                return;

            case 1: // '\001'
                b(s);
                break;
            }
        }

        public int b()
        {
            return c;
        }

        public String c()
        {
            return d;
        }

        public a(e e1, String as[])
        {
            c = -1;
            e = new HashMap();
            a = e1;
            b = as;
        }
    }

    public static interface b
    {

        public abstract void onCompleteUpload(d d1, long l);

        public abstract void onFaileUpload(d d1, int k, int l);

        public abstract void onProgressChange(d d1, int k, float f1);

        public abstract void onStartUpload(d d1);

        public abstract void onTaskCountChange(int k);
    }

    public static interface c
    {

        public abstract void a(int k, int l);
    }

    public class d
        implements Runnable
    {

        final z a;
        private RecordingModel b;

        static RecordingModel a(d d1)
        {
            return d1.b;
        }

        public RecordingModel a()
        {
            return b;
        }

        public void run()
        {
            boolean flag;
            z.a(a, true);
            flag = z.b(a);
            if (flag)
            {
                z.a(a, false);
                synchronized (z.d())
                {
                    if (z.d(a).size() > 0)
                    {
                        d d1 = (d)z.d(a).poll();
                        z.e(a).post(d1);
                    }
                }
                return;
            }
            b.processState = 25600;
            obj = z.c(a);
            obj;
            JVM INSTR monitorenter ;
            for (Iterator iterator = z.c(a).iterator(); iterator.hasNext(); ((b)iterator.next()).onStartUpload(this)) { }
              goto _L1
            Object obj1;
            obj1;
            obj;
            JVM INSTR monitorexit ;
            try
            {
                throw obj1;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
            finally
            {
                z.a(a, false);
            }
            ((Exception) (obj)).printStackTrace();
            b.isNeedUpload = false;
            b.processState = 26368;
            v.b(z.a(a), b);
_L33:
            for (obj = z.c(a).iterator(); ((Iterator) (obj)).hasNext(); ((b)((Iterator) (obj)).next()).onFaileUpload(this, 20, 0)) { }
            Object obj2;
            b b1;
            int k;
            int l;
            int i1;
            long l1;
            if (false)
            {
                synchronized (z.d())
                {
                    if (z.d(a).size() > 0)
                    {
                        d d7 = (d)z.d(a).poll();
                        z.e(a).post(d7);
                    }
                }
                throw obj1;
            } else
            {
                z.a(a, false);
                synchronized (z.d())
                {
                    if (z.d(a).size() > 0)
                    {
                        d d4 = (d)z.d(a).poll();
                        z.e(a).post(d4);
                    }
                }
                return;
            }
_L1:
            obj;
            JVM INSTR monitorexit ;
            if (b._id <= 0L)
            {
                b.isNeedUpload = false;
                a.c(b, true);
            }
            flag = z.b(a);
            if (flag)
            {
                z.a(a, false);
                synchronized (z.d())
                {
                    if (z.d(a).size() > 0)
                    {
                        d d2 = (d)z.d(a).poll();
                        z.e(a).post(d2);
                    }
                }
                return;
            }
            break MISSING_BLOCK_LABEL_406;
            exception;
            obj;
            JVM INSTR monitorexit ;
            throw exception;
            if (b.covers == null || b.covers.length <= 0) goto _L3; else goto _L2
_L2:
            l = b.covers.length;
            if (b.coversId != null) goto _L5; else goto _L4
_L4:
            obj = null;
_L14:
            if (obj == null) goto _L7; else goto _L6
_L6:
            k = obj.length;
            if (k != 0) goto _L8; else goto _L7
_L7:
            k = 0;
_L15:
            if (k >= l) goto _L3; else goto _L9
_L9:
            i1 = k + 1;
            b.processState = i1 + 25856;
            obj = z.a(e.c, new ag(this, i1), new String[] {
                b.covers[k]
            });
_L16:
            if (obj != null) goto _L11; else goto _L10
_L10:
            l1 = 0L;
_L17:
            if (k != 0) goto _L13; else goto _L12
_L12:
            b.coversId = (new StringBuilder()).append(l1).append("").toString();
_L18:
            b.isNeedUpload = false;
            v.b(z.a(a), b);
_L19:
            Logger.e("UploadManager", (new StringBuilder()).append("Cover ").append(k).append(", id ").append(l1).toString());
            if (!z.b(a))
            {
                break MISSING_BLOCK_LABEL_2100;
            }
            b.isNeedUpload = false;
            b.processState = 0;
            a.c(b, true);
            z.a(a, false);
            synchronized (z.d())
            {
                if (z.d(a).size() > 0)
                {
                    d d3 = (d)z.d(a).poll();
                    z.e(a).post(d3);
                }
            }
            return;
            exception1;
            obj;
            JVM INSTR monitorexit ;
            throw exception1;
_L5:
            obj = b.coversId.split(",");
              goto _L14
_L8:
            k = obj.length;
              goto _L15
            obj;
            ((FileNotFoundException) (obj)).printStackTrace();
            obj = null;
              goto _L16
_L11:
            l1 = ((a) (obj)).a();
              goto _L17
_L13:
            obj = new StringBuilder();
            RecordingModel recordingmodel = b;
            recordingmodel.coversId = ((StringBuilder) (obj)).append(recordingmodel.coversId).append(",").append(l1).toString();
              goto _L18
            obj;
            ((Throwable) (obj)).printStackTrace();
              goto _L19
_L3:
            if (b.audioId > 0L)
            {
                break MISSING_BLOCK_LABEL_1447;
            }
_L20:
            if (b.saveTask == null)
            {
                break MISSING_BLOCK_LABEL_979;
            }
            obj = b.saveTask.getStatus();
            obj2 = android.os.AsyncTask.Status.FINISHED;
            if (obj == obj2)
            {
                break MISSING_BLOCK_LABEL_979;
            }
            if (!z.b(a))
            {
                break MISSING_BLOCK_LABEL_962;
            }
            b.isNeedUpload = false;
            b.processState = 0;
            a.c(b, true);
            z.a(a, false);
            synchronized (z.d())
            {
                if (z.d(a).size() > 0)
                {
                    obj2 = (d)z.d(a).poll();
                    z.e(a).post(((Runnable) (obj2)));
                }
            }
            return;
            exception2;
            obj;
            JVM INSTR monitorexit ;
            throw exception2;
            Thread.sleep(500L);
              goto _L20
            obj;
            ((Exception) (obj)).printStackTrace();
              goto _L20
            if (b.saveTask != null && b.saveTask.a() != 0 && !TextUtils.isEmpty(b.mOutName) && b.mRecordFile != null && b.mOperations != null)
            {
                obj = new w(b.mOutName, b.mHeadSetOn);
                ((w) (obj)).myexec(new Object[] {
                    b, b.mRecordFile, b.mOperations
                });
                b.saveTask = ((w) (obj));
            }
_L21:
            if (b.saveTask == null)
            {
                break MISSING_BLOCK_LABEL_1252;
            }
            obj = b.saveTask.getStatus();
            exception2 = android.os.AsyncTask.Status.FINISHED;
            if (obj == exception2)
            {
                break MISSING_BLOCK_LABEL_1252;
            }
            if (!z.b(a))
            {
                break MISSING_BLOCK_LABEL_1235;
            }
            b.isNeedUpload = false;
            b.processState = 0;
            a.c(b, true);
            z.a(a, false);
            synchronized (z.d())
            {
                if (z.d(a).size() > 0)
                {
                    exception2 = (d)z.d(a).poll();
                    z.e(a).post(exception2);
                }
            }
            return;
            exception3;
            obj;
            JVM INSTR monitorexit ;
            throw exception3;
            Thread.sleep(500L);
              goto _L21
            obj;
            ((Exception) (obj)).printStackTrace();
              goto _L21
            b.processState = 25856;
            obj = z.a(e.a, new ah(this), new String[] {
                b.getAudioPath()
            });
_L23:
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_1302;
            }
            if (((a) (obj)).b() == 0)
            {
                break MISSING_BLOCK_LABEL_1422;
            }
            Logger.e("UploadManager", "Upload failed due to upload audio faile");
            b.isNeedUpload = false;
            b.processState = 26368;
            v.b(z.a(a), b);
_L24:
            exception3 = z.c(a).iterator();
_L22:
            if (!exception3.hasNext())
            {
                break MISSING_BLOCK_LABEL_1965;
            }
            b1 = (b)exception3.next();
            if (obj != null)
            {
                break MISSING_BLOCK_LABEL_1413;
            }
            k = 0;
_L25:
            b1.onFaileUpload(this, 18, k);
              goto _L22
            obj;
            ((FileNotFoundException) (obj)).printStackTrace();
            obj = null;
              goto _L23
            exception3;
            exception3.printStackTrace();
              goto _L24
            k = ((a) (obj)).b();
              goto _L25
            b.audioId = ((a) (obj)).a();
            v.b(z.a(a), b);
_L26:
            if (!z.b(a))
            {
                break MISSING_BLOCK_LABEL_1559;
            }
            b.isNeedUpload = false;
            b.processState = 0;
            a.c(b, true);
            z.a(a, false);
            synchronized (z.d())
            {
                if (z.d(a).size() > 0)
                {
                    exception3 = (d)z.d(a).poll();
                    z.e(a).post(exception3);
                }
            }
            return;
            exception4;
            obj;
            JVM INSTR monitorexit ;
            throw exception4;
            obj;
            ((Throwable) (obj)).printStackTrace();
              goto _L26
            if (b.trackId <= 0L)
            {
                z.b(b);
            }
            l1 = b.trackId;
            if (l1 <= 0L)
            {
                break MISSING_BLOCK_LABEL_1790;
            }
            l = 0;
            k = 0;
_L28:
            if (k > 0 || l >= 3)
            {
                break MISSING_BLOCK_LABEL_1694;
            }
            l++;
            i1 = v.a(z.a(a), b._id);
            k = i1;
_L29:
            if (k > 0 || l < 3) goto _L28; else goto _L27
_L27:
            Logger.e("UploadManager", (new StringBuilder()).append("Upload success but delete drafts failed! ").append(b.title).toString());
              goto _L28
            obj;
            ((Throwable) (obj)).printStackTrace();
              goto _L29
            a.a(b, true);
            b.processState = 26624;
            exception4 = z.c(a).iterator();
_L30:
            if (!exception4.hasNext())
            {
                break MISSING_BLOCK_LABEL_2031;
            }
            b1 = (b)exception4.next();
            b1.onCompleteUpload(this, 0L);
            synchronized (z.d())
            {
                b1.onTaskCountChange(z.d(a).size());
            }
              goto _L30
            exception5;
            obj;
            JVM INSTR monitorexit ;
            throw exception5;
            Logger.e("UploadManager", "Upload failed due to merge form");
            b.isNeedUpload = false;
            b.processState = 26368;
            v.b(z.a(a), b);
_L32:
            for (obj = z.c(a).iterator(); ((Iterator) (obj)).hasNext(); ((b)((Iterator) (obj)).next()).onFaileUpload(this, 19, 0)) { }
            break MISSING_BLOCK_LABEL_2031;
            obj;
            ((Throwable) (obj)).printStackTrace();
            if (true) goto _L32; else goto _L31
_L31:
            obj;
            ((Throwable) (obj)).printStackTrace();
              goto _L33
            exception6;
            obj;
            JVM INSTR monitorexit ;
            throw exception6;
            exception7;
            obj;
            JVM INSTR monitorexit ;
            throw exception7;
            exception8;
            obj;
            JVM INSTR monitorexit ;
            throw exception8;
            z.a(a, false);
            synchronized (z.d())
            {
                if (z.d(a).size() > 0)
                {
                    d d5 = (d)z.d(a).poll();
                    z.e(a).post(d5);
                }
            }
            return;
            exception9;
            abyte0;
            JVM INSTR monitorexit ;
            throw exception9;
            z.a(a, false);
            synchronized (z.d())
            {
                if (z.d(a).size() > 0)
                {
                    d d6 = (d)z.d(a).poll();
                    z.e(a).post(d6);
                }
            }
            return;
            exception10;
            abyte1;
            JVM INSTR monitorexit ;
            throw exception10;
            k++;
              goto _L15
        }

        public d(RecordingModel recordingmodel)
        {
            a = z.this;
            super();
            b = recordingmodel;
        }
    }

    public static final class e extends Enum
    {

        public static final e A;
        public static final e B;
        public static final e C;
        public static final e D;
        public static final e E;
        public static final e F;
        public static final e G;
        private static final e K[];
        public static final e a;
        public static final e b;
        public static final e c;
        public static final e d;
        public static final e e;
        public static final e f;
        public static final e g;
        public static final e h;
        public static final e i;
        public static final e j;
        public static final e k;
        public static final e l;
        public static final e m;
        public static final e n;
        public static final e o;
        public static final e p;
        public static final e q;
        public static final e r;
        public static final e s;
        public static final e t;
        public static final e u;
        public static final e v;
        public static final e w;
        public static final e x;
        public static final e y;
        public static final e z;
        public int H;
        public String I;
        public String J;

        public static e valueOf(String s1)
        {
            return (e)Enum.valueOf(com/ximalaya/ting/android/transaction/d/z$e, s1);
        }

        public static e[] values()
        {
            return (e[])K.clone();
        }

        static 
        {
            a = new e("TYPE_ADUIO", 0, 1, "audio", "\u97F3\u9891");
            b = new e("TYPE_FACE", 1, 2, "face", "\u5934\u50CF");
            c = new e("TYPE_COVER", 2, 3, "cover", "\u58F0\u97F3\u5C01\u9762");
            d = new e("TYPE_ATTACHMENT", 3, 4, "attachment", "\u9644\u4EF6\u56FE\u7247");
            e = new e("TYPE_SUBTITLE", 4, 5, "subtitle", "\u5B57\u5E55");
            f = new e("TYPE_TAG", 5, 6, "tag", "\u6807\u7B7E\u56FE\u7247");
            g = new e("TYPE_ALBUM", 6, 7, "album", "\u4E13\u8F91\u5C01\u9762");
            h = new e("TYPE_HEADER_THUMB", 7, 8, "headerThumb", "\u5934\u50CF\u7F29\u7565\u56FE");
            i = new e("TYPE_CATEGORY", 8, 9, "category", "\u5206\u7C7B\u56FE\u7247");
            j = new e("TYPE_PICTURE", 9, 10, "picture", "\u4EFB\u610F\u4E0D\u88C1\u526A\u7684\u56FE\u7247");
            k = new e("TYPE_APP_LOADING", 10, 11, "appLoading", "\u5B50 app loading \u56FE");
            l = new e("TYPE_APP_BACKGROUND", 11, 12, "appBackground", "app\u7528\u5230\u7684\u80CC\u666F\u56FE");
            m = new e("TYPE_DISCOVER_THUMB", 12, 13, "discoverThumb", "\u53D1\u73B0\u4E5F\u9996\u9875\u56FE\u7247");
            n = new e("TYPE_DISCOVER_RECOMMEND", 13, 14, "discoverRecommend", "\u53D1\u73B0\u9875\u5C0F\u7F16\u63A8\u8350");
            o = new e("TYPE_SUBAPP_BACKGROUND", 14, 15, "subappBackground", "\u5B50APP\u80CC\u666F\u56FE");
            p = new e("TYPE_APP_FOCUS", 15, 16, "appFocus", "app\u53D1\u73B0\u9875\u7126\u70B9\u56FE");
            q = new e("TYPE_AUDIO_PC", 16, 17, "audioPc", "pc\u7AEF\u4E0A\u4F20\u7684\u97F3\u9891");
            r = new e("TYPE_SELECTED_ALBUM", 17, 18, "selectedAlbum", "\u7CBE\u9009\u96C6\u5C01\u9762");
            s = new e("TYPE_APP_LOADING_IPAD", 18, 19, "appLoadingIpad", "\u5B50apploading\u56FEfor ipad");
            t = new e("TYPE_SPECIAL_ALBUM", 19, 20, "specialAlbum", "\u4E13\u9898\u56FE\u7247");
            u = new e("TYPE_SUB_APP_ICON", 20, 21, "subappIcon", "\u5B50app icon");
            v = new e("TYPE_ACTIVITY", 21, 22, "activity", "\u6D3B\u52A8\u5C01\u9762");
            w = new e("TYPE_APP_LOADING_IPHONE6", 22, 23, "appLoadingIphone6", "\u5B50apploading\u56FEfor iphone6");
            x = new e("TYPE_ANDROID_SUB_APP_ICON", 23, 24, "androidSubappIcon", "android\u5B50app Icon");
            y = new e("TYPE_ZONE_LOGO", 24, 25, "zoneLogo", "\u5708\u5B50LOGO\u56FE");
            z = new e("TYPE_POST_PICTURE", 25, 26, "postPicture", "\u5E16\u5B50\u56FE\u7247");
            A = new e("TYPE_LIVE_RADIO", 26, 27, "liveRadio", "\u76F4\u64AD\u7535\u53F0");
            B = new e("TYPE_LIVE_RADIO_PROGRAM", 27, 28, "liveRadioProgram", "\u76F4\u64AD\u8282\u76EE");
            C = new e("TYPE_AD_BANNER", 28, 29, "adBanner", "banner\u5E7F\u544A");
            D = new e("TYPE_AD_COMMENT", 29, 30, "adComment", "\u8BC4\u8BBA\u9876\u90E8\u5E7F\u544A");
            E = new e("TYPE_SPECIAL_ALBUM_CUT", 30, 31, "specialAlbumCut", "\u4E13\u9898\u56FE\u7247\u624B\u5DE5\u88C1\u526A\u7684");
            F = new e("TYPE_AD_LOADING", 31, 32, "adLoading", "\u5F00\u5C4F\u5E7F\u544A");
            G = new e("TYPE_AD_FEED", 32, 33, "adFeed", "feed\u5E7F\u544A\u56FE");
            K = (new e[] {
                a, b, c, d, e, f, g, h, i, j, 
                k, l, m, n, o, p, q, r, s, t, 
                u, v, w, x, y, z, A, B, C, D, 
                E, F, G
            });
        }

        private e(String s1, int i1, int j1, String s2, String s3)
        {
            super(s1, i1);
            H = j1;
            I = s2;
            J = s3;
        }
    }


    private static z a;
    private static byte b[] = new byte[0];
    private static byte c[] = new byte[0];
    private HandlerThread d;
    private Handler e;
    private Context f;
    private volatile boolean g;
    private volatile boolean h;
    private Queue i;
    private List j;

    private z(Context context)
    {
        g = false;
        h = false;
        i = new LinkedList();
        j = new ArrayList();
        f = context.getApplicationContext();
        d = new HandlerThread("upload-thread");
        d.start();
        e = new Handler(d.getLooper());
    }

    static Context a(z z1)
    {
        return z1.f;
    }

    public static transient a a(e e1, c c1, String as[])
        throws FileNotFoundException
    {
        a a1 = new a(e1, as);
        RequestParams requestparams = new RequestParams();
        int l = as.length;
        for (int k = 0; k < l; k++)
        {
            File file = new File(as[k]);
            requestparams.put((new StringBuilder()).append("upload_file").append(k).toString(), file);
        }

        com.ximalaya.ting.android.b.f.a().a(a(e1), requestparams, null, new af(a1, c1));
        return a1;
    }

    public static z a(Context context)
    {
        if (a == null)
        {
            synchronized (b)
            {
                if (a == null)
                {
                    a = new z(context);
                }
            }
        }
        return a;
        context;
        abyte0;
        JVM INSTR monitorexit ;
        throw context;
    }

    public static String a(e e1)
    {
        return (new StringBuilder()).append(ApiUtil.getUploadHost()).append("dtres/").append(e1.I).append("/upload").toString();
    }

    static boolean a(z z1, boolean flag)
    {
        z1.g = flag;
        return flag;
    }

    public static boolean b(RecordingModel recordingmodel)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("audioId", (new StringBuilder()).append("").append(recordingmodel.audioId).toString());
        requestparams.put("formId", (new StringBuilder()).append("").append(recordingmodel.formId).toString());
        if (!TextUtils.isEmpty(recordingmodel.coversId))
        {
            requestparams.put("imageIds", (new StringBuilder()).append("").append(recordingmodel.coversId).toString());
        }
        ab ab1 = new ab();
        com.ximalaya.ting.android.b.f.a().a("mobile/api1/upload/submit", requestparams, null, new ae(ab1, recordingmodel));
        if (ab1.a == 0)
        {
            Logger.e("UploadManager", (new StringBuilder()).append("Merge upload ret trackId: ").append(ab1.c).append(", msg: ").append(ab1.b).toString());
            recordingmodel.trackId = ab1.c;
            return true;
        } else
        {
            return false;
        }
    }

    static boolean b(z z1)
    {
        return z1.h;
    }

    static List c(z z1)
    {
        return z1.j;
    }

    static Queue d(z z1)
    {
        return z1.i;
    }

    static byte[] d()
    {
        return c;
    }

    static Handler e(z z1)
    {
        return z1.e;
    }

    public void a()
    {
        h = true;
        a = null;
    }

    public void a(RecordingModel recordingmodel)
    {
        d d1 = new d(recordingmodel);
        recordingmodel = c;
        recordingmodel;
        JVM INSTR monitorenter ;
        if (!g)
        {
            break MISSING_BLOCK_LABEL_50;
        }
        if (!i.contains(d1))
        {
            i.add(d1);
        }
_L2:
        return;
        e.post(d1);
        if (true) goto _L2; else goto _L1
_L1:
        Exception exception;
        exception;
        recordingmodel;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void a(RecordingModel recordingmodel, boolean flag)
    {
        if (recordingmodel == null)
        {
            return;
        }
        recordingmodel = new aa(this, recordingmodel);
        if (flag)
        {
            recordingmodel.run();
            return;
        } else
        {
            e.post(recordingmodel);
            return;
        }
    }

    public void a(b b1)
    {
        synchronized (j)
        {
            if (!j.contains(b1))
            {
                j.add(b1);
            }
        }
        return;
        b1;
        list;
        JVM INSTR monitorexit ;
        throw b1;
    }

    public List b()
    {
        List list;
        try
        {
            list = v.a(f);
        }
        catch (Throwable throwable)
        {
            throwable.printStackTrace();
            return null;
        }
        return list;
    }

    public void b(RecordingModel recordingmodel, boolean flag)
    {
        if (recordingmodel == null)
        {
            return;
        }
        recordingmodel = new ac(this, recordingmodel);
        if (flag)
        {
            recordingmodel.run();
            return;
        } else
        {
            e.post(recordingmodel);
            return;
        }
    }

    public void b(b b1)
    {
        synchronized (j)
        {
            j.remove(b1);
        }
        return;
        b1;
        list;
        JVM INSTR monitorexit ;
        throw b1;
    }

    public int c()
    {
        byte abyte0[] = c;
        abyte0;
        JVM INSTR monitorenter ;
        int l = i.size();
        int k = l;
        if (g)
        {
            k = l + 1;
        }
        abyte0;
        JVM INSTR monitorexit ;
        return k;
        Exception exception;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void c(RecordingModel recordingmodel, boolean flag)
    {
        if (recordingmodel == null)
        {
            return;
        }
        recordingmodel = new ad(this, recordingmodel);
        if (flag)
        {
            recordingmodel.run();
            return;
        } else
        {
            e.post(recordingmodel);
            return;
        }
    }

}
