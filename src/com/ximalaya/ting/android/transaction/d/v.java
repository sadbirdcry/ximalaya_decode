// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.text.TextUtils;
import com.ximalaya.ting.android.database.DataBaseHelper;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.util.Logger;
import java.util.ArrayList;
import java.util.List;

public class v
{

    public static final int a(Context context, long l)
        throws Throwable
    {
        return DataBaseHelper.dbDelete(context, "t_recorder", "_id = ?", new String[] {
            (new StringBuilder()).append(l).append("").toString()
        });
    }

    public static final long a(Context context, RecordingModel recordingmodel)
    {
        if (recordingmodel != null) goto _L2; else goto _L1
_L1:
        long l;
        Logger.e("RecorderDBUtil", "soundsInfo \u4E0D\u80FD\u4E3A null");
        l = -1L;
_L4:
        return l;
_L2:
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("uid", Long.valueOf(recordingmodel.uid));
        contentvalues.put("audio_path", recordingmodel.getAudioPath());
        if (recordingmodel.covers != null && recordingmodel.covers.length > 0)
        {
            String s = "";
            for (int i = 0; i < recordingmodel.covers.length; i++)
            {
                s = (new StringBuilder()).append(s).append(recordingmodel.covers[i]).append(",").toString();
            }

            contentvalues.put("image_path", s);
        }
        contentvalues.put("title", recordingmodel.title);
        contentvalues.put("source", Integer.valueOf(recordingmodel.userSource));
        String s1;
        long l1;
        if (recordingmodel.intro == null)
        {
            s1 = "";
        } else
        {
            s1 = recordingmodel.intro;
        }
        contentvalues.put("intro", s1);
        if (recordingmodel.isPublic)
        {
            s1 = "1";
        } else
        {
            s1 = "0";
        }
        contentvalues.put("isPublic", s1);
        if (recordingmodel.isNeedUpload)
        {
            s1 = "1";
        } else
        {
            s1 = "0";
        }
        contentvalues.put("needUpload", s1);
        if (recordingmodel.isAudioUploaded)
        {
            s1 = "1";
        } else
        {
            s1 = "0";
        }
        contentvalues.put("file_uploaded", s1);
        if (recordingmodel.isFormUploaded)
        {
            s1 = "1";
        } else
        {
            s1 = "0";
        }
        contentvalues.put("form_uploaded", s1);
        contentvalues.put("duration", Double.valueOf(recordingmodel.duration));
        contentvalues.put("create_at", Long.valueOf(recordingmodel.createdAt));
        contentvalues.put("album_id", Long.valueOf(recordingmodel.albumId));
        contentvalues.put("album_title", recordingmodel.albumTitle);
        contentvalues.put("image_id", recordingmodel.coversId);
        contentvalues.put("form_id", Long.valueOf(recordingmodel.formId));
        contentvalues.put("audioId", Long.valueOf(recordingmodel.audioId));
        contentvalues.put("processState", Integer.valueOf(recordingmodel.processState));
        contentvalues.put("activity_id", Long.valueOf(recordingmodel.activityId));
        l1 = DataBaseHelper.dbInsert(context, "t_recorder", contentvalues);
        l = l1;
        if (-1L != l1) goto _L4; else goto _L3
_L3:
        return -1L;
        context;
        context.printStackTrace();
        if (true) goto _L3; else goto _L5
_L5:
    }

    public static final List a(Context context)
        throws Throwable
    {
        Context context1;
        Context context2;
        ArrayList arraylist;
        context2 = null;
        context1 = null;
        arraylist = new ArrayList();
        context = DataBaseHelper.dbRawQuery(context, "SELECT _id,uid,audio_path,image_path,title,source,intro,isPublic,duration,create_at,image_id,form_id,album_id,album_title,processState,activity_id,audioId FROM t_recorder WHERE needUpload = '0' ORDER BY create_at DESC", null);
        context1 = context;
        context2 = context;
        context.moveToFirst();
_L10:
        context1 = context;
        context2 = context;
        if (context.getPosition() == context.getCount()) goto _L2; else goto _L1
_L1:
        context1 = context;
        context2 = context;
        if (context.getString(context.getColumnIndex("duration")) != null) goto _L4; else goto _L3
_L3:
        double d = 0.0D;
_L8:
        context1 = context;
        context2 = context;
        RecordingModel recordingmodel = new RecordingModel();
        context1 = context;
        context2 = context;
        recordingmodel._id = context.getLong(context.getColumnIndex("_id"));
        context1 = context;
        context2 = context;
        recordingmodel.uid = context.getLong(context.getColumnIndex("uid"));
        context1 = context;
        context2 = context;
        recordingmodel.setAudioPath(context.getString(context.getColumnIndex("audio_path")));
        context1 = context;
        context2 = context;
        String s = context.getString(context.getColumnIndex("image_path"));
        context1 = context;
        context2 = context;
        if (TextUtils.isEmpty(s))
        {
            break; /* Loop/switch isn't completed */
        }
        context1 = context;
        context2 = context;
        String as[] = s.split(",");
        context1 = context;
        context2 = context;
        recordingmodel.covers = new String[as.length];
        int i = 0;
_L6:
        context1 = context;
        context2 = context;
        if (i >= as.length)
        {
            break; /* Loop/switch isn't completed */
        }
        context1 = context;
        context2 = context;
        recordingmodel.covers[i] = as[i];
        i++;
        if (true) goto _L6; else goto _L5
_L5:
        break; /* Loop/switch isn't completed */
_L4:
        context1 = context;
        context2 = context;
        d = context.getDouble(context.getColumnIndex("duration"));
        if (true) goto _L8; else goto _L7
_L7:
        context1 = context;
        context2 = context;
        recordingmodel.title = context.getString(context.getColumnIndex("title"));
        context1 = context;
        context2 = context;
        recordingmodel.userSource = Integer.parseInt(context.getString(context.getColumnIndex("source")));
        context1 = context;
        context2 = context;
        recordingmodel.intro = context.getString(context.getColumnIndex("intro"));
        context1 = context;
        context2 = context;
        boolean flag;
        if (Integer.parseInt(context.getString(context.getColumnIndex("isPublic"))) == 1)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        context1 = context;
        context2 = context;
        recordingmodel.isPublic = flag;
        context1 = context;
        context2 = context;
        recordingmodel.isNeedUpload = false;
        context1 = context;
        context2 = context;
        recordingmodel.duration = d;
        context1 = context;
        context2 = context;
        recordingmodel.processState = context.getInt(context.getColumnIndex("processState"));
        context1 = context;
        context2 = context;
        recordingmodel.createdAt = Long.parseLong(context.getString(context.getColumnIndex("create_at")));
        context1 = context;
        context2 = context;
        recordingmodel.coversId = context.getString(context.getColumnIndex("image_id"));
        context1 = context;
        context2 = context;
        recordingmodel.formId = context.getLong(context.getColumnIndex("form_id"));
        context1 = context;
        context2 = context;
        recordingmodel.audioId = context.getLong(context.getColumnIndex("audioId"));
        context1 = context;
        context2 = context;
        recordingmodel.albumId = context.getLong(context.getColumnIndex("album_id"));
        context1 = context;
        context2 = context;
        recordingmodel.albumTitle = context.getString(context.getColumnIndex("album_title"));
        context1 = context;
        context2 = context;
        recordingmodel.activityId = context.getLong(context.getColumnIndex("activity_id"));
        context1 = context;
        context2 = context;
        arraylist.add(recordingmodel);
        context1 = context;
        context2 = context;
        context.moveToNext();
        if (true) goto _L10; else goto _L9
_L9:
        context;
        context2 = context1;
        Logger.log("RecorderDBUtil", context.toString(), true);
        if (context1 == null) goto _L12; else goto _L11
_L11:
        context = context1;
_L13:
        context.close();
_L12:
        Logger.log("RecorderDBUtil", (new StringBuilder()).append("\u67E5\u8BE2\u6211\u7684\u58F0\u97F3\u5217\u8868(\u9700\u8981\u4E0A\u4F20).......t_recorder...... \u8FD4\u56DE\u7684\u6761\u76EE\u4E3A\uFF1A===").append(arraylist.size()).toString(), true);
        return arraylist;
        context;
        if (context2 != null)
        {
            context2.close();
        }
        throw context;
_L2:
        if (context == null) goto _L12; else goto _L13
    }

    public static final void b(Context context, RecordingModel recordingmodel)
        throws Throwable
    {
        int i = 0;
        String s2 = (new StringBuilder()).append(recordingmodel._id).append("").toString();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("audio_path", recordingmodel.getAudioPath());
        if (recordingmodel.covers != null && recordingmodel.covers.length > 0)
        {
            String s = "";
            for (; i < recordingmodel.covers.length; i++)
            {
                s = (new StringBuilder()).append(s).append(recordingmodel.covers[i]).append(",").toString();
            }

            contentvalues.put("image_path", s);
        }
        contentvalues.put("title", recordingmodel.title);
        contentvalues.put("categoryId", Long.valueOf(recordingmodel.categoryId));
        contentvalues.put("source", Integer.valueOf(recordingmodel.userSource));
        String s1;
        if (recordingmodel.intro == null)
        {
            s1 = "";
        } else
        {
            s1 = recordingmodel.intro;
        }
        contentvalues.put("intro", s1);
        if (recordingmodel.isPublic)
        {
            s1 = "1";
        } else
        {
            s1 = "0";
        }
        contentvalues.put("isPublic", s1);
        if (recordingmodel.isNeedUpload)
        {
            s1 = "1";
        } else
        {
            s1 = "0";
        }
        contentvalues.put("needUpload", s1);
        if (recordingmodel.isAudioUploaded)
        {
            s1 = "1";
        } else
        {
            s1 = "0";
        }
        contentvalues.put("file_uploaded", s1);
        if (recordingmodel.isFormUploaded)
        {
            s1 = "1";
        } else
        {
            s1 = "0";
        }
        contentvalues.put("form_uploaded", s1);
        contentvalues.put("duration", Double.valueOf(recordingmodel.duration));
        contentvalues.put("create_at", Long.valueOf(recordingmodel.createdAt));
        contentvalues.put("image_id", recordingmodel.coversId);
        contentvalues.put("audioId", Long.valueOf(recordingmodel.audioId));
        contentvalues.put("album_id", Long.valueOf(recordingmodel.albumId));
        contentvalues.put("album_title", recordingmodel.albumTitle);
        contentvalues.put("processState", Integer.valueOf(recordingmodel.processState));
        contentvalues.put("activity_id", Long.valueOf(recordingmodel.activityId));
        DataBaseHelper.dbUpdate(context, "t_recorder", contentvalues, "_id = ?", new String[] {
            s2
        });
    }
}
