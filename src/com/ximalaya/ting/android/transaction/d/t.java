// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.media.AudioRecord;
import android.os.SystemClock;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            g

public class t extends Thread
{
    public static interface a
    {

        public abstract void a(g g1);

        public abstract void a(Exception exception);

        public abstract void e();

        public abstract void f();
    }


    private volatile boolean a;
    private volatile boolean b;
    private a c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private AudioRecord i;

    public t(int j, int k, int l, int i1)
    {
        a = false;
        b = true;
        g = j;
        d = k;
        e = l;
        f = i1;
        h = AudioRecord.getMinBufferSize(d, e, f);
        h = Math.max(h, com.ximalaya.ting.android.transaction.d.g.b());
        h = (int)(Math.ceil((float)h / 2048F) * 2048D);
        com.ximalaya.ting.android.transaction.d.g.a(h);
    }

    public void a()
    {
        a = true;
    }

    public void a(a a1)
    {
        c = a1;
    }

    public boolean b()
    {
        return b;
    }

    public void run()
    {
        b = false;
        super.run();
        a = false;
        i = new AudioRecord(g, d, e, f, h);
        if (i.getState() == 0)
        {
            throw new IllegalStateException("AudioRecord UNINITIALIZED!");
        }
          goto _L1
        Exception exception;
        exception;
        if (c != null)
        {
            c.a(exception);
            c.e();
        }
        a = true;
        if (i != null)
        {
            i.release();
            i = null;
        }
_L7:
        return;
_L1:
        i.startRecording();
        if (c != null)
        {
            c.f();
        }
_L3:
        Object obj;
        int j;
        if (a)
        {
            break MISSING_BLOCK_LABEL_282;
        }
        obj = com.ximalaya.ting.android.transaction.d.g.a();
        j = i.read(((g) (obj)).a, 0, ((g) (obj)).b);
        obj.d = 0;
        obj.c = j;
        if (a || j <= 0)
        {
            break MISSING_BLOCK_LABEL_275;
        }
        if (c == null) goto _L3; else goto _L2
_L2:
        long l = SystemClock.uptimeMillis();
        c.a(((g) (obj)));
        Logger.e("RecordThread", (new StringBuilder()).append("Handle Rec Data Cost ").append(SystemClock.uptimeMillis() - l).append("ms").toString());
          goto _L3
        obj;
        Object obj1;
        a = true;
        try
        {
            if (i != null)
            {
                i.release();
                i = null;
            }
        }
        // Misplaced declaration of an exception variable
        catch (Object obj1)
        {
            if (c != null)
            {
                c.a(((Exception) (obj1)));
            }
        }
        throw obj;
        com.ximalaya.ting.android.transaction.d.g.a(((g) (obj)));
          goto _L3
        i.stop();
        if (c != null)
        {
            c.e();
        }
        a = true;
        if (i != null)
        {
            i.release();
            i = null;
            return;
        }
        continue; /* Loop/switch isn't completed */
        obj;
        if (c == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        obj1 = c;
_L5:
        ((a) (obj1)).a(((Exception) (obj)));
        return;
        obj;
        if (c == null)
        {
            continue; /* Loop/switch isn't completed */
        }
        obj1 = c;
        if (true) goto _L5; else goto _L4
_L4:
        if (true) goto _L7; else goto _L6
_L6:
    }
}
