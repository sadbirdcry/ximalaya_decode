// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.media.AudioTrack;
import com.ximalaya.ting.android.library.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            g

public class q extends Thread
{
    public static interface a
    {

        public abstract void a(g g1);

        public abstract void a(Exception exception);

        public abstract void c();

        public abstract void d();

        public abstract void e();

        public abstract void g();
    }


    private AudioTrack a;
    private a b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private volatile boolean i;
    private volatile boolean j;
    private volatile boolean k;

    public q(int l, int i1, int j1, int k1, int l1)
    {
        i = true;
        j = false;
        k = false;
        c = l;
        d = i1;
        e = j1;
        f = k1;
        h = l1;
    }

    public void a(a a1)
    {
        b = a1;
    }

    public boolean a()
    {
        return i;
    }

    public void b()
    {
        j = true;
        this;
        JVM INSTR monitorenter ;
        notify();
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        this;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public boolean c()
    {
        return !i && !j;
    }

    public boolean d()
    {
        return j && isAlive();
    }

    public void run()
    {
        i = false;
        super.run();
        j = false;
        k = false;
        g = AudioTrack.getMinBufferSize(d, e, f);
        if (g > 0) goto _L2; else goto _L1
_L1:
        String s = (new StringBuilder()).append("Init AudioTracker failed! errorCode ").append(g).toString();
        Logger.e("PlayerThread", s);
        if (b != null)
        {
            b.a(new Exception(s));
        }
        if (a == null) goto _L4; else goto _L3
_L3:
        a.release();
_L12:
        a = null;
_L4:
        return;
_L2:
        g = Math.max(g, com.ximalaya.ting.android.transaction.d.g.b());
        com.ximalaya.ting.android.transaction.d.g.a(g);
        a = new AudioTrack(c, d, e, f, g, h);
        a.play();
        if (b != null)
        {
            b.c();
        }
_L8:
        if (j)
        {
            break MISSING_BLOCK_LABEL_406;
        }
        if (!k) goto _L6; else goto _L5
_L5:
        this;
        JVM INSTR monitorenter ;
        if (b != null)
        {
            b.e();
        }
        wait();
_L9:
        if (!j && b != null)
        {
            b.g();
        }
        this;
        JVM INSTR monitorexit ;
_L6:
        if (j || b == null) goto _L8; else goto _L7
_L7:
        g g1 = com.ximalaya.ting.android.transaction.d.g.a();
        b.a(g1);
        a.write(g1.a, g1.d, g1.c - g1.d);
        com.ximalaya.ting.android.transaction.d.g.a(g1);
          goto _L8
        Object obj;
        obj;
        ((Exception) (obj)).printStackTrace();
        j = true;
        if (b != null)
        {
            b.a(((Exception) (obj)));
            b.d();
        }
        if (a != null)
        {
            a.release();
            continue; /* Loop/switch isn't completed */
        }
          goto _L4
        obj;
        ((InterruptedException) (obj)).printStackTrace();
          goto _L9
        obj;
        this;
        JVM INSTR monitorexit ;
        throw obj;
        obj;
        if (a != null)
        {
            a.release();
            a = null;
        }
        throw obj;
        a.stop();
        if (b != null)
        {
            b.d();
        }
        if (a == null) goto _L4; else goto _L10
_L10:
        a.release();
        if (true) goto _L12; else goto _L11
_L11:
    }
}
