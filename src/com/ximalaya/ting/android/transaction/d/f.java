// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import com.ximalaya.ting.android.model.record.BgSound;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            h

public class f
{

    private h a;
    private BgSound b;
    private long c;
    private long d;
    private Context e;
    private AssetManager f;
    private FileDescriptor g;
    private AssetFileDescriptor h;
    private File i;
    private FileInputStream j;

    public f(Context context)
    {
        e = context.getApplicationContext();
        f = e.getAssets();
        a = new h();
        a.a(true);
    }

    private void a(BgSound bgsound, long l, long l1)
    {
        k();
        b = bgsound;
        c = l;
        d = l1;
        if (b == null)
        {
            return;
        }
        b.type;
        JVM INSTR tableswitch 1 3: default 60
    //                   1 61
    //                   2 192
    //                   3 207;
           goto _L1 _L2 _L3 _L4
_L1:
        return;
_L2:
        try
        {
            h = f.openFd(b.path);
            g = h.getFileDescriptor();
            if (d != 0L)
            {
                if (d > h.getLength())
                {
                    d = h.getLength();
                }
                a.a(g, h.getStartOffset() + c, d);
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (BgSound bgsound)
        {
            bgsound.printStackTrace();
            a.c();
            return;
        }
        a.a(g, h.getStartOffset(), h.getLength());
        return;
_L3:
        a.a(b.songLink);
        return;
_L4:
        try
        {
            i = new File(b.path);
            j = new FileInputStream(i);
            g = j.getFD();
            if (d != 0L)
            {
                if (d > i.length())
                {
                    d = i.length();
                }
                a.a(g, c, d);
                return;
            }
            break; /* Loop/switch isn't completed */
        }
        // Misplaced declaration of an exception variable
        catch (BgSound bgsound)
        {
            bgsound.printStackTrace();
        }
        a.c();
        return;
        a.a(g, 0L, i.length());
        return;
    }

    private void b(BgSound bgsound, int l)
    {
        k();
        b = bgsound;
        if (b == null)
        {
            return;
        }
        switch (b.type)
        {
        default:
            return;

        case 1: // '\001'
            try
            {
                h = f.openFd(b.path);
                g = h.getFileDescriptor();
                a.a(g, l);
                return;
            }
            // Misplaced declaration of an exception variable
            catch (BgSound bgsound)
            {
                bgsound.printStackTrace();
            }
            a.c();
            return;

        case 2: // '\002'
            a.a(b.songLink);
            return;

        case 3: // '\003'
            break;
        }
        try
        {
            i = new File(b.path);
            j = new FileInputStream(i);
            g = j.getFD();
            a.a(g, l);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (BgSound bgsound)
        {
            bgsound.printStackTrace();
        }
        a.c();
    }

    private void k()
    {
        a.i();
        g = null;
        if (h != null)
        {
            try
            {
                h.close();
            }
            catch (IOException ioexception)
            {
                ioexception.printStackTrace();
            }
        }
        h = null;
        if (j != null)
        {
            try
            {
                j.close();
            }
            catch (IOException ioexception1)
            {
                ioexception1.printStackTrace();
            }
        }
        j = null;
        i = null;
        b = null;
    }

    public void a(float f1, float f2)
    {
        a.a(f1, f2);
    }

    public void a(BgSound bgsound)
    {
        a(bgsound, 0L, 0L);
    }

    public void a(BgSound bgsound, int l)
    {
        b(bgsound, l);
    }

    public void a(h.a a1)
    {
        a.a(a1);
    }

    public boolean a()
    {
        return a.e();
    }

    public boolean b()
    {
        return a.f();
    }

    public int c()
    {
        return a.b();
    }

    public int d()
    {
        return a.a();
    }

    public BgSound e()
    {
        return b;
    }

    public int f()
    {
        return a.d();
    }

    public void g()
    {
        a.g();
    }

    public void h()
    {
        a.h();
    }

    public void i()
    {
        a.i();
    }

    public void j()
    {
        k();
        a.a(null);
        a.a(null);
        a.j();
        a = null;
        f = null;
        e = null;
    }
}
