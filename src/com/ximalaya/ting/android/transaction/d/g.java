// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import com.ximalaya.ting.android.library.util.Logger;
import java.util.LinkedList;
import java.util.List;

public class g
{

    private static int e = 4096;
    private static List f = new LinkedList();
    public byte a[];
    public int b;
    public int c;
    public int d;

    public g()
    {
        this(e);
    }

    public g(int i)
    {
        int j = i;
        if (i <= 0)
        {
            j = e;
        }
        a = new byte[j];
        b = j;
        c = 0;
        d = 0;
    }

    public static g a()
    {
        g g1 = null;
        synchronized (f)
        {
            if (f.size() > 0)
            {
                g1 = (g)f.remove(0);
                g1.c();
            }
        }
        obj = g1;
        if (g1 == null)
        {
            obj = new g();
        }
        return ((g) (obj));
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public static void a(int i)
    {
        if (i != e)
        {
            Logger.e("Buffer", (new StringBuilder()).append("Buff def size change new : ").append(i).append(", old : ").append(e).toString());
            e = i;
        }
    }

    public static void a(g g1)
    {
        List list = f;
        list;
        JVM INSTR monitorenter ;
        if (f.size() >= 5)
        {
            break MISSING_BLOCK_LABEL_31;
        }
        f.add(g1);
_L2:
        return;
        Logger.e("Buffer", "GC due to max cache size");
        if (true) goto _L2; else goto _L1
_L1:
        g1;
        list;
        JVM INSTR monitorexit ;
        throw g1;
    }

    public static int b()
    {
        return e;
    }

    public void c()
    {
        if (a == null || a.length != e)
        {
            a = new byte[e];
            Logger.e("Buffer", "GC due to def size change");
        }
        b = e;
        c = 0;
        d = 0;
    }

}
