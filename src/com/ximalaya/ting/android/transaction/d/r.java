// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.util.FileUtils;
import java.io.File;

public class r
{

    private static r a;
    private static byte b[] = new byte[0];
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;

    private r()
    {
        d = (new StringBuilder()).append("ting").append(File.separator).append("record").append(File.separator).toString();
        e = (new StringBuilder()).append("bg").append(File.separator).toString();
        f = (new StringBuilder()).append("rec_pcm").append(File.separator).toString();
        g = (new StringBuilder()).append("cover").append(File.separator).toString();
        h = (new StringBuilder()).append("rec_mp3").append(File.separator).toString();
        c();
    }

    public static r a()
    {
        if (a == null)
        {
            synchronized (b)
            {
                if (a == null)
                {
                    a = new r();
                }
            }
        }
        return a;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void i()
    {
        if (!TextUtils.isEmpty(c))
        {
            return;
        }
        if (android.os.Build.VERSION.SDK_INT >= 19 && MyApplication.b() != null && MyApplication.b().getExternalCacheDir() != null)
        {
            c = MyApplication.b().getExternalCacheDir().getAbsolutePath();
        } else
        {
            c = Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        c = (new StringBuilder()).append(c).append(File.separator).toString();
    }

    public boolean b()
    {
        return "mounted".equals(Environment.getExternalStorageState()) && !"mounted_ro".equals(Environment.getExternalStorageState());
    }

    public boolean c()
    {
        boolean flag;
        if (!b())
        {
            flag = false;
        } else
        {
            i();
            boolean flag1 = true;
            File file = new File((new StringBuilder()).append(c).append(File.separator).append(d).toString(), e);
            if (file.exists() && file.isFile())
            {
                flag1 = true & file.delete();
            } else
            if (!file.exists())
            {
                flag1 = true & file.mkdirs();
            }
            file = new File((new StringBuilder()).append(c).append(File.separator).append(d).toString(), f);
            if (file.exists() && file.isFile())
            {
                flag = flag1 & file.delete();
            } else
            {
                flag = flag1;
                if (!file.exists())
                {
                    flag = flag1 & file.mkdirs();
                }
            }
            file = new File((new StringBuilder()).append(c).append(File.separator).append(d).toString(), h);
            if (file.exists() && file.isFile())
            {
                flag1 = flag & file.delete();
            } else
            {
                flag1 = flag;
                if (!file.exists())
                {
                    flag1 = flag & file.mkdirs();
                }
            }
            file = new File((new StringBuilder()).append(c).append(File.separator).append(d).toString(), g);
            if (file.exists() && file.isFile())
            {
                return flag1 & file.delete();
            }
            flag = flag1;
            if (!file.exists())
            {
                return flag1 & file.mkdirs();
            }
        }
        return flag;
    }

    public String d()
    {
        return (new StringBuilder()).append(c).append(d).append(e).toString();
    }

    public String e()
    {
        return (new StringBuilder()).append(c).append(d).append(f).toString();
    }

    public String f()
    {
        return (new StringBuilder()).append(c).append(d).append(g).toString();
    }

    public String g()
    {
        return (new StringBuilder()).append(c).append(d).append(h).toString();
    }

    public long h()
    {
        return FileUtils.getFileSize(new File((new StringBuilder()).append(c).append(File.separator).append(d).toString()));
    }

}
