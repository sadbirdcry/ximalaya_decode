// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.record.BgSound;
import java.io.IOException;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            o, f, l, m, 
//            n, s

public class k
{
    private class a
        implements Runnable
    {

        final k a;
        private n b;
        private int c;
        private float d;

        public void a(float f1)
        {
            d = f1;
        }

        public void run()
        {
            if (k.e(a) != null && k.f(a) != null) goto _L2; else goto _L1
_L1:
            Logger.e("MixPlayback", "play task return");
_L8:
            return;
_L2:
            Object obj1 = (new StringBuilder()).append("mCurrIndex ").append(c).append("/").append(k.g(a).size() - 1).append(", mTask.op ").append(b.d).append(", title ");
            Object obj;
            int i1;
            long l1;
            if (b.e == null)
            {
                obj = "";
            } else
            {
                obj = b.e.title;
            }
            Logger.e("MixPlayback", ((StringBuilder) (obj1)).append(((String) (obj))).toString());
            if (c < k.g(a).size() - 1 && !k.e(a).f())
            {
                try
                {
                    Logger.e("MixPlayback", "mix player restart pcm player");
                    k.e(a).a(k.b(a));
                    Thread.sleep(100L);
                    k.e(a).a(k.h(a), k.i(a));
                }
                catch (Exception exception)
                {
                    exception.printStackTrace();
                }
            }
            b.d;
            JVM INSTR tableswitch 1 3: default 236
        //                       1 388
        //                       2 574
        //                       3 601;
               goto _L3 _L4 _L5 _L6
_L6:
            break MISSING_BLOCK_LABEL_601;
_L3:
            break; /* Loop/switch isn't completed */
_L4:
            break; /* Loop/switch isn't completed */
_L9:
            i1 = c + 1;
            if (i1 < k.g(a).size())
            {
                obj = (n)k.g(a).get(i1);
                obj1 = a. new a(((n) (obj)), i1);
                l1 = (long)((float)((((n) (obj)).a - b.a) * 1000L) / k.b(a).a() - d);
                Logger.e("MixPlayback", (new StringBuilder()).append("Play task post delay ").append(l1).toString());
                k.j(a).postDelayed(((Runnable) (obj1)), l1);
                return;
            }
            if (true) goto _L8; else goto _L7
_L7:
            if (b.e != null)
            {
                if (d > 0.0F)
                {
                    long l2 = (long)((float)b.g + d);
                    Logger.e("MixPlayback", (new StringBuilder()).append("play run seek ").append(l2).append("/").append(b.e.duration).toString());
                    k.f(a).a(b.e, (int)((float)b.g + d));
                } else
                {
                    k.f(a).a(b.e, b.g);
                }
                k.f(a).g();
                k.f(a).a(b.b, b.b);
            } else
            {
                k.f(a).i();
            }
              goto _L9
_L5:
            k.f(a).a(b.b, b.b);
              goto _L9
            k.f(a).i();
              goto _L9
        }

        a(n n1, int i1)
        {
            a = k.this;
            super();
            b = n1;
            c = i1;
        }
    }


    private Context a;
    private List b;
    private s c;
    private Handler d;
    private HandlerThread e;
    private o f;
    private f g;
    private o.a h;
    private long i;
    private long j;
    private long k;

    public k(Context context, s s1, List list)
    {
        a = context;
        c = s1;
        b = list;
        e = new HandlerThread("thd-playback");
        e.start();
        d = new Handler(e.getLooper());
        f = new o();
        g = new f(a);
        g.a(new l(this));
        try
        {
            f.a(c);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
        }
        f.a(new m(this));
    }

    static long a(k k1)
    {
        return k1.k;
    }

    static s b(k k1)
    {
        return k1.c;
    }

    static o.a c(k k1)
    {
        return k1.h;
    }

    static void d(k k1)
    {
        k1.e();
    }

    static o e(k k1)
    {
        return k1.f;
    }

    private void e()
    {
        g.i();
    }

    static f f(k k1)
    {
        return k1.g;
    }

    static List g(k k1)
    {
        return k1.b;
    }

    static long h(k k1)
    {
        return k1.i;
    }

    static long i(k k1)
    {
        return k1.j;
    }

    static Handler j(k k1)
    {
        return k1.d;
    }

    public void a()
    {
        d.removeCallbacksAndMessages(null);
        e.quit();
        f.b();
        f.a(null);
        g.j();
        g = null;
        f = null;
    }

    public void a(long l1)
    {
        k = l1;
    }

    public void a(long l1, long l2)
    {
        int j1;
        if (l1 != 0L)
        {
            Logger.e("MixPlayback", (new StringBuilder()).append("playback ").append(l1).append("/").append(l2).toString());
        }
        i = l1;
        j = l2;
        j1 = b.size();
        if (j1 == 0)
        {
            return;
        }
        if (l1 <= 0L) goto _L2; else goto _L1
_L1:
        int i1 = 0;
_L5:
        n n1;
        n n3;
        if (i1 >= j1 - 1)
        {
            break MISSING_BLOCK_LABEL_248;
        }
        n1 = (n)b.get(i1);
        n3 = (n)b.get(i1 + 1);
        if (l1 < n1.a || l1 >= n3.a) goto _L4; else goto _L3
_L3:
        n n2 = (n)b.get(i1);
        a a2 = new a(n2, i1);
        a2.a((float)((l1 - n2.a) * 1000L) / c.a());
        d.post(a2);
        return;
_L4:
        i1++;
          goto _L5
_L2:
        a a1 = new a((n)b.get(0), 0);
        d.post(a1);
        return;
        i1 = 0;
          goto _L3
    }

    public void a(o.a a1)
    {
        h = a1;
    }

    public boolean b()
    {
        return f.f();
    }

    public void c()
    {
        a(0L, c.i());
    }

    public void d()
    {
        Logger.e("MixPlayback", "stop mix player");
        d.removeCallbacksAndMessages(null);
        if (f != null)
        {
            f.b();
        }
        if (g != null)
        {
            g.i();
        }
    }
}
