// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import java.nio.ByteOrder;

public class u
{

    private static boolean a;

    private static float a(float f)
    {
        f = Math.abs(f);
        if (f > 0.0F)
        {
            return (float)(20D * Math.log10(f));
        } else
        {
            return -9999.9F;
        }
    }

    private static float a(float f, float f1, float f2)
    {
        return Math.max(f1, Math.min(f, f2));
    }

    private static float a(float f, float f1, float f2, float f3, float f4)
    {
        return a(((f4 - f3) * (f - f1)) / (f2 - f1) + f3, f3, f4);
    }

    public static int a(byte abyte0[], int i, int j)
    {
        int k = 0;
        int l = 0;
        for (; k < i; k += 2)
        {
            l = Math.max(l, a(abyte0[k + 0], abyte0[k + 1]));
        }

        return (int)a(a((float)l / 32767F), -40F, 0.0F, 1.0F, j);
    }

    private static short a(byte byte0, byte byte1)
    {
        if (a)
        {
            return (short)(byte0 << 8 | byte1 & 0xff);
        } else
        {
            return (short)(byte1 << 8 | byte0 & 0xff);
        }
    }

    static 
    {
        boolean flag;
        if (ByteOrder.BIG_ENDIAN == ByteOrder.nativeOrder())
        {
            flag = true;
        } else
        {
            flag = false;
        }
        a = flag;
    }
}
