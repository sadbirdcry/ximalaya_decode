// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;

import com.ximalaya.ting.android.library.util.Logger;
import java.io.IOException;

// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            s, q, g

public class o
    implements q.a
{
    public static interface a
    {

        public abstract void onPlayerPaused();

        public abstract void onPlayerStart();

        public abstract void onPlayerStopped();

        public abstract void onProgressUpdate(float f1);
    }


    private s a;
    private q b;
    private q c;
    private a d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private long j;
    private long k;
    private long l;
    private long m;
    private int n;

    public o()
    {
        e = 3;
        i = 1;
        j = 0L;
        k = 0L;
        l = 0L;
    }

    private long b(long l1)
    {
        if (l1 == 0L)
        {
            return 0L;
        } else
        {
            return (long)(int)(l1 / l) * l;
        }
    }

    private void h()
    {
        g = a.e();
        int i1;
        if (a.f() == 2)
        {
            i1 = 12;
        } else
        {
            i1 = 4;
        }
        f = i1;
        if (a.g() == 16)
        {
            i1 = 2;
        } else
        {
            i1 = 3;
        }
        h = i1;
    }

    private void i()
    {
        h();
        c = b;
        b = new q(e, g, f, h, i);
        b.a(this);
    }

    public void a()
    {
        a(0L, a.i());
    }

    public void a(long l1)
    {
        m = l1;
    }

    public void a(long l1, long l2)
    {
        if (l1 >= 0L)
        {
            j = b(l1);
        }
        if (l2 >= 0L)
        {
            k = b(l2);
        }
        n = (int)j;
        if (b != null && b.c())
        {
            b.b();
        }
        if (b == null || !b.a())
        {
            i();
        }
        int i1;
        if (j != 0L)
        {
            try
            {
                a.d();
                a.b(j);
            }
            catch (IOException ioexception)
            {
                ioexception.printStackTrace();
            }
        }
        i1 = 0;
        while (c != null && c.d() && i1 < 50) 
        {
            try
            {
                Thread.sleep(10L);
            }
            catch (InterruptedException interruptedexception)
            {
                interruptedexception.printStackTrace();
            }
            i1++;
        }
        b.start();
    }

    public void a(g g1)
    {
        float f1;
        int i1;
        try
        {
            i1 = a.a(g1.a, 0, g1.b);
        }
        // Misplaced declaration of an exception variable
        catch (g g1)
        {
            g1.printStackTrace();
            return;
        }
        if (i1 <= 0)
        {
            break MISSING_BLOCK_LABEL_162;
        }
        g1.c = i1;
        n = i1 + n;
        if (m != 0L && (long)n >= m)
        {
            b();
        }
        if (d != null)
        {
            if (a.i() <= 0L)
            {
                break MISSING_BLOCK_LABEL_143;
            }
            f1 = (float)n / (float)a.i();
            d.onProgressUpdate(f1);
        }
_L2:
        if (k != 0L && (long)n >= k)
        {
            Logger.e("PcmPlayer", "Pcm player stop due to end position");
            b.b();
            return;
        }
        break MISSING_BLOCK_LABEL_176;
        d.onProgressUpdate(0.0F);
        if (true) goto _L2; else goto _L1
_L1:
        Logger.e("PcmPlayer", "Pcm player stop due to file end");
        b.b();
    }

    public void a(a a1)
    {
        d = a1;
    }

    public void a(s s1)
        throws IOException
    {
        if (b != null)
        {
            Logger.e("PcmPlayer", "Pcm player stop due to setRecordFile");
            b.b();
        }
        a = s1;
        a.d();
        l = (a.f() * a.g()) / 8;
        i();
    }

    public void a(Exception exception)
    {
        exception.printStackTrace();
    }

    public void b()
    {
        if (b != null)
        {
            Logger.e("PcmPlayer", "Pcm player stop due to stopPlay");
            b.b();
        }
    }

    public void c()
    {
        Logger.e("PcmPlayer", "onPlayerStart");
        if (d != null)
        {
            d.onPlayerStart();
        }
    }

    public void d()
    {
        Logger.e("PcmPlayer", "onPlayerEnd");
        if (d != null)
        {
            d.onPlayerStopped();
        }
    }

    public void e()
    {
        if (d != null)
        {
            d.onPlayerPaused();
        }
    }

    public boolean f()
    {
        return b != null && b.c();
    }

    public void g()
    {
        Logger.e("PcmPlayer", "onPlayerResume");
    }
}
