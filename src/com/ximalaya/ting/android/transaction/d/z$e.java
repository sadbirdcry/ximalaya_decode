// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.d;


// Referenced classes of package com.ximalaya.ting.android.transaction.d:
//            z

public static final class J extends Enum
{

    public static final G A;
    public static final G B;
    public static final G C;
    public static final G D;
    public static final G E;
    public static final G F;
    public static final G G;
    private static final G K[];
    public static final G a;
    public static final G b;
    public static final G c;
    public static final G d;
    public static final G e;
    public static final G f;
    public static final G g;
    public static final G h;
    public static final G i;
    public static final G j;
    public static final G k;
    public static final G l;
    public static final G m;
    public static final G n;
    public static final G o;
    public static final G p;
    public static final G q;
    public static final G r;
    public static final G s;
    public static final G t;
    public static final G u;
    public static final G v;
    public static final G w;
    public static final G x;
    public static final G y;
    public static final G z;
    public int H;
    public String I;
    public String J;

    public static J valueOf(String s1)
    {
        return (J)Enum.valueOf(com/ximalaya/ting/android/transaction/d/z$e, s1);
    }

    public static J[] values()
    {
        return (J[])K.clone();
    }

    static 
    {
        a = new <init>("TYPE_ADUIO", 0, 1, "audio", "\u97F3\u9891");
        b = new <init>("TYPE_FACE", 1, 2, "face", "\u5934\u50CF");
        c = new <init>("TYPE_COVER", 2, 3, "cover", "\u58F0\u97F3\u5C01\u9762");
        d = new <init>("TYPE_ATTACHMENT", 3, 4, "attachment", "\u9644\u4EF6\u56FE\u7247");
        e = new <init>("TYPE_SUBTITLE", 4, 5, "subtitle", "\u5B57\u5E55");
        f = new <init>("TYPE_TAG", 5, 6, "tag", "\u6807\u7B7E\u56FE\u7247");
        g = new <init>("TYPE_ALBUM", 6, 7, "album", "\u4E13\u8F91\u5C01\u9762");
        h = new <init>("TYPE_HEADER_THUMB", 7, 8, "headerThumb", "\u5934\u50CF\u7F29\u7565\u56FE");
        i = new <init>("TYPE_CATEGORY", 8, 9, "category", "\u5206\u7C7B\u56FE\u7247");
        j = new <init>("TYPE_PICTURE", 9, 10, "picture", "\u4EFB\u610F\u4E0D\u88C1\u526A\u7684\u56FE\u7247");
        k = new <init>("TYPE_APP_LOADING", 10, 11, "appLoading", "\u5B50 app loading \u56FE");
        l = new <init>("TYPE_APP_BACKGROUND", 11, 12, "appBackground", "app\u7528\u5230\u7684\u80CC\u666F\u56FE");
        m = new <init>("TYPE_DISCOVER_THUMB", 12, 13, "discoverThumb", "\u53D1\u73B0\u4E5F\u9996\u9875\u56FE\u7247");
        n = new <init>("TYPE_DISCOVER_RECOMMEND", 13, 14, "discoverRecommend", "\u53D1\u73B0\u9875\u5C0F\u7F16\u63A8\u8350");
        o = new <init>("TYPE_SUBAPP_BACKGROUND", 14, 15, "subappBackground", "\u5B50APP\u80CC\u666F\u56FE");
        p = new <init>("TYPE_APP_FOCUS", 15, 16, "appFocus", "app\u53D1\u73B0\u9875\u7126\u70B9\u56FE");
        q = new <init>("TYPE_AUDIO_PC", 16, 17, "audioPc", "pc\u7AEF\u4E0A\u4F20\u7684\u97F3\u9891");
        r = new <init>("TYPE_SELECTED_ALBUM", 17, 18, "selectedAlbum", "\u7CBE\u9009\u96C6\u5C01\u9762");
        s = new <init>("TYPE_APP_LOADING_IPAD", 18, 19, "appLoadingIpad", "\u5B50apploading\u56FEfor ipad");
        t = new <init>("TYPE_SPECIAL_ALBUM", 19, 20, "specialAlbum", "\u4E13\u9898\u56FE\u7247");
        u = new <init>("TYPE_SUB_APP_ICON", 20, 21, "subappIcon", "\u5B50app icon");
        v = new <init>("TYPE_ACTIVITY", 21, 22, "activity", "\u6D3B\u52A8\u5C01\u9762");
        w = new <init>("TYPE_APP_LOADING_IPHONE6", 22, 23, "appLoadingIphone6", "\u5B50apploading\u56FEfor iphone6");
        x = new <init>("TYPE_ANDROID_SUB_APP_ICON", 23, 24, "androidSubappIcon", "android\u5B50app Icon");
        y = new <init>("TYPE_ZONE_LOGO", 24, 25, "zoneLogo", "\u5708\u5B50LOGO\u56FE");
        z = new <init>("TYPE_POST_PICTURE", 25, 26, "postPicture", "\u5E16\u5B50\u56FE\u7247");
        A = new <init>("TYPE_LIVE_RADIO", 26, 27, "liveRadio", "\u76F4\u64AD\u7535\u53F0");
        B = new <init>("TYPE_LIVE_RADIO_PROGRAM", 27, 28, "liveRadioProgram", "\u76F4\u64AD\u8282\u76EE");
        C = new <init>("TYPE_AD_BANNER", 28, 29, "adBanner", "banner\u5E7F\u544A");
        D = new <init>("TYPE_AD_COMMENT", 29, 30, "adComment", "\u8BC4\u8BBA\u9876\u90E8\u5E7F\u544A");
        E = new <init>("TYPE_SPECIAL_ALBUM_CUT", 30, 31, "specialAlbumCut", "\u4E13\u9898\u56FE\u7247\u624B\u5DE5\u88C1\u526A\u7684");
        F = new <init>("TYPE_AD_LOADING", 31, 32, "adLoading", "\u5F00\u5C4F\u5E7F\u544A");
        G = new <init>("TYPE_AD_FEED", 32, 33, "adFeed", "feed\u5E7F\u544A\u56FE");
        K = (new K[] {
            a, b, c, d, e, f, g, h, i, j, 
            k, l, m, n, o, p, q, r, s, t, 
            u, v, w, x, y, z, A, B, C, D, 
            E, F, G
        });
    }

    private (String s1, int i1, int j1, String s2, String s3)
    {
        super(s1, i1);
        H = j1;
        I = s2;
        J = s3;
    }
}
