// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.download;

import com.ximalaya.ting.android.model.download.DownloadTask;
import java.util.Comparator;

// Referenced classes of package com.ximalaya.ting.android.transaction.download:
//            DownloadHandler

class g
    implements Comparator
{

    final DownloadHandler a;

    g(DownloadHandler downloadhandler)
    {
        a = downloadhandler;
        super();
    }

    public int a(DownloadTask downloadtask, DownloadTask downloadtask1)
    {
        if (downloadtask == null || downloadtask1 == null)
        {
            break MISSING_BLOCK_LABEL_148;
        }
        if (downloadtask1.albumId != downloadtask.albumId || downloadtask1.albumId <= 0L || downloadtask.downloadStatus != 4 || downloadtask1.downloadStatus != 4) goto _L2; else goto _L1
_L1:
        if (downloadtask.orderNum <= downloadtask1.orderNum) goto _L4; else goto _L3
_L3:
        return 1;
_L4:
        if (downloadtask.orderNum < downloadtask1.orderNum)
        {
            return -1;
        }
        if (downloadtask.orderNum != downloadtask1.orderNum)
        {
            break MISSING_BLOCK_LABEL_148;
        }
        if (downloadtask.create_at > downloadtask1.create_at) goto _L3; else goto _L5
_L5:
        if (downloadtask.create_at < downloadtask1.create_at)
        {
            return -1;
        }
        break MISSING_BLOCK_LABEL_148;
_L2:
        if (downloadtask.downloadStatus > downloadtask1.downloadStatus) goto _L3; else goto _L6
_L6:
        if (downloadtask.downloadStatus < downloadtask1.downloadStatus)
        {
            return -1;
        }
        if (downloadtask.downloadStatus == downloadtask1.downloadStatus)
        {
            return 0;
        }
        return 0;
    }

    public int compare(Object obj, Object obj1)
    {
        return a((DownloadTask)obj, (DownloadTask)obj1);
    }
}
