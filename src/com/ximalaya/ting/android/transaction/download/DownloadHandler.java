// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.download;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.database.DownloadTableHandler;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.download.PercentUpdatePacket;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.MyFileUtils;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

// Referenced classes of package com.ximalaya.ting.android.transaction.download:
//            g, i, h

public class DownloadHandler
{
    public static interface DownloadSoundsListener
    {

        public abstract void onTaskComplete();

        public abstract void onTaskDelete();

        public abstract void updateActionInfo();

        public abstract void updateDownloadInfo(int j);
    }

    private class a
    {

        final DownloadHandler a;
        private boolean b;
        private int c;

        public boolean a()
        {
            return b;
        }

        public int b()
        {
            return c;
        }

        public a(boolean flag, int j)
        {
            a = DownloadHandler.this;
            super();
            b = flag;
            if (flag)
            {
                c = j;
            }
        }
    }


    public static final int RESUME_DOWNLOAD = 161;
    public static volatile ThreadPoolExecutor downloadPool;
    private static volatile DownloadHandler instance = null;
    private static final Object listLock = new Object();
    public static BlockingQueue workQueue = new LinkedBlockingQueue();
    public Context appContext;
    public DownloadTask currentExcutingTask;
    public ArrayList downloadList;
    public List downloadListeners;
    public DownloadTableHandler downloadTableHandler;
    private Handler uiHandler;

    private DownloadHandler(Context context)
    {
        appContext = null;
        downloadTableHandler = null;
        downloadListeners = new ArrayList();
        appContext = context;
        downloadTableHandler = new DownloadTableHandler(context);
        downloadList = new ArrayList();
        currentExcutingTask = null;
    }

    private a containsTask(DownloadTask downloadtask)
    {
        Iterator iterator = downloadList.iterator();
        for (int j = 0; iterator.hasNext(); j++)
        {
            if (((DownloadTask)iterator.next()).trackId == downloadtask.trackId)
            {
                return new a(true, j);
            }
        }

        return new a(false, -1);
    }

    public static DownloadHandler getCurrentInstance()
    {
        return instance;
    }

    public static DownloadHandler getInstance(Context context)
    {
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/transaction/download/DownloadHandler;
        JVM INSTR monitorenter ;
        if (instance == null)
        {
            ToolUtil.createAppDirectory();
            instance = new DownloadHandler(context);
            instance.start();
        }
        com/ximalaya/ting/android/transaction/download/DownloadHandler;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        context;
        com/ximalaya/ting/android/transaction/download/DownloadHandler;
        JVM INSTR monitorexit ;
        throw context;
    }

    public static DownloadHandler getInstance(Context context, int j)
    {
        j;
        JVM INSTR tableswitch 161 161: default 20
    //                   161 24;
           goto _L1 _L2
_L1:
        return instance;
_L2:
        context = getInstance(context);
        if (context != null)
        {
            context.resumeAllIncomplete();
        }
        if (true) goto _L1; else goto _L3
_L3:
    }

    public static DownloadHandler getInstance2(Context context, int j)
    {
        if (j != 161 || instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/transaction/download/DownloadHandler;
        JVM INSTR monitorenter ;
        if (instance != null)
        {
            break MISSING_BLOCK_LABEL_156;
        }
        ToolUtil.createAppDirectory();
        instance = new DownloadHandler(context);
        instance.start();
        downloadPool = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, workQueue);
        if (instance.downloadTableHandler != null)
        {
            synchronized (listLock)
            {
                if (instance.downloadTableHandler != null)
                {
                    instance.downloadList = instance.downloadTableHandler.readDownloadList(downloadPool, j);
                    Logger.log("1122-2", (new StringBuilder()).append("[getInstance2]===").append(instance.downloadList.size()).toString());
                }
            }
        }
_L3:
        com/ximalaya/ting/android/transaction/download/DownloadHandler;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        exception;
        context;
        JVM INSTR monitorexit ;
        throw exception;
        context;
        com/ximalaya/ting/android/transaction/download/DownloadHandler;
        JVM INSTR monitorexit ;
        throw context;
        instance.resumeAllIncomplete();
          goto _L3
    }

    public static boolean hasDownloaded(String s, Context context)
    {
        if (!Utilities.isBlank(s))
        {
            if (s.contains(com.ximalaya.ting.android.a.aj))
            {
                return true;
            }
            if (s.contains("http"))
            {
                context = getInstance(context);
                if (context != null && context.isDownloadCompleted(s))
                {
                    s = PlayTools.getDownloadFile(s);
                    if (s != null && s.exists())
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static boolean hasDownloaded2(String s, Context context)
    {
        if (!Utilities.isBlank(s))
        {
            if (s.contains(com.ximalaya.ting.android.a.aj))
            {
                return true;
            }
            if (s.contains("http"))
            {
                context = getInstance(context);
                if (context != null && context.isDownloadCompleted(s))
                {
                    context = PlayTools.getDownloadFileWithExpandedName(s);
                    s = PlayTools.getDownloadFileWithoutExpandedName(s);
                    if (context != null && context.exists())
                    {
                        return true;
                    }
                    if (s != null && s.exists())
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private final void initSortKeyword(DownloadTask downloadtask)
    {
        switch (downloadtask.downloadStatus)
        {
        default:
            downloadtask.sortKeyword = 4;
            return;

        case 2: // '\002'
            downloadtask.sortKeyword = 0;
            return;

        case 1: // '\001'
            downloadtask.sortKeyword = 1;
            return;

        case 0: // '\0'
            downloadtask.sortKeyword = 2;
            return;

        case 3: // '\003'
            downloadtask.sortKeyword = 3;
            break;
        }
    }

    public static void releaseCurrentInstance()
    {
        instance = null;
    }

    private void saveDownloadListSDCARD()
    {
        try
        {
            MyFileUtils.writeDownloadSoundToSD(JSON.toJSONString(getInstance(appContext).downloadList));
            return;
        }
        catch (Exception exception)
        {
            return;
        }
    }

    private final void shutdownAndAwaitTermination()
    {
        if (downloadPool != null)
        {
            downloadPool.shutdown();
            try
            {
                if (!downloadPool.awaitTermination(1000L, TimeUnit.MILLISECONDS))
                {
                    downloadPool.shutdownNow();
                    if (!downloadPool.awaitTermination(1000L, TimeUnit.MILLISECONDS))
                    {
                        System.err.println("Pool did not terminate");
                    }
                }
            }
            catch (InterruptedException interruptedexception)
            {
                downloadPool.shutdownNow();
                Thread.currentThread().interrupt();
            }
            downloadPool = null;
        }
    }

    private final ArrayList sortDownloadList(ArrayList arraylist)
    {
        if (arraylist != null && arraylist.size() > 1)
        {
            try
            {
                Collections.sort(arraylist, new g(this));
            }
            catch (Exception exception)
            {
                Logger.throwRuntimeException((new StringBuilder()).append("\u4E0B\u8F7D\u5217\u8868\u6392\u5E8F\u7B97\u6CD5\u5F02\u5E38\uFF1A ").append(Logger.getLineInfo()).toString());
            }
            if (downloadList == null || downloadList.size() <= 0)
            {
                Logger.log("0814", "====DownloadHandler======== ! ! ! ! ! !");
                return arraylist;
            }
        }
        return arraylist;
    }

    private void updateFavoritedInfo(long l, boolean flag)
    {
        Iterator iterator = downloadList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            if (downloadtask.trackId == l)
            {
                downloadtask.is_favorited = flag;
            }
        } while (true);
        iterator = downloadListeners.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            DownloadSoundsListener downloadsoundslistener = (DownloadSoundsListener)iterator.next();
            if (downloadsoundslistener != null)
            {
                downloadsoundslistener.updateActionInfo();
            }
        } while (true);
    }

    public void addDownloadListeners(DownloadSoundsListener downloadsoundslistener)
    {
        synchronized (downloadListeners)
        {
            if (!downloadListeners.contains(downloadsoundslistener))
            {
                downloadListeners.add(downloadsoundslistener);
            }
        }
        return;
        downloadsoundslistener;
        list;
        JVM INSTR monitorexit ;
        throw downloadsoundslistener;
    }

    public void cancelAll()
    {
        Logger.log("dl_download", "[pauseAllAndExit]==================");
        pauseAllDownload(false);
    }

    public void cancelAll2()
    {
        Logger.log("dl_download", "[pauseAllAndExit]==================");
        for (Iterator iterator = downloadList.iterator(); iterator.hasNext();)
        {
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            downloadtask.isRunning = false;
            workQueue.remove(downloadtask);
            downloadtask.is_playing = false;
        }

    }

    public boolean containsTask(long l)
    {
        for (Iterator iterator = downloadList.iterator(); iterator.hasNext();)
        {
            if (((DownloadTask)iterator.next()).trackId == l)
            {
                return true;
            }
        }

        return false;
    }

    public void debugMonitorWorkQueue(String s)
    {
        Iterator iterator = workQueue.iterator();
        Logger.log(s, (new StringBuilder()).append("debugMonitorWorkQueue====").append(s).append("======").toString());
        DownloadTask downloadtask;
        for (; iterator.hasNext(); Logger.log(s, (new StringBuilder()).append("=").append(downloadtask.trackId).append(", ").append(downloadtask.title).toString()))
        {
            downloadtask = (DownloadTask)iterator.next();
        }

    }

    public void delAllCompleteTasks()
    {
        delDownloadTasks(getFinishedTasks());
    }

    public void delAllDownloadTask()
    {
        if (instance != null && downloadList != null && !downloadList.isEmpty())
        {
            synchronized (listLock)
            {
                if (instance != null && downloadList != null && !downloadList.isEmpty())
                {
                    cancelAll();
                    instance.shutdownAndAwaitTermination();
                    instance.downloadList.clear();
                }
            }
        }
        if (instance != null && instance.downloadTableHandler != null)
        {
            instance.downloadTableHandler.deleteAllRows();
        }
        StorageUtils.cleanAllDownloadFiles();
        MyFileUtils.deleteFileForImage();
        downloadPool = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, workQueue);
        if (downloadListeners != null)
        {
            obj = downloadListeners.iterator();
            do
            {
                if (!((Iterator) (obj)).hasNext())
                {
                    break;
                }
                DownloadSoundsListener downloadsoundslistener = (DownloadSoundsListener)((Iterator) (obj)).next();
                if (downloadsoundslistener != null)
                {
                    downloadsoundslistener.onTaskDelete();
                }
            } while (true);
        }
        break MISSING_BLOCK_LABEL_177;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        gotoUpdateUI();
        return;
    }

    public void delAllIncompleteTasks()
    {
        delDownloadTasks(getUnfinishedTasks());
    }

    public int delDownloadTask(DownloadTask downloadtask)
    {
        byte byte0;
        byte0 = -1;
        debugMonitorWorkQueue((new StringBuilder()).append("delDownloadTask[+]: ").append(downloadtask.trackId).append(", ").append(downloadtask.title).toString());
        if (!downloadList.contains(downloadtask)) goto _L2; else goto _L1
_L1:
        Object obj = listLock;
        obj;
        JVM INSTR monitorenter ;
        if (!downloadList.contains(downloadtask))
        {
            break MISSING_BLOCK_LABEL_138;
        }
        workQueue.remove(downloadtask);
        downloadtask.isRunning = false;
        if (downloadtask.coverLarge != null)
        {
            if (appContext == null)
            {
                appContext = MyApplication.b();
            }
            if (appContext != null)
            {
                ImageManager2.from(appContext).deleteBitmapFromDownloadCache(downloadtask.coverLarge);
            }
        }
        downloadList.remove(downloadtask);
        byte0 = 0;
        obj;
        JVM INSTR monitorexit ;
_L4:
        debugMonitorWorkQueue("delDownloadTask[-]");
        delLocalData(downloadtask);
        gotoUpdateUI();
        downloadtask = downloadListeners.iterator();
        do
        {
            if (!downloadtask.hasNext())
            {
                break;
            }
            obj = (DownloadSoundsListener)downloadtask.next();
            if (obj != null)
            {
                ((DownloadSoundsListener) (obj)).onTaskDelete();
            }
        } while (true);
        break MISSING_BLOCK_LABEL_204;
        downloadtask;
        obj;
        JVM INSTR monitorexit ;
        throw downloadtask;
        return byte0;
_L2:
        byte0 = -1;
        if (true) goto _L4; else goto _L3
_L3:
    }

    public int delDownloadTask2(DownloadTask downloadtask)
    {
        byte byte0;
        int j;
        byte0 = -1;
        a a1 = containsTask(downloadtask);
        a1.b();
        boolean flag = a1.a();
        debugMonitorWorkQueue((new StringBuilder()).append("delDownloadTask[+]: ").append(downloadtask.trackId).append(", ").append(downloadtask.title).toString());
        j = byte0;
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_140;
        }
        Object obj = listLock;
        obj;
        JVM INSTR monitorenter ;
        Object obj1 = containsTask(downloadtask);
        j = ((a) (obj1)).b();
        if (!((a) (obj1)).a())
        {
            break MISSING_BLOCK_LABEL_134;
        }
        obj1 = (DownloadTask)downloadList.get(j);
        workQueue.remove(downloadtask);
        obj1.isRunning = false;
        downloadList.remove(downloadtask);
        byte0 = 0;
        obj;
        JVM INSTR monitorexit ;
        j = byte0;
        debugMonitorWorkQueue("delDownloadTask[-]");
        delLocalData(downloadtask);
        return j;
        downloadtask;
        obj;
        JVM INSTR monitorexit ;
        throw downloadtask;
    }

    public void delDownloadTasks(List list)
    {
        if (list == null || list.size() <= 0)
        {
            return;
        }
        for (Iterator iterator = list.iterator(); iterator.hasNext(); pauseDownload((DownloadTask)iterator.next())) { }
        synchronized (listLock)
        {
            downloadList.removeAll(list);
        }
        downloadTableHandler.deleteRecords(list);
        for (list = list.iterator(); list.hasNext();)
        {
            obj = (DownloadTask)list.next();
            if (((DownloadTask) (obj)).coverLarge != null && MyApplication.b() != null)
            {
                ImageManager2.from(MyApplication.b()).deleteBitmapFromDownloadCache(((DownloadTask) (obj)).coverLarge);
            }
            try
            {
                MyFileUtils.deleteFile(new File(((DownloadTask) (obj)).downloadLocation, ToolUtil.md5(((DownloadTask) (obj)).downLoadUrl)));
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
        }

        break MISSING_BLOCK_LABEL_157;
        list;
        obj;
        JVM INSTR monitorexit ;
        throw list;
        list = downloadListeners.iterator();
        do
        {
            if (!list.hasNext())
            {
                break;
            }
            DownloadSoundsListener downloadsoundslistener = (DownloadSoundsListener)list.next();
            if (downloadsoundslistener != null)
            {
                downloadsoundslistener.onTaskDelete();
            }
        } while (true);
        gotoUpdateUI();
        return;
    }

    public void delDownloadTasks1(List list)
    {
        if (list != null && list.size() > 0)
        {
            list = list.iterator();
            while (list.hasNext()) 
            {
                delDownloadTask((DownloadTask)list.next());
            }
        }
    }

    public final boolean delLocalData(DownloadTask downloadtask)
    {
        boolean flag = downloadTableHandler.deleteRecord(downloadtask.trackId);
        Logger.log("2013-08-30", (new StringBuilder()).append("deleteRecord=[").append(downloadtask.trackId).append(", ").append(downloadtask.title).append("]===").append(flag).append("======").toString());
        try
        {
            MyFileUtils.deleteFile(new File(downloadtask.downloadLocation, ToolUtil.md5(downloadtask.downLoadUrl)));
        }
        // Misplaced declaration of an exception variable
        catch (DownloadTask downloadtask)
        {
            return flag;
        }
        return flag;
    }

    public final File getDownloadFile(SoundInfo soundinfo)
    {
        for (Iterator iterator = downloadList.iterator(); iterator.hasNext();)
        {
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            if (soundinfo.trackId == downloadtask.trackId && Utilities.isNotBlank(downloadtask.downLoadUrl) && Utilities.isNotBlank(downloadtask.downloadLocation))
            {
                return new File(downloadtask.downloadLocation, ToolUtil.md5(downloadtask.downLoadUrl));
            }
        }

        return null;
    }

    public ArrayList getDownloadPageList()
    {
        return sortDownloadList(downloadList);
    }

    public final ArrayList getFinishedTasks()
    {
        Object obj1 = new ArrayList();
        if (downloadList == null || downloadList.size() <= 0) goto _L2; else goto _L1
_L1:
        Object obj = listLock;
        obj;
        JVM INSTR monitorenter ;
        if (downloadList == null || downloadList.size() <= 0)
        {
            break MISSING_BLOCK_LABEL_115;
        }
        Iterator iterator = downloadList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break MISSING_BLOCK_LABEL_115;
            }
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            switch (downloadtask.downloadStatus)
            {
            case 4: // '\004'
                ((ArrayList) (obj1)).add(downloadtask);
                break;
            }
        } while (true);
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
        obj;
        JVM INSTR monitorexit ;
_L2:
        return ((ArrayList) (obj1));
    }

    public final int getIncompleteTaskCount()
    {
        boolean flag;
        for (flag = false; downloadList == null || downloadList.size() <= 0 || downloadList == null || downloadList.size() <= 0;)
        {
            return 0;
        }

        Object obj = listLock;
        obj;
        JVM INSTR monitorenter ;
        int j = ((flag) ? 1 : 0);
        if (downloadList == null) goto _L2; else goto _L1
_L1:
        j = ((flag) ? 1 : 0);
        if (downloadList.size() <= 0) goto _L2; else goto _L3
_L3:
        Iterator iterator = downloadList.iterator();
        j = 0;
_L7:
        if (!iterator.hasNext()) goto _L2; else goto _L4
_L4:
        switch (((DownloadTask)iterator.next()).downloadStatus)
        {
        default:
            if (false)
            {
            }
            continue; /* Loop/switch isn't completed */

        case -1: 
        case 0: // '\0'
        case 1: // '\001'
        case 2: // '\002'
        case 3: // '\003'
            break;
        }
          goto _L5
_L2:
        obj;
        JVM INSTR monitorexit ;
        return j;
        Exception exception;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
_L5:
        j++;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public ArrayList getLocalAlbumRelateSound(long l, Comparator comparator)
    {
        Object obj = getSortedFinishedDownloadList();
        if (obj == null || ((ArrayList) (obj)).size() == 0)
        {
            return null;
        }
        ArrayList arraylist = new ArrayList();
        obj = ((ArrayList) (obj)).iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break;
            }
            DownloadTask downloadtask = (DownloadTask)((Iterator) (obj)).next();
            if (downloadtask.albumId == l)
            {
                arraylist.add(downloadtask);
            }
        } while (true);
        if (comparator != null)
        {
            Collections.sort(arraylist, comparator);
        }
        return arraylist;
    }

    public final ArrayList getSortedFinishedDownloadList()
    {
        ArrayList arraylist;
        arraylist = getFinishedTasks();
        if (arraylist == null || arraylist.size() <= 1)
        {
            return arraylist;
        }
        synchronized (listLock)
        {
            Collections.sort(arraylist, new i(this));
        }
        return arraylist;
        exception1;
        obj;
        JVM INSTR monitorexit ;
        try
        {
            throw exception1;
        }
        catch (Exception exception)
        {
            Logger.throwRuntimeException((new StringBuilder()).append("\u4E0B\u8F7D\u5217\u8868\u6392\u5E8F\u7B97\u6CD5\u5F02\u5E38\uFF1A ").append(Logger.getLineInfo()).toString());
        }
        return arraylist;
    }

    public final ArrayList getUnfinishedTasks()
    {
        Object obj1 = new ArrayList();
        if (downloadList == null || downloadList.size() <= 0) goto _L2; else goto _L1
_L1:
        Object obj = listLock;
        obj;
        JVM INSTR monitorenter ;
        if (downloadList == null || downloadList.size() <= 0)
        {
            break MISSING_BLOCK_LABEL_131;
        }
        Iterator iterator = downloadList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break MISSING_BLOCK_LABEL_131;
            }
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            switch (downloadtask.downloadStatus)
            {
            case -1: 
            case 0: // '\0'
            case 1: // '\001'
            case 2: // '\002'
            case 3: // '\003'
                ((ArrayList) (obj1)).add(downloadtask);
                break;
            }
        } while (true);
        obj1;
        obj;
        JVM INSTR monitorexit ;
        throw obj1;
        obj;
        JVM INSTR monitorexit ;
_L2:
        return ((ArrayList) (obj1));
    }

    public void gotoUpdateUI()
    {
        if (downloadList != null)
        {
            int j = getIncompleteTaskCount();
            Iterator iterator = downloadListeners.iterator();
            while (iterator.hasNext()) 
            {
                DownloadSoundsListener downloadsoundslistener = (DownloadSoundsListener)iterator.next();
                if (downloadsoundslistener != null)
                {
                    downloadsoundslistener.updateDownloadInfo(j);
                }
            }
        }
    }

    public final boolean hasPauseTasks()
    {
label0:
        {
            if (getIncompleteTaskCount() <= 0 || downloadList == null || downloadList.size() <= 0)
            {
                break MISSING_BLOCK_LABEL_94;
            }
            synchronized (listLock)
            {
                if (downloadList == null || downloadList.size() <= 0)
                {
                    break label0;
                }
                Iterator iterator = downloadList.iterator();
                do
                {
                    if (!iterator.hasNext())
                    {
                        break label0;
                    }
                } while (2 != ((DownloadTask)iterator.next()).downloadStatus);
            }
            return true;
        }
        obj;
        JVM INSTR monitorexit ;
        break MISSING_BLOCK_LABEL_94;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        return false;
    }

    public final boolean hasUnfinishedTask()
    {
label0:
        {
            if (getIncompleteTaskCount() <= 0 || downloadList == null || downloadList.size() <= 0)
            {
                break MISSING_BLOCK_LABEL_94;
            }
            synchronized (listLock)
            {
                if (downloadList == null || downloadList.size() <= 0)
                {
                    break label0;
                }
                Iterator iterator = downloadList.iterator();
                do
                {
                    if (!iterator.hasNext())
                    {
                        break label0;
                    }
                } while (4 == ((DownloadTask)iterator.next()).downloadStatus);
            }
            return true;
        }
        obj;
        JVM INSTR monitorexit ;
        break MISSING_BLOCK_LABEL_94;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        return false;
    }

    public final void informUI(int j, DownloadTask downloadtask)
    {
        this;
        JVM INSTR monitorenter ;
        if (instance == null) goto _L2; else goto _L1
_L1:
        Handler handler = uiHandler;
        if (handler != null) goto _L3; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        if (uiHandler == null) goto _L2; else goto _L4
_L4:
        Logger.log("dl_download", (new StringBuilder()).append("[Action: informUI]--").append(j).append(", ").append(downloadtask.title).append("--").append(downloadtask.getDownloadPercent()).append("%").toString());
        if (4 != downloadtask.downloadStatus && downloadtask.downloaded != downloadtask.filesize)
        {
            break MISSING_BLOCK_LABEL_168;
        }
        PercentUpdatePacket percentupdatepacket = new PercentUpdatePacket();
        downloadtask.downloadStatus = 4;
        percentupdatepacket.trackId = downloadtask.trackId;
        percentupdatepacket.action = 4;
        downloadtask = new Message();
        downloadtask.what = 0x7fff1003;
        downloadtask.obj = percentupdatepacket;
        uiHandler.sendMessage(downloadtask);
          goto _L2
        downloadtask;
        throw downloadtask;
        PercentUpdatePacket percentupdatepacket1 = new PercentUpdatePacket();
        percentupdatepacket1.percentage = downloadtask.getDownloadPercent();
        percentupdatepacket1.trackId = downloadtask.trackId;
        percentupdatepacket1.action = j;
        percentupdatepacket1.downloaded = downloadtask.downloaded;
        percentupdatepacket1.filesize = downloadtask.filesize;
        downloadtask = new Message();
        downloadtask.what = 0x7fff1003;
        downloadtask.obj = percentupdatepacket1;
        uiHandler.sendMessage(downloadtask);
          goto _L2
    }

    public com.ximalaya.ting.android.a.a.a insert(DownloadTask downloadtask)
    {
        return insert(downloadtask, true);
    }

    public com.ximalaya.ting.android.a.a.a insert(DownloadTask downloadtask, boolean flag)
    {
        com.ximalaya.ting.android.a.a.a a1;
        com.ximalaya.ting.android.a.a.a a2;
        a2 = com.ximalaya.ting.android.a.a.a.a;
        Logger.log("2013-08-29", (new StringBuilder()).append("insert(+)").append(workQueue.size()).append(", ").append(downloadPool.getQueue().size()).toString());
        a1 = a2;
        if (downloadTableHandler == null) goto _L2; else goto _L1
_L1:
        a1 = a2;
        if (downloadTableHandler.contains(downloadtask.trackId)) goto _L2; else goto _L3
_L3:
        Object obj = listLock;
        obj;
        JVM INSTR monitorenter ;
        com.ximalaya.ting.android.a.a.a a3;
        a1 = a2;
        a3 = a2;
        if (downloadTableHandler == null) goto _L5; else goto _L4
_L4:
        a1 = a2;
        a3 = a2;
        if (downloadTableHandler.contains(downloadtask.trackId)) goto _L5; else goto _L6
_L6:
        a3 = a2;
        if (NetworkUtils.isNetworkAvaliable(appContext)) goto _L8; else goto _L7
_L7:
        a3 = a2;
        a1 = com.ximalaya.ting.android.a.a.a.d;
_L5:
        a3 = a1;
        obj;
        JVM INSTR monitorexit ;
_L2:
        Logger.log("0814", (new StringBuilder()).append("====DownloadSoundsListForAlbumFragment========after INSERT downloadList: ").append(downloadList.size()).toString());
        Logger.log("2013-08-29", (new StringBuilder()).append("===insert(-)").append(workQueue.size()).append(", ").append(downloadPool.getQueue().size()).toString());
        gotoUpdateUI();
        return a1;
_L8:
        a3 = a2;
        downloadtask.downloadStatus = -1;
        a3 = a2;
        if (downloadTableHandler.save(downloadtask))
        {
            break MISSING_BLOCK_LABEL_282;
        }
        a3 = a2;
        a1 = com.ximalaya.ting.android.a.a.a.b;
          goto _L5
        a3 = a2;
        downloadList.add(downloadtask);
        if (!flag) goto _L10; else goto _L9
_L9:
        a3 = a2;
        downloadtask.downloadStatus = 0;
        a3 = a2;
        downloadPool.execute(downloadtask);
_L11:
        a3 = a2;
        refreshUnfinishedDownloadListUI();
        a3 = a2;
        downloadTableHandler.updateSoundRecord(downloadtask);
        a1 = a2;
          goto _L5
        downloadtask;
_L13:
        obj;
        JVM INSTR monitorexit ;
        try
        {
            throw downloadtask;
        }
        // Misplaced declaration of an exception variable
        catch (DownloadTask downloadtask)
        {
            a1 = a3;
        }
_L12:
        Logger.log("DownloadHandler", downloadtask.getMessage());
          goto _L2
_L10:
        a3 = a2;
        downloadtask.downloadStatus = 2;
          goto _L11
        downloadtask;
        a1 = a2;
          goto _L12
        downloadtask;
          goto _L13
    }

    public final com.ximalaya.ting.android.a.a.a insertAll(List list)
    {
        return insertAll(list, true);
    }

    public final com.ximalaya.ting.android.a.a.a insertAll(List list, boolean flag)
    {
        com.ximalaya.ting.android.a.a.a a1;
        if (list == null || list.size() <= 0)
        {
            return com.ximalaya.ting.android.a.a.a.g;
        }
        if (downloadTableHandler == null)
        {
            return com.ximalaya.ting.android.a.a.a.b;
        }
        a1 = com.ximalaya.ting.android.a.a.a.a;
        Object obj = listLock;
        obj;
        JVM INSTR monitorenter ;
        if (NetworkUtils.isNetworkAvaliable(appContext)) goto _L2; else goto _L1
_L1:
        list = com.ximalaya.ting.android.a.a.a.d;
        return list;
        list;
        obj;
        JVM INSTR monitorexit ;
        try
        {
            throw list;
        }
        // Misplaced declaration of an exception variable
        catch (List list) { }
_L6:
        return a1;
_L2:
        List list1 = downloadTableHandler.save(list);
        if (list1 == null) goto _L4; else goto _L3
_L3:
        if (list1.size() > 0 && list1.size() == list.size()) goto _L5; else goto _L4
_L4:
        list = com.ximalaya.ting.android.a.a.a.b;
        obj;
        JVM INSTR monitorexit ;
        return list;
_L7:
        DownloadTask downloadtask;
        int j;
        for (; j >= list1.size(); j++)
        {
            break MISSING_BLOCK_LABEL_204;
        }

        if (!((Boolean)list1.get(j)).booleanValue())
        {
            break MISSING_BLOCK_LABEL_224;
        }
        downloadtask = (DownloadTask)list.get(j);
        downloadList.add(downloadtask);
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_195;
        }
        downloadtask.downloadStatus = 0;
        downloadPool.execute(downloadtask);
        break MISSING_BLOCK_LABEL_224;
        downloadtask.downloadStatus = 2;
        break MISSING_BLOCK_LABEL_224;
        refreshUnfinishedDownloadListUI();
        gotoUpdateUI();
        obj;
        JVM INSTR monitorexit ;
          goto _L6
_L5:
        j = 0;
          goto _L7
    }

    public final boolean isDownloadCompleted(SoundInfo soundinfo)
    {
        for (Iterator iterator = downloadList.iterator(); iterator.hasNext();)
        {
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            if (soundinfo.trackId == downloadtask.trackId && downloadtask.downloadStatus == 4)
            {
                return true;
            }
        }

        return false;
    }

    public final boolean isDownloadCompleted(String s)
    {
label0:
        {
            if (!Utilities.isNotBlank(s))
            {
                break label0;
            }
            Iterator iterator = downloadList.iterator();
            DownloadTask downloadtask;
            do
            {
                if (!iterator.hasNext())
                {
                    break label0;
                }
                downloadtask = (DownloadTask)iterator.next();
                if (s.equalsIgnoreCase(downloadtask.playUrl64) && downloadtask.downloadStatus == 4)
                {
                    return true;
                }
            } while (!s.equalsIgnoreCase(downloadtask.playUrl32) || downloadtask.downloadStatus != 4);
            return true;
        }
        return false;
    }

    public final boolean isDownloading()
    {
label0:
        {
            if (getIncompleteTaskCount() <= 0 || downloadList == null || downloadList.size() <= 0)
            {
                break MISSING_BLOCK_LABEL_103;
            }
            synchronized (listLock)
            {
                if (downloadList == null || downloadList.size() <= 0)
                {
                    break label0;
                }
                Iterator iterator = downloadList.iterator();
                DownloadTask downloadtask;
                do
                {
                    if (!iterator.hasNext())
                    {
                        break label0;
                    }
                    downloadtask = (DownloadTask)iterator.next();
                } while (1 != downloadtask.downloadStatus && downloadtask.downloadStatus != 0);
            }
            return true;
        }
        obj;
        JVM INSTR monitorexit ;
        break MISSING_BLOCK_LABEL_103;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
        return false;
    }

    public boolean isDownloading(SoundInfo soundinfo)
    {
        for (Iterator iterator = downloadList.iterator(); iterator.hasNext();)
        {
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            if (downloadtask.trackId == soundinfo.trackId && (downloadtask.downloadStatus == 2 || downloadtask.downloadStatus == -1 || downloadtask.downloadStatus == 0 || downloadtask.downloadStatus == 1))
            {
                return true;
            }
        }

        return false;
    }

    public final boolean isInDownloadList(long l)
    {
        this;
        JVM INSTR monitorenter ;
        Iterator iterator = downloadList.iterator();
_L4:
        if (!iterator.hasNext()) goto _L2; else goto _L1
_L1:
        long l1 = ((DownloadTask)iterator.next()).trackId;
        if (l != l1) goto _L4; else goto _L3
_L3:
        boolean flag = true;
_L6:
        this;
        JVM INSTR monitorexit ;
        return flag;
        Object obj;
        obj;
        flag = false;
        continue; /* Loop/switch isn't completed */
        obj;
        throw obj;
_L2:
        flag = false;
        if (true) goto _L6; else goto _L5
_L5:
    }

    public void pauseAllDownload()
    {
        pauseAllDownload(true);
    }

    public void pauseAllDownload(boolean flag)
    {
        Logger.log("dl_download", "[pauseAllDownload]==================");
        (new ArrayList()).addAll(workQueue);
        workQueue.clear();
        Iterator iterator = downloadList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            if (!downloadtask.equals(currentExcutingTask) && (downloadtask.downloadStatus == 1 || downloadtask.downloadStatus == 0))
            {
                downloadtask.isRunning = false;
                downloadtask.downloadStatus = 2;
            }
        } while (true);
        pauseDownload(currentExcutingTask);
        if (flag)
        {
            refreshUnfinishedDownloadListUI();
        }
    }

    public void pauseAllDownload1()
    {
        Logger.log("dl_download", (new StringBuilder()).append("[pauseAllDownload]==================").append(currentExcutingTask.title).append(", ").append(workQueue.size()).toString());
        Logger.log("1122", (new StringBuilder()).append("[pauseAllDownload]==================").append(currentExcutingTask.title).append("=").append(currentExcutingTask.downloadStatus).append(", ").append(workQueue.size()).toString());
        DownloadTask downloadtask;
        for (Iterator iterator = workQueue.iterator(); iterator.hasNext(); Logger.log("1122", (new StringBuilder()).append("[pauseAllDownload]===").append(downloadtask.title).append(", ").append(downloadtask.downloadStatus).toString()))
        {
            downloadtask = (DownloadTask)(Runnable)iterator.next();
        }

        Logger.log("1122", (new StringBuilder()).append("==================").append(downloadList.size()).toString());
        DownloadTask downloadtask1;
        for (Iterator iterator1 = downloadList.iterator(); iterator1.hasNext(); Logger.log("1122", (new StringBuilder()).append("[pauseAllDownload]===").append(downloadtask1.title).append(", ").append(downloadtask1.downloadStatus).toString()))
        {
            downloadtask1 = (DownloadTask)iterator1.next();
        }

        Object obj = new ArrayList();
        ((ArrayList) (obj)).addAll(workQueue);
        workQueue.clear();
        obj = ((ArrayList) (obj)).iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break;
            }
            DownloadTask downloadtask2 = (DownloadTask)((Iterator) (obj)).next();
            if (!downloadtask2.equals(currentExcutingTask) && (downloadtask2.downloadStatus == 1 || downloadtask2.downloadStatus == 0))
            {
                downloadtask2.isRunning = false;
                downloadtask2.downloadStatus = 2;
            }
        } while (true);
        pauseDownload(currentExcutingTask);
        refreshUnfinishedDownloadListUI();
        Logger.log("1122", (new StringBuilder()).append("[]==================").append(currentExcutingTask.title).append("=").append(currentExcutingTask.downloadStatus).append(", ").append(workQueue.size()).toString());
        DownloadTask downloadtask3;
        for (Iterator iterator2 = workQueue.iterator(); iterator2.hasNext(); Logger.log("1122", (new StringBuilder()).append("[]===").append(downloadtask3.title).append(", ").append(downloadtask3.downloadStatus).toString()))
        {
            downloadtask3 = (DownloadTask)(Runnable)iterator2.next();
        }

        Logger.log("1122", (new StringBuilder()).append("[]==================").append(downloadList.size()).toString());
        DownloadTask downloadtask4;
        for (Iterator iterator3 = downloadList.iterator(); iterator3.hasNext(); Logger.log("1122", (new StringBuilder()).append("[]===").append(downloadtask4.title).append(", ").append(downloadtask4.downloadStatus).toString()))
        {
            downloadtask4 = (DownloadTask)iterator3.next();
        }

    }

    public void pauseAllDownload2()
    {
        Logger.log("dl_download", "[pauseAllDownload]==================");
        Iterator iterator = downloadList.iterator();
        do
        {
            if (!iterator.hasNext())
            {
                break;
            }
            DownloadTask downloadtask = (DownloadTask)iterator.next();
            if (downloadtask.downloadStatus == 1 || downloadtask.downloadStatus == 0)
            {
                downloadtask.isRunning = false;
                workQueue.remove(downloadtask);
            }
        } while (true);
    }

    public void pauseDownload(DownloadTask downloadtask)
    {
        if (downloadtask == null)
        {
            return;
        } else
        {
            downloadtask.isRunning = false;
            return;
        }
    }

    public final void refreshDownloadListUI()
    {
        this;
        JVM INSTR monitorenter ;
        if (instance == null) goto _L2; else goto _L1
_L1:
        Handler handler = uiHandler;
        if (handler != null) goto _L3; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        PercentUpdatePacket percentupdatepacket = new PercentUpdatePacket();
        percentupdatepacket.downloadList = getDownloadPageList();
        Message message = new Message();
        message.what = 0x7fff1004;
        message.obj = percentupdatepacket;
        uiHandler.sendMessage(message);
        if (true) goto _L2; else goto _L4
_L4:
        Exception exception;
        exception;
        throw exception;
    }

    public final void refreshFinishedDownloadListUI()
    {
        this;
        JVM INSTR monitorenter ;
        if (instance == null) goto _L2; else goto _L1
_L1:
        Handler handler = uiHandler;
        if (handler != null) goto _L3; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        PercentUpdatePacket percentupdatepacket = new PercentUpdatePacket();
        percentupdatepacket.downloadList = getSortedFinishedDownloadList();
        Message message = new Message();
        message.what = 0x7fff1005;
        message.obj = percentupdatepacket;
        uiHandler.sendMessage(message);
        if (true) goto _L2; else goto _L4
_L4:
        Exception exception;
        exception;
        throw exception;
    }

    public final void refreshUnfinishedDownloadListUI()
    {
        this;
        JVM INSTR monitorenter ;
        if (instance == null) goto _L2; else goto _L1
_L1:
        Handler handler = uiHandler;
        if (handler != null) goto _L3; else goto _L2
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
_L3:
        PercentUpdatePacket percentupdatepacket = new PercentUpdatePacket();
        percentupdatepacket.downloadList = getUnfinishedTasks();
        Message message = new Message();
        message.what = 0x7fff1006;
        message.obj = percentupdatepacket;
        uiHandler.sendMessage(message);
        if (true) goto _L2; else goto _L4
_L4:
        Exception exception;
        exception;
        throw exception;
    }

    public final void releaseOnExit()
    {
        if (instance == null)
        {
            break MISSING_BLOCK_LABEL_49;
        }
        instance.stop();
        instance.shutdownAndAwaitTermination();
        saveDownloadListSDCARD();
        instance.currentExcutingTask = null;
        instance.downloadList.clear();
        instance.downloadList = null;
        instance = null;
        return;
        Exception exception;
        exception;
        exception.printStackTrace();
        return;
    }

    public void removeDownloadListeners(DownloadSoundsListener downloadsoundslistener)
    {
        downloadsoundslistener;
        JVM INSTR monitorenter ;
        downloadListeners.remove(downloadsoundslistener);
        downloadsoundslistener;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        downloadsoundslistener;
        JVM INSTR monitorexit ;
        throw exception;
    }

    public void resume(DownloadTask downloadtask)
    {
        if (appContext == null || NetworkUtils.getNetType(appContext) == -1)
        {
            return;
        } else
        {
            downloadtask.isRunning = true;
            downloadtask.downloadStatus = 0;
            refreshUnfinishedDownloadListUI();
            downloadTableHandler.updateSoundRecord(downloadtask);
            downloadPool.execute(downloadtask);
            return;
        }
    }

    public void resumeAllIncomplete()
    {
        if (appContext == null || NetworkUtils.getNetType(appContext) == -1)
        {
            return;
        }
        Object obj;
        DownloadTask downloadtask;
        try
        {
            obj = new ArrayList();
            Iterator iterator = downloadList.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                DownloadTask downloadtask1 = (DownloadTask)iterator.next();
                if (downloadtask1.downloadStatus == 2 || downloadtask1.downloadStatus == 3)
                {
                    ((ArrayList) (obj)).add(downloadtask1);
                }
            } while (true);
        }
        catch (Exception exception)
        {
            return;
        }
        for (obj = ((ArrayList) (obj)).iterator(); ((Iterator) (obj)).hasNext(); downloadPool.execute(downloadtask))
        {
            downloadtask = (DownloadTask)((Iterator) (obj)).next();
            downloadtask.isRunning = true;
            downloadtask.downloadStatus = 0;
        }

        refreshUnfinishedDownloadListUI();
        return;
    }

    public void resumeFromFailed(DownloadTask downloadtask)
    {
        resume(downloadtask);
    }

    public void resumeFromPause(DownloadTask downloadtask)
    {
        resume(downloadtask);
    }

    public void setHandler(Handler handler)
    {
        this;
        JVM INSTR monitorenter ;
        uiHandler = handler;
        this;
        JVM INSTR monitorexit ;
        return;
        handler;
        throw handler;
    }

    public final void sortData(List list)
    {
        synchronized (listLock)
        {
            Collections.sort(list, new h(this));
        }
        return;
        list;
        obj;
        JVM INSTR monitorexit ;
        throw list;
    }

    public void start()
    {
        if (StorageUtils.count <= 0)
        {
            StorageUtils.determineStorageUtils(appContext);
        }
        downloadPool = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, workQueue);
        Logger.log("1122-2", (new StringBuilder()).append("[start]==1===").append(downloadList.size()).toString());
        Object obj = null;
        if (downloadTableHandler == null || downloadTableHandler.getCount() <= 0)
        {
            obj = MyFileUtils.getDownloadSoundFromSD();
            Logger.log("1122-2", (new StringBuilder()).append("[start]==2===[").append(((String) (obj))).append("]").toString());
        }
        if (Utilities.isNotBlank(((String) (obj))))
        {
            try
            {
                obj = JSON.parseArray(((String) (obj)), com/ximalaya/ting/android/model/download/DownloadTask);
                downloadTableHandler.save(((List) (obj)));
                Logger.log("1122-2", (new StringBuilder()).append("[start]==3===[").append(((List) (obj)).size()).append("]").toString());
            }
            catch (Exception exception) { }
        }
        synchronized (listLock)
        {
            downloadList = downloadTableHandler.readDownloadList(downloadPool);
            Logger.log("1122-2", (new StringBuilder()).append("[start]==4===").append(downloadList.size()).toString());
        }
        return;
        exception1;
        obj1;
        JVM INSTR monitorexit ;
        throw exception1;
    }

    public void startNow(DownloadTask downloadtask)
    {
        while (downloadtask == null || downloadtask.trackId <= 0L || downloadtask.downloadStatus != 0 || appContext == null || NetworkUtils.getNetType(appContext) == -1) 
        {
            return;
        }
        debugMonitorWorkQueue("startNow[+]");
        Object obj = new ArrayList();
        ((ArrayList) (obj)).addAll(workQueue);
        workQueue.clear();
        if (currentExcutingTask == null || currentExcutingTask.trackId != downloadtask.trackId)
        {
            downloadPool.execute(downloadtask);
        }
        obj = ((ArrayList) (obj)).iterator();
        do
        {
            if (!((Iterator) (obj)).hasNext())
            {
                break;
            }
            DownloadTask downloadtask1 = (DownloadTask)((Iterator) (obj)).next();
            if (downloadtask1.trackId != downloadtask.trackId)
            {
                downloadPool.execute(downloadtask1);
            }
        } while (true);
        try
        {
            if (currentExcutingTask != null && currentExcutingTask.trackId != downloadtask.trackId)
            {
                pauseDownload(currentExcutingTask);
            }
            Logger.log("2013-08-29", (new StringBuilder()).append("=<excute>=, workQueueSize: ").append(workQueue.size()).toString());
        }
        // Misplaced declaration of an exception variable
        catch (DownloadTask downloadtask)
        {
            Logger.log("DownloadHandler", downloadtask.getMessage());
        }
        while (true) 
        {
            debugMonitorWorkQueue("startNow[-]");
            return;
        }
    }

    public void stop()
    {
        cancelAll();
    }

    public void updateFavorited(long l, boolean flag, boolean flag1)
    {
        if (downloadTableHandler == null)
        {
            downloadTableHandler = new DownloadTableHandler(appContext);
        }
        downloadTableHandler.updateFavoritedInDB(l, flag);
        if (flag1)
        {
            updateFavoritedInfo(l, flag);
        }
    }

}
