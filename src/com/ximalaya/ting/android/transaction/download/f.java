// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.download;

import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.Logger;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.transaction.download:
//            AlbumDownload

class f extends a
{

    final AlbumDownload.b a;

    f(AlbumDownload.b b)
    {
        a = b;
        super();
    }

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, AlbumDownload.access$500(a.h));
    }

    public void onNetError(int i, String s)
    {
        AlbumDownload.access$300(a.h, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF");
    }

    public void onSuccess(String s)
    {
        if (!AlbumDownload.access$400(a.h))
        {
            return;
        }
        Logger.d("AlbumDownload", (new StringBuilder()).append("\u83B7\u53D6\u79EF\u5206\u4FE1\u606F\u7B49\uFF1A").append(s).toString());
        try
        {
            s = JSONObject.parseObject(s);
            if (s.getIntValue("ret") == 0)
            {
                int i = s.getIntValue("userPoint");
                AlbumDownload.access$100(a.h).setScore(i);
                a.g = s.getIntValue("restTimes");
                return;
            }
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
        AlbumDownload.access$300(a.h, s.getString("msg"));
        return;
    }
}
