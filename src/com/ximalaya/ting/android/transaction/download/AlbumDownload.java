// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.download;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.share.BaseShareDialog;
import com.ximalaya.ting.android.activity.share.ShareWxTask;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.album.AlbumSectionDownloadFragment;
import com.ximalaya.ting.android.fragment.userspace.MyScoreFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.modelmanage.ScoreManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.transaction.download:
//            c, a, b, e, 
//            d, f

public class AlbumDownload
{
    public static interface FinishCallback
    {

        public abstract void onFinish();
    }

    class a extends Dialog
        implements android.view.View.OnClickListener
    {

        Activity a;
        TextView b;
        TextView c;
        Button d;
        Button e;
        ImageButton f;
        int g;
        final AlbumDownload h;

        private void a()
        {
            if (g > 0)
            {
                (new DialogBuilder(a)).setTitle("\u6B63\u5728\u4E3A\u4F60\u4E0B\u8F7D\u4E13\u8F91").setMessage((new StringBuilder()).append("\u6E29\u99A8\u63D0\u793A\uFF1A\u5206\u4EAB\u5230\u670B\u53CB\u5708\u80FD\u8FD4\u56DE").append(h.downloadNeedScore / 2).append("\u79EF\u5206\u54E6").toString()).setOkBtn("\u5206\u4EAB\u8FD4\u79EF\u5206", new e(this)).setCancelBtn("\u4E0B\u6B21\u518D\u8BF4", new d(this)).showConfirm();
            }
        }

        public void onClick(View view)
        {
            view.getId();
            JVM INSTR tableswitch 2131362106 2131362109: default 36
        //                       2131362106 41
        //                       2131362107 55
        //                       2131362108 36
        //                       2131362109 36;
               goto _L1 _L2 _L3 _L1 _L1
_L1:
            dismiss();
            return;
_L2:
            h.goDownload();
            a();
            continue; /* Loop/switch isn't completed */
_L3:
            (new BaseShareDialog(a, h.mAlbumModel, view)).show();
            if (true) goto _L1; else goto _L4
_L4:
        }

        protected void onCreate(Bundle bundle)
        {
            super.onCreate(bundle);
            requestWindowFeature(1);
            setContentView(0x7f030038);
            b = (TextView)findViewById(0x7f0a0136);
            c = (TextView)findViewById(0x7f0a0138);
            b.setText((new StringBuilder()).append(h.userScore).append("").toString());
            c.setText((new StringBuilder()).append(h.downloadNeedScore).append("").toString());
            d = (Button)findViewById(0x7f0a013a);
            e = (Button)findViewById(0x7f0a013b);
            f = (ImageButton)findViewById(0x7f0a013d);
            d.setOnClickListener(this);
            e.setOnClickListener(this);
            f.setOnClickListener(this);
        }

        a(Activity activity, int i)
        {
            h = AlbumDownload.this;
            super(activity, 0x7f0b0027);
            a = activity;
            g = i;
        }
    }

    class b extends Dialog
        implements android.view.View.OnClickListener
    {

        Activity a;
        TextView b;
        TextView c;
        Button d;
        Button e;
        ImageButton f;
        int g;
        final AlbumDownload h;

        private void a()
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("behavior", 4);
            com.ximalaya.ting.android.b.f.a().a("mobile/api1/point/query/deduct/rest", requestparams, DataCollectUtil.getDataFromView(h.mDownloadBtn), new com.ximalaya.ting.android.transaction.download.f(this));
        }

        public void onClick(View view)
        {
            view.getId();
            JVM INSTR lookupswitch 3: default 40
        //                       2131362109: 40
        //                       2131362114: 45
        //                       2131362115: 96;
               goto _L1 _L1 _L2 _L3
_L1:
            dismiss();
_L5:
            return;
_L2:
            if (!h.isFragmentAdded() || a == null) goto _L5; else goto _L4
_L4:
            Bundle bundle = new Bundle();
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            h.mFragment.startFragment(com/ximalaya/ting/android/fragment/userspace/MyScoreFragment, bundle);
            continue; /* Loop/switch isn't completed */
_L3:
            if (!h.isFragmentAdded() || a == null) goto _L5; else goto _L6
_L6:
            if (g > 0)
            {
                (new ShareWxTask(a)).sendWXShare(false);
            } else
            {
                Bundle bundle1 = new Bundle();
                bundle1.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                h.mFragment.startFragment(com/ximalaya/ting/android/fragment/userspace/MyScoreFragment, bundle1);
            }
            if (true) goto _L1; else goto _L7
_L7:
        }

        protected void onCreate(Bundle bundle)
        {
            super.onCreate(bundle);
            requestWindowFeature(1);
            setContentView(0x7f030039);
            b = (TextView)findViewById(0x7f0a0141);
            c = (TextView)findViewById(0x7f0a0138);
            b.setText((new StringBuilder()).append(h.downloadNeedScore - h.userScore).append("").toString());
            c.setText((new StringBuilder()).append(h.downloadNeedScore).append("").toString());
            d = (Button)findViewById(0x7f0a0142);
            e = (Button)findViewById(0x7f0a0143);
            f = (ImageButton)findViewById(0x7f0a013d);
            d.setOnClickListener(this);
            e.setOnClickListener(this);
            f.setOnClickListener(this);
            a();
        }

        b(Activity activity)
        {
            h = AlbumDownload.this;
            super(activity, 0x7f0b0027);
            g = 0;
            a = activity;
        }
    }


    private static final String TAG = "AlbumDownload";
    private int downloadNeedScore;
    private Activity mActivity;
    private AlbumModel mAlbumModel;
    private FinishCallback mCallback;
    private View mDownloadBtn;
    private BaseFragment mFragment;
    private ScoreManage mScoreManage;
    private int userScore;

    public AlbumDownload(Activity activity, BaseFragment basefragment, AlbumModel albummodel, FinishCallback finishcallback, View view)
    {
        downloadNeedScore = 0;
        userScore = 0;
        mActivity = activity;
        mAlbumModel = albummodel;
        mScoreManage = ScoreManage.getInstance(mActivity.getApplicationContext());
        mCallback = finishcallback;
        mDownloadBtn = view;
        mFragment = basefragment;
    }

    private void goDownload()
    {
        ToolUtil.onEvent(mActivity.getApplicationContext(), "Album_DownOneAlbum");
        c c1 = new c(this);
        DownLoadTools downloadtools = DownLoadTools.getInstance();
        downloadtools.goDownloadAlbum(mAlbumModel, c1, null);
        downloadtools.release();
    }

    private boolean isFragmentAdded()
    {
        return mFragment != null && mFragment.isAdded();
    }

    private void onGetScoreFinish(int i, int j, int k)
    {
        if (k <= 0)
        {
            if (mAlbumModel != null)
            {
                AlbumSectionDownloadFragment albumsectiondownloadfragment = AlbumSectionDownloadFragment.newInstance(mAlbumModel);
                albumsectiondownloadfragment.setArguments(albumsectiondownloadfragment.getArguments());
                mFragment.startFragment(albumsectiondownloadfragment);
            }
            return;
        }
        downloadNeedScore = k;
        userScore = i;
        if (i >= k)
        {
            (new a(mActivity, j)).show();
            return;
        } else
        {
            (new b(mActivity)).show();
            showToast("\u4EB2\uFF0C\u60A8\u7684\u79EF\u5206\u4E0D\u591F\u54E6~\u53BB\u8D5A\u70B9\u79EF\u5206\u5427");
            return;
        }
    }

    private void showToast(String s)
    {
        if (mActivity == null)
        {
            return;
        } else
        {
            Toast.makeText(mActivity, s, 0).show();
            return;
        }
    }

    public void batchDownload()
    {
        if (!UserInfoMannage.hasLogined())
        {
            (new DialogBuilder(mActivity)).setMessage("\u6279\u91CF\u4E0B\u8F7D\u529F\u80FD\u4EC5\u767B\u5F55\u7528\u6237\u624D\u80FD\u4F7F\u7528\u54E6\uFF01").setCancelBtn("\u7A0D\u540E\u518D\u8BF4").setOkBtn("\u53BB\u767B\u5F55", new com.ximalaya.ting.android.transaction.download.a(this)).showConfirm();
            return;
        }
        mScoreManage = ScoreManage.getInstance(mActivity.getApplicationContext());
        if (mScoreManage == null)
        {
            showToast("\u4E0D\u80FD\u83B7\u53D6\u79EF\u5206\u4FE1\u606F");
            return;
        } else
        {
            mDownloadBtn.setClickable(false);
            RequestParams requestparams = new RequestParams();
            requestparams.put("behavior", 1);
            requestparams.put("contentType", "album");
            requestparams.put("contentId", (new StringBuilder()).append(mAlbumModel.albumId).append("").toString());
            f.a().a("mobile/api1/point/query/deduct/rest", requestparams, null, new com.ximalaya.ting.android.transaction.download.b(this));
            return;
        }
    }












}
