// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.download;

import android.content.Context;
import android.widget.Toast;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.HistoryManage;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.PlaylistFromDownload;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.transaction.download:
//            j

public class DownloadInfoManager
{

    private static DownloadInfoManager manage;
    private final long NO_ALBUM = 0L;
    private HashMap albumMap;
    private Context mContext;
    private List mapKey;
    private List taskList;

    private DownloadInfoManager(Context context)
    {
        taskList = null;
        mContext = context;
    }

    public static DownloadInfoManager getInstance(Context context)
    {
        if (manage != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/transaction/download/DownloadInfoManager;
        JVM INSTR monitorenter ;
        manage = new DownloadInfoManager(context.getApplicationContext());
        com/ximalaya/ting/android/transaction/download/DownloadInfoManager;
        JVM INSTR monitorexit ;
_L2:
        return manage;
        context;
        com/ximalaya/ting/android/transaction/download/DownloadInfoManager;
        JVM INSTR monitorexit ;
        throw context;
    }

    private void initAlbumList()
    {
        if (taskList != null)
        {
            albumMap = new HashMap();
            mapKey = new ArrayList();
            for (Iterator iterator = taskList.iterator(); iterator.hasNext();)
            {
                DownloadTask downloadtask = (DownloadTask)iterator.next();
                long l;
                if (downloadtask.albumId != 0L)
                {
                    l = downloadtask.albumId;
                } else
                {
                    l = 0L;
                }
                if (albumMap.containsKey(Long.valueOf(l)))
                {
                    ((List)albumMap.get(Long.valueOf(l))).add(downloadtask);
                } else
                if (l != 0L)
                {
                    ArrayList arraylist = new ArrayList();
                    arraylist.add(downloadtask);
                    albumMap.put(Long.valueOf(l), arraylist);
                    mapKey.add(Long.valueOf(l));
                } else
                {
                    ArrayList arraylist1 = new ArrayList();
                    arraylist1.add(downloadtask);
                    mapKey.add(Long.valueOf(l));
                    albumMap.put(Long.valueOf(0L), arraylist1);
                }
            }

        }
    }

    public void doGetDownloadList()
    {
        (new j(this)).execute(new Void[0]);
    }

    public HashMap getAlbumMap()
    {
        return albumMap;
    }

    public List getMapKey()
    {
        return mapKey;
    }

    public long getNextAlbumIndex(boolean flag)
    {
        if (mapKey == null || mapKey.size() == 0)
        {
            return -1L;
        }
        long l = PlayListControl.getPlayListManager().getCurSound().albumId;
        int i = mapKey.indexOf(Long.valueOf(l));
        if (i < 0)
        {
            return 0L;
        }
        int k;
        if (flag)
        {
            i++;
        } else
        {
            i--;
        }
        k = i;
        if (i < 0)
        {
            k = i + mapKey.size();
        }
        return ((Long)mapKey.get(k % mapKey.size())).longValue();
    }

    public void setAlbumMap(HashMap hashmap)
    {
        albumMap = hashmap;
    }

    public void setMapKey(List list)
    {
        mapKey = list;
    }

    public boolean toPlayNextAlbum(boolean flag)
    {
        Object obj = TingMediaPlayer.getTingMediaPlayer(mContext).getmCurrentAudioSrc();
        if (obj == null || ((String) (obj)).contains("http"))
        {
            return false;
        }
        long l = getNextAlbumIndex(flag);
        if (l < 0L)
        {
            return false;
        }
        obj = HistoryManage.getInstance(mContext).getSoundInfoList();
        if (obj == null || ((List) (obj)).size() == 0)
        {
            return false;
        }
        for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext();)
        {
            SoundInfo soundinfo = (SoundInfo)((Iterator) (obj)).next();
            if (soundinfo.albumId == l)
            {
                Toast.makeText(mContext, "\u5207\u6362\u4E13\u8F91\uFF01", 1).show();
                PlayTools.gotoPlay(10, soundinfo, mContext, null);
                return true;
            }
        }

        if (albumMap == null)
        {
            return false;
        }
        obj = (List)albumMap.get(Long.valueOf(l));
        if (obj == null || ((List) (obj)).size() == 0)
        {
            return false;
        }
        obj = ModelHelper.downloadlistToPlayList((DownloadTask)((List) (obj)).get(0), ((List) (obj)));
        if (((PlaylistFromDownload) (obj)).index >= 0)
        {
            PlayTools.gotoPlayLocals(2, ((PlaylistFromDownload) (obj)).soundsList, ((PlaylistFromDownload) (obj)).index, mContext, null);
            return true;
        } else
        {
            return false;
        }
    }




/*
    static List access$102(DownloadInfoManager downloadinfomanager, List list)
    {
        downloadinfomanager.taskList = list;
        return list;
    }

*/

}
