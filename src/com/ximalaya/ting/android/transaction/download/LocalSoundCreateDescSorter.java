// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.transaction.download;

import com.ximalaya.ting.android.model.download.DownloadTask;
import java.util.Comparator;

public class LocalSoundCreateDescSorter
    implements Comparator
{

    public LocalSoundCreateDescSorter()
    {
    }

    public int compare(DownloadTask downloadtask, DownloadTask downloadtask1)
    {
        if (downloadtask.create_at > downloadtask1.create_at)
        {
            return 1;
        }
        return downloadtask.create_at >= downloadtask1.create_at ? 0 : -1;
    }

    public volatile int compare(Object obj, Object obj1)
    {
        return compare((DownloadTask)obj, (DownloadTask)obj1);
    }
}
