// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android;


// Referenced classes of package com.ximalaya.ting.android:
//            R

public static final class 
{

    public static final int CirclePageIndicator[] = {
        0x10100c4, 0x10100d4, 0x7f01008a, 0x7f01008c, 0x7f01008e, 0x7f01008f, 0x7f010090, 0x7f010091, 0x7f010092, 0x7f010093
    };
    public static final int CirclePageIndicator_android_background = 1;
    public static final int CirclePageIndicator_android_orientation = 0;
    public static final int CirclePageIndicator_centered = 2;
    public static final int CirclePageIndicator_extraSpacing = 9;
    public static final int CirclePageIndicator_fillColor = 4;
    public static final int CirclePageIndicator_pageColor = 5;
    public static final int CirclePageIndicator_radius = 6;
    public static final int CirclePageIndicator_snap = 7;
    public static final int CirclePageIndicator_strokeColor = 8;
    public static final int CirclePageIndicator_strokeWidth = 3;
    public static final int CircleProgressBar[] = {
        0x7f010016, 0x7f010017, 0x7f010018, 0x7f010019, 0x7f01001a, 0x7f01001b, 0x7f01001c, 0x7f01001d, 0x7f01005b, 0x7f01005c, 
        0x7f01005d, 0x7f01005e, 0x7f01005f, 0x7f010060, 0x7f010061, 0x7f010062, 0x7f010063
    };
    public static final int CircleProgressBar_CPBmax = 13;
    public static final int CircleProgressBar_CPBmin = 14;
    public static final int CircleProgressBar_CPBroundColor = 8;
    public static final int CircleProgressBar_CPBroundProgressColor = 9;
    public static final int CircleProgressBar_CPBroundWidth = 10;
    public static final int CircleProgressBar_CPBstyle = 16;
    public static final int CircleProgressBar_CPBtextColor = 11;
    public static final int CircleProgressBar_CPBtextIsDisplayable = 15;
    public static final int CircleProgressBar_CPBtextSize = 12;
    public static final int CircleProgressBar_max = 5;
    public static final int CircleProgressBar_progressStyle = 7;
    public static final int CircleProgressBar_roundColor = 0;
    public static final int CircleProgressBar_roundProgressColor = 1;
    public static final int CircleProgressBar_roundWidth = 2;
    public static final int CircleProgressBar_textColor = 3;
    public static final int CircleProgressBar_textIsDisplayable = 6;
    public static final int CircleProgressBar_textSize = 4;
    public static final int DragSortListView[] = {
        0x7f010025, 0x7f010026, 0x7f010027, 0x7f010028, 0x7f010029, 0x7f01002a, 0x7f01002b, 0x7f01002c, 0x7f01002d, 0x7f01002e, 
        0x7f01002f, 0x7f010030, 0x7f010031, 0x7f010032, 0x7f010033, 0x7f010034, 0x7f010035, 0x7f010036
    };
    public static final int DragSortListView_click_remove_id = 16;
    public static final int DragSortListView_collapsed_height = 0;
    public static final int DragSortListView_drag_enabled = 10;
    public static final int DragSortListView_drag_handle_id = 14;
    public static final int DragSortListView_drag_scroll_start = 1;
    public static final int DragSortListView_drag_start_mode = 13;
    public static final int DragSortListView_drop_animation_duration = 9;
    public static final int DragSortListView_fling_handle_id = 15;
    public static final int DragSortListView_float_alpha = 6;
    public static final int DragSortListView_float_background_color = 3;
    public static final int DragSortListView_max_drag_scroll_speed = 2;
    public static final int DragSortListView_remove_animation_duration = 8;
    public static final int DragSortListView_remove_enabled = 12;
    public static final int DragSortListView_remove_mode = 4;
    public static final int DragSortListView_slide_shuffle_speed = 7;
    public static final int DragSortListView_sort_enabled = 11;
    public static final int DragSortListView_track_drag_sort = 5;
    public static final int DragSortListView_use_default_controller = 17;
    public static final int EmotionSelector[] = {
        0x7f010039, 0x7f01003a, 0x7f01003b, 0x7f01003c, 0x7f01003d
    };
    public static final int EmotionSelector_emotion_icon = 3;
    public static final int EmotionSelector_emotion_name = 4;
    public static final int EmotionSelector_max_char = 2;
    public static final int EmotionSelector_max_line = 1;
    public static final int EmotionSelector_show_emotion_bar = 0;
    public static final int FlowLayout[] = {
        0x10100af, 0x10100c4, 0x7f01001e, 0x7f01001f, 0x7f010020
    };
    public static final int FlowLayout_LayoutParams[] = {
        0x10100b3, 0x7f010021, 0x7f010022
    };
    public static final int FlowLayout_LayoutParams_android_layout_gravity = 0;
    public static final int FlowLayout_LayoutParams_layout_newLine = 1;
    public static final int FlowLayout_LayoutParams_layout_weight = 2;
    public static final int FlowLayout_android_gravity = 0;
    public static final int FlowLayout_android_orientation = 1;
    public static final int FlowLayout_debugDraw = 3;
    public static final int FlowLayout_layoutDirection = 2;
    public static final int FlowLayout_weightDefault = 4;
    public static final int JazzyViewPager[] = {
        0x7f010044, 0x7f010045, 0x7f010046, 0x7f010047
    };
    public static final int JazzyViewPager_fadeEnabled = 1;
    public static final int JazzyViewPager_outlineColor = 3;
    public static final int JazzyViewPager_outlineEnabled = 2;
    public static final int JazzyViewPager_style = 0;
    public static final int LinePageIndicator[] = {
        0x10100d4, 0x7f01008a, 0x7f01008b, 0x7f01008c, 0x7f01008d, 0x7f010094, 0x7f010095
    };
    public static final int LinePageIndicator_android_background = 0;
    public static final int LinePageIndicator_centered = 1;
    public static final int LinePageIndicator_gapWidth = 6;
    public static final int LinePageIndicator_lineWidth = 5;
    public static final int LinePageIndicator_selectedColor = 2;
    public static final int LinePageIndicator_strokeWidth = 3;
    public static final int LinePageIndicator_unselectedColor = 4;
    public static final int PagerSlidingTabStrip[] = {
        0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003, 0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007, 0x7f010008, 0x7f010009, 
        0x7f01000a, 0x7f01000b, 0x7f01000c, 0x7f01000d
    };
    public static final int PagerSlidingTabStrip_pstsActivateTextColor = 12;
    public static final int PagerSlidingTabStrip_pstsDeactivateTextColor = 13;
    public static final int PagerSlidingTabStrip_pstsDividerColor = 2;
    public static final int PagerSlidingTabStrip_pstsDividerPadding = 5;
    public static final int PagerSlidingTabStrip_pstsIndicatorColor = 0;
    public static final int PagerSlidingTabStrip_pstsIndicatorHeight = 3;
    public static final int PagerSlidingTabStrip_pstsScrollOffset = 7;
    public static final int PagerSlidingTabStrip_pstsShouldExpand = 9;
    public static final int PagerSlidingTabStrip_pstsTabBackground = 8;
    public static final int PagerSlidingTabStrip_pstsTabPaddingLeftRight = 6;
    public static final int PagerSlidingTabStrip_pstsTabSwitch = 11;
    public static final int PagerSlidingTabStrip_pstsTextAllCaps = 10;
    public static final int PagerSlidingTabStrip_pstsUnderlineColor = 1;
    public static final int PagerSlidingTabStrip_pstsUnderlineHeight = 4;
    public static final int PullToRefresh[] = {
        0x7f010048, 0x7f010049, 0x7f01004a, 0x7f01004b, 0x7f01004c, 0x7f01004d, 0x7f01004e, 0x7f01004f, 0x7f010050, 0x7f010051, 
        0x7f010052, 0x7f010053, 0x7f010054, 0x7f010055, 0x7f010056, 0x7f010057, 0x7f010058, 0x7f010059, 0x7f01005a
    };
    public static final int PullToRefresh_ptrAdapterViewBackground = 16;
    public static final int PullToRefresh_ptrAnimationStyle = 12;
    public static final int PullToRefresh_ptrDrawable = 6;
    public static final int PullToRefresh_ptrDrawableBottom = 18;
    public static final int PullToRefresh_ptrDrawableEnd = 8;
    public static final int PullToRefresh_ptrDrawableStart = 7;
    public static final int PullToRefresh_ptrDrawableTop = 17;
    public static final int PullToRefresh_ptrHeaderBackground = 1;
    public static final int PullToRefresh_ptrHeaderSubTextColor = 3;
    public static final int PullToRefresh_ptrHeaderTextAppearance = 10;
    public static final int PullToRefresh_ptrHeaderTextColor = 2;
    public static final int PullToRefresh_ptrListViewExtrasEnabled = 14;
    public static final int PullToRefresh_ptrMode = 4;
    public static final int PullToRefresh_ptrOverScroll = 9;
    public static final int PullToRefresh_ptrRefreshableViewBackground = 0;
    public static final int PullToRefresh_ptrRotateDrawableWhilePulling = 15;
    public static final int PullToRefresh_ptrScrollingWhileRefreshingEnabled = 13;
    public static final int PullToRefresh_ptrShowIndicator = 5;
    public static final int PullToRefresh_ptrSubHeaderTextAppearance = 11;
    public static final int RoundedImageView[] = {
        0x101011d, 0x7f01003e, 0x7f01003f, 0x7f010040, 0x7f010041, 0x7f010042, 0x7f010043
    };
    public static final int RoundedImageView_android_scaleType = 0;
    public static final int RoundedImageView_border_bg_color = 4;
    public static final int RoundedImageView_border_color = 3;
    public static final int RoundedImageView_border_width = 2;
    public static final int RoundedImageView_corner_radius = 1;
    public static final int RoundedImageView_pressdown_shade = 6;
    public static final int RoundedImageView_round_background = 5;
    public static final int StickyListHeadersListView[] = {
        0x101007f, 0x10100d5, 0x10100d6, 0x10100d7, 0x10100d8, 0x10100d9, 0x10100e0, 0x10100eb, 0x10100fb, 0x10100fc, 
        0x10100fe, 0x1010101, 0x1010129, 0x101012a, 0x101012b, 0x1010226, 0x1010335, 0x10103a5, 0x7f010037, 0x7f010038
    };
    public static final int StickyListHeadersListView_android_cacheColorHint = 11;
    public static final int StickyListHeadersListView_android_choiceMode = 14;
    public static final int StickyListHeadersListView_android_clipToPadding = 7;
    public static final int StickyListHeadersListView_android_divider = 12;
    public static final int StickyListHeadersListView_android_dividerHeight = 13;
    public static final int StickyListHeadersListView_android_drawSelectorOnTop = 9;
    public static final int StickyListHeadersListView_android_fadingEdgeLength = 6;
    public static final int StickyListHeadersListView_android_fastScrollAlwaysVisible = 16;
    public static final int StickyListHeadersListView_android_fastScrollEnabled = 15;
    public static final int StickyListHeadersListView_android_listSelector = 8;
    public static final int StickyListHeadersListView_android_padding = 1;
    public static final int StickyListHeadersListView_android_paddingBottom = 5;
    public static final int StickyListHeadersListView_android_paddingLeft = 2;
    public static final int StickyListHeadersListView_android_paddingRight = 4;
    public static final int StickyListHeadersListView_android_paddingTop = 3;
    public static final int StickyListHeadersListView_android_requiresFadingEdge = 17;
    public static final int StickyListHeadersListView_android_scrollbarStyle = 0;
    public static final int StickyListHeadersListView_android_scrollingCache = 10;
    public static final int StickyListHeadersListView_hasStickyHeaders = 18;
    public static final int StickyListHeadersListView_isDrawingListUnderStickyHeader = 19;
    public static final int StickyScrollView[] = {
        0x7f010023, 0x7f010024
    };
    public static final int StickyScrollView_stuckShadowDrawable = 1;
    public static final int StickyScrollView_stuckShadowHeight = 0;
    public static final int SwipeListView[] = {
        0x7f010064, 0x7f010065, 0x7f010066, 0x7f010067, 0x7f010068, 0x7f010069, 0x7f01006a, 0x7f01006b, 0x7f01006c, 0x7f01006d, 
        0x7f01006e, 0x7f01006f
    };
    public static final int SwipeListView_swipeActionLeft = 8;
    public static final int SwipeListView_swipeActionRight = 9;
    public static final int SwipeListView_swipeAnimationTime = 1;
    public static final int SwipeListView_swipeBackView = 6;
    public static final int SwipeListView_swipeCloseAllItemsWhenMoveList = 4;
    public static final int SwipeListView_swipeDrawableChecked = 10;
    public static final int SwipeListView_swipeDrawableUnchecked = 11;
    public static final int SwipeListView_swipeFrontView = 5;
    public static final int SwipeListView_swipeMode = 7;
    public static final int SwipeListView_swipeOffsetLeft = 2;
    public static final int SwipeListView_swipeOffsetRight = 3;
    public static final int SwipeListView_swipeOpenOnLongPress = 0;
    public static final int TaoPullToRefresh[] = {
        0x7f010070, 0x7f010071, 0x7f010072, 0x7f010073, 0x7f010074, 0x7f010075, 0x7f010076, 0x7f010077, 0x7f010078, 0x7f010079, 
        0x7f01007a, 0x7f01007b, 0x7f01007c, 0x7f01007d, 0x7f01007e, 0x7f01007f, 0x7f010080, 0x7f010081, 0x7f010082
    };
    public static final int TaoPullToRefresh_taoAdapterViewBackground = 16;
    public static final int TaoPullToRefresh_taoAnimationStyle = 12;
    public static final int TaoPullToRefresh_taoDrawable = 6;
    public static final int TaoPullToRefresh_taoDrawableBottom = 18;
    public static final int TaoPullToRefresh_taoDrawableEnd = 8;
    public static final int TaoPullToRefresh_taoDrawableStart = 7;
    public static final int TaoPullToRefresh_taoDrawableTop = 17;
    public static final int TaoPullToRefresh_taoHeaderBackground = 1;
    public static final int TaoPullToRefresh_taoHeaderSubTextColor = 3;
    public static final int TaoPullToRefresh_taoHeaderTextAppearance = 10;
    public static final int TaoPullToRefresh_taoHeaderTextColor = 2;
    public static final int TaoPullToRefresh_taoListViewExtrasEnabled = 14;
    public static final int TaoPullToRefresh_taoMode = 4;
    public static final int TaoPullToRefresh_taoOverScroll = 9;
    public static final int TaoPullToRefresh_taoRefreshableViewBackground = 0;
    public static final int TaoPullToRefresh_taoRotateDrawableWhilePulling = 15;
    public static final int TaoPullToRefresh_taoScrollingWhileRefreshingEnabled = 13;
    public static final int TaoPullToRefresh_taoShowIndicator = 5;
    public static final int TaoPullToRefresh_taoSubHeaderTextAppearance = 11;
    public static final int TitlePageIndicator[] = {
        0x1010095, 0x1010098, 0x10100d4, 0x7f01008b, 0x7f010096, 0x7f010097, 0x7f010098, 0x7f010099, 0x7f01009a, 0x7f01009b, 
        0x7f01009c, 0x7f01009d, 0x7f01009e, 0x7f01009f, 0x7f0100a0
    };
    public static final int TitlePageIndicator_android_background = 2;
    public static final int TitlePageIndicator_android_textColor = 1;
    public static final int TitlePageIndicator_android_textSize = 0;
    public static final int TitlePageIndicator_clipPadding = 4;
    public static final int TitlePageIndicator_footerColor = 5;
    public static final int TitlePageIndicator_footerIndicatorHeight = 8;
    public static final int TitlePageIndicator_footerIndicatorStyle = 7;
    public static final int TitlePageIndicator_footerIndicatorUnderlinePadding = 9;
    public static final int TitlePageIndicator_footerLineHeight = 6;
    public static final int TitlePageIndicator_footerPadding = 10;
    public static final int TitlePageIndicator_linePosition = 11;
    public static final int TitlePageIndicator_selectedBold = 12;
    public static final int TitlePageIndicator_selectedColor = 3;
    public static final int TitlePageIndicator_titlePadding = 13;
    public static final int TitlePageIndicator_topPadding = 14;
    public static final int UnderlinePageIndicator[] = {
        0x10100d4, 0x7f01008b, 0x7f0100a1, 0x7f0100a2, 0x7f0100a3
    };
    public static final int UnderlinePageIndicator_android_background = 0;
    public static final int UnderlinePageIndicator_fadeDelay = 3;
    public static final int UnderlinePageIndicator_fadeLength = 4;
    public static final int UnderlinePageIndicator_fades = 2;
    public static final int UnderlinePageIndicator_selectedColor = 1;
    public static final int VerticalSeekBar[] = {
        0x7f01000e
    };
    public static final int VerticalSeekBar_seekBarRotation = 0;
    public static final int ViewPagerIndicator[] = {
        0x7f010083, 0x7f010084, 0x7f010085, 0x7f010086, 0x7f010087, 0x7f010088, 0x7f010089
    };
    public static final int ViewPagerIndicator_vpiCirclePageIndicatorStyle = 1;
    public static final int ViewPagerIndicator_vpiIconPageIndicatorStyle = 2;
    public static final int ViewPagerIndicator_vpiLinePageIndicatorStyle = 3;
    public static final int ViewPagerIndicator_vpiTabPageIndicatorStyle = 5;
    public static final int ViewPagerIndicator_vpiTitlePageIndicatorStyle = 4;
    public static final int ViewPagerIndicator_vpiUnderlinePageIndicatorStyle = 6;
    public static final int ViewPagerIndicator_wallTabPageIndicatorStyle = 0;
    public static final int WaveformView[] = {
        0x7f01000f, 0x7f010010, 0x7f010011, 0x7f010012, 0x7f010013, 0x7f010014, 0x7f010015
    };
    public static final int WaveformView_backgroundColor = 5;
    public static final int WaveformView_backgroundSoundColor = 3;
    public static final int WaveformView_barPadding = 1;
    public static final int WaveformView_barWidth = 0;
    public static final int WaveformView_cutterDrawable = 6;
    public static final int WaveformView_playedColor = 4;
    public static final int WaveformView_recordingColor = 2;


    public ()
    {
    }
}
