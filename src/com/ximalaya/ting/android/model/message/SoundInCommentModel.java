// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.message;

import com.ximalaya.ting.android.model.BaseModel;

public class SoundInCommentModel extends BaseModel
{

    public Long commentId;
    public String pCommentContent;
    public Long pCommentId;
    public Double second;
    public String trackCoverPath;
    public Long trackId;
    public String trackNickname;
    public String trackTitle;
    public Long trackUid;

    public SoundInCommentModel()
    {
    }

    public Long getCommentId()
    {
        return commentId;
    }

    public String getPcommentContent()
    {
        return pCommentContent;
    }

    public Long getPcommentId()
    {
        return pCommentId;
    }

    public Double getSecond()
    {
        return second;
    }

    public String getTrackCoverPath()
    {
        return trackCoverPath;
    }

    public Long getTrackId()
    {
        return trackId;
    }

    public String getTrackNickname()
    {
        return trackNickname;
    }

    public String getTrackTitle()
    {
        return trackTitle;
    }

    public Long getTrackUid()
    {
        return trackUid;
    }

    public void setCommentId(Long long1)
    {
        commentId = long1;
    }

    public void setPcommentContent(String s)
    {
        pCommentContent = s;
    }

    public void setPcommentId(Long long1)
    {
        pCommentId = long1;
    }

    public void setSecond(Double double1)
    {
        second = double1;
    }

    public void setTrackCoverPath(String s)
    {
        trackCoverPath = s;
    }

    public void setTrackId(Long long1)
    {
        trackId = long1;
    }

    public void setTrackNickname(String s)
    {
        trackNickname = s;
    }

    public void setTrackTitle(String s)
    {
        trackTitle = s;
    }

    public void setTrackUid(Long long1)
    {
        trackUid = long1;
    }
}
