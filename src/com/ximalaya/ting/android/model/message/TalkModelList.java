// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.message;

import com.ximalaya.ting.android.model.BaseListModel;
import java.util.List;

public class TalkModelList extends BaseListModel
{

    public long currentTime;
    public boolean isDown;
    public List list;
    public long toUid;

    public TalkModelList()
    {
    }

    public TalkModelList(long l)
    {
        toUid = l;
    }

    public boolean equals(Object obj)
    {
        if (obj != null)
        {
            if (this == obj)
            {
                return true;
            }
            if ((obj instanceof TalkModelList) && ((TalkModelList)obj).toUid == toUid)
            {
                return true;
            }
        }
        return false;
    }
}
