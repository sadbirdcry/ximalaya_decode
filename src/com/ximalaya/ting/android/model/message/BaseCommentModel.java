// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.message;

import com.ximalaya.ting.android.model.BaseModel;

// Referenced classes of package com.ximalaya.ting.android.model.message:
//            SoundInCommentModel

public class BaseCommentModel extends BaseModel
{

    public String avatarPath;
    public String content;
    public Long createdAt;
    public SoundInCommentModel extra;
    public boolean flag;
    public Boolean hasRead;
    public Long id;
    public Integer messageType;
    public String nickname;
    public String time;
    public String toAvatarPath;
    public String toNickname;
    public Long toUid;
    public Long uid;
    public Long updatedAt;

    public BaseCommentModel()
    {
        flag = true;
    }

    public String getAvatarPath()
    {
        return avatarPath;
    }

    public String getContent()
    {
        return content;
    }

    public Long getCreatedAt()
    {
        return createdAt;
    }

    public Boolean getHasRead()
    {
        return hasRead;
    }

    public Long getId()
    {
        return id;
    }

    public Integer getMessageType()
    {
        return messageType;
    }

    public String getNickname()
    {
        return nickname;
    }

    public SoundInCommentModel getSicm()
    {
        return extra;
    }

    public String getTime()
    {
        return time;
    }

    public String getToAvatarPath()
    {
        return toAvatarPath;
    }

    public String getToNickname()
    {
        return toNickname;
    }

    public Long getToUid()
    {
        return toUid;
    }

    public Long getUid()
    {
        return uid;
    }

    public Long getUpdatedAt()
    {
        return updatedAt;
    }

    public boolean isFlag()
    {
        return flag;
    }

    public void setAvatarPath(String s)
    {
        avatarPath = s;
    }

    public void setContent(String s)
    {
        content = s;
    }

    public void setCreatedAt(Long long1)
    {
        createdAt = long1;
    }

    public void setFlag(boolean flag1)
    {
        flag = flag1;
    }

    public void setHasRead(Boolean boolean1)
    {
        hasRead = boolean1;
    }

    public void setId(Long long1)
    {
        id = long1;
    }

    public void setMessageType(Integer integer)
    {
        messageType = integer;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setSicm(SoundInCommentModel soundincommentmodel)
    {
        extra = soundincommentmodel;
    }

    public void setTime(String s)
    {
        time = s;
    }

    public void setToAvatarPath(String s)
    {
        toAvatarPath = s;
    }

    public void setToNickname(String s)
    {
        toNickname = s;
    }

    public void setToUid(Long long1)
    {
        toUid = long1;
    }

    public void setUid(Long long1)
    {
        uid = long1;
    }

    public void setUpdatedAt(Long long1)
    {
        updatedAt = long1;
    }
}
