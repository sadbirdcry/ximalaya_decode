// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.message;

import com.ximalaya.ting.android.model.BaseModel;

public class PrivateMsgModel extends BaseModel
{

    public String avatarPath;
    public String content;
    public long createdAt;
    public boolean flag;
    public long id;
    public boolean isIn;
    public long lastId;
    public String linkmanAvatarPath;
    public String linkmanNickname;
    public long linkmanUid;
    public String nickname;
    public int noReadCount;
    public long uid;
    public long updatedAt;

    public PrivateMsgModel()
    {
        flag = true;
    }

    public String getAvatarPath()
    {
        return avatarPath;
    }

    public String getContent()
    {
        return content;
    }

    public long getCreatedAt()
    {
        return createdAt;
    }

    public long getId()
    {
        return id;
    }

    public long getLastId()
    {
        return lastId;
    }

    public String getLinkmanAvatarPath()
    {
        return linkmanAvatarPath;
    }

    public String getLinkmanNickname()
    {
        return linkmanNickname;
    }

    public long getLinkmanUid()
    {
        return linkmanUid;
    }

    public String getNickname()
    {
        return nickname;
    }

    public int getNoReadCount()
    {
        return noReadCount;
    }

    public long getUid()
    {
        return uid;
    }

    public long getUpdatedAt()
    {
        return updatedAt;
    }

    public boolean isIn()
    {
        return isIn;
    }

    public void setAvatarPath(String s)
    {
        avatarPath = s;
    }

    public void setContent(String s)
    {
        content = s;
    }

    public void setCreatedAt(long l)
    {
        createdAt = l;
    }

    public void setId(long l)
    {
        id = l;
    }

    public void setIsIn(boolean flag1)
    {
        isIn = flag1;
    }

    public void setLastId(long l)
    {
        lastId = l;
    }

    public void setLinkmanAvatarPath(String s)
    {
        linkmanAvatarPath = s;
    }

    public void setLinkmanNickname(String s)
    {
        linkmanNickname = s;
    }

    public void setLinkmanUid(long l)
    {
        linkmanUid = l;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setNoReadCount(int i)
    {
        noReadCount = i;
    }

    public void setUid(long l)
    {
        uid = l;
    }

    public void setUpdatedAt(long l)
    {
        updatedAt = l;
    }
}
