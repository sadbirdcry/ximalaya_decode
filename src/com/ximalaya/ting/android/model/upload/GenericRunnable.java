// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.upload;

import com.ximalaya.ting.android.model.sound.RecordingModel;

public class GenericRunnable
    implements Runnable
{

    private String audioPath;
    private Object o;
    public int percentage;
    public RecordingModel rm;

    public GenericRunnable()
    {
    }

    public String getAudioPath()
    {
        return audioPath;
    }

    public Object getObject()
    {
        return o;
    }

    public int getPercentage()
    {
        return percentage;
    }

    public void run()
    {
    }

    public void setAudioPath(String s)
    {
        audioPath = s;
    }

    public void setObject(Object obj)
    {
        o = obj;
    }

    public void setPercentage(int i)
    {
        percentage = i;
    }
}
