// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.upload;

import com.ximalaya.ting.android.model.sound.RecordingModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.http.entity.mime.content.AbstractContentBody;

// Referenced classes of package com.ximalaya.ting.android.model.upload:
//            GenericRunnable

public class ExFileBody extends AbstractContentBody
{
    public static interface percentChangedCallback
    {

        public abstract void execute(String s, int i);
    }


    private long bytesSendOut;
    private final String charset;
    private final File file;
    private final String filename;
    private RecordingModel mSoundsInfo;
    private GenericRunnable runInUIThread;

    public ExFileBody(RecordingModel recordingmodel, File file1)
    {
        this(recordingmodel, file1, "application/octet-stream");
    }

    public ExFileBody(RecordingModel recordingmodel, File file1, String s)
    {
        this(recordingmodel, file1, s, null);
    }

    public ExFileBody(RecordingModel recordingmodel, File file1, String s, String s1)
    {
        this(recordingmodel, file1, null, s, s1);
    }

    public ExFileBody(RecordingModel recordingmodel, File file1, String s, String s1, String s2)
    {
        super(s1);
        if (file1 == null)
        {
            throw new IllegalArgumentException("File may not be null");
        }
        file = file1;
        if (s != null)
        {
            filename = s;
        } else
        {
            filename = file1.getName();
        }
        charset = s2;
        bytesSendOut = 0L;
        mSoundsInfo = recordingmodel;
        runInUIThread = null;
    }

    public long getBytesSendOut()
    {
        return bytesSendOut;
    }

    public String getCharset()
    {
        return charset;
    }

    public long getContentLength()
    {
        return file.length();
    }

    public File getFile()
    {
        return file;
    }

    public String getFilename()
    {
        return filename;
    }

    public InputStream getInputStream()
        throws IOException
    {
        return new FileInputStream(file);
    }

    public String getTransferEncoding()
    {
        return "binary";
    }

    public void setPercentChangedCallback(GenericRunnable genericrunnable)
    {
        runInUIThread = genericrunnable;
    }

    public void writeTo(OutputStream outputstream)
        throws IOException
    {
        FileInputStream fileinputstream;
        if (outputstream == null)
        {
            throw new IllegalArgumentException("Output stream may not be null");
        }
        fileinputstream = new FileInputStream(file);
        byte abyte0[] = new byte[4096];
        int i = 0;
_L2:
        int j = fileinputstream.read(abyte0);
        if (j == -1)
        {
            break MISSING_BLOCK_LABEL_170;
        }
        outputstream.write(abyte0, 0, j);
        bytesSendOut = bytesSendOut + (long)j;
        j = i + j;
        mSoundsInfo.uploadedBytes = bytesSendOut;
        i = j;
        if ((long)(j * 20) < mSoundsInfo.audioFileLen)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (runInUIThread != null)
        {
            runInUIThread.setAudioPath(mSoundsInfo.getAudioPath());
            runInUIThread.setPercentage(mSoundsInfo.getUploadPercent());
            runInUIThread.rm = mSoundsInfo;
            (new Thread(runInUIThread)).start();
        }
        break MISSING_BLOCK_LABEL_186;
        outputstream.flush();
        fileinputstream.close();
        return;
        outputstream;
        fileinputstream.close();
        throw outputstream;
        i = 0;
        if (true) goto _L2; else goto _L1
_L1:
    }
}
