// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.dashang;


public class WxpayModel
{

    private String appid;
    private String noncestr;
    private String packageValue;
    private String partnerid;
    private String prepayid;
    private String sign;
    private String timestamp;

    public WxpayModel()
    {
    }

    public String getAppid()
    {
        return appid;
    }

    public String getNoncestr()
    {
        return noncestr;
    }

    public String getPackageValue()
    {
        return packageValue;
    }

    public String getPartnerid()
    {
        return partnerid;
    }

    public String getPrepayid()
    {
        return prepayid;
    }

    public String getSign()
    {
        return sign;
    }

    public String getTimestamp()
    {
        return timestamp;
    }

    public void setAppid(String s)
    {
        appid = s;
    }

    public void setNoncestr(String s)
    {
        noncestr = s;
    }

    public void setPackage(String s)
    {
        packageValue = s;
    }

    public void setPackageValue(String s)
    {
        packageValue = s;
    }

    public void setPartnerid(String s)
    {
        partnerid = s;
    }

    public void setPrepayid(String s)
    {
        prepayid = s;
    }

    public void setSign(String s)
    {
        sign = s;
    }

    public void setTimestamp(String s)
    {
        timestamp = s;
    }
}
