// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.anchor;

import com.alibaba.fastjson.JSON;
import java.util.List;

public class Track
{

    private int anchorType;
    private String coverLarge;
    private Long duration;
    private String playPath32;
    private String playPath64;
    private String playPathAacv164;
    private String playPathAacv224;
    private String title;
    private Long trackId;
    private Long uid;

    public Track()
    {
    }

    public static List parseListFromJson(String s)
    {
        return JSON.parseArray(s, com/ximalaya/ting/android/model/anchor/Track);
    }

    public int getAnchorType()
    {
        return anchorType;
    }

    public String getCoverLarge()
    {
        return coverLarge;
    }

    public Long getDuration()
    {
        return duration;
    }

    public String getPlayPath32()
    {
        return playPath32;
    }

    public String getPlayPath64()
    {
        return playPath64;
    }

    public String getPlayPathAacv164()
    {
        return playPathAacv164;
    }

    public String getPlayPathAacv224()
    {
        return playPathAacv224;
    }

    public String getTitle()
    {
        if (title == null)
        {
            return "";
        } else
        {
            return title;
        }
    }

    public Long getTrackId()
    {
        return trackId;
    }

    public Long getUid()
    {
        return uid;
    }

    public void setAnchorType(int i)
    {
        anchorType = i;
    }

    public void setCoverLarge(String s)
    {
        coverLarge = s;
    }

    public void setDuration(Long long1)
    {
        duration = long1;
    }

    public void setPlayPath32(String s)
    {
        playPath32 = s;
    }

    public void setPlayPath64(String s)
    {
        playPath64 = s;
    }

    public void setPlayPathAacv164(String s)
    {
        playPathAacv164 = s;
    }

    public void setPlayPathAacv224(String s)
    {
        playPathAacv224 = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTrackId(Long long1)
    {
        trackId = long1;
    }

    public void setUid(Long long1)
    {
        uid = long1;
    }

    public String toString()
    {
        return (new StringBuilder()).append("Track [trackId=").append(trackId).append(", title=").append(title).append(", playPath32=").append(playPath32).append(", playPath64=").append(playPath64).append(", coverLarge=").append(coverLarge).append(", duration=").append(duration).append(", uid=").append(uid).append("]").toString();
    }
}
