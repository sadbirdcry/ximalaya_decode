// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.personal_info;


// Referenced classes of package com.ximalaya.ting.android.model.personal_info:
//            UserInfoModel

public class HomePageModel extends UserInfoModel
{

    public int albums;
    public int events;
    public boolean favoriteAlbumIsUpdate;
    public int favorites;
    public int followingTags;
    public int followings;
    private boolean hasLive;
    public boolean isFollowed;
    public int leters;
    public int messages;
    public int newThirdRegisters;
    public int newUpdatedAlbums;
    public int newZoneCommentCount;
    public int noReadFollowers;
    public String personalSignature;
    public String ptitle;

    public HomePageModel()
    {
    }

    public int getAlbums()
    {
        return albums;
    }

    public String getBackgroundLogo()
    {
        return backgroundLogo;
    }

    public int getEvents()
    {
        return events;
    }

    public int getFavorites()
    {
        return favorites;
    }

    public int getFollowers()
    {
        return followers;
    }

    public int getFollowingTags()
    {
        return followingTags;
    }

    public int getFollowings()
    {
        return followings;
    }

    public int getLeters()
    {
        return leters;
    }

    public int getMessages()
    {
        return messages;
    }

    public int getNewThirdRegisters()
    {
        return newThirdRegisters;
    }

    public int getNewUpdatedAlbums()
    {
        return newUpdatedAlbums;
    }

    public int getNoReadFollowers()
    {
        return noReadFollowers;
    }

    public String getPersonDescribe()
    {
        return personDescribe;
    }

    public String getPersonalSignature()
    {
        return personalSignature;
    }

    public String getPtitle()
    {
        return ptitle;
    }

    public int getTracks()
    {
        return tracks;
    }

    public boolean isFollowed()
    {
        return isFollowed;
    }

    public boolean isHasLive()
    {
        return hasLive;
    }

    public boolean isVerified()
    {
        return isVerified;
    }

    public void setAlbums(int i)
    {
        albums = i;
    }

    public void setBackgroundLogo(String s)
    {
        backgroundLogo = s;
    }

    public void setEvents(int i)
    {
        events = i;
    }

    public void setFavorites(int i)
    {
        favorites = i;
    }

    public void setFollowed(boolean flag)
    {
        isFollowed = flag;
    }

    public void setFollowers(int i)
    {
        followers = i;
    }

    public void setFollowingTags(int i)
    {
        followingTags = i;
    }

    public void setFollowings(int i)
    {
        followings = i;
    }

    public void setHasLive(boolean flag)
    {
        hasLive = flag;
    }

    public void setLeters(int i)
    {
        leters = i;
    }

    public void setMessages(int i)
    {
        messages = i;
    }

    public void setNewThirdRegisters(int i)
    {
        newThirdRegisters = i;
    }

    public void setNewUpdatedAlbums(int i)
    {
        newUpdatedAlbums = i;
    }

    public void setNoReadFollowers(int i)
    {
        noReadFollowers = i;
    }

    public void setPersonDescribe(String s)
    {
        personDescribe = s;
    }

    public void setPersonalSignature(String s)
    {
        personalSignature = s;
    }

    public void setPtitle(String s)
    {
        ptitle = s;
    }

    public void setTracks(int i)
    {
        tracks = i;
    }

    public void setVerified(boolean flag)
    {
        isVerified = flag;
    }
}
