// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.personal_info;

import com.ximalaya.ting.android.model.BaseModel;

public class UserInfoModel extends BaseModel
{

    public String account;
    public String authType;
    public String authTypeValue;
    public String backgroundLogo;
    public String baseStation;
    public String description;
    public String deviceToken;
    public String device_type;
    public String email;
    public boolean flag;
    public int followers;
    public String imei;
    public String imsi;
    public boolean isFollowed;
    public boolean isVEmail;
    public boolean isVMobile;
    public boolean isVerified;
    public String largeLogo;
    public String logoPic;
    public String mPhone;
    public String middleLogo;
    public String mobileLargeLogo;
    public String mobileMiddleLogo;
    public String mobileSmallLogo;
    public String mobileSpecialLogo;
    public String nickname;
    public String password;
    public String personDescribe;
    public boolean rememberMe;
    public String smallLogo;
    public String thirdpartyName;
    public int tracks;
    public long uid;
    public String versionCode;
    public String versionName;
    public long zoneId;

    public UserInfoModel()
    {
        flag = true;
    }

    public boolean equals(Object obj)
    {
        if (this != obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            obj = (UserInfoModel)obj;
            if (uid != ((UserInfoModel) (obj)).uid)
            {
                return false;
            }
        }
        return true;
    }

    public String getAccount()
    {
        return account;
    }

    public String getAuthType()
    {
        return authType;
    }

    public String getAuthTypeValue()
    {
        return authTypeValue;
    }

    public String getDescription()
    {
        return description;
    }

    public String getEmail()
    {
        return email;
    }

    public Boolean getIsVEmail()
    {
        return Boolean.valueOf(isVEmail);
    }

    public Boolean getIsVMobile()
    {
        return Boolean.valueOf(isVMobile);
    }

    public String getLargeLogo()
    {
        return largeLogo;
    }

    public String getLogoPic()
    {
        return logoPic;
    }

    public String getMiddleLogo()
    {
        return middleLogo;
    }

    public String getMobile()
    {
        return mPhone;
    }

    public String getMobileLargeLogo()
    {
        return mobileLargeLogo;
    }

    public String getMobileMiddleLogo()
    {
        return mobileMiddleLogo;
    }

    public String getMobileSmallLogo()
    {
        return mobileSmallLogo;
    }

    public String getMobileSpecialLogo()
    {
        return mobileSpecialLogo;
    }

    public String getNickname()
    {
        return nickname;
    }

    public String getSmallLogo()
    {
        return smallLogo;
    }

    public String getThirdpartyName()
    {
        return thirdpartyName;
    }

    public Long getUid()
    {
        return Long.valueOf(uid);
    }

    public long getZoneId()
    {
        return zoneId;
    }

    public int hashCode()
    {
        return (int)(uid ^ uid >>> 32) + 31;
    }

    public void setAccount(String s)
    {
        account = s;
    }

    public void setAuthType(String s)
    {
        authType = s;
    }

    public void setAuthTypeValue(String s)
    {
        authTypeValue = s;
    }

    public void setDescription(String s)
    {
        description = s;
    }

    public void setEmail(String s)
    {
        email = s;
    }

    public void setIsVEmail(Boolean boolean1)
    {
        isVEmail = boolean1.booleanValue();
    }

    public void setIsVMobile(Boolean boolean1)
    {
        isVMobile = boolean1.booleanValue();
    }

    public void setLargeLogo(String s)
    {
        largeLogo = s;
    }

    public void setLogoPic(String s)
    {
        logoPic = s;
    }

    public void setMiddleLogo(String s)
    {
        middleLogo = s;
    }

    public void setMobile(String s)
    {
        mPhone = s;
    }

    public void setMobileLargeLogo(String s)
    {
        mobileLargeLogo = s;
    }

    public void setMobileMiddleLogo(String s)
    {
        mobileMiddleLogo = s;
    }

    public void setMobileSmallLogo(String s)
    {
        mobileSmallLogo = s;
    }

    public void setMobileSpecialLogo(String s)
    {
        mobileSpecialLogo = s;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setSmallLogo(String s)
    {
        smallLogo = s;
    }

    public void setThirdpartyName(String s)
    {
        thirdpartyName = s;
    }

    public void setUid(Long long1)
    {
        uid = long1.longValue();
    }

    public void setZoneId(long l)
    {
        zoneId = l;
    }
}
