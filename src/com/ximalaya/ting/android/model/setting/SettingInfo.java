// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.setting;


public class SettingInfo
{

    public boolean isExpired;
    public boolean isSetting;
    public boolean isVerified;
    public String nameWake;
    public long spaceOccupySize;
    public String textWake;
    public int time;

    public SettingInfo()
    {
    }

    public SettingInfo(String s, boolean flag)
    {
        nameWake = s;
        isSetting = flag;
    }

    public String getNameWake()
    {
        return nameWake;
    }

    public String getTextWake()
    {
        return textWake;
    }

    public int getTime()
    {
        return time;
    }

    public boolean isSetting()
    {
        return isSetting;
    }

    public void setNameWake(String s)
    {
        nameWake = s;
    }

    public void setSetting(boolean flag)
    {
        isSetting = flag;
    }

    public void setTextWake(String s)
    {
        textWake = s;
    }

    public void setTime(int i)
    {
        time = i;
    }
}
