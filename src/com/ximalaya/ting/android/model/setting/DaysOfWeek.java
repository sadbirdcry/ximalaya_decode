// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.setting;

import android.content.Context;
import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.setting:
//            WeekDay

public final class DaysOfWeek
{

    private static int DAY_MAP[] = {
        2, 3, 4, 5, 6, 7, 1
    };
    private int mDays;

    public DaysOfWeek(int i)
    {
        mDays = i;
    }

    public DaysOfWeek(boolean aflag[])
    {
        int i = 0;
        super();
        mDays = 0;
        for (; i < 7; i++)
        {
            set(i, aflag[i]);
        }

    }

    public static int getRepeatDays(List list)
    {
        int j = 0;
        int i = 0;
        while (j < list.size()) 
        {
            if (((WeekDay)list.get(j)).isSelected())
            {
                i |= 1 << j;
            } else
            {
                i &= ~(1 << j);
            }
            j++;
        }
        return i;
    }

    private boolean isSet(int i)
    {
        return (mDays & 1 << i) > 0;
    }

    public static boolean isSet(int i, int j)
    {
        return (1 << j & i) > 0;
    }

    public boolean[] getBooleanArray()
    {
        boolean aflag[] = new boolean[7];
        for (int i = 0; i < 7; i++)
        {
            aflag[i] = isSet(i);
        }

        return aflag;
    }

    public int getCoded()
    {
        return mDays;
    }

    public int getNextAlarm(Calendar calendar)
    {
        if (mDays != 0) goto _L2; else goto _L1
_L1:
        int j = -1;
_L4:
        return j;
_L2:
        int i;
        int k;
        k = calendar.get(7);
        i = 0;
_L6:
        j = i;
        if (i >= 7) goto _L4; else goto _L3
_L3:
        j = i;
        if (isSet(((k + 5) % 7 + i) % 7)) goto _L4; else goto _L5
_L5:
        i++;
          goto _L6
    }

    public boolean isRepeatSet()
    {
        return mDays != 0;
    }

    public void set(int i, boolean flag)
    {
        if (flag)
        {
            mDays = mDays | 1 << i;
            return;
        } else
        {
            mDays = mDays & ~(1 << i);
            return;
        }
    }

    public void set(DaysOfWeek daysofweek)
    {
        mDays = daysofweek.mDays;
    }

    public String toString(Context context, boolean flag)
    {
        boolean flag1 = false;
        StringBuilder stringbuilder = new StringBuilder();
        if (mDays == 0)
        {
            if (flag)
            {
                return context.getText(0x7f0900fe).toString();
            } else
            {
                return "";
            }
        }
        if (mDays == 127)
        {
            return context.getText(0x7f0900fd).toString();
        }
        int j = mDays;
        int i;
        int k;
        for (i = 0; j > 0; i = k)
        {
            k = i;
            if ((j & 1) == 1)
            {
                k = i + 1;
            }
            j >>= 1;
        }

        Object obj = new DateFormatSymbols();
        if (i > 1)
        {
            obj = ((DateFormatSymbols) (obj)).getShortWeekdays();
            j = ((flag1) ? 1 : 0);
        } else
        {
            obj = ((DateFormatSymbols) (obj)).getWeekdays();
            j = ((flag1) ? 1 : 0);
        }
        while (j < 7) 
        {
            int l = i;
            if ((mDays & 1 << j) != 0)
            {
                stringbuilder.append(obj[DAY_MAP[j]]);
                i--;
                l = i;
                if (i > 0)
                {
                    stringbuilder.append(context.getText(0x7f0900ff));
                    l = i;
                }
            }
            j++;
            i = l;
        }
        return stringbuilder.toString();
    }

}
