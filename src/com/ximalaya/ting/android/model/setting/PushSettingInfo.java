// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.setting;


public class PushSettingInfo
{

    public boolean isPushNewComment;
    public boolean isPushNewFollower;
    public boolean isPushNewMessage;
    public boolean isPushNewQuan;
    public boolean isPushZoneComment;
    public String msg;
    public int ret;
    public String uid;

    public PushSettingInfo()
    {
    }

    public String getMsg()
    {
        return msg;
    }

    public int getRet()
    {
        return ret;
    }

    public String getUid()
    {
        return uid;
    }

    public boolean isPushNewComment()
    {
        return isPushNewComment;
    }

    public boolean isPushNewFollower()
    {
        return isPushNewFollower;
    }

    public boolean isPushNewMessage()
    {
        return isPushNewMessage;
    }

    public boolean isPushNewQuan()
    {
        return isPushNewQuan;
    }

    public boolean isPushZoneComment()
    {
        return isPushZoneComment;
    }

    public void setMsg(String s)
    {
        msg = s;
    }

    public void setPushNewComment(boolean flag)
    {
        isPushNewComment = flag;
    }

    public void setPushNewFollower(boolean flag)
    {
        isPushNewFollower = flag;
    }

    public void setPushNewMessage(boolean flag)
    {
        isPushNewMessage = flag;
    }

    public void setPushNewQuan(boolean flag)
    {
        isPushNewQuan = flag;
    }

    public void setPushZoneComment(boolean flag)
    {
        isPushZoneComment = flag;
    }

    public void setRet(int i)
    {
        ret = i;
    }

    public void setUid(String s)
    {
        uid = s;
    }
}
