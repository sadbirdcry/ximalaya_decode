// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.setting;


public final class WeekDay
{

    public int indexSelected;
    public boolean isSelected;
    private boolean isSwitchOn;
    public String name;
    public long timeLeft;

    public WeekDay()
    {
        name = null;
        indexSelected = -1;
        isSelected = false;
        isSwitchOn = false;
        timeLeft = 0L;
    }

    public WeekDay(String s, int i, boolean flag, boolean flag1)
    {
        name = s;
        indexSelected = i;
        isSelected = flag;
        isSwitchOn = flag1;
    }

    public WeekDay(String s, int i, boolean flag, boolean flag1, int j)
    {
        name = s;
        indexSelected = i;
        isSelected = flag;
        isSwitchOn = flag1;
        timeLeft = j;
    }

    public final int getIndexSelected()
    {
        return indexSelected;
    }

    public final String getName()
    {
        return name;
    }

    public final long getTimeLeft()
    {
        return timeLeft;
    }

    public final boolean isSelected()
    {
        return isSelected;
    }

    public final boolean isSwitchOn()
    {
        return isSwitchOn;
    }

    public final void setIndexSelected(int i)
    {
        indexSelected = i;
    }

    public final void setName(String s)
    {
        name = s;
    }

    public final void setSelected(boolean flag)
    {
        isSelected = flag;
    }

    public final void setSwitchOn(boolean flag)
    {
        isSwitchOn = flag;
    }

    public final void setTimeLeft(long l)
    {
        timeLeft = l;
    }
}
