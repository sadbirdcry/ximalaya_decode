// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.zone;

import com.ximalaya.ting.android.model.BaseModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.zone:
//            PostModel, Poster, ImageInfo

public class CommentModel extends BaseModel
{

    private boolean canBeDeleted;
    private String content;
    private long createdTime;
    private long groupId;
    private long id;
    private List images;
    private boolean isMainPost;
    private int numOfComments;
    private int numOfFloor;
    private CommentModel parentComment;
    private long parentCommentId;
    private Poster poster;
    private long timeline;
    private String title;
    private long updatedTime;

    public CommentModel()
    {
    }

    public static CommentModel convert(PostModel postmodel)
    {
        if (postmodel == null)
        {
            return null;
        }
        CommentModel commentmodel = new CommentModel();
        commentmodel.id = postmodel.getId();
        commentmodel.canBeDeleted = postmodel.isCanBeDeleted();
        commentmodel.content = postmodel.getContent();
        commentmodel.createdTime = postmodel.getCreatedTime();
        commentmodel.updatedTime = postmodel.getUpdatedTime();
        commentmodel.numOfFloor = 0;
        commentmodel.numOfComments = postmodel.getNumOfComments();
        commentmodel.timeline = postmodel.getTimeline();
        commentmodel.title = postmodel.getTitle();
        commentmodel.isMainPost = true;
        commentmodel.poster = new Poster();
        if (postmodel.getPoster() != null)
        {
            commentmodel.poster.setNickname(postmodel.getPoster().getNickname());
            commentmodel.poster.setSmallLogo(postmodel.getPoster().getSmallLogo());
            commentmodel.poster.setUid(postmodel.getPoster().getUid());
        }
        if (postmodel.getImages() != null)
        {
            commentmodel.images = new ArrayList();
            ArrayList arraylist;
            for (postmodel = postmodel.getImages().iterator(); postmodel.hasNext(); commentmodel.images.add(arraylist))
            {
                Object obj = (List)postmodel.next();
                arraylist = new ArrayList();
                ImageInfo imageinfo1;
                for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); arraylist.add(imageinfo1))
                {
                    ImageInfo imageinfo = (ImageInfo)((Iterator) (obj)).next();
                    imageinfo1 = new ImageInfo();
                    imageinfo1.setHeight(imageinfo.getHeight());
                    imageinfo1.setWidth(imageinfo.getWidth());
                    imageinfo1.setImageUrl(imageinfo.getImageUrl());
                }

            }

        }
        return commentmodel;
    }

    public String getContent()
    {
        return content;
    }

    public long getCreatedTime()
    {
        return createdTime;
    }

    public long getGroupId()
    {
        return groupId;
    }

    public long getId()
    {
        return id;
    }

    public List getImages()
    {
        return images;
    }

    public String getImgBig(int i)
    {
        if (images == null || images.isEmpty() || i + 1 > images.size())
        {
            return null;
        }
        List list = (List)images.get(i);
        if (list != null && list.size() > 1)
        {
            return ((ImageInfo)list.get(list.size() - 1)).getImageUrl();
        } else
        {
            return null;
        }
    }

    public String getImgSmall(int i)
    {
        if (images == null || images.isEmpty() || i + 1 > images.size())
        {
            return null;
        }
        List list = (List)images.get(i);
        if (list != null && !images.isEmpty())
        {
            return ((ImageInfo)list.get(0)).getImageUrl();
        } else
        {
            return null;
        }
    }

    public int getNumOfComments()
    {
        return numOfComments;
    }

    public int getNumOfFloor()
    {
        return numOfFloor;
    }

    public CommentModel getParentComment()
    {
        return parentComment;
    }

    public long getParentCommentId()
    {
        return parentCommentId;
    }

    public Poster getPoster()
    {
        return poster;
    }

    public long getTimeline()
    {
        return timeline;
    }

    public String getTitle()
    {
        return title;
    }

    public long getUpdatedTime()
    {
        return updatedTime;
    }

    public boolean isCanBeDeleted()
    {
        return canBeDeleted;
    }

    public boolean isMainPost()
    {
        return isMainPost;
    }

    public void setCanBeDeleted(boolean flag)
    {
        canBeDeleted = flag;
    }

    public void setContent(String s)
    {
        content = s;
    }

    public void setCreatedTime(long l)
    {
        createdTime = l;
    }

    public void setGroupId(long l)
    {
        groupId = l;
    }

    public void setId(long l)
    {
        id = l;
    }

    public void setImages(List list)
    {
        images = list;
    }

    public void setMainPost(boolean flag)
    {
        isMainPost = flag;
    }

    public void setNumOfComments(int i)
    {
        numOfComments = i;
    }

    public void setNumOfFloor(int i)
    {
        numOfFloor = i;
    }

    public void setParentComment(CommentModel commentmodel)
    {
        parentComment = commentmodel;
    }

    public void setParentCommentId(long l)
    {
        parentCommentId = l;
    }

    public void setPoster(Poster poster1)
    {
        poster = poster1;
    }

    public void setTimeline(long l)
    {
        timeline = l;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setUpdatedTime(long l)
    {
        updatedTime = l;
    }
}
