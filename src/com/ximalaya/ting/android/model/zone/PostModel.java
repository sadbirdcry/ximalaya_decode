// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.zone;

import com.ximalaya.ting.android.model.BaseModel;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.zone:
//            ImageInfo, Poster

public class PostModel extends BaseModel
{

    private boolean canBeDeleted;
    private String content;
    private long createdTime;
    private long id;
    private List images;
    private boolean isContentIntact;
    private boolean isPrime;
    private boolean isTop;
    private int numOfComments;
    private Poster poster;
    private long timeline;
    private String title;
    private int topType;
    private long updatedTime;
    private long zoneId;

    public PostModel()
    {
    }

    public String getContent()
    {
        return content;
    }

    public long getCreatedTime()
    {
        return createdTime;
    }

    public long getId()
    {
        return id;
    }

    public List getImages()
    {
        return images;
    }

    public String getImgBig(int i)
    {
        if (images == null || images.isEmpty() || i + 1 > images.size())
        {
            return null;
        }
        List list = (List)images.get(i);
        if (list != null && list.size() > 1)
        {
            return ((ImageInfo)list.get(list.size() - 1)).getImageUrl();
        } else
        {
            return null;
        }
    }

    public String getImgSmall(int i)
    {
        if (images == null || images.isEmpty() || i + 1 > images.size())
        {
            return null;
        }
        List list = (List)images.get(i);
        if (list != null && !images.isEmpty())
        {
            return ((ImageInfo)list.get(0)).getImageUrl();
        } else
        {
            return null;
        }
    }

    public int getNumOfComments()
    {
        return numOfComments;
    }

    public Poster getPoster()
    {
        return poster;
    }

    public long getTimeline()
    {
        return timeline;
    }

    public String getTitle()
    {
        return title;
    }

    public int getTopType()
    {
        return topType;
    }

    public long getUpdatedTime()
    {
        return updatedTime;
    }

    public long getZoneId()
    {
        return zoneId;
    }

    public boolean isCanBeDeleted()
    {
        return canBeDeleted;
    }

    public boolean isContentIntact()
    {
        return isContentIntact;
    }

    public boolean isPrime()
    {
        return isPrime;
    }

    public boolean isTop()
    {
        return isTop;
    }

    public void setCanBeDeleted(boolean flag)
    {
        canBeDeleted = flag;
    }

    public void setContent(String s)
    {
        content = s;
    }

    public void setCreatedTime(long l)
    {
        createdTime = l;
    }

    public void setId(long l)
    {
        id = l;
    }

    public void setImages(List list)
    {
        images = list;
    }

    public void setIsContentIntact(boolean flag)
    {
        isContentIntact = flag;
    }

    public void setIsPrime(boolean flag)
    {
        isPrime = flag;
    }

    public void setIsTop(boolean flag)
    {
        isTop = flag;
    }

    public void setNumOfComments(int i)
    {
        numOfComments = i;
    }

    public void setPoster(Poster poster1)
    {
        poster = poster1;
    }

    public void setTimeline(long l)
    {
        timeline = l;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTopType(int i)
    {
        topType = i;
    }

    public void setUpdatedTime(long l)
    {
        updatedTime = l;
    }

    public void setZoneId(long l)
    {
        zoneId = l;
    }
}
