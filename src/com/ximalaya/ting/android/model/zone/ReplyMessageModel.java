// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.zone;


// Referenced classes of package com.ximalaya.ting.android.model.zone:
//            Poster

public class ReplyMessageModel
{

    private String content;
    private long createdAt;
    private String pContent;
    private long postId;
    private Poster poster;
    private long timeline;
    private long zoneId;
    private String zoneName;

    public ReplyMessageModel()
    {
    }

    public String getContent()
    {
        return content;
    }

    public long getCreatedAt()
    {
        return createdAt;
    }

    public long getPostId()
    {
        return postId;
    }

    public Poster getPoster()
    {
        return poster;
    }

    public long getTimeline()
    {
        return timeline;
    }

    public long getZoneId()
    {
        return zoneId;
    }

    public String getZoneName()
    {
        return zoneName;
    }

    public String getpContent()
    {
        return pContent;
    }

    public void setContent(String s)
    {
        content = s;
    }

    public void setCreatedAt(long l)
    {
        createdAt = l;
    }

    public void setPContent(String s)
    {
        pContent = s;
    }

    public void setPostId(long l)
    {
        postId = l;
    }

    public void setPoster(Poster poster1)
    {
        poster = poster1;
    }

    public void setTimeline(long l)
    {
        timeline = l;
    }

    public void setZoneId(long l)
    {
        zoneId = l;
    }

    public void setZoneName(String s)
    {
        zoneName = s;
    }
}
