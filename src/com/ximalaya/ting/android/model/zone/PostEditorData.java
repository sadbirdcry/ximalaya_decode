// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.zone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PostEditorData
{

    private String content;
    private boolean editComment;
    private List imagePaths;
    private long postId;
    private long replyCommentGroupId;
    private long replyCommentId;
    private String replyPosterName;
    private String title;
    private long uid;
    private Map uploadedImgs;
    private long zoneId;

    public PostEditorData()
    {
        editComment = true;
        imagePaths = new ArrayList();
        uploadedImgs = new HashMap();
    }

    public String getContent()
    {
        return content;
    }

    public List getImagePaths()
    {
        return imagePaths;
    }

    public long getPostId()
    {
        return postId;
    }

    public long getReplyCommentGroupId()
    {
        return replyCommentGroupId;
    }

    public long getReplyCommentId()
    {
        return replyCommentId;
    }

    public String getReplyPosterName()
    {
        return replyPosterName;
    }

    public String getTitle()
    {
        return title;
    }

    public long getUid()
    {
        return uid;
    }

    public Map getUploadedImgs()
    {
        return uploadedImgs;
    }

    public long getZoneId()
    {
        return zoneId;
    }

    public boolean isEditComment()
    {
        return editComment;
    }

    public void setContent(String s)
    {
        content = s;
    }

    public void setEditComment(boolean flag)
    {
        editComment = flag;
    }

    public void setImagePaths(List list)
    {
        imagePaths = list;
    }

    public void setPostId(long l)
    {
        postId = l;
    }

    public void setReplyCommentGroupId(long l)
    {
        replyCommentGroupId = l;
    }

    public void setReplyCommentId(long l)
    {
        replyCommentId = l;
    }

    public void setReplyPosterName(String s)
    {
        replyPosterName = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setUid(long l)
    {
        uid = l;
    }

    public void setUploadedImgs(Map map)
    {
        uploadedImgs = map;
    }

    public void setZoneId(long l)
    {
        zoneId = l;
    }
}
