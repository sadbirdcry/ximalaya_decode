// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.zone;

import java.io.Serializable;

public class PostReportInfo
    implements Serializable
{

    private static final long serialVersionUID = 0x6d28253449a6e69cL;
    private long commentId;
    private long postId;
    private long reportedId;
    private long reporterId;
    private int type;
    private long zoneId;

    public PostReportInfo()
    {
    }

    public long getCommentId()
    {
        return commentId;
    }

    public long getPostId()
    {
        return postId;
    }

    public long getReportedId()
    {
        return reportedId;
    }

    public long getReporterId()
    {
        return reporterId;
    }

    public int getType()
    {
        return type;
    }

    public long getZoneId()
    {
        return zoneId;
    }

    public void setCommentId(long l)
    {
        commentId = l;
    }

    public void setPostId(long l)
    {
        postId = l;
    }

    public void setReportedId(long l)
    {
        reportedId = l;
    }

    public void setReporterId(long l)
    {
        reporterId = l;
    }

    public void setType(int i)
    {
        type = i;
    }

    public void setZoneId(long l)
    {
        zoneId = l;
    }
}
