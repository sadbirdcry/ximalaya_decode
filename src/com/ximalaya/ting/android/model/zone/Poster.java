// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.zone;

import android.text.TextUtils;
import java.io.Serializable;

public class Poster
    implements Serializable
{

    private static final long serialVersionUID = 0xe757a2b51a7e69b0L;
    private String nickname;
    private String smallLogo;
    private long uid;

    public Poster()
    {
    }

    public String getNickname()
    {
        return nickname;
    }

    public String getSmallLogo()
    {
        if (TextUtils.isEmpty(smallLogo))
        {
            return null;
        } else
        {
            return smallLogo;
        }
    }

    public long getUid()
    {
        return uid;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setSmallLogo(String s)
    {
        smallLogo = s;
    }

    public void setUid(long l)
    {
        uid = l;
    }
}
