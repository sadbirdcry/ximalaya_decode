// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.zone;

import com.ximalaya.ting.android.model.BaseModel;
import java.io.Serializable;

// Referenced classes of package com.ximalaya.ting.android.model.zone:
//            RoleInfo, Poster

public class ZoneModel extends BaseModel
    implements Serializable
{

    private static final long serialVersionUID = 0x44c4474e4f750c26L;
    private String description;
    private String displayName;
    private long id;
    private String imageUrl;
    private RoleInfo myZone;
    private int numOfMembers;
    private int numOfPosts;
    private Poster owner;
    private long timeline;

    public ZoneModel()
    {
    }

    public String getDescription()
    {
        return description;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public long getId()
    {
        return id;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public RoleInfo getMyZone()
    {
        return myZone;
    }

    public int getNumOfMembers()
    {
        return numOfMembers;
    }

    public int getNumOfPosts()
    {
        return numOfPosts;
    }

    public Poster getOwner()
    {
        return owner;
    }

    public long getTimeline()
    {
        return timeline;
    }

    public void setDescription(String s)
    {
        description = s;
    }

    public void setDisplayName(String s)
    {
        displayName = s;
    }

    public void setId(long l)
    {
        id = l;
    }

    public void setImageUrl(String s)
    {
        imageUrl = s;
    }

    public void setMyZone(RoleInfo roleinfo)
    {
        myZone = roleinfo;
    }

    public void setNumOfMembers(int i)
    {
        numOfMembers = i;
    }

    public void setNumOfPosts(int i)
    {
        numOfPosts = i;
    }

    public void setOwner(Poster poster)
    {
        owner = poster;
    }

    public void setTimeline(long l)
    {
        timeline = l;
    }
}
