// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.zone;


public class ImageInfo
{

    private int height;
    private String imageUrl;
    private int width;

    public ImageInfo()
    {
    }

    public int getHeight()
    {
        return height;
    }

    public String getImageUrl()
    {
        return imageUrl;
    }

    public int getWidth()
    {
        return width;
    }

    public void setHeight(int i)
    {
        height = i;
    }

    public void setImageUrl(String s)
    {
        imageUrl = s;
    }

    public void setWidth(int i)
    {
        width = i;
    }
}
