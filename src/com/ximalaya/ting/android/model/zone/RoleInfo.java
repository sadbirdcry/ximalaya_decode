// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.zone;

import java.io.Serializable;

public class RoleInfo
    implements Serializable
{

    private static final long serialVersionUID = 0x7f208bb1f578c347L;
    private boolean isJoint;
    private int permissions;
    private long roleId;

    public RoleInfo()
    {
    }

    public int getPermissions()
    {
        return permissions;
    }

    public long getRoleId()
    {
        return roleId;
    }

    public boolean isJoint()
    {
        return isJoint;
    }

    public void setIsJoint(boolean flag)
    {
        isJoint = flag;
    }

    public void setPermissions(int i)
    {
        permissions = i;
    }

    public void setRoleId(long l)
    {
        roleId = l;
    }
}
