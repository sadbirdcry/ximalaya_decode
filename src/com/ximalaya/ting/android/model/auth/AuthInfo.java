// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.auth;

import java.io.Serializable;

public class AuthInfo
    implements Serializable
{

    private static final long serialVersionUID = 0x658af1bb03f5dac4L;
    public String access_token;
    public String deviceId;
    public String errorMsg;
    public String expires_in;
    public String macKey;
    public String msg;
    public String openid;
    public String refreshToken;
    public String remind_in;
    public int ret;
    public String token_type;
    public String uid;

    public AuthInfo()
    {
    }
}
