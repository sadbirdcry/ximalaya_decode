// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.sound;

import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.Likeable;
import java.io.File;

public class SoundModel extends BaseModel
    implements Likeable
{

    public long albumId;
    public String albumImage;
    public String albumTitle;
    public long audioFileLen;
    private String audioPath;
    public long categoryId;
    public String categoryName;
    public String commentContent;
    public long commentId;
    public int comments;
    public String coverLarge;
    public String coverSmall;
    public long createdAt;
    public int download_id;
    public double duration;
    public String intro;
    public boolean isLike;
    public boolean isPlaying;
    public boolean isPublic;
    public boolean isRelay;
    public int likes;
    public String nickname;
    public int opType;
    public int orderNum;
    public String playPathAacv164;
    public String playPathAacv224;
    public String playUrl32;
    public String playUrl64;
    public int playtimes;
    public int processState;
    public String refNickname;
    public String refSmallLogo;
    public long refUid;
    public int shares;
    public String smallLogo;
    public long startedAt;
    public String tags;
    public String title;
    public long trackId;
    public long uid;
    public long updatedAt;
    public int userSource;

    public SoundModel()
    {
        isPublic = true;
        isPlaying = false;
    }

    public String getAudioPath()
    {
        return audioPath;
    }

    public long getId()
    {
        return trackId;
    }

    public boolean isLiked()
    {
        return isLike;
    }

    public void setAudioPath(String s)
    {
        audioPath = s;
        if (s != null)
        {
            audioFileLen = (new File(s)).length();
        }
    }

    public void toggleLikeStatus()
    {
        boolean flag;
        if (!isLike)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        isLike = flag;
    }
}
