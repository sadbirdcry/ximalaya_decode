// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.sound;

import com.ximalaya.ting.android.model.Likeable;

public class HotSoundModel
    implements Likeable
{

    public String access_password;
    public String album_cover_path;
    public String album_cover_path_640;
    public String album_cover_path_86;
    public long album_id;
    public String album_title;
    public String allow_comment;
    public String allow_download;
    public String announcer;
    public String approved_at;
    public String arrangement;
    public String author;
    public String avatar_path;
    public String avatar_path_640;
    public long category_id;
    public int comments_counts;
    public String composer;
    public String coverLarge;
    public String coverSmall;
    public String cover_path;
    public String cover_path_60;
    public String cover_path_640;
    public String cover_path_86;
    public String cover_path_large;
    public String cover_path_middle;
    public String cover_path_small;
    public String cover_path_wide;
    public long createdAt;
    public String created_at;
    public int dig_status;
    public String download_path;
    public float duration;
    public String extra_tags;
    public int favorites_counts;
    public long id;
    public String ignore_tags;
    public String inet_aton_ip;
    public String intro;
    public boolean isRelay;
    public boolean is_crawler;
    public boolean is_deleted;
    public boolean is_favorited;
    public boolean is_pick;
    public boolean is_playing;
    public boolean is_public;
    public boolean is_publish;
    public boolean is_v;
    public String language;
    public String largeLogo;
    public String latitude;
    public String longitude;
    public String lyric;
    public String lyric_path;
    public String middleLogo;
    public long mp3size;
    public long mp3size_32;
    public long mp3size_64;
    public String music_category;
    public String nickname;
    public int order_num;
    public String play_path;
    public String play_path_128;
    public String play_path_32;
    public String play_path_64;
    public int plays_counts;
    public String post_production;
    public String resinger;
    public String rich_intro;
    public int shares_counts;
    public String short_intro;
    public String singer;
    public String singer_category;
    public String smallLogo;
    public String tags;
    public String tags_json;
    public String title;
    public int transcode_state;
    public long uid;
    public long updatedAt;
    public String updated_at;
    public String upload_id;
    public int upload_source;
    public int user_source;
    public String waterfall_image;
    public String waveform;

    public HotSoundModel()
    {
        is_playing = false;
    }

    public long getId()
    {
        return id;
    }

    public boolean isLiked()
    {
        return is_favorited;
    }

    public void toggleLikeStatus()
    {
        boolean flag;
        if (!is_favorited)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        is_favorited = flag;
    }
}
