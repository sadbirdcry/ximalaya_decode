// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.sound;

import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.sound:
//            SoundInfo, Comment

public class SoundDetails extends SoundInfo
{

    public int categoryId;
    public String categoryName;
    public String commentContent;
    public String commentId;
    public String coverLarge;
    public Comment firstComment;
    public String intro;
    public boolean isFollowed;
    public boolean isPublic;
    public int opType;
    public int processState;
    public String refNickname;
    public String refSmallLogo;
    public long refUid;
    public String tags;
    public List trackBlocks;

    public SoundDetails()
    {
    }

    public SoundDetails(SoundDetails sounddetails)
    {
        super(sounddetails);
        tags = sounddetails.tags;
        coverLarge = sounddetails.coverLarge;
        categoryId = sounddetails.categoryId;
        categoryName = sounddetails.categoryName;
        intro = sounddetails.intro;
        processState = sounddetails.processState;
        isPublic = sounddetails.isPublic;
        opType = sounddetails.opType;
        refUid = sounddetails.refUid;
        refNickname = sounddetails.refNickname;
        refSmallLogo = sounddetails.refSmallLogo;
        commentContent = sounddetails.commentContent;
        commentId = sounddetails.commentId;
        trackBlocks = sounddetails.trackBlocks;
        firstComment = sounddetails.firstComment;
    }
}
