// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.sound;

import com.ximalaya.ting.android.model.BaseModel;
import java.util.List;

public class SoundInfoDetail extends BaseModel
{
    public static class UserInfo
    {

        public int albums;
        public int followers;
        public int followings;
        public boolean isFollowed;
        public boolean isVerified;
        public String nickname;
        public String personDescribe;
        public String ptitle;
        public String smallLogo;
        public int tracks;
        public long uid;

        public UserInfo()
        {
        }
    }


    public long activityId;
    public String activityName;
    public long albumId;
    public String albumImage;
    public String albumTitle;
    public long categoryId;
    public String categoryName;
    public int comments;
    public String coverLarge;
    public String coverSmall;
    public long createdAt;
    public double duration;
    public List images;
    public String intro;
    public boolean isLike;
    public boolean isPublic;
    public boolean isRelay;
    public int likes;
    public String lyric;
    public int opType;
    public String playPathAacv164;
    public String playPathAacv224;
    public String playUrl32;
    public String playUrl64;
    public int playtimes;
    public int processState;
    public String richIntro;
    public int shares;
    public String tags;
    public String title;
    public long trackId;
    public long uid;
    public UserInfo userInfo;
    public int userSource;

    public SoundInfoDetail()
    {
    }

    public boolean hasAlbum()
    {
        return albumId != 0L;
    }
}
