// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.sound;

import com.ximalaya.ting.android.model.BaseModel;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.sound:
//            Comment

public class SoundDetailModel extends BaseModel
{

    public long albumId;
    public String albumImage;
    public String albumTitle;
    public int categoryId;
    public String categoryName;
    public String commentContent;
    public String commentId;
    public int comments;
    public String coverLarge;
    public String coverSmall;
    public long createdAt;
    public long duration;
    public Comment firstComment;
    public String intro;
    public boolean isFollowed;
    public boolean isLike;
    public boolean isPublic;
    public boolean isRelay;
    public int likes;
    public String nickname;
    public int opType;
    public String playPathAacv164;
    public String playPathAacv224;
    public String playUrl32;
    public String playUrl64;
    public int playtimes;
    public int processState;
    public String refNickname;
    public String refSmallLogo;
    public long refUid;
    public int shares;
    public String smallLogo;
    public String tags;
    public String title;
    public List trackBlocks;
    public long trackId;
    public long uid;
    public int userSource;

    public SoundDetailModel()
    {
    }
}
