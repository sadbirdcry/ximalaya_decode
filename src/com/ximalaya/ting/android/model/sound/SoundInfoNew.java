// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.sound;

import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.Likeable;

public class SoundInfoNew extends BaseModel
    implements Likeable
{

    public int commentsCounts;
    public String coverSmall;
    public String coverWebSmall;
    public long createdAt;
    public float duration;
    public int favoritesCounts;
    public long id;
    public boolean isFavorite;
    public boolean isRelay;
    public String nickname;
    public String playPath32;
    public String playPath64;
    public String playPathAacv164;
    public String playPathAacv224;
    public int playsCounts;
    public int sharesCounts;
    public String title;
    public long uid;
    public int userSource;

    public SoundInfoNew()
    {
    }

    public boolean equals(Object obj)
    {
        if (this != obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            obj = (SoundInfoNew)obj;
            if (id != ((SoundInfoNew) (obj)).id)
            {
                return false;
            }
        }
        return true;
    }

    public long getId()
    {
        return id;
    }

    public int hashCode()
    {
        return (int)(id ^ id >>> 32) + 31;
    }

    public boolean isLiked()
    {
        return isFavorite;
    }

    public void toggleLikeStatus()
    {
        boolean flag;
        if (!isFavorite)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        isFavorite = flag;
    }
}
