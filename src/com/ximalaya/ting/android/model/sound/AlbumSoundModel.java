// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.sound;

import com.ximalaya.ting.android.model.Likeable;

public class AlbumSoundModel
    implements Likeable
{

    public long albumId;
    public String albumImage;
    public String albumTitle;
    public long categoryId;
    public String commentContent;
    public long commentId;
    public int comments;
    public String coverLarge;
    public String coverSmall;
    public long createdAt;
    public float duration;
    public boolean isLike;
    public boolean isPublic;
    public boolean isRelay;
    public boolean is_playing;
    public int likes;
    public String nickname;
    public int opType;
    public int orderNum;
    public String playPathAacv164;
    public String playPathAacv224;
    public String playUrl32;
    public String playUrl64;
    public int playtimes;
    public int processState;
    public String refNickname;
    public String refSmallLogo;
    public long refUid;
    public int shares;
    public String smallLogo;
    public int status;
    public String title;
    public long trackId;
    public long uid;
    public int userSource;

    public AlbumSoundModel()
    {
    }

    public long getId()
    {
        return trackId;
    }

    public boolean isLiked()
    {
        return isLike;
    }

    public void toggleLikeStatus()
    {
        boolean flag;
        if (!isLike)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        isLike = flag;
    }
}
