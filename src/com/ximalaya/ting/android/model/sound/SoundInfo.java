// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.sound;

import com.ximalaya.ting.android.model.Likeable;
import java.util.Date;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.sound:
//            SoundDetails

public class SoundInfo
    implements Likeable
{

    public long actualAlbumId;
    public String albumCoverPath;
    public long albumId;
    public String albumName;
    public int anchorType;
    public List announcerList;
    public int category;
    public int comments_counts;
    public String coverLarge;
    public String coverSmall;
    public long create_at;
    public String downLoadUrl;
    public double duration;
    public String endTime;
    public int favorites_counts;
    public long filesize;
    public long history_duration;
    public long history_listener;
    public boolean isPublic;
    public boolean isRelay;
    public boolean is_favorited;
    public boolean is_playing;
    public String listenBackUrl;
    public String liveUrl;
    private boolean mIsNew;
    private int mPlaylistType;
    public String nickname;
    public long orderNum;
    public int orderPositon;
    public String playPathAacv164;
    public String playPathAacv224;
    public int playType;
    public String playUrl32;
    public String playUrl64;
    public int plays_counts;
    public long programId;
    public String programName;
    public long programScheduleId;
    public int radioId;
    public String radioName;
    public String recSrc;
    public String recTrack;
    public int shares_counts;
    public Date startPlayTime;
    public String startTime;
    public int status;
    public String title;
    public long trackId;
    public long uid;
    public String userCoverPath;
    public int user_source;
    public Date validDate;

    public SoundInfo()
    {
        status = 1;
        is_playing = false;
        anchorType = 0;
        mIsNew = false;
    }

    public SoundInfo(SoundDetails sounddetails)
    {
        status = 1;
        is_playing = false;
        anchorType = 0;
        mIsNew = false;
        trackId = sounddetails.trackId;
        title = sounddetails.title;
        playUrl32 = sounddetails.playUrl32;
        playUrl64 = sounddetails.playUrl64;
        coverSmall = sounddetails.coverSmall;
        filesize = sounddetails.filesize;
        duration = sounddetails.duration;
        create_at = sounddetails.create_at;
        orderNum = sounddetails.orderNum;
        uid = sounddetails.uid;
        nickname = sounddetails.nickname;
        userCoverPath = sounddetails.userCoverPath;
        albumId = sounddetails.albumId;
        albumName = sounddetails.albumName;
        albumCoverPath = sounddetails.albumCoverPath;
        plays_counts = sounddetails.plays_counts;
        favorites_counts = sounddetails.favorites_counts;
        is_favorited = sounddetails.is_favorited;
        isRelay = sounddetails.isRelay;
        comments_counts = sounddetails.comments_counts;
        shares_counts = sounddetails.shares_counts;
        user_source = sounddetails.user_source;
        is_playing = sounddetails.is_playing;
    }

    public boolean equals(Object obj)
    {
        if (obj != null)
        {
            if (obj == this)
            {
                return true;
            }
            if (obj instanceof SoundInfo)
            {
                obj = (SoundInfo)obj;
                if (((SoundInfo) (obj)).category == category)
                {
                    if (((SoundInfo) (obj)).category == 0 && ((SoundInfo) (obj)).trackId != 0L && ((SoundInfo) (obj)).trackId == trackId)
                    {
                        return true;
                    }
                    if (((SoundInfo) (obj)).category != 0 && ((SoundInfo) (obj)).programScheduleId != 0L && ((SoundInfo) (obj)).programScheduleId == programScheduleId)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public long getId()
    {
        return trackId;
    }

    public int getPlaylistType()
    {
        return mPlaylistType;
    }

    public long getRealAlubmId()
    {
        if (albumId == -1L)
        {
            return actualAlbumId;
        } else
        {
            return albumId;
        }
    }

    public boolean hasAlbum()
    {
        if (albumId != -1L) goto _L2; else goto _L1
_L1:
        if (actualAlbumId <= 0L) goto _L4; else goto _L3
_L3:
        return true;
_L4:
        return false;
_L2:
        if (albumId <= 0L)
        {
            return false;
        }
        if (true) goto _L3; else goto _L5
_L5:
    }

    public boolean isLiked()
    {
        return is_favorited;
    }

    public boolean isNew()
    {
        return mIsNew;
    }

    public void setIsNew(boolean flag)
    {
        mIsNew = flag;
    }

    public String toString()
    {
        return (new StringBuilder()).append("SoundInfo [trackId=").append(trackId).append(", title=").append(title).append(", playUrl32=").append(playUrl32).append(", playUrl64=").append(playUrl64).append(", coverSmall=").append(coverSmall).append(", coverLarge=").append(coverLarge).append(", filesize=").append(filesize).append(", duration=").append(duration).append(", create_at=").append(create_at).append(", status=").append(status).append(", history_listener=").append(history_listener).append(", history_duration=").append(history_duration).append(", orderNum=").append(orderNum).append(", uid=").append(uid).append(", nickname=").append(nickname).append(", userCoverPath=").append(userCoverPath).append(", albumId=").append(albumId).append(", albumName=").append(albumName).append(", albumCoverPath=").append(albumCoverPath).append(", plays_counts=").append(plays_counts).append(", favorites_counts=").append(favorites_counts).append(", is_favorited=").append(is_favorited).append(", isRelay=").append(isRelay).append(", comments_counts=").append(comments_counts).append(", shares_counts=").append(shares_counts).append(", user_source=").append(user_source).append(", downLoadUrl=").append(downLoadUrl).append(", is_playing=").append(is_playing).append(", isPublic=").append(isPublic).append(", anchorType=").append(anchorType).append(", mIsNew=").append(mIsNew).append(", actualAlbumId=").append(actualAlbumId).append(", orderPositon=").append(orderPositon).append(", recSrc=").append(recSrc).append(", recTrack=").append(recTrack).append(", radioId=").append(radioId).append(", programScheduleId=").append(programScheduleId).append(", programId=").append(programId).append(", programName=").append(programName).append(", startTime=").append(startTime).append(", endTime=").append(endTime).append(", radioName=").append(radioName).append(", playType=").append(playType).append(", category=").append(category).append(", liveUrl=").append(liveUrl).append(", listenBackUrl=").append(listenBackUrl).append(", announcerList=").append(announcerList).append(", mPlaylistType=").append(mPlaylistType).append("]").toString();
    }

    public void toggleLikeStatus()
    {
        boolean flag;
        if (!is_favorited)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        is_favorited = flag;
    }
}
