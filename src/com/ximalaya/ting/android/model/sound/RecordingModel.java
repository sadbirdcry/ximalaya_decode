// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.sound;

import android.graphics.Bitmap;
import android.text.TextUtils;
import com.ximalaya.ting.android.model.Likeable;
import com.ximalaya.ting.android.transaction.d.s;
import com.ximalaya.ting.android.transaction.d.w;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.sound:
//            SoundModel

public class RecordingModel extends SoundModel
    implements Likeable, Comparable
{

    public long _id;
    public long activityId;
    public long audioId;
    public String checkCode;
    public String checkUUID;
    public String covers[];
    public String coversId;
    public String fileExtName;
    public String fileName;
    public long formId;
    public Bitmap image;
    public boolean isAudioUploaded;
    public boolean isFormUploaded;
    public boolean isNeedUpload;
    public boolean mHeadSetOn;
    public List mOperations;
    public String mOutName;
    public s mRecordFile;
    public String pcmPath;
    public w saveTask;
    public String shareList;
    public int status;
    public int uploadPercent;
    public long uploadedBytes;

    public RecordingModel()
    {
        isAudioUploaded = false;
        isFormUploaded = false;
        isNeedUpload = false;
        status = 1;
    }

    public int compareTo(RecordingModel recordingmodel)
    {
        if (processState == 2 || recordingmodel.processState != 2)
        {
            if (processState == 2 && recordingmodel.processState != 2)
            {
                return 1;
            }
            if (createdAt <= recordingmodel.createdAt)
            {
                return createdAt >= recordingmodel.createdAt ? 0 : 1;
            }
        }
        return -1;
    }

    public volatile int compareTo(Object obj)
    {
        return compareTo((RecordingModel)obj);
    }

    public boolean equals(Object obj)
    {
        if (obj != null)
        {
            if (obj == this)
            {
                return true;
            }
            if (obj instanceof RecordingModel)
            {
                obj = (RecordingModel)obj;
                if (_id != 0L && _id == ((RecordingModel) (obj))._id)
                {
                    return true;
                }
                if (trackId != 0L && trackId == ((RecordingModel) (obj)).trackId)
                {
                    return true;
                }
                if (!TextUtils.isEmpty(title) && title.equals(((RecordingModel) (obj)).title))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public long getId()
    {
        return trackId;
    }

    public int getUploadPercent()
    {
        if (audioFileLen > 0L)
        {
            uploadPercent = (int)((uploadedBytes * 100L) / audioFileLen);
            return uploadPercent;
        } else
        {
            uploadPercent = 0;
            return 0;
        }
    }

    public int hashCode()
    {
        if (!TextUtils.isEmpty(playUrl32))
        {
            return playUrl32.hashCode();
        }
        if (!TextUtils.isEmpty(getAudioPath()))
        {
            return getAudioPath().hashCode();
        } else
        {
            return (int)(_id ^ _id >>> 32) + 31;
        }
    }

    public boolean isLiked()
    {
        return isLike;
    }

    public void toggleLikeStatus()
    {
        boolean flag;
        if (!isLike)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        isLike = flag;
    }
}
