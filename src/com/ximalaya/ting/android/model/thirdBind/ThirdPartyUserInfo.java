// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.thirdBind;

import java.io.Serializable;

// Referenced classes of package com.ximalaya.ting.android.model.thirdBind:
//            BaseThirdBind

public class ThirdPartyUserInfo extends BaseThirdBind
    implements Serializable
{

    private static final long serialVersionUID = 1L;
    public String header;
    public String identity;
    public boolean isInvite;
    public boolean isRealData;
    public boolean mobileAll;
    public String name;
    public String nickname;
    public boolean relay;
    public String smallLogo;
    public boolean webAlbum;
    public boolean webComment;
    public boolean webFavorite;
    public boolean webTrack;

    public ThirdPartyUserInfo()
    {
        isInvite = false;
        isRealData = true;
    }
}
