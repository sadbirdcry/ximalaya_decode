// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding;

import android.text.TextUtils;
import com.alibaba.fastjson.JSONObject;
import java.util.List;

public class FindingTabModel
{

    public static final String ACTIVITY = "activity";
    public static final String ANCHOR = "anchor";
    public static final String CATEGORY = "category";
    public static final String H5 = "html5";
    public static final String LIVE = "live";
    public static final String RANKING = "ranking";
    public static final String RECOMMEND = "recommend";
    public static final String SPECIAL = "special";
    public static final int TAB_ACTIVITY = 9;
    public static final int TAB_ANCHOR = 6;
    public static final int TAB_CATEGORY = 2;
    public static final int TAB_H5 = 7;
    public static final int TAB_LIVE = 5;
    public static final int TAB_RANKING = 4;
    public static final int TAB_RECOMMEND = 3;
    public static final int TAB_SPECIAL = 8;
    public static final int TAB_XZONE = 10;
    public static final String XZONE = "xzone";
    private String contentType;
    private String title;
    private String url;

    public FindingTabModel()
    {
    }

    public static final List getListFromJson(String s)
    {
        return JSONObject.parseArray(s, com/ximalaya/ting/android/model/finding/FindingTabModel);
    }

    public boolean equals(Object obj)
    {
        if (this != obj) goto _L2; else goto _L1
_L1:
        return true;
_L2:
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        obj = (FindingTabModel)obj;
        if (contentType == null)
        {
            if (((FindingTabModel) (obj)).contentType != null)
            {
                return false;
            }
        } else
        if (!contentType.equals(((FindingTabModel) (obj)).contentType))
        {
            return false;
        }
        if (title == null)
        {
            if (((FindingTabModel) (obj)).title != null)
            {
                return false;
            }
        } else
        if (!title.equals(((FindingTabModel) (obj)).title))
        {
            return false;
        }
        if (url != null)
        {
            continue; /* Loop/switch isn't completed */
        }
        if (((FindingTabModel) (obj)).url == null) goto _L1; else goto _L3
_L3:
        return false;
        if (url.equals(((FindingTabModel) (obj)).url)) goto _L1; else goto _L4
_L4:
        return false;
    }

    public String getTitle()
    {
        return title;
    }

    public int getType()
    {
        if (!TextUtils.isEmpty(contentType) && !"recommend".equals(contentType))
        {
            if ("category".equals(contentType))
            {
                return 2;
            }
            if ("live".equals(contentType))
            {
                return 5;
            }
            if ("ranking".equals(contentType))
            {
                return 4;
            }
            if ("anchor".equals(contentType))
            {
                return 6;
            }
            if ("html5".equals(contentType))
            {
                return 7;
            }
            if ("special".equals(contentType))
            {
                return 8;
            }
            if ("activity".equals(contentType))
            {
                return 9;
            }
            if ("xzone".equals(contentType))
            {
                return 10;
            }
        }
        return 3;
    }

    public String getUrl()
    {
        return url;
    }

    public int hashCode()
    {
        int k = 0;
        int i;
        int j;
        if (contentType == null)
        {
            i = 0;
        } else
        {
            i = contentType.hashCode();
        }
        if (title == null)
        {
            j = 0;
        } else
        {
            j = title.hashCode();
        }
        if (url != null)
        {
            k = url.hashCode();
        }
        return (j + (i + 31) * 31) * 31 + k;
    }

    public void setContentType(String s)
    {
        contentType = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setUrl(String s)
    {
        url = s;
    }
}
