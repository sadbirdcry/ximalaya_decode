// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding;

import android.text.TextUtils;
import com.alibaba.fastjson.JSONObject;
import java.util.List;

public class FindingCategoryModel
{

    public static final String ALBUM = "album";
    public static final String TRACK = "track";
    public static final int TYPE_ALBUM = 0;
    public static final int TYPE_TRACK = 1;
    private String contentType;
    private String coverPath;
    private long id;
    private boolean isChecked;
    private boolean isFinished;
    private String name;
    private int orderNum;
    private String title;

    public FindingCategoryModel()
    {
    }

    public static final List getListFromJson(String s)
    {
        return JSONObject.parseArray(s, com/ximalaya/ting/android/model/finding/FindingCategoryModel);
    }

    public String getContentType()
    {
        return contentType;
    }

    public String getCoverPath()
    {
        return coverPath;
    }

    public long getId()
    {
        return id;
    }

    public boolean getIsChecked()
    {
        return isChecked;
    }

    public String getName()
    {
        return name;
    }

    public int getOrderNum()
    {
        return orderNum;
    }

    public String getTitle()
    {
        return title;
    }

    public int getType()
    {
        while (TextUtils.isEmpty(contentType) || "album".equals(contentType) || !"track".equals(contentType)) 
        {
            return 0;
        }
        return 1;
    }

    public boolean isFinished()
    {
        return isFinished;
    }

    public void setContentType(String s)
    {
        contentType = s;
    }

    public void setCoverPath(String s)
    {
        coverPath = s;
    }

    public void setId(long l)
    {
        id = l;
    }

    public void setIsChecked(boolean flag)
    {
        isChecked = flag;
    }

    public void setIsFinished(boolean flag)
    {
        isFinished = flag;
    }

    public void setName(String s)
    {
        name = s;
    }

    public void setOrderNum(int i)
    {
        orderNum = i;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public String toString()
    {
        return (new StringBuilder()).append("FindingCategoryModel [coverPath=").append(coverPath).append(", id=").append(id).append(", isChecked=").append(isChecked).append(", name=").append(name).append(", title=").append(title).append(", contentType=").append(contentType).append(", orderNum=").append(orderNum).append("]").toString();
    }
}
