// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model;


public class NoReadModel
{

    public int albums;
    public String backgroundLogo;
    public int contents;
    public String errorCode;
    public int events;
    public boolean favoriteAlbumIsUpdate;
    public int favorites;
    public int followers;
    public int followingTags;
    public int followings;
    public boolean isFollowed;
    public boolean isVerified;
    public int leters;
    public int listeneds;
    public int messages;
    public String mobileLargeLogo;
    public String mobileSmallLogo;
    public String msg;
    public int newEventCount;
    public boolean newEventCountChanged;
    public int newFeedCount;
    public int newThirdRegisters;
    public int newUpdatedAlbums;
    public int newZoneCommentCount;
    public String nickname;
    public int noReadFollowers;
    public String personDescribe;
    public String personalSignature;
    public String ptitle;
    public int ret;
    public int totalSpace;
    public int tracks;
    public long uid;
    public int usedSpace;

    public NoReadModel()
    {
        newEventCountChanged = false;
    }
}
