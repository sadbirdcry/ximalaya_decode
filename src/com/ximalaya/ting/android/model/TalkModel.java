// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model;

import com.ximalaya.ting.android.util.Utilities;

// Referenced classes of package com.ximalaya.ting.android.model:
//            BaseModel

public class TalkModel extends BaseModel
{

    public boolean SendMsg_FLAG;
    public String avatarPath;
    public String content;
    public String createdAt;
    public boolean flag;
    public boolean hasRead;
    public String id;
    public boolean isIn;
    public String myKey;
    public String nickname;
    public String time;
    public String uid;
    public String updatedAt;
    public String withAvatarPath;
    public String withNickname;
    public String withUid;

    public TalkModel()
    {
        flag = true;
        myKey = "";
        SendMsg_FLAG = true;
    }

    public boolean equals(Object obj)
    {
        boolean flag2 = true;
        if (obj != null && this != null) goto _L2; else goto _L1
_L1:
        boolean flag1 = false;
_L4:
        return flag1;
_L2:
        flag1 = flag2;
        if (obj == this) goto _L4; else goto _L3
_L3:
        if (!(obj instanceof TalkModel))
        {
            break; /* Loop/switch isn't completed */
        }
        obj = (TalkModel)obj;
        if (id == null || Utilities.isBlank(((TalkModel) (obj)).getId()))
        {
            return false;
        }
        flag1 = flag2;
        if (((TalkModel) (obj)).getId().equals(id)) goto _L4; else goto _L5
_L5:
        return false;
    }

    public String getAvatarPath()
    {
        return avatarPath;
    }

    public String getContent()
    {
        return content;
    }

    public String getCreatedAt()
    {
        return createdAt;
    }

    public String getId()
    {
        return id;
    }

    public boolean getIsIn()
    {
        return isIn;
    }

    public String getNickname()
    {
        return nickname;
    }

    public String getUid()
    {
        return uid;
    }

    public String getUpdatedAt()
    {
        return updatedAt;
    }

    public String getWithAvatarPath()
    {
        return withAvatarPath;
    }

    public String getWithNickname()
    {
        return withNickname;
    }

    public String getWithUid()
    {
        return withUid;
    }

    public boolean isHasRead()
    {
        return hasRead;
    }

    public void setAvatarPath(String s)
    {
        avatarPath = s;
    }

    public void setContent(String s)
    {
        content = s;
    }

    public void setCreatedAt(String s)
    {
        createdAt = s;
    }

    public void setHasRead(boolean flag1)
    {
        hasRead = flag1;
    }

    public void setId(String s)
    {
        id = s;
    }

    public void setIsIn(boolean flag1)
    {
        isIn = flag1;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setUid(String s)
    {
        uid = s;
    }

    public void setUpdatedAt(String s)
    {
        updatedAt = s;
    }

    public void setWithAvatarPath(String s)
    {
        withAvatarPath = s;
    }

    public void setWithNickname(String s)
    {
        withNickname = s;
    }

    public void setWithUid(String s)
    {
        withUid = s;
    }
}
