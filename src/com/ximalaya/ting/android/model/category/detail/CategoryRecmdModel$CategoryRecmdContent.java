// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.category.detail;

import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.category.detail:
//            CategoryRecmdModel

public static class 
{

    public static final int MODULE_RANKLIST = 2;
    public static final int MODULE_TINGLIST = 4;
    private String calcDimension;
    private boolean hasMore;
    private List list;
    private int moduleType;
    private String tagName;
    private String title;

    public String getCalcDimension()
    {
        return calcDimension;
    }

    public List getList()
    {
        return list;
    }

    public int getModuleType()
    {
        return moduleType;
    }

    public String getTagName()
    {
        return tagName;
    }

    public String getTitle()
    {
        return title;
    }

    public boolean isHasMore()
    {
        return hasMore;
    }

    public void setCalcDimension(String s)
    {
        calcDimension = s;
    }

    public void setHasMore(boolean flag)
    {
        hasMore = flag;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setModuleType(int i)
    {
        moduleType = i;
    }

    public void setTagName(String s)
    {
        tagName = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public ()
    {
    }
}
