// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.category.detail;

import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdFocusImgGroup;
import java.util.List;

public class CategoryRecmdModel extends BaseModel
{
    public static class CategoryContentList
    {

        private List list;
        private int ret;
        private String title;

        public List getList()
        {
            return list;
        }

        public int getRet()
        {
            return ret;
        }

        public String getTitle()
        {
            return title;
        }

        public void setList(List list1)
        {
            list = list1;
        }

        public void setRet(int i)
        {
            ret = i;
        }

        public void setTitle(String s)
        {
            title = s;
        }

        public CategoryContentList()
        {
            ret = 0;
        }
    }

    public static class CategoryRecmdContent
    {

        public static final int MODULE_RANKLIST = 2;
        public static final int MODULE_TINGLIST = 4;
        private String calcDimension;
        private boolean hasMore;
        private List list;
        private int moduleType;
        private String tagName;
        private String title;

        public String getCalcDimension()
        {
            return calcDimension;
        }

        public List getList()
        {
            return list;
        }

        public int getModuleType()
        {
            return moduleType;
        }

        public String getTagName()
        {
            return tagName;
        }

        public String getTitle()
        {
            return title;
        }

        public boolean isHasMore()
        {
            return hasMore;
        }

        public void setCalcDimension(String s)
        {
            calcDimension = s;
        }

        public void setHasMore(boolean flag)
        {
            hasMore = flag;
        }

        public void setList(List list1)
        {
            list = list1;
        }

        public void setModuleType(int i)
        {
            moduleType = i;
        }

        public void setTagName(String s)
        {
            tagName = s;
        }

        public void setTitle(String s)
        {
            title = s;
        }

        public CategoryRecmdContent()
        {
        }
    }

    public static class TagModelList
    {

        private int count;
        private boolean hasRecommendedZones;
        private List list;
        private int maxPageId;

        public int getCount()
        {
            return count;
        }

        public List getList()
        {
            return list;
        }

        public int getMaxPageId()
        {
            return maxPageId;
        }

        public boolean isHasRecommendedZones()
        {
            return hasRecommendedZones;
        }

        public void setCount(int i)
        {
            count = i;
        }

        public void setHasRecommendedZones(boolean flag)
        {
            hasRecommendedZones = flag;
        }

        public void setList(List list1)
        {
            list = list1;
        }

        public void setMaxPageId(int i)
        {
            maxPageId = i;
        }

        public TagModelList()
        {
        }
    }


    private CategoryContentList categoryContents;
    private RecmdFocusImgGroup focusImages;
    private TagModelList tags;

    public CategoryRecmdModel()
    {
    }

    public CategoryContentList getCategoryContents()
    {
        return categoryContents;
    }

    public RecmdFocusImgGroup getFocusImages()
    {
        return focusImages;
    }

    public TagModelList getTags()
    {
        return tags;
    }

    public void setCategoryContents(CategoryContentList categorycontentlist)
    {
        categoryContents = categorycontentlist;
    }

    public void setFocusImages(RecmdFocusImgGroup recmdfocusimggroup)
    {
        focusImages = recmdfocusimggroup;
    }

    public void setTags(TagModelList tagmodellist)
    {
        tags = tagmodellist;
    }
}
