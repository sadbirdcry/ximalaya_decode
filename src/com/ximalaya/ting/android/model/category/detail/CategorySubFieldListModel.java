// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.category.detail;

import java.util.ArrayList;
import java.util.List;

public class CategorySubFieldListModel
{

    private String contentType;
    private int count;
    private List list;
    private String title;

    public CategorySubFieldListModel()
    {
        list = new ArrayList();
    }

    public String getContentType()
    {
        return contentType;
    }

    public int getCount()
    {
        return count;
    }

    public List getList()
    {
        return list;
    }

    public String getTitle()
    {
        return title;
    }

    public boolean isTrack()
    {
        return "track".equals(contentType);
    }

    public void setContentType(String s)
    {
        contentType = s;
    }

    public void setCount(int i)
    {
        count = i;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setTitle(String s)
    {
        title = s;
    }
}
