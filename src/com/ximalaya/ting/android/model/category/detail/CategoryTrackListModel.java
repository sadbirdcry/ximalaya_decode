// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.category.detail;

import com.ximalaya.ting.android.model.BaseModel;
import java.util.List;

public class CategoryTrackListModel extends BaseModel
{

    private List list;
    private int maxPageId;
    private int pageId;
    private int pageSize;
    private List subfields;
    private String title;
    private int totalCount;

    public CategoryTrackListModel()
    {
    }

    public List getList()
    {
        return list;
    }

    public int getMaxPageId()
    {
        return maxPageId;
    }

    public int getPageId()
    {
        return pageId;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public List getSubfields()
    {
        return subfields;
    }

    public String getTitle()
    {
        return title;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setMaxPageId(int i)
    {
        maxPageId = i;
    }

    public void setPageId(int i)
    {
        pageId = i;
    }

    public void setPageSize(int i)
    {
        pageSize = i;
    }

    public void setSubfields(List list1)
    {
        subfields = list1;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTotalCount(int i)
    {
        totalCount = i;
    }
}
