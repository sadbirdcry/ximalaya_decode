// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.category.detail;

import com.ximalaya.ting.android.model.Collectable;
import java.io.Serializable;

public class CategoryAlbumModel
    implements Collectable, Serializable
{

    public static final int TYPE_AD = 1;
    public static final int TYPE_ALBUM = 0;
    private static final long serialVersionUID = 0x6714754d751aee7dL;
    private long albumId;
    private String contentType;
    private String coverMiddle;
    private String coverPathSmall;
    private String footnote;
    private String intro;
    private boolean isCollected;
    private int isFinished;
    private String key;
    private long lastUptrackAt;
    private long lastUptrackId;
    private String lastUptrackTitle;
    private Object mTag;
    private int mType;
    private long playsCounts;
    private long specialId;
    private String subtitle;
    private String tags;
    private String title;
    private int tracks;

    public CategoryAlbumModel()
    {
    }

    public long getAlbumId()
    {
        return albumId;
    }

    public long getCId()
    {
        return albumId;
    }

    public String getContentType()
    {
        return contentType;
    }

    public String getCoverMiddle()
    {
        return coverMiddle;
    }

    public String getCoverPathSmall()
    {
        return coverPathSmall;
    }

    public String getFootnote()
    {
        return footnote;
    }

    public String getIntro()
    {
        return intro;
    }

    public int getIsFinished()
    {
        return isFinished;
    }

    public String getKey()
    {
        return key;
    }

    public long getLastUptrackAt()
    {
        return lastUptrackAt;
    }

    public long getLastUptrackId()
    {
        return lastUptrackId;
    }

    public String getLastUptrackTitle()
    {
        return lastUptrackTitle;
    }

    public long getPlaysCounts()
    {
        return playsCounts;
    }

    public long getSpecialId()
    {
        return specialId;
    }

    public String getSubtitle()
    {
        return subtitle;
    }

    public Object getTag()
    {
        return mTag;
    }

    public String getTags()
    {
        return tags;
    }

    public String getTitle()
    {
        return title;
    }

    public int getTracks()
    {
        return tracks;
    }

    public int getType()
    {
        return mType;
    }

    public boolean isCCollected()
    {
        return isCollected;
    }

    public void setAlbumId(long l)
    {
        albumId = l;
    }

    public void setCCollected(boolean flag)
    {
        isCollected = flag;
    }

    public void setContentType(String s)
    {
        contentType = s;
    }

    public void setCoverMiddle(String s)
    {
        coverMiddle = s;
    }

    public void setCoverPathSmall(String s)
    {
        coverPathSmall = s;
    }

    public void setFootnote(String s)
    {
        footnote = s;
    }

    public void setIntro(String s)
    {
        intro = s;
    }

    public void setIsFinished(int i)
    {
        isFinished = i;
    }

    public void setKey(String s)
    {
        key = s;
    }

    public void setLastUptrackAt(long l)
    {
        lastUptrackAt = l;
    }

    public void setLastUptrackId(long l)
    {
        lastUptrackId = l;
    }

    public void setLastUptrackTitle(String s)
    {
        lastUptrackTitle = s;
    }

    public void setPlaysCounts(long l)
    {
        playsCounts = l;
    }

    public void setSpecialId(long l)
    {
        specialId = l;
    }

    public void setSubtitle(String s)
    {
        subtitle = s;
    }

    public void setTag(Object obj)
    {
        mTag = obj;
    }

    public void setTags(String s)
    {
        tags = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTracks(int i)
    {
        tracks = i;
    }

    public void setType(int i)
    {
        mType = i;
    }
}
