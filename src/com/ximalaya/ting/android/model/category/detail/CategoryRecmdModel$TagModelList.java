// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.category.detail;

import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.category.detail:
//            CategoryRecmdModel

public static class 
{

    private int count;
    private boolean hasRecommendedZones;
    private List list;
    private int maxPageId;

    public int getCount()
    {
        return count;
    }

    public List getList()
    {
        return list;
    }

    public int getMaxPageId()
    {
        return maxPageId;
    }

    public boolean isHasRecommendedZones()
    {
        return hasRecommendedZones;
    }

    public void setCount(int i)
    {
        count = i;
    }

    public void setHasRecommendedZones(boolean flag)
    {
        hasRecommendedZones = flag;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setMaxPageId(int i)
    {
        maxPageId = i;
    }

    public ()
    {
    }
}
