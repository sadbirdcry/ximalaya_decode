// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.category;

import com.ximalaya.ting.android.model.BaseModel;

public class CategoryModel extends BaseModel
{

    public String coverPath;
    public long id;
    public String intro;
    public boolean isChecked;
    public String name;
    public int orderNum;
    public String title;

    public CategoryModel()
    {
    }

    public String toString()
    {
        if (title == null)
        {
            return "";
        } else
        {
            return title.trim();
        }
    }
}
