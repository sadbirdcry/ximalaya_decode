// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.xdcs;

import com.ximalaya.ting.android.model.ad.AdCollectData;

// Referenced classes of package com.ximalaya.ting.android.model.xdcs:
//            BaseEvent

public class AdEvent extends BaseEvent
{

    private AdCollectData props;

    public AdEvent()
    {
    }

    public AdCollectData getProps()
    {
        return props;
    }

    public void setProps(AdCollectData adcollectdata)
    {
        props = adcollectdata;
    }

    public String toString()
    {
        return (new StringBuilder()).append(super.toString()).append("AdEvent [props=").append(props).append("]").toString();
    }
}
