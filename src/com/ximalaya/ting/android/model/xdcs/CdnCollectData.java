// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.xdcs;


public class CdnCollectData
{

    private long audioBytes;
    private String audioUrl;
    private String cdnIP;
    private float connectedTime;
    private String downloadSpeed;
    private String errorType;
    private String exceptionReason;
    private String fileSize;
    private boolean isTimeout;
    private String module;
    private String range;
    private String responseHeader;
    private String statusCode;
    private long timestamp;
    private String type;
    private String viaInfo;

    public CdnCollectData()
    {
    }

    public long getAudioBytes()
    {
        return audioBytes;
    }

    public String getAudioUrl()
    {
        return audioUrl;
    }

    public String getCdnIP()
    {
        return cdnIP;
    }

    public float getConnectedTime()
    {
        return connectedTime;
    }

    public String getDownloadSpeed()
    {
        return downloadSpeed;
    }

    public String getErrorType()
    {
        return errorType;
    }

    public String getExceptionReason()
    {
        return exceptionReason;
    }

    public String getFileSize()
    {
        return fileSize;
    }

    public String getModule()
    {
        return module;
    }

    public String getRange()
    {
        return range;
    }

    public String getResponseHeader()
    {
        return responseHeader;
    }

    public String getStatusCode()
    {
        return statusCode;
    }

    public long getTimestamp()
    {
        return timestamp;
    }

    public String getType()
    {
        return type;
    }

    public String getViaInfo()
    {
        return viaInfo;
    }

    public boolean isTimeout()
    {
        return isTimeout;
    }

    public void setAudioBytes(long l)
    {
        audioBytes = l;
    }

    public void setAudioUrl(String s)
    {
        audioUrl = s;
    }

    public void setCdnIP(String s)
    {
        cdnIP = s;
    }

    public void setConnectedTime(float f)
    {
        connectedTime = f;
    }

    public void setDownloadSpeed(String s)
    {
        downloadSpeed = s;
    }

    public void setErrorType(String s)
    {
        errorType = s;
    }

    public void setExceptionReason(String s)
    {
        exceptionReason = s;
    }

    public void setFileSize(String s)
    {
        fileSize = s;
    }

    public void setModule(String s)
    {
        module = s;
    }

    public void setRange(String s)
    {
        range = s;
    }

    public void setResponseHeader(String s)
    {
        responseHeader = s;
    }

    public void setStatusCode(String s)
    {
        statusCode = s;
    }

    public void setTimeout(boolean flag)
    {
        isTimeout = flag;
    }

    public void setTimestamp(long l)
    {
        timestamp = l;
    }

    public void setType(String s)
    {
        type = s;
    }

    public void setViaInfo(String s)
    {
        viaInfo = s;
    }

    public String toString()
    {
        return (new StringBuilder()).append("CdnCollectData [type=").append(type).append(", module=").append(module).append(", audioUrl=").append(audioUrl).append(", audioBytes=").append(audioBytes).append(", cdnIP=").append(cdnIP).append(", isTimeout=").append(isTimeout).append(", exceptionReason=").append(exceptionReason).append(", responseHeader=").append(responseHeader).append(", statusCode=").append(statusCode).append("]").toString();
    }
}
