// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.xdcs;

import java.util.HashMap;
import java.util.Map;

// Referenced classes of package com.ximalaya.ting.android.model.xdcs:
//            BaseEvent

public class CommonEvent extends BaseEvent
{

    private Map props;

    public CommonEvent()
    {
        props = new HashMap();
    }

    public Map getProps()
    {
        return props;
    }

    public void setProps(Map map)
    {
        props = map;
    }

    public String toString()
    {
        return (new StringBuilder()).append(super.toString()).append("CommonEvent [props=").append(props).append("]").toString();
    }
}
