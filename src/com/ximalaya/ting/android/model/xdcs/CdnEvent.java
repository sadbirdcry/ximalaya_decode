// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.xdcs;


// Referenced classes of package com.ximalaya.ting.android.model.xdcs:
//            BaseEvent, CdnCollectData

public class CdnEvent extends BaseEvent
{

    private CdnCollectData props;

    public CdnEvent()
    {
    }

    public CdnCollectData getProps()
    {
        return props;
    }

    public void setProps(CdnCollectData cdncollectdata)
    {
        props = cdncollectdata;
    }

    public String toString()
    {
        return (new StringBuilder()).append(super.toString()).append("CdnEvent [props=").append(props).append("]").toString();
    }
}
