// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.xdcs;


public class HttpHeaderModel
{

    private String from;
    private String id;
    private String parentId;
    private String position;
    private int searchpage;
    private String spanId;
    private String title;
    private String traceId;
    private String viewId;

    public HttpHeaderModel()
    {
    }

    public String getFrom()
    {
        return from;
    }

    public String getId()
    {
        return id;
    }

    public String getParentId()
    {
        return parentId;
    }

    public String getPosition()
    {
        return position;
    }

    public int getSearchpage()
    {
        return searchpage;
    }

    public String getSpanId()
    {
        return spanId;
    }

    public String getTitle()
    {
        return title;
    }

    public String getTraceId()
    {
        return traceId;
    }

    public String getViewId()
    {
        return viewId;
    }

    public void setFrom(String s)
    {
        from = s;
    }

    public void setId(String s)
    {
        id = s;
    }

    public void setParentId(String s)
    {
        parentId = s;
    }

    public void setPosition(String s)
    {
        position = s;
    }

    public void setSearchpage(int i)
    {
        searchpage = i;
    }

    public void setSpanId(String s)
    {
        spanId = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTraceId(String s)
    {
        traceId = s;
    }

    public void setViewId(String s)
    {
        viewId = s;
    }

    public String toString()
    {
        return (new StringBuilder()).append("HttpHeaderModel [viewId=").append(viewId).append(", traceId=").append(traceId).append(", parentId=").append(parentId).append(", spanId=").append(spanId).append(", position=").append(position).append(", title=").append(title).append("]").toString();
    }
}
