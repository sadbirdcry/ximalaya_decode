// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.xdcs;


public abstract class BaseEvent
{

    private String parentSpanId;
    private int seqId;
    private String spanId;
    private String traceId;
    private long ts;
    private String type;
    private String viewId;

    public BaseEvent()
    {
    }

    public String getParentSpanId()
    {
        return parentSpanId;
    }

    public int getSeqId()
    {
        return seqId;
    }

    public String getSpanId()
    {
        return spanId;
    }

    public String getTraceId()
    {
        return traceId;
    }

    public long getTs()
    {
        return ts;
    }

    public String getType()
    {
        return type;
    }

    public String getViewId()
    {
        return viewId;
    }

    public void setParentSpanId(String s)
    {
        parentSpanId = s;
    }

    public void setSeqId(int i)
    {
        seqId = i;
    }

    public void setSpanId(String s)
    {
        spanId = s;
    }

    public void setTraceId(String s)
    {
        traceId = s;
    }

    public void setTs(long l)
    {
        ts = l;
    }

    public void setType(String s)
    {
        type = s;
    }

    public void setViewId(String s)
    {
        viewId = s;
    }

    public String toString()
    {
        return (new StringBuilder()).append("BaseEvent [viewId=").append(viewId).append(", parentSpanId=").append(parentSpanId).append(", seqId=").append(seqId).append(", spanId=").append(spanId).append(", traceId=").append(traceId).append(", ts=").append(ts).append(", type=").append(type).append("]").toString();
    }
}
