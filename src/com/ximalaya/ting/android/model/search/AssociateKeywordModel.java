// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.search;


public class AssociateKeywordModel
{

    private String category;
    private String highlightKeyword;
    private long id;
    private String imgPath;
    private boolean isAlbum;
    private String keyword;

    public AssociateKeywordModel()
    {
        isAlbum = false;
    }

    public String getCategory()
    {
        return category;
    }

    public String getHighlightKeyword()
    {
        return highlightKeyword;
    }

    public long getId()
    {
        return id;
    }

    public String getImgPath()
    {
        return imgPath;
    }

    public String getKeyword()
    {
        return keyword;
    }

    public boolean isAlbum()
    {
        return isAlbum;
    }

    public void setAlbum(boolean flag)
    {
        isAlbum = flag;
    }

    public void setCategory(String s)
    {
        category = s;
    }

    public void setHighlightKeyword(String s)
    {
        highlightKeyword = s;
    }

    public void setId(long l)
    {
        id = l;
    }

    public void setImgPath(String s)
    {
        imgPath = s;
    }

    public void setKeyword(String s)
    {
        keyword = s;
    }
}
