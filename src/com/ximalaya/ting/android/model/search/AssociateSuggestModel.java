// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.search;

import java.util.ArrayList;
import java.util.List;

public class AssociateSuggestModel
{

    private List albumResultList;
    private int albumResultNum;
    private List queryResultList;
    private int queryResultNum;

    public AssociateSuggestModel()
    {
        albumResultList = new ArrayList();
        queryResultList = new ArrayList();
    }

    public List getAlbumResultList()
    {
        return albumResultList;
    }

    public int getAlbumResultNum()
    {
        return albumResultNum;
    }

    public List getQueryResultList()
    {
        return queryResultList;
    }

    public int getQueryResultNum()
    {
        return queryResultNum;
    }

    public void setAlbumResultList(List list)
    {
        albumResultList = list;
    }

    public void setAlbumResultNum(int i)
    {
        albumResultNum = i;
    }

    public void setQueryResultList(List list)
    {
        queryResultList = list;
    }

    public void setQueryResultNum(int i)
    {
        queryResultNum = i;
    }
}
