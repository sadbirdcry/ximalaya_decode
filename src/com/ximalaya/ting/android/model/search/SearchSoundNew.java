// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.search;


public class SearchSoundNew
{

    public int album_id;
    public String album_title;
    public String avatar_url;
    public int category_id;
    public String category_title;
    public int comments_count;
    public String cover_url_large;
    public String cover_url_middle;
    public String cover_url_small;
    public String created_at;
    public double duration;
    public int favorites_count;
    public long id;
    public String nickname;
    public String play_size_32;
    public String play_size_64;
    public String play_url_32;
    public String play_url_64;
    public int plays_count;
    public String title;
    public long uid;

    public SearchSoundNew()
    {
    }
}
