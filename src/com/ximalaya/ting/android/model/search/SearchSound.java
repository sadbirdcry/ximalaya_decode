// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.search;

import com.ximalaya.ting.android.model.Likeable;
import java.util.List;

public class SearchSound
    implements Likeable
{

    public String album_cover_path;
    public int album_id;
    public String album_title;
    public List all_track;
    public String avatar_path;
    public int category_id;
    public int count_comment;
    public int count_like;
    public int count_play;
    public int count_share;
    public String cover_path;
    public long created_at;
    public String download_path;
    public double duration;
    public long id;
    public boolean isRelay;
    public boolean is_like;
    public boolean is_playing;
    public boolean is_public;
    public boolean is_v;
    public String nickname;
    public String play_path;
    public String play_path_32;
    public String play_path_64;
    public String smallLogo;
    public String title;
    public long uid;
    public long updated_at;
    public int upload_source;
    public int user_source;

    public SearchSound()
    {
    }

    public long getId()
    {
        return id;
    }

    public boolean isLiked()
    {
        return is_like;
    }

    public void toggleLikeStatus()
    {
        boolean flag;
        if (!is_like)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        is_like = flag;
    }
}
