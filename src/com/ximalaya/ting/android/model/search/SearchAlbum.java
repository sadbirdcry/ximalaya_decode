// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.search;


public class SearchAlbum
{

    public static final int TYPE_HOTTEST = 1;
    public String avatar_path;
    public int category_id;
    public String cover_path;
    public long created_at;
    public long id;
    public String intro;
    public boolean isFavorite;
    public boolean is_public;
    public boolean is_v;
    public String nickname;
    public int officialType;
    public int play;
    public String tags;
    public String title;
    public int tracks;
    public long uid;
    public long updated_at;
    public int user_source;

    public SearchAlbum()
    {
        officialType = 0;
    }
}
