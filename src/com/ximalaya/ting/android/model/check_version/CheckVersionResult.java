// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.check_version;

import com.ximalaya.ting.android.model.BaseModel;

public class CheckVersionResult extends BaseModel
{

    public String download;
    public String errorCode;
    public boolean forceUpdate;
    public boolean hasNewVersion;
    public boolean needNotify;
    public String upgradeDesc;
    public String upgradeDescUrl;
    public String version;

    public CheckVersionResult()
    {
    }
}
