// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.check_version;


public class MsgCarry
{

    private String downloadUrl;
    private String info;
    private int percent;
    private String serverVersion;

    public MsgCarry()
    {
    }

    public MsgCarry(String s, String s1, int i)
    {
        serverVersion = s;
        downloadUrl = s1;
        percent = i;
    }

    public String getDownloadUrl()
    {
        return downloadUrl;
    }

    public String getInfo()
    {
        return info;
    }

    public int getPercent()
    {
        return percent;
    }

    public String getServerVersion()
    {
        return serverVersion;
    }

    public void setDownloadUrl(String s)
    {
        downloadUrl = s;
    }

    public void setInfo(String s)
    {
        info = s;
    }

    public void setPercent(int i)
    {
        percent = i;
    }

    public void setServerVersion(String s)
    {
        serverVersion = s;
    }
}
