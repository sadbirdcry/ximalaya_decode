// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.album;


// Referenced classes of package com.ximalaya.ting.android.model.album:
//            AlbumSectionModel

public static class 
{

    private long albumId;
    private String albumImage;
    private String albumTitle;
    private long comments;
    private String coverLarge;
    private String coverSmall;
    private long createdAt;
    private long downloadSize;
    private String downloadUrl;
    private long duration;
    private boolean isChecked;
    private boolean isDownload;
    private boolean isLike;
    private boolean isPublic;
    private boolean isRelay;
    private long likes;
    private String nickname;
    private int opType;
    private int orderNum;
    private long playtimes;
    private int processState;
    private long shares;
    private String smallLogo;
    private int status;
    private String title;
    private long trackId;
    private long uid;
    private int userSource;

    public long getAlbumId()
    {
        return albumId;
    }

    public String getAlbumImage()
    {
        return albumImage;
    }

    public String getAlbumTitle()
    {
        return albumTitle;
    }

    public long getComments()
    {
        return comments;
    }

    public String getCoverLarge()
    {
        return coverLarge;
    }

    public String getCoverSmall()
    {
        return coverSmall;
    }

    public long getCreatedAt()
    {
        return createdAt;
    }

    public long getDownloadSize()
    {
        return downloadSize;
    }

    public String getDownloadUrl()
    {
        return downloadUrl;
    }

    public long getDuration()
    {
        return duration;
    }

    public boolean getIsChecked()
    {
        return isChecked;
    }

    public boolean getIsLike()
    {
        return isLike;
    }

    public boolean getIsPublic()
    {
        return isPublic;
    }

    public boolean getIsRelay()
    {
        return isRelay;
    }

    public long getLikes()
    {
        return likes;
    }

    public String getNickname()
    {
        return nickname;
    }

    public int getOpType()
    {
        return opType;
    }

    public int getOrderNum()
    {
        return orderNum;
    }

    public long getPlaytimes()
    {
        return playtimes;
    }

    public int getProcessState()
    {
        return processState;
    }

    public long getShares()
    {
        return shares;
    }

    public String getSmallLogo()
    {
        return smallLogo;
    }

    public int getStatus()
    {
        return status;
    }

    public String getTitle()
    {
        return title;
    }

    public long getTrackId()
    {
        return trackId;
    }

    public long getUid()
    {
        return uid;
    }

    public int getUserSource()
    {
        return userSource;
    }

    public boolean isDownload()
    {
        return isDownload;
    }

    public void setAlbumId(long l)
    {
        albumId = l;
    }

    public void setAlbumImage(String s)
    {
        albumImage = s;
    }

    public void setAlbumTitle(String s)
    {
        albumTitle = s;
    }

    public void setComments(long l)
    {
        comments = l;
    }

    public void setCoverLarge(String s)
    {
        coverLarge = s;
    }

    public void setCoverSmall(String s)
    {
        coverSmall = s;
    }

    public void setCreatedAt(long l)
    {
        createdAt = l;
    }

    public void setDownloadSize(long l)
    {
        downloadSize = l;
    }

    public void setDownloadUrl(String s)
    {
        downloadUrl = s;
    }

    public void setDuration(long l)
    {
        duration = l;
    }

    public void setIsChecked(boolean flag)
    {
        isChecked = flag;
    }

    public void setIsDownload(boolean flag)
    {
        isDownload = flag;
    }

    public void setIsLike(boolean flag)
    {
        isLike = flag;
    }

    public void setIsPublic(boolean flag)
    {
        isPublic = flag;
    }

    public void setIsRelay(boolean flag)
    {
        isRelay = flag;
    }

    public void setLikes(long l)
    {
        likes = l;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setOpType(int i)
    {
        opType = i;
    }

    public void setOrderNum(int i)
    {
        orderNum = i;
    }

    public void setPlaytimes(long l)
    {
        playtimes = l;
    }

    public void setProcessState(int i)
    {
        processState = i;
    }

    public void setShares(long l)
    {
        shares = l;
    }

    public void setSmallLogo(String s)
    {
        smallLogo = s;
    }

    public void setStatus(int i)
    {
        status = i;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTrackId(long l)
    {
        trackId = l;
    }

    public void setUid(long l)
    {
        uid = l;
    }

    public void setUserSource(int i)
    {
        userSource = i;
    }

    public ()
    {
    }
}
