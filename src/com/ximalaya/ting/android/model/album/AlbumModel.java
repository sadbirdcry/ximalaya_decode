// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.album;

import com.ximalaya.ting.android.model.ad.FeedAd2;
import java.io.Serializable;

public class AlbumModel
    implements Serializable
{

    public static final int TYPE_AD = 1;
    private static final long serialVersionUID = 1L;
    public long albumId;
    public String avatarPath;
    public long categoryId;
    public String coverLarge;
    public String coverSmall;
    public String coverWebLarge;
    public long createdAt;
    public boolean hasNew;
    public String intro;
    public String introRich;
    public boolean isFavorite;
    public boolean isPublic;
    public boolean isRecommend;
    public boolean isRecommendTitle;
    public boolean isVerified;
    public long lastUptrackAt;
    public String lastUptrackTitle;
    public String nickname;
    public int playTimes;
    public String recSrc;
    public String recTrack;
    public int shares;
    public int status;
    public Object tag;
    public String tags;
    public String title;
    public int tracks;
    public int type;
    public long uid;
    public long updatedAt;
    public int zoneId;

    public AlbumModel()
    {
        status = 1;
        isRecommend = false;
        isRecommendTitle = false;
    }

    public static AlbumModel from(FeedAd2 feedad2)
    {
        AlbumModel albummodel = new AlbumModel();
        albummodel.type = 1;
        albummodel.tag = feedad2;
        albummodel.title = feedad2.name;
        albummodel.nickname = feedad2.description;
        albummodel.coverSmall = feedad2.cover;
        return albummodel;
    }

    public boolean equals(Object obj)
    {
        if (obj != null)
        {
            if (this == obj)
            {
                return true;
            }
            if ((obj instanceof AlbumModel) && albumId != 0L && albumId == ((AlbumModel)obj).albumId)
            {
                return true;
            }
        }
        return false;
    }
}
