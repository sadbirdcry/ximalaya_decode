// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.album;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.album:
//            AlbumModel

public class LocalCollectAlbumUploader extends MyAsyncTask
{

    private static final String PATH = "mobile/album/subscribe/batch";
    private Context mContext;

    public LocalCollectAlbumUploader(Context context)
    {
        mContext = context;
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        Object obj = AlbumModelManage.getInstance().getLocalCollectAlbumList();
        if (obj != null && ((List) (obj)).size() > 0)
        {
            avoid = new RequestParams();
            StringBuffer stringbuffer = new StringBuffer();
            AlbumModel albummodel;
            for (obj = ((List) (obj)).iterator(); ((Iterator) (obj)).hasNext(); stringbuffer.append(albummodel.albumId))
            {
                albummodel = (AlbumModel)((Iterator) (obj)).next();
                if (!TextUtils.isEmpty(stringbuffer))
                {
                    stringbuffer.append(",");
                }
            }

            avoid.put("albumIds", stringbuffer.toString());
            avoid = f.a().a((new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/album/subscribe/batch").toString(), avoid, null);
            if (((com.ximalaya.ting.android.b.n.a) (avoid)).b == 1 && !TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (avoid)).a))
            {
                try
                {
                    if (JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (avoid)).a).getInteger("ret").intValue() == 0)
                    {
                        AlbumModelManage.getInstance().deleteAllLocalAlbumList();
                        SharedPreferencesUtil.getInstance(mContext).saveBoolean("allow_local_album_collect", false);
                    }
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                    return null;
                }
            }
        }
        return null;
    }
}
