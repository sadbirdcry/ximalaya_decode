// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.album;


// Referenced classes of package com.ximalaya.ting.android.model.album:
//            AlbumSectionModel

public static class 
{

    private long albumId;
    private String avatarPath;
    private long categoryId;
    private String categoryName;
    private String coverLarge;
    private String coverOrigin;
    private String coverSmall;
    private String coverWebLarge;
    private long createdAt;
    private String intro;
    private boolean isFavorite;
    private boolean isNew;
    private boolean isVerified;
    private long lastUptrackAt;
    private String nickname;
    private long playTimes;
    private long shares;
    private int status;
    private String tags;
    private String title;
    private int tracks;
    private long uid;
    private long updatedAt;

    public long getAlbumId()
    {
        return albumId;
    }

    public String getAvatarPath()
    {
        return avatarPath;
    }

    public long getCategoryId()
    {
        return categoryId;
    }

    public String getCategoryName()
    {
        return categoryName;
    }

    public String getCoverLarge()
    {
        return coverLarge;
    }

    public String getCoverOrigin()
    {
        return coverOrigin;
    }

    public String getCoverSmall()
    {
        return coverSmall;
    }

    public String getCoverWebLarge()
    {
        return coverWebLarge;
    }

    public long getCreatedAt()
    {
        return createdAt;
    }

    public String getIntro()
    {
        return intro;
    }

    public boolean getIsFavorite()
    {
        return isFavorite;
    }

    public boolean getIsNew()
    {
        return isNew;
    }

    public boolean getIsVerified()
    {
        return isVerified;
    }

    public long getLastUptrackAt()
    {
        return lastUptrackAt;
    }

    public String getNickname()
    {
        return nickname;
    }

    public long getPlayTimes()
    {
        return playTimes;
    }

    public long getShares()
    {
        return shares;
    }

    public int getStatus()
    {
        return status;
    }

    public String getTags()
    {
        return tags;
    }

    public String getTitle()
    {
        return title;
    }

    public int getTracks()
    {
        return tracks;
    }

    public long getUid()
    {
        return uid;
    }

    public long getUpdatedAt()
    {
        return updatedAt;
    }

    public void setAlbumId(long l)
    {
        albumId = l;
    }

    public void setAvatarPath(String s)
    {
        avatarPath = s;
    }

    public void setCategoryId(long l)
    {
        categoryId = l;
    }

    public void setCategoryName(String s)
    {
        categoryName = s;
    }

    public void setCoverLarge(String s)
    {
        coverLarge = s;
    }

    public void setCoverOrigin(String s)
    {
        coverOrigin = s;
    }

    public void setCoverSmall(String s)
    {
        coverSmall = s;
    }

    public void setCoverWebLarge(String s)
    {
        coverWebLarge = s;
    }

    public void setCreatedAt(long l)
    {
        createdAt = l;
    }

    public void setIntro(String s)
    {
        intro = s;
    }

    public void setIsFavorite(boolean flag)
    {
        isFavorite = flag;
    }

    public void setIsNew(boolean flag)
    {
        isNew = flag;
    }

    public void setIsVerified(boolean flag)
    {
        isVerified = flag;
    }

    public void setLastUptrackAt(long l)
    {
        lastUptrackAt = l;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setPlayTimes(long l)
    {
        playTimes = l;
    }

    public void setShares(long l)
    {
        shares = l;
    }

    public void setStatus(int i)
    {
        status = i;
    }

    public void setTags(String s)
    {
        tags = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTracks(int i)
    {
        tracks = i;
    }

    public void setUid(long l)
    {
        uid = l;
    }

    public void setUpdatedAt(long l)
    {
        updatedAt = l;
    }

    public ()
    {
    }
}
