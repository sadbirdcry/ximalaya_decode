// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.album;


public class HotAlbum
{

    public String album_cover_path_290;
    public String album_cover_path_640;
    public String album_cover_path_86;
    public long album_id;
    public String album_nickname;
    public long album_uid;
    public String avatar_path;
    public int category_id;
    public String cover_path;
    public String cover_path_60;
    public String cover_path_640;
    public String cover_path_86;
    public String cover_path_large;
    public String cover_path_middle;
    public String cover_path_small;
    public String cover_path_wide;
    public long createdAt;
    public String created_at;
    public long id;
    public String intro;
    public boolean isVerified;
    public boolean is_crawler;
    public boolean is_deleted;
    public boolean is_favorite;
    public boolean is_public;
    public boolean is_publish;
    public boolean is_v;
    public String music_category;
    public String nickname;
    public int op_type;
    public int plays_counts;
    public String rich_intro;
    public String short_intro;
    public String tags;
    public String tags_json;
    public String title;
    public String tracks_order;
    public long uid;
    public long updatedAt;
    public String updated_at;
    public int user_source;

    public HotAlbum()
    {
    }
}
