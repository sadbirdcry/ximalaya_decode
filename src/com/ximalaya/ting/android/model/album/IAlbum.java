// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.album;


public interface IAlbum
{

    public abstract String getIAlbumCoverUrl();

    public abstract long getIAlbumId();

    public abstract long getIAlbumLastUpdateAt();

    public abstract long getIAlbumPlayCount();

    public abstract String getIAlbumTag();

    public abstract String getIAlbumTitle();

    public abstract long getIAlbumTrackCount();

    public abstract String getIIntro();

    public abstract boolean isIAlbumCollect();

    public abstract void setIAlbumCollect(boolean flag);
}
