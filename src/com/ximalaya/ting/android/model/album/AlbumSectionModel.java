// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.album;

import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.util.ApiUtil;
import java.util.List;

public class AlbumSectionModel extends BaseModel
{
    public static class Album
    {

        private long albumId;
        private String avatarPath;
        private long categoryId;
        private String categoryName;
        private String coverLarge;
        private String coverOrigin;
        private String coverSmall;
        private String coverWebLarge;
        private long createdAt;
        private String intro;
        private boolean isFavorite;
        private boolean isNew;
        private boolean isVerified;
        private long lastUptrackAt;
        private String nickname;
        private long playTimes;
        private long shares;
        private int status;
        private String tags;
        private String title;
        private int tracks;
        private long uid;
        private long updatedAt;

        public long getAlbumId()
        {
            return albumId;
        }

        public String getAvatarPath()
        {
            return avatarPath;
        }

        public long getCategoryId()
        {
            return categoryId;
        }

        public String getCategoryName()
        {
            return categoryName;
        }

        public String getCoverLarge()
        {
            return coverLarge;
        }

        public String getCoverOrigin()
        {
            return coverOrigin;
        }

        public String getCoverSmall()
        {
            return coverSmall;
        }

        public String getCoverWebLarge()
        {
            return coverWebLarge;
        }

        public long getCreatedAt()
        {
            return createdAt;
        }

        public String getIntro()
        {
            return intro;
        }

        public boolean getIsFavorite()
        {
            return isFavorite;
        }

        public boolean getIsNew()
        {
            return isNew;
        }

        public boolean getIsVerified()
        {
            return isVerified;
        }

        public long getLastUptrackAt()
        {
            return lastUptrackAt;
        }

        public String getNickname()
        {
            return nickname;
        }

        public long getPlayTimes()
        {
            return playTimes;
        }

        public long getShares()
        {
            return shares;
        }

        public int getStatus()
        {
            return status;
        }

        public String getTags()
        {
            return tags;
        }

        public String getTitle()
        {
            return title;
        }

        public int getTracks()
        {
            return tracks;
        }

        public long getUid()
        {
            return uid;
        }

        public long getUpdatedAt()
        {
            return updatedAt;
        }

        public void setAlbumId(long l)
        {
            albumId = l;
        }

        public void setAvatarPath(String s)
        {
            avatarPath = s;
        }

        public void setCategoryId(long l)
        {
            categoryId = l;
        }

        public void setCategoryName(String s)
        {
            categoryName = s;
        }

        public void setCoverLarge(String s)
        {
            coverLarge = s;
        }

        public void setCoverOrigin(String s)
        {
            coverOrigin = s;
        }

        public void setCoverSmall(String s)
        {
            coverSmall = s;
        }

        public void setCoverWebLarge(String s)
        {
            coverWebLarge = s;
        }

        public void setCreatedAt(long l)
        {
            createdAt = l;
        }

        public void setIntro(String s)
        {
            intro = s;
        }

        public void setIsFavorite(boolean flag)
        {
            isFavorite = flag;
        }

        public void setIsNew(boolean flag)
        {
            isNew = flag;
        }

        public void setIsVerified(boolean flag)
        {
            isVerified = flag;
        }

        public void setLastUptrackAt(long l)
        {
            lastUptrackAt = l;
        }

        public void setNickname(String s)
        {
            nickname = s;
        }

        public void setPlayTimes(long l)
        {
            playTimes = l;
        }

        public void setShares(long l)
        {
            shares = l;
        }

        public void setStatus(int i)
        {
            status = i;
        }

        public void setTags(String s)
        {
            tags = s;
        }

        public void setTitle(String s)
        {
            title = s;
        }

        public void setTracks(int i)
        {
            tracks = i;
        }

        public void setUid(long l)
        {
            uid = l;
        }

        public void setUpdatedAt(long l)
        {
            updatedAt = l;
        }

        public Album()
        {
        }
    }

    public static class Track
    {

        private long albumId;
        private String albumImage;
        private String albumTitle;
        private long comments;
        private String coverLarge;
        private String coverSmall;
        private long createdAt;
        private long downloadSize;
        private String downloadUrl;
        private long duration;
        private boolean isChecked;
        private boolean isDownload;
        private boolean isLike;
        private boolean isPublic;
        private boolean isRelay;
        private long likes;
        private String nickname;
        private int opType;
        private int orderNum;
        private long playtimes;
        private int processState;
        private long shares;
        private String smallLogo;
        private int status;
        private String title;
        private long trackId;
        private long uid;
        private int userSource;

        public long getAlbumId()
        {
            return albumId;
        }

        public String getAlbumImage()
        {
            return albumImage;
        }

        public String getAlbumTitle()
        {
            return albumTitle;
        }

        public long getComments()
        {
            return comments;
        }

        public String getCoverLarge()
        {
            return coverLarge;
        }

        public String getCoverSmall()
        {
            return coverSmall;
        }

        public long getCreatedAt()
        {
            return createdAt;
        }

        public long getDownloadSize()
        {
            return downloadSize;
        }

        public String getDownloadUrl()
        {
            return downloadUrl;
        }

        public long getDuration()
        {
            return duration;
        }

        public boolean getIsChecked()
        {
            return isChecked;
        }

        public boolean getIsLike()
        {
            return isLike;
        }

        public boolean getIsPublic()
        {
            return isPublic;
        }

        public boolean getIsRelay()
        {
            return isRelay;
        }

        public long getLikes()
        {
            return likes;
        }

        public String getNickname()
        {
            return nickname;
        }

        public int getOpType()
        {
            return opType;
        }

        public int getOrderNum()
        {
            return orderNum;
        }

        public long getPlaytimes()
        {
            return playtimes;
        }

        public int getProcessState()
        {
            return processState;
        }

        public long getShares()
        {
            return shares;
        }

        public String getSmallLogo()
        {
            return smallLogo;
        }

        public int getStatus()
        {
            return status;
        }

        public String getTitle()
        {
            return title;
        }

        public long getTrackId()
        {
            return trackId;
        }

        public long getUid()
        {
            return uid;
        }

        public int getUserSource()
        {
            return userSource;
        }

        public boolean isDownload()
        {
            return isDownload;
        }

        public void setAlbumId(long l)
        {
            albumId = l;
        }

        public void setAlbumImage(String s)
        {
            albumImage = s;
        }

        public void setAlbumTitle(String s)
        {
            albumTitle = s;
        }

        public void setComments(long l)
        {
            comments = l;
        }

        public void setCoverLarge(String s)
        {
            coverLarge = s;
        }

        public void setCoverSmall(String s)
        {
            coverSmall = s;
        }

        public void setCreatedAt(long l)
        {
            createdAt = l;
        }

        public void setDownloadSize(long l)
        {
            downloadSize = l;
        }

        public void setDownloadUrl(String s)
        {
            downloadUrl = s;
        }

        public void setDuration(long l)
        {
            duration = l;
        }

        public void setIsChecked(boolean flag)
        {
            isChecked = flag;
        }

        public void setIsDownload(boolean flag)
        {
            isDownload = flag;
        }

        public void setIsLike(boolean flag)
        {
            isLike = flag;
        }

        public void setIsPublic(boolean flag)
        {
            isPublic = flag;
        }

        public void setIsRelay(boolean flag)
        {
            isRelay = flag;
        }

        public void setLikes(long l)
        {
            likes = l;
        }

        public void setNickname(String s)
        {
            nickname = s;
        }

        public void setOpType(int i)
        {
            opType = i;
        }

        public void setOrderNum(int i)
        {
            orderNum = i;
        }

        public void setPlaytimes(long l)
        {
            playtimes = l;
        }

        public void setProcessState(int i)
        {
            processState = i;
        }

        public void setShares(long l)
        {
            shares = l;
        }

        public void setSmallLogo(String s)
        {
            smallLogo = s;
        }

        public void setStatus(int i)
        {
            status = i;
        }

        public void setTitle(String s)
        {
            title = s;
        }

        public void setTrackId(long l)
        {
            trackId = l;
        }

        public void setUid(long l)
        {
            uid = l;
        }

        public void setUserSource(int i)
        {
            userSource = i;
        }

        public Track()
        {
        }
    }

    public static class TrackList
    {

        private List list;
        private int maxPageId;
        private int pageId;
        private int pageSize;
        private int totalCount;

        public List getList()
        {
            return list;
        }

        public int getMaxPageId()
        {
            return maxPageId;
        }

        public int getPageId()
        {
            return pageId;
        }

        public int getPageSize()
        {
            return pageSize;
        }

        public int getTotalCount()
        {
            return totalCount;
        }

        public void setList(List list1)
        {
            list = list1;
        }

        public void setMaxPageId(int i)
        {
            maxPageId = i;
        }

        public void setPageId(int i)
        {
            pageId = i;
        }

        public void setPageSize(int i)
        {
            pageSize = i;
        }

        public void setTotalCount(int i)
        {
            totalCount = i;
        }

        public TrackList()
        {
        }
    }


    private static final String url = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/api1/download/album").toString();
    private Album album;
    private boolean isAsc;
    private TrackList tracks;

    public AlbumSectionModel()
    {
    }

    public static AlbumSectionModel get(long l, int i, View view, View view1)
    {
        Object obj = null;
        view1 = f.a().a((new StringBuilder()).append(url).append("/").append(l).append("/").append(i).append("/true").toString(), null, view, view1, false);
        view = obj;
        if (((com.ximalaya.ting.android.b.n.a) (view1)).b == 1)
        {
            view = obj;
            if (!TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (view1)).a))
            {
                try
                {
                    view = (AlbumSectionModel)JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (view1)).a, com/ximalaya/ting/android/model/album/AlbumSectionModel);
                }
                // Misplaced declaration of an exception variable
                catch (View view)
                {
                    view.printStackTrace();
                    return null;
                }
            }
        }
        return view;
    }

    public Album getAlbum()
    {
        return album;
    }

    public boolean getIsAsc()
    {
        return isAsc;
    }

    public TrackList getTracks()
    {
        return tracks;
    }

    public void setAlbum(Album album1)
    {
        album = album1;
    }

    public void setIsAsc(boolean flag)
    {
        isAsc = flag;
    }

    public void setTracks(TrackList tracklist)
    {
        tracks = tracklist;
    }

}
