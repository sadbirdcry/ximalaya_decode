// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.record;


public class BgSoundDetail
{

    public long albumId;
    public String albumName;
    public String artistId;
    public String artistName;
    public int copyType;
    public String format;
    public int linkCode;
    public String lrcLink;
    public long queryId;
    public String rate;
    public int relateStatus;
    public int resourceType;
    public String showLink;
    public long size;
    public long songId;
    public String songLink;
    public String songName;
    public String songPicBig;
    public String songPicRadio;
    public String songPicSmall;
    public String source;
    public float time;
    public String version;

    public BgSoundDetail()
    {
    }
}
