// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.record;


// Referenced classes of package com.ximalaya.ting.android.model.record:
//            BgSoundDetail

public class BgSound
{

    public long album_id;
    public String album_title;
    public String all_artist_id;
    public String all_rate;
    public String appendix;
    public long artist_id;
    public String author;
    public boolean candelete;
    public int charge;
    public String content;
    public int copy_type;
    public int data_source;
    public BgSoundDetail detail;
    public float duration;
    public int has_mv;
    public int has_mv_mobile;
    public int havehigh;
    public long id;
    public String info;
    public String label;
    public int learn;
    public String lrclink;
    public String path;
    public int progress;
    public int relate_status;
    public int resource_type;
    public String songLink;
    public long song_id;
    public String title;
    public long toneid;
    public int type;

    public BgSound()
    {
        candelete = true;
    }

    public boolean equals(Object obj)
    {
        if (this != obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            obj = (BgSound)obj;
            if ((id == 0L || ((BgSound) (obj)).id == 0L || id != ((BgSound) (obj)).id) && (song_id == 0L || ((BgSound) (obj)).song_id == 0L || song_id != ((BgSound) (obj)).song_id) && (path == null || !path.equals(((BgSound) (obj)).path)))
            {
                return false;
            }
        }
        return true;
    }

    public int hashCode()
    {
        int i;
        if (path == null)
        {
            i = 0;
        } else
        {
            i = path.hashCode();
        }
        return i + 31;
    }
}
