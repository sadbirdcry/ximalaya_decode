// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PersonStationHolder
{

    public ImageView cover;
    public LinearLayout follow;
    public ImageView followImg;
    public TextView followTxt;
    public TextView name;

    public PersonStationHolder()
    {
    }

    public static View getView(Context context)
    {
        context = View.inflate(context, 0x7f0301d3, null);
        PersonStationHolder personstationholder = new PersonStationHolder();
        personstationholder.cover = (ImageView)context.findViewById(0x7f0a0177);
        personstationholder.name = (TextView)context.findViewById(0x7f0a0449);
        personstationholder.follow = (LinearLayout)context.findViewById(0x7f0a05f5);
        personstationholder.followTxt = (TextView)context.findViewById(0x7f0a025d);
        personstationholder.followImg = (ImageView)context.findViewById(0x7f0a025c);
        context.setTag(personstationholder);
        return context;
    }
}
