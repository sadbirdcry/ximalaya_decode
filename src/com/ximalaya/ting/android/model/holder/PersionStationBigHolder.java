// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PersionStationBigHolder
{

    public ImageView cover;
    public TextView describe;
    public LinearLayout follow;
    public ImageView followImg;
    public TextView followTxt;
    public TextView funsCounts;
    public TextView name;
    public TextView play;
    public int position;
    public TextView trackCounts;

    public PersionStationBigHolder()
    {
    }

    public static View getView(Context context)
    {
        context = View.inflate(context, 0x7f030188, null);
        PersionStationBigHolder persionstationbigholder = new PersionStationBigHolder();
        persionstationbigholder.cover = (ImageView)context.findViewById(0x7f0a0177);
        persionstationbigholder.name = (TextView)context.findViewById(0x7f0a0449);
        persionstationbigholder.describe = (TextView)context.findViewById(0x7f0a0496);
        persionstationbigholder.trackCounts = (TextView)context.findViewById(0x7f0a05f3);
        persionstationbigholder.funsCounts = (TextView)context.findViewById(0x7f0a05f4);
        persionstationbigholder.play = (TextView)context.findViewById(0x7f0a0282);
        persionstationbigholder.follow = (LinearLayout)context.findViewById(0x7f0a05f5);
        persionstationbigholder.followTxt = (TextView)context.findViewById(0x7f0a025d);
        persionstationbigholder.followImg = (ImageView)context.findViewById(0x7f0a025c);
        context.setTag(persionstationbigholder);
        return context;
    }
}
