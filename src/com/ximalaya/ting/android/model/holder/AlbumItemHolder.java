// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.holder;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AlbumItemHolder
{

    public View arrow;
    public View border;
    public LinearLayout collect;
    public TextView collectCount;
    public TextView collectTxt;
    public ImageView complete;
    public ImageView cover;
    public TextView name;
    public ImageView newFlag;
    public TextView playCount;
    public TextView updateAt;

    public AlbumItemHolder()
    {
    }

    public static View getView(Context context)
    {
        context = View.inflate(context, 0x7f03003d, null);
        AlbumItemHolder albumitemholder = new AlbumItemHolder();
        albumitemholder.cover = (ImageView)context.findViewById(0x7f0a0023);
        albumitemholder.name = (TextView)context.findViewById(0x7f0a00ea);
        albumitemholder.playCount = (TextView)context.findViewById(0x7f0a0151);
        albumitemholder.collectCount = (TextView)context.findViewById(0x7f0a0152);
        albumitemholder.updateAt = (TextView)context.findViewById(0x7f0a014f);
        albumitemholder.collect = (LinearLayout)context.findViewById(0x7f0a0154);
        albumitemholder.collectTxt = (TextView)context.findViewById(0x7f0a0155);
        albumitemholder.complete = (ImageView)context.findViewById(0x7f0a014d);
        albumitemholder.newFlag = (ImageView)context.findViewById(0x7f0a0156);
        albumitemholder.border = context.findViewById(0x7f0a00a8);
        albumitemholder.arrow = context.findViewById(0x7f0a0153);
        context.setTag(albumitemholder);
        return context;
    }

    public static void setCollectStatus(AlbumItemHolder albumitemholder, boolean flag)
    {
        if (flag)
        {
            albumitemholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ee, 0, 0);
            albumitemholder.collectTxt.setText("\u5DF2\u6536\u85CF");
            albumitemholder.collectTxt.setTextColor(Color.parseColor("#999999"));
            return;
        } else
        {
            albumitemholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ed, 0, 0);
            albumitemholder.collectTxt.setText("\u6536\u85CF");
            albumitemholder.collectTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }
}
