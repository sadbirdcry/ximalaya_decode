// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.holder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SoundItemHolderNew
{

    public TextView alltime;
    public View border;
    public TextView commentCount;
    public ImageView download;
    public TextView flag;
    public TextView likeCount;
    public TextView origin;
    public TextView playCount;
    public ImageView playIcon;
    public int position;
    public ProgressBar progress;
    public TextView publicTime;
    public LinearLayout relayContainer;
    public TextView relayContent;
    public ImageView relayHead;
    public TextView relayName;
    public ImageView soundCover;
    public TextView soundName;
    public TextView status;
    public LinearLayout statusContainer;
    public ImageView statusIcon;
    public TextView statusName;
    public TextView userName;

    public SoundItemHolderNew()
    {
    }

    public static View getView(Context context)
    {
        context = View.inflate(context, 0x7f03012c, null);
        SoundItemHolderNew sounditemholdernew = new SoundItemHolderNew();
        sounditemholdernew.relayContainer = (LinearLayout)context.findViewById(0x7f0a049a);
        sounditemholdernew.relayHead = (ImageView)context.findViewById(0x7f0a049b);
        sounditemholdernew.relayName = (TextView)context.findViewById(0x7f0a049c);
        sounditemholdernew.relayContent = (TextView)context.findViewById(0x7f0a049d);
        sounditemholdernew.soundCover = (ImageView)context.findViewById(0x7f0a0177);
        sounditemholdernew.playIcon = (ImageView)context.findViewById(0x7f0a0022);
        sounditemholdernew.publicTime = (TextView)context.findViewById(0x7f0a0216);
        sounditemholdernew.origin = (TextView)context.findViewById(0x7f0a0217);
        sounditemholdernew.origin.setVisibility(8);
        sounditemholdernew.soundName = (TextView)context.findViewById(0x7f0a0218);
        sounditemholdernew.userName = (TextView)context.findViewById(0x7f0a0219);
        sounditemholdernew.download = (ImageView)context.findViewById(0x7f0a04b0);
        sounditemholdernew.playCount = (TextView)context.findViewById(0x7f0a021a);
        sounditemholdernew.likeCount = (TextView)context.findViewById(0x7f0a021c);
        sounditemholdernew.commentCount = (TextView)context.findViewById(0x7f0a021d);
        sounditemholdernew.alltime = (TextView)context.findViewById(0x7f0a021b);
        sounditemholdernew.status = (TextView)context.findViewById(0x7f0a04b1);
        sounditemholdernew.statusContainer = (LinearLayout)context.findViewById(0x7f0a021f);
        sounditemholdernew.statusIcon = (ImageView)context.findViewById(0x7f0a0220);
        sounditemholdernew.statusName = (TextView)context.findViewById(0x7f0a0221);
        sounditemholdernew.progress = (ProgressBar)context.findViewById(0x7f0a0223);
        sounditemholdernew.flag = (TextView)context.findViewById(0x7f0a0224);
        sounditemholdernew.border = context.findViewById(0x7f0a00a8);
        context.setTag(sounditemholdernew);
        return context;
    }
}
