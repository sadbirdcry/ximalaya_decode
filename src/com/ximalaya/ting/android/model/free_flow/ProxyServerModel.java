// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.free_flow;


public class ProxyServerModel
{

    public String appKey;
    public String gzAgentUrl;
    public String gzBearerNetwork;
    public String key;
    public String x_description;
    public String x_remark;
    public String x_returnCode;
    public String x_status;
    public String xaAgentUrl;
    public String xaBearerNetwork;

    public ProxyServerModel()
    {
    }

    public String toString()
    {
        return (new StringBuilder()).append("[returnCode=").append(x_returnCode).append(",x_description=").append(x_description).append(",x_status=").append(x_status).append(",x_remark=").append(x_remark).append(",gzAgentUrl=").append(gzAgentUrl).append(",gzBearerNetwork=").append(gzBearerNetwork).append(",xaAgentUrl=").append(xaAgentUrl).append(",xaBearerNetwork=").append(xaBearerNetwork).append(",appKey=").append(appKey).append(",key=").append(key).toString();
    }
}
