// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.free_flow;


public class PhoneUserModel
{

    private String apn;
    private String description;
    private String imei;
    private String imsi;
    private String mobile;
    private String rateType;
    private String returnCode;

    public PhoneUserModel()
    {
    }

    public String getApn()
    {
        return apn;
    }

    public String getDescription()
    {
        return description;
    }

    public String getImei()
    {
        return imei;
    }

    public String getImsi()
    {
        return imsi;
    }

    public String getMobile()
    {
        return mobile;
    }

    public String getRateType()
    {
        return rateType;
    }

    public String getReturnCode()
    {
        return returnCode;
    }

    public void setApn(String s)
    {
        apn = s;
    }

    public void setDescription(String s)
    {
        description = s;
    }

    public void setImei(String s)
    {
        imei = s;
    }

    public void setImsi(String s)
    {
        imsi = s;
    }

    public void setMobile(String s)
    {
        mobile = s;
    }

    public void setRateType(String s)
    {
        rateType = s;
    }

    public void setReturnCode(String s)
    {
        returnCode = s;
    }

    public String toString()
    {
        return (new StringBuilder()).append("[apn=").append(apn).append(",rateType=").append(rateType).append(",imsi=").append(imsi).append(",mobile=").append(mobile).append(",imei=").append(imei).append(",description=").append(description).append(",returnCode=").append(returnCode).append("]").toString();
    }
}
