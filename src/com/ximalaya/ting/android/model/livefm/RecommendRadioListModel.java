// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.livefm;


// Referenced classes of package com.ximalaya.ting.android.model.livefm:
//            RadioPlayUrl

public class RecommendRadioListModel
{

    private String picPath;
    private int radioId;
    private int radioPlayCount;
    private RadioPlayUrl radioPlayUrl;
    private int recommendType;
    private String rname;

    public RecommendRadioListModel()
    {
    }

    public String getPicPath()
    {
        return picPath;
    }

    public int getRadioId()
    {
        return radioId;
    }

    public int getRadioPlayCount()
    {
        return radioPlayCount;
    }

    public RadioPlayUrl getRadioPlayUrl()
    {
        return radioPlayUrl;
    }

    public int getRecommendType()
    {
        return recommendType;
    }

    public String getRname()
    {
        return rname;
    }

    public void setPicPath(String s)
    {
        picPath = s;
    }

    public void setRadioId(int i)
    {
        radioId = i;
    }

    public void setRadioPlayCount(int i)
    {
        radioPlayCount = i;
    }

    public void setRadioPlayUrl(RadioPlayUrl radioplayurl)
    {
        radioPlayUrl = radioplayurl;
    }

    public void setRecommendType(int i)
    {
        recommendType = i;
    }

    public void setRname(String s)
    {
        rname = s;
    }
}
