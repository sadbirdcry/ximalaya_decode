// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.livefm;


public class ProvinceModel
{

    private String createAt;
    private int id;
    private int provinceCode;
    private String provinceName;
    private String provinceType;
    private boolean show;

    public ProvinceModel()
    {
        show = false;
    }

    public boolean equals(Object obj)
    {
        while (obj == this || (obj instanceof ProvinceModel) && ((ProvinceModel)obj).provinceCode == provinceCode) 
        {
            return true;
        }
        return false;
    }

    public String getCreateAt()
    {
        return createAt;
    }

    public int getId()
    {
        return id;
    }

    public int getProvinceCode()
    {
        return provinceCode;
    }

    public String getProvinceName()
    {
        return provinceName;
    }

    public String getProvinceType()
    {
        return provinceType;
    }

    public boolean isShow()
    {
        return show;
    }

    public void setCreateAt(String s)
    {
        createAt = s;
    }

    public void setId(int i)
    {
        id = i;
    }

    public void setProvinceCode(int i)
    {
        provinceCode = i;
    }

    public void setProvinceName(String s)
    {
        provinceName = s;
    }

    public void setProvinceType(String s)
    {
        provinceType = s;
    }

    public void setShow(boolean flag)
    {
        show = flag;
    }
}
