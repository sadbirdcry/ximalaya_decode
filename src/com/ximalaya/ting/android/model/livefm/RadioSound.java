// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.livefm;

import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.livefm:
//            RadioPlayUrl

public class RadioSound
{

    private List announcerList;
    private int category;
    private String endTime;
    private long fmuid;
    private String listenBackUrl;
    private String liveUrl;
    private String playBackgroundPic;
    private int playType;
    private int programId;
    private String programName;
    private long programScheduleId;
    private String radioCoverLarge;
    private String radioCoverSmall;
    private int radioId;
    private String radioIntro;
    private int radioPlayCount;
    private RadioPlayUrl radioPlayUrl;
    private String rname;
    private String startTime;

    public RadioSound()
    {
        announcerList = new ArrayList();
    }

    public List getAnnouncerList()
    {
        return announcerList;
    }

    public int getCategory()
    {
        return category;
    }

    public String getEndTime()
    {
        return endTime;
    }

    public long getFmuid()
    {
        return fmuid;
    }

    public String getListenBackUrl()
    {
        return listenBackUrl;
    }

    public String getLiveUrl()
    {
        return liveUrl;
    }

    public String getPlayBackgroundPic()
    {
        return playBackgroundPic;
    }

    public int getPlayType()
    {
        return playType;
    }

    public int getProgramId()
    {
        return programId;
    }

    public String getProgramName()
    {
        return programName;
    }

    public long getProgramScheduleId()
    {
        return programScheduleId;
    }

    public String getRadioCoverLarge()
    {
        return radioCoverLarge;
    }

    public String getRadioCoverSmall()
    {
        return radioCoverSmall;
    }

    public int getRadioId()
    {
        return radioId;
    }

    public String getRadioIntro()
    {
        return radioIntro;
    }

    public int getRadioPlayCount()
    {
        return radioPlayCount;
    }

    public RadioPlayUrl getRadioPlayUrl()
    {
        return radioPlayUrl;
    }

    public String getRname()
    {
        return rname;
    }

    public String getStartTime()
    {
        return startTime;
    }

    public void setAnnouncerList(List list)
    {
        announcerList = list;
    }

    public void setCategory(int i)
    {
        category = i;
    }

    public void setEndTime(String s)
    {
        endTime = s;
    }

    public void setFmuid(long l)
    {
        fmuid = l;
    }

    public void setListenBackUrl(String s)
    {
        listenBackUrl = s;
    }

    public void setLiveUrl(String s)
    {
        liveUrl = s;
    }

    public void setPlayBackgroundPic(String s)
    {
        playBackgroundPic = s;
    }

    public void setPlayType(int i)
    {
        playType = i;
    }

    public void setProgramId(int i)
    {
        programId = i;
    }

    public void setProgramName(String s)
    {
        programName = s;
    }

    public void setProgramScheduleId(long l)
    {
        programScheduleId = l;
    }

    public void setRadioCoverLarge(String s)
    {
        radioCoverLarge = s;
    }

    public void setRadioCoverSmall(String s)
    {
        radioCoverSmall = s;
    }

    public void setRadioId(int i)
    {
        radioId = i;
    }

    public void setRadioIntro(String s)
    {
        radioIntro = s;
    }

    public void setRadioPlayCount(int i)
    {
        radioPlayCount = i;
    }

    public void setRadioPlayUrl(RadioPlayUrl radioplayurl)
    {
        radioPlayUrl = radioplayurl;
    }

    public void setRname(String s)
    {
        rname = s;
    }

    public void setStartTime(String s)
    {
        startTime = s;
    }
}
