// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.livefm;

import android.util.Log;

public class MyLogger
{

    private static boolean debug = false;
    private static MyLogger instance = new MyLogger();
    private String tag;

    private MyLogger()
    {
        tag = "LivePlay";
    }

    private String createMessage(String s)
    {
        String s1 = getFunctionName();
        if (s1 == null)
        {
            return s;
        } else
        {
            return (new StringBuilder()).append(s1).append(" - ").append(s).toString();
        }
    }

    private String getFunctionName()
    {
        StackTraceElement astacktraceelement[] = Thread.currentThread().getStackTrace();
        if (astacktraceelement != null)
        {
            int k = astacktraceelement.length;
            int j = 0;
            while (j < k) 
            {
                StackTraceElement stacktraceelement = astacktraceelement[j];
                if (stacktraceelement.isNativeMethod() || stacktraceelement.getClassName().equals(java/lang/Thread.getName()) || stacktraceelement.getClassName().equals(getClass().getName()))
                {
                    j++;
                } else
                {
                    return (new StringBuilder()).append("[").append(Thread.currentThread().getName()).append("(").append(Thread.currentThread().getId()).append("): ").append(stacktraceelement.getFileName()).append(":").append(stacktraceelement.getMethodName()).append(":").append(stacktraceelement.getLineNumber()).append("]").toString();
                }
            }
        }
        return null;
    }

    public static MyLogger getLogger()
    {
        return instance;
    }

    public void d(String s)
    {
        if (debug)
        {
            s = createMessage(s);
            Log.d(tag, s);
        }
    }

    public void e(String s)
    {
        if (debug)
        {
            s = createMessage(s);
            Log.e(tag, s);
        }
    }

    public void error(Exception exception)
    {
        if (debug)
        {
            StringBuffer stringbuffer = new StringBuffer();
            String s = getFunctionName();
            StackTraceElement astacktraceelement[] = exception.getStackTrace();
            if (s != null)
            {
                stringbuffer.append((new StringBuilder()).append(s).append(" - ").append(exception).append("\r\n").toString());
            } else
            {
                stringbuffer.append((new StringBuilder()).append(exception).append("\r\n").toString());
            }
            if (astacktraceelement != null && astacktraceelement.length > 0)
            {
                int k = astacktraceelement.length;
                for (int j = 0; j < k; j++)
                {
                    exception = astacktraceelement[j];
                    if (exception != null)
                    {
                        stringbuffer.append((new StringBuilder()).append("[ ").append(exception.getFileName()).append(":").append(exception.getLineNumber()).append(" ]\r\n").toString());
                    }
                }

            }
            Log.e(tag, stringbuffer.toString());
        }
    }

    public void i(String s)
    {
        if (debug)
        {
            s = createMessage(s);
            Log.i(tag, s);
        }
    }

    public void print()
    {
        if (debug)
        {
            String s = createMessage("");
            Log.d(tag, s);
        }
    }

    public void setTag(String s)
    {
        tag = s;
    }

    public void v(String s)
    {
        if (debug)
        {
            s = createMessage(s);
            Log.v(tag, s);
        }
    }

    public void w(String s)
    {
        if (debug)
        {
            s = createMessage(s);
            Log.w(tag, s);
        }
    }

}
