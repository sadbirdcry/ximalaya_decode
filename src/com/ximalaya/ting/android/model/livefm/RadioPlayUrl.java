// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.livefm;


public class RadioPlayUrl
{

    private String radio_24_aac;
    private String radio_24_ts;
    private String radio_64_aac;
    private String radio_64_ts;

    public RadioPlayUrl()
    {
    }

    public String getRadio_24_aac()
    {
        return radio_24_aac;
    }

    public String getRadio_24_ts()
    {
        return radio_24_ts;
    }

    public String getRadio_64_aac()
    {
        return radio_64_aac;
    }

    public String getRadio_64_ts()
    {
        return radio_64_ts;
    }

    public void setRadio_24_aac(String s)
    {
        radio_24_aac = s;
    }

    public void setRadio_24_ts(String s)
    {
        radio_24_ts = s;
    }

    public void setRadio_64_aac(String s)
    {
        radio_64_aac = s;
    }

    public void setRadio_64_ts(String s)
    {
        radio_64_ts = s;
    }
}
