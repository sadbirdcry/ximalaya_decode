// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.livefm;


public class AnnouncerData
{

    private long announcerId;
    private String announcerName;

    public AnnouncerData()
    {
    }

    public long getAnnouncerId()
    {
        return announcerId;
    }

    public String getAnnouncerName()
    {
        return announcerName;
    }

    public void setAnnouncerId(long l)
    {
        announcerId = l;
    }

    public void setAnnouncerName(String s)
    {
        announcerName = s;
    }
}
