// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.rank;


public class RankAlbumModel
{

    private long albumId;
    private String coverMiddle;
    private int isFinished;
    private long lastUptrackAt;
    private long lastUptrackId;
    private long playsCounts;
    private String tags;
    private String title;
    private long tracks;

    public RankAlbumModel()
    {
    }

    public long getAlbumId()
    {
        return albumId;
    }

    public String getCoverMiddle()
    {
        return coverMiddle;
    }

    public int getIsFinished()
    {
        return isFinished;
    }

    public long getLastUptrackAt()
    {
        return lastUptrackAt;
    }

    public long getLastUptrackId()
    {
        return lastUptrackId;
    }

    public long getPlaysCounts()
    {
        return playsCounts;
    }

    public String getTags()
    {
        return tags;
    }

    public String getTitle()
    {
        return title;
    }

    public long getTracks()
    {
        return tracks;
    }

    public void setAlbumId(long l)
    {
        albumId = l;
    }

    public void setCoverMiddle(String s)
    {
        coverMiddle = s;
    }

    public void setIsFinished(int i)
    {
        isFinished = i;
    }

    public void setLastUptrackAt(long l)
    {
        lastUptrackAt = l;
    }

    public void setLastUptrackId(long l)
    {
        lastUptrackId = l;
    }

    public void setPlaysCounts(long l)
    {
        playsCounts = l;
    }

    public void setTags(String s)
    {
        tags = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTracks(long l)
    {
        tracks = l;
    }
}
