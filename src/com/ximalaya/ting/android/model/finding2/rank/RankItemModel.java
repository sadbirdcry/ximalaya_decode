// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.rank;

import java.util.List;

public class RankItemModel
{
    public static class RankFirstResults
    {

        public String contentType;
        public long id;
        public String title;

        public String getContentType()
        {
            return contentType;
        }

        public long getId()
        {
            return id;
        }

        public String getTitle()
        {
            return title;
        }

        public void setContentType(String s)
        {
            contentType = s;
        }

        public void setId(long l)
        {
            id = l;
        }

        public void setTitle(String s)
        {
            title = s;
        }

        public RankFirstResults()
        {
        }
    }


    public static final String TYPE_ALBUM = "album";
    public static final String TYPE_ANCHOR = "anchor";
    public static final String TYPE_TRACK = "track";
    private String calcPeriod;
    private long categoryId;
    private String contentType;
    private String coverPath;
    private long firstId;
    private List firstKResults;
    private String firstTitle;
    private boolean isTitleView;
    private String key;
    private int orderNum;
    private int period;
    private String rankingRule;
    private String title;
    private String titleText;

    public RankItemModel()
    {
        isTitleView = false;
    }

    public String getCalcPeriod()
    {
        return calcPeriod;
    }

    public long getCategoryId()
    {
        return categoryId;
    }

    public String getContentType()
    {
        return contentType;
    }

    public String getCoverPath()
    {
        return coverPath;
    }

    public long getFirstId()
    {
        return firstId;
    }

    public List getFirstKResults()
    {
        return firstKResults;
    }

    public String getFirstTitle()
    {
        return firstTitle;
    }

    public String getKey()
    {
        return key;
    }

    public int getOrderNum()
    {
        return orderNum;
    }

    public int getPeriod()
    {
        return period;
    }

    public String getRankingRule()
    {
        return rankingRule;
    }

    public String getTitle()
    {
        return title;
    }

    public String getTitleText()
    {
        return titleText;
    }

    public boolean isTitleView()
    {
        return isTitleView;
    }

    public void setCalcPeriod(String s)
    {
        calcPeriod = s;
    }

    public void setCategoryId(long l)
    {
        categoryId = l;
    }

    public void setContentType(String s)
    {
        contentType = s;
    }

    public void setCoverPath(String s)
    {
        coverPath = s;
    }

    public void setFirstId(long l)
    {
        firstId = l;
    }

    public void setFirstKResults(List list)
    {
        firstKResults = list;
    }

    public void setFirstTitle(String s)
    {
        firstTitle = s;
    }

    public void setKey(String s)
    {
        key = s;
    }

    public void setOrderNum(int i)
    {
        orderNum = i;
    }

    public void setPeriod(int i)
    {
        period = i;
    }

    public void setRankingRule(String s)
    {
        rankingRule = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTitleText(String s)
    {
        titleText = s;
    }

    public void setTitleView(boolean flag)
    {
        isTitleView = flag;
    }
}
