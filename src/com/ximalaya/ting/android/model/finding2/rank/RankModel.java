// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.rank;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.modelnew.FocusImageModelNew;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.finding2.rank:
//            RankGroupModel

public class RankModel
{

    private List datas;
    private List focusImages;

    public RankModel()
    {
    }

    public static RankModel getInstance(String s)
    {
        try
        {
            Object obj = JSON.parseObject(s);
            String s1 = ((JSONObject) (obj)).getString("datas");
            s = new RankModel();
            if (!TextUtils.isEmpty(s1))
            {
                s.setDatas(RankGroupModel.getListFromJson(s1));
            }
            obj = ((JSONObject) (obj)).getString("focusImages");
            if (!TextUtils.isEmpty(((CharSequence) (obj))))
            {
                s.setFocusImages(FocusImageModelNew.getListFromStr(((String) (obj))));
            }
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return null;
        }
        return s;
    }

    public List getDatas()
    {
        return datas;
    }

    public List getFocusImages()
    {
        return focusImages;
    }

    public void setDatas(List list)
    {
        datas = list;
    }

    public void setFocusImages(List list)
    {
        focusImages = list;
    }
}
