// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.rank;

import com.alibaba.fastjson.JSON;
import java.util.List;

public class RankGroupModel
{

    private int count;
    private List list;
    private int ret;
    private String title;

    public RankGroupModel()
    {
        ret = -1;
    }

    public static List getListFromJson(String s)
    {
        return JSON.parseArray(s, com/ximalaya/ting/android/model/finding2/rank/RankGroupModel);
    }

    public int getCount()
    {
        return count;
    }

    public List getList()
    {
        return list;
    }

    public int getRet()
    {
        return ret;
    }

    public String getTitle()
    {
        return title;
    }

    public void setCount(int i)
    {
        count = i;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setRet(int i)
    {
        ret = i;
    }

    public void setTitle(String s)
    {
        title = s;
    }
}
