// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.rank;


public class RankTrackModel
{

    private long albumId;
    private String albumTitle;
    private long commentsCounts;
    private String coverSmall;
    private long duration;
    private long likes;
    private String nickname;
    private String playPath32;
    private String playPath64;
    private String playPathAacv164;
    private String playPathAacv224;
    private long playsCounts;
    private long sharesCounts;
    private String tags;
    private String title;
    private long trackId;
    private long uid;
    private long updatedAt;

    public RankTrackModel()
    {
    }

    public long getAlbumId()
    {
        return albumId;
    }

    public String getAlbumTitle()
    {
        return albumTitle;
    }

    public long getCommentsCounts()
    {
        return commentsCounts;
    }

    public String getCoverSmall()
    {
        return coverSmall;
    }

    public long getDuration()
    {
        return duration;
    }

    public long getLikes()
    {
        return likes;
    }

    public String getNickname()
    {
        return nickname;
    }

    public String getPlayPath32()
    {
        return playPath32;
    }

    public String getPlayPath64()
    {
        return playPath64;
    }

    public String getPlayPathAacv164()
    {
        return playPathAacv164;
    }

    public String getPlayPathAacv224()
    {
        return playPathAacv224;
    }

    public long getPlaysCounts()
    {
        return playsCounts;
    }

    public long getSharesCounts()
    {
        return sharesCounts;
    }

    public String getTags()
    {
        return tags;
    }

    public String getTitle()
    {
        return title;
    }

    public long getTrackId()
    {
        return trackId;
    }

    public long getUid()
    {
        return uid;
    }

    public long getUpdatedAt()
    {
        return updatedAt;
    }

    public void setAlbumId(long l)
    {
        albumId = l;
    }

    public void setAlbumTitle(String s)
    {
        albumTitle = s;
    }

    public void setCommentsCounts(long l)
    {
        commentsCounts = l;
    }

    public void setCoverSmall(String s)
    {
        coverSmall = s;
    }

    public void setDuration(long l)
    {
        duration = l;
    }

    public void setLikes(long l)
    {
        likes = l;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setPlayPath32(String s)
    {
        playPath32 = s;
    }

    public void setPlayPath64(String s)
    {
        playPath64 = s;
    }

    public void setPlayPathAacv164(String s)
    {
        playPathAacv164 = s;
    }

    public void setPlayPathAacv224(String s)
    {
        playPathAacv224 = s;
    }

    public void setPlaysCounts(long l)
    {
        playsCounts = l;
    }

    public void setSharesCounts(long l)
    {
        sharesCounts = l;
    }

    public void setTags(String s)
    {
        tags = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTrackId(long l)
    {
        trackId = l;
    }

    public void setUid(long l)
    {
        uid = l;
    }

    public void setUpdatedAt(long l)
    {
        updatedAt = l;
    }
}
