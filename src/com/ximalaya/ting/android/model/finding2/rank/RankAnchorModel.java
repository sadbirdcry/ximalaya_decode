// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.rank;


public class RankAnchorModel
{

    private long followersCounts;
    private boolean isVerified;
    private String middleLogo;
    private String nickname;
    private String personDescribe;
    private long trackCounts;
    private long uid;

    public RankAnchorModel()
    {
    }

    public long getFollowersCounts()
    {
        return followersCounts;
    }

    public String getMiddleLogo()
    {
        return middleLogo;
    }

    public String getNickname()
    {
        return nickname;
    }

    public String getPersonDescribe()
    {
        return personDescribe;
    }

    public long getTrackCounts()
    {
        return trackCounts;
    }

    public long getUid()
    {
        return uid;
    }

    public boolean isVerified()
    {
        return isVerified;
    }

    public void setFollowersCounts(long l)
    {
        followersCounts = l;
    }

    public void setIsVerified(boolean flag)
    {
        isVerified = flag;
    }

    public void setMiddleLogo(String s)
    {
        middleLogo = s;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setPersonDescribe(String s)
    {
        personDescribe = s;
    }

    public void setTrackCounts(long l)
    {
        trackCounts = l;
    }

    public void setUid(long l)
    {
        uid = l;
    }
}
