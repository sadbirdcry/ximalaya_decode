// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.rank;

import com.ximalaya.ting.android.model.BaseModel;
import java.util.List;

public class RankAlbumListModel extends BaseModel
{

    private List list;
    private int maxPageId;
    private int pageId;
    private int pageSize;
    private int totalCount;

    public RankAlbumListModel()
    {
    }

    public List getList()
    {
        return list;
    }

    public int getMaxPageId()
    {
        return maxPageId;
    }

    public int getPageId()
    {
        return pageId;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setMaxPageId(int i)
    {
        maxPageId = i;
    }

    public void setPageId(int i)
    {
        pageId = i;
    }

    public void setPageSize(int i)
    {
        pageSize = i;
    }

    public void setTotalCount(int i)
    {
        totalCount = i;
    }
}
