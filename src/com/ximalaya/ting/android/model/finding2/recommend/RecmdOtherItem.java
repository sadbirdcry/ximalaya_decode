// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.recommend;


public class RecmdOtherItem
{

    private String contentType;
    private long contentUpdatedAt;
    private String coverPath;
    private boolean enableShare;
    private int id;
    private boolean isExternalUrl;
    private boolean isHot;
    private String sharePic;
    private String subtitle;
    private String title;
    private String url;

    public RecmdOtherItem()
    {
        enableShare = false;
        isExternalUrl = false;
    }

    public String getContentType()
    {
        return contentType;
    }

    public long getContentUpdatedAt()
    {
        return contentUpdatedAt;
    }

    public String getCoverPath()
    {
        return coverPath;
    }

    public int getId()
    {
        return id;
    }

    public String getSharePic()
    {
        return sharePic;
    }

    public String getSubtitle()
    {
        return subtitle;
    }

    public String getTitle()
    {
        return title;
    }

    public String getUrl()
    {
        return url;
    }

    public boolean isEnableShare()
    {
        return enableShare;
    }

    public boolean isExternalUrl()
    {
        return isExternalUrl;
    }

    public boolean isHot()
    {
        return isHot;
    }

    public void setContentType(String s)
    {
        contentType = s;
    }

    public void setContentUpdatedAt(long l)
    {
        contentUpdatedAt = l;
    }

    public void setCoverPath(String s)
    {
        coverPath = s;
    }

    public void setEnableShare(boolean flag)
    {
        enableShare = flag;
    }

    public void setId(int i)
    {
        id = i;
    }

    public void setIsExternalUrl(boolean flag)
    {
        isExternalUrl = flag;
    }

    public void setIsHot(boolean flag)
    {
        isHot = flag;
    }

    public void setSharePic(String s)
    {
        sharePic = s;
    }

    public void setSubtitle(String s)
    {
        subtitle = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setUrl(String s)
    {
        url = s;
    }
}
