// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.recommend;

import java.util.List;

public class RecmdOtherGroup
{

    private List list;
    private int locationInHotRecommend;
    private int ret;
    private String title;

    public RecmdOtherGroup()
    {
        ret = 0;
    }

    public List getList()
    {
        return list;
    }

    public int getLocationInHotRecommend()
    {
        return locationInHotRecommend;
    }

    public int getRet()
    {
        return ret;
    }

    public String getTitle()
    {
        return title;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setLocationInHotRecommend(int i)
    {
        locationInHotRecommend = i;
    }

    public void setRet(int i)
    {
        ret = i;
    }

    public void setTitle(String s)
    {
        title = s;
    }
}
