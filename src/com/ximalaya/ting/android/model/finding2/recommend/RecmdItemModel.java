// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.recommend;

import com.ximalaya.ting.android.model.Collectable;
import java.io.Serializable;

public class RecmdItemModel
    implements Collectable, Serializable
{

    private static final long serialVersionUID = 0xe580ea64745a9200L;
    private long albumId;
    private String coverLarge;
    private String coverMiddle;
    private String coverSmall;
    private String intro;
    private boolean isCollected;
    private int isFinished;
    private long lastUptrackAt;
    private long lastUptrackId;
    private String lastUptrackTitle;
    private String playPath32;
    private String playPath64;
    private String playPathAacv164;
    private String playPathAacv224;
    private long playsCounts;
    private String recReason;
    private String recSrc;
    private String recTrack;
    private String tags;
    private String title;
    private long trackId;
    private String trackTitle;
    private int tracks;

    public RecmdItemModel()
    {
    }

    public long getAlbumId()
    {
        return albumId;
    }

    public long getCId()
    {
        return getAlbumId();
    }

    public String getCoverLarge()
    {
        return coverLarge;
    }

    public String getCoverMiddle()
    {
        return coverMiddle;
    }

    public String getCoverSmall()
    {
        return coverSmall;
    }

    public String getIntro()
    {
        return intro;
    }

    public int getIsFinished()
    {
        return isFinished;
    }

    public long getLastUptrackAt()
    {
        return lastUptrackAt;
    }

    public long getLastUptrackId()
    {
        return lastUptrackId;
    }

    public String getLastUptrackTitle()
    {
        return lastUptrackTitle;
    }

    public String getPlayPath32()
    {
        return playPath32;
    }

    public String getPlayPath64()
    {
        return playPath64;
    }

    public String getPlayPathAacv164()
    {
        return playPathAacv164;
    }

    public String getPlayPathAacv224()
    {
        return playPathAacv224;
    }

    public long getPlaysCounts()
    {
        return playsCounts;
    }

    public String getRecReason()
    {
        return recReason;
    }

    public String getRecSrc()
    {
        return recSrc;
    }

    public String getRecTrack()
    {
        return recTrack;
    }

    public String getTags()
    {
        return tags;
    }

    public String getTitle()
    {
        return title;
    }

    public long getTrackId()
    {
        return trackId;
    }

    public String getTrackTitle()
    {
        return trackTitle;
    }

    public int getTracks()
    {
        return tracks;
    }

    public boolean isCCollected()
    {
        return isCollected;
    }

    public void setAlbumId(long l)
    {
        albumId = l;
    }

    public void setCCollected(boolean flag)
    {
        isCollected = flag;
    }

    public void setCoverLarge(String s)
    {
        coverLarge = s;
    }

    public void setCoverMiddle(String s)
    {
        coverMiddle = s;
    }

    public void setCoverSmall(String s)
    {
        coverSmall = s;
    }

    public void setIntro(String s)
    {
        intro = s;
    }

    public void setIsFinished(int i)
    {
        isFinished = i;
    }

    public void setLastUptrackAt(long l)
    {
        lastUptrackAt = l;
    }

    public void setLastUptrackId(long l)
    {
        lastUptrackId = l;
    }

    public void setLastUptrackTitle(String s)
    {
        lastUptrackTitle = s;
    }

    public void setPlayPath32(String s)
    {
        playPath32 = s;
    }

    public void setPlayPath64(String s)
    {
        playPath64 = s;
    }

    public void setPlayPathAacv164(String s)
    {
        playPathAacv164 = s;
    }

    public void setPlayPathAacv224(String s)
    {
        playPathAacv224 = s;
    }

    public void setPlaysCounts(long l)
    {
        playsCounts = l;
    }

    public void setRecReason(String s)
    {
        recReason = s;
    }

    public void setRecSrc(String s)
    {
        recSrc = s;
    }

    public void setRecTrack(String s)
    {
        recTrack = s;
    }

    public void setTags(String s)
    {
        tags = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTrackId(long l)
    {
        trackId = l;
    }

    public void setTrackTitle(String s)
    {
        trackTitle = s;
    }

    public void setTracks(int i)
    {
        tracks = i;
    }
}
