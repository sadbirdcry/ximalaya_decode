// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.recommend;


public class RecmdSpecialModel
{

    public static final int TYPE_COLUMN_RANK = 2;
    public static final int TYPE_COLUMN_SUBJECT = 1;
    public static final String TYPE_CONTENT_ALBUM = "album";
    public static final String TYPE_CONTENT_TRACK = "track";
    private long categoryId;
    private int columnType;
    private String contentType;
    private String coverPath;
    private long firstId;
    private String footnote;
    private String key;
    private int orderNum;
    private int specialId;
    private String subtitle;
    private String title;

    public RecmdSpecialModel()
    {
    }

    public long getCategoryId()
    {
        return categoryId;
    }

    public int getColumnType()
    {
        return columnType;
    }

    public String getContentType()
    {
        return contentType;
    }

    public String getCoverPath()
    {
        return coverPath;
    }

    public long getFirstId()
    {
        return firstId;
    }

    public String getFootnote()
    {
        return footnote;
    }

    public String getKey()
    {
        return key;
    }

    public int getOrderNum()
    {
        return orderNum;
    }

    public int getSpecialId()
    {
        return specialId;
    }

    public String getSubtitle()
    {
        return subtitle;
    }

    public String getTitle()
    {
        return title;
    }

    public void setCategoryId(long l)
    {
        categoryId = l;
    }

    public void setColumnType(int i)
    {
        columnType = i;
    }

    public void setContentType(String s)
    {
        contentType = s;
    }

    public void setCoverPath(String s)
    {
        coverPath = s;
    }

    public void setFirstId(long l)
    {
        firstId = l;
    }

    public void setFootnote(String s)
    {
        footnote = s;
    }

    public void setKey(String s)
    {
        key = s;
    }

    public void setOrderNum(int i)
    {
        orderNum = i;
    }

    public void setSpecialId(int i)
    {
        specialId = i;
    }

    public void setSubtitle(String s)
    {
        subtitle = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }
}
