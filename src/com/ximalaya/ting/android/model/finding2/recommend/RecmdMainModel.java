// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.recommend;

import com.ximalaya.ting.android.model.BaseModel;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.finding2.recommend:
//            RecmdOtherGroup, RecmdItemGroup, RecmdEntrancesGroup, RecmdFocusImgGroup, 
//            RecmdHotGroupList, RecmdSpecialGroup

public class RecmdMainModel extends BaseModel
{

    private RecmdOtherGroup discoveryColumns;
    private RecmdItemGroup editorRecommendAlbums;
    private RecmdEntrancesGroup entrances;
    private RecmdFocusImgGroup focusImages;
    private RecmdItemGroup guessYouLikeAlbums;
    private RecmdHotGroupList hotRecommends;
    private RecmdSpecialGroup specialColumn;

    public RecmdMainModel()
    {
    }

    public RecmdOtherGroup getDiscoveryColumns()
    {
        return discoveryColumns;
    }

    public RecmdItemGroup getEditorRecommendAlbums()
    {
        return editorRecommendAlbums;
    }

    public RecmdEntrancesGroup getEntrances()
    {
        return entrances;
    }

    public RecmdFocusImgGroup getFocusImages()
    {
        return focusImages;
    }

    public RecmdItemGroup getGuessYouLikeAlbums()
    {
        return guessYouLikeAlbums;
    }

    public RecmdHotGroupList getHotRecommends()
    {
        return hotRecommends;
    }

    public RecmdSpecialGroup getSpecialColumn()
    {
        return specialColumn;
    }

    public boolean needShow()
    {
        RecmdOtherGroup recmdothergroup = getDiscoveryColumns();
        return recmdothergroup != null && recmdothergroup.getRet() == 0 && recmdothergroup.getList() != null && !recmdothergroup.getList().isEmpty();
    }

    public void setDiscoveryColumns(RecmdOtherGroup recmdothergroup)
    {
        discoveryColumns = recmdothergroup;
    }

    public void setEditorRecommendAlbums(RecmdItemGroup recmditemgroup)
    {
        editorRecommendAlbums = recmditemgroup;
    }

    public void setEntrances(RecmdEntrancesGroup recmdentrancesgroup)
    {
        entrances = recmdentrancesgroup;
    }

    public void setFocusImages(RecmdFocusImgGroup recmdfocusimggroup)
    {
        focusImages = recmdfocusimggroup;
    }

    public void setGuessYouLikeAlbums(RecmdItemGroup recmditemgroup)
    {
        guessYouLikeAlbums = recmditemgroup;
    }

    public void setHotRecommends(RecmdHotGroupList recmdhotgrouplist)
    {
        hotRecommends = recmdhotgrouplist;
    }

    public void setSpecialColumn(RecmdSpecialGroup recmdspecialgroup)
    {
        specialColumn = recmdspecialgroup;
    }
}
