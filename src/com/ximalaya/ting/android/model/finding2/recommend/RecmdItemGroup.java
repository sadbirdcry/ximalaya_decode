// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.recommend;

import java.util.List;

public class RecmdItemGroup
{

    private boolean hasMore;
    private List list;
    private int maxPageId;
    private int pageId;
    private int pageSize;
    private int ret;
    private String title;
    private int totalCount;
    private long uid;

    public RecmdItemGroup()
    {
        ret = 0;
    }

    public List getList()
    {
        return list;
    }

    public int getMaxPageId()
    {
        return maxPageId;
    }

    public int getPageId()
    {
        return pageId;
    }

    public int getPageSize()
    {
        return pageSize;
    }

    public int getRet()
    {
        return ret;
    }

    public String getTitle()
    {
        return title;
    }

    public int getTotalCount()
    {
        return totalCount;
    }

    public long getUid()
    {
        return uid;
    }

    public boolean hasMore()
    {
        return isHasMore() || getMaxPageId() > getPageId();
    }

    public boolean isHasMore()
    {
        return hasMore;
    }

    public void setHasMore(boolean flag)
    {
        hasMore = flag;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setMaxPageId(int i)
    {
        maxPageId = i;
    }

    public void setPageId(int i)
    {
        pageId = i;
    }

    public void setPageSize(int i)
    {
        pageSize = i;
    }

    public void setRet(int i)
    {
        ret = i;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setTotalCount(int i)
    {
        totalCount = i;
    }

    public void setUid(long l)
    {
        uid = l;
    }
}
