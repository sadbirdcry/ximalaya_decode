// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.recommend;


public class RecmdEntrancesItem
{

    private String coverPath;
    private String entranceType;
    private int id;
    private String title;

    public RecmdEntrancesItem()
    {
    }

    public String getCoverPath()
    {
        return coverPath;
    }

    public String getEntranceType()
    {
        return entranceType;
    }

    public int getId()
    {
        return id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setCoverPath(String s)
    {
        coverPath = s;
    }

    public void setEntranceType(String s)
    {
        entranceType = s;
    }

    public void setId(int i)
    {
        id = i;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public String toString()
    {
        return (new StringBuilder()).append("RecmdEntrancesItem [id=").append(id).append(", entranceType=").append(entranceType).append(", coverPath=").append(coverPath).append(", title=").append(title).append("]").toString();
    }
}
