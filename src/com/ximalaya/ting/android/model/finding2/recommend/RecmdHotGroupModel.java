// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.finding2.recommend;

import java.util.List;

public class RecmdHotGroupModel
{

    public static final String TYPE_ALBUM = "album";
    public static final String TYPE_TRACK = "track";
    private long categoryId;
    private String contentType;
    private int count;
    private boolean hasMore;
    private List list;
    private String title;

    public RecmdHotGroupModel()
    {
    }

    public long getCategoryId()
    {
        return categoryId;
    }

    public String getContentType()
    {
        return contentType;
    }

    public int getCount()
    {
        return count;
    }

    public List getList()
    {
        return list;
    }

    public String getTitle()
    {
        return title;
    }

    public boolean isHasMore()
    {
        return hasMore;
    }

    public boolean isTrack()
    {
        return "track".equals(getContentType());
    }

    public void setCategoryId(long l)
    {
        categoryId = l;
    }

    public void setContentType(String s)
    {
        contentType = s;
    }

    public void setCount(int i)
    {
        count = i;
    }

    public void setHasMore(boolean flag)
    {
        hasMore = flag;
    }

    public void setList(List list1)
    {
        list = list1;
    }

    public void setTitle(String s)
    {
        title = s;
    }
}
