// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.subject;

import com.ximalaya.ting.android.model.BaseModel;

public class SubjectModel extends BaseModel
    implements Comparable
{

    public int contentType;
    public String coverPathBig;
    public String coverPathSmall;
    public String footnote;
    public String intro;
    public boolean isHot;
    public String nickname;
    public String personalSignature;
    public long releasedAt;
    public String smallLogo;
    public long specialId;
    public String subtitle;
    public String title;
    public long uid;

    public SubjectModel()
    {
    }

    public int compareTo(SubjectModel subjectmodel)
    {
        if (subjectmodel != null)
        {
            if (releasedAt > subjectmodel.releasedAt)
            {
                return -1;
            }
            if (releasedAt == subjectmodel.releasedAt)
            {
                return 0;
            }
        }
        return 1;
    }

    public volatile int compareTo(Object obj)
    {
        return compareTo((SubjectModel)obj);
    }
}
