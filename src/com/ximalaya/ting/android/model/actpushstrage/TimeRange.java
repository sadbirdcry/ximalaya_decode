// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.actpushstrage;

import java.util.List;
import java.util.Random;

public class TimeRange
{

    private int begin;
    private List descripeList;
    private int end;

    public TimeRange()
    {
    }

    public int getBegin()
    {
        return begin;
    }

    public String getDescripe()
    {
        if (descripeList != null && descripeList.size() > 0)
        {
            return (String)descripeList.get((new Random()).nextInt(descripeList.size()));
        } else
        {
            return null;
        }
    }

    public List getDescripeList()
    {
        return descripeList;
    }

    public int getEnd()
    {
        return end;
    }

    public void setBegin(int i)
    {
        begin = i;
    }

    public void setDescripeList(List list)
    {
        descripeList = list;
    }

    public void setEnd(int i)
    {
        end = i;
    }
}
