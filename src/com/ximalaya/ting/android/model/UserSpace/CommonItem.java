// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.UserSpace;


public final class CommonItem
{

    public boolean boolValue;
    public String name;
    public int number;
    public float spaceOccupySize;
    public String text;

    public CommonItem()
    {
        name = null;
        text = null;
        number = -1;
        boolValue = false;
    }

    public CommonItem(String s, String s1, int i, boolean flag)
    {
        name = s;
        text = s1;
        number = i;
        boolValue = flag;
    }
}
