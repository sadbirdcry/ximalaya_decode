// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.feed2;

import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.ad.FeedAd2;

public class FeedRecommendModel extends BaseModel
{

    public static final int TYPE_AD = 1;
    public String albumCover;
    public long albumId;
    public String albumTitle;
    public String avatar;
    public String nickname;
    public String personalSignature;
    public String playUrl32;
    public String playUrl64;
    public String recSrc;
    public String recTrack;
    public Object tag;
    public long trackId;
    public String trackTitle;
    public int trackType;
    public int type;
    public long uid;

    public FeedRecommendModel()
    {
    }

    public static FeedRecommendModel from(FeedAd2 feedad2)
    {
        FeedRecommendModel feedrecommendmodel = new FeedRecommendModel();
        feedrecommendmodel.tag = feedad2;
        feedrecommendmodel.albumTitle = feedad2.name;
        feedrecommendmodel.trackTitle = feedad2.description;
        feedrecommendmodel.albumCover = feedad2.cover;
        feedrecommendmodel.type = 1;
        return feedrecommendmodel;
    }
}
