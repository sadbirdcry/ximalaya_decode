// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.feed2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.feed2:
//            FeedRecommendModel

public class FeedModel2
    implements Serializable
{

    private static final long serialVersionUID = 0x741ba6a452e3f7feL;
    public String albumCover;
    public long albumId;
    public String albumTitle;
    public String avatar;
    public int clickType;
    public String cover;
    public int dynamicType;
    public boolean isAd;
    public boolean isBlocked;
    public boolean isFollowed;
    public boolean isRecommend;
    public boolean isRecommendTitle;
    public boolean isTop;
    public long lastUpdateAt;
    public String link;
    public String nickname;
    public int openlinkType;
    public String personalSignature;
    public String playPath32;
    public String playPath64;
    public String recSrc;
    public String recTrack;
    public long timeline;
    public long trackId;
    public String trackTitle;
    public int trackType;
    public int type;
    public long uid;
    public int unreadNum;

    public FeedModel2()
    {
        isAd = false;
        isRecommend = false;
        isRecommendTitle = false;
        isFollowed = false;
        isBlocked = false;
    }

    public static FeedModel2 convert(FeedRecommendModel feedrecommendmodel)
    {
        if (feedrecommendmodel == null)
        {
            return null;
        } else
        {
            FeedModel2 feedmodel2 = new FeedModel2();
            feedmodel2.uid = feedrecommendmodel.uid;
            feedmodel2.avatar = feedrecommendmodel.avatar;
            feedmodel2.nickname = feedrecommendmodel.nickname;
            feedmodel2.personalSignature = feedrecommendmodel.personalSignature;
            feedmodel2.albumId = feedrecommendmodel.albumId;
            feedmodel2.albumTitle = feedrecommendmodel.albumTitle;
            feedmodel2.albumCover = feedrecommendmodel.albumCover;
            feedmodel2.trackId = feedrecommendmodel.trackId;
            feedmodel2.trackTitle = feedrecommendmodel.trackTitle;
            feedmodel2.playPath32 = feedrecommendmodel.playUrl32;
            feedmodel2.trackType = feedrecommendmodel.trackType;
            feedmodel2.playPath64 = feedrecommendmodel.playUrl64;
            feedmodel2.isRecommend = true;
            feedmodel2.recSrc = feedrecommendmodel.recSrc;
            feedmodel2.recTrack = feedrecommendmodel.recTrack;
            return feedmodel2;
        }
    }

    public static List convert(List list)
    {
        ArrayList arraylist = new ArrayList();
        if (list == null)
        {
            return arraylist;
        }
        list = list.iterator();
        do
        {
            if (!list.hasNext())
            {
                break;
            }
            FeedRecommendModel feedrecommendmodel = (FeedRecommendModel)list.next();
            if (feedrecommendmodel != null)
            {
                arraylist.add(convert(feedrecommendmodel));
            }
        } while (true);
        return arraylist;
    }
}
