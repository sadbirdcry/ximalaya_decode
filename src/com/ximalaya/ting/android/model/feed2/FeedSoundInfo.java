// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.feed2;

import com.ximalaya.ting.android.model.BaseModel;

public class FeedSoundInfo extends BaseModel
{

    public long albumId;
    public String albumTitle;
    public String coverPath;
    public String coverSmall;
    public long createdAt;
    public boolean downloaded;
    public double duration;
    public long favoritesCounts;
    public boolean newUpdated;
    public String nickname;
    public String playPathAacv164;
    public String playPathAacv224;
    public String playUrl32;
    public String playUrl64;
    public long playsCounts;
    public boolean selected;
    public long sharesCounts;
    public String title;
    public long trackId;
    public long uid;
    public int userSource;

    public FeedSoundInfo()
    {
        selected = false;
        newUpdated = false;
        downloaded = false;
    }
}
