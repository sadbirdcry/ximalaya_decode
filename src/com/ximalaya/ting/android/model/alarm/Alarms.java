// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.alarm;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// Referenced classes of package com.ximalaya.ting.android.model.alarm:
//            Alarm

public class Alarms
{

    public Alarms()
    {
    }

    public static void clearAlarm(Context context)
    {
        context = SharedPreferencesUtil.getInstance(context);
        context.removeByKey("alarm_ringtone_location");
        context.removeByKey("alarm_ringtone_download_url");
        context.removeByKey("alarm_ringtone_title");
        context.removeByKey("type");
    }

    public static ArrayList getSoundInfoFromOnline(Context context, int i, String s)
    {
        context = new RequestParams();
        context.put("type", String.valueOf(i));
        context.put("uid", s);
        return parseInfoFromJson(f.a().a(Alarm.ONLINE_ALARM, context, null, null));
    }

    public static ArrayList getSoundInfoFromOnline(Context context, String s)
    {
        context = null;
        s = f.a().a(s, null, null, null, false);
        if (((com.ximalaya.ting.android.b.n.a) (s)).b == 1)
        {
            context = ((com.ximalaya.ting.android.b.n.a) (s)).a;
        }
        return parseInfoFromJson(context);
    }

    private static boolean isStrEquals(String s, String s1)
    {
        boolean flag = false;
        if (s == null)
        {
            if (s1 == null)
            {
                flag = true;
            }
        } else
        if (s1 != null)
        {
            return s.equals(s1);
        }
        return flag;
    }

    private static ArrayList parseInfoFromJson(String s)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        Object obj = null;
_L4:
        return ((ArrayList) (obj));
_L2:
        JSONArray jsonarray;
        jsonarray = (new JSONObject(s)).getJSONArray("list");
        s = new ArrayList(jsonarray.length());
        int i = 0;
_L5:
        obj = s;
        if (i >= jsonarray.length()) goto _L4; else goto _L3
_L3:
        JSONObject jsonobject = jsonarray.getJSONObject(i);
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.title = jsonobject.optString("title");
        soundinfo.trackId = jsonobject.optInt("id");
        soundinfo.playUrl32 = jsonobject.optString("play_path_32");
        soundinfo.playUrl64 = jsonobject.optString("play_path_64");
        if (!TextUtils.isEmpty(soundinfo.playUrl32) && !TextUtils.isEmpty(soundinfo.playUrl64))
        {
            s.add(soundinfo);
        }
        i++;
          goto _L5
        JSONException jsonexception;
        jsonexception;
        s = null;
_L7:
        jsonexception.printStackTrace();
        return s;
        jsonexception;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public static void setAlarm(Context context, Alarm alarm)
    {
        context = context.getSharedPreferences("ting_data", 0);
        String s = context.getString("alarm_ringtone_location", "");
        String s1 = context.getString("alarm_ringtone_download_url", "");
        String s2 = context.getString("alarm_ringtone_title", "");
        if (isStrEquals(s, alarm.mLocationDir) && isStrEquals(s1, alarm.mUrl) && isStrEquals(s2, alarm.mTitle))
        {
            return;
        } else
        {
            context = context.edit();
            context.putString("alarm_ringtone_location", alarm.mLocationDir);
            context.putString("alarm_ringtone_download_url", alarm.mUrl);
            context.putString("alarm_ringtone_title", alarm.mTitle);
            context.putInt("type", alarm.mType);
            context.commit();
            return;
        }
    }

    public static void setAlarmSound(Context context, String s, String s1, String s2, int i)
    {
        context = SharedPreferencesUtil.getInstance(context);
        String s3 = context.getString("alarm_ringtone_location");
        String s4 = context.getString("alarm_ringtone_download_url");
        String s5 = context.getString("alarm_ringtone_title");
        if (isStrEquals(s3, s1) && isStrEquals(s4, s) && isStrEquals(s5, s2))
        {
            return;
        } else
        {
            context.saveString("alarm_ringtone_location", s1);
            context.saveString("alarm_ringtone_download_url", s);
            context.saveString("alarm_ringtone_title", s2);
            context.saveInt("type", i);
            return;
        }
    }
}
