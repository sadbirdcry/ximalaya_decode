// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.alarm;

import android.content.Context;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;

public class Alarm
{

    public static final String ALARM_TYPE = "type";
    public static final String ONLINE_ALARM;
    public static final int ONLINE_ALARM_CROSSTALK = 4;
    public static final int ONLINE_ALARM_MUSIC = 2;
    public static final int ONLINE_ALARM_NEWS = 1;
    public static final int TYPE_BUILDIN = 2;
    public static final int TYPE_DOWNLOADED = 0;
    public static final int TYPE_LIVE = 3;
    public static final int TYPE_ONLINE = 1;
    public String mLocalFilePath;
    public String mLocationDir;
    public String mTitle;
    public int mType;
    public String mUrl;

    public Alarm()
    {
    }

    public static Alarm getAlarmFromPref(Context context)
    {
        context = SharedPreferencesUtil.getInstance(context);
        Alarm alarm = new Alarm();
        alarm.mLocationDir = context.getString("alarm_ringtone_location");
        alarm.mTitle = context.getString("alarm_ringtone_title");
        alarm.mUrl = context.getString("alarm_ringtone_download_url");
        alarm.mType = context.getInt("type", 2);
        return alarm;
    }

    static 
    {
        ONLINE_ALARM = (new StringBuilder()).append(a.u).append("m/alarm_clock").toString();
    }
}
