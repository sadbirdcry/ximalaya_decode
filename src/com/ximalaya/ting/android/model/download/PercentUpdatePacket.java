// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.download;

import java.util.ArrayList;

public class PercentUpdatePacket
{

    public int action;
    public ArrayList downloadList;
    public long downloaded;
    public long filesize;
    public String name;
    public int percentage;
    public long trackId;

    public PercentUpdatePacket()
    {
        percentage = 0;
        trackId = 0L;
        action = -1;
        name = "null name";
        downloadList = null;
        downloaded = 0L;
        filesize = 0L;
    }
}
