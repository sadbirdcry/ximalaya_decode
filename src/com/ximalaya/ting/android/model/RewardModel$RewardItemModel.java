// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model;


// Referenced classes of package com.ximalaya.ting.android.model:
//            RewardModel

public static class A
{

    private String amount;
    private long id;
    private String mark;
    private String nickname;
    private String smallLogo;
    private long sn;
    private long trackId;
    private long uid;

    public String getAmount()
    {
        return amount;
    }

    public long getId()
    {
        return id;
    }

    public String getMark()
    {
        return mark;
    }

    public String getNickname()
    {
        return nickname;
    }

    public String getSmallLogo()
    {
        return smallLogo;
    }

    public long getSn()
    {
        return sn;
    }

    public long getTrackId()
    {
        return trackId;
    }

    public long getUid()
    {
        return uid;
    }

    public void setAmount(String s)
    {
        amount = s;
    }

    public void setId(long l)
    {
        id = l;
    }

    public void setMark(String s)
    {
        mark = s;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setSmallLogo(String s)
    {
        smallLogo = s;
    }

    public void setSn(long l)
    {
        sn = l;
    }

    public void setTrackId(long l)
    {
        trackId = l;
    }

    public void setUid(long l)
    {
        uid = l;
    }

    public A()
    {
    }
}
