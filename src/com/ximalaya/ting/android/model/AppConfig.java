// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model;


public class AppConfig
{

    private static AppConfig sInstance;
    public long adLoadingIntervalTime;
    public int adLoadingShowNume;
    public int appraisedPopType;
    public int cdnNotWifiAlertRate;
    public int cdnNotWifiConnectTimeout;
    public int cdnWifiAlertRate;
    public int cdnWifiConnectTimeout;
    public int downloadAlbumPoints;
    public int downloadsBeforeAppraised;
    public int fiveStarPraisedAwardPoints;
    public String hardwareBuyUrl;
    public String hardwareShoppingUrl;
    public boolean isActivePoint;
    public boolean isHardwareBook;
    public boolean isHardwareDoss;
    public boolean isOpenUnicomTrafficPackage;
    public boolean isShowHardware;
    public boolean isShowLoginPanel;
    public long listenedTimeBeforeAppraised;
    public long loadingWaitTime;
    public String mobileType;
    public boolean playerMode;
    public int pushReceiveDelay;
    public int sharePoints;
    public int thirdAd;
    public String unicomTrafficPackageDomain;
    public int userInitializedPionts;

    private AppConfig()
    {
        adLoadingIntervalTime = 0L;
    }

    public static AppConfig getInstance()
    {
        com/ximalaya/ting/android/model/AppConfig;
        JVM INSTR monitorenter ;
        AppConfig appconfig;
        if (sInstance == null)
        {
            sInstance = new AppConfig();
        }
        appconfig = sInstance;
        com/ximalaya/ting/android/model/AppConfig;
        JVM INSTR monitorexit ;
        return appconfig;
        Exception exception;
        exception;
        throw exception;
    }
}
