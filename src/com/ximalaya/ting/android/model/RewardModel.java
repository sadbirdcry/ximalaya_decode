// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model;

import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import java.util.List;

public class RewardModel
{
    public static class RewardItemModel
    {

        private String amount;
        private long id;
        private String mark;
        private String nickname;
        private String smallLogo;
        private long sn;
        private long trackId;
        private long uid;

        public String getAmount()
        {
            return amount;
        }

        public long getId()
        {
            return id;
        }

        public String getMark()
        {
            return mark;
        }

        public String getNickname()
        {
            return nickname;
        }

        public String getSmallLogo()
        {
            return smallLogo;
        }

        public long getSn()
        {
            return sn;
        }

        public long getTrackId()
        {
            return trackId;
        }

        public long getUid()
        {
            return uid;
        }

        public void setAmount(String s)
        {
            amount = s;
        }

        public void setId(long l)
        {
            id = l;
        }

        public void setMark(String s)
        {
            mark = s;
        }

        public void setNickname(String s)
        {
            nickname = s;
        }

        public void setSmallLogo(String s)
        {
            smallLogo = s;
        }

        public void setSn(long l)
        {
            sn = l;
        }

        public void setTrackId(long l)
        {
            trackId = l;
        }

        public void setUid(long l)
        {
            uid = l;
        }

        public RewardItemModel()
        {
        }
    }


    private boolean isOpen;
    private long numOfRewards;
    private List rewords;

    public RewardModel()
    {
    }

    public static RewardModel getInstanceFromRemote(long l)
    {
        Object obj;
        obj = new RequestParams();
        ((RequestParams) (obj)).put("trackId", l);
        ((RequestParams) (obj)).put("sn", 0);
        ((RequestParams) (obj)).put("order", 0);
        ((RequestParams) (obj)).put("size", 6);
        ((RequestParams) (obj)).put("version", "v1");
        obj = f.a().a((new StringBuilder()).append(a.U).append("ting-shang-mobile-web/v1/rewardOrders/track/").append(l).append(".json").toString(), ((RequestParams) (obj)), false);
        if (((com.ximalaya.ting.android.b.n.a) (obj)).b != 1 || TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (obj)).a))
        {
            break MISSING_BLOCK_LABEL_150;
        }
        obj = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (obj)).a);
        if (((JSONObject) (obj)).getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_150;
        }
        obj = ((JSONObject) (obj)).getString("result");
        if (TextUtils.isEmpty(((CharSequence) (obj))))
        {
            break MISSING_BLOCK_LABEL_150;
        }
        obj = (RewardModel)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/model/RewardModel);
        return ((RewardModel) (obj));
        Exception exception;
        exception;
        exception.printStackTrace();
        return null;
    }

    public boolean getIsOpen()
    {
        return isOpen;
    }

    public long getNumOfRewards()
    {
        return numOfRewards;
    }

    public List getRewords()
    {
        return rewords;
    }

    public void setIsOpen(boolean flag)
    {
        isOpen = flag;
    }

    public void setNumOfRewards(long l)
    {
        numOfRewards = l;
    }

    public void setRewords(List list)
    {
        rewords = list;
    }
}
