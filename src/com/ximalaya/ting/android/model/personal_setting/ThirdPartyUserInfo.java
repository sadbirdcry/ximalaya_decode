// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.personal_setting;

import java.io.Serializable;

public class ThirdPartyUserInfo
    implements Serializable
{

    public static final int THIRDPARTY_ID_QQ = 2;
    public static final int THIRDPARTY_ID_RENN = 3;
    public static final int THIRDPARTY_ID_SINA = 1;
    private static final long serialVersionUID = 1L;
    public String header;
    public String homePage;
    public boolean isExpired;
    public boolean isInvite;
    public boolean isRealData;
    public boolean mobileAll;
    public String name;
    public String nickname;
    public boolean relay;
    public String smallLogo;
    public String thirdpartyId;
    public String thirdpartyName;
    public String thirdpartyNickname;
    public String thirdpartyUid;
    public boolean webAlbum;
    public boolean webComment;
    public boolean webFavorite;
    public boolean webTrack;

    public ThirdPartyUserInfo()
    {
        isInvite = false;
        isRealData = true;
    }

    public ThirdPartyUserInfo(String s, String s1, String s2)
    {
        isInvite = false;
        isRealData = true;
        thirdpartyId = s;
        nickname = s1;
        header = s2;
    }

    public String getHeader()
    {
        return header;
    }

    public String getHomePage()
    {
        return homePage;
    }

    public String getIdentity()
    {
        return thirdpartyId;
    }

    public String getName()
    {
        return name;
    }

    public String getNickname()
    {
        return nickname;
    }

    public String getThirdpartyName()
    {
        return thirdpartyName;
    }

    public String getThirdpartyUid()
    {
        return thirdpartyUid;
    }

    public boolean isExpired()
    {
        return isExpired;
    }

    public boolean isInvite()
    {
        return isInvite;
    }

    public boolean isMobileAll()
    {
        return mobileAll;
    }

    public boolean isRelay()
    {
        return relay;
    }

    public boolean isWebAlbum()
    {
        return webAlbum;
    }

    public boolean isWebComment()
    {
        return webComment;
    }

    public boolean isWebFavorite()
    {
        return webFavorite;
    }

    public boolean isWebTrack()
    {
        return webTrack;
    }

    public void setExpired(boolean flag)
    {
        isExpired = flag;
    }

    public void setHeader(String s)
    {
        header = s;
    }

    public void setHomePage(String s)
    {
        homePage = s;
    }

    public void setIdentity(String s)
    {
        thirdpartyId = s;
    }

    public void setInvite(boolean flag)
    {
        isInvite = flag;
    }

    public void setMobileAll(boolean flag)
    {
        mobileAll = flag;
    }

    public void setName(String s)
    {
        name = s;
    }

    public void setNickname(String s)
    {
        nickname = s;
    }

    public void setRelay(boolean flag)
    {
        relay = flag;
    }

    public void setThirdpartyName(String s)
    {
        thirdpartyName = s;
    }

    public void setThirdpartyUid(String s)
    {
        thirdpartyUid = s;
    }

    public void setWebAlbum(boolean flag)
    {
        webAlbum = flag;
    }

    public void setWebComment(boolean flag)
    {
        webComment = flag;
    }

    public void setWebFavorite(boolean flag)
    {
        webFavorite = flag;
    }

    public void setWebTrack(boolean flag)
    {
        webTrack = flag;
    }
}
