// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.personal_setting;


// Referenced classes of package com.ximalaya.ting.android.model.personal_setting:
//            ThirdPartyUserInfo

public class FindFriendModel extends ThirdPartyUserInfo
{

    private static final long serialVersionUID = 0x6bc61d883ad3f780L;
    public String followedTime;
    public int followers;
    public String intro;
    public boolean isFollowed;
    public boolean isVerified;
    public String personDescribe;
    public String ptitle;
    public int tracks;
    public String uid;

    public FindFriendModel()
    {
    }
}
