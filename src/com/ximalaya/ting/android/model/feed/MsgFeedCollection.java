// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.feed;

import com.ximalaya.ting.android.model.BaseModel;
import java.util.List;

public class MsgFeedCollection extends BaseModel
{

    public String currentSize;
    public String delNum;
    public List feedData;
    public String noReadNum;
    public String pageSize;
    public String totalSize;

    public MsgFeedCollection()
    {
    }

    public List getBaseFeedList()
    {
        return feedData;
    }

    public String getCurrentSize()
    {
        return currentSize;
    }

    public String getDelNum()
    {
        return delNum;
    }

    public String getNoReadNum()
    {
        return noReadNum;
    }

    public String getPageSize()
    {
        return pageSize;
    }

    public String getTotalSize()
    {
        return totalSize;
    }

    public void setBaseFeedList(List list)
    {
        feedData = list;
    }

    public void setCurrentSize(String s)
    {
        currentSize = s;
    }

    public void setDelNum(String s)
    {
        delNum = s;
    }

    public void setNoReadNum(String s)
    {
        noReadNum = s;
    }

    public void setPageSize(String s)
    {
        pageSize = s;
    }

    public void setTotalSize(String s)
    {
        totalSize = s;
    }
}
