// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.feed;


public class FeedData
{

    public long albumID;
    public String albumImage;
    public String albumName;
    public String cName;
    public int cid;
    public int comments;
    public String content;
    public long createdAt;
    public double duration;
    public long id;
    public boolean isLike;
    public boolean isRelay;
    public int likes;
    public String mAlbumImage;
    public String mUrl;
    public String mtImagePath;
    public String parentID;
    public int playtimes;
    public long recordID;
    public String second;
    public int shares;
    public String tImagePath;
    public String tagName;
    public String tagid;
    public String timeLine;
    public String title;
    public long toFeedid;
    public String toImage;
    public String toNickName;
    public long toTid;
    public long toUid;
    public String trackAvatarPath;
    public String trackTime;
    public String type;
    public int uploadSource;
    public String url;
    public int userSource;
    public String wtImagePath;

    public FeedData()
    {
    }
}
