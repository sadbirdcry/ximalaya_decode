// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.feed;

import com.ximalaya.ting.android.model.BaseModel;
import java.util.List;

public class BaseFeedModel extends BaseModel
{

    public Long createdAt;
    public List datas;
    public boolean flag;
    public String id;
    public String imagePath;
    public String isCreateAlbum;
    public String nickName;
    public Double timeLine;
    public String type;
    public Long uid;

    public BaseFeedModel()
    {
        flag = true;
    }

    public List getChildFeedList()
    {
        return datas;
    }

    public Long getCreatedAt()
    {
        return createdAt;
    }

    public String getId()
    {
        return id;
    }

    public String getImagePath()
    {
        return imagePath;
    }

    public String getNickName()
    {
        return nickName;
    }

    public Double getTimeLine()
    {
        return timeLine;
    }

    public String getType()
    {
        return type;
    }

    public Long getUid()
    {
        return uid;
    }

    public boolean isFlag()
    {
        return flag;
    }

    public void setChildFeedList(List list)
    {
        datas = list;
    }

    public void setCreatedAt(Long long1)
    {
        createdAt = long1;
    }

    public BaseFeedModel setFlag(boolean flag1)
    {
        flag = flag1;
        return this;
    }

    public void setId(String s)
    {
        id = s;
    }

    public void setImagePath(String s)
    {
        imagePath = s;
    }

    public void setNickName(String s)
    {
        nickName = s;
    }

    public void setTimeLine(Double double1)
    {
        timeLine = double1;
    }

    public void setType(String s)
    {
        type = s;
    }

    public void setUid(Long long1)
    {
        uid = long1;
    }
}
