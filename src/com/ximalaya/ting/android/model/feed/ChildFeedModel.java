// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.feed;


public class ChildFeedModel
{

    public Long albumID;
    public String albumImage;
    public String albumName;
    public String albums;
    public String cName;
    public String cid;
    public int comments;
    public String content;
    public Long createdAt;
    public Double duration;
    public String followers;
    public String id;
    public String isFollowed;
    public String isLike;
    public Boolean isRelay;
    public String isVerified;
    public int likes;
    public String mAlbumImage;
    public String mUrl;
    public String mtImagePath;
    public String parentID;
    public String personDescribe;
    public int playtimes;
    public String recordID;
    public String second;
    public int shares;
    public String tImagePath;
    public String tagName;
    public String tagid;
    public String timeLine;
    public String title;
    public String toFeedid;
    public String toImage;
    public String toNickName;
    public String toTid;
    public Long toUid;
    public String trackAvatarPath;
    public String trackTime;
    public String tracks;
    public String type;
    public String uploadSource;
    public String url;
    public String userSource;
    public String wtImagePath;

    public ChildFeedModel()
    {
    }

    public Long getAlbumID()
    {
        return albumID;
    }

    public String getAlbumImage()
    {
        return albumImage;
    }

    public String getAlbumName()
    {
        return albumName;
    }

    public String getAlbums()
    {
        return albums;
    }

    public String getCid()
    {
        return cid;
    }

    public int getComments()
    {
        return comments;
    }

    public String getContent()
    {
        return content;
    }

    public Long getCreatedAt()
    {
        return createdAt;
    }

    public Double getDuration()
    {
        if (duration == null)
        {
            duration = Double.valueOf(0.0D);
        }
        return duration;
    }

    public String getFollowers()
    {
        return followers;
    }

    public String getId()
    {
        return id;
    }

    public String getIsFollowed()
    {
        return isFollowed;
    }

    public String getIsLike()
    {
        return isLike;
    }

    public String getIsVerified()
    {
        return isVerified;
    }

    public int getLikes()
    {
        return likes;
    }

    public String getMtImagePath()
    {
        return mtImagePath;
    }

    public int getPlaytimes()
    {
        return playtimes;
    }

    public String getTagName()
    {
        return tagName;
    }

    public String getTagid()
    {
        return tagid;
    }

    public String getTitle()
    {
        return title;
    }

    public String getToImage()
    {
        return toImage;
    }

    public String getToNickName()
    {
        return toNickName;
    }

    public String getTracks()
    {
        return tracks;
    }

    public String getType()
    {
        return type;
    }

    public String getUrl()
    {
        return url;
    }

    public String getUserSource()
    {
        return userSource;
    }

    public String getWtImagePath()
    {
        return wtImagePath;
    }

    public String getcName()
    {
        return cName;
    }

    public String getmAlbumImage()
    {
        return mAlbumImage;
    }

    public String getmUrl()
    {
        return mUrl;
    }

    public void setAlbumID(Long long1)
    {
        albumID = long1;
    }

    public void setAlbumImage(String s)
    {
        albumImage = s;
    }

    public void setAlbumName(String s)
    {
        albumName = s;
    }

    public void setAlbums(String s)
    {
        albums = s;
    }

    public void setCid(String s)
    {
        cid = s;
    }

    public void setComments(int i)
    {
        comments = i;
    }

    public void setContent(String s)
    {
        content = s;
    }

    public void setCreatedAt(long l)
    {
        createdAt = Long.valueOf(l);
    }

    public void setDuration(Double double1)
    {
        duration = double1;
    }

    public void setFollowers(String s)
    {
        followers = s;
    }

    public void setId(String s)
    {
        id = s;
    }

    public void setIsFollowed(String s)
    {
        isFollowed = s;
    }

    public void setIsLike(String s)
    {
        isLike = s;
    }

    public void setIsVerified(String s)
    {
        isVerified = s;
    }

    public void setLikes(int i)
    {
        likes = i;
    }

    public void setMtImagePath(String s)
    {
        mtImagePath = s;
    }

    public void setPlaytimes(int i)
    {
        playtimes = i;
    }

    public void setTagName(String s)
    {
        tagName = s;
    }

    public void setTagid(String s)
    {
        tagid = s;
    }

    public void setTitle(String s)
    {
        title = s;
    }

    public void setToImage(String s)
    {
        toImage = s;
    }

    public void setToNickName(String s)
    {
        toNickName = s;
    }

    public void setTracks(String s)
    {
        tracks = s;
    }

    public void setType(String s)
    {
        type = s;
    }

    public void setUrl(String s)
    {
        url = s;
    }

    public void setUserSource(String s)
    {
        userSource = s;
    }

    public void setWtImagePath(String s)
    {
        wtImagePath = s;
    }

    public void setcName(String s)
    {
        cName = s;
    }

    public void setmAlbumImage(String s)
    {
        mAlbumImage = s;
    }

    public void setmUrl(String s)
    {
        mUrl = s;
    }
}
