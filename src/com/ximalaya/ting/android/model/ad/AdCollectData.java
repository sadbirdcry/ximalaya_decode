// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.ad;


public class AdCollectData
{

    private String adItemId;
    private String adSource;
    private String androidId;
    private String logType;
    private String positionName;
    private String responseId;
    private String time;
    private String trackId;

    public AdCollectData()
    {
    }

    public String getAdItemId()
    {
        return adItemId;
    }

    public String getAdSource()
    {
        return adSource;
    }

    public String getAndroidId()
    {
        return androidId;
    }

    public String getLogType()
    {
        return logType;
    }

    public String getPositionName()
    {
        return positionName;
    }

    public String getResponseId()
    {
        return responseId;
    }

    public String getTime()
    {
        return time;
    }

    public String getTrackId()
    {
        return trackId;
    }

    public void setAdItemId(String s)
    {
        adItemId = s;
    }

    public void setAdSource(String s)
    {
        adSource = s;
    }

    public void setAndroidId(String s)
    {
        androidId = s;
    }

    public void setLogType(String s)
    {
        logType = s;
    }

    public void setPositionName(String s)
    {
        positionName = s;
    }

    public void setResponseId(String s)
    {
        responseId = s;
    }

    public void setTime(String s)
    {
        time = s;
    }

    public void setTrackId(String s)
    {
        trackId = s;
    }
}
