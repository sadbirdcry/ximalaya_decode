// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.ad;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.modelmanage.AdManager;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.ad:
//            AdCollectData

public class FeedAd2 extends BaseModel
{
    public static class Loader extends AsyncTaskLoader
    {

        private List mData;
        private String mName;

        public volatile void deliverResult(Object obj)
        {
            deliverResult((List)obj);
        }

        public void deliverResult(List list)
        {
            super.deliverResult(list);
            mData = list;
        }

        public volatile Object loadInBackground()
        {
            return loadInBackground();
        }

        public List loadInBackground()
        {
            mData = FeedAd2.getFromRemote(getContext(), mName);
            return mData;
        }

        protected void onStartLoading()
        {
            super.onStartLoading();
            if (mData == null)
            {
                forceLoad();
                return;
            } else
            {
                deliverResult(mData);
                return;
            }
        }

        public Loader(Context context, String s)
        {
            super(context);
            mName = s;
        }
    }


    private static final String PATH = "ting/category";
    public boolean auto;
    public int clickType;
    public String cover;
    public String description;
    public int displayType;
    public String link;
    public int linkType;
    public String name;
    public int openlinkType;
    public int position;
    public String thirdStatUrl;

    public FeedAd2()
    {
    }

    public static List getFromRemote(Context context, String s)
    {
        Object obj;
        obj = null;
        RequestParams requestparams = new RequestParams();
        requestparams.put("name", s);
        requestparams.put("version", ToolUtil.getAppVersion(context));
        requestparams.put("network", NetworkUtils.getNetworkClass(context));
        requestparams.put("operator", NetworkUtils.getOperator(context));
        s = f.a().a((new StringBuilder()).append(a.Q).append("ting/category").toString(), requestparams, null, null, false);
        context = obj;
        if (((com.ximalaya.ting.android.b.n.a) (s)).b != 1)
        {
            break MISSING_BLOCK_LABEL_146;
        }
        context = obj;
        if (TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (s)).a))
        {
            break MISSING_BLOCK_LABEL_146;
        }
        s = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (s)).a);
        context = obj;
        if (s.getInteger("ret").intValue() != 0)
        {
            break MISSING_BLOCK_LABEL_146;
        }
        s = s.getString("data");
        context = obj;
        try
        {
            if (!TextUtils.isEmpty(s))
            {
                context = JSON.parseArray(s, com/ximalaya/ting/android/model/ad/FeedAd2);
            }
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return null;
        }
        return context;
    }

    public static void handleClick(final BaseFragment fragment, FeedAd2 feedad2, String s)
    {
        if (feedad2.linkType == 1 && feedad2.clickType == 1)
        {
            AdCollectData adcollectdata = new AdCollectData();
            String s1 = AdManager.getInstance().getAdIdFromUrl(feedad2.link);
            adcollectdata.setAdItemId(s1);
            adcollectdata.setAdSource("0");
            adcollectdata.setAndroidId(ToolUtil.getAndroidId(fragment.getActivity().getApplicationContext()));
            adcollectdata.setLogType("tingClick");
            adcollectdata.setPositionName(s);
            adcollectdata.setResponseId(s1);
            adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
            adcollectdata.setTrackId("-1");
            s = AdManager.getInstance().getAdRealJTUrl(feedad2.link, adcollectdata);
            if (feedad2.openlinkType == 0)
            {
                ThirdAdStatUtil.getInstance().execAfterDecorateUrl(s, new _cls1());
            } else
            if (feedad2.openlinkType == 1)
            {
                ThirdAdStatUtil.getInstance().execAfterDecorateUrl(s, new _cls2());
                return;
            }
        }
    }

    private class _cls1
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final BaseFragment val$fragment;

        public void execute(String s)
        {
            s = WebFragment.newInstance(s);
            fragment.startFragment(s);
        }

        _cls1()
        {
            fragment = basefragment;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final BaseFragment val$fragment;

        public void execute(String s)
        {
            try
            {
                if (!TextUtils.isEmpty(s))
                {
                    s = new Intent("android.intent.action.VIEW", Uri.parse(s));
                    fragment.startActivity(s);
                }
                return;
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
            }
        }

        _cls2()
        {
            fragment = basefragment;
            super();
        }
    }

}
