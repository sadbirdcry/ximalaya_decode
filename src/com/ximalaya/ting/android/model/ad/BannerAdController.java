// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.ad;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ximalaya.ting.android.fragment.tab.BannerAdLoader;
import com.ximalaya.ting.android.fragment.tab.PlayerCommentBannerAdLoader;
import com.ximalaya.ting.android.fragment.ting.AdsPagerAdapter;
import com.ximalaya.ting.android.modelmanage.AdManager;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.viewpager.ViewPagerInScroll;
import com.ximalaya.ting.android.view.viewpagerindicator.CirclePageIndicator;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.ad:
//            NoPageAd, AdCollectData

public class BannerAdController
    implements android.support.v4.app.LoaderManager.LoaderCallbacks
{
    private class SwapAdRunnable
        implements Runnable
    {

        final BannerAdController this$0;

        public void run()
        {
            if (mBottomAdPager != null && mBottomAdPager.getAdapter() != null && canSwap())
            {
                int i = mBottomAdPager.getCurrentItem();
                int j = mBottomAdPager.getAdapter().getCount();
                mBottomAdPager.setCurrentItem((i + 1) % j, true);
                mBottomAdPager.postDelayed(this, 5000L);
            }
        }

        private SwapAdRunnable()
        {
            this$0 = BannerAdController.this;
            super();
        }

    }


    private static final long SWAP_AD_INTERNAL = 5000L;
    private View adLayout;
    private ViewGroup mAdViewContainer;
    private AdsPagerAdapter mAdsPagerAdapter;
    private ViewPagerInScroll mBottomAdPager;
    private Context mContext;
    private List mData;
    private Fragment mFragment;
    private FragmentManager mFragmentManager;
    private CirclePageIndicator mIndicator;
    private boolean mLoading;
    private String mName;
    private SwapAdRunnable mSwapAdRunnable;
    private long mTrackId;

    public BannerAdController(Context context, ViewGroup viewgroup, String s)
    {
        mLoading = false;
        mContext = context;
        mAdViewContainer = viewgroup;
        mName = s;
        adLayout = LayoutInflater.from(mContext).inflate(0x7f03014a, mAdViewContainer, false);
    }

    public BannerAdController(Context context, ViewGroup viewgroup, String s, long l)
    {
        mLoading = false;
        mContext = context;
        mAdViewContainer = viewgroup;
        mName = s;
        mTrackId = l;
        adLayout = LayoutInflater.from(mContext).inflate(0x7f03014a, mAdViewContainer, false);
    }

    private void buildBannerView(List list)
    {
        adLayout.findViewById(0x7f0a04ff).setVisibility(8);
        mBottomAdPager = (ViewPagerInScroll)adLayout.findViewById(0x7f0a04fe);
        mBottomAdPager.setVisibility(0);
        mIndicator = (CirclePageIndicator)adLayout.findViewById(0x7f0a0230);
        mIndicator.setPagerRealCount(list.size());
        mIndicator.setVisibility(0);
        mAdsPagerAdapter = new AdsPagerAdapter(mFragmentManager, list, mName);
        mBottomAdPager.setAdapter(mAdsPagerAdapter);
        mIndicator.setViewPager(mBottomAdPager);
    }

    private boolean canSwap()
    {
        return mFragment.isAdded() && mFragment.isResumed();
    }

    public View getView()
    {
        return adLayout;
    }

    public void load(Fragment fragment)
    {
        if (!mLoading)
        {
            mFragment = fragment;
            mFragmentManager = fragment.getChildFragmentManager();
            fragment.getLoaderManager().restartLoader(0x7f0a002f, null, this);
        }
    }

    public Loader onCreateLoader(int i, Bundle bundle)
    {
        mLoading = true;
        if ("comm_top".equals(mName))
        {
            return new PlayerCommentBannerAdLoader(mContext, mTrackId);
        } else
        {
            return new BannerAdLoader(mContext, mName);
        }
    }

    public volatile void onLoadFinished(Loader loader, Object obj)
    {
        onLoadFinished(loader, (List)obj);
    }

    public void onLoadFinished(Loader loader, List list)
    {
        mLoading = false;
        if (list != null && list.size() > 0)
        {
            mData = list;
            buildBannerView(list);
            swapAd();
        } else
        {
            adLayout.setVisibility(8);
            loader = adLayout.getLayoutParams();
            if (loader != null)
            {
                loader.width = 0;
                loader.height = 0;
                adLayout.setLayoutParams(loader);
                return;
            }
        }
    }

    public void onLoaderReset(Loader loader)
    {
    }

    public void stopSwapAd()
    {
        if (mBottomAdPager != null && mSwapAdRunnable != null)
        {
            mBottomAdPager.removeCallbacks(mSwapAdRunnable);
            mSwapAdRunnable = null;
        }
    }

    public void swapAd()
    {
        if (mBottomAdPager != null && mSwapAdRunnable == null && mData != null && mData.size() > 1)
        {
            mSwapAdRunnable = new SwapAdRunnable();
            mBottomAdPager.postDelayed(mSwapAdRunnable, 5000L);
            if (mData != null)
            {
                Iterator iterator = mData.iterator();
                do
                {
                    if (!iterator.hasNext())
                    {
                        break;
                    }
                    Object obj = (NoPageAd)iterator.next();
                    if (!TextUtils.isEmpty(((NoPageAd) (obj)).getThirdStatUrl()))
                    {
                        ThirdAdStatUtil.getInstance().thirdAdStatRequest(((NoPageAd) (obj)).getThirdStatUrl());
                    }
                    obj = AdManager.getInstance().getAdIdFromUrl(((NoPageAd) (obj)).getLink());
                    if (!TextUtils.isEmpty(((CharSequence) (obj))))
                    {
                        AdCollectData adcollectdata = new AdCollectData();
                        adcollectdata.setAdItemId(((String) (obj)));
                        adcollectdata.setAdSource("0");
                        adcollectdata.setAndroidId(ToolUtil.getAndroidId(mContext.getApplicationContext()));
                        adcollectdata.setLogType("tingShow");
                        adcollectdata.setPositionName(mName);
                        adcollectdata.setResponseId(((String) (obj)));
                        adcollectdata.setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
                        adcollectdata.setTrackId("-1");
                        DataCollectUtil.getInstance(mContext.getApplicationContext()).statOnlineAd(adcollectdata);
                    }
                } while (true);
            }
        }
    }


}
