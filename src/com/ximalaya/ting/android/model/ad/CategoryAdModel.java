// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.ad;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.model.BaseAdModel;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

public class CategoryAdModel extends BaseAdModel
{

    private static final String PATH;
    private int clickType;
    private String cover;
    private String description;
    private int displayType;
    private String link;
    private int linkType;
    private String name;
    private int openlinkType;
    private int position;
    private String thirdStatUrl;

    public CategoryAdModel()
    {
    }

    public static List getListFromJSON(String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            return JSON.parseArray(s, com/ximalaya/ting/android/model/ad/CategoryAdModel);
        } else
        {
            return null;
        }
    }

    public static List getListFromRemote(Context context, String s)
    {
        Object obj = null;
        RequestParams requestparams = new RequestParams();
        requestparams.put("category", s);
        requestparams.put("scale", 2);
        requestparams.put("version", ToolUtil.getAppVersion(context));
        requestparams.put("network", NetworkUtils.getNetworkClass(context));
        requestparams.put("operator", NetworkUtils.getOperator(context));
        s = f.a().a(PATH, requestparams, null, null, false);
        context = obj;
        if (((com.ximalaya.ting.android.b.n.a) (s)).b != 1)
        {
            break MISSING_BLOCK_LABEL_130;
        }
        context = obj;
        if (TextUtils.isEmpty(((com.ximalaya.ting.android.b.n.a) (s)).a))
        {
            break MISSING_BLOCK_LABEL_130;
        }
        try
        {
            s = JSON.parseObject(((com.ximalaya.ting.android.b.n.a) (s)).a);
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context.printStackTrace();
            return null;
        }
        context = obj;
        if (s.getIntValue("ret") != 0)
        {
            break MISSING_BLOCK_LABEL_130;
        }
        s = s.getString("data");
        context = obj;
        if (!TextUtils.isEmpty(s))
        {
            context = getListFromJSON(s);
        }
        return context;
    }

    public int getClickType()
    {
        return clickType;
    }

    public String getCover()
    {
        return cover;
    }

    public String getDescription()
    {
        return description;
    }

    public int getDisplayType()
    {
        return displayType;
    }

    public String getICover()
    {
        return cover;
    }

    public String getILink()
    {
        return link;
    }

    public int getILinkType()
    {
        return linkType;
    }

    public String getLink()
    {
        return link;
    }

    public int getLinkType()
    {
        return linkType;
    }

    public String getName()
    {
        return name;
    }

    public int getOpenlinkType()
    {
        return openlinkType;
    }

    public int getPosition()
    {
        return position;
    }

    public String getThirdStatUrl()
    {
        return thirdStatUrl;
    }

    public void setClickType(int i)
    {
        clickType = i;
    }

    public void setCover(String s)
    {
        cover = s;
    }

    public void setDescription(String s)
    {
        description = s;
    }

    public void setDisplayType(int i)
    {
        displayType = i;
    }

    public void setLink(String s)
    {
        link = s;
    }

    public void setLinkType(int i)
    {
        linkType = i;
    }

    public void setName(String s)
    {
        name = s;
    }

    public void setOpenlinkType(int i)
    {
        openlinkType = i;
    }

    public void setPosition(int i)
    {
        position = i;
    }

    public void setThirdStatUrl(String s)
    {
        thirdStatUrl = s;
    }

    static 
    {
        PATH = (new StringBuilder()).append(a.Q).append("ting/category").toString();
    }
}
