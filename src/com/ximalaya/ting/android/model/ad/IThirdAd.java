// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.ad;

import android.view.View;

// Referenced classes of package com.ximalaya.ting.android.model.ad:
//            IThirdAdCallback

public interface IThirdAd
{

    public abstract void close();

    public abstract void destroy();

    public abstract View getView();

    public abstract boolean isBuildInClosed();

    public abstract void loadAd();

    public abstract void pause();

    public abstract void resume();

    public abstract void setAdId(String s);

    public abstract void setCallback(IThirdAdCallback ithirdadcallback);
}
