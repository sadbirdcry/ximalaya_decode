// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.ad;

import com.ximalaya.ting.android.library.model.BaseAdModel;

public class FeedBannerAd extends BaseAdModel
{

    private boolean auto;
    private int clickType;
    private String description;
    private int displayType;
    private int linkType;
    private String name;
    private String thirdStatUrl;

    public FeedBannerAd()
    {
    }

    public int getClickType()
    {
        return clickType;
    }

    public String getCover()
    {
        return cover;
    }

    public String getDescription()
    {
        return description;
    }

    public int getDisplayType()
    {
        return displayType;
    }

    public String getICover()
    {
        return cover;
    }

    public String getILink()
    {
        return link;
    }

    public int getILinkType()
    {
        return linkType;
    }

    public String getLink()
    {
        return link;
    }

    public int getLinkType()
    {
        return linkType;
    }

    public String getName()
    {
        return name;
    }

    public String getThirdStatUrl()
    {
        return thirdStatUrl;
    }

    public boolean isAuto()
    {
        return auto;
    }

    public void setAuto(boolean flag)
    {
        auto = flag;
    }

    public void setClickType(int i)
    {
        clickType = i;
    }

    public void setCover(String s)
    {
        cover = s;
    }

    public void setDescription(String s)
    {
        description = s;
    }

    public void setDisplayType(int i)
    {
        displayType = i;
    }

    public void setLink(String s)
    {
        link = s;
    }

    public void setLinkType(int i)
    {
        linkType = i;
    }

    public void setName(String s)
    {
        name = s;
    }

    public void setThirdStatUrl(String s)
    {
        thirdStatUrl = s;
    }
}
