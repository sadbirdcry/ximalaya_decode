// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.ad;

import android.content.Context;
import android.text.TextUtils;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.AdManager;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.model.ad:
//            AbstractSoundAdModel, AdCollectData

public class XMSoundAd extends AbstractSoundAdModel
{

    private long adid;
    private int clickType;
    private String image;
    private String link;
    private String logo;
    private String name;
    private long responseId;
    private String soundUrl;
    private String thirdStatUrl;

    public XMSoundAd()
    {
    }

    public static List getListFromJSONStr(String s)
    {
        return JSON.parseArray(s, com/ximalaya/ting/android/model/ad/XMSoundAd);
    }

    public long getAdId()
    {
        return adid;
    }

    public String getICover()
    {
        return image;
    }

    public String getILink()
    {
        return link;
    }

    public int getILinkType()
    {
        return clickType;
    }

    public String getILogo()
    {
        return logo;
    }

    public String getISoundUrl()
    {
        return soundUrl;
    }

    public String getIThirdStatUrl()
    {
        return thirdStatUrl;
    }

    public String getITitle()
    {
        return name;
    }

    public long getResponseId()
    {
        return responseId;
    }

    public void onAdPlayerRelease(AbstractSoundAdModel abstractsoundadmodel)
    {
    }

    public void onBufferedPlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
    }

    public void onClickAd(final Context context)
    {
        Object obj = new AdCollectData();
        ((AdCollectData) (obj)).setAdItemId((new StringBuilder()).append("").append(adid).toString());
        ((AdCollectData) (obj)).setAdSource("0");
        ((AdCollectData) (obj)).setAndroidId(ToolUtil.getAndroidId(context.getApplicationContext()));
        ((AdCollectData) (obj)).setLogType("soundClick");
        ((AdCollectData) (obj)).setPositionName("sound_patch");
        ((AdCollectData) (obj)).setResponseId((new StringBuilder()).append(responseId).append("").toString());
        ((AdCollectData) (obj)).setTime((new StringBuilder()).append("").append(System.currentTimeMillis()).toString());
        SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
        if (soundinfo != null)
        {
            ((AdCollectData) (obj)).setTrackId((new StringBuilder()).append("").append(soundinfo.trackId).toString());
        } else
        {
            ((AdCollectData) (obj)).setTrackId("-1");
        }
        obj = AdManager.getInstance().getAdRealJTUrl(link, ((AdCollectData) (obj)));
        ThirdAdStatUtil.getInstance().execAfterDecorateUrl(((String) (obj)), new _cls1());
    }

    public void onCompletePlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
    }

    public void onPausePlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
    }

    public void onPicAdClose()
    {
    }

    public void onPicAdShow(boolean flag)
    {
        if (!TextUtils.isEmpty(thirdStatUrl))
        {
            ThirdAdStatUtil.getInstance().thirdAdStatRequest(thirdStatUrl);
        }
    }

    public void onPreparePlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
    }

    public void onPreparedPlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
    }

    public void onResumePlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
    }

    public void onStartPlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
    }

    public void setAdid(long l)
    {
        adid = l;
    }

    public void setClickType(int i)
    {
        clickType = i;
    }

    public void setISoundUrl(String s)
    {
        soundUrl = s;
    }

    public void setImage(String s)
    {
        image = s;
    }

    public void setLink(String s)
    {
        link = s;
    }

    public void setLogo(String s)
    {
        logo = s;
    }

    public void setResponseId(long l)
    {
        responseId = l;
    }

    public void setSoundUrl(String s)
    {
        soundUrl = s;
    }

    public void setThirdStatUrl(String s)
    {
        thirdStatUrl = s;
    }


    private class _cls1
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final XMSoundAd this$0;
        final Context val$context;

        public void execute(String s)
        {
            switch (clickType)
            {
            case 2: // '\002'
            default:
                return;

            case 1: // '\001'
                Intent intent = new Intent(context, com/ximalaya/ting/android/activity/web/WebActivityNew);
                intent.putExtra("ExtraUrl", s);
                context.startActivity(intent);
                return;

            case 3: // '\003'
                openInThirdBrowser(context, s);
                break;
            }
        }

        _cls1()
        {
            this$0 = XMSoundAd.this;
            context = context1;
            super();
        }
    }

}
