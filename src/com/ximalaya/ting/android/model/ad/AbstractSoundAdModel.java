// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.ad;

import android.content.Context;
import com.ximalaya.ting.android.library.model.BaseAdModel;

public abstract class AbstractSoundAdModel extends BaseAdModel
{

    public AbstractSoundAdModel()
    {
    }

    public abstract long getAdId();

    public abstract String getILogo();

    public abstract String getISoundUrl();

    public abstract String getIThirdStatUrl();

    public abstract String getITitle();

    public abstract long getResponseId();

    public abstract void onAdPlayerRelease(AbstractSoundAdModel abstractsoundadmodel);

    public abstract void onBufferedPlayAd(AbstractSoundAdModel abstractsoundadmodel);

    public abstract void onClickAd(Context context);

    public abstract void onCompletePlayAd(AbstractSoundAdModel abstractsoundadmodel);

    public abstract void onPausePlayAd(AbstractSoundAdModel abstractsoundadmodel);

    public abstract void onPicAdClose();

    public abstract void onPicAdShow(boolean flag);

    public abstract void onPreparePlayAd(AbstractSoundAdModel abstractsoundadmodel);

    public abstract void onPreparedPlayAd(AbstractSoundAdModel abstractsoundadmodel);

    public abstract void onResumePlayAd(AbstractSoundAdModel abstractsoundadmodel);

    public abstract void onStartPlayAd(AbstractSoundAdModel abstractsoundadmodel);

    public abstract void setISoundUrl(String s);
}
