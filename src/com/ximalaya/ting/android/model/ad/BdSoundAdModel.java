// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.model.ad;

import android.content.Context;
import android.text.TextUtils;
import com.baidu.alliance.audio.logic.ad.AdvertisementManager;
import com.baidu.alliance.audio.logic.api.model.AdInfo;
import com.baidu.alliance.audio.logic.report.ReportManager;
import com.ximalaya.ting.android.activity.web.WebActivityNew;

// Referenced classes of package com.ximalaya.ting.android.model.ad:
//            AbstractSoundAdModel

public class BdSoundAdModel extends AbstractSoundAdModel
{

    private static final String APP_KEY = "zU52H7trYxE4SA05Ny";
    private static final String APP_SECRET_KEY = "RsZXvOshaDBO711x7SQzgXH3BgDPZs9a";
    private static ReportManager sReportManager;
    private AdInfo mAdInfo;
    private String mLocalPath;
    private long responseId;

    public BdSoundAdModel(AdInfo adinfo)
    {
        mAdInfo = adinfo;
    }

    private static void createReportManager()
    {
        if (sReportManager == null)
        {
            sReportManager = ReportManager.getInstance();
        }
    }

    private static void releaseReportManager()
    {
        if (sReportManager != null)
        {
            sReportManager.release();
            sReportManager = null;
        }
    }

    public static void requestData(Context context, final long responseId, final com.ximalaya.ting.android.transaction.b.e.a callback)
    {
        context = AdvertisementManager.getInstance();
        context.setWebAdvertisementClass(com/ximalaya/ting/android/activity/web/WebActivityNew);
        context.setWebAdvertisementURLKey("ExtraUrl");
        context.setAdvertisementDisplayerResultCallback(new _cls1());
        context.requestAdvertisement();
    }

    public long getAdId()
    {
        if (mAdInfo != null)
        {
            return (long)mAdInfo.id;
        } else
        {
            return 0L;
        }
    }

    public String getICover()
    {
        if (mAdInfo != null && mAdInfo.displayContent != null)
        {
            return mAdInfo.displayContent.picture;
        } else
        {
            return null;
        }
    }

    public String getILink()
    {
        if (mAdInfo != null)
        {
            return mAdInfo.linkUrl;
        } else
        {
            return null;
        }
    }

    public int getILinkType()
    {
        return 0;
    }

    public String getILogo()
    {
        if (mAdInfo != null && mAdInfo.displayContent != null)
        {
            return mAdInfo.displayContent.attachment;
        } else
        {
            return null;
        }
    }

    public String getISoundUrl()
    {
        if (!TextUtils.isEmpty(mLocalPath))
        {
            return mLocalPath;
        }
        if (mAdInfo != null)
        {
            return mAdInfo.audioUrl;
        } else
        {
            return null;
        }
    }

    public String getIThirdStatUrl()
    {
        return null;
    }

    public String getITitle()
    {
        if (mAdInfo != null)
        {
            return mAdInfo.info;
        } else
        {
            return null;
        }
    }

    public long getResponseId()
    {
        return responseId;
    }

    public void onAdPlayerRelease(AbstractSoundAdModel abstractsoundadmodel)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnRelease(mAdInfo);
        }
        releaseReportManager();
    }

    public void onBufferedPlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnBuffct(mAdInfo);
        }
    }

    public void onClickAd(Context context)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnClick(mAdInfo);
        }
    }

    public void onCompletePlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnComplete(mAdInfo, false);
        }
    }

    public void onPausePlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnPause(mAdInfo);
        }
    }

    public void onPicAdClose()
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnClose(mAdInfo);
        }
    }

    public void onPicAdShow(boolean flag)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnShow(mAdInfo);
        }
    }

    public void onPreparePlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnAudioConnectRequest(mAdInfo);
        }
    }

    public void onPreparedPlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnAudioConnectResponse(mAdInfo);
        }
    }

    public void onResumePlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnPlay(mAdInfo);
        }
    }

    public void onStartPlayAd(AbstractSoundAdModel abstractsoundadmodel)
    {
        if (mAdInfo != null)
        {
            createReportManager();
            sReportManager.reportAdvertisementOnPlay(mAdInfo);
        }
    }

    public void setISoundUrl(String s)
    {
        mLocalPath = s;
    }


/*
    static long access$002(BdSoundAdModel bdsoundadmodel, long l)
    {
        bdsoundadmodel.responseId = l;
        return l;
    }

*/

    private class _cls1
        implements AdvertisementResultCallback
    {

        final com.ximalaya.ting.android.transaction.b.e.a val$callback;
        final long val$responseId;

        public void onResultCallback(List list)
        {
label0:
            {
                if (callback != null)
                {
                    if (list == null)
                    {
                        break label0;
                    }
                    ArrayList arraylist = new ArrayList();
                    BdSoundAdModel bdsoundadmodel;
                    for (list = list.iterator(); list.hasNext(); arraylist.add(bdsoundadmodel))
                    {
                        bdsoundadmodel = new BdSoundAdModel((AdInfo)list.next());
                        bdsoundadmodel.responseId = responseId;
                    }

                    callback.a(arraylist);
                }
                return;
            }
            callback.a(null);
        }

        public void onResultError(int i, String s)
        {
            if (callback != null)
            {
                callback.a(null);
            }
        }

        _cls1()
        {
            callback = a;
            responseId = l;
            super();
        }
    }

}
