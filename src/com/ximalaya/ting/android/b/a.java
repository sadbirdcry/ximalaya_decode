// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.b;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.verification_code.VerificationCodeModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.VersionUpdateUtil;
import java.util.Arrays;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.b:
//            f, b

public abstract class a extends AsyncHttpResponseHandler
{

    public static final int STATUS_RET_OK = 0;
    private boolean isPostMethod;
    private RequestParams params;
    private boolean sort;
    private String url;
    private VerificationCodeModel verificationCodeModel;

    public a()
    {
    }

    private void handleNoAuthorized()
    {
        if (MainTabActivity2.mainTabActivity != null)
        {
            SharedPreferencesUtil.getInstance(MyApplication.b()).saveString("loginforesult", null);
            UserInfoMannage.getInstance().setUser(null);
            Intent intent = new Intent(MainTabActivity2.mainTabActivity, com/ximalaya/ting/android/activity/MainTabActivity2);
            intent.putExtra("shouldRelogin", true);
            MainTabActivity2.mainTabActivity.startActivity(intent);
        }
    }

    private boolean retHandle(String s)
    {
        if (s != null)
        {
            int i;
            try
            {
                i = Integer.parseInt(JSON.parseObject(s).getString("ret"));
            }
            catch (Exception exception)
            {
                i = 0;
            }
            if (i == 50)
            {
                handleNoAuthorized();
                return true;
            }
            if (i == 300)
            {
                (new VersionUpdateUtil()).upgrade(s);
                return true;
            }
            if (i == 211 && !f.c)
            {
                Activity activity = MyApplication.a();
                if (activity != null && !activity.isFinishing())
                {
                    f.c = true;
                    activity.runOnUiThread(new b(this, s, activity));
                    return true;
                }
            } else
            {
                return false;
            }
        }
        return true;
    }

    private void retToast()
    {
        if (MainTabActivity2.mainTabActivity != null)
        {
            Toast.makeText(MyApplication.b(), "\u670D\u52A1\u5668\u5728\u7EF4\u62A4\u4E2D\uFF0C\u8BF7\u7A0D\u540E\u91CD\u8BD5!", 1).show();
        }
    }

    public RequestParams getParams()
    {
        return params;
    }

    public String getUrl()
    {
        return url;
    }

    public boolean isPostMethod()
    {
        return isPostMethod;
    }

    public boolean isSort()
    {
        return sort;
    }

    public abstract void onBindXDCS(Header aheader[]);

    public void onFailure(int i, Header aheader[], byte abyte0[], Throwable throwable)
    {
        String s = "";
        if (abyte0 != null)
        {
            s = new String(abyte0);
        }
        Logger.logToSd((new StringBuilder()).append("\u7F51\u7EDC\u9519\u8BEF\u5F00\u59CB\u8BB0\u5F55\uFF1Aheaders:").append(Arrays.toString(aheader)).append("responseBody").append(s).append("statusCode:").append(i).append(" \u7F51\u7EDC\u9519\u8BEF:").append(throwable.toString()).append(":\u7ED3\u675F").toString());
        if (i != 503) goto _L2; else goto _L1
_L1:
        retToast();
_L4:
        onNetError(i, throwable.getMessage());
        return;
_L2:
        if (i == 401)
        {
            handleNoAuthorized();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void onFinish()
    {
    }

    public abstract void onNetError(int i, String s);

    public void onProgress(int i, int j)
    {
    }

    public void onStart()
    {
    }

    public void onSuccess(int i, Header aheader[], byte abyte0[])
    {
        onBindXDCS(aheader);
        if (i == 401)
        {
            handleNoAuthorized();
        } else
        {
            String s = "";
            if (abyte0 != null)
            {
                s = new String(abyte0);
            }
            Logger.logToSd((new StringBuilder()).append("\u7F51\u7EDC\u8BF7\u6C42\u6210\u529F\u5F00\u59CB\u8BB0\u5F55\uFF1Aheaders:").append(Arrays.toString(aheader)).append("responseBody").append(s).append("statusCode:").append(i).append(":\u7ED3\u675F").toString());
            if (abyte0 != null && abyte0.length > 0)
            {
                aheader = new String(abyte0);
                if (!retHandle(aheader))
                {
                    onSuccess(((String) (aheader)));
                    return;
                }
            }
        }
    }

    public abstract void onSuccess(String s);

    public void setParams(RequestParams requestparams)
    {
        params = requestparams;
    }

    public void setPostMethod(boolean flag)
    {
        isPostMethod = flag;
    }

    public void setSort(boolean flag)
    {
        sort = flag;
    }

    public void setUrl(String s)
    {
        url = s;
    }



/*
    static VerificationCodeModel access$002(a a1, VerificationCodeModel verificationcodemodel)
    {
        a1.verificationCodeModel = verificationcodemodel;
        return verificationcodemodel;
    }

*/



}
