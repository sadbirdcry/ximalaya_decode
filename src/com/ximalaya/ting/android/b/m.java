// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.b;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.ResponseHandlerInterface;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpResponseException;
import org.apache.http.util.ByteArrayBuffer;

// Referenced classes of package com.ximalaya.ting.android.b:
//            g

public abstract class m
    implements ResponseHandlerInterface, g
{

    protected static final int BUFFER_SIZE = 4096;
    protected static final String TAG = "SyncHttpResponseHandler";
    protected Header mReqHeaders[];
    protected boolean mReqMode;
    protected URI mReqUri;

    public m()
    {
    }

    public Header[] getRequestHeaders()
    {
        return mReqHeaders;
    }

    public URI getRequestURI()
    {
        return mReqUri;
    }

    byte[] getResponseData(HttpEntity httpentity)
        throws IOException
    {
        InputStream inputstream;
        long l;
        long l1;
        int i = 4096;
        if (httpentity == null)
        {
            break MISSING_BLOCK_LABEL_177;
        }
        inputstream = httpentity.getContent();
        if (inputstream == null)
        {
            break MISSING_BLOCK_LABEL_177;
        }
        l1 = httpentity.getContentLength();
        if (l1 > 0x7fffffffL)
        {
            throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
        }
        byte abyte0[];
        int j;
        if (l1 > 0L)
        {
            i = (int)l1;
        }
        httpentity = new ByteArrayBuffer(i);
        abyte0 = new byte[4096];
        i = 0;
_L2:
        j = inputstream.read(abyte0);
        if (j == -1)
        {
            break; /* Loop/switch isn't completed */
        }
        if (Thread.currentThread().isInterrupted())
        {
            break; /* Loop/switch isn't completed */
        }
        i += j;
        httpentity.append(abyte0, 0, j);
        if (l1 <= 0L)
        {
            l = 1L;
        } else
        {
            l = l1;
        }
        sendProgressMessage(i, (int)l);
        if (true) goto _L2; else goto _L1
_L1:
        try
        {
            AsyncHttpClient.silentCloseInputStream(inputstream);
            return httpentity.toByteArray();
        }
        // Misplaced declaration of an exception variable
        catch (HttpEntity httpentity)
        {
            System.gc();
        }
        break MISSING_BLOCK_LABEL_167;
        httpentity;
        AsyncHttpClient.silentCloseInputStream(inputstream);
        throw httpentity;
        throw new IOException("File too large to fit into available memory");
        return null;
    }

    public boolean getUseSynchronousMode()
    {
        return mReqMode;
    }

    public void onCancel()
    {
    }

    public void onFailure(int i, Header aheader[], byte abyte0[], Throwable throwable)
    {
    }

    public void onRetry(int i)
    {
    }

    public void onSuccess(int i, Header aheader[], byte abyte0[])
    {
    }

    public void sendCancelMessage()
    {
        onCancel();
    }

    public void sendFailureMessage(int i, Header aheader[], byte abyte0[], Throwable throwable)
    {
        onFailure(i, aheader, abyte0, throwable);
    }

    public void sendFinishMessage()
    {
        onFinish();
    }

    public void sendProgressMessage(int i, int j)
    {
        try
        {
            onProgress(i, j);
            return;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    public void sendResponseMessage(HttpResponse httpresponse)
        throws IOException
    {
        StatusLine statusline = httpresponse.getStatusLine();
        byte abyte0[] = getResponseData(httpresponse.getEntity());
        if (statusline.getStatusCode() >= 300)
        {
            sendFailureMessage(statusline.getStatusCode(), httpresponse.getAllHeaders(), abyte0, new HttpResponseException(statusline.getStatusCode(), statusline.getReasonPhrase()));
            return;
        } else
        {
            sendSuccessMessage(statusline.getStatusCode(), httpresponse.getAllHeaders(), abyte0);
            return;
        }
    }

    public void sendRetryMessage(int i)
    {
        onRetry(i);
    }

    public void sendStartMessage()
    {
        onStart();
    }

    public void sendSuccessMessage(int i, Header aheader[], byte abyte0[])
    {
        onSuccess(i, aheader, abyte0);
    }

    public void setRequestHeaders(Header aheader[])
    {
        mReqHeaders = aheader;
    }

    public void setRequestURI(URI uri)
    {
        mReqUri = uri;
    }

    public void setUseSynchronousMode(boolean flag)
    {
        mReqMode = flag;
    }
}
