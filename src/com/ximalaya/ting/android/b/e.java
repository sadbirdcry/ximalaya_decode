// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.b;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import com.loopj.android.http.PersistentCookieStore;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.modelmanage.MyLocationManager;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.player.cdn.CdnCookie;
import com.ximalaya.ting.android.util.NetworkUtils;
import com.ximalaya.ting.android.util.ToolUtil;
import java.net.URLEncoder;
import java.util.HashMap;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.cookie.BasicClientCookie;

public class e
{

    public MyApplication a;
    public String b;
    private String c;

    public e()
    {
    }

    public CdnCookie a(MyApplication myapplication)
    {
        if (myapplication == null)
        {
            return null;
        }
        CdnCookie cdncookie = new CdnCookie();
        HashMap hashmap = new HashMap();
        cdncookie.setDoMain(".ximalaya.com");
        cdncookie.setPath("/");
        Object obj1 = new StringBuilder();
        Object obj;
        int i;
        int j;
        if (a.e)
        {
            obj = "androidpad";
        } else
        {
            obj = "android";
        }
        ((StringBuilder) (obj1)).append(((String) (obj)));
        ((StringBuilder) (obj1)).append("&");
        ((StringBuilder) (obj1)).append(myapplication.i());
        ((StringBuilder) (obj1)).append("&");
        ((StringBuilder) (obj1)).append(myapplication.m());
        hashmap.put((new StringBuilder()).append(a.d).append("&_device").toString(), ((StringBuilder) (obj1)).toString());
        if (UserInfoMannage.hasLogined())
        {
            obj = UserInfoMannage.getInstance().getUser();
            obj1 = new StringBuilder();
            if (obj != null)
            {
                ((StringBuilder) (obj1)).append(((LoginInfoModel) (obj)).uid);
                ((StringBuilder) (obj1)).append("&");
                ((StringBuilder) (obj1)).append(((LoginInfoModel) (obj)).token);
            }
            hashmap.put((new StringBuilder()).append(a.d).append("&_token").toString(), ((StringBuilder) (obj1)).toString());
        }
        Exception exception;
        if (b == null || c == null)
        {
            try
            {
                obj = myapplication.getPackageManager().getApplicationInfo(myapplication.getPackageName(), 128);
                b = ((ApplicationInfo) (obj)).metaData.getString("UMENG_CHANNEL");
                c = ((ApplicationInfo) (obj)).packageName;
            }
            catch (Exception exception1) { }
        }
        hashmap.put("channel", b);
        hashmap.put("impl", c);
        obj1 = MyLocationManager.getInstance(myapplication).getMyLocationStr();
        obj = obj1;
        try
        {
            if (!TextUtils.isEmpty(((CharSequence) (obj1))))
            {
                obj = (new StringBuilder()).append(((String) (obj1))).append(",").append(System.currentTimeMillis()).toString();
            }
            hashmap.put("SUP", URLEncoder.encode(((String) (obj)), "utf-8"));
            hashmap.put("XUM", URLEncoder.encode(ToolUtil.getLocalMacAddress(myapplication), "utf-8"));
        }
        // Misplaced declaration of an exception variable
        catch (Exception exception) { }
        try
        {
            hashmap.put("c-oper", URLEncoder.encode(ToolUtil.getMobileOperatorName(myapplication), "utf-8"));
            hashmap.put("net-mode", URLEncoder.encode(NetworkUtils.getNetTypeDetail(myapplication), "utf-8"));
            i = ToolUtil.getScreenWidth(myapplication);
            j = ToolUtil.getScreenHeight(myapplication);
            hashmap.put("res", URLEncoder.encode((new StringBuilder()).append(i).append(",").append(j).toString(), "utf-8"));
        }
        // Misplaced declaration of an exception variable
        catch (MyApplication myapplication) { }
        cdncookie.setMap(hashmap);
        return cdncookie;
    }

    public CookieStore a(CookieStore cookiestore, boolean flag)
    {
        Object obj = cookiestore;
        if (cookiestore == null)
        {
            obj = new PersistentCookieStore(a);
        }
        ((CookieStore) (obj)).clear();
        Object obj1 = new StringBuilder();
        int i;
        int j;
        if (a.e)
        {
            cookiestore = "androidpad";
        } else
        {
            cookiestore = "android";
        }
        ((StringBuilder) (obj1)).append(cookiestore);
        ((StringBuilder) (obj1)).append("&");
        ((StringBuilder) (obj1)).append(a.i());
        ((StringBuilder) (obj1)).append("&");
        ((StringBuilder) (obj1)).append(a.m());
        cookiestore = new BasicClientCookie((new StringBuilder()).append(a.d).append("&_device").toString(), ((StringBuilder) (obj1)).toString());
        cookiestore.setDomain(".ximalaya.com");
        cookiestore.setPath("/");
        ((CookieStore) (obj)).addCookie(cookiestore);
        if (UserInfoMannage.hasLogined())
        {
            cookiestore = UserInfoMannage.getInstance().getUser();
            obj1 = new StringBuilder();
            if (cookiestore != null)
            {
                ((StringBuilder) (obj1)).append(((LoginInfoModel) (cookiestore)).uid);
                ((StringBuilder) (obj1)).append("&");
                ((StringBuilder) (obj1)).append(((LoginInfoModel) (cookiestore)).token);
            }
            cookiestore = new BasicClientCookie((new StringBuilder()).append(a.d).append("&_token").toString(), ((StringBuilder) (obj1)).toString());
            cookiestore.setDomain(".ximalaya.com");
            cookiestore.setPath("/");
            ((CookieStore) (obj)).addCookie(cookiestore);
        }
        if (b == null || c == null)
        {
            try
            {
                cookiestore = a.getPackageManager().getApplicationInfo(a.getPackageName(), 128);
                b = ((ApplicationInfo) (cookiestore)).metaData.getString("UMENG_CHANNEL");
                c = ((ApplicationInfo) (cookiestore)).packageName;
            }
            // Misplaced declaration of an exception variable
            catch (CookieStore cookiestore) { }
        }
        cookiestore = new BasicClientCookie("channel", b);
        cookiestore.setDomain(".ximalaya.com");
        cookiestore.setPath("/");
        ((CookieStore) (obj)).addCookie(cookiestore);
        cookiestore = new BasicClientCookie("impl", c);
        cookiestore.setDomain(".ximalaya.com");
        cookiestore.setPath("/");
        ((CookieStore) (obj)).addCookie(cookiestore);
        obj1 = MyLocationManager.getInstance(a).getMyLocationStr();
        cookiestore = ((CookieStore) (obj1));
        try
        {
            if (!TextUtils.isEmpty(((CharSequence) (obj1))))
            {
                cookiestore = (new StringBuilder()).append(((String) (obj1))).append(",").append(System.currentTimeMillis()).toString();
            }
            cookiestore = new BasicClientCookie("SUP", URLEncoder.encode(cookiestore, "utf-8"));
            cookiestore.setDomain(".ximalaya.com");
            cookiestore.setPath("/");
            ((CookieStore) (obj)).addCookie(cookiestore);
            cookiestore = new BasicClientCookie("XUM", URLEncoder.encode(ToolUtil.getLocalMacAddress(a), "utf-8"));
            cookiestore.setDomain(".ximalaya.com");
            cookiestore.setPath("/");
            ((CookieStore) (obj)).addCookie(cookiestore);
        }
        // Misplaced declaration of an exception variable
        catch (CookieStore cookiestore) { }
        try
        {
            cookiestore = new BasicClientCookie("c-oper", URLEncoder.encode(ToolUtil.getMobileOperatorName(a), "utf-8"));
            cookiestore.setDomain(".ximalaya.com");
            cookiestore.setPath("/");
            ((CookieStore) (obj)).addCookie(cookiestore);
            cookiestore = new BasicClientCookie("net-mode", URLEncoder.encode(NetworkUtils.getNetTypeDetail(a), "utf-8"));
            cookiestore.setDomain(".ximalaya.com");
            cookiestore.setPath("/");
            ((CookieStore) (obj)).addCookie(cookiestore);
            i = ToolUtil.getScreenWidth(a);
            j = ToolUtil.getScreenHeight(a);
            cookiestore = new BasicClientCookie("res", URLEncoder.encode((new StringBuilder()).append(i).append(",").append(j).toString(), "utf-8"));
            cookiestore.setDomain(".ximalaya.com");
            cookiestore.setPath("/");
            ((CookieStore) (obj)).addCookie(cookiestore);
        }
        // Misplaced declaration of an exception variable
        catch (CookieStore cookiestore)
        {
            return ((CookieStore) (obj));
        }
        return ((CookieStore) (obj));
    }
}
