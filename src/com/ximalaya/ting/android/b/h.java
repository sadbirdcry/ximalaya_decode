// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.b;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.verification_code.VerificationCodeModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.VersionUpdateUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.b:
//            m, f, i

public abstract class h extends m
{

    private boolean isPostMethod;
    private RequestParams params;
    private String responseContent;
    private boolean sort;
    private String url;
    private VerificationCodeModel verificationCodeModel;

    public h()
    {
    }

    private void cancelWait()
    {
        this;
        JVM INSTR monitorenter ;
        f.c = false;
        notifyAll();
        this;
        JVM INSTR monitorexit ;
        return;
        Exception exception;
        exception;
        throw exception;
    }

    private void handleNoAuthorized()
    {
        if (MainTabActivity2.mainTabActivity != null)
        {
            SharedPreferencesUtil.getInstance(MyApplication.b()).saveString("loginforesult", null);
            UserInfoMannage.getInstance().setUser(null);
            Intent intent = new Intent(MainTabActivity2.mainTabActivity, com/ximalaya/ting/android/activity/MainTabActivity2);
            intent.putExtra("shouldRelogin", true);
            MainTabActivity2.mainTabActivity.startActivity(intent);
        }
    }

    private boolean retHandle(String s)
    {
        if (s == null)
        {
            return true;
        }
        int j;
        try
        {
            j = Integer.parseInt(JSON.parseObject(s).getString("ret"));
        }
        catch (Exception exception)
        {
            j = 0;
        }
        if (j == 50)
        {
            handleNoAuthorized();
            return true;
        }
        if (j == 300)
        {
            (new VersionUpdateUtil()).upgrade(s);
            return true;
        }
        if (j == 211 && !f.c)
        {
            showVerificationCode(s);
        }
        return false;
    }

    private void retToast()
    {
        if (MainTabActivity2.mainTabActivity != null)
        {
            Toast.makeText(MyApplication.b(), "\u670D\u52A1\u5668\u5728\u7EF4\u62A4\u4E2D\uFF0C\u8BF7\u7A0D\u540E\u91CD\u8BD5!", 1).show();
        }
    }

    private void showVerificationCode(String s)
    {
        this;
        JVM INSTR monitorenter ;
        Activity activity = MyApplication.a();
        if (activity == null) goto _L2; else goto _L1
_L1:
        if (activity.isFinishing()) goto _L2; else goto _L3
_L3:
        f.c = true;
        activity.runOnUiThread(new i(this, s, activity));
        MyLogger.getLogger().d("test wait begin");
        wait();
_L4:
        MyLogger.getLogger().d("test wait end");
_L2:
        this;
        JVM INSTR monitorexit ;
        return;
        s;
        MyLogger.getLogger().d("test wait interrupt");
          goto _L4
        s;
        throw s;
    }

    public RequestParams getParams()
    {
        return params;
    }

    public String getUrl()
    {
        return url;
    }

    public boolean isPostMethod()
    {
        return isPostMethod;
    }

    public boolean isSort()
    {
        return sort;
    }

    public abstract void onBindXDCS(Header aheader[]);

    public void onFailure(int j, String s)
    {
    }

    public void onFailure(int j, Header aheader[], byte abyte0[], Throwable throwable)
    {
        Logger.log((new StringBuilder()).append("statusCode:").append(j).append(" \u7F51\u7EDC\u9519\u8BEF:").append(throwable.toString()).toString());
        onBindXDCS(aheader);
        if (j != 503) goto _L2; else goto _L1
_L1:
        retToast();
_L4:
        onNetError(j, throwable.getMessage());
        return;
_L2:
        if (j == 401)
        {
            handleNoAuthorized();
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void onProgress(int j, int k)
    {
    }

    public void onSuccess(int j, Header aheader[], byte abyte0[])
    {
        onBindXDCS(aheader);
        if (j == 401)
        {
            handleNoAuthorized();
        } else
        if (abyte0 != null && abyte0.length > 0)
        {
            responseContent = new String(abyte0);
            if (!retHandle(responseContent))
            {
                onSuccess(responseContent);
                return;
            }
        }
    }

    public void setParams(RequestParams requestparams)
    {
        params = requestparams;
    }

    public void setPostMethod(boolean flag)
    {
        isPostMethod = flag;
    }

    public void setSort(boolean flag)
    {
        sort = flag;
    }

    public void setUrl(String s)
    {
        url = s;
    }



/*
    static VerificationCodeModel access$002(h h1, VerificationCodeModel verificationcodemodel)
    {
        h1.verificationCodeModel = verificationcodemodel;
        return verificationcodemodel;
    }

*/




/*
    static String access$302(h h1, String s)
    {
        h1.responseContent = s;
        return s;
    }

*/



}
