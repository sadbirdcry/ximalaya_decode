// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.b;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.xdcs.HttpHeaderModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.http.HttpEntity;

// Referenced classes of package com.ximalaya.ting.android.b:
//            e, n, d, a, 
//            l, h

public class f extends e
{

    public static volatile boolean c = false;
    public static int d = 10000;
    private static f g;
    private static Object h = new Object();
    private AsyncHttpClient e;
    private SyncHttpClient f;

    private f()
    {
        e = new AsyncHttpClient();
        e.setTimeout(d);
        f = new SyncHttpClient();
        f.setTimeout(d);
    }

    public static f a()
    {
        if (g == null)
        {
            synchronized (h)
            {
                g = new f();
            }
        }
        return g;
        exception;
        obj;
        JVM INSTR monitorexit ;
        throw exception;
    }

    private void a(RequestParams requestparams)
    {
        ConcurrentHashMap concurrenthashmap = (ConcurrentHashMap)requestparams.getStringParamsMap();
        if (a != null && !concurrenthashmap.containsKey("device"))
        {
            requestparams.put("device", a.h());
        }
    }

    private void a(String s, RequestParams requestparams)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        return;
_L2:
        try
        {
            s = (HttpHeaderModel)JSON.parseObject(s, com/ximalaya/ting/android/model/xdcs/HttpHeaderModel);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
        if (s == null) goto _L1; else goto _L3
_L3:
        if (s.getSearchpage() > 0)
        {
            requestparams.put("searchpage", s.getSearchpage());
        }
        if (!TextUtils.isEmpty(s.getPosition()))
        {
            requestparams.put("position", s.getPosition());
        }
        if (!TextUtils.isEmpty(s.getTitle()))
        {
            requestparams.put("title", s.getTitle());
        }
        if (!TextUtils.isEmpty(s.getId()))
        {
            requestparams.put("focus_id", s.getId());
        }
        if (!TextUtils.isEmpty(s.getFrom()))
        {
            requestparams.put("focusfrom", s.getFrom());
            return;
        }
          goto _L1
    }

    private void a(String s, String s1, boolean flag)
    {
        if (TextUtils.isEmpty(s1) || s1.equals("0") || s1.equals("-1"))
        {
            a(s);
            return;
        }
        if (flag)
        {
            f.addHeader(s, s1);
            return;
        } else
        {
            e.addHeader(s, s1);
            return;
        }
    }

    private void a(String s, boolean flag)
    {
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        a("x-pId");
        a("x-sId");
        a("x-tId");
        a("x-viewId");
_L4:
        return;
_L2:
        try
        {
            s = (HttpHeaderModel)JSON.parseObject(s, com/ximalaya/ting/android/model/xdcs/HttpHeaderModel);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
        if (s == null) goto _L4; else goto _L3
_L3:
        MyLogger.getLogger().d((new StringBuilder()).append("xdcs data =").append(s).toString());
        a("x-pId", s.getParentId(), flag);
        a("x-sId", s.getSpanId(), flag);
        a("x-tId", s.getTraceId(), flag);
        a("x-viewId", s.getViewId(), flag);
        return;
    }

    private void a(boolean flag, boolean flag1)
    {
        org.apache.http.client.CookieStore cookiestore = a(((org.apache.http.client.CookieStore) (null)), flag);
        if (flag1)
        {
            f.setCookieStore(cookiestore);
            return;
        } else
        {
            e.setCookieStore(cookiestore);
            return;
        }
    }

    private String b(String s)
    {
        return (new StringBuilder()).append(a.u).append(s).toString();
    }

    private String b(String s, RequestParams requestparams)
        throws UnsupportedEncodingException
    {
        ConcurrentHashMap concurrenthashmap;
        ArrayList arraylist;
        Object obj;
        concurrenthashmap = (ConcurrentHashMap)requestparams.getStringParamsMap();
        obj = concurrenthashmap.entrySet().iterator();
        arraylist = new ArrayList();
        for (; ((Iterator) (obj)).hasNext(); arraylist.add((String)((java.util.Map.Entry)((Iterator) (obj)).next()).getKey())) { }
        Collections.sort(arraylist);
        obj = new StringBuffer(s);
        if (s.contains("?")) goto _L2; else goto _L1
_L1:
        ((StringBuffer) (obj)).append("?");
_L4:
        String s1;
        for (s = arraylist.iterator(); s.hasNext(); requestparams.remove(s1))
        {
            s1 = (String)s.next();
            ((StringBuffer) (obj)).append((new StringBuilder()).append(s1).append("=").append(URLEncoder.encode((String)concurrenthashmap.get(s1), "utf-8")).append("&").toString());
        }

        break; /* Loop/switch isn't completed */
_L2:
        if (s.indexOf("?") != s.length() - 1 && s.lastIndexOf("&") != s.length() - 1)
        {
            ((StringBuffer) (obj)).append("&");
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (arraylist.size() > 0 && ((StringBuffer) (obj)).length() > 2)
        {
            return ((StringBuffer) (obj)).substring(0, ((StringBuffer) (obj)).length() - 1);
        } else
        {
            return null;
        }
    }

    private void b(boolean flag, boolean flag1)
    {
        MyApplication myapplication;
        if (flag1)
        {
            f.setUserAgent(null);
        } else
        {
            e.setUserAgent(null);
        }
        myapplication = d();
        if (myapplication != null)
        {
            if (flag1)
            {
                f.addHeader("user-agent", myapplication.l());
                f.addHeader("Accept", "*/*");
            } else
            {
                e.addHeader("user-agent", myapplication.l());
                e.addHeader("Accept", "*/*");
            }
            a(flag, flag1);
        }
    }

    public n.a a(Context context, String s, String s1, HttpEntity httpentity, String s2)
    {
        n n1 = new n();
        b(true, true);
        a(s1, true);
        if (!s.startsWith("http"))
        {
            s = b(s);
        }
        f.post(context, s, httpentity, s2, n1);
        return n1.a();
    }

    public n.a a(String s, RequestParams requestparams, View view, View view1, boolean flag)
    {
        n n1 = new n();
        a(s, requestparams, DataCollectUtil.getDataFromView(view), flag, ((h) (n1)));
        DataCollectUtil.bindDataToView(n1.a().c, view1);
        return n1.a();
    }

    public n.a a(String s, RequestParams requestparams, String s1)
    {
        n n1 = new n();
        a(s, requestparams, s1, ((h) (n1)));
        return n1.a();
    }

    public n.a a(String s, RequestParams requestparams, boolean flag)
    {
        return a(s, requestparams, ((View) (null)), ((View) (null)), flag);
    }

    public String a(String s, RequestParams requestparams, View view, View view1)
    {
        s = a(s, requestparams, view, view1, false);
        if (((n.a) (s)).b == 1)
        {
            return ((n.a) (s)).a;
        } else
        {
            return null;
        }
    }

    public void a(int i)
    {
        e.setTimeout(i);
        f.setTimeout(i);
    }

    public void a(String s)
    {
        e.removeHeader(s);
        f.removeHeader(s);
    }

    public void a(String s, RequestParams requestparams, String s1, com.ximalaya.ting.android.b.a a1)
    {
        a(s, requestparams, s1, a1, false);
    }

    public void a(String s, RequestParams requestparams, String s1, com.ximalaya.ting.android.b.a a1, boolean flag)
    {
        Object obj = a1;
        if (a1 == null)
        {
            obj = new d();
        }
        a1 = requestparams;
        if (requestparams == null)
        {
            a1 = new RequestParams();
        }
        ((com.ximalaya.ting.android.b.a) (obj)).setUrl(s);
        ((com.ximalaya.ting.android.b.a) (obj)).setParams(a1);
        ((com.ximalaya.ting.android.b.a) (obj)).setPostMethod(false);
        ((com.ximalaya.ting.android.b.a) (obj)).setSort(flag);
        b(false, false);
        a(s1, false);
        a(s1, ((RequestParams) (a1)));
        a(((RequestParams) (a1)));
        if (!s.startsWith("http"))
        {
            s = b(s);
        }
        requestparams = s;
        if (flag)
        {
            try
            {
                requestparams = b(s, a1);
            }
            // Misplaced declaration of an exception variable
            catch (RequestParams requestparams)
            {
                requestparams = s;
            }
        }
        e.get(requestparams, a1, ((com.loopj.android.http.ResponseHandlerInterface) (obj)));
    }

    public void a(String s, RequestParams requestparams, String s1, h h1)
    {
        Object obj = h1;
        if (h1 == null)
        {
            obj = new l();
        }
        h1 = requestparams;
        if (requestparams == null)
        {
            h1 = new RequestParams();
        }
        ((h) (obj)).setUrl(s);
        ((h) (obj)).setParams(h1);
        ((h) (obj)).setPostMethod(true);
        b(true, true);
        a(s1, true);
        a(s1, ((RequestParams) (h1)));
        a(((RequestParams) (h1)));
        requestparams = s;
        if (!s.startsWith("http"))
        {
            requestparams = b(s);
        }
        f.post(requestparams, h1, ((com.loopj.android.http.ResponseHandlerInterface) (obj)));
    }

    public void a(String s, RequestParams requestparams, String s1, boolean flag, h h1)
    {
        Object obj = h1;
        if (h1 == null)
        {
            obj = new l();
        }
        h1 = requestparams;
        if (requestparams == null)
        {
            h1 = new RequestParams();
        }
        ((h) (obj)).setUrl(s);
        ((h) (obj)).setParams(h1);
        ((h) (obj)).setSort(flag);
        ((h) (obj)).setPostMethod(false);
        b(false, true);
        a(s1, true);
        a(s1, ((RequestParams) (h1)));
        a(((RequestParams) (h1)));
        if (!s.startsWith("http"))
        {
            s = b(s);
        }
        requestparams = s;
        if (flag)
        {
            try
            {
                requestparams = b(s, h1);
            }
            // Misplaced declaration of an exception variable
            catch (RequestParams requestparams)
            {
                requestparams = s;
            }
        }
        f.get(requestparams, h1, ((com.loopj.android.http.ResponseHandlerInterface) (obj)));
    }

    public void a(String s, String s1)
    {
        e.addHeader(s, s1);
        f.addHeader(s, s1);
    }

    public void a(String s, String s1, com.ximalaya.ting.android.b.a a1)
    {
        c(s, null, s1, a1);
    }

    public AsyncHttpClient b()
    {
        return e;
    }

    public String b(String s, RequestParams requestparams, View view, View view1)
    {
        s = a(s, requestparams, DataCollectUtil.getDataFromView(view));
        DataCollectUtil.bindDataToView(((n.a) (s)).c, view1);
        if (((n.a) (s)).b == 1)
        {
            return ((n.a) (s)).a;
        } else
        {
            return null;
        }
    }

    public void b(String s, RequestParams requestparams, String s1, com.ximalaya.ting.android.b.a a1)
    {
        Object obj = a1;
        if (a1 == null)
        {
            obj = new d();
        }
        a1 = requestparams;
        if (requestparams == null)
        {
            a1 = new RequestParams();
        }
        b(true, false);
        a(s1, false);
        a(s1, a1);
        a(a1);
        requestparams = s;
        if (!s.startsWith("http"))
        {
            requestparams = b(s);
        }
        e.post(requestparams, a1, ((com.loopj.android.http.ResponseHandlerInterface) (obj)));
    }

    public SyncHttpClient c()
    {
        return f;
    }

    public void c(String s, RequestParams requestparams, String s1, com.ximalaya.ting.android.b.a a1)
    {
        if (a1 == null)
        {
            a1 = new d();
        }
        b(true, false);
        a(s1, false);
        a(s1, requestparams);
        if (!s.startsWith("http"))
        {
            s = b(s);
        }
        if (requestparams == null)
        {
            e.delete(s, a1);
            return;
        } else
        {
            e.delete(d(), s, null, requestparams, a1);
            return;
        }
    }

    public MyApplication d()
    {
        if (a != null)
        {
            return a;
        }
        if (MyApplication.b() != null && (MyApplication.b() instanceof MyApplication))
        {
            a = (MyApplication)MyApplication.b();
        }
        return a;
    }

}
