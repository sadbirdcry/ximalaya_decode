// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.model.holder.PersionStationBigHolder;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            HotStationListAdapter

class lder
    implements android.view.StationListAdapter._cls2
{

    final HotStationListAdapter this$0;
    final PersionStationBigHolder val$holder;

    public void onClick(View view)
    {
        Object obj = (StationModel)view.getTag(0x7f090000);
        if (obj == null)
        {
            return;
        }
        if (!UserInfoMannage.hasLogined())
        {
            HotStationListAdapter.access$200(HotStationListAdapter.this, val$holder, ((StationModel) (obj)).isFollowed);
            obj = new Intent(HotStationListAdapter.access$300(HotStationListAdapter.this), com/ximalaya/ting/android/activity/login/LoginActivity);
            ((Intent) (obj)).putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            HotStationListAdapter.access$300(HotStationListAdapter.this).startActivity(((Intent) (obj)));
            return;
        } else
        {
            HotStationListAdapter.access$400(HotStationListAdapter.this, HotStationListAdapter.access$300(HotStationListAdapter.this), ((StationModel) (obj)), val$holder, view);
            return;
        }
    }

    lder()
    {
        this$0 = final_hotstationlistadapter;
        val$holder = PersionStationBigHolder.this;
        super();
    }
}
