// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AboutSettingAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        View border;
        ImageView flagImageView;
        ImageView settingImageView;
        TextView settingnameTextView;

        private ViewHolder()
        {
        }

    }


    private LayoutInflater layoutInflater;
    private String str[];

    public AboutSettingAdapter(Context context, String as[])
    {
        layoutInflater = LayoutInflater.from(context);
        str = as;
    }

    public int getCount()
    {
        return str.length;
    }

    public Object getItem(int i)
    {
        return str[i];
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        view = new ViewHolder();
        viewgroup = layoutInflater.inflate(0x7f0301c4, null);
        view.settingImageView = (ImageView)viewgroup.findViewById(0x7f0a017d);
        view.settingnameTextView = (TextView)viewgroup.findViewById(0x7f0a017e);
        view.flagImageView = (ImageView)viewgroup.findViewById(0x7f0a06c9);
        view.border = viewgroup.findViewById(0x7f0a00a8);
        if (str[i] == null) goto _L2; else goto _L1
_L1:
        ((ViewHolder) (view)).settingnameTextView.setText(str[i]);
        i;
        JVM INSTR tableswitch 0 2: default 120
    //                   0 127
    //                   1 156
    //                   2 185;
           goto _L2 _L3 _L4 _L5
_L2:
        viewgroup.setTag(view);
        return viewgroup;
_L3:
        ((ViewHolder) (view)).settingImageView.setImageResource(0x7f02014d);
        ((ViewHolder) (view)).flagImageView.setVisibility(8);
        ((ViewHolder) (view)).border.setVisibility(0);
        continue; /* Loop/switch isn't completed */
_L4:
        ((ViewHolder) (view)).settingImageView.setImageResource(0x7f020524);
        ((ViewHolder) (view)).flagImageView.setVisibility(8);
        ((ViewHolder) (view)).border.setVisibility(0);
        continue; /* Loop/switch isn't completed */
_L5:
        ((ViewHolder) (view)).settingImageView.setImageResource(0x7f02051a);
        ((ViewHolder) (view)).flagImageView.setVisibility(8);
        ((ViewHolder) (view)).border.setVisibility(8);
        if (true) goto _L2; else goto _L6
_L6:
    }
}
