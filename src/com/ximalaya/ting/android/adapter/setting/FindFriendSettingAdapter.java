// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.personal_setting.FindFriendModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.util.List;

public class FindFriendSettingAdapter extends BaseAdapter
{
    static class ViewHolder
    {

        ToggleButton concern_btn;
        TextView fans_num;
        View ll_progress_false;
        View ll_tempId1;
        TextView personDescribe;
        int posistion;
        TextView sounds_num;
        View station_flag;
        RoundedImageView station_image;
        TextView station_name;
        TextView txt_intro;

        ViewHolder()
        {
        }
    }


    private Activity act;
    public List ffmList;

    public FindFriendSettingAdapter(Activity activity, int i)
    {
        act = activity;
    }

    public FindFriendSettingAdapter(Activity activity, List list)
    {
        act = activity;
        ffmList = list;
    }

    private void setGroup(final FindFriendModel bc, final View v)
    {
        if (UserInfoMannage.hasLogined())
        {
            if (bc.isFollowed)
            {
                (new DialogBuilder(act)).setMessage("\u786E\u5B9A\u8981\u53D6\u6D88\u5173\u6CE8\uFF1F").setOkBtn(new _cls3()).setCancelBtn(new _cls2()).showConfirm();
                return;
            } else
            {
                setGroupRequest(bc, v);
                return;
            }
        } else
        {
            bc = new Intent(act, com/ximalaya/ting/android/activity/login/LoginActivity);
            bc.setFlags(0x20000000);
            bc.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(v));
            act.startActivity(bc);
            return;
        }
    }

    private void setGroupRequest(final FindFriendModel bc, final View v)
    {
        (new _cls4()).myexec(new Void[0]);
    }

    public void close()
    {
        act = null;
        ffmList = null;
    }

    public int getCount()
    {
        if (ffmList == null)
        {
            return 0;
        } else
        {
            return ffmList.size();
        }
    }

    public FindFriendModel getItem(int i)
    {
        if (ffmList == null)
        {
            return null;
        } else
        {
            return (FindFriendModel)ffmList.get(i);
        }
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        FindFriendModel findfriendmodel;
label0:
        {
            int j = 0;
            if (view == null)
            {
                view = act.getLayoutInflater().inflate(0x7f030087, null);
                viewgroup = new ViewHolder();
                viewgroup.concern_btn = (ToggleButton)view.findViewById(0x7f0a01b9);
                viewgroup.fans_num = (TextView)view.findViewById(0x7f0a026c);
                viewgroup.ll_progress_false = view.findViewById(0x7f0a0645);
                viewgroup.ll_tempId1 = view.findViewById(0x7f0a0267);
                viewgroup.personDescribe = (TextView)view.findViewById(0x7f0a026d);
                viewgroup.sounds_num = (TextView)view.findViewById(0x7f0a026b);
                viewgroup.station_flag = view.findViewById(0x7f0a026a);
                viewgroup.station_image = (RoundedImageView)view.findViewById(0x7f0a0268);
                viewgroup.station_name = (TextView)view.findViewById(0x7f0a0269);
                viewgroup.txt_intro = (TextView)view.findViewById(0x7f0a026e);
                ((ViewHolder) (viewgroup)).concern_btn.setOnClickListener(new _cls1());
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (ViewHolder)view.getTag();
            }
            findfriendmodel = (FindFriendModel)ffmList.get(i);
            if (findfriendmodel != null)
            {
                if (!findfriendmodel.isRealData)
                {
                    break label0;
                }
                ((ViewHolder) (viewgroup)).ll_progress_false.setVisibility(8);
                ((ViewHolder) (viewgroup)).ll_tempId1.setVisibility(0);
                ((ViewHolder) (viewgroup)).txt_intro.setVisibility(8);
                ImageManager2.from(act.getApplicationContext()).displayImage(((ViewHolder) (viewgroup)).station_image, findfriendmodel.smallLogo, 0x7f0202df);
                ((ViewHolder) (viewgroup)).station_name.setText(findfriendmodel.nickname);
                ((ViewHolder) (viewgroup)).sounds_num.setText((new StringBuilder()).append("\u58F0\u97F3 ").append(StringUtil.getFriendlyNumStr(findfriendmodel.tracks)).toString());
                ((ViewHolder) (viewgroup)).fans_num.setText((new StringBuilder()).append("\u7C89\u4E1D ").append(StringUtil.getFriendlyNumStr(findfriendmodel.followers)).toString());
                View view1;
                if (Utilities.isBlank(findfriendmodel.personDescribe))
                {
                    ((ViewHolder) (viewgroup)).personDescribe.setVisibility(8);
                } else
                {
                    ((ViewHolder) (viewgroup)).personDescribe.setText(findfriendmodel.personDescribe);
                    ((ViewHolder) (viewgroup)).personDescribe.setVisibility(0);
                }
                view1 = ((ViewHolder) (viewgroup)).station_flag;
                if (!findfriendmodel.isVerified)
                {
                    j = 8;
                }
                view1.setVisibility(j);
                ((ViewHolder) (viewgroup)).concern_btn.setChecked(findfriendmodel.isFollowed);
                viewgroup.posistion = i;
                ((ViewHolder) (viewgroup)).concern_btn.setTag(viewgroup);
            }
            return view;
        }
        if (Utilities.isBlank(findfriendmodel.intro))
        {
            ((ViewHolder) (viewgroup)).ll_progress_false.setVisibility(0);
            ((ViewHolder) (viewgroup)).ll_tempId1.setVisibility(8);
            ((ViewHolder) (viewgroup)).txt_intro.setVisibility(8);
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).ll_progress_false.setVisibility(8);
            ((ViewHolder) (viewgroup)).ll_tempId1.setVisibility(8);
            ((ViewHolder) (viewgroup)).txt_intro.setVisibility(0);
            ((ViewHolder) (viewgroup)).txt_intro.setText(findfriendmodel.intro);
            return view;
        }
    }




    private class _cls3
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final FindFriendSettingAdapter this$0;
        final FindFriendModel val$bc;
        final View val$v;

        public void onExecute()
        {
            setGroupRequest(bc, v);
        }

        _cls3()
        {
            this$0 = FindFriendSettingAdapter.this;
            bc = findfriendmodel;
            v = view;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final FindFriendSettingAdapter this$0;
        final View val$v;

        public void onExecute()
        {
            ((ToggleButton)v).setChecked(true);
        }

        _cls2()
        {
            this$0 = FindFriendSettingAdapter.this;
            v = view;
            super();
        }
    }


    private class _cls4 extends MyAsyncTask
    {

        final FindFriendSettingAdapter this$0;
        final FindFriendModel val$bc;
        final View val$v;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            return CommonRequest.doSetGroup(act.getApplicationContext(), (new StringBuilder()).append(bc.uid).append("").toString(), bc.isFollowed, v, v);
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            boolean flag = false;
            if (act == null || act.isFinishing())
            {
                return;
            }
            if (s == null)
            {
                s = bc;
                if (!bc.isFollowed)
                {
                    flag = true;
                }
                s.isFollowed = flag;
                ((ToggleButton)v).setChecked(bc.isFollowed);
                return;
            } else
            {
                Toast.makeText(act, s, 0).show();
                return;
            }
        }

        protected void onPreExecute()
        {
        }

        _cls4()
        {
            this$0 = FindFriendSettingAdapter.this;
            bc = findfriendmodel;
            v = view;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FindFriendSettingAdapter this$0;

        public void onClick(View view)
        {
            int i = ((ViewHolder)view.getTag()).posistion;
            setGroup((FindFriendModel)ffmList.get(i), view);
        }

        _cls1()
        {
            this$0 = FindFriendSettingAdapter.this;
            super();
        }
    }

}
