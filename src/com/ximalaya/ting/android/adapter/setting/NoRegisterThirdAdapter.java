// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.thirdBind.ThirdPartyUserInfo;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.util.List;

public class NoRegisterThirdAdapter extends BaseAdapter
{
    private class FindInviteThirdTask extends MyAsyncTask
    {

        int index;
        String name;
        final NoRegisterThirdAdapter this$0;
        ImageButton view;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected transient String doInBackground(Object aobj[])
        {
            Object obj;
            Object obj1;
            obj = null;
            name = (String)aobj[0];
            view = (ImageButton)aobj[1];
            index = ((Integer)aobj[2]).intValue();
            aobj = ApiUtil.getApiHost();
            aobj = (new StringBuilder()).append(((String) (aobj))).append("mobile/v1/auth/invite").toString();
            obj1 = new RequestParams();
            if (flag != 2) goto _L2; else goto _L1
_L1:
            ((RequestParams) (obj1)).put("tpName", "tSina");
_L8:
            ((RequestParams) (obj1)).put("names", name);
            obj1 = f.a().b(((String) (aobj)), ((RequestParams) (obj1)), view, view);
            aobj = obj;
            if (obj1 == null) goto _L4; else goto _L3
_L3:
            try
            {
                aobj = JSON.parseObject(((String) (obj1)));
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                ((Exception) (aobj)).printStackTrace();
                aobj = null;
            }
            if (((JSONObject) (aobj)).getInteger("ret").intValue() != 0) goto _L6; else goto _L5
_L5:
            aobj = "0";
_L4:
            return ((String) (aobj));
_L2:
            if (flag == 3)
            {
                ((RequestParams) (obj1)).put("tpName", "tQQ");
            }
            continue; /* Loop/switch isn't completed */
_L6:
            return ((JSONObject) (aobj)).getString("msg");
            if (true) goto _L8; else goto _L7
_L7:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if (activity == null || activity.isFinishing())
            {
                return;
            }
            if ("0".equals(s))
            {
                Toast.makeText(activity, "\u9080\u8BF7\u5DF2\u6210\u529F\u53D1\u51FA", 1).show();
                view.setBackgroundResource(0x7f0202f3);
                ((ThirdPartyUserInfo)list.get(index)).isInvite = true;
                return;
            } else
            {
                Toast.makeText(activity, s, 1).show();
                return;
            }
        }

        private FindInviteThirdTask()
        {
            this$0 = NoRegisterThirdAdapter.this;
            super();
        }

        FindInviteThirdTask(_cls1 _pcls1)
        {
            this();
        }
    }

    private static class ViewHolder
    {

        ImageButton concernImageButton;
        RoundedImageView itemImageView;
        TextView stationNameTextView;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private Activity activity;
    private int flag;
    private LayoutInflater layoutInflater;
    public List list;

    public NoRegisterThirdAdapter(Activity activity1, List list1, int i)
    {
        layoutInflater = LayoutInflater.from(activity1.getApplicationContext());
        activity = activity1;
        flag = i;
        list = list1;
    }

    public int getCount()
    {
        if (list == null)
        {
            return 0;
        } else
        {
            return list.size();
        }
    }

    public ThirdPartyUserInfo getItem(int i)
    {
        return (ThirdPartyUserInfo)list.get(i);
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        ThirdPartyUserInfo thirdpartyuserinfo;
        if (view == null)
        {
            viewgroup = new ViewHolder(null);
            view = layoutInflater.inflate(0x7f03017e, null);
            viewgroup.itemImageView = (RoundedImageView)view.findViewById(0x7f0a0268);
            viewgroup.stationNameTextView = (TextView)view.findViewById(0x7f0a0269);
            viewgroup.concernImageButton = (ImageButton)view.findViewById(0x7f0a01b9);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        thirdpartyuserinfo = getItem(i);
        ((ViewHolder) (viewgroup)).stationNameTextView.setText(thirdpartyuserinfo.nickname);
        ((ViewHolder) (viewgroup)).itemImageView.setTag(0x7f0a003f, Boolean.valueOf(true));
        ImageManager2.from(activity.getApplicationContext()).displayImage(((ViewHolder) (viewgroup)).itemImageView, thirdpartyuserinfo.header, 0x7f0202e0);
        if (thirdpartyuserinfo.isInvite)
        {
            ((ViewHolder) (viewgroup)).concernImageButton.setBackgroundResource(0x7f0202f3);
        } else
        {
            ((ViewHolder) (viewgroup)).concernImageButton.setBackgroundResource(0x7f0202f2);
        }
        ((ViewHolder) (viewgroup)).concernImageButton.setOnClickListener(new _cls1(i));
        return view;
    }



    private class _cls1 extends ExtOnClickListener
    {

        final NoRegisterThirdAdapter this$0;

        public void onClick(View view)
        {
            if (list == null || list.size() <= getPos() || ((ThirdPartyUserInfo)list.get(getPos())).isInvite)
            {
                return;
            }
            Activity activity1 = activity;
            Object obj;
            if (flag == 1)
            {
                obj = "Invite_phone";
            } else
            if (flag == 2)
            {
                obj = "Invite_weibo";
            } else
            if (flag == 3)
            {
                obj = "Invite_qqweibo";
            } else
            {
                obj = "\u9080\u8BF7\u597D\u53CB";
            }
            ToolUtil.onEvent(activity1, ((String) (obj)));
            obj = (ThirdPartyUserInfo)list.get(getPos());
            (new FindInviteThirdTask(null)).myexec(new Object[] {
                ((ThirdPartyUserInfo) (obj)).name, view, Integer.valueOf(getPos())
            });
        }

        _cls1(int i)
        {
            this$0 = NoRegisterThirdAdapter.this;
            super(i);
        }
    }

}
