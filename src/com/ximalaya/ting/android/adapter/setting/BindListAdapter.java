// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import java.util.List;

public class BindListAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        View border;
        TextView otherInfo;
        ImageView rigthImageView;
        ImageView settingImageView;
        TextView settingnameTextView;

        private ViewHolder()
        {
        }

    }


    private LayoutInflater layoutInflater;
    private List list;

    public BindListAdapter(Context context, List list1)
    {
        layoutInflater = LayoutInflater.from(context);
        list = list1;
    }

    public int getCount()
    {
        return list.size();
    }

    public Object getItem(int i)
    {
        return list.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        SettingInfo settinginfo;
        if (view == null)
        {
            viewgroup = new ViewHolder();
            view = layoutInflater.inflate(0x7f03004b, null);
            viewgroup.settingImageView = (ImageView)view.findViewById(0x7f0a017d);
            viewgroup.settingnameTextView = (TextView)view.findViewById(0x7f0a017e);
            viewgroup.otherInfo = (TextView)view.findViewById(0x7f0a017f);
            viewgroup.rigthImageView = (ImageView)view.findViewById(0x7f0a0049);
            viewgroup.border = view.findViewById(0x7f0a00a8);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        settinginfo = (SettingInfo)getItem(i);
        if (i == 1)
        {
            if (!settinginfo.isSetting || !settinginfo.isVerified)
            {
                ((ViewHolder) (viewgroup)).otherInfo.setVisibility(0);
                ((ViewHolder) (viewgroup)).otherInfo.setText(0x7f090104);
            } else
            {
                ((ViewHolder) (viewgroup)).otherInfo.setVisibility(4);
            }
        } else
        if (settinginfo.isSetting)
        {
            if (settinginfo.isExpired)
            {
                ((ViewHolder) (viewgroup)).otherInfo.setVisibility(0);
                ((ViewHolder) (viewgroup)).otherInfo.setText(0x7f090105);
            } else
            {
                ((ViewHolder) (viewgroup)).otherInfo.setVisibility(4);
            }
        } else
        {
            ((ViewHolder) (viewgroup)).otherInfo.setText(0x7f090103);
            ((ViewHolder) (viewgroup)).otherInfo.setVisibility(0);
        }
        if (((ViewHolder) (viewgroup)).otherInfo.getVisibility() == 0)
        {
            ((ViewHolder) (viewgroup)).settingnameTextView.setMaxEms(8);
        } else
        {
            ((ViewHolder) (viewgroup)).settingnameTextView.setMaxEms(12);
        }
        i;
        JVM INSTR tableswitch 0 5: default 196
    //                   0 356
    //                   1 368
    //                   2 380
    //                   3 392
    //                   4 404
    //                   5 416;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7
_L1:
        break; /* Loop/switch isn't completed */
_L7:
        break MISSING_BLOCK_LABEL_416;
_L8:
        ((ViewHolder) (viewgroup)).settingnameTextView.setText((new StringBuilder()).append(settinginfo.nameWake).append(settinginfo.textWake).toString());
        if (i + 1 == list.size())
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(8);
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(0);
            return view;
        }
_L2:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f02051b);
          goto _L8
_L3:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f02051c);
          goto _L8
_L4:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f02051d);
          goto _L8
_L5:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f02051f);
          goto _L8
_L6:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f02051e);
          goto _L8
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f020522);
          goto _L8
    }
}
