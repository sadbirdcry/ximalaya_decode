// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.text.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.model.thirdBind.ThirdPartyUserInfo;
import java.util.List;

public class NoRegisterAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        TextView noRegisterName;
        TextView noRegisterNum;
        ImageButton registerButton;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private String contentMsg;
    private LayoutInflater layoutInflater;
    public List list;
    private Activity mContext;

    public NoRegisterAdapter(Activity activity, List list1, String s)
    {
        layoutInflater = LayoutInflater.from(activity);
        list = list1;
        mContext = activity;
        contentMsg = s;
    }

    private void sendMsg2(ThirdPartyUserInfo thirdpartyuserinfo)
    {
        thirdpartyuserinfo = new Intent("android.intent.action.VIEW", Uri.parse((new StringBuilder()).append("smsto:").append(thirdpartyuserinfo.identity).toString()));
        thirdpartyuserinfo.putExtra("sms_body", contentMsg);
        ((ClipboardManager)mContext.getSystemService("clipboard")).setText(contentMsg);
        Toast.makeText(mContext, "\u6D88\u606F\u5DF2\u590D\u5236\u5230\u7CFB\u7EDF\u526A\u8D34\u677F", 1).show();
        mContext.startActivityForResult(thirdpartyuserinfo, 0);
    }

    public int getCount()
    {
        if (list == null)
        {
            return 0;
        } else
        {
            return list.size();
        }
    }

    public ThirdPartyUserInfo getItem(int i)
    {
        if (list == null)
        {
            return null;
        } else
        {
            return (ThirdPartyUserInfo)list.get(i);
        }
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        ThirdPartyUserInfo thirdpartyuserinfo;
        if (view == null)
        {
            viewgroup = new ViewHolder(null);
            view = layoutInflater.inflate(0x7f03017d, null);
            viewgroup.noRegisterName = (TextView)view.findViewById(0x7f0a05cc);
            viewgroup.noRegisterNum = (TextView)view.findViewById(0x7f0a05cd);
            viewgroup.registerButton = (ImageButton)view.findViewById(0x7f0a05ce);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        thirdpartyuserinfo = getItem(i);
        if (thirdpartyuserinfo.nickname != null)
        {
            ((ViewHolder) (viewgroup)).noRegisterName.setText(thirdpartyuserinfo.nickname);
            ((ViewHolder) (viewgroup)).noRegisterNum.setText((new StringBuilder()).append("").append(thirdpartyuserinfo.identity).toString());
            if (thirdpartyuserinfo.isInvite)
            {
                ((ViewHolder) (viewgroup)).registerButton.setBackgroundResource(0x7f0202f3);
            } else
            {
                ((ViewHolder) (viewgroup)).registerButton.setBackgroundResource(0x7f0202f2);
            }
            ((ViewHolder) (viewgroup)).registerButton.setOnClickListener(new _cls1(i));
        }
        return view;
    }


    private class _cls1 extends ExtOnClickListener
    {

        final NoRegisterAdapter this$0;

        public void onClick(View view)
        {
            if (list.size() > getPos())
            {
                sendMsg2((ThirdPartyUserInfo)list.get(getPos()));
                ((ThirdPartyUserInfo)list.get(getPos())).isInvite = true;
                notifyDataSetChanged();
            }
        }

        _cls1(int i)
        {
            this$0 = NoRegisterAdapter.this;
            super(i);
        }
    }

}
