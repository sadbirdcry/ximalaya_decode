// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.app.Activity;
import android.view.View;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.ximalaya.ting.android.model.personal_setting.FindFriendModel;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.adapter.setting:
//            FindFriendSettingAdapter

class val.v extends MyAsyncTask
{

    final FindFriendSettingAdapter this$0;
    final FindFriendModel val$bc;
    final View val$v;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient String doInBackground(Void avoid[])
    {
        return CommonRequest.doSetGroup(FindFriendSettingAdapter.access$200(FindFriendSettingAdapter.this).getApplicationContext(), (new StringBuilder()).append(val$bc.uid).append("").toString(), val$bc.isFollowed, val$v, val$v);
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((String)obj);
    }

    protected void onPostExecute(String s)
    {
        boolean flag = false;
        if (FindFriendSettingAdapter.access$200(FindFriendSettingAdapter.this) == null || FindFriendSettingAdapter.access$200(FindFriendSettingAdapter.this).isFinishing())
        {
            return;
        }
        if (s == null)
        {
            s = val$bc;
            if (!val$bc.isFollowed)
            {
                flag = true;
            }
            s.isFollowed = flag;
            ((ToggleButton)val$v).setChecked(val$bc.isFollowed);
            return;
        } else
        {
            Toast.makeText(FindFriendSettingAdapter.access$200(FindFriendSettingAdapter.this), s, 0).show();
            return;
        }
    }

    protected void onPreExecute()
    {
    }

    ()
    {
        this$0 = final_findfriendsettingadapter;
        val$bc = findfriendmodel;
        val$v = View.this;
        super();
    }
}
