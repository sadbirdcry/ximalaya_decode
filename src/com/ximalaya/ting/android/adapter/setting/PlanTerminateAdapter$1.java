// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.widget.CompoundButton;
import com.ximalaya.ting.android.activity.setting.PlanTerminateActivity;
import com.ximalaya.ting.android.model.setting.WeekDay;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter.setting:
//            PlanTerminateAdapter

class this._cls0
    implements android.widget.dChangeListener
{

    final PlanTerminateAdapter this$0;

    public void onCheckedChanged(CompoundButton compoundbutton, final boolean isChecked)
    {
        ((WeekDay)PlanTerminateAdapter.access$000(PlanTerminateAdapter.this).get(0)).setSwitchOn(isChecked);
        isSwitchOn = isChecked;
        class _cls1 extends Thread
        {

            final PlanTerminateAdapter._cls1 this$1;
            final boolean val$isChecked;

            public void run()
            {
                SharedPreferencesUtil.getInstance(PlanTerminateAdapter.access$100(this$0).getApplicationContext()).saveBoolean("isOnForPlan", isChecked);
            }

            _cls1()
            {
                this$1 = PlanTerminateAdapter._cls1.this;
                isChecked = flag;
                super();
            }
        }

        (new _cls1()).start();
        if (!isChecked)
        {
            PlanTerminateAdapter.access$100(PlanTerminateAdapter.this).cancelAlarmTask();
            PlanTerminateAdapter.access$100(PlanTerminateAdapter.this).updateSelected(-1);
        }
        if (isChecked)
        {
            PlanTerminateAdapter.access$202(PlanTerminateAdapter.this, PlanTerminateAdapter.access$000(PlanTerminateAdapter.this));
        } else
        {
            PlanTerminateAdapter.access$202(PlanTerminateAdapter.this, new ArrayList());
            PlanTerminateAdapter.access$200(PlanTerminateAdapter.this).add(PlanTerminateAdapter.access$000(PlanTerminateAdapter.this).get(0));
        }
        notifyDataSetChanged();
    }

    _cls1()
    {
        this$0 = PlanTerminateAdapter.this;
        super();
    }
}
