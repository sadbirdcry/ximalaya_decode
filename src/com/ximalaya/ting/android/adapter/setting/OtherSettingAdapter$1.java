// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.widget.CompoundButton;
import com.ximalaya.ting.android.dialog.FollowUpdateToast;
import com.ximalaya.ting.android.model.setting.SettingInfo;

// Referenced classes of package com.ximalaya.ting.android.adapter.setting:
//            OtherSettingAdapter

class val.info
    implements android.widget.edChangeListener
{

    final OtherSettingAdapter this$0;
    final SettingInfo val$info;

    public void onCheckedChanged(CompoundButton compoundbutton, final boolean isChecked)
    {
        int i = ((Integer)compoundbutton.getTag()).intValue();
        val$info.setSetting(isChecked);
        if (i == 0)
        {
            class _cls1 extends Thread
            {

                final OtherSettingAdapter._cls1 this$1;
                final boolean val$isChecked;

                public void run()
                {
                    SharedPreferencesUtil.getInstance(OtherSettingAdapter.access$100(this$0)).saveBoolean("isRadioAlert", isChecked);
                }

            _cls1()
            {
                this$1 = OtherSettingAdapter._cls1.this;
                isChecked = flag;
                super();
            }
            }

            (new _cls1()).start();
            FollowUpdateToast.setIsSound(isChecked);
        } else
        if (i == 1)
        {
            class _cls2 extends Thread
            {

                final OtherSettingAdapter._cls1 this$1;
                final boolean val$isChecked;

                public void run()
                {
                    SharedPreferencesUtil.getInstance(OtherSettingAdapter.access$100(this$0)).saveBoolean("is_download_enabled_in_3g", isChecked);
                }

            _cls2()
            {
                this$1 = OtherSettingAdapter._cls1.this;
                isChecked = flag;
                super();
            }
            }

            (new _cls2()).start();
            return;
        }
    }

    _cls2()
    {
        this$0 = final_othersettingadapter;
        val$info = SettingInfo.this;
        super();
    }
}
