// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.view.View;
import com.ximalaya.ting.android.model.thirdBind.ThirdPartyUserInfo;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter.setting:
//            ExtOnClickListener, NoRegisterThirdAdapter

class  extends ExtOnClickListener
{

    final NoRegisterThirdAdapter this$0;

    public void onClick(View view)
    {
        if (list == null || list.size() <= getPos() || ((ThirdPartyUserInfo)list.get(getPos())).isInvite)
        {
            return;
        }
        android.app.Activity activity = NoRegisterThirdAdapter.access$100(NoRegisterThirdAdapter.this);
        Object obj;
        if (NoRegisterThirdAdapter.access$000(NoRegisterThirdAdapter.this) == 1)
        {
            obj = "Invite_phone";
        } else
        if (NoRegisterThirdAdapter.access$000(NoRegisterThirdAdapter.this) == 2)
        {
            obj = "Invite_weibo";
        } else
        if (NoRegisterThirdAdapter.access$000(NoRegisterThirdAdapter.this) == 3)
        {
            obj = "Invite_qqweibo";
        } else
        {
            obj = "\u9080\u8BF7\u597D\u53CB";
        }
        ToolUtil.onEvent(activity, ((String) (obj)));
        obj = (ThirdPartyUserInfo)list.get(getPos());
        (new ndInviteThirdTask(NoRegisterThirdAdapter.this, null)).myexec(new Object[] {
            ((ThirdPartyUserInfo) (obj)).name, view, Integer.valueOf(getPos())
        });
    }

    ndInviteThirdTask(int i)
    {
        this$0 = NoRegisterThirdAdapter.this;
        super(i);
    }
}
