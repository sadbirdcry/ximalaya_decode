// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.setting.PlanTerminateActivity;
import com.ximalaya.ting.android.model.setting.WeekDay;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import java.util.ArrayList;
import java.util.List;

public class PlanTerminateAdapter extends BaseAdapter
{
    public static class ViewHolder
    {

        public ImageView selectedIcon;
        public SwitchButton slipSwitch;
        public TextView timeLeftTV;
        public TextView wakegnameTextView;

        public ViewHolder()
        {
        }
    }


    private PlanTerminateActivity activity;
    private List currentShowList;
    boolean isSwitchOn;
    private LayoutInflater layoutInflater;
    private List list;

    public PlanTerminateAdapter(PlanTerminateActivity planterminateactivity, List list1)
    {
        isSwitchOn = false;
        activity = planterminateactivity;
        layoutInflater = LayoutInflater.from(planterminateactivity);
        list = list1;
        isSwitchOn = ((WeekDay)list1.get(0)).isSwitchOn();
        if (isSwitchOn)
        {
            currentShowList = list1;
            return;
        } else
        {
            currentShowList = new ArrayList();
            currentShowList.add(list1.get(0));
            return;
        }
    }

    public int getCount()
    {
        return currentShowList.size();
    }

    public WeekDay getItem(int i)
    {
        return (WeekDay)currentShowList.get(i);
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        WeekDay weekday;
        if (view == null)
        {
            viewgroup = new ViewHolder();
            view = layoutInflater.inflate(0x7f030189, null);
            viewgroup.wakegnameTextView = (TextView)view.findViewById(0x7f0a05f6);
            viewgroup.slipSwitch = (SwitchButton)view.findViewById(0x7f0a05f8);
            viewgroup.selectedIcon = (ImageView)view.findViewById(0x7f0a05f9);
            viewgroup.timeLeftTV = (TextView)view.findViewById(0x7f0a05f7);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        weekday = getItem(i);
        ((ViewHolder) (viewgroup)).wakegnameTextView.setText(weekday.getName());
        if (i == 0)
        {
            ((ViewHolder) (viewgroup)).slipSwitch.setVisibility(0);
            ((ViewHolder) (viewgroup)).selectedIcon.setVisibility(4);
            ((ViewHolder) (viewgroup)).slipSwitch.initCheckedState(weekday.isSwitchOn());
            ((ViewHolder) (viewgroup)).slipSwitch.setOnCheckedChangeListener(new _cls1());
            if (weekday.isSwitchOn() && weekday.indexSelected >= 1 && weekday.timeLeft > 0L)
            {
                ((ViewHolder) (viewgroup)).timeLeftTV.setVisibility(0);
                ((ViewHolder) (viewgroup)).timeLeftTV.setText(ToolUtil.toTime(weekday.timeLeft));
                return view;
            } else
            {
                ((ViewHolder) (viewgroup)).timeLeftTV.setVisibility(8);
                return view;
            }
        }
        ((ViewHolder) (viewgroup)).timeLeftTV.setVisibility(8);
        ((ViewHolder) (viewgroup)).slipSwitch.setVisibility(8);
        if (((WeekDay)list.get(i)).isSelected())
        {
            ((ViewHolder) (viewgroup)).selectedIcon.setVisibility(0);
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).selectedIcon.setVisibility(4);
            return view;
        }
    }





/*
    static List access$202(PlanTerminateAdapter planterminateadapter, List list1)
    {
        planterminateadapter.currentShowList = list1;
        return list1;
    }

*/

    private class _cls1
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final PlanTerminateAdapter this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, final boolean isChecked)
        {
            ((WeekDay)list.get(0)).setSwitchOn(isChecked);
            isSwitchOn = isChecked;
            class _cls1 extends Thread
            {

                final _cls1 this$1;
                final boolean val$isChecked;

                public void run()
                {
                    SharedPreferencesUtil.getInstance(activity.getApplicationContext()).saveBoolean("isOnForPlan", isChecked);
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    isChecked = flag;
                    super();
                }
            }

            (new _cls1()).start();
            if (!isChecked)
            {
                activity.cancelAlarmTask();
                activity.updateSelected(-1);
            }
            if (isChecked)
            {
                currentShowList = list;
            } else
            {
                currentShowList = new ArrayList();
                currentShowList.add(list.get(0));
            }
            notifyDataSetChanged();
        }

        _cls1()
        {
            this$0 = PlanTerminateAdapter.this;
            super();
        }
    }

}
