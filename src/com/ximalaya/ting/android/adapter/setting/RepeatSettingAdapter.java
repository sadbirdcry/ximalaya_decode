// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.ximalaya.ting.android.model.setting.WeekDay;
import java.util.HashMap;
import java.util.List;

public class RepeatSettingAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        CheckBox selected;
        TextView wakegnameTextView;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private LayoutInflater layoutInflater;
    private List list;
    HashMap map;

    public RepeatSettingAdapter(Context context, List list1)
    {
        map = new HashMap();
        layoutInflater = LayoutInflater.from(context);
        list = list1;
    }

    public int getCount()
    {
        return list.size();
    }

    public Object getItem(int i)
    {
        return list.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(final int p, View view, ViewGroup viewgroup)
    {
        WeekDay weekday;
        if (view == null)
        {
            viewgroup = new ViewHolder(null);
            view = layoutInflater.inflate(0x7f0301bd, null);
            viewgroup.wakegnameTextView = (TextView)view.findViewById(0x7f0a06b1);
            viewgroup.selected = (CheckBox)view.findViewById(0x7f0a06b2);
            view.setTag(viewgroup);
            ((ViewHolder) (viewgroup)).selected.setOnClickListener(new _cls1());
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        weekday = (WeekDay)getItem(p);
        if (weekday.getName() != null)
        {
            ((ViewHolder) (viewgroup)).wakegnameTextView.setText(weekday.getName());
            ((ViewHolder) (viewgroup)).selected.setChecked(((WeekDay)list.get(p)).isSelected());
        }
        return view;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final RepeatSettingAdapter this$0;
        final int val$p;

        public void onClick(View view)
        {
            view = (CheckBox)view;
            ((WeekDay)list.get(p)).setSelected(view.isChecked());
        }

        _cls1()
        {
            this$0 = RepeatSettingAdapter.this;
            p = i;
            super();
        }
    }

}
