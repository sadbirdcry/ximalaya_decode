// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.util.FreeFlowUtil;

public class AlarmAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        View border;
        ImageView flagImageView;
        ImageView settingImageView;
        TextView settingnameTextView;

        private ViewHolder()
        {
        }

    }


    private Bitmap bitmap01;
    private Bitmap bitmap02;
    private Bitmap bitmap03;
    private LayoutInflater layoutInflater;
    private String str[];

    public AlarmAdapter(Context context, String as[])
    {
        layoutInflater = LayoutInflater.from(context);
        str = as;
        bitmap01 = BitmapFactory.decodeResource(context.getResources(), 0x7f02052d);
        bitmap02 = BitmapFactory.decodeResource(context.getResources(), 0x7f02052a);
        bitmap03 = BitmapFactory.decodeResource(context.getResources(), 0x7f0205a6);
    }

    public int getCount()
    {
        return str.length;
    }

    public Object getItem(int i)
    {
        return str[i];
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        view = new ViewHolder();
        viewgroup = layoutInflater.inflate(0x7f0301c4, null);
        view.settingImageView = (ImageView)viewgroup.findViewById(0x7f0a017d);
        view.settingnameTextView = (TextView)viewgroup.findViewById(0x7f0a017e);
        view.flagImageView = (ImageView)viewgroup.findViewById(0x7f0a06c9);
        view.border = viewgroup.findViewById(0x7f0a00a8);
        if (str[i] == null) goto _L2; else goto _L1
_L1:
        ((ViewHolder) (view)).settingnameTextView.setText(str[i]);
        i;
        JVM INSTR tableswitch 0 2: default 120
    //                   0 127
    //                   1 149
    //                   2 190;
           goto _L2 _L3 _L4 _L5
_L2:
        viewgroup.setTag(view);
        return viewgroup;
_L3:
        ((ViewHolder) (view)).settingImageView.setImageBitmap(bitmap01);
        ((ViewHolder) (view)).border.setVisibility(0);
        continue; /* Loop/switch isn't completed */
_L4:
        ((ViewHolder) (view)).settingImageView.setImageBitmap(bitmap02);
        ((ViewHolder) (view)).flagImageView.setVisibility(8);
        if (str.length == 2)
        {
            ((ViewHolder) (view)).border.setVisibility(8);
        }
        continue; /* Loop/switch isn't completed */
_L5:
        ((ViewHolder) (view)).settingImageView.setImageBitmap(bitmap03);
        if (FreeFlowUtil.getInstance().getOrderStatus() == 1)
        {
            ((ViewHolder) (view)).flagImageView.setVisibility(8);
        } else
        {
            ((ViewHolder) (view)).flagImageView.setVisibility(0);
        }
        ((ViewHolder) (view)).settingnameTextView.setText("\u514D\u6D41\u91CF\u670D\u52A1");
        ((ViewHolder) (view)).border.setVisibility(8);
        if (true) goto _L2; else goto _L6
_L6:
    }
}
