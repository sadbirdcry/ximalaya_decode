// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import java.util.List;

public class OtherSettingAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        View border;
        TextView intrTextView;
        SwitchButton radioSwitch;
        ImageView settingImageView;
        TextView settingnameTextView;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private Context context;
    private LayoutInflater layoutInflater;
    private List otherInfos;

    public OtherSettingAdapter(Context context1, List list)
    {
        context = context1;
        layoutInflater = LayoutInflater.from(context1);
        otherInfos = list;
    }

    public int getCount()
    {
        return otherInfos.size();
    }

    public Object getItem(int i)
    {
        return otherInfos.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        final SettingInfo info;
        viewgroup = new ViewHolder(null);
        if (view == null)
        {
            view = layoutInflater.inflate(0x7f0301c6, null);
            viewgroup.settingImageView = (ImageView)view.findViewById(0x7f0a06ce);
            viewgroup.settingnameTextView = (TextView)view.findViewById(0x7f0a06cf);
            viewgroup.intrTextView = (TextView)view.findViewById(0x7f0a06d1);
            viewgroup.radioSwitch = (SwitchButton)view.findViewById(0x7f0a06d0);
            viewgroup.border = view.findViewById(0x7f0a00a8);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        info = (SettingInfo)getItem(i);
        if (info == null) goto _L2; else goto _L1
_L1:
        ((ViewHolder) (viewgroup)).settingnameTextView.setText(info.nameWake);
        ((ViewHolder) (viewgroup)).radioSwitch.setTag(Integer.valueOf(i));
        ((ViewHolder) (viewgroup)).radioSwitch.initCheckedState(info.isSetting);
        ((ViewHolder) (viewgroup)).radioSwitch.setOnCheckedChangeListener(new _cls1());
        i;
        JVM INSTR tableswitch 0 6: default 200
    //                   0 218
    //                   1 254
    //                   2 290
    //                   3 356
    //                   4 404
    //                   5 440
    //                   6 476;
           goto _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9
_L2:
        view.setTag(viewgroup);
        return view;
_L3:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f02047f);
        ((ViewHolder) (viewgroup)).radioSwitch.setVisibility(0);
        ((ViewHolder) (viewgroup)).intrTextView.setVisibility(4);
        ((ViewHolder) (viewgroup)).border.setVisibility(0);
        continue; /* Loop/switch isn't completed */
_L4:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f020376);
        ((ViewHolder) (viewgroup)).radioSwitch.setVisibility(0);
        ((ViewHolder) (viewgroup)).intrTextView.setVisibility(4);
        ((ViewHolder) (viewgroup)).border.setVisibility(0);
        continue; /* Loop/switch isn't completed */
_L5:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f020375);
        ((ViewHolder) (viewgroup)).radioSwitch.setVisibility(4);
        ((ViewHolder) (viewgroup)).intrTextView.setVisibility(0);
        ((ViewHolder) (viewgroup)).intrTextView.setText((new StringBuilder()).append(info.spaceOccupySize).append("M").toString());
        ((ViewHolder) (viewgroup)).border.setVisibility(0);
        continue; /* Loop/switch isn't completed */
_L6:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f0201f9);
        ((ViewHolder) (viewgroup)).radioSwitch.setVisibility(4);
        ((ViewHolder) (viewgroup)).intrTextView.setVisibility(0);
        ((ViewHolder) (viewgroup)).intrTextView.setText(info.textWake);
        ((ViewHolder) (viewgroup)).border.setVisibility(0);
        continue; /* Loop/switch isn't completed */
_L7:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f020529);
        ((ViewHolder) (viewgroup)).radioSwitch.setVisibility(4);
        ((ViewHolder) (viewgroup)).intrTextView.setVisibility(0);
        ((ViewHolder) (viewgroup)).border.setVisibility(0);
        continue; /* Loop/switch isn't completed */
_L8:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f02052c);
        ((ViewHolder) (viewgroup)).radioSwitch.setVisibility(4);
        ((ViewHolder) (viewgroup)).intrTextView.setVisibility(0);
        ((ViewHolder) (viewgroup)).border.setVisibility(0);
        continue; /* Loop/switch isn't completed */
_L9:
        ((ViewHolder) (viewgroup)).settingImageView.setImageResource(0x7f020521);
        ((ViewHolder) (viewgroup)).radioSwitch.setVisibility(4);
        ((ViewHolder) (viewgroup)).intrTextView.setVisibility(0);
        ((ViewHolder) (viewgroup)).border.setVisibility(8);
        if (true) goto _L2; else goto _L10
_L10:
    }


    private class _cls1
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final OtherSettingAdapter this$0;
        final SettingInfo val$info;

        public void onCheckedChanged(CompoundButton compoundbutton, final boolean isChecked)
        {
            int i = ((Integer)compoundbutton.getTag()).intValue();
            info.setSetting(isChecked);
            if (i == 0)
            {
                class _cls1 extends Thread
                {

                    final _cls1 this$1;
                    final boolean val$isChecked;

                    public void run()
                    {
                        SharedPreferencesUtil.getInstance(context).saveBoolean("isRadioAlert", isChecked);
                    }

                _cls1()
                {
                    this$1 = _cls1.this;
                    isChecked = flag;
                    super();
                }
                }

                (new _cls1()).start();
                FollowUpdateToast.setIsSound(isChecked);
            } else
            if (i == 1)
            {
                class _cls2 extends Thread
                {

                    final _cls1 this$1;
                    final boolean val$isChecked;

                    public void run()
                    {
                        SharedPreferencesUtil.getInstance(context).saveBoolean("is_download_enabled_in_3g", isChecked);
                    }

                _cls2()
                {
                    this$1 = _cls1.this;
                    isChecked = flag;
                    super();
                }
                }

                (new _cls2()).start();
                return;
            }
        }

        _cls1()
        {
            this$0 = OtherSettingAdapter.this;
            info = settinginfo;
            super();
        }
    }

}
