// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import java.util.ArrayList;
import java.util.List;

public class WakeAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        ProgressBar mPorgressBar;
        ImageView nextvImageView;
        SwitchButton slipSwitch;
        TextView wakeTextView;
        TextView wakegnameTextView;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private Activity activity;
    private LayoutInflater layoutInflater;
    private List list;
    private List tempList;

    public WakeAdapter(Activity activity1, List list1)
    {
        layoutInflater = LayoutInflater.from(activity1);
        list = list1;
        if (((SettingInfo)list1.get(0)).isSetting())
        {
            tempList = list1;
        } else
        {
            tempList = new ArrayList();
            tempList.add(list1.get(0));
        }
        activity = activity1;
    }

    public int getCount()
    {
        return tempList.size();
    }

    public Object getItem(int i)
    {
        return tempList.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        SettingInfo settinginfo;
        if (view == null)
        {
            viewgroup = new ViewHolder(null);
            view = layoutInflater.inflate(0x7f0301fd, null);
            viewgroup.wakegnameTextView = (TextView)view.findViewById(0x7f0a0743);
            viewgroup.wakeTextView = (TextView)view.findViewById(0x7f0a0745);
            viewgroup.slipSwitch = (SwitchButton)view.findViewById(0x7f0a0744);
            viewgroup.nextvImageView = (ImageView)view.findViewById(0x7f0a037c);
            viewgroup.mPorgressBar = (ProgressBar)view.findViewById(0x7f0a0093);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        settinginfo = (SettingInfo)getItem(i);
        if (settinginfo.getNameWake() != null)
        {
            ((ViewHolder) (viewgroup)).wakegnameTextView.setText(settinginfo.getNameWake());
            ((ViewHolder) (viewgroup)).wakeTextView.setText(settinginfo.getTextWake());
            if (i == 0)
            {
                ((ViewHolder) (viewgroup)).slipSwitch.setVisibility(0);
                ((ViewHolder) (viewgroup)).slipSwitch.setChecked(settinginfo.isSetting());
                ((ViewHolder) (viewgroup)).nextvImageView.setVisibility(8);
            } else
            {
                ((ViewHolder) (viewgroup)).nextvImageView.setVisibility(0);
                ((ViewHolder) (viewgroup)).slipSwitch.setVisibility(8);
            }
        }
        ((ViewHolder) (viewgroup)).slipSwitch.setOnCheckedChangeListener(new _cls1());
        return view;
    }




/*
    static List access$202(WakeAdapter wakeadapter, List list1)
    {
        wakeadapter.tempList = list1;
        return list1;
    }

*/


    private class _cls1
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final WakeAdapter this$0;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            ((SettingInfo)list.get(0)).setSetting(flag);
            if (flag)
            {
                tempList = list;
            } else
            {
                tempList = new ArrayList();
                tempList.add(list.get(0));
            }
            ((WakeUpSettingActivity)activity).saveWakeupSwitch(flag);
        }

        _cls1()
        {
            this$0 = WakeAdapter.this;
            super();
        }
    }

}
