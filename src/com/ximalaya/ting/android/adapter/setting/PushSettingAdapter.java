// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.setting;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.setting.PushSettingActivity;
import com.ximalaya.ting.android.model.setting.SettingInfo;
import com.ximalaya.ting.android.view.switchbtn.SwitchButton;
import java.util.List;

public class PushSettingAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        TextView nameTextView;
        SwitchButton slipSwitch;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private PushSettingActivity activity;
    private LayoutInflater layoutInflater;
    private List list;

    public PushSettingAdapter(Activity activity1, List list1)
    {
        layoutInflater = LayoutInflater.from(activity1);
        activity = (PushSettingActivity)activity1;
        list = list1;
    }

    public int getCount()
    {
        return list.size();
    }

    public Object getItem(int i)
    {
        return list.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
        SettingInfo settinginfo;
        if (view == null)
        {
            viewgroup = new ViewHolder(null);
            view = layoutInflater.inflate(0x7f03019e, null);
            viewgroup.nameTextView = (TextView)view.findViewById(0x7f0a0652);
            viewgroup.slipSwitch = (SwitchButton)view.findViewById(0x7f0a0653);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        settinginfo = (SettingInfo)getItem(position);
        if (settinginfo.getNameWake() != null)
        {
            ((ViewHolder) (viewgroup)).nameTextView.setText(settinginfo.getNameWake());
            ((ViewHolder) (viewgroup)).slipSwitch.initCheckedState(settinginfo.isSetting);
            ((ViewHolder) (viewgroup)).slipSwitch.setOnCheckedChangeListener(new _cls1());
        }
        return view;
    }


    private class _cls1
        implements android.widget.CompoundButton.OnCheckedChangeListener
    {

        final PushSettingAdapter this$0;
        final int val$position;

        public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
        {
            activity.saveSwitched(flag, position);
        }

        _cls1()
        {
            this$0 = PushSettingAdapter.this;
            position = i;
            super();
        }
    }

}
