// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.MyFileUtils;
import com.ximalaya.ting.android.util.StorageUtils;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.io.File;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class SoundsDownloadAdapter extends BaseListSoundsAdapter
    implements android.view.View.OnClickListener
{
    public static interface DelDownloadListener
    {

        public abstract void remove();
    }

    private static class ViewHolder
    {

        TextView alltimeNumTV;
        View border;
        TextView commentsNumTV;
        LinearLayout context;
        TextView createTimeTV;
        RoundedImageView itemImageView;
        TextView likesNumTV;
        TextView playtimesNumTV;
        int position;
        ProgressBar progress;
        LinearLayout sdcardStatusLayout;
        TextView sflagTV;
        TextView soundfileStatusTV;
        TextView soundsNameText;
        LinearLayout statusContainer;
        ImageView statusIV;
        TextView statusNameTV;
        TextView statusflagTV;
        TextView transmitNumTV;
        TextView usernameTextView;

        private ViewHolder()
        {
        }

    }


    private DelDownloadListener delDownload;
    private boolean mIsUnfinishDownload;

    public SoundsDownloadAdapter(Activity activity, List list)
    {
        super(activity, list);
    }

    public SoundsDownloadAdapter(ListView listview, Activity activity, List list)
    {
        super(activity, list);
    }

    private void delDownLoad(int i)
    {
        if (mData != null && mData.size() > 0 && i < mData.size() && (DownloadTask)mData.get(i) != null)
        {
            DownloadHandler downloadhandler = DownloadHandler.getInstance(mContext);
            PlayListControl.getPlayListManager().doBeforeDelete((SoundInfo)mData.get(i));
            if (downloadhandler.delDownloadTask((DownloadTask)mData.get(i)) == 0 && delDownload != null)
            {
                delDownload.remove();
                return;
            }
        }
    }

    private String getDownloadedInMBString(DownloadTask downloadtask)
    {
        String s1 = "0.00";
        String s = s1;
        if (downloadtask != null)
        {
            s = s1;
            if (downloadtask.downloaded >= 0L)
            {
                s = ToolUtil.toMBFormatString(downloadtask.downloaded);
            }
        }
        return s;
    }

    private String getSizeInMBString(DownloadTask downloadtask)
    {
        String s1 = "0.00";
        String s = s1;
        if (downloadtask != null)
        {
            s = s1;
            if (downloadtask.filesize >= 0L)
            {
                s = ToolUtil.toMBFormatString(downloadtask.filesize);
            }
        }
        return s;
    }

    protected void bindData(DownloadTask downloadtask, BaseListSoundsAdapter.ViewHolder viewholder)
    {
    }

    protected volatile void bindData(Object obj, BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((DownloadTask)obj, viewholder);
    }

    public DelDownloadListener getDelDownload()
    {
        return delDownload;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        DownloadTask downloadtask;
        int j;
        String s;
        TextView textview;
        boolean flag;
        boolean flag1;
        if (view == null)
        {
            viewgroup = new ViewHolder();
            view = new RelativeLayout(mContext);
            LayoutInflater.from(mContext).inflate(0x7f030077, (RelativeLayout)view, true);
            viewgroup.itemImageView = (RoundedImageView)view.findViewById(0x7f0a0177);
            viewgroup.usernameTextView = (TextView)view.findViewById(0x7f0a0219);
            viewgroup.sflagTV = (TextView)view.findViewById(0x7f0a0217);
            viewgroup.createTimeTV = (TextView)view.findViewById(0x7f0a0216);
            viewgroup.soundsNameText = (TextView)view.findViewById(0x7f0a0218);
            viewgroup.playtimesNumTV = (TextView)view.findViewById(0x7f0a021a);
            viewgroup.likesNumTV = (TextView)view.findViewById(0x7f0a021c);
            viewgroup.commentsNumTV = (TextView)view.findViewById(0x7f0a021d);
            viewgroup.transmitNumTV = (TextView)view.findViewById(0x7f0a021e);
            viewgroup.alltimeNumTV = (TextView)view.findViewById(0x7f0a021b);
            viewgroup.context = (LinearLayout)view.findViewById(0x7f0a0214);
            viewgroup.statusContainer = (LinearLayout)view.findViewById(0x7f0a021f);
            viewgroup.statusIV = (ImageView)view.findViewById(0x7f0a0220);
            viewgroup.statusNameTV = (TextView)view.findViewById(0x7f0a0221);
            viewgroup.progress = (ProgressBar)view.findViewById(0x7f0a0223);
            viewgroup.statusflagTV = (TextView)view.findViewById(0x7f0a0224);
            viewgroup.sdcardStatusLayout = (LinearLayout)view.findViewById(0x7f0a0226);
            viewgroup.soundfileStatusTV = (TextView)view.findViewById(0x7f0a0227);
            viewgroup.border = view.findViewById(0x7f0a00a8);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        if (i + 1 == mData.size())
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(4);
        } else
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(0);
        }
        downloadtask = (DownloadTask)getItem(i);
        ((ViewHolder) (viewgroup)).soundsNameText.setText(downloadtask.title);
        ((ViewHolder) (viewgroup)).context.setBackgroundResource(0x7f02006e);
        if (downloadtask.create_at <= 0L)
        {
            ((ViewHolder) (viewgroup)).createTimeTV.setVisibility(8);
        } else
        {
            ((ViewHolder) (viewgroup)).createTimeTV.setText(ToolUtil.convertTime(downloadtask.create_at));
        }
        textview = ((ViewHolder) (viewgroup)).usernameTextView;
        if (TextUtils.isEmpty(downloadtask.nickname))
        {
            s = "";
        } else
        {
            s = (new StringBuilder()).append("by ").append(downloadtask.nickname).toString();
        }
        textview.setText(s);
        if (downloadtask.plays_counts > 0)
        {
            ((ViewHolder) (viewgroup)).playtimesNumTV.setText(StringUtil.getFriendlyNumStr(downloadtask.plays_counts));
            ((ViewHolder) (viewgroup)).playtimesNumTV.setVisibility(0);
            flag = true;
        } else
        {
            ((ViewHolder) (viewgroup)).playtimesNumTV.setVisibility(8);
            flag = false;
        }
        if (downloadtask.favorites_counts > 0)
        {
            if (!flag)
            {
                ((ViewHolder) (viewgroup)).playtimesNumTV.setText(StringUtil.getFriendlyNumStr(downloadtask.favorites_counts));
            }
            ((ViewHolder) (viewgroup)).likesNumTV.setText(StringUtil.getFriendlyNumStr(downloadtask.favorites_counts));
            ((ViewHolder) (viewgroup)).likesNumTV.setVisibility(0);
            if (downloadtask.is_favorited)
            {
                ((ViewHolder) (viewgroup)).likesNumTV.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020090), null, null, null);
                j = 1;
            } else
            {
                ((ViewHolder) (viewgroup)).likesNumTV.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020092), null, null, null);
                j = 1;
            }
        } else
        {
            ((ViewHolder) (viewgroup)).likesNumTV.setVisibility(8);
            j = 0;
        }
        if (downloadtask.comments_counts > 0)
        {
            ((ViewHolder) (viewgroup)).commentsNumTV.setText(StringUtil.getFriendlyNumStr(downloadtask.comments_counts));
            ((ViewHolder) (viewgroup)).commentsNumTV.setVisibility(0);
            flag1 = true;
        } else
        {
            ((ViewHolder) (viewgroup)).commentsNumTV.setVisibility(8);
            flag1 = false;
        }
        if (downloadtask.shares_counts > 0)
        {
            ((ViewHolder) (viewgroup)).transmitNumTV.setText(StringUtil.getFriendlyNumStr(downloadtask.shares_counts));
            ((ViewHolder) (viewgroup)).transmitNumTV.setVisibility(0);
        } else
        {
            ((ViewHolder) (viewgroup)).transmitNumTV.setVisibility(8);
        }
        if (flag || j != 0 || flag1 || downloadtask.duration > 0.0D)
        {
            ((ViewHolder) (viewgroup)).alltimeNumTV.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)downloadtask.duration)).toString());
            ((ViewHolder) (viewgroup)).alltimeNumTV.setVisibility(0);
        } else
        {
            ((ViewHolder) (viewgroup)).alltimeNumTV.setVisibility(8);
        }
        ((ViewHolder) (viewgroup)).itemImageView.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).itemImageView, downloadtask.coverSmall, 0x7f0202de);
        if (downloadtask.user_source == 1)
        {
            ((ViewHolder) (viewgroup)).sflagTV.setText("\u539F\u521B");
        } else
        {
            ((ViewHolder) (viewgroup)).sflagTV.setText("\u91C7\u96C6");
        }
        j = downloadtask.getDownloadPercent();
        if (j < 0 || j >= 100 || downloadtask.downloadStatus == 4) goto _L2; else goto _L1
_L1:
        ((ViewHolder) (viewgroup)).progress.setVisibility(4);
        downloadtask.downloadStatus;
        JVM INSTR tableswitch 0 3: default 772
    //                   0 1071
    //                   1 1275
    //                   2 968
    //                   3 1173;
           goto _L3 _L4 _L5 _L6 _L7
_L3:
        ((ViewHolder) (viewgroup)).statusContainer.setVisibility(8);
_L9:
        viewgroup.position = i;
        return view;
_L6:
        ((ViewHolder) (viewgroup)).statusContainer.setVisibility(0);
        ((ViewHolder) (viewgroup)).statusNameTV.setVisibility(0);
        ((ViewHolder) (viewgroup)).statusflagTV.setVisibility(0);
        ((ViewHolder) (viewgroup)).progress.setVisibility(8);
        ((ViewHolder) (viewgroup)).statusIV.setImageResource(0x7f020320);
        ((ViewHolder) (viewgroup)).statusNameTV.setText("\u6682\u505C\u4E0B\u8F7D");
        ((ViewHolder) (viewgroup)).statusflagTV.setText((new StringBuilder()).append(getDownloadedInMBString(downloadtask)).append("M/").append(getSizeInMBString(downloadtask)).append("M").toString());
        continue; /* Loop/switch isn't completed */
_L4:
        ((ViewHolder) (viewgroup)).statusContainer.setVisibility(0);
        ((ViewHolder) (viewgroup)).progress.setVisibility(4);
        ((ViewHolder) (viewgroup)).statusNameTV.setVisibility(0);
        ((ViewHolder) (viewgroup)).statusflagTV.setVisibility(0);
        ((ViewHolder) (viewgroup)).statusIV.setImageResource(0x7f020321);
        ((ViewHolder) (viewgroup)).statusNameTV.setText("\u7B49\u5F85\u4E0B\u8F7D");
        ((ViewHolder) (viewgroup)).statusflagTV.setText((new StringBuilder()).append(getDownloadedInMBString(downloadtask)).append("M/").append(getSizeInMBString(downloadtask)).append("M").toString());
        continue; /* Loop/switch isn't completed */
_L7:
        ((ViewHolder) (viewgroup)).statusContainer.setVisibility(0);
        ((ViewHolder) (viewgroup)).progress.setVisibility(4);
        ((ViewHolder) (viewgroup)).statusNameTV.setVisibility(0);
        ((ViewHolder) (viewgroup)).statusflagTV.setVisibility(0);
        ((ViewHolder) (viewgroup)).statusIV.setImageResource(0x7f02031f);
        ((ViewHolder) (viewgroup)).statusNameTV.setText("\u4E0B\u8F7D\u5931\u8D25");
        ((ViewHolder) (viewgroup)).statusflagTV.setText((new StringBuilder()).append(getDownloadedInMBString(downloadtask)).append("M/").append(getSizeInMBString(downloadtask)).append("M").toString());
        continue; /* Loop/switch isn't completed */
_L5:
        ((ViewHolder) (viewgroup)).statusContainer.setVisibility(0);
        ((ViewHolder) (viewgroup)).progress.setVisibility(0);
        ((ViewHolder) (viewgroup)).statusNameTV.setVisibility(0);
        ((ViewHolder) (viewgroup)).statusflagTV.setVisibility(0);
        ((ViewHolder) (viewgroup)).statusIV.setImageResource(0x7f0201f7);
        ((ViewHolder) (viewgroup)).progress.setProgress(j);
        ((ViewHolder) (viewgroup)).statusNameTV.setText("\u6B63\u5728\u4E0B\u8F7D");
        ((ViewHolder) (viewgroup)).statusflagTV.setText((new StringBuilder()).append(getDownloadedInMBString(downloadtask)).append("M/").append(getSizeInMBString(downloadtask)).append("M").toString());
        continue; /* Loop/switch isn't completed */
_L2:
        ((ViewHolder) (viewgroup)).statusContainer.setVisibility(8);
        ((ViewHolder) (viewgroup)).progress.setVisibility(4);
        ((ViewHolder) (viewgroup)).statusNameTV.setVisibility(4);
        ((ViewHolder) (viewgroup)).statusflagTV.setVisibility(4);
        if (StorageUtils.isDirAvaliable(downloadtask.downloadLocation))
        {
            if (StorageUtils.isFileAvaliable(new File(downloadtask.downloadLocation, ToolUtil.md5(downloadtask.downLoadUrl))))
            {
                ((ViewHolder) (viewgroup)).sdcardStatusLayout.setVisibility(8);
                ((ViewHolder) (viewgroup)).soundfileStatusTV.setVisibility(8);
            } else
            {
                ((ViewHolder) (viewgroup)).sdcardStatusLayout.setVisibility(0);
                ((ViewHolder) (viewgroup)).soundfileStatusTV.setVisibility(0);
                ((ViewHolder) (viewgroup)).soundfileStatusTV.setText("\u4E0B\u8F7D\u6587\u4EF6\u53EF\u80FD\u5DF2\u88AB\u5220\u9664");
            }
        } else
        {
            ((ViewHolder) (viewgroup)).sdcardStatusLayout.setVisibility(0);
            ((ViewHolder) (viewgroup)).soundfileStatusTV.setVisibility(0);
            if (MyFileUtils.isSDcardInvalid())
            {
                ((ViewHolder) (viewgroup)).soundfileStatusTV.setText("\u624B\u673A\u65E0\u6CD5\u8BFB\u53D6\u5B58\u50A8\u5361\u4E2D\u7684\u58F0\u97F3\u6587\u4EF6\uFF0C\u8BF7\u68C0\u67E5\u5B58\u50A8\u5361");
            } else
            {
                ((ViewHolder) (viewgroup)).soundfileStatusTV.setText("\u624B\u673A\u6B63\u5728\u8BFB\u53D6\u5B58\u50A8\u5361\u4E2D\u7684\u58F0\u97F3\u6587\u4EF6...");
            }
        }
        if (true) goto _L9; else goto _L8
_L8:
    }

    public void onClick(View view)
    {
        int i = ((ViewHolder)view.getTag()).position;
        int j = view.getId();
        if (j == 0x7f0a04a7)
        {
            delDownLoad(i);
        } else
        {
            if (j == 0x7f0a06de)
            {
                delDownLoad(i);
                return;
            }
            if (ToolUtil.isConnectToNetwork(mContext))
            {
                if (!UserInfoMannage.hasLogined())
                {
                    Intent intent = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                    intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                    mContext.startActivity(intent);
                    return;
                }
            } else
            {
                Toast.makeText(mContext, 0x7f0900a0, 1).show();
                return;
            }
        }
    }

    public void setDelDownloadListener(DelDownloadListener deldownloadlistener)
    {
        delDownload = deldownloadlistener;
    }

    public void setIsUnfinishDownload()
    {
        mIsUnfinishDownload = true;
    }
}
