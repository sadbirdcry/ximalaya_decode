// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.livefm;

import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.model.livefm.ProvinceModel;
import java.util.List;

public class ProvinceCategoryAdapter extends BaseAdapter
{

    private List mCategory;
    private int mCollapseSize;
    private Context mContext;

    public ProvinceCategoryAdapter(Context context, List list)
    {
        mCollapseSize = 5;
        mContext = context;
        mCategory = list;
    }

    public int getCollapseSize()
    {
        return mCollapseSize;
    }

    public int getCount()
    {
        return mCategory.size();
    }

    public Object getItem(int i)
    {
        return mCategory.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        view = View.inflate(mContext, 0x7f03015f, null);
        viewgroup = (TextView)view.findViewById(0x7f0a01a1);
        ProvinceModel provincemodel = (ProvinceModel)mCategory.get(i);
        viewgroup.setText(provincemodel.getProvinceName());
        if (provincemodel.isShow())
        {
            viewgroup.setBackgroundResource(0x7f020082);
            viewgroup.setTextColor(mContext.getResources().getColor(0x7f070006));
            return view;
        } else
        {
            viewgroup.setBackgroundResource(0x7f020081);
            viewgroup.setTextColor(mContext.getResources().getColor(0x7f0700c3));
            return view;
        }
    }
}
