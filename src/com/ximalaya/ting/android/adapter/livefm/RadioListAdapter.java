// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.livefm;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

public class RadioListAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        public View divider;
        public ImageView playImg;
        public TextView programNameTxt;
        public ImageView radioCoverImg;
        public TextView radioNameTxt;
        public TextView radioPlayCountTxt;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private Context mContext;
    private List radioLists;

    public RadioListAdapter(Context context, List list)
    {
        mContext = context;
        radioLists = list;
    }

    private void playSound(ImageView imageview, SoundInfo soundinfo, String s)
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        SoundInfo soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
        if (localmediaservice == null || soundinfo == null)
        {
            return;
        }
        if (soundinfo1 == null)
        {
            PlayTools.gotoPlay(32, soundinfo, mContext, false, s);
            imageview.setImageResource(0x7f020254);
            return;
        }
        switch (localmediaservice.getPlayServiceState())
        {
        default:
            return;

        case 0: // '\0'
            PlayTools.gotoPlay(32, soundinfo, mContext, false, s);
            imageview.setImageResource(0x7f020254);
            return;

        case 1: // '\001'
        case 3: // '\003'
            if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.radioId == soundinfo1.radioId)
            {
                localmediaservice.pause();
                imageview.setImageResource(0x7f020253);
                return;
            } else
            {
                PlayTools.gotoPlay(32, soundinfo, mContext, false, s);
                return;
            }

        case 2: // '\002'
            break;
        }
        if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.radioId == soundinfo1.radioId)
        {
            localmediaservice.start();
            imageview.setImageResource(0x7f020254);
            return;
        } else
        {
            PlayTools.gotoPlay(32, soundinfo, mContext, false, s);
            return;
        }
    }

    public int getCount()
    {
        return radioLists.size();
    }

    public Object getItem(int i)
    {
        return radioLists.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, final ViewGroup holder)
    {
        final RadioSound model;
        if (view == null)
        {
            holder = new ViewHolder(null);
            view = LayoutInflater.from(mContext).inflate(0x7f030156, null);
            holder.radioCoverImg = (ImageView)view.findViewById(0x7f0a0519);
            holder.radioNameTxt = (TextView)view.findViewById(0x7f0a051a);
            holder.programNameTxt = (TextView)view.findViewById(0x7f0a051b);
            holder.radioPlayCountTxt = (TextView)view.findViewById(0x7f0a0151);
            holder.playImg = (ImageView)view.findViewById(0x7f0a0022);
            holder.divider = view.findViewById(0x7f0a051c);
            view.setTag(holder);
        } else
        {
            holder = (ViewHolder)view.getTag();
        }
        model = (RadioSound)radioLists.get(i);
        ImageManager2.from(mContext).displayImage(((ViewHolder) (holder)).radioCoverImg, model.getRadioCoverSmall(), 0x7f02025e);
        ((ViewHolder) (holder)).radioNameTxt.setText(model.getRname());
        if (TextUtils.isEmpty(model.getProgramName()))
        {
            ((ViewHolder) (holder)).programNameTxt.setText("\u6682\u65E0\u8282\u76EE\u5355");
        } else
        {
            ((ViewHolder) (holder)).programNameTxt.setText((new StringBuilder()).append("\u6B63\u5728\u76F4\u64AD\uFF1A ").append(model.getProgramName()).toString());
        }
        ((ViewHolder) (holder)).radioPlayCountTxt.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(model.getRadioPlayCount())).append("\u4EBA").toString());
        if (ToolUtil.isLivePlaying(model.getRadioId()))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                ((ViewHolder) (holder)).playImg.setImageResource(0x7f020253);
            } else
            {
                ((ViewHolder) (holder)).playImg.setImageResource(0x7f020254);
            }
        } else
        {
            ((ViewHolder) (holder)).playImg.setImageResource(0x7f020253);
        }
        ((ViewHolder) (holder)).playImg.setOnClickListener(new _cls1());
        if (i == radioLists.size() - 1)
        {
            ((ViewHolder) (holder)).divider.setVisibility(8);
            return view;
        } else
        {
            ((ViewHolder) (holder)).divider.setVisibility(0);
            return view;
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final RadioListAdapter this$0;
        final ViewHolder val$holder;
        final RadioSound val$model;

        public void onClick(View view)
        {
            model.setCategory(1);
            SoundInfo soundinfo = ModelHelper.toSoundInfo(model);
            playSound(holder.playImg, soundinfo, DataCollectUtil.getDataFromView(view));
            notifyDataSetChanged();
        }

        _cls1()
        {
            this$0 = RadioListAdapter.this;
            model = radiosound;
            holder = viewholder;
            super();
        }
    }

}
