// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.livefm;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.model.livefm.AnnouncerData;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.Iterator;
import java.util.List;

public class LivePlaylistAdapter extends BaseAdapter
{
    static class ViewHolder
    {

        public TextView broadcasterTxt;
        public TextView playTypeTxt;
        public TextView programNameTxt;
        public TextView timeTxt;

        ViewHolder()
        {
        }
    }


    private Context mContext;
    private List sounds;

    public LivePlaylistAdapter(Context context, List list)
    {
        mContext = context;
        sounds = list;
    }

    public int getCount()
    {
        return sounds.size();
    }

    public Object getItem(int i)
    {
        return sounds.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        Object obj;
        Object obj1;
        if (view == null)
        {
            view = LayoutInflater.from(mContext).inflate(0x7f03015a, null);
            viewgroup = new ViewHolder();
            viewgroup.programNameTxt = (TextView)view.findViewById(0x7f0a051b);
            viewgroup.broadcasterTxt = (TextView)view.findViewById(0x7f0a051f);
            viewgroup.timeTxt = (TextView)view.findViewById(0x7f0a0520);
            viewgroup.playTypeTxt = (TextView)view.findViewById(0x7f0a0521);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        obj = (RadioSound)sounds.get(i);
        ((ViewHolder) (viewgroup)).timeTxt.setText((new StringBuilder()).append(((RadioSound) (obj)).getStartTime()).append("-").append(((RadioSound) (obj)).getEndTime()).toString());
        obj1 = PlayListControl.getPlayListManager().getCurSound();
        if (obj1 != null && ((SoundInfo) (obj1)).programScheduleId == ((RadioSound) (obj)).getProgramScheduleId())
        {
            ((ViewHolder) (viewgroup)).programNameTxt.setCompoundDrawablesWithIntrinsicBounds(0x7f02031a, 0, 0, 0);
            ((ViewHolder) (viewgroup)).programNameTxt.setCompoundDrawablePadding(ToolUtil.dp2px(mContext, 5F));
            view.setBackgroundColor(mContext.getResources().getColor(0x7f0700c1));
        } else
        {
            ((ViewHolder) (viewgroup)).programNameTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            ((ViewHolder) (viewgroup)).programNameTxt.setCompoundDrawablePadding(0);
            view.setBackgroundColor(mContext.getResources().getColor(0x7f070006));
        }
        if (((RadioSound) (obj)).getProgramId() == 0)
        {
            ((ViewHolder) (viewgroup)).programNameTxt.setText("\u65E0\u8282\u76EE");
        } else
        {
            ((ViewHolder) (viewgroup)).programNameTxt.setText(((RadioSound) (obj)).getProgramName());
        }
        if (((RadioSound) (obj)).getCategory() != 2) goto _L2; else goto _L1
_L1:
        ((ViewHolder) (viewgroup)).playTypeTxt.setText("\u56DE\u542C");
        ((ViewHolder) (viewgroup)).playTypeTxt.setTextColor(mContext.getResources().getColor(0x7f0700bb));
        ((ViewHolder) (viewgroup)).playTypeTxt.setBackgroundDrawable(null);
        ((ViewHolder) (viewgroup)).programNameTxt.setTextColor(mContext.getResources().getColor(0x7f0700c3));
        ((ViewHolder) (viewgroup)).broadcasterTxt.setTextColor(mContext.getResources().getColor(0x7f070068));
        ((ViewHolder) (viewgroup)).timeTxt.setTextColor(mContext.getResources().getColor(0x7f070068));
_L4:
        obj1 = new StringBuilder();
        for (obj = ((RadioSound) (obj)).getAnnouncerList().iterator(); ((Iterator) (obj)).hasNext(); ((StringBuilder) (obj1)).append(" "))
        {
            ((StringBuilder) (obj1)).append(((AnnouncerData)((Iterator) (obj)).next()).getAnnouncerName());
        }

        break; /* Loop/switch isn't completed */
_L2:
        if (((RadioSound) (obj)).getCategory() == 1)
        {
            ((ViewHolder) (viewgroup)).playTypeTxt.setText("\u76F4\u64AD");
            ((ViewHolder) (viewgroup)).playTypeTxt.setTextColor(mContext.getResources().getColor(0x7f070006));
            ((ViewHolder) (viewgroup)).playTypeTxt.setBackgroundResource(0x7f020083);
            ((ViewHolder) (viewgroup)).programNameTxt.setTextColor(mContext.getResources().getColor(0x7f0700c3));
            ((ViewHolder) (viewgroup)).broadcasterTxt.setTextColor(mContext.getResources().getColor(0x7f070068));
            ((ViewHolder) (viewgroup)).timeTxt.setTextColor(mContext.getResources().getColor(0x7f070068));
        } else
        if (((RadioSound) (obj)).getCategory() == 3)
        {
            ((ViewHolder) (viewgroup)).playTypeTxt.setText("");
            ((ViewHolder) (viewgroup)).playTypeTxt.setTextColor(mContext.getResources().getColor(0x7f0700c2));
            ((ViewHolder) (viewgroup)).playTypeTxt.setBackgroundDrawable(null);
            ((ViewHolder) (viewgroup)).programNameTxt.setTextColor(mContext.getResources().getColor(0x7f0700c2));
            ((ViewHolder) (viewgroup)).broadcasterTxt.setTextColor(mContext.getResources().getColor(0x7f0700c2));
            ((ViewHolder) (viewgroup)).timeTxt.setTextColor(mContext.getResources().getColor(0x7f0700c2));
        }
        if (true) goto _L4; else goto _L3
_L3:
        if (!TextUtils.isEmpty(((StringBuilder) (obj1)).toString()))
        {
            ((ViewHolder) (viewgroup)).broadcasterTxt.setVisibility(0);
            ((ViewHolder) (viewgroup)).broadcasterTxt.setText(((StringBuilder) (obj1)).toString());
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).broadcasterTxt.setVisibility(8);
            return view;
        }
    }
}
