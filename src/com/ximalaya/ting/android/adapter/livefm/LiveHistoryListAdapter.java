// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.livefm;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import java.text.SimpleDateFormat;
import java.util.List;

public class LiveHistoryListAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        public ImageView playImg;
        public TextView programNameTxt;
        public ImageView radioCoverImg;
        public TextView radioNameTxt;
        public TextView radioPlayCountTxt;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private SimpleDateFormat format;
    private Context mContext;
    private List mSounds;

    public LiveHistoryListAdapter(List list, Context context)
    {
        mContext = context;
        mSounds = list;
        format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    }

    private void playSound(ImageView imageview, SoundInfo soundinfo, String s)
    {
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        SoundInfo soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
        if (localmediaservice == null || soundinfo == null)
        {
            return;
        }
        if (soundinfo1 == null)
        {
            PlayTools.gotoPlay(32, soundinfo, mContext, false, s);
            imageview.setImageResource(0x7f020254);
            return;
        }
        switch (localmediaservice.getPlayServiceState())
        {
        default:
            return;

        case 0: // '\0'
            PlayTools.gotoPlay(32, soundinfo, mContext, false, s);
            imageview.setImageResource(0x7f020254);
            return;

        case 1: // '\001'
        case 3: // '\003'
            if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.radioId == soundinfo1.radioId)
            {
                localmediaservice.pause();
                imageview.setImageResource(0x7f020253);
                return;
            } else
            {
                PlayTools.gotoPlay(32, soundinfo, mContext, false, s);
                return;
            }

        case 2: // '\002'
            break;
        }
        if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.radioId == soundinfo1.radioId)
        {
            localmediaservice.start();
            imageview.setImageResource(0x7f020254);
            return;
        } else
        {
            PlayTools.gotoPlay(32, soundinfo, mContext, false, s);
            return;
        }
    }

    public int getCount()
    {
        return mSounds.size();
    }

    public Object getItem(int i)
    {
        return mSounds.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, final ViewGroup holder)
    {
        final SoundInfo sound;
        if (view == null)
        {
            holder = new ViewHolder(null);
            view = LayoutInflater.from(mContext).inflate(0x7f030156, null);
            holder.radioCoverImg = (ImageView)view.findViewById(0x7f0a0519);
            holder.radioNameTxt = (TextView)view.findViewById(0x7f0a051a);
            holder.programNameTxt = (TextView)view.findViewById(0x7f0a051b);
            holder.radioPlayCountTxt = (TextView)view.findViewById(0x7f0a0151);
            holder.playImg = (ImageView)view.findViewById(0x7f0a0022);
            view.setTag(holder);
        } else
        {
            holder = (ViewHolder)view.getTag();
        }
        sound = (SoundInfo)mSounds.get(i);
        ImageManager2.from(mContext).displayImage(((ViewHolder) (holder)).radioCoverImg, sound.coverSmall, 0x7f02025e);
        ((ViewHolder) (holder)).radioNameTxt.setText(sound.radioName);
        ((ViewHolder) (holder)).programNameTxt.setText((new StringBuilder()).append("\u4E0A\u6B21\u6536\u542C\u8282\u76EE\uFF1A").append(sound.programName).toString());
        if (sound.startPlayTime != null)
        {
            ((ViewHolder) (holder)).radioPlayCountTxt.setText((new StringBuilder()).append("\u4E0A\u6B21\u6536\u542C\u65F6\u95F4\uFF1A").append(format.format(sound.startPlayTime)).toString());
            ((ViewHolder) (holder)).radioPlayCountTxt.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
        if (ToolUtil.isLivePlaying(sound.radioId))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                ((ViewHolder) (holder)).playImg.setImageResource(0x7f020253);
            } else
            {
                ((ViewHolder) (holder)).playImg.setImageResource(0x7f020254);
            }
        } else
        {
            ((ViewHolder) (holder)).playImg.setImageResource(0x7f020253);
        }
        ((ViewHolder) (holder)).playImg.setOnClickListener(new _cls1());
        return view;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final LiveHistoryListAdapter this$0;
        final ViewHolder val$holder;
        final SoundInfo val$sound;

        public void onClick(View view)
        {
            sound.category = 1;
            playSound(holder.playImg, sound, DataCollectUtil.getDataFromView(view));
            notifyDataSetChanged();
        }

        _cls1()
        {
            this$0 = LiveHistoryListAdapter.this;
            sound = soundinfo;
            holder = viewholder;
            super();
        }
    }

}
