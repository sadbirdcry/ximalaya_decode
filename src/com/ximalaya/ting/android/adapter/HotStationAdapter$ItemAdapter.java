// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.model.holder.PersonStationHolder;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            HotStationAdapter

private static class setList extends BaseAdapter
{

    private Context mActivity;
    private BaseFragment mFragment;
    private GridView mGridView;
    private List mStations;
    private String mTitle;

    public int getCount()
    {
        if (mStations == null)
        {
            return 0;
        } else
        {
            return mStations.size();
        }
    }

    public Object getItem(int i)
    {
        return mStations.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(final int position, final View holder, ViewGroup viewgroup)
    {
        StationModel stationmodel = (StationModel)mStations.get(position);
        viewgroup = holder;
        if (holder == null)
        {
            viewgroup = PersonStationHolder.getView(mActivity);
            holder = (PersonStationHolder)viewgroup.getTag();
            viewgroup.setLayoutParams(new android.widget.>(-1, -2));
            mFragment.markImageView(((PersonStationHolder) (holder)).cover);
            class _cls1
                implements android.view.View.OnClickListener
            {

                final HotStationAdapter.ItemAdapter this$0;
                final int val$position;

                public void onClick(View view)
                {
                    StationModel stationmodel1 = (StationModel)view.getTag(0x7f090000);
                    if (stationmodel1 != null)
                    {
                        Bundle bundle = new Bundle();
                        bundle.putLong("toUid", stationmodel1.uid);
                        bundle.putInt("from", 1);
                        bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, mTitle, (new StringBuilder()).append(position + 1).append("").toString()));
                        if (mActivity instanceof MainTabActivity2)
                        {
                            ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
                            return;
                        }
                    }
                }

            _cls1()
            {
                this$0 = HotStationAdapter.ItemAdapter.this;
                position = i;
                super();
            }
            }

            class _cls2
                implements android.view.View.OnClickListener
            {

                final HotStationAdapter.ItemAdapter this$0;
                final PersonStationHolder val$holder;

                public void onClick(View view)
                {
                    Object obj = (StationModel)view.getTag(0x7f090000);
                    if (obj == null)
                    {
                        return;
                    }
                    if (UserInfoMannage.hasLogined())
                    {
                        HotStationAdapter.doFollow(mActivity, ((StationModel) (obj)), holder);
                        return;
                    } else
                    {
                        HotStationAdapter.access$200(holder, ((StationModel) (obj)).isFollowed);
                        obj = new Intent(mActivity, com/ximalaya/ting/android/activity/login/LoginActivity);
                        ((Intent) (obj)).putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                        mActivity.startActivity(((Intent) (obj)));
                        return;
                    }
                }

            _cls2()
            {
                this$0 = HotStationAdapter.ItemAdapter.this;
                holder = personstationholder;
                super();
            }
            }

            android.view.tationModel tationmodel;
            int i;
            if (a.e)
            {
                i = (int)((float)ToolUtil.getScreenWidth(mActivity) / 6F);
            } else
            {
                i = (int)((float)(ToolUtil.getScreenWidth(mActivity) - ToolUtil.dp2px(mActivity, 60F)) / 3F);
            }
            tationmodel = ((PersonStationHolder) (holder)).cover.getLayoutParams();
            tationmodel.cover = i;
            tationmodel.cover = tationmodel.cover;
            ((PersonStationHolder) (holder)).cover.setLayoutParams(tationmodel);
            ((PersonStationHolder) (holder)).cover.setOnClickListener(new _cls1());
            ((PersonStationHolder) (holder)).follow.setOnClickListener(new _cls2());
        }
        holder = (PersonStationHolder)viewgroup.getTag();
        ((PersonStationHolder) (holder)).cover.setTag(0x7f0a0037, Boolean.valueOf(true));
        ((PersonStationHolder) (holder)).cover.setTag(0x7f090000, stationmodel);
        ImageManager2.from(mActivity).displayImage(((PersonStationHolder) (holder)).cover, stationmodel.smallLogo, 0x7f0201b5);
        ((PersonStationHolder) (holder)).name.setText(stationmodel.nickname);
        HotStationAdapter.access$200(holder, stationmodel.isFollowed);
        ((PersonStationHolder) (holder)).follow.setTag(0x7f090000, stationmodel);
        return viewgroup;
    }

    public void setList(List list)
    {
        mStations.clear();
        if (list != null)
        {
            mStations.addAll(list);
        }
        notifyDataSetChanged();
    }

    public void setTitle(String s)
    {
        if (!TextUtils.isEmpty(s))
        {
            mTitle = null;
            mTitle = s;
        }
        notifyDataSetChanged();
    }

    void updateItem()
    {
        int i = 0;
_L6:
        if (i >= mStations.size()) goto _L2; else goto _L1
_L1:
        Object obj;
        int j = mGridView.getFirstVisiblePosition();
        obj = mGridView.getChildAt(i - j);
        if (obj != null) goto _L3; else goto _L2
_L2:
        return;
_L3:
        if ((obj = (PersonStationHolder)((View) (obj)).getTag()) == null) goto _L2; else goto _L4
_L4:
        HotStationAdapter.access$200(((PersonStationHolder) (obj)), ((StationModel)mStations.get(i)).isFollowed);
        i++;
        if (true) goto _L6; else goto _L5
_L5:
    }



    public _cls2(BaseFragment basefragment, GridView gridview, List list)
    {
        mStations = new ArrayList();
        mFragment = basefragment;
        mGridView = gridview;
        mActivity = mGridView.getContext();
        setList(list);
    }
}
