// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundModel;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class MyLikedSoundsAdapter extends BaseListSoundsAdapter
{

    private Context appContext;
    public List list;

    public MyLikedSoundsAdapter(Activity activity, List list1, Handler handler)
    {
        super(activity, list1);
        mContext = activity;
        appContext = mContext.getApplicationContext();
        list = list1;
        mHandler = handler;
    }

    private com.ximalaya.ting.android.a.a.a goDownLoad(SoundModel soundmodel, View view)
    {
        Object obj = null;
        DownloadTask downloadtask = new DownloadTask(soundmodel);
        soundmodel = obj;
        if (downloadtask != null)
        {
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            soundmodel = downloadtools.goDownload(downloadtask, appContext, view);
            downloadtools.release();
        }
        return soundmodel;
    }

    protected void bindData(final SoundModel info, final BaseListSoundsAdapter.ViewHolder holder)
    {
        b.a().a(ModelHelper.toSoundInfo(info));
        boolean flag1;
        if (isPlaying(info.trackId))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        holder.createTime.setText(ToolUtil.convertTime(info.createdAt));
        holder.owner.setText((new StringBuilder()).append("by ").append(info.nickname).toString());
        holder.title.setText(info.title);
        if (info.playtimes > 0)
        {
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.playtimes));
            holder.playCount.setVisibility(0);
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        if (info.likes > 0)
        {
            if (!flag1)
            {
                holder.playCount.setText(StringUtil.getFriendlyNumStr(info.likes));
            }
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.likes));
            holder.likeCount.setVisibility(0);
            boolean flag;
            boolean flag2;
            if (appContext != null)
            {
                if (info.isLike)
                {
                    holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(appContext.getResources().getDrawable(0x7f020090), null, null, null);
                    flag = true;
                } else
                {
                    holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(appContext.getResources().getDrawable(0x7f020092), null, null, null);
                    flag = true;
                }
            } else
            {
                flag = true;
            }
        } else
        {
            holder.likeCount.setVisibility(8);
            flag = false;
        }
        if (info.comments > 0)
        {
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.comments));
            holder.commentCount.setVisibility(0);
            flag2 = true;
        } else
        {
            holder.commentCount.setVisibility(8);
            flag2 = false;
        }
        if (flag1 || flag || flag2)
        {
            holder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)info.duration)).toString());
            holder.duration.setVisibility(0);
        } else
        {
            holder.duration.setVisibility(8);
        }
        holder.cover.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(appContext).displayImage(holder.cover, info.coverSmall, 0x7f0202de);
        if (info.userSource == 1)
        {
            holder.origin.setText("\u539F\u521B");
        } else
        {
            holder.origin.setText("\u91C7\u96C6");
        }
        if (isDownload(info.trackId))
        {
            holder.btn.setImageResource(0x7f0200f1);
            holder.btn.setEnabled(false);
        } else
        {
            holder.btn.setImageResource(0x7f0201fe);
            holder.btn.setEnabled(true);
        }
        holder.btn.setOnClickListener(new _cls1());
        holder.cover.setOnClickListener(new _cls2());
        if (holder.position + 1 == list.size())
        {
            holder.border.setVisibility(8);
            return;
        } else
        {
            holder.border.setVisibility(0);
            return;
        }
    }

    protected volatile void bindData(Object obj, BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((SoundModel)obj, viewholder);
    }

    public void cleanData()
    {
        mContext = null;
        appContext = null;
        mHandler = null;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final MyLikedSoundsAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SoundModel val$info;

        public void onClick(View view)
        {
            if (goDownLoad(info, view) == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls1()
        {
            this$0 = MyLikedSoundsAdapter.this;
            info = soundmodel;
            holder = viewholder;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final MyLikedSoundsAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SoundModel val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, getData().indexOf(info), ModelHelper.toSoundInfo(info), ModelHelper.soundModelToSoundInfoList(getData()));
        }

        _cls2()
        {
            this$0 = MyLikedSoundsAdapter.this;
            holder = viewholder;
            info = soundmodel;
            super();
        }
    }

}
