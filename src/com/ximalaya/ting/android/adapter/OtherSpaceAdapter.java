// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.RecordingModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class OtherSpaceAdapter extends BaseListSoundsAdapter
{

    private Context mAppContext;
    public LoginInfoModel mInfoModel;
    private BaseFragment osf;
    private int updateInex;

    public OtherSpaceAdapter(BaseFragment basefragment, List list)
    {
        super(basefragment.getActivity(), list);
        osf = basefragment;
        mAppContext = mContext.getApplicationContext();
        if (mInfoModel == null)
        {
            mInfoModel = UserInfoMannage.getInstance().getUser();
        }
    }

    protected void bindData(final RecordingModel info, final BaseListSoundsAdapter.ViewHolder holder)
    {
        b.a().a(ModelHelper.toSoundInfo(info));
        String s;
        TextView textview;
        boolean flag;
        boolean flag1;
        if (isPlaying(info.trackId))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        holder.createTime.setText(ToolUtil.convertTime(info.createdAt));
        textview = holder.origin;
        if (info.userSource == 1)
        {
            s = "\u539F\u521B";
        } else
        {
            s = "\u91C7\u96C6";
        }
        textview.setText(s);
        holder.owner.setText((new StringBuilder()).append("by ").append(info.nickname).toString());
        holder.duration.setText(TimeHelper.secondToMinute((new StringBuilder()).append(info.duration).append("").toString()));
        holder.title.setText(info.title);
        if (info.playtimes > 0)
        {
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.playtimes));
            holder.playCount.setVisibility(0);
            flag1 = true;
        } else
        {
            holder.playCount.setVisibility(8);
            flag1 = false;
        }
        if (info.likes > 0)
        {
            if (flag1)
            {
                holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.likes));
                holder.likeCount.setVisibility(0);
                if (info.isLike)
                {
                    holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mAppContext.getResources().getDrawable(0x7f020090), null, null, null);
                    flag = true;
                } else
                {
                    holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mAppContext.getResources().getDrawable(0x7f020092), null, null, null);
                    flag = true;
                }
            } else
            {
                holder.playCount.setText(StringUtil.getFriendlyNumStr(info.likes));
                if (info.isLike)
                {
                    holder.playCount.setCompoundDrawablesWithIntrinsicBounds(mAppContext.getResources().getDrawable(0x7f020090), null, null, null);
                } else
                {
                    holder.playCount.setCompoundDrawablesWithIntrinsicBounds(mAppContext.getResources().getDrawable(0x7f020092), null, null, null);
                }
                holder.likeCount.setVisibility(8);
                flag = true;
            }
        } else
        {
            holder.likeCount.setVisibility(8);
            flag = false;
        }
        if (info.comments > 0)
        {
            if (flag1 || flag)
            {
                holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.comments));
                holder.commentCount.setVisibility(0);
            } else
            {
                holder.commentCount.setVisibility(8);
            }
        } else
        {
            holder.commentCount.setVisibility(8);
        }
        ImageManager2.from(mAppContext).displayImage(holder.cover, info.coverSmall, 0x7f0202de);
        if (isDownload(info.trackId))
        {
            holder.btn.setImageResource(0x7f0200f1);
            holder.btn.setEnabled(false);
        } else
        {
            holder.btn.setImageResource(0x7f0201fe);
            holder.btn.setEnabled(true);
        }
        holder.btn.setOnClickListener(new _cls1());
        holder.cover.setOnClickListener(new _cls2());
    }

    protected volatile void bindData(Object obj, BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((RecordingModel)obj, viewholder);
    }

    public int getUpdateInex()
    {
        return updateInex;
    }

    public com.ximalaya.ting.android.a.a.a goDownLoad(RecordingModel recordingmodel, View view)
    {
        Object obj = null;
        DownloadTask downloadtask = new DownloadTask(recordingmodel);
        recordingmodel = obj;
        if (downloadtask != null)
        {
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            recordingmodel = downloadtools.goDownload(downloadtask, mAppContext, view);
            downloadtools.release();
        }
        return recordingmodel;
    }

    public void notifyDataSetChanged()
    {
        super.notifyDataSetChanged();
        if (osf != null && (osf instanceof OtherSpaceFragment))
        {
            ((OtherSpaceFragment)osf).setSoundCount();
        }
    }

    public void releseData()
    {
        mContext = null;
        osf = null;
        mAppContext = null;
    }

    public void setUpdateInex(int i)
    {
        updateInex = i;
    }

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final OtherSpaceAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final RecordingModel val$info;

        public void onClick(View view)
        {
            if (goDownLoad(info, view) == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls1()
        {
            this$0 = OtherSpaceAdapter.this;
            info = recordingmodel;
            holder = viewholder;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final OtherSpaceAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final RecordingModel val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, mData.indexOf(info), ModelHelper.toSoundInfo(info), ModelHelper.recordingModelToSoundInfoList(mData));
        }

        _cls2()
        {
            this$0 = OtherSpaceAdapter.this;
            holder = viewholder;
            info = recordingmodel;
            super();
        }
    }

}
