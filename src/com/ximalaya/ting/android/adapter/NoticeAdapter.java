// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.model.message.BaseCommentModel;
import com.ximalaya.ting.android.model.message.CommentListInCommentNotice;
import com.ximalaya.ting.android.model.message.SoundInCommentModel;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

public class NoticeAdapter extends BaseAdapter
{
    static class ViewHolder
    {

        ImageView img_comment_head;
        ImageView img_comment_mv_img;
        View ll_comment_real_content;
        View ll_progress;
        View ll_tempId3;
        TextView txt_comment;
        TextView txt_comment_time;
        TextView txt_content;

        ViewHolder()
        {
        }
    }


    public static final String ReceiveType = "\u6536\u5230\u7684";
    public static final String SendType = "\u53D1\u51FA\u7684";
    public static String curType = "\u6536\u5230\u7684";
    private Context act;
    private CommentListInCommentNotice cicn;
    private Context realAct;

    public NoticeAdapter(Context context, CommentListInCommentNotice commentlistincommentnotice)
    {
        act = context.getApplicationContext();
        realAct = context;
        cicn = commentlistincommentnotice;
        if (commentlistincommentnotice != null)
        {
            curType = commentlistincommentnotice.sendType;
        }
    }

    private void goPlaySound(BaseCommentModel basecommentmodel, View view)
    {
        if (basecommentmodel.getSicm() == null)
        {
            Toast.makeText(act, "\u58F0\u97F3\u6570\u636E\u4E0D\u5B58\u5728~", 0).show();
            return;
        } else
        {
            PlayTools.gotoPlayWithoutUrl(8, ModelHelper.toSoundInfo(basecommentmodel.getSicm()), realAct, true, DataCollectUtil.getDataFromView(view));
            return;
        }
    }

    public CommentListInCommentNotice getCicn()
    {
        return cicn;
    }

    public int getCount()
    {
        if (cicn == null || cicn.list == null)
        {
            return 0;
        } else
        {
            return cicn.list.size();
        }
    }

    public BaseCommentModel getItem(int i)
    {
        if (cicn == null || cicn.list == null || cicn.list.isEmpty())
        {
            return null;
        } else
        {
            return (BaseCommentModel)cicn.list.get(i);
        }
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        final BaseCommentModel bcm;
        if (view == null)
        {
            view = LayoutInflater.from(act).inflate(0x7f03005f, viewgroup, false);
            viewgroup = new ViewHolder();
            viewgroup.img_comment_head = (ImageView)view.findViewById(0x7f0a01d7);
            viewgroup.ll_tempId3 = view.findViewById(0x7f0a01da);
            viewgroup.img_comment_mv_img = (ImageView)view.findViewById(0x7f0a01d8);
            viewgroup.ll_comment_real_content = view.findViewById(0x7f0a01d6);
            viewgroup.ll_progress = view.findViewById(0x7f0a0645);
            viewgroup.txt_comment = (TextView)view.findViewById(0x7f0a01db);
            viewgroup.txt_comment_time = (TextView)view.findViewById(0x7f0a01dc);
            viewgroup.txt_content = (TextView)view.findViewById(0x7f0a01d9);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        bcm = (BaseCommentModel)cicn.list.get(i);
        if (!bcm.isFlag())
        {
            break MISSING_BLOCK_LABEL_1508;
        }
        ((ViewHolder) (viewgroup)).ll_tempId3.setVisibility(0);
        view.setBackgroundResource(0x7f02006e);
        ((ViewHolder) (viewgroup)).ll_progress.setVisibility(8);
        ((ViewHolder) (viewgroup)).ll_comment_real_content.setVisibility(0);
        ImageManager2.from(act).displayImage(((ViewHolder) (viewgroup)).img_comment_head, bcm.avatarPath, 0x7f0202df);
        ((ViewHolder) (viewgroup)).img_comment_mv_img.setOnClickListener(new _cls1());
        if (((ViewHolder) (viewgroup)).ll_tempId3.getBackground() == null)
        {
            ((ViewHolder) (viewgroup)).ll_tempId3.setBackgroundResource(0x7f020286);
        }
        bcm.messageType.intValue();
        JVM INSTR lookupswitch 5: default 300
    //                   2: 374
    //                   3: 671
    //                   4: 903
    //                   8: 1090
    //                   14: 1245;
           goto _L1 _L2 _L3 _L4 _L5 _L6
_L1:
        break; /* Loop/switch isn't completed */
_L6:
        break MISSING_BLOCK_LABEL_1245;
_L7:
        String s;
        Object obj;
        ImageView imageview;
        if (Utilities.isBlank(bcm.getContent()))
        {
            ((ViewHolder) (viewgroup)).ll_tempId3.setBackgroundDrawable(null);
            ((ViewHolder) (viewgroup)).txt_comment.setVisibility(8);
        } else
        {
            ((ViewHolder) (viewgroup)).txt_comment.setVisibility(0);
            ((ViewHolder) (viewgroup)).txt_comment.setText(bcm.getContent());
        }
        ((ViewHolder) (viewgroup)).txt_comment_time.setText(TimeHelper.CountTime((new StringBuilder()).append(bcm.getCreatedAt()).append("").toString()));
        return view;
_L2:
        ((ViewHolder) (viewgroup)).img_comment_mv_img.setVisibility(0);
        if (curType.equals("\u53D1\u51FA\u7684"))
        {
            s = "\u6211";
        } else
        {
            s = (new StringBuilder()).append("<font color='#f86442'>").append(bcm.nickname).append("</font>").toString();
        }
        s = (new StringBuilder()).append(s).append("\u8BC4\u8BBA\u4E86").toString();
        obj = (new StringBuilder()).append(s);
        if (curType.equals("\u6536\u5230\u7684"))
        {
            s = "\u6211";
        } else
        {
            s = (new StringBuilder()).append("<font color='#f86442'>").append(bcm.getToNickname()).append("</font>").toString();
        }
        s = ((StringBuilder) (obj)).append(s).toString();
        obj = (new StringBuilder()).append(s).append("\u7684\u58F0\u97F3\uFF1A");
        if (bcm.getSicm() == null)
        {
            s = "";
        } else
        {
            s = bcm.getSicm().trackTitle;
        }
        s = ((StringBuilder) (obj)).append(s).toString();
        ((ViewHolder) (viewgroup)).txt_content.setText(Html.fromHtml(s));
        ((ViewHolder) (viewgroup)).img_comment_mv_img.setTag(0x7f0a0036, Boolean.valueOf(true));
        obj = ImageManager2.from(act);
        imageview = ((ViewHolder) (viewgroup)).img_comment_mv_img;
        if (bcm.getSicm() == null)
        {
            s = "";
        } else
        {
            s = bcm.getSicm().getTrackCoverPath();
        }
        ((ImageManager2) (obj)).displayImage(imageview, s, 0x7f0202de);
          goto _L7
_L3:
        ((ViewHolder) (viewgroup)).img_comment_mv_img.setVisibility(8);
        if (curType.equals("\u53D1\u51FA\u7684"))
        {
            s = "\u6211";
        } else
        {
            s = (new StringBuilder()).append("<font color='#f86442'>").append(bcm.getNickname()).append("</font>").toString();
        }
        s = (new StringBuilder()).append(s).append("\u56DE\u590D\u4E86").toString();
        obj = (new StringBuilder()).append(s);
        if (curType.equals("\u53D1\u51FA\u7684"))
        {
            s = (new StringBuilder()).append("<font color='#f86442'>").append(bcm.getToNickname()).append("</font>").toString();
        } else
        {
            s = "\u6211";
        }
        s = ((StringBuilder) (obj)).append(s).toString();
        obj = (new StringBuilder()).append(s).append("\u7684\u8BC4\u8BBA\uFF1A");
        if (bcm.getSicm() == null)
        {
            s = "";
        } else
        {
            s = bcm.getSicm().getPcommentContent();
        }
        s = ((StringBuilder) (obj)).append(s).toString();
        ((ViewHolder) (viewgroup)).txt_content.setText(Html.fromHtml(s));
          goto _L7
_L4:
        ((ViewHolder) (viewgroup)).img_comment_mv_img.setVisibility(0);
        obj = (new StringBuilder()).append("<font color='#f86442'>").append(bcm.getNickname()).append("</font>").append("\u5728").append("<font color='#f86442'>").append(bcm.getToNickname()).append("</font>").append("\u7684\u58F0\u97F3\u91CC@\u4E86\u6211\uFF1A");
        if (bcm.getSicm() == null)
        {
            s = "";
        } else
        {
            s = bcm.getSicm().getTrackTitle();
        }
        s = ((StringBuilder) (obj)).append(s).toString();
        ((ViewHolder) (viewgroup)).txt_content.setText(Html.fromHtml(s));
        ((ViewHolder) (viewgroup)).img_comment_mv_img.setTag(0x7f0a0036, Boolean.valueOf(true));
        obj = ImageManager2.from(act);
        imageview = ((ViewHolder) (viewgroup)).img_comment_mv_img;
        if (bcm.getSicm() == null)
        {
            s = "";
        } else
        {
            s = bcm.getSicm().getTrackCoverPath();
        }
        ((ImageManager2) (obj)).displayImage(imageview, s, 0x7f0202de);
          goto _L7
_L5:
        ((ViewHolder) (viewgroup)).img_comment_mv_img.setVisibility(0);
        obj = (new StringBuilder()).append("<font color='#f86442'>").append(bcm.getNickname()).append("</font>\u8F6C\u91C7\u4E86\u6211\u7684\u58F0\u97F3:");
        if (bcm.getSicm() == null)
        {
            s = "";
        } else
        {
            s = bcm.getSicm().getTrackTitle();
        }
        s = ((StringBuilder) (obj)).append(s).toString();
        ((ViewHolder) (viewgroup)).txt_content.setText(Html.fromHtml(s));
        ((ViewHolder) (viewgroup)).img_comment_mv_img.setTag(0x7f0a0036, Boolean.valueOf(true));
        obj = ImageManager2.from(act);
        imageview = ((ViewHolder) (viewgroup)).img_comment_mv_img;
        if (bcm.getSicm() == null)
        {
            s = "";
        } else
        {
            s = bcm.getSicm().getTrackCoverPath();
        }
        ((ImageManager2) (obj)).displayImage(imageview, s, 0x7f0202de);
          goto _L7
        ((ViewHolder) (viewgroup)).img_comment_mv_img.setVisibility(8);
        if (curType.equals("\u53D1\u51FA\u7684"))
        {
            s = "\u6211";
        } else
        {
            s = (new StringBuilder()).append("<font color='#F86442'>").append(bcm.getNickname()).append("</font>").toString();
        }
        obj = (new StringBuilder()).append(s).append("\u5728\u53D1\u5E03\u58F0\u97F3");
        if (bcm.getSicm() == null)
        {
            s = "";
        } else
        {
            s = (new StringBuilder()).append("<font color='#F86442'>").append(bcm.getSicm().trackTitle).append("</font>").toString();
        }
        s = ((StringBuilder) (obj)).append(s).append("\u7684\u65F6\u5019@\u4E86").toString();
        obj = (new StringBuilder()).append(s);
        if (curType.equals("\u53D1\u51FA\u7684"))
        {
            s = (new StringBuilder()).append("<font color='#F86442'>").append(bcm.getToNickname()).append("</font>").toString();
        } else
        {
            s = "\u6211";
        }
        s = ((StringBuilder) (obj)).append(s).toString();
        ((ViewHolder) (viewgroup)).txt_content.setText(Html.fromHtml(s));
          goto _L7
        ((ViewHolder) (viewgroup)).ll_comment_real_content.setVisibility(8);
        ((ViewHolder) (viewgroup)).ll_progress.setVisibility(0);
        view.setBackgroundDrawable(null);
        return view;
    }

    public void setCicn(CommentListInCommentNotice commentlistincommentnotice)
    {
        cicn = commentlistincommentnotice;
        if (commentlistincommentnotice != null)
        {
            curType = commentlistincommentnotice.sendType;
        }
    }

    public void setDataNull()
    {
        act = null;
        realAct = null;
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final NoticeAdapter this$0;
        final BaseCommentModel val$bcm;

        public void onClick(View view)
        {
            goPlaySound(bcm, view);
        }

        _cls1()
        {
            this$0 = NoticeAdapter.this;
            bcm = basecommentmodel;
            super();
        }
    }

}
