// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.userspace.NewsCenterFragment;
import com.ximalaya.ting.android.model.message.PrivateMsgModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.TimeHelper;
import java.util.List;

public class PrivateMsgAdapter extends BaseAdapter
{
    public static class ViewHolder
    {

        ImageView img_privateMsg_userHead;
        View ll_privateMsg_trueContent;
        View ll_progress_false;
        View ll_tempId1;
        public TextView txt_privateMsg_content;
        public TextView txt_privateMsg_noReadCount;
        public TextView txt_privateMsg_time;
        public TextView txt_privateMsg_userName;

        public ViewHolder()
        {
        }
    }


    private Context act;
    private NewsCenterFragment ncf;
    public List pmmList;

    public PrivateMsgAdapter(NewsCenterFragment newscenterfragment)
    {
        ncf = newscenterfragment;
        act = newscenterfragment.getActivity().getApplicationContext();
    }

    public int getCount()
    {
        if (pmmList == null)
        {
            return 0;
        } else
        {
            return pmmList.size();
        }
    }

    public PrivateMsgModel getItem(int i)
    {
        if (pmmList == null || i - 2 >= pmmList.size() || i - 2 < 0)
        {
            return null;
        } else
        {
            return (PrivateMsgModel)pmmList.get(i - 2);
        }
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        PrivateMsgModel privatemsgmodel;
        if (view == null)
        {
            viewgroup = new ViewHolder();
            view = LayoutInflater.from(act).inflate(0x7f030169, null);
            viewgroup.img_privateMsg_userHead = (ImageView)view.findViewById(0x7f0a0569);
            viewgroup.ll_privateMsg_trueContent = view.findViewById(0x7f0a0568);
            viewgroup.ll_progress_false = view.findViewById(0x7f0a0645);
            viewgroup.ll_tempId1 = view.findViewById(0x7f0a0267);
            viewgroup.txt_privateMsg_content = (TextView)view.findViewById(0x7f0a056d);
            viewgroup.txt_privateMsg_noReadCount = (TextView)view.findViewById(0x7f0a056c);
            viewgroup.txt_privateMsg_time = (TextView)view.findViewById(0x7f0a056b);
            viewgroup.txt_privateMsg_userName = (TextView)view.findViewById(0x7f0a056a);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        privatemsgmodel = (PrivateMsgModel)pmmList.get(i);
        if (privatemsgmodel != null)
        {
            if (privatemsgmodel.flag)
            {
                ((ViewHolder) (viewgroup)).ll_progress_false.setVisibility(8);
                ((ViewHolder) (viewgroup)).ll_privateMsg_trueContent.setVisibility(0);
                ((ViewHolder) (viewgroup)).ll_tempId1.setVisibility(0);
                Object obj;
                if (act == null)
                {
                    obj = ncf.getActivity().getApplicationContext();
                } else
                {
                    obj = act;
                }
                ImageManager2.from(((Context) (obj))).displayImage(((ViewHolder) (viewgroup)).img_privateMsg_userHead, privatemsgmodel.getLinkmanAvatarPath(), 0x7f0202de);
                obj = ((ViewHolder) (viewgroup)).txt_privateMsg_noReadCount;
                if (privatemsgmodel.noReadCount > 0)
                {
                    i = 0;
                } else
                {
                    i = 8;
                }
                ((TextView) (obj)).setVisibility(i);
                if (((ViewHolder) (viewgroup)).txt_privateMsg_noReadCount.getVisibility() == 0 && privatemsgmodel.noReadCount > 0)
                {
                    ((ViewHolder) (viewgroup)).txt_privateMsg_noReadCount.setText((new StringBuilder()).append(privatemsgmodel.noReadCount).append("").toString());
                }
                ((ViewHolder) (viewgroup)).txt_privateMsg_userName.setText((new StringBuilder()).append(privatemsgmodel.getLinkmanNickname()).append(":").toString());
                ((ViewHolder) (viewgroup)).txt_privateMsg_time.setText(TimeHelper.countTime2((new StringBuilder()).append(privatemsgmodel.getUpdatedAt()).append("").toString()));
                ((ViewHolder) (viewgroup)).txt_privateMsg_content.setText(privatemsgmodel.getContent());
                if (privatemsgmodel.getNoReadCount() > 0)
                {
                    ((ViewHolder) (viewgroup)).txt_privateMsg_noReadCount.setText((new StringBuilder()).append(privatemsgmodel.getNoReadCount()).append("").toString());
                    ((ViewHolder) (viewgroup)).txt_privateMsg_noReadCount.setVisibility(0);
                    return view;
                } else
                {
                    ((ViewHolder) (viewgroup)).txt_privateMsg_noReadCount.setVisibility(4);
                    return view;
                }
            } else
            {
                ((ViewHolder) (viewgroup)).ll_progress_false.setVisibility(0);
                ((ViewHolder) (viewgroup)).ll_privateMsg_trueContent.setVisibility(8);
                view.setBackgroundDrawable(null);
                return view;
            }
        } else
        {
            return null;
        }
    }

    public void releseData()
    {
        act = null;
        ncf = null;
    }
}
