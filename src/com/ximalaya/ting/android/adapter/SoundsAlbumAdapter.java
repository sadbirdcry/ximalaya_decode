// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.graphics.drawable.AnimationDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.sound.AlbumSoundModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class SoundsAlbumAdapter extends BaseListSoundsAdapter
{

    private int mPlaySource;

    public SoundsAlbumAdapter(Activity activity, List list)
    {
        super(activity, list);
        mPlaySource = 3;
    }

    public SoundsAlbumAdapter(Activity activity, List list, int i)
    {
        super(activity, list);
        mPlaySource = 3;
        mPlaySource = i;
    }

    protected void bindData(final AlbumSoundModel info, final BaseListSoundsAdapter.ViewHolder holder)
    {
        b.a().a(ModelHelper.toSoundInfo(info));
        holder.title.setText(info.title);
        holder.createTime.setText(ToolUtil.convertTime(info.createdAt));
        if (isPlaying(info.trackId))
        {
            holder.playFlag.setVisibility(0);
            if (LocalMediaService.getInstance().isPaused() || LocalMediaService.getInstance().isPlayCompleted())
            {
                holder.playFlag.setImageResource(0x7f0203e9);
            } else
            {
                holder.playFlag.setImageResource(0x7f0203e8);
                if (holder.playFlag.getDrawable() instanceof AnimationDrawable)
                {
                    final AnimationDrawable animationDrawable = (AnimationDrawable)holder.playFlag.getDrawable();
                    holder.playFlag.post(new _cls1());
                }
            }
        } else
        {
            holder.playFlag.setVisibility(8);
        }
        if (info.playtimes > 0)
        {
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.playtimes));
            holder.playCount.setVisibility(0);
        } else
        {
            holder.playCount.setText("0");
            holder.playCount.setVisibility(8);
        }
        if (info.comments > 0)
        {
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.comments));
            holder.commentCount.setVisibility(0);
        } else
        {
            holder.commentCount.setVisibility(8);
        }
        holder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)info.duration)).toString());
        holder.duration.setVisibility(0);
        if (isDownload(info.trackId))
        {
            holder.download.setImageResource(0x7f0200f1);
            holder.download.setEnabled(false);
        } else
        {
            holder.download.setImageResource(0x7f0201fe);
            holder.download.setEnabled(true);
        }
        holder.download.setOnClickListener(new _cls2());
        holder.playFlag.setOnClickListener(new _cls3());
    }

    protected volatile void bindData(Object obj, BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((AlbumSoundModel)obj, viewholder);
    }

    protected View buildItemView(int i, View view, ViewGroup viewgroup)
    {
        View view1;
        if (view == null)
        {
            view1 = LayoutInflater.from(mContext).inflate(0x7f03013f, viewgroup, false);
            view = new BaseListSoundsAdapter.ViewHolder();
            view.playFlag = (ImageView)view1.findViewById(0x7f0a0022);
            view.title = (TextView)view1.findViewById(0x7f0a00e6);
            view.createTime = (TextView)view1.findViewById(0x7f0a04ed);
            view.duration = (TextView)view1.findViewById(0x7f0a04ef);
            view.commentCount = (TextView)view1.findViewById(0x7f0a04f0);
            view.playCount = (TextView)view1.findViewById(0x7f0a0151);
            view.download = (ImageView)view1.findViewById(0x7f0a02d1);
            view1.setTag(view);
            viewgroup = view;
        } else
        {
            viewgroup = (BaseListSoundsAdapter.ViewHolder)view.getTag();
            view1 = view;
        }
        viewgroup.position = i;
        return view1;
    }

    public void playSound(View view, int i, SoundInfo soundinfo, List list)
    {
        ImageView imageview;
        LocalMediaService localmediaservice;
        SoundInfo soundinfo1;
        imageview = (ImageView)view;
        localmediaservice = LocalMediaService.getInstance();
        soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
        if (localmediaservice != null && soundinfo != null) goto _L2; else goto _L1
_L1:
        return;
_L2:
        if (soundinfo1 != null)
        {
            break; /* Loop/switch isn't completed */
        }
        PlayTools.gotoPlay(mPlaySource, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
        if (imageview != null)
        {
            imageview.setImageResource(0x7f0203e9);
            return;
        }
        if (true) goto _L1; else goto _L3
_L3:
        switch (localmediaservice.getPlayServiceState())
        {
        default:
            return;

        case 2: // '\002'
            break; /* Loop/switch isn't completed */

        case 0: // '\0'
            PlayTools.gotoPlay(mPlaySource, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
            if (imageview != null)
            {
                imageview.setImageResource(0x7f0203e9);
                return;
            }
            break;

        case 1: // '\001'
        case 3: // '\003'
            if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.trackId == soundinfo1.trackId)
            {
                localmediaservice.pause();
                if (imageview != null)
                {
                    imageview.setImageResource(0x7f0203e9);
                    return;
                }
            } else
            {
                PlayTools.gotoPlay(mPlaySource, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
                return;
            }
            break;
        }
        if (true) goto _L1; else goto _L4
_L4:
        if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.trackId == soundinfo1.trackId)
        {
            localmediaservice.start();
            if (imageview != null)
            {
                imageview.setImageResource(0x7f0203e9);
                return;
            }
        } else
        {
            PlayTools.gotoPlay(mPlaySource, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
            return;
        }
        if (true) goto _L1; else goto _L5
_L5:
    }

    private class _cls2
        implements android.view.View.OnClickListener
    {

        final SoundsAlbumAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final AlbumSoundModel val$info;

        public void onClick(View view)
        {
            DownloadTask downloadtask = new DownloadTask(ModelHelper.toSoundInfo(info));
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            view = downloadtools.goDownload(downloadtask, mContext.getApplicationContext(), view);
            downloadtools.release();
            if (view == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.download.setImageResource(0x7f0200f1);
                holder.download.setEnabled(false);
            }
        }

        _cls2()
        {
            this$0 = SoundsAlbumAdapter.this;
            info = albumsoundmodel;
            holder = viewholder;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final SoundsAlbumAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final AlbumSoundModel val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, mData.indexOf(info), ModelHelper.toSoundInfo(info), ModelHelper.albumSoundlistToSoundInfoList(mData));
        }

        _cls3()
        {
            this$0 = SoundsAlbumAdapter.this;
            holder = viewholder;
            info = albumsoundmodel;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final SoundsAlbumAdapter this$0;
        final AnimationDrawable val$animationDrawable;

        public void run()
        {
            if (animationDrawable != null && !animationDrawable.isRunning())
            {
                animationDrawable.start();
            }
        }

        _cls1()
        {
            this$0 = SoundsAlbumAdapter.this;
            animationDrawable = animationdrawable;
            super();
        }
    }

}
