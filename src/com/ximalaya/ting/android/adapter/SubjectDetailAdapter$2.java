// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.modelnew.AlbumModelNew;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            SubjectDetailAdapter

class val.view extends a
{

    final SubjectDetailAdapter this$0;
    final AlbumItemHolder val$holder;
    final AlbumModelNew val$model;
    final View val$view;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$view);
    }

    public void onFinish()
    {
        super.onFinish();
        val$holder.collect.setEnabled(true);
    }

    public void onNetError(int i, String s)
    {
        Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
        SubjectDetailAdapter.access$100(SubjectDetailAdapter.this, val$holder, val$model.isFavorite);
    }

    public void onStart()
    {
        super.onStart();
        val$holder.collect.setEnabled(false);
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            SubjectDetailAdapter.access$100(SubjectDetailAdapter.this, val$holder, val$model.isFavorite);
            return;
        }
        Object obj = null;
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            s = obj;
        }
        if (s != null && s.getIntValue("ret") == 0)
        {
            if (val$model.isFavorite)
            {
                s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
                val$model.isFavorite = false;
            } else
            {
                s = "\u6536\u85CF\u6210\u529F\uFF01";
                val$model.isFavorite = true;
            }
            SubjectDetailAdapter.access$100(SubjectDetailAdapter.this, val$holder, val$model.isFavorite);
            Toast.makeText(mContext, s, 0).show();
            return;
        }
        s = s.getString("msg");
        if (TextUtils.isEmpty(s))
        {
            s = "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
        }
        Toast.makeText(mContext, s, 0).show();
        SubjectDetailAdapter.access$100(SubjectDetailAdapter.this, val$holder, val$model.isFavorite);
    }

    ()
    {
        this$0 = final_subjectdetailadapter;
        val$holder = albumitemholder;
        val$model = albummodelnew;
        val$view = View.this;
        super();
    }
}
