// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ToggleButton;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.model.feed.ChildFeedModel;
import com.ximalaya.ting.android.util.CommonRequest;
import com.ximalaya.ting.android.util.MyAsyncTask;
import com.ximalaya.ting.android.util.ToolUtil;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            NewThingsAdapter

class val.v extends MyAsyncTask
{

    final NewThingsAdapter this$0;
    final ChildFeedModel val$bc;
    final View val$v;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient String doInBackground(Void avoid[])
    {
        if (MyApplication.b() != null)
        {
            return CommonRequest.doSetGroup(MyApplication.b(), (new StringBuilder()).append(val$bc.toUid).append("").toString(), ToolUtil.isTrue(val$bc.isFollowed), val$v, val$v);
        } else
        {
            return null;
        }
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((String)obj);
    }

    protected void onPostExecute(String s)
    {
        if (act2 == null || act2.isFinishing())
        {
            return;
        }
        NewThingsAdapter.access$400(NewThingsAdapter.this).setVisibility(8);
        if (s == null)
        {
            if ("true".equals(val$bc.isFollowed))
            {
                val$bc.isFollowed = "false";
            } else
            {
                val$bc.isFollowed = "true";
            }
            ((ToggleButton)val$v).setChecked(ToolUtil.isTrue(val$bc.isFollowed));
            return;
        } else
        {
            Toast.makeText(act2, s, 0).show();
            ((ToggleButton)val$v).setChecked(false);
            return;
        }
    }

    protected void onPreExecute()
    {
        NewThingsAdapter.access$400(NewThingsAdapter.this).setVisibility(0);
    }

    ()
    {
        this$0 = final_newthingsadapter;
        val$bc = childfeedmodel;
        val$v = View.this;
        super();
    }
}
