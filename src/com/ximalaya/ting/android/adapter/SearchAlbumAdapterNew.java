// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.model.search.SearchAlbum;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

public class SearchAlbumAdapterNew extends BaseAdapter
{

    private Activity mActiviry;
    private List mData;

    public SearchAlbumAdapterNew(Activity activity, List list)
    {
        mActiviry = activity;
        mData = list;
    }

    private static void doCollect(final Context context, final SearchAlbum model, final AlbumItemHolder holder)
    {
        if (UserInfoMannage.hasLogined())
        {
            String s;
            RequestParams requestparams;
            if (model.isFavorite)
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.id).toString());
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(holder.collect), new _cls2());
        } else
        {
            final AlbumModel am = ModelHelper.toAlbumModel(model);
            if (AlbumModelManage.getInstance().ensureLocalCollectAllow(context, am, holder.collect))
            {
                (new _cls3()).myexec(new Void[0]);
                return;
            }
        }
    }

    public static View getAlbumView(final Context context, Object obj, int i, final View viewHolder, ViewGroup viewgroup, boolean flag, boolean flag1)
    {
        viewgroup = viewHolder;
        if (viewHolder == null)
        {
            viewgroup = AlbumItemHolder.getView(context);
            ((AlbumItemHolder)viewgroup.getTag()).collect.setVisibility(8);
        }
        viewHolder = (AlbumItemHolder)viewgroup.getTag();
        if (obj instanceof SearchAlbum)
        {
            android.widget.RelativeLayout.LayoutParams layoutparams = (android.widget.RelativeLayout.LayoutParams)((AlbumItemHolder) (viewHolder)).border.getLayoutParams();
            layoutparams.height = Utilities.dip2px(context, 1.0F);
            ((AlbumItemHolder) (viewHolder)).border.setLayoutParams(layoutparams);
            ViewUtil.buildAlbumItemSpace(context, viewgroup, flag, flag1);
            obj = (SearchAlbum)obj;
            ImageManager2.from(context).displayImage(((AlbumItemHolder) (viewHolder)).cover, ((SearchAlbum) (obj)).cover_path, 0x7f0202e0);
            if (((SearchAlbum) (obj)).officialType == 1)
            {
                ((AlbumItemHolder) (viewHolder)).name.setCompoundDrawablesWithIntrinsicBounds(0x7f0202ae, 0, 0, 0);
            } else
            {
                ((AlbumItemHolder) (viewHolder)).name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            ((AlbumItemHolder) (viewHolder)).name.setText(((SearchAlbum) (obj)).title);
            ((AlbumItemHolder) (viewHolder)).collectCount.setText((new StringBuilder()).append(((SearchAlbum) (obj)).tracks).append("\u96C6").toString());
            ((AlbumItemHolder) (viewHolder)).updateAt.setText(((SearchAlbum) (obj)).intro);
            if (((SearchAlbum) (obj)).play > 0)
            {
                ((AlbumItemHolder) (viewHolder)).playCount.setVisibility(0);
                ((AlbumItemHolder) (viewHolder)).playCount.setText(StringUtil.getFriendlyNumStr(((SearchAlbum) (obj)).play));
            } else
            {
                ((AlbumItemHolder) (viewHolder)).playCount.setVisibility(8);
            }
            AlbumItemHolder.setCollectStatus(viewHolder, ((SearchAlbum) (obj)).isFavorite);
            ((AlbumItemHolder) (viewHolder)).collect.setTag(0x7f090000, obj);
            ((AlbumItemHolder) (viewHolder)).collect.setOnClickListener(new _cls1());
        }
        return viewgroup;
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public int getItemViewType(int i)
    {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        boolean flag2 = true;
        Object obj = mData.get(i);
        Activity activity = mActiviry;
        boolean flag;
        boolean flag1;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (i + 1 == mData.size())
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        view = getAlbumView(activity, obj, i, view, viewgroup, flag, flag1);
        viewgroup = mActiviry;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (i + 1 == mData.size())
        {
            flag1 = flag2;
        } else
        {
            flag1 = false;
        }
        ViewUtil.buildAlbumItemSpace(viewgroup, view, flag, flag1);
        return view;
    }

    public int getViewTypeCount()
    {
        return 1;
    }


    private class _cls2 extends a
    {

        final Context val$context;
        final AlbumItemHolder val$holder;
        final SearchAlbum val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, holder.collect);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            if (((SearchAlbum)holder.collect.getTag(0x7f090000)).id == model.id)
            {
                AlbumItemHolder.setCollectStatus(holder, model.isFavorite);
            }
        }

        public void onSuccess(String s)
        {
            boolean flag = true;
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
_L4:
            return;
_L2:
            int i;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return;
            }
            if (s == null) goto _L4; else goto _L3
_L3:
            i = s.getIntValue("ret");
            if (i != 0)
            {
                break MISSING_BLOCK_LABEL_132;
            }
            s = model;
            if (model.isFavorite)
            {
                flag = false;
            }
            s.isFavorite = flag;
            if (((SearchAlbum)holder.collect.getTag(0x7f090000)).id == model.id)
            {
                AlbumItemHolder.setCollectStatus(holder, model.isFavorite);
            }
            if (model.isFavorite)
            {
                s = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            Toast.makeText(context, s, 0).show();
            return;
            if (i != 791)
            {
                break MISSING_BLOCK_LABEL_147;
            }
            model.isFavorite = true;
            if (((SearchAlbum)holder.collect.getTag(0x7f090000)).id == model.id)
            {
                AlbumItemHolder.setCollectStatus(holder, model.isFavorite);
            }
            if (s.getString("msg") != null)
            {
                break MISSING_BLOCK_LABEL_215;
            }
            s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
_L6:
            Toast.makeText(context, s, 0).show();
            return;
            s = s.getString("msg");
            if (true) goto _L6; else goto _L5
_L5:
        }

        _cls2()
        {
            context = context1;
            holder = albumitemholder;
            model = searchalbum;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final AlbumModel val$am;
        final AlbumItemHolder val$holder;
        final SearchAlbum val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            SearchAlbum searchalbum = model;
            boolean flag;
            if (!model.isFavorite)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            searchalbum.isFavorite = flag;
            if (!model.isFavorite)
            {
                avoid.deleteAlbumInLocalAlbumList(am);
            } else
            {
                avoid.saveAlbumModel(am);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            if (((SearchAlbum)holder.collect.getTag(0x7f090000)).id == model.id)
            {
                AlbumItemHolder.setCollectStatus(holder, model.isFavorite);
            }
        }

        _cls3()
        {
            model = searchalbum;
            am = albummodel;
            holder = albumitemholder;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final Context val$context;
        final AlbumItemHolder val$viewHolder;

        public void onClick(View view)
        {
            view = (SearchAlbum)view.getTag(0x7f090000);
            if (view == null)
            {
                return;
            } else
            {
                SearchAlbumAdapterNew.doCollect(context, view, viewHolder);
                return;
            }
        }

        _cls1()
        {
            context = context1;
            viewHolder = albumitemholder;
            super();
        }
    }

}
