// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.fragment.userspace.MyAttentionFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

public class UserListAdapter extends BaseAdapter
    implements android.view.View.OnClickListener
{
    private static class ViewHolder
    {

        View border;
        ToggleButton concernImageButton;
        TextView fansNumTextView;
        ImageView itemImageView;
        TextView personDescribe;
        TextView soundsNumTextView;
        TextView stationNameTextView;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private List list;
    private Context mContext;
    MyAttentionFragment maf;
    private long meUid;

    public UserListAdapter(MyAttentionFragment myattentionfragment, List list1, LoginInfoModel logininfomodel)
    {
        mContext = myattentionfragment.getActivity().getApplicationContext();
        maf = myattentionfragment;
        if (logininfomodel != null)
        {
            meUid = logininfomodel.uid;
        }
        list = list1;
    }

    private void setGroup(final UserInfoModel bc, final View v)
    {
        if (UserInfoMannage.hasLogined())
        {
            if (bc.isFollowed)
            {
                (new DialogBuilder(maf.getActivity())).setMessage("\u786E\u5B9A\u8981\u53D6\u6D88\u5173\u6CE8\uFF1F").setOkBtn(new _cls2()).setCancelBtn(new _cls1()).showConfirm();
                return;
            } else
            {
                setGroupRequest(bc, v);
                return;
            }
        } else
        {
            bc = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
            bc.setFlags(0x20000000);
            maf.startActivity(bc);
            return;
        }
    }

    private void setGroupRequest(final UserInfoModel bc, final View v)
    {
        if (bc == null)
        {
            return;
        } else
        {
            (new _cls3()).myexec(new Void[0]);
            return;
        }
    }

    public void cleanData()
    {
        mContext = null;
        maf = null;
    }

    public int getCount()
    {
        if (list == null)
        {
            return 0;
        } else
        {
            return list.size();
        }
    }

    public UserInfoModel getItem(int i)
    {
        return (UserInfoModel)list.get(i);
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        UserInfoModel userinfomodel;
        if (view == null)
        {
            viewgroup = new ViewHolder(null);
            view = View.inflate(mContext, 0x7f030099, null);
            viewgroup.itemImageView = (ImageView)view.findViewById(0x7f0a0268);
            viewgroup.stationNameTextView = (TextView)view.findViewById(0x7f0a0269);
            viewgroup.soundsNumTextView = (TextView)view.findViewById(0x7f0a026b);
            viewgroup.fansNumTextView = (TextView)view.findViewById(0x7f0a026c);
            viewgroup.concernImageButton = (ToggleButton)view.findViewById(0x7f0a01b9);
            viewgroup.personDescribe = (TextView)view.findViewById(0x7f0a026d);
            viewgroup.border = view.findViewById(0x7f0a00a8);
            view.setTag(viewgroup);
            ((ViewHolder) (viewgroup)).concernImageButton.setOnClickListener(this);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        if (i + 1 == list.size())
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(4);
        } else
        {
            view.findViewById(0x7f0a00a8).setVisibility(0);
        }
        userinfomodel = (UserInfoModel)list.get(i);
        ((ViewHolder) (viewgroup)).stationNameTextView.setText(userinfomodel.getNickname());
        ((ViewHolder) (viewgroup)).soundsNumTextView.setText((new StringBuilder()).append("\u58F0\u97F3 ").append(StringUtil.getFriendlyNumStr(userinfomodel.tracks)).toString());
        ((ViewHolder) (viewgroup)).fansNumTextView.setText((new StringBuilder()).append("\u7C89\u4E1D ").append(StringUtil.getFriendlyNumStr(userinfomodel.followers)).toString());
        if (userinfomodel.isVerified)
        {
            if (mContext != null)
            {
                ((ViewHolder) (viewgroup)).stationNameTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, mContext.getResources().getDrawable(0x7f0200c0), null);
            }
        } else
        {
            ((ViewHolder) (viewgroup)).stationNameTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
        ((ViewHolder) (viewgroup)).concernImageButton.setChecked(userinfomodel.isFollowed);
        if (Utilities.isBlank(userinfomodel.personDescribe))
        {
            ((ViewHolder) (viewgroup)).personDescribe.setVisibility(8);
        } else
        {
            ((ViewHolder) (viewgroup)).personDescribe.setVisibility(0);
            ((ViewHolder) (viewgroup)).personDescribe.setText(userinfomodel.personDescribe);
        }
        if (userinfomodel.getSmallLogo() != null)
        {
            ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).itemImageView, userinfomodel.getSmallLogo(), -1);
        }
        if (0L != meUid && userinfomodel.getUid().longValue() != meUid) goto _L2; else goto _L1
_L1:
        ((ViewHolder) (viewgroup)).concernImageButton.setVisibility(8);
_L4:
        ((ViewHolder) (viewgroup)).concernImageButton.setTag(Integer.valueOf(i));
        return view;
_L2:
        try
        {
            ((ViewHolder) (viewgroup)).concernImageButton.setVisibility(0);
        }
        catch (Exception exception)
        {
            ((ViewHolder) (viewgroup)).concernImageButton.setVisibility(0);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public void onClick(View view)
    {
        int i = Integer.parseInt(view.getTag().toString());
        UserInfoModel userinfomodel = (UserInfoModel)list.get(i);
        switch (view.getId())
        {
        default:
            return;

        case 2131362233: 
            setGroup(userinfomodel, view);
            break;
        }
    }



    private class _cls2
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final UserListAdapter this$0;
        final UserInfoModel val$bc;
        final View val$v;

        public void onExecute()
        {
            setGroupRequest(bc, v);
        }

        _cls2()
        {
            this$0 = UserListAdapter.this;
            bc = userinfomodel;
            v = view;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final UserListAdapter this$0;
        final View val$v;

        public void onExecute()
        {
            ((ToggleButton)v).setChecked(true);
        }

        _cls1()
        {
            this$0 = UserListAdapter.this;
            v = view;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final UserListAdapter this$0;
        final UserInfoModel val$bc;
        final View val$v;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            if (MyApplication.b() != null)
            {
                return CommonRequest.doSetGroup(MyApplication.b(), (new StringBuilder()).append(bc.uid).append("").toString(), bc.isFollowed, v, v);
            } else
            {
                return null;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if (maf == null || !maf.canGoon())
            {
                return;
            }
            maf.dismissConcertProgressDialog();
            if (s == null)
            {
                s = bc;
                boolean flag;
                if (!bc.isFollowed)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                s.isFollowed = flag;
                ((ToggleButton)v).setChecked(bc.isFollowed);
                return;
            } else
            {
                Toast.makeText(mContext, s, 0).show();
                ((ToggleButton)v).setChecked(false);
                return;
            }
        }

        protected void onPreExecute()
        {
            if (maf != null && maf.canGoon())
            {
                maf.showConcertProgressDialog();
            }
        }

        _cls3()
        {
            this$0 = UserListAdapter.this;
            bc = userinfomodel;
            v = view;
            super();
        }
    }

}
