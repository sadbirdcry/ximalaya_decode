// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.widget.LinearLayout;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelnew.AlbumModelNew;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            SubjectDetailAdapter

class val.holder extends MyAsyncTask
{

    final SubjectDetailAdapter this$0;
    final AlbumModel val$am;
    final AlbumItemHolder val$holder;
    final AlbumModelNew val$model;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        avoid = AlbumModelManage.getInstance();
        AlbumModelNew albummodelnew = val$model;
        boolean flag;
        if (!val$model.isFavorite)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        albummodelnew.isFavorite = flag;
        if (!val$model.isFavorite)
        {
            avoid.deleteAlbumInLocalAlbumList(val$am);
        } else
        {
            avoid.saveAlbumModel(val$am);
        }
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        void1 = (AlbumModelNew)val$holder.collect.getTag(0x7f090000);
        if (void1 != null && ((AlbumModelNew) (void1)).id == val$model.id)
        {
            AlbumItemHolder.setCollectStatus(val$holder, val$model.isFavorite);
        }
    }

    ()
    {
        this$0 = final_subjectdetailadapter;
        val$model = albummodelnew;
        val$am = albummodel;
        val$holder = AlbumItemHolder.this;
        super();
    }
}
