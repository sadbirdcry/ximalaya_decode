// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Handler;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.HotSoundModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class SoundsHotAdapter extends BaseListSoundsAdapter
    implements android.view.View.OnClickListener
{
    private static class ViewHolder
    {

        TextView alltimeNumTV;
        TextView comment_tv;
        TextView commentsNumTV;
        LinearLayout context;
        TextView createTimeTV;
        TextView download_tv;
        ImageView itemImageView;
        TextView like_tv;
        TextView likesNumTV;
        ImageView playingImageView;
        TextView playtimesNumTV;
        int position;
        TextView sflagTV;
        TextView soundsNameText;
        TextView transmitNumTV;
        TextView usernameTextView;

        private ViewHolder()
        {
        }
    }


    public SoundsHotAdapter(Activity activity, List list, Handler handler)
    {
        super(activity, list);
        mHandler = handler;
    }

    private com.ximalaya.ting.android.a.a.a goDownLoad(int i, View view)
    {
        if (mData != null && mData.size() > i)
        {
            DownloadTask downloadtask = new DownloadTask((HotSoundModel)mData.get(i));
            if (downloadtask != null)
            {
                DownLoadTools downloadtools = DownLoadTools.getInstance();
                view = downloadtools.goDownload(downloadtask, (MyApplication)mContext.getApplicationContext(), view);
                downloadtools.release();
                return view;
            }
        }
        return null;
    }

    protected void bindData(final HotSoundModel info, final BaseListSoundsAdapter.ViewHolder holder)
    {
        b.a().a(ModelHelper.toSoundInfo(info));
        holder.cover.setOnClickListener(new _cls1());
        String s;
        TextView textview;
        if (isPlaying(info.id))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        holder.createTime.setText(ToolUtil.convertTime(info.createdAt));
        holder.owner.setText((new StringBuilder()).append("by ").append(info.nickname).toString());
        if (info.plays_counts > 0)
        {
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.plays_counts));
        } else
        {
            holder.playCount.setVisibility(8);
        }
        if (info.is_favorited)
        {
            holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020090), null, null, null);
        } else
        {
            holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020092), null, null, null);
        }
        if (info.comments_counts > 0)
        {
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.comments_counts));
        } else
        {
            holder.commentCount.setVisibility(8);
        }
        if (isDownload(info.id))
        {
            holder.btn.setImageResource(0x7f0200f1);
            holder.btn.setEnabled(false);
        } else
        {
            holder.btn.setImageResource(0x7f0201fe);
            holder.btn.setEnabled(true);
        }
        holder.btn.setOnClickListener(new _cls2());
        holder.title.setText(info.title);
        textview = holder.origin;
        if (info.user_source == 1)
        {
            s = "\u539F\u521B";
        } else
        {
            s = "\u91C7\u96C6";
        }
        textview.setText(s);
        holder.duration.setText(String.valueOf(ToolUtil.toTime(info.duration)));
        holder.cover.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(holder.cover, info.coverSmall, 0x7f0202de);
    }

    protected volatile void bindData(Object obj, BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((HotSoundModel)obj, viewholder);
    }

    public void onClick(View view)
    {
        ViewHolder viewholder = (ViewHolder)view.getTag();
        int i = viewholder.position;
        int j = view.getId();
        if (j == 0x7f0a04a1)
        {
            playSound(viewholder.playingImageView, i, ModelHelper.toSoundInfo((HotSoundModel)mData.get(i)), ModelHelper.hotlistToSoundInfoList(mData));
        } else
        if (ToolUtil.isConnectToNetwork(mContext))
        {
            if (j == 0x7f0a04a5)
            {
                view = goDownLoad(i, view);
                if (view != null && view.b() == 1)
                {
                    viewholder.download_tv.setText("\u5DF2\u4E0B\u8F7D");
                    viewholder.download_tv.setEnabled(false);
                    return;
                }
            } else
            if (UserInfoMannage.hasLogined())
            {
                switch (j)
                {
                default:
                    return;

                case 2131362183: 
                    toLike(((HotSoundModel)mData.get(i)).id, ((HotSoundModel)mData.get(i)).is_favorited, viewholder.like_tv, viewholder.likesNumTV);
                    view = (HotSoundModel)mData.get(i);
                    boolean flag;
                    if (!((HotSoundModel)mData.get(i)).is_favorited)
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                    view.is_favorited = flag;
                    return;

                case 2131362980: 
                    toComment(((HotSoundModel)mData.get(i)).id);
                    return;
                }
            } else
            {
                Intent intent = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                mContext.startActivity(intent);
                return;
            }
        } else
        {
            Toast.makeText(mContext, 0x7f0900a0, 1).show();
            return;
        }
    }

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SoundsHotAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final HotSoundModel val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, mData.indexOf(info), ModelHelper.toSoundInfo(info), ModelHelper.hotlistToSoundInfoList(mData));
        }

        _cls1()
        {
            this$0 = SoundsHotAdapter.this;
            holder = viewholder;
            info = hotsoundmodel;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final SoundsHotAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final HotSoundModel val$info;

        public void onClick(View view)
        {
            DownloadTask downloadtask = new DownloadTask(ModelHelper.toSoundInfo(info));
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            view = downloadtools.goDownload(downloadtask, mContext.getApplicationContext(), view);
            downloadtools.release();
            if (view == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls2()
        {
            this$0 = SoundsHotAdapter.this;
            info = hotsoundmodel;
            holder = viewholder;
            super();
        }
    }

}
