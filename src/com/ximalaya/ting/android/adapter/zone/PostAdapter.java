// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.zone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.zone.PostModel;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.MyTextView;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.util.ArrayList;
import java.util.List;

public class PostAdapter extends BaseAdapter
{
    static class ViewHolder
    {

        View borderThick;
        View borderThin;
        ImageView imgIv1;
        ImageView imgIv2;
        ImageView imgIv3;
        TextView lastUpdateTv;
        MyTextView postContentTv;
        MyTextView postTitleTv;
        RoundedImageView posterImg;
        TextView posterNameTv;
        TextView replyCountTv;
        ImageView tagIv;

        ViewHolder()
        {
        }
    }


    private Context mCon;
    private ArrayList mDataList;
    private BaseFragment mFragment;
    private boolean mIsBorderThin;

    public PostAdapter(BaseFragment basefragment, Context context, ArrayList arraylist, boolean flag)
    {
        mFragment = basefragment;
        mCon = context;
        mDataList = arraylist;
        mIsBorderThin = flag;
    }

    private void displayImg(ImageView imageview, String s)
    {
        ImageManager2.from(mCon).displayImage(imageview, s, 0x7f0200dc);
    }

    private void displayImgSample(ViewHolder viewholder, PostModel postmodel)
    {
        int i;
        if (postmodel.getImages() == null)
        {
            i = 0;
        } else
        {
            i = postmodel.getImages().size();
        }
        if (i > 0)
        {
            displayImg(viewholder.imgIv1, postmodel.getImgSmall(0));
            viewholder.imgIv1.setVisibility(0);
            if (i > 1)
            {
                displayImg(viewholder.imgIv2, postmodel.getImgSmall(1));
                viewholder.imgIv2.setVisibility(0);
                if (i > 2)
                {
                    displayImg(viewholder.imgIv3, postmodel.getImgSmall(2));
                    viewholder.imgIv3.setVisibility(0);
                    return;
                } else
                {
                    viewholder.imgIv3.setVisibility(8);
                    return;
                }
            } else
            {
                viewholder.imgIv2.setVisibility(8);
                viewholder.imgIv3.setVisibility(8);
                return;
            }
        } else
        {
            viewholder.imgIv1.setVisibility(8);
            viewholder.imgIv2.setVisibility(8);
            viewholder.imgIv3.setVisibility(8);
            return;
        }
    }

    public int getCount()
    {
        return mDataList.size();
    }

    public Object getItem(int i)
    {
        if (mDataList.size() >= i + 1)
        {
            return (PostModel)mDataList.get(i);
        } else
        {
            return null;
        }
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        String s;
        final PostModel m;
        Object obj;
        RoundedImageView roundedimageview;
        if (view == null)
        {
            ViewHolder viewholder = new ViewHolder();
            view = LayoutInflater.from(mCon).inflate(0x7f030131, viewgroup, false);
            viewholder.posterImg = (RoundedImageView)view.findViewById(0x7f0a04ba);
            viewholder.posterImg.setTag(0x7f0a0037, Boolean.valueOf(true));
            viewholder.imgIv1 = (ImageView)view.findViewById(0x7f0a04d7);
            viewholder.imgIv1.setTag(0x7f0a0037, Boolean.valueOf(true));
            viewholder.imgIv2 = (ImageView)view.findViewById(0x7f0a04d8);
            viewholder.imgIv2.setTag(0x7f0a0037, Boolean.valueOf(true));
            viewholder.imgIv3 = (ImageView)view.findViewById(0x7f0a04d9);
            viewholder.imgIv3.setTag(0x7f0a0037, Boolean.valueOf(true));
            int j = (ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 32F)) / 3;
            viewgroup = (android.widget.RelativeLayout.LayoutParams)viewholder.imgIv1.getLayoutParams();
            viewgroup.height = j;
            viewgroup.width = j;
            viewholder.imgIv1.setLayoutParams(viewgroup);
            viewgroup = (android.widget.RelativeLayout.LayoutParams)viewholder.imgIv2.getLayoutParams();
            viewgroup.height = j;
            viewgroup.width = j;
            viewgroup.leftMargin = ToolUtil.dp2px(mCon, 6F);
            viewholder.imgIv2.setLayoutParams(viewgroup);
            viewgroup = (android.widget.RelativeLayout.LayoutParams)viewholder.imgIv3.getLayoutParams();
            viewgroup.height = j;
            viewgroup.width = j;
            j = ToolUtil.dp2px(mCon, 6F);
            viewgroup.leftMargin = j;
            viewgroup.leftMargin = j;
            viewholder.imgIv3.setLayoutParams(viewgroup);
            viewholder.postTitleTv = (MyTextView)view.findViewById(0x7f0a04c0);
            viewholder.postContentTv = (MyTextView)view.findViewById(0x7f0a04d6);
            viewholder.posterNameTv = (TextView)view.findViewById(0x7f0a04bc);
            viewholder.tagIv = (ImageView)view.findViewById(0x7f0a04d4);
            viewholder.lastUpdateTv = (TextView)view.findViewById(0x7f0a04bf);
            viewholder.replyCountTv = (TextView)view.findViewById(0x7f0a04da);
            viewholder.borderThin = view.findViewById(0x7f0a04db);
            viewholder.borderThick = view.findViewById(0x7f0a04d2);
            mFragment.markImageView(viewholder.imgIv1);
            mFragment.markImageView(viewholder.imgIv2);
            mFragment.markImageView(viewholder.imgIv3);
            mFragment.markImageView(viewholder.posterImg);
            view.setTag(viewholder);
            viewgroup = viewholder;
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        m = (PostModel)mDataList.get(i);
        displayImgSample(viewgroup, m);
        ((ViewHolder) (viewgroup)).lastUpdateTv.setText(TimeHelper.countTime2(String.valueOf(m.getUpdatedTime())));
        ((ViewHolder) (viewgroup)).postTitleTv.setTakenWidth(ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 60F), 2);
        ((ViewHolder) (viewgroup)).postContentTv.setTakenWidth(ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 60F), 1);
        obj = ((ViewHolder) (viewgroup)).postTitleTv;
        if (m.getTitle() == null)
        {
            s = "";
        } else
        {
            s = m.getTitle();
        }
        ((MyTextView) (obj)).setText(s);
        obj = ((ViewHolder) (viewgroup)).postContentTv;
        if (m.getContent() == null)
        {
            s = "";
        } else
        {
            s = m.getContent();
        }
        ((MyTextView) (obj)).setText(s);
        obj = ((ViewHolder) (viewgroup)).posterNameTv;
        if (m.getPoster() == null)
        {
            s = "";
        } else
        {
            s = m.getPoster().getNickname();
        }
        ((TextView) (obj)).setText(s);
        ((ViewHolder) (viewgroup)).replyCountTv.setText(StringUtil.getFriendlyNumStr(m.getNumOfComments()));
        obj = ImageManager2.from(mCon);
        roundedimageview = ((ViewHolder) (viewgroup)).posterImg;
        if (m.getPoster() == null)
        {
            s = "";
        } else
        {
            s = m.getPoster().getSmallLogo();
        }
        ((ImageManager2) (obj)).displayImage(roundedimageview, s, 0x7f0202df);
        ((ViewHolder) (viewgroup)).posterImg.setOnClickListener(new _cls1());
        if (mIsBorderThin)
        {
            if (i + 1 == mDataList.size())
            {
                ((ViewHolder) (viewgroup)).borderThin.setVisibility(8);
                return view;
            } else
            {
                ((ViewHolder) (viewgroup)).borderThin.setVisibility(0);
                return view;
            }
        }
        if (i != 0)
        {
            ((ViewHolder) (viewgroup)).borderThick.setVisibility(0);
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).borderThick.setVisibility(8);
            return view;
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final PostAdapter this$0;
        final PostModel val$m;

        public void onClick(View view)
        {
            if (OneClickHelper.getInstance().onClick(view) && m != null && mFragment.isAdded() && mFragment.getActivity() != null)
            {
                view = new Bundle();
                view.putLong("toUid", m.getPoster().getUid());
                Intent intent = new Intent(mFragment.getActivity(), com/ximalaya/ting/android/activity/MainTabActivity2);
                intent.addFlags(0x4000000);
                intent.putExtra(a.s, view);
                intent.putExtra(a.t, com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment);
                mFragment.getActivity().startActivity(intent);
            }
        }

        _cls1()
        {
            this$0 = PostAdapter.this;
            m = postmodel;
            super();
        }
    }

}
