// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.zone;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PicAddAdapter extends BaseAdapter
{
    private class ViewHolder
    {

        private ImageView deleteBtnIv;
        private ImageView imageIv;
        final PicAddAdapter this$0;



/*
        static ImageView access$102(ViewHolder viewholder, ImageView imageview)
        {
            viewholder.imageIv = imageview;
            return imageview;
        }

*/



/*
        static ImageView access$202(ViewHolder viewholder, ImageView imageview)
        {
            viewholder.deleteBtnIv = imageview;
            return imageview;
        }

*/

        private ViewHolder()
        {
            this$0 = PicAddAdapter.this;
            super();
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private Context mContext;
    private List mDataList;
    private boolean mFirstLoaded;
    private String mFirstPath;
    private Map mUploadResult;

    public PicAddAdapter(Context context, List list, Map map)
    {
        mDataList = new ArrayList();
        mUploadResult = new HashMap();
        mContext = context;
        mDataList = list;
        mUploadResult = map;
    }

    public int getCount()
    {
        if (mDataList == null)
        {
            return 0;
        } else
        {
            return mDataList.size();
        }
    }

    public Object getItem(int i)
    {
        if (mDataList == null)
        {
            return null;
        } else
        {
            return (String)mDataList.get(i);
        }
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
        ViewHolder viewholder;
label0:
        {
label1:
            {
                String s;
                if (view == null)
                {
                    view = View.inflate(mContext, 0x7f03012f, null);
                    viewholder = new ViewHolder(null);
                    viewholder.imageIv = (ImageView)view.findViewById(0x7f0a04b7);
                    viewholder.imageIv.setTag(0x7f0a0037, Boolean.valueOf(true));
                    viewholder.deleteBtnIv = (ImageView)view.findViewById(0x7f0a04b8);
                    view.setTag(viewholder);
                } else
                {
                    viewholder = (ViewHolder)view.getTag();
                }
                if (mDataList != null && position < mDataList.size())
                {
                    s = (String)mDataList.get(position);
                } else
                {
                    s = "add_default";
                }
                if (s.contains("add_default"))
                {
                    viewholder.imageIv.setScaleType(android.widget.ImageView.ScaleType.FIT_XY);
                    viewholder.imageIv.setImageResource(0x7f0200e9);
                    viewholder.deleteBtnIv.setVisibility(8);
                }
                if (viewgroup != null && viewgroup.getChildCount() == position)
                {
                    if (mDataList != null && position < mDataList.size())
                    {
                        viewgroup = (String)mDataList.get(position);
                    } else
                    {
                        viewgroup = "add_default";
                    }
                    if (position != 0)
                    {
                        break label0;
                    }
                    if (!mFirstLoaded || !mFirstPath.equals("add_default") || !mFirstPath.equals(viewgroup))
                    {
                        break label1;
                    }
                }
                return view;
            }
            mFirstLoaded = true;
            mFirstPath = viewgroup;
        }
        if (viewgroup.contains("add_default"))
        {
            viewholder.imageIv.setScaleType(android.widget.ImageView.ScaleType.FIT_XY);
            viewholder.imageIv.setImageResource(0x7f0200e9);
            viewholder.deleteBtnIv.setVisibility(8);
        } else
        {
            viewholder.imageIv.setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
            ImageManager2.from(mContext).displayImage(viewholder.imageIv, ToolUtil.addFilePrefix((String)mDataList.get(position)), 0x7f0200dc, ToolUtil.dp2px(mContext, 65F), ToolUtil.dp2px(mContext, 65F));
            viewholder.deleteBtnIv.setVisibility(0);
        }
        viewholder.deleteBtnIv.setOnClickListener(new _cls1());
        return view;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final PicAddAdapter this$0;
        final int val$position;

        public void onClick(View view)
        {
            while (!OneClickHelper.getInstance().onClick(view) || position + 1 > mDataList.size()) 
            {
                return;
            }
            mDataList.remove(position);
            if (!mDataList.contains("add_default"))
            {
                mDataList.add("add_default");
            }
            notifyDataSetChanged();
        }

        _cls1()
        {
            this$0 = PicAddAdapter.this;
            position = i;
            super();
        }
    }

}
