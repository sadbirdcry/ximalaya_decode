// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.zone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.model.zone.RoleInfo;
import com.ximalaya.ting.android.model.zone.ZoneModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import com.ximalaya.ting.android.view.RoundedHexagonImageView;
import java.util.ArrayList;

public class ZoneAdapter extends BaseAdapter
{
    static class ViewHolder
    {

        ImageView arrowIv;
        View border;
        TextView followTv;
        RoundedHexagonImageView imgIv;
        TextView introTv;
        TextView memberNumTv;
        TextView nameTv;
        TextView postNumTv;

        ViewHolder()
        {
        }
    }

    public static interface ZoneActionListener
    {

        public abstract void follow(View view, int i);

        public abstract void unFollow(View view, int i);
    }


    private ZoneActionListener mActionListener;
    private Context mCon;
    private ArrayList mDataList;
    private BaseFragment mFragment;
    private boolean mShowFollow;

    public ZoneAdapter(BaseFragment basefragment, Context context, ArrayList arraylist)
    {
        this(basefragment, context, arraylist, true);
    }

    public ZoneAdapter(BaseFragment basefragment, Context context, ArrayList arraylist, boolean flag)
    {
        mFragment = basefragment;
        if (basefragment instanceof ZoneActionListener)
        {
            mActionListener = (ZoneActionListener)basefragment;
        }
        mCon = context;
        mDataList = arraylist;
        mShowFollow = flag;
    }

    public int getCount()
    {
        return mDataList.size();
    }

    public Object getItem(int i)
    {
        if (mDataList.size() >= i + 1)
        {
            return (ZoneModel)mDataList.get(i);
        } else
        {
            return null;
        }
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
        Object obj;
        final ZoneModel m;
        TextView textview;
        if (view == null)
        {
            view = LayoutInflater.from(mCon).inflate(0x7f030149, viewgroup, false);
            viewgroup = new ViewHolder();
            viewgroup.nameTv = (TextView)view.findViewById(0x7f0a03f1);
            viewgroup.imgIv = (RoundedHexagonImageView)view.findViewById(0x7f0a03ef);
            viewgroup.introTv = (TextView)view.findViewById(0x7f0a03f2);
            viewgroup.memberNumTv = (TextView)view.findViewById(0x7f0a03f3);
            viewgroup.postNumTv = (TextView)view.findViewById(0x7f0a03f4);
            viewgroup.followTv = (TextView)view.findViewById(0x7f0a03f5);
            viewgroup.arrowIv = (ImageView)view.findViewById(0x7f0a0153);
            viewgroup.border = view.findViewById(0x7f0a00a8);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        m = (ZoneModel)mDataList.get(position);
        textview = ((ViewHolder) (viewgroup)).nameTv;
        if (m.getDisplayName() == null)
        {
            obj = "";
        } else
        {
            obj = m.getDisplayName();
        }
        textview.setText(((CharSequence) (obj)));
        ((ViewHolder) (viewgroup)).memberNumTv.setText((new StringBuilder()).append("\u6210\u5458").append(StringUtil.getFriendlyNumStr(m.getNumOfMembers())).toString());
        ((ViewHolder) (viewgroup)).postNumTv.setText((new StringBuilder()).append("\u5E16\u5B50").append(StringUtil.getFriendlyNumStr(m.getNumOfPosts())).toString());
        ((ViewHolder) (viewgroup)).introTv.setText(m.getDescription());
        ImageManager2.from(mCon).displayImage(((ViewHolder) (viewgroup)).imgIv, m.getImageUrl(), 0x7f0200db);
        obj = UserInfoMannage.getInstance().getUser();
        if (mShowFollow)
        {
            if (obj != null && m.getOwner() != null && ((LoginInfoModel) (obj)).uid == m.getOwner().getUid())
            {
                ((ViewHolder) (viewgroup)).followTv.setVisibility(8);
            } else
            {
                ((ViewHolder) (viewgroup)).followTv.setVisibility(0);
                ViewUtil.expandClickArea(mCon, ((ViewHolder) (viewgroup)).followTv, 10, 25, 10, 25);
                if (m.getMyZone() != null && m.getMyZone().isJoint())
                {
                    ((ViewHolder) (viewgroup)).followTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    ((ViewHolder) (viewgroup)).followTv.setPadding(0, 0, 0, 0);
                    if (mCon != null)
                    {
                        ((ViewHolder) (viewgroup)).followTv.setBackgroundResource(0x7f020102);
                    }
                    ((ViewHolder) (viewgroup)).followTv.setText("\u5DF2\u52A0\u5165");
                } else
                {
                    ((ViewHolder) (viewgroup)).followTv.setPadding(ToolUtil.dp2px(mCon, 10F), 0, 0, 0);
                    ((ViewHolder) (viewgroup)).followTv.setCompoundDrawablesWithIntrinsicBounds(0x7f0205dc, 0, 0, 0);
                    if (mCon != null)
                    {
                        ((ViewHolder) (viewgroup)).followTv.setBackgroundResource(0x7f020103);
                    }
                    ((ViewHolder) (viewgroup)).followTv.setText("\u52A0\u5165");
                }
            }
            ((ViewHolder) (viewgroup)).followTv.setOnClickListener(new _cls1());
            ((ViewHolder) (viewgroup)).arrowIv.setVisibility(8);
        } else
        {
            ((ViewHolder) (viewgroup)).arrowIv.setVisibility(0);
            ((ViewHolder) (viewgroup)).followTv.setVisibility(8);
        }
        if (position + 1 == mDataList.size())
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(8);
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(0);
            return view;
        }
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final ZoneAdapter this$0;
        final ZoneModel val$m;
        final int val$position;

        public void onClick(final View v)
        {
            if (!UserInfoMannage.hasLogined())
            {
                if (mFragment.isAdded() && mFragment.getActivity() != null)
                {
                    v = new Intent(mFragment.getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity);
                    mFragment.getActivity().startActivity(v);
                }
            } else
            if (m != null && m.getMyZone() != null && mActionListener != null)
            {
                if (m.getMyZone().isJoint() && mFragment.getActivity() != null)
                {
                    class _cls1
                        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                    {

                        final _cls1 this$1;
                        final View val$v;

                        public void onExecute()
                        {
                            mActionListener.unFollow(v, position);
                        }

                _cls1()
                {
                    this$1 = _cls1.this;
                    v = view;
                    super();
                }
                    }

                    (new DialogBuilder(mFragment.getActivity())).setMessage("\u786E\u5B9A\u8981\u9000\u51FA\u6B64\u5708\u5B50\uFF1F").setOkBtn(new _cls1()).showConfirm();
                    return;
                } else
                {
                    mActionListener.follow(v, position);
                    return;
                }
            }
        }

        _cls1()
        {
            this$0 = ZoneAdapter.this;
            m = zonemodel;
            position = i;
            super();
        }
    }

}
