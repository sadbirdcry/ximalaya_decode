// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.zone;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.play.ImageViewer;
import com.ximalaya.ting.android.model.zone.CommentModel;
import com.ximalaya.ting.android.model.zone.ImageInfo;
import com.ximalaya.ting.android.model.zone.PostReportInfo;
import com.ximalaya.ting.android.model.zone.Poster;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter.zone:
//            ImageViewAdapter, ImageViewAdapter2

public class CommentAdapter extends BaseAdapter
{
    public static interface PostActionListner
    {

        public abstract void comment();

        public abstract void reply(String s, long l, long l1);

        public abstract void report(PostReportInfo postreportinfo, View view);
    }

    static class ViewHolder
    {

        ImageViewAdapter adpt;
        ImageViewAdapter2 adpt2;
        View border;
        View borderThick;
        TextView contentTv;
        TextView floorTv;
        GridView gridView;
        ImageView hostIv;
        TextView lastUpdateTv;
        ListView listView;
        RelativeLayout listViewLayout;
        TextView p_contentTv;
        TextView p_floorTv;
        ImageView p_hostIv;
        TextView p_lastUpdateTv;
        TextView p_posterNameTv;
        RoundedImageView p_poster_icon_img;
        RelativeLayout parentLayout;
        TextView postTitleTv;
        RoundedImageView posterImg;
        TextView posterNameTv;
        TextView replyTv;
        TextView reportTv;
        View temp;

        ViewHolder()
        {
        }
    }


    private PostActionListner mActionListener;
    private Context mCon;
    private ArrayList mDataList;
    private BaseFragment mFragment;
    private long mHostUid;
    private ImageViewer mImageViewer;

    public CommentAdapter(BaseFragment basefragment, ImageViewer imageviewer, Context context, ArrayList arraylist)
    {
        mFragment = basefragment;
        mImageViewer = imageviewer;
        if (mFragment instanceof PostActionListner)
        {
            mActionListener = (PostActionListner)mFragment;
        }
        mCon = context;
        mDataList = arraylist;
    }

    public CommentAdapter(BaseFragment basefragment, ImageViewer imageviewer, Context context, ArrayList arraylist, long l)
    {
        this(basefragment, imageviewer, context, arraylist);
        mHostUid = l;
    }

    private ArrayList getImagesAndSetHeightForGridView(GridView gridview, CommentModel commentmodel)
    {
        int i1 = 3;
        ArrayList arraylist = new ArrayList();
        if (commentmodel.getImages() == null)
        {
            return arraylist;
        }
        int j1 = (ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 83F)) / 3;
        int i = commentmodel.getImages().size();
        int k1 = i / 3;
        int j;
        int k;
        int l;
        int l1;
        int i2;
        int j2;
        if (i % 3 == 0)
        {
            j = 0;
        } else
        {
            j = 1;
        }
        l1 = ToolUtil.dp2px(mCon, 6F);
        i2 = ToolUtil.dp2px(mCon, 6F);
        if (i > 3)
        {
            k = 3;
        } else
        {
            k = i;
        }
        j2 = ToolUtil.dp2px(mCon, 6F);
        if (i > 3)
        {
            l = 2;
        } else
        {
            l = i - 1;
        }
        if (i <= 3)
        {
            i1 = i;
        }
        gridview.setNumColumns(i1);
        for (i1 = 0; i1 != i; i1++)
        {
            arraylist.add((ImageInfo)((List)commentmodel.getImages().get(i1)).get(0));
        }

        commentmodel = gridview.getLayoutParams();
        commentmodel.height = (j + k1) * (l1 + j1) - i2;
        commentmodel.width = j1 * k + l * j2;
        gridview.setLayoutParams(commentmodel);
        return arraylist;
    }

    private ArrayList getImagesAndSetHeightForListView(ListView listview, CommentModel commentmodel)
    {
        ArrayList arraylist = new ArrayList();
        if (commentmodel.getImages() == null)
        {
            return arraylist;
        }
        int j1 = ToolUtil.getScreenWidth(mCon) - ToolUtil.dp2px(mCon, 71F);
        int k1 = commentmodel.getImages().size();
        int i1 = 0;
        int l = 0;
        int k = 0;
        while (i1 != k1) 
        {
            float f;
            ImageInfo imageinfo;
            int i;
            if (((ImageInfo)((List)commentmodel.getImages().get(i1)).get(1)).getWidth() <= ((ImageInfo)((List)commentmodel.getImages().get(i1)).get(2)).getWidth())
            {
                imageinfo = (ImageInfo)((List)commentmodel.getImages().get(i1)).get(1);
            } else
            {
                imageinfo = (ImageInfo)((List)commentmodel.getImages().get(i1)).get(2);
            }
            if (j1 <= imageinfo.getWidth())
            {
                float f1 = ((float)j1 / (float)imageinfo.getWidth()) * (float)imageinfo.getHeight();
                f = f1;
                i = l;
                if (j1 > l)
                {
                    i = j1;
                    f = f1;
                }
            } else
            {
                float f2 = imageinfo.getHeight();
                f = f2;
                i = l;
                if (imageinfo.getWidth() > l)
                {
                    i = imageinfo.getWidth();
                    f = f2;
                }
            }
            k = (int)(f + (float)k);
            arraylist.add(imageinfo);
            i1++;
            l = i;
        }
        android.view.ViewGroup.LayoutParams layoutparams = listview.getLayoutParams();
        i1 = listview.getDividerHeight();
        int j;
        if (commentmodel.getImages().size() == 0)
        {
            j = 0;
        } else
        {
            j = commentmodel.getImages().size() - 1;
        }
        layoutparams.height = j * i1 + k;
        layoutparams.width = l;
        listview.setLayoutParams(layoutparams);
        return arraylist;
    }

    private ArrayList getOriginalImgs(CommentModel commentmodel)
    {
        ArrayList arraylist = new ArrayList();
        if (commentmodel.getImages() == null)
        {
            return arraylist;
        }
        int j = commentmodel.getImages().size();
        for (int i = 0; i != j; i++)
        {
            if (((List)commentmodel.getImages().get(i)).size() > 0)
            {
                arraylist.add(((List)commentmodel.getImages().get(i)).get(((List)commentmodel.getImages().get(i)).size() - 1));
            }
        }

        return arraylist;
    }

    public int getCount()
    {
        return mDataList.size();
    }

    public Object getItem(int i)
    {
        if (mDataList.size() >= i + 1)
        {
            return (CommentModel)mDataList.get(i);
        } else
        {
            return null;
        }
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, final ViewGroup vh)
    {
        Object obj;
        final CommentModel m;
        ImageManager2 imagemanager2;
        RoundedImageView roundedimageview;
        boolean flag;
        if (view == null)
        {
            vh = new ViewHolder();
            view = LayoutInflater.from(mCon).inflate(0x7f030130, null);
            vh.posterImg = (RoundedImageView)view.findViewById(0x7f0a04ba);
            ((ViewHolder) (vh)).posterImg.setTag(0x7f0a0037, Boolean.valueOf(true));
            vh.postTitleTv = (TextView)view.findViewById(0x7f0a04c0);
            vh.posterNameTv = (TextView)view.findViewById(0x7f0a04bc);
            vh.hostIv = (ImageView)view.findViewById(0x7f0a04bd);
            vh.lastUpdateTv = (TextView)view.findViewById(0x7f0a04bf);
            vh.floorTv = (TextView)view.findViewById(0x7f0a04be);
            vh.replyTv = (TextView)view.findViewById(0x7f0a04cf);
            vh.reportTv = (TextView)view.findViewById(0x7f0a04d1);
            vh.contentTv = (TextView)view.findViewById(0x7f0a04c1);
            vh.border = view.findViewById(0x7f0a00a8);
            vh.temp = view.findViewById(0x7f0a04d0);
            vh.borderThick = view.findViewById(0x7f0a04d2);
            vh.adpt = new ImageViewAdapter(mFragment, mCon, new ArrayList());
            vh.adpt2 = new ImageViewAdapter2(mFragment, mCon, new ArrayList());
            vh.gridView = (GridView)view.findViewById(0x7f0a04c3);
            ((ViewHolder) (vh)).gridView.setAdapter(((ViewHolder) (vh)).adpt);
            vh.listView = (ListView)view.findViewById(0x7f0a04c4);
            ((ViewHolder) (vh)).listView.setAdapter(((ViewHolder) (vh)).adpt2);
            vh.listViewLayout = (RelativeLayout)view.findViewById(0x7f0a04c2);
            vh.parentLayout = (RelativeLayout)view.findViewById(0x7f0a04c5);
            vh.p_poster_icon_img = (RoundedImageView)view.findViewById(0x7f0a04c7);
            vh.p_posterNameTv = (TextView)view.findViewById(0x7f0a04c8);
            vh.p_lastUpdateTv = (TextView)view.findViewById(0x7f0a04cc);
            vh.p_floorTv = (TextView)view.findViewById(0x7f0a04cb);
            vh.p_contentTv = (TextView)view.findViewById(0x7f0a04cd);
            vh.p_hostIv = (ImageView)view.findViewById(0x7f0a04c9);
            view.setTag(vh);
        } else
        {
            vh = (ViewHolder)view.getTag();
        }
        m = (CommentModel)mDataList.get(i);
        imagemanager2 = ImageManager2.from(mCon);
        roundedimageview = ((ViewHolder) (vh)).posterImg;
        if (m.getPoster() == null)
        {
            obj = "";
        } else
        {
            obj = m.getPoster().getSmallLogo();
        }
        imagemanager2.displayImage(roundedimageview, ((String) (obj)), 0x7f0202df);
        if (m.isMainPost())
        {
            TextView textview = ((ViewHolder) (vh)).postTitleTv;
            if (m.getTitle() == null)
            {
                obj = "";
            } else
            {
                obj = m.getTitle();
            }
            textview.setText(((CharSequence) (obj)));
            ((ViewHolder) (vh)).postTitleTv.setVisibility(0);
            ((ViewHolder) (vh)).reportTv.setVisibility(0);
            ((ViewHolder) (vh)).posterNameTv.setTextColor(mCon.getResources().getColor(0x7f07009e));
            ((ViewHolder) (vh)).floorTv.setText("\u697C\u4E3B");
            ((ViewHolder) (vh)).reportTv.setOnClickListener(new _cls1());
            ((ViewHolder) (vh)).replyTv.setText(StringUtil.getFriendlyNumStr(m.getNumOfComments()));
            ((ViewHolder) (vh)).temp.setVisibility(0);
        } else
        {
            ((ViewHolder) (vh)).postTitleTv.setVisibility(8);
            ((ViewHolder) (vh)).replyTv.setText("\u56DE\u590D");
            ((ViewHolder) (vh)).posterNameTv.setTextColor(mCon.getResources().getColor(0x7f0700ac));
            ((ViewHolder) (vh)).floorTv.setText((new StringBuilder()).append(m.getNumOfFloor()).append("\u697C").toString());
            ((ViewHolder) (vh)).reportTv.setVisibility(8);
            ((ViewHolder) (vh)).temp.setVisibility(8);
        }
        ((ViewHolder) (vh)).replyTv.setOnClickListener(new _cls2());
        if (m.getPoster() != null && m.getPoster().getUid() == mHostUid)
        {
            ((ViewHolder) (vh)).hostIv.setVisibility(0);
        } else
        {
            ((ViewHolder) (vh)).hostIv.setVisibility(8);
        }
        textview = ((ViewHolder) (vh)).posterNameTv;
        if (m.getPoster() == null)
        {
            obj = "";
        } else
        {
            obj = m.getPoster().getNickname();
        }
        textview.setText(((CharSequence) (obj)));
        ((ViewHolder) (vh)).lastUpdateTv.setText(TimeHelper.countTime2(String.valueOf(m.getCreatedTime())));
        textview = ((ViewHolder) (vh)).contentTv;
        if (m.getContent() == null)
        {
            obj = "";
        } else
        {
            obj = EmotionUtil.getInstance().convertEmotionTextWithChangeLine(m.getContent());
        }
        textview.setText(((CharSequence) (obj)));
        if (m.isMainPost() && m.getImages().size() > 0 && ((List)m.getImages().get(0)).size() > 2)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (flag)
        {
            ((ViewHolder) (vh)).adpt2.setDataList(getImagesAndSetHeightForListView(((ViewHolder) (vh)).listView, m));
            ((ViewHolder) (vh)).listView.setVisibility(0);
            ((ViewHolder) (vh)).gridView.setVisibility(8);
            ((ViewHolder) (vh)).adpt2.setImageViewer(getOriginalImgs(m), mImageViewer);
            ((ViewHolder) (vh)).adpt2.notifyDataSetChanged();
            if (((ViewHolder) (vh)).adpt2.getCount() == 0)
            {
                if (((ViewHolder) (vh)).listViewLayout.getVisibility() != 8)
                {
                    ((ViewHolder) (vh)).listViewLayout.setVisibility(8);
                }
            } else
            if (((ViewHolder) (vh)).listViewLayout.getVisibility() != 0)
            {
                ((ViewHolder) (vh)).listViewLayout.setVisibility(0);
            }
        } else
        {
            ((ViewHolder) (vh)).adpt.setDataList(getImagesAndSetHeightForGridView(((ViewHolder) (vh)).gridView, m));
            ((ViewHolder) (vh)).listView.setVisibility(8);
            ((ViewHolder) (vh)).gridView.setVisibility(0);
            ((ViewHolder) (vh)).adpt.setImageViewer(getOriginalImgs(m), mImageViewer);
            ((ViewHolder) (vh)).adpt.notifyDataSetChanged();
            if (((ViewHolder) (vh)).adpt.getCount() == 0)
            {
                if (((ViewHolder) (vh)).listViewLayout.getVisibility() != 8)
                {
                    ((ViewHolder) (vh)).listViewLayout.setVisibility(8);
                }
            } else
            if (((ViewHolder) (vh)).listViewLayout.getVisibility() != 0)
            {
                ((ViewHolder) (vh)).listViewLayout.setVisibility(0);
            }
        }
        if (m.getParentComment() == null)
        {
            ((ViewHolder) (vh)).parentLayout.setVisibility(8);
        } else
        {
            Object obj2 = ImageManager2.from(mCon);
            RoundedImageView roundedimageview1 = ((ViewHolder) (vh)).p_poster_icon_img;
            Object obj1;
            if (m.getParentComment().getPoster() == null)
            {
                obj1 = "";
            } else
            {
                obj1 = m.getParentComment().getPoster().getSmallLogo();
            }
            ((ImageManager2) (obj2)).displayImage(roundedimageview1, ((String) (obj1)), 0x7f0202df);
            obj2 = ((ViewHolder) (vh)).p_contentTv;
            if (m.getParentComment().getContent() == null)
            {
                obj1 = "";
            } else
            {
                obj1 = EmotionUtil.getInstance().convertEmotionTextWithChangeLine(m.getParentComment().getContent());
            }
            ((TextView) (obj2)).setText(((CharSequence) (obj1)));
            ((ViewHolder) (vh)).p_floorTv.setText((new StringBuilder()).append(m.getParentComment().getNumOfFloor()).append("\u697C").toString());
            obj2 = ((ViewHolder) (vh)).p_posterNameTv;
            if (m.getParentComment().getPoster() == null)
            {
                obj1 = "";
            } else
            {
                obj1 = m.getParentComment().getPoster().getNickname();
            }
            ((TextView) (obj2)).setText(((CharSequence) (obj1)));
            ((ViewHolder) (vh)).p_lastUpdateTv.setText(TimeHelper.countTime2(String.valueOf(m.getParentComment().getCreatedTime())));
            if (m.getParentComment().getPoster() != null && m.getParentComment().getPoster().getUid() == mHostUid)
            {
                ((ViewHolder) (vh)).p_hostIv.setVisibility(0);
            } else
            {
                ((ViewHolder) (vh)).p_hostIv.setVisibility(8);
            }
            ((ViewHolder) (vh)).parentLayout.setVisibility(0);
        }
        ((ViewHolder) (vh)).posterImg.setOnClickListener(new _cls3());
        ((ViewHolder) (vh)).p_poster_icon_img.setOnClickListener(new _cls4());
        if (i + 1 == mDataList.size() || m.isMainPost() && mDataList.size() > 1)
        {
            ((ViewHolder) (vh)).border.setVisibility(8);
        } else
        {
            ((ViewHolder) (vh)).border.setVisibility(0);
        }
        if (m.isMainPost() && mDataList.size() > 1)
        {
            ((ViewHolder) (vh)).borderThick.setVisibility(0);
            return view;
        } else
        {
            ((ViewHolder) (vh)).borderThick.setVisibility(8);
            return view;
        }
    }

    public void setHostUid(long l)
    {
        mHostUid = l;
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final CommentAdapter this$0;
        final CommentModel val$m;
        final ViewHolder val$vh;

        public void onClick(View view)
        {
            if (OneClickHelper.getInstance().onClick(view)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if (UserInfoMannage.hasLogined())
            {
                break; /* Loop/switch isn't completed */
            }
            if (mFragment.isAdded() && mFragment.getActivity() != null)
            {
                view = new Intent(mFragment.getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity);
                mFragment.getActivity().startActivity(view);
                return;
            }
            if (true) goto _L1; else goto _L3
_L3:
            view = new PostReportInfo();
            view.setReporterId(UserInfoMannage.getInstance().getUser().getUid().longValue());
            view.setReportedId(m.getPoster().getUid());
            view.setPostId(m.getId());
            view.setType(1);
            if (mActionListener != null)
            {
                mActionListener.report(view, vh.reportTv);
                return;
            }
            if (true) goto _L1; else goto _L4
_L4:
        }

        _cls1()
        {
            this$0 = CommentAdapter.this;
            m = commentmodel;
            vh = viewholder;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final CommentAdapter this$0;
        final CommentModel val$m;

        public void onClick(View view)
        {
            if (OneClickHelper.getInstance().onClick(view)) goto _L2; else goto _L1
_L1:
            return;
_L2:
            if (UserInfoMannage.hasLogined())
            {
                continue; /* Loop/switch isn't completed */
            }
            if (!mFragment.isAdded() || mFragment.getActivity() == null) goto _L1; else goto _L3
_L3:
            view = new Intent(mFragment.getActivity(), com/ximalaya/ting/android/activity/login/LoginActivity);
            mFragment.getActivity().startActivity(view);
            return;
            if (mActionListener == null) goto _L1; else goto _L4
_L4:
            if (m.isMainPost())
            {
                mActionListener.comment();
                return;
            } else
            {
                mActionListener.reply(m.getPoster().getNickname(), m.getId(), m.getGroupId());
                return;
            }
        }

        _cls2()
        {
            this$0 = CommentAdapter.this;
            m = commentmodel;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final CommentAdapter this$0;
        final CommentModel val$m;

        public void onClick(View view)
        {
            if (OneClickHelper.getInstance().onClick(view) && m != null && mFragment.isAdded() && mFragment.getActivity() != null)
            {
                view = new Bundle();
                view.putLong("toUid", m.getPoster().getUid());
                Intent intent = new Intent(mFragment.getActivity(), com/ximalaya/ting/android/activity/MainTabActivity2);
                intent.addFlags(0x4000000);
                intent.putExtra(a.s, view);
                intent.putExtra(a.t, com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment);
                mFragment.getActivity().startActivity(intent);
            }
        }

        _cls3()
        {
            this$0 = CommentAdapter.this;
            m = commentmodel;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final CommentAdapter this$0;
        final CommentModel val$m;

        public void onClick(View view)
        {
            if (OneClickHelper.getInstance().onClick(view) && m.getParentComment() != null && mFragment.isAdded() && mFragment.getActivity() != null)
            {
                view = new Bundle();
                view.putLong("toUid", m.getParentComment().getPoster().getUid());
                Intent intent = new Intent(mFragment.getActivity(), com/ximalaya/ting/android/activity/MainTabActivity2);
                intent.addFlags(0x4000000);
                intent.putExtra(a.s, view);
                intent.putExtra(a.t, com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment);
                mFragment.getActivity().startActivity(intent);
            }
        }

        _cls4()
        {
            this$0 = CommentAdapter.this;
            m = commentmodel;
            super();
        }
    }

}
