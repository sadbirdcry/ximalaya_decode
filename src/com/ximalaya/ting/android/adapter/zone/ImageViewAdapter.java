// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.zone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.play.ImageViewer;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.zone.ImageInfo;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ImageViewAdapter extends BaseAdapter
{
    static class ViewHolder
    {

        ImageView iv;

        ViewHolder()
        {
        }
    }


    private Context mContext;
    private ArrayList mDataList;
    private BaseFragment mFragment;
    private ImageViewer mImageViewer;
    private ArrayList mOriginalPicList;

    public ImageViewAdapter(BaseFragment basefragment, Context context, ArrayList arraylist)
    {
        mFragment = basefragment;
        mContext = context;
        mDataList = arraylist;
    }

    private List getImgPaths()
    {
        ArrayList arraylist = new ArrayList();
        if (mOriginalPicList != null)
        {
            for (Iterator iterator = mOriginalPicList.iterator(); iterator.hasNext(); arraylist.add(((ImageInfo)iterator.next()).getImageUrl())) { }
        }
        return arraylist;
    }

    public int getCount()
    {
        if (mDataList == null)
        {
            return 0;
        } else
        {
            return mDataList.size();
        }
    }

    public Object getItem(int i)
    {
        if (mDataList == null || mDataList.isEmpty())
        {
            return null;
        } else
        {
            return (ImageInfo)mDataList.get(i);
        }
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(final int position, View view, final ViewGroup vh)
    {
        Logger.d("zone", (new StringBuilder()).append("position").append(position).toString());
        ImageInfo imageinfo;
        android.widget.RelativeLayout.LayoutParams layoutparams;
        int i;
        if (view == null)
        {
            view = LayoutInflater.from(mContext).inflate(0x7f030128, null);
            vh = new ViewHolder();
            vh.iv = (ImageView)view.findViewById(0x7f0a04a9);
            ((ViewHolder) (vh)).iv.setTag(0x7f0a0037, Boolean.valueOf(true));
            ((ViewHolder) (vh)).iv.setScaleType(android.widget.ImageView.ScaleType.CENTER_CROP);
            mFragment.markImageView(((ViewHolder) (vh)).iv);
            view.setTag(vh);
        } else
        {
            vh = (ViewHolder)view.getTag();
        }
        imageinfo = (ImageInfo)mDataList.get(position);
        i = (ToolUtil.getScreenWidth(mContext) - ToolUtil.dp2px(mContext, 83F)) / 3;
        layoutparams = new android.widget.RelativeLayout.LayoutParams(i, i);
        ((ViewHolder) (vh)).iv.setLayoutParams(layoutparams);
        ImageManager2.from(mContext).displayImage(((ViewHolder) (vh)).iv, imageinfo.getImageUrl(), 0x7f0200dc, i, i);
        ((ViewHolder) (vh)).iv.setOnClickListener(new _cls1());
        return view;
    }

    public void setDataList(ArrayList arraylist)
    {
        mDataList.clear();
        mDataList.addAll(arraylist);
    }

    public void setImageViewer(ArrayList arraylist, ImageViewer imageviewer)
    {
        mOriginalPicList = arraylist;
        mImageViewer = imageviewer;
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final ImageViewAdapter this$0;
        final int val$position;
        final ViewHolder val$vh;

        public void onClick(View view)
        {
            if (OneClickHelper.getInstance().onClick(view) && mImageViewer != null)
            {
                mImageViewer.setData(getImgPaths());
                mImageViewer.show(vh.iv, position);
            }
        }

        _cls1()
        {
            this$0 = ImageViewAdapter.this;
            vh = viewholder;
            position = i;
            super();
        }
    }

}
