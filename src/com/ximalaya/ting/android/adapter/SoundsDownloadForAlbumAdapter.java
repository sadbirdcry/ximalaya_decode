// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class SoundsDownloadForAlbumAdapter extends BaseAdapter
    implements android.view.View.OnClickListener
{
    public static class ViewHolder
    {

        public TextView albumAuthorTextView;
        public ImageView albumImageImageView;
        public TextView albumNameTextView;
        public TextView albumNumTextView;
        public ImageView del_btn;
        public TextView fileSize;
        public RelativeLayout parent;
        public int position;

        public ViewHolder()
        {
        }
    }


    private final long NO_ALBUM = 0L;
    public HashMap albumMap;
    private List list;
    protected Context mContext;
    public List mapKey;

    public SoundsDownloadForAlbumAdapter(Context context, List list1)
    {
        mContext = context;
        list = list1;
        initAlbumList();
    }

    private String getFileSize(List list1)
    {
        if (list1 == null)
        {
            return "";
        }
        int j = list1.size();
        long l = 0L;
        for (int i = 0; i != j; i++)
        {
            l += ((DownloadTask)list1.get(i)).filesize;
        }

        return ToolUtil.formatFileSize(l);
    }

    public int getCount()
    {
        if (mapKey == null)
        {
            return 0;
        } else
        {
            return albumMap.size();
        }
    }

    public Object getItem(int i)
    {
        return mapKey.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        boolean flag1 = true;
        if (view == null)
        {
            viewgroup = new ViewHolder();
            view = View.inflate(mContext, 0x7f030111, null);
            viewgroup.parent = (RelativeLayout)view.findViewById(0x7f0a00f1);
            viewgroup.albumImageImageView = (ImageView)view.findViewById(0x7f0a0023);
            viewgroup.albumNameTextView = (TextView)view.findViewById(0x7f0a00ea);
            viewgroup.albumNumTextView = (TextView)view.findViewById(0x7f0a0461);
            viewgroup.albumAuthorTextView = (TextView)view.findViewById(0x7f0a02aa);
            viewgroup.del_btn = (ImageView)view.findViewById(0x7f0a045f);
            ((ViewHolder) (viewgroup)).del_btn.setOnClickListener(this);
            viewgroup.fileSize = (TextView)view.findViewById(0x7f0a0462);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        if (mapKey != null && i < mapKey.size())
        {
            long l = ((Long)mapKey.get(i)).longValue();
            List list1 = (List)albumMap.get(Long.valueOf(l));
            if (list1 != null)
            {
                boolean flag;
                if (l == 0L)
                {
                    ((ViewHolder) (viewgroup)).albumNameTextView.setText("\u672A\u547D\u540D\u4E13\u8F91");
                    ((ViewHolder) (viewgroup)).albumAuthorTextView.setText("\u672A\u547D\u540D");
                    ((ViewHolder) (viewgroup)).albumImageImageView.setImageResource(0x7f0202e0);
                } else
                {
                    DownloadTask downloadtask = (DownloadTask)list1.get(0);
                    if (downloadtask != null)
                    {
                        TextView textview = ((ViewHolder) (viewgroup)).albumNameTextView;
                        String s;
                        StringBuilder stringbuilder;
                        if (downloadtask.albumName == null)
                        {
                            s = "";
                        } else
                        {
                            s = downloadtask.albumName;
                        }
                        textview.setText(s);
                        textview = ((ViewHolder) (viewgroup)).albumAuthorTextView;
                        stringbuilder = (new StringBuilder()).append("by ");
                        if (downloadtask.nickname == null)
                        {
                            s = "";
                        } else
                        {
                            s = downloadtask.nickname;
                        }
                        textview.setText(stringbuilder.append(s).toString());
                        ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).albumImageImageView, downloadtask.albumCoverPath, 0x7f0202e0);
                    }
                }
                ((ViewHolder) (viewgroup)).albumNumTextView.setText((new StringBuilder()).append(list1.size()).append("\u96C6").toString());
                ((ViewHolder) (viewgroup)).fileSize.setText(getFileSize(list1));
                viewgroup.position = i;
                ((ViewHolder) (viewgroup)).del_btn.setVisibility(0);
                ((ViewHolder) (viewgroup)).del_btn.setTag(viewgroup);
                viewgroup = mContext;
                if (i == 0)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                if (albumMap == null || i < 0 || i + 1 != albumMap.size() || albumMap.size() <= 4)
                {
                    flag1 = false;
                }
                ViewUtil.buildAlbumItemSpace(viewgroup, view, flag, flag1);
                return view;
            }
        }
        return view;
    }

    public void initAlbumList()
    {
        if (list != null)
        {
            albumMap = new HashMap();
            mapKey = new ArrayList();
            for (Iterator iterator = list.iterator(); iterator.hasNext();)
            {
                DownloadTask downloadtask = (DownloadTask)iterator.next();
                long l;
                if (downloadtask.albumId != 0L)
                {
                    l = downloadtask.albumId;
                } else
                {
                    l = 0L;
                }
                if (albumMap.containsKey(Long.valueOf(l)))
                {
                    ((List)albumMap.get(Long.valueOf(l))).add(downloadtask);
                } else
                if (l != 0L)
                {
                    ArrayList arraylist = new ArrayList();
                    arraylist.add(downloadtask);
                    albumMap.put(Long.valueOf(l), arraylist);
                    mapKey.add(Long.valueOf(l));
                } else
                {
                    ArrayList arraylist1 = new ArrayList();
                    arraylist1.add(downloadtask);
                    mapKey.add(Long.valueOf(l));
                    albumMap.put(Long.valueOf(0L), arraylist1);
                }
            }

        }
    }

    public void onClick(View view)
    {
        view = (ViewHolder)view.getTag();
        final long key = ((Long)mapKey.get(((ViewHolder) (view)).position)).longValue();
        (new DialogBuilder(mContext)).setMessage("\u786E\u5B9A\u5220\u9664\u8BE5\u4E13\u8F91\u7684\u6240\u6709\u58F0\u97F3\uFF1F").setOkBtn(new _cls1()).showConfirm();
    }

    public void setList(List list1)
    {
        list = list1;
        initAlbumList();
    }

    private class _cls1
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final SoundsDownloadForAlbumAdapter this$0;
        final long val$key;

        public void onExecute()
        {
            class _cls1 extends MyAsyncTask
            {

                ProgressDialog pd;
                final _cls1 this$1;

                protected transient Boolean doInBackground(Void avoid[])
                {
                    avoid = (List)albumMap.get(Long.valueOf(key));
                    if (avoid != null)
                    {
                        DownloadHandler downloadhandler = DownloadHandler.getInstance(mContext);
                        if (downloadhandler != null)
                        {
                            downloadhandler.delDownloadTasks(avoid);
                        }
                    }
                    return Boolean.valueOf(true);
                }

                protected volatile Object doInBackground(Object aobj[])
                {
                    return doInBackground((Void[])aobj);
                }

                protected void onPostExecute(Boolean boolean1)
                {
                    while (mContext == null || ((Activity)mContext).isFinishing() || pd == null) 
                    {
                        return;
                    }
                    pd.cancel();
                    pd = null;
                }

                protected volatile void onPostExecute(Object obj)
                {
                    onPostExecute((Boolean)obj);
                }

                protected void onPreExecute()
                {
                    super.onPreExecute();
                    pd = new MyProgressDialog(mContext);
                    pd.setMessage("\u6B63\u5728\u6E05\u9664\u8BE5\u4E13\u8F91\u5217\u8868\u7684\u58F0\u97F3\uFF0C\u8BF7\u7B49\u5F85...");
                    pd.setCanceledOnTouchOutside(false);
                    class _cls1
                        implements android.content.DialogInterface.OnKeyListener
                    {

                        final _cls1 this$2;

                        public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
                        {
                            return true;
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    pd.setOnKeyListener(new _cls1());
                    pd.show();
                    List list1 = (List)albumMap.get(Long.valueOf(key));
                    PlayListControl.getPlayListManager().doBeforeDelete(list1);
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                    pd = null;
                }
            }

            (new _cls1()).myexec(new Void[0]);
        }

        _cls1()
        {
            this$0 = SoundsDownloadForAlbumAdapter.this;
            key = l;
            super();
        }
    }

}
