// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.modelnew.FocusImageModelNew;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ThirdAdStatUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FocusImageAdapter extends PagerAdapter
{

    private final String FROMMAIN;
    private final String FROMOTHER;
    private boolean isCycleScroll;
    private boolean isFindingFocusImage;
    private boolean isOnlyOnePage;
    private int localDrawableId[] = {
        0x7f020258, 0x7f020259, 0x7f02025a, 0x7f02025b, 0x7f02025c, 0x7f02025d
    };
    private Activity mActivity;
    private ArrayList mFocusImageView;
    private List mFocusImages;
    private BaseFragment mFragment;
    private String wFrom;

    public FocusImageAdapter(BaseFragment basefragment, List list)
    {
        this(basefragment, list, true);
    }

    public FocusImageAdapter(BaseFragment basefragment, List list, boolean flag)
    {
        mFocusImageView = new ArrayList();
        isFindingFocusImage = false;
        FROMMAIN = "\u4E3B\u9875\u7126\u70B9\u56FE";
        FROMOTHER = "\u7126\u70B9\u56FE";
        isCycleScroll = false;
        isOnlyOnePage = false;
        mFocusImages = list;
        mFragment = basefragment;
        mActivity = mFragment.getActivity();
        isFindingFocusImage = flag;
        buildPages();
    }

    public FocusImageAdapter(BaseFragment basefragment, List list, boolean flag, String s)
    {
        mFocusImageView = new ArrayList();
        isFindingFocusImage = false;
        FROMMAIN = "\u4E3B\u9875\u7126\u70B9\u56FE";
        FROMOTHER = "\u7126\u70B9\u56FE";
        isCycleScroll = false;
        isOnlyOnePage = false;
        wFrom = s;
        mFocusImages = list;
        mFragment = basefragment;
        mActivity = mFragment.getActivity();
        isFindingFocusImage = flag;
        buildPages();
    }

    private void buildPages()
    {
        mFocusImageView.clear();
        Object obj;
        for (Iterator iterator = mFocusImages.iterator(); iterator.hasNext(); mFocusImageView.add(obj))
        {
            obj = (FocusImageModelNew)iterator.next();
            obj = new ImageView(mActivity);
            ((ImageView) (obj)).setId(0x7f0a0046);
            ((ImageView) (obj)).setScaleType(android.widget.ImageView.ScaleType.FIT_XY);
            int i = ToolUtil.getScreenWidth(mActivity) - ToolUtil.dp2px(mActivity, 20F);
            ((ImageView) (obj)).setLayoutParams(new android.view.ViewGroup.LayoutParams(i, (int)((float)i * 0.46875F)));
        }

    }

    private void goToWeb(final FocusImageModelNew model)
    {
        ThirdAdStatUtil.getInstance().execAfterDecorateUrlNoJT(model.url, new _cls2());
    }

    public void destory()
    {
        if (mFocusImageView != null && mFocusImageView.size() > 0)
        {
            ImageView imageview;
            for (Iterator iterator = mFocusImageView.iterator(); iterator.hasNext(); imageview.setBackgroundDrawable(null))
            {
                imageview = (ImageView)iterator.next();
                iterator.remove();
                imageview.setImageDrawable(null);
            }

        }
    }

    public void destroyItem(ViewGroup viewgroup, int i, Object obj)
    {
        int j = i;
        if (mFocusImageView.size() != 0)
        {
            j = i;
            if (isCycleScroll)
            {
                j = i % mFocusImageView.size();
            }
        }
        obj = (ImageView)mFocusImageView.get(j);
        if (((ImageView) (obj)).getDrawable() != null)
        {
            ((ImageView) (obj)).getDrawable().setCallback(null);
        }
        ((ImageView) (obj)).setImageBitmap(null);
        ((ImageView) (obj)).setTag(null);
        viewgroup.removeView(((View) (obj)));
    }

    public int getCount()
    {
        if (!isOnlyOnePage && isCycleScroll)
        {
            return 0x7fffffff;
        } else
        {
            return mFocusImageView.size();
        }
    }

    public int getItemPosition(Object obj)
    {
        return -2;
    }

    public Object instantiateItem(ViewGroup viewgroup, int i)
    {
        ImageView imageview;
        final FocusImageModelNew model;
        int j;
        final int pos;
        if (mFocusImageView.size() != 0 && isCycleScroll)
        {
            pos = i % mFocusImageView.size();
            i = pos;
            j = i;
        } else
        {
            j = i;
            pos = i;
        }
        imageview = (ImageView)mFocusImageView.get(j);
        model = (FocusImageModelNew)mFocusImages.get(j);
        imageview.setTag(0x7f0a0037, Boolean.valueOf(true));
        imageview.setContentDescription(model.shortTitle);
        if ("local".equals(model.pic) && j < localDrawableId.length)
        {
            imageview.setImageResource(localDrawableId[j]);
        } else
        if (isFindingFocusImage && j < localDrawableId.length)
        {
            ImageManager2.from(mActivity).displayImage(imageview, model.pic, localDrawableId[j]);
        } else
        {
            ImageManager2.from(mActivity).displayImage(imageview, model.pic, 0x7f02025e);
        }
        imageview.setOnClickListener(new _cls1());
        if (imageview.getParent() != null && (imageview.getParent() instanceof ViewGroup))
        {
            ((ViewGroup)imageview.getParent()).removeView(imageview);
        }
        viewgroup.addView(imageview);
        return imageview;
    }

    public boolean isViewFromObject(View view, Object obj)
    {
        return view == obj;
    }

    public void notifyDataSetChanged()
    {
        buildPages();
        super.notifyDataSetChanged();
    }

    public void setCycleScrollFlag(boolean flag)
    {
        isCycleScroll = flag;
    }

    public void setOnlyOnePageFlag(boolean flag)
    {
        isOnlyOnePage = flag;
    }






    private class _cls2
        implements com.ximalaya.ting.android.util.ThirdAdStatUtil.Callback
    {

        final FocusImageAdapter this$0;
        final FocusImageModelNew val$model;

        public void execute(final String url)
        {
            if (mActivity.isFinishing())
            {
                return;
            } else
            {
                class _cls1
                    implements Runnable
                {

                    final _cls2 this$1;
                    final String val$url;

                    public void run()
                    {
                        Bundle bundle = new Bundle();
                        bundle.putString("ExtraUrl", url);
                        bundle.putInt("web_view_type", model.type);
                        ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/web/WebFragment, bundle);
                    }

                _cls1()
                {
                    this$1 = _cls2.this;
                    url = s;
                    super();
                }
                }

                mActivity.runOnUiThread(new _cls1());
                return;
            }
        }

        _cls2()
        {
            this$0 = FocusImageAdapter.this;
            model = focusimagemodelnew;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FocusImageAdapter this$0;
        final FocusImageModelNew val$model;
        final int val$pos;

        public void onClick(View view)
        {
            Object obj;
            Object obj1;
            String s;
            String s1;
            obj1 = ((FocusImageModelNew)mFocusImages.get(pos)).longTitle;
            s = (new StringBuilder()).append(pos + 1).append("").toString();
            if (isFindingFocusImage)
            {
                obj = "\u4E3B\u9875\u7126\u70B9\u56FE";
            } else
            {
                obj = (new StringBuilder()).append(wFrom).append("\u7126\u70B9\u56FE").toString();
            }
            s1 = (new StringBuilder()).append(model.id).append("").toString();
            ToolUtil.onEvent(mActivity, "Found_FocusMap");
            ((MyApplication)(MyApplication)mActivity.getApplication()).a = "play_banner";
            model.type;
            JVM INSTR tableswitch 1 10: default 184
        //                       1 214
        //                       2 319
        //                       3 452
        //                       4 552
        //                       5 693
        //                       6 824
        //                       7 948
        //                       8 1108
        //                       9 1217
        //                       10 1332;
               goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11
_L1:
            return;
_L2:
            String s2 = (new StringBuilder()).append(model.uid).append("").toString();
            DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "user", s2);
            obj = new Bundle();
            ((Bundle) (obj)).putLong("toUid", model.uid);
            ((Bundle) (obj)).putInt("from", 3);
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, ((Bundle) (obj)));
            return;
_L3:
            String s3 = (new StringBuilder()).append(model.albumId).append("").toString();
            DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "album", s3);
            obj = new Bundle();
            obj1 = new AlbumModel();
            obj1.albumId = model.albumId;
            obj1.uid = model.uid;
            ((Bundle) (obj)).putString("album", JSON.toJSONString(obj1));
            ((Bundle) (obj)).putInt("from", 7);
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj)));
            return;
_L4:
            String s4 = (new StringBuilder()).append(model.trackId).append("").toString();
            DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "track", s4);
            obj = new SoundInfo();
            obj.trackId = model.trackId;
            PlayTools.gotoPlayWithoutUrl(12, ((SoundInfo) (obj)), mActivity, true, DataCollectUtil.getDataFromView(view));
            ToolUtil.onEvent(mActivity, "play_banner");
            return;
_L5:
            if (!TextUtils.isEmpty(model.url))
            {
                String s5 = model.url;
                DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "link", s5);
                obj = new Intent(mActivity, com/ximalaya/ting/android/activity/web/WebActivityNew);
                ((Intent) (obj)).putExtra("ExtraUrl", model.url);
                ((Intent) (obj)).putExtra("show_share_btn", model.isShare);
                ((Intent) (obj)).putExtra("is_external_url", model.is_External_url);
                ((Intent) (obj)).putExtra("share_cover_path", model.pic);
                ((Intent) (obj)).putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                mActivity.startActivity(((Intent) (obj)));
                return;
            }
            continue; /* Loop/switch isn't completed */
_L6:
            String s6 = (new StringBuilder()).append(model.id).append("").toString();
            DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "userlist", s6);
            obj = new Bundle();
            ((Bundle) (obj)).putLong("id", model.id);
            ((Bundle) (obj)).putInt("type", model.type);
            ((Bundle) (obj)).putString("title", model.shortTitle);
            ((Bundle) (obj)).putInt("from", 1);
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/findings/FocusPersonListFragmentNew, ((Bundle) (obj)));
            return;
_L7:
            String s7 = (new StringBuilder()).append(model.id).append("").toString();
            DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "albumlist", s7);
            obj = new Bundle();
            ((Bundle) (obj)).putLong("id", model.id);
            ((Bundle) (obj)).putInt("type", model.type);
            ((Bundle) (obj)).putString("title", model.shortTitle);
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/findings/FocusAlbumListFragmentNew, ((Bundle) (obj)));
            return;
_L8:
            String s8 = (new StringBuilder()).append(model.id).append("").toString();
            DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "tracklist", s8);
            obj = new Bundle();
            ((Bundle) (obj)).putString("id", (new StringBuilder()).append(model.id).append("").toString());
            ((Bundle) (obj)).putString("type", (new StringBuilder()).append(model.type).append("").toString());
            ((Bundle) (obj)).putString("title", model.shortTitle);
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/findings/FocusSoundListFragment, ((Bundle) (obj)));
            return;
_L9:
            if (!TextUtils.isEmpty(model.url))
            {
                String s9 = model.url;
                DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "activity", s9);
                obj = new Bundle();
                ((Bundle) (obj)).putString("ExtraUrl", model.url);
                ((Bundle) (obj)).putInt("web_view_type", model.type);
                ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/web/WebFragment, ((Bundle) (obj)));
                return;
            }
            if (true) goto _L1; else goto _L10
_L10:
            String s10 = (new StringBuilder()).append(model.specialId).append("").toString();
            DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "subject", s10);
            obj = new Bundle();
            ((Bundle) (obj)).putLong("subjectId", model.specialId);
            ((Bundle) (obj)).putInt("contentType", model.subType);
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/subject/SubjectFragment, ((Bundle) (obj)));
            return;
_L11:
            if (!TextUtils.isEmpty(model.url))
            {
                view = model.url;
                DataCollectUtil.getInstance(mActivity).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "ad", view);
                goToWeb(model);
                return;
            }
            if (true) goto _L1; else goto _L12
_L12:
        }

        _cls1()
        {
            this$0 = FocusImageAdapter.this;
            pos = i;
            model = focusimagemodelnew;
            super();
        }
    }

}
