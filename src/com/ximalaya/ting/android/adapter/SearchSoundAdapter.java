// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.search.SearchSound;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class SearchSoundAdapter extends BaseListSoundsAdapter
{

    public SearchSoundAdapter(Activity activity, List list)
    {
        super(activity, list);
    }

    protected void bindData(Object obj, final BaseListSoundsAdapter.ViewHolder holder)
    {
        if (obj instanceof SearchSound)
        {
            final SearchSound info;
            TextView textview;
            if (holder.position + 1 == mData.size())
            {
                holder.border.setVisibility(4);
            } else
            {
                holder.border.setVisibility(0);
            }
            info = (SearchSound)obj;
            ImageManager2.from(mContext).displayImage(holder.cover, info.cover_path, 0x7f0202de);
            holder.title.setText(info.title);
            holder.owner.setText((new StringBuilder()).append("by ").append(info.nickname).toString());
            holder.createTime.setText(ToolUtil.convertTime(info.created_at));
            textview = holder.origin;
            if (info.user_source == 1)
            {
                obj = "\u539F\u521B";
            } else
            {
                obj = "\u91C7\u96C6";
            }
            textview.setText(((CharSequence) (obj)));
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.count_play));
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.count_like));
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.count_comment));
            holder.duration.setText(String.valueOf(ToolUtil.toTime(info.duration)));
            if (isPlaying(info.id))
            {
                if (LocalMediaService.getInstance().isPaused())
                {
                    holder.playFlag.setImageResource(0x7f020250);
                } else
                {
                    holder.playFlag.setImageResource(0x7f02024f);
                }
            } else
            {
                holder.playFlag.setImageResource(0x7f020250);
            }
            if (info.is_like)
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020090), null, null, null);
            } else
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020092), null, null, null);
            }
            if (isDownload(info.id))
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            } else
            {
                holder.btn.setImageResource(0x7f0201fe);
                holder.btn.setEnabled(true);
            }
            holder.btn.setOnClickListener(new _cls1());
            holder.cover.setOnClickListener(new _cls2());
        }
    }

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SearchSoundAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SearchSound val$info;

        public void onClick(View view)
        {
            DownloadTask downloadtask = new DownloadTask(ModelHelper.toSoundInfo(info));
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            view = downloadtools.goDownload(downloadtask, mContext.getApplicationContext(), view);
            downloadtools.release();
            if (view == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls1()
        {
            this$0 = SearchSoundAdapter.this;
            info = searchsound;
            holder = viewholder;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final SearchSoundAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SearchSound val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, mData.indexOf(info), ModelHelper.toSoundInfo(info), ModelHelper.searchSoundToSoundInfoList(Arrays.asList(mData.toArray(new SearchSound[0]))));
        }

        _cls2()
        {
            this$0 = SearchSoundAdapter.this;
            holder = viewholder;
            info = searchsound;
            super();
        }
    }

}
