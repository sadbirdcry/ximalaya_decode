// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.feed;

import android.app.Activity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.adapter.BaseListSoundsAdapter;
import com.ximalaya.ting.android.model.feed2.FeedSoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

public class FeedSoundAdapter2 extends BaseListSoundsAdapter
{

    private int newCount;

    public FeedSoundAdapter2(Activity activity, List list)
    {
        super(activity, list);
    }

    public FeedSoundAdapter2(Activity activity, List list, int i)
    {
        super(activity, list);
        newCount = i;
    }

    protected void bindData(final FeedSoundInfo info, final com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder holder)
    {
        b.a().a(ModelHelper.toSoundInfo(info));
        String s;
        TextView textview;
        if (isPlaying(info.trackId))
        {
            if (LocalMediaService.getInstance().isPaused() || LocalMediaService.getInstance().isPlayCompleted())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        if (info.playsCounts > 0L)
        {
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.playsCounts));
            holder.playCount.setVisibility(0);
        } else
        {
            holder.playCount.setText("0");
            holder.playCount.setVisibility(8);
        }
        if (info.favoritesCounts > 0L)
        {
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.favoritesCounts));
            holder.likeCount.setVisibility(0);
        } else
        {
            holder.likeCount.setVisibility(8);
        }
        holder.commentCount.setVisibility(8);
        textview = holder.origin;
        if (info.userSource == 1)
        {
            s = "\u539F\u521B";
        } else
        {
            s = "\u91C7\u96C6";
        }
        textview.setText(s);
        if (info.duration > 0.0D)
        {
            holder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)info.duration)).toString());
            holder.duration.setVisibility(0);
        } else
        {
            holder.duration.setVisibility(8);
        }
        if (info.sharesCounts > 0L)
        {
            holder.transmitCount.setText(StringUtil.getFriendlyNumStr(info.sharesCounts));
            holder.transmitCount.setVisibility(0);
        } else
        {
            holder.transmitCount.setVisibility(8);
        }
        holder.cover.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(holder.cover, info.coverSmall, 0x7f0202de);
        textview = holder.title;
        if (info.title == null)
        {
            s = "";
        } else
        {
            s = info.title;
        }
        textview.setText(s);
        textview = holder.owner;
        if (info.nickname == null)
        {
            s = "";
        } else
        {
            s = (new StringBuilder()).append("by ").append(info.nickname).toString();
        }
        textview.setText(s);
        if (isDownload(info.trackId))
        {
            holder.btn.setImageResource(0x7f0200f1);
            holder.btn.setEnabled(false);
        } else
        {
            holder.btn.setImageResource(0x7f0201fe);
            holder.btn.setEnabled(true);
        }
        if (newCount >= holder.position + 1)
        {
            holder.newUpdate.setVisibility(0);
        } else
        {
            holder.newUpdate.setVisibility(8);
        }
        holder.createTime.setText(ToolUtil.convertTime(info.createdAt));
        holder.btn.setOnClickListener(new _cls1());
        holder.cover.setOnClickListener(new _cls2());
        if (holder.position + 1 == mData.size())
        {
            holder.border.setVisibility(8);
            return;
        } else
        {
            holder.border.setVisibility(0);
            return;
        }
    }

    protected volatile void bindData(Object obj, com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((FeedSoundInfo)obj, viewholder);
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FeedSoundAdapter2 this$0;
        final com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder val$holder;
        final FeedSoundInfo val$info;

        public void onClick(View view)
        {
            DownloadTask downloadtask = new DownloadTask(ModelHelper.toSoundInfo(info));
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            view = downloadtools.goDownload(downloadtask, mContext.getApplicationContext(), view);
            downloadtools.release();
            if (view == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls1()
        {
            this$0 = FeedSoundAdapter2.this;
            info = feedsoundinfo;
            holder = viewholder;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final FeedSoundAdapter2 this$0;
        final com.ximalaya.ting.android.adapter.BaseListSoundsAdapter.ViewHolder val$holder;
        final FeedSoundInfo val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, 
// JavaClassFileOutputException: get_constant: invalid tag

        _cls2()
        {
            this$0 = FeedSoundAdapter2.this;
            holder = viewholder;
            info = feedsoundinfo;
            super();
        }
    }

}
