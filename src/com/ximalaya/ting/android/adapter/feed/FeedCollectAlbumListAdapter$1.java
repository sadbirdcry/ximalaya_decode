// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.feed;

import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.library.util.Logger;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.adapter.feed:
//            FeedCollectAlbumListAdapter

class val.model extends a
{

    final FeedCollectAlbumListAdapter this$0;
    final ewHolder val$holder;
    final AlbumModel val$model;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$holder.collectBtnLayout);
    }

    public void onNetError(int i, String s)
    {
        FeedCollectAlbumListAdapter.access$100(FeedCollectAlbumListAdapter.this, false);
        FeedCollectAlbumListAdapter.access$200(FeedCollectAlbumListAdapter.this).showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
        if (((AlbumModel)val$holder.collectBtnLayout.getTag(0x7f090000)).albumId == val$model.albumId)
        {
            FeedCollectAlbumListAdapter.setCollectStatus(val$holder, val$model.isFavorite);
        }
    }

    public void onSuccess(String s)
    {
        boolean flag = true;
        if (FeedCollectAlbumListAdapter.access$200(FeedCollectAlbumListAdapter.this) != null && FeedCollectAlbumListAdapter.access$200(FeedCollectAlbumListAdapter.this).canGoon())
        {
            FeedCollectAlbumListAdapter.access$100(FeedCollectAlbumListAdapter.this, false);
            if (TextUtils.isEmpty(s))
            {
                FeedCollectAlbumListAdapter.access$200(FeedCollectAlbumListAdapter.this).showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
                return;
            }
            Object obj = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                s = obj;
            }
            if (s != null)
            {
                int i = s.getIntValue("ret");
                if (i == 0)
                {
                    s = val$model;
                    if (val$model.isFavorite)
                    {
                        flag = false;
                    }
                    s.isFavorite = flag;
                    if (((AlbumModel)val$holder.collectBtnLayout.getTag(0x7f090000)).albumId == val$model.albumId)
                    {
                        FeedCollectAlbumListAdapter.setCollectStatus(val$holder, val$model.isFavorite);
                    }
                    if (val$model.isFavorite)
                    {
                        s = "\u6536\u85CF\u6210\u529F\uFF01";
                    } else
                    {
                        s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
                    }
                    FeedCollectAlbumListAdapter.access$200(FeedCollectAlbumListAdapter.this).showToast(s);
                    return;
                }
                if (i == 791)
                {
                    val$model.isFavorite = true;
                }
                if (((AlbumModel)val$holder.collectBtnLayout.getTag(0x7f090000)).albumId == val$model.albumId)
                {
                    FeedCollectAlbumListAdapter.setCollectStatus(val$holder, val$model.isFavorite);
                }
                if (s.getString("msg") == null)
                {
                    s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
                } else
                {
                    s = s.getString("msg");
                }
                FeedCollectAlbumListAdapter.access$200(FeedCollectAlbumListAdapter.this).showToast(s);
                return;
            }
        }
    }

    ewHolder()
    {
        this$0 = final_feedcollectalbumlistadapter;
        val$holder = ewholder;
        val$model = AlbumModel.this;
        super();
    }
}
