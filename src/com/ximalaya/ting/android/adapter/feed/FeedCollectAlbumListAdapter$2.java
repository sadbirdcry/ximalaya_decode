// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.feed;

import android.view.View;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.util.MyAsyncTask;

// Referenced classes of package com.ximalaya.ting.android.adapter.feed:
//            FeedCollectAlbumListAdapter

class ewHolder extends MyAsyncTask
{

    final FeedCollectAlbumListAdapter this$0;
    final ewHolder val$holder;
    final AlbumModel val$model;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        avoid = AlbumModelManage.getInstance();
        AlbumModel albummodel = val$model;
        boolean flag;
        if (!val$model.isFavorite)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        albummodel.isFavorite = flag;
        if (!val$model.isFavorite)
        {
            avoid.deleteAlbumInLocalAlbumList(val$model);
        } else
        {
            val$model.isRecommend = false;
            avoid.saveAlbumModel(val$model);
            val$model.isRecommend = true;
        }
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        if (FeedCollectAlbumListAdapter.access$200(FeedCollectAlbumListAdapter.this) == null || !FeedCollectAlbumListAdapter.access$200(FeedCollectAlbumListAdapter.this).canGoon())
        {
            return;
        }
        if (((AlbumModel)val$holder.collectBtnLayout.getTag(0x7f090000)).albumId == val$model.albumId)
        {
            FeedCollectAlbumListAdapter.setCollectStatus(val$holder, val$model.isFavorite);
        }
        if (val$model.isFavorite)
        {
            void1 = "\u6536\u85CF\u6210\u529F\uFF01";
        } else
        {
            void1 = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
        }
        FeedCollectAlbumListAdapter.access$200(FeedCollectAlbumListAdapter.this).showToast(void1);
    }

    ewHolder()
    {
        this$0 = final_feedcollectalbumlistadapter;
        val$model = albummodel;
        val$holder = ewHolder.this;
        super();
    }
}
