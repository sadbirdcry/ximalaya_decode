// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.feed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.feed2.FeedRecycleModel;
import com.ximalaya.ting.android.util.ImageManager2;
import java.util.List;

public class FeedRecycleAdapter extends BaseAdapter
{
    public static interface Callback
    {

        public abstract void recycleFeed(FeedRecycleModel feedrecyclemodel, View view);
    }

    private class ViewHolder
    {

        ImageView albumCoverIv;
        TextView albumNameTv;
        View border;
        TextView recycleActionTv;
        final FeedRecycleAdapter this$0;

        private ViewHolder()
        {
            this$0 = FeedRecycleAdapter.this;
            super();
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private Callback callback;
    private Context mCon;
    private List mDataList;

    public FeedRecycleAdapter(Context context, List list, Callback callback1)
    {
        mCon = context;
        mDataList = list;
        callback = callback1;
    }

    public int getCount()
    {
        return mDataList.size();
    }

    public Object getItem(int i)
    {
        return mDataList.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        final FeedRecycleModel mData;
        if (view == null)
        {
            ViewHolder viewholder = new ViewHolder(null);
            view = LayoutInflater.from(mCon).inflate(0x7f030085, viewgroup, false);
            viewholder.albumCoverIv = (ImageView)view.findViewById(0x7f0a0023);
            viewholder.albumCoverIv.setTag(0x7f0a0037, Boolean.valueOf(true));
            viewholder.albumNameTv = (TextView)view.findViewById(0x7f0a00ea);
            viewholder.recycleActionTv = (TextView)view.findViewById(0x7f0a0260);
            viewholder.border = view.findViewById(0x7f0a00a8);
            view.setTag(viewholder);
            viewgroup = viewholder;
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        mData = (FeedRecycleModel)mDataList.get(i);
        ImageManager2.from(mCon).displayImage(((ViewHolder) (viewgroup)).albumCoverIv, mData.albumCover, 0x7f0202e0);
        ((ViewHolder) (viewgroup)).albumNameTv.setText(mData.albumTitle);
        ((ViewHolder) (viewgroup)).recycleActionTv.setOnClickListener(new _cls1());
        if (i + 1 == mDataList.size())
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(8);
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(0);
            return view;
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FeedRecycleAdapter this$0;
        final FeedRecycleModel val$mData;

        public void onClick(View view)
        {
            if (callback != null)
            {
                callback.recycleFeed(mData, view);
            }
        }

        _cls1()
        {
            this$0 = FeedRecycleAdapter.this;
            mData = feedrecyclemodel;
            super();
        }
    }

}
