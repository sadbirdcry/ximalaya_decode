// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.feed;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.model.feed2.FeedRecommendModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ViewUtil;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.util.ArrayList;
import java.util.List;

public class FeedRecommendAdapter extends BaseAdapter
{
    public static interface FeedRecommendActionHandler
    {

        public abstract void doRegAndFollow(long l, long l1, View view);

        public abstract void goHostHomePage(long l, long l1, View view);

        public abstract void goToPlay(int i, View view);
    }

    private static class ViewHolder
    {

        ImageView adIndicator;
        ImageView albumCoverIv;
        RelativeLayout albumLayout;
        TextView albumNameTv;
        LinearLayout followActionLayout;
        RoundedImageView hostIconIv;
        TextView hostInfoTv;
        RelativeLayout hostLayout;
        TextView hostNameTv;
        ImageView recommendImg;
        TextView soundTitleTv;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private FeedRecommendActionHandler actionListener;
    private Context mCon;
    private List mDataList;

    public FeedRecommendAdapter(Context context, List list, FeedRecommendActionHandler feedrecommendactionhandler)
    {
        mDataList = new ArrayList();
        mCon = context;
        mDataList = list;
        if (feedrecommendactionhandler instanceof FeedRecommendActionHandler)
        {
            actionListener = feedrecommendactionhandler;
        }
    }

    public int getCount()
    {
        return mDataList.size();
    }

    public Object getItem(int i)
    {
        return mDataList.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
        View view1;
        Object obj;
        final FeedRecommendModel data;
        TextView textview;
        if (view == null)
        {
            view = new ViewHolder(null);
            view1 = LayoutInflater.from(mCon).inflate(0x7f030082, viewgroup, false);
            view.albumCoverIv = (ImageView)view1.findViewById(0x7f0a0023);
            ((ViewHolder) (view)).albumCoverIv.setTag(0x7f0a0037, Boolean.valueOf(true));
            view.albumNameTv = (TextView)view1.findViewById(0x7f0a00ea);
            view.soundTitleTv = (TextView)view1.findViewById(0x7f0a00e6);
            view.hostIconIv = (RoundedImageView)view1.findViewById(0x7f0a023c);
            ((ViewHolder) (view)).hostIconIv.setTag(0x7f0a0037, Boolean.valueOf(true));
            view.hostNameTv = (TextView)view1.findViewById(0x7f0a023d);
            view.hostInfoTv = (TextView)view1.findViewById(0x7f0a025b);
            view.followActionLayout = (LinearLayout)view1.findViewById(0x7f0a025a);
            view.albumLayout = (RelativeLayout)view1.findViewById(0x7f0a024f);
            view.hostLayout = (RelativeLayout)view1.findViewById(0x7f0a0259);
            view.adIndicator = (ImageView)view1.findViewById(0x7f0a0255);
            view.recommendImg = (ImageView)view1.findViewById(0x7f0a0258);
            view1.setTag(view);
            viewgroup = view;
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
            view1 = view;
        }
        data = (FeedRecommendModel)mDataList.get(position);
        if (data.type == 1)
        {
            ((ViewHolder) (viewgroup)).adIndicator.setVisibility(0);
            ((ViewHolder) (viewgroup)).followActionLayout.setVisibility(8);
            ((ViewHolder) (viewgroup)).recommendImg.setVisibility(8);
            ((ViewHolder) (viewgroup)).hostLayout.setVisibility(8);
        } else
        {
            ((ViewHolder) (viewgroup)).adIndicator.setVisibility(8);
            ((ViewHolder) (viewgroup)).followActionLayout.setVisibility(0);
            ((ViewHolder) (viewgroup)).recommendImg.setVisibility(0);
            ((ViewHolder) (viewgroup)).hostLayout.setVisibility(0);
        }
        ImageManager2.from(mCon).displayImage(((ViewHolder) (viewgroup)).albumCoverIv, data.albumCover, 0x7f0202e0);
        obj = ((ViewHolder) (viewgroup)).albumNameTv;
        if (data.albumTitle == null)
        {
            view = "";
        } else
        {
            view = data.albumTitle;
        }
        ((TextView) (obj)).setText(view);
        if (data.trackTitle == null)
        {
            view = "";
        } else
        {
            view = data.trackTitle;
        }
        textview = ((ViewHolder) (viewgroup)).soundTitleTv;
        obj = view;
        if (data.trackType == 2)
        {
            obj = (new StringBuilder()).append("\u3010\u8F6C\u91C7\u3011").append(view).toString();
        }
        textview.setText(((CharSequence) (obj)));
        ImageManager2.from(mCon).displayImage(((ViewHolder) (viewgroup)).hostIconIv, data.avatar, 0x7f0202df);
        obj = ((ViewHolder) (viewgroup)).hostNameTv;
        if (data.nickname == null)
        {
            view = "";
        } else
        {
            view = data.nickname;
        }
        ((TextView) (obj)).setText(view);
        obj = ((ViewHolder) (viewgroup)).hostInfoTv;
        if (data.personalSignature == null)
        {
            view = "";
        } else
        {
            view = data.personalSignature;
        }
        ((TextView) (obj)).setText(view);
        ViewUtil.expandClickArea(mCon, ((ViewHolder) (viewgroup)).followActionLayout, 15, 10, 10, 10);
        ((ViewHolder) (viewgroup)).albumLayout.setOnClickListener(new _cls1());
        ((ViewHolder) (viewgroup)).hostLayout.setOnClickListener(new _cls2());
        ((ViewHolder) (viewgroup)).followActionLayout.setOnClickListener(new _cls3());
        return view1;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FeedRecommendAdapter this$0;
        final int val$position;

        public void onClick(View view)
        {
            if (actionListener != null)
            {
                actionListener.goToPlay(position, view);
            }
        }

        _cls1()
        {
            this$0 = FeedRecommendAdapter.this;
            position = i;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final FeedRecommendAdapter this$0;
        final FeedRecommendModel val$data;

        public void onClick(View view)
        {
            if (actionListener != null)
            {
                actionListener.goHostHomePage(data.uid, data.albumId, view);
            }
        }

        _cls2()
        {
            this$0 = FeedRecommendAdapter.this;
            data = feedrecommendmodel;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final FeedRecommendAdapter this$0;
        final FeedRecommendModel val$data;

        public void onClick(View view)
        {
            if (actionListener != null)
            {
                actionListener.doRegAndFollow(data.uid, data.albumId, view);
            }
        }

        _cls3()
        {
            this$0 = FeedRecommendAdapter.this;
            data = feedrecommendmodel;
            super();
        }
    }

}
