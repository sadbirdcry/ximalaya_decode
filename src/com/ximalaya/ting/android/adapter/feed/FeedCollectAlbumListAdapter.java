// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.feed;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.ting.CollectFragment;
import com.ximalaya.ting.android.fragment.ting.feed.FeedCollectFragment;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

public class FeedCollectAlbumListAdapter extends BaseAdapter
    implements android.view.View.OnClickListener
{
    static class AdViewHolder
    {

        ImageView cover;
        TextView description;
        View divider;
        TextView name;

        AdViewHolder()
        {
        }
    }

    private static class ViewHolder
    {

        ImageView albumImageImageView;
        TextView albumNameTextView;
        TextView albumUpdateTextView;
        View border;
        View collectBtnLayout;
        public TextView collectTxt;
        View content;
        ImageView delBtnIv;
        View img_update;
        AlbumModel info;
        int position;
        TextView soundTitleTextView;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private static final int TYPE_AD = 1;
    private static final int TYPE_OTHER = 0;
    public List list;
    private Context mContext;
    private BaseFragment mFragment;

    public FeedCollectAlbumListAdapter(Context context, List list1, BaseFragment basefragment)
    {
        mContext = context;
        list = list1;
        mFragment = basefragment;
    }

    private void doAbaddonCollect(final AlbumModel info, final int position, final View view)
    {
        (new DialogBuilder(mContext)).setMessage("\u786E\u5B9A\u8981\u53D6\u6D88\u6536\u85CF\uFF1F").setOkBtn(new _cls3()).showConfirm();
    }

    private void doCollect(final AlbumModel model, final ViewHolder holder)
    {
        ToolUtil.onEvent(mContext, "Collection_Recommend_Follow");
        if (UserInfoMannage.hasLogined())
        {
            String s;
            RequestParams requestparams;
            if (model.isFavorite)
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.albumId).toString());
            showLoading(true);
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(holder.collectBtnLayout), new _cls1());
        } else
        if (AlbumModelManage.getInstance().ensureLocalCollectAllow(mContext, model, holder.collectBtnLayout))
        {
            (new _cls2()).myexec(new Void[0]);
            return;
        }
    }

    private void effectOtherCollect()
    {
        FeedCollectFragment.isNeedRefresh = true;
    }

    private void favoriteAlbum(final AlbumModel album, final int position, final View view)
    {
        if (UserInfoMannage.hasLogined())
        {
            if (ToolUtil.isConnectToNetwork(mContext))
            {
                (new _cls4()).myexec(new Void[0]);
                return;
            } else
            {
                mFragment.showToast("\u7F51\u7EDC\u672A\u8FDE\u63A5");
                return;
            }
        } else
        {
            (new _cls5()).myexec(new Void[0]);
            return;
        }
    }

    private View getAlbumView(int i, View view, ViewGroup viewgroup)
    {
        int j = 10;
        Object obj;
        AlbumModel albummodel;
        TextView textview;
        long l;
        if (isAlbumView(view))
        {
            viewgroup = new ViewHolder(null);
            obj = new RelativeLayout(mContext);
            LayoutInflater.from(mContext).inflate(0x7f03007d, (RelativeLayout)obj, true);
            viewgroup.content = ((View) (obj)).findViewById(0x7f0a0233);
            viewgroup.albumImageImageView = (ImageView)((View) (obj)).findViewById(0x7f0a0023);
            ((ViewHolder) (viewgroup)).albumImageImageView.setTag(0x7f0a0037, Boolean.valueOf(true));
            viewgroup.albumNameTextView = (TextView)((View) (obj)).findViewById(0x7f0a00ea);
            viewgroup.collectBtnLayout = ((View) (obj)).findViewById(0x7f0a0154);
            viewgroup.collectTxt = (TextView)((View) (obj)).findViewById(0x7f0a0155);
            viewgroup.soundTitleTextView = (TextView)((View) (obj)).findViewById(0x7f0a0234);
            viewgroup.albumUpdateTextView = (TextView)((View) (obj)).findViewById(0x7f0a014f);
            viewgroup.img_update = ((View) (obj)).findViewById(0x7f0a0156);
            ((ViewHolder) (viewgroup)).collectBtnLayout.setOnClickListener(this);
            viewgroup.delBtnIv = (ImageView)((View) (obj)).findViewById(0x7f0a0236);
            ((ViewHolder) (viewgroup)).delBtnIv.setOnClickListener(this);
            viewgroup.border = ((View) (obj)).findViewById(0x7f0a00a8);
            ((View) (obj)).setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
            obj = view;
        }
        ViewUtil.expandClickArea(mContext, ((ViewHolder) (viewgroup)).collectBtnLayout, 15, 20, 10, 10);
        albummodel = getItem(i);
        textview = ((ViewHolder) (viewgroup)).albumNameTextView;
        if (albummodel.title == null)
        {
            view = "";
        } else
        {
            view = albummodel.title;
        }
        textview.setText(view);
        if (mContext instanceof MainTabActivity2)
        {
            ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).albumImageImageView, albummodel.coverSmall, 0x7f0202e0, true);
        } else
        {
            ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).albumImageImageView, albummodel.coverSmall, 0x7f0202e0);
        }
        if (albummodel.isRecommend)
        {
            TextView textview1 = ((ViewHolder) (viewgroup)).soundTitleTextView;
            if (albummodel.lastUptrackTitle == null)
            {
                if (albummodel.nickname == null)
                {
                    view = "";
                } else
                {
                    view = (new StringBuilder()).append("by ").append(albummodel.nickname).toString();
                }
            } else
            {
                view = albummodel.lastUptrackTitle;
            }
            textview1.setText(view);
        } else
        {
            TextView textview2 = ((ViewHolder) (viewgroup)).soundTitleTextView;
            if (albummodel.nickname == null)
            {
                view = "";
            } else
            {
                view = (new StringBuilder()).append("by ").append(albummodel.nickname).toString();
            }
            textview2.setText(view);
        }
        if (albummodel.lastUptrackAt == 0L)
        {
            l = albummodel.updatedAt;
        } else
        {
            l = albummodel.lastUptrackAt;
        }
        if (l == 0L)
        {
            ((ViewHolder) (viewgroup)).albumUpdateTextView.setVisibility(4);
        } else
        {
            if (((ViewHolder) (viewgroup)).albumUpdateTextView.getVisibility() == 4)
            {
                ((ViewHolder) (viewgroup)).albumUpdateTextView.setVisibility(0);
            }
            ((ViewHolder) (viewgroup)).albumUpdateTextView.setText((new StringBuilder()).append("\u6700\u540E\u66F4\u65B0  ").append(ToolUtil.convertTime(l)).toString());
        }
        viewgroup.info = albummodel;
        viewgroup.position = i;
        if (albummodel.hasNew)
        {
            ((ViewHolder) (viewgroup)).img_update.setVisibility(0);
        } else
        {
            ((ViewHolder) (viewgroup)).img_update.setVisibility(8);
        }
        ((ViewHolder) (viewgroup)).collectBtnLayout.setTag(viewgroup);
        ((ViewHolder) (viewgroup)).collectBtnLayout.setTag(0x7f090000, albummodel);
        ((ViewHolder) (viewgroup)).delBtnIv.setTag(viewgroup);
        if (albummodel.isRecommend)
        {
            ((ViewHolder) (viewgroup)).collectBtnLayout.setVisibility(0);
            ((ViewHolder) (viewgroup)).delBtnIv.setVisibility(8);
        } else
        {
            ((ViewHolder) (viewgroup)).collectBtnLayout.setVisibility(8);
            ((ViewHolder) (viewgroup)).delBtnIv.setVisibility(0);
        }
        setCollectStatus(viewgroup, albummodel.isFavorite);
        ((View) (obj)).setId(0x7f0a0047);
        if (i + 1 == list.size() || nextIsTitleView(i))
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(8);
        } else
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(0);
            j = 8;
        }
        if (i == 0 || preIsTitleView(i))
        {
            i = 2;
        } else
        {
            i = 0;
        }
        if (mContext != null)
        {
            ((ViewHolder) (viewgroup)).content.setPadding(0, Utilities.dip2px(mContext, i), 0, Utilities.dip2px(mContext, j));
        }
        return ((View) (obj));
    }

    private boolean isAlbumView(View view)
    {
        while (view == null || !(view.getTag() instanceof ViewHolder)) 
        {
            return true;
        }
        return false;
    }

    private boolean nextIsTitleView(int i)
    {
        if (i + 2 <= list.size())
        {
            return ((AlbumModel)list.get(i + 1)).isRecommendTitle;
        } else
        {
            return false;
        }
    }

    private boolean preIsTitleView(int i)
    {
        if (i - 1 >= 0)
        {
            return ((AlbumModel)list.get(i - 1)).isRecommendTitle;
        } else
        {
            return false;
        }
    }

    public static void setCollectStatus(ViewHolder viewholder, boolean flag)
    {
        if (flag)
        {
            viewholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ee, 0, 0);
            viewholder.collectTxt.setText("\u5DF2\u6536\u85CF");
            viewholder.collectTxt.setTextColor(Color.parseColor("#999999"));
            return;
        } else
        {
            viewholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ed, 0, 0);
            viewholder.collectTxt.setText("\u6536\u85CF");
            viewholder.collectTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    private void showEmpty()
    {
        if (mFragment instanceof CollectFragment)
        {
            ((CollectFragment)mFragment).showEmptyView(true);
        } else
        if (mFragment instanceof FeedCollectFragment)
        {
            ((FeedCollectFragment)mFragment).onRefresh();
            return;
        }
    }

    private void showLoading(boolean flag)
    {
        if (mFragment instanceof FeedCollectFragment)
        {
            ((FeedCollectFragment)mFragment).showLoading(flag);
        } else
        if (mFragment instanceof CollectFragment)
        {
            ((CollectFragment)mFragment).showLoading(flag);
            return;
        }
    }

    public int getCount()
    {
        if (list == null)
        {
            return 0;
        } else
        {
            return list.size();
        }
    }

    public AlbumModel getItem(int i)
    {
        if (list == null || i >= list.size())
        {
            return new AlbumModel();
        } else
        {
            return (AlbumModel)list.get(i);
        }
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public int getItemViewType(int i)
    {
        return ((AlbumModel)list.get(i)).type != 1 ? 0 : 1;
    }

    public View getRecommendTitleView(int i, View view, ViewGroup viewgroup)
    {
        View view1;
label0:
        {
            if (view != null)
            {
                view1 = view;
                if ("recommendTitle".equals(view.getTag()))
                {
                    break label0;
                }
            }
            view1 = LayoutInflater.from(mContext).inflate(0x7f030083, viewgroup, false);
        }
        if (mContext != null)
        {
            view1.setPadding(0, Utilities.dip2px(mContext, 10F), 0, Utilities.dip2px(mContext, 10F));
        }
        ((TextView)view1.findViewById(0x7f0a0158)).setText("\u63A8\u8350\u6536\u85CF");
        view1.setTag("recommendTitle");
        return view1;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        Object obj = (AlbumModel)list.get(i);
        if (getItemViewType(i) == 1)
        {
            TextView textview;
            if (view == null || !(view.getTag() instanceof AdViewHolder))
            {
                view = LayoutInflater.from(mContext).inflate(0x7f030122, viewgroup, false);
                viewgroup = new AdViewHolder();
                viewgroup.name = (TextView)view.findViewById(0x7f0a0449);
                viewgroup.description = (TextView)view.findViewById(0x7f0a048c);
                viewgroup.cover = (ImageView)view.findViewById(0x7f0a0177);
                viewgroup.divider = view.findViewById(0x7f0a00a8);
                view.setTag(viewgroup);
            } else
            {
                viewgroup = (AdViewHolder)view.getTag();
            }
            ImageManager2.from(mContext).displayImage(((AdViewHolder) (viewgroup)).cover, ((AlbumModel) (obj)).coverSmall, 0x7f0202e0);
            ((AdViewHolder) (viewgroup)).name.setText(((AlbumModel) (obj)).title);
            textview = ((AdViewHolder) (viewgroup)).description;
            if (((AlbumModel) (obj)).nickname == null)
            {
                obj = "";
            } else
            {
                obj = ((AlbumModel) (obj)).nickname.trim();
            }
            textview.setText(((CharSequence) (obj)));
            if (i == getCount() - 1)
            {
                ((AdViewHolder) (viewgroup)).divider.setVisibility(4);
                return view;
            } else
            {
                ((AdViewHolder) (viewgroup)).divider.setVisibility(0);
                return view;
            }
        }
        if (((AlbumModel) (obj)).isRecommendTitle)
        {
            return getRecommendTitleView(i, view, viewgroup);
        } else
        {
            return getAlbumView(i, view, viewgroup);
        }
    }

    public int getViewTypeCount()
    {
        return 2;
    }

    public void onClick(View view)
    {
        ViewHolder viewholder = (ViewHolder)view.getTag();
        AlbumModel albummodel = viewholder.info;
        int i = viewholder.position;
        int j = view.getId();
        if (j == 0x7f0a0154)
        {
            doCollect(albummodel, viewholder);
        } else
        if (j == 0x7f0a0236)
        {
            doAbaddonCollect(albummodel, i, view);
            return;
        }
    }






    private class _cls3
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final FeedCollectAlbumListAdapter this$0;
        final AlbumModel val$info;
        final int val$position;
        final View val$view;

        public void onExecute()
        {
            favoriteAlbum(info, position, view);
        }

        _cls3()
        {
            this$0 = FeedCollectAlbumListAdapter.this;
            info = albummodel;
            position = i;
            view = view1;
            super();
        }
    }


    private class _cls1 extends a
    {

        final FeedCollectAlbumListAdapter this$0;
        final ViewHolder val$holder;
        final AlbumModel val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, holder.collectBtnLayout);
        }

        public void onNetError(int i, String s)
        {
            showLoading(false);
            mFragment.showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
            if (((AlbumModel)holder.collectBtnLayout.getTag(0x7f090000)).albumId == model.albumId)
            {
                FeedCollectAlbumListAdapter.setCollectStatus(holder, model.isFavorite);
            }
        }

        public void onSuccess(String s)
        {
            boolean flag = true;
            if (mFragment != null && mFragment.canGoon())
            {
                showLoading(false);
                if (TextUtils.isEmpty(s))
                {
                    mFragment.showToast("\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01");
                    return;
                }
                Object obj = null;
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
                    s = obj;
                }
                if (s != null)
                {
                    int i = s.getIntValue("ret");
                    if (i == 0)
                    {
                        s = model;
                        if (model.isFavorite)
                        {
                            flag = false;
                        }
                        s.isFavorite = flag;
                        if (((AlbumModel)holder.collectBtnLayout.getTag(0x7f090000)).albumId == model.albumId)
                        {
                            FeedCollectAlbumListAdapter.setCollectStatus(holder, model.isFavorite);
                        }
                        if (model.isFavorite)
                        {
                            s = "\u6536\u85CF\u6210\u529F\uFF01";
                        } else
                        {
                            s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
                        }
                        mFragment.showToast(s);
                        return;
                    }
                    if (i == 791)
                    {
                        model.isFavorite = true;
                    }
                    if (((AlbumModel)holder.collectBtnLayout.getTag(0x7f090000)).albumId == model.albumId)
                    {
                        FeedCollectAlbumListAdapter.setCollectStatus(holder, model.isFavorite);
                    }
                    if (s.getString("msg") == null)
                    {
                        s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
                    } else
                    {
                        s = s.getString("msg");
                    }
                    mFragment.showToast(s);
                    return;
                }
            }
        }

        _cls1()
        {
            this$0 = FeedCollectAlbumListAdapter.this;
            holder = viewholder;
            model = albummodel;
            super();
        }
    }


    private class _cls2 extends MyAsyncTask
    {

        final FeedCollectAlbumListAdapter this$0;
        final ViewHolder val$holder;
        final AlbumModel val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            AlbumModel albummodel = model;
            boolean flag;
            if (!model.isFavorite)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            albummodel.isFavorite = flag;
            if (!model.isFavorite)
            {
                avoid.deleteAlbumInLocalAlbumList(model);
            } else
            {
                model.isRecommend = false;
                avoid.saveAlbumModel(model);
                model.isRecommend = true;
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            if (mFragment == null || !mFragment.canGoon())
            {
                return;
            }
            if (((AlbumModel)holder.collectBtnLayout.getTag(0x7f090000)).albumId == model.albumId)
            {
                FeedCollectAlbumListAdapter.setCollectStatus(holder, model.isFavorite);
            }
            if (model.isFavorite)
            {
                void1 = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                void1 = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            mFragment.showToast(void1);
        }

        _cls2()
        {
            this$0 = FeedCollectAlbumListAdapter.this;
            model = albummodel;
            holder = viewholder;
            super();
        }
    }


    private class _cls4 extends MyAsyncTask
    {

        final FeedCollectAlbumListAdapter this$0;
        final AlbumModel val$album;
        final int val$position;
        final View val$view;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            Object obj;
            avoid = ApiUtil.getApiHost();
            avoid = (new StringBuilder()).append(avoid).append("mobile/album/favorite/destroy").toString();
            obj = new RequestParams();
            ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append("").append(album.albumId).toString());
            obj = f.a().b(avoid, ((RequestParams) (obj)), view, view);
            if (obj == null)
            {
                break MISSING_BLOCK_LABEL_127;
            }
            avoid = null;
            obj = JSON.parseObject(((String) (obj)));
            avoid = ((Void []) (obj));
_L1:
            Exception exception;
            if (avoid != null)
            {
                if (avoid.getInteger("ret").intValue() == 0)
                {
                    return "0";
                } else
                {
                    return avoid.getString("msg");
                }
            }
            break MISSING_BLOCK_LABEL_127;
            exception;
            exception.printStackTrace();
              goto _L1
            return "\u7F51\u7EDC\u8BBF\u95EE\u5F02\u5E38";
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if (mFragment == null || !mFragment.canGoon())
            {
                return;
            }
            showLoading(false);
            notifyDataSetChanged();
            if (getCount() == 0)
            {
                showEmpty();
            }
            if (!"0".equals(s))
            {
                mFragment.showToast(s);
                return;
            } else
            {
                effectOtherCollect();
                return;
            }
        }

        protected void onPreExecute()
        {
            if (mFragment != null && mFragment.canGoon())
            {
                showLoading(true);
                if (list != null && position < list.size())
                {
                    list.remove(position);
                    return;
                }
            }
        }

        _cls4()
        {
            this$0 = FeedCollectAlbumListAdapter.this;
            position = i;
            album = albummodel;
            view = view1;
            super();
        }
    }


    private class _cls5 extends MyAsyncTask
    {

        final FeedCollectAlbumListAdapter this$0;
        final AlbumModel val$album;
        final int val$position;

        protected transient Boolean doInBackground(Void avoid[])
        {
            boolean flag;
            try
            {
                flag = AlbumModelManage.getInstance().deleteAlbumInLocalAlbumList(album);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[])
            {
                avoid.printStackTrace();
                return Boolean.valueOf(false);
            }
            return Boolean.valueOf(flag);
        }

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected void onPostExecute(Boolean boolean1)
        {
            if (mFragment == null || !mFragment.canGoon())
            {
                return;
            }
            showLoading(false);
            if (boolean1.booleanValue() && list != null && position < list.size())
            {
                list.remove(position);
                notifyDataSetChanged();
            }
            if (getCount() == 0)
            {
                showEmpty();
            }
            effectOtherCollect();
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Boolean)obj);
        }

        protected void onPreExecute()
        {
            if (mFragment == null || !mFragment.canGoon())
            {
                return;
            } else
            {
                showLoading(true);
                return;
            }
        }

        _cls5()
        {
            this$0 = FeedCollectAlbumListAdapter.this;
            album = albummodel;
            position = i;
            super();
        }
    }

}
