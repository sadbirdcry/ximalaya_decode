// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.feed;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.ting.feed.FeedFragmentNew;
import com.ximalaya.ting.android.model.feed2.FeedModel2;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import com.ximalaya.ting.android.view.RoundedImageView;
import java.util.List;

public class FeedAdapter2 extends BaseAdapter
{
    public static interface ActionListener
    {

        public abstract void doRegAndFollow(FeedModel2 feedmodel2, View view);

        public abstract void goHostHomePage(long l, long l1, View view);

        public abstract void goToPlay(FeedModel2 feedmodel2, View view);
    }

    private static class RecommendViewHolder
    {

        ImageView albumCoverIv;
        RelativeLayout albumLayout;
        TextView albumNameTv;
        LinearLayout followActionLayout;
        ImageView followIv;
        TextView followTv;
        RoundedImageView hostIconIv;
        TextView hostInfoTv;
        RelativeLayout hostLayout;
        TextView hostNameTv;
        TextView soundTitleTv;

        private RecommendViewHolder()
        {
        }

        RecommendViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }

    private static class ViewHolder
    {

        ImageView adIndicator;
        ImageView adIv;
        ImageView albumCoverIv;
        TextView albumNameTv;
        View border;
        RelativeLayout feedLayout;
        TextView hostNameTv;
        TextView lastUpdateTimeTv;
        RelativeLayout newThingsLayout;
        TextView soundTitleTv;
        ImageView topTagIv;
        TextView updateCountTv;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private ActionListener actionListener;
    private List dataList;
    private FeedFragmentNew fragment;
    private Context mContext;

    public FeedAdapter2(Context context, List list, FeedFragmentNew feedfragmentnew)
    {
        mContext = context;
        dataList = list;
        if (feedfragmentnew instanceof ActionListener)
        {
            actionListener = feedfragmentnew;
        }
        fragment = feedfragmentnew;
    }

    private RecommendViewHolder buildRecommendViewHolder(View view)
    {
        RecommendViewHolder recommendviewholder = new RecommendViewHolder(null);
        recommendviewholder.albumCoverIv = (ImageView)view.findViewById(0x7f0a0023);
        recommendviewholder.albumCoverIv.setTag(0x7f0a0037, Boolean.valueOf(true));
        recommendviewholder.albumNameTv = (TextView)view.findViewById(0x7f0a00ea);
        recommendviewholder.soundTitleTv = (TextView)view.findViewById(0x7f0a00e6);
        recommendviewholder.hostIconIv = (RoundedImageView)view.findViewById(0x7f0a023c);
        recommendviewholder.hostIconIv.setTag(0x7f0a0037, Boolean.valueOf(true));
        recommendviewholder.hostNameTv = (TextView)view.findViewById(0x7f0a023d);
        recommendviewholder.hostInfoTv = (TextView)view.findViewById(0x7f0a025b);
        recommendviewholder.followActionLayout = (LinearLayout)view.findViewById(0x7f0a025a);
        recommendviewholder.followIv = (ImageView)view.findViewById(0x7f0a025c);
        recommendviewholder.followTv = (TextView)view.findViewById(0x7f0a025d);
        recommendviewholder.hostLayout = (RelativeLayout)view.findViewById(0x7f0a0259);
        recommendviewholder.albumLayout = (RelativeLayout)view.findViewById(0x7f0a024f);
        return recommendviewholder;
    }

    private ViewHolder buildViewHolder(View view, ViewGroup viewgroup)
    {
        viewgroup = new ViewHolder(null);
        viewgroup.feedLayout = (RelativeLayout)view.findViewById(0x7f0a024a);
        viewgroup.adIv = (ImageView)view.findViewById(0x7f0a0252);
        ((ViewHolder) (viewgroup)).adIv.setTag(0x7f0a0037, Boolean.valueOf(true));
        viewgroup.newThingsLayout = (RelativeLayout)view.findViewById(0x7f0a024d);
        viewgroup.albumCoverIv = (ImageView)view.findViewById(0x7f0a0023);
        ((ViewHolder) (viewgroup)).albumCoverIv.setTag(0x7f0a0037, Boolean.valueOf(true));
        fragment.markImageView(((ViewHolder) (viewgroup)).albumCoverIv);
        viewgroup.albumNameTv = (TextView)view.findViewById(0x7f0a00ea);
        viewgroup.soundTitleTv = (TextView)view.findViewById(0x7f0a00e6);
        viewgroup.hostNameTv = (TextView)view.findViewById(0x7f0a023d);
        viewgroup.topTagIv = (ImageView)view.findViewById(0x7f0a0251);
        viewgroup.lastUpdateTimeTv = (TextView)view.findViewById(0x7f0a024b);
        viewgroup.updateCountTv = (TextView)view.findViewById(0x7f0a024c);
        viewgroup.border = view.findViewById(0x7f0a00a8);
        viewgroup.adIndicator = (ImageView)view.findViewById(0x7f0a0255);
        return viewgroup;
    }

    private boolean isNewFeedAdView(View view)
    {
        while (view == null || !(view.getTag() instanceof ViewHolder)) 
        {
            return true;
        }
        return false;
    }

    private boolean isNewReccomendView(View view)
    {
        while (view == null || !(view.getTag() instanceof RecommendViewHolder)) 
        {
            return true;
        }
        return false;
    }

    private boolean nextIsTitleView(int i)
    {
        if (i + 2 <= dataList.size())
        {
            return ((FeedModel2)dataList.get(i + 1)).isRecommendTitle;
        } else
        {
            return false;
        }
    }

    private boolean preIsTitleView(int i)
    {
        if (i - 1 >= 0)
        {
            return ((FeedModel2)dataList.get(i - 1)).isRecommendTitle;
        } else
        {
            return false;
        }
    }

    public int getCount()
    {
        return dataList.size();
    }

    public View getFeedOrAdView(int i, View view, ViewGroup viewgroup)
    {
        View view1;
        FeedModel2 feedmodel2;
        if (isNewFeedAdView(view))
        {
            view1 = LayoutInflater.from(mContext).inflate(0x7f030080, viewgroup, false);
            viewgroup = buildViewHolder(view1, viewgroup);
            view1.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
            view1 = view;
        }
        feedmodel2 = (FeedModel2)dataList.get(i);
        if (feedmodel2.isAd)
        {
            ((ViewHolder) (viewgroup)).adIndicator.setVisibility(0);
            ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).albumCoverIv, feedmodel2.cover, 0x7f0202e0);
            ((ViewHolder) (viewgroup)).newThingsLayout.setVisibility(8);
            TextView textview = ((ViewHolder) (viewgroup)).albumNameTv;
            if (feedmodel2.albumTitle == null)
            {
                view = "";
            } else
            {
                view = feedmodel2.albumTitle;
            }
            textview.setText(view);
            if (feedmodel2.trackTitle == null)
            {
                view = "";
            } else
            {
                view = feedmodel2.trackTitle;
            }
            ((ViewHolder) (viewgroup)).albumNameTv.setVisibility(0);
            ((ViewHolder) (viewgroup)).soundTitleTv.setText(view);
            ((ViewHolder) (viewgroup)).soundTitleTv.setVisibility(0);
            ((ViewHolder) (viewgroup)).hostNameTv.setVisibility(8);
            ((ViewHolder) (viewgroup)).lastUpdateTimeTv.setVisibility(8);
            ((ViewHolder) (viewgroup)).updateCountTv.setVisibility(8);
            ((ViewHolder) (viewgroup)).topTagIv.setVisibility(8);
        } else
        {
            if (((ViewHolder) (viewgroup)).feedLayout.getVisibility() != 0)
            {
                ((ViewHolder) (viewgroup)).feedLayout.setVisibility(0);
                ((ViewHolder) (viewgroup)).adIv.setVisibility(4);
            }
            ((ViewHolder) (viewgroup)).lastUpdateTimeTv.setVisibility(0);
            ((ViewHolder) (viewgroup)).adIndicator.setVisibility(4);
            ((ViewHolder) (viewgroup)).lastUpdateTimeTv.setText((new StringBuilder()).append("\u6700\u540E\u66F4\u65B0  ").append(ToolUtil.convertTimeForFeed(feedmodel2.lastUpdateAt)).toString());
            TextView textview1 = ((ViewHolder) (viewgroup)).updateCountTv;
            if (feedmodel2.unreadNum >= 99)
            {
                view = "N";
            } else
            {
                view = String.valueOf(feedmodel2.unreadNum);
            }
            textview1.setText(view);
            if (feedmodel2.unreadNum <= 0)
            {
                ((ViewHolder) (viewgroup)).updateCountTv.setVisibility(8);
            } else
            if (((ViewHolder) (viewgroup)).updateCountTv.getVisibility() != 0)
            {
                ((ViewHolder) (viewgroup)).updateCountTv.setVisibility(0);
            }
            if (feedmodel2.dynamicType == 2)
            {
                ((ViewHolder) (viewgroup)).albumCoverIv.setImageDrawable(mContext.getResources().getDrawable(0x7f02039f));
                ((ViewHolder) (viewgroup)).albumCoverIv.setTag(null);
                ((ViewHolder) (viewgroup)).soundTitleTv.setVisibility(8);
                ((ViewHolder) (viewgroup)).hostNameTv.setVisibility(8);
                ((ViewHolder) (viewgroup)).topTagIv.setVisibility(8);
                ((ViewHolder) (viewgroup)).albumNameTv.setVisibility(8);
                ((ViewHolder) (viewgroup)).newThingsLayout.setVisibility(0);
            } else
            {
                ((ViewHolder) (viewgroup)).hostNameTv.setVisibility(0);
                ((ViewHolder) (viewgroup)).albumNameTv.setVisibility(0);
                ((ViewHolder) (viewgroup)).soundTitleTv.setVisibility(0);
                ((ViewHolder) (viewgroup)).newThingsLayout.setVisibility(8);
                ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).albumCoverIv, feedmodel2.albumCover, 0x7f0202e0);
                Object obj = ((ViewHolder) (viewgroup)).albumNameTv;
                TextView textview2;
                int j;
                if (feedmodel2.albumTitle == null)
                {
                    view = "";
                } else
                {
                    view = feedmodel2.albumTitle;
                }
                ((TextView) (obj)).setText(view);
                if (feedmodel2.trackTitle == null)
                {
                    view = "";
                } else
                {
                    view = feedmodel2.trackTitle;
                }
                textview2 = ((ViewHolder) (viewgroup)).soundTitleTv;
                obj = view;
                if (feedmodel2.trackType == 2)
                {
                    obj = (new StringBuilder()).append("\u3010\u8F6C\u91C7\u3011").append(view).toString();
                }
                textview2.setText(((CharSequence) (obj)));
                if (feedmodel2.nickname == null)
                {
                    view = "";
                } else
                {
                    view = feedmodel2.nickname;
                }
                ((ViewHolder) (viewgroup)).hostNameTv.setText((new StringBuilder()).append("by ").append(view).toString());
                view = ((ViewHolder) (viewgroup)).topTagIv;
                if (feedmodel2.isTop)
                {
                    j = 0;
                } else
                {
                    j = 8;
                }
                view.setVisibility(j);
            }
        }
        if (i + 1 == dataList.size() || nextIsTitleView(i))
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(8);
            return view1;
        } else
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(0);
            return view1;
        }
    }

    public Object getItem(int i)
    {
        if (dataList == null || dataList.isEmpty())
        {
            return null;
        } else
        {
            return (FeedModel2)dataList.get(i);
        }
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getRecommendTitleView(int i, View view, ViewGroup viewgroup)
    {
        View view1;
label0:
        {
            if (view != null)
            {
                view1 = view;
                if ("recommendTitle".equals(view.getTag()))
                {
                    break label0;
                }
            }
            view1 = LayoutInflater.from(mContext).inflate(0x7f030083, viewgroup, false);
        }
        ((TextView)view1.findViewById(0x7f0a0158)).setText("\u63A8\u8350\u5173\u6CE8");
        view1.setTag("recommendTitle");
        return view1;
    }

    public View getRecommendView(int i, View view, ViewGroup viewgroup)
    {
        View view1;
        Object obj;
        final FeedModel2 data;
        TextView textview;
        if (isNewReccomendView(view))
        {
            view1 = LayoutInflater.from(mContext).inflate(0x7f030082, viewgroup, false);
            viewgroup = buildRecommendViewHolder(view1);
            view1.setTag(viewgroup);
        } else
        {
            viewgroup = (RecommendViewHolder)view.getTag();
            view1 = view;
        }
        data = (FeedModel2)dataList.get(i);
        ImageManager2.from(mContext).displayImage(((RecommendViewHolder) (viewgroup)).albumCoverIv, data.albumCover, 0x7f0202e0);
        obj = ((RecommendViewHolder) (viewgroup)).albumNameTv;
        if (data.albumTitle == null)
        {
            view = "";
        } else
        {
            view = data.albumTitle;
        }
        ((TextView) (obj)).setText(view);
        if (data.trackTitle == null)
        {
            view = "";
        } else
        {
            view = data.trackTitle;
        }
        textview = ((RecommendViewHolder) (viewgroup)).soundTitleTv;
        obj = view;
        if (data.trackType == 2)
        {
            obj = (new StringBuilder()).append("\u3010\u8F6C\u91C7\u3011").append(view).toString();
        }
        textview.setText(((CharSequence) (obj)));
        ImageManager2.from(mContext).displayImage(((RecommendViewHolder) (viewgroup)).hostIconIv, data.avatar, 0x7f0202df);
        obj = ((RecommendViewHolder) (viewgroup)).hostNameTv;
        if (data.nickname == null)
        {
            view = "";
        } else
        {
            view = data.nickname;
        }
        ((TextView) (obj)).setText(view);
        obj = ((RecommendViewHolder) (viewgroup)).hostInfoTv;
        if (data.personalSignature == null)
        {
            view = "";
        } else
        {
            view = data.personalSignature;
        }
        ((TextView) (obj)).setText(view);
        if (data.isFollowed)
        {
            ((RecommendViewHolder) (viewgroup)).followTv.setText("\u5DF2\u5173\u6CE8");
            ((RecommendViewHolder) (viewgroup)).followTv.setTextColor(mContext.getResources().getColor(0x7f070006));
            ((RecommendViewHolder) (viewgroup)).followActionLayout.setBackgroundResource(0x7f020265);
            ((RecommendViewHolder) (viewgroup)).followIv.setVisibility(8);
        } else
        {
            ((RecommendViewHolder) (viewgroup)).followTv.setText("\u5173\u6CE8");
            ((RecommendViewHolder) (viewgroup)).followTv.setTextColor(mContext.getResources().getColor(0x7f070004));
            ((RecommendViewHolder) (viewgroup)).followActionLayout.setBackgroundResource(0x7f020263);
            ((RecommendViewHolder) (viewgroup)).followIv.setVisibility(0);
        }
        ViewUtil.expandClickArea(mContext, ((RecommendViewHolder) (viewgroup)).followActionLayout, 15, 10, 10, 10);
        ((RecommendViewHolder) (viewgroup)).hostLayout.setOnClickListener(new _cls1());
        ((RecommendViewHolder) (viewgroup)).followActionLayout.setOnClickListener(new _cls2());
        ((RecommendViewHolder) (viewgroup)).albumLayout.setOnClickListener(new _cls3());
        return view1;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        FeedModel2 feedmodel2 = (FeedModel2)dataList.get(i);
        if (feedmodel2.isRecommend)
        {
            return getRecommendView(i, view, viewgroup);
        }
        if (feedmodel2.isRecommendTitle)
        {
            return getRecommendTitleView(i, view, viewgroup);
        } else
        {
            return getFeedOrAdView(i, view, viewgroup);
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final FeedAdapter2 this$0;
        final FeedModel2 val$data;

        public void onClick(View view)
        {
            if (actionListener != null)
            {
                actionListener.goHostHomePage(data.uid, data.albumId, view);
            }
        }

        _cls1()
        {
            this$0 = FeedAdapter2.this;
            data = feedmodel2;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final FeedAdapter2 this$0;
        final FeedModel2 val$data;

        public void onClick(View view)
        {
            if (actionListener != null)
            {
                actionListener.doRegAndFollow(data, view);
            }
        }

        _cls2()
        {
            this$0 = FeedAdapter2.this;
            data = feedmodel2;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final FeedAdapter2 this$0;
        final FeedModel2 val$data;

        public void onClick(View view)
        {
            if (actionListener != null)
            {
                actionListener.goToPlay(data, view);
            }
        }

        _cls3()
        {
            this$0 = FeedAdapter2.this;
            data = feedmodel2;
            super();
        }
    }

}
