// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.hot_activity.ActivityListModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

public class ActivityListAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        ImageView bg_img;
        ImageView hot_flag_img;
        TextView statusTxt;
        TextView titleTxt;

        private ViewHolder()
        {
        }

    }


    private List items;
    private Context mContext;

    public ActivityListAdapter(Context context, List list)
    {
        mContext = context;
        items = list;
    }

    public int getCount()
    {
        if (items == null)
        {
            return 0;
        } else
        {
            return items.size();
        }
    }

    public Object getItem(int i)
    {
        return items.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        ActivityListModel activitylistmodel;
        if (view == null)
        {
            viewgroup = new ViewHolder();
            view = LayoutInflater.from(mContext).inflate(0x7f030036, null);
            viewgroup.bg_img = (ImageView)view.findViewById(0x7f0a012e);
            android.view.ViewGroup.LayoutParams layoutparams = ((ViewHolder) (viewgroup)).bg_img.getLayoutParams();
            layoutparams.width = ToolUtil.getScreenWidth(mContext);
            layoutparams.height = layoutparams.width / 2;
            ((ViewHolder) (viewgroup)).bg_img.setLayoutParams(layoutparams);
            viewgroup.hot_flag_img = (ImageView)view.findViewById(0x7f0a012f);
            viewgroup.titleTxt = (TextView)view.findViewById(0x7f0a0130);
            viewgroup.statusTxt = (TextView)view.findViewById(0x7f0a0131);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        activitylistmodel = (ActivityListModel)items.get(i);
        ((ViewHolder) (viewgroup)).bg_img.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).bg_img, activitylistmodel.coverPathLarge, 0x7f02000e);
        if (activitylistmodel.isHot)
        {
            ((ViewHolder) (viewgroup)).hot_flag_img.setVisibility(0);
        } else
        {
            ((ViewHolder) (viewgroup)).hot_flag_img.setVisibility(8);
        }
        ((ViewHolder) (viewgroup)).titleTxt.setText(activitylistmodel.title);
        switch (activitylistmodel.activityStatus)
        {
        default:
            return view;

        case 1: // '\001'
            ((ViewHolder) (viewgroup)).statusTxt.setText("\u5F00\u59CB\u62A5\u540D");
            ((ViewHolder) (viewgroup)).statusTxt.setCompoundDrawablesWithIntrinsicBounds(0x7f020010, 0, 0, 0);
            ((ViewHolder) (viewgroup)).statusTxt.setTextColor(mContext.getResources().getColor(0x7f070079));
            return view;

        case 2: // '\002'
        case 3: // '\003'
            ((ViewHolder) (viewgroup)).statusTxt.setText("\u6D3B\u52A8\u4E2D");
            ((ViewHolder) (viewgroup)).statusTxt.setCompoundDrawablesWithIntrinsicBounds(0x7f02000d, 0, 0, 0);
            ((ViewHolder) (viewgroup)).statusTxt.setTextColor(mContext.getResources().getColor(0x7f070079));
            return view;

        case 4: // '\004'
            ((ViewHolder) (viewgroup)).statusTxt.setText("\u6D3B\u52A8\u7ED3\u675F");
            break;
        }
        ((ViewHolder) (viewgroup)).statusTxt.setCompoundDrawablesWithIntrinsicBounds(0x7f02000b, 0, 0, 0);
        ((ViewHolder) (viewgroup)).statusTxt.setTextColor(mContext.getResources().getColor(0x7f070006));
        return view;
    }

    public void setData(List list)
    {
        items = list;
    }
}
