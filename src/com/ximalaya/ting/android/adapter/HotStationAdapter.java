// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.broadcast.StationCategory;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.model.holder.PersonStationHolder;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.MyGridView;
import java.util.ArrayList;
import java.util.List;

public class HotStationAdapter extends BaseExpandableListAdapter
{
    private static class ItemAdapter extends BaseAdapter
    {

        private Context mActivity;
        private BaseFragment mFragment;
        private GridView mGridView;
        private List mStations;
        private String mTitle;

        public int getCount()
        {
            if (mStations == null)
            {
                return 0;
            } else
            {
                return mStations.size();
            }
        }

        public Object getItem(int i)
        {
            return mStations.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(final int position, final View holder, ViewGroup viewgroup)
        {
            StationModel stationmodel = (StationModel)mStations.get(position);
            viewgroup = holder;
            if (holder == null)
            {
                viewgroup = PersonStationHolder.getView(mActivity);
                holder = (PersonStationHolder)viewgroup.getTag();
                viewgroup.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, -2));
                mFragment.markImageView(((PersonStationHolder) (holder)).cover);
                class _cls1
                    implements android.view.View.OnClickListener
                {

                    final ItemAdapter this$0;
                    final int val$position;

                    public void onClick(View view)
                    {
                        StationModel stationmodel1 = (StationModel)view.getTag(0x7f090000);
                        if (stationmodel1 != null)
                        {
                            Bundle bundle = new Bundle();
                            bundle.putLong("toUid", stationmodel1.uid);
                            bundle.putInt("from", 1);
                            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view, mTitle, (new StringBuilder()).append(position + 1).append("").toString()));
                            if (mActivity instanceof MainTabActivity2)
                            {
                                ((MainTabActivity2)mActivity).startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, bundle);
                                return;
                            }
                        }
                    }

                _cls1()
                {
                    this$0 = ItemAdapter.this;
                    position = i;
                    super();
                }
                }

                class _cls2
                    implements android.view.View.OnClickListener
                {

                    final ItemAdapter this$0;
                    final PersonStationHolder val$holder;

                    public void onClick(View view)
                    {
                        Object obj = (StationModel)view.getTag(0x7f090000);
                        if (obj == null)
                        {
                            return;
                        }
                        if (UserInfoMannage.hasLogined())
                        {
                            HotStationAdapter.doFollow(mActivity, ((StationModel) (obj)), holder);
                            return;
                        } else
                        {
                            HotStationAdapter.setFollowStatus(holder, ((StationModel) (obj)).isFollowed);
                            obj = new Intent(mActivity, com/ximalaya/ting/android/activity/login/LoginActivity);
                            ((Intent) (obj)).putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                            mActivity.startActivity(((Intent) (obj)));
                            return;
                        }
                    }

                _cls2()
                {
                    this$0 = ItemAdapter.this;
                    holder = personstationholder;
                    super();
                }
                }

                android.view.ViewGroup.LayoutParams layoutparams;
                int i;
                if (a.e)
                {
                    i = (int)((float)ToolUtil.getScreenWidth(mActivity) / 6F);
                } else
                {
                    i = (int)((float)(ToolUtil.getScreenWidth(mActivity) - ToolUtil.dp2px(mActivity, 60F)) / 3F);
                }
                layoutparams = ((PersonStationHolder) (holder)).cover.getLayoutParams();
                layoutparams.width = i;
                layoutparams.height = layoutparams.width;
                ((PersonStationHolder) (holder)).cover.setLayoutParams(layoutparams);
                ((PersonStationHolder) (holder)).cover.setOnClickListener(new _cls1());
                ((PersonStationHolder) (holder)).follow.setOnClickListener(new _cls2());
            }
            holder = (PersonStationHolder)viewgroup.getTag();
            ((PersonStationHolder) (holder)).cover.setTag(0x7f0a0037, Boolean.valueOf(true));
            ((PersonStationHolder) (holder)).cover.setTag(0x7f090000, stationmodel);
            ImageManager2.from(mActivity).displayImage(((PersonStationHolder) (holder)).cover, stationmodel.smallLogo, 0x7f0201b5);
            ((PersonStationHolder) (holder)).name.setText(stationmodel.nickname);
            HotStationAdapter.setFollowStatus(holder, stationmodel.isFollowed);
            ((PersonStationHolder) (holder)).follow.setTag(0x7f090000, stationmodel);
            return viewgroup;
        }

        public void setList(List list)
        {
            mStations.clear();
            if (list != null)
            {
                mStations.addAll(list);
            }
            notifyDataSetChanged();
        }

        public void setTitle(String s)
        {
            if (!TextUtils.isEmpty(s))
            {
                mTitle = null;
                mTitle = s;
            }
            notifyDataSetChanged();
        }

        void updateItem()
        {
            int i = 0;
_L6:
            if (i >= mStations.size()) goto _L2; else goto _L1
_L1:
            Object obj;
            int j = mGridView.getFirstVisiblePosition();
            obj = mGridView.getChildAt(i - j);
            if (obj != null) goto _L3; else goto _L2
_L2:
            return;
_L3:
            if ((obj = (PersonStationHolder)((View) (obj)).getTag()) == null) goto _L2; else goto _L4
_L4:
            HotStationAdapter.setFollowStatus(((PersonStationHolder) (obj)), ((StationModel)mStations.get(i)).isFollowed);
            i++;
            if (true) goto _L6; else goto _L5
_L5:
        }



        public ItemAdapter(BaseFragment basefragment, GridView gridview, List list)
        {
            mStations = new ArrayList();
            mFragment = basefragment;
            mGridView = gridview;
            mActivity = mGridView.getContext();
            setList(list);
        }
    }


    private Context mActivity;
    private ArrayList mCategorys;
    private BaseFragment mFragment;
    private ExpandableListView mListView;

    public HotStationAdapter(BaseFragment basefragment, ExpandableListView expandablelistview, ArrayList arraylist)
    {
        mFragment = basefragment;
        mListView = expandablelistview;
        mActivity = mListView.getContext();
        mCategorys = arraylist;
    }

    public static void doFollow(final Context context, final StationModel model, final PersonStationHolder holder)
    {
        if (UserInfoMannage.getInstance().getUser() == null)
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("toUid", (new StringBuilder()).append("").append(model.uid).toString());
        StringBuilder stringbuilder = (new StringBuilder()).append("");
        boolean flag;
        if (!model.isFollowed)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        requestparams.put("isFollow", stringbuilder.append(flag).toString());
        f.a().b("mobile/follow", requestparams, DataCollectUtil.getDataFromView(holder.follow), new _cls1());
    }

    private static void setFollowStatus(PersonStationHolder personstationholder, boolean flag)
    {
        if (flag)
        {
            personstationholder.follow.setBackgroundResource(0x7f020266);
            personstationholder.followImg.setVisibility(8);
            personstationholder.followTxt.setText("\u5DF2\u5173\u6CE8");
            personstationholder.followTxt.setTextColor(Color.parseColor("#ffffff"));
            return;
        } else
        {
            personstationholder.follow.setBackgroundResource(0x7f020263);
            personstationholder.followImg.setVisibility(0);
            personstationholder.followTxt.setText("\u5173\u6CE8");
            personstationholder.followTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    public Object getChild(int i, int j)
    {
        return ((StationCategory)mCategorys.get(i)).list.get(j);
    }

    public long getChildId(int i, int j)
    {
        return (long)j;
    }

    public View getChildView(int i, int j, boolean flag, View view, ViewGroup viewgroup)
    {
        viewgroup = (StationCategory)mCategorys.get(i);
        if (view == null)
        {
            view = new MyGridView(mActivity);
            GridView gridview;
            if (a.e)
            {
                view.setNumColumns(6);
            } else
            {
                view.setNumColumns(3);
            }
            view.setSelector(0x7f070003);
            view.setBackgroundColor(-1);
            view.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, -2));
            view.setPadding(ToolUtil.dp2px(mActivity.getApplicationContext(), 8F), 0, ToolUtil.dp2px(mActivity.getApplicationContext(), 8F), 0);
            view.setAdapter(new ItemAdapter(mFragment, view, null));
        }
        gridview = (GridView)view;
        ((ItemAdapter)gridview.getAdapter()).setList(((StationCategory) (viewgroup)).list);
        ((ItemAdapter)gridview.getAdapter()).setTitle(((StationCategory) (viewgroup)).title);
        return view;
    }

    public int getChildrenCount(int i)
    {
        return 1;
    }

    public Object getGroup(int i)
    {
        return mCategorys.get(i);
    }

    public int getGroupCount()
    {
        if (mCategorys == null)
        {
            return 0;
        } else
        {
            return mCategorys.size();
        }
    }

    public long getGroupId(int i)
    {
        return (long)i;
    }

    public View getGroupView(int i, boolean flag, View view, ViewGroup viewgroup)
    {
        viewgroup = view;
        if (view == null)
        {
            viewgroup = View.inflate(mActivity, 0x7f03008c, null);
            viewgroup.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ToolUtil.dp2px(mActivity, 45F)));
            ((TextView)viewgroup.findViewById(0x7f0a0277)).setOnClickListener(new _cls2());
        }
        TextView textview;
        StationCategory stationcategory;
        if (i == 0)
        {
            viewgroup.findViewById(0x7f0a0276).setVisibility(8);
        } else
        {
            viewgroup.findViewById(0x7f0a0276).setVisibility(0);
        }
        view = (TextView)viewgroup.findViewById(0x7f0a0175);
        textview = (TextView)viewgroup.findViewById(0x7f0a0277);
        stationcategory = (StationCategory)mCategorys.get(i);
        view.setText(stationcategory.title);
        textview.setTag(0x7f090000, stationcategory);
        return viewgroup;
    }

    public boolean hasStableIds()
    {
        return false;
    }

    public boolean isChildSelectable(int i, int j)
    {
        return true;
    }

    public void updateItem(int i)
    {
        if (i >= 0 && i < getGroupCount())
        {
            int j = mListView.getFirstVisiblePosition();
            int k = mListView.getHeaderViewsCount();
            View view = mListView.getChildAt((i + 1) * 2 - (j - k) - 1);
            if (view != null && (view instanceof GridView))
            {
                ((ItemAdapter)((GridView)view).getAdapter()).updateItem();
                return;
            }
        }
    }


    private class _cls1 extends com.ximalaya.ting.android.b.a
    {

        final Context val$context;
        final PersonStationHolder val$holder;
        final StationModel val$model;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, holder.follow);
        }

        public void onFinish()
        {
            super.onFinish();
            HotStationAdapter.setFollowStatus(holder, model.isFollowed);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
                return;
            }
            s = JSON.parseObject(s);
            if (s == null || s.getIntValue("ret") != 0)
            {
                Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
                return;
            }
            s = model;
            boolean flag;
            if (!model.isFollowed)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            s.isFollowed = flag;
            if (model.isFollowed)
            {
                s = "\u5173\u6CE8\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u5173\u6CE8\u6210\u529F";
            }
            Toast.makeText(context, s, 0).show();
        }

        _cls1()
        {
            holder = personstationholder;
            model = stationmodel;
            context = context1;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final HotStationAdapter this$0;

        public void onClick(View view)
        {
            StationCategory stationcategory = (StationCategory)view.getTag(0x7f090000);
            if (stationcategory == null)
            {
                return;
            } else
            {
                Bundle bundle = new Bundle();
                bundle.putString("category", stationcategory.name);
                bundle.putString("title", stationcategory.title);
                bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                ((MainTabActivity2)MyApplication.a()).startFragment(com/ximalaya/ting/android/fragment/findings/FindingHotStationListFragment, bundle);
                return;
            }
        }

        _cls2()
        {
            this$0 = HotStationAdapter.this;
            super();
        }
    }

}
