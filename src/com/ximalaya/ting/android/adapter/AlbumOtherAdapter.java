// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

public class AlbumOtherAdapter extends BaseAdapter
{

    private View emptyView;
    private boolean flag;
    private float lastDownX;
    private float lastDownY;
    private float lastUpX;
    private float lastUpY;
    public List list;
    private Context mContext;

    public AlbumOtherAdapter(Context context, List list1, View view, boolean flag1)
    {
        flag = true;
        mContext = context;
        flag = flag1;
        list = list1;
        emptyView = view;
    }

    public AlbumOtherAdapter(Context context, List list1, boolean flag1)
    {
        flag = true;
        mContext = context;
        flag = flag1;
        list = list1;
    }

    public static void doCollect(final Context context, final AlbumModel model, final AlbumItemHolder holder, final View view)
    {
        if (UserInfoMannage.hasLogined())
        {
            String s;
            RequestParams requestparams;
            if (model.isFavorite)
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.albumId).toString());
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(view), new _cls2());
        } else
        if (AlbumModelManage.getInstance().ensureLocalCollectAllow(context, model, view))
        {
            (new _cls3()).myexec(new Void[0]);
            return;
        }
    }

    private void favoriteAlbum(final AlbumModel album, final int position)
    {
        if (ToolUtil.isConnectToNetwork(mContext))
        {
            if (UserInfoMannage.hasLogined())
            {
                (new _cls4()).myexec(new Void[0]);
                return;
            } else
            {
                album = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                mContext.startActivity(album);
                return;
            }
        } else
        {
            showToast("\u7F51\u7EDC\u672A\u8FDE\u63A5");
            return;
        }
    }

    private void showToast(String s)
    {
        Toast.makeText(mContext, s, 1).show();
    }

    public int getCount()
    {
        if (list == null)
        {
            return 0;
        } else
        {
            return list.size();
        }
    }

    public AlbumModel getItem(int i)
    {
        if (list == null || i >= list.size())
        {
            return new AlbumModel();
        } else
        {
            return (AlbumModel)list.get(i);
        }
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, final View holder, ViewGroup viewgroup)
    {
        boolean flag2 = true;
        viewgroup = holder;
        if (holder == null)
        {
            viewgroup = AlbumItemHolder.getView(mContext);
        }
        holder = (AlbumItemHolder)viewgroup.getTag();
        final AlbumModel info = getItem(i);
        long l;
        boolean flag1;
        if (mContext instanceof MainTabActivity2)
        {
            ImageManager2.from(mContext).displayImage(((AlbumItemHolder) (holder)).cover, info.coverLarge, 0x7f0202e0, true);
        } else
        {
            ImageManager2.from(mContext).displayImage(((AlbumItemHolder) (holder)).cover, info.coverLarge, 0x7f0202e0);
        }
        ((AlbumItemHolder) (holder)).collectCount.setText((new StringBuilder()).append(info.tracks).append("\u96C6").toString());
        if (info.lastUptrackAt == 0L)
        {
            l = info.updatedAt;
        } else
        {
            l = info.lastUptrackAt;
        }
        ((AlbumItemHolder) (holder)).updateAt.setText(mContext.getString(0x7f0901e7, new Object[] {
            ToolUtil.convertTime(l)
        }));
        if (info.playTimes > 0)
        {
            ((AlbumItemHolder) (holder)).playCount.setText(StringUtil.getFriendlyNumStr(info.playTimes));
            ((AlbumItemHolder) (holder)).playCount.setVisibility(0);
        } else
        {
            ((AlbumItemHolder) (holder)).playCount.setVisibility(8);
        }
        ((AlbumItemHolder) (holder)).name.setText(info.title);
        AlbumItemHolder.setCollectStatus(holder, info.isFavorite);
        ((AlbumItemHolder) (holder)).collect.setTag(0x7f090000, info);
        ((AlbumItemHolder) (holder)).collect.setOnClickListener(new _cls1());
        holder = mContext;
        if (i == 0)
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        if (i + 1 != list.size())
        {
            flag2 = false;
        }
        ViewUtil.buildAlbumItemSpace(holder, viewgroup, flag1, flag2);
        return viewgroup;
    }

    public void releseData()
    {
        mContext = null;
    }




    private class _cls2 extends a
    {

        final Context val$context;
        final AlbumItemHolder val$holder;
        final AlbumModel val$model;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            if (((AlbumModel)holder.collect.getTag(0x7f090000)).albumId == model.albumId)
            {
                AlbumItemHolder.setCollectStatus(holder, model.isFavorite);
            }
        }

        public void onSuccess(String s)
        {
            boolean flag1 = true;
            if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
_L4:
            return;
_L2:
            int i;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                return;
            }
            if (s == null) goto _L4; else goto _L3
_L3:
            i = s.getIntValue("ret");
            if (i != 0)
            {
                break MISSING_BLOCK_LABEL_132;
            }
            s = model;
            if (model.isFavorite)
            {
                flag1 = false;
            }
            s.isFavorite = flag1;
            if (((AlbumModel)holder.collect.getTag(0x7f090000)).albumId == model.albumId)
            {
                AlbumItemHolder.setCollectStatus(holder, model.isFavorite);
            }
            if (model.isFavorite)
            {
                s = "\u6536\u85CF\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
            }
            Toast.makeText(context, s, 0).show();
            return;
            if (i != 791)
            {
                break MISSING_BLOCK_LABEL_147;
            }
            model.isFavorite = true;
            if (((AlbumModel)holder.collect.getTag(0x7f090000)).albumId == model.albumId)
            {
                AlbumItemHolder.setCollectStatus(holder, model.isFavorite);
            }
            if (s.getString("msg") != null)
            {
                break MISSING_BLOCK_LABEL_215;
            }
            s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
_L6:
            Toast.makeText(context, s, 0).show();
            return;
            s = s.getString("msg");
            if (true) goto _L6; else goto _L5
_L5:
        }

        _cls2()
        {
            context = context1;
            holder = albumitemholder;
            model = albummodel;
            view = view1;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final AlbumItemHolder val$holder;
        final AlbumModel val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            AlbumModel albummodel = model;
            boolean flag1;
            if (!model.isFavorite)
            {
                flag1 = true;
            } else
            {
                flag1 = false;
            }
            albummodel.isFavorite = flag1;
            if (!model.isFavorite)
            {
                avoid.deleteAlbumInLocalAlbumList(model);
            } else
            {
                avoid.saveAlbumModel(model);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            if (((AlbumModel)holder.collect.getTag(0x7f090000)).albumId == model.albumId)
            {
                AlbumItemHolder.setCollectStatus(holder, model.isFavorite);
            }
        }

        _cls3()
        {
            model = albummodel;
            holder = albumitemholder;
            super();
        }
    }


    private class _cls4 extends MyAsyncTask
    {

        final AlbumOtherAdapter this$0;
        final AlbumModel val$album;
        final int val$position;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            Object obj = null;
            RequestParams requestparams;
            try
            {
                AlbumModelManage.getInstance().deleteAlbumInLocalAlbumList(album);
            }
            // Misplaced declaration of an exception variable
            catch (Void avoid[]) { }
            avoid = ApiUtil.getApiHost();
            avoid = (new StringBuilder()).append(avoid).append("mobile/album/favorite/destroy").toString();
            requestparams = new RequestParams();
            requestparams.put("albumId", (new StringBuilder()).append("").append(album.albumId).toString());
            avoid = f.a().a(avoid, requestparams, null);
            if (((com.ximalaya.ting.android.b.n.a) (avoid)).b == 1)
            {
                avoid = ((com.ximalaya.ting.android.b.n.a) (avoid)).a;
            } else
            {
                avoid = null;
            }
            if (avoid != null)
            {
                try
                {
                    avoid = JSON.parseObject(avoid);
                }
                // Misplaced declaration of an exception variable
                catch (Void avoid[])
                {
                    avoid.printStackTrace();
                    avoid = obj;
                }
                if (avoid != null)
                {
                    if (avoid.getInteger("ret").intValue() == 0)
                    {
                        return "0";
                    } else
                    {
                        return avoid.getString("msg");
                    }
                }
            }
            return "\u7F51\u7EDC\u8BBF\u95EE\u5F02\u5E38";
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            while (mContext == null || ((Activity)mContext).isFinishing() || "0".equals(s)) 
            {
                return;
            }
            showToast(s);
        }

        protected void onPreExecute()
        {
label0:
            {
                if (list != null && position < list.size())
                {
                    list.remove(position);
                    notifyDataSetChanged();
                    if (emptyView == null || list.size() != 0)
                    {
                        break label0;
                    }
                    emptyView.setVisibility(0);
                }
                return;
            }
            emptyView.setVisibility(8);
        }

        _cls4()
        {
            this$0 = AlbumOtherAdapter.this;
            position = i;
            album = albummodel;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final AlbumOtherAdapter this$0;
        final AlbumItemHolder val$holder;
        final AlbumModel val$info;

        public void onClick(View view)
        {
            AlbumOtherAdapter.doCollect(mContext, info, holder, view);
        }

        _cls1()
        {
            this$0 = AlbumOtherAdapter.this;
            info = albummodel;
            holder = albumitemholder;
            super();
        }
    }

}
