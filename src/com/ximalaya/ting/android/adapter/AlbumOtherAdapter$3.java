// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.widget.LinearLayout;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.util.MyAsyncTask;

final class val.holder extends MyAsyncTask
{

    final AlbumItemHolder val$holder;
    final AlbumModel val$model;

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected transient Void doInBackground(Void avoid[])
    {
        avoid = AlbumModelManage.getInstance();
        AlbumModel albummodel = val$model;
        boolean flag;
        if (!val$model.isFavorite)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        albummodel.isFavorite = flag;
        if (!val$model.isFavorite)
        {
            avoid.deleteAlbumInLocalAlbumList(val$model);
        } else
        {
            avoid.saveAlbumModel(val$model);
        }
        return null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Void)obj);
    }

    protected void onPostExecute(Void void1)
    {
        if (((AlbumModel)val$holder.collect.getTag(0x7f090000)).albumId == val$model.albumId)
        {
            AlbumItemHolder.setCollectStatus(val$holder, val$model.isFavorite);
        }
    }

    ()
    {
        val$model = albummodel;
        val$holder = albumitemholder;
        super();
    }
}
