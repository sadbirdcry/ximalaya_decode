// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.web.WebActivityNew;
import com.ximalaya.ting.android.fragment.album.AlbumFragment;
import com.ximalaya.ting.android.fragment.findings.FocusAlbumListFragmentNew;
import com.ximalaya.ting.android.fragment.findings.FocusPersonListFragmentNew;
import com.ximalaya.ting.android.fragment.findings.FocusSoundListFragment;
import com.ximalaya.ting.android.fragment.subject.SubjectFragment;
import com.ximalaya.ting.android.fragment.userspace.OtherSpaceFragment;
import com.ximalaya.ting.android.fragment.web.WebFragment;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelnew.FocusImageModelNew;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            FocusImageAdapter

class val.model
    implements android.view.
{

    final FocusImageAdapter this$0;
    final FocusImageModelNew val$model;
    final int val$pos;

    public void onClick(View view)
    {
        Object obj;
        Object obj1;
        String s;
        String s1;
        obj1 = ((FocusImageModelNew)FocusImageAdapter.access$000(FocusImageAdapter.this).get(val$pos)).longTitle;
        s = (new StringBuilder()).append(val$pos + 1).append("").toString();
        if (FocusImageAdapter.access$100(FocusImageAdapter.this))
        {
            obj = "\u4E3B\u9875\u7126\u70B9\u56FE";
        } else
        {
            obj = (new StringBuilder()).append(FocusImageAdapter.access$200(FocusImageAdapter.this)).append("\u7126\u70B9\u56FE").toString();
        }
        s1 = (new StringBuilder()).append(val$model.id).append("").toString();
        ToolUtil.onEvent(FocusImageAdapter.access$300(FocusImageAdapter.this), "Found_FocusMap");
        ((MyApplication)(MyApplication)FocusImageAdapter.access$300(FocusImageAdapter.this).getApplication()).a = "play_banner";
        val$model.type;
        JVM INSTR tableswitch 1 10: default 184
    //                   1 214
    //                   2 319
    //                   3 452
    //                   4 552
    //                   5 693
    //                   6 824
    //                   7 948
    //                   8 1108
    //                   9 1217
    //                   10 1332;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7 _L8 _L9 _L10 _L11
_L1:
        return;
_L2:
        String s2 = (new StringBuilder()).append(val$model.uid).append("").toString();
        DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "user", s2);
        obj = new Bundle();
        ((Bundle) (obj)).putLong("toUid", val$model.uid);
        ((Bundle) (obj)).putInt("from", 3);
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        ((MainTabActivity2)FocusImageAdapter.access$300(FocusImageAdapter.this)).startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, ((Bundle) (obj)));
        return;
_L3:
        String s3 = (new StringBuilder()).append(val$model.albumId).append("").toString();
        DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "album", s3);
        obj = new Bundle();
        obj1 = new AlbumModel();
        obj1.albumId = val$model.albumId;
        obj1.uid = val$model.uid;
        ((Bundle) (obj)).putString("album", JSON.toJSONString(obj1));
        ((Bundle) (obj)).putInt("from", 7);
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        ((MainTabActivity2)FocusImageAdapter.access$300(FocusImageAdapter.this)).startFragment(com/ximalaya/ting/android/fragment/album/AlbumFragment, ((Bundle) (obj)));
        return;
_L4:
        String s4 = (new StringBuilder()).append(val$model.trackId).append("").toString();
        DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "track", s4);
        obj = new SoundInfo();
        obj.trackId = val$model.trackId;
        PlayTools.gotoPlayWithoutUrl(12, ((SoundInfo) (obj)), FocusImageAdapter.access$300(FocusImageAdapter.this), true, DataCollectUtil.getDataFromView(view));
        ToolUtil.onEvent(FocusImageAdapter.access$300(FocusImageAdapter.this), "play_banner");
        return;
_L5:
        if (!TextUtils.isEmpty(val$model.url))
        {
            String s5 = val$model.url;
            DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "link", s5);
            obj = new Intent(FocusImageAdapter.access$300(FocusImageAdapter.this), com/ximalaya/ting/android/activity/web/WebActivityNew);
            ((Intent) (obj)).putExtra("ExtraUrl", val$model.url);
            ((Intent) (obj)).putExtra("show_share_btn", val$model.isShare);
            ((Intent) (obj)).putExtra("is_external_url", val$model.is_External_url);
            ((Intent) (obj)).putExtra("share_cover_path", val$model.pic);
            ((Intent) (obj)).putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            FocusImageAdapter.access$300(FocusImageAdapter.this).startActivity(((Intent) (obj)));
            return;
        }
        continue; /* Loop/switch isn't completed */
_L6:
        String s6 = (new StringBuilder()).append(val$model.id).append("").toString();
        DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "userlist", s6);
        obj = new Bundle();
        ((Bundle) (obj)).putLong("id", val$model.id);
        ((Bundle) (obj)).putInt("type", val$model.type);
        ((Bundle) (obj)).putString("title", val$model.shortTitle);
        ((Bundle) (obj)).putInt("from", 1);
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        ((MainTabActivity2)FocusImageAdapter.access$300(FocusImageAdapter.this)).startFragment(com/ximalaya/ting/android/fragment/findings/FocusPersonListFragmentNew, ((Bundle) (obj)));
        return;
_L7:
        String s7 = (new StringBuilder()).append(val$model.id).append("").toString();
        DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "albumlist", s7);
        obj = new Bundle();
        ((Bundle) (obj)).putLong("id", val$model.id);
        ((Bundle) (obj)).putInt("type", val$model.type);
        ((Bundle) (obj)).putString("title", val$model.shortTitle);
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        ((MainTabActivity2)FocusImageAdapter.access$300(FocusImageAdapter.this)).startFragment(com/ximalaya/ting/android/fragment/findings/FocusAlbumListFragmentNew, ((Bundle) (obj)));
        return;
_L8:
        String s8 = (new StringBuilder()).append(val$model.id).append("").toString();
        DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "tracklist", s8);
        obj = new Bundle();
        ((Bundle) (obj)).putString("id", (new StringBuilder()).append(val$model.id).append("").toString());
        ((Bundle) (obj)).putString("type", (new StringBuilder()).append(val$model.type).append("").toString());
        ((Bundle) (obj)).putString("title", val$model.shortTitle);
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        ((MainTabActivity2)FocusImageAdapter.access$300(FocusImageAdapter.this)).startFragment(com/ximalaya/ting/android/fragment/findings/FocusSoundListFragment, ((Bundle) (obj)));
        return;
_L9:
        if (!TextUtils.isEmpty(val$model.url))
        {
            String s9 = val$model.url;
            DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "activity", s9);
            obj = new Bundle();
            ((Bundle) (obj)).putString("ExtraUrl", val$model.url);
            ((Bundle) (obj)).putInt("web_view_type", val$model.type);
            ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            ((MainTabActivity2)FocusImageAdapter.access$300(FocusImageAdapter.this)).startFragment(com/ximalaya/ting/android/fragment/web/WebFragment, ((Bundle) (obj)));
            return;
        }
        if (true) goto _L1; else goto _L10
_L10:
        String s10 = (new StringBuilder()).append(val$model.specialId).append("").toString();
        DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "subject", s10);
        obj = new Bundle();
        ((Bundle) (obj)).putLong("subjectId", val$model.specialId);
        ((Bundle) (obj)).putInt("contentType", val$model.subType);
        ((Bundle) (obj)).putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
        ((MainTabActivity2)FocusImageAdapter.access$300(FocusImageAdapter.this)).startFragment(com/ximalaya/ting/android/fragment/subject/SubjectFragment, ((Bundle) (obj)));
        return;
_L11:
        if (!TextUtils.isEmpty(val$model.url))
        {
            view = val$model.url;
            DataCollectUtil.getInstance(FocusImageAdapter.access$300(FocusImageAdapter.this)).statFocus(((String) (obj1)), s, s1, ((String) (obj)), "ad", view);
            FocusImageAdapter.access$400(FocusImageAdapter.this, val$model);
            return;
        }
        if (true) goto _L1; else goto _L12
_L12:
    }

    ment()
    {
        this$0 = final_focusimageadapter;
        val$pos = i;
        val$model = FocusImageModelNew.this;
        super();
    }
}
