// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.finding;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.finding.FindingCategoryModel;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Utilities;
import java.util.HashMap;
import java.util.List;

public class CategoryAdapter extends BaseAdapter
{
    public static interface OnCellClickListener
    {

        public abstract void onCellClick(int i, FindingCategoryModel findingcategorymodel, View view);
    }

    private static class ViewHolder
    {

        public View borderSpace;
        public View leftCell;
        public ImageView leftCellIv;
        public TextView leftCellTv;
        public View leftSpace;
        public View rightCell;
        public ImageView rightCellIv;
        public TextView rightCellTv;
        public View rightSpace;

        private ViewHolder()
        {
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private static final int COLUMNS = 2;
    private static final int DIVIDER = 1;
    private static final int GROUP_ITEM_COUNT = 6;
    private static final int GROUP_ROWS = 3;
    private static final int ITEM = 0;
    private static HashMap sDefaultDrawables;
    private OnCellClickListener mCellClickListener;
    private Context mContext;
    private List mData;
    private LayoutInflater mInflater;

    public CategoryAdapter(Context context, List list)
    {
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mData = list;
        buildDefaultDrawable();
    }

    private void buildDefaultDrawable()
    {
        if (sDefaultDrawables == null)
        {
            sDefaultDrawables = new HashMap();
            sDefaultDrawables.put("book", Integer.valueOf(0x7f020141));
            sDefaultDrawables.put("music", Integer.valueOf(0x7f02013f));
            sDefaultDrawables.put("entertainment", Integer.valueOf(0x7f02012f));
            sDefaultDrawables.put("comic", Integer.valueOf(0x7f02011a));
            sDefaultDrawables.put("news", Integer.valueOf(0x7f020140));
            sDefaultDrawables.put("emotion", Integer.valueOf(0x7f02013a));
            sDefaultDrawables.put("culture", Integer.valueOf(0x7f020137));
            sDefaultDrawables.put("train", Integer.valueOf(0x7f020139));
            sDefaultDrawables.put("chair", Integer.valueOf(0x7f020148));
            sDefaultDrawables.put("baijia", Integer.valueOf(0x7f020147));
            sDefaultDrawables.put("radioplay", Integer.valueOf(0x7f020145));
            sDefaultDrawables.put("opera", Integer.valueOf(0x7f020119));
            sDefaultDrawables.put("kid", Integer.valueOf(0x7f020117));
            sDefaultDrawables.put("radio", Integer.valueOf(0x7f020144));
            sDefaultDrawables.put("finance", Integer.valueOf(0x7f020131));
            sDefaultDrawables.put("it", Integer.valueOf(0x7f020138));
            sDefaultDrawables.put("health", Integer.valueOf(0x7f020135));
            sDefaultDrawables.put("xiaoyuan", Integer.valueOf(0x7f020146));
            sDefaultDrawables.put("qiche", Integer.valueOf(0x7f020116));
            sDefaultDrawables.put("lvyou", Integer.valueOf(0x7f020149));
            sDefaultDrawables.put("dianying", Integer.valueOf(0x7f02013d));
            sDefaultDrawables.put("youxi", Integer.valueOf(0x7f020132));
            sDefaultDrawables.put("other", Integer.valueOf(0x7f020142));
        }
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        }
        int i;
        if (mData.size() % 2 == 0)
        {
            i = mData.size() / 2;
        } else
        {
            i = mData.size() / 2 + 1;
        }
        if (mData.size() % 6 == 0)
        {
            return i + mData.size() / 6;
        } else
        {
            return i + (mData.size() / 6 + 1);
        }
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public int getItemViewType(int i)
    {
        while ((i - i / 4) * 2 >= mData.size() || (i + 1) % 4 == 0) 
        {
            return 1;
        }
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        int j = 0x7f020142;
        if (getItemViewType(i) == 1)
        {
            viewgroup = view;
            if (view == null)
            {
                viewgroup = new View(mContext);
                viewgroup.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, Utilities.dip2px(mContext, 10F)));
            }
            return viewgroup;
        }
        android.view.ViewGroup.LayoutParams layoutparams;
        final int row;
        if (view == null)
        {
            view = mInflater.inflate(0x7f030123, viewgroup, false);
            viewgroup = new ViewHolder(null);
            viewgroup.leftCell = view.findViewById(0x7f0a048d);
            viewgroup.leftCellIv = (ImageView)view.findViewById(0x7f0a048f);
            viewgroup.leftCellTv = (TextView)view.findViewById(0x7f0a0490);
            viewgroup.rightCell = view.findViewById(0x7f0a0492);
            viewgroup.rightCellIv = (ImageView)view.findViewById(0x7f0a0494);
            viewgroup.rightCellTv = (TextView)view.findViewById(0x7f0a0495);
            viewgroup.leftSpace = view.findViewById(0x7f0a048e);
            viewgroup.rightSpace = view.findViewById(0x7f0a0493);
            viewgroup.borderSpace = view.findViewById(0x7f0a0491);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        layoutparams = view.getLayoutParams();
        layoutparams.height = Utilities.dip2px(mContext, 45F);
        view.setLayoutParams(layoutparams);
        ((ViewHolder) (viewgroup)).leftSpace.setVisibility(8);
        ((ViewHolder) (viewgroup)).rightSpace.setVisibility(8);
        ((ViewHolder) (viewgroup)).borderSpace.setVisibility(8);
        row = i - i / 4;
        if (row * 2 < mData.size())
        {
            final FindingCategoryModel leftModel = (FindingCategoryModel)mData.get(row * 2);
            ((ViewHolder) (viewgroup)).leftCellTv.setText(leftModel.getTitle());
            final FindingCategoryModel rightModel;
            if (sDefaultDrawables.get(leftModel.getName()) != null)
            {
                i = ((Integer)sDefaultDrawables.get(leftModel.getName())).intValue();
            } else
            {
                i = 0x7f020142;
            }
            if (!TextUtils.isEmpty(leftModel.getCoverPath()))
            {
                ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).leftCellIv, leftModel.getCoverPath(), i);
            } else
            {
                ((ViewHolder) (viewgroup)).leftCellIv.setImageResource(i);
            }
            ((ViewHolder) (viewgroup)).leftCell.setOnClickListener(new _cls1());
        }
        if (row * 2 + 1 < mData.size())
        {
            ((ViewHolder) (viewgroup)).rightCell.setVisibility(0);
            rightModel = (FindingCategoryModel)mData.get(row * 2 + 1);
            ((ViewHolder) (viewgroup)).rightCellTv.setText(rightModel.getTitle());
            i = j;
            if (sDefaultDrawables.get(rightModel.getName()) != null)
            {
                i = ((Integer)sDefaultDrawables.get(rightModel.getName())).intValue();
            }
            if (!TextUtils.isEmpty(rightModel.getCoverPath()))
            {
                ImageManager2.from(mContext).displayImage(((ViewHolder) (viewgroup)).rightCellIv, rightModel.getCoverPath(), i);
            } else
            {
                ((ViewHolder) (viewgroup)).rightCellIv.setImageResource(i);
            }
            ((ViewHolder) (viewgroup)).rightCell.setOnClickListener(new _cls2());
            return view;
        } else
        {
            ((ViewHolder) (viewgroup)).rightCell.setVisibility(4);
            ((ViewHolder) (viewgroup)).rightCell.setClickable(false);
            return view;
        }
    }

    public int getViewTypeCount()
    {
        return 2;
    }

    public void setOnCellClickListener(OnCellClickListener oncellclicklistener)
    {
        mCellClickListener = oncellclicklistener;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final CategoryAdapter this$0;
        final FindingCategoryModel val$leftModel;
        final int val$row;

        public void onClick(View view)
        {
            if (mCellClickListener != null)
            {
                mCellClickListener.onCellClick(row * 2, leftModel, view);
            }
        }

        _cls1()
        {
            this$0 = CategoryAdapter.this;
            row = i;
            leftModel = findingcategorymodel;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final CategoryAdapter this$0;
        final FindingCategoryModel val$rightModel;
        final int val$row;

        public void onClick(View view)
        {
            if (mCellClickListener != null)
            {
                mCellClickListener.onCellClick(row * 2 + 1, rightModel, view);
            }
        }

        _cls2()
        {
            this$0 = CategoryAdapter.this;
            row = i;
            rightModel = findingcategorymodel;
            super();
        }
    }

}
