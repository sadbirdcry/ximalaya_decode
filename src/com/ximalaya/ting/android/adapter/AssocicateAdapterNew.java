// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.search.AssociateKeywordModel;
import com.ximalaya.ting.android.util.ImageManager2;
import java.util.List;

public class AssocicateAdapterNew extends BaseAdapter
{

    private Context mContext;
    private List mList;
    private int mType;

    public AssocicateAdapterNew(Context context, List list)
    {
        mList = list;
        mContext = context;
    }

    private View getAlbumView(AssociateKeywordModel associatekeywordmodel, View view, ViewGroup viewgroup)
    {
        View view1;
label0:
        {
            AssociateKeywordModel associatekeywordmodel1 = null;
            if (view != null)
            {
                associatekeywordmodel1 = (AssociateKeywordModel)view.getTag();
            }
            if (view != null)
            {
                view1 = view;
                if (associatekeywordmodel1 == null)
                {
                    break label0;
                }
                view1 = view;
                if (associatekeywordmodel1.isAlbum())
                {
                    break label0;
                }
            }
            view1 = LayoutInflater.from(mContext).inflate(0x7f030045, viewgroup, false);
            view1.setTag(associatekeywordmodel);
        }
        view = (ImageView)view1.findViewById(0x7f0a0177);
        ImageManager2.from(mContext).displayImage(view, associatekeywordmodel.getImgPath(), 0x7f0202e0);
        view = (TextView)view1.findViewById(0x7f0a0175);
        view.setSingleLine(true);
        view.setEllipsize(android.text.TextUtils.TruncateAt.valueOf("END"));
        view.setText(Html.fromHtml(highlight(associatekeywordmodel.getHighlightKeyword())));
        view = (TextView)view1.findViewById(0x7f0a0178);
        view.setSingleLine(true);
        view.setEllipsize(android.text.TextUtils.TruncateAt.valueOf("END"));
        view.setText(associatekeywordmodel.getCategory());
        return view1;
    }

    private View getSimpleView(AssociateKeywordModel associatekeywordmodel, View view, ViewGroup viewgroup)
    {
        View view1;
label0:
        {
            AssociateKeywordModel associatekeywordmodel1 = null;
            if (view != null)
            {
                associatekeywordmodel1 = (AssociateKeywordModel)view.getTag();
            }
            if (view != null)
            {
                view1 = view;
                if (associatekeywordmodel1 == null)
                {
                    break label0;
                }
                view1 = view;
                if (!associatekeywordmodel1.isAlbum())
                {
                    break label0;
                }
            }
            view1 = LayoutInflater.from(mContext).inflate(0x7f030044, viewgroup, false);
            view1.setTag(associatekeywordmodel);
        }
        view = (TextView)view1.findViewById(0x7f0a0175);
        view.setSingleLine(true);
        view.setEllipsize(android.text.TextUtils.TruncateAt.valueOf("END"));
        view.setText(Html.fromHtml(highlight(associatekeywordmodel.getHighlightKeyword())));
        return view1;
    }

    private String highlight(String s)
    {
        return s.replaceAll("<em>(.*?)</em>", "<font color=\"#f86442\">$1</font>");
    }

    public int getAdapterType()
    {
        return mType;
    }

    public int getCount()
    {
        return mList.size();
    }

    public Object getItem(int i)
    {
        return mList.get(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        Object obj;
label0:
        {
            Object obj1 = mList.get(i);
            obj = view;
            if (obj1 instanceof AssociateKeywordModel)
            {
                obj = (AssociateKeywordModel)obj1;
                if (!((AssociateKeywordModel) (obj)).isAlbum())
                {
                    break label0;
                }
                obj = getAlbumView(((AssociateKeywordModel) (obj)), view, viewgroup);
            }
            return ((View) (obj));
        }
        return getSimpleView(((AssociateKeywordModel) (obj)), view, viewgroup);
    }
}
