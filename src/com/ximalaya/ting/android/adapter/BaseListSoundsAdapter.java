// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.activity.comments.Send_CommentAct;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.comments.CommentListFragment;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.Likeable;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_setting.ThirdPartyUserInfo;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public abstract class BaseListSoundsAdapter extends BaseAdapter
{
    class LikeTask extends MyAsyncTask
    {

        boolean isFirstLike;
        boolean isShare;
        TextView like_tv;
        TextView likesNumTV;
        boolean paramFavorite;
        String shareList;
        String str;
        final BaseListSoundsAdapter this$0;
        long trackId;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground(aobj);
        }

        protected transient String doInBackground(Object aobj[])
        {
            trackId = ((Long)aobj[0]).longValue();
            isShare = ((Boolean)aobj[1]).booleanValue();
            paramFavorite = ((Boolean)aobj[2]).booleanValue();
            isFirstLike = ((Boolean)aobj[3]).booleanValue();
            like_tv = (TextView)aobj[4];
            likesNumTV = (TextView)aobj[5];
            if (loginInfoModel == null || loginInfoModel.bindStatus == null) goto _L2; else goto _L1
_L1:
            Object obj;
            obj = loginInfoModel.bindStatus;
            if (!isShare)
            {
                break MISSING_BLOCK_LABEL_336;
            }
            aobj = ((List) (obj)).iterator();
_L7:
            Object obj1;
            do
            {
                if (!((Iterator) (aobj)).hasNext())
                {
                    break MISSING_BLOCK_LABEL_336;
                }
                obj1 = (ThirdPartyUserInfo)((Iterator) (aobj)).next();
            } while (((ThirdPartyUserInfo) (obj1)).isExpired());
            if (!((ThirdPartyUserInfo) (obj1)).getIdentity().equals("1")) goto _L4; else goto _L3
_L3:
            if (!"".equals(shareList)) goto _L6; else goto _L5
_L5:
            shareList = "tSina";
              goto _L7
_L6:
            try
            {
                shareList = (new StringBuilder()).append(shareList).append("tSina").toString();
            }
            // Misplaced declaration of an exception variable
            catch (Object aobj[])
            {
                return "";
            }
              goto _L7
_L4:
            if (!((ThirdPartyUserInfo) (obj1)).getIdentity().equals("2")) goto _L9; else goto _L8
_L8:
            if (!"".equals(shareList))
            {
                break MISSING_BLOCK_LABEL_245;
            }
            shareList = "qzone";
              goto _L7
            shareList = (new StringBuilder()).append(shareList).append("qzone").toString();
              goto _L7
_L9:
            if (!((ThirdPartyUserInfo) (obj1)).getIdentity().equals("3")) goto _L7; else goto _L10
_L10:
            if (!"".equals(shareList))
            {
                break MISSING_BLOCK_LABEL_307;
            }
            shareList = "renren";
              goto _L7
            shareList = (new StringBuilder()).append(shareList).append("renren").toString();
              goto _L7
            if (!isFirstLike || paramFavorite) goto _L2; else goto _L11
_L11:
            aobj = ApiUtil.getApiHost();
            int i = 0;
_L18:
            if (i >= ((List) (obj)).size()) goto _L13; else goto _L12
_L12:
            aobj = (new StringBuilder()).append(((String) (aobj))).append("mobile/sync/").append(((ThirdPartyUserInfo)((List) (obj)).get(i)).thirdpartyId).append("/set").toString();
            obj1 = new RequestParams();
            ((RequestParams) (obj1)).put("type", "favorite");
            ((RequestParams) (obj1)).put("isChecked", String.valueOf(isShare));
            obj1 = f.a().b(((String) (aobj)), ((RequestParams) (obj1)), like_tv, like_tv);
            if (obj1 == null) goto _L15; else goto _L14
_L14:
            obj1 = JSON.parseObject(((String) (obj1)));
            if (obj1 == null) goto _L15; else goto _L16
_L16:
            if (((JSONObject) (obj1)).getInteger("ret").intValue() == 0)
            {
                ((ThirdPartyUserInfo)((List) (obj)).get(i)).setWebFavorite(isShare);
            }
              goto _L15
_L13:
            loginInfoModel.bindStatus = ((List) (obj));
            saveFirstLike();
_L2:
            aobj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/favorite/track").toString();
            obj = new RequestParams();
            ((RequestParams) (obj)).put("trackId", String.valueOf(trackId));
            boolean flag;
            if (!paramFavorite)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            ((RequestParams) (obj)).put("favorite", String.valueOf(flag));
            aobj = f.a().b(((String) (aobj)), ((RequestParams) (obj)), like_tv, like_tv);
            if (aobj == null)
            {
                break MISSING_BLOCK_LABEL_634;
            }
            aobj = JSON.parseObject(((String) (aobj)));
            if (((JSONObject) (aobj)).getInteger("ret").intValue() == 0)
            {
                return "0";
            }
            aobj = ((JSONObject) (aobj)).getString("msg");
            return ((String) (aobj));
            return "";
_L15:
            i++;
            if (true) goto _L18; else goto _L17
_L17:
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if (mContext == null || mContext.isFinishing())
            {
                return;
            }
            if ("0".equals(s))
            {
                SoundInfo soundinfo;
                long l;
                boolean flag;
                if (!paramFavorite)
                {
                    str = "\u8D5E\u6210\u529F";
                } else
                {
                    str = "\u53D6\u6D88\u8D5E\u6210\u529F";
                }
                s = DownloadHandler.getInstance(mContext.getApplicationContext());
                l = trackId;
                if (!paramFavorite)
                {
                    flag = true;
                } else
                {
                    flag = false;
                }
                s.updateFavorited(l, flag, true);
                s = LocalMediaService.getInstance();
                soundinfo = PlayListControl.getPlayListManager().getCurSound();
                if (soundinfo != null && soundinfo.trackId == trackId && s != null)
                {
                    SoundInfo soundinfo1 = new SoundInfo();
                    soundinfo1.trackId = soundinfo.trackId;
                    if (!paramFavorite)
                    {
                        soundinfo.is_favorited = true;
                        soundinfo.favorites_counts = soundinfo.favorites_counts + 1;
                    } else
                    {
                        soundinfo.is_favorited = false;
                        soundinfo.favorites_counts = soundinfo.favorites_counts - 1;
                    }
                    soundinfo1.is_favorited = soundinfo.is_favorited;
                    soundinfo1.favorites_counts = soundinfo.favorites_counts;
                    s.updateOnSoundInfoModified(PlayListControl.getPlayListManager().curIndex, soundinfo1);
                }
            } else
            if ("".equals(s))
            {
                str = "\u7F51\u7EDC\u8FDE\u63A5\u5931\u8D25";
            } else
            {
                str = s;
            }
            Toast.makeText(mContext.getApplicationContext(), str, 0).show();
        }

        LikeTask()
        {
            this$0 = BaseListSoundsAdapter.this;
            super();
            str = "";
            shareList = "";
        }
    }

    protected static class ViewHolder
    {

        public ImageView adIndicator;
        public View border;
        public ImageButton btn;
        public TextView commentCount;
        public ImageView cover;
        public TextView createTime;
        public ImageView download;
        public TextView duration;
        public TextView likeCount;
        public ImageView newUpdate;
        public TextView origin;
        public TextView owner;
        public TextView playCount;
        public ImageView playFlag;
        public int position;
        public TextView title;
        public TextView transmitCount;

        protected ViewHolder()
        {
        }
    }


    protected LayoutInflater inflater;
    public LoginInfoModel loginInfoModel;
    public Activity mContext;
    protected List mData;
    protected String mDataSourceUrl;
    public Handler mHandler;
    protected int mPageId;
    protected HashMap mParams;

    public BaseListSoundsAdapter(Activity activity, List list)
    {
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        mContext = activity;
        inflater = LayoutInflater.from(mContext);
        mData = list;
    }

    private void doSomeThingAfterToLike(boolean flag, TextView textview, TextView textview1)
    {
        if (!flag)
        {
            if (textview1 != null)
            {
                textview1.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020090), null, null, null);
            }
            if (textview != null)
            {
                textview.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020217), null, null, null);
                textview.setText("\u53D6\u6D88");
            }
        } else
        {
            if (textview != null)
            {
                textview.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020213), null, null, null);
                textview.setText("\u8D5E");
            }
            if (textview1 != null)
            {
                textview1.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020092), null, null, null);
                return;
            }
        }
    }

    private boolean isFirstLike()
    {
        return SharedPreferencesUtil.getInstance(mContext.getApplicationContext()).getBoolean("isFirstLike", true);
    }

    private void saveFirstLike()
    {
        SharedPreferencesUtil.getInstance(mContext.getApplicationContext()).saveBoolean("isFirstLike", false);
    }

    public void addData(List list)
    {
        mData.addAll(list);
        notifyDataSetChanged();
    }

    protected abstract void bindData(Object obj, ViewHolder viewholder);

    protected View buildItemView(int i, View view, ViewGroup viewgroup)
    {
        if (view == null)
        {
            view = inflater.inflate(0x7f03013e, viewgroup, false);
            viewgroup = findViewsRef(view, new ViewHolder());
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        if (i + 1 == mData.size())
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(8);
        } else
        {
            ((ViewHolder) (viewgroup)).border.setVisibility(0);
        }
        viewgroup.position = i;
        return view;
    }

    protected ViewHolder findViewsRef(View view, ViewHolder viewholder)
    {
        viewholder.cover = (ImageView)view.findViewById(0x7f0a0177);
        viewholder.title = (TextView)view.findViewById(0x7f0a0218);
        viewholder.owner = (TextView)view.findViewById(0x7f0a0219);
        viewholder.btn = (ImageButton)view.findViewById(0x7f0a04b0);
        viewholder.createTime = (TextView)view.findViewById(0x7f0a0216);
        viewholder.playCount = (TextView)view.findViewById(0x7f0a021a);
        viewholder.likeCount = (TextView)view.findViewById(0x7f0a021c);
        viewholder.commentCount = (TextView)view.findViewById(0x7f0a021d);
        viewholder.duration = (TextView)view.findViewById(0x7f0a021b);
        viewholder.origin = (TextView)view.findViewById(0x7f0a0217);
        viewholder.playFlag = (ImageView)view.findViewById(0x7f0a0022);
        viewholder.newUpdate = (ImageView)view.findViewById(0x7f0a023e);
        viewholder.transmitCount = (TextView)view.findViewById(0x7f0a021e);
        viewholder.adIndicator = (ImageView)view.findViewById(0x7f0a0255);
        viewholder.border = view.findViewById(0x7f0a00a8);
        return viewholder;
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public List getData()
    {
        return mData;
    }

    public Object getItem(int i)
    {
        if (mData == null || mData.size() < i + 1)
        {
            return null;
        } else
        {
            return mData.get(i);
        }
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        view = buildItemView(i, view, viewgroup);
        bindData(mData.get(i), (ViewHolder)view.getTag());
        return view;
    }

    public void handleItemLongClick(final Likeable likeable, final View view)
    {
        String as[] = new String[2];
        as[0] = "\u70B9\u8D5E";
        as[1] = "\u8BC4\u8BBA";
        if (likeable.isLiked())
        {
            as[0] = "\u53D6\u6D88\u70B9\u8D5E";
        }
        final MenuDialog dlg = new MenuDialog(mContext, Arrays.asList(as));
        dlg.setOnItemClickListener(new _cls3());
        dlg.show();
    }

    public boolean isDownload(long l)
    {
        if (mContext != null) goto _L2; else goto _L1
_L1:
        Object obj;
        return false;
_L2:
        if ((obj = DownloadHandler.getInstance(mContext.getApplicationContext()).downloadList) == null || ((List) (obj)).size() <= 0) goto _L1; else goto _L3
_L3:
        obj = ((List) (obj)).iterator();
_L7:
        if (!((Iterator) (obj)).hasNext()) goto _L5; else goto _L4
_L4:
        long l1 = ((DownloadTask)((Iterator) (obj)).next()).trackId;
        if (l1 != l) goto _L7; else goto _L6
_L6:
        boolean flag = true;
_L9:
        return flag;
        Exception exception;
        exception;
        flag = false;
        continue; /* Loop/switch isn't completed */
_L5:
        flag = false;
        if (true) goto _L9; else goto _L8
_L8:
    }

    public boolean isPlaying(long l)
    {
        return PlayListControl.getPlayListManager().getCurSound() != null && l == PlayListControl.getPlayListManager().getCurSound().trackId;
    }

    public void playSound(View view, int i, SoundInfo soundinfo, List list)
    {
        ImageView imageview = (ImageView)view;
        LocalMediaService localmediaservice = LocalMediaService.getInstance();
        SoundInfo soundinfo1 = PlayListControl.getPlayListManager().getCurSound();
        if (localmediaservice == null || soundinfo == null)
        {
            return;
        }
        if (soundinfo1 == null)
        {
            PlayTools.gotoPlay(1, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
            imageview.setImageResource(0x7f02024f);
            return;
        }
        switch (localmediaservice.getPlayServiceState())
        {
        default:
            return;

        case 0: // '\0'
            PlayTools.gotoPlay(1, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
            imageview.setImageResource(0x7f02024f);
            return;

        case 1: // '\001'
        case 3: // '\003'
            if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.trackId == soundinfo1.trackId)
            {
                localmediaservice.pause();
                imageview.setImageResource(0x7f020250);
                return;
            } else
            {
                PlayTools.gotoPlay(1, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
                return;
            }

        case 2: // '\002'
            break;
        }
        if (!TextUtils.isEmpty(localmediaservice.getCurrentUrl()) && soundinfo.trackId == soundinfo1.trackId)
        {
            localmediaservice.start();
            imageview.setImageResource(0x7f02024f);
            return;
        } else
        {
            PlayTools.gotoPlay(1, mDataSourceUrl, mPageId, mParams, list, i, mContext, false, DataCollectUtil.getDataFromView(view));
            return;
        }
    }

    public void setData(List list)
    {
        mData = list;
        notifyDataSetChanged();
    }

    public void setDataSource(String s)
    {
        mDataSourceUrl = s;
    }

    public void setPageId(int i)
    {
        mPageId = i;
    }

    public void setParams(HashMap hashmap)
    {
        mParams = hashmap;
    }

    public void toComment(long l)
    {
        Bundle bundle = new Bundle();
        bundle.putString("trackId", (new StringBuilder()).append("").append(l).toString());
        bundle.putInt("flag", 1);
        if (mContext instanceof MainTabActivity2)
        {
            ((MainTabActivity2)mContext).startFragment(com/ximalaya/ting/android/fragment/comments/CommentListFragment, bundle);
        }
    }

    public void toLike(final long trackId, final boolean isLike, final TextView like_tv, final TextView likesNumTV)
    {
        final boolean isFirstLike = isFirstLike();
        loginInfoModel = UserInfoMannage.getInstance().getUser();
        if (loginInfoModel == null)
        {
            return;
        }
        if (isFirstLike && !isLike)
        {
            if (loginInfoModel.bindStatus != null && loginInfoModel.bindStatus.size() > 0)
            {
                (new DialogBuilder(mContext)).setMessage("\u5206\u4EAB\u4F60\u559C\u6B22\u7684\u58F0\u97F3\uFF0C\u70B9\u51FB\u540C\u6B65\u5206\u4EAB\u5230\u65B0\u6D6A/QQ\u5FAE\u535A\u3002\u4F60\u4E5F\u53EF\u4EE5\u5728\u5206\u4EAB\u8BBE\u7F6E\u91CC\u5173\u95ED\u540C\u6B65\u5F00\u5173\u54E6\uFF01").setOkBtn("\u540C\u6B65", new _cls2()).setCancelBtn(new _cls1()).showConfirm();
            } else
            {
                (new LikeTask()).myexec(new Object[] {
                    Long.valueOf(trackId), Boolean.valueOf(true), Boolean.valueOf(isLike), Boolean.valueOf(isFirstLike), like_tv, likesNumTV
                });
            }
        } else
        {
            (new LikeTask()).myexec(new Object[] {
                Long.valueOf(trackId), Boolean.valueOf(true), Boolean.valueOf(isLike), Boolean.valueOf(isFirstLike), like_tv, likesNumTV
            });
        }
        doSomeThingAfterToLike(isLike, like_tv, likesNumTV);
    }

    public void toTransmit(long l, boolean flag, TextView textview, int i)
    {
        textview = new Intent(mContext, com/ximalaya/ting/android/activity/comments/Send_CommentAct);
        textview.putExtra("trackId", (new StringBuilder()).append("").append(l).toString());
        textview.putExtra("flag", 2);
        textview.putExtra("position", i);
        textview.setFlags(0x20000000);
        mContext.startActivityForResult(textview, 1);
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final BaseListSoundsAdapter this$0;
        final MenuDialog val$dlg;
        final Likeable val$likeable;
        final View val$view;

        public void onItemClick(AdapterView adapterview, View view1, int i, long l)
        {
            if (dlg != null)
            {
                dlg.dismiss();
            }
            i;
            JVM INSTR tableswitch 0 1: default 36
        //                       0 37
        //                       1 141;
               goto _L1 _L2 _L3
_L1:
            return;
_L2:
            if (!UserInfoMannage.hasLogined())
            {
                adapterview = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                mContext.startActivity(adapterview);
                return;
            }
            if (likeable != null)
            {
                toLike(likeable.getId(), likeable.isLiked(), null, (TextView)view.findViewById(0x7f0a021c));
                likeable.toggleLikeStatus();
                return;
            }
              goto _L1
_L3:
            toComment(likeable.getId());
            return;
        }

        _cls3()
        {
            this$0 = BaseListSoundsAdapter.this;
            dlg = menudialog;
            view = view1;
            likeable = likeable1;
            super();
        }
    }


    private class _cls2
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final BaseListSoundsAdapter this$0;
        final boolean val$isFirstLike;
        final boolean val$isLike;
        final TextView val$like_tv;
        final TextView val$likesNumTV;
        final long val$trackId;

        public void onExecute()
        {
            (new LikeTask()).myexec(new Object[] {
                Long.valueOf(trackId), Boolean.valueOf(true), Boolean.valueOf(isLike), Boolean.valueOf(isFirstLike), like_tv, likesNumTV
            });
        }

        _cls2()
        {
            this$0 = BaseListSoundsAdapter.this;
            trackId = l;
            isLike = flag;
            isFirstLike = flag1;
            like_tv = textview;
            likesNumTV = textview1;
            super();
        }
    }


    private class _cls1
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final BaseListSoundsAdapter this$0;
        final boolean val$isFirstLike;
        final boolean val$isLike;
        final TextView val$like_tv;
        final TextView val$likesNumTV;
        final long val$trackId;

        public void onExecute()
        {
            (new LikeTask()).myexec(new Object[] {
                Long.valueOf(trackId), Boolean.valueOf(false), Boolean.valueOf(isLike), Boolean.valueOf(isFirstLike), like_tv, likesNumTV
            });
        }

        _cls1()
        {
            this$0 = BaseListSoundsAdapter.this;
            trackId = l;
            isLike = flag;
            isFirstLike = flag1;
            like_tv = textview;
            likesNumTV = textview1;
            super();
        }
    }

}
