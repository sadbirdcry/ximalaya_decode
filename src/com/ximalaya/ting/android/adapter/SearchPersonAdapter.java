// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.search.SearchPerson;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

public class SearchPersonAdapter extends BaseAdapter
{
    private class PersonHolder
    {

        View border;
        ImageView cover;
        TextView dsscription;
        ToggleButton follow;
        TextView funs;
        TextView name;
        int position;
        TextView sounds;
        final SearchPersonAdapter this$0;

        private PersonHolder()
        {
            this$0 = SearchPersonAdapter.this;
            super();
        }

        PersonHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private Activity mActiviry;
    private List mData;

    public SearchPersonAdapter(Activity activity, List list)
    {
        mActiviry = activity;
        mData = list;
    }

    private View getPersonView(int i, View view, ViewGroup viewgroup)
    {
        Object obj = mData.get(i);
        viewgroup = view;
        if (view == null)
        {
            viewgroup = View.inflate(mActiviry, 0x7f030099, null);
            view = new PersonHolder(null);
            view.cover = (ImageView)viewgroup.findViewById(0x7f0a0268);
            view.name = (TextView)viewgroup.findViewById(0x7f0a0269);
            view.sounds = (TextView)viewgroup.findViewById(0x7f0a026b);
            view.funs = (TextView)viewgroup.findViewById(0x7f0a026c);
            view.dsscription = (TextView)viewgroup.findViewById(0x7f0a026d);
            view.follow = (ToggleButton)viewgroup.findViewById(0x7f0a01b9);
            ((PersonHolder) (view)).follow.setVisibility(8);
            view.border = viewgroup.findViewById(0x7f0a00a8);
            ((PersonHolder) (view)).follow.setOnClickListener(new _cls1());
            viewgroup.setTag(view);
        }
        view = (PersonHolder)viewgroup.getTag();
        if (obj instanceof SearchPerson)
        {
            if (i + 1 == mData.size())
            {
                ((PersonHolder) (view)).border.setVisibility(4);
            } else
            {
                viewgroup.findViewById(0x7f0a00a8).setVisibility(0);
            }
            obj = (SearchPerson)obj;
            if (((SearchPerson) (obj)).isVerified)
            {
                ((PersonHolder) (view)).name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f0200c0, 0);
            } else
            {
                ((PersonHolder) (view)).name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            UserInfoMannage.getInstance().getUser();
            ImageManager2.from(mActiviry).displayImage(((PersonHolder) (view)).cover, ((SearchPerson) (obj)).smallPic, 0x7f0202df);
            ((PersonHolder) (view)).name.setText(((SearchPerson) (obj)).nickname);
            ((PersonHolder) (view)).sounds.setText((new StringBuilder()).append("\u58F0\u97F3  ").append(StringUtil.getFriendlyNumStr(((SearchPerson) (obj)).tracks_counts)).toString());
            ((PersonHolder) (view)).funs.setText((new StringBuilder()).append("\u7C89\u4E1D  ").append(StringUtil.getFriendlyNumStr(((SearchPerson) (obj)).followers_counts)).toString());
            if (Utilities.isBlank(((SearchPerson) (obj)).intro))
            {
                ((PersonHolder) (view)).dsscription.setVisibility(8);
            } else
            {
                ((PersonHolder) (view)).dsscription.setVisibility(0);
                ((PersonHolder) (view)).dsscription.setText(((SearchPerson) (obj)).intro);
            }
            ((PersonHolder) (view)).follow.setChecked(((SearchPerson) (obj)).is_follow);
            view.position = i;
            ((PersonHolder) (view)).follow.setTag(0x7f090000, view);
        }
        return viewgroup;
    }

    private void toFollow(final SearchPerson sp, final ToggleButton btn)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("toUid", (new StringBuilder()).append("").append(sp.uid).toString());
        StringBuilder stringbuilder = (new StringBuilder()).append("");
        boolean flag;
        if (!sp.is_follow)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        requestparams.put("isFollow", stringbuilder.append(flag).toString());
        f.a().b("mobile/follow", requestparams, DataCollectUtil.getDataFromView(btn), new _cls2());
    }

    public int getCount()
    {
        if (mData == null)
        {
            return 0;
        } else
        {
            return mData.size();
        }
    }

    public Object getItem(int i)
    {
        return mData.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public int getItemViewType(int i)
    {
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        return getPersonView(i, view, viewgroup);
    }

    public int getViewTypeCount()
    {
        return 1;
    }




    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SearchPersonAdapter this$0;

        public void onClick(View view)
        {
            if (!UserInfoMannage.hasLogined())
            {
                Intent intent = new Intent(mActiviry, com/ximalaya/ting/android/activity/login/LoginActivity);
                intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                mActiviry.startActivity(intent);
            } else
            {
                Object obj = (PersonHolder)view.getTag(0x7f090000);
                if (obj != null)
                {
                    obj = mData.get(((PersonHolder) (obj)).position);
                    if (obj != null && (obj instanceof SearchPerson))
                    {
                        obj = (SearchPerson)obj;
                        toFollow(((SearchPerson) (obj)), (ToggleButton)view);
                        return;
                    }
                }
            }
        }

        _cls1()
        {
            this$0 = SearchPersonAdapter.this;
            super();
        }
    }


    private class _cls2 extends a
    {

        final SearchPersonAdapter this$0;
        final ToggleButton val$btn;
        final SearchPerson val$sp;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, btn);
        }

        public void onFinish()
        {
            super.onFinish();
            btn.setChecked(sp.is_follow);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(mActiviry, "\u4EB2\uFF0C\u7F51\u7EDC\u4E0D\u7ED9\u529B\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
        }

        public void onStart()
        {
            super.onStart();
            btn.setChecked(sp.is_follow);
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s))
            {
                Object obj = null;
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = obj;
                }
                if (s != null && s.getIntValue("ret") == 0)
                {
                    s = sp;
                    boolean flag;
                    if (!sp.is_follow)
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                    s.is_follow = flag;
                    if (sp.is_follow)
                    {
                        s = "\u5173\u6CE8\u6210\u529F\uFF01";
                    } else
                    {
                        s = "\u53D6\u6D88\u5173\u6CE8\u6210\u529F\uFF01";
                    }
                    Toast.makeText(mActiviry, s, 0).show();
                    return;
                }
            }
        }

        _cls2()
        {
            this$0 = SearchPersonAdapter.this;
            btn = togglebutton;
            sp = searchperson;
            super();
        }
    }

}
