// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class DownloadedSoundListAdapter extends BaseListSoundsAdapter
{
    public static interface DelDownloadListener
    {

        public abstract void remove();
    }


    private DelDownloadListener delDownload;

    public DownloadedSoundListAdapter(Activity activity, List list)
    {
        super(activity, list);
    }

    private void delDownLoad(DownloadTask downloadtask)
    {
        if (downloadtask != null)
        {
            DownloadHandler downloadhandler = DownloadHandler.getInstance(mContext);
            PlayListControl.getPlayListManager().doBeforeDelete(downloadtask);
            if (downloadhandler.delDownloadTask(downloadtask) == 0 && delDownload != null)
            {
                delDownload.remove();
            }
        }
    }

    protected void bindData(final DownloadTask info, final BaseListSoundsAdapter.ViewHolder holder)
    {
        holder.createTime.setTextColor(Color.parseColor("#999999"));
        holder.createTime.setTextSize(2, 10F);
        boolean flag;
        boolean flag1;
        boolean flag2;
        if (isPlaying(info.trackId))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        if (info.filesize > 0L)
        {
            holder.createTime.setVisibility(0);
            holder.createTime.setText(ToolUtil.formatFileSize(info.filesize));
        } else
        {
            holder.createTime.setVisibility(8);
        }
        if (isPlaying(info.trackId) && LocalMediaService.getInstance().isPlaying())
        {
            holder.playFlag.setImageResource(0x7f02024f);
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        holder.title.setText(info.title);
        holder.owner.setText((new StringBuilder()).append("by ").append(info.nickname).toString());
        holder.origin.setVisibility(8);
        if (info.plays_counts > 0)
        {
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.plays_counts));
            holder.playCount.setVisibility(0);
            flag1 = true;
        } else
        {
            holder.playCount.setVisibility(8);
            flag1 = false;
        }
        if (info.favorites_counts > 0)
        {
            if (!flag1)
            {
                holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.favorites_counts));
            }
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.favorites_counts));
            holder.likeCount.setVisibility(0);
            if (info.is_favorited)
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020090), null, null, null);
                flag = true;
            } else
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020092), null, null, null);
                flag = true;
            }
        } else
        {
            holder.likeCount.setVisibility(8);
            flag = false;
        }
        if (info.comments_counts > 0)
        {
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.comments_counts));
            holder.commentCount.setVisibility(0);
            flag2 = true;
        } else
        {
            holder.commentCount.setVisibility(8);
            flag2 = false;
        }
        if (flag1 || flag || flag2 || info.duration > 0.0D)
        {
            holder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)info.duration)).toString());
            holder.duration.setVisibility(0);
        } else
        {
            holder.duration.setVisibility(8);
        }
        holder.cover.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(holder.cover, info.coverSmall, 0x7f0202de);
        holder.cover.setOnClickListener(new _cls1());
        holder.btn.setImageResource(0x7f0201bd);
        holder.btn.setOnClickListener(new _cls2());
        if (holder.position + 1 == mData.size())
        {
            holder.border.setVisibility(4);
            return;
        } else
        {
            holder.border.setVisibility(0);
            return;
        }
    }

    protected volatile void bindData(Object obj, BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((DownloadTask)obj, viewholder);
    }

    public DelDownloadListener getDelDownload()
    {
        return delDownload;
    }

    public void setDelDownloadListener(DelDownloadListener deldownloadlistener)
    {
        delDownload = deldownloadlistener;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final DownloadedSoundListAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final DownloadTask val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, mData.indexOf(info), ModelHelper.toSoundInfo(info), ModelHelper.downloadlistToSoundInfoList(mData));
        }

        _cls1()
        {
            this$0 = DownloadedSoundListAdapter.this;
            holder = viewholder;
            info = downloadtask;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final DownloadedSoundListAdapter this$0;
        final DownloadTask val$info;

        public void onClick(View view)
        {
            delDownLoad(info);
        }

        _cls2()
        {
            this$0 = DownloadedSoundListAdapter.this;
            info = downloadtask;
            super();
        }
    }

}
