// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.transaction.download.DownloadHandler;
import com.ximalaya.ting.android.util.MyAsyncTask;
import java.util.HashMap;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            SoundsDownloadForAlbumAdapter

class pd extends MyAsyncTask
{

    ProgressDialog pd;
    final e this$1;

    protected transient Boolean doInBackground(Void avoid[])
    {
        avoid = (List)albumMap.get(Long.valueOf(key));
        if (avoid != null)
        {
            DownloadHandler downloadhandler = DownloadHandler.getInstance(mContext);
            if (downloadhandler != null)
            {
                downloadhandler.delDownloadTasks(avoid);
            }
        }
        return Boolean.valueOf(true);
    }

    protected volatile Object doInBackground(Object aobj[])
    {
        return doInBackground((Void[])aobj);
    }

    protected void onPostExecute(Boolean boolean1)
    {
        while (mContext == null || ((Activity)mContext).isFinishing() || pd == null) 
        {
            return;
        }
        pd.cancel();
        pd = null;
    }

    protected volatile void onPostExecute(Object obj)
    {
        onPostExecute((Boolean)obj);
    }

    protected void onPreExecute()
    {
        super.onPreExecute();
        pd = new MyProgressDialog(mContext);
        pd.setMessage("\u6B63\u5728\u6E05\u9664\u8BE5\u4E13\u8F91\u5217\u8868\u7684\u58F0\u97F3\uFF0C\u8BF7\u7B49\u5F85...");
        pd.setCanceledOnTouchOutside(false);
        class _cls1
            implements android.content.DialogInterface.OnKeyListener
        {

            final SoundsDownloadForAlbumAdapter._cls1._cls1 this$2;

            public boolean onKey(DialogInterface dialoginterface, int i, KeyEvent keyevent)
            {
                return true;
            }

            _cls1()
            {
                this$2 = SoundsDownloadForAlbumAdapter._cls1._cls1.this;
                super();
            }
        }

        pd.setOnKeyListener(new _cls1());
        pd.show();
        List list = (List)albumMap.get(Long.valueOf(key));
        PlayListControl.getPlayListManager().doBeforeDelete(list);
    }

    _cls1()
    {
        this$1 = this._cls1.this;
        super();
        pd = null;
    }
}
