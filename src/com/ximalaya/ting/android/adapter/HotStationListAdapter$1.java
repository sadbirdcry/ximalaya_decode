// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.view.View;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            HotStationListAdapter

class this._cls0
    implements android.view.StationListAdapter._cls1
{

    final HotStationListAdapter this$0;

    public void onClick(View view)
    {
        StationModel stationmodel = (StationModel)view.getTag(0x7f090000);
        if (stationmodel != null)
        {
            if (stationmodel.tracks == null)
            {
                HotStationListAdapter.access$000(HotStationListAdapter.this, stationmodel, true, false, view);
                return;
            }
            com.ximalaya.ting.android.model.sound.SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
            LocalMediaService localmediaservice = LocalMediaService.getInstance();
            if (soundinfo != null && stationmodel.tracks.contains(HotStationListAdapter.access$100(HotStationListAdapter.this, soundinfo)) && !localmediaservice.isPaused())
            {
                localmediaservice.pause();
                return;
            }
            if (stationmodel.tracks.contains(HotStationListAdapter.access$100(HotStationListAdapter.this, soundinfo)) && localmediaservice.isPaused())
            {
                localmediaservice.start();
                return;
            }
            if (MyApplication.a() != null)
            {
                PlayTools.gotoPlay(0, null, 0, null, ModelHelper.toSoundInfo(stationmodel.tracks), 0, MyApplication.a(), false, DataCollectUtil.getDataFromView(view));
                return;
            }
        }
    }

    ()
    {
        this$0 = HotStationListAdapter.this;
        super();
    }
}
