// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.userspace;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

public class AlbumListAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        ImageView albumImageImageView;
        TextView albumNameTextView;
        TextView albumNumTextView;
        TextView albumStatusTextView;
        TextView albumUpdateTextView;

        private ViewHolder()
        {
        }
    }


    public List list;
    private Context mContext;
    private boolean mShowCollect;

    public AlbumListAdapter(Context context, List list1, boolean flag)
    {
        mContext = context;
        list = list1;
        mShowCollect = flag;
    }

    public int getCount()
    {
        if (list == null)
        {
            return 0;
        } else
        {
            return list.size();
        }
    }

    public AlbumModel getItem(int i)
    {
        if (list == null || i >= list.size())
        {
            return new AlbumModel();
        } else
        {
            return (AlbumModel)list.get(i);
        }
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, final View holder, ViewGroup viewgroup)
    {
        boolean flag1 = true;
        viewgroup = holder;
        if (holder == null)
        {
            viewgroup = AlbumItemHolder.getView(mContext);
        }
        holder = (AlbumItemHolder)viewgroup.getTag();
        final AlbumModel info = getItem(i);
        ((AlbumItemHolder) (holder)).name.setText(info.title);
        ImageManager2.from(mContext).displayImage(((AlbumItemHolder) (holder)).cover, info.coverLarge, 0x7f0202e0);
        ((AlbumItemHolder) (holder)).collectCount.setText((new StringBuilder()).append(info.tracks).append("\u96C6").toString());
        long l;
        boolean flag;
        if (info.lastUptrackAt == 0L)
        {
            l = info.updatedAt;
        } else
        {
            l = info.lastUptrackAt;
        }
        ((AlbumItemHolder) (holder)).updateAt.setText(mContext.getString(0x7f0901e7, new Object[] {
            ToolUtil.convertTime(l)
        }));
        AlbumItemHolder.setCollectStatus(holder, info.isFavorite);
        if (info.playTimes > 0)
        {
            ((AlbumItemHolder) (holder)).playCount.setVisibility(0);
            ((AlbumItemHolder) (holder)).playCount.setText(StringUtil.getFriendlyNumStr(info.playTimes));
        } else
        {
            ((AlbumItemHolder) (holder)).playCount.setVisibility(8);
        }
        if (mShowCollect)
        {
            ((AlbumItemHolder) (holder)).collect.setVisibility(0);
            ((AlbumItemHolder) (holder)).arrow.setVisibility(8);
        } else
        {
            ((AlbumItemHolder) (holder)).collect.setVisibility(8);
            ((AlbumItemHolder) (holder)).arrow.setVisibility(0);
        }
        ((AlbumItemHolder) (holder)).collect.setTag(0x7f090000, info);
        ((AlbumItemHolder) (holder)).collect.setOnClickListener(new _cls1());
        holder = mContext;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (i + 1 != list.size())
        {
            flag1 = false;
        }
        ViewUtil.buildAlbumItemSpace(holder, viewgroup, flag, flag1);
        return viewgroup;
    }

    public void releseData()
    {
        mContext = null;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final AlbumListAdapter this$0;
        final AlbumItemHolder val$holder;
        final AlbumModel val$info;

        public void onClick(View view)
        {
            AlbumOtherAdapter.doCollect(mContext, info, holder, view);
        }

        _cls1()
        {
            this$0 = AlbumListAdapter.this;
            info = albummodel;
            holder = albumitemholder;
            super();
        }
    }

}
