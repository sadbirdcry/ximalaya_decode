// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.model.holder.PersionStationBigHolder;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.service.play.PlayListControl;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.ArrayList;
import java.util.List;

public class HotStationListAdapter extends BaseAdapter
{

    private Context mContext;
    private BaseFragment mFragment;
    private ListView mListView;
    private ArrayList mStations;

    public HotStationListAdapter(BaseFragment basefragment, ListView listview, ArrayList arraylist)
    {
        mFragment = basefragment;
        mListView = listview;
        mStations = arraylist;
        mContext = mListView.getContext();
    }

    private void doFollow(final Context context, final StationModel model, final PersionStationBigHolder holder, final View view)
    {
        if (UserInfoMannage.getInstance().getUser() == null)
        {
            return;
        }
        RequestParams requestparams = new RequestParams();
        requestparams.put("toUid", (new StringBuilder()).append("").append(model.uid).toString());
        StringBuilder stringbuilder = (new StringBuilder()).append("");
        boolean flag;
        if (!model.isFollowed)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        requestparams.put("isFollow", stringbuilder.append(flag).toString());
        f.a().b("mobile/follow", requestparams, DataCollectUtil.getDataFromView(view), new _cls4());
    }

    private void loadAlbumSound(final StationModel model, final boolean play, final boolean showPlay, final View view)
    {
        if (model == null)
        {
            return;
        } else
        {
            RequestParams requestparams = new RequestParams();
            requestparams.put("uid", (new StringBuilder()).append("").append(model.uid).toString());
            f.a().a("m/prelisten", requestparams, DataCollectUtil.getDataFromView(view), new _cls3());
            return;
        }
    }

    private void setFollowStatus(PersionStationBigHolder persionstationbigholder, boolean flag)
    {
        if (flag)
        {
            persionstationbigholder.follow.setBackgroundResource(0x7f020266);
            persionstationbigholder.followImg.setVisibility(8);
            persionstationbigholder.followTxt.setText("\u5DF2\u5173\u6CE8");
            persionstationbigholder.followTxt.setTextColor(Color.parseColor("#ffffff"));
            return;
        } else
        {
            persionstationbigholder.follow.setBackgroundResource(0x7f020263);
            persionstationbigholder.followImg.setVisibility(0);
            persionstationbigholder.followTxt.setText("\u5173\u6CE8");
            persionstationbigholder.followTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    private SoundInfoNew toSoundInfoNew(SoundInfo soundinfo)
    {
        if (soundinfo == null)
        {
            return null;
        } else
        {
            SoundInfoNew soundinfonew = new SoundInfoNew();
            soundinfonew.id = soundinfo.trackId;
            return soundinfonew;
        }
    }

    public int getCount()
    {
        if (mStations == null)
        {
            return 0;
        } else
        {
            return mStations.size();
        }
    }

    public Object getItem(int i)
    {
        return mStations.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, final View holder, ViewGroup viewgroup)
    {
        viewgroup = holder;
        if (holder == null)
        {
            viewgroup = PersionStationBigHolder.getView(mContext);
            viewgroup.setLayoutParams(new android.widget.AbsListView.LayoutParams(-1, ToolUtil.dp2px(mContext, 145F)));
            holder = (PersionStationBigHolder)viewgroup.getTag();
            mFragment.markImageView(((PersionStationBigHolder) (holder)).cover);
            ((PersionStationBigHolder) (holder)).play.setOnClickListener(new _cls1());
            ((PersionStationBigHolder) (holder)).follow.setOnClickListener(new _cls2());
        }
        holder = (PersionStationBigHolder)viewgroup.getTag();
        StationModel stationmodel = (StationModel)mStations.get(i);
        ((PersionStationBigHolder) (holder)).cover.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(((PersionStationBigHolder) (holder)).cover, stationmodel.largeLogo, 0x7f0204ba);
        ((PersionStationBigHolder) (holder)).name.setText(stationmodel.nickname);
        SoundInfo soundinfo;
        LocalMediaService localmediaservice;
        if (!stationmodel.isVerified)
        {
            ((PersionStationBigHolder) (holder)).name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        } else
        {
            ((PersionStationBigHolder) (holder)).name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f0200c0, 0);
        }
        ((PersionStationBigHolder) (holder)).describe.setText(stationmodel.personDescribe);
        ((PersionStationBigHolder) (holder)).trackCounts.setText(StringUtil.getFriendlyNumStr(stationmodel.tracksCounts));
        ((PersionStationBigHolder) (holder)).funsCounts.setText(StringUtil.getFriendlyNumStr(stationmodel.followersCounts));
        setFollowStatus(holder, stationmodel.isFollowed);
        ((PersionStationBigHolder) (holder)).follow.setTag(0x7f090000, stationmodel);
        ((PersionStationBigHolder) (holder)).play.setTag(0x7f090000, stationmodel);
        holder.position = i;
        soundinfo = PlayListControl.getPlayListManager().getCurSound();
        localmediaservice = LocalMediaService.getInstance();
        if (soundinfo != null && stationmodel.tracks != null && stationmodel.tracks.contains(toSoundInfoNew(soundinfo)) && localmediaservice != null && !localmediaservice.isPaused())
        {
            ((PersionStationBigHolder) (holder)).play.setCompoundDrawablesWithIntrinsicBounds(0x7f02002d, 0, 0, 0);
            return viewgroup;
        } else
        {
            ((PersionStationBigHolder) (holder)).play.setCompoundDrawablesWithIntrinsicBounds(0x7f02002e, 0, 0, 0);
            return viewgroup;
        }
    }

    public void updateItem(int i)
    {
        if (i >= 0 && i < getCount())
        {
            int j = mListView.getFirstVisiblePosition();
            int k = mListView.getHeaderViewsCount();
            Object obj = mListView.getChildAt(i - (j - k));
            if (obj != null)
            {
                obj = (PersionStationBigHolder)((View) (obj)).getTag();
                if (obj != null)
                {
                    setFollowStatus(((PersionStationBigHolder) (obj)), ((StationModel)mStations.get(i)).isFollowed);
                    return;
                }
            }
        }
    }






    private class _cls4 extends a
    {

        final HotStationListAdapter this$0;
        final Context val$context;
        final PersionStationBigHolder val$holder;
        final StationModel val$model;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            super.onFinish();
            setFollowStatus(holder, model.isFollowed);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
                return;
            }
            s = JSON.parseObject(s);
            if (s == null || s.getIntValue("ret") != 0)
            {
                Toast.makeText(context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
                return;
            }
            s = model;
            boolean flag;
            if (!model.isFollowed)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            s.isFollowed = flag;
            if (model.isFollowed)
            {
                s = "\u5173\u6CE8\u6210\u529F\uFF01";
            } else
            {
                s = "\u53D6\u6D88\u5173\u6CE8\u6210\u529F";
            }
            Toast.makeText(context, s, 0).show();
        }

        _cls4()
        {
            this$0 = HotStationListAdapter.this;
            context = context1;
            model = stationmodel;
            holder = persionstationbigholder;
            view = view1;
            super();
        }
    }


    private class _cls3 extends a
    {

        ProgressDialog pd;
        final HotStationListAdapter this$0;
        final StationModel val$model;
        final boolean val$play;
        final boolean val$showPlay;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            super.onFinish();
            pd.cancel();
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
        }

        public void onStart()
        {
            super.onStart();
            pd = new MyProgressDialog(mContext);
            pd.setMessage("\u6B63\u5728\u52A0\u8F7D\u58F0\u97F3\u5217\u8868...");
            pd.show();
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            } else
            {
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = null;
                }
                if (s == null || s.getIntValue("ret") != 0)
                {
                    Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                    return;
                }
                s = s.getString("list");
                if (TextUtils.isEmpty(s))
                {
                    Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                    return;
                }
                s = JSON.parseArray(s, com/ximalaya/ting/android/model/sound/SoundInfoNew);
                if (s == null || s.size() == 0)
                {
                    Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                    return;
                }
                model.tracks = s;
                if (play && MyApplication.a() != null)
                {
                    PlayTools.gotoPlay(0, null, 0, null, ModelHelper.toSoundInfo(s), 0, MyApplication.a(), showPlay, DataCollectUtil.getDataFromView(view));
                    return;
                }
            }
        }

        _cls3()
        {
            this$0 = HotStationListAdapter.this;
            model = stationmodel;
            play = flag;
            showPlay = flag1;
            view = view1;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final HotStationListAdapter this$0;

        public void onClick(View view)
        {
            StationModel stationmodel = (StationModel)view.getTag(0x7f090000);
            if (stationmodel != null)
            {
                if (stationmodel.tracks == null)
                {
                    loadAlbumSound(stationmodel, true, false, view);
                    return;
                }
                SoundInfo soundinfo = PlayListControl.getPlayListManager().getCurSound();
                LocalMediaService localmediaservice = LocalMediaService.getInstance();
                if (soundinfo != null && stationmodel.tracks.contains(toSoundInfoNew(soundinfo)) && !localmediaservice.isPaused())
                {
                    localmediaservice.pause();
                    return;
                }
                if (stationmodel.tracks.contains(toSoundInfoNew(soundinfo)) && localmediaservice.isPaused())
                {
                    localmediaservice.start();
                    return;
                }
                if (MyApplication.a() != null)
                {
                    PlayTools.gotoPlay(0, null, 0, null, ModelHelper.toSoundInfo(stationmodel.tracks), 0, MyApplication.a(), false, DataCollectUtil.getDataFromView(view));
                    return;
                }
            }
        }

        _cls1()
        {
            this$0 = HotStationListAdapter.this;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final HotStationListAdapter this$0;
        final PersionStationBigHolder val$holder;

        public void onClick(View view)
        {
            Object obj = (StationModel)view.getTag(0x7f090000);
            if (obj == null)
            {
                return;
            }
            if (!UserInfoMannage.hasLogined())
            {
                setFollowStatus(holder, ((StationModel) (obj)).isFollowed);
                obj = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                ((Intent) (obj)).putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                mContext.startActivity(((Intent) (obj)));
                return;
            } else
            {
                doFollow(mContext, ((StationModel) (obj)), holder, view);
                return;
            }
        }

        _cls2()
        {
            this$0 = HotStationListAdapter.this;
            holder = persionstationbigholder;
            super();
        }
    }

}
