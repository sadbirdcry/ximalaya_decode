// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.model.search.SearchAlbum;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

final class val.model extends a
{

    final Context val$context;
    final AlbumItemHolder val$holder;
    final SearchAlbum val$model;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$holder.collect);
    }

    public void onNetError(int i, String s)
    {
        Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
        if (((SearchAlbum)val$holder.collect.getTag(0x7f090000)).id == val$model.id)
        {
            AlbumItemHolder.setCollectStatus(val$holder, val$model.isFavorite);
        }
    }

    public void onSuccess(String s)
    {
        boolean flag = true;
        if (!TextUtils.isEmpty(s)) goto _L2; else goto _L1
_L1:
        Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
_L4:
        return;
_L2:
        int i;
        try
        {
            s = JSON.parseObject(s);
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            return;
        }
        if (s == null) goto _L4; else goto _L3
_L3:
        i = s.getIntValue("ret");
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_132;
        }
        s = val$model;
        if (val$model.isFavorite)
        {
            flag = false;
        }
        s.isFavorite = flag;
        if (((SearchAlbum)val$holder.collect.getTag(0x7f090000)).id == val$model.id)
        {
            AlbumItemHolder.setCollectStatus(val$holder, val$model.isFavorite);
        }
        if (val$model.isFavorite)
        {
            s = "\u6536\u85CF\u6210\u529F\uFF01";
        } else
        {
            s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
        }
        Toast.makeText(val$context, s, 0).show();
        return;
        if (i != 791)
        {
            break MISSING_BLOCK_LABEL_147;
        }
        val$model.isFavorite = true;
        if (((SearchAlbum)val$holder.collect.getTag(0x7f090000)).id == val$model.id)
        {
            AlbumItemHolder.setCollectStatus(val$holder, val$model.isFavorite);
        }
        if (s.getString("msg") != null)
        {
            break MISSING_BLOCK_LABEL_215;
        }
        s = "\u4EB2\uFF0C\u64CD\u4F5C\u5931\u8D25\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
_L6:
        Toast.makeText(val$context, s, 0).show();
        return;
        s = s.getString("msg");
        if (true) goto _L6; else goto _L5
_L5:
    }

    ()
    {
        val$context = context1;
        val$holder = albumitemholder;
        val$model = searchalbum;
        super();
    }
}
