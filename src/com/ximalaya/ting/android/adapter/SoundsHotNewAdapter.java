// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.res.Resources;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class SoundsHotNewAdapter extends BaseListSoundsAdapter
{

    public SoundsHotNewAdapter(Activity activity, List list)
    {
        super(activity, list);
    }

    protected void bindData(final SoundInfoNew info, final BaseListSoundsAdapter.ViewHolder holder)
    {
        holder.title.setText(info.title);
        b.a().a(ModelHelper.toSoundInfo(info));
        holder.createTime.setText(ToolUtil.convertTime(info.createdAt));
        holder.owner.setText((new StringBuilder()).append("by ").append(info.nickname).toString());
        TextView textview = holder.origin;
        String s;
        if (info.userSource == 1)
        {
            s = "\u539F\u521B";
        } else
        {
            s = "\u91C7\u96C6";
        }
        textview.setText(s);
        if (isPlaying(info.id))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        if (info.playsCounts > 0)
        {
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.playsCounts));
        } else
        {
            holder.playCount.setVisibility(8);
        }
        if (info.favoritesCounts > 0)
        {
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.favoritesCounts));
            holder.likeCount.setVisibility(0);
            if (info.isFavorite)
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020090), null, null, null);
            } else
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020092), null, null, null);
            }
        } else
        {
            holder.likeCount.setVisibility(8);
        }
        if (info.commentsCounts > 0)
        {
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.commentsCounts));
            holder.commentCount.setVisibility(0);
        } else
        {
            holder.commentCount.setVisibility(8);
        }
        if (info.duration > 0.0F)
        {
            holder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)info.duration)).toString());
            holder.duration.setVisibility(0);
        } else
        {
            holder.duration.setVisibility(8);
        }
        if (isDownload(info.id))
        {
            holder.btn.setImageResource(0x7f0200f1);
            holder.btn.setEnabled(false);
        } else
        {
            holder.btn.setImageResource(0x7f0201fe);
            holder.btn.setEnabled(true);
        }
        holder.cover.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(holder.cover, info.coverSmall, 0x7f0202de);
        holder.cover.setOnClickListener(new _cls1());
        holder.btn.setOnClickListener(new _cls2());
    }

    protected volatile void bindData(Object obj, BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((SoundInfoNew)obj, viewholder);
    }

    public void setMargins(View view, int i, int j, int k, int l)
    {
        i = ToolUtil.dp2px(mContext, i);
        j = ToolUtil.dp2px(mContext, j);
        k = ToolUtil.dp2px(mContext, k);
        l = ToolUtil.dp2px(mContext, l);
        if (view.getLayoutParams() instanceof android.view.ViewGroup.MarginLayoutParams)
        {
            ((android.view.ViewGroup.MarginLayoutParams)view.getLayoutParams()).setMargins(i, j, k, l);
            view.requestLayout();
        }
    }

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SoundsHotNewAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SoundInfoNew val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, mData.indexOf(info), ModelHelper.toSoundInfo(info), ModelHelper.toSoundInfo(mData));
        }

        _cls1()
        {
            this$0 = SoundsHotNewAdapter.this;
            holder = viewholder;
            info = soundinfonew;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final SoundsHotNewAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SoundInfoNew val$info;

        public void onClick(View view)
        {
            DownloadTask downloadtask = new DownloadTask(ModelHelper.toSoundInfo(info));
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            view = downloadtools.goDownload(downloadtask, mContext.getApplicationContext(), view);
            downloadtools.release();
            if (view == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls2()
        {
            this$0 = SoundsHotNewAdapter.this;
            info = soundinfonew;
            holder = viewholder;
            super();
        }
    }

}
