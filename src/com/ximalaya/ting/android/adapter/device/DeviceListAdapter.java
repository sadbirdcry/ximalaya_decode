// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.device;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.device.Device;
import java.util.List;

public class DeviceListAdapter extends BaseAdapter
{
    class ViewHolder
    {

        ImageView deviceImage;
        TextView deviceName;
        ImageView deviceType;
        final DeviceListAdapter this$0;

        ViewHolder()
        {
            this$0 = DeviceListAdapter.this;
            super();
        }
    }


    List listItems;
    private Context mContext;
    private LayoutInflater mInflater;

    public DeviceListAdapter(Context context, List list)
    {
        mContext = context;
        listItems = list;
        mInflater = LayoutInflater.from(mContext);
    }

    public int getCount()
    {
        return listItems.size();
    }

    public Object getItem(int i)
    {
        return listItems.get(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        Device device;
        if (view == null)
        {
            viewgroup = new ViewHolder();
            view = mInflater.inflate(0x7f03011d, null);
            viewgroup.deviceName = (TextView)view.findViewById(0x7f0a02e8);
            viewgroup.deviceImage = (ImageView)view.findViewById(0x7f0a047e);
            viewgroup.deviceType = (ImageView)view.findViewById(0x7f0a047f);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        device = (Device)listItems.get(i);
        if (device.getDeviceType() != com.ximalaya.ting.android.fragment.device.Device.TYPE.WIFI) goto _L2; else goto _L1
_L1:
        ((ViewHolder) (viewgroup)).deviceType.setImageDrawable(mContext.getResources().getDrawable(0x7f0205cd));
_L4:
        ((ViewHolder) (viewgroup)).deviceName.setText(device.getDeviceName());
        ((ViewHolder) (viewgroup)).deviceImage.setImageDrawable(device.getDeviceImage());
        return view;
_L2:
        if (device.getDeviceType() == com.ximalaya.ting.android.fragment.device.Device.TYPE.BLUETOOTH)
        {
            ((ViewHolder) (viewgroup)).deviceType.setImageDrawable(mContext.getResources().getDrawable(0x7f0200e0));
        }
        if (true) goto _L4; else goto _L3
_L3:
    }
}
