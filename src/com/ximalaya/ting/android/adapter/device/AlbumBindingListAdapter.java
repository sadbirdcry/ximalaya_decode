// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.device;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.BaseFragment;
import com.ximalaya.ting.android.fragment.device.MyDeviceManager;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.modelnew.AlbumModelNew;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

public class AlbumBindingListAdapter extends BaseAdapter
{
    public static interface OnBindingListener
    {

        public abstract void doCollect(Context context, AlbumModelNew albummodelnew, AlbumItemHolder albumitemholder);
    }


    private String keyId;
    private Context mActivity;
    private List mAlbums;
    private OnBindingListener mBindingListener;
    private BaseFragment mFragment;
    private ListView mListView;
    private MyDeviceManager mMyDeviceManager;

    public AlbumBindingListAdapter(Activity activity, BaseFragment basefragment, ListView listview, List list)
    {
        mFragment = basefragment;
        mListView = listview;
        mActivity = mListView.getContext();
        mAlbums = list;
        mMyDeviceManager = MyDeviceManager.getInstance(activity.getApplicationContext());
    }

    public int getCount()
    {
        if (mAlbums == null)
        {
            return 0;
        } else
        {
            return mAlbums.size();
        }
    }

    public Object getItem(int i)
    {
        return mAlbums.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, final View holder, ViewGroup viewgroup)
    {
        boolean flag1 = true;
        viewgroup = holder;
        if (holder == null)
        {
            viewgroup = AlbumItemHolder.getView(mActivity);
            holder = (AlbumItemHolder)viewgroup.getTag();
            mFragment.markImageView(((AlbumItemHolder) (holder)).cover);
        }
        holder = (AlbumItemHolder)viewgroup.getTag();
        AlbumModelNew albummodelnew = (AlbumModelNew)mAlbums.get(i);
        ImageManager2.from(mActivity).displayImage(((AlbumItemHolder) (holder)).cover, albummodelnew.albumCoverUrl290, 0x7f0202e0);
        boolean flag;
        if (albummodelnew.serialState == 2)
        {
            ((AlbumItemHolder) (holder)).complete.setVisibility(0);
        } else
        {
            ((AlbumItemHolder) (holder)).complete.setVisibility(8);
        }
        ((AlbumItemHolder) (holder)).name.setText(albummodelnew.title);
        if (albummodelnew.playsCounts > 0)
        {
            ((AlbumItemHolder) (holder)).playCount.setVisibility(0);
            ((AlbumItemHolder) (holder)).playCount.setText(StringUtil.getFriendlyNumStr(albummodelnew.playsCounts));
        } else
        {
            ((AlbumItemHolder) (holder)).playCount.setVisibility(8);
        }
        ((AlbumItemHolder) (holder)).collectCount.setText((new StringBuilder()).append(albummodelnew.tracksCounts).append("\u96C6").toString());
        ((AlbumItemHolder) (holder)).updateAt.setText((new StringBuilder()).append("\u6700\u540E\u66F4\u65B0  ").append(ToolUtil.convertTime(albummodelnew.lastUptrackAt)).toString());
        AlbumItemHolder.setCollectStatus(holder, albummodelnew.isFavorite);
        if (mMyDeviceManager.isAlbumBind(albummodelnew.id))
        {
            AlbumItemHolder.setCollectStatus(holder, true);
            ((AlbumItemHolder) (holder)).collectTxt.setText("");
            ((AlbumItemHolder) (holder)).collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0201c6, 0, 0);
        } else
        {
            AlbumItemHolder.setCollectStatus(holder, false);
            ((AlbumItemHolder) (holder)).collectTxt.setText("");
            ((AlbumItemHolder) (holder)).collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0201c5, 0, 0);
        }
        ((AlbumItemHolder) (holder)).collect.setTag(0x7f090000, albummodelnew);
        ((AlbumItemHolder) (holder)).collect.setOnClickListener(new _cls1());
        holder = mActivity;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (i + 1 != mAlbums.size())
        {
            flag1 = false;
        }
        ViewUtil.buildAlbumItemSpace(holder, viewgroup, flag, flag1);
        return viewgroup;
    }

    public void removeBindingListener()
    {
        mBindingListener = null;
    }

    public void setBindingListener(OnBindingListener onbindinglistener)
    {
        mBindingListener = onbindinglistener;
    }

    public void updateItem(int i)
    {
        if (i >= 0 && i < getCount())
        {
            int j = mListView.getFirstVisiblePosition();
            int k = mListView.getHeaderViewsCount();
            Object obj = mListView.getChildAt(i - (j - k));
            if (obj != null)
            {
                obj = (AlbumItemHolder)((View) (obj)).getTag();
                if (obj != null)
                {
                    AlbumModelNew albummodelnew = (AlbumModelNew)mAlbums.get(i);
                    ((AlbumItemHolder) (obj)).collect.setVisibility(0);
                    AlbumItemHolder.setCollectStatus(((AlbumItemHolder) (obj)), albummodelnew.isFavorite);
                    return;
                }
            }
        }
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final AlbumBindingListAdapter this$0;
        final AlbumItemHolder val$holder;

        public void onClick(View view)
        {
            view = (AlbumModelNew)view.getTag(0x7f090000);
            if (view == null)
            {
                return;
            }
            if (mBindingListener != null)
            {
                mBindingListener.doCollect(mActivity, view, holder);
            }
            notifyDataSetChanged();
        }

        _cls1()
        {
            this$0 = AlbumBindingListAdapter.this;
            holder = albumitemholder;
            super();
        }
    }

}
