// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter.device;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoBTManager;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm;
import com.ximalaya.ting.android.fragment.device.ximao.XiMaoListFragment;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;

public class XiMaoListAdapter extends BaseAdapter
{
    class ViewHolder
    {

        RelativeLayout deleteSound;
        ImageView playIcon;
        TextView soundName;
        final XiMaoListAdapter this$0;

        ViewHolder()
        {
            this$0 = XiMaoListAdapter.this;
            super();
        }
    }


    private com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT curContent;
    ArrayList listItem;
    private Context mContext;
    private LayoutInflater mInflater;

    public XiMaoListAdapter(Context context, ArrayList arraylist, com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT content)
    {
        mContext = context;
        listItem = arraylist;
        mInflater = LayoutInflater.from(mContext);
        curContent = content;
    }

    public void deleteItem(int i, View view)
    {
        byte abyte0[];
        view = (TextView)view;
        abyte0 = null;
        CharSequence charsequence = view.getText();
        view = abyte0;
        if (charsequence != null)
        {
            try
            {
                view = charsequence.toString().getBytes("unicode");
            }
            // Misplaced declaration of an exception variable
            catch (View view)
            {
                view.printStackTrace();
                view = abyte0;
            }
        }
        abyte0 = new byte[view.length - 2];
        for (int j = 0; j < view.length - 2; j++)
        {
            abyte0[j] = view[j + 2];
        }

        XiMaoComm.sendPacket(mContext, 3, 15, abyte0, view.length - 2);
        listItem.remove(i);
        if (XiMaoBTManager.getInstance(mContext).mNumContent4 > 0)
        {
            view = XiMaoBTManager.getInstance(mContext);
            view.mNumContent4 = ((XiMaoBTManager) (view)).mNumContent4 - 1;
        }
        notifyDataSetChanged();
        return;
    }

    public int getCount()
    {
        return listItem.size();
    }

    public Object getItem(int i)
    {
        return null;
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public View getView(final int position, View view, final ViewGroup mViewHolder)
    {
label0:
        {
label1:
            {
                if (view == null)
                {
                    ViewHolder viewholder = new ViewHolder();
                    view = mInflater.inflate(0x7f030146, mViewHolder, false);
                    viewholder.soundName = (TextView)view.findViewById(0x7f0a0218);
                    viewholder.playIcon = (ImageView)view.findViewById(0x7f0a0022);
                    viewholder.deleteSound = (RelativeLayout)view.findViewById(0x7f0a04f8);
                    view.setTag(viewholder);
                    mViewHolder = viewholder;
                } else
                {
                    mViewHolder = (ViewHolder)view.getTag();
                }
                if (((HashMap)listItem.get(position)).get("title") != null)
                {
                    ((ViewHolder) (mViewHolder)).soundName.setText(((HashMap)listItem.get(position)).get("title").toString());
                    if (curContent == com.ximalaya.ting.android.fragment.device.ximao.XiMaoComm.CONTENT.CONTENT_4)
                    {
                        ((ViewHolder) (mViewHolder)).deleteSound.setVisibility(0);
                        ((ViewHolder) (mViewHolder)).deleteSound.setOnClickListener(new _cls1());
                    } else
                    {
                        ((ViewHolder) (mViewHolder)).deleteSound.setVisibility(8);
                    }
                    if (XiMaoListFragment.mPlayPositionNow != position || !XiMaoListFragment.mPlayContent.equals(XiMaoListFragment.mContentName))
                    {
                        break label0;
                    }
                    ((ViewHolder) (mViewHolder)).playIcon.setVisibility(0);
                    if (!XiMaoListFragment.isPlay)
                    {
                        break label1;
                    }
                    XiMaoListFragment.mPlayPositionNow = position;
                    ((ViewHolder) (mViewHolder)).playIcon.setImageResource(0x7f0203e8);
                    if (((ViewHolder) (mViewHolder)).playIcon.getDrawable() instanceof AnimationDrawable)
                    {
                        final AnimationDrawable animationDrawable = (AnimationDrawable)((ViewHolder) (mViewHolder)).playIcon.getDrawable();
                        ((ViewHolder) (mViewHolder)).playIcon.post(new _cls2());
                    }
                }
                return view;
            }
            ((ViewHolder) (mViewHolder)).playIcon.setImageResource(0x7f0203e9);
            return view;
        }
        ((ViewHolder) (mViewHolder)).playIcon.setVisibility(8);
        return view;
    }

    private class _cls1
        implements android.view.View.OnClickListener
    {

        final XiMaoListAdapter this$0;
        final ViewHolder val$mViewHolder;
        final int val$position;

        public void onClick(View view)
        {
            class _cls1
                implements Runnable
            {

                final _cls1 this$1;

                public void run()
                {
                    class _cls1
                        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
                    {

                        final _cls1 this$2;

                        public void onExecute()
                        {
                            deleteItem(position, mViewHolder.soundName);
                        }

                            _cls1()
                            {
                                this$2 = _cls1.this;
                                super();
                            }
                    }

                    (new DialogBuilder(MyApplication.a())).setMessage((new StringBuilder()).append("\u786E\u5B9A\u5220\u9664\u58F0\u97F3").append(((HashMap)listItem.get(position)).get("title").toString()).append("\uFF1F").toString()).setOkBtn(new _cls1()).showConfirm();
                }

                _cls1()
                {
                    this$1 = _cls1.this;
                    super();
                }
            }

            MyApplication.a().runOnUiThread(new _cls1());
        }

        _cls1()
        {
            this$0 = XiMaoListAdapter.this;
            position = i;
            mViewHolder = viewholder;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final XiMaoListAdapter this$0;
        final AnimationDrawable val$animationDrawable;

        public void run()
        {
            if (animationDrawable != null && !animationDrawable.isRunning())
            {
                animationDrawable.start();
            }
        }

        _cls2()
        {
            this$0 = XiMaoListAdapter.this;
            animationDrawable = animationdrawable;
            super();
        }
    }

}
