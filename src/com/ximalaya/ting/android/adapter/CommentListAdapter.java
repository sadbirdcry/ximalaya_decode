// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.ximalaya.ting.android.model.comment.CommentModel;
import com.ximalaya.ting.android.util.EmotionUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.view.RoundedImageView;
import com.ximalaya.ting.android.view.listview.PullToRefreshListView;
import java.util.List;

public class CommentListAdapter extends BaseAdapter
{
    private static class ViewHolder
    {

        RoundedImageView commentImageView;
        TextView commentNameTextView;
        TextView commentTextView;
        TextView commentTimeTextView;

        private ViewHolder()
        {
        }

    }


    private Context context;
    private LayoutInflater layoutInflater;
    private List list;
    private PullToRefreshListView listview;
    private EmotionUtil mEmotionUtil;

    public CommentListAdapter(Activity activity, List list1, PullToRefreshListView pulltorefreshlistview)
    {
        layoutInflater = LayoutInflater.from(activity);
        context = activity;
        listview = pulltorefreshlistview;
        list = list1;
        mEmotionUtil = EmotionUtil.getInstance();
    }

    public int getCount()
    {
        return list.size();
    }

    public CommentModel getItem(int i)
    {
        if (list != null)
        {
            return (CommentModel)list.get(i);
        } else
        {
            return null;
        }
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        CommentModel commentmodel;
        if (view == null)
        {
            viewgroup = new ViewHolder();
            view = layoutInflater.inflate(0x7f030062, null);
            viewgroup.commentImageView = (RoundedImageView)view.findViewById(0x7f0a01e2);
            viewgroup.commentNameTextView = (TextView)view.findViewById(0x7f0a01e3);
            viewgroup.commentTimeTextView = (TextView)view.findViewById(0x7f0a01e4);
            viewgroup.commentTextView = (TextView)view.findViewById(0x7f0a01e5);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (ViewHolder)view.getTag();
        }
        commentmodel = getItem(i);
        if (commentmodel != null && commentmodel.nickname != null)
        {
            ((ViewHolder) (viewgroup)).commentNameTextView.setText(commentmodel.nickname);
            ((ViewHolder) (viewgroup)).commentTimeTextView.setText(ToolUtil.convertTime(commentmodel.createdAt));
            ((ViewHolder) (viewgroup)).commentTextView.setText(mEmotionUtil.convertEmotionText(commentmodel.content));
        }
        ImageManager2.from(context).displayImage(((ViewHolder) (viewgroup)).commentImageView, commentmodel.smallHeader, 0x7f0202e0);
        return view;
    }
}
