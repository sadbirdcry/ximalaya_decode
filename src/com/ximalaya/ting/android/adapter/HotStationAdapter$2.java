// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.os.Bundle;
import android.view.View;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.activity.MainTabActivity2;
import com.ximalaya.ting.android.fragment.findings.FindingHotStationListFragment;
import com.ximalaya.ting.android.model.broadcast.StationCategory;
import com.ximalaya.ting.android.util.DataCollectUtil;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            HotStationAdapter

class this._cls0
    implements android.view.
{

    final HotStationAdapter this$0;

    public void onClick(View view)
    {
        StationCategory stationcategory = (StationCategory)view.getTag(0x7f090000);
        if (stationcategory == null)
        {
            return;
        } else
        {
            Bundle bundle = new Bundle();
            bundle.putString("category", stationcategory.name);
            bundle.putString("title", stationcategory.title);
            bundle.putString("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
            ((MainTabActivity2)MyApplication.a()).startFragment(com/ximalaya/ting/android/fragment/findings/FindingHotStationListFragment, bundle);
            return;
        }
    }

    StationListFragment()
    {
        this$0 = HotStationAdapter.this;
        super();
    }
}
