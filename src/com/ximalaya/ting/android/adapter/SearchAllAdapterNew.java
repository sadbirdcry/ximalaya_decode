// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.res.Resources;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.search.SearchAlbum;
import com.ximalaya.ting.android.model.search.SearchPerson;
import com.ximalaya.ting.android.model.search.SearchSound;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter, SearchAlbumAdapterNew

public class SearchAllAdapterNew extends BaseListSoundsAdapter
{
    private class PersonHolder
    {

        View border;
        ImageView cover;
        TextView dsscription;
        ToggleButton follow;
        TextView funs;
        TextView name;
        int position;
        TextView sounds;
        final SearchAllAdapterNew this$0;

        private PersonHolder()
        {
            this$0 = SearchAllAdapterNew.this;
            super();
        }

        PersonHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private static final int ALBUM = 1;
    private static final int DIVIDER = 0;
    private static final int PERSON = 2;
    private static final int SOUND = 3;

    public SearchAllAdapterNew(Activity activity, List list)
    {
        super(activity, list);
    }

    private View getLabelView(int i, View view, ViewGroup viewgroup)
    {
        Object obj;
        Object obj1;
        obj = mData.get(i);
        viewgroup = view;
        if (view == null)
        {
            viewgroup = View.inflate(mContext, 0x7f030137, null);
        }
        obj1 = (TextView)viewgroup.findViewById(0x7f0a0158);
        view = (TextView)viewgroup.findViewById(0x7f0a0277);
        if (i == 0)
        {
            viewgroup.findViewById(0x7f0a0276).setVisibility(8);
        } else
        {
            viewgroup.findViewById(0x7f0a0276).setVisibility(0);
        }
        if (!(obj instanceof com.ximalaya.ting.android.fragment.search.WordAssociatedFragment.SearchModelDivider)) goto _L2; else goto _L1
_L1:
        obj = (com.ximalaya.ting.android.fragment.search.WordAssociatedFragment.SearchModelDivider)obj;
        if (!"album".equals(((com.ximalaya.ting.android.fragment.search.WordAssociatedFragment.SearchModelDivider) (obj)).type)) goto _L4; else goto _L3
_L3:
        ((TextView) (obj1)).setText("\u4E13\u8F91");
_L6:
        obj1 = new StringBuilder("\u5168\u90E8");
        ((StringBuilder) (obj1)).append("<font color='#f86442'>").append(((com.ximalaya.ting.android.fragment.search.WordAssociatedFragment.SearchModelDivider) (obj)).count).append("</font>").append("\u6761\u7ED3\u679C");
        view.setText(Html.fromHtml(((StringBuilder) (obj1)).toString()));
_L2:
        return viewgroup;
_L4:
        if ("user".equals(((com.ximalaya.ting.android.fragment.search.WordAssociatedFragment.SearchModelDivider) (obj)).type))
        {
            ((TextView) (obj1)).setText("\u7528\u6237");
        } else
        if ("track".equals(((com.ximalaya.ting.android.fragment.search.WordAssociatedFragment.SearchModelDivider) (obj)).type))
        {
            ((TextView) (obj1)).setText("\u58F0\u97F3");
        }
        if (true) goto _L6; else goto _L5
_L5:
    }

    private View getPersonView(int i, View view, ViewGroup viewgroup)
    {
        Object obj = mData.get(i);
        viewgroup = view;
        if (view == null)
        {
            viewgroup = View.inflate(mContext, 0x7f030099, null);
            view = new PersonHolder(null);
            view.cover = (ImageView)viewgroup.findViewById(0x7f0a0268);
            view.name = (TextView)viewgroup.findViewById(0x7f0a0269);
            view.sounds = (TextView)viewgroup.findViewById(0x7f0a026b);
            view.funs = (TextView)viewgroup.findViewById(0x7f0a026c);
            view.dsscription = (TextView)viewgroup.findViewById(0x7f0a026d);
            view.follow = (ToggleButton)viewgroup.findViewById(0x7f0a01b9);
            ((PersonHolder) (view)).follow.setVisibility(8);
            view.border = viewgroup.findViewById(0x7f0a00a8);
            ((PersonHolder) (view)).follow.setOnClickListener(new _cls1());
            viewgroup.setTag(view);
        }
        view = (PersonHolder)viewgroup.getTag();
        if (obj instanceof SearchPerson)
        {
            if (isLastItem(i))
            {
                ((PersonHolder) (view)).border.setVisibility(4);
            } else
            {
                viewgroup.findViewById(0x7f0a00a8).setVisibility(0);
            }
            obj = (SearchPerson)obj;
            if (((SearchPerson) (obj)).isVerified)
            {
                ((PersonHolder) (view)).name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0x7f0200c0, 0);
            } else
            {
                ((PersonHolder) (view)).name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
            UserInfoMannage.getInstance().getUser();
            ImageManager2.from(mContext).displayImage(((PersonHolder) (view)).cover, ((SearchPerson) (obj)).smallPic, 0x7f0202df);
            ((PersonHolder) (view)).name.setText(((SearchPerson) (obj)).nickname);
            ((PersonHolder) (view)).sounds.setText((new StringBuilder()).append("\u58F0\u97F3  ").append(StringUtil.getFriendlyNumStr(((SearchPerson) (obj)).tracks_counts)).toString());
            ((PersonHolder) (view)).funs.setText((new StringBuilder()).append("\u7C89\u4E1D  ").append(StringUtil.getFriendlyNumStr(((SearchPerson) (obj)).followers_counts)).toString());
            if (Utilities.isBlank(((SearchPerson) (obj)).intro))
            {
                ((PersonHolder) (view)).dsscription.setVisibility(8);
            } else
            {
                ((PersonHolder) (view)).dsscription.setVisibility(0);
                ((PersonHolder) (view)).dsscription.setText(((SearchPerson) (obj)).intro);
            }
            ((PersonHolder) (view)).follow.setChecked(((SearchPerson) (obj)).is_follow);
            view.position = i;
            ((PersonHolder) (view)).follow.setTag(0x7f090000, view);
        }
        return viewgroup;
    }

    private boolean isFirstItem(int i)
    {
        while (i == 0 || i - 1 >= 0 && (mData.get(i - 1) instanceof com.ximalaya.ting.android.fragment.search.WordAssociatedFragment.SearchModelDivider)) 
        {
            return true;
        }
        return false;
    }

    private boolean isLastItem(int i)
    {
        while (i + 1 == mData.size() || i + 1 < mData.size() && (mData.get(i + 1) instanceof com.ximalaya.ting.android.fragment.search.WordAssociatedFragment.SearchModelDivider)) 
        {
            return true;
        }
        return false;
    }

    private void toFollow(final SearchPerson sp, final ToggleButton btn)
    {
        RequestParams requestparams = new RequestParams();
        requestparams.put("toUid", (new StringBuilder()).append("").append(sp.uid).toString());
        StringBuilder stringbuilder = (new StringBuilder()).append("");
        boolean flag;
        if (!sp.is_follow)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        requestparams.put("isFollow", stringbuilder.append(flag).toString());
        f.a().b("mobile/follow", requestparams, DataCollectUtil.getDataFromView(btn), new _cls4());
    }

    protected void bindData(Object obj, final BaseListSoundsAdapter.ViewHolder holder)
    {
        if (obj instanceof SearchSound)
        {
            final SearchSound info;
            TextView textview;
            if (isLastItem(holder.position))
            {
                holder.border.setVisibility(4);
            } else
            {
                holder.border.setVisibility(0);
            }
            info = (SearchSound)obj;
            ImageManager2.from(mContext).displayImage(holder.cover, info.cover_path, 0x7f0202de);
            holder.title.setText(info.title);
            holder.owner.setText((new StringBuilder()).append("by ").append(info.nickname).toString());
            holder.createTime.setText(ToolUtil.convertTime(info.created_at));
            textview = holder.origin;
            if (info.user_source == 1)
            {
                obj = "\u539F\u521B";
            } else
            {
                obj = "\u91C7\u96C6";
            }
            textview.setText(((CharSequence) (obj)));
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.count_play));
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.count_like));
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.count_comment));
            holder.duration.setText(String.valueOf(ToolUtil.toTime(info.duration)));
            if (isPlaying(info.id))
            {
                if (LocalMediaService.getInstance().isPaused())
                {
                    holder.playFlag.setImageResource(0x7f020250);
                } else
                {
                    holder.playFlag.setImageResource(0x7f02024f);
                }
            } else
            {
                holder.playFlag.setImageResource(0x7f020250);
            }
            if (info.is_like)
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020090), null, null, null);
            } else
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020092), null, null, null);
            }
            if (isDownload(info.id))
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            } else
            {
                holder.btn.setImageResource(0x7f0201fe);
                holder.btn.setEnabled(true);
            }
            holder.btn.setOnClickListener(new _cls2());
            holder.cover.setOnClickListener(new _cls3());
        }
    }

    public List getData()
    {
        return mData;
    }

    public int getItemViewType(int i)
    {
        Object obj = mData.get(i);
        if (!(obj instanceof com.ximalaya.ting.android.fragment.search.WordAssociatedFragment.SearchModelDivider))
        {
            if (obj instanceof SearchAlbum)
            {
                return 1;
            }
            if (obj instanceof SearchPerson)
            {
                return 2;
            }
            if (obj instanceof SearchSound)
            {
                return 3;
            }
        }
        return 0;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        switch (getItemViewType(i))
        {
        default:
            return view;

        case 0: // '\0'
            return getLabelView(i, view, viewgroup);

        case 1: // '\001'
            Object obj = mData.get(i);
            return SearchAlbumAdapterNew.getAlbumView(mContext, obj, i, view, viewgroup, isFirstItem(i), isLastItem(i));

        case 2: // '\002'
            return getPersonView(i, view, viewgroup);

        case 3: // '\003'
            return super.getView(i, view, viewgroup);
        }
    }

    public int getViewTypeCount()
    {
        return 4;
    }

    public boolean isSoundItem(int i)
    {
        return getItemViewType(i) == 3;
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SearchAllAdapterNew this$0;

        public void onClick(View view)
        {
            if (!UserInfoMannage.hasLogined())
            {
                Intent intent = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                intent.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                mContext.startActivity(intent);
            } else
            {
                Object obj = (PersonHolder)view.getTag(0x7f090000);
                if (obj != null)
                {
                    obj = mData.get(((PersonHolder) (obj)).position);
                    if (obj != null && (obj instanceof SearchPerson))
                    {
                        obj = (SearchPerson)obj;
                        toFollow(((SearchPerson) (obj)), (ToggleButton)view);
                        return;
                    }
                }
            }
        }

        _cls1()
        {
            this$0 = SearchAllAdapterNew.this;
            super();
        }
    }


    private class _cls4 extends a
    {

        final SearchAllAdapterNew this$0;
        final ToggleButton val$btn;
        final SearchPerson val$sp;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, btn);
        }

        public void onFinish()
        {
            super.onFinish();
            btn.setChecked(sp.is_follow);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u4E0D\u7ED9\u529B\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
        }

        public void onStart()
        {
            super.onStart();
            btn.setChecked(sp.is_follow);
        }

        public void onSuccess(String s)
        {
            if (!TextUtils.isEmpty(s))
            {
                Object obj = null;
                try
                {
                    s = JSON.parseObject(s);
                }
                // Misplaced declaration of an exception variable
                catch (String s)
                {
                    s.printStackTrace();
                    s = obj;
                }
                if (s != null && s.getIntValue("ret") == 0)
                {
                    s = sp;
                    boolean flag;
                    if (!sp.is_follow)
                    {
                        flag = true;
                    } else
                    {
                        flag = false;
                    }
                    s.is_follow = flag;
                    if (sp.is_follow)
                    {
                        s = "\u5173\u6CE8\u6210\u529F\uFF01";
                    } else
                    {
                        s = "\u53D6\u6D88\u5173\u6CE8\u6210\u529F\uFF01";
                    }
                    Toast.makeText(mContext, s, 0).show();
                    return;
                }
            }
        }

        _cls4()
        {
            this$0 = SearchAllAdapterNew.this;
            btn = togglebutton;
            sp = searchperson;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final SearchAllAdapterNew this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SearchSound val$info;

        public void onClick(View view)
        {
            DownloadTask downloadtask = new DownloadTask(ModelHelper.toSoundInfo(info));
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            view = downloadtools.goDownload(downloadtask, mContext.getApplicationContext(), view);
            downloadtools.release();
            if (view == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls2()
        {
            this$0 = SearchAllAdapterNew.this;
            info = searchsound;
            holder = viewholder;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final SearchAllAdapterNew this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SearchSound val$info;

        public void onClick(View view)
        {
            view = new ArrayList();
            Iterator iterator = mData.iterator();
            do
            {
                if (!iterator.hasNext())
                {
                    break;
                }
                Object obj = iterator.next();
                if (obj instanceof SearchSound)
                {
                    view.add((SearchSound)obj);
                }
            } while (true);
            playSound(holder.playFlag, view.indexOf(info), ModelHelper.toSoundInfo(info), ModelHelper.toSoundInfoFromSearch(view));
        }

        _cls3()
        {
            this$0 = SearchAllAdapterNew.this;
            holder = viewholder;
            info = searchsound;
            super();
        }
    }

}
