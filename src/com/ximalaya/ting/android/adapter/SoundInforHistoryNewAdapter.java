// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.library.view.dialog.MenuDialog;
import com.ximalaya.ting.android.model.Likeable;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.transaction.a.b;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.Logger;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.Arrays;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class SoundInforHistoryNewAdapter extends BaseListSoundsAdapter
{
    private class AlbumViewHolder
    {

        View Rl_album;
        ImageView album_image;
        TextView album_name;
        public LinearLayout context;
        TextView sound_title;
        final SoundInforHistoryNewAdapter this$0;
        TextView txt_lastPlay;

        private AlbumViewHolder()
        {
            this$0 = SoundInforHistoryNewAdapter.this;
            super();
        }

        AlbumViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private static final int TYPE_ALBUM = 0;
    private static final int TYPE_SOUND = 1;
    private MenuDialog menuDialog;

    public SoundInforHistoryNewAdapter(Activity activity, List list)
    {
        super(activity, list);
    }

    private View getAlbumItemConvertView(View view, int i)
    {
        boolean flag1 = true;
        View view1 = view;
        if (view == null)
        {
            view1 = LayoutInflater.from(mContext).inflate(0x7f030126, null, false);
            view = new AlbumViewHolder(null);
            view.txt_lastPlay = (TextView)view1.findViewById(0x7f0a0225);
            view.album_image = (ImageView)view1.findViewById(0x7f0a0023);
            view.album_name = (TextView)view1.findViewById(0x7f0a00ea);
            view.sound_title = (TextView)view1.findViewById(0x7f0a00e6);
            view.Rl_album = view1.findViewById(0x7f0a0497);
            view1.setTag(view);
        }
        Object obj = (AlbumViewHolder)view1.getTag();
        SoundInfo soundinfo = (SoundInfo)getItem(i);
        ((AlbumViewHolder) (obj)).Rl_album.setVisibility(0);
        ((AlbumViewHolder) (obj)).album_image.setTag(0x7f0a0023, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(((AlbumViewHolder) (obj)).album_image, soundinfo.albumCoverPath, 0x7f0202e0);
        ((AlbumViewHolder) (obj)).album_name.setText(soundinfo.albumName);
        TextView textview = ((AlbumViewHolder) (obj)).sound_title;
        boolean flag;
        if (soundinfo.title == null)
        {
            view = "";
        } else
        {
            view = soundinfo.title;
        }
        textview.setText(view);
        if (soundinfo.history_duration != 0L)
        {
            obj = ((AlbumViewHolder) (obj)).txt_lastPlay;
            StringBuilder stringbuilder = (new StringBuilder()).append("\u4E0A\u6B21\u64AD\u653E");
            if (SharedPreferencesUtil.getInstance(mContext).getBoolean("historySwitch", true))
            {
                view = (new StringBuilder()).append("\u81F3: ").append(ToolUtil.toTimeForHistory(soundinfo.history_listener / 1000L, soundinfo.history_duration / 1000L, false)).append("").toString();
            } else
            {
                view = "";
            }
            ((TextView) (obj)).setText(stringbuilder.append(view).toString());
        } else
        {
            obj = ((AlbumViewHolder) (obj)).txt_lastPlay;
            StringBuilder stringbuilder1 = (new StringBuilder()).append("\u4E0A\u6B21\u64AD\u653E");
            if (SharedPreferencesUtil.getInstance(mContext).getBoolean("historySwitch", true))
            {
                view = (new StringBuilder()).append("\u81F3: ").append(ToolUtil.toTimeForHistory(soundinfo.history_listener / 1000L, soundinfo.duration, false)).append("").toString();
            } else
            {
                view = "";
            }
            ((TextView) (obj)).setText(stringbuilder1.append(view).toString());
        }
        view = mContext;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (i + 1 != mData.size())
        {
            flag1 = false;
        }
        ViewUtil.buildAlbumItemSpace(view, view1, flag, flag1);
        return view1;
    }

    private com.ximalaya.ting.android.a.a.a goDownLoad(SoundInfo soundinfo, View view)
    {
        Object obj = null;
        DownloadTask downloadtask = new DownloadTask(soundinfo);
        soundinfo = obj;
        if (downloadtask != null)
        {
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            soundinfo = downloadtools.goDownload(downloadtask, (MyApplication)mContext.getApplicationContext(), view);
            downloadtools.release();
        }
        return soundinfo;
    }

    protected void bindData(final SoundInfo info, final BaseListSoundsAdapter.ViewHolder holder)
    {
        b.a().a(info);
        holder.title.setText(info.title);
        if (isPlaying(info.trackId))
        {
            if (LocalMediaService.getInstance().isPaused())
            {
                holder.playFlag.setImageResource(0x7f020250);
            } else
            {
                holder.playFlag.setImageResource(0x7f02024f);
            }
        } else
        {
            holder.playFlag.setImageResource(0x7f020250);
        }
        if (info.create_at <= 0L)
        {
            holder.createTime.setVisibility(8);
        } else
        {
            holder.createTime.setVisibility(0);
            holder.createTime.setText(ToolUtil.convertTime(info.create_at));
        }
        if (TextUtils.isEmpty(info.nickname))
        {
            holder.owner.setText("");
        } else
        {
            holder.owner.setText((new StringBuilder()).append("by ").append(info.nickname).toString());
        }
        if (info.plays_counts > 0)
        {
            holder.playCount.setText(StringUtil.getFriendlyNumStr(info.plays_counts));
            holder.playCount.setVisibility(0);
        } else
        {
            holder.playCount.setVisibility(8);
        }
        if (info.favorites_counts > 0)
        {
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(info.favorites_counts));
            holder.likeCount.setVisibility(0);
            if (info.is_favorited)
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020090), null, null, null);
            } else
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(mContext.getResources().getDrawable(0x7f020092), null, null, null);
            }
        } else
        {
            holder.likeCount.setVisibility(8);
        }
        if (info.comments_counts > 0)
        {
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(info.comments_counts));
            holder.commentCount.setVisibility(0);
        } else
        {
            holder.commentCount.setVisibility(8);
        }
        if (info.duration > 0.0D)
        {
            holder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)info.duration)).toString());
            holder.duration.setVisibility(0);
        } else
        {
            holder.duration.setVisibility(8);
        }
        holder.cover.setTag(0x7f0a0037, Boolean.valueOf(true));
        ImageManager2.from(mContext).displayImage(holder.cover, info.coverSmall, 0x7f0202de);
        if (isDownload(info.trackId))
        {
            holder.btn.setImageResource(0x7f0200f1);
            holder.btn.setEnabled(false);
        } else
        {
            holder.btn.setImageResource(0x7f0201fe);
            holder.btn.setEnabled(true);
        }
        if (info.user_source == 1)
        {
            holder.origin.setText("\u539F\u521B");
        } else
        {
            holder.origin.setText("\u91C7\u96C6");
        }
        holder.btn.setOnClickListener(new _cls1());
        holder.cover.setOnClickListener(new _cls2());
    }

    protected volatile void bindData(Object obj, BaseListSoundsAdapter.ViewHolder viewholder)
    {
        bindData((SoundInfo)obj, viewholder);
    }

    public void delItem(final int position)
    {
        (new DialogBuilder(mContext)).setMessage("\u786E\u5B9A\u5220\u9664\u8BE5\u6761\u64AD\u653E\u8BB0\u5F55\uFF1F").setOkBtn(new _cls4()).showConfirm();
    }

    public int getItemViewType(int i)
    {
        if (((SoundInfo)mData.get(i)).albumId > 0L)
        {
            i = 0;
        } else
        {
            i = 1;
        }
        Logger.d("TYPE:", (new StringBuilder()).append("").append(i).toString());
        return i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        switch (getItemViewType(i))
        {
        default:
            return view;

        case 1: // '\001'
            return super.getView(i, view, viewgroup);

        case 0: // '\0'
            return getAlbumItemConvertView(view, i);
        }
    }

    public int getViewTypeCount()
    {
        return 2;
    }

    public void handleItemLongClick(final Likeable likeable, final View view)
    {
        String as[] = new String[3];
        as[0] = "\u5220\u9664";
        as[1] = "\u70B9\u8D5E";
        as[2] = "\u8BC4\u8BBA";
        if (likeable.isLiked())
        {
            as[1] = "\u53D6\u6D88\u70B9\u8D5E";
        }
        menuDialog = new MenuDialog(mContext, Arrays.asList(as), new _cls3());
        menuDialog.show();
    }

    public boolean isSound(int i)
    {
        return getItemViewType(i) == 1;
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SoundInforHistoryNewAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SoundInfo val$info;

        public void onClick(View view)
        {
            if (goDownLoad(info, view) == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls1()
        {
            this$0 = SoundInforHistoryNewAdapter.this;
            info = soundinfo;
            holder = viewholder;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final SoundInforHistoryNewAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SoundInfo val$info;

        public void onClick(View view)
        {
            playSound(holder.playFlag, getData().indexOf(info), info, getData());
        }

        _cls2()
        {
            this$0 = SoundInforHistoryNewAdapter.this;
            holder = viewholder;
            info = soundinfo;
            super();
        }
    }


    private class _cls4
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final SoundInforHistoryNewAdapter this$0;
        final int val$position;

        public void onExecute()
        {
            HistoryManage.getInstance(mContext).deleteSound(position);
        }

        _cls4()
        {
            this$0 = SoundInforHistoryNewAdapter.this;
            position = i;
            super();
        }
    }


    private class _cls3
        implements android.widget.AdapterView.OnItemClickListener
    {

        final SoundInforHistoryNewAdapter this$0;
        final Likeable val$likeable;
        final View val$view;

        public void onItemClick(AdapterView adapterview, View view1, int i, long l)
        {
            if (menuDialog != null)
            {
                menuDialog.dismiss();
            }
            i;
            JVM INSTR tableswitch 0 2: default 48
        //                       0 49
        //                       1 73
        //                       2 177;
               goto _L1 _L2 _L3 _L4
_L1:
            return;
_L2:
            delItem(mData.indexOf(likeable));
            return;
_L3:
            if (!UserInfoMannage.hasLogined())
            {
                adapterview = new Intent(mContext, com/ximalaya/ting/android/activity/login/LoginActivity);
                adapterview.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(view));
                mContext.startActivity(adapterview);
                return;
            }
            if (likeable != null)
            {
                toLike(likeable.getId(), likeable.isLiked(), null, (TextView)view.findViewById(0x7f0a021c));
                likeable.toggleLikeStatus();
                return;
            }
              goto _L1
_L4:
            toComment(likeable.getId());
            return;
        }

        _cls3()
        {
            this$0 = SoundInforHistoryNewAdapter.this;
            likeable = likeable1;
            view = view1;
            super();
        }
    }

}
