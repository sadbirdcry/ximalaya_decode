// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.ProgressDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.library.view.dialog.MyProgressDialog;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ModelHelper;
import java.util.List;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            HotStationListAdapter

class val.view extends a
{

    ProgressDialog pd;
    final HotStationListAdapter this$0;
    final StationModel val$model;
    final boolean val$play;
    final boolean val$showPlay;
    final View val$view;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$view);
    }

    public void onFinish()
    {
        super.onFinish();
        pd.cancel();
    }

    public void onNetError(int i, String s)
    {
        Toast.makeText(HotStationListAdapter.access$300(HotStationListAdapter.this), "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
    }

    public void onStart()
    {
        super.onStart();
        pd = new MyProgressDialog(HotStationListAdapter.access$300(HotStationListAdapter.this));
        pd.setMessage("\u6B63\u5728\u52A0\u8F7D\u58F0\u97F3\u5217\u8868...");
        pd.show();
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            Toast.makeText(HotStationListAdapter.access$300(HotStationListAdapter.this), "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
        } else
        {
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = null;
            }
            if (s == null || s.getIntValue("ret") != 0)
            {
                Toast.makeText(HotStationListAdapter.access$300(HotStationListAdapter.this), "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                return;
            }
            s = s.getString("list");
            if (TextUtils.isEmpty(s))
            {
                Toast.makeText(HotStationListAdapter.access$300(HotStationListAdapter.this), "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                return;
            }
            s = JSON.parseArray(s, com/ximalaya/ting/android/model/sound/SoundInfoNew);
            if (s == null || s.size() == 0)
            {
                Toast.makeText(HotStationListAdapter.access$300(HotStationListAdapter.this), "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                return;
            }
            val$model.tracks = s;
            if (val$play && MyApplication.a() != null)
            {
                PlayTools.gotoPlay(0, null, 0, null, ModelHelper.toSoundInfo(s), 0, MyApplication.a(), val$showPlay, DataCollectUtil.getDataFromView(val$view));
                return;
            }
        }
    }

    alog()
    {
        this$0 = final_hotstationlistadapter;
        val$model = stationmodel;
        val$play = flag;
        val$showPlay = flag1;
        val$view = View.this;
        super();
    }
}
