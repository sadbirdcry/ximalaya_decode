// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.ximalaya.ting.android.activity.login.LoginActivity;
import com.ximalaya.ting.android.communication.DownLoadTools;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.feed.BaseFeedModel;
import com.ximalaya.ting.android.model.feed.ChildFeedModel;
import com.ximalaya.ting.android.model.feed.MsgFeedCollection;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.PlayTools;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.TimeHelper;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.Utilities;
import java.util.List;

public class NewThingsAdapter extends BaseAdapter
{
    public static class Careone_commentHolder
    {

        public ImageView img_care1;
        public ImageView img_care2;
        public ImageView img_care3;
        public ImageView img_care4;
        public View ll_careHeads;
        public View ll_care_tags;
        public LinearLayout ll_childs;
        public View ll_progress_false;
        public View ll_true;
        public ImageView msg_careone_head;
        public ToggleButton tb_careone_arrow;
        public TextView txt_careone_count;
        public TextView txt_careone_describe;
        public TextView txt_careone_time;
        public TextView txt_careone_username;
        public TextView txt_comment;

        public Careone_commentHolder()
        {
        }
    }


    private Context act;
    public Activity act2;
    private int lastLoadPos;
    private MsgFeedCollection mfc;
    private ProgressBar progressBar;
    public List showList;
    private String token;
    private long uid;

    public NewThingsAdapter(ProgressBar progressbar, Activity activity, MsgFeedCollection msgfeedcollection, long l, String s)
    {
        lastLoadPos = -1;
        mfc = msgfeedcollection;
        act = activity.getApplicationContext();
        uid = l;
        token = s;
        act2 = activity;
        progressBar = progressbar;
    }

    private void goDownLoad(ChildFeedModel childfeedmodel, View view)
    {
        childfeedmodel = new DownloadTask(childfeedmodel);
        if (childfeedmodel != null)
        {
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            downloadtools.goDownload(childfeedmodel, act, view);
            downloadtools.release();
        }
    }

    private void goPlaySound(BaseFeedModel basefeedmodel, int i, View view)
    {
        PlayTools.gotoPlay(7, ModelHelper.baseFeedToSoundInfoList(basefeedmodel), i, act2, DataCollectUtil.getDataFromView(view));
    }

    private void setGroup(final ChildFeedModel bc, final View v)
    {
        if (UserInfoMannage.hasLogined())
        {
            if (ToolUtil.isTrue(bc.isFollowed))
            {
                (new DialogBuilder(act2)).setMessage("\u786E\u5B9A\u8981\u53D6\u6D88\u5173\u6CE8\uFF1F").setOkBtn(new _cls9()).setCancelBtn(new _cls8()).showConfirm();
                return;
            } else
            {
                setGroupRequest(bc, v);
                return;
            }
        } else
        {
            bc = new Intent(act2, com/ximalaya/ting/android/activity/login/LoginActivity);
            bc.setFlags(0x20000000);
            bc.putExtra("xdcs_data_bundle", DataCollectUtil.getDataFromView(v));
            act2.startActivity(bc);
            return;
        }
    }

    private void setGroupRequest(final ChildFeedModel bc, final View v)
    {
        if (bc == null)
        {
            return;
        } else
        {
            (new _cls10()).myexec(new Void[0]);
            return;
        }
    }

    private void setLikeSomeOneChilds(BaseFeedModel basefeedmodel, int i, LinearLayout linearlayout)
    {
        linearlayout.removeAllViews();
        basefeedmodel = basefeedmodel.getChildFeedList();
        int j;
        if (basefeedmodel == null)
        {
            i = 0;
        } else
        {
            i = basefeedmodel.size();
        }
        j = 0;
        while (j < i) 
        {
            final ChildFeedModel cfm = (ChildFeedModel)basefeedmodel.get(j);
            View view = LayoutInflater.from(act).inflate(0x7f030059, null);
            ImageView imageview = (ImageView)view.findViewById(0x7f0a01b5);
            ToggleButton togglebutton = (ToggleButton)view.findViewById(0x7f0a01b9);
            if (uid == cfm.toUid.longValue())
            {
                togglebutton.setVisibility(8);
            } else
            {
                togglebutton.setVisibility(0);
                if (cfm.getIsFollowed().equals("true"))
                {
                    togglebutton.setChecked(true);
                    togglebutton.setTag("true");
                } else
                {
                    togglebutton.setChecked(false);
                    togglebutton.setTag("false");
                }
            }
            togglebutton.setOnClickListener(new _cls6());
            ImageManager2.from(act).displayImage(imageview, cfm.toImage, -1);
            imageview = (ImageView)view.findViewById(0x7f0a01b7);
            if (cfm.getIsVerified().equals("true"))
            {
                imageview.setVisibility(0);
            } else
            {
                imageview.setVisibility(8);
            }
            ((TextView)view.findViewById(0x7f0a01b6)).setText(cfm.getToNickName());
            ((TextView)view.findViewById(0x7f0a01b8)).setText((new StringBuilder()).append("\u58F0\u97F3  ").append(StringUtil.getFriendlyNumStr(cfm.getTracks())).append("  \u7C89\u4E1D  ").append(StringUtil.getFriendlyNumStr(cfm.getFollowers())).toString());
            view.setOnClickListener(new _cls7());
            linearlayout.addView(view);
            j++;
        }
    }

    private void setLikeSoundsChilds(final BaseFeedModel bfm, int i, LinearLayout linearlayout)
    {
        final Object cfm = bfm.getChildFeedList();
        final int pos;
        if (cfm == null)
        {
            i = 0;
        } else
        {
            i = ((List) (cfm)).size();
        }
        pos = 0;
        while (pos < i) 
        {
            cfm = (ChildFeedModel)bfm.getChildFeedList().get(pos);
            View view = LayoutInflater.from(act).inflate(0x7f03005a, null);
            Object obj = (ImageView)view.findViewById(0x7f0a01ba);
            ImageManager2.from(act).displayImage(((ImageView) (obj)), ((ChildFeedModel) (cfm)).getMtImagePath(), 0x7f0202df);
            obj = (TextView)view.findViewById(0x7f0a01bc);
            if (Utilities.isNotBlank(((ChildFeedModel) (cfm)).getUserSource()))
            {
                if (((ChildFeedModel) (cfm)).getUserSource().equals("1"))
                {
                    ((TextView) (obj)).setText(Html.fromHtml("<font color='#f18152'>\u539F\u521B</font>"));
                } else
                if (((ChildFeedModel) (cfm)).getUserSource().equals("2"))
                {
                    ((TextView) (obj)).setText(Html.fromHtml("<font color='#78b680'>\u91C7\u96C6</font>"));
                }
            }
            ((TextView)view.findViewById(0x7f0a01bb)).setText(((ChildFeedModel) (cfm)).getToNickName());
            ((TextView)view.findViewById(0x7f0a01bd)).setText(TimeHelper.CountTime((new StringBuilder()).append(((ChildFeedModel) (cfm)).getCreatedAt()).append("").toString()));
            ((TextView)view.findViewById(0x7f0a01be)).setText(((ChildFeedModel) (cfm)).getTitle());
            obj = (TextView)view.findViewById(0x7f0a01bf);
            if (((ChildFeedModel) (cfm)).getPlaytimes() == 0)
            {
                ((TextView) (obj)).setVisibility(8);
            } else
            {
                ((TextView) (obj)).setVisibility(0);
                ((TextView) (obj)).setText(StringUtil.getFriendlyNumStr(((ChildFeedModel) (cfm)).getPlaytimes()));
            }
            obj = (TextView)view.findViewById(0x7f0a01c0);
            if (((ChildFeedModel) (cfm)).getLikes() == 0)
            {
                ((TextView) (obj)).setVisibility(8);
            } else
            {
                ((TextView) (obj)).setVisibility(0);
                ((TextView) (obj)).setText(StringUtil.getFriendlyNumStr(((ChildFeedModel) (cfm)).getLikes()));
            }
            obj = (TextView)view.findViewById(0x7f0a01c1);
            if (((ChildFeedModel) (cfm)).getComments() == 0)
            {
                ((TextView) (obj)).setVisibility(8);
            } else
            {
                ((TextView) (obj)).setVisibility(0);
                ((TextView) (obj)).setText(StringUtil.getFriendlyNumStr(((ChildFeedModel) (cfm)).getComments()));
            }
            obj = (TextView)view.findViewById(0x7f0a01c2);
            if (((ChildFeedModel) (cfm)).getDuration() == null || 0.0D == ((ChildFeedModel) (cfm)).getDuration().doubleValue())
            {
                ((TextView) (obj)).setVisibility(8);
            } else
            {
                ((TextView) (obj)).setVisibility(0);
                ((TextView) (obj)).setText(TimeHelper.secondToMinute((new StringBuilder()).append(((ChildFeedModel) (cfm)).getDuration()).append("").toString()));
            }
            view.findViewById(0x7f0a01c3).setOnClickListener(new _cls4());
            view.setOnClickListener(new _cls5());
            linearlayout.addView(view);
            pos++;
        }
    }

    private void setSomeImgViewShowOrGone(Careone_commentHolder careone_commentholder, int i)
    {
        switch (i)
        {
        default:
            return;

        case 0: // '\0'
            careone_commentholder.img_care1.setVisibility(0);
            careone_commentholder.img_care2.setVisibility(8);
            careone_commentholder.img_care3.setVisibility(8);
            careone_commentholder.img_care4.setVisibility(8);
            return;

        case 1: // '\001'
            careone_commentholder.img_care1.setVisibility(0);
            careone_commentholder.img_care2.setVisibility(0);
            careone_commentholder.img_care3.setVisibility(8);
            careone_commentholder.img_care4.setVisibility(8);
            return;

        case 2: // '\002'
            careone_commentholder.img_care1.setVisibility(0);
            careone_commentholder.img_care2.setVisibility(0);
            careone_commentholder.img_care3.setVisibility(0);
            careone_commentholder.img_care4.setVisibility(8);
            return;

        case 3: // '\003'
            careone_commentholder.img_care1.setVisibility(0);
            break;
        }
        careone_commentholder.img_care2.setVisibility(0);
        careone_commentholder.img_care3.setVisibility(0);
        careone_commentholder.img_care4.setVisibility(0);
    }

    public void clearShowList()
    {
        if (showList != null)
        {
            showList.clear();
            showList = null;
        }
    }

    public int getCount()
    {
        if (mfc == null || mfc.getBaseFeedList() == null)
        {
            return 0;
        } else
        {
            return mfc.getBaseFeedList().size();
        }
    }

    public BaseFeedModel getItem(int i)
    {
        if (mfc == null || mfc.getBaseFeedList() == null)
        {
            return null;
        } else
        {
            return (BaseFeedModel)mfc.getBaseFeedList().get(i - 1);
        }
    }

    public volatile Object getItem(int i)
    {
        return getItem(i);
    }

    public long getItemId(int i)
    {
        return 0L;
    }

    public MsgFeedCollection getMfc()
    {
        return mfc;
    }

    public View getView(final int position, View view, ViewGroup viewgroup)
    {
        int j;
        boolean flag;
        flag = false;
        j = 0;
        if (position != lastLoadPos || view == null) goto _L2; else goto _L1
_L1:
        final Object cfm = view;
_L15:
        return ((View) (cfm));
_L2:
        final BaseFeedModel bfm;
        Object obj;
        int i;
        lastLoadPos = position;
        bfm = (BaseFeedModel)mfc.getBaseFeedList().get(position);
        if (view == null)
        {
            viewgroup = new Careone_commentHolder();
            view = LayoutInflater.from(act).inflate(0x7f030168, null);
            viewgroup.tb_careone_arrow = (ToggleButton)view.findViewById(0x7f0a055d);
            viewgroup.img_care1 = (ImageView)view.findViewById(0x7f0a0561);
            viewgroup.img_care2 = (ImageView)view.findViewById(0x7f0a0562);
            viewgroup.img_care3 = (ImageView)view.findViewById(0x7f0a0563);
            viewgroup.img_care4 = (ImageView)view.findViewById(0x7f0a0564);
            viewgroup.ll_careHeads = view.findViewById(0x7f0a0560);
            viewgroup.ll_care_tags = view.findViewById(0x7f0a055f);
            viewgroup.ll_childs = (LinearLayout)view.findViewById(0x7f0a0566);
            ((Careone_commentHolder) (viewgroup)).ll_childs.setTag("childs");
            viewgroup.ll_progress_false = view.findViewById(0x7f0a0645);
            viewgroup.ll_true = view.findViewById(0x7f0a0559);
            viewgroup.msg_careone_head = (ImageView)view.findViewById(0x7f0a055a);
            viewgroup.txt_careone_count = (TextView)view.findViewById(0x7f0a055e);
            viewgroup.txt_careone_describe = (TextView)view.findViewById(0x7f0a0565);
            viewgroup.txt_careone_time = (TextView)view.findViewById(0x7f0a055c);
            viewgroup.txt_careone_username = (TextView)view.findViewById(0x7f0a055b);
            viewgroup.txt_comment = (TextView)view.findViewById(0x7f0a01db);
            view.setTag(viewgroup);
        } else
        {
            viewgroup = (Careone_commentHolder)view.getTag();
        }
        if (!bfm.flag)
        {
            break; /* Loop/switch isn't completed */
        }
        ((Careone_commentHolder) (viewgroup)).ll_childs.setVisibility(8);
        ((Careone_commentHolder) (viewgroup)).ll_progress_false.setVisibility(8);
        ((Careone_commentHolder) (viewgroup)).ll_true.setVisibility(0);
        if (showList != null)
        {
            if (showList.contains((new StringBuilder()).append(position).append("").toString()))
            {
                ((Careone_commentHolder) (viewgroup)).ll_childs.setVisibility(0);
                ((Careone_commentHolder) (viewgroup)).tb_careone_arrow.setChecked(true);
            } else
            {
                ((Careone_commentHolder) (viewgroup)).ll_childs.setVisibility(8);
                ((Careone_commentHolder) (viewgroup)).tb_careone_arrow.setChecked(false);
            }
        } else
        {
            ((Careone_commentHolder) (viewgroup)).ll_childs.setVisibility(8);
            ((Careone_commentHolder) (viewgroup)).tb_careone_arrow.setChecked(false);
        }
        obj = bfm.getType();
        ImageManager2.from(act).displayImage(((Careone_commentHolder) (viewgroup)).msg_careone_head, bfm.imagePath, 0x7f0202df);
        ((Careone_commentHolder) (viewgroup)).txt_careone_username.setText(bfm.getNickName());
        if (bfm.getChildFeedList() == null)
        {
            i = 0;
        } else
        {
            i = bfm.getChildFeedList().size();
        }
        ((Careone_commentHolder) (viewgroup)).txt_careone_time.setText(TimeHelper.CountTime((new StringBuilder()).append(((ChildFeedModel)bfm.getChildFeedList().get(0)).getCreatedAt()).append("").toString()));
        ((Careone_commentHolder) (viewgroup)).tb_careone_arrow.setOnClickListener((new _cls1()).setL_child(((Careone_commentHolder) (viewgroup)).ll_childs));
        ((Careone_commentHolder) (viewgroup)).ll_childs.removeAllViews();
        if (!((String) (obj)).equals("ft"))
        {
            ((Careone_commentHolder) (viewgroup)).tb_careone_arrow.setVisibility(0);
            ((Careone_commentHolder) (viewgroup)).ll_care_tags.setVisibility(8);
        } else
        {
            ((Careone_commentHolder) (viewgroup)).tb_careone_arrow.setVisibility(8);
            ((Careone_commentHolder) (viewgroup)).ll_care_tags.setVisibility(0);
        }
        if (!((String) (obj)).equals("ff"))
        {
            break MISSING_BLOCK_LABEL_929;
        }
        ((Careone_commentHolder) (viewgroup)).txt_careone_count.setText((new StringBuilder()).append("\u5173\u6CE8\u4E86").append(i).append("\u4E2A\u4EBA").toString());
        ((Careone_commentHolder) (viewgroup)).ll_careHeads.setVisibility(0);
        ((Careone_commentHolder) (viewgroup)).txt_comment.setVisibility(8);
        ((Careone_commentHolder) (viewgroup)).txt_careone_describe.setVisibility(8);
        cfm = view;
        if (j >= i)
        {
            continue; /* Loop/switch isn't completed */
        }
        cfm = (ChildFeedModel)bfm.getChildFeedList().get(j);
        setSomeImgViewShowOrGone(viewgroup, j);
        switch (j)
        {
        default:
            break;

        case 0: // '\0'
            break; /* Loop/switch isn't completed */

        case 1: // '\001'
            ImageManager2.from(act).displayImage(((Careone_commentHolder) (viewgroup)).img_care2, ((ChildFeedModel) (cfm)).getToImage(), 0x7f0202df);
            continue; /* Loop/switch isn't completed */

        case 2: // '\002'
            ImageManager2.from(act).displayImage(((Careone_commentHolder) (viewgroup)).img_care3, ((ChildFeedModel) (cfm)).getToImage(), 0x7f0202df);
            continue; /* Loop/switch isn't completed */

        case 3: // '\003'
            break;
        }
        break MISSING_BLOCK_LABEL_904;
_L6:
        if (showList != null && showList.contains((new StringBuilder()).append(position).append("").toString()))
        {
            setLikeSomeOneChilds(bfm, position, ((Careone_commentHolder) (viewgroup)).ll_childs);
        }
        j++;
        if (true) goto _L4; else goto _L3
_L4:
        break MISSING_BLOCK_LABEL_617;
_L3:
        ImageManager2.from(act).displayImage(((Careone_commentHolder) (viewgroup)).img_care1, ((ChildFeedModel) (cfm)).getToImage(), 0x7f0202df);
        continue; /* Loop/switch isn't completed */
        ImageManager2.from(act).displayImage(((Careone_commentHolder) (viewgroup)).img_care4, ((ChildFeedModel) (cfm)).getToImage(), 0x7f0202df);
        if (true) goto _L6; else goto _L5
_L5:
        if (!((String) (obj)).equals("ft"))
        {
            break MISSING_BLOCK_LABEL_1143;
        }
        ((Careone_commentHolder) (viewgroup)).txt_careone_count.setText((new StringBuilder()).append("\u611F\u5174\u8DA3\u4E86").append(i).append("\u4E2A\u6807\u7B7E").toString());
        ((Careone_commentHolder) (viewgroup)).ll_careHeads.setVisibility(8);
        ((Careone_commentHolder) (viewgroup)).txt_comment.setVisibility(8);
        ((Careone_commentHolder) (viewgroup)).txt_careone_describe.setVisibility(8);
        position = ((flag) ? 1 : 0);
_L12:
        cfm = view;
        if (position >= i)
        {
            continue; /* Loop/switch isn't completed */
        }
        cfm = view;
        if (i == 3)
        {
            continue; /* Loop/switch isn't completed */
        }
        cfm = (ChildFeedModel)bfm.getChildFeedList().get(position);
        position;
        JVM INSTR tableswitch 0 2: default 1064
    //                   0 1071
    //                   1 1095
    //                   2 1119;
           goto _L7 _L8 _L9 _L10
_L10:
        break MISSING_BLOCK_LABEL_1119;
_L7:
        break; /* Loop/switch isn't completed */
_L8:
        break; /* Loop/switch isn't completed */
_L13:
        position++;
        if (true) goto _L12; else goto _L11
_L11:
        ((TextView)((Careone_commentHolder) (viewgroup)).ll_care_tags.findViewById(0x7f0a0454)).setText(((ChildFeedModel) (cfm)).getTagName());
          goto _L13
_L9:
        ((TextView)((Careone_commentHolder) (viewgroup)).ll_care_tags.findViewById(0x7f0a0455)).setText(((ChildFeedModel) (cfm)).getTagName());
          goto _L13
        ((TextView)((Careone_commentHolder) (viewgroup)).ll_care_tags.findViewById(0x7f0a0456)).setText(((ChildFeedModel) (cfm)).getTagName());
          goto _L13
        if (((String) (obj)).equals("fc"))
        {
            cfm = (ChildFeedModel)bfm.getChildFeedList().get(0);
            ((Careone_commentHolder) (viewgroup)).txt_careone_count.setText(Html.fromHtml((new StringBuilder()).append("\u8BC4\u8BBA\u4E86<font color='#F86442'>").append(((ChildFeedModel) (cfm)).getToNickName()).append("</font>\u7684\u58F0\u97F3").toString()));
            ((Careone_commentHolder) (viewgroup)).txt_comment.setText(((ChildFeedModel) (cfm)).getContent());
            ((Careone_commentHolder) (viewgroup)).txt_careone_describe.setText(((ChildFeedModel) (cfm)).getTitle());
            ((Careone_commentHolder) (viewgroup)).ll_careHeads.setVisibility(8);
            ((Careone_commentHolder) (viewgroup)).txt_comment.setVisibility(0);
            ((Careone_commentHolder) (viewgroup)).txt_careone_describe.setVisibility(0);
            obj = LayoutInflater.from(act).inflate(0x7f03005a, null);
            Object obj1 = (ImageView)((View) (obj)).findViewById(0x7f0a01ba);
            TextView textview = (TextView)((View) (obj)).findViewById(0x7f0a01bc);
            if (Utilities.isNotBlank(((ChildFeedModel) (cfm)).getUserSource()))
            {
                if (((ChildFeedModel) (cfm)).getUserSource().equals("1"))
                {
                    textview.setText(Html.fromHtml("<font color='#f18152'>\u539F\u521B</font>"));
                } else
                if (((ChildFeedModel) (cfm)).getUserSource().equals("2"))
                {
                    textview.setText(Html.fromHtml("<font color='#78b680'>\u91C7\u96C6</font>"));
                }
            }
            ImageManager2.from(act).displayImage(((ImageView) (obj1)), ((ChildFeedModel) (cfm)).getMtImagePath(), 0x7f0202df);
            ((TextView)((View) (obj)).findViewById(0x7f0a01bb)).setText(((ChildFeedModel) (cfm)).getToNickName());
            ((TextView)((View) (obj)).findViewById(0x7f0a01bd)).setText(TimeHelper.CountTime((new StringBuilder()).append(((ChildFeedModel) (cfm)).getCreatedAt()).append("").toString()));
            ((TextView)((View) (obj)).findViewById(0x7f0a01be)).setText(((ChildFeedModel) (cfm)).getTitle());
            obj1 = (TextView)((View) (obj)).findViewById(0x7f0a01bf);
            if (((ChildFeedModel) (cfm)).getPlaytimes() == 0)
            {
                ((TextView) (obj1)).setVisibility(8);
            } else
            {
                ((TextView) (obj1)).setVisibility(0);
                ((TextView) (obj1)).setText(StringUtil.getFriendlyNumStr(((ChildFeedModel) (cfm)).getPlaytimes()));
            }
            obj1 = (TextView)((View) (obj)).findViewById(0x7f0a01c0);
            if (((ChildFeedModel) (cfm)).getLikes() == 0)
            {
                ((TextView) (obj1)).setVisibility(8);
            } else
            {
                ((TextView) (obj1)).setVisibility(0);
                ((TextView) (obj1)).setText(StringUtil.getFriendlyNumStr(((ChildFeedModel) (cfm)).getLikes()));
            }
            obj1 = (TextView)((View) (obj)).findViewById(0x7f0a01c1);
            if (((ChildFeedModel) (cfm)).getComments() == 0)
            {
                ((TextView) (obj1)).setVisibility(8);
            } else
            {
                ((TextView) (obj1)).setVisibility(0);
                ((TextView) (obj1)).setText(StringUtil.getFriendlyNumStr(((ChildFeedModel) (cfm)).getComments()));
            }
            obj1 = (TextView)((View) (obj)).findViewById(0x7f0a01c2);
            if (((ChildFeedModel) (cfm)).getDuration() == null || 0.0D == ((ChildFeedModel) (cfm)).getDuration().doubleValue())
            {
                ((TextView) (obj1)).setVisibility(8);
            } else
            {
                ((TextView) (obj1)).setVisibility(0);
                ((TextView) (obj1)).setText(TimeHelper.secondToMinute((new StringBuilder()).append(((ChildFeedModel) (cfm)).getDuration()).append("").toString()));
            }
            ((View) (obj)).findViewById(0x7f0a01c3).setOnClickListener(new _cls2());
            ((View) (obj)).setOnClickListener(new _cls3());
            ((Careone_commentHolder) (viewgroup)).ll_childs.addView(((View) (obj)));
            return view;
        }
        cfm = view;
        if (((String) (obj)).equals("fl"))
        {
            ((Careone_commentHolder) (viewgroup)).txt_careone_count.setText((new StringBuilder()).append("\u559C\u6B22\u4E86").append(i).append("\u4E2A\u58F0\u97F3").toString());
            ((Careone_commentHolder) (viewgroup)).ll_careHeads.setVisibility(8);
            ((Careone_commentHolder) (viewgroup)).txt_comment.setVisibility(8);
            ((Careone_commentHolder) (viewgroup)).txt_careone_describe.setVisibility(0);
            cfm = (ChildFeedModel)bfm.getChildFeedList().get(0);
            ((Careone_commentHolder) (viewgroup)).txt_comment.setText(((ChildFeedModel) (cfm)).getContent());
            ((Careone_commentHolder) (viewgroup)).txt_careone_describe.setText(((ChildFeedModel) (cfm)).getTitle());
            cfm = view;
            if (showList != null)
            {
                cfm = view;
                if (showList.contains((new StringBuilder()).append(position).append("").toString()))
                {
                    setLikeSoundsChilds(bfm, position, ((Careone_commentHolder) (viewgroup)).ll_childs);
                    return view;
                }
            }
        }
        if (true) goto _L15; else goto _L14
_L14:
        ((Careone_commentHolder) (viewgroup)).ll_true.setVisibility(8);
        ((Careone_commentHolder) (viewgroup)).ll_progress_false.setVisibility(0);
        return view;
    }

    public void onArrowImgClick(int i, LinearLayout linearlayout)
    {
        BaseFeedModel basefeedmodel = (BaseFeedModel)mfc.getBaseFeedList().get(i);
        if (basefeedmodel.getType().equals("ff"))
        {
            setLikeSomeOneChilds(basefeedmodel, i, linearlayout);
        } else
        if (basefeedmodel.getType().equals("fl"))
        {
            setLikeSoundsChilds(basefeedmodel, i, linearlayout);
            return;
        }
    }

    public void setMfc(MsgFeedCollection msgfeedcollection)
    {
        mfc = msgfeedcollection;
    }






    private class _cls9
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final NewThingsAdapter this$0;
        final ChildFeedModel val$bc;
        final View val$v;

        public void onExecute()
        {
            setGroupRequest(bc, v);
        }

        _cls9()
        {
            this$0 = NewThingsAdapter.this;
            bc = childfeedmodel;
            v = view;
            super();
        }
    }


    private class _cls8
        implements com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback
    {

        final NewThingsAdapter this$0;
        final View val$v;

        public void onExecute()
        {
            ((ToggleButton)v).setChecked(true);
        }

        _cls8()
        {
            this$0 = NewThingsAdapter.this;
            v = view;
            super();
        }
    }


    private class _cls10 extends MyAsyncTask
    {

        final NewThingsAdapter this$0;
        final ChildFeedModel val$bc;
        final View val$v;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient String doInBackground(Void avoid[])
        {
            if (MyApplication.b() != null)
            {
                return CommonRequest.doSetGroup(MyApplication.b(), (new StringBuilder()).append(bc.toUid).append("").toString(), ToolUtil.isTrue(bc.isFollowed), v, v);
            } else
            {
                return null;
            }
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((String)obj);
        }

        protected void onPostExecute(String s)
        {
            if (act2 == null || act2.isFinishing())
            {
                return;
            }
            progressBar.setVisibility(8);
            if (s == null)
            {
                if ("true".equals(bc.isFollowed))
                {
                    bc.isFollowed = "false";
                } else
                {
                    bc.isFollowed = "true";
                }
                ((ToggleButton)v).setChecked(ToolUtil.isTrue(bc.isFollowed));
                return;
            } else
            {
                Toast.makeText(act2, s, 0).show();
                ((ToggleButton)v).setChecked(false);
                return;
            }
        }

        protected void onPreExecute()
        {
            progressBar.setVisibility(0);
        }

        _cls10()
        {
            this$0 = NewThingsAdapter.this;
            bc = childfeedmodel;
            v = view;
            super();
        }
    }


    private class _cls6
        implements android.view.View.OnClickListener
    {

        final NewThingsAdapter this$0;
        final ChildFeedModel val$cfm;

        public void onClick(View view)
        {
            setGroup(cfm, view);
        }

        _cls6()
        {
            this$0 = NewThingsAdapter.this;
            cfm = childfeedmodel;
            super();
        }
    }


    private class _cls7
        implements android.view.View.OnClickListener
    {

        final NewThingsAdapter this$0;
        final ChildFeedModel val$cfm;

        public void onClick(View view)
        {
            if (act2 != null && (act2 instanceof MainTabActivity2))
            {
                view = new Bundle();
                view.putLong("toUid", cfm.toUid.longValue());
                ((MainTabActivity2)act2).startFragment(com/ximalaya/ting/android/fragment/userspace/OtherSpaceFragment, view);
            }
        }

        _cls7()
        {
            this$0 = NewThingsAdapter.this;
            cfm = childfeedmodel;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final NewThingsAdapter this$0;
        final ChildFeedModel val$cfm;

        public void onClick(View view)
        {
            goDownLoad(cfm, view);
        }

        _cls4()
        {
            this$0 = NewThingsAdapter.this;
            cfm = childfeedmodel;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final NewThingsAdapter this$0;
        final BaseFeedModel val$bfm;
        final int val$pos;

        public void onClick(View view)
        {
            goPlaySound(bfm, pos, view);
        }

        _cls5()
        {
            this$0 = NewThingsAdapter.this;
            bfm = basefeedmodel;
            pos = i;
            super();
        }
    }


    private class _cls1
        implements android.view.View.OnClickListener
    {

        LinearLayout l_child;
        final NewThingsAdapter this$0;
        final int val$position;

        public void onClick(View view)
        {
            if (showList == null)
            {
                showList = new ArrayList();
            }
            if (l_child.getVisibility() == 8)
            {
                l_child.setVisibility(0);
                if (l_child.getChildCount() == 0)
                {
                    onArrowImgClick(position, l_child);
                }
                showList.add((new StringBuilder()).append(position).append("").toString());
                return;
            } else
            {
                l_child.setVisibility(8);
                showList.remove((new StringBuilder()).append(position).append("").toString());
                return;
            }
        }

        public android.view.View.OnClickListener setL_child(LinearLayout linearlayout)
        {
            l_child = linearlayout;
            return this;
        }

        _cls1()
        {
            this$0 = NewThingsAdapter.this;
            position = i;
            super();
        }
    }


    private class _cls2
        implements android.view.View.OnClickListener
    {

        final NewThingsAdapter this$0;
        final ChildFeedModel val$cfm;

        public void onClick(View view)
        {
            goDownLoad(cfm, view);
        }

        _cls2()
        {
            this$0 = NewThingsAdapter.this;
            cfm = childfeedmodel;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final NewThingsAdapter this$0;
        final BaseFeedModel val$bfm;

        public void onClick(View view)
        {
            goPlaySound(bfm, 0, view);
        }

        _cls3()
        {
            this$0 = NewThingsAdapter.this;
            bfm = basefeedmodel;
            super();
        }
    }

}
