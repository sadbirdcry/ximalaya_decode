// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.Toast;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.b.a;
import com.ximalaya.ting.android.model.broadcast.StationModel;
import com.ximalaya.ting.android.model.holder.PersionStationBigHolder;
import com.ximalaya.ting.android.util.DataCollectUtil;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            HotStationListAdapter

class val.view extends a
{

    final HotStationListAdapter this$0;
    final Context val$context;
    final PersionStationBigHolder val$holder;
    final StationModel val$model;
    final View val$view;

    public void onBindXDCS(Header aheader[])
    {
        DataCollectUtil.bindDataToView(aheader, val$view);
    }

    public void onFinish()
    {
        super.onFinish();
        HotStationListAdapter.access$200(HotStationListAdapter.this, val$holder, val$model.isFollowed);
    }

    public void onNetError(int i, String s)
    {
        Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
    }

    public void onSuccess(String s)
    {
        if (TextUtils.isEmpty(s))
        {
            Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
            return;
        }
        s = JSON.parseObject(s);
        if (s == null || s.getIntValue("ret") != 0)
        {
            Toast.makeText(val$context, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u4E86\uFF0C\u64CD\u4F5C\u672A\u5B8C\u6210", 0).show();
            return;
        }
        s = val$model;
        boolean flag;
        if (!val$model.isFollowed)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        s.isFollowed = flag;
        if (val$model.isFollowed)
        {
            s = "\u5173\u6CE8\u6210\u529F\uFF01";
        } else
        {
            s = "\u53D6\u6D88\u5173\u6CE8\u6210\u529F";
        }
        Toast.makeText(val$context, s, 0).show();
    }

    lder()
    {
        this$0 = final_hotstationlistadapter;
        val$context = context1;
        val$model = stationmodel;
        val$holder = persionstationbigholder;
        val$view = View.this;
        super();
    }
}
