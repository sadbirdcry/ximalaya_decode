// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.holder.AlbumItemHolder;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.modelmanage.AlbumModelManage;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.modelnew.AlbumModelNew;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import com.ximalaya.ting.android.util.DataCollectUtil;
import com.ximalaya.ting.android.util.ImageManager2;
import com.ximalaya.ting.android.util.ModelHelper;
import com.ximalaya.ting.android.util.StringUtil;
import com.ximalaya.ting.android.util.ToolUtil;
import com.ximalaya.ting.android.util.ViewUtil;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.adapter:
//            BaseListSoundsAdapter

public class SubjectDetailAdapter extends BaseListSoundsAdapter
{

    private int mContentType;
    private ListView mListView;

    public SubjectDetailAdapter(Activity activity, ListView listview, int i, List list)
    {
        super(activity, list);
        mListView = listview;
        mContentType = i;
    }

    private View getAlbumView(int i, View view, final ViewGroup holder)
    {
        boolean flag1 = true;
        AlbumModelNew albummodelnew = (AlbumModelNew)mData.get(i);
        String s;
        TextView textview;
        boolean flag;
        if (view == null)
        {
            view = AlbumItemHolder.getView(mContext);
            holder = (AlbumItemHolder)view.getTag();
            ((AlbumItemHolder) (holder)).collect.setOnClickListener(new _cls1());
        } else
        {
            holder = (AlbumItemHolder)view.getTag();
        }
        ImageManager2.from(mContext).displayImage(((AlbumItemHolder) (holder)).cover, albummodelnew.albumCoverUrl290, 0x7f0202e0);
        ((AlbumItemHolder) (holder)).name.setText(albummodelnew.title);
        if (albummodelnew.playsCounts > 0)
        {
            ((AlbumItemHolder) (holder)).playCount.setVisibility(0);
            ((AlbumItemHolder) (holder)).playCount.setText(StringUtil.getFriendlyNumStr(albummodelnew.playsCounts));
        } else
        {
            ((AlbumItemHolder) (holder)).playCount.setVisibility(8);
        }
        ((AlbumItemHolder) (holder)).collectCount.setVisibility(0);
        ((AlbumItemHolder) (holder)).collectCount.setText((new StringBuilder()).append(StringUtil.getFriendlyNumStr(albummodelnew.tracksCounts)).append("\u96C6").toString());
        textview = ((AlbumItemHolder) (holder)).updateAt;
        if (albummodelnew.intro == null)
        {
            s = "";
        } else
        {
            s = albummodelnew.intro;
        }
        textview.setText(s);
        ((AlbumItemHolder) (holder)).collect.setTag(0x7f090000, albummodelnew);
        setCollectStatus(holder, albummodelnew.isFavorite);
        holder = mContext;
        if (i == 0)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        if (i + 1 != mData.size())
        {
            flag1 = false;
        }
        ViewUtil.buildAlbumItemSpace(holder, view, flag, flag1);
        return view;
    }

    private void setCollectStatus(AlbumItemHolder albumitemholder, boolean flag)
    {
        if (flag)
        {
            albumitemholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ee, 0, 0);
            albumitemholder.collectTxt.setText("\u5DF2\u6536\u85CF");
            albumitemholder.collectTxt.setTextColor(Color.parseColor("#999999"));
            return;
        } else
        {
            albumitemholder.collectTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0x7f0200ed, 0, 0);
            albumitemholder.collectTxt.setText("\u6536\u85CF");
            albumitemholder.collectTxt.setTextColor(Color.parseColor("#f86442"));
            return;
        }
    }

    private void subscribeAlbum(final AlbumModelNew model, final AlbumItemHolder holder, final View view)
    {
        if (UserInfoMannage.hasLogined())
        {
            UserInfoMannage.getInstance().getUser();
            String s;
            RequestParams requestparams;
            if (model.isFavorite)
            {
                s = "mobile/album/subscribe/delete";
            } else
            {
                s = "mobile/album/subscribe/create";
            }
            requestparams = new RequestParams();
            requestparams.add("albumId", (new StringBuilder()).append("").append(model.id).toString());
            f.a().b(s, requestparams, DataCollectUtil.getDataFromView(view), new _cls2());
        } else
        {
            final AlbumModel am = ModelHelper.toAlbumModel(model);
            if (AlbumModelManage.getInstance().ensureLocalCollectAllow(mContext, am, view))
            {
                (new _cls3()).myexec(new Void[0]);
                return;
            }
        }
    }

    protected void bindData(final Object model, final BaseListSoundsAdapter.ViewHolder holder)
    {
        if (model instanceof SoundInfoNew)
        {
            model = (SoundInfoNew)model;
            holder.cover.setTag(0x7f0a0037, Boolean.valueOf(true));
            ImageManager2.from(mContext).displayImage(holder.cover, ((SoundInfoNew) (model)).coverSmall, 0x7f0202de);
            if (((SoundInfoNew) (model)).isFavorite)
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(0x7f02008f, 0, 0, 0);
            } else
            {
                holder.likeCount.setCompoundDrawablesWithIntrinsicBounds(0x7f020091, 0, 0, 0);
            }
            holder.title.setText(((SoundInfoNew) (model)).title);
            holder.owner.setText((new StringBuilder()).append("by ").append(((SoundInfoNew) (model)).nickname).toString());
            holder.playCount.setText(StringUtil.getFriendlyNumStr(((SoundInfoNew) (model)).playsCounts));
            holder.likeCount.setText(StringUtil.getFriendlyNumStr(((SoundInfoNew) (model)).favoritesCounts));
            holder.commentCount.setText(StringUtil.getFriendlyNumStr(((SoundInfoNew) (model)).commentsCounts));
            if (((SoundInfoNew) (model)).duration > 0.0F)
            {
                holder.duration.setText((new StringBuilder()).append("").append(ToolUtil.toTime((long)((SoundInfoNew) (model)).duration)).toString());
                holder.duration.setVisibility(0);
            } else
            {
                holder.duration.setVisibility(8);
            }
            holder.createTime.setVisibility(0);
            holder.createTime.setText(ToolUtil.convertTime(((SoundInfoNew) (model)).createdAt));
            if (((SoundInfoNew) (model)).userSource == 1)
            {
                holder.origin.setText("\u539F\u521B");
            } else
            {
                holder.origin.setText("\u91C7\u96C6");
            }
            if (isPlaying(((SoundInfoNew) (model)).id))
            {
                if (LocalMediaService.getInstance().isPaused())
                {
                    holder.playFlag.setImageResource(0x7f020250);
                } else
                {
                    holder.playFlag.setImageResource(0x7f02024f);
                }
            } else
            {
                holder.playFlag.setImageResource(0x7f020250);
            }
            if (isDownload(((SoundInfoNew) (model)).id))
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            } else
            {
                holder.btn.setImageResource(0x7f0201fe);
                holder.btn.setEnabled(true);
            }
            holder.btn.setOnClickListener(new _cls4());
            holder.cover.setOnClickListener(new _cls5());
        }
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (mContentType == 1)
        {
            return getAlbumView(i, view, viewgroup);
        }
        if (mContentType == 2)
        {
            return super.getView(i, view, viewgroup);
        } else
        {
            Log.e("", (new StringBuilder()).append("Xm wrong content type ").append(mContentType).toString());
            return view;
        }
    }

    public void updateItem(int i)
    {
        if (i >= 0 && i < getCount())
        {
            int j = mListView.getFirstVisiblePosition();
            int k = mListView.getHeaderViewsCount();
            Object obj = mListView.getChildAt(i - (j - k));
            if (obj != null)
            {
                obj = (AlbumItemHolder)((View) (obj)).getTag();
                if (obj != null)
                {
                    AlbumModelNew albummodelnew = (AlbumModelNew)mData.get(i);
                    ((AlbumItemHolder) (obj)).collect.setVisibility(0);
                    setCollectStatus(((AlbumItemHolder) (obj)), albummodelnew.isFavorite);
                    return;
                }
            }
        }
    }



    private class _cls1
        implements android.view.View.OnClickListener
    {

        final SubjectDetailAdapter this$0;
        final AlbumItemHolder val$holder;

        public void onClick(View view)
        {
            AlbumModelNew albummodelnew = (AlbumModelNew)view.getTag(0x7f090000);
            if (albummodelnew == null)
            {
                return;
            } else
            {
                subscribeAlbum(albummodelnew, holder, view);
                return;
            }
        }

        _cls1()
        {
            this$0 = SubjectDetailAdapter.this;
            holder = albumitemholder;
            super();
        }
    }


    private class _cls2 extends a
    {

        final SubjectDetailAdapter this$0;
        final AlbumItemHolder val$holder;
        final AlbumModelNew val$model;
        final View val$view;

        public void onBindXDCS(Header aheader[])
        {
            DataCollectUtil.bindDataToView(aheader, view);
        }

        public void onFinish()
        {
            super.onFinish();
            holder.collect.setEnabled(true);
        }

        public void onNetError(int i, String s)
        {
            Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
            setCollectStatus(holder, model.isFavorite);
        }

        public void onStart()
        {
            super.onStart();
            holder.collect.setEnabled(false);
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                Toast.makeText(mContext, "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01", 0).show();
                setCollectStatus(holder, model.isFavorite);
                return;
            }
            Object obj = null;
            try
            {
                s = JSON.parseObject(s);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                s = obj;
            }
            if (s != null && s.getIntValue("ret") == 0)
            {
                if (model.isFavorite)
                {
                    s = "\u53D6\u6D88\u6536\u85CF\u6210\u529F\uFF01";
                    model.isFavorite = false;
                } else
                {
                    s = "\u6536\u85CF\u6210\u529F\uFF01";
                    model.isFavorite = true;
                }
                setCollectStatus(holder, model.isFavorite);
                Toast.makeText(mContext, s, 0).show();
                return;
            }
            s = s.getString("msg");
            if (TextUtils.isEmpty(s))
            {
                s = "\u4EB2\uFF0C\u7F51\u7EDC\u9519\u8BEF\u5566\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5\uFF01";
            }
            Toast.makeText(mContext, s, 0).show();
            setCollectStatus(holder, model.isFavorite);
        }

        _cls2()
        {
            this$0 = SubjectDetailAdapter.this;
            holder = albumitemholder;
            model = albummodelnew;
            view = view1;
            super();
        }
    }


    private class _cls3 extends MyAsyncTask
    {

        final SubjectDetailAdapter this$0;
        final AlbumModel val$am;
        final AlbumItemHolder val$holder;
        final AlbumModelNew val$model;

        protected volatile Object doInBackground(Object aobj[])
        {
            return doInBackground((Void[])aobj);
        }

        protected transient Void doInBackground(Void avoid[])
        {
            avoid = AlbumModelManage.getInstance();
            AlbumModelNew albummodelnew = model;
            boolean flag;
            if (!model.isFavorite)
            {
                flag = true;
            } else
            {
                flag = false;
            }
            albummodelnew.isFavorite = flag;
            if (!model.isFavorite)
            {
                avoid.deleteAlbumInLocalAlbumList(am);
            } else
            {
                avoid.saveAlbumModel(am);
            }
            return null;
        }

        protected volatile void onPostExecute(Object obj)
        {
            onPostExecute((Void)obj);
        }

        protected void onPostExecute(Void void1)
        {
            void1 = (AlbumModelNew)holder.collect.getTag(0x7f090000);
            if (void1 != null && ((AlbumModelNew) (void1)).id == model.id)
            {
                AlbumItemHolder.setCollectStatus(holder, model.isFavorite);
            }
        }

        _cls3()
        {
            this$0 = SubjectDetailAdapter.this;
            model = albummodelnew;
            am = albummodel;
            holder = albumitemholder;
            super();
        }
    }


    private class _cls4
        implements android.view.View.OnClickListener
    {

        final SubjectDetailAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SoundInfoNew val$model;

        public void onClick(View view)
        {
            DownloadTask downloadtask = new DownloadTask(ModelHelper.toSoundInfo(model));
            DownLoadTools downloadtools = DownLoadTools.getInstance();
            view = downloadtools.goDownload(downloadtask, mContext.getApplicationContext(), view);
            downloadtools.release();
            if (view == com.ximalaya.ting.android.a.a.a.a)
            {
                holder.btn.setImageResource(0x7f0200f1);
                holder.btn.setEnabled(false);
            }
        }

        _cls4()
        {
            this$0 = SubjectDetailAdapter.this;
            model = soundinfonew;
            holder = viewholder;
            super();
        }
    }


    private class _cls5
        implements android.view.View.OnClickListener
    {

        final SubjectDetailAdapter this$0;
        final BaseListSoundsAdapter.ViewHolder val$holder;
        final SoundInfoNew val$model;

        public void onClick(View view)
        {
            playSound(holder.playFlag, mData.indexOf(model), ModelHelper.toSoundInfo(model), ModelHelper.toSoundInfo(Arrays.asList(mData.toArray(new SoundInfoNew[0]))));
        }

        _cls5()
        {
            this$0 = SubjectDetailAdapter.this;
            holder = viewholder;
            model = soundinfonew;
            super();
        }
    }

}
