// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.carlink;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import java.util.ArrayList;
import java.util.List;

public class CarlinkSettingAct extends BaseActivity
{
    private class CarlinkAdapter extends BaseAdapter
    {

        final CarlinkSettingAct this$0;

        public int getCount()
        {
            return mData.size();
        }

        public Object getItem(int i)
        {
            return mData.get(i);
        }

        public long getItemId(int i)
        {
            return (long)i;
        }

        public View getView(int i, View view, ViewGroup viewgroup)
        {
            View view1 = view;
            if (view == null)
            {
                view1 = LayoutInflater.from(mActivity).inflate(0x7f03011b, viewgroup, false);
                view = new ViewHolder(null);
                view.icon = (ImageView)view1.findViewById(0x7f0a047b);
                view.title = (TextView)view1.findViewById(0x7f0a047c);
                view1.setTag(view);
            }
            view = (ViewHolder)view1.getTag();
            viewgroup = (CarlinkModel)mData.get(i);
            ((ViewHolder) (view)).title.setText(((CarlinkModel) (viewgroup)).name);
            return view1;
        }

        private CarlinkAdapter()
        {
            this$0 = CarlinkSettingAct.this;
            super();
        }

        CarlinkAdapter(_cls1 _pcls1)
        {
            this();
        }
    }

    private class CarlinkModel
    {

        public int id;
        public String name;
        final CarlinkSettingAct this$0;

        private CarlinkModel()
        {
            this$0 = CarlinkSettingAct.this;
            super();
        }

        CarlinkModel(_cls1 _pcls1)
        {
            this();
        }
    }

    private class ViewHolder
    {

        public ImageView icon;
        final CarlinkSettingAct this$0;
        public TextView title;

        private ViewHolder()
        {
            this$0 = CarlinkSettingAct.this;
            super();
        }

        ViewHolder(_cls1 _pcls1)
        {
            this();
        }
    }


    private BaseAdapter mAdapter;
    private List mData;
    private ListView mListView;
    private TextView mMsgLabel;

    public CarlinkSettingAct()
    {
        mData = new ArrayList();
    }

    private void initData()
    {
        CarlinkModel carlinkmodel = new CarlinkModel(null);
        carlinkmodel.id = 1;
        carlinkmodel.name = "\u798F\u7279SYNC";
        mData.add(carlinkmodel);
        carlinkmodel = new CarlinkModel(null);
        carlinkmodel.id = 2;
        carlinkmodel.name = "\u957F\u57CE\u54C8\u5F17";
        mData.add(carlinkmodel);
        carlinkmodel = new CarlinkModel(null);
        carlinkmodel.id = 3;
        carlinkmodel.name = "\u767E\u5EA6CarLife";
        mData.add(carlinkmodel);
        mAdapter.notifyDataSetChanged();
    }

    private void initUi()
    {
        mMsgLabel = (TextView)findViewById(0x7f0a006e);
        mListView = (ListView)findViewById(0x7f0a0025);
        setTitleText("\u8F66\u8F7D\u6A21\u5F0F");
        mAdapter = new CarlinkAdapter(null);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(new _cls1());
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030009);
        initCommon();
        initUi();
        initData();
    }


    private class _cls1
        implements android.widget.AdapterView.OnItemClickListener
    {

        final CarlinkSettingAct this$0;

        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            while (i < 0 || i >= mData.size() || ((CarlinkModel)mData.get(i)).id != 3) 
            {
                return;
            }
            startActivity(new Intent(mActivity, com/ximalaya/ting/android/carlink/carlife/CarlifeSettingAct));
        }

        _cls1()
        {
            this$0 = CarlinkSettingAct.this;
            super();
        }
    }

}
