// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.carlink.carlife;

import android.text.TextUtils;
import com.baidu.carlife.platform.request.CLGetSongDataReq;
import com.ximalaya.ting.android.model.finding2.rank.RankTrackListModel;
import com.ximalaya.ting.android.util.Logger;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

// Referenced classes of package com.ximalaya.ting.android.carlink.carlife:
//            CarlifeManager

private class mRequest
    implements Runnable
{

    private int mBuffSize;
    private CLGetSongDataReq mRequest;
    final CarlifeManager this$0;

    public void run()
    {
        Object obj = null;
        if (CarlifeManager.getInstance(CarlifeManager.access$500(CarlifeManager.this)).isConnected()) goto _L2; else goto _L1
_L1:
        return;
_L2:
        Logger.e("CarLifeManager", (new StringBuilder()).append("LoadSongDataTask reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
        Object obj1 = CarlifeManager.access$1100(CarlifeManager.this, CarlifeManager.access$800(CarlifeManager.this).getList(), Long.valueOf(mRequest.songId).longValue());
        if (obj1 != null) goto _L4; else goto _L3
_L3:
        Logger.e("CarLifeManager", (new StringBuilder()).append("getTrackById return null reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
        if (true) goto _L1; else goto _L5
_L5:
        try
        {
            throw new NullPointerException();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
_L7:
        ((Exception) (obj)).printStackTrace();
        return;
_L4:
        HttpURLConnection httpurlconnection;
        int i;
        httpurlconnection = (HttpURLConnection)(new URL(CarlifeManager.access$1200(CarlifeManager.this, ((com.ximalaya.ting.android.model.finding2.rank.RankTrackModel) (obj1))))).openConnection();
        httpurlconnection.connect();
        i = httpurlconnection.getResponseCode();
        obj1 = httpurlconnection.getHeaderField("Content-Length");
        long l;
        long l1 = -1L;
        l = l1;
        if (i != 200)
        {
            break MISSING_BLOCK_LABEL_242;
        }
        l = l1;
        if (TextUtils.isEmpty(((CharSequence) (obj1))))
        {
            break MISSING_BLOCK_LABEL_242;
        }
        l = Long.parseLong(((String) (obj1)));
        if (l > 0L)
        {
            break MISSING_BLOCK_LABEL_313;
        }
        Logger.e("CarLifeManager", (new StringBuilder()).append("getTrackData response len invalid ,len:").append(l).append(",reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
        if (true) goto _L1; else goto _L6
_L6:
        try
        {
            throw new NullPointerException();
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L7
        obj1 = httpurlconnection.getInputStream();
        obj = new byte[mBuffSize];
        if (CarlifeManager.access$1300(CarlifeManager.this, mRequest.requestId, mRequest.songId, null, 0, 0, (int)l, 0))
        {
            break MISSING_BLOCK_LABEL_414;
        }
        Logger.e("CarLifeManager", (new StringBuilder()).append("send sound start tag failed reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
        if (obj1 == null) goto _L1; else goto _L8
_L8:
        try
        {
            ((InputStream) (obj1)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L7
        i = 0;
_L10:
        int k = ((InputStream) (obj1)).read(((byte []) (obj)));
        int j;
        if (k <= 0)
        {
            break MISSING_BLOCK_LABEL_551;
        }
        j = i + k;
        i = j;
        if (CarlifeManager.access$1300(CarlifeManager.this, mRequest.requestId, mRequest.songId, ((byte []) (obj)), k, j, (int)l, 1)) goto _L10; else goto _L9
_L9:
        Logger.e("CarLifeManager", (new StringBuilder()).append("send sound content tag failed reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).append(",offset:").append(j).append(",total:").append(l).toString());
        if (obj1 == null) goto _L1; else goto _L11
_L11:
        try
        {
            ((InputStream) (obj1)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L7
        if (CarlifeManager.access$1300(CarlifeManager.this, mRequest.requestId, mRequest.songId, null, 0, i, (int)l, 2))
        {
            break MISSING_BLOCK_LABEL_641;
        }
        Logger.e("CarLifeManager", (new StringBuilder()).append("send sound end tag failed reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
        if (obj1 == null) goto _L1; else goto _L12
_L12:
        try
        {
            ((InputStream) (obj1)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L7
        Logger.e("CarLifeManager", (new StringBuilder()).append("send sound data success reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
        if (obj1 == null) goto _L1; else goto _L13
_L13:
        try
        {
            ((InputStream) (obj1)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L7
        obj1;
_L17:
        ((Exception) (obj1)).printStackTrace();
        if (obj == null) goto _L1; else goto _L14
_L14:
        try
        {
            ((InputStream) (obj)).close();
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Object obj) { }
          goto _L7
        obj;
        obj1 = null;
_L16:
        if (obj1 != null)
        {
            try
            {
                ((InputStream) (obj1)).close();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj1)
            {
                ((Exception) (obj1)).printStackTrace();
            }
        }
        throw obj;
        obj;
        continue; /* Loop/switch isn't completed */
        Object obj2;
        obj2;
        obj1 = obj;
        obj = obj2;
        if (true) goto _L16; else goto _L15
_L15:
        obj2;
        obj = obj1;
        obj1 = obj2;
          goto _L17
    }

    public (CLGetSongDataReq clgetsongdatareq)
    {
        this$0 = CarlifeManager.this;
        super();
        mBuffSize = 8192;
        mRequest = clgetsongdatareq;
    }
}
