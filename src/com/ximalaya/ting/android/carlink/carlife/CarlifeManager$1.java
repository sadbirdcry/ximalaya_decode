// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.carlink.carlife;

import com.baidu.carlife.platform.CLPlatformCallback;
import com.baidu.carlife.platform.request.CLGetAlbumListReq;
import com.baidu.carlife.platform.request.CLGetSongDataReq;
import com.baidu.carlife.platform.request.CLGetSongListReq;
import com.baidu.carlife.platform.request.CLRequest;
import com.baidu.carlife.platform.response.CLResponse;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.carlink.carlife:
//            CarlifeManager

class this._cls0
    implements CLPlatformCallback
{

    final CarlifeManager this$0;

    public void onCarlifeError(int i, String s)
    {
        Logger.e("CarLifeManager", (new StringBuilder()).append("onCarlifeError ").append(i).append(",").append(s).toString());
        switch (i)
        {
        case 2: // '\002'
        case 3: // '\003'
        case 4: // '\004'
        case 5: // '\005'
        case 6: // '\006'
        case 8: // '\b'
        default:
            return;

        case 7: // '\007'
            CarlifeManager.access$002(CarlifeManager.this, false);
            break;
        }
    }

    public void onCarlifeRequest(CLRequest clrequest)
    {
        if (clrequest == null)
        {
            Logger.e("CarLifeManager", "onCarlifeRequest get null request!!!");
        } else
        {
            if (clrequest instanceof CLGetSongDataReq)
            {
                Logger.e("CarLifeManager", (new StringBuilder()).append("onCarlifeRequest get CLGetSongDataReq reqId:").append(clrequest.requestId).toString());
                CarlifeManager.access$100(CarlifeManager.this, (CLGetSongDataReq)clrequest);
                return;
            }
            if (clrequest instanceof CLGetSongListReq)
            {
                Logger.e("CarLifeManager", (new StringBuilder()).append("onCarlifeRequest get CLGetSongListReq reqId:").append(clrequest.requestId).toString());
                CarlifeManager.access$200(CarlifeManager.this, (CLGetSongListReq)clrequest);
                return;
            }
            if (clrequest instanceof CLGetAlbumListReq)
            {
                Logger.e("CarLifeManager", (new StringBuilder()).append("onCarlifeRequest get CLGetAlbumListReq reqId:").append(clrequest.requestId).toString());
                CarlifeManager.access$300(CarlifeManager.this, (CLGetAlbumListReq)clrequest);
                return;
            }
        }
    }

    public void onCarlifeResponse(CLResponse clresponse)
    {
        Logger.e("CarLifeManager", "onCarlifeResponse");
    }

    public void onConnected()
    {
        CarlifeManager.access$002(CarlifeManager.this, true);
    }

    ()
    {
        this$0 = CarlifeManager.this;
        super();
    }
}
