// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.carlink.carlife;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;
import com.baidu.carlife.platform.CLPlatformCallback;
import com.baidu.carlife.platform.CLPlatformManager;
import com.baidu.carlife.platform.model.CLAlbum;
import com.baidu.carlife.platform.model.CLSong;
import com.baidu.carlife.platform.model.CLSongData;
import com.baidu.carlife.platform.request.CLGetAlbumListReq;
import com.baidu.carlife.platform.request.CLGetSongDataReq;
import com.baidu.carlife.platform.request.CLGetSongListReq;
import com.baidu.carlife.platform.request.CLRequest;
import com.baidu.carlife.platform.response.CLGetAlbumListResp;
import com.baidu.carlife.platform.response.CLGetSongDataResp;
import com.baidu.carlife.platform.response.CLGetSongListResp;
import com.baidu.carlife.platform.response.CLResponse;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.finding2.rank.RankTrackListModel;
import com.ximalaya.ting.android.model.finding2.rank.RankTrackModel;
import com.ximalaya.ting.android.util.ApiUtil;
import com.ximalaya.ting.android.util.Logger;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class CarlifeManager
{
    private class LoadSongDataTask
        implements Runnable
    {

        private int mBuffSize;
        private CLGetSongDataReq mRequest;
        final CarlifeManager this$0;

        public void run()
        {
            Object obj = null;
            if (CarlifeManager.getInstance(mContext).isConnected()) goto _L2; else goto _L1
_L1:
            return;
_L2:
            Logger.e("CarLifeManager", (new StringBuilder()).append("LoadSongDataTask reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
            Object obj1 = getTrackById(mRankTrackListModel.getList(), Long.valueOf(mRequest.songId).longValue());
            if (obj1 != null) goto _L4; else goto _L3
_L3:
            Logger.e("CarLifeManager", (new StringBuilder()).append("getTrackById return null reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
            if (true) goto _L1; else goto _L5
_L5:
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
_L7:
            ((Exception) (obj)).printStackTrace();
            return;
_L4:
            HttpURLConnection httpurlconnection;
            int i;
            httpurlconnection = (HttpURLConnection)(new URL(getTrackUrl(((RankTrackModel) (obj1))))).openConnection();
            httpurlconnection.connect();
            i = httpurlconnection.getResponseCode();
            obj1 = httpurlconnection.getHeaderField("Content-Length");
            long l;
            long l1 = -1L;
            l = l1;
            if (i != 200)
            {
                break MISSING_BLOCK_LABEL_242;
            }
            l = l1;
            if (TextUtils.isEmpty(((CharSequence) (obj1))))
            {
                break MISSING_BLOCK_LABEL_242;
            }
            l = Long.parseLong(((String) (obj1)));
            if (l > 0L)
            {
                break MISSING_BLOCK_LABEL_313;
            }
            Logger.e("CarLifeManager", (new StringBuilder()).append("getTrackData response len invalid ,len:").append(l).append(",reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
            if (true) goto _L1; else goto _L6
_L6:
            try
            {
                throw new NullPointerException();
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
              goto _L7
            obj1 = httpurlconnection.getInputStream();
            obj = new byte[mBuffSize];
            if (send(mRequest.requestId, mRequest.songId, null, 0, 0, (int)l, 0))
            {
                break MISSING_BLOCK_LABEL_414;
            }
            Logger.e("CarLifeManager", (new StringBuilder()).append("send sound start tag failed reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
            if (obj1 == null) goto _L1; else goto _L8
_L8:
            try
            {
                ((InputStream) (obj1)).close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
              goto _L7
            i = 0;
_L10:
            int k = ((InputStream) (obj1)).read(((byte []) (obj)));
            int j;
            if (k <= 0)
            {
                break MISSING_BLOCK_LABEL_551;
            }
            j = i + k;
            i = j;
            if (send(mRequest.requestId, mRequest.songId, ((byte []) (obj)), k, j, (int)l, 1)) goto _L10; else goto _L9
_L9:
            Logger.e("CarLifeManager", (new StringBuilder()).append("send sound content tag failed reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).append(",offset:").append(j).append(",total:").append(l).toString());
            if (obj1 == null) goto _L1; else goto _L11
_L11:
            try
            {
                ((InputStream) (obj1)).close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
              goto _L7
            if (send(mRequest.requestId, mRequest.songId, null, 0, i, (int)l, 2))
            {
                break MISSING_BLOCK_LABEL_641;
            }
            Logger.e("CarLifeManager", (new StringBuilder()).append("send sound end tag failed reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
            if (obj1 == null) goto _L1; else goto _L12
_L12:
            try
            {
                ((InputStream) (obj1)).close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
              goto _L7
            Logger.e("CarLifeManager", (new StringBuilder()).append("send sound data success reqId:").append(mRequest.requestId).append(",songId:").append(mRequest.songId).toString());
            if (obj1 == null) goto _L1; else goto _L13
_L13:
            try
            {
                ((InputStream) (obj1)).close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
              goto _L7
            obj1;
_L17:
            ((Exception) (obj1)).printStackTrace();
            if (obj == null) goto _L1; else goto _L14
_L14:
            try
            {
                ((InputStream) (obj)).close();
                return;
            }
            // Misplaced declaration of an exception variable
            catch (Object obj) { }
              goto _L7
            obj;
            obj1 = null;
_L16:
            if (obj1 != null)
            {
                try
                {
                    ((InputStream) (obj1)).close();
                }
                // Misplaced declaration of an exception variable
                catch (Object obj1)
                {
                    ((Exception) (obj1)).printStackTrace();
                }
            }
            throw obj;
            obj;
            continue; /* Loop/switch isn't completed */
            Object obj2;
            obj2;
            obj1 = obj;
            obj = obj2;
            if (true) goto _L16; else goto _L15
_L15:
            obj2;
            obj = obj1;
            obj1 = obj2;
              goto _L17
        }

        public LoadSongDataTask(CLGetSongDataReq clgetsongdatareq)
        {
            this$0 = CarlifeManager.this;
            super();
            mBuffSize = 8192;
            mRequest = clgetsongdatareq;
        }
    }


    private static final String APP_SECRET = "cl84b81a3b93";
    private static final String TAG = "CarLifeManager";
    private static CarlifeManager sInstance;
    private static byte sLock[] = new byte[0];
    private volatile boolean isConnected;
    private boolean isLoading;
    private boolean isUseHighBitrate;
    private CLPlatformCallback mCarLifeCallback;
    private CLPlatformManager mCarlifeManager;
    private Context mContext;
    private Handler mHandler;
    private int mPageId;
    private int mPageSize;
    private RankTrackListModel mRankTrackListModel;
    private HandlerThread mWorker;
    private Handler mWorkerHandler;

    private CarlifeManager(Context context)
    {
        isUseHighBitrate = true;
        mPageId = 1;
        mPageSize = 20;
        isLoading = false;
        mContext = context.getApplicationContext();
        mHandler = new Handler(Looper.getMainLooper());
        mWorker = new HandlerThread("carlife-loader");
        mWorker.start();
        mWorkerHandler = new Handler(mWorker.getLooper());
    }

    private boolean checkCarlifeStatus()
    {
        boolean flag = isConnected();
        if (!flag)
        {
            Logger.e("CarLifeManager", "checkCarlifeStatus false");
        }
        return flag;
    }

    private ArrayList getCLSongList(RankTrackListModel ranktracklistmodel)
    {
        if (ranktracklistmodel == null || ranktracklistmodel.getList() == null || ranktracklistmodel.getList().size() == 0)
        {
            return null;
        }
        ArrayList arraylist = new ArrayList();
        CLSong clsong;
        for (ranktracklistmodel = ranktracklistmodel.getList().iterator(); ranktracklistmodel.hasNext(); arraylist.add(clsong))
        {
            RankTrackModel ranktrackmodel = (RankTrackModel)ranktracklistmodel.next();
            clsong = new CLSong();
            clsong.mediaUrl = getTrackUrl(ranktrackmodel);
            clsong.name = ranktrackmodel.getTitle();
            clsong.albumName = ranktrackmodel.getAlbumTitle();
            clsong.albumId = Long.toString(ranktrackmodel.getAlbumId());
            clsong.albumArtistName = ranktrackmodel.getNickname();
            clsong.duration = Long.toString(ranktrackmodel.getDuration() * 1000L);
            clsong.id = Long.toString(ranktrackmodel.getTrackId());
            clsong.coverUrl = ranktrackmodel.getCoverSmall();
        }

        return arraylist;
    }

    private void getHotSounds(final CLRequest req)
    {
        if (isLoading)
        {
            return;
        }
        isLoading = true;
        if (!(req instanceof CLGetAlbumListReq)) goto _L2; else goto _L1
_L1:
        mPageId = 1;
_L4:
        String s = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/discovery/v1/rankingList/track").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("key", "ranking:track:played:1:0");
        requestparams.put("pageId", (new StringBuilder()).append("").append(mPageId).toString());
        requestparams.put("pageSize", (new StringBuilder()).append("").append(mPageSize).toString());
        requestparams.put("device", "android");
        f.a().a(s, requestparams, null, new _cls5(), true);
        return;
_L2:
        if (req instanceof CLGetSongListReq)
        {
            CLGetSongListReq clgetsonglistreq = (CLGetSongListReq)req;
            if (!TextUtils.isEmpty(clgetsonglistreq.songListId))
            {
                mPageId = (clgetsonglistreq.pn + 1) / mPageSize + 1;
            }
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    public static CarlifeManager getInstance(Context context)
    {
        if (sInstance == null)
        {
            synchronized (sLock)
            {
                if (sInstance == null)
                {
                    sInstance = new CarlifeManager(context);
                }
            }
        }
        return sInstance;
        context;
        abyte0;
        JVM INSTR monitorexit ;
        throw context;
    }

    private RankTrackModel getTrackById(List list, long l)
    {
        for (list = list.iterator(); list.hasNext();)
        {
            RankTrackModel ranktrackmodel = (RankTrackModel)list.next();
            if (ranktrackmodel.getTrackId() == l)
            {
                return ranktrackmodel;
            }
        }

        return null;
    }

    private String getTrackUrl(RankTrackModel ranktrackmodel)
    {
        if (isUseHighBitrate)
        {
            return ranktrackmodel.getPlayPath64();
        } else
        {
            return ranktrackmodel.getPlayPath32();
        }
    }

    private CLPlatformCallback initCallback()
    {
        if (mCarLifeCallback == null)
        {
            mCarLifeCallback = new _cls1();
        }
        return mCarLifeCallback;
    }

    private void onGetAlbumList(final CLGetAlbumListReq req)
    {
        if (!checkCarlifeStatus())
        {
            return;
        } else
        {
            Logger.e("CarLifeManager", (new StringBuilder()).append("onGetAlbumList reqId:").append(req.requestId).toString());
            mHandler.post(new _cls2());
            return;
        }
    }

    private void onGetSoundData(CLGetSongDataReq clgetsongdatareq)
    {
        if (!checkCarlifeStatus())
        {
            return;
        } else
        {
            Logger.e("CarLifeManager", (new StringBuilder()).append("onGetSoundData ").append(clgetsongdatareq.requestId).toString());
            mWorkerHandler.post(new LoadSongDataTask(clgetsongdatareq));
            return;
        }
    }

    private void onGetSoundList(final CLGetSongListReq req)
    {
        if (!checkCarlifeStatus())
        {
            return;
        } else
        {
            Logger.e("CarLifeManager", (new StringBuilder()).append("onGetSoundList reqId:").append(req.requestId).toString());
            mHandler.post(new _cls3());
            return;
        }
    }

    private boolean send(long l, String s, byte abyte0[], int i, int j, int k, 
            int i1)
    {
        if (checkCarlifeStatus())
        {
            CLGetSongDataResp clgetsongdataresp = new CLGetSongDataResp();
            clgetsongdataresp.requestId = l;
            clgetsongdataresp.songData = new CLSongData();
            clgetsongdataresp.songData.songId = s;
            clgetsongdataresp.songData.data = abyte0;
            clgetsongdataresp.songData.len = i;
            clgetsongdataresp.songData.totalSize = k;
            clgetsongdataresp.songData.offset = j;
            clgetsongdataresp.songData.tag = i1;
            if (mCarlifeManager.sendResp(clgetsongdataresp) == 0)
            {
                return true;
            }
        }
        return false;
    }

    private void sendSoundList(CLRequest clrequest, RankTrackListModel ranktracklistmodel)
    {
        if (!checkCarlifeStatus())
        {
            return;
        }
        StringBuilder stringbuilder = (new StringBuilder()).append("sendSoundList ");
        Object obj;
        if (ranktracklistmodel != null)
        {
            obj = Integer.valueOf(ranktracklistmodel.getList().size());
        } else
        {
            obj = "";
        }
        Logger.e("CarLifeManager", stringbuilder.append(obj).toString());
        if (clrequest instanceof CLGetSongListReq)
        {
            CLGetSongListReq clgetsonglistreq = (CLGetSongListReq)clrequest;
            obj = new CLGetSongListResp();
            if (ranktracklistmodel == null || ranktracklistmodel.getList() == null || ranktracklistmodel.getList().size() == 0)
            {
                obj.errorNo = 1;
            } else
            {
                obj.songList = getCLSongList(ranktracklistmodel);
                if (!TextUtils.isEmpty(clgetsonglistreq.songListId))
                {
                    obj.pn = clgetsonglistreq.pn;
                    obj.rn = mPageSize;
                    obj.total = ranktracklistmodel.getTotalCount();
                } else
                {
                    obj.pn = 0;
                    obj.rn = 0;
                    obj.total = 0;
                }
            }
            obj.songListId = clgetsonglistreq.songListId;
            obj.version = clgetsonglistreq.version;
            obj.requestId = clrequest.requestId;
            Logger.e("CarLifeManager", (new StringBuilder()).append("send CLGetSongListResp ").append(((CLResponse) (obj)).requestId).toString());
            clrequest = ((CLRequest) (obj));
        } else
        {
            CLGetAlbumListResp clgetalbumlistresp = new CLGetAlbumListResp();
            CLAlbum clalbum = new CLAlbum();
            clalbum.albumId = "album1";
            clalbum.albumName = "\u6700\u706B\u8282\u76EE\u98D9\u5347\u699C";
            clalbum.artistId = "artistId";
            clalbum.artistName = "\u559C\u9A6C\u62C9\u96C5";
            if (ranktracklistmodel == null || ranktracklistmodel.getList() == null || ranktracklistmodel.getList().size() == 0)
            {
                clgetalbumlistresp.errorNo = 0;
                clalbum.songCount = 0;
            } else
            {
                clalbum.songCount = ranktracklistmodel.getTotalCount();
                clalbum.coverUrl = "http://fdfs.xmcdn.com/group16/M04/23/1C/wKgDbFV-qWnymdMFAABWkqxY2as195.png";
            }
            clgetalbumlistresp.albumList = new ArrayList();
            clgetalbumlistresp.albumList.add(clalbum);
            clgetalbumlistresp.requestId = clrequest.requestId;
            Logger.e("CarLifeManager", (new StringBuilder()).append("send CLGetAlbumListResp ").append(((CLResponse) (clgetalbumlistresp)).requestId).append(", count:").append(clalbum.songCount).toString());
            clrequest = clgetalbumlistresp;
        }
        mCarlifeManager.sendResp(clrequest);
    }

    private void showNetErrMessage()
    {
        _cls4 _lcls4 = new _cls4();
        mHandler.post(_lcls4);
    }

    public boolean connect()
    {
        if (!CLPlatformManager.isCarlifeInstalled(mContext))
        {
            return false;
        } else
        {
            mCarlifeManager = CLPlatformManager.getInstance();
            return mCarlifeManager.init(mContext, "cl84b81a3b93", initCallback()) & CLPlatformManager.jumpToCarlife(mContext);
        }
    }

    public void disConnect()
    {
        if (isConnected && mCarlifeManager != null)
        {
            mCarlifeManager.destroy();
        }
        mCarlifeManager = null;
        mCarLifeCallback = null;
        isConnected = false;
    }

    public boolean isCarlifeInstalled()
    {
        return CLPlatformManager.isCarlifeInstalled(mContext);
    }

    public boolean isConnected()
    {
        return isConnected;
    }

    public void release()
    {
        synchronized (sLock)
        {
            disConnect();
            mWorkerHandler.removeCallbacksAndMessages(null);
            mWorker.quit();
            sInstance = null;
        }
        return;
        exception;
        abyte0;
        JVM INSTR monitorexit ;
        throw exception;
    }



/*
    static boolean access$002(CarlifeManager carlifemanager, boolean flag)
    {
        carlifemanager.isConnected = flag;
        return flag;
    }

*/



/*
    static boolean access$1002(CarlifeManager carlifemanager, boolean flag)
    {
        carlifemanager.isLoading = flag;
        return flag;
    }

*/












/*
    static RankTrackListModel access$802(CarlifeManager carlifemanager, RankTrackListModel ranktracklistmodel)
    {
        carlifemanager.mRankTrackListModel = ranktracklistmodel;
        return ranktracklistmodel;
    }

*/


/*
    static int access$908(CarlifeManager carlifemanager)
    {
        int i = carlifemanager.mPageId;
        carlifemanager.mPageId = i + 1;
        return i;
    }

*/

    private class _cls5 extends a
    {

        final CarlifeManager this$0;
        final CLRequest val$req;

        public void onBindXDCS(Header aheader[])
        {
        }

        public void onFinish()
        {
            isLoading = false;
        }

        public void onNetError(int i, String s)
        {
            Logger.e("CarLifeManager", (new StringBuilder()).append("onNetError code:").append(i).append(", ").append(s).toString());
            showNetErrMessage();
            sendSoundList(req, null);
        }

        public void onSuccess(String s)
        {
            if (TextUtils.isEmpty(s))
            {
                showNetErrMessage();
                sendSoundList(req, null);
                return;
            }
            try
            {
                s = (RankTrackListModel)JSON.parseObject(s, com/ximalaya/ting/android/model/finding2/rank/RankTrackListModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                s.printStackTrace();
                showNetErrMessage();
                sendSoundList(req, null);
                return;
            }
            if (s == null)
            {
                break MISSING_BLOCK_LABEL_67;
            }
            if (((RankTrackListModel) (s)).ret == 0 && s.getList() != null && s.getList().size() != 0)
            {
                break MISSING_BLOCK_LABEL_112;
            }
            showNetErrMessage();
            sendSoundList(req, null);
            return;
            mRankTrackListModel = s;
            int i = ((com.ximalaya.ting.android.util) (this)).;
            sendSoundList(req, s);
            return;
        }

        _cls5()
        {
            this$0 = CarlifeManager.this;
            req = clrequest;
            super();
        }
    }


    private class _cls1
        implements CLPlatformCallback
    {

        final CarlifeManager this$0;

        public void onCarlifeError(int i, String s)
        {
            Logger.e("CarLifeManager", (new StringBuilder()).append("onCarlifeError ").append(i).append(",").append(s).toString());
            switch (i)
            {
            case 2: // '\002'
            case 3: // '\003'
            case 4: // '\004'
            case 5: // '\005'
            case 6: // '\006'
            case 8: // '\b'
            default:
                return;

            case 7: // '\007'
                isConnected = false;
                break;
            }
        }

        public void onCarlifeRequest(CLRequest clrequest)
        {
            if (clrequest == null)
            {
                Logger.e("CarLifeManager", "onCarlifeRequest get null request!!!");
            } else
            {
                if (clrequest instanceof CLGetSongDataReq)
                {
                    Logger.e("CarLifeManager", (new StringBuilder()).append("onCarlifeRequest get CLGetSongDataReq reqId:").append(clrequest.requestId).toString());
                    onGetSoundData((CLGetSongDataReq)clrequest);
                    return;
                }
                if (clrequest instanceof CLGetSongListReq)
                {
                    Logger.e("CarLifeManager", (new StringBuilder()).append("onCarlifeRequest get CLGetSongListReq reqId:").append(clrequest.requestId).toString());
                    onGetSoundList((CLGetSongListReq)clrequest);
                    return;
                }
                if (clrequest instanceof CLGetAlbumListReq)
                {
                    Logger.e("CarLifeManager", (new StringBuilder()).append("onCarlifeRequest get CLGetAlbumListReq reqId:").append(clrequest.requestId).toString());
                    onGetAlbumList((CLGetAlbumListReq)clrequest);
                    return;
                }
            }
        }

        public void onCarlifeResponse(CLResponse clresponse)
        {
            Logger.e("CarLifeManager", "onCarlifeResponse");
        }

        public void onConnected()
        {
            isConnected = true;
        }

        _cls1()
        {
            this$0 = CarlifeManager.this;
            super();
        }
    }


    private class _cls2
        implements Runnable
    {

        final CarlifeManager this$0;
        final CLGetAlbumListReq val$req;

        public void run()
        {
            getHotSounds(req);
        }

        _cls2()
        {
            this$0 = CarlifeManager.this;
            req = clgetalbumlistreq;
            super();
        }
    }


    private class _cls3
        implements Runnable
    {

        final CarlifeManager this$0;
        final CLGetSongListReq val$req;

        public void run()
        {
            getHotSounds(req);
        }

        _cls3()
        {
            this$0 = CarlifeManager.this;
            req = clgetsonglistreq;
            super();
        }
    }


    private class _cls4
        implements Runnable
    {

        final CarlifeManager this$0;

        public void run()
        {
            Toast.makeText(mContext, "\u7F51\u7EDC\u4E0D\u7ED9\u529B\uFF0C\u8BF7\u7A0D\u540E\u518D\u8BD5", 1).show();
        }

        _cls4()
        {
            this$0 = CarlifeManager.this;
            super();
        }
    }

}
