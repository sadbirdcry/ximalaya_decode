// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.carlink.carlife;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.carlink.carlife:
//            CarlifeManager

public class CarlifeService extends Service
{

    private static final int STOP_DELAY = 60000;
    private static final String TAG = "CarlifeService";
    private Handler mHandler;
    private Runnable mStopSelf;

    public CarlifeService()
    {
        mStopSelf = new _cls1();
    }

    public IBinder onBind(Intent intent)
    {
        return null;
    }

    public void onCreate()
    {
        super.onCreate();
        mHandler = new Handler();
        Logger.e("CarlifeService", "---onCreate");
    }

    public void onDestroy()
    {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
        Logger.e("CarlifeService", "---onDestroy");
    }

    public int onStartCommand(Intent intent, int i, int j)
    {
        mHandler.removeCallbacks(mStopSelf);
        CarlifeManager.getInstance(getApplicationContext()).connect();
        mHandler.postDelayed(mStopSelf, 60000L);
        return super.onStartCommand(intent, i, j);
    }

    private class _cls1
        implements Runnable
    {

        final CarlifeService this$0;

        public void run()
        {
            stopSelf();
        }

        _cls1()
        {
            this$0 = CarlifeService.this;
            super();
        }
    }

}
