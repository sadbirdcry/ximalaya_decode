// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.carlink.carlife;

import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.ximalaya.ting.android.activity.BaseActivity;
import com.ximalaya.ting.android.dl.PluginConstants;
import com.ximalaya.ting.android.dl.PluginManager;
import com.ximalaya.ting.android.util.Logger;

// Referenced classes of package com.ximalaya.ting.android.carlink.carlife:
//            CarlifeManager

public class CarlifeSettingAct extends BaseActivity
{

    private static final String CARLIFE_URL = "http://carlife.baidu.com/carlife/download";
    private static final String TAG = "CarlifeSettingAct";
    private ImageView mBack;
    private TextView mBtnSync;
    private CarlifeManager mManager;
    private TextView mMsgLabel;

    public CarlifeSettingAct()
    {
    }

    private void initBusinessLogic()
    {
        mManager = CarlifeManager.getInstance(mAppContext);
        mBtnSync.setOnClickListener(new _cls2());
    }

    private void initUI()
    {
        setTitleText("\u767E\u5EA6CarLife");
        mBtnSync = (TextView)findViewById(0x7f0a006c);
        mMsgLabel = (TextView)findViewById(0x7f0a006d);
        mBack = (ImageView)findViewById(0x7f0a006a);
        mBack.setOnClickListener(new _cls3());
    }

    private void updateCarlifeStatus()
    {
        if (mManager != null && mManager.isCarlifeInstalled())
        {
            mMsgLabel.setText("\u70B9\u51FB\u540C\u6B65\uFF0C\u5373\u53EF\u5728\u767E\u5EA6CarLife\u4E2D\u64AD\u653E\u559C\u9A6C\u62C9\u96C5\u7684\u6D77\u91CF\u7CBE\u5F69\u8282\u76EE\u3002\n\n\u60A8\u53EA\u9700\u5728\u8F66\u5185\u901A\u8FC7USB\u6216WiFi\u8FDE\u63A5\u624B\u673A\uFF0CCarLife\u6C47\u96C6\u7684\u8F66\u751F\u6D3B\u5A31\u4E50\u670D\u52A1\u5373\u53EF\u540C\u6B65\u5230\u8F66\u8F7D\u5C4F\u5E55\uFF0C\u4EAB\u53D7\u9A7E\u9A76\u4E2D\u5B89\u5168\u4FBF\u6377\u7684\u8F66\u8F7D\u4F53\u9A8C\u3002");
            mBtnSync.setText("\u7ACB\u5373\u540C\u6B65");
            return;
        } else
        {
            mMsgLabel.setText("\u68C0\u6D4B\u5230\u60A8\u7684\u624B\u673A\u5C1A\u672A\u5B89\u88C5\u767E\u5EA6CarLife\uFF0C\u8BF7\u70B9\u51FB\u4E0B\u8F7D\u3002\n\n\u60A8\u53EA\u9700\u5728\u8F66\u5185\u901A\u8FC7USB\u6216WiFi\u8FDE\u63A5\u624B\u673A\uFF0CCarLife\u6C47\u96C6\u7684\u8F66\u751F\u6D3B\u5A31\u4E50\u670D\u52A1\u5373\u53EF\u540C\u6B65\u5230\u8F66\u8F7D\u5C4F\u5E55\uFF0C\u4EAB\u53D7\u9A7E\u9A76\u4E2D\u5B89\u5168\u4FBF\u6377\u7684\u8F66\u8F7D\u4F53\u9A8C\u3002");
            mBtnSync.setText("\u4E0B\u8F7D\u767E\u5EA6CarLife");
            return;
        }
    }

    public void onBackPressed()
    {
        super.onBackPressed();
    }

    public void onConfigurationChanged(Configuration configuration)
    {
        super.onConfigurationChanged(configuration);
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(0x7f030008);
        StringBuilder stringbuilder = (new StringBuilder()).append("onCreate savedInstanceState ");
        boolean flag;
        if (bundle != null)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        Logger.e("CarlifeSettingAct", stringbuilder.append(flag).toString());
        initCommon();
        initUI();
        PluginManager.getInstance(mActivity).loadPluginAsync(PluginConstants.PLUGIN_CARLIFE, new _cls1());
    }

    protected void onDestroy()
    {
        super.onDestroy();
    }

    protected void onResume()
    {
        super.onResume();
        updateCarlifeStatus();
    }



    private class _cls2
        implements android.view.View.OnClickListener
    {

        final CarlifeSettingAct this$0;

        public void onClick(View view)
        {
            if (mManager.isCarlifeInstalled())
            {
                if (!mManager.connect())
                {
                    Toast.makeText(mActivity, "\u540C\u6B65CarLife\u4E0D\u6210\u529F", 1).show();
                    finish();
                }
                return;
            } else
            {
                view = new Intent("android.intent.action.VIEW");
                view.setData(Uri.parse("http://carlife.baidu.com/carlife/download"));
                mActivity.startActivity(view);
                return;
            }
        }

        _cls2()
        {
            this$0 = CarlifeSettingAct.this;
            super();
        }
    }


    private class _cls3
        implements android.view.View.OnClickListener
    {

        final CarlifeSettingAct this$0;

        public void onClick(View view)
        {
            finish();
        }

        _cls3()
        {
            this$0 = CarlifeSettingAct.this;
            super();
        }
    }


    private class _cls1
        implements Runnable
    {

        final CarlifeSettingAct this$0;

        public void run()
        {
            initBusinessLogic();
        }

        _cls1()
        {
            this$0 = CarlifeSettingAct.this;
            super();
        }
    }

}
