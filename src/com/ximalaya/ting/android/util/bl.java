// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

// Referenced classes of package com.ximalaya.ting.android.util:
//            MyCallback

final class bl extends Thread
{

    final String a;
    final int b;
    final MyCallback c;

    bl(String s, int i, MyCallback mycallback)
    {
        a = s;
        b = i;
        c = mycallback;
        super();
    }

    public void run()
    {
        Object obj;
        Object obj1;
        boolean flag;
        obj = new android.graphics.BitmapFactory.Options();
        obj.inJustDecodeBounds = true;
        obj1 = new File(a);
        flag = ((File) (obj1)).exists();
        if (flag) goto _L2; else goto _L1
_L1:
        obj = c;
_L3:
        ((MyCallback) (obj)).execute();
        return;
_L2:
        Bitmap bitmap = BitmapFactory.decodeFile(((File) (obj1)).getAbsolutePath(), ((android.graphics.BitmapFactory.Options) (obj)));
label0:
        {
            if (bitmap != null)
            {
                break label0;
            }
            obj = c;
        }
          goto _L3
        obj = new Matrix();
        ((Matrix) (obj)).postRotate(b);
        try
        {
            obj = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), ((Matrix) (obj)), true);
        }
        // Misplaced declaration of an exception variable
        catch (Object obj)
        {
            obj = null;
        }
        if (obj != null) goto _L5; else goto _L4
_L4:
        obj = bitmap;
_L6:
        if (bitmap == obj)
        {
            break MISSING_BLOCK_LABEL_119;
        }
        bitmap.recycle();
        ((Bitmap) (obj)).recycle();
        obj = c;
          goto _L3
_L5:
        obj1 = new FileOutputStream(((File) (obj1)));
_L7:
        ((Bitmap) (obj)).compress(android.graphics.Bitmap.CompressFormat.PNG, 100, ((java.io.OutputStream) (obj1)));
        ((FileOutputStream) (obj1)).flush();
_L8:
        ((FileOutputStream) (obj1)).close();
          goto _L6
        Object obj2;
        obj2;
        ((FileNotFoundException) (obj2)).printStackTrace();
        obj2 = null;
          goto _L7
        IOException ioexception;
        ioexception;
        ioexception.printStackTrace();
          goto _L8
        Exception exception;
        exception;
        c.execute();
        throw exception;
        obj2;
        ((IOException) (obj2)).printStackTrace();
          goto _L6
    }
}
