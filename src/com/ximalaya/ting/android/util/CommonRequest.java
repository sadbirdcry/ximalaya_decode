// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.view.View;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loopj.android.http.RequestParams;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.a;
import com.ximalaya.ting.android.a.d;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.BaseModel;
import com.ximalaya.ting.android.model.auth.AuthInfo;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoList;
import com.ximalaya.ting.android.model.verification_code.VerificationCodeModel;
import com.ximalaya.ting.android.modelmanage.CacheManager;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;
import com.ximalaya.ting.android.service.play.PlayListControl;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.util:
//            e, MD5, Logger, d, 
//            ApiUtil, DataCollectUtil, c, ToolUtil, 
//            f

public class CommonRequest
{
    public static interface Callback
    {

        public abstract void onFailure(int i, String s);

        public abstract void onSuccess(Object obj);

        public abstract void parseJsonError();
    }


    public CommonRequest()
    {
    }

    public static void DoFollowedAutor(Context context, String s)
    {
        context = new RequestParams();
        context.put("access_token", s);
        context.put("uid", "2608693591");
        f.a().a("https://api.weibo.com/2/friendships/create.json", context, null);
    }

    public static void asyncGetPagedAlbumList(long l, long l1, int i, int j, Callback callback)
    {
        callback = new com.ximalaya.ting.android.util.e(callback);
        RequestParams requestparams = new RequestParams();
        requestparams.put("albumUid", l);
        requestparams.put("albumId", l1);
        requestparams.put("pageId", i);
        requestparams.put("pageSize", j);
        f.a().a(e.ag, requestparams, null, callback);
    }

    public static BaseModel bindApp(Context context, String s, String s1, boolean flag)
    {
        Object obj = null;
        RequestParams requestparams = new RequestParams();
        Object obj1 = new StringBuffer();
        ((StringBuffer) (obj1)).append("android");
        ((StringBuffer) (obj1)).append(((MyApplication)(MyApplication)context.getApplicationContext()).m());
        ((StringBuffer) (obj1)).append(((MyApplication)context.getApplicationContext()).i());
        if (flag)
        {
            ((StringBuffer) (obj1)).append(s);
        }
        ((StringBuffer) (obj1)).append(s1);
        if (UserInfoMannage.hasLogined())
        {
            ((StringBuffer) (obj1)).append(UserInfoMannage.getInstance().getUser().uid);
            ((StringBuffer) (obj1)).append(UserInfoMannage.getInstance().getUser().token);
        }
        ((StringBuffer) (obj1)).append(d.b);
        obj1 = MD5.md5(((StringBuffer) (obj1)).toString());
        requestparams.put("deviceToken", ((MyApplication)context.getApplicationContext()).i());
        if (flag)
        {
            requestparams.put("baiduChannelId", s);
        }
        if (flag)
        {
            requestparams.put("baiduUserId", s1);
        } else
        {
            requestparams.put("getuicid", s1);
        }
        requestparams.put("signature", ((String) (obj1)));
        if (flag)
        {
            context = e.Z;
        } else
        {
            context = e.aa;
        }
        s = f.a().a(context, requestparams, null);
        context = obj;
        if (((com.ximalaya.ting.android.b.n.a) (s)).b == 1)
        {
            context = ((com.ximalaya.ting.android.b.n.a) (s)).a;
        }
        s = (BaseModel)JSONObject.parseObject(context, com/ximalaya/ting/android/model/BaseModel);
        if (((BaseModel) (s)).ret != 0)
        {
            break MISSING_BLOCK_LABEL_266;
        }
        com.ximalaya.ting.android.util.Logger.d("PushMessageReceiver", "\u7ED1\u5B9A\u6210\u529F ");
        return s;
        try
        {
            com.ximalaya.ting.android.util.Logger.d("PushMessageReceiver", (new StringBuilder()).append("\u7ED1\u5B9A\u5931\u8D25\uFF1A").append(context).toString());
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            com.ximalaya.ting.android.util.Logger.d("PushMessageReceiver", (new StringBuilder()).append("\u7ED1\u5B9A\u5931\u8D25\uFF1A").append(context).toString());
            context = new BaseModel();
            context.ret = -1;
            context.msg = "\u89E3\u6790json\u5F02\u5E38";
            return context;
        }
        return s;
    }

    public static void bingAppLogout(Context context)
    {
        f.a().b(e.ac, null, null, new com.ximalaya.ting.android.util.d());
    }

    public static String doActionFollowOrCancel(Context context, long l, String s, long l1, boolean flag)
    {
        context = null;
        s = new RequestParams();
        s.put("toUid", (new StringBuilder()).append(l1).append("").toString());
        s.put("isFollow", String.valueOf(flag));
        s = f.a().a(e.G, s, null);
        if (((com.ximalaya.ting.android.b.n.a) (s)).b == 1)
        {
            context = ((com.ximalaya.ting.android.b.n.a) (s)).a;
        }
        return context;
    }

    public static String doGetFollowers(Context context, long l, String s, long l1, int i, int j, 
            View view, View view1)
    {
        s = new RequestParams();
        s.put("pageId", (new StringBuilder()).append(i).append("").toString());
        s.put("pageSize", (new StringBuilder()).append(j).append("").toString());
        if (0L != l1)
        {
            s.put("toUid", (new StringBuilder()).append(l1).append("").toString());
            context = e.N;
        } else
        {
            context = e.L;
        }
        return f.a().a(context, s, view, view1, false).a;
    }

    public static String doGetFollowings(Context context, long l, String s, long l1, int i, int j, 
            View view, View view1)
    {
        s = new RequestParams();
        s.put("pageId", (new StringBuilder()).append(i).append("").toString());
        s.put("pageSize", (new StringBuilder()).append(j).append("").toString());
        if (l1 != 0L)
        {
            s.put("toUid", (new StringBuilder()).append(l1).append("").toString());
            context = e.M;
        } else
        {
            context = e.K;
        }
        return f.a().a(context, s, view, view1, false).a;
    }

    public static String doGetLovers(Context context, long l, int i, int j, View view, View view1)
    {
        context = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/track/lovers").toString();
        RequestParams requestparams = new RequestParams();
        requestparams.put("trackId", (new StringBuilder()).append(l).append("").toString());
        requestparams.put("pageId", (new StringBuilder()).append(i).append("").toString());
        requestparams.put("pageSize", (new StringBuilder()).append(j).append("").toString());
        return f.a().a(context, requestparams, view, view1, false).a;
    }

    public static String doGetMyOrOtherAlbums(Context context, long l, String s, long l1, int i, int j, 
            View view, View view1)
    {
        if (0L != l1)
        {
            context = (new StringBuilder()).append(e.R).append("/").append(l1).append("/").append(i).append("/").append(j).toString();
            context = f.a().a(context, null, view, view1, false);
            if (((com.ximalaya.ting.android.b.n.a) (context)).b == 1)
            {
                return ((com.ximalaya.ting.android.b.n.a) (context)).a;
            }
        } else
        {
            context = new RequestParams();
            context.put("pageId", (new StringBuilder()).append(i).append("").toString());
            context.put("pageSize", (new StringBuilder()).append(j).append("").toString());
            context.put("isAsc", "false");
            s = e.S;
            context = f.a().a(s, context, view, view1, false);
            if (((com.ximalaya.ting.android.b.n.a) (context)).b == 1)
            {
                return ((com.ximalaya.ting.android.b.n.a) (context)).a;
            }
        }
        return null;
    }

    public static void doGetUserRelationship(Context context, String s, com.ximalaya.ting.android.b.a a1, View view)
    {
        context = new RequestParams();
        context.put("uids", s);
        f.a().a((new StringBuilder()).append(a.u).append("mobile/user/relation").toString(), context, DataCollectUtil.getDataFromView(view), new c(a1));
    }

    public static LoginInfoModel doLogin(Context context, int i, String s, String s1, AuthInfo authinfo)
    {
        return doLogin(context, i, s, s1, authinfo, true);
    }

    public static LoginInfoModel doLogin(Context context, int i, String s, String s1, AuthInfo authinfo, boolean flag)
    {
        Object obj1 = new RequestParams();
        i;
        JVM INSTR tableswitch 0 5: default 48
    //                   0 50
    //                   1 259
    //                   2 339
    //                   3 419
    //                   4 499
    //                   5 579;
           goto _L1 _L2 _L3 _L4 _L5 _L6 _L7
_L1:
        return null;
_L2:
        authinfo = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/login").toString();
        ((RequestParams) (obj1)).put("account", s);
        ((RequestParams) (obj1)).put("password", s1);
        ((RequestParams) (obj1)).put("rememberMe", "true");
_L10:
        ((RequestParams) (obj1)).put("xum", ToolUtil.getLocalMacAddress(context));
        ((RequestParams) (obj1)).put("deviceToken", ToolUtil.getDeviceToken(context));
        authinfo = f.a().a(authinfo, ((RequestParams) (obj1)), null);
        Object obj;
        if (((com.ximalaya.ting.android.b.n.a) (authinfo)).b == 1)
        {
            authinfo = ((com.ximalaya.ting.android.b.n.a) (authinfo)).a;
        } else
        {
            authinfo = null;
        }
        if (authinfo == null)
        {
            break MISSING_BLOCK_LABEL_757;
        }
        obj1 = new LoginInfoModel();
        obj = JSON.parseObject(authinfo);
        if (!"0".equals(((JSONObject) (obj)).get("ret").toString())) goto _L9; else goto _L8
_L8:
        obj = (LoginInfoModel)JSON.parseObject(authinfo, com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
        obj1 = SharedPreferencesUtil.getInstance(context);
        if (i != 0)
        {
            break MISSING_BLOCK_LABEL_236;
        }
        ((SharedPreferencesUtil) (obj1)).saveString("account", s);
        ((SharedPreferencesUtil) (obj1)).saveString("password", s1);
        context = ((Context) (obj));
        if (!flag)
        {
            break MISSING_BLOCK_LABEL_257;
        }
        ((SharedPreferencesUtil) (obj1)).saveString("loginforesult", authinfo);
        context = ((Context) (obj));
_L11:
        return context;
_L3:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/").append(1).append("/access").toString();
        ((RequestParams) (obj1)).put("accessToken", authinfo.access_token);
        ((RequestParams) (obj1)).put("expiresIn", authinfo.expires_in);
        ((RequestParams) (obj1)).put("openid", authinfo.openid);
        authinfo = ((AuthInfo) (obj));
          goto _L10
_L4:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/").append(2).append("/access").toString();
        ((RequestParams) (obj1)).put("accessToken", authinfo.access_token);
        ((RequestParams) (obj1)).put("expiresIn", authinfo.expires_in);
        ((RequestParams) (obj1)).put("openid", authinfo.openid);
        authinfo = ((AuthInfo) (obj));
          goto _L10
_L5:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/").append(3).append("/access").toString();
        ((RequestParams) (obj1)).put("accessToken", authinfo.access_token);
        ((RequestParams) (obj1)).put("expiresIn", authinfo.expires_in);
        ((RequestParams) (obj1)).put("macKey", authinfo.macKey);
        authinfo = ((AuthInfo) (obj));
          goto _L10
_L6:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/").append(4).append("/access").toString();
        ((RequestParams) (obj1)).put("accessToken", authinfo.access_token);
        ((RequestParams) (obj1)).put("expiresIn", authinfo.expires_in);
        ((RequestParams) (obj1)).put("openid", authinfo.openid);
        authinfo = ((AuthInfo) (obj));
          goto _L10
_L7:
        obj = (new StringBuilder()).append(ApiUtil.getApiHost()).append("mobile/").append(11).append("/access").toString();
        ((RequestParams) (obj1)).put("accessToken", authinfo.access_token);
        ((RequestParams) (obj1)).put("expiresIn", authinfo.expires_in);
        ((RequestParams) (obj1)).put("openid", authinfo.openid);
        ((RequestParams) (obj1)).put("macKey", authinfo.macKey);
        authinfo = ((AuthInfo) (obj));
          goto _L10
_L9:
        obj1.ret = -1;
        obj1.msg = ((JSONObject) (obj)).getString("msg");
        context = ((Context) (obj1));
          goto _L11
        s;
        context = ((Context) (obj1));
_L12:
        com.ximalaya.ting.android.util.Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(s.getMessage()).append(Logger.getLineInfo()).toString());
        context.ret = -1;
        context.msg = "\u89E3\u6790json\u5F02\u5E38";
          goto _L11
        s;
        context = ((Context) (obj));
          goto _L12
        context = null;
          goto _L11
    }

    public static String doSetGroup(Context context, String s, boolean flag, View view, View view1)
    {
        context = e.Q;
        RequestParams requestparams = new RequestParams();
        requestparams.put("toUid", (new StringBuilder()).append(s).append("").toString());
        s = new StringBuilder();
        if (!flag)
        {
            flag = true;
        } else
        {
            flag = false;
        }
        requestparams.put("isFollow", s.append(flag).append("").toString());
        context = f.a().b(context, requestparams, view, view1);
        Logger.log((new StringBuilder()).append("result:").append(context).toString());
        context = JSON.parseObject(context);
        if ("0".equals(context.getString("ret")))
        {
            return null;
        }
        try
        {
            context = context.getString("msg");
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            com.ximalaya.ting.android.util.Logger.e("\u89E3\u6790json\u5F02\u5E38", (new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(context.getMessage()).append(Logger.getLineInfo()).toString());
            return null;
        }
        return context;
    }

    public static SoundInfoList getAlbumList(Context context, long l, long l1, long l2, String s, 
            String s1, String s2, String s3, View view, View view1)
    {
        com/ximalaya/ting/android/util/CommonRequest;
        JVM INSTR monitorenter ;
        SoundInfoList soundinfolist = CacheManager.getAlbumPlayList(context, l1);
        Object obj;
        if (soundinfolist == null)
        {
            break MISSING_BLOCK_LABEL_38;
        }
        obj = soundinfolist;
        if (soundinfolist.data.contains(PlayListControl.getPlayListManager().getCurSound()))
        {
            break MISSING_BLOCK_LABEL_258;
        }
        obj = new RequestParams();
        ((RequestParams) (obj)).put("albumId", (new StringBuilder()).append(l1).append("").toString());
        ((RequestParams) (obj)).put("trackId", (new StringBuilder()).append(l).append("").toString());
        view = f.a().a(e.af, ((RequestParams) (obj)), view, view1);
        view = (SoundInfoList)JSONObject.parseObject(view, com/ximalaya/ting/android/model/sound/SoundInfoList);
        obj = view;
        if (view == null)
        {
            break MISSING_BLOCK_LABEL_258;
        }
        obj = view;
        if (((SoundInfoList) (view)).ret != 0)
        {
            break MISSING_BLOCK_LABEL_258;
        }
        obj = view;
        if (((SoundInfoList) (view)).data == null)
        {
            break MISSING_BLOCK_LABEL_258;
        }
        int i = 0;
_L2:
        if (i >= ((SoundInfoList) (view)).data.size())
        {
            break; /* Loop/switch isn't completed */
        }
        view1 = (SoundInfo)((SoundInfoList) (view)).data.get(i);
        view1.albumId = l1;
        view1.albumCoverPath = s1;
        view1.albumName = s;
        view1.uid = l2;
        view1.recSrc = s2;
        view1.recTrack = s3;
        i++;
        if (true) goto _L2; else goto _L1
_L1:
        CacheManager.saveAlbumPlayList(context, view);
        obj = view;
_L4:
        com/ximalaya/ting/android/util/CommonRequest;
        JVM INSTR monitorexit ;
        return ((SoundInfoList) (obj));
        context;
        obj = new SoundInfoList();
        obj.msg = "\u89E3\u6790\u5F02\u5E38";
        if (true) goto _L4; else goto _L3
_L3:
        context;
        throw context;
    }

    public static VerificationCodeModel getCheckCode(Context context, String s, long l, String s1, View view)
    {
        com/ximalaya/ting/android/util/CommonRequest;
        JVM INSTR monitorenter ;
        s1 = new RequestParams();
        if (!s.equals("from_register")) goto _L2; else goto _L1
_L1:
        context = e.ad;
_L3:
        context = f.a().a(context, s1, view, view);
        context = (VerificationCodeModel)JSONObject.parseObject(context, com/ximalaya/ting/android/model/verification_code/VerificationCodeModel);
_L4:
        com/ximalaya/ting/android/util/CommonRequest;
        JVM INSTR monitorexit ;
        return context;
_L2:
        if (!s.equals("from_sound"))
        {
            break MISSING_BLOCK_LABEL_89;
        }
        context = e.ae;
          goto _L3
        context;
        context.printStackTrace();
        context = null;
          goto _L4
        context;
        throw context;
        context = null;
          goto _L3
    }

    public static void getFeedSortedPlaylist(long l, long l1, int i, int j, boolean flag, Callback callback)
    {
        callback = new com.ximalaya.ting.android.util.f(callback);
        RequestParams requestparams = new RequestParams();
        requestparams.put("albumUid", l);
        requestparams.put("albumId", l1);
        requestparams.put("pageId", i);
        requestparams.put("pageSize", j);
        if (flag)
        {
            i = 1;
        } else
        {
            i = 0;
        }
        requestparams.put("isAsc", i);
        f.a().a(e.ah, requestparams, null, callback);
    }
}
