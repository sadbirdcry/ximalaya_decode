// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.view.View;

public abstract class MyAsyncTaskLoader extends AsyncTaskLoader
{

    public View fromBindView;
    public View toBindView;

    public MyAsyncTaskLoader(Context context)
    {
        super(context);
    }

    public void clearViews()
    {
        fromBindView = null;
        toBindView = null;
    }

    public void setXDCSBindView(View view, View view1)
    {
        fromBindView = view;
        toBindView = view1;
    }
}
