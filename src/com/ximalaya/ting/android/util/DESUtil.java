// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

public class DESUtil
{

    public DESUtil()
    {
    }

    private static byte[] convertHexString(String s)
    {
        byte abyte0[] = new byte[s.length() / 2];
        for (int i = 0; i < abyte0.length; i++)
        {
            abyte0[i] = (byte)Integer.parseInt(s.substring(i * 2, i * 2 + 2), 16);
        }

        return abyte0;
    }

    public static String decrypt(String s, String s1)
        throws Exception
    {
        s = convertHexString(s);
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        DESKeySpec deskeyspec = new DESKeySpec(s1.getBytes("UTF-8"));
        cipher.init(2, SecretKeyFactory.getInstance("DES").generateSecret(deskeyspec), new IvParameterSpec(s1.getBytes("UTF-8")));
        return new String(cipher.doFinal(s));
    }

    public static String desEncrypt(String s, String s1)
        throws Exception
    {
        return toHexString(desEncryptbyte(s, s1));
    }

    public static byte[] desEncryptbyte(String s, String s1)
        throws Exception
    {
        Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
        DESKeySpec deskeyspec = new DESKeySpec(s1.getBytes("UTF-8"));
        cipher.init(1, SecretKeyFactory.getInstance("DES").generateSecret(deskeyspec), new IvParameterSpec(s1.getBytes("UTF-8")));
        return cipher.doFinal(s.getBytes("UTF-8"));
    }

    private static String toHexString(byte abyte0[])
    {
        StringBuffer stringbuffer = new StringBuffer();
        for (int i = 0; i < abyte0.length; i++)
        {
            String s1 = Integer.toHexString(abyte0[i] & 0xff);
            String s = s1;
            if (s1.length() < 2)
            {
                s = (new StringBuilder()).append("0").append(s1).toString();
            }
            stringbuffer.append(s);
        }

        return stringbuffer.toString();
    }
}
