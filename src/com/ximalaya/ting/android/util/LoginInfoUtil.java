// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.personal_info.LoginInfoModel;
import com.ximalaya.ting.android.model.personal_info.UserInfoModel;
import com.ximalaya.ting.android.modelmanage.UserInfoMannage;

// Referenced classes of package com.ximalaya.ting.android.util:
//            Logger

public class LoginInfoUtil
{

    public LoginInfoUtil()
    {
    }

    private static LoginInfoModel parseLoginfo(String s)
    {
label0:
        {
            try
            {
                if (!"0".equals(JSON.parseObject(s).get("ret").toString()))
                {
                    break label0;
                }
                s = (LoginInfoModel)JSON.parseObject(s, com/ximalaya/ting/android/model/personal_info/LoginInfoModel);
            }
            // Misplaced declaration of an exception variable
            catch (String s)
            {
                Logger.log((new StringBuilder()).append("\u89E3\u6790json\u5F02\u5E38\uFF1A").append(Logger.getLineInfo()).toString());
                return null;
            }
            return s;
        }
        return null;
    }

    public static void refreshEmailAndPhone(Context context, UserInfoModel userinfomodel, UserInfoModel userinfomodel1)
    {
        setEmailAndPhone(userinfomodel, userinfomodel1);
        userinfomodel = UserInfoMannage.getInstance();
        if (userinfomodel != null && userinfomodel.getUser() != null)
        {
            setEmailAndPhone(userinfomodel.getUser(), userinfomodel1);
        }
        context = SharedPreferencesUtil.getInstance(context);
        userinfomodel = context.getString("loginforesult");
        if (userinfomodel != null && !"".equals(userinfomodel))
        {
            userinfomodel = parseLoginfo(userinfomodel);
            if (userinfomodel != null)
            {
                setEmailAndPhone(userinfomodel, userinfomodel1);
                context.saveString("loginforesult", JSON.toJSONString(userinfomodel));
            }
        }
    }

    private static void setEmailAndPhone(UserInfoModel userinfomodel, UserInfoModel userinfomodel1)
    {
        userinfomodel.email = userinfomodel1.email;
        userinfomodel.isVEmail = userinfomodel1.isVEmail;
        userinfomodel.isVMobile = userinfomodel1.isVMobile;
        userinfomodel.mPhone = userinfomodel1.mPhone;
    }
}
