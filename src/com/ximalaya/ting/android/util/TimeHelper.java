// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

// Referenced classes of package com.ximalaya.ting.android.util:
//            Utilities

public class TimeHelper
{

    public TimeHelper()
    {
    }

    public static String CountTime(String s)
    {
        if (Utilities.isBlank(s))
        {
            return "\u4F20\u5165\u65E5\u671F\u6709\u8BEF";
        }
        long l6 = (System.currentTimeMillis() - Long.valueOf(s).longValue()) / 1000L;
        if (l6 < 0L)
        {
            return countTime2(Long.valueOf(s).longValue());
        }
        long l = l6 / 0x1da9c00L;
        long l1 = l6 / 0x278d00L;
        long l2 = l6 / 0x93a80L;
        long l3 = l6 / 0x15180L;
        long l4 = (l6 % 0x15180L) / 3600L;
        long l5 = (l6 % 3600L) / 60L;
        l6 = (l6 % 60L) / 60L;
        s = new StringBuffer();
        if (l != 0L)
        {
            s.append((new StringBuilder()).append(l).append("\u5E74").toString());
            return (new StringBuilder()).append(s.toString()).append("\u524D").toString();
        }
        if (l1 != 0L)
        {
            s.append((new StringBuilder()).append(l1).append("\u4E2A\u6708").toString());
            return (new StringBuilder()).append(s.toString()).append("\u524D").toString();
        }
        if (l2 != 0L)
        {
            s.append((new StringBuilder()).append(l2).append("\u5468").toString());
            return (new StringBuilder()).append(s.toString()).append("\u524D").toString();
        }
        if (l3 != 0L)
        {
            s.append((new StringBuilder()).append(l3).append("\u5929").toString());
            return (new StringBuilder()).append(s.toString()).append("\u524D").toString();
        }
        if (l4 != 0L)
        {
            s.append((new StringBuilder()).append(l4).append("\u5C0F\u65F6").toString());
            return (new StringBuilder()).append(s.toString()).append("\u524D").toString();
        }
        if (l5 != 0L)
        {
            s.append((new StringBuilder()).append(l5).append("\u5206\u949F").toString());
            return (new StringBuilder()).append(s.toString()).append("\u524D").toString();
        }
        if (l6 != 0L)
        {
            s.append((new StringBuilder()).append(l6).append("\u79D2").toString());
            return (new StringBuilder()).append(s.toString()).append("\u524D").toString();
        } else
        {
            return "";
        }
    }

    public static String countTime2(long l)
    {
        SimpleDateFormat simpledateformat1 = new SimpleDateFormat("yyyy");
        Date date1 = new Date();
        Date date = new Date(l);
        if (Integer.valueOf(simpledateformat1.format(date1)) != Integer.valueOf(simpledateformat1.format(date)))
        {
            return (new SimpleDateFormat("yyyy-MM-dd HH:mm")).format(date);
        }
        simpledateformat1 = new SimpleDateFormat("MM-dd");
        if (!simpledateformat1.format(date1).equals(simpledateformat1.format(date)))
        {
            return (new SimpleDateFormat("MM-dd HH:mm")).format(date);
        }
        long l2 = (System.currentTimeMillis() - Long.valueOf(l).longValue()) / 1000L;
        l = (l2 % 0x15180L) / 3600L;
        long l1 = (l2 % 3600L) / 60L;
        l2 = (l2 % 60L) / 60L;
        if (l != 0L)
        {
            SimpleDateFormat simpledateformat = new SimpleDateFormat("HH:mm");
            return (new StringBuilder()).append("\u4ECA\u5929 ").append(simpledateformat.format(date)).toString();
        }
        if (l1 > 0L)
        {
            return (new StringBuilder()).append(l1).append("\u5206\u949F").append("\u524D").toString();
        }
        if (l2 >= 0L)
        {
            return "\u521A\u521A";
        } else
        {
            return "";
        }
    }

    public static String countTime2(String s)
    {
        if (Utilities.isBlank(s))
        {
            return "\u4F20\u5165\u65E5\u671F\u6709\u8BEF";
        } else
        {
            return countTime2(Long.valueOf(s).longValue());
        }
    }

    public static String getDateFriendlyStr()
    {
        Calendar calendar = Calendar.getInstance();
        int i = calendar.get(1);
        int j = calendar.get(2);
        int k = calendar.get(5);
        return (new StringBuilder()).append("").append(i).append(j + 1).append(k).toString();
    }

    public static boolean isToday(long l)
    {
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTimeInMillis(l);
        while (l <= 0L || calendar.get(1) != calendar1.get(1) || calendar.get(2) != calendar1.get(2) || calendar.get(5) != calendar1.get(5)) 
        {
            return false;
        }
        return true;
    }

    public static boolean isYesterday(long l)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(6, -1);
        Calendar calendar1 = Calendar.getInstance();
        calendar1.setTime(new Date(l));
        return calendar.get(1) == calendar1.get(1) && calendar.get(6) == calendar1.get(6);
    }

    public static String secondToMinute(String s)
    {
        StringBuffer stringbuffer;
        int j;
        int k;
        double d;
        int i;
        try
        {
            stringbuffer = new StringBuffer();
            d = Double.valueOf(s).doubleValue();
            i = (int)d / 3600;
        }
        // Misplaced declaration of an exception variable
        catch (String s)
        {
            s.printStackTrace();
            return "\u672A\u77E5";
        }
        k = (int)d % 3600;
        j = k / 60;
        k %= 60;
        if (i <= 0) goto _L2; else goto _L1
_L1:
        if (i <= 9) goto _L4; else goto _L3
_L3:
        stringbuffer.append((new StringBuilder()).append(i).append(":").toString());
_L2:
        if (j <= 9) goto _L6; else goto _L5
_L5:
        stringbuffer.append((new StringBuilder()).append(j).append(":").toString());
_L7:
        if (k <= 9)
        {
            break MISSING_BLOCK_LABEL_208;
        }
        stringbuffer.append(k);
_L8:
        return stringbuffer.toString();
_L4:
        stringbuffer.append((new StringBuilder()).append("0").append(i).append(":").toString());
          goto _L2
_L6:
        stringbuffer.append((new StringBuilder()).append("0").append(j).append(":").toString());
          goto _L7
        stringbuffer.append((new StringBuilder()).append("0").append(k).toString());
          goto _L8
    }

    public static int weekOfToday()
    {
        int i = Calendar.getInstance().get(7);
        if (i == 1)
        {
            return 6;
        } else
        {
            return i - 2;
        }
    }
}
