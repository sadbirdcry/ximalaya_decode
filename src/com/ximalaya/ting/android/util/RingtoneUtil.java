// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.media.RingtoneManager;
import java.io.File;
import java.net.URI;

// Referenced classes of package com.ximalaya.ting.android.util:
//            Logger

public class RingtoneUtil
{
    static class a
    {

        private String a;
        private String b;
        private String c;

        static String a(a a1)
        {
            return a1.a;
        }

        static String b(a a1)
        {
            return a1.b;
        }

        static String c(a a1)
        {
            return a1.c;
        }

        public a(String s, String s1, String s2)
        {
            a = s;
            b = s1;
            c = s2;
        }
    }


    public RingtoneUtil()
    {
    }

    public static a buildMediaInfo(String s, String s1, String s2)
    {
        return new a(s, s1, s2);
    }

    private static ContentValues decoMediaInfo(ContentValues contentvalues, a a1)
    {
        contentvalues.put("title", a.a(a1));
        contentvalues.put("title", a.a(a1));
        contentvalues.put("album", a.b(a1));
        contentvalues.put("artist", a.c(a1));
        return contentvalues;
    }

    public static void setMyAlarm(Context context, String s, a a1)
    {
        File file = new File(URI.create(s));
        s = new ContentValues();
        s.put("_data", file.getAbsolutePath());
        s.put("mime_type", "audio/*");
        int i;
        if (a1 == null)
        {
            s.put("title", file.getName());
        } else
        {
            s = decoMediaInfo(s, a1);
        }
        s.put("is_ringtone", Boolean.valueOf(false));
        s.put("is_notification", Boolean.valueOf(false));
        s.put("is_alarm", Boolean.valueOf(true));
        s.put("is_music", Boolean.valueOf(false));
        a1 = android.provider.MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
        i = context.getContentResolver().delete(a1, "_data=?", new String[] {
            file.getAbsolutePath()
        });
        Logger.d("RingtoneUtil.setMyAlarm", (new StringBuilder()).append("deleteOldData:").append(i).toString());
        RingtoneManager.setActualDefaultRingtoneUri(context, 4, context.getContentResolver().insert(a1, s));
    }

    public static void setMyNotification(Context context, String s, a a1)
    {
        File file = new File(URI.create(s));
        s = new ContentValues();
        s.put("_data", file.getAbsolutePath());
        s.put("mime_type", "audio/*");
        int i;
        if (a1 == null)
        {
            s.put("title", file.getName());
        } else
        {
            s = decoMediaInfo(s, a1);
        }
        s.put("is_ringtone", Boolean.valueOf(false));
        s.put("is_notification", Boolean.valueOf(true));
        s.put("is_alarm", Boolean.valueOf(false));
        s.put("is_music", Boolean.valueOf(false));
        a1 = android.provider.MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
        i = context.getContentResolver().delete(a1, "_data=?", new String[] {
            file.getAbsolutePath()
        });
        Logger.d("RingtoneUtil.setMyNotification", (new StringBuilder()).append("deleteOldData:").append(i).toString());
        RingtoneManager.setActualDefaultRingtoneUri(context, 2, context.getContentResolver().insert(a1, s));
    }

    public static void setMyRingtone(Context context, String s, a a1)
    {
        File file = new File(URI.create(s));
        s = new ContentValues();
        s.put("_data", file.getAbsolutePath());
        s.put("mime_type", "audio/*");
        int i;
        if (a1 == null)
        {
            s.put("title", file.getName());
        } else
        {
            s = decoMediaInfo(s, a1);
        }
        s.put("is_ringtone", Boolean.valueOf(true));
        s.put("is_notification", Boolean.valueOf(false));
        s.put("is_alarm", Boolean.valueOf(false));
        s.put("is_music", Boolean.valueOf(false));
        a1 = android.provider.MediaStore.Audio.Media.getContentUriForPath(file.getAbsolutePath());
        i = context.getContentResolver().delete(a1, "_data=?", new String[] {
            file.getAbsolutePath()
        });
        Logger.d("RingtoneUtil.setMyRingtone", (new StringBuilder()).append("deleteOldData:").append(i).toString());
        RingtoneManager.setActualDefaultRingtoneUri(context, 1, context.getContentResolver().insert(a1, s));
    }
}
