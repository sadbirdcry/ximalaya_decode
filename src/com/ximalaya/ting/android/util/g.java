// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.a.e;
import com.ximalaya.ting.android.b.f;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import java.util.List;
import org.apache.http.entity.ByteArrayEntity;

// Referenced classes of package com.ximalaya.ting.android.util:
//            DataCollectUtil

class g
    implements Runnable
{

    final DataCollectUtil a;

    g(DataCollectUtil datacollectutil)
    {
        a = datacollectutil;
        super();
    }

    public void run()
    {
        try
        {
            Object obj = new DataCollectUtil.EventRecord();
            obj.events = DataCollectUtil.access$000(a);
            obj = JSON.toJSONString(obj);
            MyLogger.getLogger().d((new StringBuilder()).append("AdCollectData=").append(((String) (obj))).toString());
            obj = new ByteArrayEntity(DataCollectUtil.compress(((String) (obj)).getBytes()));
            f.a().a(DataCollectUtil.access$100(a), e.an, null, ((org.apache.http.HttpEntity) (obj)), "application/octet-stream");
            DataCollectUtil.access$000(a).clear();
            a.saveEvents();
            return;
        }
        catch (Exception exception)
        {
            MyLogger.getLogger().d((new StringBuilder()).append("exception=").append(exception.getMessage()).toString());
        }
    }
}
