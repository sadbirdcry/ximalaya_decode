// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.telephony.TelephonyManager;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.library.view.dialog.DialogBuilder;
import com.ximalaya.ting.android.service.play.LocalMediaService;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

// Referenced classes of package com.ximalaya.ting.android.util:
//            ao, Logger, FreeFlowUtil, aj, 
//            ak, ToolUtil, an, am, 
//            al

public final class NetworkUtils
{

    public static final int OPERATOR_CMCC = 0;
    public static final int OPERATOR_OTHER = 3;
    public static final int OPERATOR_TELECOM = 2;
    public static final int OPERATOR_UNICOME = 1;

    public NetworkUtils()
    {
    }

    public static void changeNetWorkSet(com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback)
    {
        (new ao(dialogcallback)).myexec(new Void[0]);
    }

    private static String getMobileIp()
    {
        Object obj = NetworkInterface.getNetworkInterfaces();
_L2:
        Enumeration enumeration;
        if (!((Enumeration) (obj)).hasMoreElements())
        {
            break MISSING_BLOCK_LABEL_63;
        }
        enumeration = ((NetworkInterface)((Enumeration) (obj)).nextElement()).getInetAddresses();
_L4:
        if (!enumeration.hasMoreElements()) goto _L2; else goto _L1
_L1:
        InetAddress inetaddress = (InetAddress)enumeration.nextElement();
        if (inetaddress.isLoopbackAddress()) goto _L4; else goto _L3
_L3:
        obj = inetaddress.getHostAddress().toString();
        return ((String) (obj));
        SocketException socketexception;
        socketexception;
        return null;
    }

    public static int getNetType(Context context)
    {
        if (context == null)
        {
            return -1;
        }
        context = (ConnectivityManager)context.getSystemService("connectivity");
        if (context == null)
        {
            return -1;
        }
        try
        {
            context = context.getActiveNetworkInfo();
        }
        // Misplaced declaration of an exception variable
        catch (Context context)
        {
            context = null;
        }
        if (context != null && context.isAvailable())
        {
            return context.getType() != 1 ? 0 : 1;
        } else
        {
            return -1;
        }
    }

    public static String getNetTypeDetail(Context context)
    {
        if (context == null)
        {
            return null;
        }
        context = (ConnectivityManager)context.getSystemService("connectivity");
        if (context == null)
        {
            return null;
        }
        context = context.getActiveNetworkInfo();
        if (context == null)
        {
            return null;
        }
        if (context.getType() == 1)
        {
            return "WIFI";
        } else
        {
            return context.getExtraInfo();
        }
    }

    public static String getNetworkClass(Context context)
    {
        NetworkInfo networkinfo = ((ConnectivityManager)context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (networkinfo != null && networkinfo.isConnected() && networkinfo.getType() == 1)
        {
            return "WIFI";
        }
        switch (((TelephonyManager)context.getSystemService("phone")).getNetworkType())
        {
        default:
            return "";

        case 1: // '\001'
        case 2: // '\002'
        case 4: // '\004'
        case 7: // '\007'
        case 11: // '\013'
            return "2G";

        case 3: // '\003'
        case 5: // '\005'
        case 6: // '\006'
        case 8: // '\b'
        case 9: // '\t'
        case 10: // '\n'
        case 12: // '\f'
        case 14: // '\016'
        case 15: // '\017'
            return "3G";

        case 13: // '\r'
            return "4G";
        }
    }

    public static int getOperator(Context context)
    {
        context = ((TelephonyManager)context.getSystemService("phone")).getSimOperator();
        if (context != null)
        {
            if (context.equals("46000") || context.equals("46002") || context.equals("46007"))
            {
                return 0;
            }
            if (context.equals("46001"))
            {
                return 1;
            }
            return !context.equals("46003") ? 3 : 2;
        } else
        {
            return 3;
        }
    }

    public static String getPhoneIP(Context context)
    {
        String s;
        int i;
        s = "192.168.1.1";
        i = getNetType(context);
        if (i != 0) goto _L2; else goto _L1
_L1:
        s = getMobileIp();
_L4:
        Logger.log((new StringBuilder()).append("KDTAction ip = ").append(s).toString());
        return s;
_L2:
        if (i == 1)
        {
            s = getWifiIp(context);
        }
        if (true) goto _L4; else goto _L3
_L3:
    }

    private static String getWifiIp(Context context)
    {
        context = ((WifiManager)context.getSystemService("wifi")).getConnectionInfo();
        if (context == null)
        {
            return "";
        } else
        {
            return intToIp(context.getIpAddress());
        }
    }

    private static String intToIp(int i)
    {
        return (new StringBuilder()).append(i & 0xff).append(".").append(i >> 8 & 0xff).append(".").append(i >> 16 & 0xff).append(".").append(i >> 24 & 0xff).toString();
    }

    public static boolean isNetworkAvaliable(Context context)
    {
        boolean flag = false;
        if (-1 != getNetType(context))
        {
            flag = true;
        }
        return flag;
    }

    public static void showChangeNetWorkSetConfirm(com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback, com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback1)
    {
        if (LocalMediaService.getInstance() != null && LocalMediaService.getInstance().isPlaying() && LocalMediaService.getInstance().isPlayFromNetwork())
        {
            LocalMediaService.getInstance().pause();
        }
        FreeFlowUtil.getInstance().informFreeFlow(new aj(dialogcallback, dialogcallback1), new ak(dialogcallback), dialogcallback, dialogcallback1);
    }

    private static void showNormalNoWifiDialog(com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback, com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback1, com.ximalaya.ting.android.library.view.dialog.DialogBuilder.DialogCallback dialogcallback2)
    {
        Activity activity = MyApplication.a();
        if (activity == null || activity.isFinishing())
        {
            return;
        } else
        {
            ToolUtil.onEvent(activity.getApplicationContext(), "NetworkSwitch_NoAds");
            (new DialogBuilder(MyApplication.a())).setTitle("\u6D41\u91CF\u63D0\u9192").setMessage(0x7f090150).setCancelBtn(dialogcallback2).setOkBtn("\u603B\u662F\u5141\u8BB8", new an(activity, dialogcallback)).setNeutralBtn("\u5141\u8BB8\u672C\u6B21", new am(dialogcallback1)).setCancelBtn(new al(activity, dialogcallback2)).showMultiButton();
            return;
        }
    }

}
