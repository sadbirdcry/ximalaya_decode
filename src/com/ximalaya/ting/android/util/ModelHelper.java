// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.text.TextUtils;
import com.ximalaya.ting.android.MyApplication;
import com.ximalaya.ting.android.communication.AlbumDetailDTO;
import com.ximalaya.ting.android.communication.AlbumHeadInfoDTO;
import com.ximalaya.ting.android.communication.AlbumTracksDTO;
import com.ximalaya.ting.android.communication.TrackInfoBelongAlbumDTO;
import com.ximalaya.ting.android.model.album.AlbumModel;
import com.ximalaya.ting.android.model.album.HotAlbum;
import com.ximalaya.ting.android.model.album.IAlbum;
import com.ximalaya.ting.android.model.anchor.Track;
import com.ximalaya.ting.android.model.category.detail.CategoryAlbumModel;
import com.ximalaya.ting.android.model.category.detail.CategoryTrackModel;
import com.ximalaya.ting.android.model.download.DownloadTask;
import com.ximalaya.ting.android.model.feed.BaseFeedModel;
import com.ximalaya.ting.android.model.feed.ChildFeedModel;
import com.ximalaya.ting.android.model.feed.FeedData;
import com.ximalaya.ting.android.model.feed.FeedModel;
import com.ximalaya.ting.android.model.feed2.FeedSoundInfo;
import com.ximalaya.ting.android.model.finding2.rank.RankTrackModel;
import com.ximalaya.ting.android.model.finding2.recommend.RecmdItemModel;
import com.ximalaya.ting.android.model.livefm.RadioPlayUrl;
import com.ximalaya.ting.android.model.livefm.RadioSound;
import com.ximalaya.ting.android.model.livefm.RecommendRadioListModel;
import com.ximalaya.ting.android.model.message.SoundInCommentModel;
import com.ximalaya.ting.android.model.search.SearchAlbum;
import com.ximalaya.ting.android.model.search.SearchAlbumNew;
import com.ximalaya.ting.android.model.search.SearchSound;
import com.ximalaya.ting.android.model.search.SearchSoundNew;
import com.ximalaya.ting.android.model.sound.AlbumSoundModel;
import com.ximalaya.ting.android.model.sound.AlbumSoundModelNew;
import com.ximalaya.ting.android.model.sound.HotSoundModel;
import com.ximalaya.ting.android.model.sound.SoundDetailModel;
import com.ximalaya.ting.android.model.sound.SoundDetails;
import com.ximalaya.ting.android.model.sound.SoundInfo;
import com.ximalaya.ting.android.model.sound.SoundInfoDetail;
import com.ximalaya.ting.android.model.sound.SoundInfoNew;
import com.ximalaya.ting.android.model.sound.SoundModel;
import com.ximalaya.ting.android.modelnew.AlbumModelNew;
import com.ximalaya.ting.android.service.play.TingMediaPlayer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.ximalaya.ting.android.util:
//            PlaylistFromDownload, Utilities

public class ModelHelper
{

    public ModelHelper()
    {
    }

    public static final List albumSoundlistNewToSoundInfoList(List list, String s, String s1)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((AlbumSoundModelNew)list.next(), s, s1))) { }
        return arraylist;
    }

    public static final List albumSoundlistToSoundInfoList(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((AlbumSoundModel)list.next()))) { }
        return arraylist;
    }

    public static final List albumSoundlistToSoundInfoList(List list, String s, String s1)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((AlbumSoundModel)list.next(), s, s1))) { }
        return arraylist;
    }

    public static final List baseFeedToSoundInfoList(BaseFeedModel basefeedmodel)
    {
        ArrayList arraylist = new ArrayList();
        for (basefeedmodel = basefeedmodel.getChildFeedList().iterator(); basefeedmodel.hasNext(); arraylist.add(toSoundInfo((ChildFeedModel)basefeedmodel.next()))) { }
        return arraylist;
    }

    public static final PlaylistFromDownload downloadlistToPlayList(DownloadTask downloadtask, List list)
    {
        ArrayList arraylist = new ArrayList();
        list = list.iterator();
        int i = 0;
        int j = -1;
        while (list.hasNext()) 
        {
            DownloadTask downloadtask1 = (DownloadTask)list.next();
            int l;
            if (downloadtask1.downloadStatus == 4)
            {
                arraylist.add(toSoundInfo(downloadtask1));
                if (downloadtask1.trackId == downloadtask.trackId)
                {
                    j = i;
                }
                int k = j;
                j = i + 1;
                i = k;
            } else
            {
                int i1 = i;
                i = j;
                j = i1;
            }
            l = j;
            j = i;
            i = l;
        }
        return new PlaylistFromDownload(j, arraylist);
    }

    public static final List downloadlistToSoundInfoList(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((DownloadTask)list.next()))) { }
        return arraylist;
    }

    public static final List feedlistToSoundInfoList(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((FeedModel)list.next()))) { }
        return arraylist;
    }

    public static final List hotlistToSoundInfoList(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((HotSoundModel)list.next()))) { }
        return arraylist;
    }

    public static final List recordingModelToSoundInfoList(List list)
    {
        ArrayList arraylist = new ArrayList();
        list = list.iterator();
        do
        {
            if (!list.hasNext())
            {
                break;
            }
            SoundModel soundmodel = (SoundModel)list.next();
            if (soundmodel.processState == 2)
            {
                arraylist.add(toSoundInfo(soundmodel));
            }
        } while (true);
        return arraylist;
    }

    public static final List searchSoundToSoundInfoList(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((SearchSound)list.next()))) { }
        return arraylist;
    }

    public static final List searchSoundToSoundInfoList2(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((SearchSoundNew)list.next()))) { }
        return arraylist;
    }

    public static final List soundInCommentModelToSoundInfoList(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((SoundInCommentModel)list.next()))) { }
        return arraylist;
    }

    public static List soundInfoDetailListToSoundInfoList(List list)
    {
        if (list == null)
        {
            return null;
        }
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((SoundInfoDetail)list.next()))) { }
        return arraylist;
    }

    public static final List soundModelToSoundInfoList(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((SoundModel)list.next()))) { }
        return arraylist;
    }

    public static final AlbumModel toAlbumModel(HotAlbum hotalbum)
    {
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = hotalbum.album_id;
        albummodel.title = hotalbum.title;
        albummodel.updatedAt = hotalbum.updatedAt;
        albummodel.createdAt = hotalbum.createdAt;
        albummodel.categoryId = hotalbum.category_id;
        String s;
        if (TextUtils.isEmpty(hotalbum.cover_path_small))
        {
            s = hotalbum.album_cover_path_86;
        } else
        {
            s = hotalbum.cover_path_small;
        }
        albummodel.coverSmall = s;
        albummodel.intro = hotalbum.intro;
        albummodel.uid = hotalbum.uid;
        albummodel.isVerified = hotalbum.is_v;
        albummodel.nickname = hotalbum.nickname;
        albummodel.avatarPath = hotalbum.avatar_path;
        albummodel.isFavorite = hotalbum.is_favorite;
        albummodel.playTimes = hotalbum.plays_counts;
        albummodel.lastUptrackAt = hotalbum.updatedAt;
        return albummodel;
    }

    public static AlbumModel toAlbumModel(IAlbum ialbum)
    {
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = ialbum.getIAlbumId();
        albummodel.coverLarge = ialbum.getIAlbumCoverUrl();
        albummodel.title = ialbum.getIAlbumTitle();
        albummodel.lastUptrackAt = ialbum.getIAlbumLastUpdateAt();
        albummodel.playTimes = (int)ialbum.getIAlbumPlayCount();
        albummodel.tracks = (int)ialbum.getIAlbumTrackCount();
        return albummodel;
    }

    public static final AlbumModel toAlbumModel(CategoryAlbumModel categoryalbummodel)
    {
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = categoryalbummodel.getAlbumId();
        albummodel.lastUptrackAt = categoryalbummodel.getLastUptrackAt();
        albummodel.lastUptrackTitle = categoryalbummodel.getLastUptrackTitle();
        albummodel.tracks = categoryalbummodel.getTracks();
        albummodel.coverSmall = categoryalbummodel.getCoverMiddle();
        albummodel.title = categoryalbummodel.getTitle();
        albummodel.playTimes = (int)categoryalbummodel.getPlaysCounts();
        return albummodel;
    }

    public static AlbumModel toAlbumModel(DownloadTask downloadtask)
    {
        if (downloadtask == null)
        {
            return null;
        } else
        {
            AlbumModel albummodel = new AlbumModel();
            albummodel.albumId = downloadtask.albumId;
            albummodel.title = downloadtask.albumName;
            return albummodel;
        }
    }

    public static final AlbumModel toAlbumModel(SearchAlbum searchalbum)
    {
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = searchalbum.id;
        albummodel.title = searchalbum.title;
        albummodel.updatedAt = searchalbum.updated_at;
        albummodel.createdAt = searchalbum.created_at;
        albummodel.categoryId = searchalbum.category_id;
        albummodel.coverSmall = searchalbum.cover_path;
        albummodel.intro = searchalbum.intro;
        albummodel.uid = searchalbum.uid;
        albummodel.tracks = searchalbum.tracks;
        albummodel.isVerified = searchalbum.is_v;
        albummodel.nickname = searchalbum.nickname;
        albummodel.avatarPath = searchalbum.avatar_path;
        albummodel.isFavorite = searchalbum.isFavorite;
        albummodel.lastUptrackAt = searchalbum.updated_at;
        return albummodel;
    }

    public static final AlbumModel toAlbumModel(AlbumModelNew albummodelnew)
    {
        AlbumModel albummodel = new AlbumModel();
        albummodel.albumId = albummodelnew.id;
        albummodel.title = albummodelnew.title;
        albummodel.coverSmall = albummodelnew.albumCoverUrl290;
        albummodel.intro = albummodelnew.msg;
        albummodel.isFavorite = albummodelnew.isFavorite;
        albummodel.tracks = albummodelnew.tracksCounts;
        albummodel.nickname = albummodelnew.nickname;
        albummodel.lastUptrackAt = albummodelnew.lastUptrackAt;
        return albummodel;
    }

    public static AlbumModelNew toAlbumModelNew(SearchAlbum searchalbum)
    {
        if (searchalbum == null)
        {
            return null;
        } else
        {
            AlbumModelNew albummodelnew = new AlbumModelNew();
            albummodelnew.albumCoverUrl290 = searchalbum.cover_path;
            albummodelnew.id = searchalbum.id;
            albummodelnew.isFavorite = searchalbum.isFavorite;
            albummodelnew.playsCounts = searchalbum.play;
            albummodelnew.title = searchalbum.title;
            albummodelnew.tracksCounts = searchalbum.tracks;
            return albummodelnew;
        }
    }

    public static AlbumModelNew toAlbumModelNew2(SearchAlbumNew searchalbumnew)
    {
        if (searchalbumnew == null)
        {
            return null;
        } else
        {
            AlbumModelNew albummodelnew = new AlbumModelNew();
            albummodelnew.albumCoverUrl290 = searchalbumnew.cover_url_middle;
            albummodelnew.id = searchalbumnew.id;
            albummodelnew.isFavorite = false;
            albummodelnew.playsCounts = searchalbumnew.plays_count;
            albummodelnew.title = searchalbumnew.title;
            albummodelnew.tracksCounts = searchalbumnew.tracks_count;
            return albummodelnew;
        }
    }

    public static List toAlbumModelNewList(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toAlbumModelNew((SearchAlbum)list.next()))) { }
        return arraylist;
    }

    public static List toAlbumModelNewList2(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toAlbumModelNew2((SearchAlbumNew)list.next()))) { }
        return arraylist;
    }

    public static CategoryAlbumModel toCategoryAlbumModel(SearchAlbumNew searchalbumnew)
    {
        if (searchalbumnew == null)
        {
            return null;
        } else
        {
            CategoryAlbumModel categoryalbummodel = new CategoryAlbumModel();
            categoryalbummodel.setCoverMiddle(searchalbumnew.cover_url_middle);
            categoryalbummodel.setAlbumId(searchalbumnew.id);
            categoryalbummodel.setCCollected(false);
            categoryalbummodel.setPlaysCounts(searchalbumnew.plays_count);
            categoryalbummodel.setTitle(searchalbumnew.title);
            categoryalbummodel.setTracks(searchalbumnew.tracks_count);
            return categoryalbummodel;
        }
    }

    public static List toCategoryAlbumModelList(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toCategoryAlbumModel((SearchAlbumNew)list.next()))) { }
        return arraylist;
    }

    public static final List toDownloadList(AlbumDetailDTO albumdetaildto)
    {
        ArrayList arraylist = new ArrayList();
        if (albumdetaildto == null || albumdetaildto.ret != 0 || albumdetaildto.album == null || albumdetaildto.tracks == null || albumdetaildto.tracks.list == null || albumdetaildto.tracks.list.size() == 0)
        {
            return arraylist;
        }
        Iterator iterator = albumdetaildto.tracks.list.iterator();
        int i = 0;
        while (iterator.hasNext()) 
        {
            TrackInfoBelongAlbumDTO trackinfobelongalbumdto = (TrackInfoBelongAlbumDTO)iterator.next();
            DownloadTask downloadtask = new DownloadTask();
            downloadtask.trackId = trackinfobelongalbumdto.trackId;
            downloadtask.title = trackinfobelongalbumdto.title;
            downloadtask.playUrl32 = trackinfobelongalbumdto.playUrl32;
            downloadtask.playUrl64 = trackinfobelongalbumdto.playUrl64;
            downloadtask.playPathAacv224 = trackinfobelongalbumdto.playPathAacv224;
            downloadtask.playPathAacv164 = trackinfobelongalbumdto.playPathAacv164;
            downloadtask.duration = trackinfobelongalbumdto.duration;
            downloadtask.create_at = trackinfobelongalbumdto.createdAt;
            downloadtask.plays_counts = trackinfobelongalbumdto.playtimes;
            downloadtask.favorites_counts = trackinfobelongalbumdto.likes;
            downloadtask.nickname = trackinfobelongalbumdto.nickname;
            downloadtask.orderNum = i;
            downloadtask.uid = trackinfobelongalbumdto.uid;
            downloadtask.comments_counts = trackinfobelongalbumdto.comments;
            downloadtask.downloaded = 0L;
            downloadtask.coverSmall = trackinfobelongalbumdto.coverSmall;
            downloadtask.coverLarge = trackinfobelongalbumdto.coverLarge;
            if (trackinfobelongalbumdto.albumId > 0L)
            {
                downloadtask.albumId = trackinfobelongalbumdto.albumId;
            } else
            {
                downloadtask.albumId = albumdetaildto.album.albumId;
            }
            if (Utilities.isNotBlank(trackinfobelongalbumdto.albumTitle))
            {
                downloadtask.albumName = trackinfobelongalbumdto.albumTitle;
            } else
            {
                downloadtask.albumName = albumdetaildto.album.title;
            }
            if (Utilities.isNotBlank(trackinfobelongalbumdto.albumImage))
            {
                downloadtask.albumCoverPath = trackinfobelongalbumdto.albumImage;
            } else
            {
                downloadtask.albumCoverPath = albumdetaildto.album.coverSmall;
            }
            downloadtask.userCoverPath = trackinfobelongalbumdto.smallLogo;
            if (android.os.Build.VERSION.SDK_INT == 10)
            {
                downloadtask.downLoadUrl = downloadtask.playUrl64;
            } else
            {
                downloadtask.downLoadUrl = trackinfobelongalbumdto.downloadUrl;
            }
            downloadtask.processState = trackinfobelongalbumdto.processState;
            downloadtask.is_favorited = trackinfobelongalbumdto.isLike;
            downloadtask.user_source = trackinfobelongalbumdto.userSource;
            downloadtask.opType = trackinfobelongalbumdto.opType;
            downloadtask.isRelay = trackinfobelongalbumdto.isRelay;
            downloadtask.isPublic = trackinfobelongalbumdto.isPublic;
            downloadtask.favorites_counts = trackinfobelongalbumdto.likes;
            downloadtask.plays_counts = trackinfobelongalbumdto.playtimes;
            downloadtask.comments_counts = trackinfobelongalbumdto.comments;
            downloadtask.shares_counts = trackinfobelongalbumdto.shares;
            downloadtask.status = trackinfobelongalbumdto.status;
            downloadtask.is_playing = false;
            downloadtask.isRunning = true;
            downloadtask.sequnceId = albumdetaildto.sequnceId;
            downloadtask.downloadType = albumdetaildto.downloadType;
            downloadtask.albumHeader = albumdetaildto.album;
            arraylist.add(downloadtask);
            i++;
        }
        return arraylist;
    }

    public static final List toDownloadListForAlbum(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(new DownloadTask((AlbumSoundModel)list.next()))) { }
        return arraylist;
    }

    public static DownloadTask toDownloadTask(com.ximalaya.ting.android.model.album.AlbumSectionModel.Track track)
    {
        DownloadTask downloadtask = new DownloadTask();
        downloadtask.trackId = track.getTrackId();
        downloadtask.title = track.getTitle();
        downloadtask.duration = track.getDuration();
        downloadtask.create_at = track.getCreatedAt();
        downloadtask.plays_counts = (int)track.getPlaytimes();
        downloadtask.favorites_counts = (int)track.getLikes();
        downloadtask.nickname = track.getNickname();
        downloadtask.orderNum = track.getOrderNum();
        downloadtask.albumId = track.getAlbumId();
        downloadtask.albumName = track.getAlbumTitle();
        downloadtask.uid = track.getUid();
        downloadtask.comments_counts = (int)track.getComments();
        downloadtask.downloaded = 0L;
        downloadtask.coverSmall = track.getCoverSmall();
        downloadtask.coverLarge = track.getCoverLarge();
        downloadtask.albumCoverPath = track.getAlbumImage();
        downloadtask.downLoadUrl = track.getDownloadUrl();
        downloadtask.filesize = track.getDownloadSize();
        downloadtask.is_playing = false;
        downloadtask.isRunning = true;
        return downloadtask;
    }

    public static List toDownloadTaskList(List list)
    {
        if (list != null && list.size() > 0)
        {
            ArrayList arraylist = new ArrayList(list.size());
            for (list = list.iterator(); list.hasNext(); arraylist.add(toDownloadTask((com.ximalaya.ting.android.model.album.AlbumSectionModel.Track)list.next()))) { }
            return arraylist;
        } else
        {
            return null;
        }
    }

    public static final SoundDetails toSoundDetail(SoundDetailModel sounddetailmodel)
    {
        SoundDetails sounddetails = new SoundDetails();
        if (sounddetailmodel == null)
        {
            return null;
        } else
        {
            sounddetails.trackId = sounddetailmodel.trackId;
            sounddetails.tags = sounddetailmodel.tags;
            sounddetails.title = sounddetailmodel.title;
            sounddetails.uid = sounddetailmodel.uid;
            sounddetails.nickname = sounddetailmodel.nickname;
            sounddetails.userCoverPath = sounddetailmodel.smallLogo;
            sounddetails.coverLarge = sounddetailmodel.coverLarge;
            sounddetails.coverSmall = sounddetailmodel.coverSmall;
            sounddetails.albumId = sounddetailmodel.albumId;
            sounddetails.albumName = sounddetailmodel.albumTitle;
            sounddetails.categoryId = sounddetailmodel.categoryId;
            sounddetails.categoryName = sounddetailmodel.categoryName;
            sounddetails.intro = sounddetailmodel.intro;
            sounddetails.playUrl32 = sounddetailmodel.playUrl32;
            sounddetails.playUrl64 = sounddetailmodel.playUrl64;
            sounddetails.playPathAacv224 = sounddetailmodel.playPathAacv224;
            sounddetails.playPathAacv164 = sounddetailmodel.playPathAacv164;
            sounddetails.create_at = sounddetailmodel.createdAt;
            sounddetails.processState = sounddetailmodel.processState;
            sounddetails.duration = sounddetailmodel.duration;
            sounddetails.is_favorited = sounddetailmodel.isLike;
            sounddetails.favorites_counts = sounddetailmodel.likes;
            sounddetails.comments_counts = sounddetailmodel.comments;
            sounddetails.plays_counts = sounddetailmodel.playtimes;
            sounddetails.shares_counts = sounddetailmodel.shares;
            sounddetails.isRelay = sounddetailmodel.isRelay;
            sounddetails.isPublic = sounddetailmodel.isPublic;
            sounddetails.opType = sounddetailmodel.opType;
            sounddetails.refUid = sounddetailmodel.refUid;
            sounddetails.refNickname = sounddetailmodel.refNickname;
            sounddetails.refSmallLogo = sounddetailmodel.refSmallLogo;
            sounddetails.commentContent = sounddetailmodel.commentContent;
            sounddetails.commentId = sounddetailmodel.commentId;
            sounddetails.trackBlocks = sounddetailmodel.trackBlocks;
            sounddetails.firstComment = sounddetailmodel.firstComment;
            sounddetails.isFollowed = sounddetailmodel.isFollowed;
            return sounddetails;
        }
    }

    public static SoundInfo toSoundInfo(CategoryTrackModel categorytrackmodel)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.playUrl32 = categorytrackmodel.getPlayPath32();
        soundinfo.playUrl64 = categorytrackmodel.getPlayPath64();
        soundinfo.playPathAacv224 = categorytrackmodel.getPlayPathAacv224();
        soundinfo.playPathAacv164 = categorytrackmodel.getPlayPathAacv164();
        soundinfo.coverSmall = categorytrackmodel.getCoverSmall();
        soundinfo.trackId = categorytrackmodel.getTrackId();
        soundinfo.title = categorytrackmodel.getTitle();
        soundinfo.duration = categorytrackmodel.getDuration();
        soundinfo.favorites_counts = (int)categorytrackmodel.getFavoritesCounts();
        soundinfo.albumId = categorytrackmodel.getAlbumId();
        soundinfo.albumName = categorytrackmodel.getAlbumTitle();
        soundinfo.plays_counts = (int)categorytrackmodel.getPlaysCounts();
        soundinfo.uid = categorytrackmodel.getUid();
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(DownloadTask downloadtask)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.trackId = downloadtask.trackId;
        soundinfo.title = downloadtask.title;
        soundinfo.playUrl32 = downloadtask.playUrl32;
        soundinfo.playUrl64 = downloadtask.playUrl64;
        soundinfo.playPathAacv224 = downloadtask.playPathAacv224;
        soundinfo.playPathAacv164 = downloadtask.playPathAacv164;
        soundinfo.coverSmall = downloadtask.coverSmall;
        soundinfo.coverLarge = downloadtask.coverLarge;
        soundinfo.filesize = downloadtask.filesize;
        soundinfo.duration = downloadtask.duration;
        soundinfo.create_at = downloadtask.create_at;
        soundinfo.orderNum = downloadtask.orderNum;
        soundinfo.uid = downloadtask.uid;
        soundinfo.nickname = downloadtask.nickname;
        soundinfo.userCoverPath = downloadtask.userCoverPath;
        soundinfo.albumId = downloadtask.albumId;
        soundinfo.albumName = downloadtask.albumName;
        soundinfo.albumCoverPath = downloadtask.albumCoverPath;
        soundinfo.plays_counts = downloadtask.plays_counts;
        soundinfo.favorites_counts = downloadtask.favorites_counts;
        soundinfo.is_favorited = downloadtask.is_favorited;
        soundinfo.isRelay = downloadtask.isRelay;
        soundinfo.comments_counts = downloadtask.comments_counts;
        soundinfo.shares_counts = downloadtask.shares_counts;
        soundinfo.user_source = downloadtask.user_source;
        soundinfo.is_playing = downloadtask.is_playing;
        soundinfo.isRelay = downloadtask.isRelay;
        soundinfo.downLoadUrl = downloadtask.downLoadUrl;
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(ChildFeedModel childfeedmodel)
    {
        boolean flag1 = false;
        long l1 = 0L;
        SoundInfo soundinfo = new SoundInfo();
        String s;
        int i;
        long l;
        boolean flag;
        if (Utilities.isBlank(childfeedmodel.toTid))
        {
            l = 0L;
        } else
        {
            l = Long.valueOf(childfeedmodel.toTid).longValue();
        }
        soundinfo.trackId = l;
        soundinfo.title = childfeedmodel.title;
        soundinfo.playUrl32 = childfeedmodel.mUrl;
        soundinfo.playUrl64 = childfeedmodel.url;
        soundinfo.coverSmall = childfeedmodel.mtImagePath;
        soundinfo.filesize = 0L;
        soundinfo.duration = childfeedmodel.duration.doubleValue();
        soundinfo.create_at = childfeedmodel.createdAt.longValue();
        soundinfo.orderNum = 0L;
        soundinfo.uid = childfeedmodel.toUid.longValue();
        soundinfo.nickname = childfeedmodel.toNickName;
        soundinfo.userCoverPath = childfeedmodel.toImage;
        if (childfeedmodel.albumID == null)
        {
            l = l1;
        } else
        {
            l = childfeedmodel.albumID.longValue();
        }
        soundinfo.albumId = l;
        if (Utilities.isBlank(childfeedmodel.albumName))
        {
            s = "";
        } else
        {
            s = childfeedmodel.albumName;
        }
        soundinfo.albumName = s;
        soundinfo.albumCoverPath = childfeedmodel.albumImage;
        soundinfo.plays_counts = childfeedmodel.playtimes;
        soundinfo.favorites_counts = childfeedmodel.likes;
        if (Utilities.isBlank(childfeedmodel.isLike))
        {
            flag = false;
        } else
        {
            flag = Boolean.valueOf(childfeedmodel.isLike).booleanValue();
        }
        soundinfo.is_favorited = flag;
        if (childfeedmodel.isRelay != null)
        {
            flag = flag1;
        } else
        {
            flag = childfeedmodel.isRelay.booleanValue();
        }
        soundinfo.isRelay = flag;
        soundinfo.comments_counts = childfeedmodel.comments;
        soundinfo.shares_counts = childfeedmodel.shares;
        if (Utilities.isBlank(childfeedmodel.userSource))
        {
            i = 2;
        } else
        {
            i = Integer.valueOf(childfeedmodel.userSource).intValue();
        }
        soundinfo.user_source = i;
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(FeedModel feedmodel)
    {
        SoundInfo soundinfo = new SoundInfo();
        FeedData feeddata = (FeedData)feedmodel.datas.get(0);
        soundinfo.trackId = feeddata.toTid;
        soundinfo.title = feeddata.title;
        soundinfo.playUrl32 = feeddata.mUrl;
        soundinfo.playUrl64 = feeddata.url;
        soundinfo.coverSmall = feeddata.mtImagePath;
        soundinfo.filesize = 0L;
        soundinfo.duration = feeddata.duration;
        soundinfo.create_at = feeddata.createdAt;
        soundinfo.orderNum = 0L;
        soundinfo.uid = feedmodel.uid;
        soundinfo.nickname = feedmodel.nickName;
        soundinfo.userCoverPath = feedmodel.imagePath;
        soundinfo.albumId = feeddata.albumID;
        soundinfo.albumName = feeddata.albumName;
        soundinfo.albumCoverPath = feeddata.albumImage;
        soundinfo.plays_counts = feeddata.playtimes;
        soundinfo.favorites_counts = feeddata.likes;
        soundinfo.is_favorited = feeddata.isLike;
        soundinfo.isRelay = feeddata.isRelay;
        soundinfo.comments_counts = feeddata.comments;
        soundinfo.shares_counts = feeddata.shares;
        soundinfo.user_source = feeddata.userSource;
        soundinfo.coverLarge = feeddata.albumImage;
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(FeedSoundInfo feedsoundinfo)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.uid = feedsoundinfo.uid;
        soundinfo.trackId = feedsoundinfo.trackId;
        soundinfo.title = feedsoundinfo.title;
        soundinfo.playUrl32 = feedsoundinfo.playUrl32;
        soundinfo.playUrl64 = feedsoundinfo.playUrl64;
        soundinfo.playPathAacv224 = feedsoundinfo.playPathAacv224;
        soundinfo.playPathAacv164 = feedsoundinfo.playPathAacv164;
        soundinfo.coverSmall = feedsoundinfo.coverSmall;
        soundinfo.plays_counts = (int)feedsoundinfo.playsCounts;
        soundinfo.favorites_counts = (int)feedsoundinfo.favoritesCounts;
        soundinfo.shares_counts = (int)feedsoundinfo.sharesCounts;
        soundinfo.create_at = feedsoundinfo.createdAt;
        soundinfo.duration = feedsoundinfo.duration;
        soundinfo.user_source = feedsoundinfo.userSource;
        soundinfo.nickname = feedsoundinfo.nickname;
        soundinfo.albumName = feedsoundinfo.albumTitle;
        soundinfo.albumId = feedsoundinfo.albumId;
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(RankTrackModel ranktrackmodel)
    {
        if (ranktrackmodel == null)
        {
            return null;
        } else
        {
            SoundInfo soundinfo = new SoundInfo();
            soundinfo.albumId = ranktrackmodel.getAlbumId();
            soundinfo.playUrl32 = ranktrackmodel.getPlayPath32();
            soundinfo.playUrl64 = ranktrackmodel.getPlayPath64();
            soundinfo.playPathAacv224 = ranktrackmodel.getPlayPathAacv224();
            soundinfo.playPathAacv164 = ranktrackmodel.getPlayPathAacv164();
            soundinfo.trackId = ranktrackmodel.getTrackId();
            soundinfo.comments_counts = (int)ranktrackmodel.getCommentsCounts();
            soundinfo.plays_counts = (int)ranktrackmodel.getPlaysCounts();
            soundinfo.coverSmall = ranktrackmodel.getCoverSmall();
            soundinfo.shares_counts = (int)ranktrackmodel.getSharesCounts();
            soundinfo.favorites_counts = (int)ranktrackmodel.getLikes();
            soundinfo.title = ranktrackmodel.getTitle();
            soundinfo.albumName = ranktrackmodel.getAlbumTitle();
            soundinfo.duration = ranktrackmodel.getDuration();
            soundinfo.uid = ranktrackmodel.getUid();
            return soundinfo;
        }
    }

    public static SoundInfo toSoundInfo(RecmdItemModel recmditemmodel)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.playUrl32 = recmditemmodel.getPlayPath32();
        soundinfo.playUrl64 = recmditemmodel.getPlayPath64();
        soundinfo.playPathAacv224 = recmditemmodel.getPlayPathAacv224();
        soundinfo.playPathAacv164 = recmditemmodel.getPlayPathAacv164();
        String s;
        if (TextUtils.isEmpty(recmditemmodel.getCoverSmall()))
        {
            s = recmditemmodel.getCoverMiddle();
        } else
        {
            s = recmditemmodel.getCoverSmall();
        }
        soundinfo.coverSmall = s;
        soundinfo.coverLarge = recmditemmodel.getCoverLarge();
        soundinfo.albumId = recmditemmodel.getAlbumId();
        soundinfo.trackId = recmditemmodel.getTrackId();
        soundinfo.title = recmditemmodel.getTitle();
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(RadioSound radiosound)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.uid = radiosound.getFmuid();
        soundinfo.radioId = radiosound.getRadioId();
        soundinfo.radioName = radiosound.getRname();
        soundinfo.programScheduleId = radiosound.getProgramScheduleId();
        soundinfo.programId = radiosound.getProgramId();
        soundinfo.programName = radiosound.getProgramName();
        soundinfo.startTime = radiosound.getStartTime();
        soundinfo.endTime = radiosound.getEndTime();
        soundinfo.category = radiosound.getCategory();
        soundinfo.plays_counts = radiosound.getRadioPlayCount();
        soundinfo.coverSmall = radiosound.getRadioCoverSmall();
        soundinfo.coverLarge = radiosound.getRadioCoverLarge();
        soundinfo.listenBackUrl = radiosound.getListenBackUrl();
        soundinfo.announcerList = radiosound.getAnnouncerList();
        soundinfo.validDate = new Date();
        if (radiosound.getCategory() == 2)
        {
            soundinfo.liveUrl = radiosound.getLiveUrl();
            soundinfo.playUrl32 = radiosound.getListenBackUrl();
            soundinfo.playUrl64 = radiosound.getListenBackUrl();
            return soundinfo;
        }
        if (radiosound.getCategory() == 1)
        {
            if (radiosound.getRadioPlayUrl() != null)
            {
                if (TingMediaPlayer.getTingMediaPlayer(MyApplication.b()).isUseSystemPlayer())
                {
                    soundinfo.playUrl32 = radiosound.getRadioPlayUrl().getRadio_24_ts();
                    soundinfo.playUrl64 = radiosound.getRadioPlayUrl().getRadio_24_ts();
                } else
                {
                    soundinfo.playUrl32 = radiosound.getRadioPlayUrl().getRadio_24_aac();
                    soundinfo.playUrl64 = radiosound.getRadioPlayUrl().getRadio_24_aac();
                }
            }
            soundinfo.liveUrl = soundinfo.playUrl32;
            return soundinfo;
        } else
        {
            soundinfo.liveUrl = radiosound.getLiveUrl();
            soundinfo.playUrl32 = radiosound.getLiveUrl();
            soundinfo.playUrl64 = radiosound.getLiveUrl();
            return soundinfo;
        }
    }

    public static SoundInfo toSoundInfo(RecommendRadioListModel recommendradiolistmodel)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.radioId = recommendradiolistmodel.getRadioId();
        soundinfo.radioName = recommendradiolistmodel.getRname();
        soundinfo.category = 1;
        soundinfo.coverSmall = recommendradiolistmodel.getPicPath();
        soundinfo.validDate = new Date();
        if (recommendradiolistmodel.getRadioPlayUrl() != null)
        {
            if (TingMediaPlayer.getTingMediaPlayer(MyApplication.b()).isUseSystemPlayer())
            {
                soundinfo.playUrl32 = recommendradiolistmodel.getRadioPlayUrl().getRadio_24_ts();
                soundinfo.playUrl64 = recommendradiolistmodel.getRadioPlayUrl().getRadio_24_ts();
            } else
            {
                soundinfo.playUrl32 = recommendradiolistmodel.getRadioPlayUrl().getRadio_24_aac();
                soundinfo.playUrl64 = recommendradiolistmodel.getRadioPlayUrl().getRadio_24_aac();
            }
        }
        soundinfo.plays_counts = recommendradiolistmodel.getRadioPlayCount();
        soundinfo.liveUrl = soundinfo.playUrl32;
        return soundinfo;
    }

    public static final SoundInfo toSoundInfo(SoundInCommentModel soundincommentmodel)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.trackId = soundincommentmodel.trackId.longValue();
        soundinfo.title = soundincommentmodel.trackTitle;
        soundinfo.uid = soundincommentmodel.trackUid.longValue();
        soundinfo.coverSmall = soundincommentmodel.trackCoverPath;
        soundinfo.nickname = soundincommentmodel.trackNickname;
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(SearchSound searchsound)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.trackId = searchsound.id;
        soundinfo.title = searchsound.title;
        soundinfo.playUrl32 = searchsound.play_path_32;
        soundinfo.playUrl64 = searchsound.play_path_64;
        soundinfo.duration = (long)searchsound.duration;
        soundinfo.create_at = searchsound.created_at;
        soundinfo.plays_counts = searchsound.count_play;
        soundinfo.favorites_counts = searchsound.count_like;
        soundinfo.nickname = searchsound.nickname;
        soundinfo.albumId = searchsound.album_id;
        soundinfo.albumName = searchsound.album_title;
        soundinfo.uid = searchsound.uid;
        soundinfo.comments_counts = searchsound.count_comment;
        soundinfo.coverSmall = searchsound.smallLogo;
        soundinfo.albumCoverPath = searchsound.album_cover_path;
        soundinfo.userCoverPath = searchsound.avatar_path;
        soundinfo.is_playing = searchsound.is_playing;
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(SearchSoundNew searchsoundnew)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.trackId = searchsoundnew.id;
        soundinfo.title = searchsoundnew.title;
        soundinfo.playUrl32 = searchsoundnew.play_url_32;
        soundinfo.playUrl64 = searchsoundnew.play_url_64;
        soundinfo.duration = (long)searchsoundnew.duration;
        soundinfo.plays_counts = searchsoundnew.plays_count;
        soundinfo.favorites_counts = searchsoundnew.favorites_count;
        soundinfo.nickname = searchsoundnew.nickname;
        soundinfo.albumId = searchsoundnew.album_id;
        soundinfo.albumName = searchsoundnew.album_title;
        soundinfo.uid = searchsoundnew.uid;
        soundinfo.comments_counts = searchsoundnew.comments_count;
        soundinfo.coverSmall = searchsoundnew.cover_url_small;
        soundinfo.userCoverPath = searchsoundnew.avatar_url;
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(AlbumSoundModel albumsoundmodel)
    {
        return toSoundInfo(albumsoundmodel, null, null);
    }

    public static SoundInfo toSoundInfo(AlbumSoundModel albumsoundmodel, String s, String s1)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.trackId = albumsoundmodel.trackId;
        soundinfo.title = albumsoundmodel.title;
        soundinfo.playUrl32 = albumsoundmodel.playUrl32;
        soundinfo.playUrl64 = albumsoundmodel.playUrl64;
        soundinfo.playPathAacv224 = albumsoundmodel.playPathAacv224;
        soundinfo.playPathAacv164 = albumsoundmodel.playPathAacv164;
        soundinfo.coverSmall = albumsoundmodel.coverSmall;
        soundinfo.coverLarge = albumsoundmodel.coverLarge;
        soundinfo.filesize = 0L;
        soundinfo.duration = albumsoundmodel.duration;
        soundinfo.create_at = albumsoundmodel.createdAt;
        soundinfo.orderNum = 0L;
        soundinfo.uid = albumsoundmodel.uid;
        soundinfo.nickname = albumsoundmodel.nickname;
        soundinfo.userCoverPath = albumsoundmodel.smallLogo;
        soundinfo.albumId = albumsoundmodel.albumId;
        soundinfo.albumName = albumsoundmodel.albumTitle;
        soundinfo.albumCoverPath = albumsoundmodel.albumImage;
        soundinfo.plays_counts = albumsoundmodel.playtimes;
        soundinfo.favorites_counts = albumsoundmodel.likes;
        soundinfo.is_favorited = albumsoundmodel.isLike;
        soundinfo.isRelay = albumsoundmodel.isRelay;
        soundinfo.comments_counts = albumsoundmodel.comments;
        soundinfo.shares_counts = albumsoundmodel.shares;
        soundinfo.user_source = albumsoundmodel.userSource;
        soundinfo.is_playing = albumsoundmodel.is_playing;
        soundinfo.recSrc = s;
        soundinfo.recTrack = s1;
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(AlbumSoundModelNew albumsoundmodelnew, String s, String s1)
    {
        s = new SoundInfo();
        s.trackId = albumsoundmodelnew.id;
        s.title = albumsoundmodelnew.title;
        s.playUrl32 = albumsoundmodelnew.play_url_32;
        s.playUrl64 = albumsoundmodelnew.play_url_64;
        s.coverSmall = albumsoundmodelnew.cover_url_small;
        s.coverLarge = albumsoundmodelnew.cover_url_large;
        s.filesize = 0L;
        s.duration = albumsoundmodelnew.duration;
        albumsoundmodelnew.created_at = albumsoundmodelnew.created_at.replaceAll("T", " ");
        albumsoundmodelnew.created_at = albumsoundmodelnew.created_at.replaceAll("Z", " ");
        s1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            s1.parse(albumsoundmodelnew.created_at).getTime();
        }
        // Misplaced declaration of an exception variable
        catch (String s1)
        {
            s1.printStackTrace();
        }
        s.create_at = 0L;
        s.orderNum = 0L;
        s.uid = albumsoundmodelnew.uid;
        s.nickname = albumsoundmodelnew.nickname;
        s.userCoverPath = "";
        s.plays_counts = albumsoundmodelnew.plays_count;
        s.comments_counts = albumsoundmodelnew.comments_count;
        return s;
    }

    public static final SoundInfo toSoundInfo(HotSoundModel hotsoundmodel)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.trackId = hotsoundmodel.id;
        soundinfo.title = hotsoundmodel.title;
        soundinfo.playUrl32 = hotsoundmodel.play_path_32;
        soundinfo.playUrl64 = hotsoundmodel.play_path_64;
        soundinfo.duration = (long)hotsoundmodel.duration;
        soundinfo.create_at = hotsoundmodel.createdAt;
        soundinfo.plays_counts = hotsoundmodel.plays_counts;
        soundinfo.favorites_counts = hotsoundmodel.favorites_counts;
        soundinfo.nickname = hotsoundmodel.nickname;
        soundinfo.orderNum = hotsoundmodel.order_num;
        soundinfo.albumId = hotsoundmodel.album_id;
        soundinfo.albumName = hotsoundmodel.album_title;
        soundinfo.uid = hotsoundmodel.uid;
        soundinfo.comments_counts = hotsoundmodel.comments_counts;
        soundinfo.coverSmall = hotsoundmodel.coverSmall;
        soundinfo.albumCoverPath = hotsoundmodel.album_cover_path_86;
        soundinfo.userCoverPath = hotsoundmodel.avatar_path;
        soundinfo.is_playing = hotsoundmodel.is_playing;
        soundinfo.is_favorited = hotsoundmodel.is_favorited;
        soundinfo.isRelay = hotsoundmodel.isRelay;
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(SoundInfoDetail soundinfodetail)
    {
        SoundInfo soundinfo;
        if (soundinfodetail == null)
        {
            soundinfo = null;
        } else
        {
            SoundInfo soundinfo1 = new SoundInfo();
            soundinfo1.trackId = soundinfodetail.trackId;
            soundinfo1.title = soundinfodetail.title;
            soundinfo1.playUrl32 = soundinfodetail.playUrl32;
            soundinfo1.playUrl64 = soundinfodetail.playUrl64;
            soundinfo1.playPathAacv224 = soundinfodetail.playPathAacv224;
            soundinfo1.playPathAacv164 = soundinfodetail.playPathAacv164;
            soundinfo1.coverSmall = soundinfodetail.coverSmall;
            soundinfo1.coverLarge = soundinfodetail.coverLarge;
            soundinfo1.duration = soundinfodetail.duration;
            soundinfo1.create_at = soundinfodetail.createdAt;
            soundinfo1.uid = soundinfodetail.uid;
            soundinfo1.albumId = soundinfodetail.albumId;
            soundinfo1.albumCoverPath = soundinfodetail.albumImage;
            soundinfo1.plays_counts = soundinfodetail.playtimes;
            soundinfo1.favorites_counts = soundinfodetail.likes;
            soundinfo1.is_favorited = soundinfodetail.isLike;
            soundinfo1.isRelay = soundinfodetail.isRelay;
            soundinfo1.comments_counts = soundinfodetail.comments;
            soundinfo1.shares_counts = soundinfodetail.shares;
            soundinfo1.user_source = soundinfodetail.userSource;
            soundinfo = soundinfo1;
            if (soundinfodetail.userInfo != null)
            {
                soundinfo1.nickname = soundinfodetail.userInfo.nickname;
                soundinfo1.userCoverPath = soundinfodetail.userInfo.smallLogo;
                return soundinfo1;
            }
        }
        return soundinfo;
    }

    public static SoundInfo toSoundInfo(SoundInfoNew soundinfonew)
    {
        if (soundinfonew == null)
        {
            return null;
        }
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.trackId = soundinfonew.id;
        soundinfo.title = soundinfonew.title;
        soundinfo.uid = soundinfonew.uid;
        soundinfo.nickname = soundinfonew.nickname;
        soundinfo.duration = soundinfonew.duration;
        soundinfo.plays_counts = soundinfonew.playsCounts;
        soundinfo.comments_counts = soundinfonew.commentsCounts;
        soundinfo.shares_counts = soundinfonew.sharesCounts;
        soundinfo.favorites_counts = soundinfonew.favoritesCounts;
        soundinfo.playUrl32 = soundinfonew.playPath32;
        soundinfo.playUrl64 = soundinfonew.playPath64;
        soundinfo.playPathAacv224 = soundinfonew.playPathAacv224;
        soundinfo.playPathAacv164 = soundinfonew.playPathAacv164;
        soundinfo.coverSmall = soundinfonew.coverSmall;
        if (TextUtils.isEmpty(soundinfo.coverSmall))
        {
            soundinfo.coverSmall = soundinfonew.coverWebSmall;
        }
        soundinfo.user_source = soundinfonew.userSource;
        soundinfo.create_at = soundinfonew.createdAt;
        soundinfo.is_favorited = soundinfonew.isFavorite;
        soundinfo.isRelay = soundinfonew.isRelay;
        return soundinfo;
    }

    public static final SoundInfo toSoundInfo(SoundModel soundmodel)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.trackId = soundmodel.trackId;
        soundinfo.title = soundmodel.title;
        soundinfo.playUrl32 = soundmodel.playUrl32;
        soundinfo.playUrl64 = soundmodel.playUrl64;
        soundinfo.playPathAacv224 = soundmodel.playPathAacv224;
        soundinfo.playPathAacv164 = soundmodel.playPathAacv164;
        soundinfo.duration = soundmodel.duration;
        soundinfo.create_at = soundmodel.createdAt;
        soundinfo.plays_counts = soundmodel.playtimes;
        soundinfo.favorites_counts = soundmodel.likes;
        soundinfo.nickname = soundmodel.nickname;
        soundinfo.orderNum = soundmodel.orderNum;
        soundinfo.albumId = soundmodel.albumId;
        soundinfo.albumName = soundmodel.albumTitle;
        soundinfo.uid = soundmodel.uid;
        soundinfo.comments_counts = soundmodel.comments;
        soundinfo.is_playing = soundmodel.isPlaying;
        soundinfo.is_favorited = soundmodel.isLike;
        soundinfo.isRelay = soundmodel.isRelay;
        soundinfo.coverSmall = soundmodel.coverSmall;
        soundinfo.coverLarge = soundmodel.coverLarge;
        return soundinfo;
    }

    public static List toSoundInfo(List list)
    {
        ArrayList arraylist = new ArrayList();
        if (list == null)
        {
            return null;
        }
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((SoundInfoNew)list.next()))) { }
        return arraylist;
    }

    public static List toSoundInfoFromSearch(List list)
    {
        if (list == null)
        {
            return null;
        }
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((SearchSound)list.next()))) { }
        return arraylist;
    }

    public static List toSoundInfoList(List list)
    {
        ArrayList arraylist = new ArrayList(list.size());
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((FeedSoundInfo)list.next()))) { }
        return arraylist;
    }

    public static List toSoundInfoListForCategoryTrack(List list)
    {
        ArrayList arraylist = new ArrayList();
        if (list == null)
        {
            return arraylist;
        }
        list = list.iterator();
        do
        {
            if (!list.hasNext())
            {
                break;
            }
            CategoryTrackModel categorytrackmodel = (CategoryTrackModel)list.next();
            if (categorytrackmodel != null)
            {
                arraylist.add(toSoundInfo(categorytrackmodel));
            }
        } while (true);
        return arraylist;
    }

    public static List toSoundInfoListForRank(List list)
    {
        ArrayList arraylist = new ArrayList();
        if (list == null)
        {
            return arraylist;
        }
        list = list.iterator();
        do
        {
            if (!list.hasNext())
            {
                break;
            }
            RankTrackModel ranktrackmodel = (RankTrackModel)list.next();
            if (ranktrackmodel != null)
            {
                arraylist.add(toSoundInfo(ranktrackmodel));
            }
        } while (true);
        return arraylist;
    }

    public static final List toSoundInfoforAlbum(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((AlbumSoundModel)list.next()))) { }
        return arraylist;
    }

    public static List toSoundInfolist(List list)
    {
        ArrayList arraylist = new ArrayList();
        for (list = list.iterator(); list.hasNext(); arraylist.add(toSoundInfo((RadioSound)list.next()))) { }
        return arraylist;
    }

    public static Track toTrack(SoundInfo soundinfo)
    {
        Track track = new Track();
        track.setCoverLarge(soundinfo.coverLarge);
        track.setTitle(soundinfo.title);
        track.setPlayPath32(soundinfo.playUrl32);
        track.setPlayPath64(soundinfo.playUrl64);
        track.setPlayPathAacv224(soundinfo.playPathAacv224);
        track.setPlayPathAacv164(soundinfo.playPathAacv164);
        track.setTrackId(Long.valueOf(soundinfo.trackId));
        track.setUid(Long.valueOf(soundinfo.uid));
        track.setDuration(Long.valueOf(Math.round(soundinfo.duration)));
        track.setAnchorType(soundinfo.anchorType);
        return track;
    }

    public static List toTracks(List list)
    {
        ArrayList arraylist = new ArrayList(list.size());
        for (list = list.iterator(); list.hasNext(); arraylist.add(toTrack((SoundInfo)list.next()))) { }
        return arraylist;
    }

    public static SoundInfo trackToSoundInfo(Track track)
    {
        SoundInfo soundinfo = new SoundInfo();
        soundinfo.albumCoverPath = track.getCoverLarge();
        soundinfo.coverLarge = track.getCoverLarge();
        soundinfo.coverSmall = track.getCoverLarge();
        soundinfo.title = track.getTitle();
        soundinfo.playUrl32 = track.getPlayPath32();
        soundinfo.playUrl64 = track.getPlayPath64();
        soundinfo.playPathAacv224 = track.getPlayPathAacv224();
        soundinfo.playPathAacv164 = track.getPlayPathAacv164();
        soundinfo.trackId = track.getTrackId().longValue();
        soundinfo.uid = track.getUid().longValue();
        soundinfo.duration = track.getDuration().longValue();
        soundinfo.anchorType = track.getAnchorType();
        return soundinfo;
    }

    public static List tracksToSoundInfos(List list)
    {
        ArrayList arraylist = new ArrayList(list.size());
        for (list = list.iterator(); list.hasNext(); arraylist.add(trackToSoundInfo((Track)list.next()))) { }
        return arraylist;
    }

    public static void updateSoundInfo(SoundInfo soundinfo, SoundInfo soundinfo1)
    {
        if (soundinfo == null || soundinfo1 == null || soundinfo.trackId != soundinfo1.trackId)
        {
            return;
        } else
        {
            soundinfo.title = soundinfo1.title;
            soundinfo.playUrl32 = soundinfo1.playUrl32;
            soundinfo.playUrl64 = soundinfo1.playUrl64;
            soundinfo.playPathAacv224 = soundinfo1.playPathAacv224;
            soundinfo.playPathAacv164 = soundinfo1.playPathAacv164;
            soundinfo.coverSmall = soundinfo1.coverSmall;
            soundinfo.coverLarge = soundinfo1.coverLarge;
            soundinfo.filesize = soundinfo1.filesize;
            soundinfo.duration = soundinfo1.duration;
            soundinfo.create_at = soundinfo1.create_at;
            soundinfo.status = soundinfo1.status;
            soundinfo.history_listener = soundinfo1.history_listener;
            soundinfo.history_duration = soundinfo1.history_duration;
            soundinfo.uid = soundinfo1.uid;
            soundinfo.nickname = soundinfo1.nickname;
            soundinfo.userCoverPath = soundinfo1.userCoverPath;
            soundinfo.albumId = soundinfo1.albumId;
            soundinfo.albumName = soundinfo1.albumName;
            soundinfo.albumCoverPath = soundinfo1.albumCoverPath;
            soundinfo.plays_counts = soundinfo1.plays_counts;
            soundinfo.favorites_counts = soundinfo1.favorites_counts;
            soundinfo.is_favorited = soundinfo1.is_favorited;
            soundinfo.comments_counts = soundinfo1.comments_counts;
            soundinfo.shares_counts = soundinfo1.shares_counts;
            soundinfo.user_source = soundinfo1.user_source;
            soundinfo.downLoadUrl = soundinfo1.downLoadUrl;
            soundinfo.is_playing = soundinfo1.is_playing;
            soundinfo.isPublic = soundinfo1.isPublic;
            soundinfo.anchorType = soundinfo1.anchorType;
            return;
        }
    }

    public static void updateSoundInfo(SoundInfoDetail soundinfodetail, SoundInfo soundinfo)
    {
        if (soundinfo != null && soundinfodetail != null && soundinfo.trackId == soundinfodetail.trackId)
        {
            soundinfo.uid = soundinfodetail.uid;
            if (!Utilities.isBlank(soundinfodetail.title) && !soundinfodetail.title.equals(soundinfo.title))
            {
                soundinfo.title = soundinfodetail.title;
            }
            if (!Utilities.isBlank(soundinfodetail.playUrl32) && !soundinfodetail.playUrl32.equals(soundinfo.playUrl32))
            {
                soundinfo.playUrl32 = soundinfodetail.playUrl32;
            }
            if (!Utilities.isBlank(soundinfodetail.playUrl64) && !soundinfodetail.playUrl64.equals(soundinfo.playUrl64))
            {
                soundinfo.playUrl64 = soundinfodetail.playUrl64;
            }
            if (!Utilities.isBlank(soundinfodetail.playPathAacv224))
            {
                soundinfo.playPathAacv224 = soundinfodetail.playPathAacv224;
            }
            if (!Utilities.isBlank(soundinfodetail.playPathAacv164))
            {
                soundinfo.playPathAacv164 = soundinfodetail.playPathAacv164;
            }
            if (!Utilities.isBlank(soundinfodetail.coverSmall) && !soundinfodetail.coverSmall.equals(soundinfo.coverSmall))
            {
                soundinfo.coverSmall = soundinfodetail.coverSmall;
            }
            if (!Utilities.isBlank(soundinfodetail.coverLarge) && !soundinfodetail.coverLarge.equals(soundinfo.coverLarge))
            {
                soundinfo.coverLarge = soundinfodetail.coverLarge;
            }
            if (soundinfodetail.duration != 0.0D && soundinfodetail.duration != soundinfo.duration)
            {
                soundinfo.duration = soundinfodetail.duration;
            }
            if (soundinfodetail.createdAt != 0L && soundinfodetail.createdAt != soundinfo.create_at)
            {
                soundinfo.create_at = soundinfodetail.createdAt;
            }
            if (soundinfodetail.playtimes != 0 && soundinfodetail.playtimes != soundinfo.plays_counts)
            {
                soundinfo.plays_counts = soundinfodetail.playtimes;
            }
            if (soundinfodetail.likes != 0 && soundinfodetail.likes != soundinfo.favorites_counts)
            {
                soundinfo.favorites_counts = soundinfodetail.likes;
            }
            if (soundinfodetail.comments != 0 && soundinfodetail.comments != soundinfo.comments_counts)
            {
                soundinfo.comments_counts = soundinfodetail.comments;
            }
            if (soundinfodetail.shares != 0 && soundinfodetail.shares != soundinfo.shares_counts)
            {
                soundinfo.shares_counts = soundinfodetail.shares;
            }
            if (soundinfodetail.isLike != soundinfo.is_favorited)
            {
                soundinfo.is_favorited = soundinfodetail.isLike;
            }
            if (soundinfodetail.isRelay != soundinfo.isRelay)
            {
                soundinfo.isRelay = soundinfodetail.isRelay;
            }
            if (soundinfodetail.userSource != 0 && soundinfodetail.userSource != soundinfo.user_source)
            {
                soundinfo.user_source = soundinfodetail.userSource;
            }
            if (!Utilities.isBlank(soundinfodetail.albumImage) && !soundinfodetail.albumImage.equals(soundinfo.albumCoverPath))
            {
                soundinfo.albumCoverPath = soundinfodetail.albumImage;
            }
            if (soundinfodetail.albumId != 0L)
            {
                soundinfo.albumId = soundinfodetail.albumId;
            }
            if (!Utilities.isBlank(soundinfodetail.title) && !soundinfodetail.title.equals(soundinfo.title))
            {
                soundinfo.title = soundinfodetail.title;
            }
            if (soundinfodetail.userInfo != null)
            {
                soundinfo.nickname = soundinfodetail.userInfo.nickname;
                soundinfo.userCoverPath = soundinfodetail.userInfo.smallLogo;
            }
            if (!TextUtils.isEmpty(soundinfodetail.albumTitle))
            {
                soundinfo.albumName = soundinfodetail.albumTitle;
                return;
            }
        }
    }
}
