// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.ximalaya.ting.android.util;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import com.alibaba.fastjson.JSON;
import com.ximalaya.ting.android.library.util.SharedPreferencesUtil;
import com.ximalaya.ting.android.model.ad.AdCollectData;
import com.ximalaya.ting.android.model.livefm.MyLogger;
import com.ximalaya.ting.android.model.xdcs.BaseEvent;
import com.ximalaya.ting.android.model.xdcs.CdnCollectData;
import com.ximalaya.ting.android.model.xdcs.HttpHeaderModel;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.GZIPOutputStream;
import org.apache.http.Header;

// Referenced classes of package com.ximalaya.ting.android.util:
//            Utilities, i, k, m, 
//            j, l, g, h

public class DataCollectUtil
{
    public static class EventRecord
    {

        public List events;

        public EventRecord()
        {
        }
    }


    public static final String APP_NAME_DASHANG = "shang";
    public static final String APP_NAME_H5_JUMP = "h5_jump";
    public static final String APP_NAME_ITING = "iting";
    public static final String SERVICE_COOL_RING = "cool_ring";
    public static final String SERVICE_DASHANG_CLICK = "click_dashang";
    public static final String SERVICE_DASHANG_SHOW = "show_dashang";
    public static final String SERVICE_FOCUS = "focus";
    public static final String SERVICE_ITING = "iting";
    public static final String SERVICE_NEW_LIFE_SHOP = "new_life_shop";
    private static DataCollectUtil instance;
    private Context appContext;
    private List events;
    private ExecutorService pool;
    private int seqId;

    private DataCollectUtil(Context context)
    {
        seqId = 0;
        events = new ArrayList();
        pool = Executors.newFixedThreadPool(5);
    }

    public static void bindDataToView(String s, View view)
    {
        if (TextUtils.isEmpty(s) || view == null)
        {
            return;
        } else
        {
            view.setTag(0x7f0a0045, s);
            return;
        }
    }

    public static void bindDataToView(Header aheader[], View view)
    {
        if (view == null || aheader == null)
        {
            return;
        }
        HttpHeaderModel httpheadermodel = new HttpHeaderModel();
        int j1 = aheader.length;
        int i1 = 0;
        while (i1 < j1) 
        {
            Header header = aheader[i1];
            if (header.getName().equals("x-tId"))
            {
                httpheadermodel.setTraceId(header.getValue());
            } else
            if (header.getName().equals("x-sId"))
            {
                httpheadermodel.setSpanId(header.getValue());
            } else
            if (header.getName().equals("x-pId"))
            {
                httpheadermodel.setParentId(header.getValue());
            }
            i1++;
        }
        try
        {
            view.setTag(0x7f0a0045, JSON.toJSONString(httpheadermodel));
            MyLogger.getLogger().d((new StringBuilder()).append("xdcs data=").append(httpheadermodel).toString());
            return;
        }
        // Misplaced declaration of an exception variable
        catch (Header aheader[])
        {
            return;
        }
    }

    public static void bindViewIdDataToView(View view)
    {
        if (view == null)
        {
            return;
        }
        Object obj = new HttpHeaderModel();
        ((HttpHeaderModel) (obj)).setViewId((new StringBuilder()).append(view.getId()).append("").toString());
        obj = JSON.toJSONString(obj);
        MyLogger.getLogger().d((new StringBuilder()).append("xdcs data=").append(((String) (obj))).toString());
        try
        {
            view.setTag(0x7f0a0045, obj);
            return;
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return;
        }
    }

    public static void compress(InputStream inputstream, OutputStream outputstream)
        throws Exception
    {
        byte abyte0[];
        outputstream = new GZIPOutputStream(outputstream);
        abyte0 = new byte[2048];
_L1:
        int i1 = inputstream.read(abyte0, 0, 2048);
        if (i1 == -1)
        {
            break MISSING_BLOCK_LABEL_47;
        }
        outputstream.write(abyte0, 0, i1);
          goto _L1
        inputstream;
        outputstream.close();
        throw inputstream;
        outputstream.close();
        return;
    }

    public static byte[] compress(byte abyte0[])
        throws Exception
    {
        ByteArrayInputStream bytearrayinputstream;
        bytearrayinputstream = new ByteArrayInputStream(abyte0);
        abyte0 = new ByteArrayOutputStream();
        byte abyte1[];
        compress(((InputStream) (bytearrayinputstream)), ((OutputStream) (abyte0)));
        abyte1 = abyte0.toByteArray();
        abyte0.close();
        return abyte1;
        Exception exception;
        exception;
        abyte0.close();
        throw exception;
    }

    public static String getDataFromView(View view)
    {
        Object obj = view;
_L2:
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        Object obj1;
        HttpHeaderModel httpheadermodel;
        try
        {
            obj1 = ((View) (obj)).getTag(0x7f0a0045);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return null;
        }
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_119;
        }
        obj1 = (String)obj1;
        obj = obj1;
        try
        {
            httpheadermodel = (HttpHeaderModel)JSON.parseObject(((String) (obj1)), com/ximalaya/ting/android/model/xdcs/HttpHeaderModel);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return ((String) (obj));
        }
        if (httpheadermodel == null)
        {
            return ((String) (obj1));
        }
        obj = obj1;
        if (!TextUtils.isEmpty(httpheadermodel.getViewId()))
        {
            break MISSING_BLOCK_LABEL_80;
        }
        obj = obj1;
        httpheadermodel.setViewId((new StringBuilder()).append(view.getId()).append("").toString());
        obj = obj1;
        view = JSON.toJSONString(httpheadermodel);
        obj = view;
        MyLogger.getLogger().d((new StringBuilder()).append("xdcs data =").append(view).toString());
        return view;
        obj = ((View) (obj)).getParent();
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!(obj instanceof ViewGroup))
        {
            return null;
        }
        obj = (ViewGroup)obj;
        if (true) goto _L2; else goto _L1
_L1:
        return null;
    }

    public static String getDataFromView(View view, int i1, String s)
    {
        Object obj = view;
_L2:
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        Object obj1;
        HttpHeaderModel httpheadermodel;
        try
        {
            obj1 = ((View) (obj)).getTag(0x7f0a0045);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return null;
        }
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_152;
        }
        obj1 = (String)obj1;
        obj = obj1;
        try
        {
            httpheadermodel = (HttpHeaderModel)JSON.parseObject(((String) (obj1)), com/ximalaya/ting/android/model/xdcs/HttpHeaderModel);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return ((String) (obj));
        }
        if (httpheadermodel == null)
        {
            return ((String) (obj1));
        }
        obj = obj1;
        if (!TextUtils.isEmpty(httpheadermodel.getViewId()))
        {
            break MISSING_BLOCK_LABEL_93;
        }
        obj = obj1;
        httpheadermodel.setViewId((new StringBuilder()).append(view.getId()).append("").toString());
        obj = obj1;
        httpheadermodel.setSearchpage(i1);
        obj = obj1;
        httpheadermodel.setPosition(s);
        obj = obj1;
        view = JSON.toJSONString(httpheadermodel);
        obj = view;
        MyLogger.getLogger().d((new StringBuilder()).append("xdcs data =").append(view).toString());
        return view;
        obj = ((View) (obj)).getParent();
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!(obj instanceof ViewGroup))
        {
            return null;
        }
        obj = (ViewGroup)obj;
        if (true) goto _L2; else goto _L1
_L1:
        return null;
    }

    public static String getDataFromView(View view, String s)
    {
        Object obj = view;
_L2:
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        Object obj1;
        HttpHeaderModel httpheadermodel;
        try
        {
            obj1 = ((View) (obj)).getTag(0x7f0a0045);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return null;
        }
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_132;
        }
        obj1 = (String)obj1;
        obj = obj1;
        try
        {
            httpheadermodel = (HttpHeaderModel)JSON.parseObject(((String) (obj1)), com/ximalaya/ting/android/model/xdcs/HttpHeaderModel);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return ((String) (obj));
        }
        if (httpheadermodel == null)
        {
            return ((String) (obj1));
        }
        obj = obj1;
        if (!TextUtils.isEmpty(httpheadermodel.getViewId()))
        {
            break MISSING_BLOCK_LABEL_84;
        }
        obj = obj1;
        httpheadermodel.setViewId((new StringBuilder()).append(view.getId()).append("").toString());
        obj = obj1;
        httpheadermodel.setTitle(s);
        obj = obj1;
        view = JSON.toJSONString(httpheadermodel);
        obj = view;
        MyLogger.getLogger().d((new StringBuilder()).append("xdcs data =").append(view).toString());
        return view;
        obj = ((View) (obj)).getParent();
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!(obj instanceof ViewGroup))
        {
            return null;
        }
        obj = (ViewGroup)obj;
        if (true) goto _L2; else goto _L1
_L1:
        return null;
    }

    public static String getDataFromView(View view, String s, String s1)
    {
        Object obj = view;
_L2:
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        Object obj1;
        HttpHeaderModel httpheadermodel;
        try
        {
            obj1 = ((View) (obj)).getTag(0x7f0a0045);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return null;
        }
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_152;
        }
        obj1 = (String)obj1;
        obj = obj1;
        try
        {
            httpheadermodel = (HttpHeaderModel)JSON.parseObject(((String) (obj1)), com/ximalaya/ting/android/model/xdcs/HttpHeaderModel);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return ((String) (obj));
        }
        if (httpheadermodel == null)
        {
            return ((String) (obj1));
        }
        obj = obj1;
        if (!TextUtils.isEmpty(httpheadermodel.getViewId()))
        {
            break MISSING_BLOCK_LABEL_93;
        }
        obj = obj1;
        httpheadermodel.setViewId((new StringBuilder()).append(view.getId()).append("").toString());
        obj = obj1;
        httpheadermodel.setTitle(s);
        obj = obj1;
        httpheadermodel.setPosition(s1);
        obj = obj1;
        view = JSON.toJSONString(httpheadermodel);
        obj = view;
        MyLogger.getLogger().d((new StringBuilder()).append("xdcs data =").append(view).toString());
        return view;
        obj = ((View) (obj)).getParent();
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!(obj instanceof ViewGroup))
        {
            return null;
        }
        obj = (ViewGroup)obj;
        if (true) goto _L2; else goto _L1
_L1:
        return null;
    }

    public static String getDataFromView(View view, String s, String s1, String s2, String s3)
    {
        Object obj = view;
_L2:
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        Object obj1;
        HttpHeaderModel httpheadermodel;
        try
        {
            obj1 = ((View) (obj)).getTag(0x7f0a0045);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return null;
        }
        if (obj1 == null)
        {
            break MISSING_BLOCK_LABEL_184;
        }
        obj1 = (String)obj1;
        obj = obj1;
        try
        {
            httpheadermodel = (HttpHeaderModel)JSON.parseObject(((String) (obj1)), com/ximalaya/ting/android/model/xdcs/HttpHeaderModel);
        }
        // Misplaced declaration of an exception variable
        catch (View view)
        {
            return ((String) (obj));
        }
        if (httpheadermodel == null)
        {
            return ((String) (obj1));
        }
        obj = obj1;
        if (!TextUtils.isEmpty(httpheadermodel.getViewId()))
        {
            break MISSING_BLOCK_LABEL_99;
        }
        obj = obj1;
        httpheadermodel.setViewId((new StringBuilder()).append(view.getId()).append("").toString());
        obj = obj1;
        httpheadermodel.setTitle(s3);
        obj = obj1;
        httpheadermodel.setPosition(s1);
        obj = obj1;
        httpheadermodel.setId(s2);
        obj = obj1;
        httpheadermodel.setFrom(s);
        obj = obj1;
        view = JSON.toJSONString(httpheadermodel);
        obj = view;
        MyLogger.getLogger().d((new StringBuilder()).append("xdcs data =").append(view).toString());
        return view;
        obj = ((View) (obj)).getParent();
        if (obj == null)
        {
            break; /* Loop/switch isn't completed */
        }
        if (!(obj instanceof ViewGroup))
        {
            return null;
        }
        obj = (ViewGroup)obj;
        if (true) goto _L2; else goto _L1
_L1:
        return null;
    }

    public static DataCollectUtil getInstance(Context context)
    {
        if (instance != null) goto _L2; else goto _L1
_L1:
        com/ximalaya/ting/android/util/DataCollectUtil;
        JVM INSTR monitorenter ;
        if (instance == null)
        {
            instance = new DataCollectUtil(context);
        }
        com/ximalaya/ting/android/util/DataCollectUtil;
        JVM INSTR monitorexit ;
_L2:
        return instance;
        context;
        com/ximalaya/ting/android/util/DataCollectUtil;
        JVM INSTR monitorexit ;
        throw context;
    }

    private void saveSequenceId()
    {
        if (seqId <= 0)
        {
            return;
        } else
        {
            SharedPreferencesUtil.getInstance(appContext).saveInt("xdcs_seqid", seqId);
            return;
        }
    }

    public void destroy()
    {
        saveEvents();
        pool.shutdownNow();
        instance = null;
    }

    public void init()
    {
        readEventsFromPrefs();
    }

    public void produceEvent(BaseEvent baseevent)
    {
        this;
        JVM INSTR monitorenter ;
        if (seqId <= 0)
        {
            seqId = SharedPreferencesUtil.getInstance(appContext).getInt("xdcs_seqid", 0);
        }
        seqId = seqId + 1;
        baseevent.setSeqId(seqId);
        events.add(baseevent);
        MyLogger.getLogger().d((new StringBuilder()).append("event=").append(baseevent).toString());
        this;
        JVM INSTR monitorexit ;
        return;
        baseevent;
        throw baseevent;
    }

    public boolean readEventsFromPrefs()
    {
        if (events == null || events.size() <= 0) goto _L2; else goto _L1
_L1:
        return false;
_L2:
        Object obj;
        obj = SharedPreferencesUtil.getInstance(appContext).getString("xdcs_event_record");
        MyLogger.getLogger().d((new StringBuilder()).append("eventsRecord=").append(((String) (obj))).toString());
        if (!Utilities.isNotBlank(((String) (obj)))) goto _L1; else goto _L3
_L3:
        boolean flag;
        try
        {
            obj = (EventRecord)JSON.parseObject(((String) (obj)), com/ximalaya/ting/android/util/DataCollectUtil$EventRecord);
        }
        catch (Exception exception)
        {
            MyLogger.getLogger().d((new StringBuilder()).append("exception=").append(exception.getMessage()).toString());
            return false;
        }
        if (obj == null) goto _L5; else goto _L4
_L4:
        events = ((EventRecord) (obj)).events;
        flag = true;
_L7:
        return flag;
_L5:
        flag = false;
        if (true) goto _L7; else goto _L6
_L6:
    }

    public void saveEvents()
    {
        this;
        JVM INSTR monitorenter ;
        SharedPreferencesUtil sharedpreferencesutil = SharedPreferencesUtil.getInstance(appContext);
        if (events != null && events.size() != 0) goto _L2; else goto _L1
_L1:
        sharedpreferencesutil.removeByKey("xdcs_event_record");
_L3:
        saveSequenceId();
_L4:
        this;
        JVM INSTR monitorexit ;
        return;
_L2:
        Object obj1 = new EventRecord();
        obj1.events = events;
        obj1 = JSON.toJSONString(obj1);
        MyLogger.getLogger().d((new StringBuilder()).append("saveEvents=").append(((String) (obj1))).toString());
        sharedpreferencesutil.saveString("xdcs_event_record", ((String) (obj1)));
          goto _L3
        Object obj;
        obj;
          goto _L4
        obj;
        throw obj;
          goto _L3
    }

    public void statDashang(String s, long l1, long l2, long l3)
    {
        pool.submit(new i(this, s, l1, l2, l3));
    }

    public void statDownloadCDN(CdnCollectData cdncollectdata)
    {
        if (cdncollectdata == null)
        {
            return;
        } else
        {
            pool.submit(new k(this, cdncollectdata));
            return;
        }
    }

    public void statFocus(String s, String s1, String s2, String s3, String s4, String s5)
    {
        pool.submit(new m(this, s3, s1, s2, s, s4, s5));
    }

    public void statH5Jump(String s)
    {
        pool.submit(new j(this, s));
    }

    public void statIting(int i1, long l1, long l2, String s, String s1, 
            long l3, long l4)
    {
        pool.submit(new l(this, i1, l1, l2, s, s1, l3, l4));
    }

    public void statOfflineEvent()
    {
        if (events == null || events.size() == 0)
        {
            return;
        } else
        {
            pool.submit(new g(this));
            return;
        }
    }

    public void statOnlineAd(AdCollectData adcollectdata)
    {
        pool.submit(new h(this, adcollectdata));
    }


}
